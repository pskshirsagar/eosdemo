<?php

class CMessageOriginator extends CBaseMessageOriginator {

	const PS_NUMBER_1 		= 1;
	const PS_NUMBER_2		= 2;
	const PS_NUMBER_3		= 3;
	const PS_NUMBER_4		= 4;
	const PS_NUMBER_5		= 5;
	const PS_NUMBER_6		= 6;
	const PS_NUMBER_7		= 7;
	const PS_NUMBER_8		= 8;
	const HA_NUMBER			= 9;
	const PS_NUMBER_10		= 10;
	const PS_NUMBER_11		= 11;
	const PS_NUMBER_12		= 12;
	const PS_NUMBER_13		= 13;
	const ENTRATA_WHATSAPP_SANDBOX_NUMBER 			= 3123;

	const PUBLISHED			= 1;
	const UNPUBLISHED		= 0;

	const HOSTED_NUMBER_STATUS_COMPLETED = 'completed';
	const HOSTED_NUMBER_STATUS_FAILED	 = 'failed';

	public static $c_arrintTaskOriginatorIds = array(
		self::PS_NUMBER_10,
		self::PS_NUMBER_11,
		self::PS_NUMBER_12,
		self::PS_NUMBER_13
	);

}
?>