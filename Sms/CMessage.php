<?php

use Psi\Eos\Sms\CKeywords;
use Psi\Eos\Entrata\CPropertyProducts;
use Psi\Libraries\UtilConfig\CConfig;
use Psi\Libraries\Cryptography\CCrypto;
use Psi\Libraries\ExternalBitly\CBitly;

class CMessage extends CBaseMessage {

	const SMS_LENGTH                    = 160;
	const INFO_1_STATIC_CONTENT         = 'Info Msg. Rply STOP INFO to cancel msgs. HELP for help. Msg-Data rates may apply.';
	const INFO_2_STATIC_CONTENT         = 'Unlimited texts/mnth. PS Terms: http://bit.ly/2xBVD4h ';
	const REPLY_POLL_OPTION_MESSAGE     = 'Thank You. Your response has been received. Reply STOP to cancel msgs.';
	const REPLY_STOP_TO_STOP_MESSAGE    = ' Reply STOP to stop.';
	const REPLY_STOP_TO_STOP_MESSAGE_SPANISH = ' Responda STOP para cancelar.';
	const VISA_VALID_RESPONSE_MESSAGE   = 'Thank you for completing your payment.';
	const VISA_INVALID_RESPONSE_MESSAGE = 'Payment confirmation was not valid.';
	const ENTRATA_WEBSITE_URL           = 'https://www.entrata.com';
	const VALIDATE_SEND                 = 'send';
	const INDIA_EXTENSION               = '91';
	const HELP_LINE_NUMBER              = '1-877-826-9700';
	const SPAIN_HELP_LINE_NUMBER        = '34-900-431-289';
	const IRELAND_HELP_LINE_NUMBER      = '353-1800-938-580';
	const UK_HELP_LINE_NUMBER           = '44-808-164-8534';
	const FRANCE_HELP_LINE_NUMBER       = '33-805-08-03-81';
	const SMS_TITLE_US_LENGTH           = 25;
	const SMS_TITLE_IN_LENGTH           = 50;

	protected $m_objPropertyPhoneNumber;
	protected $m_objPropertyAddress;
	protected $m_objClientDatabase;
	protected $m_objDocumentManager;
	protected $m_objObjectStorageGateway;

	protected $m_strScheduledSendDate;
	protected $m_strScheduledSendTime;
	protected $m_strFileTypeSystemCode;

	protected $m_intSmsUserId;
	protected $m_intLeaseId;
	protected $m_intIsResidentSms;
	protected $m_intTaskId;
	protected $m_arrmixMetadata = [];

	protected $m_boolIsCheckForBlockedNumber = true;

	public function __construct() {
		parent::__construct();

		$this->m_strScheduledSendDate = date( 'm/d/Y' );

		return;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {

		$this->setMessageEncodingTypeId( CMessageEncodingType::TEXT );
		$this->setMessageAggregatorId( CMessageAggregator::TWILIO );
		$this->setMessageOriginatorId( CMessageOriginator::PS_NUMBER_1 );
		$this->setMessageStatusTypeId( CMessageStatusType::PENDING_SEND );
		$this->setMessagePriorityId( CMessagePriority::NORMAL );

		$this->setMessageDatetime( date( 'Y-m-d H:i:s' ) );
		$this->setIsInbound( 0 );

		return true;
	}

	public function setPropertyPhoneNumber( $objPropertyPhoneNumber ) {
		$this->m_objPropertyPhoneNumber = $objPropertyPhoneNumber;
	}

	public function setPropertyAddress( $objPropertyAddress ) {
		$this->m_objPropertyAddress = $objPropertyAddress;
	}

	// On message record we should always store phone number as numeric. PBY.

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = preg_replace( '/[^0-9]/', '', $strPhoneNumber );
		// This is for time being.
		if( 10 == \Psi\CStringService::singleton()->strlen( $this->m_strPhoneNumber ) ) {
			$this->m_strPhoneNumber = '1' . $this->m_strPhoneNumber;
		} elseif( 12 == \Psi\CStringService::singleton()->strlen( $this->m_strPhoneNumber ) && '91' == \Psi\CStringService::singleton()->substr( $this->m_strPhoneNumber, 0, 2 ) ) {
			$this->m_strPhoneNumber = '+' . $this->m_strPhoneNumber;
		} elseif( 10 < \Psi\CStringService::singleton()->strlen( $this->m_strPhoneNumber ) && '+' != \Psi\CStringService::singleton()->substr( $this->m_strPhoneNumber, 0, 1 ) ) {
			$this->m_strPhoneNumber = '+' . $this->m_strPhoneNumber;
		}
	}

	public function setScheduledSendDate( $strScheduledSendDate ) {
		$this->m_strScheduledSendDate = CStrings::strTrimDef( $strScheduledSendDate, -1, NULL, true );
	}

	public function setScheduledSendTime( $strScheduledSendTime ) {
		$this->m_strScheduledSendTime = CStrings::strTrimDef( $strScheduledSendTime, -1, NULL, true );
	}

	public function setScheduledSendDatetime( $strScheduledSendDatetime ) {
		parent::setScheduledSendDatetime( $strScheduledSendDatetime );
		$this->m_strScheduledSendDate = date( 'm/d/Y', strtotime( $strScheduledSendDatetime ) );
		$this->m_strScheduledSendTime = date( 'H', strtotime( $strScheduledSendDatetime ) );
	}

	public function setClientDatabase( $objClientDatabase ) {
		$this->m_objClientDatabase = $objClientDatabase;
	}

	public function resetScheduledSendDatetime() {
		if( 0 < strlen( $this->m_strScheduledSendDate ) && 0 < strlen( $this->m_strScheduledSendTime ) ) {
			parent::setScheduledSendDatetime( date( 'Y-m-d H:i:s', ( strtotime( $this->m_strScheduledSendDate ) + ( 3600 * ( int ) $this->m_strScheduledSendTime ) ) ) );
		}
	}

	public function setSmsUserId( $intSmsUserId ) {
		$this->m_intSmsUserId = $intSmsUserId;
	}

	public function setFileTypeSystemCode( $strFileTypeSystemCode ) {
		$this->m_strFileTypeSystemCode = $strFileTypeSystemCode;
	}

	public function setLeaseId( $intLeaseId ) {
		$this->m_intLeaseId = ( int ) $intLeaseId;
	}

	public function setIsResidentSms( $intIsResidentSms ) {
		$this->m_intIsResidentSms = ( int ) $intIsResidentSms;
	}

	public function setDocumentManager( $objDocumentManager ) {
		$this->m_objDocumentManager = $objDocumentManager;
	}

	public function setObjectStorageGateway( $objObjectStorageGateway ) {
		$this->m_objObjectStorageGateway = $objObjectStorageGateway;
	}

	public function setTaskId( $intTaskId ) {
		$this->m_intTaskId = $intTaskId;
	}

	public function setIsCheckForBlockedNumber( $boolIsCheckForBlockedNumber ) {
		$this->m_boolIsCheckForBlockedNumber = $boolIsCheckForBlockedNumber;
	}

	public function setMetadata( $arrmixMetadata ) {
		$this->m_arrmixMetadata = $arrmixMetadata;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['scheduled_send_date'] ) ) {
			$this->setScheduledSendDate( $arrmixValues['scheduled_send_date'] );
		}
		if( true == isset( $arrmixValues['scheduled_send_time'] ) ) {
			$this->setScheduledSendTime( $arrmixValues['scheduled_send_time'] );
		}

		return;
	}

	/**
	 * Get Functions
	 */

	public function getClientDatabase() {
		return $this->m_objClientDatabase;
	}

	public function getPropertyPhoneNumber() {
		return $this->m_objPropertyPhoneNumber;
	}

	public function getPropertyAddress() {
		return $this->m_objPropertyAddress;
	}

	public function getScheduledSendDate() {
		return $this->m_strScheduledSendDate;
	}

	public function getScheduledSendTime() {
		return $this->m_strScheduledSendTime;
	}

	public function getFileTypeSystemCode() {
		return $this->m_strFileTypeSystemCode;
	}

	public function getLeaseId() {
		return $this->m_intLeaseId;
	}

	public function getDocumentManager() {
		return $this->m_objDocumentManager;
	}

	public function getObjectStorageGateway() {
		return $this->m_objObjectStorageGateway;
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function getIsCheckForBlockedNumber() {
		return $this->m_boolIsCheckForBlockedNumber;
	}

	public function getMetadata() {
		return $this->m_arrmixMetadata;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchClient( $objAdminDatabase ) {
		return CClients::fetchClientById( $this->getCid(), $objAdminDatabase );
	}

	public function fetchProperty( $objAdminDatabase ) {
		return \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objAdminDatabase );
	}

	public function fetchMessageStatusType( $objSmsDatabase ) {
		return \Psi\Eos\Sms\CMessageStatusTypes::createService()->fetchMessageStatusTypeById( $this->getMessageStatusTypeId(), $objSmsDatabase );
	}

	public function fetchMessageAggregator( $objSmsDatabase ) {
		return \Psi\Eos\Sms\CMessageAggregators::createService()->fetchMessageAggregatorById( $this->getMessageAggregatorId(), $objSmsDatabase );
	}

	public function fetchMessageOperator( $objSmsDatabase ) {
		return \Psi\Eos\Sms\CMessageOperators::createService()->fetchMessageOperatorById( $this->getMessageOperatorId(), $objSmsDatabase );
	}

	public function fetchMessageOriginator( $objSmsDatabase ) {
		return \Psi\Eos\Sms\CMessageOriginators::createService()->fetchMessageOriginatorById( $this->getMessageOriginatorId(), $objSmsDatabase );
	}

	public function fetchMessageType( $objSmsDatabase ) {
		return \Psi\Eos\Sms\CMessageTypes::createService()->fetchMessageTypeById( $this->getMessageTypeId(), $objSmsDatabase );
	}

	public function fetchMessagePriority( $objSmsDatabase ) {
		return \Psi\Eos\Sms\CMessagePriorities::createService()->fetchMessagePriorityById( $this->getMessagePriorityId(), $objSmsDatabase );
	}

	public function fetchMessageEncodingType( $objSmsDatabase ) {
		return \Psi\Eos\Sms\CMessageEncodingTypes::createService()->fetchMessageEncodingTypeById( $this->getMessageEncodingTypeId(), $objSmsDatabase );
	}

	public function fetchMessageErrorType( $objSmsDatabase ) {
		return  \Psi\Eos\Sms\CMessageErrorTypes::createService()->fetchMessageErrorTypeById( $this->getMessageErrorTypeId(), $objSmsDatabase );
	}

	public function fetchCustomer( $objClientDatabase ) {
		return \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomerByIdByCid( $this->getCustomerId(), $this->getCid(), $objClientDatabase );
	}

	public function getSmsUserId() {
		return $this->m_intSmsUserId;
	}

	/**
	 * Create Functions
	 */

	public function createBlock() {

		$objBlock = new CBlock();

		$objBlock->setCid( $this->getCid() );
		$objBlock->setPropertyId( $this->getPropertyId() );
		$objBlock->setMessageId( $this->getId() );
		$objBlock->setPhoneNumber( $this->getPhoneNumber() );
		$objBlock->setBlockDatetime( date( 'Y-m-d H:i:s' ) );

		return $objBlock;
	}

	public function createReplyMessage() {

		$objReplyMessage = new CMessage();

		$objReplyMessage->setMessageId( $this->getId() );
		$objReplyMessage->setCid( $this->getCid() );
		$objReplyMessage->setMessageAggregatorId( CMessageAggregator::TWILIO );
		$objReplyMessage->setMessageOperatorId( $this->getMessageOperatorId() );
		$objReplyMessage->setMessageOriginatorId( $this->getMessageOriginatorId() );
		$objReplyMessage->setMessageTypeId( $this->getMessageTypeId() );
		$objReplyMessage->setMessageStatusTypeId( CMessageStatusType::PENDING_SEND );
		$objReplyMessage->setMessagePriorityId( $this->getMessagePriorityId() );
		$objReplyMessage->setMessageEncodingTypeId( $this->getMessageEncodingTypeId() );
		$objReplyMessage->setPropertyId( $this->getPropertyId() );
		$objReplyMessage->setRemotePrimaryKey( $this->getRemotePrimaryKey() );
		$objReplyMessage->setPhoneNumber( $this->getPhoneNumber() );
		$objReplyMessage->setMessageDatetime( date( 'Y-m-d H:i:s' ) );
		$objReplyMessage->setIsInbound( 0 );

		return $objReplyMessage;
	}

	public function createConfirmationMessage( $objDatabase, $intLeaseId = NULL ) {
		$objProperty = \Psi\Eos\Entrata\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objDatabase );
		if( false == valObj( $objProperty, CProperty::class ) ) {
			return false;
		}

		$boolIsResident = false;

		switch( $this->getMessageTypeId() ) {
			case CMessageType::APPLICATION_UPDATES:
			case CMessageType::AVAILABILITY_ALERT:
				$boolIsResident = false;
				break;

			case CMessageType::COMMUNITY_ANNOUNCEMENTS:
			case CMessageType::PROPERTY_NOTIFICATION:
			case CMessageType::MAINTENANCE_NOTIFICATION:
			case CMessageType::RENT_REMINDER:
			case CMessageType::PACKAGE_NOTIFICATION:
			case CMessageType::JOIN_ALL:
				$boolIsResident = true;
				break;

			case CMessageType::MARKETING_SMS_OPTIN_CONFIRMATION:
				$strKeyword = CMessageType::HANDLE_OPTIN_CONFIRMATION;
				break;

			default:
				return false;
		}

		if( CMessageType::MARKETING_SMS_OPTIN_CONFIRMATION == $this->getMessageTypeId() ) {
			$strMessage = __( 'Thanks for contacting {%s, 0}. You have requested to receive SMS notifications. Please respond "{%s, 1}" to stay opted in, or "STOP" to stop receiving these messages. Data Rates May Apply', [ $objProperty->getPropertyName(), $strKeyword ] );
		} else {
			$strLink = $this->generateConfirmationLink( $boolIsResident, $objDatabase, $intLeaseId );
			if( false == valStr( $strLink ) ) {
				return false;
			}
			$strMessage = __( 'Thanks for reaching out to {%s, 0}. Click the link to confirm: {%s, 1}', [ $objProperty->getPropertyName(), $strLink ] );
			$this->setMetadata( [ 'action' => 'insert' ] );
		}
		$this->setMessage( $strMessage );

		return true;
	}

	private function generateConfirmationLink( $boolIsResident, $objDatabase, $intLeaseId = NULL ) {
		$objClient = \Psi\Eos\Entrata\CClients::createService()->fetchClientById( $this->getCid(), $objDatabase );
		if( false == valObj( $objClient, CClient::class ) ) {
			return '';
		}

		$objOneTimeLink = new COneTimeLink();
		$intUniqueId = substr( \Psi\Libraries\Cryptography\CCrypto::createService()->encryptUrl( uniqid( mt_rand(), true ), CONFIG_SODIUM_KEY_ID ), 0, 240 );
		if( true == $boolIsResident ) {
			$intApplicantId = ( int ) \Psi\Eos\Entrata\CApplicants::createService()->fetchApplicantIdByLeaseIdByCustomerIdByCid( $intLeaseId, $this->getCustomerId(), $this->getCid(), $objDatabase );
		} else {
			$intApplicantId = $this->getApplicantId();
		}
		$objOneTimeLink->setValues( [ 'cid' => $this->getCid(), 'applicant_id' => $intApplicantId, 'key_encrypted' => $intUniqueId, 'is_resident' => $boolIsResident, 'ip_address' => $_SERVER['REMOTE_ADDR'] ] );

		if( false == $objOneTimeLink->insert( SYSTEM_USER_ID, $objDatabase ) ) {
			return '';
		}

		$strBaseUrl = $objClient->getEntrataUrl() . '/?module=subscriptionxxx&action=opt_in_text_message&key=';
		$strSmsConfirmationParameters = '&message_id=' . $this->getId() . '&cid=' . $this->getCid() . '&is_resident=' . $boolIsResident . '&unique_id=' . $intUniqueId . '&applicant_id=' . $intApplicantId;

		try {
			$objJwtToken = new \Psi\Libraries\UtilJwt\CJsonWebToken();
			$objJwtToken->setPayloadClaim( 'details', ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strSmsConfirmationParameters, CConfig::get( 'SODIUM_KEY_MESSAGE_CENTER_EMAIL' ) ) );
			$strJwtToken = $objJwtToken->generateToken( $objJwtToken::ALGO_HS256, CConfig::get( 'SODIUM_KEY_MESSAGE_CENTER_EMAIL' ), 157680000 );
			$strSmsConfirmationUrl  = $strBaseUrl . $strJwtToken;
		} catch( \Psi\Libraries\UtilJwt\CJsonWebTokenException $objJsonWebToken ) {
			$strSmsConfirmationUrl = '';
		} catch( \Psi\Libraries\Cryptography\Exceptions\CCryptoInvalidException $objCryptoException ) {
			$strSmsConfirmationUrl = '';
		}

		if( false == valStr( $strSmsConfirmationUrl ) ) {
			return '';
		}

		if( 'production' === CConfig::createService()->get( 'environment' ) ) {
			$objBitly = new CBitly( CCrypto::createService()->decrypt( CConfig::createService()->get( 'BITLY_USERNAME' ), CConfig::createService()->get( 'SODIUM_KEY_LOGIN_USERNAME' ) ), CCrypto::createService()->decrypt( CConfig::createService()->get( 'BITLY_API_KEY' ), CConfig::createService()->get( 'SODIUM_KEY_LOGIN_USERNAME' ) ) );
			return $objBitly->shorten( $strSmsConfirmationUrl );
		}

		return $strSmsConfirmationUrl;
	}

	/**
	 * Other Functions
	 */

	public function buildReplyMessages( $objSmsDatabase, $objClientDatabase, $objAdminDatabase, $objReceivedKeyword, $boolIsUnknownNumber = true ) {

		$arrobjReplyMessages = [];

		$arrstrKeyword = explode( ' ', \Psi\CStringService::singleton()->strtoupper( trim( preg_replace( '/[^a-zA-Z0-9 ]/', '', $this->getMessage() ) ) ) );

		if( 28444 == CMessageOriginator::PS_NUMBER_1 ) {
			array_shift( $arrstrKeyword ); // Use array shift for mblox test environment as it prefixes PTYSOL for each keyword.
		}

		$strKeyword           = '';
		$strMessageTypeHandle = array_pop( $arrstrKeyword );

		foreach( $arrstrKeyword as $strMessageKeyword ) {
			$strKeyword .= $strMessageKeyword . ' ';
		}

		switch( $this->getMessageTypeId() ) {
			case CMessageType::HELP:
				$objReply1Message = $this->createReplyMessage();
				$objReply1Message->setMessage( '(1/3)' . CONFIG_COMPANY_NAME . ' Alerts. Reply PROPERTY for keyword lookup, STOP [keyword] INFO for Property Info Msgs, STOP RENT for Rent Reminders.' );
				array_push( $arrobjReplyMessages, $objReply1Message );

				$objReply2Message = $this->createReplyMessage();
				$objReply2Message->setMessage( '(2/3)STOP ALERT for Availability Alerts, STOP FIX for Maintenance Updates, STOP NOTE for Property Updates, or STOP ALL to stop all communication.' );
				array_push( $arrobjReplyMessages, $objReply2Message );

				$objReply3Message = $this->createReplyMessage();
				$objReply3Message->setMessage( '(3/3)' . self::ENTRATA_WEBSITE_URL . '/help/. 3msgs/week. Msg&Data Rates May Apply. 1-877-826-9700' );
				array_push( $arrobjReplyMessages, $objReply3Message );
				break;

			case CMessageType::MENU:
				$objReply1Message = $this->createReplyMessage();
				$objReply1Message->setMessage( '(1/2)You are subscribed to a few programs.STOP INFO for Property Info Msgs.STOP ALERT for Availability Alerts' );
				array_push( $arrobjReplyMessages, $objReply1Message );

				$objReply2Message = $this->createReplyMessage();
				$objReply2Message->setMessage( '(2/2)STOP RENT for Rent Reminders.STOP FIX for Maintenance Updates.STOP NOTE for Property Updates.STOP ALL to stop all communication.' );

				array_push( $arrobjReplyMessages, $objReply2Message );
				break;

			case CMessageType::PROPERTY:
				$arrintPropertyIds = [];
				// Do not pick any keyword with client id and property id 1 as they are RESERVED keywords.
				$arrobjKeywords = CKeywords::createService()->fetchKeywords( 'SELECT DISTINCT ON( property_id ) * FROM keywords WHERE cid <> 1 AND property_id <> 1 AND char_length(keyword) < 9 LIMIT 3', $objSmsDatabase );

				if( false == valArr( $arrobjKeywords ) || 0 == \Psi\Libraries\UtilFunctions\count( $arrobjKeywords ) ) {
					break;
				}

				foreach( $arrobjKeywords as $objKeyword ) {
					$arrintPropertyIds[$objKeyword->getPropertyId()] = $objKeyword->getPropertyId();
				}

				if( true == valObj( $objAdminDatabase, 'CDatabase' ) ) {

					$arrobjProperties = \Psi\Eos\Entrata\CProperties::createService()->fetchCustomPropertiesByIds( $arrintPropertyIds, $objAdminDatabase );

					$intCount      = 1;
					$strMessageOne = '';
					$strMessageTwo = '';

					foreach( $arrobjKeywords as $objKeyword ) {
						if( true == array_key_exists( $objKeyword->getPropertyId(), $arrobjProperties ) ) {
							$objProperty = $arrobjProperties[$objKeyword->getPropertyId()];

							if( 1 == $intCount ) {
								$strMessageOne = '(1/2) ' . CONFIG_COMPANY_NAME . ' Alerts: Reply with keyword of desired property name. For example: 1) ' . $objProperty->getPropertyName() . ' = ' . $objKeyword->getKeyword();
							} else {
								$strMessageTwo .= '(2/2) ' . ( int ) $intCount . ') ' . $objProperty->getPropertyName() . ' = ' . $objKeyword->getKeyword() . ' . 3msgs/week. Msg&Data Rates May Apply. Text HELP for help, STOP to cancel msgs.';
							}
							$intCount++;
						}
					}

					if( true == isset( $strMessageOne ) && 1 < strlen( $strMessageOne ) ) {
						$objReply1Message = $this->createReplyMessage();
						$objReply1Message->setMessage( $strMessageOne );
						array_push( $arrobjReplyMessages, $objReply1Message );
					}

					if( true == isset( $strMessageTwo ) && 1 < strlen( $strMessageTwo ) ) {
						$objReply2Message = $this->createReplyMessage();
						$objReply2Message->setMessage( $strMessageTwo );
						array_push( $arrobjReplyMessages, $objReply2Message );
					}

				}
				break;

			case CMessageType::INFORMATION_REQUEST:
				$strUrl = NULL;

				$arrintResidentMessageTypeIds = [
					CMessageType::RESIDENT_COMMUNICATION,
					CMessageType::MAINTENANCE_NOTIFICATION,
					CMessageType::RENT_REMINDER,
					CMessageType::COMMUNITY_ANNOUNCEMENTS,
					CMessageType::PACKAGE_NOTIFICATION
				];

				// Why create two reply messages?
				$objReply1Message = $this->createReplyMessage();
				$objReply2Message = clone $objReply1Message;

				$objKeyword = CKeywords::createService()->fetchKeywordByKeyword( $this->getMessage(), $objSmsDatabase );

				if( false == valObj( $objKeyword, 'CKeyword' ) ) {
					$objReply1Message->setMessage( 'That keyword is not recognized. Please try again.' );
					array_push( $arrobjReplyMessages, $objReply1Message );
					break;
				}

				$objClientDatabase->open();
				$objProperty = $this->fetchProperty( $objClientDatabase );

				if( false == valObj( $objProperty, 'CProperty' ) ) {
					if( true == valStr( $objKeyword->getMatchedMessage() ) ) {
						$objReply1Message->setMessage( $objKeyword->getMatchedMessage() );
						array_push( $arrobjReplyMessages, $objReply1Message );
					}
					break;
				}

				if( true == valObj( $objReceivedKeyword, 'CKeyword' ) && true == in_array( $objReceivedKeyword->getMessageTypeId(), $arrintResidentMessageTypeIds ) ) {
					$arrobjReplyMessages = $this->processResidentKeyword( $objKeyword, $objProperty, $objReply1Message, $boolIsUnknownNumber );
					break;
				}

				if( true == isset( $objKeyword ) && false == is_null( $objKeyword->getMatchedMessage() ) ) {
					$strMessage = ( true == $boolIsUnknownNumber ) ? $objKeyword->getUnmatchedMessage() : $objKeyword->getMatchedMessage();
				} else {

					$objPropertyPhoneNumber = $this->getPropertyPhoneNumber();
					$objPropertyAddress     = $this->getPropertyAddress();

					$arrintAvailablePsProducts = [];

					if( true == valObj( $objProperty, 'CProperty' ) ) {
						$objWebsite                = $objProperty->fetchEnabledDefaultWebsite( $objClientDatabase );
						$arrintAvailablePsProducts = CPropertyProducts::createService()->fetchPsProductIdsByPropertyIdsByCid( [ $objProperty->getId() ], $objProperty->getCid(), $this->m_objClientDatabase );
					}

					if( true == valObj( $objWebsite, 'CWebsite' ) && false == is_null( $objWebsite->getWebsiteDomain() ) ) {
						$arrstrWebsitePreferenceKeys = [ 'CITY_SEO_FORMAT_EXTENSION', 'PROPERTY_SEO_FORMAT_EXTENSION' ];
						$arrobjWebsitePreferences    = $objWebsite->fetchWebsitePreferencesByKeys( $arrstrWebsitePreferenceKeys, $objClientDatabase );

						if( true == valArr( $arrobjWebsitePreferences ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrobjWebsitePreferences ) ) {
							$arrobjWebsitePreferences = rekeyObjects( 'Key', $arrobjWebsitePreferences );

							( true == array_key_exists( 'CITY_SEO_FORMAT_EXTENSION', $arrobjWebsitePreferences ) && false == is_null( $arrobjWebsitePreferences['CITY_SEO_FORMAT_EXTENSION']->getValue() ) ) ? $strCitySeoExtension = '-' . $arrobjWebsitePreferences['CITY_SEO_FORMAT_EXTENSION']->getValue() : $strCitySeoExtension = NULL;
							( true == array_key_exists( 'PROPERTY_SEO_FORMAT_EXTENSION', $arrobjWebsitePreferences ) && false == is_null( $arrobjWebsitePreferences['PROPERTY_SEO_FORMAT_EXTENSION']->getValue() ) ) ? $strPropertySeoExtension = '-' . $arrobjWebsitePreferences['PROPERTY_SEO_FORMAT_EXTENSION']->getValue() : $strPropertySeoExtension = NULL;

							$strExtensionUrl = '/' . str_replace( ' ', '-', $objProperty->getOrFetchSeoCity( $objClientDatabase ) ) . $strCitySeoExtension . '/' . str_replace( ' ', '-', $objProperty->getPropertyName() ) . $strPropertySeoExtension . '/';

						} else {
							$strExtensionUrl = '/Apartments/module/property_info/property[id]/' . $objProperty->getId() . '/';
						}

						$strUrl = $strBaseName = 'http://' . $objWebsite->getWebsiteDomain() . $strExtensionUrl;
						if( 'production' === CConfig::createService()->get( 'environment' ) ) {
							$objBitly = new CBitly( CCrypto::createService()->decrypt( CConfig::createService()->get( 'BITLY_USERNAME' ), CConfig::createService()->get( 'SODIUM_KEY_LOGIN_USERNAME' ) ), CCrypto::createService()->decrypt( CConfig::createService()->get( 'BITLY_API_KEY' ), CConfig::createService()->get( 'SODIUM_KEY_LOGIN_USERNAME' ) ) );
							$strUrl = $objBitly->shorten( $strBaseName );
						}
					}

					$objClientDatabase->close();

					$strMessage = '';

					if( true == valObj( $objPropertyPhoneNumber, 'CPropertyPhoneNumber' ) ) {
						( false == is_null( $objPropertyPhoneNumber->getPhoneNumber() ) ) ? $strMessage .= 'Ph: ' . $objPropertyPhoneNumber->getPhoneNumber() . ' - ' : $strMessage .= '';
					}

					( false == is_null( $strUrl ) && 0 < strlen( trim( $strUrl ) ) ) ? $strMessage .= 'Visit Our Website: ' . $strUrl : $strMessage .= NULL;

					if( true == valObj( $objPropertyAddress, 'CPropertyAddress' ) ) {
						( false == is_null( $objPropertyAddress->getStreetLine1() ) ) ? $strMessage .= ' ' . $objPropertyAddress->getStreetLine1() : $strMessage .= '';
						( false == is_null( $objPropertyAddress->getStreetLine2() ) ) ? $strMessage .= ' ' . $objPropertyAddress->getStreetLine2() : $strMessage .= '';
						( false == is_null( $objPropertyAddress->getStreetLine3() ) ) ? $strMessage .= ' ' . $objPropertyAddress->getStreetLine3() : $strMessage .= '';
						( false == is_null( $objPropertyAddress->getCity() ) ) ? $strMessage .= ' ' . $objPropertyAddress->getCity() : $strMessage .= '';
						( false == is_null( $objPropertyAddress->getStateCode() ) ) ? $strMessage .= ' ' . $objPropertyAddress->getStateCode() : $strMessage .= '';
						( false == is_null( $objPropertyAddress->getPostalCode() ) ) ? $strMessage .= ' ' . $objPropertyAddress->getPostalCode() : $strMessage .= '';
					}
				}

				// This is really poorly programmed.
				// You should always loop and create messages.
				// What if reply 3 message doesn't hold the overage text?  Then you have to go to four messages.  Should be in a loop.

				$strInfoMessage = '(1/2) ' . trim( \Psi\CStringService::singleton()->strtoupper( $objProperty->getPropertyName() ) ) . ' ' . self::INFO_1_STATIC_CONTENT . ' ' . self::INFO_2_STATIC_CONTENT;

				if( 160 < strlen( $strInfoMessage ) ) {

					if( 155 < strlen( $strMessage ) ) {

						$objReply1Message->setMessage( '(1/4) ' . trim( \Psi\CStringService::singleton()->strtoupper( $objProperty->getPropertyName() ) ) . ' ' . self::INFO_1_STATIC_CONTENT );
						array_push( $arrobjReplyMessages, $objReply1Message );

						$objReply2Message->setMessage( '(2/4) ' . self::INFO_2_STATIC_CONTENT );
						array_push( $arrobjReplyMessages, $objReply2Message );

						$objReply3Message = $this->createReplyMessage();
						$objReply3Message->setMessage( '(3/4) ' . trim( \Psi\CStringService::singleton()->substr( $strMessage, 0, 153 ) ) );
						array_push( $arrobjReplyMessages, $objReply3Message );

						$objReply4Message = $this->createReplyMessage();
						$objReply4Message->setMessage( '(4/4)' . trim( \Psi\CStringService::singleton()->substr( $strMessage, 154 ) ) );
						array_push( $arrobjReplyMessages, $objReply4Message );

					} else {

						$objReply1Message->setMessage( '(1/3) ' . trim( \Psi\CStringService::singleton()->strtoupper( $objProperty->getPropertyName() ) ) . ' ' . self::INFO_1_STATIC_CONTENT );
						array_push( $arrobjReplyMessages, $objReply1Message );

						$objReply2Message->setMessage( '(2/3) ' . self::INFO_2_STATIC_CONTENT );
						array_push( $arrobjReplyMessages, $objReply2Message );

						$objReply3Message = $this->createReplyMessage();
						$objReply3Message->setMessage( '(3/3)' . trim( $strMessage ) );
						array_push( $arrobjReplyMessages, $objReply3Message );

					}

				} else {

					if( 155 < strlen( $strMessage ) ) {

						$objReply1Message->setMessage( '(1/3) ' . trim( \Psi\CStringService::singleton()->strtoupper( $objProperty->getPropertyName() ) ) . ' ' . self::INFO_1_STATIC_CONTENT . ' ' . self::INFO_2_STATIC_CONTENT );
						array_push( $arrobjReplyMessages, $objReply1Message );

						$objReply2Message->setMessage( '(2/3) ' . trim( \Psi\CStringService::singleton()->substr( $strMessage, 0, 153 ) ) );
						array_push( $arrobjReplyMessages, $objReply2Message );

						$objReply3Message = $this->createReplyMessage();
						$objReply3Message->setMessage( '(3/3)' . trim( \Psi\CStringService::singleton()->substr( $strMessage, 154 ) ) );
						array_push( $arrobjReplyMessages, $objReply3Message );

					} else {

						$objReply1Message->setMessage( $strInfoMessage );
						array_push( $arrobjReplyMessages, $objReply1Message );

						$objReply2Message->setMessage( '(2/2)' . trim( $strMessage ) );
						array_push( $arrobjReplyMessages, $objReply2Message );
					}
				}
				break;

			case CMessageType::STOP:
				$objReply1Message = $this->createReplyMessage();

				// If The request is just STOP, then stop all the services
				// If the request is STOP followed by one of the message type like INFO, FIX, NOTE, RENT, ALERT, APPLICATION UPDATES.
				$strStopType = NULL;
				switch( $strMessageTypeHandle ) {
					case CMessageType::HANDLE_AVAILABILITY_ALERT:
						$strStopType = ' Availability Alerts';
						break;

					case CMessageType::HANDLE_INFORMATION_REQUEST:
						$strStopType = ' Property Information & Application Update Msgs';
						break;

					case CMessageType::HANDLE_MAINTENANCE_NOTIFICATION:
						$strStopType = ' Maintenance Updates';
						break;

					case CMessageType::HANDLE_PROPERTY_NOTIFICATION:
						$strStopType = ' Property Updates';
						break;

					case CMessageType::HANDLE_RENT_REMINDER:
						$strStopType = ' Rent Reminders';
						break;

					case CMessageType::HANDLE_APPLICATION_UPDATES:
						$strStopType = ' Application Updates';
						break;

					case CMessageType::HANDLE_PACKAGE_NOTIFICATION:
						$strStopType = ' Parcel Alerts';
						break;

					default:
						$strStopType = ' all communication';
				}

				$objReply1Message->setMessage( '(1/2)You\'ve opted out of' . $strStopType . ' with ' . CONFIG_COMPANY_NAME . '. No additional messages will be sent.' );
				array_push( $arrobjReplyMessages, $objReply1Message );

				$objReply2Message = $this->createReplyMessage();
				$objReply2Message->setMessage( '(2/2)For more information visit ' . self::ENTRATA_WEBSITE_URL . '/help/. 1-877-826-9700' );
				array_push( $arrobjReplyMessages, $objReply2Message );
				break;

			case CMessageType::ADMIN:
				// The option in the if loop are specifically for Mblox certification purpose. These will be removed once we get certified.

				$objReplyMessage = $this->createReplyMessage();
				$objReplyMessage->setMessage( 'Welcome to ' . CONFIG_COMPANY_NAME . '! Msg&Data Rates May Apply. Get unlimited texts/month. Reply HELP for help. Reply STOP to cancel msgs. T&Cs: http://bit.ly/2xBVD4h' );
				array_push( $arrobjReplyMessages, $objReplyMessage );

				$objKeyword = CKeywords::createService()->fetchKeywordByKeyword( $this->getMessage(), $objSmsDatabase );

				if( true == isset( $objKeyword ) && false == is_null( $objKeyword->getMatchedMessage() ) ) {

					$intNumberOfMessages     = ceil( strlen( $objKeyword->getMatchedMessage() ) / self::SMS_LENGTH );
					$intStartMessagePosition = 0;

					while( 0 < $intNumberOfMessages ) {

						$strMessage       = \Psi\CStringService::singleton()->substr( $objKeyword->getMatchedMessage(), $intStartMessagePosition, self::SMS_LENGTH - 1 );
						$objReply2Message = $this->createReplyMessage();

						$objReply2Message->setOptInId( $this->getId() );
						$objReply2Message->setMessage( $strMessage );

						array_push( $arrobjReplyMessages, $objReply2Message );

						$intNumberOfMessages--;
						$intStartMessagePosition += strlen( $strMessage );
					}
				}
				break;

			case CMessageType::POLL:
				$objReplyMessage = $this->createReplyMessage();

				// The options in the if loop are specifically for Mblox certification purpose. These will be removed once we get certified.
				if( 'POLL VANILLA' == $this->getMessage() || 'POLL CHOCOLATE' == $this->getMessage() || 'POLL STRAWBERRY' == $this->getMessage() || 'POLL OTHER' == $this->getMessage() ) {
					$objReplyMessage->setMessage( CONFIG_COMPANY_NAME . ' Voting: Thanks for voting! Msg and Data Rates May Apply. Reply STOP to cancel msgs. Reply HELP for info.' );
				} else {
					$objReplyMessage->setMessage( self::REPLY_POLL_OPTION_MESSAGE );
				}
				array_push( $arrobjReplyMessages, $objReplyMessage );
				break;

			case CMessageType::PAPERLESS:
			case CMessageType::SCHEDULED_PAYMENT:
			case CMessageType::MARKETING_SMS_OPTIN_CONFIRMATION:
			case CMessageType::JOIN_ALL:
			case CMessageType::APPLICATION_UPDATES:
			case CMessageType::AVAILABILITY_ALERT:
			case CMessageType::COMMUNITY_ANNOUNCEMENTS:
			case CMessageType::RENT_REMINDER:
			case CMessageType::MAINTENANCE_NOTIFICATION:
			case CMessageType::PACKAGE_NOTIFICATION:
				break;

			case CMessageType::TICKET_NOTIFICATION:
				$objAdminDatabase = CDatabases::loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::ADMIN, CDatabaseUserType::PS_DEVELOPER );

				$arrintMessageTypeIds      = [ CMessageType::TICKET_NOTIFICATION, CMessageType::STOP ];
				$arrintBlockedPhoneNumbers = \Psi\Eos\Sms\CBlocks::createService()->fetchBlockedPhoneNumbersByMobileNumberByMessageTypeIds( $this->getPhoneNumber(), $arrintMessageTypeIds, $objSmsDatabase );

				if( false == valArr( $arrintBlockedPhoneNumbers ) ) {
					if( CMessageStatusType::FAILED == $this->getMessageStatusTypeId() ) {

						$strMessage = 'Sorry, ';

						// add taskid from task reference.
						$intTaskReferenceNumber = $this->getId();

						$objCTaskReference = \Psi\Eos\Admin\CTaskReferences::createService()->fetchTaskReferenceByTaskReferenceTypeIdByReferenceNumber( CTaskReferenceType::TICKET_NOTIFICATION, $intTaskReferenceNumber, $objAdminDatabase );
						if( true == valObj( $objCTaskReference, 'CTaskReference' ) ) {
							$strMessage .= 'Ticket ' . $objCTaskReference->getTaskId() . ' is not subscribed with your number.';
						} else {
							$strMessage .= 'Unable to recognize ticket id.';
						}

						$arrobjTaskDetails = \Psi\Eos\Admin\CTaskDetails::createService()->fetchTaskDetailsBySmsNumberBySmsEnabledByLimit( $this->getPhoneNumber(), $objAdminDatabase, 3 );

						if( true == valArr( $arrobjTaskDetails ) ) {
							foreach( $arrobjTaskDetails as $objTaskDetail ) {
								$strTaskIds .= $objTaskDetail->getTaskId() . ' ';
							}
							$strMessage .= ' Your number is associated with ticket id\'s ' . $strTaskIds;
						}

					} elseif( CMessageStatusType::CONFIRMED == $this->getMessageStatusTypeId() ) {

						// Get taskid from task reference.
						$intTaskReferenceNumber = $this->getId();
						$objCTaskReference      = \Psi\Eos\Admin\CTaskReferences::createService()->fetchTaskReferenceByTaskReferenceTypeIdByReferenceNumber( CTaskReferenceType::TICKET_NOTIFICATION, $intTaskReferenceNumber, $objAdminDatabase );

						switch( $this->getKeywordId() ) {
							case CKeyword::ID_STOPTICKET:
								$strMessage = 'Sms Notifications for ticket #' . $objCTaskReference->getTaskId() . ' is stopped.';
								break;

							case CKeyword::ID_STOPALLTICKET:
								$strMessage = 'All  SMS ticket notifications have been stopped for your number ' . $this->getPhoneNumber() . '.';
								break;

							case CKeyword::ID_REOPENTICKET:
								$strMessage = 'Ticket  #' . $objCTaskReference->getTaskId() . '  has been reopened.';
								break;

							default:
								// default case
								break;
						}
					}

					// To check whether we already sent terms & condition message to number, else add that as well
					$objReplyMessage = $this->createReplyMessage();
					$arrobjMessages  = \Psi\Eos\Sms\CMessages::createService()->fetchMessagesByPhoneNumberByMessageTypeIdByMessageStatusTypeId( $this->getPhoneNumber(), CMessageType::TICKET_NOTIFICATION, CMessageStatusType::SENT, $objSmsDatabase );
					if( false == valArr( $arrobjMessages ) ) {
						$strMessage = '(1/2)' . $strMessage;
						$objReplyMessage->setMessage( $strMessage );
						array_push( $arrobjReplyMessages, $objReplyMessage );

						$objReply1Message = $this->createReplyMessage();
						$objReply1Message->setMessage( '(2/2) Msg and data rates may apply. Terms and Conditions ' . self::ENTRATA_WEBSITE_URL . '/help. Pwrd by \n' . CONFIG_COMPANY_NAME );
						array_push( $arrobjReplyMessages, $objReply1Message );
					} else {
						$objReplyMessage->setMessage( $strMessage );
						array_push( $arrobjReplyMessages, $objReplyMessage );
					}
				}
				break;

			case CMessageType::UNRECOGNIZED:
				$objReplyMessage = $this->createReplyMessage();
				$objReplyMessage->setMessage( '(1/2)Sorry, we did not recognize that keyword. Text HELP for HELP. Reply STOP to cancel receiving alerts.' );
				array_push( $arrobjReplyMessages, $objReplyMessage );

				$objReply1Message = $this->createReplyMessage();
				$objReply1Message->setMessage( '(2/2)For more information visit ' . self::ENTRATA_WEBSITE_URL . '/help/. 1-877-826-9700' );
				array_push( $arrobjReplyMessages, $objReply1Message );
				break;

			case CMessageType::TASK_NOTIFICATION:
				$objReply1Message = $this->createReplyMessage();
				$objReply1Message->setMessage( 'Sorry, we did not recognize that keyword. For more information visit ' . self::ENTRATA_WEBSITE_URL . '/help/. ' . self::HELP_LINE_NUMBER );
				array_push( $arrobjReplyMessages, $objReply1Message );
				break;

			case CMessageType::SMS_CHAT:
			default:
				trigger_error( 'Message Type was not found.', E_USER_WARNING );

				return false;
		}

		return $arrobjReplyMessages;
	}

	public function appendReplyStopMessage( $strLocaleCode = CLocale::DEFAULT_LOCALE ) {

		$boolAppendStopMessage   = false;
		$intAllowedMessageLength = self::SMS_LENGTH - 20;

		if( 0 == ( strlen( trim( $this->getMessage() ) ) ) ) {
			$boolAppendStopMessage = false;
		} elseif( true == \Psi\CStringService::singleton()->strstr( $this->getMessage(), self::REPLY_STOP_TO_STOP_MESSAGE ) || CMessageType::EMERGENCY == $this->getMessageTypeId() ) {
			$boolAppendStopMessage = false;
		} elseif( 0 < strlen( trim( $this->getMessage() ) ) && $intAllowedMessageLength >= strlen( trim( $this->getMessage() ) ) ) {
			$boolAppendStopMessage = true;
		}

		if( true == $boolAppendStopMessage ) {
			if( $strLocaleCode == CLocale::SPANISH_ENGLISH ) {
				$this->setMessage( trim( $this->getMessage() ) . self::REPLY_STOP_TO_STOP_MESSAGE_SPANISH );
			} else {
				$this->setMessage( trim( $this->getMessage() ) . self::REPLY_STOP_TO_STOP_MESSAGE );
			}
		}

		return;
	}

	public function processResidentKeyword( $objKeyword, $objProperty, $objReply1Message, $boolIsUnknownNumber ) {

		$arrobjReplyMessages = [];

		$strInfoMessage   = '(1/2) ' . trim( \Psi\CStringService::singleton()->strtoupper( $objProperty->getPropertyName() ) ) . ' ' . self::INFO_1_STATIC_CONTENT . ' ' . self::INFO_2_STATIC_CONTENT;
		$objReply2Message = clone $objReply1Message;

		if( true == $boolIsUnknownNumber ) {
			$strMessage = $objKeyword->getUnmatchedMessage();
		} else {
			$strMessage = $objKeyword->getMatchedMessage();
		}

		if( 160 < strlen( $strInfoMessage ) ) {

			if( 155 < strlen( $strMessage ) ) {

				$objReply1Message->setMessage( '(1/4) ' . trim( \Psi\CStringService::singleton()->strtoupper( $objProperty->getPropertyName() ) ) . ' ' . self::INFO_1_STATIC_CONTENT );
				array_push( $arrobjReplyMessages, $objReply1Message );

				$objReply2Message->setMessage( '(2/4) ' . self::INFO_2_STATIC_CONTENT );
				array_push( $arrobjReplyMessages, $objReply2Message );

				$objReply3Message = $this->createReplyMessage();
				$objReply3Message->setMessage( '(3/4) ' . trim( \Psi\CStringService::singleton()->substr( $strMessage, 0, 153 ) ) );
				array_push( $arrobjReplyMessages, $objReply3Message );

				$objReply4Message = $this->createReplyMessage();
				$objReply4Message->setMessage( '(4/4)' . trim( \Psi\CStringService::singleton()->substr( $strMessage, 154 ) ) );
				array_push( $arrobjReplyMessages, $objReply4Message );

			} else {

				$objReply1Message->setMessage( '(1/3) ' . trim( \Psi\CStringService::singleton()->strtoupper( $objProperty->getPropertyName() ) ) . ' ' . self::INFO_1_STATIC_CONTENT );
				array_push( $arrobjReplyMessages, $objReply1Message );

				$objReply2Message->setMessage( '(2/3) ' . self::INFO_2_STATIC_CONTENT );
				array_push( $arrobjReplyMessages, $objReply2Message );

				$objReply3Message = $this->createReplyMessage();
				$objReply3Message->setMessage( '(3/3)' . trim( $strMessage ) );
				array_push( $arrobjReplyMessages, $objReply3Message );

			}

		} else {

			if( 155 < strlen( $strMessage ) ) {

				$objReply1Message->setMessage( '(1/3) ' . trim( \Psi\CStringService::singleton()->strtoupper( $objProperty->getPropertyName() ) ) . ' ' . self::INFO_1_STATIC_CONTENT . ' ' . self::INFO_2_STATIC_CONTENT );
				array_push( $arrobjReplyMessages, $objReply1Message );

				$objReply2Message->setMessage( '(2/3) ' . trim( \Psi\CStringService::singleton()->substr( $strMessage, 0, 153 ) ) );
				array_push( $arrobjReplyMessages, $objReply2Message );

				$objReply3Message = $this->createReplyMessage();
				$objReply3Message->setMessage( '(3/3)' . trim( \Psi\CStringService::singleton()->substr( $strMessage, 154 ) ) );
				array_push( $arrobjReplyMessages, $objReply3Message );

			} else {

				$objReply1Message->setMessage( $strInfoMessage );
				array_push( $arrobjReplyMessages, $objReply1Message );

				$objReply2Message->setMessage( '(2/2)' . trim( $strMessage ) );
				array_push( $arrobjReplyMessages, $objReply2Message );
			}
		}

		return $arrobjReplyMessages;
	}

	public function insertResidentEventsForSms( $objProperty, $objClientDatabase ) {

		$strMobileNumber = $this->getPhoneNumber();

		if( 10 < strlen( $this->getPhoneNumber() ) && $this->getPhoneNumber()[0] == 1 ) {
			$strMobileNumber = \Psi\CStringService::singleton()->substr( $this->getPhoneNumber(), 1 );
		}

		$arrobjCustomers = ( array ) \Psi\Eos\Entrata\CCustomers::createService()->fetchCustomersByMobileNumberByCidByPropertyId( $strMobileNumber, $this->getCid(), $objProperty->getId(), $objClientDatabase );

		if( false == valArr( $arrobjCustomers ) ) {
			return;
		}

		foreach( $arrobjCustomers as $objCustomer ) {

			// Create event for SMS_INCOMING
			$objEventLibrary = new CEventLibrary();

			$objEventLibraryDataObject = $objEventLibrary->getEventLibraryDataObject();
			$objEventLibraryDataObject->setDatabase( $objClientDatabase );
			$objEventLibraryDataObject->setCustomer( $objCustomer );

			$objEvent = $objEventLibrary->createEvent( [ $objCustomer ], CEventType::SMS_INCOMING );
			$objEvent->setCid( $objCustomer->getCid() );
			$objEvent->setNotes( $this->getMessage() );
			$objEvent->setDataReferenceId( $this->getId() );
			$objEvent->setReference( $this );
			$objEvent->setEventDatetime( 'NOW()' );

			$objEventLibrary->buildEventDescription( $objCustomer );

			if( false == $objEventLibrary->insertEvent( SYSTEM_USER_ID, $objClientDatabase ) ) {
				continue;
			}
		}

		return;
	}

	public function insert( $intCurrentUserId, $objDatabase = NULL, $boolReturnSqlOnly = false, $boolIsSkipQueue = true ) {

		if( true == $boolReturnSqlOnly ) {
			return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		if( false == $boolIsSkipQueue ) {
			$boolIsValid = $this->enqueue( $intCurrentUserId, $objDatabase );

			if( true == $this->m_intIsResidentSms && false == is_null( $this->m_objDocumentManager ) ) {
				$this->insertDocument( $intCurrentUserId );
			}

			return $boolIsValid;
		}

		parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == $this->m_intIsResidentSms ) {
			$this->insertDocument( $intCurrentUserId );
		}

		return true;
	}

	// this function will store the email in documents

	public function insertDocument( $intCurrentUserId ) {
		$objFileLibrary = new CFileLibrary( $this->m_intCid );

		$objFileLibrary->setLeaseId( $this->m_intLeaseId );
		$objFileLibrary->setPropertyId( $this->m_intPropertyId );
		$objFileLibrary->setFileTypeSystemCode( $this->m_strFileTypeSystemCode );
		$objFileLibrary->setDocumentManager( $this->m_objDocumentManager );
		$objFileLibrary->setCompanyUserId( $intCurrentUserId );
		$objFileLibrary->setMessage( $this );
		$objFileLibrary->setClientDatabase( $this->m_objClientDatabase );

		$objFileLibrary->insertDocument();

		return true;
	}

	public function createTicketNotificationMessages( $objMessage, $objTask, $objTaskDetail, $boolIsTermAndConditionSmsAlreadySent ) {

		$objMessage->setDefaults();
		$objMessage->setPropertyId( $objTask->getPropertyId() );

		$objMessage->setMessageTypeId( CMessageType::TICKET_NOTIFICATION );
		$strMessage = ( ( false == $boolIsTermAndConditionSmsAlreadySent ) ? '(1/2)' : '' ) . ' Ticket#' . $objTask->getId() . '( ' . truncate( trim( $objTask->getTitle() ), 22 ) . ' ) was closed. Reply "REOPENTICKET ' . $objTask->getId() . '", "STOPTICKET ' . $objTask->getId() . '", "STOPALLTICKET", "STOP" to unsubscribe.';

		$objMessage->setMessage( $strMessage );
		$objMessage->setPhoneNumber( trim( $objTaskDetail->getSmsNumber() ) );

		$arrobjPushMessages = [];

		array_push( $arrobjPushMessages, $objMessage );

		if( false == $boolIsTermAndConditionSmsAlreadySent ) {

			$objCloneMessage = clone $objMessage;
			$strMessage      = '(2/2) Msg and data rates may apply. Terms and Conditions ' . self::ENTRATA_WEBSITE_URL . '/help. Pwrd by ' . CONFIG_COMPANY_NAME . '.';
			$objCloneMessage->setMessage( $strMessage );
			array_push( $arrobjPushMessages, $objCloneMessage );
		}

		return $arrobjPushMessages;

	}

	public function createTaskNotificationMessages( $objMessage, $objTask, $objPsProduct, $objPsProductOption, $intEmployeeId, $intPhoneNumber, $strEmployeeCountryCode, $objAdminDatabase, $boolEscalationMessage = false, $objSmsDatabase, $boolAllDoe = false, $intTaskReferenceTypeId = CTaskReferenceType::SMS_TPM, $boolChainEnd = false, $boolResolve = false, $intHierarchyLevel = NULL, $objTaskReference = NULL ) {

		$objMessage->setDefaults();
		$objMessage->setTaskId( $objTask->getId() );
		if( true == valObj( $objTaskReference, CTaskReference::class ) ) {
			$objMessage->setTaskReferenceId( $objTaskReference->getId() );
		}
		$objMessage->setMessageTypeId( CMessageType::TASK_NOTIFICATION );
		$objMessage->setEmployeeId( $intEmployeeId );
		$strTaskTitle = ( CCountry::CODE_USA == $strEmployeeCountryCode ) ? truncate( $objTask->getTitle(), self::SMS_TITLE_US_LENGTH, false ) : truncate( $objTask->getTitle(), self::SMS_TITLE_IN_LENGTH, false ); // truncating the title to 50 character

		if( true == $boolChainEnd ) {
			$intDoeEmployeeId = ( true == valObj( $objPsProductOption, 'CPsProductOption' ) ) ? $objPsProductOption->getDoeEmployeeId() : $objPsProduct->getDoeEmployeeId();
			$intPoEmployeeId = ( true == valObj( $objPsProductOption, 'CPsProductOption' ) ) ? $objPsProductOption->getProductOwnerEmployeeId() : $objPsProduct->getProductOwnerEmployeeId();

			$arrmixEmployees = CEmployees::fetchEmployeesByEmployeeIds( array_filter( array( $intDoeEmployeeId, $intPoEmployeeId ) ), $objAdminDatabase );
			$strFinalEscalatedText = ( ( true == valArr( $arrmixEmployees, 2, true ) ) ? '(Escalted to ' . current( $arrmixEmployees )['preferred_name'] . ' & ' . $arrmixEmployees[1]['preferred_name'] . ')' : ( ( true == valArr( $arrmixEmployees, 1, true ) ) ? '(Escalted to ' . current( $arrmixEmployees )['preferred_name'] . ')' : '' ) );
		}

		include_once PATH_LIBRARIES_PSI . 'Sms/CSmsTaskLibrary.class.php';

		if( CTaskPriority::IMMEDIATE == $objTask->getTaskPriorityId() ) {
			// send to SDM
			( CCountry::CODE_USA == $strEmployeeCountryCode ) ? $objMessage->setPhoneNumber( trim( $intPhoneNumber ) ) : $objMessage->setPhoneNumber( self::INDIA_EXTENSION . trim( $intPhoneNumber ) );
			$strAccessKey = $objTask->generateAccessKey( $intEmployeeId, $objAdminDatabase );
			$strUrl       = CONFIG_INSECURE_HOST_PREFIX . 'clientadmin.' . CONFIG_COMPANY_BASE_DOMAIN . '/?module=authentication-new&action=view_sms_reply&task_id=' . $objTask->getId() . '&employee_id=' . ( int ) $intEmployeeId . '&access_token=' . $strAccessKey . '&reference_id=' . ( int ) $intTaskReferenceTypeId;
			$strHierarchyLevel = ( true == is_numeric( $intHierarchyLevel ) ) ? ' You are receiving this SMS for hierarchy level ' . $intHierarchyLevel : '';
			if( false == $boolResolve ) {
				if( CCountry::CODE_USA == $strEmployeeCountryCode ) {
					$strUrl           .= '&is_details=true';
					$strShortUrl      = $objTask->urlShortner( $strUrl );
					$strAllDoeText    = ( true == $boolAllDoe ) ? ' (Escalated to all US directors) Reply:' : ( true == $boolChainEnd && true == valStr( $strFinalEscalatedText ) ) ? $strFinalEscalatedText : 'Reply:1 for ACK,';
					$strEscalatedText = $objTask->getEscalatedEmployeeName( $objPsProduct, $objPsProductOption, $objAdminDatabase, $intTaskReferenceTypeId );
					$strEscText       = ( false == $boolAllDoe && false == $boolChainEnd ) ? ', 3 for ESCALATE To ' . $strEscalatedText : '';
					$strMessage       = 'ENTRATA IMMEDIATE TASK #' . $objTask->getId() . '(' . $strTaskTitle . '). ' . $strAllDoeText . ' 2 for RESOLVE' . $strEscText . ', Details ' . $strShortUrl . $strHierarchyLevel;
				} else {
					$strShortUrl = $objTask->urlShortner( $strUrl );
					$strMessage  = ( true == valStr( $strShortUrl ) ) ? 'ENTRATA IMMEDIATE TASK #' . $objTask->getId() . '(' . $strTaskTitle . ').  Please click here to respond ' . $strShortUrl . $strHierarchyLevel : '';
				}
			} else {
				$strUrl           .= '&is_details=true';
				$strShortUrl      = $objTask->urlShortner( $strUrl );
				$strMessage  = 'ENTRATA IMMEDIATE TASK #' . $objTask->getId() . '(' . $strTaskTitle . '). is Resolved.' . ' Details ' . $strShortUrl . $strHierarchyLevel;
			}
			$objTaskSmsLibrary = new CSmsTaskLibrary();
			$intOriginatorId   = $objTaskSmsLibrary->loadOriginatorId( $objMessage, $objSmsDatabase, $objAdminDatabase );
			$objMessage->setMessageOriginatorId( $intOriginatorId );
		} else {
			$strProductName = ( true == valObj( $objPsProduct, 'CPsProduct' ) ) ? $objPsProduct->getName() : 'No Product';
			$strSubString   = ( true == valObj( $objPsProductOption, 'CPsProductOption' ) ) ? '>>' . $objPsProductOption->getName() . ']' : ']';
			$strMsg         = \Psi\CStringService::singleton()->substr( \Psi\CStringService::singleton()->str_ireplace( 'Escalated Task:', '', $objTask->getTitle() ), 0, 30 );
			$strMsg         = ( strlen( $strMsg ) < 30 ) ? $strMsg : $strMsg . '...';
			$strMessage     = 'URGENT PRI TICKET CREATED #' . $objTask->getId() . '( ' . $strMsg . ' )' . ' [' . $strProductName . $strSubString;
			$objMessage->setPhoneNumber( self::INDIA_EXTENSION . trim( $intPhoneNumber ) );
			$objMessage->setMessageOriginatorId( CMessageOriginator::PS_NUMBER_3 );
		}
		$objMessage->setMessage( $strMessage );
		$arrobjPushMessages = [];

		if( true == valStr( $objMessage->getMessage() ) ) {
			array_push( $arrobjPushMessages, $objMessage );
		}

		return $arrobjPushMessages;

	}

	public function valMessage( $boolIsSent = false ) {

		$boolIsValid = true;

		if( false == $boolIsSent ) {
			if( 0 == strlen( trim( $this->getMessage() ) ) ) {

				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', __( 'Message is required.' ) ) );

			} elseif( true == \Psi\CStringService::singleton()->strstr( $this->getMessage(), self::REPLY_STOP_TO_STOP_MESSAGE ) ) {

				if( ( self::SMS_LENGTH ) < strlen( trim( $this->getMessage() ) ) ) {
					$boolIsValid = false;

					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', __( 'Message should not exceed 160 characters' ) ) );
				}

			} elseif( ( self::SMS_LENGTH - 20 ) < strlen( trim( $this->getMessage() ) ) ) {

				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', __( 'Message should not exceed 140 characters' ) ) );
			}
		} else {

			if( false == valStr( $this->getMessage() ) ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', __( 'Message is required.' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valScheduledSendDate() {

		$boolIsValid        = true;
		$arrintSendDateYear = explode( '/', $this->getScheduledSendDate() );
		$strDateTime = date( 'm/d/Y H:i:59', strtotime( $this->getScheduledSendDatetime() ) );

		if( true == is_null( $this->getScheduledSendDate() ) ) {

			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_send_date', 'Send date is required' ) );

		} elseif( $arrintSendDateYear[2] > ( date( 'Y' ) + 10 ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_send_date', 'Send date year should not be greater than ' . ( date( 'Y' ) + 10 ) ) );

		} elseif( strtotime( $this->getScheduledSendDate() ) < strtotime( date( 'm/d/Y' ) ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_send_date', 'Send date should be greater than current date.' ) );

		} elseif( ( 0 != date( 'G', strtotime( $strDateTime ) ) ) && ( strtotime( $strDateTime ) < time() ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'send_date', 'Send time should be greater than current time.' ) );
		}

		return $boolIsValid;
	}

	public function valMessageTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMessageTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_type_id', 'Message type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;

		if( false == valStr( $this->getPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRecipientPhoneNumber() {
		$boolIsValid = true;

		if( false == valStr( $this->getPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Phone number is required.' ) ) );
		} elseif( 11 != strlen( preg_replace( '/[^0-9]/', '', $this->getPhoneNumber() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', __( 'Please enter valid phone number.' ) ) );
		}

		return $boolIsValid;
	}

	public function valMessageDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getMessageDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_datetime', 'Message datetime is required.' ) );
		}

		return $boolIsValid;
	}

	public function valScheduledSendDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getScheduledSendDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_send_datetime', 'Scheduled send datetime is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMessageAggregatorId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMessageAggregatorId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_aggregator_id', 'Message aggregator is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMessageOriginatorId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMessageOriginatorId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_originator_id', 'Message originator is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMessageStatusTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMessageStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_status_type_id', 'Message status type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMessagePriorityId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMessagePriorityId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_priority_id', 'Message priority is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMessageEncodingTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMessageEncodingTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_encoding_type_id', 'Message encoding type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMessageString() {
		$boolIsValid = true;
		if( 0 == strlen( trim( $this->getMessage() ) ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', 'Message is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'validate_insert_resident_portal':
				$boolIsValid &= $this->valPhoneNumber();

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valMessage();
				$boolIsValid &= $this->valScheduledSendDate();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valMessageAggregatorId();
				$boolIsValid &= $this->valMessageOriginatorId();
				$boolIsValid &= $this->valMessageStatusTypeId();
				$boolIsValid &= $this->valMessagePriorityId();
				$boolIsValid &= $this->valMessageEncodingTypeId();

				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valMessageDatetime();
				$boolIsValid &= $this->valScheduledSendDatetime();
				break;

			case self::VALIDATE_SEND:
				$boolIsValid &= $this->valMessageTypeId();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valMessage( true );
				break;

			case 'validate_send_info_via_text_message':
				$boolIsValid &= $this->valRecipientPhoneNumber();
				$boolIsValid &= $this->valMessage( true );
				break;

			case 'validate_consumer_insert':
				$boolIsValid &= $this->valMessageString();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function enqueue( $intCurrentUserId, $objDatabase ) {
		$objSendOutboundSmsMessage = new \Psi\Libraries\Sms\CSendOutboundSmsMessage();
		$objSendOutboundSmsMessage->setMessage( $this );
		$objSendOutboundSmsMessage->setCompanyUserId( $intCurrentUserId );
		$objSendOutboundSmsMessage->setMetadata( $this->getMetadata() );
		$boolIsValid = true;

		try {
			if( false == $objSendOutboundSmsMessage->execute() ) {
				$this->addErrorMsgs( $objSendOutboundSmsMessage->getErrorMsgs() );
				if( false == valObj( $objDatabase, CDatabase::class ) ) {
					$objDatabase = \Psi\Eos\Connect\CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::SMS, CDatabaseUserType::PS_PROPERTYMANAGER );
					if( false == valObj( $objDatabase, CDatabase::class ) ) {
						$boolIsValid = false;
						throw new Exception( 'Unable to load sms database' );
					}
				}

				if( true == $this->getIsCheckForBlockedNumber() && false == $this->checkForBlockedNumbers( $objDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( 'Skipping this message as Messages are blocked for phone number -:' . $this->getPhoneNumber() ) );
				}

				if( false == $this->insert( $intCurrentUserId, $objDatabase ) ) {
					$boolIsValid = false;
					throw new Exception( 'Unable to insert sms for ' . $this->getPhoneNumber() );
				}
			}
		} catch( Exception $objException ) {
			$this->addErrorMsg( new CErrorMsg( $objException->getMessage() ) );
		}

		if( true == valObj( $objDatabase, CDatabase::class ) && true == $objDatabase->getIsOpen() ) {
			$objDatabase->close();
		}

		return $boolIsValid;
	}

	public function checkForBlockedNumbers( $objDatabase ) {
		$arrintMessageTypeIds = [ CMessageType::STOP ];
		if( true == valId( $this->getMessageTypeId() ) ) {
			$arrintMessageTypeIds = [ $this->getMessageTypeId(), CMessageType::STOP ];
		}

		$arrobjBlocks = ( array ) \Psi\Eos\Sms\CBlocks::createService()->fetchBlocksByPhoneNumberByMessageTypeIds( $this->getPhoneNumber(), $arrintMessageTypeIds, $objDatabase );

		return !( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjBlocks ) );
	}

}
