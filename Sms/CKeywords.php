<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CKeywords
 * Do not add any new functions to this class.
 */

class CKeywords extends CBaseKeywords {

	public static function fetchKeywordsByCidByPropertyId( $intCid, $intPropertyId, $objSmsDatabase ) {
		return self::fetchKeywords( 'SELECT * FROM keywords WHERE cid = ' . ( int ) $intCid . ' AND property_id = ' . ( int ) $intPropertyId . ' ORDER BY updated_on DESC', $objSmsDatabase );
	}

	public static function fetchKeywordsByCidByPropertyIdByMessageTypeIdByExcludingAssociatedIds( $intCid, $intPropertyId, $intMessageTypeId, $arrintKeywordIdsAssociatedWithLeadSources, $objSmsDatabase ) {
		$strSql = 'SELECT * FROM keywords AS k WHERE k.cid = ' . ( int ) $intCid . ' AND k.property_id = ' . ( int ) $intPropertyId . ' AND k.message_type_id = ' . ( int ) $intMessageTypeId;

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrintKeywordIdsAssociatedWithLeadSources ) ) {
			$strSql .= ' AND k.id NOT IN ( ' . implode( ',', $arrintKeywordIdsAssociatedWithLeadSources ) . ' )';
		}

		return self::fetchKeywords( $strSql, $objSmsDatabase );
	}

	public static function fetchKeywordByKeyword( $strKeyword, $objSmsDatabase ) {
		return self::fetchKeyword( 'SELECT * FROM keywords WHERE keyword = \'' . addslashes( \Psi\CStringService::singleton()->strtoupper( $strKeyword ) ) . '\'', $objSmsDatabase );
	}

	public static function fetchAllKeywords( $objSmsDatabase ) {
		return self::fetchKeywords( 'SELECT * FROM keywords ORDER BY id', $objSmsDatabase );
	}

	public static function fetchPaginatedReservedKeywords( $intPageNo, $intPageSize, $objSmsDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		return self::fetchKeywords( 'SELECT * FROM keywords WHERE cid IS NULL AND property_id IS NULL AND message_originator_id IS NULL ORDER BY id DESC OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit, $objSmsDatabase );
	}

	public static function fetchPaginatedReservedKeywordsCount( $objSmsDatabase ) {
		$strWhere = 'WHERE cid IS NULL AND property_id IS NULL AND message_originator_id IS NULL';
		return self::fetchKeywordCount( $strWhere, $objSmsDatabase );
	}

	public static function fetchKeywordByIds( $arrintKeywordIds, $objSmsDatabase ) {
		if( false == valArr( $arrintKeywordIds ) ) return NULL;
		return self::fetchKeywords( 'SELECT * FROM keywords WHERE id IN ( ' . implode( ', ', $arrintKeywordIds ) . ' ) ', $objSmsDatabase );
	}

	public static function fetchPaginatedKeywordsByMessageTypeId( $intPageNo, $intPageSize, $intMessageTypeId, $objSmsDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		return self::fetchKeywords( 'SELECT * FROM keywords WHERE message_type_id = ' . ( int ) $intMessageTypeId . ' ORDER BY id DESC OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit, $objSmsDatabase );
	}

	public static function fetchPaginatedKeywordsCountByMessageTypeId( $intMessageTypeId, $objSmsDatabase ) {
		$strWhere = 'WHERE message_type_id = ' . ( int ) $intMessageTypeId;
		return self::fetchKeywordCount( $strWhere, $objSmsDatabase );
	}

	public static function fetchNonSuspendedKeywordsByCidByPropertyIds( $intCid, $arrintPropertyIds, $objSmsDatabase ) {
		return self::fetchKeywords( 'SELECT * FROM keywords WHERE cid = ' . ( int ) $intCid . ' AND property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) AND suspended_on IS NULL', $objSmsDatabase );
	}

	public static function fetchSuspendedKeywordsForDelete( $objSmsDatabase ) {
		return self::fetchKeywords( 'SELECT * FROM keywords WHERE date_trunc( \'day\', suspended_on ) <= date_trunc( \'day\', NOW() - INTERVAL \'3 months\' )', $objSmsDatabase );
	}

	public static function fetchKeywordsByMessegeTypeIdByPropertyIdByCid( $arrintMessageTypeIds, $intPropertyId, $intCid, $objSmsDatabase ) {

		if( false == valArr( $arrintMessageTypeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						keywords
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND message_type_id IN ( ' . sqlIntImplode( $arrintMessageTypeIds ) . ' )
					ORDER BY updated_on DESC';

		return self::fetchKeywords( $strSql, $objSmsDatabase );
	}

}
?>