<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageAggregators
 * Do not add any new functions to this class.
 */

class CMessageAggregators extends CBaseMessageAggregators {

	public static function fetchMessageAggregator( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMessageAggregator', $objDatabase, DATA_CACHE_MEMORY );
	}

}
?>