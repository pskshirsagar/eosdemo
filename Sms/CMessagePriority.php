<?php

class CMessagePriority extends CBaseMessagePriority {

	const NORMAL 		= 1;
	const URGENT 		= 2;
	const CRITICAL 		= 3;
}
?>