<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessages
 * Do not add any new functions to this class.
 */

class CMessages extends CBaseMessages {

	const RECIPIENT_TYPE_RESIDENTS = 1;
	const RECIPIENT_TYPE_PROSPECTS = 2;
	const RECIPIENT_TYPE_EMPLOYEES = 3;

	public static function fetchMessagesByFilter( $objMessagesFilter, $objSmsDatabase, $boolIsDownload = false ) {

		$arrstrAndSearchParameters = [];
		$intOffset 	= ( 0 < $objMessagesFilter->getPageNo() ) ? $objMessagesFilter->getPageSize() * ( $objMessagesFilter->getPageNo() - 1 ) : 0;
		$intLimit 	= ( int ) $objMessagesFilter->getPageSize();

		 $strSql = 'SELECT * FROM messages WHERE 1 = 1 ';

	   	$arrstrAndSearchParameters = self::buildSearchCriteria( $objMessagesFilter );
		$strSql	.= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		$strSql .= ' ORDER BY id DESC ';

		if( false == $boolIsDownload ) {
			$strSql	.= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		return self::fetchMessages( $strSql, $objSmsDatabase );
	}

	public static function fetchMessagesCountBySearchParameters( $objMessagesFilter, $objSmsDatabase ) {

		$arrstrAndSearchParameters = [];

		$strSql = 'SELECT count(id) FROM messages WHERE 1 = 1 ';

		$arrstrAndSearchParameters = self::buildSearchCriteria( $objMessagesFilter );
		$strSql	.= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		$arrintResponse = fetchData( $strSql, $objSmsDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function buildSearchCriteria( $objMessagesFilter ) {

		$arrstrWhereParameters = [];

		// Create SQL parameters.
		( false == isset( $objMessagesFilter ) || true == is_null( $objMessagesFilter->getCid() ) || false == $objMessagesFilter->getCid() )	?  false : array_push( $arrstrWhereParameters, 'cid = ' . $objMessagesFilter->getCid() );
		( false == isset( $objMessagesFilter ) || true == is_null( $objMessagesFilter->getPropertyId() ) || false == $objMessagesFilter->getPropertyId() )						?  false : array_push( $arrstrWhereParameters, 'property_id = ' . $objMessagesFilter->getPropertyId() );
		( false == isset( $objMessagesFilter ) || true == is_null( $objMessagesFilter->getIsInbound() ) || false == $objMessagesFilter->getIsInbound() )						?  false : array_push( $arrstrWhereParameters, 'is_inbound = ' . $objMessagesFilter->getIsInbound() );
		( false == isset( $objMessagesFilter ) || true == is_null( $objMessagesFilter->getIsFlashSms() ) || false == $objMessagesFilter->getIsFlashSms() )						?  false : array_push( $arrstrWhereParameters, 'is_flash_sms = ' . $objMessagesFilter->getIsFlashSms() );

		if( false == is_null( $objMessagesFilter->getMessageId() ) ) {
			$arrstrWhereParameters[] = ' to_char( id, \'999999999\' ) LIKE \'%' . $objMessagesFilter->getMessageId() . '%\'';
		}

		$intPhoneNumber = preg_replace( '/[^0-9]/', '', $objMessagesFilter->getPhoneNumber() );

		if( false == is_null( $objMessagesFilter->getPhoneNumber() ) ) {
			if( 11 <= strlen( $intPhoneNumber ) ) {
				$arrstrWhereParameters[] = ' phone_number LIKE \'%' . addslashes( \Psi\CStringService::singleton()->substr( $intPhoneNumber, 1 ) ) . '%\'';
			} else {
				$arrstrWhereParameters[] = 'phone_number LIKE \'%' . addslashes( $intPhoneNumber ) . '%\'';
			}
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $objMessagesFilter->getPropertyIds() ) ) 			$arrstrWhereParameters[] = 'property_id IN ( ' . $objMessagesFilter->getPropertyIds() . ')';
		if( 0 < \Psi\Libraries\UtilFunctions\count( $objMessagesFilter->getMessageStatusTypeIds() ) ) 	$arrstrWhereParameters[] = 'message_status_type_id IN ( ' . $objMessagesFilter->getMessageStatusTypeIds() . ')';
		if( 0 < \Psi\Libraries\UtilFunctions\count( $objMessagesFilter->getMessageTypeIds() ) ) 		$arrstrWhereParameters[] = 'message_type_id IN ( ' . $objMessagesFilter->getMessageTypeIds() . ')';
		if( 0 < \Psi\Libraries\UtilFunctions\count( $objMessagesFilter->getMessagePriorityIds() ) ) 	$arrstrWhereParameters[] = 'message_priority_id IN ( ' . $objMessagesFilter->getMessagePriorityIds() . ')';
	   	if( 0 < \Psi\Libraries\UtilFunctions\count( $objMessagesFilter->getMessageOperatorIds() ) )  	$arrstrWhereParameters[] = 'message_operator_id IN ( ' . $objMessagesFilter->getMessageOperatorIds() . ')';
	   	if( 0 < \Psi\Libraries\UtilFunctions\count( $objMessagesFilter->getMessageEncodingTypeIds() ) ) $arrstrWhereParameters[] = 'message_encoding_type_id IN ( ' . $objMessagesFilter->getMessageEncodingTypeIds() . ')';
		if( false == is_null( $objMessagesFilter->getFromDate() ) )  		$arrstrWhereParameters[] = 'message_datetime >= \'' . date( 'Y-m-d', strtotime( $objMessagesFilter->getFromDate() ) ) . ' 00:00:00\'';
		if( false == is_null( $objMessagesFilter->getToDate() ) )   		$arrstrWhereParameters[] = 'message_datetime <= \'' . date( 'Y-m-d', strtotime( $objMessagesFilter->getToDate() ) ) . ' 23:59:59\'';
		if( false == is_null( $objMessagesFilter->getKeyword() ) )   		$arrstrWhereParameters[] = 'message ILIKE \'%' . addslashes( $objMessagesFilter->getKeyword() ) . '%\'';

		if( false == is_null( $objMessagesFilter->getRecipientTypeId() ) ) {
			if( self::RECIPIENT_TYPE_RESIDENTS == $objMessagesFilter->getRecipientTypeId() ) {
				$arrstrWhereParameters[] = ' customer_id IS NOT NULL ';
			} elseif( self::RECIPIENT_TYPE_PROSPECTS == $objMessagesFilter->getRecipientTypeId() ) {
				$arrstrWhereParameters[] = ' applicant_id IS NOT NULL ';
			} elseif( self::RECIPIENT_TYPE_EMPLOYEES == $objMessagesFilter->getRecipientTypeId() ) {
				$arrstrWhereParameters[] = ' company_employee_id IS NOT NULL ';
			}
		}

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrWhereParameters ) ) {
			return $arrstrWhereParameters;
		} else {
			return NULL;
		}
	}

	public static function fetchMessageByIdByCid( $intMessageId, $intCid, $objSmsDatabase ) {
		return self::fetchMessage( 'SELECT * FROM messages WHERE cid = ' . ( int ) $intCid . ' AND id = ' . ( int ) $intMessageId, $objSmsDatabase );
	}

	public static function fetchMessageByMessageId( $intMessageId, $objSmsDatabase ) {
		return self::fetchMessage( 'SELECT * FROM messages WHERE message_id = ' . ( int ) $intMessageId . ' AND applicant_id IS NOT NULL LIMIT 1', $objSmsDatabase );
	}

	public static function fetchMessageByRemotePrimaryKey( $strRemotePrimaryKey, $objSmsDatabase ) {
		return self::fetchMessage( 'SELECT * FROM messages WHERE remote_primary_key = \'' . addslashes( $strRemotePrimaryKey ) . '\' LIMIT 1', $objSmsDatabase );
	}

 	public static function fetchOptedOutPhoneNumberCountByKeywordByMessageTypeIdByKeywordId( $strKeyword, $intMessageTypeId, $intKeywordId, $objSmsDatabase ) {

		$strSql = 'SELECT
						 COUNT( DISTINCT( phone_number ) )
					 FROM
						 blocks
					 WHERE
						  message_type_id = ' . ( int ) $intMessageTypeId . '
						  AND keyword_id = ' . ( int ) $intKeywordId . '
						  AND phone_number IN ( SELECT
													DISTINCT phone_number
												FROM
													messages
												WHERE
													message ILIKE \'' . ( string ) trim( addslashes( $strKeyword ) ) . '%\'
												)';

		$arrintOptOutMembers	= fetchData( $strSql, $objSmsDatabase );

		if( true == isset( $arrintOptOutMembers[0]['count'] ) ) return $arrintOptOutMembers[0]['count'];

		return 0;
	}

 	public static function fetchPhoneNumberCountByKeyword( $strKeyword, $objSmsDatabase ) {

		$strSql = 'SELECT
						 COUNT( DISTINCT( phone_number ) )
					 FROM
						  messages
					 WHERE
						  message = \'' . ( string ) trim( addslashes( $strKeyword ) ) . '\'';

		$arrintTotalUniqueMembers	= fetchData( $strSql, $objSmsDatabase );
		return $arrintTotalUniqueMembers[0]['count'];
	}

 	public static function fetchMessagesByPhoneNumberByMessageTypeIdByMessageStatusTypeId( $strPhoneNumber, $intMessageTypeId, $intMessageStatusTypeId, $objSmsDatabase ) {
		return self::fetchMessages( 'SELECT * FROM messages WHERE regexp_replace( phone_number, \'[^0-9]\', \'\', \'g\') = \'' . ( string ) addslashes( $strPhoneNumber ) . '\' AND message_type_id = ' . ( int ) $intMessageTypeId . ' AND message_status_type_id = ' . ( int ) $intMessageStatusTypeId, $objSmsDatabase );
	}

	public static function fetchMessagesbyMessageByPhoneNumberByMessageTypeId( $strMessage, $strPhoneNumber, $intMessageTypeId, $objSmsDatabase ) {
		return self::fetchMessages( 'SELECT * FROM messages WHERE message ILIKE \'' . ( string ) addslashes( $strMessage ) . '\' AND phone_number = \'' . ( string ) addslashes( $strPhoneNumber ) . '\' AND message_type_id = ' . ( int ) $intMessageTypeId, $objSmsDatabase );
	}

	public static function fetchParsingMessagesByMessage( $strKeyWord, $objSmsDatabase ) {
		$strSql = 'SELECT * FROM messages WHERE message=\'' . addslashes( \Psi\CStringService::singleton()->strtoupper( $strKeyWord ) ) . '\' AND message_status_type_id = ' . CMessageStatusType::PARSING;

		return self::fetchMessages( $strSql, $objSmsDatabase );

	}

	public static function fetchMessagesCountByCidsBySearchCriteria( $arrintCids, $objMessageFilter, $objSmsDatabase ) {

		if( false == valArr( $arrintCids ) || false == valObj( $objMessageFilter, 'CMessagesFilter' ) ) return NULL;

			$strSql = 'SELECT
							COUNT(id),
							cid
						FROM
							messages
						WHERE
							 cid IN ( ' . implode( ',', $arrintCids ) . ' )';

			$arrstrAndSearchParameters = self::buildSearchCriteria( $objMessageFilter );

			$strSql	.= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

			$strSql .= ' GROUP BY cid';

			$arrintResponse = fetchData( $strSql, $objSmsDatabase );

			return ( true == valArr( $arrintResponse ) ) ? $arrintResponse : NULL;
	}

	public static function fetchMessagesCountGroupByPropertyBySearchCriteria( $objMessageFilter, $objSmsDatabase ) {

		if( false == valObj( $objMessageFilter, 'CMessagesFilter' ) ) return NULL;

		$strSql = 'SELECT
						COUNT(id),
						property_id
					FROM
						messages
					WHERE 1 = 1 ';

		$arrstrAndSearchParameters = self::buildSearchCriteria( $objMessageFilter );

		$strSql	.= ( ( false == is_null( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		$strSql .= ' GROUP BY property_id';

		$arrintResponse = fetchData( $strSql, $objSmsDatabase );

		return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : NULL;
	}

	public static function fetchSearchedMessages( $arrstrFilteredExplodedSearch, $objSmsDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						messages
					WHERE
 						to_char( id, \'999999999\' ) LIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
						OR phone_number LIKE \'%' . $arrstrFilteredExplodedSearch . '%\'
 					LIMIT 10';

		return self::fetchMessages( $strSql, $objSmsDatabase );
	}

	public static function fetchCustomInboundMessagesInfo( $objSmsDatabase, $arrstrMessageTypes = [ CMessageType::INFORMATION_REQUEST ], $strIntervalTime = '1 hour' ) {

		$strSqlWhereClause = 'message_datetime > ( NOW() - INTERVAL \'' . $strIntervalTime . '\' ) AND ';

		if( true == in_array( CMessageType::PAPERLESS, $arrstrMessageTypes ) ) {
			$strSqlWhereClause = '';
		}

		$strSql = 'SELECT
						*
					FROM
						messages
					WHERE
						' . $strSqlWhereClause . '
						is_inbound = 1
						AND message_status_type_id = ' . CMessageStatusType::PARSED . '
						AND message_type_id IN( ' . sqlIntImplode( $arrstrMessageTypes ) . ' )';

		return self::fetchMessages( $strSql, $objSmsDatabase );
	}

	public static function fetchMessageByEmployeeId( $intEmployeeId, $objSmsDatabase ) {
		$strSql = 'SELECT id
					FROM
						messages
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND message_type_id = ' . CMessageType::TASK_NOTIFICATION . '
					ORDER BY message_datetime DESC
					LIMIT 1';

		$arrintMessage = fetchData( $strSql, $objSmsDatabase );

		if( true == isset( $arrintMessage[0]['id'] ) ) return $arrintMessage[0]['id'];

		return 0;
	}

	public static function fetchMessagesByIds( $arrintMessageIds, $objDatabase ) {
		if( false == valArr( $arrintMessageIds ) ) return;
		$strSql = 'SELECT * FROM messages WHERE id IN( ' . implode( ',', $arrintMessageIds ) . ' )';
		return self::fetchMessages( $strSql, $objDatabase );
	}

	public static function fetchMessageByApplicantIdByPropertyIdByPhoneNumberByCid( $intApplicantId, $intPropertyId, $strPhoneNumber, $intCid, $objSmsDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						messages
					WHERE
						cid = ' . ( int ) $intCid . '
						AND applicant_id = ' . ( int ) $intApplicantId . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND phone_number like \'' . $strPhoneNumber . '\'
					ORDER BY
						id desc
					LIMIT 1';

		return self::fetchMessage( $strSql, $objSmsDatabase );
	}

	public static function fetchMessageByPhoneNumberByMessageOriginatorId( $strPhoneNumber, $intMessageOriginator, $objSmsDatabase, $boolIsUpdate = false ) {

		$strSql = 'SELECT
						*
					FROM
						messages
					WHERE
						phone_number like \'%' . $strPhoneNumber . '\'
						AND message_originator_id = ' . ( int ) $intMessageOriginator . '
					ORDER BY
						id desc
					LIMIT 1';

		return self::fetchMessage( $strSql, $objSmsDatabase );
	}

	public static function fetchMessageByPhoneNumberByOriginatorId( $strPhoneNumber, $intOriginatorId, $objSmsDatabase ) {
		// we are fetching message within last 4 hour as originator id will be free after 4 hour for the same task and same person.
		$strSql = 'SELECT 
						* 
					FROM 
						messages
					WHERE 
						phone_number = \'' . ( string ) addslashes( $strPhoneNumber ) . '\'
						AND message_originator_id = ' . ( int ) $intOriginatorId . '
						AND message_datetime > ( NOW() - INTERVAL \'4 hour\' )
						AND message_type_id = ' . CMessageType::TASK_NOTIFICATION . '
						AND is_inbound = 0
					ORDER BY 
						message_datetime DESC
					LIMIT 1 ';

		return self::fetchMessage( $strSql, $objSmsDatabase );
	}

	public static function fetchMessageByPhoneNumberByMessageTypeIds( $strPhoneNumber, $arrintMessageTypeIds, $objSmsDatabase ) {

		if( false == valStr( $strPhoneNumber ) || false == valArr( $arrintMessageTypeIds ) ) {
			return false;
		}

		$strSql = 'SELECT 
						* 
					FROM
						messages
					WHERE 
						phone_number = \'' . $strPhoneNumber . '\' 
						AND message_type_id IN ( ' . implode( ',', $arrintMessageTypeIds ) . ' ) 
						AND customer_id IS NOT NULL 
						ORDER BY id DESC 
						LIMIT 1';

		return self::fetchMessage( $strSql, $objSmsDatabase );
	}

	public static function fetchMessageOriginatorByPhoneNumberByPropertyIdByCid( $strPhoneNumber, $intPropertyId, $intCid, $objSmsDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}

		$strPhoneNumber = str_replace( [ '+', ' ', '(', ')', '-' ], '', trim( $strPhoneNumber ) );

		$strSql = sprintf( 'SELECT
									m.message_originator_id
								FROM
									messages m
									JOIN message_originators mo ON ( m.message_originator_id = mo.id AND m.cid = mo.cid AND m.property_id = mo.property_id )
								WHERE
									m.cid = %d
									AND m.property_id = %d
									AND m.phone_number LIKE %s
									AND mo.is_published = 1
									AND mo.suspended_on IS NULL
									AND mo.suspended_by IS NULL
									AND m.message_type_id = %d
									AND m.message_datetime > NOW() - INTERVAL %s 
								ORDER BY
									m.created_on DESC
								LIMIT
									1',
						( int ) $intCid,
						( int ) $intPropertyId,
						pg_escape_literal( $objSmsDatabase->getHandle(), '%' . $strPhoneNumber . '%' ),
						( int ) CMessageType::SMS_CHAT,
						pg_escape_literal( $objSmsDatabase->getHandle(), '7 days' ) );

		return self::fetchColumn( $strSql, 'message_originator_id', $objSmsDatabase );
	}

}
?>