<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessagePriorities
 * Do not add any new functions to this class.
 */

class CMessagePriorities extends CBaseMessagePriorities {

	public static function fetchMessagePriorities( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMessagePriority', $objDatabase, DATA_CACHE_MEMORY );
	}

	public static function fetchAllMessagePriorities( $objSmsDatabse ) {
       return self::fetchMessagePriorities( 'SELECT * FROM message_priorities ORDER BY order_num', $objSmsDatabse );
    }

}
?>