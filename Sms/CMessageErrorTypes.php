<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageErrorTypes
 * Do not add any new functions to this class.
 */

class CMessageErrorTypes extends CBaseMessageErrorTypes {

	public static function fetchMessageErrorTypesByMessageErrorLocationTypeIdByCode( $intMessageErrorLocationTypeId, $strCode, $objSmsDatabase ) {
		return self::fetchMessageErrorType( 'SELECT * FROM message_error_types WHERE message_error_location_type_id = ' . ( int ) $intMessageErrorLocationTypeId . ' AND code = \'' . addslashes( trim( $strCode ) ) . '\'', $objSmsDatabase );
	}

	public static function fetchMessageErrorTypesByMessageAggregatorIdByCode( $intMessageAggregatorId, $strCode, $objSmsDatabase ) {
		return self::fetchMessageErrorType( 'SELECT * FROM message_error_types WHERE message_aggregator_id = ' . ( int ) $intMessageAggregatorId . ' AND code = \'' . addslashes( trim( $strCode ) ) . '\'', $objSmsDatabase );
	}
}
?>