<?php

class CMessageErrorLocationType extends CBaseMessageErrorLocationType {

	const FRONT_END						= 1;
	const BACK_END 						= 2;
	const REQUEST_RESULT_CODE 			= 3;
	const NOTIFICATION_RESULT_CODE		= 4;
	const SUBSCRIBER_RESULT_CODE 		= 5;
	const TWILIO_OUTBOUND_SOFT_CONFIRM 	= 6;
	const WHATSAPP_OUTBOUND_REQUEST 	= 7;
}
?>