<?php

class CMessageAggregator extends CBaseMessageAggregator {

	const MBLOX 		= 1;
	const TWILIO		= 2;
	const WHATSAPP		= 3;
	const WECHAT 		= 4;

    public function valName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valDescription() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valUsername() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valPasswordEncrypted() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valAccountNumber() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valIsPublished() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valOrderNum() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            case VALIDATE_DELETE:
            	break;

            default:
            	// default
            	$boolIsValid = false;
            	break;
        }

        return $boolIsValid;
    }

}
?>