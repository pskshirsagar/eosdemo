<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageOriginators
 * Do not add any new functions to this class.
 */

class CMessageOriginators extends CBaseMessageOriginators {

	public static function fetchMessageOriginators( $strSql, $objDatabase ) {
		return self::fetchObjects( $strSql, 'CMessageOriginator', $objDatabase );
	}

	public static function fetchMessageOriginator( $strSql, $objDatabase ) {
		return self::fetchObject( $strSql, 'CMessageOriginator', $objDatabase );
	}

	public static function fetchMessageOriginatorsByIds( $arrintMessageOriginatorIds, $objSmsDatabase ) {
		if( false == valArr( $arrintMessageOriginatorIds ) ) {
			return NULL;
		}
		return self::fetchMessageOriginators( 'SELECT * FROM message_originators WHERE id IN ( ' . implode( ',', $arrintMessageOriginatorIds ) . ' )', $objSmsDatabase );
	}

	public static function fetchMessageOriginatorById( $intMessageOriginatorId, $objSmsDatabase ) {
		return self::fetchMessageOriginator( 'SELECT * FROM message_originators WHERE id = ' . ( int ) $intMessageOriginatorId . ' LIMIT 1', $objSmsDatabase );
	}

	public static function fetchMessageOriginatorByNumber( $strNumber, $objSmsDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM 
						message_originators mo
					WHERE 
						mo.number = \'' . addslashes( $strNumber ) . '\'
						AND mo.is_published = 1
						AND mo.suspended_on IS NULL
						AND mo.suspended_by IS NULL;';

		return self::fetchObject( $strSql, CMessageOriginator::class, $objSmsDatabase );
	}

	public static function fetchMessageOriginatorByNumberByCallPhoneNumberIdByPropertyIdByCid( $strNumber, $intCallPhoneNumberId, $intPropertyId, $intCid, $objSmsDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM 
						message_originators 
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND call_phone_number_id = ' . ( int ) $intCallPhoneNumberId . '
						AND number = \'' . addslashes( $strNumber ) . '\'';

		return self::fetchMessageOriginator( $strSql, $objSmsDatabase );
	}

	public static function fetchAvailableOriginatorIdByMessageIds( $arrintMessageIds, $objSmsDatabase ) {
		$strCondition = '';
		if( true == valArr( array_filter( $arrintMessageIds ) ) ) {
			$strCondition = ' WHERE id NOT IN (
								SELECT DISTINCT m.message_originator_id
								FROM messages m
								WHERE m.id IN ( ' . sqlIntImplode( array_filter( $arrintMessageIds ) ) . ' ) )';
		}
		$strSql = 'SELECT 
						DISTINCT mo.id
					FROM 
						message_originators mo' . $strCondition;
		return fetchData( $strSql, $objSmsDatabase );
	}

	public static function fetchOutboundDefaultMessageOriginatorByPropertyIdByCid( $intPropertyId, $intCid, $objSmsDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						message_originators
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND details->>\'outbound_default\' = \'true\'
						AND released_on IS NULL 
						AND released_by IS NULL 
						AND suspended_by IS NULL 
						AND suspended_on IS NULL';

		return self::fetchMessageOriginator( $strSql, $objSmsDatabase );
	}

	public static function fetchPublishedMessageOriginatorByNumberByCallPhoneNumberIdByPropertyIdByCid( $strNumber, $intCallPhoneNumberId, $intPropertyId, $intCid, $objSmsDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						message_originators
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND call_phone_number_id = ' . ( int ) $intCallPhoneNumberId . '
						AND number = \'' . addslashes( $strNumber ) . '\'
						AND is_published = ' . CMessageOriginator::PUBLISHED;

		return self::fetchMessageOriginator( $strSql, $objSmsDatabase );
	}

	public static function fetchAvailableMessageOriginatorByNumberByCallPhoneNumberIdByPropertyIdByCid( $strNumber, $intCallPhoneNumberId, $intPropertyId, $intCid, $objSmsDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						message_originators
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND call_phone_number_id = ' . ( int ) $intCallPhoneNumberId . '
						AND number = \'' . addslashes( $strNumber ) . '\'';

		return self::fetchMessageOriginator( $strSql, $objSmsDatabase );
	}

	public static function fetchMessageOriginatorBySid( $strSid, $objSmsDatabase ) {
		$strSql = 'SELECT 
						* 
					FROM 
						message_originators 
					WHERE
						details->>\'sid\' = \'' . addslashes( $strSid ) . '\'';

		return self::fetchObject( $strSql, 'CMessageOriginator', $objSmsDatabase );
	}

	public static function fetchPublishedMessageOriginatorByPropertyIdByCid( $intPropertyId, $intCid, $objSmsDatabase ) {
		if( false == valId( $intPropertyId ) || false == valId( $intCid ) ) {
			return NULL;
		}
		$strSql = sprintf( 'SELECT 
									id
								FROM 
									message_originators 
								WHERE
									is_published = 1
									AND details IS NOT NULL
									AND details->\'outbound_default\' = \'true\'
									AND suspended_on IS NULL
									AND suspended_by IS NULL
									AND property_id = %d
									AND cid = %d',
								( int ) $intPropertyId,
								( int ) $intCid );

		return self::fetchObject( $strSql, CMessageOriginator::class, $objSmsDatabase );
	}

}
?>