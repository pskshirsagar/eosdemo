<?php

class CMessageEncodingType extends CBaseMessageEncodingType {

	const TEXT 		= 1;
	const BINARY 	= 2;
	const UNICODE	= 3;
	const IMODE 	= 4;
}
?>