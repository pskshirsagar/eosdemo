<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessages
 * Do not add any new functions to this class.
 */

class CBaseMessages extends CEosPluralBase {

	/**
	 * @return CMessage[]
	 */
	public static function fetchMessages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMessage', $objDatabase );
	}

	/**
	 * @return CMessage
	 */
	public static function fetchMessage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMessage', $objDatabase );
	}

	public static function fetchMessageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'messages', $objDatabase );
	}

	public static function fetchMessageById( $intId, $objDatabase ) {
		return self::fetchMessage( sprintf( 'SELECT * FROM messages WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchMessagesByCid( $intCid, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchMessagesByMessageAggregatorId( $intMessageAggregatorId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE message_aggregator_id = %d', ( int ) $intMessageAggregatorId ), $objDatabase );
	}

	public static function fetchMessagesByMessageOperatorId( $intMessageOperatorId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE message_operator_id = %d', ( int ) $intMessageOperatorId ), $objDatabase );
	}

	public static function fetchMessagesByMessageOriginatorId( $intMessageOriginatorId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE message_originator_id = %d', ( int ) $intMessageOriginatorId ), $objDatabase );
	}

	public static function fetchMessagesByMessageTypeId( $intMessageTypeId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE message_type_id = %d', ( int ) $intMessageTypeId ), $objDatabase );
	}

	public static function fetchMessagesByMessageStatusTypeId( $intMessageStatusTypeId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE message_status_type_id = %d', ( int ) $intMessageStatusTypeId ), $objDatabase );
	}

	public static function fetchMessagesByMessagePriorityId( $intMessagePriorityId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE message_priority_id = %d', ( int ) $intMessagePriorityId ), $objDatabase );
	}

	public static function fetchMessagesByMessageEncodingTypeId( $intMessageEncodingTypeId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE message_encoding_type_id = %d', ( int ) $intMessageEncodingTypeId ), $objDatabase );
	}

	public static function fetchMessagesByMessageErrorTypeId( $intMessageErrorTypeId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE message_error_type_id = %d', ( int ) $intMessageErrorTypeId ), $objDatabase );
	}

	public static function fetchMessagesByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

	public static function fetchMessagesByTransactionId( $intTransactionId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE transaction_id = %d', ( int ) $intTransactionId ), $objDatabase );
	}

	public static function fetchMessagesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchMessagesByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE customer_id = %d', ( int ) $intCustomerId ), $objDatabase );
	}

	public static function fetchMessagesByCompanyEmployeeId( $intCompanyEmployeeId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE company_employee_id = %d', ( int ) $intCompanyEmployeeId ), $objDatabase );
	}

	public static function fetchMessagesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchMessagesByOptInId( $intOptInId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE opt_in_id = %d', ( int ) $intOptInId ), $objDatabase );
	}

	public static function fetchMessagesByApplicantId( $intApplicantId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE applicant_id = %d', ( int ) $intApplicantId ), $objDatabase );
	}

	public static function fetchMessagesByMessageId( $intMessageId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE message_id = %d', ( int ) $intMessageId ), $objDatabase );
	}

	public static function fetchMessagesByCustomerSettingId( $intCustomerSettingId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE customer_setting_id = %d', ( int ) $intCustomerSettingId ), $objDatabase );
	}

	public static function fetchMessagesByMaintenanceRequestId( $intMaintenanceRequestId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE maintenance_request_id = %d', ( int ) $intMaintenanceRequestId ), $objDatabase );
	}

	public static function fetchMessagesByKeywordId( $intKeywordId, $objDatabase ) {
		return self::fetchMessages( sprintf( 'SELECT * FROM messages WHERE keyword_id = %d', ( int ) $intKeywordId ), $objDatabase );
	}

}
?>