<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageOriginators
 * Do not add any new functions to this class.
 */

class CBaseMessageOriginators extends CEosPluralBase {

	/**
	 * @return CMessageOriginator[]
	 */
	public static function fetchMessageOriginators( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMessageOriginator::class, $objDatabase );
	}

	/**
	 * @return CMessageOriginator
	 */
	public static function fetchMessageOriginator( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMessageOriginator::class, $objDatabase );
	}

	public static function fetchMessageOriginatorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_originators', $objDatabase );
	}

	public static function fetchMessageOriginatorById( $intId, $objDatabase ) {
		return self::fetchMessageOriginator( sprintf( 'SELECT * FROM message_originators WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMessageOriginatorsByCid( $intCid, $objDatabase ) {
		return self::fetchMessageOriginators( sprintf( 'SELECT * FROM message_originators WHERE cid = %d', $intCid ), $objDatabase );
	}

	public static function fetchMessageOriginatorsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchMessageOriginators( sprintf( 'SELECT * FROM message_originators WHERE property_id = %d', $intPropertyId ), $objDatabase );
	}

	public static function fetchMessageOriginatorsByMessageAggregatorId( $intMessageAggregatorId, $objDatabase ) {
		return self::fetchMessageOriginators( sprintf( 'SELECT * FROM message_originators WHERE message_aggregator_id = %d', $intMessageAggregatorId ), $objDatabase );
	}

	public static function fetchMessageOriginatorsByCallPhoneNumberId( $intCallPhoneNumberId, $objDatabase ) {
		return self::fetchMessageOriginators( sprintf( 'SELECT * FROM message_originators WHERE call_phone_number_id = %d', $intCallPhoneNumberId ), $objDatabase );
	}

}
?>