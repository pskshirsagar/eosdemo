<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageErrorTypes
 * Do not add any new functions to this class.
 */

class CBaseMessageErrorTypes extends CEosPluralBase {

	/**
	 * @return CMessageErrorType[]
	 */
	public static function fetchMessageErrorTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMessageErrorType::class, $objDatabase );
	}

	/**
	 * @return CMessageErrorType
	 */
	public static function fetchMessageErrorType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMessageErrorType::class, $objDatabase );
	}

	public static function fetchMessageErrorTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_error_types', $objDatabase );
	}

	public static function fetchMessageErrorTypeById( $intId, $objDatabase ) {
		return self::fetchMessageErrorType( sprintf( 'SELECT * FROM message_error_types WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchMessageErrorTypesByMessageAggregatorId( $intMessageAggregatorId, $objDatabase ) {
		return self::fetchMessageErrorTypes( sprintf( 'SELECT * FROM message_error_types WHERE message_aggregator_id = %d', $intMessageAggregatorId ), $objDatabase );
	}

	public static function fetchMessageErrorTypesByMessageErrorLocationTypeId( $intMessageErrorLocationTypeId, $objDatabase ) {
		return self::fetchMessageErrorTypes( sprintf( 'SELECT * FROM message_error_types WHERE message_error_location_type_id = %d', $intMessageErrorLocationTypeId ), $objDatabase );
	}

}
?>