<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CBlocks
 * Do not add any new functions to this class.
 */

class CBaseBlocks extends CEosPluralBase {

	/**
	 * @return CBlock[]
	 */
	public static function fetchBlocks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CBlock', $objDatabase );
	}

	/**
	 * @return CBlock
	 */
	public static function fetchBlock( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CBlock', $objDatabase );
	}

	public static function fetchBlockCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'blocks', $objDatabase );
	}

	public static function fetchBlockById( $intId, $objDatabase ) {
		return self::fetchBlock( sprintf( 'SELECT * FROM blocks WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchBlocksByCid( $intCid, $objDatabase ) {
		return self::fetchBlocks( sprintf( 'SELECT * FROM blocks WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchBlocksByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchBlocks( sprintf( 'SELECT * FROM blocks WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchBlocksByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchBlocks( sprintf( 'SELECT * FROM blocks WHERE customer_id = %d', ( int ) $intCustomerId ), $objDatabase );
	}

	public static function fetchBlocksByMessageTypeId( $intMessageTypeId, $objDatabase ) {
		return self::fetchBlocks( sprintf( 'SELECT * FROM blocks WHERE message_type_id = %d', ( int ) $intMessageTypeId ), $objDatabase );
	}

	public static function fetchBlocksByMessageId( $intMessageId, $objDatabase ) {
		return self::fetchBlocks( sprintf( 'SELECT * FROM blocks WHERE message_id = %d', ( int ) $intMessageId ), $objDatabase );
	}

	public static function fetchBlocksByKeywordId( $intKeywordId, $objDatabase ) {
		return self::fetchBlocks( sprintf( 'SELECT * FROM blocks WHERE keyword_id = %d', ( int ) $intKeywordId ), $objDatabase );
	}

}
?>