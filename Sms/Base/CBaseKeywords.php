<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CKeywords
 * Do not add any new functions to this class.
 */

class CBaseKeywords extends CEosPluralBase {

	/**
	 * @return CKeyword[]
	 */
	public static function fetchKeywords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CKeyword', $objDatabase );
	}

	/**
	 * @return CKeyword
	 */
	public static function fetchKeyword( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CKeyword', $objDatabase );
	}

	public static function fetchKeywordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'keywords', $objDatabase );
	}

	public static function fetchKeywordById( $intId, $objDatabase ) {
		return self::fetchKeyword( sprintf( 'SELECT * FROM keywords WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchKeywordsByCid( $intCid, $objDatabase ) {
		return self::fetchKeywords( sprintf( 'SELECT * FROM keywords WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchKeywordsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchKeywords( sprintf( 'SELECT * FROM keywords WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchKeywordsBySettingsTemplateId( $intSettingsTemplateId, $objDatabase ) {
		return self::fetchKeywords( sprintf( 'SELECT * FROM keywords WHERE settings_template_id = %d', ( int ) $intSettingsTemplateId ), $objDatabase );
	}

	public static function fetchKeywordsByMessageOriginatorId( $intMessageOriginatorId, $objDatabase ) {
		return self::fetchKeywords( sprintf( 'SELECT * FROM keywords WHERE message_originator_id = %d', ( int ) $intMessageOriginatorId ), $objDatabase );
	}

	public static function fetchKeywordsByMessageTypeId( $intMessageTypeId, $objDatabase ) {
		return self::fetchKeywords( sprintf( 'SELECT * FROM keywords WHERE message_type_id = %d', ( int ) $intMessageTypeId ), $objDatabase );
	}

}
?>