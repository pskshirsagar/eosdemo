<?php

class CBaseKeyword extends CEosSingularBase {

	const TABLE_NAME = 'public.keywords';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSettingsTemplateId;
	protected $m_intMessageOriginatorId;
	protected $m_intMessageTypeId;
	protected $m_strKeyword;
	protected $m_strEmailAddress;
	protected $m_strMatchedMessage;
	protected $m_strUnmatchedMessage;
	protected $m_strSelectionDatetime;
	protected $m_strSuspendedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['settings_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSettingsTemplateId', trim( $arrValues['settings_template_id'] ) ); elseif( isset( $arrValues['settings_template_id'] ) ) $this->setSettingsTemplateId( $arrValues['settings_template_id'] );
		if( isset( $arrValues['message_originator_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageOriginatorId', trim( $arrValues['message_originator_id'] ) ); elseif( isset( $arrValues['message_originator_id'] ) ) $this->setMessageOriginatorId( $arrValues['message_originator_id'] );
		if( isset( $arrValues['message_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageTypeId', trim( $arrValues['message_type_id'] ) ); elseif( isset( $arrValues['message_type_id'] ) ) $this->setMessageTypeId( $arrValues['message_type_id'] );
		if( isset( $arrValues['keyword'] ) && $boolDirectSet ) $this->set( 'm_strKeyword', trim( stripcslashes( $arrValues['keyword'] ) ) ); elseif( isset( $arrValues['keyword'] ) ) $this->setKeyword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['keyword'] ) : $arrValues['keyword'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['matched_message'] ) && $boolDirectSet ) $this->set( 'm_strMatchedMessage', trim( stripcslashes( $arrValues['matched_message'] ) ) ); elseif( isset( $arrValues['matched_message'] ) ) $this->setMatchedMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['matched_message'] ) : $arrValues['matched_message'] );
		if( isset( $arrValues['unmatched_message'] ) && $boolDirectSet ) $this->set( 'm_strUnmatchedMessage', trim( stripcslashes( $arrValues['unmatched_message'] ) ) ); elseif( isset( $arrValues['unmatched_message'] ) ) $this->setUnmatchedMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['unmatched_message'] ) : $arrValues['unmatched_message'] );
		if( isset( $arrValues['selection_datetime'] ) && $boolDirectSet ) $this->set( 'm_strSelectionDatetime', trim( $arrValues['selection_datetime'] ) ); elseif( isset( $arrValues['selection_datetime'] ) ) $this->setSelectionDatetime( $arrValues['selection_datetime'] );
		if( isset( $arrValues['suspended_on'] ) && $boolDirectSet ) $this->set( 'm_strSuspendedOn', trim( $arrValues['suspended_on'] ) ); elseif( isset( $arrValues['suspended_on'] ) ) $this->setSuspendedOn( $arrValues['suspended_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSettingsTemplateId( $intSettingsTemplateId ) {
		$this->set( 'm_intSettingsTemplateId', CStrings::strToIntDef( $intSettingsTemplateId, NULL, false ) );
	}

	public function getSettingsTemplateId() {
		return $this->m_intSettingsTemplateId;
	}

	public function sqlSettingsTemplateId() {
		return ( true == isset( $this->m_intSettingsTemplateId ) ) ? ( string ) $this->m_intSettingsTemplateId : 'NULL';
	}

	public function setMessageOriginatorId( $intMessageOriginatorId ) {
		$this->set( 'm_intMessageOriginatorId', CStrings::strToIntDef( $intMessageOriginatorId, NULL, false ) );
	}

	public function getMessageOriginatorId() {
		return $this->m_intMessageOriginatorId;
	}

	public function sqlMessageOriginatorId() {
		return ( true == isset( $this->m_intMessageOriginatorId ) ) ? ( string ) $this->m_intMessageOriginatorId : 'NULL';
	}

	public function setMessageTypeId( $intMessageTypeId ) {
		$this->set( 'm_intMessageTypeId', CStrings::strToIntDef( $intMessageTypeId, NULL, false ) );
	}

	public function getMessageTypeId() {
		return $this->m_intMessageTypeId;
	}

	public function sqlMessageTypeId() {
		return ( true == isset( $this->m_intMessageTypeId ) ) ? ( string ) $this->m_intMessageTypeId : 'NULL';
	}

	public function setKeyword( $strKeyword ) {
		$this->set( 'm_strKeyword', CStrings::strTrimDef( $strKeyword, 80, NULL, true ) );
	}

	public function getKeyword() {
		return $this->m_strKeyword;
	}

	public function sqlKeyword() {
		return ( true == isset( $this->m_strKeyword ) ) ? '\'' . addslashes( $this->m_strKeyword ) . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setMatchedMessage( $strMatchedMessage ) {
		$this->set( 'm_strMatchedMessage', CStrings::strTrimDef( $strMatchedMessage, 160, NULL, true ) );
	}

	public function getMatchedMessage() {
		return $this->m_strMatchedMessage;
	}

	public function sqlMatchedMessage() {
		return ( true == isset( $this->m_strMatchedMessage ) ) ? '\'' . addslashes( $this->m_strMatchedMessage ) . '\'' : 'NULL';
	}

	public function setUnmatchedMessage( $strUnmatchedMessage ) {
		$this->set( 'm_strUnmatchedMessage', CStrings::strTrimDef( $strUnmatchedMessage, 160, NULL, true ) );
	}

	public function getUnmatchedMessage() {
		return $this->m_strUnmatchedMessage;
	}

	public function sqlUnmatchedMessage() {
		return ( true == isset( $this->m_strUnmatchedMessage ) ) ? '\'' . addslashes( $this->m_strUnmatchedMessage ) . '\'' : 'NULL';
	}

	public function setSelectionDatetime( $strSelectionDatetime ) {
		$this->set( 'm_strSelectionDatetime', CStrings::strTrimDef( $strSelectionDatetime, -1, NULL, true ) );
	}

	public function getSelectionDatetime() {
		return $this->m_strSelectionDatetime;
	}

	public function sqlSelectionDatetime() {
		return ( true == isset( $this->m_strSelectionDatetime ) ) ? '\'' . $this->m_strSelectionDatetime . '\'' : 'NULL';
	}

	public function setSuspendedOn( $strSuspendedOn ) {
		$this->set( 'm_strSuspendedOn', CStrings::strTrimDef( $strSuspendedOn, -1, NULL, true ) );
	}

	public function getSuspendedOn() {
		return $this->m_strSuspendedOn;
	}

	public function sqlSuspendedOn() {
		return ( true == isset( $this->m_strSuspendedOn ) ) ? '\'' . $this->m_strSuspendedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, settings_template_id, message_originator_id, message_type_id, keyword, email_address, matched_message, unmatched_message, selection_datetime, suspended_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSettingsTemplateId() . ', ' .
 						$this->sqlMessageOriginatorId() . ', ' .
 						$this->sqlMessageTypeId() . ', ' .
 						$this->sqlKeyword() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlMatchedMessage() . ', ' .
 						$this->sqlUnmatchedMessage() . ', ' .
 						$this->sqlSelectionDatetime() . ', ' .
 						$this->sqlSuspendedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' settings_template_id = ' . $this->sqlSettingsTemplateId() . ','; } elseif( true == array_key_exists( 'SettingsTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' settings_template_id = ' . $this->sqlSettingsTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_originator_id = ' . $this->sqlMessageOriginatorId() . ','; } elseif( true == array_key_exists( 'MessageOriginatorId', $this->getChangedColumns() ) ) { $strSql .= ' message_originator_id = ' . $this->sqlMessageOriginatorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_type_id = ' . $this->sqlMessageTypeId() . ','; } elseif( true == array_key_exists( 'MessageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' message_type_id = ' . $this->sqlMessageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keyword = ' . $this->sqlKeyword() . ','; } elseif( true == array_key_exists( 'Keyword', $this->getChangedColumns() ) ) { $strSql .= ' keyword = ' . $this->sqlKeyword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' matched_message = ' . $this->sqlMatchedMessage() . ','; } elseif( true == array_key_exists( 'MatchedMessage', $this->getChangedColumns() ) ) { $strSql .= ' matched_message = ' . $this->sqlMatchedMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unmatched_message = ' . $this->sqlUnmatchedMessage() . ','; } elseif( true == array_key_exists( 'UnmatchedMessage', $this->getChangedColumns() ) ) { $strSql .= ' unmatched_message = ' . $this->sqlUnmatchedMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' selection_datetime = ' . $this->sqlSelectionDatetime() . ','; } elseif( true == array_key_exists( 'SelectionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' selection_datetime = ' . $this->sqlSelectionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' suspended_on = ' . $this->sqlSuspendedOn() . ','; } elseif( true == array_key_exists( 'SuspendedOn', $this->getChangedColumns() ) ) { $strSql .= ' suspended_on = ' . $this->sqlSuspendedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'settings_template_id' => $this->getSettingsTemplateId(),
			'message_originator_id' => $this->getMessageOriginatorId(),
			'message_type_id' => $this->getMessageTypeId(),
			'keyword' => $this->getKeyword(),
			'email_address' => $this->getEmailAddress(),
			'matched_message' => $this->getMatchedMessage(),
			'unmatched_message' => $this->getUnmatchedMessage(),
			'selection_datetime' => $this->getSelectionDatetime(),
			'suspended_on' => $this->getSuspendedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>