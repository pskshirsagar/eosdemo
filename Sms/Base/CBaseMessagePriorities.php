<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessagePriorities
 * Do not add any new functions to this class.
 */

class CBaseMessagePriorities extends CEosPluralBase {

	/**
	 * @return CMessagePriority[]
	 */
	public static function fetchMessagePriorities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMessagePriority', $objDatabase );
	}

	/**
	 * @return CMessagePriority
	 */
	public static function fetchMessagePriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMessagePriority', $objDatabase );
	}

	public static function fetchMessagePriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_priorities', $objDatabase );
	}

	public static function fetchMessagePriorityById( $intId, $objDatabase ) {
		return self::fetchMessagePriority( sprintf( 'SELECT * FROM message_priorities WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>