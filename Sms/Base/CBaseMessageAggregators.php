<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageAggregators
 * Do not add any new functions to this class.
 */

class CBaseMessageAggregators extends CEosPluralBase {

	/**
	 * @return CMessageAggregator[]
	 */
	public static function fetchMessageAggregators( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMessageAggregator', $objDatabase );
	}

	/**
	 * @return CMessageAggregator
	 */
	public static function fetchMessageAggregator( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMessageAggregator', $objDatabase );
	}

	public static function fetchMessageAggregatorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_aggregators', $objDatabase );
	}

	public static function fetchMessageAggregatorById( $intId, $objDatabase ) {
		return self::fetchMessageAggregator( sprintf( 'SELECT * FROM message_aggregators WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>