<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageEncodingTypes
 * Do not add any new functions to this class.
 */

class CBaseMessageEncodingTypes extends CEosPluralBase {

	/**
	 * @return CMessageEncodingType[]
	 */
	public static function fetchMessageEncodingTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMessageEncodingType', $objDatabase );
	}

	/**
	 * @return CMessageEncodingType
	 */
	public static function fetchMessageEncodingType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMessageEncodingType', $objDatabase );
	}

	public static function fetchMessageEncodingTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_encoding_types', $objDatabase );
	}

	public static function fetchMessageEncodingTypeById( $intId, $objDatabase ) {
		return self::fetchMessageEncodingType( sprintf( 'SELECT * FROM message_encoding_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>