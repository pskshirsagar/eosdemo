<?php

class CBaseMessage extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.messages';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intMessageAggregatorId;
	protected $m_intMessageOperatorId;
	protected $m_intMessageOriginatorId;
	protected $m_intMessageTypeId;
	protected $m_intMessageStatusTypeId;
	protected $m_intMessagePriorityId;
	protected $m_intMessageEncodingTypeId;
	protected $m_intMessageErrorTypeId;
	protected $m_intAccountId;
	protected $m_intTransactionId;
	protected $m_intPropertyId;
	protected $m_intCustomerId;
	protected $m_intCompanyEmployeeId;
	protected $m_intEmployeeId;
	protected $m_intOptInId;
	protected $m_intApplicantId;
	protected $m_intMessageId;
	protected $m_intCustomerSettingId;
	protected $m_intMaintenanceRequestId;
	protected $m_intKeywordId;
	protected $m_strRemotePrimaryKey;
	protected $m_strMessage;
	protected $m_strPhoneNumber;
	protected $m_strMessageDatetime;
	protected $m_strScheduledSendDatetime;
	protected $m_strSenderDescription;
	protected $m_intAttemptCount;
	protected $m_intLockNumber;
	protected $m_strLockedOn;
	protected $m_strSentOn;
	protected $m_strFailedOn;
	protected $m_strExpireOn;
	protected $m_strSoftConfirmOn;
	protected $m_strHardConfirmOn;
	protected $m_strParsedOn;
	protected $m_intIsFlashSms;
	protected $m_intIsInbound;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intEventId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intScheduledMessageId;

	public function __construct() {
		parent::__construct();

		$this->m_intAttemptCount = '0';
		$this->m_intIsFlashSms = '0';
		$this->m_intIsInbound = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['message_aggregator_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageAggregatorId', trim( $arrValues['message_aggregator_id'] ) ); elseif( isset( $arrValues['message_aggregator_id'] ) ) $this->setMessageAggregatorId( $arrValues['message_aggregator_id'] );
		if( isset( $arrValues['message_operator_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageOperatorId', trim( $arrValues['message_operator_id'] ) ); elseif( isset( $arrValues['message_operator_id'] ) ) $this->setMessageOperatorId( $arrValues['message_operator_id'] );
		if( isset( $arrValues['message_originator_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageOriginatorId', trim( $arrValues['message_originator_id'] ) ); elseif( isset( $arrValues['message_originator_id'] ) ) $this->setMessageOriginatorId( $arrValues['message_originator_id'] );
		if( isset( $arrValues['message_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageTypeId', trim( $arrValues['message_type_id'] ) ); elseif( isset( $arrValues['message_type_id'] ) ) $this->setMessageTypeId( $arrValues['message_type_id'] );
		if( isset( $arrValues['message_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageStatusTypeId', trim( $arrValues['message_status_type_id'] ) ); elseif( isset( $arrValues['message_status_type_id'] ) ) $this->setMessageStatusTypeId( $arrValues['message_status_type_id'] );
		if( isset( $arrValues['message_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intMessagePriorityId', trim( $arrValues['message_priority_id'] ) ); elseif( isset( $arrValues['message_priority_id'] ) ) $this->setMessagePriorityId( $arrValues['message_priority_id'] );
		if( isset( $arrValues['message_encoding_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageEncodingTypeId', trim( $arrValues['message_encoding_type_id'] ) ); elseif( isset( $arrValues['message_encoding_type_id'] ) ) $this->setMessageEncodingTypeId( $arrValues['message_encoding_type_id'] );
		if( isset( $arrValues['message_error_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageErrorTypeId', trim( $arrValues['message_error_type_id'] ) ); elseif( isset( $arrValues['message_error_type_id'] ) ) $this->setMessageErrorTypeId( $arrValues['message_error_type_id'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionId', trim( $arrValues['transaction_id'] ) ); elseif( isset( $arrValues['transaction_id'] ) ) $this->setTransactionId( $arrValues['transaction_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['opt_in_id'] ) && $boolDirectSet ) $this->set( 'm_intOptInId', trim( $arrValues['opt_in_id'] ) ); elseif( isset( $arrValues['opt_in_id'] ) ) $this->setOptInId( $arrValues['opt_in_id'] );
		if( isset( $arrValues['applicant_id'] ) && $boolDirectSet ) $this->set( 'm_intApplicantId', trim( $arrValues['applicant_id'] ) ); elseif( isset( $arrValues['applicant_id'] ) ) $this->setApplicantId( $arrValues['applicant_id'] );
		if( isset( $arrValues['message_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageId', trim( $arrValues['message_id'] ) ); elseif( isset( $arrValues['message_id'] ) ) $this->setMessageId( $arrValues['message_id'] );
		if( isset( $arrValues['customer_setting_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerSettingId', trim( $arrValues['customer_setting_id'] ) ); elseif( isset( $arrValues['customer_setting_id'] ) ) $this->setCustomerSettingId( $arrValues['customer_setting_id'] );
		if( isset( $arrValues['maintenance_request_id'] ) && $boolDirectSet ) $this->set( 'm_intMaintenanceRequestId', trim( $arrValues['maintenance_request_id'] ) ); elseif( isset( $arrValues['maintenance_request_id'] ) ) $this->setMaintenanceRequestId( $arrValues['maintenance_request_id'] );
		if( isset( $arrValues['keyword_id'] ) && $boolDirectSet ) $this->set( 'm_intKeywordId', trim( $arrValues['keyword_id'] ) ); elseif( isset( $arrValues['keyword_id'] ) ) $this->setKeywordId( $arrValues['keyword_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['message'] ) && $boolDirectSet ) $this->set( 'm_strMessage', trim( $arrValues['message'] ) ); elseif( isset( $arrValues['message'] ) ) $this->setMessage( $arrValues['message'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['message_datetime'] ) && $boolDirectSet ) $this->set( 'm_strMessageDatetime', trim( $arrValues['message_datetime'] ) ); elseif( isset( $arrValues['message_datetime'] ) ) $this->setMessageDatetime( $arrValues['message_datetime'] );
		if( isset( $arrValues['scheduled_send_datetime'] ) && $boolDirectSet ) $this->set( 'm_strScheduledSendDatetime', trim( $arrValues['scheduled_send_datetime'] ) ); elseif( isset( $arrValues['scheduled_send_datetime'] ) ) $this->setScheduledSendDatetime( $arrValues['scheduled_send_datetime'] );
		if( isset( $arrValues['sender_description'] ) && $boolDirectSet ) $this->set( 'm_strSenderDescription', trim( $arrValues['sender_description'] ) ); elseif( isset( $arrValues['sender_description'] ) ) $this->setSenderDescription( $arrValues['sender_description'] );
		if( isset( $arrValues['attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intAttemptCount', trim( $arrValues['attempt_count'] ) ); elseif( isset( $arrValues['attempt_count'] ) ) $this->setAttemptCount( $arrValues['attempt_count'] );
		if( isset( $arrValues['lock_number'] ) && $boolDirectSet ) $this->set( 'm_intLockNumber', trim( $arrValues['lock_number'] ) ); elseif( isset( $arrValues['lock_number'] ) ) $this->setLockNumber( $arrValues['lock_number'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		if( isset( $arrValues['sent_on'] ) && $boolDirectSet ) $this->set( 'm_strSentOn', trim( $arrValues['sent_on'] ) ); elseif( isset( $arrValues['sent_on'] ) ) $this->setSentOn( $arrValues['sent_on'] );
		if( isset( $arrValues['failed_on'] ) && $boolDirectSet ) $this->set( 'm_strFailedOn', trim( $arrValues['failed_on'] ) ); elseif( isset( $arrValues['failed_on'] ) ) $this->setFailedOn( $arrValues['failed_on'] );
		if( isset( $arrValues['expire_on'] ) && $boolDirectSet ) $this->set( 'm_strExpireOn', trim( $arrValues['expire_on'] ) ); elseif( isset( $arrValues['expire_on'] ) ) $this->setExpireOn( $arrValues['expire_on'] );
		if( isset( $arrValues['soft_confirm_on'] ) && $boolDirectSet ) $this->set( 'm_strSoftConfirmOn', trim( $arrValues['soft_confirm_on'] ) ); elseif( isset( $arrValues['soft_confirm_on'] ) ) $this->setSoftConfirmOn( $arrValues['soft_confirm_on'] );
		if( isset( $arrValues['hard_confirm_on'] ) && $boolDirectSet ) $this->set( 'm_strHardConfirmOn', trim( $arrValues['hard_confirm_on'] ) ); elseif( isset( $arrValues['hard_confirm_on'] ) ) $this->setHardConfirmOn( $arrValues['hard_confirm_on'] );
		if( isset( $arrValues['parsed_on'] ) && $boolDirectSet ) $this->set( 'm_strParsedOn', trim( $arrValues['parsed_on'] ) ); elseif( isset( $arrValues['parsed_on'] ) ) $this->setParsedOn( $arrValues['parsed_on'] );
		if( isset( $arrValues['is_flash_sms'] ) && $boolDirectSet ) $this->set( 'm_intIsFlashSms', trim( $arrValues['is_flash_sms'] ) ); elseif( isset( $arrValues['is_flash_sms'] ) ) $this->setIsFlashSms( $arrValues['is_flash_sms'] );
		if( isset( $arrValues['is_inbound'] ) && $boolDirectSet ) $this->set( 'm_intIsInbound', trim( $arrValues['is_inbound'] ) ); elseif( isset( $arrValues['is_inbound'] ) ) $this->setIsInbound( $arrValues['is_inbound'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['event_id'] ) && $boolDirectSet ) $this->set( 'm_intEventId', trim( $arrValues['event_id'] ) ); elseif( isset( $arrValues['event_id'] ) ) $this->setEventId( $arrValues['event_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['scheduled_message_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledMessageId', trim( $arrValues['scheduled_message_id'] ) ); elseif( isset( $arrValues['scheduled_message_id'] ) ) $this->setScheduledMessageId( $arrValues['scheduled_message_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setMessageAggregatorId( $intMessageAggregatorId ) {
		$this->set( 'm_intMessageAggregatorId', CStrings::strToIntDef( $intMessageAggregatorId, NULL, false ) );
	}

	public function getMessageAggregatorId() {
		return $this->m_intMessageAggregatorId;
	}

	public function sqlMessageAggregatorId() {
		return ( true == isset( $this->m_intMessageAggregatorId ) ) ? ( string ) $this->m_intMessageAggregatorId : 'NULL';
	}

	public function setMessageOperatorId( $intMessageOperatorId ) {
		$this->set( 'm_intMessageOperatorId', CStrings::strToIntDef( $intMessageOperatorId, NULL, false ) );
	}

	public function getMessageOperatorId() {
		return $this->m_intMessageOperatorId;
	}

	public function sqlMessageOperatorId() {
		return ( true == isset( $this->m_intMessageOperatorId ) ) ? ( string ) $this->m_intMessageOperatorId : 'NULL';
	}

	public function setMessageOriginatorId( $intMessageOriginatorId ) {
		$this->set( 'm_intMessageOriginatorId', CStrings::strToIntDef( $intMessageOriginatorId, NULL, false ) );
	}

	public function getMessageOriginatorId() {
		return $this->m_intMessageOriginatorId;
	}

	public function sqlMessageOriginatorId() {
		return ( true == isset( $this->m_intMessageOriginatorId ) ) ? ( string ) $this->m_intMessageOriginatorId : 'NULL';
	}

	public function setMessageTypeId( $intMessageTypeId ) {
		$this->set( 'm_intMessageTypeId', CStrings::strToIntDef( $intMessageTypeId, NULL, false ) );
	}

	public function getMessageTypeId() {
		return $this->m_intMessageTypeId;
	}

	public function sqlMessageTypeId() {
		return ( true == isset( $this->m_intMessageTypeId ) ) ? ( string ) $this->m_intMessageTypeId : 'NULL';
	}

	public function setMessageStatusTypeId( $intMessageStatusTypeId ) {
		$this->set( 'm_intMessageStatusTypeId', CStrings::strToIntDef( $intMessageStatusTypeId, NULL, false ) );
	}

	public function getMessageStatusTypeId() {
		return $this->m_intMessageStatusTypeId;
	}

	public function sqlMessageStatusTypeId() {
		return ( true == isset( $this->m_intMessageStatusTypeId ) ) ? ( string ) $this->m_intMessageStatusTypeId : 'NULL';
	}

	public function setMessagePriorityId( $intMessagePriorityId ) {
		$this->set( 'm_intMessagePriorityId', CStrings::strToIntDef( $intMessagePriorityId, NULL, false ) );
	}

	public function getMessagePriorityId() {
		return $this->m_intMessagePriorityId;
	}

	public function sqlMessagePriorityId() {
		return ( true == isset( $this->m_intMessagePriorityId ) ) ? ( string ) $this->m_intMessagePriorityId : 'NULL';
	}

	public function setMessageEncodingTypeId( $intMessageEncodingTypeId ) {
		$this->set( 'm_intMessageEncodingTypeId', CStrings::strToIntDef( $intMessageEncodingTypeId, NULL, false ) );
	}

	public function getMessageEncodingTypeId() {
		return $this->m_intMessageEncodingTypeId;
	}

	public function sqlMessageEncodingTypeId() {
		return ( true == isset( $this->m_intMessageEncodingTypeId ) ) ? ( string ) $this->m_intMessageEncodingTypeId : 'NULL';
	}

	public function setMessageErrorTypeId( $intMessageErrorTypeId ) {
		$this->set( 'm_intMessageErrorTypeId', CStrings::strToIntDef( $intMessageErrorTypeId, NULL, false ) );
	}

	public function getMessageErrorTypeId() {
		return $this->m_intMessageErrorTypeId;
	}

	public function sqlMessageErrorTypeId() {
		return ( true == isset( $this->m_intMessageErrorTypeId ) ) ? ( string ) $this->m_intMessageErrorTypeId : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setTransactionId( $intTransactionId ) {
		$this->set( 'm_intTransactionId', CStrings::strToIntDef( $intTransactionId, NULL, false ) );
	}

	public function getTransactionId() {
		return $this->m_intTransactionId;
	}

	public function sqlTransactionId() {
		return ( true == isset( $this->m_intTransactionId ) ) ? ( string ) $this->m_intTransactionId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setOptInId( $intOptInId ) {
		$this->set( 'm_intOptInId', CStrings::strToIntDef( $intOptInId, NULL, false ) );
	}

	public function getOptInId() {
		return $this->m_intOptInId;
	}

	public function sqlOptInId() {
		return ( true == isset( $this->m_intOptInId ) ) ? ( string ) $this->m_intOptInId : 'NULL';
	}

	public function setApplicantId( $intApplicantId ) {
		$this->set( 'm_intApplicantId', CStrings::strToIntDef( $intApplicantId, NULL, false ) );
	}

	public function getApplicantId() {
		return $this->m_intApplicantId;
	}

	public function sqlApplicantId() {
		return ( true == isset( $this->m_intApplicantId ) ) ? ( string ) $this->m_intApplicantId : 'NULL';
	}

	public function setMessageId( $intMessageId ) {
		$this->set( 'm_intMessageId', CStrings::strToIntDef( $intMessageId, NULL, false ) );
	}

	public function getMessageId() {
		return $this->m_intMessageId;
	}

	public function sqlMessageId() {
		return ( true == isset( $this->m_intMessageId ) ) ? ( string ) $this->m_intMessageId : 'NULL';
	}

	public function setCustomerSettingId( $intCustomerSettingId ) {
		$this->set( 'm_intCustomerSettingId', CStrings::strToIntDef( $intCustomerSettingId, NULL, false ) );
	}

	public function getCustomerSettingId() {
		return $this->m_intCustomerSettingId;
	}

	public function sqlCustomerSettingId() {
		return ( true == isset( $this->m_intCustomerSettingId ) ) ? ( string ) $this->m_intCustomerSettingId : 'NULL';
	}

	public function setMaintenanceRequestId( $intMaintenanceRequestId ) {
		$this->set( 'm_intMaintenanceRequestId', CStrings::strToIntDef( $intMaintenanceRequestId, NULL, false ) );
	}

	public function getMaintenanceRequestId() {
		return $this->m_intMaintenanceRequestId;
	}

	public function sqlMaintenanceRequestId() {
		return ( true == isset( $this->m_intMaintenanceRequestId ) ) ? ( string ) $this->m_intMaintenanceRequestId : 'NULL';
	}

	public function setKeywordId( $intKeywordId ) {
		$this->set( 'm_intKeywordId', CStrings::strToIntDef( $intKeywordId, NULL, false ) );
	}

	public function getKeywordId() {
		return $this->m_intKeywordId;
	}

	public function sqlKeywordId() {
		return ( true == isset( $this->m_intKeywordId ) ) ? ( string ) $this->m_intKeywordId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 80, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRemotePrimaryKey ) : '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setMessage( $strMessage ) {
		$this->set( 'm_strMessage', CStrings::strTrimDef( $strMessage, 1600, NULL, true ) );
	}

	public function getMessage() {
		return $this->m_strMessage;
	}

	public function sqlMessage() {
		return ( true == isset( $this->m_strMessage ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMessage ) : '\'' . addslashes( $this->m_strMessage ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setMessageDatetime( $strMessageDatetime ) {
		$this->set( 'm_strMessageDatetime', CStrings::strTrimDef( $strMessageDatetime, -1, NULL, true ) );
	}

	public function getMessageDatetime() {
		return $this->m_strMessageDatetime;
	}

	public function sqlMessageDatetime() {
		return ( true == isset( $this->m_strMessageDatetime ) ) ? '\'' . $this->m_strMessageDatetime . '\'' : 'NOW()';
	}

	public function setScheduledSendDatetime( $strScheduledSendDatetime ) {
		$this->set( 'm_strScheduledSendDatetime', CStrings::strTrimDef( $strScheduledSendDatetime, -1, NULL, true ) );
	}

	public function getScheduledSendDatetime() {
		return $this->m_strScheduledSendDatetime;
	}

	public function sqlScheduledSendDatetime() {
		return ( true == isset( $this->m_strScheduledSendDatetime ) ) ? '\'' . $this->m_strScheduledSendDatetime . '\'' : 'NOW()';
	}

	public function setSenderDescription( $strSenderDescription ) {
		$this->set( 'm_strSenderDescription', CStrings::strTrimDef( $strSenderDescription, 255, NULL, true ) );
	}

	public function getSenderDescription() {
		return $this->m_strSenderDescription;
	}

	public function sqlSenderDescription() {
		return ( true == isset( $this->m_strSenderDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSenderDescription ) : '\'' . addslashes( $this->m_strSenderDescription ) . '\'' ) : 'NULL';
	}

	public function setAttemptCount( $intAttemptCount ) {
		$this->set( 'm_intAttemptCount', CStrings::strToIntDef( $intAttemptCount, NULL, false ) );
	}

	public function getAttemptCount() {
		return $this->m_intAttemptCount;
	}

	public function sqlAttemptCount() {
		return ( true == isset( $this->m_intAttemptCount ) ) ? ( string ) $this->m_intAttemptCount : '0';
	}

	public function setLockNumber( $intLockNumber ) {
		$this->set( 'm_intLockNumber', CStrings::strToIntDef( $intLockNumber, NULL, false ) );
	}

	public function getLockNumber() {
		return $this->m_intLockNumber;
	}

	public function sqlLockNumber() {
		return ( true == isset( $this->m_intLockNumber ) ) ? ( string ) $this->m_intLockNumber : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function setSentOn( $strSentOn ) {
		$this->set( 'm_strSentOn', CStrings::strTrimDef( $strSentOn, -1, NULL, true ) );
	}

	public function getSentOn() {
		return $this->m_strSentOn;
	}

	public function sqlSentOn() {
		return ( true == isset( $this->m_strSentOn ) ) ? '\'' . $this->m_strSentOn . '\'' : 'NULL';
	}

	public function setFailedOn( $strFailedOn ) {
		$this->set( 'm_strFailedOn', CStrings::strTrimDef( $strFailedOn, -1, NULL, true ) );
	}

	public function getFailedOn() {
		return $this->m_strFailedOn;
	}

	public function sqlFailedOn() {
		return ( true == isset( $this->m_strFailedOn ) ) ? '\'' . $this->m_strFailedOn . '\'' : 'NULL';
	}

	public function setExpireOn( $strExpireOn ) {
		$this->set( 'm_strExpireOn', CStrings::strTrimDef( $strExpireOn, -1, NULL, true ) );
	}

	public function getExpireOn() {
		return $this->m_strExpireOn;
	}

	public function sqlExpireOn() {
		return ( true == isset( $this->m_strExpireOn ) ) ? '\'' . $this->m_strExpireOn . '\'' : 'NULL';
	}

	public function setSoftConfirmOn( $strSoftConfirmOn ) {
		$this->set( 'm_strSoftConfirmOn', CStrings::strTrimDef( $strSoftConfirmOn, -1, NULL, true ) );
	}

	public function getSoftConfirmOn() {
		return $this->m_strSoftConfirmOn;
	}

	public function sqlSoftConfirmOn() {
		return ( true == isset( $this->m_strSoftConfirmOn ) ) ? '\'' . $this->m_strSoftConfirmOn . '\'' : 'NULL';
	}

	public function setHardConfirmOn( $strHardConfirmOn ) {
		$this->set( 'm_strHardConfirmOn', CStrings::strTrimDef( $strHardConfirmOn, -1, NULL, true ) );
	}

	public function getHardConfirmOn() {
		return $this->m_strHardConfirmOn;
	}

	public function sqlHardConfirmOn() {
		return ( true == isset( $this->m_strHardConfirmOn ) ) ? '\'' . $this->m_strHardConfirmOn . '\'' : 'NULL';
	}

	public function setParsedOn( $strParsedOn ) {
		$this->set( 'm_strParsedOn', CStrings::strTrimDef( $strParsedOn, -1, NULL, true ) );
	}

	public function getParsedOn() {
		return $this->m_strParsedOn;
	}

	public function sqlParsedOn() {
		return ( true == isset( $this->m_strParsedOn ) ) ? '\'' . $this->m_strParsedOn . '\'' : 'NULL';
	}

	public function setIsFlashSms( $intIsFlashSms ) {
		$this->set( 'm_intIsFlashSms', CStrings::strToIntDef( $intIsFlashSms, NULL, false ) );
	}

	public function getIsFlashSms() {
		return $this->m_intIsFlashSms;
	}

	public function sqlIsFlashSms() {
		return ( true == isset( $this->m_intIsFlashSms ) ) ? ( string ) $this->m_intIsFlashSms : '0';
	}

	public function setIsInbound( $intIsInbound ) {
		$this->set( 'm_intIsInbound', CStrings::strToIntDef( $intIsInbound, NULL, false ) );
	}

	public function getIsInbound() {
		return $this->m_intIsInbound;
	}

	public function sqlIsInbound() {
		return ( true == isset( $this->m_intIsInbound ) ) ? ( string ) $this->m_intIsInbound : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setEventId( $intEventId ) {
		$this->set( 'm_intEventId', CStrings::strToIntDef( $intEventId, NULL, false ) );
	}

	public function getEventId() {
		return $this->m_intEventId;
	}

	public function sqlEventId() {
		return ( true == isset( $this->m_intEventId ) ) ? ( string ) $this->m_intEventId : 'NULL';
	}

	public function setScheduledMessageId( $intScheduledMessageId ) {
		$this->set( 'm_intScheduledMessageId', CStrings::strToIntDef( $intScheduledMessageId, NULL, false ) );
	}

	public function getScheduledMessageId() {
		return $this->m_intScheduledMessageId;
	}

	public function sqlScheduledMessageId() {
		return ( true == isset( $this->m_intScheduledMessageId ) ) ? ( string ) $this->m_intScheduledMessageId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, message_aggregator_id, message_operator_id, message_originator_id, message_type_id, message_status_type_id, message_priority_id, message_encoding_type_id, message_error_type_id, account_id, transaction_id, property_id, customer_id, company_employee_id, employee_id, opt_in_id, applicant_id, message_id, customer_setting_id, maintenance_request_id, keyword_id, remote_primary_key, message, phone_number, message_datetime, scheduled_send_datetime, sender_description, attempt_count, lock_number, locked_on, sent_on, failed_on, expire_on, soft_confirm_on, hard_confirm_on, parsed_on, is_flash_sms, is_inbound, updated_by, updated_on, created_by, created_on, event_id, details, scheduled_message_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlMessageAggregatorId() . ', ' .
						$this->sqlMessageOperatorId() . ', ' .
						$this->sqlMessageOriginatorId() . ', ' .
						$this->sqlMessageTypeId() . ', ' .
						$this->sqlMessageStatusTypeId() . ', ' .
						$this->sqlMessagePriorityId() . ', ' .
						$this->sqlMessageEncodingTypeId() . ', ' .
						$this->sqlMessageErrorTypeId() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlTransactionId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlCompanyEmployeeId() . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlOptInId() . ', ' .
						$this->sqlApplicantId() . ', ' .
						$this->sqlMessageId() . ', ' .
						$this->sqlCustomerSettingId() . ', ' .
						$this->sqlMaintenanceRequestId() . ', ' .
						$this->sqlKeywordId() . ', ' .
						$this->sqlRemotePrimaryKey() . ', ' .
						$this->sqlMessage() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlMessageDatetime() . ', ' .
						$this->sqlScheduledSendDatetime() . ', ' .
						$this->sqlSenderDescription() . ', ' .
						$this->sqlAttemptCount() . ', ' .
						$this->sqlLockNumber() . ', ' .
						$this->sqlLockedOn() . ', ' .
						$this->sqlSentOn() . ', ' .
						$this->sqlFailedOn() . ', ' .
						$this->sqlExpireOn() . ', ' .
						$this->sqlSoftConfirmOn() . ', ' .
						$this->sqlHardConfirmOn() . ', ' .
						$this->sqlParsedOn() . ', ' .
						$this->sqlIsFlashSms() . ', ' .
						$this->sqlIsInbound() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlEventId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlScheduledMessageId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_aggregator_id = ' . $this->sqlMessageAggregatorId(). ',' ; } elseif( true == array_key_exists( 'MessageAggregatorId', $this->getChangedColumns() ) ) { $strSql .= ' message_aggregator_id = ' . $this->sqlMessageAggregatorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_operator_id = ' . $this->sqlMessageOperatorId(). ',' ; } elseif( true == array_key_exists( 'MessageOperatorId', $this->getChangedColumns() ) ) { $strSql .= ' message_operator_id = ' . $this->sqlMessageOperatorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_originator_id = ' . $this->sqlMessageOriginatorId(). ',' ; } elseif( true == array_key_exists( 'MessageOriginatorId', $this->getChangedColumns() ) ) { $strSql .= ' message_originator_id = ' . $this->sqlMessageOriginatorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_type_id = ' . $this->sqlMessageTypeId(). ',' ; } elseif( true == array_key_exists( 'MessageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' message_type_id = ' . $this->sqlMessageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_status_type_id = ' . $this->sqlMessageStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'MessageStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' message_status_type_id = ' . $this->sqlMessageStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_priority_id = ' . $this->sqlMessagePriorityId(). ',' ; } elseif( true == array_key_exists( 'MessagePriorityId', $this->getChangedColumns() ) ) { $strSql .= ' message_priority_id = ' . $this->sqlMessagePriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_encoding_type_id = ' . $this->sqlMessageEncodingTypeId(). ',' ; } elseif( true == array_key_exists( 'MessageEncodingTypeId', $this->getChangedColumns() ) ) { $strSql .= ' message_encoding_type_id = ' . $this->sqlMessageEncodingTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_error_type_id = ' . $this->sqlMessageErrorTypeId(). ',' ; } elseif( true == array_key_exists( 'MessageErrorTypeId', $this->getChangedColumns() ) ) { $strSql .= ' message_error_type_id = ' . $this->sqlMessageErrorTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId(). ',' ; } elseif( true == array_key_exists( 'TransactionId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' opt_in_id = ' . $this->sqlOptInId(). ',' ; } elseif( true == array_key_exists( 'OptInId', $this->getChangedColumns() ) ) { $strSql .= ' opt_in_id = ' . $this->sqlOptInId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId(). ',' ; } elseif( true == array_key_exists( 'ApplicantId', $this->getChangedColumns() ) ) { $strSql .= ' applicant_id = ' . $this->sqlApplicantId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_id = ' . $this->sqlMessageId(). ',' ; } elseif( true == array_key_exists( 'MessageId', $this->getChangedColumns() ) ) { $strSql .= ' message_id = ' . $this->sqlMessageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_setting_id = ' . $this->sqlCustomerSettingId(). ',' ; } elseif( true == array_key_exists( 'CustomerSettingId', $this->getChangedColumns() ) ) { $strSql .= ' customer_setting_id = ' . $this->sqlCustomerSettingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId(). ',' ; } elseif( true == array_key_exists( 'MaintenanceRequestId', $this->getChangedColumns() ) ) { $strSql .= ' maintenance_request_id = ' . $this->sqlMaintenanceRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keyword_id = ' . $this->sqlKeywordId(). ',' ; } elseif( true == array_key_exists( 'KeywordId', $this->getChangedColumns() ) ) { $strSql .= ' keyword_id = ' . $this->sqlKeywordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message = ' . $this->sqlMessage(). ',' ; } elseif( true == array_key_exists( 'Message', $this->getChangedColumns() ) ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_datetime = ' . $this->sqlMessageDatetime(). ',' ; } elseif( true == array_key_exists( 'MessageDatetime', $this->getChangedColumns() ) ) { $strSql .= ' message_datetime = ' . $this->sqlMessageDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_send_datetime = ' . $this->sqlScheduledSendDatetime(). ',' ; } elseif( true == array_key_exists( 'ScheduledSendDatetime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_send_datetime = ' . $this->sqlScheduledSendDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sender_description = ' . $this->sqlSenderDescription(). ',' ; } elseif( true == array_key_exists( 'SenderDescription', $this->getChangedColumns() ) ) { $strSql .= ' sender_description = ' . $this->sqlSenderDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attempt_count = ' . $this->sqlAttemptCount(). ',' ; } elseif( true == array_key_exists( 'AttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' attempt_count = ' . $this->sqlAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lock_number = ' . $this->sqlLockNumber(). ',' ; } elseif( true == array_key_exists( 'LockNumber', $this->getChangedColumns() ) ) { $strSql .= ' lock_number = ' . $this->sqlLockNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn(). ',' ; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_on = ' . $this->sqlSentOn(). ',' ; } elseif( true == array_key_exists( 'SentOn', $this->getChangedColumns() ) ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn(). ',' ; } elseif( true == array_key_exists( 'FailedOn', $this->getChangedColumns() ) ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expire_on = ' . $this->sqlExpireOn(). ',' ; } elseif( true == array_key_exists( 'ExpireOn', $this->getChangedColumns() ) ) { $strSql .= ' expire_on = ' . $this->sqlExpireOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' soft_confirm_on = ' . $this->sqlSoftConfirmOn(). ',' ; } elseif( true == array_key_exists( 'SoftConfirmOn', $this->getChangedColumns() ) ) { $strSql .= ' soft_confirm_on = ' . $this->sqlSoftConfirmOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hard_confirm_on = ' . $this->sqlHardConfirmOn(). ',' ; } elseif( true == array_key_exists( 'HardConfirmOn', $this->getChangedColumns() ) ) { $strSql .= ' hard_confirm_on = ' . $this->sqlHardConfirmOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parsed_on = ' . $this->sqlParsedOn(). ',' ; } elseif( true == array_key_exists( 'ParsedOn', $this->getChangedColumns() ) ) { $strSql .= ' parsed_on = ' . $this->sqlParsedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_flash_sms = ' . $this->sqlIsFlashSms(). ',' ; } elseif( true == array_key_exists( 'IsFlashSms', $this->getChangedColumns() ) ) { $strSql .= ' is_flash_sms = ' . $this->sqlIsFlashSms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_inbound = ' . $this->sqlIsInbound(). ',' ; } elseif( true == array_key_exists( 'IsInbound', $this->getChangedColumns() ) ) { $strSql .= ' is_inbound = ' . $this->sqlIsInbound() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_id = ' . $this->sqlEventId(). ',' ; } elseif( true == array_key_exists( 'EventId', $this->getChangedColumns() ) ) { $strSql .= ' event_id = ' . $this->sqlEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_message_id = ' . $this->sqlScheduledMessageId(). ',' ; } elseif( true == array_key_exists( 'ScheduledMessageId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_message_id = ' . $this->sqlScheduledMessageId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'message_aggregator_id' => $this->getMessageAggregatorId(),
			'message_operator_id' => $this->getMessageOperatorId(),
			'message_originator_id' => $this->getMessageOriginatorId(),
			'message_type_id' => $this->getMessageTypeId(),
			'message_status_type_id' => $this->getMessageStatusTypeId(),
			'message_priority_id' => $this->getMessagePriorityId(),
			'message_encoding_type_id' => $this->getMessageEncodingTypeId(),
			'message_error_type_id' => $this->getMessageErrorTypeId(),
			'account_id' => $this->getAccountId(),
			'transaction_id' => $this->getTransactionId(),
			'property_id' => $this->getPropertyId(),
			'customer_id' => $this->getCustomerId(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'employee_id' => $this->getEmployeeId(),
			'opt_in_id' => $this->getOptInId(),
			'applicant_id' => $this->getApplicantId(),
			'message_id' => $this->getMessageId(),
			'customer_setting_id' => $this->getCustomerSettingId(),
			'maintenance_request_id' => $this->getMaintenanceRequestId(),
			'keyword_id' => $this->getKeywordId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'message' => $this->getMessage(),
			'phone_number' => $this->getPhoneNumber(),
			'message_datetime' => $this->getMessageDatetime(),
			'scheduled_send_datetime' => $this->getScheduledSendDatetime(),
			'sender_description' => $this->getSenderDescription(),
			'attempt_count' => $this->getAttemptCount(),
			'lock_number' => $this->getLockNumber(),
			'locked_on' => $this->getLockedOn(),
			'sent_on' => $this->getSentOn(),
			'failed_on' => $this->getFailedOn(),
			'expire_on' => $this->getExpireOn(),
			'soft_confirm_on' => $this->getSoftConfirmOn(),
			'hard_confirm_on' => $this->getHardConfirmOn(),
			'parsed_on' => $this->getParsedOn(),
			'is_flash_sms' => $this->getIsFlashSms(),
			'is_inbound' => $this->getIsInbound(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'event_id' => $this->getEventId(),
			'details' => $this->getDetails(),
			'scheduled_message_id' => $this->getScheduledMessageId()
		);
	}

}
?>