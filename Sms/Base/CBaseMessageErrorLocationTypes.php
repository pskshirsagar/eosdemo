<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageErrorLocationTypes
 * Do not add any new functions to this class.
 */

class CBaseMessageErrorLocationTypes extends CEosPluralBase {

	/**
	 * @return CMessageErrorLocationType[]
	 */
	public static function fetchMessageErrorLocationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMessageErrorLocationType', $objDatabase );
	}

	/**
	 * @return CMessageErrorLocationType
	 */
	public static function fetchMessageErrorLocationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMessageErrorLocationType', $objDatabase );
	}

	public static function fetchMessageErrorLocationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_error_location_types', $objDatabase );
	}

	public static function fetchMessageErrorLocationTypeById( $intId, $objDatabase ) {
		return self::fetchMessageErrorLocationType( sprintf( 'SELECT * FROM message_error_location_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>