<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseMessageStatusTypes extends CEosPluralBase {

	/**
	 * @return CMessageStatusType[]
	 */
	public static function fetchMessageStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CMessageStatusType::class, $objDatabase );
	}

	/**
	 * @return CMessageStatusType
	 */
	public static function fetchMessageStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CMessageStatusType::class, $objDatabase );
	}

	public static function fetchMessageStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'message_status_types', $objDatabase );
	}

	public static function fetchMessageStatusTypeById( $intId, $objDatabase ) {
		return self::fetchMessageStatusType( sprintf( 'SELECT * FROM message_status_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>