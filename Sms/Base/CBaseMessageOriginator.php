<?php

class CBaseMessageOriginator extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.message_originators';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intMessageAggregatorId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strNumber;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intCallPhoneNumberId;
	protected $m_strReleasedOn;
	protected $m_intReleasedBy;
	protected $m_strSuspendedOn;
	protected $m_intSuspendedBy;
	protected $m_strPublishedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['message_aggregator_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageAggregatorId', trim( $arrValues['message_aggregator_id'] ) ); elseif( isset( $arrValues['message_aggregator_id'] ) ) $this->setMessageAggregatorId( $arrValues['message_aggregator_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['number'] ) && $boolDirectSet ) $this->set( 'm_strNumber', trim( stripcslashes( $arrValues['number'] ) ) ); elseif( isset( $arrValues['number'] ) ) $this->setNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['number'] ) : $arrValues['number'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['call_phone_number_id'] ) && $boolDirectSet ) $this->set( 'm_intCallPhoneNumberId', trim( $arrValues['call_phone_number_id'] ) ); elseif( isset( $arrValues['call_phone_number_id'] ) ) $this->setCallPhoneNumberId( $arrValues['call_phone_number_id'] );
		if( isset( $arrValues['released_on'] ) && $boolDirectSet ) $this->set( 'm_strReleasedOn', trim( $arrValues['released_on'] ) ); elseif( isset( $arrValues['released_on'] ) ) $this->setReleasedOn( $arrValues['released_on'] );
		if( isset( $arrValues['released_by'] ) && $boolDirectSet ) $this->set( 'm_intReleasedBy', trim( $arrValues['released_by'] ) ); elseif( isset( $arrValues['released_by'] ) ) $this->setReleasedBy( $arrValues['released_by'] );
		if( isset( $arrValues['suspended_on'] ) && $boolDirectSet ) $this->set( 'm_strSuspendedOn', trim( $arrValues['suspended_on'] ) ); elseif( isset( $arrValues['suspended_on'] ) ) $this->setSuspendedOn( $arrValues['suspended_on'] );
		if( isset( $arrValues['suspended_by'] ) && $boolDirectSet ) $this->set( 'm_intSuspendedBy', trim( $arrValues['suspended_by'] ) ); elseif( isset( $arrValues['suspended_by'] ) ) $this->setSuspendedBy( $arrValues['suspended_by'] );
		if( isset( $arrValues['published_on'] ) && $boolDirectSet ) $this->set( 'm_strPublishedOn', trim( $arrValues['published_on'] ) ); elseif( isset( $arrValues['published_on'] ) ) $this->setPublishedOn( $arrValues['published_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setMessageAggregatorId( $intMessageAggregatorId ) {
		$this->set( 'm_intMessageAggregatorId', CStrings::strToIntDef( $intMessageAggregatorId, NULL, false ) );
	}

	public function getMessageAggregatorId() {
		return $this->m_intMessageAggregatorId;
	}

	public function sqlMessageAggregatorId() {
		return ( true == isset( $this->m_intMessageAggregatorId ) ) ? ( string ) $this->m_intMessageAggregatorId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setNumber( $strNumber ) {
		$this->set( 'm_strNumber', CStrings::strTrimDef( $strNumber, 30, NULL, true ) );
	}

	public function getNumber() {
		return $this->m_strNumber;
	}

	public function sqlNumber() {
		return ( true == isset( $this->m_strNumber ) ) ? '\'' . addslashes( $this->m_strNumber ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCallPhoneNumberId( $intCallPhoneNumberId ) {
		$this->set( 'm_intCallPhoneNumberId', CStrings::strToIntDef( $intCallPhoneNumberId, NULL, false ) );
	}

	public function getCallPhoneNumberId() {
		return $this->m_intCallPhoneNumberId;
	}

	public function sqlCallPhoneNumberId() {
		return ( true == isset( $this->m_intCallPhoneNumberId ) ) ? ( string ) $this->m_intCallPhoneNumberId : 'NULL';
	}

	public function setReleasedOn( $strReleasedOn ) {
		$this->set( 'm_strReleasedOn', CStrings::strTrimDef( $strReleasedOn, -1, NULL, true ) );
	}

	public function getReleasedOn() {
		return $this->m_strReleasedOn;
	}

	public function sqlReleasedOn() {
		return ( true == isset( $this->m_strReleasedOn ) ) ? '\'' . $this->m_strReleasedOn . '\'' : 'NULL';
	}

	public function setReleasedBy( $intReleasedBy ) {
		$this->set( 'm_intReleasedBy', CStrings::strToIntDef( $intReleasedBy, NULL, false ) );
	}

	public function getReleasedBy() {
		return $this->m_intReleasedBy;
	}

	public function sqlReleasedBy() {
		return ( true == isset( $this->m_intReleasedBy ) ) ? ( string ) $this->m_intReleasedBy : 'NULL';
	}

	public function setSuspendedOn( $strSuspendedOn ) {
		$this->set( 'm_strSuspendedOn', CStrings::strTrimDef( $strSuspendedOn, -1, NULL, true ) );
	}

	public function getSuspendedOn() {
		return $this->m_strSuspendedOn;
	}

	public function sqlSuspendedOn() {
		return ( true == isset( $this->m_strSuspendedOn ) ) ? '\'' . $this->m_strSuspendedOn . '\'' : 'NULL';
	}

	public function setSuspendedBy( $intSuspendedBy ) {
		$this->set( 'm_intSuspendedBy', CStrings::strToIntDef( $intSuspendedBy, NULL, false ) );
	}

	public function getSuspendedBy() {
		return $this->m_intSuspendedBy;
	}

	public function sqlSuspendedBy() {
		return ( true == isset( $this->m_intSuspendedBy ) ) ? ( string ) $this->m_intSuspendedBy : 'NULL';
	}

	public function setPublishedOn( $strPublishedOn ) {
		$this->set( 'm_strPublishedOn', CStrings::strTrimDef( $strPublishedOn, -1, NULL, true ) );
	}

	public function getPublishedOn() {
		return $this->m_strPublishedOn;
	}

	public function sqlPublishedOn() {
		return ( true == isset( $this->m_strPublishedOn ) ) ? '\'' . $this->m_strPublishedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, message_aggregator_id, name, description, number, is_published, order_num, updated_by, updated_on, created_by, created_on, call_phone_number_id, released_on, released_by, suspended_on, suspended_by, published_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlMessageAggregatorId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlNumber() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCallPhoneNumberId() . ', ' .
						$this->sqlReleasedOn() . ', ' .
						$this->sqlReleasedBy() . ', ' .
						$this->sqlSuspendedOn() . ', ' .
						$this->sqlSuspendedBy() . ', ' .
						$this->sqlPublishedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_aggregator_id = ' . $this->sqlMessageAggregatorId(). ',' ; } elseif( true == array_key_exists( 'MessageAggregatorId', $this->getChangedColumns() ) ) { $strSql .= ' message_aggregator_id = ' . $this->sqlMessageAggregatorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number = ' . $this->sqlNumber(). ',' ; } elseif( true == array_key_exists( 'Number', $this->getChangedColumns() ) ) { $strSql .= ' number = ' . $this->sqlNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_phone_number_id = ' . $this->sqlCallPhoneNumberId(). ',' ; } elseif( true == array_key_exists( 'CallPhoneNumberId', $this->getChangedColumns() ) ) { $strSql .= ' call_phone_number_id = ' . $this->sqlCallPhoneNumberId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' released_on = ' . $this->sqlReleasedOn(). ',' ; } elseif( true == array_key_exists( 'ReleasedOn', $this->getChangedColumns() ) ) { $strSql .= ' released_on = ' . $this->sqlReleasedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' released_by = ' . $this->sqlReleasedBy(). ',' ; } elseif( true == array_key_exists( 'ReleasedBy', $this->getChangedColumns() ) ) { $strSql .= ' released_by = ' . $this->sqlReleasedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' suspended_on = ' . $this->sqlSuspendedOn(). ',' ; } elseif( true == array_key_exists( 'SuspendedOn', $this->getChangedColumns() ) ) { $strSql .= ' suspended_on = ' . $this->sqlSuspendedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' suspended_by = ' . $this->sqlSuspendedBy(). ',' ; } elseif( true == array_key_exists( 'SuspendedBy', $this->getChangedColumns() ) ) { $strSql .= ' suspended_by = ' . $this->sqlSuspendedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn(). ',' ; } elseif( true == array_key_exists( 'PublishedOn', $this->getChangedColumns() ) ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'message_aggregator_id' => $this->getMessageAggregatorId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'number' => $this->getNumber(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'call_phone_number_id' => $this->getCallPhoneNumberId(),
			'released_on' => $this->getReleasedOn(),
			'released_by' => $this->getReleasedBy(),
			'suspended_on' => $this->getSuspendedOn(),
			'suspended_by' => $this->getSuspendedBy(),
			'published_on' => $this->getPublishedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>