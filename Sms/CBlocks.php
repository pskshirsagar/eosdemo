<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CBlocks
 * Do not add any new functions to this class.
 */

// Consider a "Block" an entry in a Blacklist for Text Messages
class CBlocks extends CBaseBlocks {

	public static function fetchBlockById( $intId, $objSmsDatabase ) {
		return self::fetchBlock( 'SELECT * FROM blocks WHERE id = ' . ( int ) $intId, $objSmsDatabase );
	}

	public static function fetchBlocksByPhoneNumber( $strPhoneNumber, $objSmsDatabase ) {
		return self::fetchBlocks( 'SELECT * FROM blocks WHERE phone_number = \'' . addslashes( $strPhoneNumber ) . '\'', $objSmsDatabase );
	}

	public static function fetchPaginatedBlocks( $intPageNo, $intPageSize, $objBlocksFilter, $objSmsDatabase, $strOrderByFieldName = NULL, $strOrderField = NULL ) {

		$intOffset 	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit 	= ( int ) $intPageSize;

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$strSubSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$arrstrAndSearchParameters = self::fetchSearchCriteria( $objBlocksFilter );

		$strSql  = 'SELECT * FROM blocks WHERE 1 = 1 ';
		$strSql	.= ( ( false == is_null( $arrstrAndSearchParameters ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		$strOrderByFieldName = ( true == is_null( $strOrderByFieldName ) ) ? 'id' : $strOrderByFieldName;

		$strSql .= ' ORDER BY ' . ' ' . $strOrderByFieldName . ' ' . $strOrderField . $strSubSql;

		return self::fetchBlocks( $strSql, $objSmsDatabase );
	}

	public static function fetchSearchCriteria( $objBlocksFilter ) {

		$arrstrWhereParameters = array();

		if( true == valObj( $objBlocksFilter, 'CBlocksFilter' ) ) {
			if( 0 < strlen( $objBlocksFilter->getCid() ) )	$arrstrWhereParameters[] = 'cid=' . ( int ) $objBlocksFilter->getCid();
			if( 0 < strlen( $objBlocksFilter->getPropertyId() ) )			$arrstrWhereParameters[] = 'property_id=' . ( int ) $objBlocksFilter->getPropertyId();
			if( 0 < strlen( $objBlocksFilter->getKeywordId() ) )			$arrstrWhereParameters[] = 'keyword_id=' . ( int ) $objBlocksFilter->getKeywordId();
			if( 0 < strlen( $objBlocksFilter->getMessageTypeIds() ) ) 		$arrstrWhereParameters[] = 'message_type_id IN ( ' . $objBlocksFilter->getMessageTypeIds() . ' )';
			if( 0 < strlen( $objBlocksFilter->getBlockDateTime() ) ) 		$arrstrWhereParameters[] = 'block_datetime BETWEEN \'' . date( 'Y/m/d', strtotime( $objBlocksFilter->getBlockDateTime() ) ) . ' 00:00:00\'AND\'' . date( 'Y/m/d', strtotime( $objBlocksFilter->getBlockDateTime() ) ) . ' 23:59:59\'';
		}

   		return $arrstrWhereParameters;
	}

	public static function fetchBlocksCountBySearchParameters( $objBlocksFilter, $objSmsDatabase ) {

		$arrstrAndSearchParameters = self::fetchSearchCriteria( $objBlocksFilter );

		$strSql  = 'SELECT count(id) FROM blocks WHERE 1 = 1';
		$strSql	.= ( ( false == is_null( $arrstrAndSearchParameters ) && 0 < \Psi\Libraries\UtilFunctions\count( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '' );

		$arrintResponse = fetchData( $strSql, $objSmsDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchBlocksByPhoneNumberByMessageTypeId( $strPhoneNumber, $intMessageType, $objSmsDatabase ) {
		return self::fetchBlocks( 'SELECT * FROM blocks WHERE phone_number = \'' . ( string ) addslashes( $strPhoneNumber ) . '\' AND message_type_id = ' . ( int ) $intMessageType, $objSmsDatabase );
	}

	public static function fetchBlocksByPhoneNumberByMessageTypeIdByKeywordId( $strPhoneNumber, $intMessageType, $intKeywordId, $objSmsDatabase ) {
		return self::fetchBlocks( 'SELECT * FROM blocks WHERE phone_number = \'' . ( string ) addslashes( $strPhoneNumber ) . '\' AND message_type_id = ' . ( int ) $intMessageType . ' AND ( keyword_id IS NULL OR keyword_id = ' . ( int ) $intKeywordId . ')', $objSmsDatabase );
	}

	public static function fetchBlocksByPhoneNumberByMessageTypeIds( $strPhoneNumber, $arrintMessageTypeIds, $objSmsDatabase ) {
		if( false == valStr( $strPhoneNumber ) ) return;
		if( false == valArr( $arrintMessageTypeIds ) ) return;

		$strSql = 'SELECT * FROM blocks WHERE message_type_id IN ( ' . implode( ',', $arrintMessageTypeIds ) . ' )';
		$strSql .= ' AND ( phone_number = \'' . $strPhoneNumber . '\' OR phone_number = \'1' . $strPhoneNumber . '\' OR phone_number = \'' . substr( $strPhoneNumber, 1 ) . '\' ) ';

		return self::fetchBlocks( $strSql, $objSmsDatabase );
	}

	public static function fetchQuickSearchBlocks( $arrmixFilteredExplodedSearch,$objSmsDatabase ) {
		$strSql = 'SELECT
						*
				   FROM
					   blocks
				   WHERE
					   ( CAST( message_id as text )  LIKE \'%' . implode( '', $arrmixFilteredExplodedSearch ) . '%\') OR
					   ( CAST( id as text ) LIKE \'%' . implode( '', $arrmixFilteredExplodedSearch ) . '%\') OR  ( phone_number LIKE \'%' . implode( '', $arrmixFilteredExplodedSearch ) . '%\')  ORDER BY id LIMIT 10';

		return self::fetchBlocks( $strSql, $objSmsDatabase );
	}

	public static function fetchBlockedPhoneNumbersByMobileNumberByMessageTypeIds( $intMobileNumber, $arrintMessageTypeIds, $objSmsDatabase ) {
		$strMobileNumberSql = ' 1=1 ';

		if( 11 <= strlen( $intMobileNumber ) ) {
			$strMobileNumberSql .= ' AND phone_number LIKE \'%' . addslashes( \Psi\CStringService::singleton()->substr( $intMobileNumber, 1 ) ) . '%\'';
		} else {
			$strMobileNumberSql .= ' AND phone_number LIKE \'%' . addslashes( $intMobileNumber ) . '%\'';
		}

		$strSql = 'SELECT phone_number FROM blocks WHERE ' . $strMobileNumberSql . ' AND message_type_id IN ( ' . implode( ',', $arrintMessageTypeIds ) . ' )';

		return fetchData( $strSql, $objSmsDatabase );
	}

	public static function fetchBlocksByCustomerIdsByPropertyIdByCid( $arrintCustomerIds, $intPropertyId, $intCid, $objDatabase ) {
		if( false == valArr( $arrintCustomerIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						blocks
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . '
						AND customer_id IN ( ' . implode( ',', $arrintCustomerIds ) . ' )';

		return self::fetchBlocks( $strSql, $objDatabase );
	}

	public static function fetchBlocksCountByPropertyIdByPhoneNumberByMessageTypeIdsByCid( $intPropertyId, $strPhoneNumber, $arrintMessageTypeIds, $intCid, $objSmsDatabase ) {
		$strWhereSql = ' WHERE
							cid = ' . ( int ) $intCid . '
							AND property_id = ' . ( int ) $intPropertyId . '
							AND ( phone_number = \'' . $strPhoneNumber . '\' OR phone_number = \'1' . $strPhoneNumber . '\' )
							AND message_type_id IN ( ' . implode( ',', $arrintMessageTypeIds ) . ' )';
		return CBlocks::fetchRowCount( $strWhereSql, 'blocks', $objSmsDatabase );
	}

	public static function fetchBlocksByPropertyIdByPhoneNumberByMessageTypeIdsByCid( $intPropertyId, $strPhoneNumber, $arrintMessageTypeIds, $intCid, $objSmsDatabase ) {

		if( false == valArr( $arrintMessageTypeIds ) ) {
			return [];
		} else {
			$strSql = 'SELECT
						*
					FROM
						blocks
					WHERE
						cid = ' . ( int ) $intCid . '
						AND property_id = ' . ( int ) $intPropertyId . ' AND
						( phone_number = \'' . $strPhoneNumber . '\' OR phone_number = \'1' . $strPhoneNumber . '\' )  
						AND message_type_id IN ( ' . implode( ',', $arrintMessageTypeIds ) . ' )';
			return self::fetchBlocks( $strSql, $objSmsDatabase );
		}

	}

}
?>