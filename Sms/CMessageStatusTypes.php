<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageStatusTypes
 * Do not add any new functions to this class.
 */

class CMessageStatusTypes extends CBaseMessageStatusTypes {

	public static function fetchMessageStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMessageStatusType', $objDatabase, DATA_CACHE_MEMORY );
	}

	public static function fetchMessageStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMessageStatusType', $objDatabase, DATA_CACHE_MEMORY );
	}

	public static function fetchMessageStatusTypesByIds( $arrintMessageStatusTypeIds, $objSmsDatabase ) {
		if( false == valArr( $arrintMessageStatusTypeIds ) ) return NULL;
		return self::fetchMessageStatusTypes( 'SELECT * FROM message_status_types WHERE id IN ( ' . implode( ',', $arrintMessageStatusTypeIds ) . ' ) ORDER BY order_num', $objSmsDatabase );
	}

	public static function fetchAllMessageStatusTypes( $objSmsDatabase ) {
		return self::fetchMessageStatusTypes( 'SELECT * FROM message_status_types ORDER BY order_num', $objSmsDatabase );
    }
}
?>