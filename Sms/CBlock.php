<?php

class CBlock extends CBaseBlock {

    /**
     * Set Functions
     */

    // On block record we should always stroe phone number as numeric. PBY.

 	public function setPhoneNumber( $strPhoneNumber ) {
		$this->m_strPhoneNumber = preg_replace( '/[^0-9]/', '', $strPhoneNumber );
		// This is for time being.
		if( 10 == strlen( $this->m_strPhoneNumber ) ) {
			$this->m_strPhoneNumber = '1' . $this->m_strPhoneNumber;
		}
    }

	/**
	 * Validate Functions
	 */

    public function valId() {
        $boolIsValid = true;

        if( true == is_null( $this->getId() ) ) {
        	$boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Block ID is required.' ) );
        }

        return $boolIsValid;
    }

    public function valCid() {
        $boolIsValid = true;

        if( true == is_null( $this->getCid() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id is required.' ) );
        }

        return $boolIsValid;
    }

    public function valPropertyId() {
        $boolIsValid = true;

        if( true == is_null( $this->getPropertyId() ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property ID is required.' ) );
        }

        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
            case VALIDATE_INSERT:
            case VALIDATE_UPDATE:
            	$boolIsValid &= $this->valCid();
            	$boolIsValid &= $this->valPropertyId();
				break;

            case VALIDATE_DELETE:
            	$boolIsValid &= $this->valId();
            	break;

            default:
            	$boolIsValid = false;
            	break;
        }

        return $boolIsValid;
    }

    public function buildMessageType( $strMessage, $objSmsDatabase ) {

    	$strTokenOne   = '';
    	$strTokenTwo   = '';
    	$strTokenThree = '';

    	$strMessage = trim( preg_replace( '/[^a-zA-Z0-9 ]/', '', $strMessage ) );

    	$intTokenCount = \Psi\Libraries\UtilFunctions\count( explode( ' ', $strMessage ) );

    	if( 3 < $intTokenCount ) {
    		$this->setMessageTypeId( CMessageType::STOP );
    	} else {
			$arrstrTokens = explode( ' ', $strMessage );

			if( true == isset( $arrstrTokens[0] ) ) {
				$strTokenOne = \Psi\CStringService::singleton()->strtoupper( $arrstrTokens[0] );
			}

    		if( true == isset( $arrstrTokens[1] ) ) {
				$strTokenTwo = \Psi\CStringService::singleton()->strtoupper( $arrstrTokens[1] );
			}

    		if( true == isset( $arrstrTokens[2] ) ) {
				$strTokenThree = $arrstrTokens[2];
			}

			switch( $intTokenCount ) {
				case 1:
					switch( $strTokenOne ) {
						case CMessageType::HANDLE_END:
						case CMessageType::HANDLE_STOP:
						case CMessageType::HANDLE_STOPALL:
						case CMessageType::HANDLE_QUIT:
						case CMessageType::HANDLE_CANCEL:
						case CMessageType::HANDLE_UNSUBSCRIBE:
							$this->setMessageTypeId( CMessageType::STOP );
							break;

						default:
							// default case
							break;
					}
					break;

				case 2:
					switch( $strTokenOne ) {

						case CMessageType::HANDLE_STOP:
							switch( $strTokenTwo ) {

								case CMessageType::HANDLE_INFORMATION_REQUEST:
									$this->setMessageTypeId( CMessageType::INFORMATION_REQUEST );
									break;

								case CMessageType::HANDLE_MAINTENANCE_NOTIFICATION:
									$this->setMessageTypeId( CMessageType::MAINTENANCE_NOTIFICATION );
									break;

								case CMessageType::HANDLE_PROPERTY_NOTIFICATION:
									$this->setMessageTypeId( CMessageType::PROPERTY_NOTIFICATION );
									break;

								case CMessageType::HANDLE_RENT_REMINDER:
									$this->setMessageTypeId( CMessageType::RENT_REMINDER );
									break;

								case CMessageType::HANDLE_AVAILABILITY_ALERT:
									$this->setMessageTypeId( CMessageType::AVAILABILITY_ALERT );
									break;

								case CMessageType::HANDLE_APPLICATION_UPDATES:
									$this->setMessageTypeId( CMessageType::APPLICATION_UPDATES );
									break;

								case CMessageType::HANDLE_PACKAGE_NOTIFICATION:
									$this->setMessageTypeId( CMessageType::PACKAGE_NOTIFICATION );
									break;

								case 'ALL':
									$this->setMessageTypeId( CMessageType::STOP );
									break;

								default:
									$this->setMessageTypeId( CMessageType::STOP );
									break;
							}
							break;

						case CMessageType::HANDLE_END:
						case CMessageType::HANDLE_STOPALL:
						case CMessageType::HANDLE_QUIT:
						case CMessageType::HANDLE_CANCEL:
						case CMessageType::HANDLE_UNSUBSCRIBE:
							$this->setMessageTypeId( CMessageType::STOP );
							break;

						default:
							// default case
							break;
					}
					break;

				case 3:
					switch( $strTokenOne ) {
						case CMessageType::HANDLE_STOP:
							switch( $strTokenThree ) {

								case CMessageType::HANDLE_INFORMATION_REQUEST:
									$objKeyword = \Psi\Eos\Sms\CKeywords::createService()->fetchKeywordByKeyword( $strTokenTwo, $objSmsDatabase );

									if( true == valObj( $objKeyword, 'CKeyword' ) ) {
										$this->setMessageTypeId( CMessageType::INFORMATION_REQUEST );
										$this->setCid( $objKeyword->getCid() );
										$this->setPropertyId( $objKeyword->getPropertyId() );
									}
									break;

								default:
									$this->setMessageTypeId( CMessageType::STOP );
									break;
							}
							break;

						case CMessageType::HANDLE_END:
						case CMessageType::HANDLE_STOPALL:
						case CMessageType::HANDLE_QUIT:
						case CMessageType::HANDLE_CANCEL:
						case CMessageType::HANDLE_UNSUBSCRIBE:
							$this->setMessageTypeId( CMessageType::STOP );
							break;

						default:
							// default case
							break;
					}
					break;

				default:
					// default case
					break;
			}
    	}
    }

}
?>