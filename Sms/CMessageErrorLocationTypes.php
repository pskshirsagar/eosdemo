<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageErrorLocationTypes
 * Do not add any new functions to this class.
 */

class CMessageErrorLocationTypes extends CBaseMessageErrorLocationTypes {

	public static function fetchMessageErrorLocationTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMessageErrorLocationType', $objDatabase, DATA_CACHE_MEMORY );
	}

	public static function fetchMessageErrorLocationType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMessageErrorLocationType', $objDatabase, DATA_CACHE_MEMORY );
	}

}
?>