<?php

class CMessageStatusType extends CBaseMessageStatusType {

	const PENDING_SEND					= 1;
	const SENDING 						= 2;
	const SENT 							= 3;
	const PARSING 						= 4;
	const PARSED 						= 5;
	const CONFIRMED 					= 6;
	const RECEIVED 						= 7;
	const FAILED	 					= 8;
	const READ 							= 9;
	const DELETED 						= 10;
}
?>