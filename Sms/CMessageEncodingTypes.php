<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Sms\CMessageEncodingTypes
 * Do not add any new functions to this class.
 */

class CMessageEncodingTypes extends CBaseMessageEncodingTypes {

	public static function fetchMessageEncodingTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CMessageEncodingType', $objDatabase, DATA_CACHE_MEMORY );
	}

	public static function fetchMessageEncodingType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CMessageEncodingType', $objDatabase, DATA_CACHE_MEMORY );
	}

	public static function fetchAllMessageEncodingTypes( $objSmsDatabase ) {
		return self::fetchMessageEncodingTypes( 'SELECT * FROM message_encoding_types ORDER BY order_num', $objSmsDatabase );
	}
}
?>