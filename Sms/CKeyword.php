<?php

class CKeyword extends CBaseKeyword {

	const PROPERTY_KEYWORDS_LIMIT	= 6;
	const LENGTH_LIMIT				= 80;

	const ID_STOPTICKET				= 1084;
	const ID_STOPALLTICKET			= 1085;
	const ID_REOPENTICKET			= 1086;
	const ID_ACKTICKET				= 1431;
	const ID_RESOLVETICKET			= 1432;
	const ID_ESCALATETICKET			= 1433;

	/**
	 * Set Functions
	 *
	 */

	public function setKeyword( $strKeyword ) {
		parent::setKeyword( $strKeyword );

		if( 0 < strlen( $strKeyword ) ) {
			$this->m_strKeyword = \Psi\CStringService::singleton()->strtoupper( $strKeyword );
		}
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valKeyword( $objSmsDatabase ) {
		$boolIsValid = true;

	// Validation Example
	$strKeyword = trim( $this->getKeyword() );
		if( true == empty( $strKeyword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keyword', __( 'Keyword is required. ' ) ) );
		} elseif( self::LENGTH_LIMIT < strlen( $this->getKeyword() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keyword', __( 'Keyword cannot have more than 80 characters. ' ) ) );
		} elseif( false == preg_match( '/^[a-zA-Z0-9]+( [a-zA-Z0-9]+)*$/i', $strKeyword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keyword', __( 'Keyword must be alphanumeric characters. ' ) ) );
		} else {
		// Validate for conflicting keyword in system.
			if( false == isset( $objSmsDatabase ) ) {
				trigger_error( __( 'Application Error: Failed to load SMS database object.' ), E_USER_ERROR );
			}

			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$intCount = \Psi\Eos\Sms\CKeywords::createService()->fetchKeywordCount( ' WHERE  keyword = \'' . addslashes( \Psi\CStringService::singleton()->strtoupper( $this->getKeyword() ) ) . '\'' . $strSqlCondition, $objSmsDatabase );
			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keyword', __( 'Keyword already exists. ' ) ) );
			}
		}

		return $boolIsValid;
	}

	public function valMessageTypeId() {
		$boolIsValid = true;

		if( 0 == strlen( $this->getMessageTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message_type_id', __( 'Message Type is required. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valMatchedMessage() {
		$boolIsValid = true;

		if( 0 == strlen( $this->getMatchedMessage() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', __( 'Matched Message is required. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valUnmatchedMessage() {
		$boolIsValid = true;

		if( 0 == strlen( $this->getUnmatchedMessage() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', __( 'Unmatched Message is required. ' ) ) );
		}

		return $boolIsValid;
	}

	public function valDependantInformation( $objSmsDatabase ) {
		$boolIsValid = true;

		$intBlockCount = \Psi\Eos\Sms\CBlocks::createService()->fetchBlockCount( ' WHERE  keyword_id = ' . $this->getId(), $objSmsDatabase );
			if( 0 < $intBlockCount ) {
				$boolIsValid = false;
			}

		return $boolIsValid;
	}

	public function validate( $strAction, $objSmsDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {

			// For Entrata
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valKeyword( $objSmsDatabase );
				$boolIsValid &= $this->valMessageTypeId();
				$boolIsValid &= $this->valMatchedMessage();
				$boolIsValid &= $this->valUnmatchedMessage();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDependency( $objSmsDatabase );
				break;

			// For Client Admin
			case 'validate_insert':
			case 'validate_update':
				$boolIsValid &= $this->valKeyword( $objSmsDatabase );
				break;

			case 'validate_delete':
				$boolIsValid &= $this->valDependantInformation( $objSmsDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valDependency( $objSmsDatabase ) {

		$boolIsValid = true;
		$arrobjMessages = \Psi\Eos\Sms\CMessages::createService()->fetchParsingMessagesByMessage( $this->getKeyword(), $objSmsDatabase );

		if( 0 < \Psi\Libraries\UtilFunctions\count( $arrobjMessages ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'keyword', __( 'Failed to delete the keyword, it is being associated with message(s). ' ) ) );
		}

		return $boolIsValid;

	}

}
?>