<?php

class CVoicemailMsg extends CBaseVoicemailMsg {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCreatedEpoch() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReadEpoch() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUsername() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDomain() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUuid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCidName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCidNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInFolder() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMessageLen() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFlags() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReadFlags() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valForwardedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>