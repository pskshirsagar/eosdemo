<?php

class CTier extends CBaseTier {

	const ACTIVE_INBOUND	= 'Active Inbound';
	const NO_ANSWER			= 'No Answer';
	const OFFERING			= 'Offering';
	const READY				= 'Ready';
	const STAND_BY			= 'Standy By';
	const UNKNOWN			= 'Uknown';
	const WAITING			= 'Waiting';

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		$this->setAllowDifferentialUpdate( true );
		return;
	}

}

?>