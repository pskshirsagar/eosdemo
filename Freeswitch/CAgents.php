<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Freeswitch\CAgents
 * Do not add any new functions to this class.
 */

class CAgents extends CBaseAgents {

	public static function fetchAgentByCallAgentId( $intCallAgentId, $objFreeswitchDatabase ) {
		return self::fetchAgent( 'SELECT *FROM agents WHERE call_agent_id = ' . ( int ) $intCallAgentId, $objFreeswitchDatabase );
	}

	public static function fetchAllAgents( $objFreeswitchDatabase ) {
		return self::fetchAgents( 'SELECT * FROM agents', $objFreeswitchDatabase );
	}

	public static function fetchAllActiveAgents( $objFreeswitchDatabase ) {
		return self::fetchAgents( 'SELECT *FROM agents WHERE name <> \'' . CCallHelper::SIP_DOMAIN_NAME . '\'', $objFreeswitchDatabase );
	}

	public static function fetchAgentsByCallQueueIdsByAgentStatusesByAgentState( $arrintCallQueueIds, $arrstrAgentStatuses, $strAgentState, $objFreeswitchDatabase ) {

		if( false == valArr( $arrstrAgentStatuses ) || false == valArr( $arrintCallQueueIds ) ) return;

		$strSql = 'SELECT
						agents.*,
						tiers.call_queue_id AS call_queue_id,
						tiers.queue AS call_queue_name
					FROM
						agents
					JOIN tiers ON ( tiers.call_agent_id = agents.call_agent_id )
					WHERE
							tiers.call_queue_id IN ( ' . implode( ',', array_keys( $arrintCallQueueIds ) ) . ' )
						AND agents.status IN ( \'' . implode( '\', \'', $arrstrAgentStatuses ) . '\' )
						AND agents.state = \'' . $strAgentState . '\'';

		return self::fetchAgents( $strSql, $objFreeswitchDatabase );
	}

	public static function updateAgentsWithCallAgents( $arrobjCallAgents, $objFreeswitchDatabase ) {

		if( false == valArr( $arrobjCallAgents ) ) return false;

		foreach( $arrobjCallAgents as $objCallAgent ) {

			$strUpdateSql = 'UPDATE
										agents
									SET
										state = \'' . CCallAgentStateType::$c_arrstrCallAgentStateTypeIdToStr[$objCallAgent->getCallAgentStateTypeId()] . '\',
										status = \'' . CCallAgentStatusType::$c_arrstrCallAgentStatusTypeIdToStr[$objCallAgent->getCallAgentStatusTypeId()] . '\',
										contact = \'' . $objCallAgent->getBuildCallAgentContact() . '\'
									WHERE
										call_agent_id = ' . ( int ) $objCallAgent->getId();

			fetchData( $strUpdateSql, $objFreeswitchDatabase );
		}

		return true;
	}

	public static function fetchConflictAgentByCallAgentIdByName( $intCallAgentId, $strName, $objFreeswitchDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						agents
					WHERE
						call_agent_id = ' . ( int ) $intCallAgentId . '
					AND name = \'' . $strName . '\'
					AND contact IS NOT NULL
					AND state IN ( \'' . CAgent::STATUS_AVAILABLE . '\', \'' . CAgent::STATUS_AVAILABLE_ON_DEMAND . '\' )
					LIMIT 1';

		return self::fetchAgents( $strSql, $objFreeswitchDatabase );
	}

	public static function fetchAgentsByCallAgentIds( $arrintCallAgentIds, $objFreeSwitchDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) ) return NULL;

		$strSql = 'SELECT * from agents WHERE call_agent_id IN ( ' . implode( ',', $arrintCallAgentIds ) . ' )';

		return self::fetchAgents( $strSql, $objFreeSwitchDatabase );
	}

}
?>