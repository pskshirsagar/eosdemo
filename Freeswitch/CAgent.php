<?php

class CAgent extends CBaseAgent {

	protected $m_intCallQueueId;
	protected $m_strCallQueueName;
	protected $m_strSipDomainName;

	public static $c_arrstrActiveState = [
		CAgent::STATE_WAITING,
		CAgent::STATE_RECEIVING,
		CAgent::STATE_IN_A_QUEUE_CALL,
		CAgent::STATE_IN_A_OUTBOUND_CALL,
		CAgent::STATE_DIALING
	];

	public static $c_arrstrActiveStatus = [
		CAgent::STATUS_AVAILABLE,
		CAgent::STATUS_AVAILABLE_ON_DEMAND,
		CAgent::STATUS_ON_BREAK
	];

	/**
	 * Agents State and Status
	 */

	const STATE_WAITING					= 'Waiting';
	const STATE_IDLE					= 'Idle';
	const STATE_RECEIVING				= 'Receiving';
	const STATE_IN_A_QUEUE_CALL			= 'In a queue call';
	const STATE_REMOVED					= 'Removed';
	const STATE_IN_A_OUTBOUND_CALL      = 'In a Outbound Call';
	const STATE_DIALING                 = 'Dialing';

	const STATUS_LOGGED_OUT				= 'Logged Out';
	const STATUS_AVAILABLE				= 'Available';
	const STATUS_AVAILABLE_ON_DEMAND	= 'Available (On Demand)';
	const STATUS_ON_BREAK				= 'On Break';
	const STATUS_HR_APPROVED_OFF		= 'Hr Approved Off';

	/**
	 * Custom Agents Status
	 */

	const STATUS_ON_MEETING				= 'On Meeting';
	const STATUS_ON_PROJECT				= 'On Project';
	const STATUS_ON_LUNCH				= 'On Lunch';
	const STATUS_ON_TRAINING			= 'On Training';
	const STATUS_ON_COACHING			= 'On Coaching';
	const STATUS_IN_EMAIL_QUEUE			= 'In Email Queue';
	const STATUS_IN_LEASING_CHAT		= 'In Leasing Chat';
	const SINGLE_BOX 					= 'single_box';
	const CALLBACK						= 'callback';
	const MAX_NO_ANSWER						= 10;
	const WRAP_UP_TIME						= 60;
	const REJECT_DELAY_TIME					= 0;
	const BUSY_DELAY_TIME					= 0;

	/**
	 * Construct function
	 */

	public function resetAgentStats() {
		$this->setWrapUpTime( 0 );
		$this->setMaxNoAnswer( self::MAX_NO_ANSWER );
		$this->setLastBridgeStart( 0 );
		$this->setLastBridgeEnd( 0 );
		$this->setLastOfferedCall( 0 );
		$this->setRejectDelayTime( 0 );
		$this->setBusyDelayTime( 0 );
		$this->setNoAnswerDelayTime( 0 );
		$this->setLastBridgeStart( 0 );
		$this->setLastBridgeEnd( 0 );
		$this->setLastOfferedCall( 0 );
		$this->setNoAnswerCount( 0 );
		$this->setCallsAnswered( 0 );
		$this->setTalkTime( 0 );
		$this->setReadyTime( 0 );
		return;
	}

	public function setDefaults() {
		$this->setWrapUpTime( 0 );
		$this->setMaxNoAnswer( 0 );
		$this->setLastBridgeStart( 0 );
		$this->setLastBridgeEnd( 0 );
		$this->setLastOfferedCall( 0 );
		$this->setRejectDelayTime( 0 );
		$this->setBusyDelayTime( 0 );
		$this->setNoAnswerDelayTime( 0 );
		$this->setLastBridgeStart( 0 );
		$this->setLastBridgeEnd( 0 );
		$this->setLastOfferedCall( 0 );
		$this->setNoAnswerCount( 0 );
		$this->setCallsAnswered( 0 );
		$this->setTalkTime( 0 );
		$this->setReadyTime( 0 );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['call_queue_id'] ) )	$this->setCallQueueId( $arrmixValues['call_queue_id'] );
		if( true == isset( $arrmixValues['call_queue_name'] ) )	$this->setCallQueueName( $arrmixValues['call_queue_name'] );

		$this->setAllowDifferentialUpdate( true );
		return;
	}

	/**
	 * Setter functions
	 */

	public function setCallQueueId( $intCallQueueId ) {
		$this->m_intCallQueueId = $intCallQueueId;
	}

	public function setCallQueueName( $strCallQueueName ) {
		$this->m_strCallQueueName = $strCallQueueName;
	}

	/**
	 * Getter functions
	 */

	public function getCallQueueId() {
		return $this->m_intCallQueueId;
	}

	public function getCallQueueName() {
		return $this->m_strCallQueueName;
	}

	public function buildContact( $strAgentExtension ) {
		$this->m_strSipDomainName = CCallHelper::SIP_DOMAIN_NAME;
		return ( true == valStr( $strAgentExtension ) ? ( '[call_timeout=40]user/' . $strAgentExtension . $this->m_strSipDomainName ) : '' );
	}

	/**
	 * Transient functions
	 */

	public function calPhysicalExtension() {
		if( false == valStr( $this->getContact() ) ) return;

	 	preg_match( '/([0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})/', $this->getContact(), $arrstrContact );

		if( false == valArr( $arrstrContact ) || false == isset( $arrstrContact[0] ) ) return;

		$this->m_strPhysicalExtension = \Psi\CStringService::singleton()->substr( $arrstrContact[0], 0, \Psi\CStringService::singleton()->strpos( $arrstrContact[0], '@' ) );

		return $this->m_strPhysicalExtension;
	}

	/**
	 * Inherited functions
	 */

	public function update( $intCurrentUserId, $objFreeswitchDatabase, $boolReturnSqlOnly = false ) {
		if( false == valObj( $objFreeswitchDatabase, 'CDatabase' ) ) {
			trigger_error( 'Application Error: Invalid database object provided.', E_USER_WARNING );
			return false;
		}

		if( false == parent::update( $intCurrentUserId, $objFreeswitchDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}
		return true;

		$arrobjTiers = ( array ) CTiers::fetchTiersByCallAgentId( $this->getCallAgentId(), $objFreeswitchDatabase );

		foreach( $arrobjTiers as $objTier ) {
			$objTier->setAgent( $this->getName() );
			$objTier->setState( CTier::READY );

			if( false == $objTier->update( $intCurrentUserId, $objFreeswitchDatabase, $boolReturnSqlOnly ) ) {
				return false;
			}
		}

		return true;
	}

	public function valContact( $objFreeswitchDatabase ) {
		$boolIsValid = true;

		if( true == valObj( CAgents::fetchConflictAgentByCallAgentIdByName( $this->getCallAgentId(), $this->getName(), $objFreeswitchDatabase ), 'CAgent' ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact', 'Agent already registered with different desk.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objFreeswitchDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'login_call_agent':
				$boolIsValid &= $this->valContact( $objFreeswitchDatabase );
				break;

			case 'call_agent_logout':
				break;

			default:
				// default case.
				break;
		}

		return $boolIsValid;
	}

	public function loggedOut() {
		$this->setState( CAgent::STATE_IDLE );
		$this->setStatus( CAgent::STATUS_LOGGED_OUT );
		$this->setContact( NULL );
		$this->setWrapUpTime( 0 );
		$this->setMaxNoAnswer( 0 );
		$this->setLastBridgeStart( 0 );
		$this->setLastBridgeEnd( 0 );
		$this->setLastOfferedCall( 0 );
		$this->setRejectDelayTime( 0 );
		$this->setBusyDelayTime( 0 );
		$this->setNoAnswerDelayTime( 0 );
		$this->setLastBridgeStart( 0 );
		$this->setLastBridgeEnd( 0 );
		$this->setLastOfferedCall( 0 );
		$this->setNoAnswerCount( 0 );
		$this->setCallsAnswered( 0 );
		$this->setTalkTime( 0 );
		$this->setReadyTime( 0 );
		$this->setLastStatusChange( 0 );
		return;
	}

	public function availableOnDemand( $objFreeswitchDatabase ) {
			$this->setState( CAgent::STATE_WAITING );
			$this->setStatus( CAgent::STATUS_AVAILABLE_ON_DEMAND );
			$this->setWrapUpTime( 0 );

		if( false == $this->update( CUser::ID_SYSTEM, $objFreeswitchDatabase ) ) {
			return false;
		}

		return true;
	}

	public function onBreak( $objFreeswitchDatabase ) {
		$this->setStatus( self::STATUS_ON_BREAK );
		$this->setState( self::STATE_IDLE );
		$this->setWrapUpTime( 0 );

		if( false == $this->update( CUser::ID_SYSTEM, $objFreeswitchDatabase ) ) {
			return false;
		}

		return true;
	}

	public function available( $objFreeswitchDatabase ) {
		$this->setStatus( self::STATUS_AVAILABLE );
		$this->setState( self::STATE_WAITING );
		$this->setWrapUpTime( 30 );

		if( false == $this->update( CUser::ID_SYSTEM, $objFreeswitchDatabase ) ) {
			return false;
		}

		return true;
	}

}
?>