<?php

class CVoicemailPref extends CBaseVoicemailPref {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUsername() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDomain() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNamePath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGreetingPath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPassword() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>