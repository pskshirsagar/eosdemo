<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Freeswitch\CTiers
 * Do not add any new functions to this class.
 */

class CTiers extends CBaseTiers {

	public static function fetchTiersByCallQueueIdsByCallAgentIds( $arrintCallQueueIds, $arrintCallAgentIds, $objFreeswitchDatabase ) {
		if( false == valArr( $arrintCallQueueIds ) || false == valArr( $arrintCallAgentIds ) )  return NULL;
		$strSql = 'SELECT * FROM tiers WHERE call_queue_id IN ( ' . implode( ',', $arrintCallQueueIds ) . ' ) AND call_agent_id IN ( ' . implode( ',', $arrintCallAgentIds ) . ' )';
		return self::fetchTiers( $strSql, $objFreeswitchDatabase );
	}

	public static function fetchTierByCallQueueIdByCallAgentId( $intCallQueueId, $intCallAgentId, $objFreeswitchDatabase ) {
		return self::fetchTier( 'SELECT * FROM tiers WHERE call_queue_id = ' . ( int ) $intCallQueueId . ' AND call_agent_id = ' . ( int ) $intCallAgentId, $objFreeswitchDatabase );
	}

	public static function fetchTiersByCallQueueIdByCallAgentId( $intCallQueueId, $intCallAgentId, $objFreeswitchDatabase ) {
		return self::fetchTiers( 'SELECT * FROM tiers WHERE call_queue_id = ' . ( int ) $intCallQueueId . ' AND call_agent_id = ' . ( int ) $intCallAgentId, $objFreeswitchDatabase );
	}

	public static function fetchTierByCallAgentIdByCallQueueId( $intCallAgentId, $intCallQueueId, $objFreeswitchDatabase ) {
		return self::fetchTier( 'SELECT * FROM tiers WHERE call_agent_id = ' . ( int ) $intCallAgentId . ' AND call_queue_id = ' . ( int ) $intCallQueueId . ' LIMIT 1', $objFreeswitchDatabase );
	}

	public static function fetchAllTiers( $objFreeswitchDatabase ) {
		return self::fetchTiers( 'SELECT * FROM tiers', $objFreeswitchDatabase );
	}

	public static function fetchTiersByCallAgentIds( $arrintCallAgentIds, $objFreeSwitchDatabase ) {
		if( false == valArr( $arrintCallAgentIds ) ) return NULL;

		$strSql = 'SELECT * from tiers WHERE call_agent_id IN ( ' . implode( ',', $arrintCallAgentIds ) . ' )';

		return self::fetchTiers( $strSql, $objFreeSwitchDatabase );
	}

}
?>