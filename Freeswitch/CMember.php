<?php

class CMember extends CBaseMember {

	const ANSWERED		= 'Answered';
	const TRYING		= 'Trying';

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		$this->setAllowDifferentialUpdate( true );
		return;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

}

?>