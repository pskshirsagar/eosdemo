<?php

class CBaseMember extends CEosSingularBase {

	const TABLE_NAME = 'public.members';

	protected $m_intId;
	protected $m_strQueue;
	protected $m_strSystem;
	protected $m_strUuid;
	protected $m_strSessionUuid;
	protected $m_strCidNumber;
	protected $m_strCidName;
	protected $m_intSystemEpoch;
	protected $m_intJoinedEpoch;
	protected $m_intRejoinedEpoch;
	protected $m_intBridgeEpoch;
	protected $m_intAbandonedEpoch;
	protected $m_intBaseScore;
	protected $m_intSkillScore;
	protected $m_strServingAgent;
	protected $m_strServingSystem;
	protected $m_strState;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strInstanceId;

	public function __construct() {
		parent::__construct();

		$this->m_strUuid = '';
		$this->m_strSessionUuid = '';
		$this->m_intSystemEpoch = '0';
		$this->m_intJoinedEpoch = '0';
		$this->m_intRejoinedEpoch = '0';
		$this->m_intBridgeEpoch = '0';
		$this->m_intAbandonedEpoch = '0';
		$this->m_intBaseScore = '0';
		$this->m_intSkillScore = '0';
		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['queue'] ) && $boolDirectSet ) $this->set( 'm_strQueue', trim( $arrValues['queue'] ) ); elseif( isset( $arrValues['queue'] ) ) $this->setQueue( $arrValues['queue'] );
		if( isset( $arrValues['system'] ) && $boolDirectSet ) $this->set( 'm_strSystem', trim( $arrValues['system'] ) ); elseif( isset( $arrValues['system'] ) ) $this->setSystem( $arrValues['system'] );
		if( isset( $arrValues['uuid'] ) && $boolDirectSet ) $this->set( 'm_strUuid', trim( $arrValues['uuid'] ) ); elseif( isset( $arrValues['uuid'] ) ) $this->setUuid( $arrValues['uuid'] );
		if( isset( $arrValues['session_uuid'] ) && $boolDirectSet ) $this->set( 'm_strSessionUuid', trim( $arrValues['session_uuid'] ) ); elseif( isset( $arrValues['session_uuid'] ) ) $this->setSessionUuid( $arrValues['session_uuid'] );
		if( isset( $arrValues['cid_number'] ) && $boolDirectSet ) $this->set( 'm_strCidNumber', trim( $arrValues['cid_number'] ) ); elseif( isset( $arrValues['cid_number'] ) ) $this->setCidNumber( $arrValues['cid_number'] );
		if( isset( $arrValues['cid_name'] ) && $boolDirectSet ) $this->set( 'm_strCidName', trim( $arrValues['cid_name'] ) ); elseif( isset( $arrValues['cid_name'] ) ) $this->setCidName( $arrValues['cid_name'] );
		if( isset( $arrValues['system_epoch'] ) && $boolDirectSet ) $this->set( 'm_intSystemEpoch', trim( $arrValues['system_epoch'] ) ); elseif( isset( $arrValues['system_epoch'] ) ) $this->setSystemEpoch( $arrValues['system_epoch'] );
		if( isset( $arrValues['joined_epoch'] ) && $boolDirectSet ) $this->set( 'm_intJoinedEpoch', trim( $arrValues['joined_epoch'] ) ); elseif( isset( $arrValues['joined_epoch'] ) ) $this->setJoinedEpoch( $arrValues['joined_epoch'] );
		if( isset( $arrValues['rejoined_epoch'] ) && $boolDirectSet ) $this->set( 'm_intRejoinedEpoch', trim( $arrValues['rejoined_epoch'] ) ); elseif( isset( $arrValues['rejoined_epoch'] ) ) $this->setRejoinedEpoch( $arrValues['rejoined_epoch'] );
		if( isset( $arrValues['bridge_epoch'] ) && $boolDirectSet ) $this->set( 'm_intBridgeEpoch', trim( $arrValues['bridge_epoch'] ) ); elseif( isset( $arrValues['bridge_epoch'] ) ) $this->setBridgeEpoch( $arrValues['bridge_epoch'] );
		if( isset( $arrValues['abandoned_epoch'] ) && $boolDirectSet ) $this->set( 'm_intAbandonedEpoch', trim( $arrValues['abandoned_epoch'] ) ); elseif( isset( $arrValues['abandoned_epoch'] ) ) $this->setAbandonedEpoch( $arrValues['abandoned_epoch'] );
		if( isset( $arrValues['base_score'] ) && $boolDirectSet ) $this->set( 'm_intBaseScore', trim( $arrValues['base_score'] ) ); elseif( isset( $arrValues['base_score'] ) ) $this->setBaseScore( $arrValues['base_score'] );
		if( isset( $arrValues['skill_score'] ) && $boolDirectSet ) $this->set( 'm_intSkillScore', trim( $arrValues['skill_score'] ) ); elseif( isset( $arrValues['skill_score'] ) ) $this->setSkillScore( $arrValues['skill_score'] );
		if( isset( $arrValues['serving_agent'] ) && $boolDirectSet ) $this->set( 'm_strServingAgent', trim( $arrValues['serving_agent'] ) ); elseif( isset( $arrValues['serving_agent'] ) ) $this->setServingAgent( $arrValues['serving_agent'] );
		if( isset( $arrValues['serving_system'] ) && $boolDirectSet ) $this->set( 'm_strServingSystem', trim( $arrValues['serving_system'] ) ); elseif( isset( $arrValues['serving_system'] ) ) $this->setServingSystem( $arrValues['serving_system'] );
		if( isset( $arrValues['state'] ) && $boolDirectSet ) $this->set( 'm_strState', trim( $arrValues['state'] ) ); elseif( isset( $arrValues['state'] ) ) $this->setState( $arrValues['state'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['instance_id'] ) && $boolDirectSet ) $this->set( 'm_strInstanceId', trim( $arrValues['instance_id'] ) ); elseif( isset( $arrValues['instance_id'] ) ) $this->setInstanceId( $arrValues['instance_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setQueue( $strQueue ) {
		$this->set( 'm_strQueue', CStrings::strTrimDef( $strQueue, 255, NULL, true ) );
	}

	public function getQueue() {
		return $this->m_strQueue;
	}

	public function sqlQueue() {
		return ( true == isset( $this->m_strQueue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strQueue ) : '\'' . addslashes( $this->m_strQueue ) . '\'' ) : 'NULL';
	}

	public function setSystem( $strSystem ) {
		$this->set( 'm_strSystem', CStrings::strTrimDef( $strSystem, 255, NULL, true ) );
	}

	public function getSystem() {
		return $this->m_strSystem;
	}

	public function sqlSystem() {
		return ( true == isset( $this->m_strSystem ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSystem ) : '\'' . addslashes( $this->m_strSystem ) . '\'' ) : 'NULL';
	}

	public function setUuid( $strUuid ) {
		$this->set( 'm_strUuid', CStrings::strTrimDef( $strUuid, 255, NULL, true ) );
	}

	public function getUuid() {
		return $this->m_strUuid;
	}

	public function sqlUuid() {
		return ( true == isset( $this->m_strUuid ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUuid ) : '\'' . addslashes( $this->m_strUuid ) . '\'' ) : '\'\'';
	}

	public function setSessionUuid( $strSessionUuid ) {
		$this->set( 'm_strSessionUuid', CStrings::strTrimDef( $strSessionUuid, 255, NULL, true ) );
	}

	public function getSessionUuid() {
		return $this->m_strSessionUuid;
	}

	public function sqlSessionUuid() {
		return ( true == isset( $this->m_strSessionUuid ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSessionUuid ) : '\'' . addslashes( $this->m_strSessionUuid ) . '\'' ) : '\'\'';
	}

	public function setCidNumber( $strCidNumber ) {
		$this->set( 'm_strCidNumber', CStrings::strTrimDef( $strCidNumber, 255, NULL, true ) );
	}

	public function getCidNumber() {
		return $this->m_strCidNumber;
	}

	public function sqlCidNumber() {
		return ( true == isset( $this->m_strCidNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCidNumber ) : '\'' . addslashes( $this->m_strCidNumber ) . '\'' ) : 'NULL';
	}

	public function setCidName( $strCidName ) {
		$this->set( 'm_strCidName', CStrings::strTrimDef( $strCidName, 255, NULL, true ) );
	}

	public function getCidName() {
		return $this->m_strCidName;
	}

	public function sqlCidName() {
		return ( true == isset( $this->m_strCidName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCidName ) : '\'' . addslashes( $this->m_strCidName ) . '\'' ) : 'NULL';
	}

	public function setSystemEpoch( $intSystemEpoch ) {
		$this->set( 'm_intSystemEpoch', CStrings::strToIntDef( $intSystemEpoch, NULL, false ) );
	}

	public function getSystemEpoch() {
		return $this->m_intSystemEpoch;
	}

	public function sqlSystemEpoch() {
		return ( true == isset( $this->m_intSystemEpoch ) ) ? ( string ) $this->m_intSystemEpoch : '0';
	}

	public function setJoinedEpoch( $intJoinedEpoch ) {
		$this->set( 'm_intJoinedEpoch', CStrings::strToIntDef( $intJoinedEpoch, NULL, false ) );
	}

	public function getJoinedEpoch() {
		return $this->m_intJoinedEpoch;
	}

	public function sqlJoinedEpoch() {
		return ( true == isset( $this->m_intJoinedEpoch ) ) ? ( string ) $this->m_intJoinedEpoch : '0';
	}

	public function setRejoinedEpoch( $intRejoinedEpoch ) {
		$this->set( 'm_intRejoinedEpoch', CStrings::strToIntDef( $intRejoinedEpoch, NULL, false ) );
	}

	public function getRejoinedEpoch() {
		return $this->m_intRejoinedEpoch;
	}

	public function sqlRejoinedEpoch() {
		return ( true == isset( $this->m_intRejoinedEpoch ) ) ? ( string ) $this->m_intRejoinedEpoch : '0';
	}

	public function setBridgeEpoch( $intBridgeEpoch ) {
		$this->set( 'm_intBridgeEpoch', CStrings::strToIntDef( $intBridgeEpoch, NULL, false ) );
	}

	public function getBridgeEpoch() {
		return $this->m_intBridgeEpoch;
	}

	public function sqlBridgeEpoch() {
		return ( true == isset( $this->m_intBridgeEpoch ) ) ? ( string ) $this->m_intBridgeEpoch : '0';
	}

	public function setAbandonedEpoch( $intAbandonedEpoch ) {
		$this->set( 'm_intAbandonedEpoch', CStrings::strToIntDef( $intAbandonedEpoch, NULL, false ) );
	}

	public function getAbandonedEpoch() {
		return $this->m_intAbandonedEpoch;
	}

	public function sqlAbandonedEpoch() {
		return ( true == isset( $this->m_intAbandonedEpoch ) ) ? ( string ) $this->m_intAbandonedEpoch : '0';
	}

	public function setBaseScore( $intBaseScore ) {
		$this->set( 'm_intBaseScore', CStrings::strToIntDef( $intBaseScore, NULL, false ) );
	}

	public function getBaseScore() {
		return $this->m_intBaseScore;
	}

	public function sqlBaseScore() {
		return ( true == isset( $this->m_intBaseScore ) ) ? ( string ) $this->m_intBaseScore : '0';
	}

	public function setSkillScore( $intSkillScore ) {
		$this->set( 'm_intSkillScore', CStrings::strToIntDef( $intSkillScore, NULL, false ) );
	}

	public function getSkillScore() {
		return $this->m_intSkillScore;
	}

	public function sqlSkillScore() {
		return ( true == isset( $this->m_intSkillScore ) ) ? ( string ) $this->m_intSkillScore : '0';
	}

	public function setServingAgent( $strServingAgent ) {
		$this->set( 'm_strServingAgent', CStrings::strTrimDef( $strServingAgent, 255, NULL, true ) );
	}

	public function getServingAgent() {
		return $this->m_strServingAgent;
	}

	public function sqlServingAgent() {
		return ( true == isset( $this->m_strServingAgent ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServingAgent ) : '\'' . addslashes( $this->m_strServingAgent ) . '\'' ) : 'NULL';
	}

	public function setServingSystem( $strServingSystem ) {
		$this->set( 'm_strServingSystem', CStrings::strTrimDef( $strServingSystem, 255, NULL, true ) );
	}

	public function getServingSystem() {
		return $this->m_strServingSystem;
	}

	public function sqlServingSystem() {
		return ( true == isset( $this->m_strServingSystem ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServingSystem ) : '\'' . addslashes( $this->m_strServingSystem ) . '\'' ) : 'NULL';
	}

	public function setState( $strState ) {
		$this->set( 'm_strState', CStrings::strTrimDef( $strState, 255, NULL, true ) );
	}

	public function getState() {
		return $this->m_strState;
	}

	public function sqlState() {
		return ( true == isset( $this->m_strState ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strState ) : '\'' . addslashes( $this->m_strState ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setInstanceId( $strInstanceId ) {
		$this->set( 'm_strInstanceId', CStrings::strTrimDef( $strInstanceId, 255, NULL, true ) );
	}

	public function getInstanceId() {
		return $this->m_strInstanceId;
	}

	public function sqlInstanceId() {
		return ( true == isset( $this->m_strInstanceId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInstanceId ) : '\'' . addslashes( $this->m_strInstanceId ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, queue, system, uuid, session_uuid, cid_number, cid_name, system_epoch, joined_epoch, rejoined_epoch, bridge_epoch, abandoned_epoch, base_score, skill_score, serving_agent, serving_system, state, updated_by, updated_on, created_by, created_on, instance_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlQueue() . ', ' .
						$this->sqlSystem() . ', ' .
						$this->sqlUuid() . ', ' .
						$this->sqlSessionUuid() . ', ' .
						$this->sqlCidNumber() . ', ' .
						$this->sqlCidName() . ', ' .
						$this->sqlSystemEpoch() . ', ' .
						$this->sqlJoinedEpoch() . ', ' .
						$this->sqlRejoinedEpoch() . ', ' .
						$this->sqlBridgeEpoch() . ', ' .
						$this->sqlAbandonedEpoch() . ', ' .
						$this->sqlBaseScore() . ', ' .
						$this->sqlSkillScore() . ', ' .
						$this->sqlServingAgent() . ', ' .
						$this->sqlServingSystem() . ', ' .
						$this->sqlState() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlInstanceId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queue = ' . $this->sqlQueue(). ',' ; } elseif( true == array_key_exists( 'Queue', $this->getChangedColumns() ) ) { $strSql .= ' queue = ' . $this->sqlQueue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system = ' . $this->sqlSystem(). ',' ; } elseif( true == array_key_exists( 'System', $this->getChangedColumns() ) ) { $strSql .= ' system = ' . $this->sqlSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uuid = ' . $this->sqlUuid(). ',' ; } elseif( true == array_key_exists( 'Uuid', $this->getChangedColumns() ) ) { $strSql .= ' uuid = ' . $this->sqlUuid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' session_uuid = ' . $this->sqlSessionUuid(). ',' ; } elseif( true == array_key_exists( 'SessionUuid', $this->getChangedColumns() ) ) { $strSql .= ' session_uuid = ' . $this->sqlSessionUuid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid_number = ' . $this->sqlCidNumber(). ',' ; } elseif( true == array_key_exists( 'CidNumber', $this->getChangedColumns() ) ) { $strSql .= ' cid_number = ' . $this->sqlCidNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid_name = ' . $this->sqlCidName(). ',' ; } elseif( true == array_key_exists( 'CidName', $this->getChangedColumns() ) ) { $strSql .= ' cid_name = ' . $this->sqlCidName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_epoch = ' . $this->sqlSystemEpoch(). ',' ; } elseif( true == array_key_exists( 'SystemEpoch', $this->getChangedColumns() ) ) { $strSql .= ' system_epoch = ' . $this->sqlSystemEpoch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' joined_epoch = ' . $this->sqlJoinedEpoch(). ',' ; } elseif( true == array_key_exists( 'JoinedEpoch', $this->getChangedColumns() ) ) { $strSql .= ' joined_epoch = ' . $this->sqlJoinedEpoch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejoined_epoch = ' . $this->sqlRejoinedEpoch(). ',' ; } elseif( true == array_key_exists( 'RejoinedEpoch', $this->getChangedColumns() ) ) { $strSql .= ' rejoined_epoch = ' . $this->sqlRejoinedEpoch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bridge_epoch = ' . $this->sqlBridgeEpoch(). ',' ; } elseif( true == array_key_exists( 'BridgeEpoch', $this->getChangedColumns() ) ) { $strSql .= ' bridge_epoch = ' . $this->sqlBridgeEpoch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' abandoned_epoch = ' . $this->sqlAbandonedEpoch(). ',' ; } elseif( true == array_key_exists( 'AbandonedEpoch', $this->getChangedColumns() ) ) { $strSql .= ' abandoned_epoch = ' . $this->sqlAbandonedEpoch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_score = ' . $this->sqlBaseScore(). ',' ; } elseif( true == array_key_exists( 'BaseScore', $this->getChangedColumns() ) ) { $strSql .= ' base_score = ' . $this->sqlBaseScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' skill_score = ' . $this->sqlSkillScore(). ',' ; } elseif( true == array_key_exists( 'SkillScore', $this->getChangedColumns() ) ) { $strSql .= ' skill_score = ' . $this->sqlSkillScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' serving_agent = ' . $this->sqlServingAgent(). ',' ; } elseif( true == array_key_exists( 'ServingAgent', $this->getChangedColumns() ) ) { $strSql .= ' serving_agent = ' . $this->sqlServingAgent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' serving_system = ' . $this->sqlServingSystem(). ',' ; } elseif( true == array_key_exists( 'ServingSystem', $this->getChangedColumns() ) ) { $strSql .= ' serving_system = ' . $this->sqlServingSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state = ' . $this->sqlState(). ',' ; } elseif( true == array_key_exists( 'State', $this->getChangedColumns() ) ) { $strSql .= ' state = ' . $this->sqlState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instance_id = ' . $this->sqlInstanceId(). ',' ; } elseif( true == array_key_exists( 'InstanceId', $this->getChangedColumns() ) ) { $strSql .= ' instance_id = ' . $this->sqlInstanceId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'queue' => $this->getQueue(),
			'system' => $this->getSystem(),
			'uuid' => $this->getUuid(),
			'session_uuid' => $this->getSessionUuid(),
			'cid_number' => $this->getCidNumber(),
			'cid_name' => $this->getCidName(),
			'system_epoch' => $this->getSystemEpoch(),
			'joined_epoch' => $this->getJoinedEpoch(),
			'rejoined_epoch' => $this->getRejoinedEpoch(),
			'bridge_epoch' => $this->getBridgeEpoch(),
			'abandoned_epoch' => $this->getAbandonedEpoch(),
			'base_score' => $this->getBaseScore(),
			'skill_score' => $this->getSkillScore(),
			'serving_agent' => $this->getServingAgent(),
			'serving_system' => $this->getServingSystem(),
			'state' => $this->getState(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'instance_id' => $this->getInstanceId()
		);
	}

}
?>