<?php

class CBaseVoicemailMsg extends CEosSingularBase {

	const TABLE_NAME = 'public.voicemail_msgs';

	protected $m_intId;
	protected $m_intCreatedEpoch;
	protected $m_intReadEpoch;
	protected $m_strUsername;
	protected $m_strDomain;
	protected $m_strUuid;
	protected $m_strCidName;
	protected $m_strCidNumber;
	protected $m_strInFolder;
	protected $m_strFilePath;
	protected $m_intMessageLen;
	protected $m_strFlags;
	protected $m_strReadFlags;
	protected $m_strForwardedBy;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['created_epoch'] ) && $boolDirectSet ) $this->set( 'm_intCreatedEpoch', trim( $arrValues['created_epoch'] ) ); elseif( isset( $arrValues['created_epoch'] ) ) $this->setCreatedEpoch( $arrValues['created_epoch'] );
		if( isset( $arrValues['read_epoch'] ) && $boolDirectSet ) $this->set( 'm_intReadEpoch', trim( $arrValues['read_epoch'] ) ); elseif( isset( $arrValues['read_epoch'] ) ) $this->setReadEpoch( $arrValues['read_epoch'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['domain'] ) && $boolDirectSet ) $this->set( 'm_strDomain', trim( stripcslashes( $arrValues['domain'] ) ) ); elseif( isset( $arrValues['domain'] ) ) $this->setDomain( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['domain'] ) : $arrValues['domain'] );
		if( isset( $arrValues['uuid'] ) && $boolDirectSet ) $this->set( 'm_strUuid', trim( stripcslashes( $arrValues['uuid'] ) ) ); elseif( isset( $arrValues['uuid'] ) ) $this->setUuid( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['uuid'] ) : $arrValues['uuid'] );
		if( isset( $arrValues['cid_name'] ) && $boolDirectSet ) $this->set( 'm_strCidName', trim( stripcslashes( $arrValues['cid_name'] ) ) ); elseif( isset( $arrValues['cid_name'] ) ) $this->setCidName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cid_name'] ) : $arrValues['cid_name'] );
		if( isset( $arrValues['cid_number'] ) && $boolDirectSet ) $this->set( 'm_strCidNumber', trim( stripcslashes( $arrValues['cid_number'] ) ) ); elseif( isset( $arrValues['cid_number'] ) ) $this->setCidNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cid_number'] ) : $arrValues['cid_number'] );
		if( isset( $arrValues['in_folder'] ) && $boolDirectSet ) $this->set( 'm_strInFolder', trim( stripcslashes( $arrValues['in_folder'] ) ) ); elseif( isset( $arrValues['in_folder'] ) ) $this->setInFolder( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['in_folder'] ) : $arrValues['in_folder'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['message_len'] ) && $boolDirectSet ) $this->set( 'm_intMessageLen', trim( $arrValues['message_len'] ) ); elseif( isset( $arrValues['message_len'] ) ) $this->setMessageLen( $arrValues['message_len'] );
		if( isset( $arrValues['flags'] ) && $boolDirectSet ) $this->set( 'm_strFlags', trim( stripcslashes( $arrValues['flags'] ) ) ); elseif( isset( $arrValues['flags'] ) ) $this->setFlags( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['flags'] ) : $arrValues['flags'] );
		if( isset( $arrValues['read_flags'] ) && $boolDirectSet ) $this->set( 'm_strReadFlags', trim( stripcslashes( $arrValues['read_flags'] ) ) ); elseif( isset( $arrValues['read_flags'] ) ) $this->setReadFlags( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['read_flags'] ) : $arrValues['read_flags'] );
		if( isset( $arrValues['forwarded_by'] ) && $boolDirectSet ) $this->set( 'm_strForwardedBy', trim( stripcslashes( $arrValues['forwarded_by'] ) ) ); elseif( isset( $arrValues['forwarded_by'] ) ) $this->setForwardedBy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['forwarded_by'] ) : $arrValues['forwarded_by'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCreatedEpoch( $intCreatedEpoch ) {
		$this->set( 'm_intCreatedEpoch', CStrings::strToIntDef( $intCreatedEpoch, NULL, false ) );
	}

	public function getCreatedEpoch() {
		return $this->m_intCreatedEpoch;
	}

	public function sqlCreatedEpoch() {
		return ( true == isset( $this->m_intCreatedEpoch ) ) ? ( string ) $this->m_intCreatedEpoch : 'NULL';
	}

	public function setReadEpoch( $intReadEpoch ) {
		$this->set( 'm_intReadEpoch', CStrings::strToIntDef( $intReadEpoch, NULL, false ) );
	}

	public function getReadEpoch() {
		return $this->m_intReadEpoch;
	}

	public function sqlReadEpoch() {
		return ( true == isset( $this->m_intReadEpoch ) ) ? ( string ) $this->m_intReadEpoch : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 255, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setDomain( $strDomain ) {
		$this->set( 'm_strDomain', CStrings::strTrimDef( $strDomain, 255, NULL, true ) );
	}

	public function getDomain() {
		return $this->m_strDomain;
	}

	public function sqlDomain() {
		return ( true == isset( $this->m_strDomain ) ) ? '\'' . addslashes( $this->m_strDomain ) . '\'' : 'NULL';
	}

	public function setUuid( $strUuid ) {
		$this->set( 'm_strUuid', CStrings::strTrimDef( $strUuid, 255, NULL, true ) );
	}

	public function getUuid() {
		return $this->m_strUuid;
	}

	public function sqlUuid() {
		return ( true == isset( $this->m_strUuid ) ) ? '\'' . addslashes( $this->m_strUuid ) . '\'' : 'NULL';
	}

	public function setCidName( $strCidName ) {
		$this->set( 'm_strCidName', CStrings::strTrimDef( $strCidName, 255, NULL, true ) );
	}

	public function getCidName() {
		return $this->m_strCidName;
	}

	public function sqlCidName() {
		return ( true == isset( $this->m_strCidName ) ) ? '\'' . addslashes( $this->m_strCidName ) . '\'' : 'NULL';
	}

	public function setCidNumber( $strCidNumber ) {
		$this->set( 'm_strCidNumber', CStrings::strTrimDef( $strCidNumber, 255, NULL, true ) );
	}

	public function getCidNumber() {
		return $this->m_strCidNumber;
	}

	public function sqlCidNumber() {
		return ( true == isset( $this->m_strCidNumber ) ) ? '\'' . addslashes( $this->m_strCidNumber ) . '\'' : 'NULL';
	}

	public function setInFolder( $strInFolder ) {
		$this->set( 'm_strInFolder', CStrings::strTrimDef( $strInFolder, 255, NULL, true ) );
	}

	public function getInFolder() {
		return $this->m_strInFolder;
	}

	public function sqlInFolder() {
		return ( true == isset( $this->m_strInFolder ) ) ? '\'' . addslashes( $this->m_strInFolder ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 255, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setMessageLen( $intMessageLen ) {
		$this->set( 'm_intMessageLen', CStrings::strToIntDef( $intMessageLen, NULL, false ) );
	}

	public function getMessageLen() {
		return $this->m_intMessageLen;
	}

	public function sqlMessageLen() {
		return ( true == isset( $this->m_intMessageLen ) ) ? ( string ) $this->m_intMessageLen : 'NULL';
	}

	public function setFlags( $strFlags ) {
		$this->set( 'm_strFlags', CStrings::strTrimDef( $strFlags, 255, NULL, true ) );
	}

	public function getFlags() {
		return $this->m_strFlags;
	}

	public function sqlFlags() {
		return ( true == isset( $this->m_strFlags ) ) ? '\'' . addslashes( $this->m_strFlags ) . '\'' : 'NULL';
	}

	public function setReadFlags( $strReadFlags ) {
		$this->set( 'm_strReadFlags', CStrings::strTrimDef( $strReadFlags, 255, NULL, true ) );
	}

	public function getReadFlags() {
		return $this->m_strReadFlags;
	}

	public function sqlReadFlags() {
		return ( true == isset( $this->m_strReadFlags ) ) ? '\'' . addslashes( $this->m_strReadFlags ) . '\'' : 'NULL';
	}

	public function setForwardedBy( $strForwardedBy ) {
		$this->set( 'm_strForwardedBy', CStrings::strTrimDef( $strForwardedBy, 255, NULL, true ) );
	}

	public function getForwardedBy() {
		return $this->m_strForwardedBy;
	}

	public function sqlForwardedBy() {
		return ( true == isset( $this->m_strForwardedBy ) ) ? '\'' . addslashes( $this->m_strForwardedBy ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, created_epoch, read_epoch, username, domain, uuid, cid_name, cid_number, in_folder, file_path, message_len, flags, read_flags, forwarded_by, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCreatedEpoch() . ', ' .
 						$this->sqlReadEpoch() . ', ' .
 						$this->sqlUsername() . ', ' .
 						$this->sqlDomain() . ', ' .
 						$this->sqlUuid() . ', ' .
 						$this->sqlCidName() . ', ' .
 						$this->sqlCidNumber() . ', ' .
 						$this->sqlInFolder() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlMessageLen() . ', ' .
 						$this->sqlFlags() . ', ' .
 						$this->sqlReadFlags() . ', ' .
 						$this->sqlForwardedBy() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' created_epoch = ' . $this->sqlCreatedEpoch() . ','; } elseif( true == array_key_exists( 'CreatedEpoch', $this->getChangedColumns() ) ) { $strSql .= ' created_epoch = ' . $this->sqlCreatedEpoch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' read_epoch = ' . $this->sqlReadEpoch() . ','; } elseif( true == array_key_exists( 'ReadEpoch', $this->getChangedColumns() ) ) { $strSql .= ' read_epoch = ' . $this->sqlReadEpoch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' domain = ' . $this->sqlDomain() . ','; } elseif( true == array_key_exists( 'Domain', $this->getChangedColumns() ) ) { $strSql .= ' domain = ' . $this->sqlDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uuid = ' . $this->sqlUuid() . ','; } elseif( true == array_key_exists( 'Uuid', $this->getChangedColumns() ) ) { $strSql .= ' uuid = ' . $this->sqlUuid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid_name = ' . $this->sqlCidName() . ','; } elseif( true == array_key_exists( 'CidName', $this->getChangedColumns() ) ) { $strSql .= ' cid_name = ' . $this->sqlCidName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid_number = ' . $this->sqlCidNumber() . ','; } elseif( true == array_key_exists( 'CidNumber', $this->getChangedColumns() ) ) { $strSql .= ' cid_number = ' . $this->sqlCidNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' in_folder = ' . $this->sqlInFolder() . ','; } elseif( true == array_key_exists( 'InFolder', $this->getChangedColumns() ) ) { $strSql .= ' in_folder = ' . $this->sqlInFolder() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_len = ' . $this->sqlMessageLen() . ','; } elseif( true == array_key_exists( 'MessageLen', $this->getChangedColumns() ) ) { $strSql .= ' message_len = ' . $this->sqlMessageLen() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' flags = ' . $this->sqlFlags() . ','; } elseif( true == array_key_exists( 'Flags', $this->getChangedColumns() ) ) { $strSql .= ' flags = ' . $this->sqlFlags() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' read_flags = ' . $this->sqlReadFlags() . ','; } elseif( true == array_key_exists( 'ReadFlags', $this->getChangedColumns() ) ) { $strSql .= ' read_flags = ' . $this->sqlReadFlags() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' forwarded_by = ' . $this->sqlForwardedBy() . ','; } elseif( true == array_key_exists( 'ForwardedBy', $this->getChangedColumns() ) ) { $strSql .= ' forwarded_by = ' . $this->sqlForwardedBy() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'created_epoch' => $this->getCreatedEpoch(),
			'read_epoch' => $this->getReadEpoch(),
			'username' => $this->getUsername(),
			'domain' => $this->getDomain(),
			'uuid' => $this->getUuid(),
			'cid_name' => $this->getCidName(),
			'cid_number' => $this->getCidNumber(),
			'in_folder' => $this->getInFolder(),
			'file_path' => $this->getFilePath(),
			'message_len' => $this->getMessageLen(),
			'flags' => $this->getFlags(),
			'read_flags' => $this->getReadFlags(),
			'forwarded_by' => $this->getForwardedBy(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>