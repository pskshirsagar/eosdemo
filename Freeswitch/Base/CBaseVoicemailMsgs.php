<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Freeswitch\CVoicemailMsgs
 * Do not add any new functions to this class.
 */

class CBaseVoicemailMsgs extends CEosPluralBase {

	/**
	 * @return CVoicemailMsg[]
	 */
	public static function fetchVoicemailMsgs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CVoicemailMsg::class, $objDatabase );
	}

	/**
	 * @return CVoicemailMsg
	 */
	public static function fetchVoicemailMsg( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CVoicemailMsg::class, $objDatabase );
	}

	public static function fetchVoicemailMsgCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'voicemail_msgs', $objDatabase );
	}

	public static function fetchVoicemailMsgById( $intId, $objDatabase ) {
		return self::fetchVoicemailMsg( sprintf( 'SELECT * FROM voicemail_msgs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>