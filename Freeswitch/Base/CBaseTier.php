<?php

class CBaseTier extends CEosSingularBase {

	const TABLE_NAME = 'public.tiers';

	protected $m_intId;
	protected $m_intCallQueueId;
	protected $m_intCallAgentId;
	protected $m_strQueue;
	protected $m_strAgent;
	protected $m_strState;
	protected $m_intLevel;
	protected $m_intPosition;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intLevel = '1';
		$this->m_intPosition = '1';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_queue_id'] ) && $boolDirectSet ) $this->set( 'm_intCallQueueId', trim( $arrValues['call_queue_id'] ) ); elseif( isset( $arrValues['call_queue_id'] ) ) $this->setCallQueueId( $arrValues['call_queue_id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['queue'] ) && $boolDirectSet ) $this->set( 'm_strQueue', trim( stripcslashes( $arrValues['queue'] ) ) ); elseif( isset( $arrValues['queue'] ) ) $this->setQueue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['queue'] ) : $arrValues['queue'] );
		if( isset( $arrValues['agent'] ) && $boolDirectSet ) $this->set( 'm_strAgent', trim( stripcslashes( $arrValues['agent'] ) ) ); elseif( isset( $arrValues['agent'] ) ) $this->setAgent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['agent'] ) : $arrValues['agent'] );
		if( isset( $arrValues['state'] ) && $boolDirectSet ) $this->set( 'm_strState', trim( stripcslashes( $arrValues['state'] ) ) ); elseif( isset( $arrValues['state'] ) ) $this->setState( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state'] ) : $arrValues['state'] );
		if( isset( $arrValues['level'] ) && $boolDirectSet ) $this->set( 'm_intLevel', trim( $arrValues['level'] ) ); elseif( isset( $arrValues['level'] ) ) $this->setLevel( $arrValues['level'] );
		if( isset( $arrValues['position'] ) && $boolDirectSet ) $this->set( 'm_intPosition', trim( $arrValues['position'] ) ); elseif( isset( $arrValues['position'] ) ) $this->setPosition( $arrValues['position'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallQueueId( $intCallQueueId ) {
		$this->set( 'm_intCallQueueId', CStrings::strToIntDef( $intCallQueueId, NULL, false ) );
	}

	public function getCallQueueId() {
		return $this->m_intCallQueueId;
	}

	public function sqlCallQueueId() {
		return ( true == isset( $this->m_intCallQueueId ) ) ? ( string ) $this->m_intCallQueueId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setQueue( $strQueue ) {
		$this->set( 'm_strQueue', CStrings::strTrimDef( $strQueue, 255, NULL, true ) );
	}

	public function getQueue() {
		return $this->m_strQueue;
	}

	public function sqlQueue() {
		return ( true == isset( $this->m_strQueue ) ) ? '\'' . addslashes( $this->m_strQueue ) . '\'' : 'NULL';
	}

	public function setAgent( $strAgent ) {
		$this->set( 'm_strAgent', CStrings::strTrimDef( $strAgent, 255, NULL, true ) );
	}

	public function getAgent() {
		return $this->m_strAgent;
	}

	public function sqlAgent() {
		return ( true == isset( $this->m_strAgent ) ) ? '\'' . addslashes( $this->m_strAgent ) . '\'' : 'NULL';
	}

	public function setState( $strState ) {
		$this->set( 'm_strState', CStrings::strTrimDef( $strState, 255, NULL, true ) );
	}

	public function getState() {
		return $this->m_strState;
	}

	public function sqlState() {
		return ( true == isset( $this->m_strState ) ) ? '\'' . addslashes( $this->m_strState ) . '\'' : 'NULL';
	}

	public function setLevel( $intLevel ) {
		$this->set( 'm_intLevel', CStrings::strToIntDef( $intLevel, NULL, false ) );
	}

	public function getLevel() {
		return $this->m_intLevel;
	}

	public function sqlLevel() {
		return ( true == isset( $this->m_intLevel ) ) ? ( string ) $this->m_intLevel : '1';
	}

	public function setPosition( $intPosition ) {
		$this->set( 'm_intPosition', CStrings::strToIntDef( $intPosition, NULL, false ) );
	}

	public function getPosition() {
		return $this->m_intPosition;
	}

	public function sqlPosition() {
		return ( true == isset( $this->m_intPosition ) ) ? ( string ) $this->m_intPosition : '1';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_queue_id, call_agent_id, queue, agent, state, level, position, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCallQueueId() . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlQueue() . ', ' .
 						$this->sqlAgent() . ', ' .
 						$this->sqlState() . ', ' .
 						$this->sqlLevel() . ', ' .
 						$this->sqlPosition() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_queue_id = ' . $this->sqlCallQueueId() . ','; } elseif( true == array_key_exists( 'CallQueueId', $this->getChangedColumns() ) ) { $strSql .= ' call_queue_id = ' . $this->sqlCallQueueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' queue = ' . $this->sqlQueue() . ','; } elseif( true == array_key_exists( 'Queue', $this->getChangedColumns() ) ) { $strSql .= ' queue = ' . $this->sqlQueue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agent = ' . $this->sqlAgent() . ','; } elseif( true == array_key_exists( 'Agent', $this->getChangedColumns() ) ) { $strSql .= ' agent = ' . $this->sqlAgent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state = ' . $this->sqlState() . ','; } elseif( true == array_key_exists( 'State', $this->getChangedColumns() ) ) { $strSql .= ' state = ' . $this->sqlState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' level = ' . $this->sqlLevel() . ','; } elseif( true == array_key_exists( 'Level', $this->getChangedColumns() ) ) { $strSql .= ' level = ' . $this->sqlLevel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' position = ' . $this->sqlPosition() . ','; } elseif( true == array_key_exists( 'Position', $this->getChangedColumns() ) ) { $strSql .= ' position = ' . $this->sqlPosition() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_queue_id' => $this->getCallQueueId(),
			'call_agent_id' => $this->getCallAgentId(),
			'queue' => $this->getQueue(),
			'agent' => $this->getAgent(),
			'state' => $this->getState(),
			'level' => $this->getLevel(),
			'position' => $this->getPosition(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>