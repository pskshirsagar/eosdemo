<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Freeswitch\CVoicemailPrefs
 * Do not add any new functions to this class.
 */

class CBaseVoicemailPrefs extends CEosPluralBase {

	/**
	 * @return CVoicemailPref[]
	 */
	public static function fetchVoicemailPrefs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CVoicemailPref::class, $objDatabase );
	}

	/**
	 * @return CVoicemailPref
	 */
	public static function fetchVoicemailPref( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CVoicemailPref::class, $objDatabase );
	}

	public static function fetchVoicemailPrefCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'voicemail_prefs', $objDatabase );
	}

	public static function fetchVoicemailPrefById( $intId, $objDatabase ) {
		return self::fetchVoicemailPref( sprintf( 'SELECT * FROM voicemail_prefs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>