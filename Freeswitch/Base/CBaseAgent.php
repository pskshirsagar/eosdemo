<?php

class CBaseAgent extends CEosSingularBase {

	const TABLE_NAME = 'public.agents';

	protected $m_intId;
	protected $m_intCallAgentId;
	protected $m_strName;
	protected $m_strSystem;
	protected $m_strUuid;
	protected $m_strType;
	protected $m_strContact;
	protected $m_strStatus;
	protected $m_strState;
	protected $m_intMaxNoAnswer;
	protected $m_intWrapUpTime;
	protected $m_intRejectDelayTime;
	protected $m_intBusyDelayTime;
	protected $m_intNoAnswerDelayTime;
	protected $m_intLastBridgeStart;
	protected $m_intLastBridgeEnd;
	protected $m_intLastOfferedCall;
	protected $m_intLastStatusChange;
	protected $m_intNoAnswerCount;
	protected $m_intCallsAnswered;
	protected $m_intTalkTime;
	protected $m_intReadyTime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strInstanceId;
	protected $m_intExternalCallsCount;

	public function __construct() {
		parent::__construct();

		$this->m_intMaxNoAnswer = '0';
		$this->m_intWrapUpTime = '0';
		$this->m_intRejectDelayTime = '0';
		$this->m_intBusyDelayTime = '0';
		$this->m_intNoAnswerDelayTime = '0';
		$this->m_intLastBridgeStart = '0';
		$this->m_intLastBridgeEnd = '0';
		$this->m_intLastOfferedCall = '0';
		$this->m_intLastStatusChange = '0';
		$this->m_intNoAnswerCount = '0';
		$this->m_intCallsAnswered = '0';
		$this->m_intTalkTime = '0';
		$this->m_intReadyTime = '0';
		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';
		$this->m_strInstanceId = 'single_box';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['system'] ) && $boolDirectSet ) $this->set( 'm_strSystem', trim( $arrValues['system'] ) ); elseif( isset( $arrValues['system'] ) ) $this->setSystem( $arrValues['system'] );
		if( isset( $arrValues['uuid'] ) && $boolDirectSet ) $this->set( 'm_strUuid', trim( $arrValues['uuid'] ) ); elseif( isset( $arrValues['uuid'] ) ) $this->setUuid( $arrValues['uuid'] );
		if( isset( $arrValues['type'] ) && $boolDirectSet ) $this->set( 'm_strType', trim( $arrValues['type'] ) ); elseif( isset( $arrValues['type'] ) ) $this->setType( $arrValues['type'] );
		if( isset( $arrValues['contact'] ) && $boolDirectSet ) $this->set( 'm_strContact', trim( $arrValues['contact'] ) ); elseif( isset( $arrValues['contact'] ) ) $this->setContact( $arrValues['contact'] );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_strStatus', trim( $arrValues['status'] ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( $arrValues['status'] );
		if( isset( $arrValues['state'] ) && $boolDirectSet ) $this->set( 'm_strState', trim( $arrValues['state'] ) ); elseif( isset( $arrValues['state'] ) ) $this->setState( $arrValues['state'] );
		if( isset( $arrValues['max_no_answer'] ) && $boolDirectSet ) $this->set( 'm_intMaxNoAnswer', trim( $arrValues['max_no_answer'] ) ); elseif( isset( $arrValues['max_no_answer'] ) ) $this->setMaxNoAnswer( $arrValues['max_no_answer'] );
		if( isset( $arrValues['wrap_up_time'] ) && $boolDirectSet ) $this->set( 'm_intWrapUpTime', trim( $arrValues['wrap_up_time'] ) ); elseif( isset( $arrValues['wrap_up_time'] ) ) $this->setWrapUpTime( $arrValues['wrap_up_time'] );
		if( isset( $arrValues['reject_delay_time'] ) && $boolDirectSet ) $this->set( 'm_intRejectDelayTime', trim( $arrValues['reject_delay_time'] ) ); elseif( isset( $arrValues['reject_delay_time'] ) ) $this->setRejectDelayTime( $arrValues['reject_delay_time'] );
		if( isset( $arrValues['busy_delay_time'] ) && $boolDirectSet ) $this->set( 'm_intBusyDelayTime', trim( $arrValues['busy_delay_time'] ) ); elseif( isset( $arrValues['busy_delay_time'] ) ) $this->setBusyDelayTime( $arrValues['busy_delay_time'] );
		if( isset( $arrValues['no_answer_delay_time'] ) && $boolDirectSet ) $this->set( 'm_intNoAnswerDelayTime', trim( $arrValues['no_answer_delay_time'] ) ); elseif( isset( $arrValues['no_answer_delay_time'] ) ) $this->setNoAnswerDelayTime( $arrValues['no_answer_delay_time'] );
		if( isset( $arrValues['last_bridge_start'] ) && $boolDirectSet ) $this->set( 'm_intLastBridgeStart', trim( $arrValues['last_bridge_start'] ) ); elseif( isset( $arrValues['last_bridge_start'] ) ) $this->setLastBridgeStart( $arrValues['last_bridge_start'] );
		if( isset( $arrValues['last_bridge_end'] ) && $boolDirectSet ) $this->set( 'm_intLastBridgeEnd', trim( $arrValues['last_bridge_end'] ) ); elseif( isset( $arrValues['last_bridge_end'] ) ) $this->setLastBridgeEnd( $arrValues['last_bridge_end'] );
		if( isset( $arrValues['last_offered_call'] ) && $boolDirectSet ) $this->set( 'm_intLastOfferedCall', trim( $arrValues['last_offered_call'] ) ); elseif( isset( $arrValues['last_offered_call'] ) ) $this->setLastOfferedCall( $arrValues['last_offered_call'] );
		if( isset( $arrValues['last_status_change'] ) && $boolDirectSet ) $this->set( 'm_intLastStatusChange', trim( $arrValues['last_status_change'] ) ); elseif( isset( $arrValues['last_status_change'] ) ) $this->setLastStatusChange( $arrValues['last_status_change'] );
		if( isset( $arrValues['no_answer_count'] ) && $boolDirectSet ) $this->set( 'm_intNoAnswerCount', trim( $arrValues['no_answer_count'] ) ); elseif( isset( $arrValues['no_answer_count'] ) ) $this->setNoAnswerCount( $arrValues['no_answer_count'] );
		if( isset( $arrValues['calls_answered'] ) && $boolDirectSet ) $this->set( 'm_intCallsAnswered', trim( $arrValues['calls_answered'] ) ); elseif( isset( $arrValues['calls_answered'] ) ) $this->setCallsAnswered( $arrValues['calls_answered'] );
		if( isset( $arrValues['talk_time'] ) && $boolDirectSet ) $this->set( 'm_intTalkTime', trim( $arrValues['talk_time'] ) ); elseif( isset( $arrValues['talk_time'] ) ) $this->setTalkTime( $arrValues['talk_time'] );
		if( isset( $arrValues['ready_time'] ) && $boolDirectSet ) $this->set( 'm_intReadyTime', trim( $arrValues['ready_time'] ) ); elseif( isset( $arrValues['ready_time'] ) ) $this->setReadyTime( $arrValues['ready_time'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['instance_id'] ) && $boolDirectSet ) $this->set( 'm_strInstanceId', trim( $arrValues['instance_id'] ) ); elseif( isset( $arrValues['instance_id'] ) ) $this->setInstanceId( $arrValues['instance_id'] );
		if( isset( $arrValues['external_calls_count'] ) && $boolDirectSet ) $this->set( 'm_intExternalCallsCount', trim( $arrValues['external_calls_count'] ) ); elseif( isset( $arrValues['external_calls_count'] ) ) $this->setExternalCallsCount( $arrValues['external_calls_count'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 255, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setSystem( $strSystem ) {
		$this->set( 'm_strSystem', CStrings::strTrimDef( $strSystem, 255, NULL, true ) );
	}

	public function getSystem() {
		return $this->m_strSystem;
	}

	public function sqlSystem() {
		return ( true == isset( $this->m_strSystem ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSystem ) : '\'' . addslashes( $this->m_strSystem ) . '\'' ) : 'NULL';
	}

	public function setUuid( $strUuid ) {
		$this->set( 'm_strUuid', CStrings::strTrimDef( $strUuid, 255, NULL, true ) );
	}

	public function getUuid() {
		return $this->m_strUuid;
	}

	public function sqlUuid() {
		return ( true == isset( $this->m_strUuid ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUuid ) : '\'' . addslashes( $this->m_strUuid ) . '\'' ) : 'NULL';
	}

	public function setType( $strType ) {
		$this->set( 'm_strType', CStrings::strTrimDef( $strType, 255, NULL, true ) );
	}

	public function getType() {
		return $this->m_strType;
	}

	public function sqlType() {
		return ( true == isset( $this->m_strType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strType ) : '\'' . addslashes( $this->m_strType ) . '\'' ) : 'NULL';
	}

	public function setContact( $strContact ) {
		$this->set( 'm_strContact', CStrings::strTrimDef( $strContact, 255, NULL, true ) );
	}

	public function getContact() {
		return $this->m_strContact;
	}

	public function sqlContact() {
		return ( true == isset( $this->m_strContact ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContact ) : '\'' . addslashes( $this->m_strContact ) . '\'' ) : 'NULL';
	}

	public function setStatus( $strStatus ) {
		$this->set( 'm_strStatus', CStrings::strTrimDef( $strStatus, 255, NULL, true ) );
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_strStatus ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStatus ) : '\'' . addslashes( $this->m_strStatus ) . '\'' ) : 'NULL';
	}

	public function setState( $strState ) {
		$this->set( 'm_strState', CStrings::strTrimDef( $strState, 255, NULL, true ) );
	}

	public function getState() {
		return $this->m_strState;
	}

	public function sqlState() {
		return ( true == isset( $this->m_strState ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strState ) : '\'' . addslashes( $this->m_strState ) . '\'' ) : 'NULL';
	}

	public function setMaxNoAnswer( $intMaxNoAnswer ) {
		$this->set( 'm_intMaxNoAnswer', CStrings::strToIntDef( $intMaxNoAnswer, NULL, false ) );
	}

	public function getMaxNoAnswer() {
		return $this->m_intMaxNoAnswer;
	}

	public function sqlMaxNoAnswer() {
		return ( true == isset( $this->m_intMaxNoAnswer ) ) ? ( string ) $this->m_intMaxNoAnswer : '0';
	}

	public function setWrapUpTime( $intWrapUpTime ) {
		$this->set( 'm_intWrapUpTime', CStrings::strToIntDef( $intWrapUpTime, NULL, false ) );
	}

	public function getWrapUpTime() {
		return $this->m_intWrapUpTime;
	}

	public function sqlWrapUpTime() {
		return ( true == isset( $this->m_intWrapUpTime ) ) ? ( string ) $this->m_intWrapUpTime : '0';
	}

	public function setRejectDelayTime( $intRejectDelayTime ) {
		$this->set( 'm_intRejectDelayTime', CStrings::strToIntDef( $intRejectDelayTime, NULL, false ) );
	}

	public function getRejectDelayTime() {
		return $this->m_intRejectDelayTime;
	}

	public function sqlRejectDelayTime() {
		return ( true == isset( $this->m_intRejectDelayTime ) ) ? ( string ) $this->m_intRejectDelayTime : '0';
	}

	public function setBusyDelayTime( $intBusyDelayTime ) {
		$this->set( 'm_intBusyDelayTime', CStrings::strToIntDef( $intBusyDelayTime, NULL, false ) );
	}

	public function getBusyDelayTime() {
		return $this->m_intBusyDelayTime;
	}

	public function sqlBusyDelayTime() {
		return ( true == isset( $this->m_intBusyDelayTime ) ) ? ( string ) $this->m_intBusyDelayTime : '0';
	}

	public function setNoAnswerDelayTime( $intNoAnswerDelayTime ) {
		$this->set( 'm_intNoAnswerDelayTime', CStrings::strToIntDef( $intNoAnswerDelayTime, NULL, false ) );
	}

	public function getNoAnswerDelayTime() {
		return $this->m_intNoAnswerDelayTime;
	}

	public function sqlNoAnswerDelayTime() {
		return ( true == isset( $this->m_intNoAnswerDelayTime ) ) ? ( string ) $this->m_intNoAnswerDelayTime : '0';
	}

	public function setLastBridgeStart( $intLastBridgeStart ) {
		$this->set( 'm_intLastBridgeStart', CStrings::strToIntDef( $intLastBridgeStart, NULL, false ) );
	}

	public function getLastBridgeStart() {
		return $this->m_intLastBridgeStart;
	}

	public function sqlLastBridgeStart() {
		return ( true == isset( $this->m_intLastBridgeStart ) ) ? ( string ) $this->m_intLastBridgeStart : '0';
	}

	public function setLastBridgeEnd( $intLastBridgeEnd ) {
		$this->set( 'm_intLastBridgeEnd', CStrings::strToIntDef( $intLastBridgeEnd, NULL, false ) );
	}

	public function getLastBridgeEnd() {
		return $this->m_intLastBridgeEnd;
	}

	public function sqlLastBridgeEnd() {
		return ( true == isset( $this->m_intLastBridgeEnd ) ) ? ( string ) $this->m_intLastBridgeEnd : '0';
	}

	public function setLastOfferedCall( $intLastOfferedCall ) {
		$this->set( 'm_intLastOfferedCall', CStrings::strToIntDef( $intLastOfferedCall, NULL, false ) );
	}

	public function getLastOfferedCall() {
		return $this->m_intLastOfferedCall;
	}

	public function sqlLastOfferedCall() {
		return ( true == isset( $this->m_intLastOfferedCall ) ) ? ( string ) $this->m_intLastOfferedCall : '0';
	}

	public function setLastStatusChange( $intLastStatusChange ) {
		$this->set( 'm_intLastStatusChange', CStrings::strToIntDef( $intLastStatusChange, NULL, false ) );
	}

	public function getLastStatusChange() {
		return $this->m_intLastStatusChange;
	}

	public function sqlLastStatusChange() {
		return ( true == isset( $this->m_intLastStatusChange ) ) ? ( string ) $this->m_intLastStatusChange : '0';
	}

	public function setNoAnswerCount( $intNoAnswerCount ) {
		$this->set( 'm_intNoAnswerCount', CStrings::strToIntDef( $intNoAnswerCount, NULL, false ) );
	}

	public function getNoAnswerCount() {
		return $this->m_intNoAnswerCount;
	}

	public function sqlNoAnswerCount() {
		return ( true == isset( $this->m_intNoAnswerCount ) ) ? ( string ) $this->m_intNoAnswerCount : '0';
	}

	public function setCallsAnswered( $intCallsAnswered ) {
		$this->set( 'm_intCallsAnswered', CStrings::strToIntDef( $intCallsAnswered, NULL, false ) );
	}

	public function getCallsAnswered() {
		return $this->m_intCallsAnswered;
	}

	public function sqlCallsAnswered() {
		return ( true == isset( $this->m_intCallsAnswered ) ) ? ( string ) $this->m_intCallsAnswered : '0';
	}

	public function setTalkTime( $intTalkTime ) {
		$this->set( 'm_intTalkTime', CStrings::strToIntDef( $intTalkTime, NULL, false ) );
	}

	public function getTalkTime() {
		return $this->m_intTalkTime;
	}

	public function sqlTalkTime() {
		return ( true == isset( $this->m_intTalkTime ) ) ? ( string ) $this->m_intTalkTime : '0';
	}

	public function setReadyTime( $intReadyTime ) {
		$this->set( 'm_intReadyTime', CStrings::strToIntDef( $intReadyTime, NULL, false ) );
	}

	public function getReadyTime() {
		return $this->m_intReadyTime;
	}

	public function sqlReadyTime() {
		return ( true == isset( $this->m_intReadyTime ) ) ? ( string ) $this->m_intReadyTime : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setInstanceId( $strInstanceId ) {
		$this->set( 'm_strInstanceId', CStrings::strTrimDef( $strInstanceId, 255, NULL, true ) );
	}

	public function getInstanceId() {
		return $this->m_strInstanceId;
	}

	public function sqlInstanceId() {
		return ( true == isset( $this->m_strInstanceId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInstanceId ) : '\'' . addslashes( $this->m_strInstanceId ) . '\'' ) : '\'single_box\'';
	}

	public function setExternalCallsCount( $intExternalCallsCount ) {
		$this->set( 'm_intExternalCallsCount', CStrings::strToIntDef( $intExternalCallsCount, NULL, false ) );
	}

	public function getExternalCallsCount() {
		return $this->m_intExternalCallsCount;
	}

	public function sqlExternalCallsCount() {
		return ( true == isset( $this->m_intExternalCallsCount ) ) ? ( string ) $this->m_intExternalCallsCount : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, call_agent_id, name, system, uuid, type, contact, status, state, max_no_answer, wrap_up_time, reject_delay_time, busy_delay_time, no_answer_delay_time, last_bridge_start, last_bridge_end, last_offered_call, last_status_change, no_answer_count, calls_answered, talk_time, ready_time, updated_by, updated_on, created_by, created_on, instance_id, external_calls_count )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCallAgentId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlSystem() . ', ' .
						$this->sqlUuid() . ', ' .
						$this->sqlType() . ', ' .
						$this->sqlContact() . ', ' .
						$this->sqlStatus() . ', ' .
						$this->sqlState() . ', ' .
						$this->sqlMaxNoAnswer() . ', ' .
						$this->sqlWrapUpTime() . ', ' .
						$this->sqlRejectDelayTime() . ', ' .
						$this->sqlBusyDelayTime() . ', ' .
						$this->sqlNoAnswerDelayTime() . ', ' .
						$this->sqlLastBridgeStart() . ', ' .
						$this->sqlLastBridgeEnd() . ', ' .
						$this->sqlLastOfferedCall() . ', ' .
						$this->sqlLastStatusChange() . ', ' .
						$this->sqlNoAnswerCount() . ', ' .
						$this->sqlCallsAnswered() . ', ' .
						$this->sqlTalkTime() . ', ' .
						$this->sqlReadyTime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlInstanceId() . ', ' .
						$this->sqlExternalCallsCount() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId(). ',' ; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system = ' . $this->sqlSystem(). ',' ; } elseif( true == array_key_exists( 'System', $this->getChangedColumns() ) ) { $strSql .= ' system = ' . $this->sqlSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' uuid = ' . $this->sqlUuid(). ',' ; } elseif( true == array_key_exists( 'Uuid', $this->getChangedColumns() ) ) { $strSql .= ' uuid = ' . $this->sqlUuid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' type = ' . $this->sqlType(). ',' ; } elseif( true == array_key_exists( 'Type', $this->getChangedColumns() ) ) { $strSql .= ' type = ' . $this->sqlType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact = ' . $this->sqlContact(). ',' ; } elseif( true == array_key_exists( 'Contact', $this->getChangedColumns() ) ) { $strSql .= ' contact = ' . $this->sqlContact() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus(). ',' ; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state = ' . $this->sqlState(). ',' ; } elseif( true == array_key_exists( 'State', $this->getChangedColumns() ) ) { $strSql .= ' state = ' . $this->sqlState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_no_answer = ' . $this->sqlMaxNoAnswer(). ',' ; } elseif( true == array_key_exists( 'MaxNoAnswer', $this->getChangedColumns() ) ) { $strSql .= ' max_no_answer = ' . $this->sqlMaxNoAnswer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' wrap_up_time = ' . $this->sqlWrapUpTime(). ',' ; } elseif( true == array_key_exists( 'WrapUpTime', $this->getChangedColumns() ) ) { $strSql .= ' wrap_up_time = ' . $this->sqlWrapUpTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reject_delay_time = ' . $this->sqlRejectDelayTime(). ',' ; } elseif( true == array_key_exists( 'RejectDelayTime', $this->getChangedColumns() ) ) { $strSql .= ' reject_delay_time = ' . $this->sqlRejectDelayTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' busy_delay_time = ' . $this->sqlBusyDelayTime(). ',' ; } elseif( true == array_key_exists( 'BusyDelayTime', $this->getChangedColumns() ) ) { $strSql .= ' busy_delay_time = ' . $this->sqlBusyDelayTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' no_answer_delay_time = ' . $this->sqlNoAnswerDelayTime(). ',' ; } elseif( true == array_key_exists( 'NoAnswerDelayTime', $this->getChangedColumns() ) ) { $strSql .= ' no_answer_delay_time = ' . $this->sqlNoAnswerDelayTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_bridge_start = ' . $this->sqlLastBridgeStart(). ',' ; } elseif( true == array_key_exists( 'LastBridgeStart', $this->getChangedColumns() ) ) { $strSql .= ' last_bridge_start = ' . $this->sqlLastBridgeStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_bridge_end = ' . $this->sqlLastBridgeEnd(). ',' ; } elseif( true == array_key_exists( 'LastBridgeEnd', $this->getChangedColumns() ) ) { $strSql .= ' last_bridge_end = ' . $this->sqlLastBridgeEnd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_offered_call = ' . $this->sqlLastOfferedCall(). ',' ; } elseif( true == array_key_exists( 'LastOfferedCall', $this->getChangedColumns() ) ) { $strSql .= ' last_offered_call = ' . $this->sqlLastOfferedCall() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_status_change = ' . $this->sqlLastStatusChange(). ',' ; } elseif( true == array_key_exists( 'LastStatusChange', $this->getChangedColumns() ) ) { $strSql .= ' last_status_change = ' . $this->sqlLastStatusChange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' no_answer_count = ' . $this->sqlNoAnswerCount(). ',' ; } elseif( true == array_key_exists( 'NoAnswerCount', $this->getChangedColumns() ) ) { $strSql .= ' no_answer_count = ' . $this->sqlNoAnswerCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calls_answered = ' . $this->sqlCallsAnswered(). ',' ; } elseif( true == array_key_exists( 'CallsAnswered', $this->getChangedColumns() ) ) { $strSql .= ' calls_answered = ' . $this->sqlCallsAnswered() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' talk_time = ' . $this->sqlTalkTime(). ',' ; } elseif( true == array_key_exists( 'TalkTime', $this->getChangedColumns() ) ) { $strSql .= ' talk_time = ' . $this->sqlTalkTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ready_time = ' . $this->sqlReadyTime(). ',' ; } elseif( true == array_key_exists( 'ReadyTime', $this->getChangedColumns() ) ) { $strSql .= ' ready_time = ' . $this->sqlReadyTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instance_id = ' . $this->sqlInstanceId(). ',' ; } elseif( true == array_key_exists( 'InstanceId', $this->getChangedColumns() ) ) { $strSql .= ' instance_id = ' . $this->sqlInstanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_calls_count = ' . $this->sqlExternalCallsCount(). ',' ; } elseif( true == array_key_exists( 'ExternalCallsCount', $this->getChangedColumns() ) ) { $strSql .= ' external_calls_count = ' . $this->sqlExternalCallsCount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'call_agent_id' => $this->getCallAgentId(),
			'name' => $this->getName(),
			'system' => $this->getSystem(),
			'uuid' => $this->getUuid(),
			'type' => $this->getType(),
			'contact' => $this->getContact(),
			'status' => $this->getStatus(),
			'state' => $this->getState(),
			'max_no_answer' => $this->getMaxNoAnswer(),
			'wrap_up_time' => $this->getWrapUpTime(),
			'reject_delay_time' => $this->getRejectDelayTime(),
			'busy_delay_time' => $this->getBusyDelayTime(),
			'no_answer_delay_time' => $this->getNoAnswerDelayTime(),
			'last_bridge_start' => $this->getLastBridgeStart(),
			'last_bridge_end' => $this->getLastBridgeEnd(),
			'last_offered_call' => $this->getLastOfferedCall(),
			'last_status_change' => $this->getLastStatusChange(),
			'no_answer_count' => $this->getNoAnswerCount(),
			'calls_answered' => $this->getCallsAnswered(),
			'talk_time' => $this->getTalkTime(),
			'ready_time' => $this->getReadyTime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'instance_id' => $this->getInstanceId(),
			'external_calls_count' => $this->getExternalCallsCount()
		);
	}

}
?>