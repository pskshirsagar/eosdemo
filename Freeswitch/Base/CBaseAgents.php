<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Freeswitch\CAgents
 * Do not add any new functions to this class.
 */

class CBaseAgents extends CEosPluralBase {

	/**
	 * @return CAgent[]
	 */
	public static function fetchAgents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CAgent', $objDatabase );
	}

	/**
	 * @return CAgent
	 */
	public static function fetchAgent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAgent', $objDatabase );
	}

	public static function fetchAgentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'agents', $objDatabase );
	}

	public static function fetchAgentById( $intId, $objDatabase ) {
		return self::fetchAgent( sprintf( 'SELECT * FROM agents WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchAgentsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchAgents( sprintf( 'SELECT * FROM agents WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

}
?>