<?php

class CBaseVoicemailPref extends CEosSingularBase {

	const TABLE_NAME = 'public.voicemail_prefs';

	protected $m_intId;
	protected $m_strUsername;
	protected $m_strDomain;
	protected $m_strNamePath;
	protected $m_strGreetingPath;
	protected $m_strPassword;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intUpdatedBy = '1';
		$this->m_strUpdatedOn = 'now()';
		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['domain'] ) && $boolDirectSet ) $this->set( 'm_strDomain', trim( stripcslashes( $arrValues['domain'] ) ) ); elseif( isset( $arrValues['domain'] ) ) $this->setDomain( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['domain'] ) : $arrValues['domain'] );
		if( isset( $arrValues['name_path'] ) && $boolDirectSet ) $this->set( 'm_strNamePath', trim( stripcslashes( $arrValues['name_path'] ) ) ); elseif( isset( $arrValues['name_path'] ) ) $this->setNamePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_path'] ) : $arrValues['name_path'] );
		if( isset( $arrValues['greeting_path'] ) && $boolDirectSet ) $this->set( 'm_strGreetingPath', trim( stripcslashes( $arrValues['greeting_path'] ) ) ); elseif( isset( $arrValues['greeting_path'] ) ) $this->setGreetingPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['greeting_path'] ) : $arrValues['greeting_path'] );
		if( isset( $arrValues['password'] ) && $boolDirectSet ) $this->set( 'm_strPassword', trim( stripcslashes( $arrValues['password'] ) ) ); elseif( isset( $arrValues['password'] ) ) $this->setPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password'] ) : $arrValues['password'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 255, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setDomain( $strDomain ) {
		$this->set( 'm_strDomain', CStrings::strTrimDef( $strDomain, 255, NULL, true ) );
	}

	public function getDomain() {
		return $this->m_strDomain;
	}

	public function sqlDomain() {
		return ( true == isset( $this->m_strDomain ) ) ? '\'' . addslashes( $this->m_strDomain ) . '\'' : 'NULL';
	}

	public function setNamePath( $strNamePath ) {
		$this->set( 'm_strNamePath', CStrings::strTrimDef( $strNamePath, 255, NULL, true ) );
	}

	public function getNamePath() {
		return $this->m_strNamePath;
	}

	public function sqlNamePath() {
		return ( true == isset( $this->m_strNamePath ) ) ? '\'' . addslashes( $this->m_strNamePath ) . '\'' : 'NULL';
	}

	public function setGreetingPath( $strGreetingPath ) {
		$this->set( 'm_strGreetingPath', CStrings::strTrimDef( $strGreetingPath, 255, NULL, true ) );
	}

	public function getGreetingPath() {
		return $this->m_strGreetingPath;
	}

	public function sqlGreetingPath() {
		return ( true == isset( $this->m_strGreetingPath ) ) ? '\'' . addslashes( $this->m_strGreetingPath ) . '\'' : 'NULL';
	}

	public function setPassword( $strPassword ) {
		$this->set( 'm_strPassword', CStrings::strTrimDef( $strPassword, 255, NULL, true ) );
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function sqlPassword() {
		return ( true == isset( $this->m_strPassword ) ) ? '\'' . addslashes( $this->m_strPassword ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, username, domain, name_path, greeting_path, password, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlUsername() . ', ' .
 						$this->sqlDomain() . ', ' .
 						$this->sqlNamePath() . ', ' .
 						$this->sqlGreetingPath() . ', ' .
 						$this->sqlPassword() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' domain = ' . $this->sqlDomain() . ','; } elseif( true == array_key_exists( 'Domain', $this->getChangedColumns() ) ) { $strSql .= ' domain = ' . $this->sqlDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_path = ' . $this->sqlNamePath() . ','; } elseif( true == array_key_exists( 'NamePath', $this->getChangedColumns() ) ) { $strSql .= ' name_path = ' . $this->sqlNamePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' greeting_path = ' . $this->sqlGreetingPath() . ','; } elseif( true == array_key_exists( 'GreetingPath', $this->getChangedColumns() ) ) { $strSql .= ' greeting_path = ' . $this->sqlGreetingPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password = ' . $this->sqlPassword() . ','; } elseif( true == array_key_exists( 'Password', $this->getChangedColumns() ) ) { $strSql .= ' password = ' . $this->sqlPassword() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'username' => $this->getUsername(),
			'domain' => $this->getDomain(),
			'name_path' => $this->getNamePath(),
			'greeting_path' => $this->getGreetingPath(),
			'password' => $this->getPassword(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>