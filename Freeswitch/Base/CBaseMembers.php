<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Freeswitch\CMembers
 * Do not add any new functions to this class.
 */

class CBaseMembers extends CEosPluralBase {

	/**
	 * @return CMember[]
	 */
	public static function fetchMembers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMember', $objDatabase );
	}

	/**
	 * @return CMember
	 */
	public static function fetchMember( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMember', $objDatabase );
	}

	public static function fetchMemberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'members', $objDatabase );
	}

	public static function fetchMemberById( $intId, $objDatabase ) {
		return self::fetchMember( sprintf( 'SELECT * FROM members WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>