<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Freeswitch\CTiers
 * Do not add any new functions to this class.
 */

class CBaseTiers extends CEosPluralBase {

	/**
	 * @return CTier[]
	 */
	public static function fetchTiers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTier', $objDatabase );
	}

	/**
	 * @return CTier
	 */
	public static function fetchTier( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTier', $objDatabase );
	}

	public static function fetchTierCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tiers', $objDatabase );
	}

	public static function fetchTierById( $intId, $objDatabase ) {
		return self::fetchTier( sprintf( 'SELECT * FROM tiers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTiersByCallQueueId( $intCallQueueId, $objDatabase ) {
		return self::fetchTiers( sprintf( 'SELECT * FROM tiers WHERE call_queue_id = %d', ( int ) $intCallQueueId ), $objDatabase );
	}

	public static function fetchTiersByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchTiers( sprintf( 'SELECT * FROM tiers WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

}
?>