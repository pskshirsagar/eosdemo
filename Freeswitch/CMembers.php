<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Freeswitch\CMembers
 * Do not add any new functions to this class.
 */

class CMembers extends CBaseMembers {

	public static function fetchMemberByCallCenterMemberUuid( $strCallCenterMemberUuid, $objFreeswitchDatabase ) {
		$strSql = 'SELECT *FROM members WHERE uuid = \'' . $strCallCenterMemberUuid . '\'';
		return self::fetchMember( $strSql, $objFreeswitchDatabase );
	}

	public static function fetchMemberBySessionUuidByState( $strSessionUuid, $strState, $objFreeswitchDatabase ) {
		$strSql = 'SELECT *FROM members WHERE session_uuid = \'' . $strSessionUuid . '\' AND state = \'' . $strState . '\' LIMIT 1';
		return self::fetchMember( $strSql, $objFreeswitchDatabase );
	}

	public static function fetchMemberByAgentContact( $strAgentContact, $objFreeswitchDatabase ) {
		if( false == valStr( $strAgentContact ) ) return false;

		$strSql = 'SELECT
						m.*
					FROM
						members AS m
					JOIN agents AS a ON ( a.name = m.serving_agent )
					WHERE
						m.state = \'Answered\'
						AND contact ILIKE \'%' . $strAgentContact . '\' LIMIT 1';

		return self::fetchMember( $strSql, $objFreeswitchDatabase );
	}

	public static function fetchMemberBySessionUuid( $strSessionUuid, $objFreeswitchDatabase ) {
		$strSql = 'SELECT *FROM members WHERE session_uuid = \'' . $strSessionUuid . '\'' . ' LIMIT 1';
		return self::fetchMember( $strSql, $objFreeswitchDatabase );
	}
}
?>