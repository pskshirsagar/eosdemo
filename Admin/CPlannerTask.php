<?php

class CPlannerTask extends CBasePlannerTask {

	protected $m_intDevTdStoryPoints;
	protected $m_intQaTdStoryPoints;
	const PLANNED_TASK = 'Planned';
	const UNPLANNED_TASK = 'Unplanned';
	const CARRY_FORWARDED_TASK = 'CarryForwarded';
	public static $c_arrintPlannerTaskStatusType = [
		self::PLANNED_TASK => 'Planned Task',
		self::UNPLANNED_TASK => 'Unplanned Task',
		self::CARRY_FORWARDED_TASK => 'Carry Forward Task',
	];

	public function getQaTdStoryPoints() {
		return $this->m_intQaTdStoryPoints;
	}

	public function getDevTdStoryPoints() {
		return $this->m_intDevTdStoryPoints;
	}

	public function setQaTdStoryPoints( $intDevTdStroyPoints ) {
		$this->m_intQaTdStoryPoints = $intDevTdStroyPoints;
	}

	public function setDevTdStoryPoints( $intQaTdStoryPoints ) {
		$this->m_intDevTdStoryPoints = $intQaTdStoryPoints;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['dev_td_story_points'] ) ) 				$this->setDevTdStoryPoints( $arrmixValues['dev_td_story_points'] );
		if( true == isset( $arrmixValues['qa_td_story_points'] ) ) 						$this->setQaTdStoryPoints( $arrmixValues['qa_td_story_points'] );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPlannerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrentTask() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
