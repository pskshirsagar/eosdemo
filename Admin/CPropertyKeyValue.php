<?php

class CPropertyKeyValue extends CBasePropertyKeyValue {

	/**
	 * Validation Functions
	 */

	public function valValue() {
		$boolIsValid = true;

		if( true == is_null( $this->getValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Property value is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			$boolIsValid &= $this->valValue();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public static function fetchPropertyKeyValueByPropertyIdAndCid( $intPropertyId, $intCid, $objDatabase ) {
		return CPropertyKeyValues::fetchPropertyKeyValue( 'SELECT * FROM property_key_values WHERE property_id = ' . ( int ) $intPropertyId . ' AND  cid = ' . ( int ) $intCid, $objDatabase );
	}
}
?>