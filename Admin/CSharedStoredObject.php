<?php

class CSharedStoredObject extends CBaseSharedStoredObject {
	protected $m_strTempFileName;

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function setFileError( $strFileError ) {
		$this->m_strFileError = $strFileError;
	}

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceTable() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceTag() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVendor() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContainer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContentLength() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEtag() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function uploadObject( $boolEncrypt = true ) {
		if( empty( $this->getId() ) || empty( $this->getKey() ) || empty( $this->getVendor() ) || empty( $this->getContainer() ) ) return false;

		$strFileContent = CFileIo::fileGetContents( $this->getTempFileName() );

		if( empty( $strFileContent ) ) {
			$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'StorageGateway', 'Invalid file content' ) );
			return false;
		}

		$objObjectStorageGateway = $this->getObjectStorageGateway();
		$objObjectStorageGateway->initialize();

		if( false == is_null( $this->getKey() ) && false == is_null( $this->getTitle() ) ) {
			$objObjectStorageGateway = $objObjectStorageGateway->putObject( [
				'storageGateway'  => $this->getVendor(),
				'objectId'        => $this->getId(),
				'container'       => $this->getContainer(),
				'key'             => $this->getKey(),
				'data'            => CFileIo::fileGetContents( $this->getTempFileName() ),
				'noEncrypt'       => !$boolEncrypt
			] );

			if( true == $objObjectStorageGateway->hasErrors() ) {
				$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'StorageGateway', 'Failed to upload file' ) );
				return false;
			}

			return true;
		}

		return false;
	}

	public function downloadObject( $boolReturnPath = false, $boolDisposition = false ) {
		if( empty( $this->getId() ) || empty( $this->getKey() ) || empty( $this->getVendor() ) || empty( $this->getContainer() ) ) return false;

		$objObjectStorageGateway = $this->getObjectStorageGateway();

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObject( [
			'storageGateway'  => $this->getVendor(),
			'objectId'		=> $this->getId(),
			'container'		=> $this->getContainer(),
			'key'			=> $this->getKey(),
			'outputFile'	=> 'temp'
		] );

		$strTempFullPath = $objObjectStorageGatewayResponse['outputFile'];

		if( false == CFileIo::fileExists( $strTempFullPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'File is not present or has been deleted...' ) );
			return false;
		}

		$arrstrFileParts    = pathinfo( $strTempFullPath );
		$strFileName        = $this->getTitle();
		$strMimeType		= '';

		if( false == empty( $arrstrFileParts['extension'] ) ) {
			$strMimeType = CFileExtension::$c_arrmixAllFileExtensions[strtolower( $arrstrFileParts['extension'] )];
		}

		if( true == empty( $strMimeType ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Invalid File extension.' ) );
			return false;
		}

		if( true == $boolReturnPath ) {
			return $strTempFullPath;
		}

		$strDisposition = ( false == $boolDisposition ) ? 'attachment' : 'inline';

		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: public' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-Length:' . CFileIo::getFileSize( $strTempFullPath ) );
		header( 'Content-Type:' . $strMimeType );
		header( 'Content-Disposition: ' . $strDisposition . '; filename= "' . $strFileName . '"' );
		header( 'Content-Transfer-Encoding:binary' );
		header( 'Accept-Ranges: bytes' );

		if( 'text/html' == $strMimeType )
			echo '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';

		echo CFileIo::fileGetContents( $strTempFullPath );
		return true;
	}

	public function deleteObject() {
		if( empty( $this->getId() ) || empty( $this->getKey() ) || empty( $this->getVendor() ) || empty( $this->getContainer() ) ) return false;

		$objObjectStorageGateway = $this->getObjectStorageGateway();

		$objObjectStorageGateway->initialize();

		$objDeleteObjectResponse = $objObjectStorageGateway->deleteObject( [
			'storageGateway'  => $this->getVendor(),
			'objectId'        => $this->getId(),
			'container'       => $this->getContainer(),
			'key'             => $this->getKey()
		] );

		if( true == valObj( $objDeleteObjectResponse, 'CRouterObjectStorageGateway ' ) && true == $objDeleteObjectResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to remove the file.' ) );
			return false;
		}

		return true;
	}

	public function getFileSize( $objObjectStorageGateway ) {
		if( empty( $this->getId() ) || empty( $this->getKey() ) || empty( $this->getVendor() ) || empty( $this->getContainer() ) ) return false;

		if( false == empty( $this->getContentLength() ) ) {
			return $this->getContentLength();
		}

		$objObjectStorageGateway->initialize();

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->getObjectMetadata( [
			'storageGateway'  => $this->getVendor(),
			'objectId'        => $this->getId(),
			'container'       => $this->getContainer(),
			'key'             => $this->getKey()
		] );

		if( true == $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to load file' ) );
			return false;
		}

		return $objObjectStorageGatewayResponse['contentLength'] ?? NULL;
	}

	private function getObjectStorageGateway() {
		$objContainer = \Psi\Libraries\Container\CDependencyContainer::getInstance()->getContainer();
		$objObjectStorageGateway = $objContainer['object_storage'] ?? CObjectStorageGatewayFactory::createObjectStorageGateway( \Psi\Libraries\ObjectStorage\DataTransferObjects\CObjectStorageGatewayType::MULTI_SOURCED );

		return $objObjectStorageGateway;
	}

}
?>