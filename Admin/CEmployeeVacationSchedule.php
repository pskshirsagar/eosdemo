<?php

class CEmployeeVacationSchedule extends CBaseEmployeeVacationSchedule {

	protected  $m_intScheduledDays;
	protected  $m_intScheduledHours;

	const PER_DAY_ON_FLOOR_HOURS = 8;

	/**
	 * Get Functions
	 */

	public function getScheduledDays() {
		return $this->m_intScheduledDays;
	}

	public function getScheduledHours() {
		return $this->m_intScheduledHours;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['scheduled_days'] ) ) $this->setScheduledDays( $arrValues['scheduled_days'] );
		if( true == isset( $arrValues['scheduled_hours'] ) ) $this->setScheduledHours( $arrValues['scheduled_hours'] );

		return;
	}

	public function setScheduledDays( $intScheduledDays ) {
	  $this->m_intScheduledDays = $intScheduledDays;
	}

	public function setScheduledHours( $intScheduledHours ) {
	  $this->m_intScheduledHours = $intScheduledHours;

	}

	public function setDefaults() {

		if( true == is_null( $this->getScheduledDays() ) && true == is_null( $this->getScheduledHours() ) ) {
			$this->setHoursGranted( 0 );
		} else {
			$this->setHoursGranted( ( ( int ) $this->getScheduledDays() * 8 ) + ( int ) $this->getScheduledHours() );
		}
	}

	/**
	 * Validate Functions
	 */

	public function valEmployeeId() {
		$boolIsValid = true;

	   if( true == is_null( $this->getEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Employee Id is required.' ) );
	   }

		return $boolIsValid;
	}

	public function valYear() {
		$boolIsValid = true;

		if( true == is_null( $this->getYear() ) ) {
			$boolIsValid = false;
		   $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year', 'Year is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'insert_or_update':
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valYear();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

}
?>