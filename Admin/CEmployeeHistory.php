<?php

class CEmployeeHistory extends CBaseEmployeeHistory {

	public function createEmployeeHistory( $intEmployeeId, $intProposedDesignationId ) {
		$this->setEmployeeId( $intEmployeeId );
		$this->setDatetime( 'now' );
		$this->setDesignationId( $intProposedDesignationId );
	}

	public function valHistoryData() {
		$boolValid			= true;
		$objJsonHistoryData	= $this->getHistoryData();
		$strBonusAmount		= '';
		if( false == is_null( $objJsonHistoryData->annual_bonus_potential ) ) {
			$strBonusAmount		= ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $objJsonHistoryData->annual_bonus_potential, CONFIG_SODIUM_KEY_PURCHASE_REQUEST_BASE_AMOUNT, [ 'legacy_secret_key' => CONFIG_KEY_PURCHASE_REQUEST_BASE_AMOUNT ] );
		}

		if( false == valObj( $objJsonHistoryData, 'stdClass' ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'history_data', 'Bonus data is not proper.' ) );
		}

		if( true == empty( $strBonusAmount ) || 1 > $strBonusAmount ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'history_data', ( true == empty( $strBonusAmount ) ) ? 'Annual Bonus potential is required.' : 'Please enter valid Annual Bonus Potential.' ) );
		}

		if( true == empty( $objJsonHistoryData->annual_bonus_type_id ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'history_data', 'Bonus type is required.' ) );
		}

		if( true == empty( $objJsonHistoryData->bonus_requirement ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'history_data', 'Bonus criteria is required.' ) );
		}

		return $boolValid;
	}

	public function validate( $strAction ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolValid &= $this->valHistoryData();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolValid = false;
				break;
		}

		return $boolValid;
	}

}
?>