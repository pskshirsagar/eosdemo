<?php

class CChargeCodeType extends CBaseChargeCodeType {

	const PAYMENT 				= 1;
	const BEGINNING_BALANCE 	= 2;
	const DEPOSIT_HELD 			= 3;
	const DEPOSIT 				= 4;
	const REFUND 				= 5;
	const CHARGE 				= 6;
	const ADJUSTMENT 			= 7;
	const CHECK 				= 8;

}
?>