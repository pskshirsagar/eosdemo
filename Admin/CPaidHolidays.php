<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPaidHolidays
 * Do not add any new functions to this class.
 */

class CPaidHolidays extends CBasePaidHolidays {

	public static function fetchPaginatedPaidHolidaysByYearByCountryCode( $intPageNo, $intPageSize, $intYear, $strCountryCode, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL, $intMonth = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;
		$strWhereCondition = ( 0 < $intMonth ) ? ' AND DATE_PART ( \'month\', DATE ( date ) ) = ' . ( int ) $intMonth : '';

		$strSql = 'SELECT * FROM paid_holidays WHERE country_code = \'' . $strCountryCode . '\' AND DATE_PART ( \'year\', DATE ( date ) ) = ' . ( int ) $intYear . $strWhereCondition . ' ORDER BY ' . $strOrderByField . ' ' . $strOrderByType . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit . ' ';

		return parent::fetchPaidHolidays( $strSql, $objDatabase );
	}

	public static function fetchCurrentPaidHolidaysByCurrentDate( $objDatabase ) {

		$strSql = 'SELECT * FROM paid_holidays WHERE is_published = 1 AND to_char( NOW(), \'YYYY/MM/DD\' ) = to_char( paid_holidays.date, \'YYYY/MM/DD\' ) ORDER BY date LIMIT 2';

		return parent::fetchPaidHolidays( $strSql, $objDatabase );
	}

	public static function fetchNextPaidHolidaysByCurrentDate( $objDatabase ) {

		$strSql = 'SELECT * FROM paid_holidays WHERE is_published = 1 AND to_char( NOW(), \'YYYY/MM/DD\' ) < to_char( paid_holidays.date, \'YYYY/MM/DD\' ) ORDER BY date LIMIT 2';

		return parent::fetchPaidHolidays( $strSql, $objDatabase );
	}

	public static function fetchPaginatedPaidHolidaysCountByYearByCountryCode( $intYear, $strCountryCode, $objDatabase, $intMonth = NULL ) {

		$strWhereCondition = ( 0 < $intMonth ) ? ' AND DATE_PART ( \'month\', DATE ( date ) ) = ' . ( int ) $intMonth : '';

		$strSql = 'SELECT count(id) FROM paid_holidays WHERE country_code = \'' . $strCountryCode . '\' AND DATE_PART( \'year\', DATE( date ) ) = ' . ( int ) $intYear . $strWhereCondition;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchPaidHolidaysByIds( $arrintPaidHolidayIds, $objDatabase ) {

		return parent::fetchPaidHolidays( sprintf( 'SELECT * FROM paid_holidays WHERE id IN ( %s )', implode( ',', $arrintPaidHolidayIds ) ), $objDatabase );

	}

	public static function fetchPaidHolidaysCountByDate( $strHolidayDate, $intPaidHolidayId, $strCountryCode,$objDatabase ) {

		if( false == is_null( $intPaidHolidayId ) ) {

			$strSql = "SELECT count(id) FROM paid_holidays WHERE date = '" . $strHolidayDate . "' AND id != " . ( int ) $intPaidHolidayId . ' AND country_code =\'' . addslashes( $strCountryCode ) . "'";

		} else {

			$strSql = "SELECT count(id) FROM paid_holidays WHERE date = '" . $strHolidayDate . "'";
		}
		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchPaidHolidaysCountByDatesByCountryCode( $strBeginDate, $strEndDate, $strCountryCode, $objDatabase ) {
		if( false == valStr( $strBeginDate ) || false == valStr( $strEndDate ) ) return NULL;

		$strSql = 'SELECT count(id) FROM paid_holidays WHERE date BETWEEN \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\' AND country_code =\'' . addslashes( $strCountryCode ) . '\' AND is_published = 1 AND is_optional IS FALSE';
		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchPaidHolidaysByCountryCode( $strCountryCode, $objDatabase ) {
		return self::fetchPaidHolidays( sprintf( 'SELECT * FROM paid_holidays WHERE country_code = \'%s\' AND is_optional IS FALSE', $strCountryCode ), $objDatabase );
	}

	public static function fetchPaidHolidaysByDatesByCountryCode( $strBeginDate, $strEndDate, $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT * FROM paid_holidays WHERE date BETWEEN \'' . $strBeginDate . '\' AND \'' . $strEndDate . '\' AND country_code =\'' . addslashes( $strCountryCode ) . '\'  AND is_optional IS FALSE';

		return parent::fetchPaidHolidays( $strSql, $objDatabase );
	}

	public static function fetchPaidHolidaysDateByCountryCode( $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT
						to_char(date, \'YYYY-MM-DD\') as holiday_date
					FROM
						paid_holidays
					WHERE
						country_code =\'' . addslashes( $strCountryCode ) . '\'
						AND is_optional IS FALSE
					ORDER BY
						holiday_date';

		$arrstrHolidays = fetchData( $strSql, $objDatabase );

		$arrstrHolidayDates = array();

		if( true == valArr( $arrstrHolidays ) ) {
			foreach( $arrstrHolidays as $arrstrHoliday ) {
				$arrstrHolidayDates[] = $arrstrHoliday['holiday_date'];
			}
		}

		return $arrstrHolidayDates;
	}

	public static function fetchPaidHolidaysWithWeekendWorkingDays( $strStartDate, $strEndDate, $objDatabase ) {

		$strStartDate 	= '\'' . $strStartDate . '\'';
		$strEndDate 	= '\'' . $strEndDate . '\'';

		$intDayOfWeekSaturday 	= 6;
		$intDayOfWeekSunday 	= 0;

		$strSql = ' SELECT
						DATE ' . $strStartDate . ' + serial_number AS day
					FROM
						generate_series ( 0, DATE ' . $strEndDate . '  - DATE ' . $strStartDate . '  ) serial_number
					WHERE
						EXTRACT ( \'dow\' FROM DATE ' . $strStartDate . ' + serial_number ) IN ( ' . ( int ) $intDayOfWeekSaturday . ', ' . ( int ) $intDayOfWeekSunday . ' )

					UNION ALL

					SELECT
						DATE AS day
					FROM
						paid_holidays
					WHERE
						country_code = \'IN\'
						AND DATE BETWEEN ' . $strStartDate . ' AND ' . $strEndDate . '
					ORDER BY
						day';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaidHolidaysByTitleByCountryCode( $arrstrFilteredExplodedSearch, $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						paid_holidays
					Where
						country_code = \'' . $strCountryCode . '\'
						AND title ILIKE E\'%' . implode( '%\' AND title ILIKE E\'%', $arrstrFilteredExplodedSearch ) . '%\'
						AND DATE_PART ( \'year\', DATE ( date ) ) = ' . date( 'Y' );

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentAndNextPaidHolidaysByMonth( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						paid_holidays
					WHERE
						is_published = 1
						AND to_char( NOW(), \'YYYY/MM\' ) = to_char( paid_holidays . date, \'YYYY/MM\' )
					UNION
					( SELECT
							*
						FROM
							paid_holidays
						WHERE
							is_published = 1
							AND to_char( NOW(), \'YYYY/MM\' ) < to_char( paid_holidays . date, \'YYYY/MM\' )
							ORDER BY date
							LIMIT 2
					)ORDER BY date';

		return parent::fetchPaidHolidays( $strSql, $objDatabase );
	}

	public static function fetchPaidHolidayByCurrentDateByCountryCode( $strDate, $strCountryCode, $objAdminDatabase ) {
		if( false == valStr( $strDate ) || false == valStr( $strCountryCode ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						paid_holidays
					WHERE
						is_published = 0
						AND is_optional IS FALSE
						AND to_char( paid_holidays. date, \'YYYY/MM/DD\' ) = \'' . $strDate . '\'
						AND country_code = \'' . $strCountryCode . '\' ';

		return parent::fetchPaidHoliday( $strSql, $objAdminDatabase );
	}

	public static function fetchPublishPaidHolidayByCurrentDateByCountryCode( $strDate, $strCountryCode, $objAdminDatabase ) {
		if( false == valStr( $strDate ) || false == valStr( $strCountryCode ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						paid_holidays
					WHERE
						is_published = 1
						AND is_optional IS FALSE
						AND to_char( paid_holidays. date, \'YYYY/MM/DD\' ) = \'' . $strDate . '\'
						AND country_code = \'' . $strCountryCode . '\' ';

		return parent::fetchPaidHoliday( $strSql, $objAdminDatabase );
	}

}
?>
