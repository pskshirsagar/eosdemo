<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CClientNameDocuments
 * Do not add any new functions to this class.
 */

class CClientNameDocuments extends CBaseClientNameDocuments {

	public static function fetchClientNameDocuments( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CClientNameDocument', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchClientNameDocument( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CClientNameDocument', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>