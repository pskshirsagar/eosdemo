<?php

class CResourceAllocation extends CBaseResourceAllocation {

	public function valResourceRequisitionId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intResourceRequisitionId ) || false == is_int( $this->m_intResourceRequisitionId ) || 0 == $this->m_intResourceRequisitionId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'resource_requisition_id', 'enter resource requisition id' ) );
		}
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intEmployeeId ) || false == is_int( $this->m_intEmployeeId ) || 0 == $this->m_intEmployeeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'enter employee id' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valResourceRequisitionId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}
}
?>