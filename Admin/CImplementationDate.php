<?php

class CImplementationDate extends CBaseImplementationDate {

	protected $m_strRequestedByEmployee;
	protected $m_strApprovedByEmployee;

	const CONFIRMATION_STATUS_APPROVED = 'Approved';
	const CONFIRMATION_STATUS_DENIED = 'Denied';

	/**
	 * Get Functions
	 *
	 */

	public function getRequestedByEmployee() {
		return $this->m_strRequestedByEmployee;
	}

	public function getApprovedByEmployee() {
		return $this->m_strApprovedByEmployee;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setRequestedByEmployee( $strRequestedByEmployee ) {
		$this->m_strRequestedByEmployee = $strRequestedByEmployee;
	}

	public function setApprovedByEmployee( $strApprovedByEmployee ) {
		$this->m_strApprovedByEmployee = $strApprovedByEmployee;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['requested_by_employee'] ) ) 			$this->setRequestedByEmployee( $arrmixValues['requested_by_employee'] );
		if( true == isset( $arrmixValues['approved_by_employee'] ) ) 			$this->setApprovedByEmployee( $arrmixValues['approved_by_employee'] );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationEndDate() {
		$boolIsValid = true;

		if( false == valStr( $this->getImplementationEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Implementation End Date is required. ' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valConfirmationStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReasonChanged() {
		$boolIsValid = true;

		if( false == valStr( $this->getReasonChanged() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Reason is required. ' ) );
			return $boolIsValid;
		}

		if( false == preg_match( '/^[a-zA-Z0-9 \r\n .\:\;\<\>\[\]\{\}\(\)\$\#\%\*\@\~\/\,\-\_\&\'\"\!\?\/\\\]+$/', $this->getReasonChanged() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Reason contains invalid characters. ' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valReasonConfirmed() {
		$boolIsValid = true;

		if( false == valStr( $this->getReasonConfirmed() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Reason is required. ' ) );
			return $boolIsValid;
		}

		if( false == preg_match( '/^[a-zA-Z0-9 .\:\;\<\>\[\]\{\}\(\)\$\#\%\*\@\~\/\,\-\_\&\'\"\!\?\/\\\]+$/', $this->getReasonConfirmed() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Reason contains invalid characters. ' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valConfirmedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valConfirmedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valImplementationEndDate();
				$boolIsValid &= $this->valReasonChanged();
				break;

			case 'validate_response':
				$boolIsValid &= $this->valReasonConfirmed();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>