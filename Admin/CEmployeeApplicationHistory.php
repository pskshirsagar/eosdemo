<?php

class CEmployeeApplicationHistory extends CBaseEmployeeApplicationHistory {

	public function valEmployeeApplicationCompanyId( $intCompanyIndex ) {
		$boolIsValid = true;
		if( true == is_null( $this->getEmployeeApplicationCompanyId() ) ) {
			$boolIsValid = false;

			$strErrorMessage = ( 0 == $intCompanyIndex ) ? 'Current' : 'Previous';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_application_company_id', $strErrorMessage . ' company name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valDesignation( $intCompanyIndex ) {
		$boolIsValid = true;
		if( true == is_null( $this->getDesignation() ) ) {
			$boolIsValid = false;

			$strErrorMessage = ( 0 == $intCompanyIndex ) ? 'Current' : 'Previous';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'designation', $strErrorMessage . ' company designation is required.' ) );
		}
		return $boolIsValid;
	}

	public function valStartDate( $intCompanyIndex ) {
		$boolIsValid = true;
		if( false == \Psi\CStringService::singleton()->preg_match( '/(\d{1,2})\/(\d{2})\/(\d{4})/', $this->getStartDate() ) ) {
			$boolIsValid = false;

			$strErrorMessage = ( 0 == $intCompanyIndex ) ? 'Current' : 'Previous';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', $strErrorMessage . ' company start date is required.' ) );
		}
		return $boolIsValid;
	}

	public function valEndDate( $intCompanyIndex ) {
		$boolIsValid = true;
		if( true == is_null( $this->getEndDate() ) ) {
			$boolIsValid = true;
		} elseif( false == \Psi\CStringService::singleton()->preg_match( '/(\d{1,2})\/(\d{2})\/(\d{4})/', $this->getEndDate() ) ) {
			$boolIsValid = false;

			$strErrorMessage = ( 0 == $intCompanyIndex ) ? 'Current' : 'Previous';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', $strErrorMessage . ' company end date is required.' ) );

		} elseif( true == \Psi\CStringService::singleton()->preg_match( '/(\d{1,2})\/(\d{2})\/(\d{4})/', $this->getEndDate() ) && true == \Psi\CStringService::singleton()->preg_match( '/(\d{1,2})\/(\d{2})\/(\d{4})/', $this->getStartDate() ) ) {
			if( strtotime( $this->getStartDate() ) >= strtotime( $this->getEndDate() ) ) {
				$boolIsValid = false;

				$strErrorMessage = ( 0 == $intCompanyIndex ) ? 'Current' : 'Previous';
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', $strErrorMessage . ' company end date should be greater than start date.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valReasonForLeaving( $intCompanyIndex ) {
		$boolIsValid = true;
		if( true == is_null( $this->getReasonForLeaving() ) ) {
			$boolIsValid = false;

			$strErrorMessage = ( 0 == $intCompanyIndex ) ? 'Current' : 'Previous';
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reason', $strErrorMessage . ' company reason for leaving is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsOtherCompanyName = false, $intCompanyIndex = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				if( false == $boolIsOtherCompanyName ) {
					$boolIsValid &= $this->valEmployeeApplicationCompanyId( $intCompanyIndex );
				}

				$boolIsValid &= $this->valDesignation( $intCompanyIndex );
				$boolIsValid &= $this->valStartDate( $intCompanyIndex );

				// comenting end date validation for work experince details
				$boolIsValid &= $this->valEndDate( $intCompanyIndex );

				$boolIsValid &= $this->valReasonForLeaving( $intCompanyIndex );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>