<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CJobPostingNotifications
 * Do not add any new functions to this class.
 */

class CJobPostingNotifications extends CBaseJobPostingNotifications {

	public static function fetchJobPostingNotificationsByEmailAddress( $strEmailAddress, $objDatabase ) {

		if( false == valStr( $strEmailAddress ) ) return NULL;
		$strSql = ' SELECT
						jpn.*,
						jpc.name as category_name
					FROM
						job_posting_notifications jpn
						LEFT JOIN job_posting_categories jpc ON ( jpn.job_posting_category_id = jpc.id )
					WHERE
						jpn.email_address = \'' . $strEmailAddress . '\'';

		return parent::fetchJobPostingNotifications( $strSql, $objDatabase );
	}

	public static function fetchJobPostingNotificationsByDepartmentByCountryCode( $intDepartmentId, $strCountryCode, $objDatabase, $arrstrBlockedEmails = NULL ) {

		$strWhereCondition = '';
		if( true == valArr( $arrstrBlockedEmails ) ) {
			$strWhereCondition = 'WHERE jpn.email_address NOT IN (\'' . implode( '\',\'', $arrstrBlockedEmails ) . '\')';
		}

		$strSql = ' SELECT
						jpn.*
					FROM
						job_posting_category_departments jpcd
						JOIN job_posting_categories jpc ON ( jpc.id = jpcd.job_posting_category_id AND jpcd.department_id = ' . ( int ) $intDepartmentId . ' AND jpc.country_code = \'' . $strCountryCode . '\')
						JOIN job_posting_notifications jpn ON ( jpc.id = jpn.job_posting_category_id )';

		$strSql .= $strWhereCondition;

		return parent::fetchJobPostingNotifications( $strSql, $objDatabase );
	}
}
?>