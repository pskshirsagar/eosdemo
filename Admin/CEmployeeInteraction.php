<?php

class CEmployeeInteraction extends CBaseEmployeeInteraction {

	protected $m_strEmployeeInteractionTypeName;
	protected $m_strScheduledByManagerName;

	const MAX_CHAR_LENGTH = 1000;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['employee_interaction_type_name'] ) )	$this->setEmployeeInteractionTypeName( $arrmixValues['employee_interaction_type_name'] );
		if( isset( $arrmixValues['scheduled_by_manager_name'] ) )		$this->setScheduledByManagerName( $arrmixValues['scheduled_by_manager_name'] );
		return true;
	}

	public function setEmployeeInteractionTypeName( $strEmployeeInteractionTypeName ) {
		$this->m_strEmployeeInteractionTypeName = $strEmployeeInteractionTypeName;
	}

	public function setScheduledByManagerName( $strScheduledByManagerName ) {
		$this->m_strScheduledByManagerName = $strScheduledByManagerName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function getEmployeeInteractionTypeName() {
		return $this->m_strEmployeeInteractionTypeName;
	}

	public function getScheduledByManagerName() {
		return $this->m_strScheduledByManagerName;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeInteractionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallAgentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCallId() {
		$boolIsValid = true;

		if( false == valStr( $this->getCallId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Please select call id.' ) );
		}

		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;

		if( false == valStr( $this->getNotes() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Public Note is required.' ) );
		} elseif( self::MAX_CHAR_LENGTH < strlen( $this->getNotes() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Public Note should not be more than 1000 characters.' ) );
		}

		return $boolIsValid;
	}

	public function valRemarks() {
		$boolIsValid = true;

		if( self::MAX_CHAR_LENGTH < strlen( $this->getRemarks() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Private Note should not be more than 1000 characters.' ) );
		}

		return $boolIsValid;
	}

	public function valScheduledBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduledOn() {
		$boolIsValid = true;

		if( false == valStr( $this->getScheduledOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Scheduled date is required.' ) );
		} elseif( strtotime( $this->getScheduledOn() ) > time() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Scheduled Date can not be grater than current date.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valNotes();
				$boolIsValid &= $this->valRemarks();
				$boolIsValid &= $this->valScheduledOn();

				if( CEmployeeInteractionType::CALL_REVIEW == $this->getEmployeeInteractionTypeId() ) {
					$boolIsValid &= $this->valCallId();
				}
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>