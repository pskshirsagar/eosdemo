<?php

class CUserPermission extends CBaseUserPermission {

	public function valUserId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intUserId ) ) {
			trigger_error( 'User id was not set.', E_USER_ERROR );
			exit;
		}

		return $boolIsValid;
	}

	public function valPsModuleId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPsModuleId ) ) {
			$boolIsValid = false;
			trigger_error( 'Module was not set on group permission insert or update attempt.', E_USER_ERROR );
			exit;
		}

		return $boolIsValid;
	}

	public function valAllowAndInherited() {
		$boolIsValid = true;

		if( false == isset( $this->m_intIsAllowed ) || false == isset( $this->m_intIsInherited ) ) {
			$boolIsValid = false;
			trigger_error( 'Allow and Inherited must be set to either 0 or 1.', E_USER_ERROR );
		}

		if( ( 1 == $this->m_intIsAllowed && 1 == $this->m_intIsInherited ) ) {
			$boolIsValid = false;
			trigger_error( 'Allow and Inherited cannot both be set to the same value.', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUserId();
				$boolIsValid &= $this->valPsModuleId();
				$boolIsValid &= $this->valAllowAndInherited();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>