<?php

class CIlsEmailReparseLog extends CBaseIlsEmailReparseLog {
	protected $m_strExecutionTime;
	protected $m_intIlsEmailErrorTypeId;

	/**
	 * Setters
	 */

	public function setExecutionTime( $strExecutionTime ) {
		$this->m_strExecutionTime = $strExecutionTime;
	}

	public function setIlsEmailErrorTypeId( $intIlsEmailErrorTypeId ) {
		$this->m_intIlsEmailErrorTypeId = $intIlsEmailErrorTypeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['execution_time'] ) ) $this->setExecutionTime( $arrmixValues['execution_time'] );
		if( true == isset( $arrmixValues['ils_email_error_type_id'] ) ) $this->setIlsEmailErrorTypeId( $arrmixValues['ils_email_error_type_id'] );
	}

	/**
	 * Getters
	 */

	public function getExecutionTime() {
		return $this->m_strExecutionTime;
	}

	public function getIlsEmailErrorTypeId() {
		return $this->m_intIlsEmailErrorTypeId;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>