<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemCampaignSources
 * Do not add any new functions to this class.
 */

class CSemCampaignSources extends CBaseSemCampaignSources {

	public static function fetchSemCampaignSourceBySemSourceIdBySemCampignId( $intSemSourceId, $intSemCampaignId, $objAdminDatabase ) {

		$strSql = 'SELECT * FROM sem_campaign_sources WHERE sem_source_id = ' . ( int ) $intSemSourceId . ' AND sem_campaign_id = ' . ( int ) $intSemCampaignId . ' LIMIT 1';

		return parent::fetchSemCampaignSource( $strSql, $objAdminDatabase );
	}

	public static function fetchSemCampaignSourcesBySemSourceId( $intSemSourceId, $objAdminDatabase ) {

		$strSql = 'SELECT
						scs.*,
						sa.remote_primary_key AS sem_account_remote_primary_key
				   FROM
						sem_campaign_sources scs,sem_accounts sa
				   WHERE
				   		scs.sem_account_id = sa.id
				   AND
				   		scs.sem_source_id =  ' . ( int ) $intSemSourceId;

		return self::fetchSemCampaignSources( $strSql, $objAdminDatabase );
	}

	public static function fetchSemCampaignSourcesBySemAccountIdBySemSourceId( $intSemAccountId, $intSemSourceId, $objAdminDatabase ) {

		$strSql = 'SELECT
						scs.*,
						sc.name
				   FROM
						sem_campaign_sources scs,
						sem_campaigns sc
				   WHERE
				   		scs.sem_campaign_id = sc.id
				   AND
				   		scs.sem_account_id =  ' . ( int ) $intSemAccountId . '
				   AND
				   		scs.sem_source_id =  ' . ( int ) $intSemSourceId;

		return self::fetchSemCampaignSources( $strSql, $objAdminDatabase );
	}

	public static function fetchSemCampaignSourceBySemAccountIdBySemSourceIdByRemotePrimaryKey( $intSemAccountId, $intSemSourceId, $strRemotePrimaryKey, $objAdminDatabase ) {

		$strSql = 'SELECT
						scs.*,
						sc.name
				   FROM
						sem_campaign_sources scs,
						sem_campaigns sc
				   WHERE
				   		scs.sem_campaign_id = sc.id
				   AND
				   		scs.sem_account_id =  ' . ( int ) $intSemAccountId . '
				   AND
				   		scs.sem_source_id =  ' . ( int ) $intSemSourceId . '
				   AND
				   		scs.remote_primary_key =  \'' . ( string ) addslashes( $strRemotePrimaryKey ) . '\'';

		return self::fetchSemCampaignSource( $strSql, $objAdminDatabase );
	}

	public static function fetchSemCampaignSourceBySemAccountIdBySemSourceIdByName( $intSemAccountId, $intSemSourceId, $strName, $objAdminDatabase ) {

		$strSql = 'SELECT
						scs.*,
						sc.name
				   FROM
						sem_campaign_sources scs,
						sem_campaigns sc
				   WHERE
				   		scs.sem_campaign_id = sc.id
				   AND
				   		scs.sem_account_id =  ' . ( int ) $intSemAccountId . '
				   AND
				   		scs.sem_source_id =  ' . ( int ) $intSemSourceId . '
				   AND
				   		sc.name =  \'' . ( string ) addslashes( $strName ) . '\'';

		return parent::fetchSemCampaignSource( $strSql, $objAdminDatabase );
	}

	public static function fetchSemCampaignSourcesBySemCampaignIdBySemSourceId( $intSemCampaignId, $intSemSourceId, $objDatabase ) {
		$strSql = 'SELECT * FROM sem_campaign_sources WHERE sem_campaign_id = ' . ( int ) $intSemCampaignId . ' AND sem_source_id = ' . ( int ) $intSemSourceId;
		return self::fetchSemCampaignSource( $strSql, $objDatabase );
	}

}
?>