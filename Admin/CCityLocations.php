<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCityLocations
 * Do not add any new functions to this class.
 */

class CCityLocations extends CBaseCityLocations {
	public static function fetchCityLocationsByCityId( $intCityId, $objDatabase ) {

		if( true == is_null( $intCityId ) ) return NULL;

		$strSql = ' SELECT * FROM city_locations WHERE city_id = ' . ( int ) $intCityId;

		return self::fetchCityLocations( $strSql, $objDatabase );
	}
}
?>