<?php

use Psi\Libraries\UtilDates\CDates;

class CPsNotification extends CBasePsNotification {

	protected $m_strGroupName;
	protected $m_strTimeDifference;
	protected $m_strDepartmentName;
	protected $m_strPsNotificationIds;
	protected $m_strCreatedByEmployee;
	protected $m_strPsNotificationTypeName;

	const CUSTOM_NOTIFICATION = 7;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if ( isset( $arrmixValues['ps_notification_type_name'] ) )	$this->setPsNotificationTypeName( $arrmixValues['ps_notification_type_name'] );
		if ( isset( $arrmixValues['group_name'] ) ) 				$this->setGroupName( $arrmixValues['group_name'] );
		if ( isset( $arrmixValues['ps_notification_ids'] ) )		$this->setPsNotificationIds( $arrmixValues['ps_notification_ids'] );
		if ( isset( $arrmixValues['created_by_employee'] ) ) 		$this->setCreatedByEmployee( $arrmixValues['created_by_employee'] );
		if ( isset( $arrmixValues['departmant_name'] ) )			$this->setDepartmentName( $arrmixValues['departmant_name'] );

		$this->setTimeDifference( $this->getCreatedOn() );

		return;
	}

	public function setPsNotificationTypeName( $strName ) {
		$this->m_strPsNotificationTypeName = CStrings::strTrimDef( $strName, 100, NULL, true );
	}

	public function setGroupName( $strName ) {
		$this->m_strGroupName = CStrings::strTrimDef( $strName, 50, NULL, true );
	}

	public function setTimeDifference( $strDateTime ) {
		$intDateTimestamp			= valStr( $strDateTime ) ? strtotime( $strDateTime ) : $strDateTime;
		$intCurrentTimestamp		= time();
		$arrstrDateDifferenceParts 	= CDates::createService()->getDateDifference( $intDateTimestamp, $intCurrentTimestamp );

		$this->m_strTimeDifference	= NULL;
		if( 1 == $arrstrDateDifferenceParts['days_total'] ) {
			$this->m_strTimeDifference = 'Yesterday at ' . date( 'h:i A', strtotime( $this->getCreatedOn() ) );
		} elseif( 1 < $arrstrDateDifferenceParts['days_total'] && 3 >= $arrstrDateDifferenceParts['days_total'] ) {
			$this->m_strTimeDifference = date( 'D', strtotime( $this->getCreatedOn() ) ) . ' at ' . date( 'h:i A', strtotime( $this->getCreatedOn() ) );
		} elseif( 4 <= $arrstrDateDifferenceParts['days_total'] ) {
			$this->m_strTimeDifference = date( 'd M', strtotime( $this->getCreatedOn() ) ) . ' at ' . date( 'h:i A', strtotime( $this->getCreatedOn() ) );
		} else {
			$this->m_strTimeDifference = getTimelineFormat( strtotime( $this->getCreatedOn() ) );
		}
	}

	public function setPsNotificationIds( $strPsNotificationIds ) {
		$this->m_strPsNotificationIds = $strPsNotificationIds;
	}

	public function setCreatedByEmployee( $strCreatedByEmployee ) {
		$this->m_strCreatedByEmployee = $strCreatedByEmployee;
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getPsNotificationTypeName() {
		return $this->m_strPsNotificationTypeName;
	}

	public function getGroupName() {
		return $this->m_strGroupName;
	}

	public function getTimeDifference() {
		return $this->m_strTimeDifference;
	}

	public function getPsNotificationIds() {
		return $this->m_strPsNotificationIds;
	}

	public function getCreatedByEmployee() {
		return $this->m_strCreatedByEmployee;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valMessage() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strMessage ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', 'Message is required.' ) );
		}

		return $boolIsValid;
	}

	public function valReferenceUrl() {
		$boolIsValid = true;

		if( true == valStr( $this->m_strReferenceUrl ) && false == CValidation::checkUrl( $this->m_strReferenceUrl ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'message', 'A valid URL is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valMessage();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_reference_url':
				$boolIsValid &= $this->valReferenceUrl();
				break;

			default:
				// no validation avaliable for delete and update action
		}

		return $boolIsValid;
	}

	public function sendNotifications( $intPsNotificationTypeId, $strMessage, $intCurrentUserId, $objAdminDatabase, $strReferenceUrl = NULL, $arrintEmployeeIds = NULL, $arrintGroupIds = NULL, $boolIsPublic = 0, $intPsDocumentId = NULL, $boolReturnSqlOnly = false, $boolShowPopup = false, $boolFromDepartment = false ) {

		$arrobjPsNotifications			= array();
		$arrobjPsNotificationEmployees 	= array();

		if( true == is_null( $arrintEmployeeIds ) && true == is_null( $arrintGroupIds ) && false == $boolIsPublic ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'target_employees', 'Target employee is required.' ) );
			return false;
		}

		if( false == is_numeric( $intPsNotificationTypeId ) || false == is_numeric( $intCurrentUserId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'target_employees', 'Invalid paremeter is passed.' ) );
			return false;
		}

		if( true == valArr( $arrintEmployeeIds ) ) {

			$arrobjPsNotifications[] = $this->createPsNotifications( $intPsNotificationTypeId, $strMessage, $strReferenceUrl, $intPsDocumentId, NULL, NULL, $boolIsPublic, true, $boolShowPopup, $boolFromDepartment, $objAdminDatabase );
			foreach( $arrintEmployeeIds as $intEmployeeId ) {
				$arrobjPsNotificationEmployees[] = $this->createPsNotificationEmployees( $intEmployeeId, $arrobjPsNotifications[0]->getId() );
			}
		}

		if( true == valArr( $arrintGroupIds ) ) {
			foreach( $arrintGroupIds as $intGroupId ) {
				$arrobjPsNotifications[] = $this->createPsNotifications( $intPsNotificationTypeId, $strMessage, $strReferenceUrl, $intPsDocumentId, NULL, $intGroupId, $boolIsPublic, false, $boolShowPopup, $boolFromDepartment, $objAdminDatabase );
			}
		}

		if( true == $boolIsPublic ) {
			$arrobjPsNotifications[] = $this->createPsNotifications( $intPsNotificationTypeId, $strMessage, $strReferenceUrl, $intPsDocumentId, NULL, NULL, $boolIsPublic, false, $boolShowPopup, $boolFromDepartment, $objAdminDatabase );
		}

		if( true == valArr( $arrobjPsNotifications ) && false == \Psi\Eos\Admin\CPsNotifications::createService()->bulkInsert( $arrobjPsNotifications, $intCurrentUserId, $objAdminDatabase ) ) {
			return false;
		}

		if( true == valArr( $arrobjPsNotificationEmployees ) && false == CPsNotificationEmployees::bulkInsert( $arrobjPsNotificationEmployees, $intCurrentUserId, $objAdminDatabase ) ) {
			return false;
		}

		return true;
	}

	public function createPsNotifications( $intPsNotificationTypeId, $strMessage, $strReferenceUrl = NULL, $intPsDocumentId, $intEmployeeId = NULL, $intGroupId = NULL, $boolIsPublic= 0, $boolIsSync = false, $boolShowPopup = false,  $boolFromDepartment=false, $objAdminDatabase ) {

		$objPsNotification = new CPsNotification();
		$objPsNotification->setId( $objPsNotification->fetchNextId( $objAdminDatabase ) );
		$objPsNotification->setPsNotificationTypeId( $intPsNotificationTypeId );
		$objPsNotification->setReferenceUrl( $strReferenceUrl );
		$objPsNotification->setMessage( $strMessage );
		$objPsNotification->setEmployeeId( $intEmployeeId );
		$objPsNotification->setGroupId( $intGroupId );
		$objPsNotification->setPsDocumentId( $intPsDocumentId );
		$objPsNotification->setIsPublic( $boolIsPublic );
		$objPsNotification->setIsSync( $boolIsSync );
		$objPsNotification->setIsShowPopup( $boolShowPopup );
		$objPsNotification->setIsFromDepartment( $boolFromDepartment );

		return $objPsNotification;
	}

	public function createPsNotificationEmployees( $intEmployeeId, $intPsNotificationId ) {
		$objPsNotificationEmployee = new CPsNotificationEmployee();
		$objPsNotificationEmployee->setPsNotificationId( $intPsNotificationId );
		$objPsNotificationEmployee->setEmployeeId( $intEmployeeId );

		return $objPsNotificationEmployee;
	}

}
?>