<?php
class CTestLevelType extends CBaseTestLevelType {

	const BEGINNER		= 1;
	const INTERMEDIATE	= 2;
	const ADVANCE		= 3;
	const PROFESSIONAL	= 4;
}
?>