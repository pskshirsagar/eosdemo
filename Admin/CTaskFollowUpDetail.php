<?php

class CTaskFollowUpDetail extends CBaseTaskFollowUpDetail {

	const ID_SYSTEM					= '';

	public static $c_arrstrInternalNoteFollowUp	= array(
		CTaskPriority::IMMEDIATE			=> '1:00:00',
		CTaskPriority::URGENT				=> '3:30:00',
		CTaskPriority::HIGH					=> '24:00:00',
		CTaskPriority::NORMAL				=> '24:00:00',
		CTaskPriority::LOW					=> '24:00:00'
	);

	public static $c_arrstrUserIdFollowUp	= array(
		CTaskPriority::IMMEDIATE	=> '0:15:00',
		CTaskPriority::URGENT		=> '0:30:00',
		CTaskPriority::HIGH			=> '1:30:00',
		CTaskPriority::NORMAL		=> '3:30:00',
		CTaskPriority::LOW			=> '5:30:00'
	);

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskPriorityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFollowUpTime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>