<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDbWatchPriorities
 * Do not add any new functions to this class.
 */

class CDbWatchPriorities extends CBaseDbWatchPriorities {

	public static function fetchDbWatchPriorities( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CDbWatchPriority', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchDbWatchPriority( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CDbWatchPriority', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllPublishedDbWatchPriorities( $objDatabase ) {

		$strSql = 'SELECT * FROM db_watch_priorities WHERE is_published = 1';

		return self::fetchDbWatchPriorities( $strSql, $objDatabase );
	}
}
?>