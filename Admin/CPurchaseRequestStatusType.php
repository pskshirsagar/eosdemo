<?php

class CPurchaseRequestStatusType extends CBasePurchaseRequestStatusType {

	const IN_PROCESS			        = 'In Process';
	const DENIED				        = 'Denied';
	const APPROVED				        = 'Approved';
	const APPROVED_BY_MANAGER	        = 'Approved By Manager';
	const DENIED_BY_MANAGER		        = 'Denied By Manager';
	const PENDING_BY_MANAGER	        = 'Pending By Manager';
	const ALLOCATED				        = 'Allocated';
	const HIRING_IN_PROCESS		        = 'Hiring In Process';
	const PENDING_FOR_REMOVE_FROM_HR    = 'Pending for removal by HR';
	const PENDING_BY_HR 		        = 'Pending by HR';
	const ON_HOLD				        = 'On Hold';
	const DELETED				        = 'Deleted';
	const REMOVED				        = 'Removed';
	const WORK_IN_PROGRESS              = 'Work In Progress';
	const PR_ON_HOLD                    = 'On Hold';

	const ID_IN_PROCESS						= 1;
	const ID_DENIED							= 2;
	const ID_APPROVED						= 3;
	const ID_PENDING_BY_MANAGER				= 6;
	const ID_ALLOCATED						= 7;
	const ID_HIRING_IN_PROCESS				= 8;
	const ID_DENIED_BY_MANAGER				= 9;
	const ID_APPROVED_BY_MANAGER			= 10;
	const ID_HOLD							= 11;
	const ID_MERGED							= 12;
	const ID_SPLITED						= 13;
	const ID_REMOVED						= 14;
	const ID_PENDING_FOR_REMOVE_FROM_HR		= 15;
	const ID_PENDING_BY_HR 					= 16;
	const ID_WORK_IN_PROGRESS				= 17;
	const ID_PR_ON_HOLD						= 18;


	public static $c_arrintMyPurchaseRequestStatusTypeIds = [
		self::ID_IN_PROCESS,
		self::ID_DENIED,
		self::ID_APPROVED,
		self::ID_PENDING_BY_MANAGER,
	];

	public static $c_arrstrPurchaseRequestStatusType = array(
		'1' => self::IN_PROCESS,
		'2' => self::DENIED,
		'3' => self::APPROVED,
		'6' => self::PENDING_BY_MANAGER,
		'7' => self::ALLOCATED,
		'8' => self::HIRING_IN_PROCESS,
		'9' => self::DENIED_BY_MANAGER,
		'10' => self::APPROVED_BY_MANAGER,
		'17' => self::WORK_IN_PROGRESS,
		'18' => self::PR_ON_HOLD
	);

	public static $c_arrmixPurchaseRequestActionTypes = [
		self::ID_PENDING_BY_HR				=> CPurchaseRequestAction::FILL,
		self::ID_HOLD						=> CPurchaseRequestAction::HOLD,
		self::ID_PENDING_FOR_REMOVE_FROM_HR	=> CPurchaseRequestAction::REMOVE
	];

	public static $c_arrintClosedPurchaseRequestStatusTypeIds = [
		self::ID_DENIED,
		self::ID_APPROVED,
		self::ID_REMOVED
	];

	public static $c_arrintPendingPurchaseRequestStatusTypeIds = [
		self::ID_IN_PROCESS,
		self::ID_PENDING_BY_MANAGER,
		self::ID_HIRING_IN_PROCESS,
		self::ID_DENIED_BY_MANAGER,
		self::ID_APPROVED_BY_MANAGER,
		self::ID_PENDING_FOR_REMOVE_FROM_HR,
		self::ID_PENDING_BY_HR
	];

	public static $c_arrintPurchaseRequestStatusTypes = array(
		self::ID_HIRING_IN_PROCESS,
		self::ID_APPROVED
	);

	public static $c_arrintRequestStatusTypeIdsForPromote = [
		self::ID_DENIED,
		self::ID_ALLOCATED,
		self::ID_HIRING_IN_PROCESS,
		self::ID_DENIED_BY_MANAGER,
		self::ID_REMOVED,
		self::ID_MERGED,
		self::ID_SPLITED
	];

	public static $c_arrintPendingbyManagerPurchaseRequestStatusTypeIds = [
		self::ID_IN_PROCESS,
		self::ID_PENDING_BY_MANAGER,
		self::ID_PENDING_BY_HR,
		self::ID_APPROVED_BY_MANAGER
	];

	public static $c_arrintPendingPurchaseRequestStatusTypeIdsForEndEmployment = [
		self::ID_IN_PROCESS,
		self::ID_PENDING_BY_MANAGER,
		self::ID_HIRING_IN_PROCESS,
		self::ID_PENDING_BY_HR,
		self::ID_PENDING_FOR_REMOVE_FROM_HR,
		self::ID_HOLD,
		self::ID_APPROVED_BY_MANAGER
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>