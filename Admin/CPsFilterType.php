<?php

class CPsFilterType extends CBasePsFilterType {

	const SYSTEM_ERRORS 						= 1;
	const TASKS 								= 2;
	const SYSTEM_EMAILS 						= 3;
	const EMPLOYEE_APPLICATIONS					= 4;
	const EMPLOYEES								= 5;
	const DESIGNATIONS							= 6;
	const PS_DOCUMENTS							= 7;
	const TRAININGS								= 8;
	const PS_EMPLOYEE_ASSESSMENT_TYPES 			= 9;
	const PS_EMPLOYEES							= 10;
	const INVENTORY_ASSETS						= 11;
	const PS_WEBSITE_JOB_POSTING				= 12;
	const TRAINING_SESSIONS 					= 13;
	const TEAMS									= 14;
	const SECURITY_POLICY_ACKNOWLEDGEMENT 		= 15;
	const TESTS							 		= 16;
	const REVIEWS								= 17;
	const ADS									= 18;
	const STATISTICS							= 19;
	const SPRINT_SUMMARY_FILTER					= 20;
	const IMPLEMENTATION_TIMELINE_REPORT_FILTER	= 21;
	const EMPLOYEES_HISTORIES					= 22;
	const API_REQUEST_LOGS						= 23;
	const SYSTEM_MESSAGES						= 24;
	const EMPLOYEES_PIP							= 25;
	const CA_RELEASE_NOTE_FILTER				= 26;
	const CA_COMMING_SOON_RELEASE_NOTE_FILTER	= 27;
	const CA_PROSPECTING_EDIT_FILTER	        = 28;
	const INVENTORY_EQUIPMENTS					= 29;
	public static $c_arrintEmployeesFilterTypes = array(
		CPsFilterType::PS_EMPLOYEES,
		CPsFilterType::EMPLOYEE_APPLICATIONS
	);

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>