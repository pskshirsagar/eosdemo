<?php

class CEmailSummary extends CBaseEmailSummary {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function createEmailSummaryForTaskNote( $intTaskId, $intNoteId, $objAdminDatabase, $intUserId ) {
		if( false == valId( $intTaskId ) || false == valId( $intTaskId ) || false == valId( $intNoteId ) || false == valId( $intUserId ) ) {
			return true;
		}

		$objTaskEmailSummary = \Psi\Eos\Admin\CEmailSummaries::createService()->fetchEmailSummariesByReferenceNumber( $this->getReferenceNumber(), $objAdminDatabase );
		$arrstrJsonDecodedData['task_note'] = $intNoteId;
		if( true == valObj( $objTaskEmailSummary, 'CEmailSummary' ) ) {
			$objTaskEmailSummary->setDescription( json_encode( $arrstrJsonDecodedData ) );

		} else {
			$objTaskEmailSummary = new CEmailSummary();
			$objTaskEmailSummary->setEmailSummaryTypeId( CEmailSummaryType::TASK );
			$objTaskEmailSummary->setIsQueued( false );
			$objTaskEmailSummary->setReferenceNumber( $intTaskId );
			$objTaskEmailSummary->setDescription( json_encode( $arrstrJsonDecodedData ) );
		}

		if( true == valObj( $objTaskEmailSummary, 'CEmailSummary' ) && ( false == $objTaskEmailSummary->insertOrUpdate( $intUserId, $objAdminDatabase, false ) ) ) {
			return false;
		}
		return true;
	}

}
?>