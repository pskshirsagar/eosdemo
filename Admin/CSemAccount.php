<?php

class CSemAccount extends CBaseSemAccount {

	protected $m_arrobjSemAdGroupSources;
	protected $m_arrobjSemKeywordSources;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Add Functions
	 */

	public function addSemAdGroupSource( $objSemAdGroupSource ) {
		$this->m_arrobjSemAdGroupSources[$objSemAdGroupSource->getId()] = $objSemAdGroupSource;
	}

	public function addSemKeywordSource( $objSemKeywordSource ) {
		$this->m_arrobjSemKeywordSources[$objSemKeywordSource->getId()] = $objSemKeywordSource;
	}

	/**
	 * Get Functions
	 */

	public function getSemAdGroupSources() {
		return $this->m_arrobjSemAdGroupSources;
	}

	public function getSemKeywordSources() {
		return $this->m_arrobjSemKeywordSources;
	}

	/**
	 * Set Functions
	 */

	public function setSemAdGroupSources( $objSemAdGroupSource ) {
		$this->m_arrobjSemAdGroupSources = $objSemAdGroupSource;
	}

	public function setSemKeywordSources( $objSemKeywordSource ) {
		$this->m_arrobjSemKeywordSources = $objSemKeywordSource;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchSemAdGroups( $objAdminDatabase ) {
		return CSemAdGroups::fetchSemAdGroupsBySemAccountId( $this->getId(), $objAdminDatabase );
	}

	public function fetchSemCampaigns( $objAdminDatabase ) {
		return CSemCampaigns::fetchSemCampaignsBySemAccountId( $this->getId(), $objAdminDatabase );
	}

	public function fetchSemCampaignSources( $objAdminDatabase ) {
		return CSemCampaignSources::fetchSemCampaignSourcesBySemAccountIdBySemSourceId( $this->getId(), $this->getSemSourceId(), $objAdminDatabase );
	}

	public function fetchSemAdGroupSources( $objAdminDatabase ) {
		return CSemAdGroupSources::fetchSemAdGroupSourcesBySemAccountIdBySemSourceId( $this->getId(), $this->getSemSourceId(), $objAdminDatabase );
	}

	public function fetchSemCampaignSourceByRemotePrimaryKey( $strRemotePrimaryKey, $objAdminDatabase ) {
		return CSemCampaignSources::fetchSemCampaignSourceBySemAccountIdBySemSourceIdByRemotePrimaryKey( $this->getId(), $this->getSemSourceId(), $strRemotePrimaryKey, $objAdminDatabase );
	}

	public function fetchSemCampaignSourceByName( $strName, $objAdminDatabase ) {
		return CSemCampaignSources::fetchSemCampaignSourceBySemAccountIdBySemSourceIdByName( $this->getId(), $this->getSemSourceId(), $strName, $objAdminDatabase );
	}

	public function fetchSyncReadySemAdGroupSources( $objAdminDatabase ) {
		return CSemAdGroupSources::fetchSyncReadySemAdGroupSourcesBySemAccountIdBySemSourceId( $this->getId(), $this->getSemSourceId(), $objAdminDatabase );
	}

	public function fetchSyncReadySemKeywordSources( $objAdminDatabase ) {
		return CSemKeywordSources::fetchSyncReadySemKeywordSourcesBySemAccountIdBySemSourceId( $this->getId(), $this->getSemSourceId(), $objAdminDatabase );
	}

	public function fetchSyncReadySemAdSources( $objAdminDatabase ) {
		return CSemAdSources::fetchSyncReadySemAdSourcesBySemAccountIdBySemSourceId( $this->getId(), $this->getSemSourceId(), $objAdminDatabase );
	}

}
?>