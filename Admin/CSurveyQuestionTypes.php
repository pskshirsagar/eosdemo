<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyQuestionTypes
 * Do not add any new functions to this class.
 */

class CSurveyQuestionTypes extends CBaseSurveyQuestionTypes {

	public static function fetchSurveyQuestionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSurveyQuestionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSurveyQuestionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSurveyQuestionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedSurveyQuestionTypes( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						survey_question_types
					WHERE
						is_published = 1';

		return self::fetchSurveyQuestionTypes( $strSql, $objDatabase );
	}

	public static function fetchJobPostingQuestionTypesByIds( $arrintJobPostingQuestionTypeIds, $objDatabase ) {
		if( false == valArr( $arrintJobPostingQuestionTypeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						survey_question_types
					WHERE
						id IN (' . implode( ',', $arrintJobPostingQuestionTypeIds ) . ')';

		return parent::fetchSurveyQuestionTypes( $strSql, $objDatabase );
	}
}
?>