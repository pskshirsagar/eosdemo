<?php

class CIlsServiceRestriction extends CBaseIlsServiceRestriction {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInternetListingServiceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIlsServiceRestrictionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>