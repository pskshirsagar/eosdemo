<?php

class CMigrationWorkbookStatusType extends CBaseMigrationWorkbookStatusType {

	const NEW_WORKBOOK							= 1;
	const IN_PROGRESS							= 2;
	const COMPLETED								= 6;
	const REJECTED								= 7;
	const ARCHIVED								= 8;
	const WAITING_ON_PRE_MIGRATION				= 9;
	const READY_FOR_MIGRATION					= 10;
	const READY_FOR_VERIFICATION				= 11;
	const READY_FOR_CORE_CONSULTANT				= 12;
	const REJECTION_IN_PRE_MIGRATION_ACCOUNTING	= 13;
	const WAITING_ON_CLIENT_APPROVAL			= 14;
	const NOT_STARTED							= 15;
	const SENT_TO_CLIENT						= 16;
	const DATA_IMPORT_COMPLETED					= 17;
	const VALIDATION_COMPLETED					= 18;
	const READY_FOR_SIGNING						= 19;
	const FLAGGED_VALIDATION					= 20;
	const UPDATED								= 21;

	public static $c_arrintMigrationWorkbookListingStatusTypes = [
		self::IN_PROGRESS,
		self::READY_FOR_MIGRATION,
		self::WAITING_ON_CLIENT_APPROVAL,
		self::COMPLETED,
		self::REJECTED
	];

	public static $c_arrintMigrationDocumentStatusTypes = [
		self::WAITING_ON_CLIENT_APPROVAL,
		self::COMPLETED,
		self::REJECTED
	];

	public static $c_arrstrMigrationWorkbookStatusTypes	= [
		self::NEW_WORKBOOK							=> 'New',
		self::IN_PROGRESS							=> 'In Progress',
		self::WAITING_ON_PRE_MIGRATION				=> 'Waiting On Pre-Migration',
		self::READY_FOR_MIGRATION					=> 'Ready For Migration',
		self::REJECTION_IN_PRE_MIGRATION_ACCOUNTING	=> 'Rejection In Pre-Migration Accounting',
		self::READY_FOR_VERIFICATION				=> 'Ready For Verification',
		self::READY_FOR_CORE_CONSULTANT				=> 'Ready For Core Consultant',
		self::WAITING_ON_CLIENT_APPROVAL			=> 'Waiting On Client Approval',
		self::COMPLETED								=> 'Completed',
		self::REJECTED								=> 'Rejected',
		self::ARCHIVED								=> 'Archived',
		self::NOT_STARTED							=> 'Not Started',
		self::SENT_TO_CLIENT						=> 'Sent To Client',
		self::DATA_IMPORT_COMPLETED					=> 'Data Import Completed',
		self::VALIDATION_COMPLETED					=> 'Validation Completed',
		self::READY_FOR_SIGNING						=> 'Ready for Signing',
		self::FLAGGED_VALIDATION					=> 'Flagged Validation',
		self::UPDATED								=> 'Workbook Updated'
	];

	//  access of post migration validation before pre migration accounting
	public static $c_arrintValidatePostMigration	= [
		self::READY_FOR_MIGRATION
	];

	// hide pre rejection button in post migration signature process
	public static $c_arrintValidatePreRejectionInPostSignature	= [
		self::WAITING_ON_CLIENT_APPROVAL
	];

	public static $c_arrintShowSignatureTabToClient	= [
		self::READY_FOR_MIGRATION,
		self::WAITING_ON_CLIENT_APPROVAL,
		self::COMPLETED,
		self::REJECTED,
		self::IN_PROGRESS
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>