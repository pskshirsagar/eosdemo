<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTypes
 * Do not add any new functions to this class.
 */

class CSurveyTypes extends CBaseSurveyTypes {

	public static function fetchSurveyTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSurveyType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchSurveyType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSurveyType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedSurveyTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						survey_types
					WHERE
						is_published = 1
					ORDER BY
						name ASC';

		return self::fetchSurveyTypes( $strSql, $objDatabase );
	}

	public static function fetchAllSurveyTypesForSearch( $arrintSurveyTypes, $objDatabase ) {

		if( false == valArr( $arrintSurveyTypes ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						survey_types
					WHERE
						id IN( ' . implode( ',', $arrintSurveyTypes ) . ' )';

		return self::fetchCachedObjects( $strSql, 'CSurveyType', $objDatabase, DATA_CACHE_MEMORY );
	}
}
?>