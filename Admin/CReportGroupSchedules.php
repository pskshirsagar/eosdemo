<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportGroupSchedules
 * Do not add any new functions to this class.
 */

class CReportGroupSchedules extends CBaseReportGroupSchedules {

	public static function fetchScheduledGroupsDetails( $arrmixScheduledGroupFilter, $intUserId, $objPagination, $objDatabase, $boolFetchSingleRecord = false ) {

		$strWhereJoinClause = '';
		$strWhereClause = ' g.deleted_by IS NULL AND g.group_type_id = ' . CGroupType::REPORT;

		if( true == is_numeric( $intUserId ) ) {
			$strWhereClause .= ' AND g.created_by = ' . $intUserId;
		}

		if( false == $boolFetchSingleRecord ) {
			switch( $arrmixScheduledGroupFilter['order_by_field'] ) {
				case 'group_name':
					$arrmixScheduledGroupFilter['order_by_field'] = ' gd.group_name';
					break;

				case 'scheduled_date':
					$arrmixScheduledGroupFilter['order_by_field'] = ' gd.scheduled_date';
					break;

				default:
					$arrmixScheduledGroupFilter['order_by_field'] = ' sort_col';
					break;
			}
		} else {
			$strWhereJoinClause .= ' AND g.id = ' . $arrmixScheduledGroupFilter['group_id'];
		}

		$strSql = ' WITH group_details AS (
					SELECT 
						g.id,
						g.name AS group_name,
						g.created_on,
						rgs.schedule_date AS scheduled_date,
						rst.name AS group_frequency,
						COUNT( g.id ) OVER( PARTITION BY g.group_type_id ) AS total_records
					FROM
						groups AS g
						JOIN report_group_schedules AS rgs ON ( g.id = rgs.group_id ' . $strWhereJoinClause . ')
						JOIN group_schedule_types AS rst ON ( rgs.group_schedule_type_id = rst.id )
					WHERE
						' . $strWhereClause . '
					GROUP BY
						g.id,
						rgs.schedule_date,
						rst.name
						)

						SELECT 
							gd.*,
							json_agg( json_build_object( \'report_name\', cr.name, \'report_id\', cr.id ) ) AS report_details,
							( CASE WHEN string_agg( cr.name, \'\' ) IS NULL THEN 0 ELSE 1 END ) AS sort_col
						FROM 
							group_details AS gd
							LEFT JOIN report_group_associations AS rga ON ( gd.id = rga.group_id AND rga.deleted_by IS NULL)
							LEFT JOIN company_reports AS cr ON ( rga.company_report_id = cr.id AND cr.is_published = true AND cr.deleted_by IS NULL )
						GROUP BY 
							gd.id,
							gd.group_name,
							gd.scheduled_date,
							gd.group_frequency,
							gd.total_records,
							gd.created_on';

		if( false == $boolFetchSingleRecord ) {
			$strOffsetLimit = '
								ORDER BY
										' . $arrmixScheduledGroupFilter['order_by_field'] . ' ' . $arrmixScheduledGroupFilter['order_by_type'] . '
								OFFSET
										' . ( int ) $objPagination->getOffset() . '
								LIMIT
										' . ( int ) $objPagination->getPageSize();

			$strSql .= $strOffsetLimit;

			return fetchData( $strSql, $objDatabase );
		} else {
			$arrmixResult = fetchData( $strSql, $objDatabase );
			if( true == valArr( $arrmixResult ) ) {
				return $arrmixResult[key( $arrmixResult )];
			} else {
				return false;
			}
		}
	}

	public static function fetchGroupReportDetailsByGroupId( $intGroupId, $objDatabase ) {

		if( false == is_numeric( $intGroupId ) ) {
			return 0;
		}

		$strSql = '	SELECT 
						rgs.group_id,
						cr.id,
						cr.name,
						cr.description,
						cr.is_published,
						array_to_string( array_agg( t.name ), \',\' ) AS tag_names,
						array_to_string( array_agg( DISTINCT t.name ),\',\' ) AS tag_names,
						array_to_string( array_agg( DISTINCT d.name ),\',\' ) AS department_names,
						( SELECT id FROM report_group_associations AS rga1 WHERE rga1.company_report_id = cr.id AND rga1.group_id =rgs.group_id AND rga1.deleted_by IS NULL LIMIT 1 ) AS report_group_association_id
					FROM
						report_group_schedules AS rgs
						JOIN report_group_associations AS rga ON ( rgs.group_id = rga.group_id  AND rga.deleted_by IS NULL AND rgs.group_id = ' . ( int ) $intGroupId . ' )
						JOIN company_reports AS cr ON ( rga.company_report_id = cr.id )
						LEFT JOIN tags_associations AS ta ON ( cr.id = ta.company_report_id )
						LEFT JOIN tags AS t ON ( ta.tag_id = t.id AND t.is_published = 1 )
						LEFT JOIN report_department_associations AS rda ON ( cr.id = rda.company_report_id )
						LEFT JOIN departments AS d ON ( rda.department_id = d.id AND d.is_published = 1 )
					GROUP BY
						rgs.group_id,
						cr.id,
						cr.name,
						cr.description';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportGroupScheduleByGroupId( $intGroupId, $objDatabase ) {

		if( false == is_numeric( $intGroupId ) ) {
			return 0;
		}

		return self::fetchReportGroupSchedule( sprintf( 'SELECT * FROM report_group_schedules WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

}

?>