<?php

class CEmployeeGoalType extends CBaseEmployeeGoalType {

	const QA			= 1;
	const OCCURRENCES	= 2;
	const ADHERENCE		= 3;
	const WRAP_UP		= 4;

	public static $c_arrintEmployeeGoalTypes = [
		self::QA,
		self::OCCURRENCES,
		self::ADHERENCE,
		self::WRAP_UP
	];

	public static $c_arrstrEmployeeGoalTypeNames = [
		self::QA			=> 'QA',
		self::OCCURRENCES	=> 'Occurrence',
		self::ADHERENCE		=> 'Adherence',
		self::WRAP_UP		=> 'Wrap Up'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>