<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CActionTypes
 * Do not add any new functions to this class.
 */

class CActionTypes extends CBaseActionTypes {

	public static function fetchActionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CActionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchActionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CActionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllActionTypes( $objDatabase, $arrintExcludeActionTypes = NULL ) {

		$strWhereClause = ( true == valArr( $arrintExcludeActionTypes ) ) ? ' WHERE id NOT IN ( ' . implode( ',', $arrintExcludeActionTypes ) . ' )' : '';

		$strSql = ' SELECT
						*
					FROM
						action_types
					' . $strWhereClause . '
					ORDER BY
						order_num';

		return self::fetchCachedObjects( $strSql, 'CActionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>