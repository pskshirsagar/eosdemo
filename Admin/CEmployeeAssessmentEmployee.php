<?php

class CEmployeeAssessmentEmployee extends CBaseEmployeeAssessmentEmployee {

	protected $m_strEmployeeAssessmentDateTime;
	protected $m_strEmployeeAssessmentCompletedOn;
	protected $m_strCurrentTitle;
	protected $m_intEmployeeAssessmentTypeLevelId;
	protected $m_intEmployeeAssessmentId;
	protected $m_strNameFull;
	protected $m_strEmployeeAnswer;
	protected $m_strManagerAnswer;
	protected $m_intUserId;
	protected $m_intEmployeeId;
	protected $m_intTrainingTrainerAssociationId;
	protected $m_boolIsVerificationRequired;
	protected $m_strEmailAddress;

	/**
	 * GET Functions
	 */

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getEmployeeAssessmentDateTime() {
		return $this->m_strEmployeeAssessmentDateTime;
	}

	public function getEmployeeAssessmentId() {
		return $this->m_intEmployeeAssessmentId;
	}

	public function getEmployeeAssessmentCompletedOn() {
		return $this->m_strEmployeeAssessmentCompletedOn;
	}

	public function getEmployeeAssessmentTypeLevelId() {
		return $this->m_intEmployeeAssessmentTypeLevelId;
	}

	public function getCurrentTitle() {
		return $this->m_strCurrentTitle;
	}

	public function getEmployeeAnswer() {
		return $this->m_strEmployeeAnswer;
	}

	public function getManagerAnswer() {
		return $this->m_strManagerAnswer;
	}

	public function getDelayedDays() {
		return $this->m_intDelayedDays;
	}

	public function getIsVerificationRequired() {
		return $this->m_boolIsVerificationRequired;
	}

	public function getTrainingTrainerAssociationId() {
		return $this->m_intTrainingTrainerAssociationId;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	/**
	 * SET Functions
	 */

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setEmployeeAssessmentId( $intEmployeeAssessmentId ) {
		$this->m_intEmployeeAssessmentId = $intEmployeeAssessmentId;
	}

	public function setEmployeeAnswer( $strEmployeeAnswer ) {
		$this->m_strEmployeeAnswer = $strEmployeeAnswer;
	}

	public function setManagerAnswer( $strManagerAnswer ) {
		$this->m_strManagerAnswer = $strManagerAnswer;
	}

	public function setTrainingTrainerAssociationId( $intTrainingTrainerAssociationId ) {
		$this->m_intTrainingTrainerAssociationId = $intTrainingTrainerAssociationId;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrValues['assessment_datetime'] ) )				$this->setEmployeeAssessmentDateTime( $arrValues['assessment_datetime'] );
		if( true == isset( $arrValues['completed_on'] ) ) 						$this->setEmployeeAssessmentCompletedOn( $arrValues['completed_on'] );
		if( true == isset( $arrValues['employee_assessment_type_level_id'] ) ) 	$this->setEmployeeAssessmentTypeLevelId( $arrValues['employee_assessment_type_level_id'] );
		if( true == isset( $arrValues['current_title'] ) ) 						$this->setCurrentTitle( $arrValues['current_title'] );
		if( true == isset( $arrValues['delayed_days'] ) ) 						$this->setDelayedDays( $arrValues['delayed_days'] );
		if( true == isset( $arrValues['name_full'] ) ) 							$this->setNameFull( $arrValues['name_full'] );
		if( true == isset( $arrValues['user_id'] ) ) 							$this->setUserId( $arrValues['user_id'] );
		if( true == isset( $arrValues['employee_assessment_id'] ) ) 			$this->setEmployeeAssessmentId( $arrValues['employee_assessment_id'] );
		if( true == isset( $arrValues['employee_answer'] ) ) 					$this->setEmployeeAnswer( $arrValues['employee_answer'] );
		if( true == isset( $arrValues['manager_answer'] ) ) 					$this->setManagerAnswer( $arrValues['manager_answer'] );
		if( true == isset( $arrValues['employee_id'] ) ) 						$this->setEmployeeId( $arrValues['employee_id'] );
		if( true == isset( $arrValues['training_trainer_association_id'] ) ) 	$this->setTrainingTrainerAssociationId( $arrValues['training_trainer_association_id'] );
		if( true == isset( $arrValues['is_verification_required'] ) ) 			$this->setIsVerificationRequired( $arrValues['is_verification_required'] );
		return;
	}

	public function setEmployeeAssessmentDateTime( $strEmployeeAssessmentDateTime ) {
		$this->m_strEmployeeAssessmentDateTime = $strEmployeeAssessmentDateTime;
	}

	public function setEmployeeAssessmentCompletedOn( $strEmployeeAssessmentCompletedOn ) {
		$this->m_strEmployeeAssessmentCompletedOn = $strEmployeeAssessmentCompletedOn;
	}

	public function setEmployeeAssessmentTypeLevelId( $intEmployeeAssessmentTypeLevelId ) {
		$this->m_intEmployeeAssessmentTypeLevelId = $intEmployeeAssessmentTypeLevelId;
	}

	public function setCurrentTitle( $strCurrentTitle ) {
		$this->m_strCurrentTitle = $strCurrentTitle;
	}

	public function setDelayedDays( $intDelayedDays ) {
		$this->m_intDelayedDays = $intDelayedDays;
	}

	public function setIsVerificationRequired( $boolIsVerificationRequired ) {
		$this->m_boolIsVerificationRequired = $boolIsVerificationRequired;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchEmployeeAssessmentResponses( $objDatabase ) {
	 	return \Psi\Eos\Admin\CEmployeeAssessmentResponses::createService()->fetchEmployeeAssessmentResponsesByEmployeeAssessmentEmployeeId( $this->getId(), $objDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function valEmployeeId( $intReviewersCount, $arrintInvalidReviewerIds, $objDatabase ) {

	 	$boolIsValid = true;

	 	if( false == is_null( $arrintInvalidReviewerIds ) ) {
	 		foreach( $arrintInvalidReviewerIds as $intReviewerId ) {

	 			$objEmployee = CEmployees::fetchEmployeeById( $intReviewerId, $objDatabase );

	 			$boolIsValid = $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ids', ' No reviewers found for ' . $objEmployee->getNameFull() . ' . Please check managers and reviewers designation/department details.' ) );
	 		}
	 	}

		return $boolIsValid;
	}

   public function validate( $strAction, $intReviewersCount = NULL, $arrintInvalidReviewerIds = NULL, $objDatabase = NULL ) {
		 $boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid 	&= $this->valEmployeeId( $intReviewersCount, $arrintInvalidReviewerIds, $objDatabase );
			 	break;

			case VALIDATE_DELETE:
				break;

		   	default:
		   		// default case
		   		$boolIsValid = true;
		   		break;
		}

		return $boolIsValid;
   }

}
?>