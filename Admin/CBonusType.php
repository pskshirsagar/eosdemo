<?php

class CBonusType extends CBaseBonusType {

	const CASH				= 1;
	const GIFT_VOUCHER		= 2;
	const PAID_DAY_OFF		= 3;
	const OTHERS			= 4;

	public function validate( $strAction ) {
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
		}

		return $boolIsValid;
	}

}
?>