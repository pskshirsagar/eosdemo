<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplateOptions
 * Do not add any new functions to this class.
 */

class CSurveyTemplateOptions extends CBaseSurveyTemplateOptions {

	public static function fetchSurveyTemplateOptionsBySurveyTemplateIds( $arrintSurveyTemplateIds, $objDatabase, $arrintSurveyIds = NULL ) {

		if( false == valArr( $arrintSurveyTemplateIds ) ) return NULL;

		$strJoin = $strWhere = '';

		if( true == valArr( $arrintSurveyIds ) ) {
			$strJoin 	= ' JOIN surveys s ON ( s.survey_template_id = stqg.survey_template_id )';
			$strWhere 	= ' AND s.id IN ( ' . implode( ',', $arrintSurveyIds ) . ' )';
		}

		$strSql = 'SELECT
						 sto.*
					FROM
						survey_template_options sto
						JOIN  survey_template_question_groups stqg ON ( stqg.survey_template_id = sto.survey_template_id)
						' . $strJoin . '
					WHERE
						stqg.survey_template_id IN (' . implode( ',', $arrintSurveyTemplateIds ) . ' ) ' .
						$strWhere . '
					ORDER BY
						sto.id ASC';

		return self::fetchSurveyTemplateOptions( $strSql, $objDatabase );
	}

	public static function fetchSurveyTemplateOptionsBySurveyTemplateQuestionIds( $arrintSurveyTemplateQuestionIds, $objDatabase ) {

		if( false == valArr( $arrintSurveyTemplateQuestionIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						survey_template_options
					WHERE
						survey_template_question_id IN (' . implode( ',', $arrintSurveyTemplateQuestionIds ) . ')
						ORDER BY
						id ASC';

		return self::fetchSurveyTemplateOptions( $strSql, $objDatabase );
	}

	public static function fetchSurveyTemplateOptionsBySurveyTemplateQuestionId( $intSurveyTemplateQuestionId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						survey_template_options
					WHERE
						survey_template_question_id = ' . ( int ) $intSurveyTemplateQuestionId . '
						AND is_published = 1
						AND deleted_on IS NULL
					ORDER BY
						id ASC';

		return self::fetchSurveyTemplateOptions( $strSql, $objDatabase );
	}

	public static function fetchSurveyTemplateOptionsBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {

		$strSql = 'SELECT
							*
					FROM
						survey_template_options
					WHERE
						survey_template_id = ' . ( int ) $intSurveyTemplateId . '
					ORDER BY
							order_num';

		return self::fetchSurveyTemplateOptions( $strSql, $objDatabase );
	}
}
?>