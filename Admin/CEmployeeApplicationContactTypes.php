<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationContactTypes
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationContactTypes extends CBaseEmployeeApplicationContactTypes {

	public static function fetchEmployeeApplicationContactTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmployeeApplicationContactType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchEmployeeApplicationContactType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEmployeeApplicationContactType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedEmployeeApplicationContactTypes( $objDatabase ) {
		$strSql = 'SELECT *	FROM employee_application_contact_types WHERE is_published = 1';
		return self::fetchEmployeeApplicationContactTypes( $strSql, $objDatabase );
	}
}
?>