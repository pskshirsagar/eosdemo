<?php

class CMassEmailType extends CBaseMassEmailType {

	const MONTHLY_NEWSLETTER		= 1;
	const SCHEDULED_DOWN_TIME		= 2;
	const NEW_PRODUCT_ANNOUNCEMENT	= 3;
	const OTHER						= 4;
	const SALES_EMAIL				= 5;
	const PEEP_EMAILS				= 6;
	const SALES_CRM					= 7;
	const PRODUCT_UPDATE_EMAIL		= 8;
	const INTERNAL_NEWSLETTER		= 9;
	const PRODUCT_PROMOTION			= 10;
	const CUSTOM					= 11;

	public static $c_arrintActiveMassEmailTypeIds = [ self::MONTHLY_NEWSLETTER, self::SCHEDULED_DOWN_TIME, self::NEW_PRODUCT_ANNOUNCEMENT, self::OTHER, self::SALES_EMAIL, self::PRODUCT_UPDATE_EMAIL, self::INTERNAL_NEWSLETTER, self::PRODUCT_PROMOTION ];

	public static $c_arrstrStaticMassEmailFilterNames = [
		'units',
		'employee_status',
		'primary_contacts',
		'is_managers',
		'country'
	];

	public static $c_arrstrTemplateTypeNames = [
		self::MONTHLY_NEWSLETTER       => 'newsletter',
		self::SCHEDULED_DOWN_TIME      => 'system_maintenance',
		self::NEW_PRODUCT_ANNOUNCEMENT => 'product_announcement',
		self::OTHER                    => 'other',
		self::SALES_EMAIL              => 'sales_email',
		self::PEEP_EMAILS              => 'other',
		self::SALES_CRM                => 'sales_crm',
		self::PRODUCT_UPDATE_EMAIL     => 'product_update_email',
		self::INTERNAL_NEWSLETTER      => 'internal_newsletter',
		self::PRODUCT_PROMOTION        => 'product_promotion',
		self::CUSTOM                   => 'custom'
	];

}
?>