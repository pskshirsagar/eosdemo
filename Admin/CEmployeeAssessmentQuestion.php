<?php

class CEmployeeAssessmentQuestion extends CBaseEmployeeAssessmentQuestion {

	protected $m_objEmployeeAssessmentRespons;

	const NOTES								= 'Notes';
	const NEW_NOTES							= 'Please enter additional any notes for managerial review.';
	const GOALS								= 'Goals';
	const NEW_GOALS							= 'What are your goals between now and your next review?';
	const NEW_MANAGER_GOALS					= 'Please enlist goals to be achieved by the Reviewee.';
	const ACCOMPLISHMENTS					= 'Accomplishments';
	const NEW_ACCOMPLISHMENTS				= 'What accomplishments have you achieved since your last review?';
	const NEW_MANAGER_ACCOMPLISHMENTS		= 'Please enlist the accomplishments of the Reviewee.';
	const TRAINING 							= 'Please recommend training needs.';
	const PRIVATE_NOTE 						= 'Private note.';
	const FINAL_MANAGER_NOTE_FOR_HR			= 'Final Manager Note for HR Team';
	const FINAL_MANAGER_NOTE_FOR_REVIEWEE	= 'Final Manager Note for Reviewee';
	const SIX_MONTH_DEFAULT_QUESTION    	= 'How likely are you to recommend Entrata as a place to work?';

	const EXIT_PROCESS_EMPLOYEE_QUESTION_ONE_ID		= 2341;
	const EXIT_PROCESS_EMPLOYEE_QUESTION_TWO_ID		= 2342;
	const EXIT_PROCESS_EMPLOYEE_QUESTION_THREE_ID	= 2343;
	const EXIT_PROCESS_MANAGER_QUESTION_ID			= 2344;
	const EXIT_PROCESS_DIRECTOR_QUESTION_ONE_ID		= 2345;
	const EXIT_PROCESS_DIRECTOR_QUESTION_TWO_ID		= 2346;
	const EXIT_PROCESS_SKILL_RATING_QUESTION_ID		= 3516;
	const EXIT_PROCESS_TYPE_IMMEDIATE_QUESTION_ID	= 3545;
	const EXIT_PROCESS_FINAL_COMMENT_QUESTION_ID	= 3738;
	const EXIT_PROCESS_IS_REHIRE_QUESTION_ID		= 3739;
	const EXIT_PROCESS_TYPE_STANDARD_QUESTION_ID	= 3737;
	const EXIT_PROCESS_ACCEPT_COMMENT_QUESTION_ID	= 4237;

	const RECOMMENDED_TRAINING_QUESTION_ID			= 3503;

	const EXIT_PROCESS_OTHER_SUB_REASON_ID			= 3957;

	const EXIT_PROCESS_HR_QUESTION_ID					= 4238;
	const EXIT_PROCESS_MANAGER_QUESTION_REHIRE			= 4239;
	const EXIT_PROCESS_MANAGER_QUESTION_POSITION_OPTION	= 4240;
	const EXIT_PROCESS_MANAGER_QUESTION_LEAVING_REASON	= 4241;
    const EXIT_PROCESS_MANAGER_QUESTION_LATE_EXIT_REASON = 4362;

	public static $c_arrintEmployeeAssessmentQuestionIds = [
		self::EXIT_PROCESS_MANAGER_QUESTION_LEAVING_REASON,
		self::EXIT_PROCESS_MANAGER_QUESTION_POSITION_OPTION,
		self::EXIT_PROCESS_MANAGER_QUESTION_REHIRE,
        self::EXIT_PROCESS_EMPLOYEE_QUESTION_ONE_ID,
        self::EXIT_PROCESS_EMPLOYEE_QUESTION_THREE_ID,
        self::EXIT_PROCESS_MANAGER_QUESTION_REHIRE,
        self::EXIT_PROCESS_MANAGER_QUESTION_LATE_EXIT_REASON,
        self::EXIT_PROCESS_TYPE_STANDARD_QUESTION_ID
    ];
 	/**
 	 * Create Functions
 	 */

	public function createEmployeeAssessmentQuestionDesignation() {

		$objEmployeeAssessmentQuestionDesignation = new CEmployeeAssessmentQuestionDesignation();

		$objEmployeeAssessmentQuestionDesignation->setEmployeeAssessmentQuestionId( $this->getId() );

		return $objEmployeeAssessmentQuestionDesignation;
	}

	public function createEmployeeAssessmentResponse( $objEmployeeAssessment, $intEmployeeAssessmentEmployeeId ) {

		$objEmployeeAssessmentResponse = new CEmployeeAssessmentResponse();

		$objEmployeeAssessmentResponse->setEmployeeAssessmentId( $objEmployeeAssessment->getId() );
		$objEmployeeAssessmentResponse->setEmployeeAssessmentQuestionId( $this->getId() );
		$objEmployeeAssessmentResponse->setEmployeeAssessmentEmployeeId( $intEmployeeAssessmentEmployeeId );
		$objEmployeeAssessmentResponse->setQuestion( $this->getQuestion() );
		$objEmployeeAssessmentResponse->setIsRating( $this->getIsRating() );
		$objEmployeeAssessmentResponse->setIsMandatory( $this->getIsMandatory() );

		return $objEmployeeAssessmentResponse;
	}

 	/**
	 * Get Functions
	 */

	public function getEmployeeAssessmentResponse() {
		return $this->m_objEmployeeAssessmentRespons;
	}

	/**
	* Set Functions
	*/

	public function setEmployeeAssessmentResponse( $objEmployeeAssessmentRespons ) {
		$this->m_objEmployeeAssessmentRespons = $objEmployeeAssessmentRespons;
	}

	/**
	* Validate Functions
	*/

	public function valQuestion( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getQuestion() ) || true == is_numeric( $this->getQuestion() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', 'Self review question is required and it should be valid.' ) );
		}

		if( true == is_null( $this->getManagerQuestion() ) || true == is_numeric( $this->getManagerQuestion() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', 'Manager question is required and it should be valid.' ) );
		}

		if( true == $boolIsValid ) {

 			$intConflictingQuestionCount		  = \Psi\Eos\Admin\CEmployeeAssessmentQuestions::createService()->fetchConflictingQuestionCountByQuestionNameByAssessmentTypeId( $this->getQuestion(), $this->getId(), $this->getEmployeeAssessmentTypeId(), $objDatabase );
 			$intConflictingManagerQuestionCount	  = \Psi\Eos\Admin\CEmployeeAssessmentQuestions::createService()->fetchConflictingManagerQuestionCountByManagerQuestionNameByAssessmentTypeId( $this->getManagerQuestion(), $this->getId(), $this->getEmployeeAssessmentTypeId(), $objDatabase );

			if( 0 < $intConflictingQuestionCount ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', 'Self review question already exists for the same Assessment Type.' ) );
			}

			if( 0 < $intConflictingManagerQuestionCount ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', 'Manager Question already exists for the same Assessment Type.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valQuestion( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

	return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
 		$this->setDeletedOn( 'NOW()' );

		return parent ::update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>