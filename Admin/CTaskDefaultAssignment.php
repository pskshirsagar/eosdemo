<?php

class CTaskDefaultAssignment extends CBaseTaskDefaultAssignment {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskPriorityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase,$boolReturnSqlOnly = false ) {
		$this->setDeletedOn( 'NOW()' );
		$this->setDeletedBy( $intCurrentUserId );

		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( 'NOW()' );
		$this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		return true;
	}

}
?>