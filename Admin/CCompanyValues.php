<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyValues
 * Do not add any new functions to this class.
 */

class CCompanyValues extends CBaseCompanyValues {

	public static function fetchAllCompanyValues( $objDatabase ) {
		return self::fetchCompanyValues( 'SELECT * FROM company_values', $objDatabase );
	}

}
?>