<?php

class CCertificationType extends CBaseCertificationType {

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Please enter certification name.' ) );
		}

		$arrobjCertificationTypes = CCertificationTypes::fetchAllCertificationTypes( $objDatabase );

		foreach( $arrobjCertificationTypes as $objCertificationType ) {
			if( true == valStr( $this->getName() ) && $this->getId() != $objCertificationType->getId() && \Psi\CStringService::singleton()->strtolower( $objCertificationType->getName() ) == \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'This certification type already exist.' ) );
				break;
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Please enter description.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( date( 'm/d/Y H:i:s' ) );

		return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>