<?php

class CIlsFeedTransmission extends CBaseIlsFeedTransmission {

	use TEosStoredObject;

	protected $m_strCompanyName;
	protected $m_strUpdatedFeedFileSize;
	protected $m_strIlsFeedTypeName;
	protected $m_strTitle;
	protected $m_strBatchName;
	protected $m_strName;

	/**
	 * Get Functions
	 *
	 */

	public function getUpdatedFeedFileSize() {
		return $this->m_strUpdatedFeedFileSize;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getTitle(){
		return $this->m_strTitle;
	}

	public function getBatchName(){
		return $this->m_strBatchName;
	}

	public function getName(){
		return $this->m_strName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['updated_feed_file_size'] ) ) $this->setUpdatedFeedFileSize( $arrmixValues['updated_feed_file_size'] );
		if( true == isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['ils_feed_type_name'] ) ) $this->setIlsFeedTypeName( $arrmixValues['ils_feed_type_name'] );
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setUpdatedFeedFileSize( $strFeedFileSize ) {
		$this->m_strUpdatedFeedFileSize = $strFeedFileSize;
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setBatchName( $strBatchName ) {
		$this->m_strBatchName = $strBatchName;
	}

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setIlsFeedTypeName( $strIlsFeedTypeName ) {
		$this->m_strIlsFeedTypeName = $strIlsFeedTypeName;
	}

	public function getIlsFeedTypeName() {
		return $this->m_strIlsFeedTypeName;
	}

}
?>