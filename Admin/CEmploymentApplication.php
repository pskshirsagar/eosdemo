<?php

class CEmploymentApplication extends CBaseEmploymentApplication {

	const GENERAL 			= 1;
	const TRAINING_TRAINER	= 5;

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchEmploymentApplicationQuestions( $objDatabase, $boolShowDeleted = false ) {
		return CEmploymentApplicationQuestions::fetchEmploymentApplicationQuestionsByEmploymentApplicationId( $this->getId(), $objDatabase, $boolShowDeleted );
	}

	public function fetchDepartment( $objDatabase ) {
		return CDepartments::fetchDepartmentById( $this->getDepartmentId(), $objDatabase );
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valName( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Employment Application name is required.' ) );
		} elseif( false == is_null( $objAdminDatabase ) ) {

			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$intCount = CEmploymentApplications::fetchEmploymentApplicationCount( ' WHERE  lower( name ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\'' . $strSqlCondition, $objAdminDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Employment Application name already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objAdminDatabase );
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>