<?php
use Psi\Eos\Admin\CPsJobPostingSteps;

class CEmployeeApplicationInterview extends CBaseEmployeeApplicationInterview {

	protected $m_intMailToCandidate;
	protected $m_intMailToInterviewer;
	protected $m_intFeedbackPendingDays;

	protected $m_strScheduleHour;
	protected $m_strInterviewHour;
	protected $m_strScheduleMinute;
	protected $m_strInterviewMinute;
	protected $m_strScheduleMeridian;
	protected $m_strInterviewerEmailAddress;
	protected $m_strRecruiterEmailAddress;
	protected $m_strInterviewerName;
	protected $m_strRecruiterName;
	protected $m_strCandidateName;
	protected $m_strEmployeeApplicationCountryCode;

	public static $c_arrstrBufferTimes = [
		'No buffer time',
		'15 minutes',
		'30 minutes',
		'45 minutes',
		'60 minutes'
	];

	/**
	 * Create function
	 *
	 */

	public function createEmployeeApplicationContact() {

		$objEmployeeApplicationContact = new CEmployeeApplicationContact();

		$objEmployeeApplicationContact->setEmployeeApplicationId( $this->getId() );
		$objEmployeeApplicationContact->setContactDatetime( date( 'm/d/Y H:i:s' ) );

		return $objEmployeeApplicationContact;
	}

	public function createEmployeeApplicationNote() {

		$objEmployeeApplicationNote = new CEmployeeApplicationNote();

		$objEmployeeApplicationNote->setEmployeeApplicationId( $this->getId() );
		$objEmployeeApplicationNote->setNoteDatetime( date( 'm/d/Y H:i:s' ) );

		return $objEmployeeApplicationNote;
	}

	/**
	 * Get function
	 *
	 */

	public function getMailToCandidate() {

		return $this->m_intMailToCandidate;
	}

	public function getMailToInterviewer() {

		return $this->m_intMailToInterviewer;
	}

	public function getScheduleHour() {

		return $this->m_strScheduleHour;
	}

	public function getInterviewHour() {

		return $this->m_strInterviewHour;
	}

	public function getScheduleMinute() {

		return $this->m_strScheduleMinute;
	}

	public function getInterviewMinute() {

		return $this->m_strInterviewMinute;
	}

	public function getScheduleMeridian() {
		return $this->m_strScheduleMeridian;
	}

	public function getInterviewerEmailAddress() {
		return $this->m_strInterviewerEmailAddress;
	}

	public function getInterviewerName() {
		return $this->m_strInterviewerName;
	}

	public function getRecruiterEmailAddress() {
		return $this->m_strRecruiterEmailAddress;
	}

	public function getRecruiterName() {
		return $this->m_strRecruiterName;
	}

	public function getCandidateName() {
		return $this->m_strCandidateName;
	}

	public function getFeedbackPendingDays() {
		return $this->m_intFeedbackPendingDays;
	}

	public function getEmployeeApplicationCountryCode() {
		return $this->m_strEmployeeApplicationCountryCode;
	}

	 /**
	 * Set function
	 *
	 */

	public function setMailToCandidate( $intMailToCandidate ) {

		$this->m_intMailToCandidate = $intMailToCandidate;
	}

	public function setMailToInterviewer( $intMailToInterviewer ) {

		$this->m_intMailToInterviewer = $intMailToInterviewer;
	}

	public function setScheduleHour( $strScheduleHour ) {

		$this->m_strScheduleHour = $strScheduleHour;
	}

	public function setIntrviewHour( $strInterviewHour ) {

		$this->m_strInterviewHour = $strInterviewHour;
	}

	public function setScheduleMinute( $strScheduleMinute ) {

		$this->m_strScheduleMinute = $strScheduleMinute;
	}

	public function setIntrviewMinute( $strInterviewMinute ) {

		$this->m_strInterviewMinute = $strInterviewMinute;
	}

	public function setScheduleMeridian( $strScheduleMeridian ) {

		$this->m_strScheduleMeridian = $strScheduleMeridian;
	}

	public function setInterviewerEmailAddress( $strInterviewerEmailAddress ) {
		$this->m_strInterviewerEmailAddress = $strInterviewerEmailAddress;
	}

	public function setInterviewerName( $strInterviewerName ) {
		$this->m_strInterviewerName = $strInterviewerName;
	}

	public function setRecruiterEmailAddress( $strRecruiterEmailAddress ) {
		$this->m_strRecruiterEmailAddress = $strRecruiterEmailAddress;
	}

	public function setRecruiterName( $strRecruiterName ) {
		$this->m_strRecruiterName = $strRecruiterName;
	}

	public function setCandidateName( $strCandidateName ) {
		$this->m_strCandidateName = $strCandidateName;
	}

	public function setFeedbackPendingDays( $intFeedbackPendingDays ) {
		$this->m_intFeedbackPendingDays = $intFeedbackPendingDays;
	}

	public function setEmployeeApplicationCountryCode( $strEmployeeApplicationCountryCode ) {
		$this->m_strEmployeeApplicationCountryCode = $strEmployeeApplicationCountryCode;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['mail_candidate'] ) )  $this->setMailToCandidate( trim( $arrmixValues['mail_candidate'] ) );
		if( isset( $arrmixValues['mail_interviewer'] ) ) $this->setMailToInterviewer( trim( $arrmixValues['mail_interviewer'] ) );
		if( isset( $arrmixValues['schedule_hour'] ) ) $this->setScheduleHour( trim( $arrmixValues['schedule_hour'] ) );
	   	if( isset( $arrmixValues['interview_hour'] ) ) $this->setIntrviewHour( trim( $arrmixValues['interview_hour'] ) );
	   	if( isset( $arrmixValues['schedule_minute'] ) ) $this->setScheduleMinute( trim( $arrmixValues['schedule_minute'] ) );
	   	if( isset( $arrmixValues['interview_minute'] ) ) $this->setIntrviewMinute( trim( $arrmixValues['interview_minute'] ) );
		if( isset( $arrmixValues['schedule_meridian'] ) ) $this->setScheduleMeridian( trim( $arrmixValues['schedule_meridian'] ) );
		if( isset( $arrmixValues['interviewer_email_address'] ) ) $this->setInterviewerEmailAddress( trim( $arrmixValues['interviewer_email_address'] ) );
		if( isset( $arrmixValues['interviewer_name'] ) ) $this->setInterviewerName( trim( $arrmixValues['interviewer_name'] ) );
		if( isset( $arrmixValues['recruiter_name'] ) ) $this->setRecruiterName( trim( $arrmixValues['recruiter_name'] ) );
		if( isset( $arrmixValues['recruiter_email_address'] ) ) $this->setRecruiterEmailAddress( trim( $arrmixValues['recruiter_email_address'] ) );
		if( isset( $arrmixValues['candidate_name'] ) ) $this->setCandidateName( trim( $arrmixValues['candidate_name'] ) );
		if( isset( $arrmixValues['feedback_pending_days'] ) ) $this->setFeedbackPendingDays( trim( $arrmixValues['feedback_pending_days'] ) );
		if( isset( $arrmixValues['employee_application_country_code'] ) ) $this->setEmployeeApplicationCountryCode( trim( $arrmixValues['employee_application_country_code'] ) );

		return;
	}

	/**
	 * Validate Function
	 *
	 */

	public function valInterviewTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getInterviewTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interview_type_id', 'Interview type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Interviewer is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFeedback() {
		$boolIsValid = true;

		if( true == is_null( $this->getFeedback() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'feedback', 'Feedback is required.' ) );
		}

		return $boolIsValid;
	}

	public function valScheduledOn() {
		$boolIsValid = true;

		if( true == is_null( \Psi\CStringService::singleton()->substr( $this->getScheduledOn(), 0, 11 ) ) || '' == \Psi\CStringService::singleton()->substr( $this->getScheduledOn(), 11, 8 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scheduled_on', ' Scheduled date/time is required.' ) );

		}

		return $boolIsValid;
	}

	public function valInterviewLocation() {
		$boolIsValid = true;

		if( true == is_null( $this->getInterviewLocation() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interview_location', ' Location is required.' ) );

		}

		return $boolIsValid;
	}

	public function valInterviewedComplete() {

		$strInterviewDate = $this->getInterviewedOn();
		$strFeedback = $this->getFeedback();

		$boolIsValid = true;

		if( false == is_null( $strInterviewDate ) ) {
			if( '' == \Psi\CStringService::singleton()->substr( $strInterviewDate, 11, 8 ) ) {
		 	$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interviewed_on', 'Interview date/time is required.' ) );
			}
		}

		if( ( false == is_null( $strInterviewDate ) && false == is_null( $this->getScheduledOn() ) ) && strtotime( $strInterviewDate ) < strtotime( $this->getScheduledOn() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interviewed_on', 'Interviewe date/time must be greater or equal to scheduled date.' ) );
		}

		if( false == empty( $strInterviewDate ) && true == empty( $strFeedback ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'feedback', 'Feedback is required.' ) );
		}

		return $boolIsValid;
	}

	public function valInterviewedOnDate() {

		$boolIsValid = true;

		if( ( false == is_null( $this->getInterviewedOn() ) && '' == \Psi\CStringService::singleton()->substr( $this->getInterviewedOn(), 11, 8 ) ) || true == is_null( $this->getInterviewedOn() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interviewed_on', 'Interviewed on date/time is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $strCountryCode = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_employee_application_interview':
				if( $this->getId() ) {
					$boolIsValid &= $this->valId();
				}
				$boolIsValid &= $this->valInterviewTypeId();
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valScheduledOn();
				$boolIsValid &= $this->valInterviewedComplete();
				break;

			case 'validate_interviewed_interview':
				$boolIsValid &= $this->valInterviewedOnDate();
				break;

			case 'validate_applicant_interview':
				if( CCountry::CODE_USA == $strCountryCode )
					$boolIsValid &= $this->valEmployeeId();

				$boolIsValid &= $this->valScheduledOn();
				$boolIsValid &= $this->valInterviewLocation();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function sendEmployeeApplicationInterviewEmailToInterviwer( $objEmployee, $objEmployeeApplication, $objInterviewType, $strScheduledDate = NULL, $objEmailDatabase ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$strUrlSuffix = ( 'development' == CONFIG_ENVIRONMENT ) ? 'http' : 'https';
		$strLinkUrl = $strUrlSuffix . '://' . basename( $_SERVER['HTTP_HOST'] );

		$strHtmlContent  = 'Hello ' . $objEmployee->getNameFirst() . ', <br/>';
		$strHtmlContent .= 'You are supposed to conduct an interview. <br/>';
		$strHtmlContent .= 'Following are the details of Interview : <br/><br/>';
		$strHtmlContent .= 'Candidate Name : ' . $objEmployeeApplication->getNameFull() . '<br/>';
		$strHtmlContent .= 'Interview Date : ' . $strScheduledDate . '<br/>';
		$strHtmlContent .= 'Interview Type : ' . $objInterviewType->getDescription() . '<br/><br/>';
		$strHtmlContent .= 'Thanks and Regards,<br/><br/>Xento Systems Pvt.Ltd <br/>';
		$strHtmlContent .= '* Email: <a href="' . CSystemEmail::XENTO_HRIS_LABELED_EMAIL_ADDRESS . '"> . ' . CSystemEmail::XENTO_HRIS_LABELED_EMAIL_ADDRESS . ' </a>| <a href="' . CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS . '"> . ' . CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS . ' </a> <br/> ((Office): 020-30481988';
		$strHtmlContent .= '+India Office: Upper Ground Floor, Tower -8,Magarpatta City, SEZ, Pune. <br/>';
		$strHtmlContent .= '+USA Office: 522 South 100 West, Provo, Utah 84601';

		$strSubject		 = 'Interview Schedule';

		$strFromEmailAddress = ( CCountry::CODE_INDIA == $objEmployee->getCountryCode() ) ? CSystemEmail::XENTO_HRIS_LABELED_EMAIL_ADDRESS : CSystemEmail::PROPERTYSOLUTIONS_HR_EMAIL_ADDRESS;

		if( false == is_null( $objEmployee->getEmailAddress() ) ) {
			$strToEmailAddress	 = $objEmployee->getEmailAddress();
		} elseif( false == is_null( $objEmployee->getGmailUsername() ) ) {
			$strToEmailAddress	 = $objEmployee->getGmailUsername();
		} else {
			return false;
		}

		$objSmarty 		   	= new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'content',	 $strHtmlContent );
		$objSmarty->assign( 'logo_url',  		( CCountry::CODE_INDIA == $objEmployee->getCountryCode() ) ? 'https://www.xento.com/images/xento-logo.png' : CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress, $intIsSelfDestruct = 0, $strFromEmailAddress, $strReplyToEmailAddress = $strFromEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchCachedSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		$boolEmailSent = true;

		if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
			$boolEmailSent = false;
		}

		return $boolEmailSent;
	}

	public function sendEmployeeApplicationInterviewEmailToCandidate( $objEmployeeApplication, $objInterviewType, $strScheduledDate = NULL, $objEmailDatabase ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$strUrlSuffix = ( 'development' == CONFIG_ENVIRONMENT ) ? 'http' : 'https';
		$strLinkUrl = $strUrlSuffix . '://' . basename( $_SERVER['HTTP_HOST'] );

		$strHtmlContent  = 'Hello ' . $objEmployeeApplication->getNameFirst() . ', <br/>';
		$strHtmlContent .= 'You have been appointed for interview. <br/>';
		$strHtmlContent .= 'Following are the details of Interview : <br/><br/>';
		$strHtmlContent .= 'Interview Date : ' . $strScheduledDate . '<br/>';
		$strHtmlContent .= 'Interview Type : ' . $objInterviewType->getName() . '<br/><br/>';
		$strHtmlContent .= 'Thanks and Regards,<br/><br/>Xento Systems Pvt.Ltd <br/>';
		$strHtmlContent .= '* Email: <a href="' . CSystemEmail::XENTO_HRIS_LABELED_EMAIL_ADDRESS . '"> . ' . CSystemEmail::XENTO_HRIS_LABELED_EMAIL_ADDRESS . ' </a>| <a href="' . CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS . '"> . ' . CSystemEmail::XENTO_CAREERS_EMAIL_ADDRESS . ' </a> <br/> ((Office): 020-30481988';
		$strHtmlContent .= '+India Office: Upper Ground Floor, Tower -8,Magarpatta City, SEZ, Pune. <br/>';
		$strHtmlContent .= '+USA Office: 522 South 100 West, Provo, Utah 84601';

		$strSubject		= 'Interview Schedule';

		$strFromEmailAddress = ( CCountry::CODE_INDIA == $objEmployeeApplication->getCountryCode() ) ? CSystemEmail::XENTO_HRIS_LABELED_EMAIL_ADDRESS : CSystemEmail::PROPERTYSOLUTIONS_HR_EMAIL_ADDRESS;

		if( true == is_null( $objEmployeeApplication->getEmailAddress() ) ) {
			return false;
		}

		$strToEmailAddress	= $objEmployeeApplication->getEmailAddress();

		$objSmarty			= new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

		$objSmarty->assign( 'content',				$strHtmlContent );
		$objSmarty->assign( 'logo_url',				( CCountry::CODE_INDIA == $objEmployeeApplication->getCountryCode() ) ? CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application/xento_logo.png' : CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );
		$objSmarty->assign( 'email_image_path',		CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application' );
		$objSmarty->assign( 'country_code',			$objEmployeeApplication->getCountryCode() );
		$objSmarty->assign( 'COUNTRY_CODE_INDIA',	CCountry::CODE_INDIA );

		$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::EMPLOYEE_APPLICATION_EMAIL, $strSubject, $strHtmlContent, $strToEmailAddress, $intIsSelfDestruct = 0, $strFromEmailAddress, $strReplyToEmailAddress = $strFromEmailAddress );

		$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

		$objSystemEmailType = CSystemEmailTypes::fetchCachedSystemEmailTypeById( $objSystemEmail->getSystemEmailTypeId(), $objEmailDatabase );

		if( true == valObj( $objSystemEmailType, 'CSystemEmailType' ) ) {
			$objSystemEmail->setEmailServiceProviderId( $objSystemEmailType->getEmailServiceProviderId() );
		}

		$boolEmailSent = true;

		if( false == $objSystemEmail->insert( $this->getUpdatedBy(), $objEmailDatabase ) ) {
			$boolEmailSent = false;
		}

		return $boolEmailSent;
	}

	public function updateEmployeeApplicationInterviewStatus( $objEmployeeApplication, $intPsJobPostingStepId, $intPsJobPostingStatusId, $objUser, $objAdminDatabase, $objEmailDatabase ) {
		$intRecruiterEmployeeId = NULL;
		$objPsJobPostingStepStatus	= CPsJobPostingStepStatuses::fetchPsJobPostingStepStatusByStepIdByStatusId( $intPsJobPostingStepId, $intPsJobPostingStatusId, $objAdminDatabase );
		$objEmployeeApplicationStepOld	= \Psi\Eos\Admin\CEmployeeApplicationSteps::createService()->fetchEmployeeApplicationStepByEmployeeApplicationIdByPsJobPostingStepId( $objEmployeeApplication->getId(), $intPsJobPostingStepId, $objAdminDatabase );

		if( false == valObj( $objPsJobPostingStepStatus, 'CPsJobPostingStepStatus' ) ) {
			echo json_encode( [ 'result' => 'error', 'message' => 'Fail to update job posting step status.' ] );
			exit;
		}

		if( CCountry::CODE_INDIA == $objEmployeeApplication->getCountryCode() ) {
			date_default_timezone_set( 'Asia/Calcutta' );
			$objFirstInterviewStep		= CPsJobPostingSteps::createService()->fetchPsJobPostingStepByPsJobPostingIdByJobStepName( $objEmployeeApplication->getPsWebsiteJobPostingId(), CPsJobPostingStep::FIRST_INTERVIEW, $objAdminDatabase );
			$objSecondInterviewStep		= CPsJobPostingSteps::createService()->fetchPsJobPostingStepByPsJobPostingIdByJobStepName( $objEmployeeApplication->getPsWebsiteJobPostingId(), CPsJobPostingStep::SECOND_INTERVIEW, $objAdminDatabase );
			$objHrInterviewStep			= CPsJobPostingSteps::createService()->fetchPsJobPostingStepByPsJobPostingIdByJobStepName( $objEmployeeApplication->getPsWebsiteJobPostingId(), CPsJobPostingStep::HR_INTERVIEW, $objAdminDatabase );
			$arrmixEmployeeApplicationInterviews	= rekeyArray( 'ps_job_posting_step_id', ( array ) CEmployeeApplicationInterviews::fetchInterviewTypeIdsByEmployeeApplicationId( $objEmployeeApplication->getId(), $objAdminDatabase ) );
			$intHrInterviewSurveyTemplateId = $intSecondInterviewSurveyTemplateId = NULL;

			if( true === valObj( $objSecondInterviewStep, 'CPsJobPostingStep' ) ) {
				$arrmixSecondInterview = ( true == valArr( $arrmixEmployeeApplicationInterviews ) ) ? $arrmixEmployeeApplicationInterviews[$objSecondInterviewStep->getId()] : NULL;
				$intSecondInterviewSurveyTemplateId = $objSecondInterviewStep->getSurveyTemplateId();

				if( true == valObj( $objFirstInterviewStep, 'CPsJobPostingStep' ) && $objFirstInterviewStep->getId() == $objPsJobPostingStepStatus->getPsJobPostingStepId() && CPsJobPostingStatus::SELECTED == $intPsJobPostingStatusId ) {
					$arrmixApplicationInterviews			= rekeyArray( 'id', \Psi\Eos\Admin\CEmployeeApplicationInterviews::createService()->fetchEmployeeApplicationInterviewsByEmployeeApplicationIdByJobPostingStepId( $objEmployeeApplication->getId(), $objSecondInterviewStep->getId(), $objAdminDatabase ) );
					$arrobjEmployeeApplicationInterviews	= CEmployeeApplicationInterviews::fetchEmployeeApplicationInterviewsByIds( array_keys( $arrmixApplicationInterviews ), $objAdminDatabase );

					if( true == valArr( $arrobjEmployeeApplicationInterviews ) && 'production' == CONFIG_ENVIRONMENT && 'Error' == $objEmployeeApplication->insertOrUpdateGoogleCalendarEvent( $arrobjEmployeeApplicationInterviews, $objUser, $objAdminDatabase, true ) ) {
						return NULL;
					}
				}
			}

			if( true === valObj( $objHrInterviewStep, 'CPsJobPostingStep' ) ) {
				$arrmixHrInterview = ( true == valArr( $arrmixEmployeeApplicationInterviews ) ) ? $arrmixEmployeeApplicationInterviews[$objHrInterviewStep->getId()] : NULL;
				$intHrInterviewSurveyTemplateId = $objHrInterviewStep->getSurveyTemplateId();
			}
		}

		if( CPsJobPostingStatus::REJECTED == $intPsJobPostingStatusId ) {
			$intRecruiterEmployeeId = $objEmployeeApplication->getRecruiterEmployeeId();
			$objEmployeeApplication->setRejectedOn( date( 'm/d/Y H:i:s' ) );
			$objEmployeeApplication->setRecruiterEmployeeId( NULL );

			if( CCountry::CODE_INDIA == $objEmployeeApplication->getCountryCode() ) {
				$arrintSurveyTemplateIds[]	= CSurveyTemplate::INTERVIEW_FEEDBACK_SURVEY;

				if( true == valObj( $objFirstInterviewStep, 'CPsJobPostingStep' ) && $objFirstInterviewStep->getId() == $objPsJobPostingStepStatus->getPsJobPostingStepId() ) {
					$objEmployeeApplication->sendSecondInterviewMail( $objFirstInterviewStep, $objSecondInterviewStep, $objUser, $objEmailDatabase, $objAdminDatabase );
					$arrstrSurveyDeletionStepNames	= [ CPsJobPostingStep::SECOND_INTERVIEW, CPsJobPostingStep::HR_INTERVIEW ];
					$arrintSurveyTemplateIds		= [ $intSecondInterviewSurveyTemplateId, $intHrInterviewSurveyTemplateId ];
				} elseif( true == valObj( $objSecondInterviewStep, 'CPsJobPostingStep' ) && $objSecondInterviewStep->getId() == $objPsJobPostingStepStatus->getPsJobPostingStepId() ) {
					$arrstrSurveyDeletionStepNames	= [ CPsJobPostingStep::HR_INTERVIEW ];
					$arrintSurveyTemplateIds[]		= $intHrInterviewSurveyTemplateId;
				}

				if( true == valArr( $arrstrSurveyDeletionStepNames ) ) {
					$arrobjSurveys = \Psi\Eos\Admin\CSurveys::createService()->fetchSurveysByEmployeeApplicationIdBySurveyTypeIdBySurveyTemplateIds( $objEmployeeApplication->getId(), [ CSurveyType::TRAINING, CSurveyType::EMPLOYEE_APPLICATION_INTERVIEW ], $arrintSurveyTemplateIds, $objAdminDatabase, $arrstrSurveyDeletionStepNames );

					if( true == valArr( $arrobjSurveys ) ) {
						foreach( $arrobjSurveys as $objSurvey ) {
							$objSurvey->setDeclinedDatetime( 'NOW()' );
						}
					}
				}
			}
		}

		$objEmployeeApplicationStep = new CEmployeeApplicationStep();
		$objEmployeeApplicationStep->setEmployeeApplicationId( $objEmployeeApplication->getId() );
		$objEmployeeApplicationStep->setPsJobPostingStepStatusId( $objPsJobPostingStepStatus->getId() );

		$arrmixPrAssociationDetails = $objEmployeeApplication->checkForPrAssociation( $objAdminDatabase );
		$objEmployeeApplicationStep->setPurchaseRequestId( $arrmixPrAssociationDetails['purchase_request_id'] );

		$objPsJobPostingStep = CPsJobPostingSteps::createService()->fetchPsJobPostingStepById( $intPsJobPostingStepId, $objAdminDatabase );
		if( false == valObj( $objPsJobPostingStep, 'CPsJobPostingStep' ) ) {
			$this->displayMessage( 'Error: ', 'Fail to update job posting step.', '/?module=recruitment_standard_dashboard-new&action=view_recruitment_tab' );
			exit;
		}

		$arrintEmployeeApplicationStatusIds = array( CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_BLACKLISTED, CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_NO_SHOW, CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_NOT_INTERESTED );
		$objEmployeeApplication->setPsJobPostingStepStatusId( $objPsJobPostingStepStatus->getId() );

		$arrstrStatusName		= CPsJobPostingStepStatuses::fetchPsJobPostingStepStatusNameById( $objEmployeeApplication->getPsJobPostingStepStatusId(), $objAdminDatabase );
		$strEmployeeStatusName	= $arrstrStatusName['current_step'] . '</br>( ' . $arrstrStatusName['current_status'] . ' )';

		if( true == valArr( $arrmixSecondInterview ) && CPsJobPostingStep::FIRST_INTERVIEW == $arrstrStatusName['current_step'] && CPsJobPostingStatus::SELECTED == $intPsJobPostingStatusId ) {
			if( true == valObj( $objEmployeeApplicationStepOld, 'CEmployeeApplicationStep' ) && CPsJobPostingStatus::SELECTED == $objEmployeeApplicationStepOld->getPsJobPostingStatusId() ) {
				$objPsJobPostingStepStatusNew	= CPsJobPostingStepStatuses::fetchPsJobPostingStepStatusByStepIdByStatusId( $intPsJobPostingStepId, CPsJobPostingStatus::SELECTED, $objAdminDatabase );
				$strEmployeeStatusName		= '1st Interview' . '</br>( Selected )';
			} else {
				$objPsJobPostingStepStatusNew = CPsJobPostingStepStatuses::fetchPsJobPostingStepStatusByStepIdByStatusId( $arrmixSecondInterview['ps_job_posting_step_id'], CPsJobPostingStatus::IN_PROCESS, $objAdminDatabase );
				$strEmployeeStatusName		= '2nd Interview' . '</br>( In Process )';
			}
		}

		if( true == in_array( $objPsJobPostingStep->getName(), [ CPsJobPostingStep::CCAT, CPsJobPostingStep::MRAB, CPsJobPostingStep::TECH_TEST, CPsJobPostingStep::FIRST_INTERVIEW ] ) && CCountry::CODE_INDIA == $objEmployeeApplication->getCountryCode() ) {
			$objEmployeeApplication->setRecruitmentDriveDetails( $objAdminDatabase, SYSTEM_USER_ID, $objEmployeeApplication->getPsJobPostingStepStatusId() );
		}

		if( true == valArr( $arrmixHrInterview ) && CPsJobPostingStep::SECOND_INTERVIEW == $arrstrStatusName['current_step'] && CPsJobPostingStatus::SELECTED == $intPsJobPostingStatusId ) {
			if( true == valObj( $objEmployeeApplicationStepOld, 'CEmployeeApplicationStep' ) && CPsJobPostingStatus::SELECTED == $objEmployeeApplicationStepOld->getPsJobPostingStatusId() ) {
				$objPsJobPostingStepStatusNew	= CPsJobPostingStepStatuses::fetchPsJobPostingStepStatusByStepIdByStatusId( $intPsJobPostingStepId, CPsJobPostingStatus::SELECTED, $objAdminDatabase );
				$strEmployeeStatusName		= '2nd Interview' . '</br>( Selected )';
			} else {
				$objPsJobPostingStepStatusNew = CPsJobPostingStepStatuses::fetchPsJobPostingStepStatusByStepIdByStatusId( $arrmixHrInterview['ps_job_posting_step_id'], CPsJobPostingStatus::IN_PROCESS, $objAdminDatabase );
				$strEmployeeStatusName		= 'HR Interview' . '</br>( In Process )';
			}
		}

		if( true == valObj( $objPsJobPostingStepStatusNew, 'CPsJobPostingStepStatus' ) && true == valStr( $strEmployeeStatusName ) ) {
			$objEmployeeApplicationStepNew = new CEmployeeApplicationStep();
			$objEmployeeApplicationStepNew->setEmployeeApplicationId( $objEmployeeApplication->getId() );
			$objEmployeeApplicationStepNew->setPsJobPostingStepStatusId( $objPsJobPostingStepStatusNew->getId() );
			$objEmployeeApplicationStepNew->setPurchaseRequestId( $arrmixPrAssociationDetails['purchase_request_id'] );
			$objEmployeeApplication->setPsJobPostingStepStatusId( $objPsJobPostingStepStatusNew->getId() );
		}

		if( true == in_array( $objEmployeeApplication->getEmployeeApplicationStatusTypeId(), $arrintEmployeeApplicationStatusIds ) && true == in_array( $intPsJobPostingStatusId, array( CPsJobPostingStatus::SELECTED, CPsJobPostingStatus::IN_PROCESS ) ) ) {
			$objEmployeeApplication->setEmployeeApplicationStatusTypeId( CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_NEW );
		}
		if( false == is_null( $objEmployeeApplication->getRejectedOn() ) && true == in_array( $intPsJobPostingStatusId, array( CPsJobPostingStatus::SELECTED, CPsJobPostingStatus::IN_PROCESS ) ) ) {
			$objEmployeeApplication->setRejectedOn( NULL );
		}

		switch( NULL ) {
			default:
				$boolValid = true;
				$boolValid &= $objEmployeeApplicationStep->validate( VALIDATE_INSERT, $objAdminDatabase );

				if( false == $boolValid || false == $objEmployeeApplicationStep->insert( $objUser->getId(), $objAdminDatabase ) ) {
					break;
				}

				if( true == valObj( $objEmployeeApplicationStepNew, 'CEmployeeApplicationStep' ) && false == $objEmployeeApplicationStepNew->insert( $objUser->getId(), $objAdminDatabase ) ) {
					break;
				}

				if( true == valArr( $arrobjSurveys ) && ( false == CSurveys::bulkUpdate( $arrobjSurveys, [ 'declined_datetime' ], $objUser->getId(), $objAdminDatabase ) ) ) {
					$objAdminDatabase->rollback();
					break;
				}

				$strActionDescription = '<strong>' . $objPsJobPostingStep->getName() . ' ' . CPsJobPostingStatus::$c_arrmixPsJobPostingStatuses[$intPsJobPostingStatusId]['name'] . '</strong> by <strong>' . $objUser->getEmployee()->getNameFull() . '</strong>';
				$strActionDescription = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strActionDescription, CONFIG_SODIUM_KEY_EMPLOYEE_NOTE );

				$strComments = ( true == valStr( $objEmployeeApplicationStep->getComments() ) ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $objEmployeeApplicationStep->getComments(), CONFIG_SODIUM_KEY_EMPLOYEE_NOTE ) : '';

				$objEmployeeApplication->insertEmployeeApplicationNote( $strComments, $objUser->getId(), $objAdminDatabase, $strActionDescription, $objEmployeeApplicationStep->getId(), $intActionTypeId = CActionType::NOTE, $intRecruiterEmployeeId );

				if( false == $objEmployeeApplication->update( $objUser->getId(), $objAdminDatabase ) ) {
					break;
				}

				return [ 'result' => true, 'status_name' => $strEmployeeStatusName ];
		}
		return [ 'result' => false, 'employee_application_step' => $objEmployeeApplicationStep ];
	}

	public static function loadAvailableInterviewersByDate( $strScheduleOnDate, $objAdminDatabase ) {
		$arrobjEmployees = CEmployees::fetchEmployeesByGroupId( CGroup::INTERVIEWERS, $objAdminDatabase );

		if( true == valArr( $arrobjEmployees ) ) {
			foreach( $arrobjEmployees as $objEmployee ) {
				$arrmixEmployees[$objEmployee->getId()] = [ 'id' => $objEmployee->getId(), 'preferred_name' => $objEmployee->getPreferredName() ];
			}
		}

		$arrobjEmployeeVacationRequests = rekeyObjects( 'VacationTypeId', CEmployeeVacationRequests::fetchEmployeeVacationRequestsByEmployeeIdsByDate( array_keys( $arrmixEmployees ), $strScheduleOnDate, false, $objAdminDatabase ), true );
		unset( $arrobjEmployeeVacationRequests[CVacationType::WEEKEND_WORKING], $arrobjEmployeeVacationRequests[CVacationType::WORK_FROM_HOME] );

		$arrintEmployeeOnLeaveIds = [];
		if( true == valArr( $arrobjEmployeeVacationRequests ) ) {
			foreach( $arrobjEmployeeVacationRequests as $arrobjEmployeeVacationRequest ) {
				$arrintEmployeeOnLeaveIds = array_merge( $arrintEmployeeOnLeaveIds, array_keys( ( array ) rekeyObjects( 'EmployeeId', $arrobjEmployeeVacationRequest ) ) );
			}
		}

		return array_diff_key( $arrmixEmployees, array_flip( $arrintEmployeeOnLeaveIds ) );
	}

}
?>