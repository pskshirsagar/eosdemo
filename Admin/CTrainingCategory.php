<?php

class CTrainingCategory extends CBaseTrainingCategory {

	protected $m_arrobjSubTrainingCategories;

	public function addTrainingCategory( $objTrainingCategory ) {
		$this->m_arrobjSubTrainingCategories[$objTrainingCategory->getId()] = $objTrainingCategory;
	}

	public function setSubTrainingCategories( $arrobjSubTrainingCategories ) {
		$this->m_arrobjSubTrainingCategories = $arrobjSubTrainingCategories;
	}

	public function getSubTrainingCategories() {
		return $this->m_arrobjSubTrainingCategories;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>