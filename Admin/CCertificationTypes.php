<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCertificationTypes
 * Do not add any new functions to this class.
 */

class CCertificationTypes extends CBaseCertificationTypes {

	public static function fetchAllCertificationTypes( $objDatabase ) {

		$strSql = 'SELECT * FROM certification_types';

		return parent::fetchCertificationTypes( $strSql, $objDatabase );
	}

	public static function fetchAllCertificationTypesOrderByName( $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						certification_types
					WHERE
						is_published = true
					ORDER BY
						name';

		return parent::fetchCertificationTypes( $strSql, $objDatabase );
	}

	public static function fetchAllCurrentCertificationTypes( $objDatabase, $intPageNo, $intPageSize, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset	 = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	 = ( int ) $intPageSize;

		$strSql = 'SELECT
						*
					FROM
						certification_types
					WHERE
						is_published = true
					ORDER BY
						' . $strOrderByField . ' ' . $strOrderByType . ' OFFSET ' . ( int ) $intOffset . '
					LIMIT
						' . $intLimit;

		return parent::fetchCertificationTypes( $strSql, $objDatabase );
	}

	public static function fetchPaginatedCurrentCertificationTypes( $objDatabase ) {

		$strSql = 'SELECT
						count (id)
					FROM
						certification_types
					WHERE
						is_published = true';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

}
?>