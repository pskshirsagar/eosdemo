<?php

class CAccountType extends CBaseAccountType {

	const PRIMARY           = 1;
	const INTERMEDIARY      = 2;
	const CLEARING          = 3;
	const VACANCY           = 4;
	const STANDARD          = 5;
	const INSURANCE_MARKEL  = 6;
	const INSURANCE         = 6;
	const UTILITY_BILLING   = 7;
	const INSURANCE_KEMPER  = 8;
	const MERCHANT_SWEEP    = 9;
	const INSURANCE_QBE     = 10;
	const SMALL_PROPERTIES  = 11;
	const MERCHANT          = 12;
	const COLLECTIONS       = 13;
	const INSURANCE_IDTHEFT = 14;
	const VENDOR_ACCESS     = 15;
	const MASTER_POLICY     = 16;

	public static $c_arrintInsuranceAccountTypeIds = [ CAccountType::INSURANCE_KEMPER, CAccountType::INSURANCE_MARKEL, CAccountType::INSURANCE_QBE, CAccountType::INSURANCE_IDTHEFT, CAccountType::INSURANCE, CAccountType::MASTER_POLICY ];
	public static $c_arrintExcludeBulkCheckAccountTypeIds = [ CAccountType::INSURANCE_KEMPER, CAccountType::INSURANCE_MARKEL, CAccountType::INSURANCE_QBE, CAccountType::INSURANCE_IDTHEFT ];
	public static $c_arrintAllowedAccountTypeIds = [ CAccountType::PRIMARY, CAccountType::STANDARD, CAccountType::MASTER_POLICY ];

	public static $c_arrintGeneralAccountTypeIds = [
		CAccountType::PRIMARY,
		CAccountType::INTERMEDIARY,
		CAccountType::CLEARING,
		CAccountType::VACANCY,
		CAccountType::STANDARD,
		CAccountType::UTILITY_BILLING,
		CAccountType::MERCHANT_SWEEP,
		CAccountType::MERCHANT

	];

	public static $c_arrintStatisticsAccountTypeIds = [
		CAccountType::PRIMARY,
		CAccountType::INTERMEDIARY,
		CAccountType::CLEARING,
		CAccountType::VACANCY,
		CAccountType::STANDARD,
		CAccountType::UTILITY_BILLING,
		CAccountType::MERCHANT_SWEEP

	];

	public static $c_arrintCCAccountTypeIds = [
		CAccountType::PRIMARY,
		CAccountType::INTERMEDIARY,
		CAccountType::CLEARING,
		CAccountType::VACANCY,
		CAccountType::STANDARD,
		CAccountType::MERCHANT_SWEEP,
		CAccountType::MERCHANT
	];

	public static $c_arrintRecurringChargeAccountTypeIds = [
		CAccountType::PRIMARY,
		CAccountType::STANDARD,
		CAccountType::UTILITY_BILLING

	];

	public static $c_arrintDelinquencyAccountTypeIds = [
		CAccountType::PRIMARY,
		CAccountType::STANDARD,
		CAccountType::MERCHANT,
		CAccountType::MASTER_POLICY
	];

	public static $c_arrintAdditionalAttachmentAccountTypeIds = [
		CAccountType::STANDARD,
		CAccountType::PRIMARY,
		CAccountType::MERCHANT,
		CAccountType::MASTER_POLICY
	];

	public static $c_arrintNetSuiteNotAllowedAccountIds = [
		CAccountType::INSURANCE_MARKEL,
		CAccountType::INSURANCE_KEMPER,
		CAccountType::INSURANCE_QBE,
		CAccountType::INSURANCE_IDTHEFT,
		CAccountType::MASTER_POLICY
	];
	/**
	 * If you are making any changes in populateSmartyConstants function then
	 * please make sure the same changes would be applied to populateTemplateConstants function also.
	 */

	public static function populateSmartyConstants( $objSmarty ) {
		$objSmarty->assign( 'ACCOUNT_TYPE_PRIMARY', self::PRIMARY );
		$objSmarty->assign( 'ACCOUNT_TYPE_INTERMEDIARY', self::INTERMEDIARY );
		$objSmarty->assign( 'ACCOUNT_TYPE_CLEARING', self::CLEARING );
		$objSmarty->assign( 'ACCOUNT_TYPE_VACANCY', self::VACANCY );
		$objSmarty->assign( 'ACCOUNT_TYPE_STANDARD', self::STANDARD );
		$objSmarty->assign( 'ACCOUNT_TYPE_INSURANCE_MARKEL', self::INSURANCE_MARKEL );
		$objSmarty->assign( 'ACCOUNT_TYPE_INSURANCE', self::INSURANCE );
		$objSmarty->assign( 'ACCOUNT_TYPE_UTILITY_BILLING', self::UTILITY_BILLING );
		$objSmarty->assign( 'ACCOUNT_TYPE_INSURANCE_KEMPER', self::INSURANCE_KEMPER );
		$objSmarty->assign( 'ACCOUNT_TYPE_MERCHANT_SWEEP', self::MERCHANT_SWEEP );
		$objSmarty->assign( 'ACCOUNT_TYPE_INSURANCE_QBE', self::INSURANCE_QBE );
		$objSmarty->assign( 'ACCOUNT_TYPE_SMALL_PROPERTIES', self::SMALL_PROPERTIES );
		$objSmarty->assign( 'ACCOUNT_TYPE_MERCHANT', self::MERCHANT );
		$objSmarty->assign( 'ACCOUNT_TYPE_INSURANCE_IDTHEFT', self::INSURANCE_IDTHEFT );
	}

	public static function populateTemplateConstants( $arrmixTemplateParameters ) {
		$arrmixTemplateParameters['ACCOUNT_TYPE_PRIMARY']           = self::PRIMARY;
		$arrmixTemplateParameters['ACCOUNT_TYPE_INTERMEDIARY']      = self::INTERMEDIARY;
		$arrmixTemplateParameters['ACCOUNT_TYPE_CLEARING']          = self::CLEARING;
		$arrmixTemplateParameters['ACCOUNT_TYPE_VACANCY']           = self::VACANCY;
		$arrmixTemplateParameters['ACCOUNT_TYPE_STANDARD']          = self::STANDARD;
		$arrmixTemplateParameters['ACCOUNT_TYPE_INSURANCE_MARKEL']  = self::INSURANCE_MARKEL;
		$arrmixTemplateParameters['ACCOUNT_TYPE_INSURANCE']         = self::INSURANCE;
		$arrmixTemplateParameters['ACCOUNT_TYPE_UTILITY_BILLING']   = self::UTILITY_BILLING;
		$arrmixTemplateParameters['ACCOUNT_TYPE_INSURANCE_KEMPER']  = self::INSURANCE_KEMPER;
		$arrmixTemplateParameters['ACCOUNT_TYPE_MERCHANT_SWEEP']    = self::MERCHANT_SWEEP;
		$arrmixTemplateParameters['ACCOUNT_TYPE_INSURANCE_QBE']     = self::INSURANCE_QBE;
		$arrmixTemplateParameters['ACCOUNT_TYPE_SMALL_PROPERTIES']  = self::SMALL_PROPERTIES;
		$arrmixTemplateParameters['ACCOUNT_TYPE_MERCHANT']          = self::MERCHANT;
		$arrmixTemplateParameters['ACCOUNT_TYPE_INSURANCE_IDTHEFT'] = self::INSURANCE_IDTHEFT;

		return $arrmixTemplateParameters;
	}

	public static function getAccountTypeNameByAccountTypeId( $intAccountTypeId ) {

		switch( $intAccountTypeId ) {
			case NULL:
				return NULL;
				break;

			case self::PRIMARY:
				return 'Primary';
				break;

			case self::INTERMEDIARY:
				return 'Intermediary';
				break;

			case self::CLEARING:
				return 'Clearing';
				break;

			case self::VACANCY:
				return 'Vacancy';
				break;

			case self::STANDARD:
				return 'Standard';
				break;

			case self::INSURANCE:
				return 'Insurance';
				break;

			case self::INSURANCE_MARKEL:
				return 'Insurance Markel';
				break;

			case self::UTILITY_BILLING:
				return 'Utility Billing';
				break;

			case self::INSURANCE_KEMPER:
				return 'Insurance Kemper';
				break;

			case self::MERCHANT_SWEEP:
				return 'Merchant Sweep';
				break;

			case self::INSURANCE_QBE:
				return 'Insurance QBE';
				break;

			case self::SMALL_PROPERTIES:
				return 'Small Properties';
				break;

			case self::MERCHANT:
				return 'Merchant';
				break;

			case self::INSURANCE_IDTHEFT:
				return 'Insurance ID Theft';
				break;

			default:
				return NULL;
				break;
		}
	}

	public static function getInsuranceAccountTypeIds() {
		return [ CAccountType::INSURANCE_MARKEL, CAccountType::INSURANCE_KEMPER, CAccountType::INSURANCE_QBE, CAccountType::INSURANCE_IDTHEFT ];
	}

	public static function getLateFeeChargeAccountTypeIds() {
		return [ CAccountType::PRIMARY, CAccountType::STANDARD, CAccountType::MERCHANT ];
	}

	public static function getAccountTypeIdsByExportBatchTypeId( $intExportBatchId ) {
		switch( $intExportBatchId ) {
			case CExportBatchType::PROPERTY_SOLUTIONS:
				$arrintAccountTypeIds = [
					CAccountType::PRIMARY,
					CAccountType::INTERMEDIARY,
					CAccountType::CLEARING,
					CAccountType::VACANCY,
					CAccountType::STANDARD,
					CAccountType::UTILITY_BILLING,
					CAccountType::VENDOR_ACCESS
				];
				break;

			case CExportBatchType::RESIDENT_INSURE_MARKEL:
				$arrintAccountTypeIds = [ CAccountType::INSURANCE_MARKEL ];
				break;

			case CExportBatchType::RESIDENT_INSURE_KEMPER:
				$arrintAccountTypeIds = [ CAccountType::INSURANCE_KEMPER ];
				break;

			case CExportBatchType::RESIDENT_INSURE_QBE:
				$arrintAccountTypeIds = [ CAccountType::INSURANCE_QBE ];
				break;

			case CExportBatchType::RESIDENT_INSURE_IDTHEFT:
				$arrintAccountTypeIds = [ CAccountType::INSURANCE_IDTHEFT ];
				break;

			case CExportBatchType::MASTER_POLICY:
				$arrintAccountTypeIds = [ CAccountType::MASTER_POLICY ];
				break;

			default:
				$arrintAccountTypeIds = [];
				break;
		}

		return $arrintAccountTypeIds;
	}

}

?>