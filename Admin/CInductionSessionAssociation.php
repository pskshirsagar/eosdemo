<?php

class CInductionSessionAssociation extends CBaseInductionSessionAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInductionSessionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDepartmentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTeamId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>