<?php

class CEmailTemplateSlot extends CBaseEmailTemplateSlot {

	protected $m_intMediaWidth;
	protected $m_intMediaHeight;
	protected $m_strAlt;

	/**
	 * SetValue Functions
	 */

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );

		if( false == is_null( $arrValues['media_width'] ) ) $this->setMediaWidth( $arrValues['media_width'] );
		if( false == is_null( $arrValues['media_height'] ) ) $this->setMediaHeight( $arrValues['media_height'] );

		return true;
	}

	/**
	 * Set Functions
	 */

	public function setMediaWidth( $intMediaWidth ) {
		$this->m_intMediaWidth = $intMediaWidth;
	}

	public function setMediaHeight( $intMediaHeight ) {
		$this->m_intMediaHeight = $intMediaHeight;
	}

	public function setAlt( $strAlt ) {
		$this->m_strAlt = $strAlt;
	}

	/**
	 * Get Functions
	 */

	public function getMediaWidth() {
		return $this->m_intMediaWidth;
	}

	public function getMediaHeight() {
		return $this->m_intMediaHeight;
	}

	public function getAlt() {
		return $this->m_strAlt;
	}

	/**
	 * Validate Functions
	 */

	public function valKey() {
		$boolIsValid = true;

		if( true == is_null( $this->getKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required.' ) );
		}
		return $boolIsValid;
	}

	public function valBgcolor() {
		$boolIsValid = true;

		if( true == is_null( $this->getBgcolor() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bgcolor', 'Background color is required.' ) );
		}
		return $boolIsValid;
	}

	public function valMediaWidth() {
		$boolIsValid = true;

		if( 0 == strlen( trim( $this->getMediaWidth() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'media_width', 'Media width is required.' ) );
		} elseif( false == is_null( $this->getMediaWidth() ) && false == preg_match( '/^\d+$/', $this->getMediaWidth() ) ) {
	   		$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'media_width', 'Media width should be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valMediaHeight() {
		$boolIsValid = true;

		if( 0 == strlen( trim( $this->getMediaHeight() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'media_height', 'Media height is required.' ) );
		} elseif( false == is_null( $this->getMediaHeight() ) && false == preg_match( '/^\d+$/', $this->getMediaHeight() ) ) {
	   		$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'media_width', 'Media height should be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valBgcolor();
				$boolIsValid &= $this->valMediaWidth();
				$boolIsValid &= $this->valMediaHeight();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>