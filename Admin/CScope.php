<?php

class CScope extends CBaseScope {

	protected $m_intAppId;

	const BASIC_USER_INFO 	= 10;
	const ILS_INFO 			= 45;
	const RV_SERVICES       = 47;

	/**
	 * Get Function
	 */

	public function getAppId() {
		return $this->m_intAppId;
	}

	/**
	 * Set Function
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['app_id'] ) )	$this->setAppId( $arrmixValues['app_id'] );
		return;
	}

	public function setName( $strName ) {
		$this->m_strName = addslashes( CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function setAppId( $intAppId ) {
		$this->m_intAppId = $intAppId;
	}

	/**
	 * Create Functions
	 */

	public function createScopeService() {

		$objScopeService = new CScopeService();
		$objScopeService->setScopeId( $this->getId() );

		return $objScopeService;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchAllServices( $objDatabase ) {
		return \Psi\Eos\Admin\CServices::createService()->fetchAllServicesByScopeId( $this->getId(), $objDatabase );
	}

	public function fetchScopeServices( $objDatabase ) {
		return \Psi\Eos\Admin\CScopeServices::createService()->fetchScopeServicesByScopeId( $this->getId(), $objDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		} elseif( false != preg_match( '/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Special characters are not allowed in scope name.' ) );
		}

		if( false != isset( $objDatabase ) && false != valStr( $this->getName() ) ) {
			$strWhereSql = ' WHERE name =\'' . $this->getName() . '\'';
			if( 0 < $this->getId() ) {
				$strWhereSql .= ' AND id NOT IN (' . $this->getId() . ')';
			}

			$intConflictingScopeCount = \Psi\Eos\Admin\CScopes::createService()->fetchScopeCount( $strWhereSql, $objDatabase );

			if( 0 < $intConflictingScopeCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'scope_name', 'This scope name is already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( false == valStr( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;
		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				$boolIsValid = true;
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>