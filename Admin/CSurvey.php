<?php

class CSurvey extends CBaseSurvey {

	protected $m_arrobjSurveyTemplateOptions;
	protected $m_arrobjSurveyQuestionAnswers;
	protected $m_arrobjSurveyTemplateQuestions;
	protected $m_arrobjSurveyTemplateQuestionGroups;

	protected $m_intTaskId;
	protected $m_intIsRequired;
	protected $m_intIsResponded;
	protected $m_intFrequencyId;
	protected $m_intNumericWeight;
	protected $m_intSurveyTemplateOptionId;
	protected $m_intSurveyQuestionAnswerId;
	protected $m_intSurveyTemplateQuestionId;

	protected $m_strAnswer;
	protected $m_strSubjectName;
	protected $m_strResponderName;
	protected $m_strSurveyName;
	protected $m_strCompanyName;
	protected $m_strSerializedUrl;
	protected $m_strPreviousGoals;
	protected $m_strCompanyUserFullName;
	protected $m_strCid;
	protected $m_strEmployeeNameFull;
	protected $m_strDepartmentName;

	protected $m_boolNoShow;

	const SCORE_NO			= 0;
	const SCORE_DETRACTOR	= 6;
	const SCORE_PROMOTER	= 9;
	const CPA_LIFETIME 		= 86400;

	const ONE_YEAR			= 1;
	const THREE_MONTHS		= 3;
	const MONTHS_IN_YEAR	= 12;
	const DATE_RECEIVED		= 'Date Received';

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['previous_goals'] ) && $boolDirectSet ) {
			$this->m_strPreviousGoals = trim( $arrmixValues['previous_goals'] );
		} elseif( isset( $arrmixValues['previous_goals'] ) ) {
			$this->setPreviousGoals( $arrmixValues['previous_goals'] );
		}

		if( true == isset( $arrmixValues['answer'] ) ) $this->setAnswer( $arrmixValues['answer'] );
		if( true == isset( $arrmixValues['task_id'] ) ) $this->setTaskId( $arrmixValues['task_id'] );
		if( true == isset( $arrmixValues['frequency_id'] ) ) $this->setFrequencyId( $arrmixValues['frequency_id'] );
		if( true == isset( $arrmixValues['rating'] ) ) $this->setSurveyTemplateOptionId( $arrmixValues['rating'] );
		if( true == isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['company_id'] ) ) $this->setCid( $arrmixValues['company_id'] );
		if( true == isset( $arrmixValues['company_user_fullname'] ) ) $this->setCompanyUserFullName( $arrmixValues['company_user_fullname'] );
		if( true == isset( $arrmixValues['survey_question_answer_id'] ) ) $this->setSurveyQuestionAnswerId( $arrmixValues['survey_question_answer_id'] );
		if( true == isset( $arrmixValues['survey_name'] ) ) $this->setSurveyName( $arrmixValues['survey_name'] );
		if( true == isset( $arrmixValues['subject_name'] ) ) $this->setSubjectName( $arrmixValues['subject_name'] );
		if( true == isset( $arrmixValues['responder_name'] ) ) $this->setResponderName( $arrmixValues['responder_name'] );
		if( true == isset( $arrmixValues['is_required'] ) ) $this->setIsRequired( $arrmixValues['is_required'] );
		if( true == isset( $arrmixValues['survey_template_question_id'] ) ) $this->setSurveyTemplateQuestionId( $arrmixValues['survey_template_question_id'] );
		if( true == isset( $arrmixValues['is_responded'] ) ) $this->setIsResponded( $arrmixValues['is_responded'] );
		if( true == isset( $arrmixValues['employee_name_full'] ) ) $this->setEmployeeNameFull( $arrmixValues['employee_name_full'] );
		if( true == isset( $arrmixValues['department_name'] ) ) $this->setDepartmentName( $arrmixValues['department_name'] );
		if( true == isset( $arrmixValues['no_show'] ) ) $this->setNoShow( $arrmixValues['no_show'] );

		return;
	}

	/**
	*Set Functions
	*
	*/

	public function setSubjectName( $strSubjectName ) {
		$this->m_strSubjectName = $strSubjectName;
	}

	public function setResponderName( $strResponderName ) {
		$this->m_strResponderName = $strResponderName;
	}

	public function setSurveyName( $strSurveyName ) {
		$this->m_strSurveyName = $strSurveyName;
	}

	public function setAnswer( $strAnswer ) {
		 $this->m_strAnswer = $strAnswer;
	}

	public function setTaskId( $strTaskId ) {
		$this->m_intTaskId = $strTaskId;
	}

	public function setFrequencyId( $strFrequencyId ) {
		$this->m_intFrequencyId = $strFrequencyId;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setPreviousGoals( $strPreviousGoals ) {
		$this->m_strPreviousGoals = $strPreviousGoals;
	}

	public function setCompanyUserFullName( $strCompanyUserFullName ) {
		$this->m_strCompanyUserFullName = $strCompanyUserFullName;
	}

	public function setCid( $strCid ) {
		$this->m_strCid = $strCid;
	}

	public function setSurveyTemplateOptionId( $strSurveyTemplateOptionId ) {
		$this->m_intSurveyTemplateOptionId = $strSurveyTemplateOptionId;
	}

	public function setSurveyTemplateQuestions( $arrobjSurveyTemplateQuestions ) {
		$this->m_arrobjSurveyTemplateQuestions = $arrobjSurveyTemplateQuestions;
	}

	public function setSurveyTemplateOptions( $arrobjSurveyTemplateOptions ) {
		$this->m_arrobjSurveyTemplateOptions = $arrobjSurveyTemplateOptions;
	}

	public function setSurveyQuestionAnswers( $arrobjSurveyQuestionAnswers ) {
		$this->m_arrobjSurveyQuestionAnswers = $arrobjSurveyQuestionAnswers;
	}

	public function setSurveyTemplateQuestionGroups( $arrobjSurveyTemplateQuestionGroups ) {
		$this->m_arrobjSurveyTemplateQuestionGroups = $arrobjSurveyTemplateQuestionGroups;
	}

	public function setSerializedUrl( $strSerializedUrl ) {
		$this->m_strSerializedUrl = $strSerializedUrl;
	}

	public function setSurveyQuestionAnswerId( $intSurveyQuestionAnswerId ) {
		$this->m_intSurveyQuestionAnswerId = $intSurveyQuestionAnswerId;
	}

 	public function setIsRequired( $intIsRequired ) {
		$this->m_intIsRequired = CStrings::strToIntDef( $intIsRequired, NULL, false );
	}

	public function setIsResponded( $intIsResponded ) {
		$this->m_intIsResponded = CStrings::strToIntDef( $intIsResponded, NULL, false );
	}

	public function setSurveyTemplateQuestionId( $strSurveyTemplateQuestionId ) {
		$this->m_intSurveyTemplateQuestionId = $strSurveyTemplateQuestionId;
	}

	public function setEmployeeNameFull( $strEmployeeNameFull ) {
		$this->m_strEmployeeNameFull = $strEmployeeNameFull;
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	public function setNoShow( $boolNoShow ) {
		$this->m_boolNoShow = $boolNoShow;
	}

	/**
	*Get Functions
	*
	*/

	public function getDecryptedAnswer() {
		return $this->getDecryptedData( $this->m_strAnswer );
	}

	public function getEncryptedId( $intUserId, $strIds = NULL ) {
		// This code is for temporary purpose, we will remove this code against task: 2529591
		$intUserId = $intUserId;
		if( 'production' != CONFIG_ENVIRONMENT && CSurveyType::MANAGER_SURVEY == $this->m_intSurveyTypeId && false == valStr( $strIds ) ) {
			return $this->m_intId . ',' . $this->m_intTriggerReferenceId;
		}

		if( 'production' != CONFIG_ENVIRONMENT ) return ( true == valStr( $strIds ) ) ? $strIds : $this->m_intId;

		if( false == valStr( $strIds ) ) {
			$strIds = ( CSurveyType::MANAGER_SURVEY == $this->m_intSurveyTypeId ) ? $this->m_intId . ',' . $this->m_intTriggerReferenceId : $this->m_intId;
		}
		return base64_encode( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strIds, CONFIG_SODIUM_KEY_ID ) );
	}

	public static function getEncryptKey( $intUserId ) {
		// To resolve cache issue we will be using md5(user_id) as key
		return md5( $intUserId );
	}

	public static function getDecryptedData( $strRequestString, $strKey = NULL ) {
		if( 'production' != CONFIG_ENVIRONMENT ) return $strRequestString;

		if( true == valStr( $strRequestString ) ) {
			if( false == valStr( $strKey ) ) {
				return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $strRequestString, CONFIG_SODIUM_KEY_FORM_ANSWERS, [ 'legacy_secret_key' => CONFIG_KEY_FORM_ANSWERS ] );
			} else {
				return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $strRequestString, CONFIG_SODIUM_KEY_ID, [ 'legacy_secret_key' => $strKey ] );
			}
		}
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function getSubjectName() {
		return $this->m_strSubjectName;
	}

	public function getResponderName() {
		return $this->m_strResponderName;
	}

	public function getSurveyName() {
		return $this->m_strSurveyName;
	}

	public function getUserPreferenceId( $intUserId ) {

		$intUserReferenceId = ( 'production' != CONFIG_ENVIRONMENT ) ? NULL : $intUserId;
		return $intUserReferenceId;
	}

	public function getSurveyIds( $arrmixSurveyData ) {

		$arrmixSurveyIds = ( 'production' != CONFIG_ENVIRONMENT ) ? explode( ',', $arrmixSurveyData ) : $arrmixSurveyData;
		return $arrmixSurveyIds;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getPreviousGoals() {
		return $this->m_strPreviousGoals;
	}

	public function getCompanyUserFullName() {
		return $this->m_strCompanyUserFullName;
	}

	public function getCid() {
		return $this->m_strCid;
	}

	public function getSurveyTemplateOptionId() {
		return $this->m_intSurveyTemplateOptionId;
	}

	public function getSurveyTemplateQuestions() {
		return $this->m_arrobjSurveyTemplateQuestions;
	}

	public function getSurveyTemplateOptions() {
		return $this->m_arrobjSurveyTemplateOptions;
	}

	public function getSurveyQuestionAnswers() {
		return $this->m_arrobjSurveyQuestionAnswers;
	}

	public function getSurveyTemplateQuestionGroups() {
		return $this->m_arrobjSurveyTemplateQuestionGroups;
	}

	public function getSerializedUrl() {
		return $this->m_strSerializedUrl;
	}

	public function getSurveyQuestionAnswerId() {
		return $this->m_intSurveyQuestionAnswerId;
	}

	public function loadSerializedUrl() {
		$strSerializedUrl = base64_encode( serialize( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( ( int ) $this->getId() . '_' . ( int ) $this->getPsLeadId() . '_' . ( int ) $this->getSurveyTypeId() . '_' . ( int ) $this->getSubjectReferenceId() . '_' . ( int ) $this->getResponderReferenceId() . '_' . ( int ) $this->getTriggerReferenceId(), CONFIG_SODIUM_KEY_ID ) ) );

		if( true == valStr( $this->m_strSerializedUrl ) ) {
			$strSerializedUrl = $this->m_strSerializedUrl;
		}

		$this->setSerializedUrl( $strSerializedUrl );
	}

	public function getIsRequired() {
		return $this->m_intIsRequired;
	}

	public function getSurveyTemplateQuestionId() {
		return $this->m_intSurveyTemplateQuestionId;
	}

	public function getIsResponded() {
		return $this->m_intIsResponded;
	}

	public function getEmployeeNameFull() {
		return $this->m_strEmployeeNameFull;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getNoShow() {
		return $this->m_boolNoShow;
	}

	/**
	*Validation Functions
	*
	*/

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 *Fetch functions
	 *
	 */

	public function fetchSurveyTemplateQuestions( $objDatabase ) {
		return CSurveyTemplateQuestions::fetchSurveyTemplateQuestionsBySurveyTemplateId( $this->getSurveyTemplateId(), $objDatabase );
	}

}
?>