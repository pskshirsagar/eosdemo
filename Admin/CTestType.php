<?php
class CTestType extends CBaseTestType {

	const TEST 		= 1;
	const SURVEY 	= 2;

	/**
	 * If you are making any changes in loadSmartyConstants function then
	 * please make sure the same changes would be applied to loadTemplateConstants function also.
	 */

	public static function loadSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'TEST_TYPE_TEST', 	self::TEST );
		$objSmarty->assign( 'TEST_TYPE_SURVEY', self::SURVEY );
	}

	public static $c_arrintTemplateConstants = array(

		'TEST_TYPE_TEST'		=> self::TEST,
		'TEST_TYPE_SURVEY'		=> self::SURVEY,
	);
}
?>