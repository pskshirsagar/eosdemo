<?php

class CTag extends CBaseTag {

	protected $m_boolIsDuplicateNameRequired;

	protected $m_strEmployeeName;
	protected $m_strActionResultName;

	protected $m_intCount;

	const TRAINER_LEVEL_ONE			= 207;
	const TRAINER_LEVEL_TWO			= 208;
	const TRAINER_LEVEL_THREE		= 209;
	const TRAINER_LEVEL_FOUR		= 210;
	const TRAINER_LEVEL_FIVE		= 211;
	const EXTERNAL_TRAINER			= 216;
	const INDIAN_EMPLOYEE_RECORD	= 217;
	const INCIDENT					= 867;
	const OUTAGE					= 868;

	const BEGINNER_LEVEL			= 213;
	const SUPPORT_EMAIL				= 1461;
	const IT_EMAIL_TASK				= 2183;
	const NO_TAG                    = '2617';
	const UNIT_TEST                 = 2578;

	public function valName() {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateName( $objDatabase = NULL ) {

		$boolIsValid = true;
		if( true == $this->getIsPrivate() ) {
			$intCountDuplicateTag = CTags::fetchCountDuplicateTagsById( $this->getId(), $this->getName(), $this->getActionResultId(), $boolIsPrivate = true, $this->getCreatedBy(), $objDatabase );
		} else {
			$intCountDuplicateTag = CTags::fetchCountDuplicateTagsById( $this->getId(), $this->getName(), $this->getActionResultId(), $boolIsPrivate = false, $this->getCreatedBy(), $objDatabase );
		}

		if( false == is_null( $intCountDuplicateTag ) && 0 < $intCountDuplicateTag ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'duplicate_name', 'Name already exist.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valDuplicateName( $objAdminDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setIsDuplicateNameRequired( $boolRequired ) {
		$this->m_boolIsDuplicateNameRequired = $boolRequired;
	}

	public function getIsDuplicateNameRequired() {
		return $this->m_boolIsDuplicateNameRequired;
	}

	public function getEmployeeName() {
		return $this->m_strEmployeeName;
	}

	private function setEmployeeName( $strEmployeeName ) {
		$this->m_strEmployeeName = $strEmployeeName;
	}

	public function getCount() {
		return $this->m_strCount;
	}

	private function setCount( $intCount ) {
		$this->m_strCount = $intCount;
	}

	public function getActionResultName() {
		return $this->m_strActionResultName;
	}

	private function setActionResultName( $strActionResultName ) {
		$this->m_strActionResultName = $strActionResultName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['employee_name'] ) ) {
			$this->setEmployeeName( $arrmixValues['employee_name'] );
		}
		if( true == isset( $arrmixValues['count'] ) ) {
			$this->setCount( $arrmixValues['count'] );
		}
		if( true == isset( $arrmixValues['action_result_name'] ) ) {
			$this->setActionResultName( $arrmixValues['action_result_name'] );
		}
	}

}
?>