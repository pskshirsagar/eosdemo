<?php

class CSemLocationType extends CBaseSemLocationType {

	const CITY 						= 1;
	const METRO 					= 2;
	const STATE 					= 3;
	const CUSTOM 					= 4;
	const UNIVERSITY 				= 5;
	const TOURIST_ATTARACTION 		= 6;
}
?>