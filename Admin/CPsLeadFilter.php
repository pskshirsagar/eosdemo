<?php

use Psi\Eos\Admin\CPsLeadTypes;
use Psi\Eos\Admin\CPsLeadEvents;
use Psi\Eos\Admin\CSalesAreas;
use Psi\Eos\Admin\CPsProducts;
use Psi\Eos\Admin\CContractStatusTypes;
use Psi\Eos\Admin\CContracts;
use Psi\Eos\Admin\CPsLeadOrigins;
use Psi\Eos\Admin\CPsLeadSources;

use Psi\Libraries\UtilObjectModifiers\CObjectModifiers;

class CPsLeadFilter extends CBasePsLeadFilter {

	protected $m_arrobjCompleteContractStatusTypes;
	protected $m_arrobjCompletePsLeadOrigins;
	protected $m_arrobjCompletePsLeadSources;
	protected $m_arrobjCompleteActionTypes;
	protected $m_arrobjCompleteActionResults;
	protected $m_arrobjCompletePsProducts;
	protected $m_arrobjSalesAreas;
	protected $m_arrobjCompleteCompetitors;
	protected $m_arrobjCompletePropertyTypes;
	protected $m_arrobjCompleteEmployees;
	protected $m_arrobjCompleteStates;
	protected $m_arrobjPublishedPsLeadEvents = [];
	protected $m_arrobjCompletePsLeadTypes;

	protected $m_boolMakePublic;
	protected $m_boolSaveFilter;
	protected $m_boolJoinPersons;
	protected $m_boolIsInterested;
	protected $m_boolShowClicksOnly;
	protected $m_boolIsJoinEmployees;
	protected $m_boolIsMyOpportunity;
	protected $m_boolJoinPsLeadDetails;
	protected $m_boolIsForMassEmailing;
	protected $m_boolDoNotUsePsLeadView;
	protected $m_boolIsActiveCompaniesOnly;
	protected $m_boolIsForCloserReport;
	protected $m_boolShowNullPsEmployeeIdOnly;
	protected $m_boolShowLeadWiseOpportunities;
	protected $m_boolGroupByResponsibleEmployee;
	protected $m_boolShowImplementationWeightScore;

	protected $m_intLogWeek;
	protected $m_intEstimatedCloseMonth;
	protected $m_intActualCloseMonth;
	protected $m_intCurrentEmployeeId;
	protected $m_intGroupByPsProduct;
	protected $m_intGroupByEmployee;
	protected $m_intIsPeepMailSearch;
	protected $m_intIsCountRequest;
	protected $m_intPageNo;
	protected $m_intPageSize;
	protected $m_intContractId;
	protected $m_intEstimatedPropertyCount;
	protected $m_intEstimatedUnitCount;
	protected $m_intContractStatusTypeId;

	protected $m_strAds;
	protected $m_strMassEmailLinks;
	protected $m_strPsLeadIds;
	protected $m_strQuickSearch;
	protected $m_strDisplayType;
	protected $m_strDepartments;
	protected $m_strResponsibleSupportEmployees;
	protected $m_strActionEmployees;
	protected $m_strAdIds;
	protected $m_strAdLocations;
	protected $m_strAdProductIds;
	protected $m_strEmployees;
	protected $m_strRequestDatetime;
	protected $m_strAnticipatedCloseDate;
	protected $m_strCloseDate;
	protected $m_strResultStartDate;
	protected $m_strResultEndDate;
	protected $m_strRequestType;
	protected $m_strSalesManagers;

	protected $m_boolIsForSalesDashboard;
	protected $m_boolHasContractWatchlist;
	protected $m_boolIsPilot;
	protected $m_boolIsBigDeal;
	protected $m_boolIsForClientPage;
	protected $m_boolIsProposing;
	protected $m_boolIsSigned;
	protected $m_boolIsDemoing;
	protected $m_boolIsNew;
	protected $m_boolIsContacting;
	protected $m_boolIsQualified;
	protected $m_boolIsOnhold;
	protected $m_boolIsUnqualified;
	protected $m_boolIsIncomplete;

	public function __construct() {
		parent::__construct();

		$this->setDefaults();

		return;
	}

	/**
	 * Set Functions
	 */

	public function setDefaults() {

		$this->setSortBy( 'pl.company_name' );
		$this->setSortDirection( 'ASC' );

		$this->m_boolMakePublic 					= false;
		$this->m_boolSaveFilter 					= false;
		$this->m_strPsLeadIds 						= NULL;
		$this->m_boolJoinPersons 					= false;
		$this->m_boolIsJoinEmployees 				= false;
		$this->m_boolShowNullPsEmployeeIdOnly 		= false;
		$this->m_boolIsActiveCompaniesOnly			= false;
		$this->m_intShowWatchlistGroupsOnly 		= 0;
		$this->m_boolShowClicksOnly 				= 0;
		$this->m_intIsCountRequest	 				= 0;
		$this->m_intPageNo							= 1;
		$this->m_intPageSize						= 100;
		$this->m_boolShowAllOpportunity				= false;
		$this->m_boolShowLeadWiseOpportunities		= false;
		$this->m_boolIsBigDeal						= false;
		$this->m_boolIsDemoing						= false;
		$this->m_boolIsPilot						= false;
		$this->m_boolIsProposing					= false;
		$this->m_boolIsSigned						= false;
		$this->m_boolIsNew							= false;
		$this->m_boolIsContacting					= false;
		$this->m_boolIsQualified					= false;
		$this->m_boolIsOnhold						= false;
		$this->m_boolIsUnqualified					= false;
		$this->m_boolIsIncomplete					= false;
		$this->m_strCompanyStatusTypes				= '3,4,5';
		$this->m_boolIsForClientPage				= false;
		$this->m_boolShowImplementationWeightScore	= false;

		return;
	}

	public function setReportDefaults() {
		$this->m_strStartRequestDate 	= date( 'm' ) . '/1/' . date( 'Y' );
		$this->m_strEndRequestDate 		= date( 'm/d/Y', strtotime( '+1 month', strtotime( $this->m_strStartRequestDate ) ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['make_public'] ) ) $this->setMakePublic( $arrmixValues['make_public'] );
		if( true == isset( $arrmixValues['save_filter'] ) ) $this->setSaveFilter( $arrmixValues['save_filter'] );
		if( true == isset( $arrmixValues['join_ps_lead_details'] ) ) $this->setJoinPsLeadDetails( $arrmixValues['join_ps_lead_details'] );
		if( true == isset( $arrmixValues['quick_search'] ) ) $this->setQuickSearch( $arrmixValues['quick_search'] );
		if( true == isset( $arrmixValues['ps_lead_ids'] ) ) $this->setPsLeadIds( $arrmixValues['ps_lead_ids'] );
		if( true == isset( $arrmixValues['display_type'] ) ) $this->setDisplayType( $arrmixValues['display_type'] );
		if( true == isset( $arrmixValues['log_week'] ) ) $this->setLogWeek( $arrmixValues['log_week'] );
		if( true == isset( $arrmixValues['estimated_close_month'] ) ) $this->setEstimatedCloseMonth( $arrmixValues['estimated_close_month'] );
		if( true == isset( $arrmixValues['actual_close_month'] ) ) $this->setActualCloseMonth( $arrmixValues['actual_close_month'] );
		if( true == isset( $arrmixValues['current_employee_id'] ) ) $this->setCurrentEmployeeId( $arrmixValues['current_employee_id'] );
		if( true == isset( $arrmixValues['show_null_ps_employee_id_only'] ) ) $this->setShowNullPsEmployeeIdOnly( $arrmixValues['show_null_ps_employee_id_only'] );
		if( true == isset( $arrmixValues['responsible_support_employees'] ) ) $this->setResponsibleSupportEmployees( $arrmixValues['responsible_support_employees'] );
		if( true == isset( $arrmixValues['is_peep_mail_search'] ) ) $this->setIsPeepMailSearch( $arrmixValues['is_peep_mail_search'] );
		if( true == isset( $arrmixValues['is_interested'] ) ) $this->setIsInterested( $arrmixValues['is_interested'] );
		if( true == isset( $arrmixValues['departments'] ) ) $this->setDepartments( $arrmixValues['departments'] );
		if( true == isset( $arrmixValues['action_employees'] ) ) $this->setActionEmployees( $arrmixValues['action_employees'] );

		if( true == isset( $arrmixValues['mass_email_links'] ) ) $this->setMassEmailLinks( $arrmixValues['mass_email_links'] );
		if( true == isset( $arrmixValues['ads'] ) ) $this->setAds( $arrmixValues['ads'] );
		if( true == isset( $arrmixValues['ad_location_ids'] ) && true == valArr( $arrmixValues['ad_location_ids'] ) ) $this->setAdLocations( implode( ',', $arrmixValues['ad_location_ids'] ) );
		if( true == isset( $arrmixValues['ad_ps_product_ids'] ) && true == valArr( $arrmixValues['ad_ps_product_ids'] ) ) $this->setAdPsProductIds( implode( ',', $arrmixValues['ad_ps_product_ids'] ) );
		if( true == isset( $arrmixValues['sales_areas'] ) && true == valArr( $arrmixValues['sales_areas'] ) ) $this->setSalesAreas( implode( ',', $arrmixValues['sales_areas'] ) );
		if( true == isset( $arrmixValues['employee_ids'] ) && true == valArr( $arrmixValues['employee_ids'] ) ) $this->setEmployees( implode( ',', $arrmixValues['employee_ids'] ) );
		if( true == isset( $arrmixValues['request_datetime'] ) ) $this->setRequestDatetime( $arrmixValues['request_datetime'] );
		if( true == isset( $arrmixValues['is_for_sales_dashboard'] ) ) $this->setRequestDatetime( $arrmixValues['is_for_sales_dashboard'] );

		if( true == isset( $arrmixValues['show_all_opportunities'] ) ) $this->setShowAllOportunity( $arrmixValues['show_all_opportunities'] );
		if( true == isset( $arrmixValues['show_lead_wise_opportunities'] ) ) $this->setShowLeadWiseOpportunities( $arrmixValues['show_lead_wise_opportunities'] );

		if( true == isset( $arrmixValues['estimated_property_count'] ) ) $this->m_intEstimatedPropertyCount = trim( $arrmixValues['estimated_property_count'] );
		if( true == isset( $arrmixValues['estimated_unit_count'] ) ) $this->m_intEstimatedUnitCount = trim( $arrmixValues['estimated_unit_count'] );
		if( true == isset( $arrmixValues['has_contract_watchlist'] ) ) $this->setHasContractWatchlist( $arrmixValues['has_contract_watchlist'] );
		if( true == isset( $arrmixValues['contract_status_type_id'] ) ) $this->m_intContractStatusTypeId = trim( $arrmixValues['contract_status_type_id'] );
		if( true == isset( $arrmixValues['anticipated_close_date'] ) ) $this->m_strAnticipatedCloseDate = trim( $arrmixValues['anticipated_close_date'] );
		if( true == isset( $arrmixValues['contract_id'] ) ) $this->m_intContractId = trim( $arrmixValues['contract_id'] );
		if( true == isset( $arrmixValues['is_pilot'] ) ) $this->setIsPilot( $arrmixValues['is_pilot'] );
		if( true == isset( $arrmixValues['is_big_deal'] ) ) $this->setIsBigDeal( $arrmixValues['is_big_deal'] );
		if( true == isset( $arrmixValues['is_demoing'] ) ) $this->setIsDemoing( $arrmixValues['is_demoing'] );
		if( true == isset( $arrmixValues['close_date'] ) ) $this->setCloseDate( $arrmixValues['close_date'] );

		// New dates added to work with CloserActivitiesStats Report ?module=sales_stats-new&action=view_closer_activities_stats
		if( true == isset( $arrmixValues['action_start_date'] ) ) $this->setActionStartDate( $arrmixValues['action_start_date'] );
		if( true == isset( $arrmixValues['action_end_date'] ) ) $this->setActionEndDate( $arrmixValues['action_end_date'] );
		if( true == isset( $arrmixValues['result_start_date'] ) ) $this->setResultStartDate( $arrmixValues['result_start_date'] );
		if( true == isset( $arrmixValues['result_end_date'] ) ) $this->setResultEndDate( $arrmixValues['result_end_date'] );
		if( true == isset( $arrmixValues['sales_managers'] ) ) $this->setSalesManagers( implode( ',', $arrmixValues['sales_managers'] ) );

		// Old result dates. Keeping for now in case they break other reports.
		if( true == isset( $arrmixValues['start_result_date'] ) ) $this->setResultStartDate( $arrmixValues['start_result_date'] );
		if( true == isset( $arrmixValues['end_result_date'] ) ) $this->setResultEndDate( $arrmixValues['end_result_date'] );

		if( true == isset( $arrmixValues['request_type'] ) ) $this->setRequestType( $arrmixValues['request_type'] );

		return;
	}

	public function setResultStartDate( $strResultStartDate ) {
		$this->m_strResultStartDate = $strResultStartDate;
	}

	public function setResultEndDate( $strResultEndDate ) {
		$this->m_strResultEndDate = $strResultEndDate;
	}

	public function setSalesManagers( $strSalesManagers ) {
		$this->m_strSalesManagers = $strSalesManagers;
	}

	public function setActionEmployees( $strActionEmployees ) {
		$this->m_strActionEmployees = $strActionEmployees;
	}

	public function setMassEmailLinks( $strMassEmailLinks ) {
		$this->m_strMassEmailLinks = $strMassEmailLinks;
	}

	public function setAds( $strAds ) {
		$this->m_strAds = $strAds;
	}

	public function setAdPsProductIds( $strAdProductIds ) {
		$this->m_strAdProductIds = $strAdProductIds;
	}

	public function setAdLocations( $strAdLocations ) {
		$this->m_strAdLocations = $strAdLocations;
	}

	public function setRequestDatetime( $strRequestDatetime ) {
		$this->m_strRequestDatetime = $strRequestDatetime;
	}

	public function setIsCountRequest( $intIsCountRequest ) {
		$this->m_intIsCountRequest = $intIsCountRequest;
	}

	public function setEmployees( $strEmployees ) {
		$this->m_strEmployees = $strEmployees;
	}

	public function setDepartments( $strDepartments ) {
		$this->m_strDepartments = $strDepartments;
	}

	public function setIsPeepMailSearch( $intIsPeepMailSearch ) {
		$this->m_intIsPeepMailSearch = $intIsPeepMailSearch;
	}

	public function setResponsibleSupportEmployees( $strResponsibleSupportEmployees ) {
		$this->m_strResponsibleSupportEmployees = $strResponsibleSupportEmployees;
	}

	public function setGroupByEmployee( $intGroupByEmployee ) {
		$this->m_intGroupByEmployee = $intGroupByEmployee;
	}

	public function setGroupByPsProduct( $intGroupByPsProduct ) {
		$this->m_intGroupByPsProduct = $intGroupByPsProduct;
	}

	public function setGroupByResponsibleEmployee( $boolGroupByResponsibleEmployee ) {
		$this->m_boolGroupByResponsibleEmployee = $boolGroupByResponsibleEmployee;
	}

	public function setShowNullPsEmployeeIdOnly( $boolShowNullPsEmployeeIdOnly ) {
		$this->m_boolShowNullPsEmployeeIdOnly = $boolShowNullPsEmployeeIdOnly;
	}

	public function setCurrentEmployeeId( $intCurrentEmployeeId ) {
		$this->m_intCurrentEmployeeId = $intCurrentEmployeeId;
	}

	public function setLogWeek( $intLogWeek ) {
		$this->m_intLogWeek = $intLogWeek;
	}

	public function setEstimatedCloseMonth( $intEstimatedCloseMonth ) {
		$this->m_intEstimatedCloseMonth = $intEstimatedCloseMonth;
	}

	public function setActualCloseMonth( $intActualCloseMonth ) {
		$this->m_intActualCloseMonth = $intActualCloseMonth;
	}

	public function setCompleteContractStatusTypes( $arrobjCompleteContractStatusTypes ) {
		$this->m_arrobjCompleteContractStatusTypes = $arrobjCompleteContractStatusTypes;
	}

	public function setCompletePsLeadOrigins( $arrobjCompletePsLeadOrigins ) {
		$this->m_arrobjCompletePsLeadOrigins = $arrobjCompletePsLeadOrigins;
	}

	public function setCompletePsLeadSources( $arrobjCompletePsLeadSources ) {
		$this->m_arrobjCompletePsLeadSources = $arrobjCompletePsLeadSources;
	}

	public function setCompleteActionTypes( $arrobjCompleteActionTypes ) {
		$this->m_arrobjCompleteActionTypes = $arrobjCompleteActionTypes;
	}

	public function setCompleteActionResults( $arrobjCompleteActionResults ) {
		$this->m_arrobjCompleteActionResults = $arrobjCompleteActionResults;
	}

	public function setCompletePsProducts( $arrobjCompletePsProducts ) {
		$this->m_arrobjCompletePsProducts = $arrobjCompletePsProducts;
	}

	public function setCompleteCompetitors( $arrobjCompleteCompetitors ) {
		$this->m_arrobjCompleteCompetitors = $arrobjCompleteCompetitors;
	}

	public function setCompletePropertyTypes( $arrobjCompletePropertyTypes ) {
		$this->m_arrobjCompletePropertyTypes = $arrobjCompletePropertyTypes;
	}

	public function setCompleteEmployees( $arrobjCompleteEmployees ) {
		$this->m_arrobjCompleteEmployees = $arrobjCompleteEmployees;
	}

	public function setCompleteStates( $arrobjCompleteStates ) {
		$this->m_arrobjCompleteStates = $arrobjCompleteStates;
	}

	public function setPublishedPsLeadEvents( $arrobjPublishedPsLeadEvents ) {
		$this->m_arrobjPublishedPsLeadEvents = $arrobjPublishedPsLeadEvents;
	}

	public function setMakePublic( $boolMakePublic ) {
		$this->m_boolMakePublic = $boolMakePublic;
	}

	public function setSaveFilter( $boolSaveFilter ) {
		$this->m_boolSaveFilter = $boolSaveFilter;
	}

	public function setQuickSearch( $strQuickSearch ) {
		$this->m_strQuickSearch = $strQuickSearch;
	}

	public function setJoinPersons( $boolJoinPersons ) {
		$this->m_boolJoinPersons = $boolJoinPersons;
	}

	public function setJoinPsLeadDetails( $boolJoinPsLeadDetails ) {
		$this->m_boolJoinPsLeadDetails = $boolJoinPsLeadDetails;
	}

	public function setIsForMassEmailing( $boolIsForMassEmailing ) {
		$this->m_boolIsForMassEmailing = $boolIsForMassEmailing;
	}

	public function setPsLeadIds( $strPsLeadIds ) {
		$this->m_strPsLeadIds = $strPsLeadIds;
	}

	public function setJoinEmployees( $boolJoinEmployees ) {
		$this->m_boolIsJoinEmployees = $boolJoinEmployees;
	}

	public function setIsActiveCompaniesOnly( $boolIsActiveCompaniesOnly ) {
		$this->m_boolIsActiveCompaniesOnly = $boolIsActiveCompaniesOnly;
	}

	public function setIsForCloserReport( $boolIsForCloserReport ) {
		$this->m_boolIsForCloserReport = $boolIsForCloserReport;
	}

	public function setDisplayType( $strDisplayType ) {
		$this->m_strDisplayType = $strDisplayType;
	}

	public function setIsInterested( $boolIsInterested ) {
		$this->m_boolIsInterested = $boolIsInterested;
	}

	public function setDoNotUsePsLeadView( $boolDoNotUsePsLeadView ) {
		$this->m_boolDoNotUsePsLeadView = $boolDoNotUsePsLeadView;
	}

	public function setPageNo( $intPageNo ) {
		$this->m_intPageNo = $intPageNo;
	}

	public function setPageSize( $intPageSize ) {
		$this->m_intPageSize = $intPageSize;
	}

	public function setShowAllOpportunity( $boolShowAllOpportunity ) {
		$this->m_boolShowAllOpportunity = $boolShowAllOpportunity;
	}

	public function setShowLeadWiseOpportunities( $boolShowLeadWiseOpportunities ) {
		$this->m_boolShowLeadWiseOpportunities = $boolShowLeadWiseOpportunities;
	}

	public function setShowImplementationWeightScore( $boolShowImplementationWeightScore ) {
		$this->m_boolShowImplementationWeightScore = $boolShowImplementationWeightScore;
	}

	public function setIsMyOpportunity( $boolIsMyOpportunity ) {
		$this->m_boolIsMyOpportunity = $boolIsMyOpportunity;
	}

	// opportunity Listing

	public function setAnticipatedCloseDate( $strAnticipatedCloseDate ) {
		$this->m_strAnticipatedCloseDate = CStrings::strTrimDef( $strAnticipatedCloseDate, -1, NULL, true );
	}

	public function setEstimatedPropertyCount( $intEstimatedPropertyCount ) {
		$this->m_intEstimatedPropertyCount = CStrings::strToIntDef( $intEstimatedPropertyCount, NULL, false );
	}

	public function sqlEstimatedPropertyCount() {
		return  ( true == isset( $this->m_intEstimatedPropertyCount ) ) ? ( string ) $this->m_intEstimatedPropertyCount : 'NULL';
	}

	public function setEstimatedUnitCount( $intEstimatedUnitCount ) {
		$this->m_intEstimatedUnitCount = CStrings::strToIntDef( $intEstimatedUnitCount, NULL, false );
	}

	public function setContractStatusTypeId( $intContractStatusTypeId ) {
		$this->m_intContractStatusTypeId = CStrings::strToIntDef( $intContractStatusTypeId, NULL, false );
	}

	public function setIsForSalesDashboard( $boolIsForSalesDashboard ) {
		$this->m_boolIsForSalesDashboard = $boolIsForSalesDashboard;
	}

	public function setIsForClientPage( $boolIsForClientPage ) {
		$this->m_boolIsForClientPage = $boolIsForClientPage;
	}

	public function setCloseDate( $strCloseDate ) {
		$this->m_strCloseDate = CStrings::strTrimDef( $strCloseDate, -1, NULL, true );
	}

	public function setContractId( $intContractId ) {
		$this->m_intContractId = CStrings::strToIntDef( $intContractId, NULL, false );
	}

	public function setIsPilot( $intIsPilot ) {
		$this->m_boolIsPilot = $intIsPilot;
	}

	public function setIsBigDeal( $intIsBigDeal ) {
		$this->m_boolIsBigDeal = $intIsBigDeal;
	}

	public function setIsProposing( $intIsProposing ) {
		$this->m_boolIsProposing = $intIsProposing;
	}

	public function setIsSigned( $intIsSigned ) {
		$this->m_boolIsSigned = $intIsSigned;
	}

	public function setIsDemoing( $boolIsDemoing ) {
		$this->m_boolIsDemoing = $boolIsDemoing;
	}

	public function setIsNew( $boolIsNew ) {
		$this->m_boolIsNew = $boolIsNew;
	}

	public function setIsContacting( $boolIsContacting ) {
		$this->m_boolIsContacting = $boolIsContacting;
	}

	public function setIsQualified( $boolIsQualified ) {
		$this->m_boolIsQualified = $boolIsQualified;
	}

	public function setIsOnhold( $boolIsOnhold ) {
		$this->m_boolIsOnhold = $boolIsOnhold;
	}

	public function setIsUnqualified( $boolIsUnqualified ) {
		$this->m_boolIsUnqualified = $boolIsUnqualified;
	}

	public function setIsIncomplete( $boolIsIncomplete ) {
		$this->m_boolIsIncomplete = $boolIsIncomplete;
	}

	public function setPsLeadOrigins( $strPsLeadOrigins ) {
		$this->m_strPsLeadOrigins = $strPsLeadOrigins;
	}

	public function setSalesAreas( $strSalesAreas ) {
		$this->m_strSalesAreas = $strSalesAreas;
	}

	public function setRequestType( $strRequestType ) {
		$this->m_strRequestType = $strRequestType;
	}

	public function setCompletePsLeadTypes( $arrobjCompletePsLeadTypes ) {
		$this->m_arrobjCompletePsLeadTypes = $arrobjCompletePsLeadTypes;
	}

	/**
	 * Get Functions
	 */

	public function getResultStartDate() {
		return $this->m_strResultStartDate;
	}

	public function getResultEndDate() {
		return $this->m_strResultEndDate;
	}

	public function getActionEmployees() {
		return $this->m_strActionEmployees;
	}

	public function getSalesManagers() {
		return $this->m_strSalesManagers;
	}

	public function getMassEmailLinks() {
		return $this->m_strMassEmailLinks;
	}

	public function getAds() {
		return $this->m_strAds;
	}

	public function getAdPsProductIds() {
		return $this->m_strAdProductIds;
	}

	public function getAdLocations() {
		return $this->m_strAdLocations;
	}

	public function getIsCountRequest() {
		return $this->m_intIsCountRequest;
	}

	public function getRequestDatetime() {
		return	$this->m_strRequestDatetime;
	}

	public function getEmployees() {
		return $this->m_strEmployees;
	}

	public function getDepartments() {
		return $this->m_strDepartments;
	}

	public function getIsPeepMailSearch() {
		return $this->m_intIsPeepMailSearch;
	}

	public function getResponsibleSupportEmployees() {
		return $this->m_strResponsibleSupportEmployees;
	}

	public function getGroupByPsProduct() {
		return $this->m_intGroupByPsProduct;
	}

	public function getGroupByResponsibleEmployee() {
		return $this->m_boolGroupByResponsibleEmployee;
	}

	public function getGroupByEmployee() {
		return $this->m_intGroupByEmployee;
	}

	public function getActualCloseMonth() {
		return $this->m_intActualCloseMonth;
	}

	public function getShowNullPsEmployeeIdOnly() {
		return $this->m_boolShowNullPsEmployeeIdOnly;
	}

	public function getCurrentEmployeeId() {
		return $this->m_intCurrentEmployeeId;
	}

	public function getJoinEmployees() {
		return $this->m_boolIsJoinEmployees;
	}

	public function getIsActiveCompaniesOnly() {
		return $this->m_boolIsActiveCompaniesOnly;
	}

	public function getIsForCloserReport() {
		return $this->m_boolIsForCloserReport;
	}

	public function getEstimatedCloseMonth() {
		return $this->m_intEstimatedCloseMonth;
	}

	public function getLogWeek() {
		return $this->m_intLogWeek;
	}

	public function getCompleteContractStatusTypes() {
		return $this->m_arrobjCompleteContractStatusTypes;
	}

	public function getCompletePsLeadOrigins() {
		return $this->m_arrobjCompletePsLeadOrigins;
	}

	public function getCompletePsLeadSources() {
		return $this->m_arrobjCompletePsLeadSources;
	}

	public function getCompleteActionTypes() {
		return $this->m_arrobjCompleteActionTypes;
	}

	public function getCompleteActionResults() {
		return $this->m_arrobjCompleteActionResults;
	}

	public function getCompletePsProducts() {
		return $this->m_arrobjCompletePsProducts;
	}

	public function getCompleteSalesAreas() {
		return $this->m_arrobjSalesAreas;
	}

	public function getSalesAreas() {
		return $this->m_strSalesAreas;
	}

	public function getCompleteCompetitors() {
		return $this->m_arrobjCompleteCompetitors;
	}

	public function getCompletePropertyTypes() {
		return $this->m_arrobjCompletePropertyTypes;
	}

	public function getCompleteEmployees() {
		return $this->m_arrobjCompleteEmployees;
	}

	public function getCompleteStates() {
		return $this->m_arrobjCompleteStates;
	}

	public function getPublishedPsLeadEvents() {
		return $this->m_arrobjPublishedPsLeadEvents;
	}

	public function getMakePublic() {
		return $this->m_boolMakePublic;
	}

	public function getSaveFilter() {
		return $this->m_boolSaveFilter;
	}

	public function getQuickSearch() {
		return $this->m_strQuickSearch;
	}

	public function getJoinPersons() {
		return $this->m_boolJoinPersons;
	}

	public function getJoinPsLeadDetails() {
		return $this->m_boolJoinPsLeadDetails;
	}

	public function getIsForMassEmailing() {
		return $this->m_boolIsForMassEmailing;
	}

	public function getPsLeadIds() {
		return $this->m_strPsLeadIds;
	}

	public function getDisplayType() {
		return $this->m_strDisplayType;
	}

	public function getIsInterested() {
		return $this->m_boolIsInterested;
	}

	public function getDoNotUsePsLeadView() {
		return $this->m_boolDoNotUsePsLeadView;
	}

	public function getPageNo() {
		return $this->m_intPageNo;
	}

	public function getPageSize() {
		return $this->m_intPageSize;
	}

	public function getShowAllOpportunity() {
		return $this->m_boolShowAllOpportunity;
	}

	public function getShowLeadWiseOpportunities() {
		return $this->m_boolShowLeadWiseOpportunities;
	}

	public function getShowImplementationWeightScore() {
		return $this->m_boolShowImplementationWeightScore;
	}

	public function getIsMyOpportunity() {
		return $this->m_boolIsMyOpportunity;
	}

	// opportunity Listing

	public function getHasContractWatchlist() {
		return $this->m_boolHasContractWatchlist;
	}

	public function getContractStatusTypeId() {
		return $this->m_intContractStatusTypeId;
	}

	public function getEstimatedPropertyCount() {
		return $this->m_intEstimatedPropertyCount;
	}

	public function getEstimatedUnitCount() {
		return $this->m_intEstimatedUnitCount;
	}

	public function getAnticipatedCloseDate() {
		return $this->m_strAnticipatedCloseDate;
	}

	public function getIsForSalesDashboard() {
		return $this->m_boolIsForSalesDashboard;
	}

	public function getIsForClientPage() {
		return $this->m_boolIsForClientPage;
	}

	public function getCloseDate() {
		return $this->m_strCloseDate;
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function getIsPilot() {
		return  $this->m_boolIsPilot;
	}

	public function getIsBigDeal() {
		return  $this->m_boolIsBigDeal;
	}

	public function getIsProposing() {
		return $this->m_boolIsProposing;
	}

	public function getIsSigned() {
		return $this->m_boolIsSigned;
	}

	public function getIsDemoing() {
		return $this->m_boolIsDemoing;
	}

	public function getIsNew() {
		return $this->m_boolIsNew;
	}

	public function getIsContacting() {
		return $this->m_boolIsContacting;
	}

	public function getIsQualified() {
		return $this->m_boolIsQualified;
	}

	public function getIsOnhold() {
		return $this->m_boolIsOnhold;
	}

	public function getIsUnqualified() {
		return $this->m_boolIsUnqualified;
	}

	public function getIsIncomplete() {
		return $this->m_boolIsIncomplete;
	}

	public function getPsLeadOrigins() {
		return  $this->m_strPsLeadOrigins;
	}

	public function getRequestType() {
		return $this->m_strRequestType;
	}

	public function getCompletePsLeadTypes() {
		return $this->m_arrobjCompletePsLeadTypes;
	}

	/**
	 * Validate Functions
	 */

	public function valFilterName() {
		$boolIsValid = true;

		if( true == is_null( $this->getFilterName() ) && true == $this->getSaveFilter() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'filter_name', 'Filter name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valStartRequestDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getStartRequestDate() ) && true != CValidation::validateDate( $this->getStartRequestDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_request_date', 'Start date is not properly formatted.' ) );
		}

		return $boolIsValid;
	}

	public function valEndRequestDate() {
		$boolIsValid = true;

		if( false == is_null( $this->getEndRequestDate() ) && true != CValidation::validateDate( $this->getEndRequestDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_request_date', 'End date is not properly formatted.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				// If someone set save filter as false during insert or update he have to make that confition false so adding the below statement
				$this->setSaveFilter( true );
			case 'redirect':
				$boolIsValid &= $this->valFilterName();
				$boolIsValid &= $this->valStartRequestDate();
				$boolIsValid &= $this->valEndRequestDate();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPsLeadTypeData( $objDatabase ) {

		if( false == valArr( $this->m_arrobjCompleteContractStatusTypes ) ) $this->m_arrobjCompleteContractStatusTypes	= CContractStatusTypes::createService()->fetchAllContractStatusTypes( $objDatabase );
		if( false == valArr( $this->m_arrobjCompletePsLeadOrigins ) ) 		$this->m_arrobjCompletePsLeadOrigins 		= CPsLeadOrigins::createService()->fetchAllPsLeadOrigins( $objDatabase );
		if( false == valArr( $this->m_arrobjCompletePsLeadSources ) ) 		$this->m_arrobjCompletePsLeadSources 		= CPsLeadSources::createService()->fetchAllPsLeadSources( $objDatabase );
		if( false == valArr( $this->m_arrobjCompleteActionTypes ) ) 		$this->m_arrobjCompleteActionTypes 			= CActionTypes::fetchAllActionTypes( $objDatabase );
		if( false == valArr( $this->m_arrobjCompleteActionResults ) ) 		$this->m_arrobjCompleteActionResults 		= CActionResults::fetchAllActionResults( $objDatabase );
		if( false == valArr( $this->m_arrobjCompletePsProducts ) ) 			$this->m_arrobjCompletePsProducts 			= CPsProducts::createService()->fetchPsProductsByIsForSale( $intIsForSale = 1, $objDatabase );
		if( false == valArr( $this->m_arrobjSalesAreas ) ) 					$this->m_arrobjSalesAreas		 			= CSalesAreas::createService()->fetchAllSalesAreas( $objDatabase );
		if( false == valArr( $this->m_arrobjCompleteCompetitors ) ) 		$this->m_arrobjCompleteCompetitors 			= \Psi\Eos\Admin\CCompetitors::createService()->fetchAllCompetitors( $objDatabase );
		if( false == valArr( $this->m_arrobjCompletePropertyTypes ) ) 		$this->m_arrobjCompletePropertyTypes 		= CPropertyTypes::fetchPropertyTypesByPropertyTypeIds( CPropertyType::$c_arrintExcludePropertyTypeIds, $objDatabase, $strOrderBy = 'name' );
		if( false == valArr( $this->m_arrobjCompleteEmployees ) ) 			$this->m_arrobjCompleteEmployees 			= CEmployees::fetchEmployeesByDepartmentIdsOrderByNameFull( [ CDepartment::SALES, CDepartment::SALES_DIALER ], $objDatabase );
		if( false == valArr( $this->m_arrobjCompleteStates ) ) 				$this->m_arrobjCompleteStates	 			= \Psi\Eos\Admin\CStates::createService()->fetchAllStatesKeyedByCode( $objDatabase );
		if( false == valArr( $this->m_arrobjPublishedPsLeadEvents ) ) 		$this->m_arrobjPublishedPsLeadEvents	 	= CPsLeadEvents::createService()->fetchPublishedPsLeadEvents( $intPsLeadEventId = NULL, $objDatabase );
		if( false == valArr( $this->m_arrobjCompletePsLeadTypes ) ) 		$this->m_arrobjCompletePsLeadTypes	 		= CPsLeadTypes::createService()->fetchAllPsLeadTypes( $objDatabase );

		// Now we need to loop on all of these objects and set is_selected where required.
		$arrintContractStatusTypeIds 	= explode( ',', $this->m_strContractStatusTypes );
		$arrintPsLeadOriginIds 			= explode( ',', $this->m_strPsLeadOrigins );
		$arrintPsLeadSourceIds 			= explode( ',', $this->m_strPsLeadSources );
		$arrintActionTypeIds 			= explode( ',', $this->m_strActionTypes );
		$arrintActionResultIds			= explode( ',', $this->m_strActionResults );
		$arrintPsProductIds				= explode( ',', $this->m_strPsProducts );
		$arrintSalesAreas				= explode( ',', $this->m_strSalesAreas );
		$arrintCompetitorIds			= explode( ',', $this->m_strCompetitors );
		$arrintPropertyTypeIds			= explode( ',', $this->m_strPropertyTypes );
		$arrintEmployeeIds				= explode( ',', $this->m_strEmployees );
		$arrintPsLeadEventsIds			= explode( ',', $this->m_strPsLeadEvents );
		$arrstrStateCodes				= explode( ',', $this->m_strStateCodes );
		$arrintPsLeadTypeIds			= explode( ',', $this->m_strPsLeadTypes );

		if( true == valArr( $arrintContractStatusTypeIds ) && true == valArr( $this->m_arrobjCompleteContractStatusTypes ) ) {
			foreach( $this->m_arrobjCompleteContractStatusTypes as $objContractStatusType ) {
				if( true == in_array( $objContractStatusType->getId(), $arrintContractStatusTypeIds ) ) {
					$objContractStatusType->setIsSelected( true );
				}
			}
		}

		if( true == valArr( $arrintPsLeadOriginIds ) && true == valArr( $this->m_arrobjCompletePsLeadOrigins ) ) {
			foreach( $this->m_arrobjCompletePsLeadOrigins as $objPsLeadOrigin ) {
				if( true == in_array( $objPsLeadOrigin->getId(), $arrintPsLeadOriginIds ) ) {
					$objPsLeadOrigin->setIsSelected( true );
				}
			}
		}

		if( true == valArr( $arrintPsLeadSourceIds ) && true == valArr( $this->m_arrobjCompletePsLeadSources ) ) {
			foreach( $this->m_arrobjCompletePsLeadSources as $objPsLeadSource ) {
				if( true == in_array( $objPsLeadSource->getId(), $arrintPsLeadSourceIds ) ) {
					$objPsLeadSource->setIsSelected( true );
				}
			}
		}

		if( true == valArr( $arrintActionTypeIds ) && true == valArr( $this->m_arrobjCompleteActionTypes ) ) {
			foreach( $this->m_arrobjCompleteActionTypes as $objActionType ) {
				if( true == in_array( $objActionType->getId(), $arrintActionTypeIds ) ) {
					$objActionType->setIsSelected( true );
				}
			}
		}

		if( true == valArr( $arrintActionResultIds ) && true == valArr( $this->m_arrobjCompleteActionResults ) ) {
			foreach( $this->m_arrobjCompleteActionResults as $objActionResult ) {
				if( true == in_array( $objActionResult->getId(), $arrintActionResultIds ) ) {
					$objActionResult->setIsSelected( true );
				}
			}
		}

		if( true == valArr( $arrintPsProductIds ) && true == valArr( $this->m_arrobjCompletePsProducts ) ) {
			foreach( $this->m_arrobjCompletePsProducts as $objPsProduct ) {
				if( true == in_array( $objPsProduct->getId(), $arrintPsProductIds ) ) {
					$objPsProduct->setIsSelected( true );
				}
			}
		}

		if( true == valArr( $arrintSalesAreas ) && true == valArr( $this->m_arrobjSalesAreas ) ) {
			foreach( $this->m_arrobjSalesAreas as $objSalesArea ) {
				if( true == in_array( $objSalesArea->getId(), $arrintSalesAreas ) ) {
					$objSalesArea->setIsSelected( true );
				}
			}
		}

		if( true == valArr( $arrintCompetitorIds ) && true == valArr( $this->m_arrobjCompleteCompetitors ) ) {
			foreach( $this->m_arrobjCompleteCompetitors as $objCompetitor ) {
				if( true == in_array( $objCompetitor->getId(), $arrintCompetitorIds ) ) {
					$objCompetitor->setIsSelected( true );
				}
			}
		}

		if( true == valArr( $arrintPropertyTypeIds ) && true == valArr( $this->m_arrobjCompletePropertyTypes ) ) {
			foreach( $this->m_arrobjCompletePropertyTypes as $objPropertyType ) {
				if( true == in_array( $objPropertyType->getId(), $arrintPropertyTypeIds ) ) {
					$objPropertyType->setIsSelected( true );
				}
			}
		}

		if( true == valArr( $arrintEmployeeIds ) && true == valArr( $this->m_arrobjCompleteEmployees ) ) {
			foreach( $this->m_arrobjCompleteEmployees as $objEmployee ) {
				if( true == in_array( $objEmployee->getId(), $arrintEmployeeIds ) ) $objEmployee->setIsSelected( true );
			}
		}

		if( true == valArr( $arrstrStateCodes ) && true == valArr( $this->m_arrobjCompleteStates ) ) {
			foreach( $this->m_arrobjCompleteStates as $objState ) {
				if( true == in_array( $objState->getCode(), $arrstrStateCodes ) ) $objState->setIsSelected( true );
			}
		}

		if( true == valArr( $arrintPsLeadEventsIds ) && true == valArr( $this->m_arrobjPublishedPsLeadEvents ) ) {
			foreach( $this->m_arrobjPublishedPsLeadEvents as $objPsLeadEvent ) {
				if( true == in_array( $objPsLeadEvent->getId(), $arrintPsLeadEventsIds ) ) {
					$objPsLeadEvent->setIsSelected( true );
				}
			}
		}

		if( true == valArr( $arrintPsLeadTypeIds ) && true == valArr( $this->m_arrobjCompletePsLeadTypes ) ) {
			foreach( $this->m_arrobjCompletePsLeadTypes as $objPsLeadType ) {
				if( true == in_array( $objPsLeadType->getId(), $arrintPsLeadTypeIds ) ) {
					$objPsLeadType->setIsSelected( true );
				}
			}
		}
	}

	public function fetchPsLeads( $objAdminDatabase ) {

		return \Psi\Eos\Admin\CPsLeads::createService()->fetchPsLeadsByPsLeadFilter( $this, $objAdminDatabase );
	}

	public function fetchContracts( $objAdminDatabase ) {

		return CContracts::createService()->fetchPaginatedOpportunities( 1, 1000000, $this, $objAdminDatabase );
	}

	public function fetchPersons( $objAdminDatabase ) {

		return \Psi\Eos\Admin\CPersons::createService()->fetchPersonsByPsLeadFilter( $this, $objAdminDatabase );
	}

	public function calclulateActionRanges( $strPeriod ) {

		if( false == CValidation::validateDate( $this->getCloseDate() ) ) {
			trigger_error( 'Close date must be set before assigning action ranges.', E_USER_ERROR );
			exit;
		}

		$intCloseDate = strtotime( $this->getCloseDate() );
		$intMonth = date( 'n', $intCloseDate );
		$intYear = date( 'Y', $intCloseDate );

		switch( $strPeriod ) {

			case 'year':
				$this->m_strActionStartDate	= date( 'm/d/Y', strtotime( '1/1/' . $intYear ) );
				$this->m_strActionEndDate	= date( 'm/d/Y', strtotime( '12/31/' . $intYear ) );
				$this->m_strResultStartDate	= date( 'm/d/Y', strtotime( '1/1/' . $intYear ) );
				$this->m_strResultEndDate	= date( 'm/d/Y', strtotime( '12/31/' . $intYear ) );
				break;

			case 'quarter':
				$this->m_strActionStartDate = $this->m_strResultStartDate = getQuarterStartMonth( $this->getCloseDate() )->format( 'm/1/Y' );
				$this->m_strActionEndDate = $this->m_strResultEndDate = getQuarterStartMonth( $this->getCloseDate() )->modify( '+2 months' )->format( 'm/t/Y' );
				break;

			case 'month':
				$this->m_strActionStartDate = date( 'm/d/Y', strtotime( $intMonth . '/1/' . $intYear ) );
				$this->m_strResultStartDate	= date( 'm/d/Y', strtotime( $intMonth . '/1/' . $intYear ) );

				$this->m_strActionEndDate = date( 'm/t/Y', strtotime( $intMonth . '/1/' . $intYear ) );
				$this->m_strResultEndDate = date( 'm/t/Y', strtotime( $intMonth . '/1/' . $intYear ) );
				break;

			case 'week':
				$this->m_strActionStartDate = getUsWeekStartDate( $this->getCloseDate() )->format( 'm/d/Y' );
				$this->m_strResultStartDate	= getUsWeekStartDate( $this->getCloseDate() )->format( 'm/d/Y' );

				$this->m_strActionEndDate	= getUsWeekStartDate( $this->getCloseDate() )->modify( '+6 days' )->format( 'm/d/Y' );
				$this->m_strResultEndDate	= getUsWeekStartDate( $this->getCloseDate() )->modify( '+6 days' )->format( 'm/d/Y' );
				break;

			default:
				// default case
				break;
		}

		return;
	}

	/**
	 * Other Functions
	 */

	public function buildGet() {

		$strGet = '';

		$strGet .= ( 0 < strlen( $this->getContracts() ) ) ? '&ps_lead_filter[contracts]=' . urlencode( $this->getContracts() ) : NULL;
		$strGet .= ( 0 < strlen( $this->getPersons() ) ) ? '&ps_lead_filter[persons]=' . urlencode( $this->getPersons() ) : NULL;
		$strGet .= ( 0 < strlen( $this->getActionTypes() ) ) ? '&ps_lead_filter[action_types]=' . urlencode( $this->getActionTypes() ) : NULL;
		$strGet .= ( 0 < strlen( $this->getActionStartDate() ) ) ? '&ps_lead_filter[action_start_date]=' . urlencode( $this->getActionStartDate() ) : NULL;
		$strGet .= ( 0 < strlen( $this->getActionEndDate() ) ) ? '&ps_lead_filter[action_end_date]=' . urlencode( $this->getActionEndDate() ) : NULL;
		$strGet .= ( 0 < strlen( $this->getResultStartDate() ) ) ? '&ps_lead_filter[result_start_date]=' . urlencode( $this->getResultStartDate() ) : NULL;
		$strGet .= ( 0 < strlen( $this->getResultEndDate() ) ) ? '&ps_lead_filter[result_end_date]=' . urlencode( $this->getResultEndDate() ) : NULL;
		$strGet .= ( 0 < strlen( $this->getCloseDate() ) ) ? '&ps_lead_filter[close_date]=' . urlencode( $this->getCloseDate() ) : NULL;
		$strGet .= ( 0 < strlen( $this->getDisplayType() ) ) ? '&ps_lead_filter[display_type]=' . urlencode( $this->getDisplayType() ) : NULL;
		$strGet .= ( 0 < strlen( $this->getCompanyStatusTypes() ) ) ? '&ps_lead_filter[company_status_types]=' . urlencode( $this->getCompanyStatusTypes() ) : NULL;

		return $strGet;
	}

	public function downloadPsLeads( $objAdminDatabase ) {

	// no time limit is imposed.
		set_time_limit( 30 );

		$intRowIndex	= 0;
		$intColumnIndex	= 0;

		$arrmixPsLeadData = [];

		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'ID';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Client';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Last Contact';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Origin';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Source';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Event';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Sales Closer';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Highest Contract Status';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Last Status';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Name';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Phone';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Email';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'City';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'State';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = '# Props';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = '# Units';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Site';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Property Types';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Business Type';
		$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'Date Created';

		$strFileName = tempnam( '/tmp', 'LeadFilterResult' );
		$resFileHanldle = CFileIo::fileOpen( $strFileName, 'w' );
		$strDownloadFileFilename = 'Leads_' . date( 'd/M/y' ) . '.csv';

		$arrobjPsLeads 			= $this->fetchPsLeads( $objAdminDatabase );

		$arrobjPersonPhoneNumbers = [];
		if( valArr( $arrobjPsLeads ) ) {
			$arrintPersonIds = [];
			foreach( $arrobjPsLeads as $objPsLead ) {
				$arrintPersonIds[] = $objPsLead->getPersonId();
			}

			if( valArr( $arrintPersonIds ) ) {
				$arrobjPersonPhoneNumbers = rekeyObjects( 'PersonId', \Psi\Eos\Admin\CPersonPhoneNumbers::createService()->fetchPersonPhoneNumbersByPersonIds( $arrintPersonIds, $objAdminDatabase ), true );
			}
		}

		if( true == valArr( $arrobjPsLeads ) ) {

			$arrobjPsLeadDetails 		= \Psi\Eos\Admin\CPsLeadDetails::createService()->fetchPsLeadDetailByPsLeadIds( array_keys( $arrobjPsLeads ), $objAdminDatabase );
			$arrobjPsLeadDetails		= rekeyObjects( 'PsLeadId', $arrobjPsLeadDetails );
			$arrobjPsLeadOrigins 		= CPsLeadOrigins::createService()->fetchAllPsLeadOrigins( $objAdminDatabase );
			$arrobjContractStatusTypes 	= CContractStatusTypes::createService()->fetchAllContractStatusTypes( $objAdminDatabase );
			$arrobjEmployees 			= CEmployees::fetchActiveEmployees( $objAdminDatabase );
			$arrobjPsLeadSources 		= CPsLeadSources::createService()->fetchAllPsLeadSources( $objAdminDatabase );
			$arrobjPsLeadEvents 		= CPsLeadEvents::createService()->fetchPublishedPsLeadEvents( $intPropertyTypeId = NULL, $objAdminDatabase );
			$arrobjCompanyStatusTypes 	= \Psi\Eos\Admin\CCompanyStatusTypes::createService()->fetchAllCompanyStatusTypes( $objAdminDatabase );
			$arrobjPsLeadTypes 			= CPsLeadTypes::createService()->fetchAllPsLeadTypes( $objAdminDatabase );

			$arrobjContracts = CContracts::createService()->fetchContractsByPsLeadIds( array_keys( $arrobjPsLeads ), $objAdminDatabase );

			if( true == valArr( $arrobjContracts ) ) {
				CObjectModifiers::createService()->nestObjects( $arrobjContracts, $arrobjPsLeads );
			}

			foreach( $arrobjPsLeads as $objPsLead ) {

				$intRowIndex++;
				$intColumnIndex 		= 0;
				$intSalesEmployeeId 	= 0;

				$strLeadSourceName 			= '';
				$strPsLeadOriginName		= '';
				$strPsLeadEventName			= '';
				$strOutSideEmployeeFullName = '';
				$strWebsiteUrls				= '';
				$strPsLeadTypeName		= '';

				if( true == array_key_exists( $objPsLead->getId(), $arrobjPsLeadDetails ) ) {
					$intSalesEmployeeId 	= $arrobjPsLeadDetails[$objPsLead->getId()]->getSalesEmployeeId();
					$strWebsiteUrls			= $arrobjPsLeadDetails[$objPsLead->getId()]->getPrimaryWebsiteUrls();
				}

				if( true == isset( $arrobjPsLeadSources[$objPsLead->getPsLeadSourceId()] ) ) {
					$strLeadSourceName  = $arrobjPsLeadSources[$objPsLead->getPsLeadSourceId()]->getName();
				}

				if( true == isset( $arrobjPsLeadOrigins[$objPsLead->getPsLeadOriginId()] ) ) {
					$strPsLeadOriginName  = $arrobjPsLeadOrigins[$objPsLead->getPsLeadOriginId()]->getName();
				}

				if( true == isset( $arrobjPsLeadEvents[$objPsLead->getPsLeadEventId()] ) ) {
					$strPsLeadEventName  = $arrobjPsLeadEvents[$objPsLead->getPsLeadEventId()]->getName();
				}

				if( true == isset( $arrobjEmployees[$intSalesEmployeeId] ) ) {
					$strOutSideEmployeeFullName = $arrobjEmployees[$intSalesEmployeeId]->getPreferredName();
				}

				if( true == isset( $arrobjPsLeadTypes[$objPsLead->getPsLeadTypeId()] ) ) {
					$strPsLeadTypeName 	= $arrobjPsLeadTypes[$objPsLead->getPsLeadTypeId()]->getName(); // Buisness Type
				}

				$strPersonName = $objPsLead->getNameFirst() . ' ' . $objPsLead->getNameLast();

				$strLeadSourceName = preg_replace( '/^(http)s?:\/+/i', '', $strLeadSourceName );
				$strPsLeadEventName = preg_replace( '/^(http)s?:\/+/i', '', $strPsLeadEventName );
				$objPsLead->setEmailAddress( preg_replace( '/^(http)s?:\/+/i', '', $objPsLead->getEmailAddress() ) );
				$strWebsiteUrls	= ( preg_replace( '/^(http)s?:\/+/i', '', $strWebsiteUrls ) );
				$strPersonName = preg_replace( '/^(http)s?:\/+/i', '', $strPersonName );
				$strPersonName = preg_replace( '/^=/', '-', $strPersonName );

				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $objPsLead->getId();
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $objPsLead->getCompanyName();
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $objPsLead->getCondensedRequestDatetime();
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = ( true == isset( $strPsLeadOriginName ) ? $strPsLeadOriginName : '' );
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = ( true == isset( $strLeadSourceName ) ? $strLeadSourceName : '' );
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = ( true == isset( $strPsLeadEventName ) ? $strPsLeadEventName : '' );
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = ( true == isset( $strOutSideEmployeeFullName ) ? $strOutSideEmployeeFullName :'' );
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = ( false == is_null( $objPsLead->getMaxContractStatusTypeId() )? $arrobjContractStatusTypes[$objPsLead->getMaxContractStatusTypeId()]->getName():'' );
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = ( false == is_null( $objPsLead->getCompanyStatusTypeId() )? $arrobjCompanyStatusTypes[$objPsLead->getCompanyStatusTypeId()]->getName():'' );
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $strPersonName;

				$strPhoneNumber = '';
				if( valArr( $arrobjPersonPhoneNumbers ) && isset( $arrobjPersonPhoneNumbers[$objPsLead->getPersonId()] ) ) {
					$arrobjPhoneNumbers = rekeyObjects( 'PhoneNumberTypeId', $arrobjPersonPhoneNumbers[$objPsLead->getPersonId()], true );
					if( isset( $arrobjPhoneNumbers[CPhoneNumberType::OFFICE] ) ) {
						$strPhoneNumber = $arrobjPhoneNumbers[CPhoneNumberType::OFFICE][0]->getPhoneNumber();
					}
				}

				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $strPhoneNumber;
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $objPsLead->getEmailAddress();
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $arrobjPsLeadDetails[$objPsLead->getId()]->getCity();
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $arrobjPsLeadDetails[$objPsLead->getId()]->getStateCode();
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $objPsLead->getPropertyCount();
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $objPsLead->getNumberOfUnits();
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $strWebsiteUrls;
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $objPsLead->getPsLeadPropertyTypes();
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = $strPsLeadTypeName;
				$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = date( 'm/d/y', strtotime( $objPsLead->getCreatedOn() ) );
			}

		} else {
			$intRowIndex++;
			$arrmixPsLeadData[$intRowIndex][$intColumnIndex++] = 'No records found.';
		}

		if( true == valArr( $arrmixPsLeadData ) ) {
			foreach( $arrmixPsLeadData as $mixPsLeadData ) {
				fputcsv( $resFileHanldle, $mixPsLeadData );
			}
		}

		fclose( $resFileHanldle );

		// Download Employees Details
		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-type: application/csv; name=' . $strDownloadFileFilename );
		header( 'Content-Disposition: attachment; filename=' . $strDownloadFileFilename );

		$resFilePointer	= fopen( $strFileName, 'rb' );
		fpassthru( $resFilePointer );
		fclose( $resFilePointer );
		unlink( $strFileName );
		exit;
	}

	public function downloadOpportunities( $objAdminDatabase ) {

		set_time_limit( 500 );

		$intRowIndex	= 0;
		$intColumnIndex	= 0;

		$arrmixOpportunityData = [];

		$arrobjContracts		= CContracts::createService()->fetchPaginatedOpportunities( 1, 1000000, $this, $objAdminDatabase, $boolIsDownloadOpportunity = true );

		if( true == valArr( $arrobjContracts ) ) {
			$arrobjActionTypes 					= CActionTypes::fetchAllActionTypes( $objAdminDatabase );
			$arrobjContractStatusTypes 			= CContractStatusTypes::createService()->fetchAllContractStatusTypes( $objAdminDatabase );
			$arrobjEmployees 					= CEmployees::fetchAllEmployees( $objAdminDatabase );
			$arrobjActionNotes					= CActions::fetchLatestActionBycontractIds( array_keys( $arrobjContracts ), $objAdminDatabase );
			$arrobjActions						= CActions::fetchRecentTwoActionsByContractIds( array_keys( $arrobjContracts ), $objAdminDatabase );
			$arrobjDepartments					= CDepartments::fetchPublishedDepartments( $objAdminDatabase );

			if( true == valArr( $arrobjActionNotes ) || true == valArr( $arrobjActions ) ) {
				CObjectModifiers::createService()->nestObjects( $arrobjActionNotes, $arrobjContracts );
				CObjectModifiers::createService()->nestObjects( $arrobjActions, $arrobjContracts );
			}
		}

		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Opportunity Id';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Lead Id';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Date';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Company';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Title';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Mgmt. Com. Id';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Status';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Implementation Weight Score';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Sales Closer';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Sales Engineer';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Responsible Emp. & Dept.';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Products(date of first time sale)';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = '#Props';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Units';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Setup Rev';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Monthly Rev';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Trans Rev';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'New ACV';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Est. Close';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Percent Likelihood';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Close Date';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Last Updated';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Current Status';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Current Status Date';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Previous Status';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Previous Status Date';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Company Status';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Is Pilot';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Property Type';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Reject Type';

		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Employee Name';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Date Time';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Note';
		$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'Link to Account Plan';

		if( true == valArr( $arrobjContracts ) ) {

			foreach( $arrobjContracts as $objContract ) {

				$intRowIndex++;
				$intColumnIndex = 0;

				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getId();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getPsLeadId();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = date( 'm-d-Y', strtotime( $objContract->getCreatedOn() ) );
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getCompanyName();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getTitle();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getCid();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = ( false == is_null( $objContract->getContractStatusTypeId() )?$arrobjContractStatusTypes[$objContract->getContractStatusTypeId()]->getName():'' );
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getImplementationWeightScore();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = ( true == isset( $arrobjEmployees[$objContract->getSalesEmployeeId()] ) ? $arrobjEmployees[$objContract->getSalesEmployeeId()]->getNameFirst() . ' ' . $arrobjEmployees[$objContract->getSalesEmployeeId()]->getNameLast(): '' );

				$strSalesEngineer = '';
				if( true == is_numeric( $objContract->getSalesEngineerId() ) && true == array_key_exists( $objContract->getSalesEngineerId(), $arrobjEmployees ) ) {
					$objSalesEngineerEmployee				= $arrobjEmployees[$objContract->getSalesEngineerId()];
					$strSalesEngineer				= $objSalesEngineerEmployee->getNameFirst() . ' ' . $objSalesEngineerEmployee->getNameLast();
				}
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $strSalesEngineer;

				$strResponsibleEmployeeName = '';
				$strResponsibleEmployeeDepartment = '';
				if( true == is_numeric( $objContract->getResponsibleEmployeeId() ) && true == array_key_exists( $objContract->getResponsibleEmployeeId(), $arrobjEmployees ) ) {
					$objResponsibleEmployee				= $arrobjEmployees[$objContract->getResponsibleEmployeeId()];
					$strResponsibleEmployeeName			= $objResponsibleEmployee->getNameFirst() . ' ' . $objResponsibleEmployee->getNameLast();
					$strResponsibleEmployeeDepartment	= ', ' . $arrobjDepartments[$objResponsibleEmployee->getDepartmentId()]->getName();
				}

				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $strResponsibleEmployeeName . '' . $strResponsibleEmployeeDepartment;

				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getPsProductNames();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getEstimatedPropertyCount();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getEstimatedUnitCount();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getImplementationAmount();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getRecurringAmount();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getTransactionalAmount();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getTotalNewAcv();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getAnticipatedCloseDate();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getPercentCloseLikelihood();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getCloseDate();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = date( 'm/d/Y', strtotime( $objContract->getUpdatedOn() ) );

				$intLogCount = 0;

				if( true == valArr( $objContract->getActions() ) ) {

					foreach( $objContract->getActions() as $objAction ) {
						if( true == in_array( $objAction->getActionTypeId(), CActionType::$c_arrintStatusChangeActionTypeIds ) ) {
							$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $arrobjActionTypes[$objAction->getActionTypeId()]->getName();
							$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = date( 'm/d/y', strtotime( $objAction->getActionDatetime() ) );

							++$intLogCount;
						}
					}
				}

				if( 0 == $intLogCount ) {
					$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = '';
					$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = '';
					$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = '';
					$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = '';
				} elseif( 1 == $intLogCount ) {
					$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = '';
					$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = '';
				}

				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getCompanyStatusType();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getIsPilot();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getPsLeadPropertyTypes();
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $objContract->getContractRejectType();
				$boolLogNoData = true;
				if( true == valArr( $objContract->getActions() ) ) {
					foreach( $objContract->getActions() as $objAction ) {
						if( CActionType::NOTE == $objAction->getActionTypeId() ) {
							$intEmployeeId = $objAction->getEmployeeId();

							if( false == empty( $arrobjEmployees[$intEmployeeId] ) ) {
								$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $arrobjEmployees[$intEmployeeId]->getNameFirst() . ' ' . $arrobjEmployees[$intEmployeeId]->getNameLast();
							} else {
								$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = '';
							}

							$strActionNotes = preg_replace( '/^(http)s?:\/+/i', '', $objAction->getNotes() );

							$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = date( 'm/d/y', strtotime( $objAction->getActionDatetime() ) );
							$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = $strActionNotes;
							$boolLogNoData = false;
						}
					}
				}
				if( true == $boolLogNoData ) {
					$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = ''; // employee name
					$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = ''; // date time
					$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = ''; // note
				}
				$strLinkForAccountPlan = 'No Company Found';
				if( false == is_null( $objContract->getPsLeadId() ) ) {
					$strLinkForAccountPlan = '=HYPERLINK("' . CONFIG_INSECURE_HOST_PREFIX . 'clientadmin.' . CONFIG_COMPANY_BASE_DOMAIN . '?module=ps_lead-new&ps_lead[id]=' . $objContract->getPsLeadId() . '&load_home_profile_tab=1","Click Here")';
				}
				$arrmixOpportunityData[$intRowIndex][$intColumnIndex] = $strLinkForAccountPlan;
			}
		} else {
			$intRowIndex++;
			$arrmixOpportunityData[$intRowIndex][$intColumnIndex++] = 'No records found.';
		}

		$strFileName = tempnam( '/tmp', 'OpportunityResult' );
		$resFileHanldle = CFileIo::fileOpen( $strFileName, 'w' );
		$strDownloadFileFilename = 'Opportunities.csv';

		if( true == valArr( $arrmixOpportunityData ) ) {
			foreach( $arrmixOpportunityData as $mixOpportunityData ) {
				fputcsv( $resFileHanldle, $mixOpportunityData );
			}
		}

		fclose( $resFileHanldle );

		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-type: application/csv; name=' . $strDownloadFileFilename );
		header( 'Content-Disposition: attachment; filename=' . $strDownloadFileFilename );

		$resFilePointer	= fopen( $strFileName, 'rb' );
		fpassthru( $resFilePointer );

		fclose( $resFilePointer );
		unlink( $strFileName );
		exit;
	}

	public function buildCloseDateQuarter() {

		switch( date( 'm', strtotime( $this->m_strCloseDate ) ) ) {
			case '1':
			case '2':
			case '3':
				return 'Q1';
				break;
			case '4':
			case '5':
			case '6':
				return 'Q2';
				break;
			case '7':
			case '8':
			case '9':
				return 'Q3';
				break;
			case '10':
			case '11':
			case '12':
				return 'Q4';
				break;
			default:
				// default case
				return 'Q1';
				break;
		}
	}

}
?>