<?php

class CS2NetboxApiRequestLog extends CBaseS2NetboxApiRequestLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApiCommand() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequestData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResponseCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valResponseData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>