<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportDepartmentAssociations
 * Do not add any new functions to this class.
 */

class CReportDepartmentAssociations extends CBaseReportDepartmentAssociations {

	public static function fetchReportDepartmentsByCompanyReportId( $intCaReportId,  $objDatabase ) {
		return self::fetchReportDepartmentAssociations( sprintf( 'SELECT * FROM report_department_associations WHERE company_report_id = %d', ( int ) $intCaReportId ), $objDatabase );
	}

	public static function fetchReportDepartmentAssociationsByCompanyReportIdByReportDepartmentId( $intCaReportId,  $arrintReportDepartments, $objDatabase ) {
		if( false == is_numeric( $intCaReportId ) || false == valArr( array_filter( $arrintReportDepartments ) ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						report_department_associations AS rpa
					WHERE
						rpa.company_report_id = ' . ( int ) $intCaReportId . '
						AND rpa.department_id IN ( ' . implode( ',', array_filter( $arrintReportDepartments ) ) . ' ) ';

		return parent::fetchReportDepartmentAssociations( $strSql, $objDatabase );
	}

	public static function fetchReportsDepartmentAssociationById( $arrintIds, $objDatabase ) {

		$strSql = 'SELECT 
						* 
					FROM 
						report_department_associations 
					WHERE id IN ( ' . implode( ',', array_filter( $arrintIds ) ) . ' ) ';

		return self::fetchReportDepartmentAssociations( $strSql, $objDatabase );
	}

}
?>