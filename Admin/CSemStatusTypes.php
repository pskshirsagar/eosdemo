<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemStatusTypes
 * Do not add any new functions to this class.
 */

class CSemStatusTypes extends CBaseSemStatusTypes {

	public static function fetchSemStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSemStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSemStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSemStatusType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>