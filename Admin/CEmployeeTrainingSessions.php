<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeTrainingSessions
 * Do not add any new functions to this class.
 */

class CEmployeeTrainingSessions extends CBaseEmployeeTrainingSessions {

	public static function fetchEmployeeTrainingSessionBySessionIdByGroupIds( $intTrainingSessionId, $arrintGroupIds, $objDatabase ) {

		if( false == valArr( $arrintGroupIds ) ) return NULL;

		$strSql = 'SELECT
						ets.*,
						e.id as employee_id
					FROM
						employee_training_sessions ets
						LEFT JOIN users AS u ON ets.user_id = u.id
						LEFT JOIN employees AS e ON e.id = u.employee_id
					WHERE ets.training_session_id= ' . ( int ) $intTrainingSessionId . ' AND ets.group_id IN(' . implode( ',', $arrintGroupIds ) . ' ) ';

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionBySessionId( $intTrainingSessionId, $objDatabase ) {

		$strSql = 'SELECT
						ets.*,
						e.id as employee_id,
						u.id as user_id
					FROM
						employee_training_sessions ets
						JOIN users AS u ON ets.user_id = u.id
						JOIN employees AS e ON e.id = u.employee_id
					WHERE
						ets.training_session_id= ' . ( int ) $intTrainingSessionId;

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionBySessionIdByRemainingGroupIds( $intTrainingSessionId, $objDatabase, $arrintExistingGroupIds = NULL ) {

		$strSql = 'SELECT
						ets.*,
						e.id as employee_id
					FROM
						employee_training_sessions ets
						JOIN users AS u ON ets.user_id = u.id
						JOIN employees AS e ON e.id = u.employee_id
					WHERE
						ets.group_id IS NOT NULL AND
						ets.training_session_id= ' . ( int ) $intTrainingSessionId;

		if( true == valArr( $arrintExistingGroupIds ) ) {
			$strSql .= ' AND ets.user_id NOT IN
					( SELECT DISTINCT ( user_id ) FROM user_groups WHERE group_id IN ( ' . implode( ',', $arrintExistingGroupIds ) . ' ) AND deleted_by IS NULL ) ';
		}

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedEmployeeTrainingSessionCount( $intSessionId, $boolIsAvailable, $objDatabase, $boolShowPendingRequests = NULL ) {

		$strSql = 'SELECT
						count ( DISTINCT ( ets.user_id ) )
					FROM employee_training_sessions AS ets
						LEFT JOIN users u ON ets.user_id = u.id
					WHERE
						u.is_disabled = 0
						AND ets.training_session_id =' . ( int ) $intSessionId;

		if( true == $boolShowPendingRequests && true == is_null( $boolIsAvailable ) ) {
			$strSql .= ' AND ( ets.user_id = ets.updated_by AND ets.is_available = 0 )';
		} elseif( true == $boolShowPendingRequests && true == $boolIsAvailable ) {
			$strSql .= ' AND ( ( ets.user_id = ets.updated_by AND ets.is_available = 0 ) OR ets.is_available = 1 ) ';
		} else {
			if( true == is_null( $boolIsAvailable ) || true == $boolIsAvailable ) {
				$strSql .= ' AND ets.is_available = 1 ';
			} else {
				$strSql .= ' AND ets.is_available = 0 AND ets.user_id <> ets.updated_by';
			}
		}

		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchPaginatedEmployeesBySessionIdByFilter( $intSessionId, $intIsAvailable, $objTrainingSessionTraineesFilter, $objDatabase, $boolShowPendingRequests ) {

		$strSql = 'SELECT
							DISTINCT ( ets.user_id ) as user_id,
							ets.id,
							ets.updated_by,
							ets.created_by,
							e.id as employee_id,
							e.name_first,
							e.name_last,
							e.preferred_name,
							e.name_full,
							e.email_address,
							ets.is_available,
							dept.name as department_name,
							d.name as designation_name,
							ets.created_on
						FROM employee_training_sessions as ets
							JOIN users as u ON ets.user_id = u.id
							JOIN employees as e ON e.id = u.employee_id
							LEFT JOIN designations as d ON d.id = e.designation_id
							LEFT JOIN departments as dept ON dept.id = e.department_id
						WHERE
							u.is_disabled = 0 ';

		if( true == $boolShowPendingRequests && true == is_null( $intIsAvailable ) ) {
			$strSql .= ' AND ( ets.user_id = ets.updated_by AND ets.is_available = 0 )';
		} else {
			if( true == is_null( $intIsAvailable ) || true == $intIsAvailable ) {
				$strSql .= ' AND ets.is_available = 1 ';
			} else {
				$strSql .= ' AND ets.is_available = 0 AND ets.user_id <> ets.updated_by';
			}
		}

		$strSql .= ' AND ets.training_session_id = ' . ( int ) $intSessionId . '
						ORDER BY ets.created_on ASC, e.name_full ASC OFFSET ';

		$strSql .= ( 0 < $objTrainingSessionTraineesFilter->getPageNo() ) ? $objTrainingSessionTraineesFilter->getPageSize() * ( $objTrainingSessionTraineesFilter->getPageNo() - 1 ) : 0;
		$strSql .= ' LIMIT ' . ( int ) $objTrainingSessionTraineesFilter->getPageSize();

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionsByTrainingSessionIdByUserIds( $arrintTrainingSessionIds, $arrintUserIds, $objDatabase ) {

		if( false == valArr( $arrintTrainingSessionIds ) ) return NULL;
		if( false == valArr( $arrintUserIds ) ) return NULL;

		$strSql = 'SELECT ets.*,
						e.id as employee_id,
						e.name_first,
						e.name_last,
						e.preferred_name,
						e.name_full,
						e.email_address
					FROM
						employee_training_sessions AS ets
						JOIN users as u ON ets.user_id = u.id
						JOIN employees as e ON e.id = u.employee_id
					WHERE
						ets.training_session_id IN (' . implode( ',', $arrintTrainingSessionIds ) . ' )
						AND ets.user_id IN (' . implode( ',', $arrintUserIds ) . ' )';

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionsStatisticsByTrainingSessionIds( $arrintTrainingSessionIds, $objDatabase ) {

		if( false == valArr( $arrintTrainingSessionIds ) ) return NULL;

		$strSql = 'SELECT
						count(ets.*) as attendee_count,
						SUM ( CASE
								WHEN ets.is_present = 1 THEN ets.is_present
							END ) AS present_count,
						training_session_id,
						ts.id as training_session_id,
						ts.actual_start_time,
						ts.actual_end_time
					FROM
						employee_training_sessions as ets
						LEFT JOIN users AS u ON ets.user_id = u.id
						LEFT JOIN training_sessions as ts on ts.id = ets.training_session_id
					WHERE
						is_available = 1
						AND training_session_id IN( ' . implode( ',', $arrintTrainingSessionIds ) . ' )
					GROUP BY
						training_session_id, ts.actual_start_time, ts. actual_end_time, ts.id';

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );

	}

	public static function fetchEmployeeTrainingSessionsDetailByTrainingSessionId( $intTrainingSessionId, $objDatabase, $boolIsShowPreviousEmployees = false, $boolIsShowApprovedEmployees = true ) {

		$strSubSql = '';

		if( false == $boolIsShowPreviousEmployees ) {
			$strSubSql = ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		}

		if( true == $boolIsShowApprovedEmployees ) {
			$strSubSql .= ' AND ets.is_available = 1 ';
		}

		$strSql = 'SELECT ets.*,
						extract ( minute from age(ets.start_time, ts.actual_start_time) ) as late_by,
						ts.name as training_session_name,
						t.name as training_name,
						e.id as employee_id,
						e.name_first,
						e.name_last,
						e.preferred_name,
						e.email_address ,
						d.name as department_name,
						desg.name as designation_name
					FROM employee_training_sessions as ets
						JOIN training_sessions as ts on ets.training_session_id = ts.id
						JOIN trainings as t on t.id = ts.training_id
						JOIN users as u on ets.user_id = u.id
						JOIN employees as e on u.employee_id = e.id
						JOIN departments as d on e.department_id = d.id
						LEFT JOIN designations as desg on e.designation_id = desg.id
					WHERE
						ets.training_session_id =\'' . ( int ) $intTrainingSessionId . '\'
						' . $strSubSql . '
					ORDER By e.name_first';

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionCountByUserId( $intUserId, $objDatabase ) {
		$strSql = 'SELECT
						count ( ets.id )
					FROM
						employee_training_sessions AS ets
						JOIN training_sessions AS ts ON ts.id = ets.training_session_id
					WHERE
						ets.user_id = ' . ( int ) $intUserId . '
						AND ts.deleted_by IS NULL
						AND ts.actual_start_time IS NOT NULL';

		$arrintResponse = fetchData( $strSql, $objDatabase );
			if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

			return 0;
	}

	public static function fetchEmployeeTrainingSessionswithTrainingSessionDetailsByUserIdByFilter( $intUserId, $objAttendanceReportFilter, $objDatabase ) {

		$intOffset = ( 0 < $objAttendanceReportFilter->getPageNo() ) ? $objAttendanceReportFilter->getPageSize() * ( $objAttendanceReportFilter->getPageNo() - 1 ) : 0;
		$strOrderClause = ' ORDER BY ts.actual_start_time DESC ';
		$strOrder = '';

		if( false == is_null( $objAttendanceReportFilter ) && false == is_null( $objAttendanceReportFilter->getOrderByField() ) && false == is_null( $objAttendanceReportFilter->getOrderByType() ) ) {
			$strOrder = ( 1 == $objAttendanceReportFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';
			$strOrderClause = ' ORDER BY ' . $objAttendanceReportFilter->getOrderByField() . '' . $strOrder;
		}

		$strWhere = ' WHERE ets.user_id = ' . ( int ) $intUserId . ' AND ts.deleted_by IS NULL AND ts.actual_start_time IS NOT NULL';

		$strSql = 'SELECT
							ets.*,
							ts.name as training_session_name,
							ts.actual_start_time as actual_start_time,
							ts.actual_end_time as actual_end_time
						FROM
							employee_training_sessions AS ets
							JOIN Training_sessions AS ts ON ts.id = ets.training_session_id';

		$strSql = $strSql . $strWhere . '' . $strOrderClause . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $objAttendanceReportFilter->getPageSize() . '';

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionlateCountByTrainingSessionIds( $arrintTrainingSessionIds, $objDatabase ) {

		if( false == valArr( $arrintTrainingSessionIds ) ) return NULL;

		$strSql = 'SELECT count(ets.user_id) as late_count,
						ts.id as training_session_id,
						ts.actual_start_time,
						ts.actual_end_time
					FROM employee_training_sessions as ets
						left join training_sessions as ts on ts.id = ets.training_session_id
					WHERE ts.actual_start_time <> ets.start_time AND
						ts.id IN( ' . implode( ',', $arrintTrainingSessionIds ) . ' )
					GROUP BY ts.actual_start_time, ts. actual_end_time, ts.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionsByIds( $arrintEmployeeTrainingSessionIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeTrainingSessionIds ) ) return NULL;

		$strSql = 'SELECT
						ets.*,
						e.id AS employee_id,
						e.name_first,
						e.email_address
					FROM
						employee_training_sessions ets
						LEFT JOIN users AS u ON ets.user_id = u.id
						LEFT JOIN employees AS e ON u.employee_id = e.id
					WHERE
						ets.id IN( ' . implode( ',', $arrintEmployeeTrainingSessionIds ) . ' )';

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionswithTrainingSessionDetailsByUserId( $intUserId, $objDatabase ) {

		$strSql = 'SELECT
						ets.*,
						ts.name AS training_session_name,
						ts.actual_start_time AS actual_start_time,
						ts.actual_end_time AS actual_end_time
					FROM
						employee_training_sessions AS ets
						JOIN Training_sessions AS ts ON ts.id = ets.training_session_id
					WHERE
						ts.deleted_by IS NULL
						AND ts.scheduled_start_time IS NOT NULL
						AND ts.actual_start_time IS NOT NULL
						AND ets.user_id = ' . ( int ) $intUserId . '
					ORDER BY
						ts.actual_start_time DESC';

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionsByTrainingSessionIdByUserId( $intTrainingSessionId, $intUserId, $objDatabase ) {
		$strSql = 'SELECT
						ets.*
					FROM
						employee_training_sessions AS ets
					WHERE
						ets.training_session_id = ' . ( int ) $intTrainingSessionId . '
						AND ets.user_id = ' . ( int ) $intUserId;

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionsByTrainingSessionIds( $arrintTrainingSessionIds, $objDatabase ) {

		if( false == valArr( $arrintTrainingSessionIds ) ) return NULL;

		$strSql = 'SELECT
						ets.*
					FROM
						employee_training_sessions AS ets
					WHERE
						ets.is_available = 1
						AND ets.training_session_id IN( ' . implode( ',', $arrintTrainingSessionIds ) . ')';

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchPastEmployeeTrainingSessionsByUserId( $intUserId, $objDatabase ) {
		$strSql = 'SELECT DISTINCT(ets.training_session_id),
						ts.actual_start_time,
						ts.actual_end_time,
						ets.start_time

					FROM employee_training_sessions as ets
						JOIN training_sessions as ts ON ets.training_session_id = ts.id
					WHERE ets.user_id = ' . ( int ) $intUserId . '
						AND is_available =1
						AND is_present = 1
						AND ts.deleted_by IS NULL';

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingAttendedOfPreviousOneYearByUserIds( $arrintUserIds, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) ) return NULL;

		$strSql = ' SELECT
						date_trunc ( \'month\', ts.actual_start_time )::date AS start_date,
						TO_CHAR ( ( ( EXTRACT ( EPOCH FROM ( SUM ( ts.actual_end_time - ts.actual_start_time ) ) ::interval ) ) || \'second\' ) ::interval, \'HH24.MI\' ) AS result
					FROM
						employee_training_sessions AS ets
						JOIN Training_sessions AS ts ON ( ts.id = ets.training_session_id AND ts.deleted_by IS NULL AND ts.actual_start_time IS NOT NULL AND ts.actual_start_time >= CAST ( date_trunc ( \'month\', ( CURRENT_DATE - \'11 month\' ::INTERVAL ) ) AS DATE ) AND ets.is_present = 1 AND ets.user_id IN ( \'' . implode( '\', \'', $arrintUserIds ) . '\' ) )
					GROUP BY
						date_trunc( \'month\', ts.actual_start_time )
					ORDER BY
						date_trunc( \'month\', ts.actual_start_time )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCountOfTrainingSessionAllotedAndAttendedByUserIds( $arrintUserIds, $objDatabase, $boolPresent = NULL, $strFromDate = NULL, $strToDate = NULL ) {

		if( false == valArr( array_filter( $arrintUserIds ) ) ) return [];

		$strWhere = ( true == $boolPresent ) ? ' AND ets.is_present = 1' : NULL;

		$strWhere .= ( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? ' AND ts.actual_start_time > \'' . $strFromDate . '\' AND ts.actual_start_time < \'' . $strToDate . '\'' : '';

		$strSql = 'SELECT
						ets.user_id,
						count(ets.id)
					FROM
						employee_training_sessions AS ets
						JOIN Training_sessions AS ts ON ts.id = ets.training_session_id
					WHERE
						ets.user_id IN ( \'' . implode( '\', \'', $arrintUserIds ) . '\' )
						AND ts.deleted_by IS NULL
						AND ts.actual_start_time IS NOT NULL ' . $strWhere . '
					GROUP BY
						ets.user_id
					ORDER BY
						ets.user_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchCountPendingRequestsByTrainingSessionIds( $arrintTrainingSessionIds, $objDatabase ) {

		if( false == valArr( $arrintTrainingSessionIds ) ) return NULL;

		$strSql = 'SELECT
						count ( ets.* ) AS pending_attendee_count,
						training_session_id
					FROM
						employee_training_sessions ets
					WHERE
						user_id = updated_by
						AND is_available = 0
						AND training_session_id IN( ' . implode( ',', $arrintTrainingSessionIds ) . ' )
					GROUP BY
						ets.training_session_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchEmployeesByTrainingSessionIds( $arrintTrainingSessionIds, $objDatabase, $boolFetchArray = false, $boolPresent = false ) {

		if( false == valArr( $arrintTrainingSessionIds ) ) return NULL;

		$strWhere = NULL;
		if( true == $boolPresent ) {
			$strWhere = ' AND ets.is_present = 1';
		}

		$strSql = 'SELECT
						e.id AS employee_id,
						e.name_full,
						e.email_address,
						ets.*
					FROM
						employee_training_sessions ets
						LEFT JOIN users u ON u.id = ets.user_id
						LEFT JOIN employees e ON e.id = u.employee_id
					WHERE
						ets.training_session_id IN ( ' . implode( ',', $arrintTrainingSessionIds ) . ' ) AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ets.is_available = 1' . $strWhere;

		if( true == $boolFetchArray ) {
			return fetchData( $strSql, $objDatabase );
		}

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeesByTrainingSessionId( $intTrainingSessionId, $objDatabase ) {

		if( true == is_null( $intTrainingSessionId ) ) return NULL;

		$strSql = 'SELECT
						e.id AS employee_id,
						e.name_full,
						ets.*
					FROM
						employee_training_sessions ets
						LEFT JOIN users u ON u.id = ets.user_id
						LEFT JOIN employees e ON e.id = u.employee_id
					WHERE
						ets.training_session_id = ' . ( int ) $intTrainingSessionId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ets.is_available = 1
						AND ets.is_present IS NOT NULL';

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionsByParentTrainingSessionId( $intTrainingSessionId, $objDatabase ) {

		$strSql = 'SELECT
						ets.user_id,ets.group_id
					FROM
						employee_training_sessions ets
					WHERE
						ets.training_session_id = ' . ( int ) $intTrainingSessionId . ' AND ets.group_id IS NULL';

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionsDetailsByTrainingSessionIds( $arrintTrainingSessionIds, $objDatabase ) {

		if( false == valArr( $arrintTrainingSessionIds ) ) return NULL;

		$strSql = 'SELECT ets.*,
						extract ( minute from age(ets.start_time, ts.actual_start_time) ) as late_by,
						ts.name as training_session_name,
						e.id as employee_id,
						e.name_first,
						e.name_last,
						e.preferred_name,
						e.email_address ,
						d.name as department_name,
						desg.name as designation_name
					FROM employee_training_sessions as ets
						JOIN training_sessions as ts on ets.training_session_id = ts.id
						JOIN users as u on ets.user_id = u.id
						JOIN employees as e on u.employee_id = e.id
						JOIN departments as d on e.department_id = d.id
						LEFT JOIN designations as desg on e.designation_id = desg.id
					WHERE
						ets.training_session_id IN ( ' . implode( ',', $arrintTrainingSessionIds ) . ' )
					ORDER
						By e.name_first';

		return self::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchAttendedCountEmployeeTrainingSessionsByUserId( $intUserId, $objDatabase ) {
		if( true == is_null( $intUserId ) ) return NULL;

		$strSql = ' SELECT
						count( ts.id )
					FROM
						employee_training_sessions ets
						JOIN training_sessions ts ON ( ts.id = ets.training_session_id )
						JOIN induction_sessions iss ON ( iss.id = ts.induction_session_id )
					WHERE
						ets.user_id = ' . ( int ) $intUserId . '
						AND ets.is_present = 1
						AND ets.is_available = 1
						AND ts.deleted_by IS NULL
						AND iss.deleted_by IS NULL
						AND iss.deleted_on IS NULL';

		$arrintCount = fetchData( $strSql, $objDatabase );
		return ( true == valArr( $arrintCount ) ) ? $arrintCount[0]['count'] : NULL;
	}

	public static function fetchEmployeeTrainingSessionDetailsByUserId( $intUserId, $objDatabase ) {
		if( true == is_null( $intUserId ) ) return NULL;

		$strSql = ' SELECT
						ts.name as training_session_name,
						ts.scheduled_start_time,
						ts.scheduled_end_time,
						ts.actual_start_time,
						ts.actual_end_time,
						ts.is_published,
						ets.is_present
					FROM
						employee_training_sessions ets
						JOIN training_sessions ts ON ( ts.id = ets.training_session_id )
						JOIN induction_sessions iss ON ( iss.id = ts.induction_session_id )
					WHERE
						ets.user_id = ' . ( int ) $intUserId . '
						AND ets.is_available = 1
						AND ts.deleted_by IS NULL
						AND iss.deleted_by IS NULL
						AND iss.deleted_on IS NULL
					ORDER BY iss.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAbsentEmployeeTrainingSessionsByInductionSessionIds( $arrintInductionSessionIds, $objDatabase ) {
		if( true == is_null( $arrintInductionSessionIds ) ) return NULL;

		$strSql = ' SELECT
						ets.*,
						iss.id as induction_session_id
					FROM
						employee_training_sessions ets
						JOIN training_sessions ts ON ( ts.id = ets.training_session_id )
						JOIN induction_sessions iss ON ( iss.id = ts.induction_session_id )
					WHERE
						ets.is_present = 0
						AND ts.deleted_by IS NULL
						AND iss.id IN ( ' . '\'' . implode( '\',\'', $arrintInductionSessionIds ) . '\'' . ')';

		return parent::fetchEmployeeTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionsByInductionSessionIdsByUserIds( $arrintInductionSessionIds, $arrintUserIds, $objDatabase ) {
		if( false == valArr( $arrintInductionSessionIds ) || false == valArr( $arrintUserIds ) ) return NULL;

		$strSql = ' SELECT
						ets.*,
						iss.id as induction_session_id
					FROM
						employee_training_sessions ets
						JOIN training_sessions ts ON ( ts.id = ets.training_session_id )
						JOIN induction_sessions iss ON ( iss.id = ts.induction_session_id )
					WHERE
						iss.id IN ( ' . implode( ',', $arrintInductionSessionIds ) . ' )
						AND ets.user_id IN ( ' . implode( ',', $arrintUserIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCurrentEmployeeTrainingSessions( $objDatabase, $arrintUserIds = NULL ) {
		$strWhere = NULL;
		if( true == valArr( $arrintUserIds ) ) {
			$strWhere = ' AND ets.user_id IN ( ' . implode( ',', $arrintUserIds ) . ' )';
		}

		$strSql = ' SELECT
						session_data.*
					FROM
						(
							SELECT
								ets.training_session_id,
								ets.user_id,
								ets.is_present,
								ts.induction_session_id,
								ts.name,
								row_number ( ) over ( partition BY ets.user_id, iss.id
														ORDER BY
													 ets.id DESC ) AS rank
							FROM
								employee_training_sessions ets
								JOIN training_sessions ts ON ( ets.training_session_id = ts.id )
								JOIN induction_sessions iss ON ( iss.id = ts.induction_session_id )
							WHERE
								ts.induction_session_id IS NOT NULL ' . $strWhere . '
						) session_data
					WHERE
						session_data.rank = 1';

		return fetchData( $strSql, $objDatabase );
	}

}
?>