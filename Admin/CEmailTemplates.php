<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmailTemplates
 * Do not add any new functions to this class.
 */

class CEmailTemplates extends CBaseEmailTemplates {

	public static function fetchAllEmailTemplates( $objDatabase ) {
		$strSql = 'SELECT * FROM email_templates ORDER BY order_num';
		return self::fetchEmailTemplates( $strSql, $objDatabase );
	}

	public static function fetchNextEmailTemplateOrderNum( $objDatabase ) {
		$strSql = 'SELECT MAX( order_num )+1 as order_number FROM email_templates';
		$arrintResponse	= fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['order_number'] ) ) {
			return $arrintResponse[0]['order_number'];
		} else {
			return 0;
		}
	}

	public static function fetchEmailTemplatesByIds( $arrintIds, $objDatabase ) {
		$strSql = 'SELECT * FROM email_templates WHERE id in ( ' . implode( ',', $arrintIds ) . ' ) ORDER BY name';
		return self::fetchEmailTemplates( $strSql, $objDatabase );
	}

}
?>