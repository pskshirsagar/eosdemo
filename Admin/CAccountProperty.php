<?php

class CAccountProperty extends CBaseAccountProperty {

	protected $m_strAccountName;
	protected $m_strPropertyName;
	protected $m_intIsDisabled;

	/**
	 * Get Functions
	 */

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['account_name'] ) ) {
			$this->setAccountName( $arrmixValues['account_name'] );
		}
		if( true == isset( $arrmixValues['property_name'] ) ) {
			$this->setPropertyName( $arrmixValues['property_name'] );
		}
		if( true == isset( $arrmixValues['is_disabled'] ) ) {
			$this->setIsDisabled( $arrmixValues['is_disabled'] );
		}
	}

	public function setAccountName( $strAccountName ) {
		$this->m_strAccountName = $strAccountName;
	}

	public function setPropertyName( $strPropertyName ) {
		$this->m_strPropertyName = $strPropertyName;
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->m_intIsDisabled = $intIsDisabled;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function insertIfAccountExists( $intCurrentUserId, $objDatabase ) {

		$strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.account_properties_id_seq\' )' : ( int ) $this->m_intId;

		$strSql = 'INSERT INTO
					  public.account_properties
					SELECT ' .
			$strId . ', ' .
			$this->sqlCid() . ', ' .
			$this->sqlAccountId() . ', ' .
			$this->sqlPropertyId() . ', ' .
			( int ) $intCurrentUserId . ', ' .
			$this->sqlCreatedOn() . ' FROM Accounts WHERE id = ' . $this->sqlAccountId() . ' RETURNING id;';

		return $this->executeSql( $strSql, $this, $objDatabase );
	}

}
?>