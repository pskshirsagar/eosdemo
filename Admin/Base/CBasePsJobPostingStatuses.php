<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsJobPostingStatuses
 * Do not add any new functions to this class.
 */

class CBasePsJobPostingStatuses extends CEosPluralBase {

	/**
	 * @return CPsJobPostingStatus[]
	 */
	public static function fetchPsJobPostingStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsJobPostingStatus', $objDatabase );
	}

	/**
	 * @return CPsJobPostingStatus
	 */
	public static function fetchPsJobPostingStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsJobPostingStatus', $objDatabase );
	}

	public static function fetchPsJobPostingStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_job_posting_statuses', $objDatabase );
	}

	public static function fetchPsJobPostingStatusById( $intId, $objDatabase ) {
		return self::fetchPsJobPostingStatus( sprintf( 'SELECT * FROM ps_job_posting_statuses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>