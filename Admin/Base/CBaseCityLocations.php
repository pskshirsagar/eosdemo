<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCityLocations
 * Do not add any new functions to this class.
 */

class CBaseCityLocations extends CEosPluralBase {

	/**
	 * @return CCityLocation[]
	 */
	public static function fetchCityLocations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCityLocation', $objDatabase );
	}

	/**
	 * @return CCityLocation
	 */
	public static function fetchCityLocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCityLocation', $objDatabase );
	}

	public static function fetchCityLocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'city_locations', $objDatabase );
	}

	public static function fetchCityLocationById( $intId, $objDatabase ) {
		return self::fetchCityLocation( sprintf( 'SELECT * FROM city_locations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCityLocationsByCityId( $intCityId, $objDatabase ) {
		return self::fetchCityLocations( sprintf( 'SELECT * FROM city_locations WHERE city_id = %d', ( int ) $intCityId ), $objDatabase );
	}

}
?>