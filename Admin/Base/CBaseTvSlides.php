<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTvSlides
 * Do not add any new functions to this class.
 */

class CBaseTvSlides extends CEosPluralBase {

	/**
	 * @return CTvSlide[]
	 */
	public static function fetchTvSlides( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTvSlide', $objDatabase );
	}

	/**
	 * @return CTvSlide
	 */
	public static function fetchTvSlide( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTvSlide', $objDatabase );
	}

	public static function fetchTvSlideCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tv_slides', $objDatabase );
	}

	public static function fetchTvSlideById( $intId, $objDatabase ) {
		return self::fetchTvSlide( sprintf( 'SELECT * FROM tv_slides WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTvSlidesByTemplateTypeId( $intTemplateTypeId, $objDatabase ) {
		return self::fetchTvSlides( sprintf( 'SELECT * FROM tv_slides WHERE template_type_id = %d', ( int ) $intTemplateTypeId ), $objDatabase );
	}

}
?>