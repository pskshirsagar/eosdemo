<?php

class CBaseTestAssignmentReferenceTypes extends CEosPluralBase {

	/**
	 * @return CTestAssignmentReferenceType[]
	 */
	public static function fetchTestAssignmentReferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestAssignmentReferenceType', $objDatabase );
	}

	/**
	 * @return CTestAssignmentReferenceType
	 */
	public static function fetchTestAssignmentReferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestAssignmentReferenceType', $objDatabase );
	}

	public static function fetchTestAssignmentReferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_assignment_reference_types', $objDatabase );
	}

	public static function fetchTestAssignmentReferenceTypeById( $intId, $objDatabase ) {
		return self::fetchTestAssignmentReferenceType( sprintf( 'SELECT * FROM test_assignment_reference_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>