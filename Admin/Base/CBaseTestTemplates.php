<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestTemplates
 * Do not add any new functions to this class.
 */

class CBaseTestTemplates extends CEosPluralBase {

	/**
	 * @return CTestTemplate[]
	 */
	public static function fetchTestTemplates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestTemplate', $objDatabase );
	}

	/**
	 * @return CTestTemplate
	 */
	public static function fetchTestTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestTemplate', $objDatabase );
	}

	public static function fetchTestTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_templates', $objDatabase );
	}

	public static function fetchTestTemplateById( $intId, $objDatabase ) {
		return self::fetchTestTemplate( sprintf( 'SELECT * FROM test_templates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestTemplatesByTestLevelTypeId( $intTestLevelTypeId, $objDatabase ) {
		return self::fetchTestTemplates( sprintf( 'SELECT * FROM test_templates WHERE test_level_type_id = %d', ( int ) $intTestLevelTypeId ), $objDatabase );
	}

}
?>