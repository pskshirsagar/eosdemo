<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEeocDetails
 * Do not add any new functions to this class.
 */

class CBaseEeocDetails extends CEosPluralBase {

	/**
	 * @return CEeocDetail[]
	 */
	public static function fetchEeocDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEeocDetail::class, $objDatabase );
	}

	/**
	 * @return CEeocDetail
	 */
	public static function fetchEeocDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEeocDetail::class, $objDatabase );
	}

	public static function fetchEeocDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'eeoc_details', $objDatabase );
	}

	public static function fetchEeocDetailById( $intId, $objDatabase ) {
		return self::fetchEeocDetail( sprintf( 'SELECT * FROM eeoc_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEeocDetailsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEeocDetails( sprintf( 'SELECT * FROM eeoc_details WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>