<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSystemSettings
 * Do not add any new functions to this class.
 */

class CBaseSystemSettings extends CEosPluralBase {

	/**
	 * @return CSystemSetting[]
	 */
	public static function fetchSystemSettings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSystemSetting', $objDatabase );
	}

	/**
	 * @return CSystemSetting
	 */
	public static function fetchSystemSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSystemSetting', $objDatabase );
	}

	public static function fetchSystemSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'system_settings', $objDatabase );
	}

	public static function fetchSystemSettingById( $intId, $objDatabase ) {
		return self::fetchSystemSetting( sprintf( 'SELECT * FROM system_settings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>