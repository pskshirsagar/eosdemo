<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserFilesNotifications
 * Do not add any new functions to this class.
 */

class CBaseUserFilesNotifications extends CEosPluralBase {

	/**
	 * @return CUserFilesNotification[]
	 */
	public static function fetchUserFilesNotifications( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserFilesNotification', $objDatabase );
	}

	/**
	 * @return CUserFilesNotification
	 */
	public static function fetchUserFilesNotification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserFilesNotification', $objDatabase );
	}

	public static function fetchUserFilesNotificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_files_notifications', $objDatabase );
	}

	public static function fetchUserFilesNotificationById( $intId, $objDatabase ) {
		return self::fetchUserFilesNotification( sprintf( 'SELECT * FROM user_files_notifications WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserFilesNotificationsBySvnRepositoryId( $intSvnRepositoryId, $objDatabase ) {
		return self::fetchUserFilesNotifications( sprintf( 'SELECT * FROM user_files_notifications WHERE svn_repository_id = %d', ( int ) $intSvnRepositoryId ), $objDatabase );
	}

	public static function fetchUserFilesNotificationsByUserId( $intUserId, $objDatabase ) {
		return self::fetchUserFilesNotifications( sprintf( 'SELECT * FROM user_files_notifications WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

}
?>