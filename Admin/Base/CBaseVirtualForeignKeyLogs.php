<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CVirtualForeignKeyLogs
 * Do not add any new functions to this class.
 */

class CBaseVirtualForeignKeyLogs extends CEosPluralBase {

	/**
	 * @return CVirtualForeignKeyLog[]
	 */
	public static function fetchVirtualForeignKeyLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CVirtualForeignKeyLog', $objDatabase );
	}

	/**
	 * @return CVirtualForeignKeyLog
	 */
	public static function fetchVirtualForeignKeyLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CVirtualForeignKeyLog', $objDatabase );
	}

	public static function fetchVirtualForeignKeyLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'virtual_foreign_key_logs', $objDatabase );
	}

	public static function fetchVirtualForeignKeyLogById( $intId, $objDatabase ) {
		return self::fetchVirtualForeignKeyLog( sprintf( 'SELECT * FROM virtual_foreign_key_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchVirtualForeignKeyLogsByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchVirtualForeignKeyLogs( sprintf( 'SELECT * FROM virtual_foreign_key_logs WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

	public static function fetchVirtualForeignKeyLogsByVirtualForeignKeyId( $intVirtualForeignKeyId, $objDatabase ) {
		return self::fetchVirtualForeignKeyLogs( sprintf( 'SELECT * FROM virtual_foreign_key_logs WHERE virtual_foreign_key_id = %d', ( int ) $intVirtualForeignKeyId ), $objDatabase );
	}

}
?>