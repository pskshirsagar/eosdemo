<?php

class CBaseActivityLog extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.activity_logs';

	protected $m_intId;
	protected $m_intActivityTypeId;
	protected $m_intProjectId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strReferenceTableName;
	protected $m_intReferenceId;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['activity_type_id'] ) && $boolDirectSet ) $this->set( 'm_intActivityTypeId', trim( $arrValues['activity_type_id'] ) ); elseif( isset( $arrValues['activity_type_id'] ) ) $this->setActivityTypeId( $arrValues['activity_type_id'] );
		if( isset( $arrValues['project_id'] ) && $boolDirectSet ) $this->set( 'm_intProjectId', trim( $arrValues['project_id'] ) ); elseif( isset( $arrValues['project_id'] ) ) $this->setProjectId( $arrValues['project_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['reference_table_name'] ) && $boolDirectSet ) $this->set( 'm_strReferenceTableName', trim( $arrValues['reference_table_name'] ) ); elseif( isset( $arrValues['reference_table_name'] ) ) $this->setReferenceTableName( $arrValues['reference_table_name'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setActivityTypeId( $intActivityTypeId ) {
		$this->set( 'm_intActivityTypeId', CStrings::strToIntDef( $intActivityTypeId, NULL, false ) );
	}

	public function getActivityTypeId() {
		return $this->m_intActivityTypeId;
	}

	public function sqlActivityTypeId() {
		return ( true == isset( $this->m_intActivityTypeId ) ) ? ( string ) $this->m_intActivityTypeId : 'NULL';
	}

	public function setProjectId( $intProjectId ) {
		$this->set( 'm_intProjectId', CStrings::strToIntDef( $intProjectId, NULL, false ) );
	}

	public function getProjectId() {
		return $this->m_intProjectId;
	}

	public function sqlProjectId() {
		return ( true == isset( $this->m_intProjectId ) ) ? ( string ) $this->m_intProjectId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setReferenceTableName( $strReferenceTableName ) {
		$this->set( 'm_strReferenceTableName', CStrings::strTrimDef( $strReferenceTableName, 30, NULL, true ) );
	}

	public function getReferenceTableName() {
		return $this->m_strReferenceTableName;
	}

	public function sqlReferenceTableName() {
		return ( true == isset( $this->m_strReferenceTableName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReferenceTableName ) : '\'' . addslashes( $this->m_strReferenceTableName ) . '\'' ) : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, activity_type_id, project_id, details, created_by, created_on, reference_table_name, reference_id )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlActivityTypeId() . ', ' .
		          $this->sqlProjectId() . ', ' .
		          $this->sqlDetails() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlReferenceTableName() . ', ' .
		          $this->sqlReferenceId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' activity_type_id = ' . $this->sqlActivityTypeId(). ',' ; } elseif( true == array_key_exists( 'ActivityTypeId', $this->getChangedColumns() ) ) { $strSql .= ' activity_type_id = ' . $this->sqlActivityTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_id = ' . $this->sqlProjectId(). ',' ; } elseif( true == array_key_exists( 'ProjectId', $this->getChangedColumns() ) ) { $strSql .= ' project_id = ' . $this->sqlProjectId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_table_name = ' . $this->sqlReferenceTableName(). ',' ; } elseif( true == array_key_exists( 'ReferenceTableName', $this->getChangedColumns() ) ) { $strSql .= ' reference_table_name = ' . $this->sqlReferenceTableName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'activity_type_id' => $this->getActivityTypeId(),
			'project_id' => $this->getProjectId(),
			'details' => $this->getDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'reference_table_name' => $this->getReferenceTableName(),
			'reference_id' => $this->getReferenceId()
		);
	}

}
?>