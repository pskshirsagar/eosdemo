<?php

class CBasePurchaseRequestAction extends CEosSingularBase {

	const TABLE_NAME = 'public.purchase_request_actions';

	protected $m_intId;
	protected $m_intPurchaseRequestId;
	protected $m_strActionType;
	protected $m_strSupportData;
	protected $m_jsonSupportData;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['purchase_request_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchaseRequestId', trim( $arrValues['purchase_request_id'] ) ); elseif( isset( $arrValues['purchase_request_id'] ) ) $this->setPurchaseRequestId( $arrValues['purchase_request_id'] );
		if( isset( $arrValues['action_type'] ) && $boolDirectSet ) $this->set( 'm_strActionType', trim( stripcslashes( $arrValues['action_type'] ) ) ); elseif( isset( $arrValues['action_type'] ) ) $this->setActionType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_type'] ) : $arrValues['action_type'] );
		if( isset( $arrValues['support_data'] ) ) $this->set( 'm_strSupportData', trim( $arrValues['support_data'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPurchaseRequestId( $intPurchaseRequestId ) {
		$this->set( 'm_intPurchaseRequestId', CStrings::strToIntDef( $intPurchaseRequestId, NULL, false ) );
	}

	public function getPurchaseRequestId() {
		return $this->m_intPurchaseRequestId;
	}

	public function sqlPurchaseRequestId() {
		return ( true == isset( $this->m_intPurchaseRequestId ) ) ? ( string ) $this->m_intPurchaseRequestId : 'NULL';
	}

	public function setActionType( $strActionType ) {
		$this->set( 'm_strActionType', CStrings::strTrimDef( $strActionType, -1, NULL, true ) );
	}

	public function getActionType() {
		return $this->m_strActionType;
	}

	public function sqlActionType() {
		return ( true == isset( $this->m_strActionType ) ) ? '\'' . addslashes( $this->m_strActionType ) . '\'' : 'NULL';
	}

	public function setSupportData( $jsonSupportData ) {
		if( true == valObj( $jsonSupportData, 'stdClass' ) ) {
			$this->set( 'm_jsonSupportData', $jsonSupportData );
		} elseif( true == valJsonString( $jsonSupportData ) ) {
			$this->set( 'm_jsonSupportData', CStrings::strToJson( $jsonSupportData ) );
		} else {
			$this->set( 'm_jsonSupportData', NULL ); 
		}
		unset( $this->m_strSupportData );
	}

	public function getSupportData() {
		if( true == isset( $this->m_strSupportData ) ) {
			$this->m_jsonSupportData = CStrings::strToJson( $this->m_strSupportData );
			unset( $this->m_strSupportData );
		}
		return $this->m_jsonSupportData;
	}

	public function sqlSupportData() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getSupportData() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getSupportData() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, purchase_request_id, action_type, support_data, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPurchaseRequestId() . ', ' .
 						$this->sqlActionType() . ', ' .
 						$this->sqlSupportData() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; } elseif( true == array_key_exists( 'PurchaseRequestId', $this->getChangedColumns() ) ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_type = ' . $this->sqlActionType() . ','; } elseif( true == array_key_exists( 'ActionType', $this->getChangedColumns() ) ) { $strSql .= ' action_type = ' . $this->sqlActionType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' support_data = ' . $this->sqlSupportData() . ','; } elseif( true == array_key_exists( 'SupportData', $this->getChangedColumns() ) ) { $strSql .= ' support_data = ' . $this->sqlSupportData() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'purchase_request_id' => $this->getPurchaseRequestId(),
			'action_type' => $this->getActionType(),
			'support_data' => $this->getSupportData(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>