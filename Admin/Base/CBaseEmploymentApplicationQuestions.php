<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmploymentApplicationQuestions
 * Do not add any new functions to this class.
 */

class CBaseEmploymentApplicationQuestions extends CEosPluralBase {

	/**
	 * @return CEmploymentApplicationQuestion[]
	 */
	public static function fetchEmploymentApplicationQuestions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmploymentApplicationQuestion', $objDatabase );
	}

	/**
	 * @return CEmploymentApplicationQuestion
	 */
	public static function fetchEmploymentApplicationQuestion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmploymentApplicationQuestion', $objDatabase );
	}

	public static function fetchEmploymentApplicationQuestionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employment_application_questions', $objDatabase );
	}

	public static function fetchEmploymentApplicationQuestionById( $intId, $objDatabase ) {
		return self::fetchEmploymentApplicationQuestion( sprintf( 'SELECT * FROM employment_application_questions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmploymentApplicationQuestionsByEmploymentApplicationId( $intEmploymentApplicationId, $objDatabase ) {
		return self::fetchEmploymentApplicationQuestions( sprintf( 'SELECT * FROM employment_application_questions WHERE employment_application_id = %d', ( int ) $intEmploymentApplicationId ), $objDatabase );
	}

}
?>