<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationRejectionReasons
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationRejectionReasons extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationRejectionReason[]
	 */
	public static function fetchEmployeeApplicationRejectionReasons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationRejectionReason', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationRejectionReason
	 */
	public static function fetchEmployeeApplicationRejectionReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationRejectionReason', $objDatabase );
	}

	public static function fetchEmployeeApplicationRejectionReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_rejection_reasons', $objDatabase );
	}

	public static function fetchEmployeeApplicationRejectionReasonById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationRejectionReason( sprintf( 'SELECT * FROM employee_application_rejection_reasons WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>