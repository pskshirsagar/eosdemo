<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseReportDetails
 * Do not add any new functions to this class.
 */

class CBaseReleaseReportDetails extends CEosPluralBase {

	/**
	 * @return CReleaseReportDetail[]
	 */
	public static function fetchReleaseReportDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReleaseReportDetail::class, $objDatabase );
	}

	/**
	 * @return CReleaseReportDetail
	 */
	public static function fetchReleaseReportDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReleaseReportDetail::class, $objDatabase );
	}

	public static function fetchReleaseReportDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'release_report_details', $objDatabase );
	}

	public static function fetchReleaseReportDetailById( $intId, $objDatabase ) {
		return self::fetchReleaseReportDetail( sprintf( 'SELECT * FROM release_report_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReleaseReportDetailsByTaskReleaseId( $intTaskReleaseId, $objDatabase ) {
		return self::fetchReleaseReportDetails( sprintf( 'SELECT * FROM release_report_details WHERE task_release_id = %d', ( int ) $intTaskReleaseId ), $objDatabase );
	}

}
?>