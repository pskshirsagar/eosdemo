<?php

class CBaseActionTypeResult extends CEosSingularBase {

	const TABLE_NAME = 'public.action_type_results';

	protected $m_intActionTypeId;
	protected $m_intActionResultId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['action_type_id'] ) && $boolDirectSet ) $this->set( 'm_intActionTypeId', trim( $arrValues['action_type_id'] ) ); elseif( isset( $arrValues['action_type_id'] ) ) $this->setActionTypeId( $arrValues['action_type_id'] );
		if( isset( $arrValues['action_result_id'] ) && $boolDirectSet ) $this->set( 'm_intActionResultId', trim( $arrValues['action_result_id'] ) ); elseif( isset( $arrValues['action_result_id'] ) ) $this->setActionResultId( $arrValues['action_result_id'] );
		$this->m_boolInitialized = true;
	}

	public function setActionTypeId( $intActionTypeId ) {
		$this->set( 'm_intActionTypeId', CStrings::strToIntDef( $intActionTypeId, NULL, false ) );
	}

	public function getActionTypeId() {
		return $this->m_intActionTypeId;
	}

	public function sqlActionTypeId() {
		return ( true == isset( $this->m_intActionTypeId ) ) ? ( string ) $this->m_intActionTypeId : 'NULL';
	}

	public function setActionResultId( $intActionResultId ) {
		$this->set( 'm_intActionResultId', CStrings::strToIntDef( $intActionResultId, NULL, false ) );
	}

	public function getActionResultId() {
		return $this->m_intActionResultId;
	}

	public function sqlActionResultId() {
		return ( true == isset( $this->m_intActionResultId ) ) ? ( string ) $this->m_intActionResultId : 'NULL';
	}

	public function toArray() {
		return array(
			'action_type_id' => $this->getActionTypeId(),
			'action_result_id' => $this->getActionResultId()
		);
	}

}
?>