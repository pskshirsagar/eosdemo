<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeVacationSchedules
 * Do not add any new functions to this class.
 */

class CBaseEmployeeVacationSchedules extends CEosPluralBase {

	/**
	 * @return CEmployeeVacationSchedule[]
	 */
	public static function fetchEmployeeVacationSchedules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeVacationSchedule', $objDatabase );
	}

	/**
	 * @return CEmployeeVacationSchedule
	 */
	public static function fetchEmployeeVacationSchedule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeVacationSchedule', $objDatabase );
	}

	public static function fetchEmployeeVacationScheduleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_vacation_schedules', $objDatabase );
	}

	public static function fetchEmployeeVacationScheduleById( $intId, $objDatabase ) {
		return self::fetchEmployeeVacationSchedule( sprintf( 'SELECT * FROM employee_vacation_schedules WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeVacationSchedulesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeVacationSchedules( sprintf( 'SELECT * FROM employee_vacation_schedules WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>