<?php

class CBaseReimbursementStatusTypes extends CEosPluralBase {

    public static function fetchReimbursementStatusTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CReimbursementStatusType', $objDatabase );
    }

    public static function fetchReimbursementStatusType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CReimbursementStatusType', $objDatabase );
    }

    public static function fetchReimbursementStatusTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'employee_pay_status_type', $objDatabase );
    }

    public static function fetchReimbursementStatusTypeById( $intId, $objDatabase ) {
        return self::fetchReimbursementStatusType( sprintf( 'SELECT * FROM reimbursement_status_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>