<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationScores
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationScores extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationScore[]
	 */
	public static function fetchEmployeeApplicationScores( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationScore', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationScore
	 */
	public static function fetchEmployeeApplicationScore( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationScore', $objDatabase );
	}

	public static function fetchEmployeeApplicationScoreCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_scores', $objDatabase );
	}

	public static function fetchEmployeeApplicationScoreById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationScore( sprintf( 'SELECT * FROM employee_application_scores WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationScoresByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchEmployeeApplicationScores( sprintf( 'SELECT * FROM employee_application_scores WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationScoresByEmployeeApplicationScoreTypeId( $intEmployeeApplicationScoreTypeId, $objDatabase ) {
		return self::fetchEmployeeApplicationScores( sprintf( 'SELECT * FROM employee_application_scores WHERE employee_application_score_type_id = %d', ( int ) $intEmployeeApplicationScoreTypeId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationScoresByEmployeeApplicationInterviewId( $intEmployeeApplicationInterviewId, $objDatabase ) {
		return self::fetchEmployeeApplicationScores( sprintf( 'SELECT * FROM employee_application_scores WHERE employee_application_interview_id = %d', ( int ) $intEmployeeApplicationInterviewId ), $objDatabase );
	}

}
?>