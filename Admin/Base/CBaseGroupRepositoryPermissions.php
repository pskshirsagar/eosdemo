<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroupRepositoryPermissions
 * Do not add any new functions to this class.
 */

class CBaseGroupRepositoryPermissions extends CEosPluralBase {

	/**
	 * @return CGroupRepositoryPermission[]
	 */
	public static function fetchGroupRepositoryPermissions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CGroupRepositoryPermission', $objDatabase );
	}

	/**
	 * @return CGroupRepositoryPermission
	 */
	public static function fetchGroupRepositoryPermission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGroupRepositoryPermission', $objDatabase );
	}

	public static function fetchGroupRepositoryPermissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'group_repository_permissions', $objDatabase );
	}

	public static function fetchGroupRepositoryPermissionById( $intId, $objDatabase ) {
		return self::fetchGroupRepositoryPermission( sprintf( 'SELECT * FROM group_repository_permissions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchGroupRepositoryPermissionsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchGroupRepositoryPermissions( sprintf( 'SELECT * FROM group_repository_permissions WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

	public static function fetchGroupRepositoryPermissionsByRepositoryId( $intRepositoryId, $objDatabase ) {
		return self::fetchGroupRepositoryPermissions( sprintf( 'SELECT * FROM group_repository_permissions WHERE repository_id = %d', ( int ) $intRepositoryId ), $objDatabase );
	}

}
?>