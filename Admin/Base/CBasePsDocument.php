<?php

class CBasePsDocument extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_documents';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPsLeadId;
	protected $m_intEmployeeId;
	protected $m_intEmployeeApplicationId;
	protected $m_intPsDocumentTypeId;
	protected $m_intPsDocumentCategoryId;
	protected $m_intPsProductId;
	protected $m_intFileExtensionId;
	protected $m_intTrainingSessionId;
	protected $m_intAssociatedPsDocumentId;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_strFileName;
	protected $m_intIsExported;
	protected $m_intIsPublished;
	protected $m_intIsShared;
	protected $m_intOrderNum;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intLastUpdatedBy;
	protected $m_strLastUpdatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsExported = '0';
		$this->m_intIsPublished = '0';
		$this->m_intIsShared = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['employee_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationId', trim( $arrValues['employee_application_id'] ) ); elseif( isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( isset( $arrValues['ps_document_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsDocumentTypeId', trim( $arrValues['ps_document_type_id'] ) ); elseif( isset( $arrValues['ps_document_type_id'] ) ) $this->setPsDocumentTypeId( $arrValues['ps_document_type_id'] );
		if( isset( $arrValues['ps_document_category_id'] ) && $boolDirectSet ) $this->set( 'm_intPsDocumentCategoryId', trim( $arrValues['ps_document_category_id'] ) ); elseif( isset( $arrValues['ps_document_category_id'] ) ) $this->setPsDocumentCategoryId( $arrValues['ps_document_category_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['file_extension_id'] ) && $boolDirectSet ) $this->set( 'm_intFileExtensionId', trim( $arrValues['file_extension_id'] ) ); elseif( isset( $arrValues['file_extension_id'] ) ) $this->setFileExtensionId( $arrValues['file_extension_id'] );
		if( isset( $arrValues['training_session_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingSessionId', trim( $arrValues['training_session_id'] ) ); elseif( isset( $arrValues['training_session_id'] ) ) $this->setTrainingSessionId( $arrValues['training_session_id'] );
		if( isset( $arrValues['associated_ps_document_id'] ) && $boolDirectSet ) $this->set( 'm_intAssociatedPsDocumentId', trim( $arrValues['associated_ps_document_id'] ) ); elseif( isset( $arrValues['associated_ps_document_id'] ) ) $this->setAssociatedPsDocumentId( $arrValues['associated_ps_document_id'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['is_exported'] ) && $boolDirectSet ) $this->set( 'm_intIsExported', trim( $arrValues['is_exported'] ) ); elseif( isset( $arrValues['is_exported'] ) ) $this->setIsExported( $arrValues['is_exported'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_shared'] ) && $boolDirectSet ) $this->set( 'm_intIsShared', trim( $arrValues['is_shared'] ) ); elseif( isset( $arrValues['is_shared'] ) ) $this->setIsShared( $arrValues['is_shared'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['last_updated_by'] ) && $boolDirectSet ) $this->set( 'm_intLastUpdatedBy', trim( $arrValues['last_updated_by'] ) ); elseif( isset( $arrValues['last_updated_by'] ) ) $this->setLastUpdatedBy( $arrValues['last_updated_by'] );
		if( isset( $arrValues['last_updated_on'] ) && $boolDirectSet ) $this->set( 'm_strLastUpdatedOn', trim( $arrValues['last_updated_on'] ) ); elseif( isset( $arrValues['last_updated_on'] ) ) $this->setLastUpdatedOn( $arrValues['last_updated_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setEmployeeApplicationId( $intEmployeeApplicationId ) {
		$this->set( 'm_intEmployeeApplicationId', CStrings::strToIntDef( $intEmployeeApplicationId, NULL, false ) );
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function sqlEmployeeApplicationId() {
		return ( true == isset( $this->m_intEmployeeApplicationId ) ) ? ( string ) $this->m_intEmployeeApplicationId : 'NULL';
	}

	public function setPsDocumentTypeId( $intPsDocumentTypeId ) {
		$this->set( 'm_intPsDocumentTypeId', CStrings::strToIntDef( $intPsDocumentTypeId, NULL, false ) );
	}

	public function getPsDocumentTypeId() {
		return $this->m_intPsDocumentTypeId;
	}

	public function sqlPsDocumentTypeId() {
		return ( true == isset( $this->m_intPsDocumentTypeId ) ) ? ( string ) $this->m_intPsDocumentTypeId : 'NULL';
	}

	public function setPsDocumentCategoryId( $intPsDocumentCategoryId ) {
		$this->set( 'm_intPsDocumentCategoryId', CStrings::strToIntDef( $intPsDocumentCategoryId, NULL, false ) );
	}

	public function getPsDocumentCategoryId() {
		return $this->m_intPsDocumentCategoryId;
	}

	public function sqlPsDocumentCategoryId() {
		return ( true == isset( $this->m_intPsDocumentCategoryId ) ) ? ( string ) $this->m_intPsDocumentCategoryId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setFileExtensionId( $intFileExtensionId ) {
		$this->set( 'm_intFileExtensionId', CStrings::strToIntDef( $intFileExtensionId, NULL, false ) );
	}

	public function getFileExtensionId() {
		return $this->m_intFileExtensionId;
	}

	public function sqlFileExtensionId() {
		return ( true == isset( $this->m_intFileExtensionId ) ) ? ( string ) $this->m_intFileExtensionId : 'NULL';
	}

	public function setTrainingSessionId( $intTrainingSessionId ) {
		$this->set( 'm_intTrainingSessionId', CStrings::strToIntDef( $intTrainingSessionId, NULL, false ) );
	}

	public function getTrainingSessionId() {
		return $this->m_intTrainingSessionId;
	}

	public function sqlTrainingSessionId() {
		return ( true == isset( $this->m_intTrainingSessionId ) ) ? ( string ) $this->m_intTrainingSessionId : 'NULL';
	}

	public function setAssociatedPsDocumentId( $intAssociatedPsDocumentId ) {
		$this->set( 'm_intAssociatedPsDocumentId', CStrings::strToIntDef( $intAssociatedPsDocumentId, NULL, false ) );
	}

	public function getAssociatedPsDocumentId() {
		return $this->m_intAssociatedPsDocumentId;
	}

	public function sqlAssociatedPsDocumentId() {
		return ( true == isset( $this->m_intAssociatedPsDocumentId ) ) ? ( string ) $this->m_intAssociatedPsDocumentId : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setIsExported( $intIsExported ) {
		$this->set( 'm_intIsExported', CStrings::strToIntDef( $intIsExported, NULL, false ) );
	}

	public function getIsExported() {
		return $this->m_intIsExported;
	}

	public function sqlIsExported() {
		return ( true == isset( $this->m_intIsExported ) ) ? ( string ) $this->m_intIsExported : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setIsShared( $intIsShared ) {
		$this->set( 'm_intIsShared', CStrings::strToIntDef( $intIsShared, NULL, false ) );
	}

	public function getIsShared() {
		return $this->m_intIsShared;
	}

	public function sqlIsShared() {
		return ( true == isset( $this->m_intIsShared ) ) ? ( string ) $this->m_intIsShared : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setLastUpdatedBy( $intLastUpdatedBy ) {
		$this->set( 'm_intLastUpdatedBy', CStrings::strToIntDef( $intLastUpdatedBy, NULL, false ) );
	}

	public function getLastUpdatedBy() {
		return $this->m_intLastUpdatedBy;
	}

	public function sqlLastUpdatedBy() {
		return ( true == isset( $this->m_intLastUpdatedBy ) ) ? ( string ) $this->m_intLastUpdatedBy : 'NULL';
	}

	public function setLastUpdatedOn( $strLastUpdatedOn ) {
		$this->set( 'm_strLastUpdatedOn', CStrings::strTrimDef( $strLastUpdatedOn, -1, NULL, true ) );
	}

	public function getLastUpdatedOn() {
		return $this->m_strLastUpdatedOn;
	}

	public function sqlLastUpdatedOn() {
		return ( true == isset( $this->m_strLastUpdatedOn ) ) ? '\'' . $this->m_strLastUpdatedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ps_lead_id, employee_id, employee_application_id, ps_document_type_id, ps_document_category_id, ps_product_id, file_extension_id, training_session_id, associated_ps_document_id, title, description, file_name, is_exported, is_published, is_shared, order_num, approved_by, approved_on, last_updated_by, last_updated_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPsLeadId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlEmployeeApplicationId() . ', ' .
 						$this->sqlPsDocumentTypeId() . ', ' .
 						$this->sqlPsDocumentCategoryId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlFileExtensionId() . ', ' .
 						$this->sqlTrainingSessionId() . ', ' .
 						$this->sqlAssociatedPsDocumentId() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlIsExported() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsShared() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlLastUpdatedBy() . ', ' .
 						$this->sqlLastUpdatedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_document_type_id = ' . $this->sqlPsDocumentTypeId() . ','; } elseif( true == array_key_exists( 'PsDocumentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ps_document_type_id = ' . $this->sqlPsDocumentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_document_category_id = ' . $this->sqlPsDocumentCategoryId() . ','; } elseif( true == array_key_exists( 'PsDocumentCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' ps_document_category_id = ' . $this->sqlPsDocumentCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_extension_id = ' . $this->sqlFileExtensionId() . ','; } elseif( true == array_key_exists( 'FileExtensionId', $this->getChangedColumns() ) ) { $strSql .= ' file_extension_id = ' . $this->sqlFileExtensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_session_id = ' . $this->sqlTrainingSessionId() . ','; } elseif( true == array_key_exists( 'TrainingSessionId', $this->getChangedColumns() ) ) { $strSql .= ' training_session_id = ' . $this->sqlTrainingSessionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_ps_document_id = ' . $this->sqlAssociatedPsDocumentId() . ','; } elseif( true == array_key_exists( 'AssociatedPsDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' associated_ps_document_id = ' . $this->sqlAssociatedPsDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_exported = ' . $this->sqlIsExported() . ','; } elseif( true == array_key_exists( 'IsExported', $this->getChangedColumns() ) ) { $strSql .= ' is_exported = ' . $this->sqlIsExported() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_shared = ' . $this->sqlIsShared() . ','; } elseif( true == array_key_exists( 'IsShared', $this->getChangedColumns() ) ) { $strSql .= ' is_shared = ' . $this->sqlIsShared() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_updated_by = ' . $this->sqlLastUpdatedBy() . ','; } elseif( true == array_key_exists( 'LastUpdatedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_updated_by = ' . $this->sqlLastUpdatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_updated_on = ' . $this->sqlLastUpdatedOn() . ','; } elseif( true == array_key_exists( 'LastUpdatedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_updated_on = ' . $this->sqlLastUpdatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ps_lead_id' => $this->getPsLeadId(),
			'employee_id' => $this->getEmployeeId(),
			'employee_application_id' => $this->getEmployeeApplicationId(),
			'ps_document_type_id' => $this->getPsDocumentTypeId(),
			'ps_document_category_id' => $this->getPsDocumentCategoryId(),
			'ps_product_id' => $this->getPsProductId(),
			'file_extension_id' => $this->getFileExtensionId(),
			'training_session_id' => $this->getTrainingSessionId(),
			'associated_ps_document_id' => $this->getAssociatedPsDocumentId(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'file_name' => $this->getFileName(),
			'is_exported' => $this->getIsExported(),
			'is_published' => $this->getIsPublished(),
			'is_shared' => $this->getIsShared(),
			'order_num' => $this->getOrderNum(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'last_updated_by' => $this->getLastUpdatedBy(),
			'last_updated_on' => $this->getLastUpdatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>