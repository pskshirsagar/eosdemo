<?php

class CBaseEmployeeAttendanceRemark extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_attendance_remarks';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_strNote;
	protected $m_strExpectedInTime;
	protected $m_strRequestedDatetime;
	protected $m_intIsLate;
	protected $m_intIsOnLeave;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsLate = '0';
		$this->m_intIsOnLeave = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['expected_in_time'] ) && $boolDirectSet ) $this->set( 'm_strExpectedInTime', trim( $arrValues['expected_in_time'] ) ); elseif( isset( $arrValues['expected_in_time'] ) ) $this->setExpectedInTime( $arrValues['expected_in_time'] );
		if( isset( $arrValues['requested_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequestedDatetime', trim( $arrValues['requested_datetime'] ) ); elseif( isset( $arrValues['requested_datetime'] ) ) $this->setRequestedDatetime( $arrValues['requested_datetime'] );
		if( isset( $arrValues['is_late'] ) && $boolDirectSet ) $this->set( 'm_intIsLate', trim( $arrValues['is_late'] ) ); elseif( isset( $arrValues['is_late'] ) ) $this->setIsLate( $arrValues['is_late'] );
		if( isset( $arrValues['is_on_leave'] ) && $boolDirectSet ) $this->set( 'm_intIsOnLeave', trim( $arrValues['is_on_leave'] ) ); elseif( isset( $arrValues['is_on_leave'] ) ) $this->setIsOnLeave( $arrValues['is_on_leave'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, -1, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setExpectedInTime( $strExpectedInTime ) {
		$this->set( 'm_strExpectedInTime', CStrings::strTrimDef( $strExpectedInTime, -1, NULL, true ) );
	}

	public function getExpectedInTime() {
		return $this->m_strExpectedInTime;
	}

	public function sqlExpectedInTime() {
		return ( true == isset( $this->m_strExpectedInTime ) ) ? '\'' . $this->m_strExpectedInTime . '\'' : 'NULL';
	}

	public function setRequestedDatetime( $strRequestedDatetime ) {
		$this->set( 'm_strRequestedDatetime', CStrings::strTrimDef( $strRequestedDatetime, -1, NULL, true ) );
	}

	public function getRequestedDatetime() {
		return $this->m_strRequestedDatetime;
	}

	public function sqlRequestedDatetime() {
		return ( true == isset( $this->m_strRequestedDatetime ) ) ? '\'' . $this->m_strRequestedDatetime . '\'' : 'NULL';
	}

	public function setIsLate( $intIsLate ) {
		$this->set( 'm_intIsLate', CStrings::strToIntDef( $intIsLate, NULL, false ) );
	}

	public function getIsLate() {
		return $this->m_intIsLate;
	}

	public function sqlIsLate() {
		return ( true == isset( $this->m_intIsLate ) ) ? ( string ) $this->m_intIsLate : '0';
	}

	public function setIsOnLeave( $intIsOnLeave ) {
		$this->set( 'm_intIsOnLeave', CStrings::strToIntDef( $intIsOnLeave, NULL, false ) );
	}

	public function getIsOnLeave() {
		return $this->m_intIsOnLeave;
	}

	public function sqlIsOnLeave() {
		return ( true == isset( $this->m_intIsOnLeave ) ) ? ( string ) $this->m_intIsOnLeave : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, note, expected_in_time, requested_datetime, is_late, is_on_leave, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlNote() . ', ' .
 						$this->sqlExpectedInTime() . ', ' .
 						$this->sqlRequestedDatetime() . ', ' .
 						$this->sqlIsLate() . ', ' .
 						$this->sqlIsOnLeave() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote() . ','; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_in_time = ' . $this->sqlExpectedInTime() . ','; } elseif( true == array_key_exists( 'ExpectedInTime', $this->getChangedColumns() ) ) { $strSql .= ' expected_in_time = ' . $this->sqlExpectedInTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_datetime = ' . $this->sqlRequestedDatetime() . ','; } elseif( true == array_key_exists( 'RequestedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' requested_datetime = ' . $this->sqlRequestedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_late = ' . $this->sqlIsLate() . ','; } elseif( true == array_key_exists( 'IsLate', $this->getChangedColumns() ) ) { $strSql .= ' is_late = ' . $this->sqlIsLate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_on_leave = ' . $this->sqlIsOnLeave() . ','; } elseif( true == array_key_exists( 'IsOnLeave', $this->getChangedColumns() ) ) { $strSql .= ' is_on_leave = ' . $this->sqlIsOnLeave() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'note' => $this->getNote(),
			'expected_in_time' => $this->getExpectedInTime(),
			'requested_datetime' => $this->getRequestedDatetime(),
			'is_late' => $this->getIsLate(),
			'is_on_leave' => $this->getIsOnLeave(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>