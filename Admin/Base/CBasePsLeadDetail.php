<?php

class CBasePsLeadDetail extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.ps_lead_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPsLeadId;
	protected $m_intPsLeadTypeId;
	protected $m_intContractTerminationReasonId;
	protected $m_intSupportEmployeeId;
	protected $m_intSalesEmployeeId;
	protected $m_intTrainingEmployeeId;
	protected $m_intImplementationEmployeeId;
	protected $m_intExecutiveSponsorId;
	protected $m_intFirstResponseEmployeeId;
	protected $m_intBillingEmployeeId;
	protected $m_intSeoEmployeeId;
	protected $m_intLeasingCenterEmployeeId;
	protected $m_intRenewalFrequencyId;
	protected $m_intMaxDelinquencyLevelTypeId;
	protected $m_intCompanyTaskIntervalDays;
	protected $m_intInternalTaskIntervalDays;
	protected $m_intContactIntervalMonths;
	protected $m_intSmallPropertyUnitCount;
	protected $m_intTopNmhcRank;
	protected $m_strTaxNumberEncrypted;
	protected $m_strPrimaryWebsiteUrls;
	protected $m_strLinkForAccountPlan;
	protected $m_strAchIdentificationNumber;
	protected $m_strTaskNotificationEmails;
	protected $m_strTechnicalProductContactEmail;
	protected $m_strTerminationNotes;
	protected $m_strAddressLineOne;
	protected $m_strAddressLineTwo;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strRenewalDate;
	protected $m_boolIsAutoImplemented;
	protected $m_boolBlockDialerCalls;
	protected $m_boolAllowMultipleAccountProperties;
	protected $m_boolShowImplementationPortal;
	protected $m_intClientRiskStatus;
	protected $m_intKeyClientOverrideRank;
	protected $m_intKeyClientAcvRank;
	protected $m_strNewLogoTimerStart;
	protected $m_strCompanyTasksEmailedOn;
	protected $m_strInternalTasksEmailedOn;
	protected $m_strContractRenewalDate;
	protected $m_strDelinquencyStartDate;
	protected $m_strTerminationDate;
	protected $m_strContractStipulations;
	protected $m_strFirstRenewalOn;
	protected $m_strLastRenewedOn;
	protected $m_intLastContactedBy;
	protected $m_strLastContactedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strSelfProvisioningNote;
	protected $m_intUtilityBillingEmployeeId;
	protected $m_intUtilityManagementEmployeeId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intUtilityManagerId;
	protected $m_intPsLeadStageReasonId;
	protected $m_fltAnnualIncreasePercentage;
	protected $m_strAnnualIncreaseMonth;
	protected $m_fltRenewalIncreasePercentage;
	protected $m_boolIsSlaSupport;

	public function __construct() {
		parent::__construct();

		$this->m_intCompanyTaskIntervalDays = '4';
		$this->m_intInternalTaskIntervalDays = '7';
		$this->m_intSmallPropertyUnitCount = '10';
		$this->m_boolIsAutoImplemented = false;
		$this->m_boolBlockDialerCalls = false;
		$this->m_boolAllowMultipleAccountProperties = false;
		$this->m_intClientRiskStatus = ( 2 );
		$this->m_fltAnnualIncreasePercentage = '0';
		$this->m_fltRenewalIncreasePercentage = '0';
		$this->m_boolIsSlaSupport = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['ps_lead_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadTypeId', trim( $arrValues['ps_lead_type_id'] ) ); elseif( isset( $arrValues['ps_lead_type_id'] ) ) $this->setPsLeadTypeId( $arrValues['ps_lead_type_id'] );
		if( isset( $arrValues['contract_termination_reason_id'] ) && $boolDirectSet ) $this->set( 'm_intContractTerminationReasonId', trim( $arrValues['contract_termination_reason_id'] ) ); elseif( isset( $arrValues['contract_termination_reason_id'] ) ) $this->setContractTerminationReasonId( $arrValues['contract_termination_reason_id'] );
		if( isset( $arrValues['support_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSupportEmployeeId', trim( $arrValues['support_employee_id'] ) ); elseif( isset( $arrValues['support_employee_id'] ) ) $this->setSupportEmployeeId( $arrValues['support_employee_id'] );
		if( isset( $arrValues['sales_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSalesEmployeeId', trim( $arrValues['sales_employee_id'] ) ); elseif( isset( $arrValues['sales_employee_id'] ) ) $this->setSalesEmployeeId( $arrValues['sales_employee_id'] );
		if( isset( $arrValues['training_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingEmployeeId', trim( $arrValues['training_employee_id'] ) ); elseif( isset( $arrValues['training_employee_id'] ) ) $this->setTrainingEmployeeId( $arrValues['training_employee_id'] );
		if( isset( $arrValues['implementation_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationEmployeeId', trim( $arrValues['implementation_employee_id'] ) ); elseif( isset( $arrValues['implementation_employee_id'] ) ) $this->setImplementationEmployeeId( $arrValues['implementation_employee_id'] );
		if( isset( $arrValues['executive_sponsor_id'] ) && $boolDirectSet ) $this->set( 'm_intExecutiveSponsorId', trim( $arrValues['executive_sponsor_id'] ) ); elseif( isset( $arrValues['executive_sponsor_id'] ) ) $this->setExecutiveSponsorId( $arrValues['executive_sponsor_id'] );
		if( isset( $arrValues['first_response_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intFirstResponseEmployeeId', trim( $arrValues['first_response_employee_id'] ) ); elseif( isset( $arrValues['first_response_employee_id'] ) ) $this->setFirstResponseEmployeeId( $arrValues['first_response_employee_id'] );
		if( isset( $arrValues['billing_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingEmployeeId', trim( $arrValues['billing_employee_id'] ) ); elseif( isset( $arrValues['billing_employee_id'] ) ) $this->setBillingEmployeeId( $arrValues['billing_employee_id'] );
		if( isset( $arrValues['seo_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSeoEmployeeId', trim( $arrValues['seo_employee_id'] ) ); elseif( isset( $arrValues['seo_employee_id'] ) ) $this->setSeoEmployeeId( $arrValues['seo_employee_id'] );
		if( isset( $arrValues['leasing_center_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intLeasingCenterEmployeeId', trim( $arrValues['leasing_center_employee_id'] ) ); elseif( isset( $arrValues['leasing_center_employee_id'] ) ) $this->setLeasingCenterEmployeeId( $arrValues['leasing_center_employee_id'] );
		if( isset( $arrValues['renewal_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intRenewalFrequencyId', trim( $arrValues['renewal_frequency_id'] ) ); elseif( isset( $arrValues['renewal_frequency_id'] ) ) $this->setRenewalFrequencyId( $arrValues['renewal_frequency_id'] );
		if( isset( $arrValues['max_delinquency_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaxDelinquencyLevelTypeId', trim( $arrValues['max_delinquency_level_type_id'] ) ); elseif( isset( $arrValues['max_delinquency_level_type_id'] ) ) $this->setMaxDelinquencyLevelTypeId( $arrValues['max_delinquency_level_type_id'] );
		if( isset( $arrValues['company_task_interval_days'] ) && $boolDirectSet ) $this->set( 'm_intCompanyTaskIntervalDays', trim( $arrValues['company_task_interval_days'] ) ); elseif( isset( $arrValues['company_task_interval_days'] ) ) $this->setCompanyTaskIntervalDays( $arrValues['company_task_interval_days'] );
		if( isset( $arrValues['internal_task_interval_days'] ) && $boolDirectSet ) $this->set( 'm_intInternalTaskIntervalDays', trim( $arrValues['internal_task_interval_days'] ) ); elseif( isset( $arrValues['internal_task_interval_days'] ) ) $this->setInternalTaskIntervalDays( $arrValues['internal_task_interval_days'] );
		if( isset( $arrValues['contact_interval_months'] ) && $boolDirectSet ) $this->set( 'm_intContactIntervalMonths', trim( $arrValues['contact_interval_months'] ) ); elseif( isset( $arrValues['contact_interval_months'] ) ) $this->setContactIntervalMonths( $arrValues['contact_interval_months'] );
		if( isset( $arrValues['small_property_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intSmallPropertyUnitCount', trim( $arrValues['small_property_unit_count'] ) ); elseif( isset( $arrValues['small_property_unit_count'] ) ) $this->setSmallPropertyUnitCount( $arrValues['small_property_unit_count'] );
		if( isset( $arrValues['top_nmhc_rank'] ) && $boolDirectSet ) $this->set( 'm_intTopNmhcRank', trim( $arrValues['top_nmhc_rank'] ) ); elseif( isset( $arrValues['top_nmhc_rank'] ) ) $this->setTopNmhcRank( $arrValues['top_nmhc_rank'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( $arrValues['tax_number_encrypted'] ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['primary_website_urls'] ) && $boolDirectSet ) $this->set( 'm_strPrimaryWebsiteUrls', trim( $arrValues['primary_website_urls'] ) ); elseif( isset( $arrValues['primary_website_urls'] ) ) $this->setPrimaryWebsiteUrls( $arrValues['primary_website_urls'] );
		if( isset( $arrValues['link_for_account_plan'] ) && $boolDirectSet ) $this->set( 'm_strLinkForAccountPlan', trim( $arrValues['link_for_account_plan'] ) ); elseif( isset( $arrValues['link_for_account_plan'] ) ) $this->setLinkForAccountPlan( $arrValues['link_for_account_plan'] );
		if( isset( $arrValues['ach_identification_number'] ) && $boolDirectSet ) $this->set( 'm_strAchIdentificationNumber', trim( $arrValues['ach_identification_number'] ) ); elseif( isset( $arrValues['ach_identification_number'] ) ) $this->setAchIdentificationNumber( $arrValues['ach_identification_number'] );
		if( isset( $arrValues['task_notification_emails'] ) && $boolDirectSet ) $this->set( 'm_strTaskNotificationEmails', trim( $arrValues['task_notification_emails'] ) ); elseif( isset( $arrValues['task_notification_emails'] ) ) $this->setTaskNotificationEmails( $arrValues['task_notification_emails'] );
		if( isset( $arrValues['technical_product_contact_email'] ) && $boolDirectSet ) $this->set( 'm_strTechnicalProductContactEmail', trim( $arrValues['technical_product_contact_email'] ) ); elseif( isset( $arrValues['technical_product_contact_email'] ) ) $this->setTechnicalProductContactEmail( $arrValues['technical_product_contact_email'] );
		if( isset( $arrValues['termination_notes'] ) && $boolDirectSet ) $this->set( 'm_strTerminationNotes', trim( $arrValues['termination_notes'] ) ); elseif( isset( $arrValues['termination_notes'] ) ) $this->setTerminationNotes( $arrValues['termination_notes'] );
		if( isset( $arrValues['address_line_one'] ) && $boolDirectSet ) $this->set( 'm_strAddressLineOne', trim( $arrValues['address_line_one'] ) ); elseif( isset( $arrValues['address_line_one'] ) ) $this->setAddressLineOne( $arrValues['address_line_one'] );
		if( isset( $arrValues['address_line_two'] ) && $boolDirectSet ) $this->set( 'm_strAddressLineTwo', trim( $arrValues['address_line_two'] ) ); elseif( isset( $arrValues['address_line_two'] ) ) $this->setAddressLineTwo( $arrValues['address_line_two'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( $arrValues['city'] ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( $arrValues['state_code'] ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( $arrValues['state_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( $arrValues['postal_code'] ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( $arrValues['postal_code'] );
		if( isset( $arrValues['renewal_date'] ) && $boolDirectSet ) $this->set( 'm_strRenewalDate', trim( $arrValues['renewal_date'] ) ); elseif( isset( $arrValues['renewal_date'] ) ) $this->setRenewalDate( $arrValues['renewal_date'] );
		if( isset( $arrValues['is_auto_implemented'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoImplemented', trim( stripcslashes( $arrValues['is_auto_implemented'] ) ) ); elseif( isset( $arrValues['is_auto_implemented'] ) ) $this->setIsAutoImplemented( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_implemented'] ) : $arrValues['is_auto_implemented'] );
		if( isset( $arrValues['block_dialer_calls'] ) && $boolDirectSet ) $this->set( 'm_boolBlockDialerCalls', trim( stripcslashes( $arrValues['block_dialer_calls'] ) ) ); elseif( isset( $arrValues['block_dialer_calls'] ) ) $this->setBlockDialerCalls( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['block_dialer_calls'] ) : $arrValues['block_dialer_calls'] );
		if( isset( $arrValues['allow_multiple_account_properties'] ) && $boolDirectSet ) $this->set( 'm_boolAllowMultipleAccountProperties', trim( stripcslashes( $arrValues['allow_multiple_account_properties'] ) ) ); elseif( isset( $arrValues['allow_multiple_account_properties'] ) ) $this->setAllowMultipleAccountProperties( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allow_multiple_account_properties'] ) : $arrValues['allow_multiple_account_properties'] );
		if( isset( $arrValues['show_implementation_portal'] ) && $boolDirectSet ) $this->set( 'm_boolShowImplementationPortal', trim( stripcslashes( $arrValues['show_implementation_portal'] ) ) ); elseif( isset( $arrValues['show_implementation_portal'] ) ) $this->setShowImplementationPortal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_implementation_portal'] ) : $arrValues['show_implementation_portal'] );
		if( isset( $arrValues['client_risk_status'] ) && $boolDirectSet ) $this->set( 'm_intClientRiskStatus', trim( $arrValues['client_risk_status'] ) ); elseif( isset( $arrValues['client_risk_status'] ) ) $this->setClientRiskStatus( $arrValues['client_risk_status'] );
		if( isset( $arrValues['key_client_override_rank'] ) && $boolDirectSet ) $this->set( 'm_intKeyClientOverrideRank', trim( $arrValues['key_client_override_rank'] ) ); elseif( isset( $arrValues['key_client_override_rank'] ) ) $this->setKeyClientOverrideRank( $arrValues['key_client_override_rank'] );
		if( isset( $arrValues['key_client_acv_rank'] ) && $boolDirectSet ) $this->set( 'm_intKeyClientAcvRank', trim( $arrValues['key_client_acv_rank'] ) ); elseif( isset( $arrValues['key_client_acv_rank'] ) ) $this->setKeyClientAcvRank( $arrValues['key_client_acv_rank'] );
		if( isset( $arrValues['new_logo_timer_start'] ) && $boolDirectSet ) $this->set( 'm_strNewLogoTimerStart', trim( $arrValues['new_logo_timer_start'] ) ); elseif( isset( $arrValues['new_logo_timer_start'] ) ) $this->setNewLogoTimerStart( $arrValues['new_logo_timer_start'] );
		if( isset( $arrValues['company_tasks_emailed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompanyTasksEmailedOn', trim( $arrValues['company_tasks_emailed_on'] ) ); elseif( isset( $arrValues['company_tasks_emailed_on'] ) ) $this->setCompanyTasksEmailedOn( $arrValues['company_tasks_emailed_on'] );
		if( isset( $arrValues['internal_tasks_emailed_on'] ) && $boolDirectSet ) $this->set( 'm_strInternalTasksEmailedOn', trim( $arrValues['internal_tasks_emailed_on'] ) ); elseif( isset( $arrValues['internal_tasks_emailed_on'] ) ) $this->setInternalTasksEmailedOn( $arrValues['internal_tasks_emailed_on'] );
		if( isset( $arrValues['contract_renewal_date'] ) && $boolDirectSet ) $this->set( 'm_strContractRenewalDate', trim( $arrValues['contract_renewal_date'] ) ); elseif( isset( $arrValues['contract_renewal_date'] ) ) $this->setContractRenewalDate( $arrValues['contract_renewal_date'] );
		if( isset( $arrValues['delinquency_start_date'] ) && $boolDirectSet ) $this->set( 'm_strDelinquencyStartDate', trim( $arrValues['delinquency_start_date'] ) ); elseif( isset( $arrValues['delinquency_start_date'] ) ) $this->setDelinquencyStartDate( $arrValues['delinquency_start_date'] );
		if( isset( $arrValues['termination_date'] ) && $boolDirectSet ) $this->set( 'm_strTerminationDate', trim( $arrValues['termination_date'] ) ); elseif( isset( $arrValues['termination_date'] ) ) $this->setTerminationDate( $arrValues['termination_date'] );
		if( isset( $arrValues['contract_stipulations'] ) && $boolDirectSet ) $this->set( 'm_strContractStipulations', trim( $arrValues['contract_stipulations'] ) ); elseif( isset( $arrValues['contract_stipulations'] ) ) $this->setContractStipulations( $arrValues['contract_stipulations'] );
		if( isset( $arrValues['first_renewal_on'] ) && $boolDirectSet ) $this->set( 'm_strFirstRenewalOn', trim( $arrValues['first_renewal_on'] ) ); elseif( isset( $arrValues['first_renewal_on'] ) ) $this->setFirstRenewalOn( $arrValues['first_renewal_on'] );
		if( isset( $arrValues['last_renewed_on'] ) && $boolDirectSet ) $this->set( 'm_strLastRenewedOn', trim( $arrValues['last_renewed_on'] ) ); elseif( isset( $arrValues['last_renewed_on'] ) ) $this->setLastRenewedOn( $arrValues['last_renewed_on'] );
		if( isset( $arrValues['last_contacted_by'] ) && $boolDirectSet ) $this->set( 'm_intLastContactedBy', trim( $arrValues['last_contacted_by'] ) ); elseif( isset( $arrValues['last_contacted_by'] ) ) $this->setLastContactedBy( $arrValues['last_contacted_by'] );
		if( isset( $arrValues['last_contacted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastContactedOn', trim( $arrValues['last_contacted_on'] ) ); elseif( isset( $arrValues['last_contacted_on'] ) ) $this->setLastContactedOn( $arrValues['last_contacted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['self_provisioning_note'] ) && $boolDirectSet ) $this->set( 'm_strSelfProvisioningNote', trim( $arrValues['self_provisioning_note'] ) ); elseif( isset( $arrValues['self_provisioning_note'] ) ) $this->setSelfProvisioningNote( $arrValues['self_provisioning_note'] );
		if( isset( $arrValues['utility_billing_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBillingEmployeeId', trim( $arrValues['utility_billing_employee_id'] ) ); elseif( isset( $arrValues['utility_billing_employee_id'] ) ) $this->setUtilityBillingEmployeeId( $arrValues['utility_billing_employee_id'] );
		if( isset( $arrValues['utility_management_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityManagementEmployeeId', trim( $arrValues['utility_management_employee_id'] ) ); elseif( isset( $arrValues['utility_management_employee_id'] ) ) $this->setUtilityManagementEmployeeId( $arrValues['utility_management_employee_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['utility_manager_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityManagerId', trim( $arrValues['utility_manager_id'] ) ); elseif( isset( $arrValues['utility_manager_id'] ) ) $this->setUtilityManagerId( $arrValues['utility_manager_id'] );
		if( isset( $arrValues['ps_lead_stage_reason_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadStageReasonId', trim( $arrValues['ps_lead_stage_reason_id'] ) ); elseif( isset( $arrValues['ps_lead_stage_reason_id'] ) ) $this->setPsLeadStageReasonId( $arrValues['ps_lead_stage_reason_id'] );
		if( isset( $arrValues['annual_increase_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltAnnualIncreasePercentage', trim( $arrValues['annual_increase_percentage'] ) ); elseif( isset( $arrValues['annual_increase_percentage'] ) ) $this->setAnnualIncreasePercentage( $arrValues['annual_increase_percentage'] );
		if( isset( $arrValues['annual_increase_month'] ) && $boolDirectSet ) $this->set( 'm_strAnnualIncreaseMonth', trim( stripcslashes( $arrValues['annual_increase_month'] ) ) ); elseif( isset( $arrValues['annual_increase_month'] ) ) $this->setAnnualIncreaseMonth( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['annual_increase_month'] ) : $arrValues['annual_increase_month'] );
		if( isset( $arrValues['renewal_increase_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltRenewalIncreasePercentage', trim( $arrValues['renewal_increase_percentage'] ) ); elseif( isset( $arrValues['renewal_increase_percentage'] ) ) $this->setRenewalIncreasePercentage( $arrValues['renewal_increase_percentage'] );
		if( isset( $arrValues['is_sla_support'] ) && $boolDirectSet ) $this->set( 'm_boolIsSlaSupport', trim( stripcslashes( $arrValues['is_sla_support'] ) ) ); elseif( isset( $arrValues['is_sla_support'] ) ) $this->setIsSlaSupport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_sla_support'] ) : $arrValues['is_sla_support'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setPsLeadTypeId( $intPsLeadTypeId ) {
		$this->set( 'm_intPsLeadTypeId', CStrings::strToIntDef( $intPsLeadTypeId, NULL, false ) );
	}

	public function getPsLeadTypeId() {
		return $this->m_intPsLeadTypeId;
	}

	public function sqlPsLeadTypeId() {
		return ( true == isset( $this->m_intPsLeadTypeId ) ) ? ( string ) $this->m_intPsLeadTypeId : 'NULL';
	}

	public function setContractTerminationReasonId( $intContractTerminationReasonId ) {
		$this->set( 'm_intContractTerminationReasonId', CStrings::strToIntDef( $intContractTerminationReasonId, NULL, false ) );
	}

	public function getContractTerminationReasonId() {
		return $this->m_intContractTerminationReasonId;
	}

	public function sqlContractTerminationReasonId() {
		return ( true == isset( $this->m_intContractTerminationReasonId ) ) ? ( string ) $this->m_intContractTerminationReasonId : 'NULL';
	}

	public function setSupportEmployeeId( $intSupportEmployeeId ) {
		$this->set( 'm_intSupportEmployeeId', CStrings::strToIntDef( $intSupportEmployeeId, NULL, false ) );
	}

	public function getSupportEmployeeId() {
		return $this->m_intSupportEmployeeId;
	}

	public function sqlSupportEmployeeId() {
		return ( true == isset( $this->m_intSupportEmployeeId ) ) ? ( string ) $this->m_intSupportEmployeeId : 'NULL';
	}

	public function setSalesEmployeeId( $intSalesEmployeeId ) {
		$this->set( 'm_intSalesEmployeeId', CStrings::strToIntDef( $intSalesEmployeeId, NULL, false ) );
	}

	public function getSalesEmployeeId() {
		return $this->m_intSalesEmployeeId;
	}

	public function sqlSalesEmployeeId() {
		return ( true == isset( $this->m_intSalesEmployeeId ) ) ? ( string ) $this->m_intSalesEmployeeId : 'NULL';
	}

	public function setTrainingEmployeeId( $intTrainingEmployeeId ) {
		$this->set( 'm_intTrainingEmployeeId', CStrings::strToIntDef( $intTrainingEmployeeId, NULL, false ) );
	}

	public function getTrainingEmployeeId() {
		return $this->m_intTrainingEmployeeId;
	}

	public function sqlTrainingEmployeeId() {
		return ( true == isset( $this->m_intTrainingEmployeeId ) ) ? ( string ) $this->m_intTrainingEmployeeId : 'NULL';
	}

	public function setImplementationEmployeeId( $intImplementationEmployeeId ) {
		$this->set( 'm_intImplementationEmployeeId', CStrings::strToIntDef( $intImplementationEmployeeId, NULL, false ) );
	}

	public function getImplementationEmployeeId() {
		return $this->m_intImplementationEmployeeId;
	}

	public function sqlImplementationEmployeeId() {
		return ( true == isset( $this->m_intImplementationEmployeeId ) ) ? ( string ) $this->m_intImplementationEmployeeId : 'NULL';
	}

	public function setExecutiveSponsorId( $intExecutiveSponsorId ) {
		$this->set( 'm_intExecutiveSponsorId', CStrings::strToIntDef( $intExecutiveSponsorId, NULL, false ) );
	}

	public function getExecutiveSponsorId() {
		return $this->m_intExecutiveSponsorId;
	}

	public function sqlExecutiveSponsorId() {
		return ( true == isset( $this->m_intExecutiveSponsorId ) ) ? ( string ) $this->m_intExecutiveSponsorId : 'NULL';
	}

	public function setFirstResponseEmployeeId( $intFirstResponseEmployeeId ) {
		$this->set( 'm_intFirstResponseEmployeeId', CStrings::strToIntDef( $intFirstResponseEmployeeId, NULL, false ) );
	}

	public function getFirstResponseEmployeeId() {
		return $this->m_intFirstResponseEmployeeId;
	}

	public function sqlFirstResponseEmployeeId() {
		return ( true == isset( $this->m_intFirstResponseEmployeeId ) ) ? ( string ) $this->m_intFirstResponseEmployeeId : 'NULL';
	}

	public function setBillingEmployeeId( $intBillingEmployeeId ) {
		$this->set( 'm_intBillingEmployeeId', CStrings::strToIntDef( $intBillingEmployeeId, NULL, false ) );
	}

	public function getBillingEmployeeId() {
		return $this->m_intBillingEmployeeId;
	}

	public function sqlBillingEmployeeId() {
		return ( true == isset( $this->m_intBillingEmployeeId ) ) ? ( string ) $this->m_intBillingEmployeeId : 'NULL';
	}

	public function setSeoEmployeeId( $intSeoEmployeeId ) {
		$this->set( 'm_intSeoEmployeeId', CStrings::strToIntDef( $intSeoEmployeeId, NULL, false ) );
	}

	public function getSeoEmployeeId() {
		return $this->m_intSeoEmployeeId;
	}

	public function sqlSeoEmployeeId() {
		return ( true == isset( $this->m_intSeoEmployeeId ) ) ? ( string ) $this->m_intSeoEmployeeId : 'NULL';
	}

	public function setLeasingCenterEmployeeId( $intLeasingCenterEmployeeId ) {
		$this->set( 'm_intLeasingCenterEmployeeId', CStrings::strToIntDef( $intLeasingCenterEmployeeId, NULL, false ) );
	}

	public function getLeasingCenterEmployeeId() {
		return $this->m_intLeasingCenterEmployeeId;
	}

	public function sqlLeasingCenterEmployeeId() {
		return ( true == isset( $this->m_intLeasingCenterEmployeeId ) ) ? ( string ) $this->m_intLeasingCenterEmployeeId : 'NULL';
	}

	public function setRenewalFrequencyId( $intRenewalFrequencyId ) {
		$this->set( 'm_intRenewalFrequencyId', CStrings::strToIntDef( $intRenewalFrequencyId, NULL, false ) );
	}

	public function getRenewalFrequencyId() {
		return $this->m_intRenewalFrequencyId;
	}

	public function sqlRenewalFrequencyId() {
		return ( true == isset( $this->m_intRenewalFrequencyId ) ) ? ( string ) $this->m_intRenewalFrequencyId : 'NULL';
	}

	public function setMaxDelinquencyLevelTypeId( $intMaxDelinquencyLevelTypeId ) {
		$this->set( 'm_intMaxDelinquencyLevelTypeId', CStrings::strToIntDef( $intMaxDelinquencyLevelTypeId, NULL, false ) );
	}

	public function getMaxDelinquencyLevelTypeId() {
		return $this->m_intMaxDelinquencyLevelTypeId;
	}

	public function sqlMaxDelinquencyLevelTypeId() {
		return ( true == isset( $this->m_intMaxDelinquencyLevelTypeId ) ) ? ( string ) $this->m_intMaxDelinquencyLevelTypeId : 'NULL';
	}

	public function setCompanyTaskIntervalDays( $intCompanyTaskIntervalDays ) {
		$this->set( 'm_intCompanyTaskIntervalDays', CStrings::strToIntDef( $intCompanyTaskIntervalDays, NULL, false ) );
	}

	public function getCompanyTaskIntervalDays() {
		return $this->m_intCompanyTaskIntervalDays;
	}

	public function sqlCompanyTaskIntervalDays() {
		return ( true == isset( $this->m_intCompanyTaskIntervalDays ) ) ? ( string ) $this->m_intCompanyTaskIntervalDays : '4';
	}

	public function setInternalTaskIntervalDays( $intInternalTaskIntervalDays ) {
		$this->set( 'm_intInternalTaskIntervalDays', CStrings::strToIntDef( $intInternalTaskIntervalDays, NULL, false ) );
	}

	public function getInternalTaskIntervalDays() {
		return $this->m_intInternalTaskIntervalDays;
	}

	public function sqlInternalTaskIntervalDays() {
		return ( true == isset( $this->m_intInternalTaskIntervalDays ) ) ? ( string ) $this->m_intInternalTaskIntervalDays : '7';
	}

	public function setContactIntervalMonths( $intContactIntervalMonths ) {
		$this->set( 'm_intContactIntervalMonths', CStrings::strToIntDef( $intContactIntervalMonths, NULL, false ) );
	}

	public function getContactIntervalMonths() {
		return $this->m_intContactIntervalMonths;
	}

	public function sqlContactIntervalMonths() {
		return ( true == isset( $this->m_intContactIntervalMonths ) ) ? ( string ) $this->m_intContactIntervalMonths : 'NULL';
	}

	public function setSmallPropertyUnitCount( $intSmallPropertyUnitCount ) {
		$this->set( 'm_intSmallPropertyUnitCount', CStrings::strToIntDef( $intSmallPropertyUnitCount, NULL, false ) );
	}

	public function getSmallPropertyUnitCount() {
		return $this->m_intSmallPropertyUnitCount;
	}

	public function sqlSmallPropertyUnitCount() {
		return ( true == isset( $this->m_intSmallPropertyUnitCount ) ) ? ( string ) $this->m_intSmallPropertyUnitCount : '10';
	}

	public function setTopNmhcRank( $intTopNmhcRank ) {
		$this->set( 'm_intTopNmhcRank', CStrings::strToIntDef( $intTopNmhcRank, NULL, false ) );
	}

	public function getTopNmhcRank() {
		return $this->m_intTopNmhcRank;
	}

	public function sqlTopNmhcRank() {
		return ( true == isset( $this->m_intTopNmhcRank ) ) ? ( string ) $this->m_intTopNmhcRank : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 1000, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberEncrypted ) : '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setPrimaryWebsiteUrls( $strPrimaryWebsiteUrls ) {
		$this->set( 'm_strPrimaryWebsiteUrls', CStrings::strTrimDef( $strPrimaryWebsiteUrls, 2000, NULL, true ) );
	}

	public function getPrimaryWebsiteUrls() {
		return $this->m_strPrimaryWebsiteUrls;
	}

	public function sqlPrimaryWebsiteUrls() {
		return ( true == isset( $this->m_strPrimaryWebsiteUrls ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrimaryWebsiteUrls ) : '\'' . addslashes( $this->m_strPrimaryWebsiteUrls ) . '\'' ) : 'NULL';
	}

	public function setLinkForAccountPlan( $strLinkForAccountPlan ) {
		$this->set( 'm_strLinkForAccountPlan', CStrings::strTrimDef( $strLinkForAccountPlan, 1000, NULL, true ) );
	}

	public function getLinkForAccountPlan() {
		return $this->m_strLinkForAccountPlan;
	}

	public function sqlLinkForAccountPlan() {
		return ( true == isset( $this->m_strLinkForAccountPlan ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLinkForAccountPlan ) : '\'' . addslashes( $this->m_strLinkForAccountPlan ) . '\'' ) : 'NULL';
	}

	public function setAchIdentificationNumber( $strAchIdentificationNumber ) {
		$this->set( 'm_strAchIdentificationNumber', CStrings::strTrimDef( $strAchIdentificationNumber, 240, NULL, true ) );
	}

	public function getAchIdentificationNumber() {
		return $this->m_strAchIdentificationNumber;
	}

	public function sqlAchIdentificationNumber() {
		return ( true == isset( $this->m_strAchIdentificationNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAchIdentificationNumber ) : '\'' . addslashes( $this->m_strAchIdentificationNumber ) . '\'' ) : 'NULL';
	}

	public function setTaskNotificationEmails( $strTaskNotificationEmails ) {
		$this->set( 'm_strTaskNotificationEmails', CStrings::strTrimDef( $strTaskNotificationEmails, 240, NULL, true ) );
	}

	public function getTaskNotificationEmails() {
		return $this->m_strTaskNotificationEmails;
	}

	public function sqlTaskNotificationEmails() {
		return ( true == isset( $this->m_strTaskNotificationEmails ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaskNotificationEmails ) : '\'' . addslashes( $this->m_strTaskNotificationEmails ) . '\'' ) : 'NULL';
	}

	public function setTechnicalProductContactEmail( $strTechnicalProductContactEmail ) {
		$this->set( 'm_strTechnicalProductContactEmail', CStrings::strTrimDef( $strTechnicalProductContactEmail, 240, NULL, true ) );
	}

	public function getTechnicalProductContactEmail() {
		return $this->m_strTechnicalProductContactEmail;
	}

	public function sqlTechnicalProductContactEmail() {
		return ( true == isset( $this->m_strTechnicalProductContactEmail ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTechnicalProductContactEmail ) : '\'' . addslashes( $this->m_strTechnicalProductContactEmail ) . '\'' ) : 'NULL';
	}

	public function setTerminationNotes( $strTerminationNotes ) {
		$this->set( 'm_strTerminationNotes', CStrings::strTrimDef( $strTerminationNotes, 240, NULL, true ) );
	}

	public function getTerminationNotes() {
		return $this->m_strTerminationNotes;
	}

	public function sqlTerminationNotes() {
		return ( true == isset( $this->m_strTerminationNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTerminationNotes ) : '\'' . addslashes( $this->m_strTerminationNotes ) . '\'' ) : 'NULL';
	}

	public function setAddressLineOne( $strAddressLineOne ) {
		$this->set( 'm_strAddressLineOne', CStrings::strTrimDef( $strAddressLineOne, 100, NULL, true ) );
	}

	public function getAddressLineOne() {
		return $this->m_strAddressLineOne;
	}

	public function sqlAddressLineOne() {
		return ( true == isset( $this->m_strAddressLineOne ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAddressLineOne ) : '\'' . addslashes( $this->m_strAddressLineOne ) . '\'' ) : 'NULL';
	}

	public function setAddressLineTwo( $strAddressLineTwo ) {
		$this->set( 'm_strAddressLineTwo', CStrings::strTrimDef( $strAddressLineTwo, 100, NULL, true ) );
	}

	public function getAddressLineTwo() {
		return $this->m_strAddressLineTwo;
	}

	public function sqlAddressLineTwo() {
		return ( true == isset( $this->m_strAddressLineTwo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAddressLineTwo ) : '\'' . addslashes( $this->m_strAddressLineTwo ) . '\'' ) : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCity ) : '\'' . addslashes( $this->m_strCity ) . '\'' ) : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStateCode ) : '\'' . addslashes( $this->m_strStateCode ) . '\'' ) : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostalCode ) : '\'' . addslashes( $this->m_strPostalCode ) . '\'' ) : 'NULL';
	}

	public function setRenewalDate( $strRenewalDate ) {
		$this->set( 'm_strRenewalDate', CStrings::strTrimDef( $strRenewalDate, 20, NULL, true ) );
	}

	public function getRenewalDate() {
		return $this->m_strRenewalDate;
	}

	public function sqlRenewalDate() {
		return ( true == isset( $this->m_strRenewalDate ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRenewalDate ) : '\'' . addslashes( $this->m_strRenewalDate ) . '\'' ) : 'NULL';
	}

	public function setIsAutoImplemented( $boolIsAutoImplemented ) {
		$this->set( 'm_boolIsAutoImplemented', CStrings::strToBool( $boolIsAutoImplemented ) );
	}

	public function getIsAutoImplemented() {
		return $this->m_boolIsAutoImplemented;
	}

	public function sqlIsAutoImplemented() {
		return ( true == isset( $this->m_boolIsAutoImplemented ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoImplemented ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setBlockDialerCalls( $boolBlockDialerCalls ) {
		$this->set( 'm_boolBlockDialerCalls', CStrings::strToBool( $boolBlockDialerCalls ) );
	}

	public function getBlockDialerCalls() {
		return $this->m_boolBlockDialerCalls;
	}

	public function sqlBlockDialerCalls() {
		return ( true == isset( $this->m_boolBlockDialerCalls ) ) ? '\'' . ( true == ( bool ) $this->m_boolBlockDialerCalls ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAllowMultipleAccountProperties( $boolAllowMultipleAccountProperties ) {
		$this->set( 'm_boolAllowMultipleAccountProperties', CStrings::strToBool( $boolAllowMultipleAccountProperties ) );
	}

	public function getAllowMultipleAccountProperties() {
		return $this->m_boolAllowMultipleAccountProperties;
	}

	public function sqlAllowMultipleAccountProperties() {
		return ( true == isset( $this->m_boolAllowMultipleAccountProperties ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllowMultipleAccountProperties ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setShowImplementationPortal( $boolShowImplementationPortal ) {
		$this->set( 'm_boolShowImplementationPortal', CStrings::strToBool( $boolShowImplementationPortal ) );
	}

	public function getShowImplementationPortal() {
		return $this->m_boolShowImplementationPortal;
	}

	public function sqlShowImplementationPortal() {
		return ( true == isset( $this->m_boolShowImplementationPortal ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowImplementationPortal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setClientRiskStatus( $intClientRiskStatus ) {
		$this->set( 'm_intClientRiskStatus', CStrings::strToIntDef( $intClientRiskStatus, NULL, false ) );
	}

	public function getClientRiskStatus() {
		return $this->m_intClientRiskStatus;
	}

	public function sqlClientRiskStatus() {
		return ( true == isset( $this->m_intClientRiskStatus ) ) ? ( string ) $this->m_intClientRiskStatus : 'NULL';
	}

	public function setKeyClientOverrideRank( $intKeyClientOverrideRank ) {
		$this->set( 'm_intKeyClientOverrideRank', CStrings::strToIntDef( $intKeyClientOverrideRank, NULL, false ) );
	}

	public function getKeyClientOverrideRank() {
		return $this->m_intKeyClientOverrideRank;
	}

	public function sqlKeyClientOverrideRank() {
		return ( true == isset( $this->m_intKeyClientOverrideRank ) ) ? ( string ) $this->m_intKeyClientOverrideRank : 'NULL';
	}

	public function setKeyClientAcvRank( $intKeyClientAcvRank ) {
		$this->set( 'm_intKeyClientAcvRank', CStrings::strToIntDef( $intKeyClientAcvRank, NULL, false ) );
	}

	public function getKeyClientAcvRank() {
		return $this->m_intKeyClientAcvRank;
	}

	public function sqlKeyClientAcvRank() {
		return ( true == isset( $this->m_intKeyClientAcvRank ) ) ? ( string ) $this->m_intKeyClientAcvRank : 'NULL';
	}

	public function setNewLogoTimerStart( $strNewLogoTimerStart ) {
		$this->set( 'm_strNewLogoTimerStart', CStrings::strTrimDef( $strNewLogoTimerStart, -1, NULL, true ) );
	}

	public function getNewLogoTimerStart() {
		return $this->m_strNewLogoTimerStart;
	}

	public function sqlNewLogoTimerStart() {
		return ( true == isset( $this->m_strNewLogoTimerStart ) ) ? '\'' . $this->m_strNewLogoTimerStart . '\'' : 'NULL';
	}

	public function setCompanyTasksEmailedOn( $strCompanyTasksEmailedOn ) {
		$this->set( 'm_strCompanyTasksEmailedOn', CStrings::strTrimDef( $strCompanyTasksEmailedOn, -1, NULL, true ) );
	}

	public function getCompanyTasksEmailedOn() {
		return $this->m_strCompanyTasksEmailedOn;
	}

	public function sqlCompanyTasksEmailedOn() {
		return ( true == isset( $this->m_strCompanyTasksEmailedOn ) ) ? '\'' . $this->m_strCompanyTasksEmailedOn . '\'' : 'NULL';
	}

	public function setInternalTasksEmailedOn( $strInternalTasksEmailedOn ) {
		$this->set( 'm_strInternalTasksEmailedOn', CStrings::strTrimDef( $strInternalTasksEmailedOn, -1, NULL, true ) );
	}

	public function getInternalTasksEmailedOn() {
		return $this->m_strInternalTasksEmailedOn;
	}

	public function sqlInternalTasksEmailedOn() {
		return ( true == isset( $this->m_strInternalTasksEmailedOn ) ) ? '\'' . $this->m_strInternalTasksEmailedOn . '\'' : 'NULL';
	}

	public function setContractRenewalDate( $strContractRenewalDate ) {
		$this->set( 'm_strContractRenewalDate', CStrings::strTrimDef( $strContractRenewalDate, -1, NULL, true ) );
	}

	public function getContractRenewalDate() {
		return $this->m_strContractRenewalDate;
	}

	public function sqlContractRenewalDate() {
		return ( true == isset( $this->m_strContractRenewalDate ) ) ? '\'' . $this->m_strContractRenewalDate . '\'' : 'NULL';
	}

	public function setDelinquencyStartDate( $strDelinquencyStartDate ) {
		$this->set( 'm_strDelinquencyStartDate', CStrings::strTrimDef( $strDelinquencyStartDate, -1, NULL, true ) );
	}

	public function getDelinquencyStartDate() {
		return $this->m_strDelinquencyStartDate;
	}

	public function sqlDelinquencyStartDate() {
		return ( true == isset( $this->m_strDelinquencyStartDate ) ) ? '\'' . $this->m_strDelinquencyStartDate . '\'' : 'NULL';
	}

	public function setTerminationDate( $strTerminationDate ) {
		$this->set( 'm_strTerminationDate', CStrings::strTrimDef( $strTerminationDate, -1, NULL, true ) );
	}

	public function getTerminationDate() {
		return $this->m_strTerminationDate;
	}

	public function sqlTerminationDate() {
		return ( true == isset( $this->m_strTerminationDate ) ) ? '\'' . $this->m_strTerminationDate . '\'' : 'NULL';
	}

	public function setContractStipulations( $strContractStipulations ) {
		$this->set( 'm_strContractStipulations', CStrings::strTrimDef( $strContractStipulations, -1, NULL, true ) );
	}

	public function getContractStipulations() {
		return $this->m_strContractStipulations;
	}

	public function sqlContractStipulations() {
		return ( true == isset( $this->m_strContractStipulations ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContractStipulations ) : '\'' . addslashes( $this->m_strContractStipulations ) . '\'' ) : 'NULL';
	}

	public function setFirstRenewalOn( $strFirstRenewalOn ) {
		$this->set( 'm_strFirstRenewalOn', CStrings::strTrimDef( $strFirstRenewalOn, -1, NULL, true ) );
	}

	public function getFirstRenewalOn() {
		return $this->m_strFirstRenewalOn;
	}

	public function sqlFirstRenewalOn() {
		return ( true == isset( $this->m_strFirstRenewalOn ) ) ? '\'' . $this->m_strFirstRenewalOn . '\'' : 'NULL';
	}

	public function setLastRenewedOn( $strLastRenewedOn ) {
		$this->set( 'm_strLastRenewedOn', CStrings::strTrimDef( $strLastRenewedOn, -1, NULL, true ) );
	}

	public function getLastRenewedOn() {
		return $this->m_strLastRenewedOn;
	}

	public function sqlLastRenewedOn() {
		return ( true == isset( $this->m_strLastRenewedOn ) ) ? '\'' . $this->m_strLastRenewedOn . '\'' : 'NULL';
	}

	public function setLastContactedBy( $intLastContactedBy ) {
		$this->set( 'm_intLastContactedBy', CStrings::strToIntDef( $intLastContactedBy, NULL, false ) );
	}

	public function getLastContactedBy() {
		return $this->m_intLastContactedBy;
	}

	public function sqlLastContactedBy() {
		return ( true == isset( $this->m_intLastContactedBy ) ) ? ( string ) $this->m_intLastContactedBy : 'NULL';
	}

	public function setLastContactedOn( $strLastContactedOn ) {
		$this->set( 'm_strLastContactedOn', CStrings::strTrimDef( $strLastContactedOn, -1, NULL, true ) );
	}

	public function getLastContactedOn() {
		return $this->m_strLastContactedOn;
	}

	public function sqlLastContactedOn() {
		return ( true == isset( $this->m_strLastContactedOn ) ) ? '\'' . $this->m_strLastContactedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSelfProvisioningNote( $strSelfProvisioningNote ) {
		$this->set( 'm_strSelfProvisioningNote', CStrings::strTrimDef( $strSelfProvisioningNote, -1, NULL, true ) );
	}

	public function getSelfProvisioningNote() {
		return $this->m_strSelfProvisioningNote;
	}

	public function sqlSelfProvisioningNote() {
		return ( true == isset( $this->m_strSelfProvisioningNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSelfProvisioningNote ) : '\'' . addslashes( $this->m_strSelfProvisioningNote ) . '\'' ) : 'NULL';
	}

	public function setUtilityBillingEmployeeId( $intUtilityBillingEmployeeId ) {
		$this->set( 'm_intUtilityBillingEmployeeId', CStrings::strToIntDef( $intUtilityBillingEmployeeId, NULL, false ) );
	}

	public function getUtilityBillingEmployeeId() {
		return $this->m_intUtilityBillingEmployeeId;
	}

	public function sqlUtilityBillingEmployeeId() {
		return ( true == isset( $this->m_intUtilityBillingEmployeeId ) ) ? ( string ) $this->m_intUtilityBillingEmployeeId : 'NULL';
	}

	public function setUtilityManagementEmployeeId( $intUtilityManagementEmployeeId ) {
		$this->set( 'm_intUtilityManagementEmployeeId', CStrings::strToIntDef( $intUtilityManagementEmployeeId, NULL, false ) );
	}

	public function getUtilityManagementEmployeeId() {
		return $this->m_intUtilityManagementEmployeeId;
	}

	public function sqlUtilityManagementEmployeeId() {
		return ( true == isset( $this->m_intUtilityManagementEmployeeId ) ) ? ( string ) $this->m_intUtilityManagementEmployeeId : 'NULL';
	}

	public function setUtilityManagerId( $intUtilityManagerId ) {
		$this->set( 'm_intUtilityManagerId', CStrings::strToIntDef( $intUtilityManagerId, NULL, false ) );
	}

	public function getUtilityManagerId() {
		return $this->m_intUtilityManagerId;
	}

	public function sqlUtilityManagerId() {
		return ( true == isset( $this->m_intUtilityManagerId ) ) ? ( string ) $this->m_intUtilityManagerId : 'NULL';
	}

	public function setPsLeadStageReasonId( $intPsLeadStageReasonId ) {
		$this->set( 'm_intPsLeadStageReasonId', CStrings::strToIntDef( $intPsLeadStageReasonId, NULL, false ) );
	}

	public function getPsLeadStageReasonId() {
		return $this->m_intPsLeadStageReasonId;
	}

	public function sqlPsLeadStageReasonId() {
		return ( true == isset( $this->m_intPsLeadStageReasonId ) ) ? ( string ) $this->m_intPsLeadStageReasonId : 'NULL';
	}

	public function setAnnualIncreasePercentage( $fltAnnualIncreasePercentage ) {
		$this->set( 'm_fltAnnualIncreasePercentage', CStrings::strToFloatDef( $fltAnnualIncreasePercentage, NULL, false, 2 ) );
	}

	public function getAnnualIncreasePercentage() {
		return $this->m_fltAnnualIncreasePercentage;
	}

	public function sqlAnnualIncreasePercentage() {
		return ( true == isset( $this->m_fltAnnualIncreasePercentage ) ) ? ( string ) $this->m_fltAnnualIncreasePercentage : '0';
	}

	public function setAnnualIncreaseMonth( $strAnnualIncreaseMonth ) {
		$this->set( 'm_strAnnualIncreaseMonth', CStrings::strTrimDef( $strAnnualIncreaseMonth, -1, NULL, true ) );
	}

	public function getAnnualIncreaseMonth() {
		return $this->m_strAnnualIncreaseMonth;
	}

	public function sqlAnnualIncreaseMonth() {
		return ( true == isset( $this->m_strAnnualIncreaseMonth ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAnnualIncreaseMonth ) : '\'' . addslashes( $this->m_strAnnualIncreaseMonth ) . '\'' ) : 'NULL';
	}

	public function setRenewalIncreasePercentage( $fltRenewalIncreasePercentage ) {
		$this->set( 'm_fltRenewalIncreasePercentage', CStrings::strToFloatDef( $fltRenewalIncreasePercentage, NULL, false, 2 ) );
	}

	public function getRenewalIncreasePercentage() {
		return $this->m_fltRenewalIncreasePercentage;
	}

	public function sqlRenewalIncreasePercentage() {
		return ( true == isset( $this->m_fltRenewalIncreasePercentage ) ) ? ( string ) $this->m_fltRenewalIncreasePercentage : '0';
	}

	public function setIsSlaSupport( $boolIsSlaSupport ) {
		$this->set( 'm_boolIsSlaSupport', CStrings::strToBool( $boolIsSlaSupport ) );
	}

	public function getIsSlaSupport() {
		return $this->m_boolIsSlaSupport;
	}

	public function sqlIsSlaSupport() {
		return ( true == isset( $this->m_boolIsSlaSupport ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSlaSupport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ps_lead_id, ps_lead_type_id, contract_termination_reason_id, support_employee_id, sales_employee_id, training_employee_id, implementation_employee_id, executive_sponsor_id, first_response_employee_id, billing_employee_id, seo_employee_id, leasing_center_employee_id, renewal_frequency_id, max_delinquency_level_type_id, company_task_interval_days, internal_task_interval_days, contact_interval_months, small_property_unit_count, top_nmhc_rank, tax_number_encrypted, primary_website_urls, link_for_account_plan, ach_identification_number, task_notification_emails, technical_product_contact_email, termination_notes, address_line_one, address_line_two, city, state_code, postal_code, renewal_date, is_auto_implemented, block_dialer_calls, allow_multiple_account_properties, show_implementation_portal, client_risk_status, key_client_override_rank, key_client_acv_rank, new_logo_timer_start, company_tasks_emailed_on, internal_tasks_emailed_on, contract_renewal_date, delinquency_start_date, termination_date, contract_stipulations, first_renewal_on, last_renewed_on, last_contacted_by, last_contacted_on, updated_by, updated_on, created_by, created_on, self_provisioning_note, utility_billing_employee_id, utility_management_employee_id, details, utility_manager_id, ps_lead_stage_reason_id, annual_increase_percentage, annual_increase_month, renewal_increase_percentage, is_sla_support )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPsLeadId() . ', ' .
						$this->sqlPsLeadTypeId() . ', ' .
						$this->sqlContractTerminationReasonId() . ', ' .
						$this->sqlSupportEmployeeId() . ', ' .
						$this->sqlSalesEmployeeId() . ', ' .
						$this->sqlTrainingEmployeeId() . ', ' .
						$this->sqlImplementationEmployeeId() . ', ' .
						$this->sqlExecutiveSponsorId() . ', ' .
						$this->sqlFirstResponseEmployeeId() . ', ' .
						$this->sqlBillingEmployeeId() . ', ' .
						$this->sqlSeoEmployeeId() . ', ' .
						$this->sqlLeasingCenterEmployeeId() . ', ' .
						$this->sqlRenewalFrequencyId() . ', ' .
						$this->sqlMaxDelinquencyLevelTypeId() . ', ' .
						$this->sqlCompanyTaskIntervalDays() . ', ' .
						$this->sqlInternalTaskIntervalDays() . ', ' .
						$this->sqlContactIntervalMonths() . ', ' .
						$this->sqlSmallPropertyUnitCount() . ', ' .
						$this->sqlTopNmhcRank() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlPrimaryWebsiteUrls() . ', ' .
						$this->sqlLinkForAccountPlan() . ', ' .
						$this->sqlAchIdentificationNumber() . ', ' .
						$this->sqlTaskNotificationEmails() . ', ' .
						$this->sqlTechnicalProductContactEmail() . ', ' .
						$this->sqlTerminationNotes() . ', ' .
						$this->sqlAddressLineOne() . ', ' .
						$this->sqlAddressLineTwo() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlPostalCode() . ', ' .
						$this->sqlRenewalDate() . ', ' .
						$this->sqlIsAutoImplemented() . ', ' .
						$this->sqlBlockDialerCalls() . ', ' .
						$this->sqlAllowMultipleAccountProperties() . ', ' .
						$this->sqlShowImplementationPortal() . ', ' .
						$this->sqlClientRiskStatus() . ', ' .
						$this->sqlKeyClientOverrideRank() . ', ' .
						$this->sqlKeyClientAcvRank() . ', ' .
						$this->sqlNewLogoTimerStart() . ', ' .
						$this->sqlCompanyTasksEmailedOn() . ', ' .
						$this->sqlInternalTasksEmailedOn() . ', ' .
						$this->sqlContractRenewalDate() . ', ' .
						$this->sqlDelinquencyStartDate() . ', ' .
						$this->sqlTerminationDate() . ', ' .
						$this->sqlContractStipulations() . ', ' .
						$this->sqlFirstRenewalOn() . ', ' .
						$this->sqlLastRenewedOn() . ', ' .
						$this->sqlLastContactedBy() . ', ' .
						$this->sqlLastContactedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlSelfProvisioningNote() . ', ' .
						$this->sqlUtilityBillingEmployeeId() . ', ' .
						$this->sqlUtilityManagementEmployeeId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlUtilityManagerId() . ', ' .
						$this->sqlPsLeadStageReasonId() . ', ' .
						$this->sqlAnnualIncreasePercentage() . ', ' .
						$this->sqlAnnualIncreaseMonth() . ', ' .
						$this->sqlRenewalIncreasePercentage() . ', ' .
						$this->sqlIsSlaSupport() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_type_id = ' . $this->sqlPsLeadTypeId(). ',' ; } elseif( true == array_key_exists( 'PsLeadTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_type_id = ' . $this->sqlPsLeadTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_termination_reason_id = ' . $this->sqlContractTerminationReasonId(). ',' ; } elseif( true == array_key_exists( 'ContractTerminationReasonId', $this->getChangedColumns() ) ) { $strSql .= ' contract_termination_reason_id = ' . $this->sqlContractTerminationReasonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' support_employee_id = ' . $this->sqlSupportEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SupportEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' support_employee_id = ' . $this->sqlSupportEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_employee_id = ' . $this->sqlSalesEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SalesEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sales_employee_id = ' . $this->sqlSalesEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_employee_id = ' . $this->sqlTrainingEmployeeId(). ',' ; } elseif( true == array_key_exists( 'TrainingEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' training_employee_id = ' . $this->sqlTrainingEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_employee_id = ' . $this->sqlImplementationEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ImplementationEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_employee_id = ' . $this->sqlImplementationEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executive_sponsor_id = ' . $this->sqlExecutiveSponsorId(). ',' ; } elseif( true == array_key_exists( 'ExecutiveSponsorId', $this->getChangedColumns() ) ) { $strSql .= ' executive_sponsor_id = ' . $this->sqlExecutiveSponsorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_response_employee_id = ' . $this->sqlFirstResponseEmployeeId(). ',' ; } elseif( true == array_key_exists( 'FirstResponseEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' first_response_employee_id = ' . $this->sqlFirstResponseEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_employee_id = ' . $this->sqlBillingEmployeeId(). ',' ; } elseif( true == array_key_exists( 'BillingEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' billing_employee_id = ' . $this->sqlBillingEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' seo_employee_id = ' . $this->sqlSeoEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SeoEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' seo_employee_id = ' . $this->sqlSeoEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leasing_center_employee_id = ' . $this->sqlLeasingCenterEmployeeId(). ',' ; } elseif( true == array_key_exists( 'LeasingCenterEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' leasing_center_employee_id = ' . $this->sqlLeasingCenterEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_frequency_id = ' . $this->sqlRenewalFrequencyId(). ',' ; } elseif( true == array_key_exists( 'RenewalFrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' renewal_frequency_id = ' . $this->sqlRenewalFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_delinquency_level_type_id = ' . $this->sqlMaxDelinquencyLevelTypeId(). ',' ; } elseif( true == array_key_exists( 'MaxDelinquencyLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' max_delinquency_level_type_id = ' . $this->sqlMaxDelinquencyLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_task_interval_days = ' . $this->sqlCompanyTaskIntervalDays(). ',' ; } elseif( true == array_key_exists( 'CompanyTaskIntervalDays', $this->getChangedColumns() ) ) { $strSql .= ' company_task_interval_days = ' . $this->sqlCompanyTaskIntervalDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_task_interval_days = ' . $this->sqlInternalTaskIntervalDays(). ',' ; } elseif( true == array_key_exists( 'InternalTaskIntervalDays', $this->getChangedColumns() ) ) { $strSql .= ' internal_task_interval_days = ' . $this->sqlInternalTaskIntervalDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_interval_months = ' . $this->sqlContactIntervalMonths(). ',' ; } elseif( true == array_key_exists( 'ContactIntervalMonths', $this->getChangedColumns() ) ) { $strSql .= ' contact_interval_months = ' . $this->sqlContactIntervalMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' small_property_unit_count = ' . $this->sqlSmallPropertyUnitCount(). ',' ; } elseif( true == array_key_exists( 'SmallPropertyUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' small_property_unit_count = ' . $this->sqlSmallPropertyUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' top_nmhc_rank = ' . $this->sqlTopNmhcRank(). ',' ; } elseif( true == array_key_exists( 'TopNmhcRank', $this->getChangedColumns() ) ) { $strSql .= ' top_nmhc_rank = ' . $this->sqlTopNmhcRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_website_urls = ' . $this->sqlPrimaryWebsiteUrls(). ',' ; } elseif( true == array_key_exists( 'PrimaryWebsiteUrls', $this->getChangedColumns() ) ) { $strSql .= ' primary_website_urls = ' . $this->sqlPrimaryWebsiteUrls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_for_account_plan = ' . $this->sqlLinkForAccountPlan(). ',' ; } elseif( true == array_key_exists( 'LinkForAccountPlan', $this->getChangedColumns() ) ) { $strSql .= ' link_for_account_plan = ' . $this->sqlLinkForAccountPlan() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ach_identification_number = ' . $this->sqlAchIdentificationNumber(). ',' ; } elseif( true == array_key_exists( 'AchIdentificationNumber', $this->getChangedColumns() ) ) { $strSql .= ' ach_identification_number = ' . $this->sqlAchIdentificationNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_notification_emails = ' . $this->sqlTaskNotificationEmails(). ',' ; } elseif( true == array_key_exists( 'TaskNotificationEmails', $this->getChangedColumns() ) ) { $strSql .= ' task_notification_emails = ' . $this->sqlTaskNotificationEmails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' technical_product_contact_email = ' . $this->sqlTechnicalProductContactEmail(). ',' ; } elseif( true == array_key_exists( 'TechnicalProductContactEmail', $this->getChangedColumns() ) ) { $strSql .= ' technical_product_contact_email = ' . $this->sqlTechnicalProductContactEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_notes = ' . $this->sqlTerminationNotes(). ',' ; } elseif( true == array_key_exists( 'TerminationNotes', $this->getChangedColumns() ) ) { $strSql .= ' termination_notes = ' . $this->sqlTerminationNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_line_one = ' . $this->sqlAddressLineOne(). ',' ; } elseif( true == array_key_exists( 'AddressLineOne', $this->getChangedColumns() ) ) { $strSql .= ' address_line_one = ' . $this->sqlAddressLineOne() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_line_two = ' . $this->sqlAddressLineTwo(). ',' ; } elseif( true == array_key_exists( 'AddressLineTwo', $this->getChangedColumns() ) ) { $strSql .= ' address_line_two = ' . $this->sqlAddressLineTwo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_date = ' . $this->sqlRenewalDate(). ',' ; } elseif( true == array_key_exists( 'RenewalDate', $this->getChangedColumns() ) ) { $strSql .= ' renewal_date = ' . $this->sqlRenewalDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_implemented = ' . $this->sqlIsAutoImplemented(). ',' ; } elseif( true == array_key_exists( 'IsAutoImplemented', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_implemented = ' . $this->sqlIsAutoImplemented() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_dialer_calls = ' . $this->sqlBlockDialerCalls(). ',' ; } elseif( true == array_key_exists( 'BlockDialerCalls', $this->getChangedColumns() ) ) { $strSql .= ' block_dialer_calls = ' . $this->sqlBlockDialerCalls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_multiple_account_properties = ' . $this->sqlAllowMultipleAccountProperties(). ',' ; } elseif( true == array_key_exists( 'AllowMultipleAccountProperties', $this->getChangedColumns() ) ) { $strSql .= ' allow_multiple_account_properties = ' . $this->sqlAllowMultipleAccountProperties() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_implementation_portal = ' . $this->sqlShowImplementationPortal(). ',' ; } elseif( true == array_key_exists( 'ShowImplementationPortal', $this->getChangedColumns() ) ) { $strSql .= ' show_implementation_portal = ' . $this->sqlShowImplementationPortal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_risk_status = ' . $this->sqlClientRiskStatus(). ',' ; } elseif( true == array_key_exists( 'ClientRiskStatus', $this->getChangedColumns() ) ) { $strSql .= ' client_risk_status = ' . $this->sqlClientRiskStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key_client_override_rank = ' . $this->sqlKeyClientOverrideRank(). ',' ; } elseif( true == array_key_exists( 'KeyClientOverrideRank', $this->getChangedColumns() ) ) { $strSql .= ' key_client_override_rank = ' . $this->sqlKeyClientOverrideRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key_client_acv_rank = ' . $this->sqlKeyClientAcvRank(). ',' ; } elseif( true == array_key_exists( 'KeyClientAcvRank', $this->getChangedColumns() ) ) { $strSql .= ' key_client_acv_rank = ' . $this->sqlKeyClientAcvRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_logo_timer_start = ' . $this->sqlNewLogoTimerStart(). ',' ; } elseif( true == array_key_exists( 'NewLogoTimerStart', $this->getChangedColumns() ) ) { $strSql .= ' new_logo_timer_start = ' . $this->sqlNewLogoTimerStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_tasks_emailed_on = ' . $this->sqlCompanyTasksEmailedOn(). ',' ; } elseif( true == array_key_exists( 'CompanyTasksEmailedOn', $this->getChangedColumns() ) ) { $strSql .= ' company_tasks_emailed_on = ' . $this->sqlCompanyTasksEmailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_tasks_emailed_on = ' . $this->sqlInternalTasksEmailedOn(). ',' ; } elseif( true == array_key_exists( 'InternalTasksEmailedOn', $this->getChangedColumns() ) ) { $strSql .= ' internal_tasks_emailed_on = ' . $this->sqlInternalTasksEmailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_renewal_date = ' . $this->sqlContractRenewalDate(). ',' ; } elseif( true == array_key_exists( 'ContractRenewalDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_renewal_date = ' . $this->sqlContractRenewalDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_start_date = ' . $this->sqlDelinquencyStartDate(). ',' ; } elseif( true == array_key_exists( 'DelinquencyStartDate', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_start_date = ' . $this->sqlDelinquencyStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate(). ',' ; } elseif( true == array_key_exists( 'TerminationDate', $this->getChangedColumns() ) ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_stipulations = ' . $this->sqlContractStipulations(). ',' ; } elseif( true == array_key_exists( 'ContractStipulations', $this->getChangedColumns() ) ) { $strSql .= ' contract_stipulations = ' . $this->sqlContractStipulations() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_renewal_on = ' . $this->sqlFirstRenewalOn(). ',' ; } elseif( true == array_key_exists( 'FirstRenewalOn', $this->getChangedColumns() ) ) { $strSql .= ' first_renewal_on = ' . $this->sqlFirstRenewalOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_renewed_on = ' . $this->sqlLastRenewedOn(). ',' ; } elseif( true == array_key_exists( 'LastRenewedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_renewed_on = ' . $this->sqlLastRenewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_contacted_by = ' . $this->sqlLastContactedBy(). ',' ; } elseif( true == array_key_exists( 'LastContactedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_contacted_by = ' . $this->sqlLastContactedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_contacted_on = ' . $this->sqlLastContactedOn(). ',' ; } elseif( true == array_key_exists( 'LastContactedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_contacted_on = ' . $this->sqlLastContactedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' self_provisioning_note = ' . $this->sqlSelfProvisioningNote(). ',' ; } elseif( true == array_key_exists( 'SelfProvisioningNote', $this->getChangedColumns() ) ) { $strSql .= ' self_provisioning_note = ' . $this->sqlSelfProvisioningNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_billing_employee_id = ' . $this->sqlUtilityBillingEmployeeId(). ',' ; } elseif( true == array_key_exists( 'UtilityBillingEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_billing_employee_id = ' . $this->sqlUtilityBillingEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_management_employee_id = ' . $this->sqlUtilityManagementEmployeeId(). ',' ; } elseif( true == array_key_exists( 'UtilityManagementEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' utility_management_employee_id = ' . $this->sqlUtilityManagementEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_manager_id = ' . $this->sqlUtilityManagerId(). ',' ; } elseif( true == array_key_exists( 'UtilityManagerId', $this->getChangedColumns() ) ) { $strSql .= ' utility_manager_id = ' . $this->sqlUtilityManagerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_stage_reason_id = ' . $this->sqlPsLeadStageReasonId(). ',' ; } elseif( true == array_key_exists( 'PsLeadStageReasonId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_stage_reason_id = ' . $this->sqlPsLeadStageReasonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' annual_increase_percentage = ' . $this->sqlAnnualIncreasePercentage(). ',' ; } elseif( true == array_key_exists( 'AnnualIncreasePercentage', $this->getChangedColumns() ) ) { $strSql .= ' annual_increase_percentage = ' . $this->sqlAnnualIncreasePercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' annual_increase_month = ' . $this->sqlAnnualIncreaseMonth(). ',' ; } elseif( true == array_key_exists( 'AnnualIncreaseMonth', $this->getChangedColumns() ) ) { $strSql .= ' annual_increase_month = ' . $this->sqlAnnualIncreaseMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_increase_percentage = ' . $this->sqlRenewalIncreasePercentage(). ',' ; } elseif( true == array_key_exists( 'RenewalIncreasePercentage', $this->getChangedColumns() ) ) { $strSql .= ' renewal_increase_percentage = ' . $this->sqlRenewalIncreasePercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_sla_support = ' . $this->sqlIsSlaSupport(). ',' ; } elseif( true == array_key_exists( 'IsSlaSupport', $this->getChangedColumns() ) ) { $strSql .= ' is_sla_support = ' . $this->sqlIsSlaSupport() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ps_lead_id' => $this->getPsLeadId(),
			'ps_lead_type_id' => $this->getPsLeadTypeId(),
			'contract_termination_reason_id' => $this->getContractTerminationReasonId(),
			'support_employee_id' => $this->getSupportEmployeeId(),
			'sales_employee_id' => $this->getSalesEmployeeId(),
			'training_employee_id' => $this->getTrainingEmployeeId(),
			'implementation_employee_id' => $this->getImplementationEmployeeId(),
			'executive_sponsor_id' => $this->getExecutiveSponsorId(),
			'first_response_employee_id' => $this->getFirstResponseEmployeeId(),
			'billing_employee_id' => $this->getBillingEmployeeId(),
			'seo_employee_id' => $this->getSeoEmployeeId(),
			'leasing_center_employee_id' => $this->getLeasingCenterEmployeeId(),
			'renewal_frequency_id' => $this->getRenewalFrequencyId(),
			'max_delinquency_level_type_id' => $this->getMaxDelinquencyLevelTypeId(),
			'company_task_interval_days' => $this->getCompanyTaskIntervalDays(),
			'internal_task_interval_days' => $this->getInternalTaskIntervalDays(),
			'contact_interval_months' => $this->getContactIntervalMonths(),
			'small_property_unit_count' => $this->getSmallPropertyUnitCount(),
			'top_nmhc_rank' => $this->getTopNmhcRank(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'primary_website_urls' => $this->getPrimaryWebsiteUrls(),
			'link_for_account_plan' => $this->getLinkForAccountPlan(),
			'ach_identification_number' => $this->getAchIdentificationNumber(),
			'task_notification_emails' => $this->getTaskNotificationEmails(),
			'technical_product_contact_email' => $this->getTechnicalProductContactEmail(),
			'termination_notes' => $this->getTerminationNotes(),
			'address_line_one' => $this->getAddressLineOne(),
			'address_line_two' => $this->getAddressLineTwo(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'postal_code' => $this->getPostalCode(),
			'renewal_date' => $this->getRenewalDate(),
			'is_auto_implemented' => $this->getIsAutoImplemented(),
			'block_dialer_calls' => $this->getBlockDialerCalls(),
			'allow_multiple_account_properties' => $this->getAllowMultipleAccountProperties(),
			'show_implementation_portal' => $this->getShowImplementationPortal(),
			'client_risk_status' => $this->getClientRiskStatus(),
			'key_client_override_rank' => $this->getKeyClientOverrideRank(),
			'key_client_acv_rank' => $this->getKeyClientAcvRank(),
			'new_logo_timer_start' => $this->getNewLogoTimerStart(),
			'company_tasks_emailed_on' => $this->getCompanyTasksEmailedOn(),
			'internal_tasks_emailed_on' => $this->getInternalTasksEmailedOn(),
			'contract_renewal_date' => $this->getContractRenewalDate(),
			'delinquency_start_date' => $this->getDelinquencyStartDate(),
			'termination_date' => $this->getTerminationDate(),
			'contract_stipulations' => $this->getContractStipulations(),
			'first_renewal_on' => $this->getFirstRenewalOn(),
			'last_renewed_on' => $this->getLastRenewedOn(),
			'last_contacted_by' => $this->getLastContactedBy(),
			'last_contacted_on' => $this->getLastContactedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'self_provisioning_note' => $this->getSelfProvisioningNote(),
			'utility_billing_employee_id' => $this->getUtilityBillingEmployeeId(),
			'utility_management_employee_id' => $this->getUtilityManagementEmployeeId(),
			'details' => $this->getDetails(),
			'utility_manager_id' => $this->getUtilityManagerId(),
			'ps_lead_stage_reason_id' => $this->getPsLeadStageReasonId(),
			'annual_increase_percentage' => $this->getAnnualIncreasePercentage(),
			'annual_increase_month' => $this->getAnnualIncreaseMonth(),
			'renewal_increase_percentage' => $this->getRenewalIncreasePercentage(),
			'is_sla_support' => $this->getIsSlaSupport()
		);
	}

}
?>