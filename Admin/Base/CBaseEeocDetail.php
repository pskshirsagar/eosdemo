<?php

class CBaseEeocDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.eeoc_details';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_boolIsVolunteered;
	protected $m_strMaritalStatus;
	protected $m_strGender;
	protected $m_strVeteranStatus;
	protected $m_strDisabilityStatus;
	protected $m_strEthnicGroup;
	protected $m_strRacialGroup;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strOtherEeocDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsVolunteered = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['is_volunteered'] ) && $boolDirectSet ) $this->set( 'm_boolIsVolunteered', trim( stripcslashes( $arrValues['is_volunteered'] ) ) ); elseif( isset( $arrValues['is_volunteered'] ) ) $this->setIsVolunteered( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_volunteered'] ) : $arrValues['is_volunteered'] );
		if( isset( $arrValues['marital_status'] ) && $boolDirectSet ) $this->set( 'm_strMaritalStatus', trim( stripcslashes( $arrValues['marital_status'] ) ) ); elseif( isset( $arrValues['marital_status'] ) ) $this->setMaritalStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marital_status'] ) : $arrValues['marital_status'] );
		if( isset( $arrValues['gender'] ) && $boolDirectSet ) $this->set( 'm_strGender', trim( stripcslashes( $arrValues['gender'] ) ) ); elseif( isset( $arrValues['gender'] ) ) $this->setGender( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gender'] ) : $arrValues['gender'] );
		if( isset( $arrValues['veteran_status'] ) && $boolDirectSet ) $this->set( 'm_strVeteranStatus', trim( stripcslashes( $arrValues['veteran_status'] ) ) ); elseif( isset( $arrValues['veteran_status'] ) ) $this->setVeteranStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['veteran_status'] ) : $arrValues['veteran_status'] );
		if( isset( $arrValues['disability_status'] ) && $boolDirectSet ) $this->set( 'm_strDisabilityStatus', trim( stripcslashes( $arrValues['disability_status'] ) ) ); elseif( isset( $arrValues['disability_status'] ) ) $this->setDisabilityStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['disability_status'] ) : $arrValues['disability_status'] );
		if( isset( $arrValues['ethnic_group'] ) && $boolDirectSet ) $this->set( 'm_strEthnicGroup', trim( stripcslashes( $arrValues['ethnic_group'] ) ) ); elseif( isset( $arrValues['ethnic_group'] ) ) $this->setEthnicGroup( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ethnic_group'] ) : $arrValues['ethnic_group'] );
		if( isset( $arrValues['racial_group'] ) && $boolDirectSet ) $this->set( 'm_strRacialGroup', trim( stripcslashes( $arrValues['racial_group'] ) ) ); elseif( isset( $arrValues['racial_group'] ) ) $this->setRacialGroup( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['racial_group'] ) : $arrValues['racial_group'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['other_eeoc_details'] ) && $boolDirectSet ) $this->set( 'm_strOtherEeocDetails', trim( stripcslashes( $arrValues['other_eeoc_details'] ) ) ); elseif( isset( $arrValues['other_eeoc_details'] ) ) $this->setOtherEeocDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['other_eeoc_details'] ) : $arrValues['other_eeoc_details'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setIsVolunteered( $boolIsVolunteered ) {
		$this->set( 'm_boolIsVolunteered', CStrings::strToBool( $boolIsVolunteered ) );
	}

	public function getIsVolunteered() {
		return $this->m_boolIsVolunteered;
	}

	public function sqlIsVolunteered() {
		return ( true == isset( $this->m_boolIsVolunteered ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVolunteered ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMaritalStatus( $strMaritalStatus ) {
		$this->set( 'm_strMaritalStatus', CStrings::strTrimDef( $strMaritalStatus, 25, NULL, true ) );
	}

	public function getMaritalStatus() {
		return $this->m_strMaritalStatus;
	}

	public function sqlMaritalStatus() {
		return ( true == isset( $this->m_strMaritalStatus ) ) ? '\'' . addslashes( $this->m_strMaritalStatus ) . '\'' : 'NULL';
	}

	public function setGender( $strGender ) {
		$this->set( 'm_strGender', CStrings::strTrimDef( $strGender, 25, NULL, true ) );
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function sqlGender() {
		return ( true == isset( $this->m_strGender ) ) ? '\'' . addslashes( $this->m_strGender ) . '\'' : 'NULL';
	}

	public function setVeteranStatus( $strVeteranStatus ) {
		$this->set( 'm_strVeteranStatus', CStrings::strTrimDef( $strVeteranStatus, 25, NULL, true ) );
	}

	public function getVeteranStatus() {
		return $this->m_strVeteranStatus;
	}

	public function sqlVeteranStatus() {
		return ( true == isset( $this->m_strVeteranStatus ) ) ? '\'' . addslashes( $this->m_strVeteranStatus ) . '\'' : 'NULL';
	}

	public function setDisabilityStatus( $strDisabilityStatus ) {
		$this->set( 'm_strDisabilityStatus', CStrings::strTrimDef( $strDisabilityStatus, 25, NULL, true ) );
	}

	public function getDisabilityStatus() {
		return $this->m_strDisabilityStatus;
	}

	public function sqlDisabilityStatus() {
		return ( true == isset( $this->m_strDisabilityStatus ) ) ? '\'' . addslashes( $this->m_strDisabilityStatus ) . '\'' : 'NULL';
	}

	public function setEthnicGroup( $strEthnicGroup ) {
		$this->set( 'm_strEthnicGroup', CStrings::strTrimDef( $strEthnicGroup, 255, NULL, true ) );
	}

	public function getEthnicGroup() {
		return $this->m_strEthnicGroup;
	}

	public function sqlEthnicGroup() {
		return ( true == isset( $this->m_strEthnicGroup ) ) ? '\'' . addslashes( $this->m_strEthnicGroup ) . '\'' : 'NULL';
	}

	public function setRacialGroup( $strRacialGroup ) {
		$this->set( 'm_strRacialGroup', CStrings::strTrimDef( $strRacialGroup, 255, NULL, true ) );
	}

	public function getRacialGroup() {
		return $this->m_strRacialGroup;
	}

	public function sqlRacialGroup() {
		return ( true == isset( $this->m_strRacialGroup ) ) ? '\'' . addslashes( $this->m_strRacialGroup ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOtherEeocDetails( $strOtherEeocDetails ) {
		$this->set( 'm_strOtherEeocDetails', CStrings::strTrimDef( $strOtherEeocDetails, -1, NULL, true ) );
	}

	public function getOtherEeocDetails() {
		return $this->m_strOtherEeocDetails;
	}

	public function sqlOtherEeocDetails() {
		return ( true == isset( $this->m_strOtherEeocDetails ) ) ? '\'' . addslashes( $this->m_strOtherEeocDetails ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, is_volunteered, marital_status, gender, veteran_status, disability_status, ethnic_group, racial_group, updated_by, updated_on, created_by, created_on, other_eeoc_details )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlIsVolunteered() . ', ' .
 						$this->sqlMaritalStatus() . ', ' .
 						$this->sqlGender() . ', ' .
 						$this->sqlVeteranStatus() . ', ' .
 						$this->sqlDisabilityStatus() . ', ' .
 						$this->sqlEthnicGroup() . ', ' .
 						$this->sqlRacialGroup() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlOtherEeocDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_volunteered = ' . $this->sqlIsVolunteered() . ','; } elseif( true == array_key_exists( 'IsVolunteered', $this->getChangedColumns() ) ) { $strSql .= ' is_volunteered = ' . $this->sqlIsVolunteered() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marital_status = ' . $this->sqlMaritalStatus() . ','; } elseif( true == array_key_exists( 'MaritalStatus', $this->getChangedColumns() ) ) { $strSql .= ' marital_status = ' . $this->sqlMaritalStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; } elseif( true == array_key_exists( 'Gender', $this->getChangedColumns() ) ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' veteran_status = ' . $this->sqlVeteranStatus() . ','; } elseif( true == array_key_exists( 'VeteranStatus', $this->getChangedColumns() ) ) { $strSql .= ' veteran_status = ' . $this->sqlVeteranStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disability_status = ' . $this->sqlDisabilityStatus() . ','; } elseif( true == array_key_exists( 'DisabilityStatus', $this->getChangedColumns() ) ) { $strSql .= ' disability_status = ' . $this->sqlDisabilityStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ethnic_group = ' . $this->sqlEthnicGroup() . ','; } elseif( true == array_key_exists( 'EthnicGroup', $this->getChangedColumns() ) ) { $strSql .= ' ethnic_group = ' . $this->sqlEthnicGroup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' racial_group = ' . $this->sqlRacialGroup() . ','; } elseif( true == array_key_exists( 'RacialGroup', $this->getChangedColumns() ) ) { $strSql .= ' racial_group = ' . $this->sqlRacialGroup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' other_eeoc_details = ' . $this->sqlOtherEeocDetails() . ','; } elseif( true == array_key_exists( 'OtherEeocDetails', $this->getChangedColumns() ) ) { $strSql .= ' other_eeoc_details = ' . $this->sqlOtherEeocDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'is_volunteered' => $this->getIsVolunteered(),
			'marital_status' => $this->getMaritalStatus(),
			'gender' => $this->getGender(),
			'veteran_status' => $this->getVeteranStatus(),
			'disability_status' => $this->getDisabilityStatus(),
			'ethnic_group' => $this->getEthnicGroup(),
			'racial_group' => $this->getRacialGroup(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'other_eeoc_details' => $this->getOtherEeocDetails()
		);
	}

}
?>