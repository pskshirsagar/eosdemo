<?php

class CBaseEmployeeTokenTypes extends CEosPluralBase {

	public static function fetchEmployeeTokenTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeTokenType', $objDatabase );
	}

	public static function fetchEmployeeTokenType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeTokenType', $objDatabase );
	}

	public static function fetchEmployeeTokenTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_token_types', $objDatabase );
	}

	public static function fetchEmployeeTokenTypeById( $intId, $objDatabase ) {
		return self::fetchEmployeeTokenType( sprintf( 'SELECT * FROM employee_token_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>