<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsFilters
 * Do not add any new functions to this class.
 */

class CBasePsFilters extends CEosPluralBase {

	/**
	 * @return CPsFilter[]
	 */
	public static function fetchPsFilters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsFilter', $objDatabase );
	}

	/**
	 * @return CPsFilter
	 */
	public static function fetchPsFilter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsFilter', $objDatabase );
	}

	public static function fetchPsFilterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_filters', $objDatabase );
	}

	public static function fetchPsFilterById( $intId, $objDatabase ) {
		return self::fetchPsFilter( sprintf( 'SELECT * FROM ps_filters WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsFiltersByUserId( $intUserId, $objDatabase ) {
		return self::fetchPsFilters( sprintf( 'SELECT * FROM ps_filters WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

	public static function fetchPsFiltersByCid( $intCid, $objDatabase ) {
		return self::fetchPsFilters( sprintf( 'SELECT * FROM ps_filters WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPsFiltersByFilterTypeId( $intFilterTypeId, $objDatabase ) {
		return self::fetchPsFilters( sprintf( 'SELECT * FROM ps_filters WHERE filter_type_id = %d', ( int ) $intFilterTypeId ), $objDatabase );
	}

}
?>