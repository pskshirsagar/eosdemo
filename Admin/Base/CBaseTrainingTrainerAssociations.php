<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingTrainerAssociations
 * Do not add any new functions to this class.
 */

class CBaseTrainingTrainerAssociations extends CEosPluralBase {

	/**
	 * @return CTrainingTrainerAssociation[]
	 */
	public static function fetchTrainingTrainerAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTrainingTrainerAssociation', $objDatabase );
	}

	/**
	 * @return CTrainingTrainerAssociation
	 */
	public static function fetchTrainingTrainerAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTrainingTrainerAssociation', $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'training_trainer_associations', $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationById( $intId, $objDatabase ) {
		return self::fetchTrainingTrainerAssociation( sprintf( 'SELECT * FROM training_trainer_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationsByActionId( $intActionId, $objDatabase ) {
		return self::fetchTrainingTrainerAssociations( sprintf( 'SELECT * FROM training_trainer_associations WHERE action_id = %d', ( int ) $intActionId ), $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationsByEmployeeAssessmentId( $intEmployeeAssessmentId, $objDatabase ) {
		return self::fetchTrainingTrainerAssociations( sprintf( 'SELECT * FROM training_trainer_associations WHERE employee_assessment_id = %d', ( int ) $intEmployeeAssessmentId ), $objDatabase );
	}

}
?>