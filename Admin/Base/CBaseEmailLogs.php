<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmailLogs
 * Do not add any new functions to this class.
 */

class CBaseEmailLogs extends CEosPluralBase {

	/**
	 * @return CEmailLog[]
	 */
	public static function fetchEmailLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmailLog', $objDatabase );
	}

	/**
	 * @return CEmailLog
	 */
	public static function fetchEmailLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailLog', $objDatabase );
	}

	public static function fetchEmailLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_logs', $objDatabase );
	}

	public static function fetchEmailLogById( $intId, $objDatabase ) {
		return self::fetchEmailLog( sprintf( 'SELECT * FROM email_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmailLogsByFromEmployeeId( $intFromEmployeeId, $objDatabase ) {
		return self::fetchEmailLogs( sprintf( 'SELECT * FROM email_logs WHERE from_employee_id = %d', ( int ) $intFromEmployeeId ), $objDatabase );
	}

	public static function fetchEmailLogsByToEmployeeId( $intToEmployeeId, $objDatabase ) {
		return self::fetchEmailLogs( sprintf( 'SELECT * FROM email_logs WHERE to_employee_id = %d', ( int ) $intToEmployeeId ), $objDatabase );
	}

}
?>