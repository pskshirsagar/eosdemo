<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeDependants
 * Do not add any new functions to this class.
 */

class CBaseEmployeeDependants extends CEosPluralBase {

	/**
	 * @return CEmployeeDependant[]
	 */
	public static function fetchEmployeeDependants( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeDependant', $objDatabase );
	}

	/**
	 * @return CEmployeeDependant
	 */
	public static function fetchEmployeeDependant( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeDependant', $objDatabase );
	}

	public static function fetchEmployeeDependantCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_dependants', $objDatabase );
	}

	public static function fetchEmployeeDependantById( $intId, $objDatabase ) {
		return self::fetchEmployeeDependant( sprintf( 'SELECT * FROM employee_dependants WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeDependantsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeDependants( sprintf( 'SELECT * FROM employee_dependants WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeDependantsByDependantTypeId( $intDependantTypeId, $objDatabase ) {
		return self::fetchEmployeeDependants( sprintf( 'SELECT * FROM employee_dependants WHERE dependant_type_id = %d', ( int ) $intDependantTypeId ), $objDatabase );
	}

}
?>