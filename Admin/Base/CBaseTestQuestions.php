<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestQuestions
 * Do not add any new functions to this class.
 */

class CBaseTestQuestions extends CEosPluralBase {

	/**
	 * @return CTestQuestion[]
	 */
	public static function fetchTestQuestions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestQuestion', $objDatabase );
	}

	/**
	 * @return CTestQuestion
	 */
	public static function fetchTestQuestion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestQuestion', $objDatabase );
	}

	public static function fetchTestQuestionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_questions', $objDatabase );
	}

	public static function fetchTestQuestionById( $intId, $objDatabase ) {
		return self::fetchTestQuestion( sprintf( 'SELECT * FROM test_questions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestQuestionsByTestQuestionTypeId( $intTestQuestionTypeId, $objDatabase ) {
		return self::fetchTestQuestions( sprintf( 'SELECT * FROM test_questions WHERE test_question_type_id = %d', ( int ) $intTestQuestionTypeId ), $objDatabase );
	}

	public static function fetchTestQuestionsByTestQuestionGroupId( $intTestQuestionGroupId, $objDatabase ) {
		return self::fetchTestQuestions( sprintf( 'SELECT * FROM test_questions WHERE test_question_group_id = %d', ( int ) $intTestQuestionGroupId ), $objDatabase );
	}

	public static function fetchTestQuestionsByTestLevelTypeId( $intTestLevelTypeId, $objDatabase ) {
		return self::fetchTestQuestions( sprintf( 'SELECT * FROM test_questions WHERE test_level_type_id = %d', ( int ) $intTestLevelTypeId ), $objDatabase );
	}

}
?>