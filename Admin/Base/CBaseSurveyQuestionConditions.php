<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyQuestionConditions
 * Do not add any new functions to this class.
 */

class CBaseSurveyQuestionConditions extends CEosPluralBase {

	/**
	 * @return CSurveyQuestionCondition[]
	 */
	public static function fetchSurveyQuestionConditions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyQuestionCondition', $objDatabase );
	}

	/**
	 * @return CSurveyQuestionCondition
	 */
	public static function fetchSurveyQuestionCondition( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyQuestionCondition', $objDatabase );
	}

	public static function fetchSurveyQuestionConditionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_question_conditions', $objDatabase );
	}

	public static function fetchSurveyQuestionConditionById( $intId, $objDatabase ) {
		return self::fetchSurveyQuestionCondition( sprintf( 'SELECT * FROM survey_question_conditions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveyQuestionConditionsBySurveyQuestionTypeId( $intSurveyQuestionTypeId, $objDatabase ) {
		return self::fetchSurveyQuestionConditions( sprintf( 'SELECT * FROM survey_question_conditions WHERE survey_question_type_id = %d', ( int ) $intSurveyQuestionTypeId ), $objDatabase );
	}

	public static function fetchSurveyQuestionConditionsBySurveyConditionTypeId( $intSurveyConditionTypeId, $objDatabase ) {
		return self::fetchSurveyQuestionConditions( sprintf( 'SELECT * FROM survey_question_conditions WHERE survey_condition_type_id = %d', ( int ) $intSurveyConditionTypeId ), $objDatabase );
	}

}
?>