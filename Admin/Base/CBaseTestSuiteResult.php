<?php

class CBaseTestSuiteResult extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.test_suite_results';

	protected $m_intId;
	protected $m_intTestSuiteId;
	protected $m_intTestSuiteTypeId;
	protected $m_strResultDatetime;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strResultDatetime = 'now()';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['test_suite_id'] ) && $boolDirectSet ) $this->set( 'm_intTestSuiteId', trim( $arrValues['test_suite_id'] ) ); elseif( isset( $arrValues['test_suite_id'] ) ) $this->setTestSuiteId( $arrValues['test_suite_id'] );
		if( isset( $arrValues['test_suite_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTestSuiteTypeId', trim( $arrValues['test_suite_type_id'] ) ); elseif( isset( $arrValues['test_suite_type_id'] ) ) $this->setTestSuiteTypeId( $arrValues['test_suite_type_id'] );
		if( isset( $arrValues['result_datetime'] ) && $boolDirectSet ) $this->set( 'm_strResultDatetime', trim( $arrValues['result_datetime'] ) ); elseif( isset( $arrValues['result_datetime'] ) ) $this->setResultDatetime( $arrValues['result_datetime'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTestSuiteId( $intTestSuiteId ) {
		$this->set( 'm_intTestSuiteId', CStrings::strToIntDef( $intTestSuiteId, NULL, false ) );
	}

	public function getTestSuiteId() {
		return $this->m_intTestSuiteId;
	}

	public function sqlTestSuiteId() {
		return ( true == isset( $this->m_intTestSuiteId ) ) ? ( string ) $this->m_intTestSuiteId : 'NULL';
	}

	public function setTestSuiteTypeId( $intTestSuiteTypeId ) {
		$this->set( 'm_intTestSuiteTypeId', CStrings::strToIntDef( $intTestSuiteTypeId, NULL, false ) );
	}

	public function getTestSuiteTypeId() {
		return $this->m_intTestSuiteTypeId;
	}

	public function sqlTestSuiteTypeId() {
		return ( true == isset( $this->m_intTestSuiteTypeId ) ) ? ( string ) $this->m_intTestSuiteTypeId : 'NULL';
	}

	public function setResultDatetime( $strResultDatetime ) {
		$this->set( 'm_strResultDatetime', CStrings::strTrimDef( $strResultDatetime, -1, NULL, true ) );
	}

	public function getResultDatetime() {
		return $this->m_strResultDatetime;
	}

	public function sqlResultDatetime() {
		return ( true == isset( $this->m_strResultDatetime ) ) ? '\'' . $this->m_strResultDatetime . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, test_suite_id, test_suite_type_id, result_datetime, details, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlTestSuiteId() . ', ' .
						$this->sqlTestSuiteTypeId() . ', ' .
						$this->sqlResultDatetime() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_suite_id = ' . $this->sqlTestSuiteId(). ',' ; } elseif( true == array_key_exists( 'TestSuiteId', $this->getChangedColumns() ) ) { $strSql .= ' test_suite_id = ' . $this->sqlTestSuiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_suite_type_id = ' . $this->sqlTestSuiteTypeId(). ',' ; } elseif( true == array_key_exists( 'TestSuiteTypeId', $this->getChangedColumns() ) ) { $strSql .= ' test_suite_type_id = ' . $this->sqlTestSuiteTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' result_datetime = ' . $this->sqlResultDatetime(). ',' ; } elseif( true == array_key_exists( 'ResultDatetime', $this->getChangedColumns() ) ) { $strSql .= ' result_datetime = ' . $this->sqlResultDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'test_suite_id' => $this->getTestSuiteId(),
			'test_suite_type_id' => $this->getTestSuiteTypeId(),
			'result_datetime' => $this->getResultDatetime(),
			'details' => $this->getDetails(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>