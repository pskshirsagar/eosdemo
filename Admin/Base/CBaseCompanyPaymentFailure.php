<?php

class CBaseCompanyPaymentFailure extends CEosSingularBase {

	const TABLE_NAME = 'public.company_payment_failures';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyPaymentId;
	protected $m_intPaymentTypeId;
	protected $m_intAccountId;
	protected $m_strReturnReason;
	protected $m_fltPaymentAmount;
	protected $m_strFailedOn;
	protected $m_boolIsReviewed;
	protected $m_strNote;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltPaymentAmount = '0';
		$this->m_boolIsReviewed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyPaymentId', trim( $arrValues['company_payment_id'] ) ); elseif( isset( $arrValues['company_payment_id'] ) ) $this->setCompanyPaymentId( $arrValues['company_payment_id'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['return_reason'] ) && $boolDirectSet ) $this->set( 'm_strReturnReason', trim( stripcslashes( $arrValues['return_reason'] ) ) ); elseif( isset( $arrValues['return_reason'] ) ) $this->setReturnReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['return_reason'] ) : $arrValues['return_reason'] );
		if( isset( $arrValues['payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentAmount', trim( $arrValues['payment_amount'] ) ); elseif( isset( $arrValues['payment_amount'] ) ) $this->setPaymentAmount( $arrValues['payment_amount'] );
		if( isset( $arrValues['failed_on'] ) && $boolDirectSet ) $this->set( 'm_strFailedOn', trim( $arrValues['failed_on'] ) ); elseif( isset( $arrValues['failed_on'] ) ) $this->setFailedOn( $arrValues['failed_on'] );
		if( isset( $arrValues['is_reviewed'] ) && $boolDirectSet ) $this->set( 'm_boolIsReviewed', trim( stripcslashes( $arrValues['is_reviewed'] ) ) ); elseif( isset( $arrValues['is_reviewed'] ) ) $this->setIsReviewed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reviewed'] ) : $arrValues['is_reviewed'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyPaymentId( $intCompanyPaymentId ) {
		$this->set( 'm_intCompanyPaymentId', CStrings::strToIntDef( $intCompanyPaymentId, NULL, false ) );
	}

	public function getCompanyPaymentId() {
		return $this->m_intCompanyPaymentId;
	}

	public function sqlCompanyPaymentId() {
		return ( true == isset( $this->m_intCompanyPaymentId ) ) ? ( string ) $this->m_intCompanyPaymentId : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setReturnReason( $strReturnReason ) {
		$this->set( 'm_strReturnReason', CStrings::strTrimDef( $strReturnReason, 240, NULL, true ) );
	}

	public function getReturnReason() {
		return $this->m_strReturnReason;
	}

	public function sqlReturnReason() {
		return ( true == isset( $this->m_strReturnReason ) ) ? '\'' . addslashes( $this->m_strReturnReason ) . '\'' : 'NULL';
	}

	public function setPaymentAmount( $fltPaymentAmount ) {
		$this->set( 'm_fltPaymentAmount', CStrings::strToFloatDef( $fltPaymentAmount, NULL, false, 2 ) );
	}

	public function getPaymentAmount() {
		return $this->m_fltPaymentAmount;
	}

	public function sqlPaymentAmount() {
		return ( true == isset( $this->m_fltPaymentAmount ) ) ? ( string ) $this->m_fltPaymentAmount : '0';
	}

	public function setFailedOn( $strFailedOn ) {
		$this->set( 'm_strFailedOn', CStrings::strTrimDef( $strFailedOn, -1, NULL, true ) );
	}

	public function getFailedOn() {
		return $this->m_strFailedOn;
	}

	public function sqlFailedOn() {
		return ( true == isset( $this->m_strFailedOn ) ) ? '\'' . $this->m_strFailedOn . '\'' : 'NULL';
	}

	public function setIsReviewed( $boolIsReviewed ) {
		$this->set( 'm_boolIsReviewed', CStrings::strToBool( $boolIsReviewed ) );
	}

	public function getIsReviewed() {
		return $this->m_boolIsReviewed;
	}

	public function sqlIsReviewed() {
		return ( true == isset( $this->m_boolIsReviewed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReviewed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, -1, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_payment_id, payment_type_id, account_id, return_reason, payment_amount, failed_on, is_reviewed, note, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyPaymentId() . ', ' .
						$this->sqlPaymentTypeId() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlReturnReason() . ', ' .
						$this->sqlPaymentAmount() . ', ' .
						$this->sqlFailedOn() . ', ' .
						$this->sqlIsReviewed() . ', ' .
						$this->sqlNote() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId(). ',' ; } elseif( true == array_key_exists( 'CompanyPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_reason = ' . $this->sqlReturnReason(). ',' ; } elseif( true == array_key_exists( 'ReturnReason', $this->getChangedColumns() ) ) { $strSql .= ' return_reason = ' . $this->sqlReturnReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount(). ',' ; } elseif( true == array_key_exists( 'PaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn(). ',' ; } elseif( true == array_key_exists( 'FailedOn', $this->getChangedColumns() ) ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reviewed = ' . $this->sqlIsReviewed(). ',' ; } elseif( true == array_key_exists( 'IsReviewed', $this->getChangedColumns() ) ) { $strSql .= ' is_reviewed = ' . $this->sqlIsReviewed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote(). ',' ; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_payment_id' => $this->getCompanyPaymentId(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'account_id' => $this->getAccountId(),
			'return_reason' => $this->getReturnReason(),
			'payment_amount' => $this->getPaymentAmount(),
			'failed_on' => $this->getFailedOn(),
			'is_reviewed' => $this->getIsReviewed(),
			'note' => $this->getNote(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>