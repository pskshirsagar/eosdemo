<?php

class CBaseAdStat extends CEosSingularBase {

	const TABLE_NAME = 'public.ad_stats';

	protected $m_intId;
	protected $m_intAdId;
	protected $m_strDate;
	protected $m_intViewCount;
	protected $m_intClickCount;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intViewCount = '0';
		$this->m_intClickCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ad_id'] ) && $boolDirectSet ) $this->set( 'm_intAdId', trim( $arrValues['ad_id'] ) ); elseif( isset( $arrValues['ad_id'] ) ) $this->setAdId( $arrValues['ad_id'] );
		if( isset( $arrValues['date'] ) && $boolDirectSet ) $this->set( 'm_strDate', trim( $arrValues['date'] ) ); elseif( isset( $arrValues['date'] ) ) $this->setDate( $arrValues['date'] );
		if( isset( $arrValues['view_count'] ) && $boolDirectSet ) $this->set( 'm_intViewCount', trim( $arrValues['view_count'] ) ); elseif( isset( $arrValues['view_count'] ) ) $this->setViewCount( $arrValues['view_count'] );
		if( isset( $arrValues['click_count'] ) && $boolDirectSet ) $this->set( 'm_intClickCount', trim( $arrValues['click_count'] ) ); elseif( isset( $arrValues['click_count'] ) ) $this->setClickCount( $arrValues['click_count'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAdId( $intAdId ) {
		$this->set( 'm_intAdId', CStrings::strToIntDef( $intAdId, NULL, false ) );
	}

	public function getAdId() {
		return $this->m_intAdId;
	}

	public function sqlAdId() {
		return ( true == isset( $this->m_intAdId ) ) ? ( string ) $this->m_intAdId : 'NULL';
	}

	public function setDate( $strDate ) {
		$this->set( 'm_strDate', CStrings::strTrimDef( $strDate, -1, NULL, true ) );
	}

	public function getDate() {
		return $this->m_strDate;
	}

	public function sqlDate() {
		return ( true == isset( $this->m_strDate ) ) ? '\'' . $this->m_strDate . '\'' : 'NOW()';
	}

	public function setViewCount( $intViewCount ) {
		$this->set( 'm_intViewCount', CStrings::strToIntDef( $intViewCount, NULL, false ) );
	}

	public function getViewCount() {
		return $this->m_intViewCount;
	}

	public function sqlViewCount() {
		return ( true == isset( $this->m_intViewCount ) ) ? ( string ) $this->m_intViewCount : '0';
	}

	public function setClickCount( $intClickCount ) {
		$this->set( 'm_intClickCount', CStrings::strToIntDef( $intClickCount, NULL, false ) );
	}

	public function getClickCount() {
		return $this->m_intClickCount;
	}

	public function sqlClickCount() {
		return ( true == isset( $this->m_intClickCount ) ) ? ( string ) $this->m_intClickCount : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ad_id, date, view_count, click_count, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlAdId() . ', ' .
 						$this->sqlDate() . ', ' .
 						$this->sqlViewCount() . ', ' .
 						$this->sqlClickCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ad_id = ' . $this->sqlAdId() . ','; } elseif( true == array_key_exists( 'AdId', $this->getChangedColumns() ) ) { $strSql .= ' ad_id = ' . $this->sqlAdId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date = ' . $this->sqlDate() . ','; } elseif( true == array_key_exists( 'Date', $this->getChangedColumns() ) ) { $strSql .= ' date = ' . $this->sqlDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' view_count = ' . $this->sqlViewCount() . ','; } elseif( true == array_key_exists( 'ViewCount', $this->getChangedColumns() ) ) { $strSql .= ' view_count = ' . $this->sqlViewCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' click_count = ' . $this->sqlClickCount() . ','; } elseif( true == array_key_exists( 'ClickCount', $this->getChangedColumns() ) ) { $strSql .= ' click_count = ' . $this->sqlClickCount() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ad_id' => $this->getAdId(),
			'date' => $this->getDate(),
			'view_count' => $this->getViewCount(),
			'click_count' => $this->getClickCount(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>