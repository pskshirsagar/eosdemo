<?php

class CBaseTestCaseDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.test_case_details';

	protected $m_intId;
	protected $m_intTaskId;
	protected $m_intOldTestCaseId;
	protected $m_arrintAssociatedTaskIds;
	protected $m_arrintTaskAcceptanceCriteriaIds;
	protected $m_intQaManagerEmployeeId;
	protected $m_intTestCaseExecutionTypeId;
	protected $m_intTestCaseProductFolderId;
	protected $m_intReviewerEmployeeId;
	protected $m_strPreconditions;
	protected $m_strTestData;
	protected $m_strReviewedOn;
	protected $m_boolCanBeAutomated;
	protected $m_intAutomationStatus;
	protected $m_intLoadTestStatus;
	protected $m_strScriptName;
	protected $m_boolLoadTestCandidate;
	protected $m_boolIsArchive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strSqmApprovedOn;
	protected $m_strSqmAuditedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolCanBeAutomated = false;
		$this->m_boolLoadTestCandidate = false;
		$this->m_boolIsArchive = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['old_test_case_id'] ) && $boolDirectSet ) $this->set( 'm_intOldTestCaseId', trim( $arrValues['old_test_case_id'] ) ); elseif( isset( $arrValues['old_test_case_id'] ) ) $this->setOldTestCaseId( $arrValues['old_test_case_id'] );
		if( isset( $arrValues['associated_task_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintAssociatedTaskIds', trim( $arrValues['associated_task_ids'] ) ); elseif( isset( $arrValues['associated_task_ids'] ) ) $this->setAssociatedTaskIds( $arrValues['associated_task_ids'] );
		if( isset( $arrValues['task_acceptance_criteria_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintTaskAcceptanceCriteriaIds', trim( $arrValues['task_acceptance_criteria_ids'] ) ); elseif( isset( $arrValues['task_acceptance_criteria_ids'] ) ) $this->setTaskAcceptanceCriteriaIds( $arrValues['task_acceptance_criteria_ids'] );
		if( isset( $arrValues['qa_manager_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQaManagerEmployeeId', trim( $arrValues['qa_manager_employee_id'] ) ); elseif( isset( $arrValues['qa_manager_employee_id'] ) ) $this->setQaManagerEmployeeId( $arrValues['qa_manager_employee_id'] );
		if( isset( $arrValues['test_case_execution_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTestCaseExecutionTypeId', trim( $arrValues['test_case_execution_type_id'] ) ); elseif( isset( $arrValues['test_case_execution_type_id'] ) ) $this->setTestCaseExecutionTypeId( $arrValues['test_case_execution_type_id'] );
		if( isset( $arrValues['test_case_product_folder_id'] ) && $boolDirectSet ) $this->set( 'm_intTestCaseProductFolderId', trim( $arrValues['test_case_product_folder_id'] ) ); elseif( isset( $arrValues['test_case_product_folder_id'] ) ) $this->setTestCaseProductFolderId( $arrValues['test_case_product_folder_id'] );
		if( isset( $arrValues['reviewer_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intReviewerEmployeeId', trim( $arrValues['reviewer_employee_id'] ) ); elseif( isset( $arrValues['reviewer_employee_id'] ) ) $this->setReviewerEmployeeId( $arrValues['reviewer_employee_id'] );
		if( isset( $arrValues['preconditions'] ) && $boolDirectSet ) $this->set( 'm_strPreconditions', trim( $arrValues['preconditions'] ) ); elseif( isset( $arrValues['preconditions'] ) ) $this->setPreconditions( $arrValues['preconditions'] );
		if( isset( $arrValues['test_data'] ) && $boolDirectSet ) $this->set( 'm_strTestData', trim( $arrValues['test_data'] ) ); elseif( isset( $arrValues['test_data'] ) ) $this->setTestData( $arrValues['test_data'] );
		if( isset( $arrValues['reviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strReviewedOn', trim( $arrValues['reviewed_on'] ) ); elseif( isset( $arrValues['reviewed_on'] ) ) $this->setReviewedOn( $arrValues['reviewed_on'] );
		if( isset( $arrValues['can_be_automated'] ) && $boolDirectSet ) $this->set( 'm_boolCanBeAutomated', trim( stripcslashes( $arrValues['can_be_automated'] ) ) ); elseif( isset( $arrValues['can_be_automated'] ) ) $this->setCanBeAutomated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['can_be_automated'] ) : $arrValues['can_be_automated'] );
		if( isset( $arrValues['automation_status'] ) && $boolDirectSet ) $this->set( 'm_intAutomationStatus', trim( $arrValues['automation_status'] ) ); elseif( isset( $arrValues['automation_status'] ) ) $this->setAutomationStatus( $arrValues['automation_status'] );
		if( isset( $arrValues['load_test_status'] ) && $boolDirectSet ) $this->set( 'm_intLoadTestStatus', trim( $arrValues['load_test_status'] ) ); elseif( isset( $arrValues['load_test_status'] ) ) $this->setLoadTestStatus( $arrValues['load_test_status'] );
		if( isset( $arrValues['script_name'] ) && $boolDirectSet ) $this->set( 'm_strScriptName', trim( $arrValues['script_name'] ) ); elseif( isset( $arrValues['script_name'] ) ) $this->setScriptName( $arrValues['script_name'] );
		if( isset( $arrValues['load_test_candidate'] ) && $boolDirectSet ) $this->set( 'm_boolLoadTestCandidate', trim( stripcslashes( $arrValues['load_test_candidate'] ) ) ); elseif( isset( $arrValues['load_test_candidate'] ) ) $this->setLoadTestCandidate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['load_test_candidate'] ) : $arrValues['load_test_candidate'] );
		if( isset( $arrValues['is_archive'] ) && $boolDirectSet ) $this->set( 'm_boolIsArchive', trim( stripcslashes( $arrValues['is_archive'] ) ) ); elseif( isset( $arrValues['is_archive'] ) ) $this->setIsArchive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_archive'] ) : $arrValues['is_archive'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['sqm_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strSqmApprovedOn', trim( $arrValues['sqm_approved_on'] ) ); elseif( isset( $arrValues['sqm_approved_on'] ) ) $this->setSqmApprovedOn( $arrValues['sqm_approved_on'] );
		if( isset( $arrValues['sqm_audited_on'] ) && $boolDirectSet ) $this->set( 'm_strSqmAuditedOn', trim( $arrValues['sqm_audited_on'] ) ); elseif( isset( $arrValues['sqm_audited_on'] ) ) $this->setSqmAuditedOn( $arrValues['sqm_audited_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setOldTestCaseId( $intOldTestCaseId ) {
		$this->set( 'm_intOldTestCaseId', CStrings::strToIntDef( $intOldTestCaseId, NULL, false ) );
	}

	public function getOldTestCaseId() {
		return $this->m_intOldTestCaseId;
	}

	public function sqlOldTestCaseId() {
		return ( true == isset( $this->m_intOldTestCaseId ) ) ? ( string ) $this->m_intOldTestCaseId : 'NULL';
	}

	public function setAssociatedTaskIds( $arrintAssociatedTaskIds ) {
		$this->set( 'm_arrintAssociatedTaskIds', CStrings::strToArrIntDef( $arrintAssociatedTaskIds, NULL ) );
	}

	public function getAssociatedTaskIds() {
		return $this->m_arrintAssociatedTaskIds;
	}

	public function sqlAssociatedTaskIds() {
		return ( true == isset( $this->m_arrintAssociatedTaskIds ) && true == valArr( $this->m_arrintAssociatedTaskIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintAssociatedTaskIds, NULL ) . '\'' : 'NULL';
	}

	public function setTaskAcceptanceCriteriaIds( $arrintTaskAcceptanceCriteriaIds ) {
		$this->set( 'm_arrintTaskAcceptanceCriteriaIds', CStrings::strToArrIntDef( $arrintTaskAcceptanceCriteriaIds, NULL ) );
	}

	public function getTaskAcceptanceCriteriaIds() {
		return $this->m_arrintTaskAcceptanceCriteriaIds;
	}

	public function sqlTaskAcceptanceCriteriaIds() {
		return ( true == isset( $this->m_arrintTaskAcceptanceCriteriaIds ) && true == valArr( $this->m_arrintTaskAcceptanceCriteriaIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintTaskAcceptanceCriteriaIds, NULL ) . '\'' : 'NULL';
	}

	public function setQaManagerEmployeeId( $intQaManagerEmployeeId ) {
		$this->set( 'm_intQaManagerEmployeeId', CStrings::strToIntDef( $intQaManagerEmployeeId, NULL, false ) );
	}

	public function getQaManagerEmployeeId() {
		return $this->m_intQaManagerEmployeeId;
	}

	public function sqlQaManagerEmployeeId() {
		return ( true == isset( $this->m_intQaManagerEmployeeId ) ) ? ( string ) $this->m_intQaManagerEmployeeId : 'NULL';
	}

	public function setTestCaseExecutionTypeId( $intTestCaseExecutionTypeId ) {
		$this->set( 'm_intTestCaseExecutionTypeId', CStrings::strToIntDef( $intTestCaseExecutionTypeId, NULL, false ) );
	}

	public function getTestCaseExecutionTypeId() {
		return $this->m_intTestCaseExecutionTypeId;
	}

	public function sqlTestCaseExecutionTypeId() {
		return ( true == isset( $this->m_intTestCaseExecutionTypeId ) ) ? ( string ) $this->m_intTestCaseExecutionTypeId : 'NULL';
	}

	public function setTestCaseProductFolderId( $intTestCaseProductFolderId ) {
		$this->set( 'm_intTestCaseProductFolderId', CStrings::strToIntDef( $intTestCaseProductFolderId, NULL, false ) );
	}

	public function getTestCaseProductFolderId() {
		return $this->m_intTestCaseProductFolderId;
	}

	public function sqlTestCaseProductFolderId() {
		return ( true == isset( $this->m_intTestCaseProductFolderId ) ) ? ( string ) $this->m_intTestCaseProductFolderId : 'NULL';
	}

	public function setReviewerEmployeeId( $intReviewerEmployeeId ) {
		$this->set( 'm_intReviewerEmployeeId', CStrings::strToIntDef( $intReviewerEmployeeId, NULL, false ) );
	}

	public function getReviewerEmployeeId() {
		return $this->m_intReviewerEmployeeId;
	}

	public function sqlReviewerEmployeeId() {
		return ( true == isset( $this->m_intReviewerEmployeeId ) ) ? ( string ) $this->m_intReviewerEmployeeId : 'NULL';
	}

	public function setPreconditions( $strPreconditions ) {
		$this->set( 'm_strPreconditions', CStrings::strTrimDef( $strPreconditions, -1, NULL, true ) );
	}

	public function getPreconditions() {
		return $this->m_strPreconditions;
	}

	public function sqlPreconditions() {
		return ( true == isset( $this->m_strPreconditions ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreconditions ) : '\'' . addslashes( $this->m_strPreconditions ) . '\'' ) : 'NULL';
	}

	public function setTestData( $strTestData ) {
		$this->set( 'm_strTestData', CStrings::strTrimDef( $strTestData, -1, NULL, true ) );
	}

	public function getTestData() {
		return $this->m_strTestData;
	}

	public function sqlTestData() {
		return ( true == isset( $this->m_strTestData ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTestData ) : '\'' . addslashes( $this->m_strTestData ) . '\'' ) : 'NULL';
	}

	public function setReviewedOn( $strReviewedOn ) {
		$this->set( 'm_strReviewedOn', CStrings::strTrimDef( $strReviewedOn, -1, NULL, true ) );
	}

	public function getReviewedOn() {
		return $this->m_strReviewedOn;
	}

	public function sqlReviewedOn() {
		return ( true == isset( $this->m_strReviewedOn ) ) ? '\'' . $this->m_strReviewedOn . '\'' : 'NULL';
	}

	public function setCanBeAutomated( $boolCanBeAutomated ) {
		$this->set( 'm_boolCanBeAutomated', CStrings::strToBool( $boolCanBeAutomated ) );
	}

	public function getCanBeAutomated() {
		return $this->m_boolCanBeAutomated;
	}

	public function sqlCanBeAutomated() {
		return ( true == isset( $this->m_boolCanBeAutomated ) ) ? '\'' . ( true == ( bool ) $this->m_boolCanBeAutomated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAutomationStatus( $intAutomationStatus ) {
		$this->set( 'm_intAutomationStatus', CStrings::strToIntDef( $intAutomationStatus, NULL, false ) );
	}

	public function getAutomationStatus() {
		return $this->m_intAutomationStatus;
	}

	public function sqlAutomationStatus() {
		return ( true == isset( $this->m_intAutomationStatus ) ) ? ( string ) $this->m_intAutomationStatus : 'NULL';
	}

	public function setLoadTestStatus( $intLoadTestStatus ) {
		$this->set( 'm_intLoadTestStatus', CStrings::strToIntDef( $intLoadTestStatus, NULL, false ) );
	}

	public function getLoadTestStatus() {
		return $this->m_intLoadTestStatus;
	}

	public function sqlLoadTestStatus() {
		return ( true == isset( $this->m_intLoadTestStatus ) ) ? ( string ) $this->m_intLoadTestStatus : 'NULL';
	}

	public function setScriptName( $strScriptName ) {
		$this->set( 'm_strScriptName', CStrings::strTrimDef( $strScriptName, -1, NULL, true ) );
	}

	public function getScriptName() {
		return $this->m_strScriptName;
	}

	public function sqlScriptName() {
		return ( true == isset( $this->m_strScriptName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strScriptName ) : '\'' . addslashes( $this->m_strScriptName ) . '\'' ) : 'NULL';
	}

	public function setLoadTestCandidate( $boolLoadTestCandidate ) {
		$this->set( 'm_boolLoadTestCandidate', CStrings::strToBool( $boolLoadTestCandidate ) );
	}

	public function getLoadTestCandidate() {
		return $this->m_boolLoadTestCandidate;
	}

	public function sqlLoadTestCandidate() {
		return ( true == isset( $this->m_boolLoadTestCandidate ) ) ? '\'' . ( true == ( bool ) $this->m_boolLoadTestCandidate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsArchive( $boolIsArchive ) {
		$this->set( 'm_boolIsArchive', CStrings::strToBool( $boolIsArchive ) );
	}

	public function getIsArchive() {
		return $this->m_boolIsArchive;
	}

	public function sqlIsArchive() {
		return ( true == isset( $this->m_boolIsArchive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsArchive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSqmApprovedOn( $strSqmApprovedOn ) {
		$this->set( 'm_strSqmApprovedOn', CStrings::strTrimDef( $strSqmApprovedOn, -1, NULL, true ) );
	}

	public function getSqmApprovedOn() {
		return $this->m_strSqmApprovedOn;
	}

	public function sqlSqmApprovedOn() {
		return ( true == isset( $this->m_strSqmApprovedOn ) ) ? '\'' . $this->m_strSqmApprovedOn . '\'' : 'NULL';
	}

	public function setSqmAuditedOn( $strSqmAuditedOn ) {
		$this->set( 'm_strSqmAuditedOn', CStrings::strTrimDef( $strSqmAuditedOn, -1, NULL, true ) );
	}

	public function getSqmAuditedOn() {
		return $this->m_strSqmAuditedOn;
	}

	public function sqlSqmAuditedOn() {
		return ( true == isset( $this->m_strSqmAuditedOn ) ) ? '\'' . $this->m_strSqmAuditedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, task_id, old_test_case_id, associated_task_ids, task_acceptance_criteria_ids, qa_manager_employee_id, test_case_execution_type_id, test_case_product_folder_id, reviewer_employee_id, preconditions, test_data, reviewed_on, can_be_automated, automation_status, load_test_status, script_name, load_test_candidate, is_archive, updated_by, updated_on, created_by, created_on, sqm_approved_on, sqm_audited_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlTaskId() . ', ' .
						$this->sqlOldTestCaseId() . ', ' .
						$this->sqlAssociatedTaskIds() . ', ' .
						$this->sqlTaskAcceptanceCriteriaIds() . ', ' .
						$this->sqlQaManagerEmployeeId() . ', ' .
						$this->sqlTestCaseExecutionTypeId() . ', ' .
						$this->sqlTestCaseProductFolderId() . ', ' .
						$this->sqlReviewerEmployeeId() . ', ' .
						$this->sqlPreconditions() . ', ' .
						$this->sqlTestData() . ', ' .
						$this->sqlReviewedOn() . ', ' .
						$this->sqlCanBeAutomated() . ', ' .
						$this->sqlAutomationStatus() . ', ' .
						$this->sqlLoadTestStatus() . ', ' .
						$this->sqlScriptName() . ', ' .
						$this->sqlLoadTestCandidate() . ', ' .
						$this->sqlIsArchive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlSqmApprovedOn() . ', ' .
						$this->sqlSqmAuditedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId(). ',' ; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_test_case_id = ' . $this->sqlOldTestCaseId(). ',' ; } elseif( true == array_key_exists( 'OldTestCaseId', $this->getChangedColumns() ) ) { $strSql .= ' old_test_case_id = ' . $this->sqlOldTestCaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' associated_task_ids = ' . $this->sqlAssociatedTaskIds(). ',' ; } elseif( true == array_key_exists( 'AssociatedTaskIds', $this->getChangedColumns() ) ) { $strSql .= ' associated_task_ids = ' . $this->sqlAssociatedTaskIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_acceptance_criteria_ids = ' . $this->sqlTaskAcceptanceCriteriaIds(). ',' ; } elseif( true == array_key_exists( 'TaskAcceptanceCriteriaIds', $this->getChangedColumns() ) ) { $strSql .= ' task_acceptance_criteria_ids = ' . $this->sqlTaskAcceptanceCriteriaIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_manager_employee_id = ' . $this->sqlQaManagerEmployeeId(). ',' ; } elseif( true == array_key_exists( 'QaManagerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qa_manager_employee_id = ' . $this->sqlQaManagerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_execution_type_id = ' . $this->sqlTestCaseExecutionTypeId(). ',' ; } elseif( true == array_key_exists( 'TestCaseExecutionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' test_case_execution_type_id = ' . $this->sqlTestCaseExecutionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_product_folder_id = ' . $this->sqlTestCaseProductFolderId(). ',' ; } elseif( true == array_key_exists( 'TestCaseProductFolderId', $this->getChangedColumns() ) ) { $strSql .= ' test_case_product_folder_id = ' . $this->sqlTestCaseProductFolderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewer_employee_id = ' . $this->sqlReviewerEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ReviewerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' reviewer_employee_id = ' . $this->sqlReviewerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preconditions = ' . $this->sqlPreconditions(). ',' ; } elseif( true == array_key_exists( 'Preconditions', $this->getChangedColumns() ) ) { $strSql .= ' preconditions = ' . $this->sqlPreconditions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_data = ' . $this->sqlTestData(). ',' ; } elseif( true == array_key_exists( 'TestData', $this->getChangedColumns() ) ) { $strSql .= ' test_data = ' . $this->sqlTestData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn(). ',' ; } elseif( true == array_key_exists( 'ReviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' can_be_automated = ' . $this->sqlCanBeAutomated(). ',' ; } elseif( true == array_key_exists( 'CanBeAutomated', $this->getChangedColumns() ) ) { $strSql .= ' can_be_automated = ' . $this->sqlCanBeAutomated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' automation_status = ' . $this->sqlAutomationStatus(). ',' ; } elseif( true == array_key_exists( 'AutomationStatus', $this->getChangedColumns() ) ) { $strSql .= ' automation_status = ' . $this->sqlAutomationStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' load_test_status = ' . $this->sqlLoadTestStatus(). ',' ; } elseif( true == array_key_exists( 'LoadTestStatus', $this->getChangedColumns() ) ) { $strSql .= ' load_test_status = ' . $this->sqlLoadTestStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_name = ' . $this->sqlScriptName(). ',' ; } elseif( true == array_key_exists( 'ScriptName', $this->getChangedColumns() ) ) { $strSql .= ' script_name = ' . $this->sqlScriptName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' load_test_candidate = ' . $this->sqlLoadTestCandidate(). ',' ; } elseif( true == array_key_exists( 'LoadTestCandidate', $this->getChangedColumns() ) ) { $strSql .= ' load_test_candidate = ' . $this->sqlLoadTestCandidate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archive = ' . $this->sqlIsArchive(). ',' ; } elseif( true == array_key_exists( 'IsArchive', $this->getChangedColumns() ) ) { $strSql .= ' is_archive = ' . $this->sqlIsArchive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sqm_approved_on = ' . $this->sqlSqmApprovedOn(). ',' ; } elseif( true == array_key_exists( 'SqmApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' sqm_approved_on = ' . $this->sqlSqmApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sqm_audited_on = ' . $this->sqlSqmAuditedOn(). ',' ; } elseif( true == array_key_exists( 'SqmAuditedOn', $this->getChangedColumns() ) ) { $strSql .= ' sqm_audited_on = ' . $this->sqlSqmAuditedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'task_id' => $this->getTaskId(),
			'old_test_case_id' => $this->getOldTestCaseId(),
			'associated_task_ids' => $this->getAssociatedTaskIds(),
			'task_acceptance_criteria_ids' => $this->getTaskAcceptanceCriteriaIds(),
			'qa_manager_employee_id' => $this->getQaManagerEmployeeId(),
			'test_case_execution_type_id' => $this->getTestCaseExecutionTypeId(),
			'test_case_product_folder_id' => $this->getTestCaseProductFolderId(),
			'reviewer_employee_id' => $this->getReviewerEmployeeId(),
			'preconditions' => $this->getPreconditions(),
			'test_data' => $this->getTestData(),
			'reviewed_on' => $this->getReviewedOn(),
			'can_be_automated' => $this->getCanBeAutomated(),
			'automation_status' => $this->getAutomationStatus(),
			'load_test_status' => $this->getLoadTestStatus(),
			'script_name' => $this->getScriptName(),
			'load_test_candidate' => $this->getLoadTestCandidate(),
			'is_archive' => $this->getIsArchive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'sqm_approved_on' => $this->getSqmApprovedOn(),
			'sqm_audited_on' => $this->getSqmAuditedOn()
		);
	}

}
?>