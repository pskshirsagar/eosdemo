<?php

class CBasePostalCode extends CEosSingularBase {

	const TABLE_NAME = 'public.postal_codes';

	protected $m_strPostalCode;
	protected $m_fltLongitude;
	protected $m_fltLatitude;
	protected $m_strCity;
	protected $m_strCounty;
	protected $m_strStateCode;
	protected $m_strType;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['longitude'] ) && $boolDirectSet ) $this->set( 'm_fltLongitude', trim( $arrValues['longitude'] ) ); elseif( isset( $arrValues['longitude'] ) ) $this->setLongitude( $arrValues['longitude'] );
		if( isset( $arrValues['latitude'] ) && $boolDirectSet ) $this->set( 'm_fltLatitude', trim( $arrValues['latitude'] ) ); elseif( isset( $arrValues['latitude'] ) ) $this->setLatitude( $arrValues['latitude'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['county'] ) && $boolDirectSet ) $this->set( 'm_strCounty', trim( stripcslashes( $arrValues['county'] ) ) ); elseif( isset( $arrValues['county'] ) ) $this->setCounty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['county'] ) : $arrValues['county'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['type'] ) && $boolDirectSet ) $this->set( 'm_strType', trim( stripcslashes( $arrValues['type'] ) ) ); elseif( isset( $arrValues['type'] ) ) $this->setType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['type'] ) : $arrValues['type'] );
		$this->m_boolInitialized = true;
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setLongitude( $fltLongitude ) {
		$this->set( 'm_fltLongitude', CStrings::strToFloatDef( $fltLongitude, NULL, false, 0 ) );
	}

	public function getLongitude() {
		return $this->m_fltLongitude;
	}

	public function sqlLongitude() {
		return ( true == isset( $this->m_fltLongitude ) ) ? ( string ) $this->m_fltLongitude : 'NULL';
	}

	public function setLatitude( $fltLatitude ) {
		$this->set( 'm_fltLatitude', CStrings::strToFloatDef( $fltLatitude, NULL, false, 0 ) );
	}

	public function getLatitude() {
		return $this->m_fltLatitude;
	}

	public function sqlLatitude() {
		return ( true == isset( $this->m_fltLatitude ) ) ? ( string ) $this->m_fltLatitude : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setCounty( $strCounty ) {
		$this->set( 'm_strCounty', CStrings::strTrimDef( $strCounty, 50, NULL, true ) );
	}

	public function getCounty() {
		return $this->m_strCounty;
	}

	public function sqlCounty() {
		return ( true == isset( $this->m_strCounty ) ) ? '\'' . addslashes( $this->m_strCounty ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setType( $strType ) {
		$this->set( 'm_strType', CStrings::strTrimDef( $strType, 240, NULL, true ) );
	}

	public function getType() {
		return $this->m_strType;
	}

	public function sqlType() {
		return ( true == isset( $this->m_strType ) ) ? '\'' . addslashes( $this->m_strType ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'postal_code' => $this->getPostalCode(),
			'longitude' => $this->getLongitude(),
			'latitude' => $this->getLatitude(),
			'city' => $this->getCity(),
			'county' => $this->getCounty(),
			'state_code' => $this->getStateCode(),
			'type' => $this->getType()
		);
	}

}
?>