<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTransferProperties
 * Do not add any new functions to this class.
 */

class CBaseTransferProperties extends CEosPluralBase {

	/**
	 * @return CTransferProperty[]
	 */
	public static function fetchTransferProperties( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTransferProperty', $objDatabase );
	}

	/**
	 * @return CTransferProperty
	 */
	public static function fetchTransferProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTransferProperty', $objDatabase );
	}

	public static function fetchTransferPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transfer_properties', $objDatabase );
	}

	public static function fetchTransferPropertyById( $intId, $objDatabase ) {
		return self::fetchTransferProperty( sprintf( 'SELECT * FROM transfer_properties WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTransferPropertiesByCid( $intCid, $objDatabase ) {
		return self::fetchTransferProperties( sprintf( 'SELECT * FROM transfer_properties WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransferPropertiesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchTransferProperties( sprintf( 'SELECT * FROM transfer_properties WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchTransferPropertiesByPreviousCid( $intPreviousCid, $objDatabase ) {
		return self::fetchTransferProperties( sprintf( 'SELECT * FROM transfer_properties WHERE previous_cid = %d', ( int ) $intPreviousCid ), $objDatabase );
	}

	public static function fetchTransferPropertiesByPreviousPropertyId( $intPreviousPropertyId, $objDatabase ) {
		return self::fetchTransferProperties( sprintf( 'SELECT * FROM transfer_properties WHERE previous_property_id = %d', ( int ) $intPreviousPropertyId ), $objDatabase );
	}

	public static function fetchTransferPropertiesByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchTransferProperties( sprintf( 'SELECT * FROM transfer_properties WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

	public static function fetchTransferPropertiesByPreviousContractTerminationRequestId( $intPreviousContractTerminationRequestId, $objDatabase ) {
		return self::fetchTransferProperties( sprintf( 'SELECT * FROM transfer_properties WHERE previous_contract_termination_request_id = %d', ( int ) $intPreviousContractTerminationRequestId ), $objDatabase );
	}

}
?>