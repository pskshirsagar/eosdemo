<?php

class CBaseSurveyQuestionAnswer extends CEosSingularBase {

	const TABLE_NAME = 'public.survey_question_answers';

	protected $m_intId;
	protected $m_intSurveyTemplateId;
	protected $m_intSurveyId;
	protected $m_intSurveyTemplateQuestionGroupId;
	protected $m_intSurveyQuestionTypeId;
	protected $m_intSurveyTemplateQuestionId;
	protected $m_intSurveyTemplateOptionId;
	protected $m_strAnswer;
	protected $m_strAnswerModified;
	protected $m_strAnswerEmailResponse;
	protected $m_fltNumericWeight;
	protected $m_strScheduledOn;
	protected $m_intFollowUpCalledBy;
	protected $m_strFollowUpCalledOn;
	protected $m_intModifiedBy;
	protected $m_strModifiedOn;
	protected $m_intIsClosed;
	protected $m_boolIsHidden;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsClosed = '0';
		$this->m_boolIsHidden = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['survey_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateId', trim( $arrValues['survey_template_id'] ) ); elseif( isset( $arrValues['survey_template_id'] ) ) $this->setSurveyTemplateId( $arrValues['survey_template_id'] );
		if( isset( $arrValues['survey_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyId', trim( $arrValues['survey_id'] ) ); elseif( isset( $arrValues['survey_id'] ) ) $this->setSurveyId( $arrValues['survey_id'] );
		if( isset( $arrValues['survey_template_question_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateQuestionGroupId', trim( $arrValues['survey_template_question_group_id'] ) ); elseif( isset( $arrValues['survey_template_question_group_id'] ) ) $this->setSurveyTemplateQuestionGroupId( $arrValues['survey_template_question_group_id'] );
		if( isset( $arrValues['survey_question_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyQuestionTypeId', trim( $arrValues['survey_question_type_id'] ) ); elseif( isset( $arrValues['survey_question_type_id'] ) ) $this->setSurveyQuestionTypeId( $arrValues['survey_question_type_id'] );
		if( isset( $arrValues['survey_template_question_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateQuestionId', trim( $arrValues['survey_template_question_id'] ) ); elseif( isset( $arrValues['survey_template_question_id'] ) ) $this->setSurveyTemplateQuestionId( $arrValues['survey_template_question_id'] );
		if( isset( $arrValues['survey_template_option_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateOptionId', trim( $arrValues['survey_template_option_id'] ) ); elseif( isset( $arrValues['survey_template_option_id'] ) ) $this->setSurveyTemplateOptionId( $arrValues['survey_template_option_id'] );
		if( isset( $arrValues['answer'] ) && $boolDirectSet ) $this->set( 'm_strAnswer', trim( stripcslashes( $arrValues['answer'] ) ) ); elseif( isset( $arrValues['answer'] ) ) $this->setAnswer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['answer'] ) : $arrValues['answer'] );
		if( isset( $arrValues['answer_modified'] ) && $boolDirectSet ) $this->set( 'm_strAnswerModified', trim( stripcslashes( $arrValues['answer_modified'] ) ) ); elseif( isset( $arrValues['answer_modified'] ) ) $this->setAnswerModified( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['answer_modified'] ) : $arrValues['answer_modified'] );
		if( isset( $arrValues['answer_email_response'] ) && $boolDirectSet ) $this->set( 'm_strAnswerEmailResponse', trim( stripcslashes( $arrValues['answer_email_response'] ) ) ); elseif( isset( $arrValues['answer_email_response'] ) ) $this->setAnswerEmailResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['answer_email_response'] ) : $arrValues['answer_email_response'] );
		if( isset( $arrValues['numeric_weight'] ) && $boolDirectSet ) $this->set( 'm_fltNumericWeight', trim( $arrValues['numeric_weight'] ) ); elseif( isset( $arrValues['numeric_weight'] ) ) $this->setNumericWeight( $arrValues['numeric_weight'] );
		if( isset( $arrValues['scheduled_on'] ) && $boolDirectSet ) $this->set( 'm_strScheduledOn', trim( $arrValues['scheduled_on'] ) ); elseif( isset( $arrValues['scheduled_on'] ) ) $this->setScheduledOn( $arrValues['scheduled_on'] );
		if( isset( $arrValues['follow_up_called_by'] ) && $boolDirectSet ) $this->set( 'm_intFollowUpCalledBy', trim( $arrValues['follow_up_called_by'] ) ); elseif( isset( $arrValues['follow_up_called_by'] ) ) $this->setFollowUpCalledBy( $arrValues['follow_up_called_by'] );
		if( isset( $arrValues['follow_up_called_on'] ) && $boolDirectSet ) $this->set( 'm_strFollowUpCalledOn', trim( $arrValues['follow_up_called_on'] ) ); elseif( isset( $arrValues['follow_up_called_on'] ) ) $this->setFollowUpCalledOn( $arrValues['follow_up_called_on'] );
		if( isset( $arrValues['modified_by'] ) && $boolDirectSet ) $this->set( 'm_intModifiedBy', trim( $arrValues['modified_by'] ) ); elseif( isset( $arrValues['modified_by'] ) ) $this->setModifiedBy( $arrValues['modified_by'] );
		if( isset( $arrValues['modified_on'] ) && $boolDirectSet ) $this->set( 'm_strModifiedOn', trim( $arrValues['modified_on'] ) ); elseif( isset( $arrValues['modified_on'] ) ) $this->setModifiedOn( $arrValues['modified_on'] );
		if( isset( $arrValues['is_closed'] ) && $boolDirectSet ) $this->set( 'm_intIsClosed', trim( $arrValues['is_closed'] ) ); elseif( isset( $arrValues['is_closed'] ) ) $this->setIsClosed( $arrValues['is_closed'] );
		if( isset( $arrValues['is_hidden'] ) && $boolDirectSet ) $this->set( 'm_boolIsHidden', trim( stripcslashes( $arrValues['is_hidden'] ) ) ); elseif( isset( $arrValues['is_hidden'] ) ) $this->setIsHidden( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hidden'] ) : $arrValues['is_hidden'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSurveyTemplateId( $intSurveyTemplateId ) {
		$this->set( 'm_intSurveyTemplateId', CStrings::strToIntDef( $intSurveyTemplateId, NULL, false ) );
	}

	public function getSurveyTemplateId() {
		return $this->m_intSurveyTemplateId;
	}

	public function sqlSurveyTemplateId() {
		return ( true == isset( $this->m_intSurveyTemplateId ) ) ? ( string ) $this->m_intSurveyTemplateId : 'NULL';
	}

	public function setSurveyId( $intSurveyId ) {
		$this->set( 'm_intSurveyId', CStrings::strToIntDef( $intSurveyId, NULL, false ) );
	}

	public function getSurveyId() {
		return $this->m_intSurveyId;
	}

	public function sqlSurveyId() {
		return ( true == isset( $this->m_intSurveyId ) ) ? ( string ) $this->m_intSurveyId : 'NULL';
	}

	public function setSurveyTemplateQuestionGroupId( $intSurveyTemplateQuestionGroupId ) {
		$this->set( 'm_intSurveyTemplateQuestionGroupId', CStrings::strToIntDef( $intSurveyTemplateQuestionGroupId, NULL, false ) );
	}

	public function getSurveyTemplateQuestionGroupId() {
		return $this->m_intSurveyTemplateQuestionGroupId;
	}

	public function sqlSurveyTemplateQuestionGroupId() {
		return ( true == isset( $this->m_intSurveyTemplateQuestionGroupId ) ) ? ( string ) $this->m_intSurveyTemplateQuestionGroupId : 'NULL';
	}

	public function setSurveyQuestionTypeId( $intSurveyQuestionTypeId ) {
		$this->set( 'm_intSurveyQuestionTypeId', CStrings::strToIntDef( $intSurveyQuestionTypeId, NULL, false ) );
	}

	public function getSurveyQuestionTypeId() {
		return $this->m_intSurveyQuestionTypeId;
	}

	public function sqlSurveyQuestionTypeId() {
		return ( true == isset( $this->m_intSurveyQuestionTypeId ) ) ? ( string ) $this->m_intSurveyQuestionTypeId : 'NULL';
	}

	public function setSurveyTemplateQuestionId( $intSurveyTemplateQuestionId ) {
		$this->set( 'm_intSurveyTemplateQuestionId', CStrings::strToIntDef( $intSurveyTemplateQuestionId, NULL, false ) );
	}

	public function getSurveyTemplateQuestionId() {
		return $this->m_intSurveyTemplateQuestionId;
	}

	public function sqlSurveyTemplateQuestionId() {
		return ( true == isset( $this->m_intSurveyTemplateQuestionId ) ) ? ( string ) $this->m_intSurveyTemplateQuestionId : 'NULL';
	}

	public function setSurveyTemplateOptionId( $intSurveyTemplateOptionId ) {
		$this->set( 'm_intSurveyTemplateOptionId', CStrings::strToIntDef( $intSurveyTemplateOptionId, NULL, false ) );
	}

	public function getSurveyTemplateOptionId() {
		return $this->m_intSurveyTemplateOptionId;
	}

	public function sqlSurveyTemplateOptionId() {
		return ( true == isset( $this->m_intSurveyTemplateOptionId ) ) ? ( string ) $this->m_intSurveyTemplateOptionId : 'NULL';
	}

	public function setAnswer( $strAnswer ) {
		$this->set( 'm_strAnswer', CStrings::strTrimDef( $strAnswer, -1, NULL, true ) );
	}

	public function getAnswer() {
		return $this->m_strAnswer;
	}

	public function sqlAnswer() {
		return ( true == isset( $this->m_strAnswer ) ) ? '\'' . addslashes( $this->m_strAnswer ) . '\'' : 'NULL';
	}

	public function setAnswerModified( $strAnswerModified ) {
		$this->set( 'm_strAnswerModified', CStrings::strTrimDef( $strAnswerModified, -1, NULL, true ) );
	}

	public function getAnswerModified() {
		return $this->m_strAnswerModified;
	}

	public function sqlAnswerModified() {
		return ( true == isset( $this->m_strAnswerModified ) ) ? '\'' . addslashes( $this->m_strAnswerModified ) . '\'' : 'NULL';
	}

	public function setAnswerEmailResponse( $strAnswerEmailResponse ) {
		$this->set( 'm_strAnswerEmailResponse', CStrings::strTrimDef( $strAnswerEmailResponse, -1, NULL, true ) );
	}

	public function getAnswerEmailResponse() {
		return $this->m_strAnswerEmailResponse;
	}

	public function sqlAnswerEmailResponse() {
		return ( true == isset( $this->m_strAnswerEmailResponse ) ) ? '\'' . addslashes( $this->m_strAnswerEmailResponse ) . '\'' : 'NULL';
	}

	public function setNumericWeight( $fltNumericWeight ) {
		$this->set( 'm_fltNumericWeight', CStrings::strToFloatDef( $fltNumericWeight, NULL, false, 2 ) );
	}

	public function getNumericWeight() {
		return $this->m_fltNumericWeight;
	}

	public function sqlNumericWeight() {
		return ( true == isset( $this->m_fltNumericWeight ) ) ? ( string ) $this->m_fltNumericWeight : 'NULL';
	}

	public function setScheduledOn( $strScheduledOn ) {
		$this->set( 'm_strScheduledOn', CStrings::strTrimDef( $strScheduledOn, -1, NULL, true ) );
	}

	public function getScheduledOn() {
		return $this->m_strScheduledOn;
	}

	public function sqlScheduledOn() {
		return ( true == isset( $this->m_strScheduledOn ) ) ? '\'' . $this->m_strScheduledOn . '\'' : 'NULL';
	}

	public function setFollowUpCalledBy( $intFollowUpCalledBy ) {
		$this->set( 'm_intFollowUpCalledBy', CStrings::strToIntDef( $intFollowUpCalledBy, NULL, false ) );
	}

	public function getFollowUpCalledBy() {
		return $this->m_intFollowUpCalledBy;
	}

	public function sqlFollowUpCalledBy() {
		return ( true == isset( $this->m_intFollowUpCalledBy ) ) ? ( string ) $this->m_intFollowUpCalledBy : 'NULL';
	}

	public function setFollowUpCalledOn( $strFollowUpCalledOn ) {
		$this->set( 'm_strFollowUpCalledOn', CStrings::strTrimDef( $strFollowUpCalledOn, -1, NULL, true ) );
	}

	public function getFollowUpCalledOn() {
		return $this->m_strFollowUpCalledOn;
	}

	public function sqlFollowUpCalledOn() {
		return ( true == isset( $this->m_strFollowUpCalledOn ) ) ? '\'' . $this->m_strFollowUpCalledOn . '\'' : 'NULL';
	}

	public function setModifiedBy( $intModifiedBy ) {
		$this->set( 'm_intModifiedBy', CStrings::strToIntDef( $intModifiedBy, NULL, false ) );
	}

	public function getModifiedBy() {
		return $this->m_intModifiedBy;
	}

	public function sqlModifiedBy() {
		return ( true == isset( $this->m_intModifiedBy ) ) ? ( string ) $this->m_intModifiedBy : 'NULL';
	}

	public function setModifiedOn( $strModifiedOn ) {
		$this->set( 'm_strModifiedOn', CStrings::strTrimDef( $strModifiedOn, -1, NULL, true ) );
	}

	public function getModifiedOn() {
		return $this->m_strModifiedOn;
	}

	public function sqlModifiedOn() {
		return ( true == isset( $this->m_strModifiedOn ) ) ? '\'' . $this->m_strModifiedOn . '\'' : 'NULL';
	}

	public function setIsClosed( $intIsClosed ) {
		$this->set( 'm_intIsClosed', CStrings::strToIntDef( $intIsClosed, NULL, false ) );
	}

	public function getIsClosed() {
		return $this->m_intIsClosed;
	}

	public function sqlIsClosed() {
		return ( true == isset( $this->m_intIsClosed ) ) ? ( string ) $this->m_intIsClosed : '0';
	}

	public function setIsHidden( $boolIsHidden ) {
		$this->set( 'm_boolIsHidden', CStrings::strToBool( $boolIsHidden ) );
	}

	public function getIsHidden() {
		return $this->m_boolIsHidden;
	}

	public function sqlIsHidden() {
		return ( true == isset( $this->m_boolIsHidden ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHidden ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, survey_template_id, survey_id, survey_template_question_group_id, survey_question_type_id, survey_template_question_id, survey_template_option_id, answer, answer_modified, answer_email_response, numeric_weight, scheduled_on, follow_up_called_by, follow_up_called_on, modified_by, modified_on, is_closed, is_hidden, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSurveyTemplateId() . ', ' .
 						$this->sqlSurveyId() . ', ' .
 						$this->sqlSurveyTemplateQuestionGroupId() . ', ' .
 						$this->sqlSurveyQuestionTypeId() . ', ' .
 						$this->sqlSurveyTemplateQuestionId() . ', ' .
 						$this->sqlSurveyTemplateOptionId() . ', ' .
 						$this->sqlAnswer() . ', ' .
 						$this->sqlAnswerModified() . ', ' .
 						$this->sqlAnswerEmailResponse() . ', ' .
 						$this->sqlNumericWeight() . ', ' .
 						$this->sqlScheduledOn() . ', ' .
 						$this->sqlFollowUpCalledBy() . ', ' .
 						$this->sqlFollowUpCalledOn() . ', ' .
 						$this->sqlModifiedBy() . ', ' .
 						$this->sqlModifiedOn() . ', ' .
 						$this->sqlIsClosed() . ', ' .
 						$this->sqlIsHidden() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_id = ' . $this->sqlSurveyId() . ','; } elseif( true == array_key_exists( 'SurveyId', $this->getChangedColumns() ) ) { $strSql .= ' survey_id = ' . $this->sqlSurveyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_question_group_id = ' . $this->sqlSurveyTemplateQuestionGroupId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateQuestionGroupId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_question_group_id = ' . $this->sqlSurveyTemplateQuestionGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_question_type_id = ' . $this->sqlSurveyQuestionTypeId() . ','; } elseif( true == array_key_exists( 'SurveyQuestionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' survey_question_type_id = ' . $this->sqlSurveyQuestionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_question_id = ' . $this->sqlSurveyTemplateQuestionId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_question_id = ' . $this->sqlSurveyTemplateQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_option_id = ' . $this->sqlSurveyTemplateOptionId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateOptionId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_option_id = ' . $this->sqlSurveyTemplateOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answer = ' . $this->sqlAnswer() . ','; } elseif( true == array_key_exists( 'Answer', $this->getChangedColumns() ) ) { $strSql .= ' answer = ' . $this->sqlAnswer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answer_modified = ' . $this->sqlAnswerModified() . ','; } elseif( true == array_key_exists( 'AnswerModified', $this->getChangedColumns() ) ) { $strSql .= ' answer_modified = ' . $this->sqlAnswerModified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answer_email_response = ' . $this->sqlAnswerEmailResponse() . ','; } elseif( true == array_key_exists( 'AnswerEmailResponse', $this->getChangedColumns() ) ) { $strSql .= ' answer_email_response = ' . $this->sqlAnswerEmailResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' numeric_weight = ' . $this->sqlNumericWeight() . ','; } elseif( true == array_key_exists( 'NumericWeight', $this->getChangedColumns() ) ) { $strSql .= ' numeric_weight = ' . $this->sqlNumericWeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn() . ','; } elseif( true == array_key_exists( 'ScheduledOn', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' follow_up_called_by = ' . $this->sqlFollowUpCalledBy() . ','; } elseif( true == array_key_exists( 'FollowUpCalledBy', $this->getChangedColumns() ) ) { $strSql .= ' follow_up_called_by = ' . $this->sqlFollowUpCalledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' follow_up_called_on = ' . $this->sqlFollowUpCalledOn() . ','; } elseif( true == array_key_exists( 'FollowUpCalledOn', $this->getChangedColumns() ) ) { $strSql .= ' follow_up_called_on = ' . $this->sqlFollowUpCalledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' modified_by = ' . $this->sqlModifiedBy() . ','; } elseif( true == array_key_exists( 'ModifiedBy', $this->getChangedColumns() ) ) { $strSql .= ' modified_by = ' . $this->sqlModifiedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' modified_on = ' . $this->sqlModifiedOn() . ','; } elseif( true == array_key_exists( 'ModifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' modified_on = ' . $this->sqlModifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_closed = ' . $this->sqlIsClosed() . ','; } elseif( true == array_key_exists( 'IsClosed', $this->getChangedColumns() ) ) { $strSql .= ' is_closed = ' . $this->sqlIsClosed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hidden = ' . $this->sqlIsHidden() . ','; } elseif( true == array_key_exists( 'IsHidden', $this->getChangedColumns() ) ) { $strSql .= ' is_hidden = ' . $this->sqlIsHidden() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'survey_template_id' => $this->getSurveyTemplateId(),
			'survey_id' => $this->getSurveyId(),
			'survey_template_question_group_id' => $this->getSurveyTemplateQuestionGroupId(),
			'survey_question_type_id' => $this->getSurveyQuestionTypeId(),
			'survey_template_question_id' => $this->getSurveyTemplateQuestionId(),
			'survey_template_option_id' => $this->getSurveyTemplateOptionId(),
			'answer' => $this->getAnswer(),
			'answer_modified' => $this->getAnswerModified(),
			'answer_email_response' => $this->getAnswerEmailResponse(),
			'numeric_weight' => $this->getNumericWeight(),
			'scheduled_on' => $this->getScheduledOn(),
			'follow_up_called_by' => $this->getFollowUpCalledBy(),
			'follow_up_called_on' => $this->getFollowUpCalledOn(),
			'modified_by' => $this->getModifiedBy(),
			'modified_on' => $this->getModifiedOn(),
			'is_closed' => $this->getIsClosed(),
			'is_hidden' => $this->getIsHidden(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>