<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsNotifications
 * Do not add any new functions to this class.
 */

class CBasePsNotifications extends CEosPluralBase {

	/**
	 * @return CPsNotification[]
	 */
	public static function fetchPsNotifications( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsNotification', $objDatabase );
	}

	/**
	 * @return CPsNotification
	 */
	public static function fetchPsNotification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsNotification', $objDatabase );
	}

	public static function fetchPsNotificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_notifications', $objDatabase );
	}

	public static function fetchPsNotificationById( $intId, $objDatabase ) {
		return self::fetchPsNotification( sprintf( 'SELECT * FROM ps_notifications WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsNotificationsByPsNotificationTypeId( $intPsNotificationTypeId, $objDatabase ) {
		return self::fetchPsNotifications( sprintf( 'SELECT * FROM ps_notifications WHERE ps_notification_type_id = %d', ( int ) $intPsNotificationTypeId ), $objDatabase );
	}

	public static function fetchPsNotificationsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchPsNotifications( sprintf( 'SELECT * FROM ps_notifications WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchPsNotificationsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchPsNotifications( sprintf( 'SELECT * FROM ps_notifications WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

	public static function fetchPsNotificationsByPsDocumentId( $intPsDocumentId, $objDatabase ) {
		return self::fetchPsNotifications( sprintf( 'SELECT * FROM ps_notifications WHERE ps_document_id = %d', ( int ) $intPsDocumentId ), $objDatabase );
	}

}
?>