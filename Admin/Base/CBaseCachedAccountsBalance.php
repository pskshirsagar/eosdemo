<?php

class CBaseCachedAccountsBalance extends CEosSingularBase {

	const TABLE_NAME = 'public.cached_accounts_balances';

	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_fltTotalAmountDue;
	protected $m_fltTotalRecurringAmount;
	protected $m_fltAmountAgedCurrent;
	protected $m_fltAmountAgedThirty;
	protected $m_fltAmountAgedSixty;
	protected $m_fltAmountAgedNinety;
	protected $m_fltAmountAgedOneTwenty;
	protected $m_fltTotalCreditAmount;
	protected $m_fltTotalChargeAmount;

	public function __construct() {
		parent::__construct();

		$this->m_fltAmountAgedCurrent = ( 0 );
		$this->m_fltAmountAgedThirty = ( 0 );
		$this->m_fltAmountAgedSixty = ( 0 );
		$this->m_fltAmountAgedNinety = ( 0 );
		$this->m_fltAmountAgedOneTwenty = ( 0 );
		$this->m_fltTotalCreditAmount = ( 0 );
		$this->m_fltTotalChargeAmount = ( 0 );

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['total_amount_due'] ) && $boolDirectSet ) $this->set( 'm_fltTotalAmountDue', trim( $arrValues['total_amount_due'] ) ); elseif( isset( $arrValues['total_amount_due'] ) ) $this->setTotalAmountDue( $arrValues['total_amount_due'] );
		if( isset( $arrValues['total_recurring_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalRecurringAmount', trim( $arrValues['total_recurring_amount'] ) ); elseif( isset( $arrValues['total_recurring_amount'] ) ) $this->setTotalRecurringAmount( $arrValues['total_recurring_amount'] );
		if( isset( $arrValues['amount_aged_current'] ) && $boolDirectSet ) $this->set( 'm_fltAmountAgedCurrent', trim( $arrValues['amount_aged_current'] ) ); elseif( isset( $arrValues['amount_aged_current'] ) ) $this->setAmountAgedCurrent( $arrValues['amount_aged_current'] );
		if( isset( $arrValues['amount_aged_thirty'] ) && $boolDirectSet ) $this->set( 'm_fltAmountAgedThirty', trim( $arrValues['amount_aged_thirty'] ) ); elseif( isset( $arrValues['amount_aged_thirty'] ) ) $this->setAmountAgedThirty( $arrValues['amount_aged_thirty'] );
		if( isset( $arrValues['amount_aged_sixty'] ) && $boolDirectSet ) $this->set( 'm_fltAmountAgedSixty', trim( $arrValues['amount_aged_sixty'] ) ); elseif( isset( $arrValues['amount_aged_sixty'] ) ) $this->setAmountAgedSixty( $arrValues['amount_aged_sixty'] );
		if( isset( $arrValues['amount_aged_ninety'] ) && $boolDirectSet ) $this->set( 'm_fltAmountAgedNinety', trim( $arrValues['amount_aged_ninety'] ) ); elseif( isset( $arrValues['amount_aged_ninety'] ) ) $this->setAmountAgedNinety( $arrValues['amount_aged_ninety'] );
		if( isset( $arrValues['amount_aged_one_twenty'] ) && $boolDirectSet ) $this->set( 'm_fltAmountAgedOneTwenty', trim( $arrValues['amount_aged_one_twenty'] ) ); elseif( isset( $arrValues['amount_aged_one_twenty'] ) ) $this->setAmountAgedOneTwenty( $arrValues['amount_aged_one_twenty'] );
		if( isset( $arrValues['total_credit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalCreditAmount', trim( $arrValues['total_credit_amount'] ) ); elseif( isset( $arrValues['total_credit_amount'] ) ) $this->setTotalCreditAmount( $arrValues['total_credit_amount'] );
		if( isset( $arrValues['total_charge_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTotalChargeAmount', trim( $arrValues['total_charge_amount'] ) ); elseif( isset( $arrValues['total_charge_amount'] ) ) $this->setTotalChargeAmount( $arrValues['total_charge_amount'] );
		$this->m_boolInitialized = true;
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setTotalAmountDue( $fltTotalAmountDue ) {
		$this->set( 'm_fltTotalAmountDue', CStrings::strToFloatDef( $fltTotalAmountDue, NULL, false, 4 ) );
	}

	public function getTotalAmountDue() {
		return $this->m_fltTotalAmountDue;
	}

	public function sqlTotalAmountDue() {
		return ( true == isset( $this->m_fltTotalAmountDue ) ) ? ( string ) $this->m_fltTotalAmountDue : 'NULL';
	}

	public function setTotalRecurringAmount( $fltTotalRecurringAmount ) {
		$this->set( 'm_fltTotalRecurringAmount', CStrings::strToFloatDef( $fltTotalRecurringAmount, NULL, false, 4 ) );
	}

	public function getTotalRecurringAmount() {
		return $this->m_fltTotalRecurringAmount;
	}

	public function sqlTotalRecurringAmount() {
		return ( true == isset( $this->m_fltTotalRecurringAmount ) ) ? ( string ) $this->m_fltTotalRecurringAmount : 'NULL';
	}

	public function setAmountAgedCurrent( $fltAmountAgedCurrent ) {
		$this->set( 'm_fltAmountAgedCurrent', CStrings::strToFloatDef( $fltAmountAgedCurrent, NULL, false, 4 ) );
	}

	public function getAmountAgedCurrent() {
		return $this->m_fltAmountAgedCurrent;
	}

	public function sqlAmountAgedCurrent() {
		return ( true == isset( $this->m_fltAmountAgedCurrent ) ) ? ( string ) $this->m_fltAmountAgedCurrent : '( 0 )::numeric';
	}

	public function setAmountAgedThirty( $fltAmountAgedThirty ) {
		$this->set( 'm_fltAmountAgedThirty', CStrings::strToFloatDef( $fltAmountAgedThirty, NULL, false, 4 ) );
	}

	public function getAmountAgedThirty() {
		return $this->m_fltAmountAgedThirty;
	}

	public function sqlAmountAgedThirty() {
		return ( true == isset( $this->m_fltAmountAgedThirty ) ) ? ( string ) $this->m_fltAmountAgedThirty : '( 0 )::numeric';
	}

	public function setAmountAgedSixty( $fltAmountAgedSixty ) {
		$this->set( 'm_fltAmountAgedSixty', CStrings::strToFloatDef( $fltAmountAgedSixty, NULL, false, 4 ) );
	}

	public function getAmountAgedSixty() {
		return $this->m_fltAmountAgedSixty;
	}

	public function sqlAmountAgedSixty() {
		return ( true == isset( $this->m_fltAmountAgedSixty ) ) ? ( string ) $this->m_fltAmountAgedSixty : '( 0 )::numeric';
	}

	public function setAmountAgedNinety( $fltAmountAgedNinety ) {
		$this->set( 'm_fltAmountAgedNinety', CStrings::strToFloatDef( $fltAmountAgedNinety, NULL, false, 4 ) );
	}

	public function getAmountAgedNinety() {
		return $this->m_fltAmountAgedNinety;
	}

	public function sqlAmountAgedNinety() {
		return ( true == isset( $this->m_fltAmountAgedNinety ) ) ? ( string ) $this->m_fltAmountAgedNinety : '( 0 )::numeric';
	}

	public function setAmountAgedOneTwenty( $fltAmountAgedOneTwenty ) {
		$this->set( 'm_fltAmountAgedOneTwenty', CStrings::strToFloatDef( $fltAmountAgedOneTwenty, NULL, false, 4 ) );
	}

	public function getAmountAgedOneTwenty() {
		return $this->m_fltAmountAgedOneTwenty;
	}

	public function sqlAmountAgedOneTwenty() {
		return ( true == isset( $this->m_fltAmountAgedOneTwenty ) ) ? ( string ) $this->m_fltAmountAgedOneTwenty : '( 0 )::numeric';
	}

	public function setTotalCreditAmount( $fltTotalCreditAmount ) {
		$this->set( 'm_fltTotalCreditAmount', CStrings::strToFloatDef( $fltTotalCreditAmount, NULL, false, 4 ) );
	}

	public function getTotalCreditAmount() {
		return $this->m_fltTotalCreditAmount;
	}

	public function sqlTotalCreditAmount() {
		return ( true == isset( $this->m_fltTotalCreditAmount ) ) ? ( string ) $this->m_fltTotalCreditAmount : '( 0 )::numeric';
	}

	public function setTotalChargeAmount( $fltTotalChargeAmount ) {
		$this->set( 'm_fltTotalChargeAmount', CStrings::strToFloatDef( $fltTotalChargeAmount, NULL, false, 4 ) );
	}

	public function getTotalChargeAmount() {
		return $this->m_fltTotalChargeAmount;
	}

	public function sqlTotalChargeAmount() {
		return ( true == isset( $this->m_fltTotalChargeAmount ) ) ? ( string ) $this->m_fltTotalChargeAmount : '( 0 )::numeric';
	}

	public function toArray() {
		return array(
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'total_amount_due' => $this->getTotalAmountDue(),
			'total_recurring_amount' => $this->getTotalRecurringAmount(),
			'amount_aged_current' => $this->getAmountAgedCurrent(),
			'amount_aged_thirty' => $this->getAmountAgedThirty(),
			'amount_aged_sixty' => $this->getAmountAgedSixty(),
			'amount_aged_ninety' => $this->getAmountAgedNinety(),
			'amount_aged_one_twenty' => $this->getAmountAgedOneTwenty(),
			'total_credit_amount' => $this->getTotalCreditAmount(),
			'total_charge_amount' => $this->getTotalChargeAmount()
		);
	}

}
?>