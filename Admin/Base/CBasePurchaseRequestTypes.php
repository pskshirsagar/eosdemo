<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPurchaseRequestTypes
 * Do not add any new functions to this class.
 */

class CBasePurchaseRequestTypes extends CEosPluralBase {

	/**
	 * @return CPurchaseRequestType[]
	 */
	public static function fetchPurchaseRequestTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPurchaseRequestType', $objDatabase );
	}

	/**
	 * @return CPurchaseRequestType
	 */
	public static function fetchPurchaseRequestType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPurchaseRequestType', $objDatabase );
	}

	public static function fetchPurchaseRequestTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'purchase_request_types', $objDatabase );
	}

	public static function fetchPurchaseRequestTypeById( $intId, $objDatabase ) {
		return self::fetchPurchaseRequestType( sprintf( 'SELECT * FROM purchase_request_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>