<?php

class CBaseExpensesStatusType extends CEosSingularBase {

    protected $m_intId;
    protected $m_strName;
    protected $m_boolIsPublished;
    protected $m_intOrderNum;

    public function __construct() {
        parent::__construct();

        $this->m_boolIsPublished = true;
        $this->m_intOrderNum = '0';

        return;
    }

    public function setDefaults() {
        ;
    }

    /**
     * @SuppressWarnings( BooleanArgumentFlag )
     */
    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->m_strName = trim( stripcslashes( $arrValues['name'] ) ); else if( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
        if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->m_boolIsPublished = trim( stripcslashes( $arrValues['is_published'] ) ); else if( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
        if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->m_intOrderNum = trim( $arrValues['order_num'] ); else if( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setName( $strName ) {
        $this->m_strName = CStrings::strTrimDef( $strName, 100, NULL, true );
    }

    public function getName() {
        return $this->m_strName;
    }

    public function sqlName() {
        return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
    }

    public function setIsPublished( $boolIsPublished ) {
        $this->m_boolIsPublished = CStrings::strToBool( $boolIsPublished );
    }

    public function getIsPublished() {
        return $this->m_boolIsPublished;
    }

    public function sqlIsPublished() {
        return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
    }

    public function setOrderNum( $intOrderNum ) {
        $this->m_intOrderNum = CStrings::strToIntDef( $intOrderNum, NULL, false );
    }

    public function getOrderNum() {
        return $this->m_intOrderNum;
    }

    public function sqlOrderNum() {
        return ( true == isset( $this->m_intOrderNum ) ) ? (string) $this->m_intOrderNum : '0';
    }

    public function toArray() {
        return array(
        	'id' => $this->getId(),
        	'name' => $this->getName(),
        	'is_published' => $this->getIsPublished(),
        	'order_num' => $this->getOrderNum()
        );
    }

}
?>