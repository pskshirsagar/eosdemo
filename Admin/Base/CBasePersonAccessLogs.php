<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPersonAccessLogs
 * Do not add any new functions to this class.
 */

class CBasePersonAccessLogs extends CEosPluralBase {

	/**
	 * @return CPersonAccessLog[]
	 */
	public static function fetchPersonAccessLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPersonAccessLog', $objDatabase );
	}

	/**
	 * @return CPersonAccessLog
	 */
	public static function fetchPersonAccessLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPersonAccessLog', $objDatabase );
	}

	public static function fetchPersonAccessLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'person_access_logs', $objDatabase );
	}

	public static function fetchPersonAccessLogById( $intId, $objDatabase ) {
		return self::fetchPersonAccessLog( sprintf( 'SELECT * FROM person_access_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPersonAccessLogsByPersonId( $intPersonId, $objDatabase ) {
		return self::fetchPersonAccessLogs( sprintf( 'SELECT * FROM person_access_logs WHERE person_id = %d', ( int ) $intPersonId ), $objDatabase );
	}

	public static function fetchPersonAccessLogsByPersonRoleId( $intPersonRoleId, $objDatabase ) {
		return self::fetchPersonAccessLogs( sprintf( 'SELECT * FROM person_access_logs WHERE person_role_id = %d', ( int ) $intPersonRoleId ), $objDatabase );
	}

}
?>