<?php

class CBaseCachedActiveCommission extends CEosSingularBase {

	const TABLE_NAME = 'public.cached_active_commissions';

	protected $m_intId;
	protected $m_intCommissionRateAssociationId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPsProductId;
	protected $m_intCommissionStructureId;
	protected $m_intCommissionRateId;
	protected $m_intCommissionRecipientId;
	protected $m_intChargeCodeId;
	protected $m_fltCostAmount;
	protected $m_fltCostPercent;
	protected $m_fltCommissionAmount;
	protected $m_fltCommissionPercent;
	protected $m_fltFrontLoadAmount;
	protected $m_fltFrontLoadPercentage;
	protected $m_intFrontLoadPosts;
	protected $m_fltSetupCommissionPercent;
	protected $m_intIsBookingBased;
	protected $m_intIsTransactional;
	protected $m_intIsSetup;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_fltEffectiveCommissionAmount;
	protected $m_fltPostedCommissions;
	protected $m_intPostedFrontLoads;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['commission_rate_association_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionRateAssociationId', trim( $arrValues['commission_rate_association_id'] ) ); elseif( isset( $arrValues['commission_rate_association_id'] ) ) $this->setCommissionRateAssociationId( $arrValues['commission_rate_association_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['commission_structure_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionStructureId', trim( $arrValues['commission_structure_id'] ) ); elseif( isset( $arrValues['commission_structure_id'] ) ) $this->setCommissionStructureId( $arrValues['commission_structure_id'] );
		if( isset( $arrValues['commission_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionRateId', trim( $arrValues['commission_rate_id'] ) ); elseif( isset( $arrValues['commission_rate_id'] ) ) $this->setCommissionRateId( $arrValues['commission_rate_id'] );
		if( isset( $arrValues['commission_recipient_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionRecipientId', trim( $arrValues['commission_recipient_id'] ) ); elseif( isset( $arrValues['commission_recipient_id'] ) ) $this->setCommissionRecipientId( $arrValues['commission_recipient_id'] );
		if( isset( $arrValues['charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeCodeId', trim( $arrValues['charge_code_id'] ) ); elseif( isset( $arrValues['charge_code_id'] ) ) $this->setChargeCodeId( $arrValues['charge_code_id'] );
		if( isset( $arrValues['cost_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCostAmount', trim( $arrValues['cost_amount'] ) ); elseif( isset( $arrValues['cost_amount'] ) ) $this->setCostAmount( $arrValues['cost_amount'] );
		if( isset( $arrValues['cost_percent'] ) && $boolDirectSet ) $this->set( 'm_fltCostPercent', trim( $arrValues['cost_percent'] ) ); elseif( isset( $arrValues['cost_percent'] ) ) $this->setCostPercent( $arrValues['cost_percent'] );
		if( isset( $arrValues['commission_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCommissionAmount', trim( $arrValues['commission_amount'] ) ); elseif( isset( $arrValues['commission_amount'] ) ) $this->setCommissionAmount( $arrValues['commission_amount'] );
		if( isset( $arrValues['commission_percent'] ) && $boolDirectSet ) $this->set( 'm_fltCommissionPercent', trim( $arrValues['commission_percent'] ) ); elseif( isset( $arrValues['commission_percent'] ) ) $this->setCommissionPercent( $arrValues['commission_percent'] );
		if( isset( $arrValues['front_load_amount'] ) && $boolDirectSet ) $this->set( 'm_fltFrontLoadAmount', trim( $arrValues['front_load_amount'] ) ); elseif( isset( $arrValues['front_load_amount'] ) ) $this->setFrontLoadAmount( $arrValues['front_load_amount'] );
		if( isset( $arrValues['front_load_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltFrontLoadPercentage', trim( $arrValues['front_load_percentage'] ) ); elseif( isset( $arrValues['front_load_percentage'] ) ) $this->setFrontLoadPercentage( $arrValues['front_load_percentage'] );
		if( isset( $arrValues['front_load_posts'] ) && $boolDirectSet ) $this->set( 'm_intFrontLoadPosts', trim( $arrValues['front_load_posts'] ) ); elseif( isset( $arrValues['front_load_posts'] ) ) $this->setFrontLoadPosts( $arrValues['front_load_posts'] );
		if( isset( $arrValues['setup_commission_percent'] ) && $boolDirectSet ) $this->set( 'm_fltSetupCommissionPercent', trim( $arrValues['setup_commission_percent'] ) ); elseif( isset( $arrValues['setup_commission_percent'] ) ) $this->setSetupCommissionPercent( $arrValues['setup_commission_percent'] );
		if( isset( $arrValues['is_booking_based'] ) && $boolDirectSet ) $this->set( 'm_intIsBookingBased', trim( $arrValues['is_booking_based'] ) ); elseif( isset( $arrValues['is_booking_based'] ) ) $this->setIsBookingBased( $arrValues['is_booking_based'] );
		if( isset( $arrValues['is_transactional'] ) && $boolDirectSet ) $this->set( 'm_intIsTransactional', trim( $arrValues['is_transactional'] ) ); elseif( isset( $arrValues['is_transactional'] ) ) $this->setIsTransactional( $arrValues['is_transactional'] );
		if( isset( $arrValues['is_setup'] ) && $boolDirectSet ) $this->set( 'm_intIsSetup', trim( $arrValues['is_setup'] ) ); elseif( isset( $arrValues['is_setup'] ) ) $this->setIsSetup( $arrValues['is_setup'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['effective_commission_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEffectiveCommissionAmount', trim( $arrValues['effective_commission_amount'] ) ); elseif( isset( $arrValues['effective_commission_amount'] ) ) $this->setEffectiveCommissionAmount( $arrValues['effective_commission_amount'] );
		if( isset( $arrValues['posted_commissions'] ) && $boolDirectSet ) $this->set( 'm_fltPostedCommissions', trim( $arrValues['posted_commissions'] ) ); elseif( isset( $arrValues['posted_commissions'] ) ) $this->setPostedCommissions( $arrValues['posted_commissions'] );
		if( isset( $arrValues['posted_front_loads'] ) && $boolDirectSet ) $this->set( 'm_intPostedFrontLoads', trim( $arrValues['posted_front_loads'] ) ); elseif( isset( $arrValues['posted_front_loads'] ) ) $this->setPostedFrontLoads( $arrValues['posted_front_loads'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCommissionRateAssociationId( $intCommissionRateAssociationId ) {
		$this->set( 'm_intCommissionRateAssociationId', CStrings::strToIntDef( $intCommissionRateAssociationId, NULL, false ) );
	}

	public function getCommissionRateAssociationId() {
		return $this->m_intCommissionRateAssociationId;
	}

	public function sqlCommissionRateAssociationId() {
		return ( true == isset( $this->m_intCommissionRateAssociationId ) ) ? ( string ) $this->m_intCommissionRateAssociationId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setCommissionStructureId( $intCommissionStructureId ) {
		$this->set( 'm_intCommissionStructureId', CStrings::strToIntDef( $intCommissionStructureId, NULL, false ) );
	}

	public function getCommissionStructureId() {
		return $this->m_intCommissionStructureId;
	}

	public function sqlCommissionStructureId() {
		return ( true == isset( $this->m_intCommissionStructureId ) ) ? ( string ) $this->m_intCommissionStructureId : 'NULL';
	}

	public function setCommissionRateId( $intCommissionRateId ) {
		$this->set( 'm_intCommissionRateId', CStrings::strToIntDef( $intCommissionRateId, NULL, false ) );
	}

	public function getCommissionRateId() {
		return $this->m_intCommissionRateId;
	}

	public function sqlCommissionRateId() {
		return ( true == isset( $this->m_intCommissionRateId ) ) ? ( string ) $this->m_intCommissionRateId : 'NULL';
	}

	public function setCommissionRecipientId( $intCommissionRecipientId ) {
		$this->set( 'm_intCommissionRecipientId', CStrings::strToIntDef( $intCommissionRecipientId, NULL, false ) );
	}

	public function getCommissionRecipientId() {
		return $this->m_intCommissionRecipientId;
	}

	public function sqlCommissionRecipientId() {
		return ( true == isset( $this->m_intCommissionRecipientId ) ) ? ( string ) $this->m_intCommissionRecipientId : 'NULL';
	}

	public function setChargeCodeId( $intChargeCodeId ) {
		$this->set( 'm_intChargeCodeId', CStrings::strToIntDef( $intChargeCodeId, NULL, false ) );
	}

	public function getChargeCodeId() {
		return $this->m_intChargeCodeId;
	}

	public function sqlChargeCodeId() {
		return ( true == isset( $this->m_intChargeCodeId ) ) ? ( string ) $this->m_intChargeCodeId : 'NULL';
	}

	public function setCostAmount( $fltCostAmount ) {
		$this->set( 'm_fltCostAmount', CStrings::strToFloatDef( $fltCostAmount, NULL, false, 4 ) );
	}

	public function getCostAmount() {
		return $this->m_fltCostAmount;
	}

	public function sqlCostAmount() {
		return ( true == isset( $this->m_fltCostAmount ) ) ? ( string ) $this->m_fltCostAmount : 'NULL';
	}

	public function setCostPercent( $fltCostPercent ) {
		$this->set( 'm_fltCostPercent', CStrings::strToFloatDef( $fltCostPercent, NULL, false, 4 ) );
	}

	public function getCostPercent() {
		return $this->m_fltCostPercent;
	}

	public function sqlCostPercent() {
		return ( true == isset( $this->m_fltCostPercent ) ) ? ( string ) $this->m_fltCostPercent : 'NULL';
	}

	public function setCommissionAmount( $fltCommissionAmount ) {
		$this->set( 'm_fltCommissionAmount', CStrings::strToFloatDef( $fltCommissionAmount, NULL, false, 4 ) );
	}

	public function getCommissionAmount() {
		return $this->m_fltCommissionAmount;
	}

	public function sqlCommissionAmount() {
		return ( true == isset( $this->m_fltCommissionAmount ) ) ? ( string ) $this->m_fltCommissionAmount : 'NULL';
	}

	public function setCommissionPercent( $fltCommissionPercent ) {
		$this->set( 'm_fltCommissionPercent', CStrings::strToFloatDef( $fltCommissionPercent, NULL, false, 6 ) );
	}

	public function getCommissionPercent() {
		return $this->m_fltCommissionPercent;
	}

	public function sqlCommissionPercent() {
		return ( true == isset( $this->m_fltCommissionPercent ) ) ? ( string ) $this->m_fltCommissionPercent : 'NULL';
	}

	public function setFrontLoadAmount( $fltFrontLoadAmount ) {
		$this->set( 'm_fltFrontLoadAmount', CStrings::strToFloatDef( $fltFrontLoadAmount, NULL, false, 4 ) );
	}

	public function getFrontLoadAmount() {
		return $this->m_fltFrontLoadAmount;
	}

	public function sqlFrontLoadAmount() {
		return ( true == isset( $this->m_fltFrontLoadAmount ) ) ? ( string ) $this->m_fltFrontLoadAmount : 'NULL';
	}

	public function setFrontLoadPercentage( $fltFrontLoadPercentage ) {
		$this->set( 'm_fltFrontLoadPercentage', CStrings::strToFloatDef( $fltFrontLoadPercentage, NULL, false, 4 ) );
	}

	public function getFrontLoadPercentage() {
		return $this->m_fltFrontLoadPercentage;
	}

	public function sqlFrontLoadPercentage() {
		return ( true == isset( $this->m_fltFrontLoadPercentage ) ) ? ( string ) $this->m_fltFrontLoadPercentage : 'NULL';
	}

	public function setFrontLoadPosts( $intFrontLoadPosts ) {
		$this->set( 'm_intFrontLoadPosts', CStrings::strToIntDef( $intFrontLoadPosts, NULL, false ) );
	}

	public function getFrontLoadPosts() {
		return $this->m_intFrontLoadPosts;
	}

	public function sqlFrontLoadPosts() {
		return ( true == isset( $this->m_intFrontLoadPosts ) ) ? ( string ) $this->m_intFrontLoadPosts : 'NULL';
	}

	public function setSetupCommissionPercent( $fltSetupCommissionPercent ) {
		$this->set( 'm_fltSetupCommissionPercent', CStrings::strToFloatDef( $fltSetupCommissionPercent, NULL, false, 4 ) );
	}

	public function getSetupCommissionPercent() {
		return $this->m_fltSetupCommissionPercent;
	}

	public function sqlSetupCommissionPercent() {
		return ( true == isset( $this->m_fltSetupCommissionPercent ) ) ? ( string ) $this->m_fltSetupCommissionPercent : 'NULL';
	}

	public function setIsBookingBased( $intIsBookingBased ) {
		$this->set( 'm_intIsBookingBased', CStrings::strToIntDef( $intIsBookingBased, NULL, false ) );
	}

	public function getIsBookingBased() {
		return $this->m_intIsBookingBased;
	}

	public function sqlIsBookingBased() {
		return ( true == isset( $this->m_intIsBookingBased ) ) ? ( string ) $this->m_intIsBookingBased : 'NULL';
	}

	public function setIsTransactional( $intIsTransactional ) {
		$this->set( 'm_intIsTransactional', CStrings::strToIntDef( $intIsTransactional, NULL, false ) );
	}

	public function getIsTransactional() {
		return $this->m_intIsTransactional;
	}

	public function sqlIsTransactional() {
		return ( true == isset( $this->m_intIsTransactional ) ) ? ( string ) $this->m_intIsTransactional : 'NULL';
	}

	public function setIsSetup( $intIsSetup ) {
		$this->set( 'm_intIsSetup', CStrings::strToIntDef( $intIsSetup, NULL, false ) );
	}

	public function getIsSetup() {
		return $this->m_intIsSetup;
	}

	public function sqlIsSetup() {
		return ( true == isset( $this->m_intIsSetup ) ) ? ( string ) $this->m_intIsSetup : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setEffectiveCommissionAmount( $fltEffectiveCommissionAmount ) {
		$this->set( 'm_fltEffectiveCommissionAmount', CStrings::strToFloatDef( $fltEffectiveCommissionAmount, NULL, false, 4 ) );
	}

	public function getEffectiveCommissionAmount() {
		return $this->m_fltEffectiveCommissionAmount;
	}

	public function sqlEffectiveCommissionAmount() {
		return ( true == isset( $this->m_fltEffectiveCommissionAmount ) ) ? ( string ) $this->m_fltEffectiveCommissionAmount : 'NULL';
	}

	public function setPostedCommissions( $fltPostedCommissions ) {
		$this->set( 'm_fltPostedCommissions', CStrings::strToFloatDef( $fltPostedCommissions, NULL, false, 4 ) );
	}

	public function getPostedCommissions() {
		return $this->m_fltPostedCommissions;
	}

	public function sqlPostedCommissions() {
		return ( true == isset( $this->m_fltPostedCommissions ) ) ? ( string ) $this->m_fltPostedCommissions : 'NULL';
	}

	public function setPostedFrontLoads( $intPostedFrontLoads ) {
		$this->set( 'm_intPostedFrontLoads', CStrings::strToIntDef( $intPostedFrontLoads, NULL, false ) );
	}

	public function getPostedFrontLoads() {
		return $this->m_intPostedFrontLoads;
	}

	public function sqlPostedFrontLoads() {
		return ( true == isset( $this->m_intPostedFrontLoads ) ) ? ( string ) $this->m_intPostedFrontLoads : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, commission_rate_association_id, cid, property_id, ps_product_id, commission_structure_id, commission_rate_id, commission_recipient_id, charge_code_id, cost_amount, cost_percent, commission_amount, commission_percent, front_load_amount, front_load_percentage, front_load_posts, setup_commission_percent, is_booking_based, is_transactional, is_setup, start_date, end_date, effective_commission_amount, posted_commissions, posted_front_loads, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCommissionRateAssociationId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlCommissionStructureId() . ', ' .
 						$this->sqlCommissionRateId() . ', ' .
 						$this->sqlCommissionRecipientId() . ', ' .
 						$this->sqlChargeCodeId() . ', ' .
 						$this->sqlCostAmount() . ', ' .
 						$this->sqlCostPercent() . ', ' .
 						$this->sqlCommissionAmount() . ', ' .
 						$this->sqlCommissionPercent() . ', ' .
 						$this->sqlFrontLoadAmount() . ', ' .
 						$this->sqlFrontLoadPercentage() . ', ' .
 						$this->sqlFrontLoadPosts() . ', ' .
 						$this->sqlSetupCommissionPercent() . ', ' .
 						$this->sqlIsBookingBased() . ', ' .
 						$this->sqlIsTransactional() . ', ' .
 						$this->sqlIsSetup() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlEffectiveCommissionAmount() . ', ' .
 						$this->sqlPostedCommissions() . ', ' .
 						$this->sqlPostedFrontLoads() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_rate_association_id = ' . $this->sqlCommissionRateAssociationId() . ','; } elseif( true == array_key_exists( 'CommissionRateAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' commission_rate_association_id = ' . $this->sqlCommissionRateAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_structure_id = ' . $this->sqlCommissionStructureId() . ','; } elseif( true == array_key_exists( 'CommissionStructureId', $this->getChangedColumns() ) ) { $strSql .= ' commission_structure_id = ' . $this->sqlCommissionStructureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_rate_id = ' . $this->sqlCommissionRateId() . ','; } elseif( true == array_key_exists( 'CommissionRateId', $this->getChangedColumns() ) ) { $strSql .= ' commission_rate_id = ' . $this->sqlCommissionRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_recipient_id = ' . $this->sqlCommissionRecipientId() . ','; } elseif( true == array_key_exists( 'CommissionRecipientId', $this->getChangedColumns() ) ) { $strSql .= ' commission_recipient_id = ' . $this->sqlCommissionRecipientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId() . ','; } elseif( true == array_key_exists( 'ChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cost_amount = ' . $this->sqlCostAmount() . ','; } elseif( true == array_key_exists( 'CostAmount', $this->getChangedColumns() ) ) { $strSql .= ' cost_amount = ' . $this->sqlCostAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cost_percent = ' . $this->sqlCostPercent() . ','; } elseif( true == array_key_exists( 'CostPercent', $this->getChangedColumns() ) ) { $strSql .= ' cost_percent = ' . $this->sqlCostPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_amount = ' . $this->sqlCommissionAmount() . ','; } elseif( true == array_key_exists( 'CommissionAmount', $this->getChangedColumns() ) ) { $strSql .= ' commission_amount = ' . $this->sqlCommissionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_percent = ' . $this->sqlCommissionPercent() . ','; } elseif( true == array_key_exists( 'CommissionPercent', $this->getChangedColumns() ) ) { $strSql .= ' commission_percent = ' . $this->sqlCommissionPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' front_load_amount = ' . $this->sqlFrontLoadAmount() . ','; } elseif( true == array_key_exists( 'FrontLoadAmount', $this->getChangedColumns() ) ) { $strSql .= ' front_load_amount = ' . $this->sqlFrontLoadAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' front_load_percentage = ' . $this->sqlFrontLoadPercentage() . ','; } elseif( true == array_key_exists( 'FrontLoadPercentage', $this->getChangedColumns() ) ) { $strSql .= ' front_load_percentage = ' . $this->sqlFrontLoadPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' front_load_posts = ' . $this->sqlFrontLoadPosts() . ','; } elseif( true == array_key_exists( 'FrontLoadPosts', $this->getChangedColumns() ) ) { $strSql .= ' front_load_posts = ' . $this->sqlFrontLoadPosts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' setup_commission_percent = ' . $this->sqlSetupCommissionPercent() . ','; } elseif( true == array_key_exists( 'SetupCommissionPercent', $this->getChangedColumns() ) ) { $strSql .= ' setup_commission_percent = ' . $this->sqlSetupCommissionPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_booking_based = ' . $this->sqlIsBookingBased() . ','; } elseif( true == array_key_exists( 'IsBookingBased', $this->getChangedColumns() ) ) { $strSql .= ' is_booking_based = ' . $this->sqlIsBookingBased() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_transactional = ' . $this->sqlIsTransactional() . ','; } elseif( true == array_key_exists( 'IsTransactional', $this->getChangedColumns() ) ) { $strSql .= ' is_transactional = ' . $this->sqlIsTransactional() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_setup = ' . $this->sqlIsSetup() . ','; } elseif( true == array_key_exists( 'IsSetup', $this->getChangedColumns() ) ) { $strSql .= ' is_setup = ' . $this->sqlIsSetup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_commission_amount = ' . $this->sqlEffectiveCommissionAmount() . ','; } elseif( true == array_key_exists( 'EffectiveCommissionAmount', $this->getChangedColumns() ) ) { $strSql .= ' effective_commission_amount = ' . $this->sqlEffectiveCommissionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_commissions = ' . $this->sqlPostedCommissions() . ','; } elseif( true == array_key_exists( 'PostedCommissions', $this->getChangedColumns() ) ) { $strSql .= ' posted_commissions = ' . $this->sqlPostedCommissions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_front_loads = ' . $this->sqlPostedFrontLoads() . ','; } elseif( true == array_key_exists( 'PostedFrontLoads', $this->getChangedColumns() ) ) { $strSql .= ' posted_front_loads = ' . $this->sqlPostedFrontLoads() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'commission_rate_association_id' => $this->getCommissionRateAssociationId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ps_product_id' => $this->getPsProductId(),
			'commission_structure_id' => $this->getCommissionStructureId(),
			'commission_rate_id' => $this->getCommissionRateId(),
			'commission_recipient_id' => $this->getCommissionRecipientId(),
			'charge_code_id' => $this->getChargeCodeId(),
			'cost_amount' => $this->getCostAmount(),
			'cost_percent' => $this->getCostPercent(),
			'commission_amount' => $this->getCommissionAmount(),
			'commission_percent' => $this->getCommissionPercent(),
			'front_load_amount' => $this->getFrontLoadAmount(),
			'front_load_percentage' => $this->getFrontLoadPercentage(),
			'front_load_posts' => $this->getFrontLoadPosts(),
			'setup_commission_percent' => $this->getSetupCommissionPercent(),
			'is_booking_based' => $this->getIsBookingBased(),
			'is_transactional' => $this->getIsTransactional(),
			'is_setup' => $this->getIsSetup(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'effective_commission_amount' => $this->getEffectiveCommissionAmount(),
			'posted_commissions' => $this->getPostedCommissions(),
			'posted_front_loads' => $this->getPostedFrontLoads(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>