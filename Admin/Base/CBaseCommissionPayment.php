<?php

class CBaseCommissionPayment extends CEosSingularBase {

	const TABLE_NAME = 'public.commission_payments';

	protected $m_intId;
	protected $m_intCommissionRecipientId;
	protected $m_intCommissionBatchId;
	protected $m_intReturnTypeId;
	protected $m_strPaymentDatetime;
	protected $m_fltPaymentAmount;
	protected $m_strPaymentMemo;
	protected $m_strCheckDate;
	protected $m_intCheckNumber;
	protected $m_strCheckPayableTo;
	protected $m_strCheckBankName;
	protected $m_intCheckAccountTypeId;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_strReturnedOn;
	protected $m_intExportedBy;
	protected $m_strExportedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strCurrencyCode;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['commission_recipient_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionRecipientId', trim( $arrValues['commission_recipient_id'] ) ); elseif( isset( $arrValues['commission_recipient_id'] ) ) $this->setCommissionRecipientId( $arrValues['commission_recipient_id'] );
		if( isset( $arrValues['commission_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionBatchId', trim( $arrValues['commission_batch_id'] ) ); elseif( isset( $arrValues['commission_batch_id'] ) ) $this->setCommissionBatchId( $arrValues['commission_batch_id'] );
		if( isset( $arrValues['return_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnTypeId', trim( $arrValues['return_type_id'] ) ); elseif( isset( $arrValues['return_type_id'] ) ) $this->setReturnTypeId( $arrValues['return_type_id'] );
		if( isset( $arrValues['payment_datetime'] ) && $boolDirectSet ) $this->set( 'm_strPaymentDatetime', trim( $arrValues['payment_datetime'] ) ); elseif( isset( $arrValues['payment_datetime'] ) ) $this->setPaymentDatetime( $arrValues['payment_datetime'] );
		if( isset( $arrValues['payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentAmount', trim( $arrValues['payment_amount'] ) ); elseif( isset( $arrValues['payment_amount'] ) ) $this->setPaymentAmount( $arrValues['payment_amount'] );
		if( isset( $arrValues['payment_memo'] ) && $boolDirectSet ) $this->set( 'm_strPaymentMemo', trim( $arrValues['payment_memo'] ) ); elseif( isset( $arrValues['payment_memo'] ) ) $this->setPaymentMemo( $arrValues['payment_memo'] );
		if( isset( $arrValues['check_date'] ) && $boolDirectSet ) $this->set( 'm_strCheckDate', trim( $arrValues['check_date'] ) ); elseif( isset( $arrValues['check_date'] ) ) $this->setCheckDate( $arrValues['check_date'] );
		if( isset( $arrValues['check_number'] ) && $boolDirectSet ) $this->set( 'm_intCheckNumber', trim( $arrValues['check_number'] ) ); elseif( isset( $arrValues['check_number'] ) ) $this->setCheckNumber( $arrValues['check_number'] );
		if( isset( $arrValues['check_payable_to'] ) && $boolDirectSet ) $this->set( 'm_strCheckPayableTo', trim( $arrValues['check_payable_to'] ) ); elseif( isset( $arrValues['check_payable_to'] ) ) $this->setCheckPayableTo( $arrValues['check_payable_to'] );
		if( isset( $arrValues['check_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strCheckBankName', trim( $arrValues['check_bank_name'] ) ); elseif( isset( $arrValues['check_bank_name'] ) ) $this->setCheckBankName( $arrValues['check_bank_name'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckAccountTypeId', trim( $arrValues['check_account_type_id'] ) ); elseif( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckRoutingNumber', trim( $arrValues['check_routing_number'] ) ); elseif( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberEncrypted', trim( $arrValues['check_account_number_encrypted'] ) ); elseif( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['exported_by'] ) && $boolDirectSet ) $this->set( 'm_intExportedBy', trim( $arrValues['exported_by'] ) ); elseif( isset( $arrValues['exported_by'] ) ) $this->setExportedBy( $arrValues['exported_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( $arrValues['currency_code'] ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( $arrValues['currency_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCommissionRecipientId( $intCommissionRecipientId ) {
		$this->set( 'm_intCommissionRecipientId', CStrings::strToIntDef( $intCommissionRecipientId, NULL, false ) );
	}

	public function getCommissionRecipientId() {
		return $this->m_intCommissionRecipientId;
	}

	public function sqlCommissionRecipientId() {
		return ( true == isset( $this->m_intCommissionRecipientId ) ) ? ( string ) $this->m_intCommissionRecipientId : 'NULL';
	}

	public function setCommissionBatchId( $intCommissionBatchId ) {
		$this->set( 'm_intCommissionBatchId', CStrings::strToIntDef( $intCommissionBatchId, NULL, false ) );
	}

	public function getCommissionBatchId() {
		return $this->m_intCommissionBatchId;
	}

	public function sqlCommissionBatchId() {
		return ( true == isset( $this->m_intCommissionBatchId ) ) ? ( string ) $this->m_intCommissionBatchId : 'NULL';
	}

	public function setReturnTypeId( $intReturnTypeId ) {
		$this->set( 'm_intReturnTypeId', CStrings::strToIntDef( $intReturnTypeId, NULL, false ) );
	}

	public function getReturnTypeId() {
		return $this->m_intReturnTypeId;
	}

	public function sqlReturnTypeId() {
		return ( true == isset( $this->m_intReturnTypeId ) ) ? ( string ) $this->m_intReturnTypeId : 'NULL';
	}

	public function setPaymentDatetime( $strPaymentDatetime ) {
		$this->set( 'm_strPaymentDatetime', CStrings::strTrimDef( $strPaymentDatetime, -1, NULL, true ) );
	}

	public function getPaymentDatetime() {
		return $this->m_strPaymentDatetime;
	}

	public function sqlPaymentDatetime() {
		return ( true == isset( $this->m_strPaymentDatetime ) ) ? '\'' . $this->m_strPaymentDatetime . '\'' : 'NOW()';
	}

	public function setPaymentAmount( $fltPaymentAmount ) {
		$this->set( 'm_fltPaymentAmount', CStrings::strToFloatDef( $fltPaymentAmount, NULL, false, 2 ) );
	}

	public function getPaymentAmount() {
		return $this->m_fltPaymentAmount;
	}

	public function sqlPaymentAmount() {
		return ( true == isset( $this->m_fltPaymentAmount ) ) ? ( string ) $this->m_fltPaymentAmount : 'NULL';
	}

	public function setPaymentMemo( $strPaymentMemo ) {
		$this->set( 'm_strPaymentMemo', CStrings::strTrimDef( $strPaymentMemo, 2000, NULL, true ) );
	}

	public function getPaymentMemo() {
		return $this->m_strPaymentMemo;
	}

	public function sqlPaymentMemo() {
		return ( true == isset( $this->m_strPaymentMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPaymentMemo ) : '\'' . addslashes( $this->m_strPaymentMemo ) . '\'' ) : 'NULL';
	}

	public function setCheckDate( $strCheckDate ) {
		$this->set( 'm_strCheckDate', CStrings::strTrimDef( $strCheckDate, -1, NULL, true ) );
	}

	public function getCheckDate() {
		return $this->m_strCheckDate;
	}

	public function sqlCheckDate() {
		return ( true == isset( $this->m_strCheckDate ) ) ? '\'' . $this->m_strCheckDate . '\'' : 'NULL';
	}

	public function setCheckNumber( $intCheckNumber ) {
		$this->set( 'm_intCheckNumber', CStrings::strToIntDef( $intCheckNumber, NULL, false ) );
	}

	public function getCheckNumber() {
		return $this->m_intCheckNumber;
	}

	public function sqlCheckNumber() {
		return ( true == isset( $this->m_intCheckNumber ) ) ? ( string ) $this->m_intCheckNumber : 'NULL';
	}

	public function setCheckPayableTo( $strCheckPayableTo ) {
		$this->set( 'm_strCheckPayableTo', CStrings::strTrimDef( $strCheckPayableTo, 240, NULL, true ) );
	}

	public function getCheckPayableTo() {
		return $this->m_strCheckPayableTo;
	}

	public function sqlCheckPayableTo() {
		return ( true == isset( $this->m_strCheckPayableTo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckPayableTo ) : '\'' . addslashes( $this->m_strCheckPayableTo ) . '\'' ) : 'NULL';
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->set( 'm_strCheckBankName', CStrings::strTrimDef( $strCheckBankName, 100, NULL, true ) );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function sqlCheckBankName() {
		return ( true == isset( $this->m_strCheckBankName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckBankName ) : '\'' . addslashes( $this->m_strCheckBankName ) . '\'' ) : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->set( 'm_intCheckAccountTypeId', CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false ) );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->set( 'm_strCheckRoutingNumber', CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true ) );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckRoutingNumber ) : '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' ) : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setExportedBy( $intExportedBy ) {
		$this->set( 'm_intExportedBy', CStrings::strToIntDef( $intExportedBy, NULL, false ) );
	}

	public function getExportedBy() {
		return $this->m_intExportedBy;
	}

	public function sqlExportedBy() {
		return ( true == isset( $this->m_intExportedBy ) ) ? ( string ) $this->m_intExportedBy : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrencyCode ) : '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' ) : '\'USD\'';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, commission_recipient_id, commission_batch_id, return_type_id, payment_datetime, payment_amount, payment_memo, check_date, check_number, check_payable_to, check_bank_name, check_account_type_id, check_routing_number, check_account_number_encrypted, returned_on, exported_by, exported_on, approved_by, approved_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, currency_code )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCommissionRecipientId() . ', ' .
						$this->sqlCommissionBatchId() . ', ' .
						$this->sqlReturnTypeId() . ', ' .
						$this->sqlPaymentDatetime() . ', ' .
						$this->sqlPaymentAmount() . ', ' .
						$this->sqlPaymentMemo() . ', ' .
						$this->sqlCheckDate() . ', ' .
						$this->sqlCheckNumber() . ', ' .
						$this->sqlCheckPayableTo() . ', ' .
						$this->sqlCheckBankName() . ', ' .
						$this->sqlCheckAccountTypeId() . ', ' .
						$this->sqlCheckRoutingNumber() . ', ' .
						$this->sqlCheckAccountNumberEncrypted() . ', ' .
						$this->sqlReturnedOn() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCurrencyCode() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_recipient_id = ' . $this->sqlCommissionRecipientId(). ',' ; } elseif( true == array_key_exists( 'CommissionRecipientId', $this->getChangedColumns() ) ) { $strSql .= ' commission_recipient_id = ' . $this->sqlCommissionRecipientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_batch_id = ' . $this->sqlCommissionBatchId(). ',' ; } elseif( true == array_key_exists( 'CommissionBatchId', $this->getChangedColumns() ) ) { $strSql .= ' commission_batch_id = ' . $this->sqlCommissionBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId(). ',' ; } elseif( true == array_key_exists( 'ReturnTypeId', $this->getChangedColumns() ) ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_datetime = ' . $this->sqlPaymentDatetime(). ',' ; } elseif( true == array_key_exists( 'PaymentDatetime', $this->getChangedColumns() ) ) { $strSql .= ' payment_datetime = ' . $this->sqlPaymentDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount(). ',' ; } elseif( true == array_key_exists( 'PaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_memo = ' . $this->sqlPaymentMemo(). ',' ; } elseif( true == array_key_exists( 'PaymentMemo', $this->getChangedColumns() ) ) { $strSql .= ' payment_memo = ' . $this->sqlPaymentMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_date = ' . $this->sqlCheckDate(). ',' ; } elseif( true == array_key_exists( 'CheckDate', $this->getChangedColumns() ) ) { $strSql .= ' check_date = ' . $this->sqlCheckDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_number = ' . $this->sqlCheckNumber(). ',' ; } elseif( true == array_key_exists( 'CheckNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_number = ' . $this->sqlCheckNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_payable_to = ' . $this->sqlCheckPayableTo(). ',' ; } elseif( true == array_key_exists( 'CheckPayableTo', $this->getChangedColumns() ) ) { $strSql .= ' check_payable_to = ' . $this->sqlCheckPayableTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName(). ',' ; } elseif( true == array_key_exists( 'CheckBankName', $this->getChangedColumns() ) ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'CheckAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'CheckRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn(). ',' ; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy(). ',' ; } elseif( true == array_key_exists( 'ExportedBy', $this->getChangedColumns() ) ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'commission_recipient_id' => $this->getCommissionRecipientId(),
			'commission_batch_id' => $this->getCommissionBatchId(),
			'return_type_id' => $this->getReturnTypeId(),
			'payment_datetime' => $this->getPaymentDatetime(),
			'payment_amount' => $this->getPaymentAmount(),
			'payment_memo' => $this->getPaymentMemo(),
			'check_date' => $this->getCheckDate(),
			'check_number' => $this->getCheckNumber(),
			'check_payable_to' => $this->getCheckPayableTo(),
			'check_bank_name' => $this->getCheckBankName(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'returned_on' => $this->getReturnedOn(),
			'exported_by' => $this->getExportedBy(),
			'exported_on' => $this->getExportedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'currency_code' => $this->getCurrencyCode()
		);
	}

}
?>