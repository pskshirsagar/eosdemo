<?php

class CBaseInternalTransfer extends CEosSingularBase {

	const TABLE_NAME = 'public.internal_transfers';

	protected $m_intId;
	protected $m_intDebitProcessingBankAccountId;
	protected $m_intCreditProcessingBankAccountId;
	protected $m_intExportBatchId;
	protected $m_intMerchantGatewayId;
	protected $m_intInternalTransferTypeId;
	protected $m_intPaymentTypeId;
	protected $m_intPaymentStatusTypeId;
	protected $m_intReturnTypeId;
	protected $m_strRemoteReferenceNumber;
	protected $m_intDebitEftId;
	protected $m_intCreditEftId;
	protected $m_strPaymentDatetime;
	protected $m_intPaymentAmount;
	protected $m_strPaymentMemo;
	protected $m_strReturnedOn;
	protected $m_strBatchedOn;
	protected $m_strEffectiveOn;
	protected $m_strApprovedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strCurrencyCode;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['debit_processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDebitProcessingBankAccountId', trim( $arrValues['debit_processing_bank_account_id'] ) ); elseif( isset( $arrValues['debit_processing_bank_account_id'] ) ) $this->setDebitProcessingBankAccountId( $arrValues['debit_processing_bank_account_id'] );
		if( isset( $arrValues['credit_processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditProcessingBankAccountId', trim( $arrValues['credit_processing_bank_account_id'] ) ); elseif( isset( $arrValues['credit_processing_bank_account_id'] ) ) $this->setCreditProcessingBankAccountId( $arrValues['credit_processing_bank_account_id'] );
		if( isset( $arrValues['export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intExportBatchId', trim( $arrValues['export_batch_id'] ) ); elseif( isset( $arrValues['export_batch_id'] ) ) $this->setExportBatchId( $arrValues['export_batch_id'] );
		if( isset( $arrValues['merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantGatewayId', trim( $arrValues['merchant_gateway_id'] ) ); elseif( isset( $arrValues['merchant_gateway_id'] ) ) $this->setMerchantGatewayId( $arrValues['merchant_gateway_id'] );
		if( isset( $arrValues['internal_transfer_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInternalTransferTypeId', trim( $arrValues['internal_transfer_type_id'] ) ); elseif( isset( $arrValues['internal_transfer_type_id'] ) ) $this->setInternalTransferTypeId( $arrValues['internal_transfer_type_id'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['payment_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentStatusTypeId', trim( $arrValues['payment_status_type_id'] ) ); elseif( isset( $arrValues['payment_status_type_id'] ) ) $this->setPaymentStatusTypeId( $arrValues['payment_status_type_id'] );
		if( isset( $arrValues['return_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnTypeId', trim( $arrValues['return_type_id'] ) ); elseif( isset( $arrValues['return_type_id'] ) ) $this->setReturnTypeId( $arrValues['return_type_id'] );
		if( isset( $arrValues['remote_reference_number'] ) && $boolDirectSet ) $this->set( 'm_strRemoteReferenceNumber', trim( stripcslashes( $arrValues['remote_reference_number'] ) ) ); elseif( isset( $arrValues['remote_reference_number'] ) ) $this->setRemoteReferenceNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_reference_number'] ) : $arrValues['remote_reference_number'] );
		if( isset( $arrValues['debit_eft_id'] ) && $boolDirectSet ) $this->set( 'm_intDebitEftId', trim( $arrValues['debit_eft_id'] ) ); elseif( isset( $arrValues['debit_eft_id'] ) ) $this->setDebitEftId( $arrValues['debit_eft_id'] );
		if( isset( $arrValues['credit_eft_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditEftId', trim( $arrValues['credit_eft_id'] ) ); elseif( isset( $arrValues['credit_eft_id'] ) ) $this->setCreditEftId( $arrValues['credit_eft_id'] );
		if( isset( $arrValues['payment_datetime'] ) && $boolDirectSet ) $this->set( 'm_strPaymentDatetime', trim( $arrValues['payment_datetime'] ) ); elseif( isset( $arrValues['payment_datetime'] ) ) $this->setPaymentDatetime( $arrValues['payment_datetime'] );
		if( isset( $arrValues['payment_amount'] ) && $boolDirectSet ) $this->set( 'm_intPaymentAmount', trim( $arrValues['payment_amount'] ) ); elseif( isset( $arrValues['payment_amount'] ) ) $this->setPaymentAmount( $arrValues['payment_amount'] );
		if( isset( $arrValues['payment_memo'] ) && $boolDirectSet ) $this->set( 'm_strPaymentMemo', trim( stripcslashes( $arrValues['payment_memo'] ) ) ); elseif( isset( $arrValues['payment_memo'] ) ) $this->setPaymentMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['payment_memo'] ) : $arrValues['payment_memo'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['batched_on'] ) && $boolDirectSet ) $this->set( 'm_strBatchedOn', trim( $arrValues['batched_on'] ) ); elseif( isset( $arrValues['batched_on'] ) ) $this->setBatchedOn( $arrValues['batched_on'] );
		if( isset( $arrValues['effective_on'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveOn', trim( $arrValues['effective_on'] ) ); elseif( isset( $arrValues['effective_on'] ) ) $this->setEffectiveOn( $arrValues['effective_on'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDebitProcessingBankAccountId( $intDebitProcessingBankAccountId ) {
		$this->set( 'm_intDebitProcessingBankAccountId', CStrings::strToIntDef( $intDebitProcessingBankAccountId, NULL, false ) );
	}

	public function getDebitProcessingBankAccountId() {
		return $this->m_intDebitProcessingBankAccountId;
	}

	public function sqlDebitProcessingBankAccountId() {
		return ( true == isset( $this->m_intDebitProcessingBankAccountId ) ) ? ( string ) $this->m_intDebitProcessingBankAccountId : 'NULL';
	}

	public function setCreditProcessingBankAccountId( $intCreditProcessingBankAccountId ) {
		$this->set( 'm_intCreditProcessingBankAccountId', CStrings::strToIntDef( $intCreditProcessingBankAccountId, NULL, false ) );
	}

	public function getCreditProcessingBankAccountId() {
		return $this->m_intCreditProcessingBankAccountId;
	}

	public function sqlCreditProcessingBankAccountId() {
		return ( true == isset( $this->m_intCreditProcessingBankAccountId ) ) ? ( string ) $this->m_intCreditProcessingBankAccountId : 'NULL';
	}

	public function setExportBatchId( $intExportBatchId ) {
		$this->set( 'm_intExportBatchId', CStrings::strToIntDef( $intExportBatchId, NULL, false ) );
	}

	public function getExportBatchId() {
		return $this->m_intExportBatchId;
	}

	public function sqlExportBatchId() {
		return ( true == isset( $this->m_intExportBatchId ) ) ? ( string ) $this->m_intExportBatchId : 'NULL';
	}

	public function setMerchantGatewayId( $intMerchantGatewayId ) {
		$this->set( 'm_intMerchantGatewayId', CStrings::strToIntDef( $intMerchantGatewayId, NULL, false ) );
	}

	public function getMerchantGatewayId() {
		return $this->m_intMerchantGatewayId;
	}

	public function sqlMerchantGatewayId() {
		return ( true == isset( $this->m_intMerchantGatewayId ) ) ? ( string ) $this->m_intMerchantGatewayId : 'NULL';
	}

	public function setInternalTransferTypeId( $intInternalTransferTypeId ) {
		$this->set( 'm_intInternalTransferTypeId', CStrings::strToIntDef( $intInternalTransferTypeId, NULL, false ) );
	}

	public function getInternalTransferTypeId() {
		return $this->m_intInternalTransferTypeId;
	}

	public function sqlInternalTransferTypeId() {
		return ( true == isset( $this->m_intInternalTransferTypeId ) ) ? ( string ) $this->m_intInternalTransferTypeId : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setPaymentStatusTypeId( $intPaymentStatusTypeId ) {
		$this->set( 'm_intPaymentStatusTypeId', CStrings::strToIntDef( $intPaymentStatusTypeId, NULL, false ) );
	}

	public function getPaymentStatusTypeId() {
		return $this->m_intPaymentStatusTypeId;
	}

	public function sqlPaymentStatusTypeId() {
		return ( true == isset( $this->m_intPaymentStatusTypeId ) ) ? ( string ) $this->m_intPaymentStatusTypeId : 'NULL';
	}

	public function setReturnTypeId( $intReturnTypeId ) {
		$this->set( 'm_intReturnTypeId', CStrings::strToIntDef( $intReturnTypeId, NULL, false ) );
	}

	public function getReturnTypeId() {
		return $this->m_intReturnTypeId;
	}

	public function sqlReturnTypeId() {
		return ( true == isset( $this->m_intReturnTypeId ) ) ? ( string ) $this->m_intReturnTypeId : 'NULL';
	}

	public function setRemoteReferenceNumber( $strRemoteReferenceNumber ) {
		$this->set( 'm_strRemoteReferenceNumber', CStrings::strTrimDef( $strRemoteReferenceNumber, 240, NULL, true ) );
	}

	public function getRemoteReferenceNumber() {
		return $this->m_strRemoteReferenceNumber;
	}

	public function sqlRemoteReferenceNumber() {
		return ( true == isset( $this->m_strRemoteReferenceNumber ) ) ? '\'' . addslashes( $this->m_strRemoteReferenceNumber ) . '\'' : 'NULL';
	}

	public function setDebitEftId( $intDebitEftId ) {
		$this->set( 'm_intDebitEftId', CStrings::strToIntDef( $intDebitEftId, NULL, false ) );
	}

	public function getDebitEftId() {
		return $this->m_intDebitEftId;
	}

	public function sqlDebitEftId() {
		return ( true == isset( $this->m_intDebitEftId ) ) ? ( string ) $this->m_intDebitEftId : 'NULL';
	}

	public function setCreditEftId( $intCreditEftId ) {
		$this->set( 'm_intCreditEftId', CStrings::strToIntDef( $intCreditEftId, NULL, false ) );
	}

	public function getCreditEftId() {
		return $this->m_intCreditEftId;
	}

	public function sqlCreditEftId() {
		return ( true == isset( $this->m_intCreditEftId ) ) ? ( string ) $this->m_intCreditEftId : 'NULL';
	}

	public function setPaymentDatetime( $strPaymentDatetime ) {
		$this->set( 'm_strPaymentDatetime', CStrings::strTrimDef( $strPaymentDatetime, -1, NULL, true ) );
	}

	public function getPaymentDatetime() {
		return $this->m_strPaymentDatetime;
	}

	public function sqlPaymentDatetime() {
		return ( true == isset( $this->m_strPaymentDatetime ) ) ? '\'' . $this->m_strPaymentDatetime . '\'' : 'NOW()';
	}

	public function setPaymentAmount( $intPaymentAmount ) {
		$this->set( 'm_intPaymentAmount', CStrings::strToIntDef( $intPaymentAmount, NULL, false ) );
	}

	public function getPaymentAmount() {
		return $this->m_intPaymentAmount;
	}

	public function sqlPaymentAmount() {
		return ( true == isset( $this->m_intPaymentAmount ) ) ? ( string ) $this->m_intPaymentAmount : 'NULL';
	}

	public function setPaymentMemo( $strPaymentMemo ) {
		$this->set( 'm_strPaymentMemo', CStrings::strTrimDef( $strPaymentMemo, 2000, NULL, true ) );
	}

	public function getPaymentMemo() {
		return $this->m_strPaymentMemo;
	}

	public function sqlPaymentMemo() {
		return ( true == isset( $this->m_strPaymentMemo ) ) ? '\'' . addslashes( $this->m_strPaymentMemo ) . '\'' : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setBatchedOn( $strBatchedOn ) {
		$this->set( 'm_strBatchedOn', CStrings::strTrimDef( $strBatchedOn, -1, NULL, true ) );
	}

	public function getBatchedOn() {
		return $this->m_strBatchedOn;
	}

	public function sqlBatchedOn() {
		return ( true == isset( $this->m_strBatchedOn ) ) ? '\'' . $this->m_strBatchedOn . '\'' : 'NULL';
	}

	public function setEffectiveOn( $strEffectiveOn ) {
		$this->set( 'm_strEffectiveOn', CStrings::strTrimDef( $strEffectiveOn, -1, NULL, true ) );
	}

	public function getEffectiveOn() {
		return $this->m_strEffectiveOn;
	}

	public function sqlEffectiveOn() {
		return ( true == isset( $this->m_strEffectiveOn ) ) ? '\'' . $this->m_strEffectiveOn . '\'' : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : '\'USD\'';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, debit_processing_bank_account_id, credit_processing_bank_account_id, export_batch_id, merchant_gateway_id, internal_transfer_type_id, payment_type_id, payment_status_type_id, return_type_id, remote_reference_number, debit_eft_id, credit_eft_id, payment_datetime, payment_amount, payment_memo, returned_on, batched_on, effective_on, approved_on, updated_by, updated_on, created_by, created_on, currency_code )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDebitProcessingBankAccountId() . ', ' .
 						$this->sqlCreditProcessingBankAccountId() . ', ' .
 						$this->sqlExportBatchId() . ', ' .
 						$this->sqlMerchantGatewayId() . ', ' .
 						$this->sqlInternalTransferTypeId() . ', ' .
 						$this->sqlPaymentTypeId() . ', ' .
 						$this->sqlPaymentStatusTypeId() . ', ' .
 						$this->sqlReturnTypeId() . ', ' .
 						$this->sqlRemoteReferenceNumber() . ', ' .
 						$this->sqlDebitEftId() . ', ' .
 						$this->sqlCreditEftId() . ', ' .
 						$this->sqlPaymentDatetime() . ', ' .
 						$this->sqlPaymentAmount() . ', ' .
 						$this->sqlPaymentMemo() . ', ' .
 						$this->sqlReturnedOn() . ', ' .
 						$this->sqlBatchedOn() . ', ' .
 						$this->sqlEffectiveOn() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlCurrencyCode() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' debit_processing_bank_account_id = ' . $this->sqlDebitProcessingBankAccountId() . ','; } elseif( true == array_key_exists( 'DebitProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' debit_processing_bank_account_id = ' . $this->sqlDebitProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_processing_bank_account_id = ' . $this->sqlCreditProcessingBankAccountId() . ','; } elseif( true == array_key_exists( 'CreditProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' credit_processing_bank_account_id = ' . $this->sqlCreditProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_batch_id = ' . $this->sqlExportBatchId() . ','; } elseif( true == array_key_exists( 'ExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' export_batch_id = ' . $this->sqlExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; } elseif( true == array_key_exists( 'MerchantGatewayId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_transfer_type_id = ' . $this->sqlInternalTransferTypeId() . ','; } elseif( true == array_key_exists( 'InternalTransferTypeId', $this->getChangedColumns() ) ) { $strSql .= ' internal_transfer_type_id = ' . $this->sqlInternalTransferTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId() . ','; } elseif( true == array_key_exists( 'PaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_status_type_id = ' . $this->sqlPaymentStatusTypeId() . ','; } elseif( true == array_key_exists( 'PaymentStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_status_type_id = ' . $this->sqlPaymentStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; } elseif( true == array_key_exists( 'ReturnTypeId', $this->getChangedColumns() ) ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_reference_number = ' . $this->sqlRemoteReferenceNumber() . ','; } elseif( true == array_key_exists( 'RemoteReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' remote_reference_number = ' . $this->sqlRemoteReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' debit_eft_id = ' . $this->sqlDebitEftId() . ','; } elseif( true == array_key_exists( 'DebitEftId', $this->getChangedColumns() ) ) { $strSql .= ' debit_eft_id = ' . $this->sqlDebitEftId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_eft_id = ' . $this->sqlCreditEftId() . ','; } elseif( true == array_key_exists( 'CreditEftId', $this->getChangedColumns() ) ) { $strSql .= ' credit_eft_id = ' . $this->sqlCreditEftId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_datetime = ' . $this->sqlPaymentDatetime() . ','; } elseif( true == array_key_exists( 'PaymentDatetime', $this->getChangedColumns() ) ) { $strSql .= ' payment_datetime = ' . $this->sqlPaymentDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount() . ','; } elseif( true == array_key_exists( 'PaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_memo = ' . $this->sqlPaymentMemo() . ','; } elseif( true == array_key_exists( 'PaymentMemo', $this->getChangedColumns() ) ) { $strSql .= ' payment_memo = ' . $this->sqlPaymentMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batched_on = ' . $this->sqlBatchedOn() . ','; } elseif( true == array_key_exists( 'BatchedOn', $this->getChangedColumns() ) ) { $strSql .= ' batched_on = ' . $this->sqlBatchedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_on = ' . $this->sqlEffectiveOn() . ','; } elseif( true == array_key_exists( 'EffectiveOn', $this->getChangedColumns() ) ) { $strSql .= ' effective_on = ' . $this->sqlEffectiveOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'debit_processing_bank_account_id' => $this->getDebitProcessingBankAccountId(),
			'credit_processing_bank_account_id' => $this->getCreditProcessingBankAccountId(),
			'export_batch_id' => $this->getExportBatchId(),
			'merchant_gateway_id' => $this->getMerchantGatewayId(),
			'internal_transfer_type_id' => $this->getInternalTransferTypeId(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'payment_status_type_id' => $this->getPaymentStatusTypeId(),
			'return_type_id' => $this->getReturnTypeId(),
			'remote_reference_number' => $this->getRemoteReferenceNumber(),
			'debit_eft_id' => $this->getDebitEftId(),
			'credit_eft_id' => $this->getCreditEftId(),
			'payment_datetime' => $this->getPaymentDatetime(),
			'payment_amount' => $this->getPaymentAmount(),
			'payment_memo' => $this->getPaymentMemo(),
			'returned_on' => $this->getReturnedOn(),
			'batched_on' => $this->getBatchedOn(),
			'effective_on' => $this->getEffectiveOn(),
			'approved_on' => $this->getApprovedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'currency_code' => $this->getCurrencyCode()
		);
	}

}
?>