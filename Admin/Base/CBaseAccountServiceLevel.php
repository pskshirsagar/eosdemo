<?php

class CBaseAccountServiceLevel extends CEosSingularBase {

	const TABLE_NAME = 'public.account_service_levels';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intEligibleAccountServiceLevelTypeId;
	protected $m_intAccountServiceLevelTypeId;
	protected $m_intSetBy;
	protected $m_strSetOn;
	protected $m_intOverrideBy;
	protected $m_strOverrideOn;
	protected $m_strOverrideReason;
	protected $m_intServiceLevelOptions;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['eligible_account_service_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEligibleAccountServiceLevelTypeId', trim( $arrValues['eligible_account_service_level_type_id'] ) ); elseif( isset( $arrValues['eligible_account_service_level_type_id'] ) ) $this->setEligibleAccountServiceLevelTypeId( $arrValues['eligible_account_service_level_type_id'] );
		if( isset( $arrValues['account_service_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountServiceLevelTypeId', trim( $arrValues['account_service_level_type_id'] ) ); elseif( isset( $arrValues['account_service_level_type_id'] ) ) $this->setAccountServiceLevelTypeId( $arrValues['account_service_level_type_id'] );
		if( isset( $arrValues['set_by'] ) && $boolDirectSet ) $this->set( 'm_intSetBy', trim( $arrValues['set_by'] ) ); elseif( isset( $arrValues['set_by'] ) ) $this->setSetBy( $arrValues['set_by'] );
		if( isset( $arrValues['set_on'] ) && $boolDirectSet ) $this->set( 'm_strSetOn', trim( $arrValues['set_on'] ) ); elseif( isset( $arrValues['set_on'] ) ) $this->setSetOn( $arrValues['set_on'] );
		if( isset( $arrValues['override_by'] ) && $boolDirectSet ) $this->set( 'm_intOverrideBy', trim( $arrValues['override_by'] ) ); elseif( isset( $arrValues['override_by'] ) ) $this->setOverrideBy( $arrValues['override_by'] );
		if( isset( $arrValues['override_on'] ) && $boolDirectSet ) $this->set( 'm_strOverrideOn', trim( $arrValues['override_on'] ) ); elseif( isset( $arrValues['override_on'] ) ) $this->setOverrideOn( $arrValues['override_on'] );
		if( isset( $arrValues['override_reason'] ) && $boolDirectSet ) $this->set( 'm_strOverrideReason', trim( stripcslashes( $arrValues['override_reason'] ) ) ); elseif( isset( $arrValues['override_reason'] ) ) $this->setOverrideReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['override_reason'] ) : $arrValues['override_reason'] );
		if( isset( $arrValues['service_level_options'] ) && $boolDirectSet ) $this->set( 'm_intServiceLevelOptions', trim( $arrValues['service_level_options'] ) ); elseif( isset( $arrValues['service_level_options'] ) ) $this->setServiceLevelOptions( $arrValues['service_level_options'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setEligibleAccountServiceLevelTypeId( $intEligibleAccountServiceLevelTypeId ) {
		$this->set( 'm_intEligibleAccountServiceLevelTypeId', CStrings::strToIntDef( $intEligibleAccountServiceLevelTypeId, NULL, false ) );
	}

	public function getEligibleAccountServiceLevelTypeId() {
		return $this->m_intEligibleAccountServiceLevelTypeId;
	}

	public function sqlEligibleAccountServiceLevelTypeId() {
		return ( true == isset( $this->m_intEligibleAccountServiceLevelTypeId ) ) ? ( string ) $this->m_intEligibleAccountServiceLevelTypeId : 'NULL';
	}

	public function setAccountServiceLevelTypeId( $intAccountServiceLevelTypeId ) {
		$this->set( 'm_intAccountServiceLevelTypeId', CStrings::strToIntDef( $intAccountServiceLevelTypeId, NULL, false ) );
	}

	public function getAccountServiceLevelTypeId() {
		return $this->m_intAccountServiceLevelTypeId;
	}

	public function sqlAccountServiceLevelTypeId() {
		return ( true == isset( $this->m_intAccountServiceLevelTypeId ) ) ? ( string ) $this->m_intAccountServiceLevelTypeId : 'NULL';
	}

	public function setSetBy( $intSetBy ) {
		$this->set( 'm_intSetBy', CStrings::strToIntDef( $intSetBy, NULL, false ) );
	}

	public function getSetBy() {
		return $this->m_intSetBy;
	}

	public function sqlSetBy() {
		return ( true == isset( $this->m_intSetBy ) ) ? ( string ) $this->m_intSetBy : 'NULL';
	}

	public function setSetOn( $strSetOn ) {
		$this->set( 'm_strSetOn', CStrings::strTrimDef( $strSetOn, -1, NULL, true ) );
	}

	public function getSetOn() {
		return $this->m_strSetOn;
	}

	public function sqlSetOn() {
		return ( true == isset( $this->m_strSetOn ) ) ? '\'' . $this->m_strSetOn . '\'' : 'NULL';
	}

	public function setOverrideBy( $intOverrideBy ) {
		$this->set( 'm_intOverrideBy', CStrings::strToIntDef( $intOverrideBy, NULL, false ) );
	}

	public function getOverrideBy() {
		return $this->m_intOverrideBy;
	}

	public function sqlOverrideBy() {
		return ( true == isset( $this->m_intOverrideBy ) ) ? ( string ) $this->m_intOverrideBy : 'NULL';
	}

	public function setOverrideOn( $strOverrideOn ) {
		$this->set( 'm_strOverrideOn', CStrings::strTrimDef( $strOverrideOn, -1, NULL, true ) );
	}

	public function getOverrideOn() {
		return $this->m_strOverrideOn;
	}

	public function sqlOverrideOn() {
		return ( true == isset( $this->m_strOverrideOn ) ) ? '\'' . $this->m_strOverrideOn . '\'' : 'NULL';
	}

	public function setOverrideReason( $strOverrideReason ) {
		$this->set( 'm_strOverrideReason', CStrings::strTrimDef( $strOverrideReason, -1, NULL, true ) );
	}

	public function getOverrideReason() {
		return $this->m_strOverrideReason;
	}

	public function sqlOverrideReason() {
		return ( true == isset( $this->m_strOverrideReason ) ) ? '\'' . addslashes( $this->m_strOverrideReason ) . '\'' : 'NULL';
	}

	public function setServiceLevelOptions( $intServiceLevelOptions ) {
		$this->set( 'm_intServiceLevelOptions', CStrings::strToIntDef( $intServiceLevelOptions, NULL, false ) );
	}

	public function getServiceLevelOptions() {
		return $this->m_intServiceLevelOptions;
	}

	public function sqlServiceLevelOptions() {
		return ( true == isset( $this->m_intServiceLevelOptions ) ) ? ( string ) $this->m_intServiceLevelOptions : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, eligible_account_service_level_type_id, account_service_level_type_id, set_by, set_on, override_by, override_on, override_reason, service_level_options, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlAccountId() . ', ' .
 						$this->sqlEligibleAccountServiceLevelTypeId() . ', ' .
 						$this->sqlAccountServiceLevelTypeId() . ', ' .
 						$this->sqlSetBy() . ', ' .
 						$this->sqlSetOn() . ', ' .
 						$this->sqlOverrideBy() . ', ' .
 						$this->sqlOverrideOn() . ', ' .
 						$this->sqlOverrideReason() . ', ' .
 						$this->sqlServiceLevelOptions() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eligible_account_service_level_type_id = ' . $this->sqlEligibleAccountServiceLevelTypeId() . ','; } elseif( true == array_key_exists( 'EligibleAccountServiceLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' eligible_account_service_level_type_id = ' . $this->sqlEligibleAccountServiceLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_service_level_type_id = ' . $this->sqlAccountServiceLevelTypeId() . ','; } elseif( true == array_key_exists( 'AccountServiceLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' account_service_level_type_id = ' . $this->sqlAccountServiceLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' set_by = ' . $this->sqlSetBy() . ','; } elseif( true == array_key_exists( 'SetBy', $this->getChangedColumns() ) ) { $strSql .= ' set_by = ' . $this->sqlSetBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' set_on = ' . $this->sqlSetOn() . ','; } elseif( true == array_key_exists( 'SetOn', $this->getChangedColumns() ) ) { $strSql .= ' set_on = ' . $this->sqlSetOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' override_by = ' . $this->sqlOverrideBy() . ','; } elseif( true == array_key_exists( 'OverrideBy', $this->getChangedColumns() ) ) { $strSql .= ' override_by = ' . $this->sqlOverrideBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' override_on = ' . $this->sqlOverrideOn() . ','; } elseif( true == array_key_exists( 'OverrideOn', $this->getChangedColumns() ) ) { $strSql .= ' override_on = ' . $this->sqlOverrideOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' override_reason = ' . $this->sqlOverrideReason() . ','; } elseif( true == array_key_exists( 'OverrideReason', $this->getChangedColumns() ) ) { $strSql .= ' override_reason = ' . $this->sqlOverrideReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_level_options = ' . $this->sqlServiceLevelOptions() . ','; } elseif( true == array_key_exists( 'ServiceLevelOptions', $this->getChangedColumns() ) ) { $strSql .= ' service_level_options = ' . $this->sqlServiceLevelOptions() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'eligible_account_service_level_type_id' => $this->getEligibleAccountServiceLevelTypeId(),
			'account_service_level_type_id' => $this->getAccountServiceLevelTypeId(),
			'set_by' => $this->getSetBy(),
			'set_on' => $this->getSetOn(),
			'override_by' => $this->getOverrideBy(),
			'override_on' => $this->getOverrideOn(),
			'override_reason' => $this->getOverrideReason(),
			'service_level_options' => $this->getServiceLevelOptions(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>