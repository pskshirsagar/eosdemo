<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplates
 * Do not add any new functions to this class.
 */

class CBaseSurveyTemplates extends CEosPluralBase {

	/**
	 * @return CSurveyTemplate[]
	 */
	public static function fetchSurveyTemplates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyTemplate', $objDatabase );
	}

	/**
	 * @return CSurveyTemplate
	 */
	public static function fetchSurveyTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyTemplate', $objDatabase );
	}

	public static function fetchSurveyTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_templates', $objDatabase );
	}

	public static function fetchSurveyTemplateById( $intId, $objDatabase ) {
		return self::fetchSurveyTemplate( sprintf( 'SELECT * FROM survey_templates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveyTemplatesBySurveyTypeId( $intSurveyTypeId, $objDatabase ) {
		return self::fetchSurveyTemplates( sprintf( 'SELECT * FROM survey_templates WHERE survey_type_id = %d', ( int ) $intSurveyTypeId ), $objDatabase );
	}

}
?>