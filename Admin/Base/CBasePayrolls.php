<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPayrolls
 * Do not add any new functions to this class.
 */

class CBasePayrolls extends CEosPluralBase {

	/**
	 * @return CPayroll[]
	 */
	public static function fetchPayrolls( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPayroll', $objDatabase );
	}

	/**
	 * @return CPayroll
	 */
	public static function fetchPayroll( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPayroll', $objDatabase );
	}

	public static function fetchPayrollCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'payrolls', $objDatabase );
	}

	public static function fetchPayrollById( $intId, $objDatabase ) {
		return self::fetchPayroll( sprintf( 'SELECT * FROM payrolls WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPayrollsByPayrollPeriodId( $intPayrollPeriodId, $objDatabase ) {
		return self::fetchPayrolls( sprintf( 'SELECT * FROM payrolls WHERE payroll_period_id = %d', ( int ) $intPayrollPeriodId ), $objDatabase );
	}

}
?>