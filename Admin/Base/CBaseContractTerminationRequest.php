<?php

class CBaseContractTerminationRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.contract_termination_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intContractTerminationReasonId;
	protected $m_intContractTerminationRequestStatusId;
	protected $m_intCompanyUserId;
	protected $m_strRequestDatetime;
	protected $m_strIpAddress;
	protected $m_strDeactivationDate;
	protected $m_strTerminationDate;
	protected $m_strTerminationReason;
	protected $m_strTerminationRequestDetails;
	protected $m_strTerminationNote;
	protected $m_intSalesApprovedBy;
	protected $m_strSalesApprovedOn;
	protected $m_intPostedBy;
	protected $m_strPostedOn;
	protected $m_strTerminationProcessedOn;
	protected $m_intIsDismissed;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strReasonForDeny;

	public function __construct() {
		parent::__construct();

		$this->m_intContractTerminationRequestStatusId = '9';
		$this->m_strIpAddress = 'NULL';
		$this->m_intIsDismissed = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['contract_termination_reason_id'] ) && $boolDirectSet ) $this->set( 'm_intContractTerminationReasonId', trim( $arrValues['contract_termination_reason_id'] ) ); elseif( isset( $arrValues['contract_termination_reason_id'] ) ) $this->setContractTerminationReasonId( $arrValues['contract_termination_reason_id'] );
		if( isset( $arrValues['contract_termination_request_status_id'] ) && $boolDirectSet ) $this->set( 'm_intContractTerminationRequestStatusId', trim( $arrValues['contract_termination_request_status_id'] ) ); elseif( isset( $arrValues['contract_termination_request_status_id'] ) ) $this->setContractTerminationRequestStatusId( $arrValues['contract_termination_request_status_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['request_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequestDatetime', trim( $arrValues['request_datetime'] ) ); elseif( isset( $arrValues['request_datetime'] ) ) $this->setRequestDatetime( $arrValues['request_datetime'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['deactivation_date'] ) && $boolDirectSet ) $this->set( 'm_strDeactivationDate', trim( $arrValues['deactivation_date'] ) ); elseif( isset( $arrValues['deactivation_date'] ) ) $this->setDeactivationDate( $arrValues['deactivation_date'] );
		if( isset( $arrValues['termination_date'] ) && $boolDirectSet ) $this->set( 'm_strTerminationDate', trim( $arrValues['termination_date'] ) ); elseif( isset( $arrValues['termination_date'] ) ) $this->setTerminationDate( $arrValues['termination_date'] );
		if( isset( $arrValues['termination_reason'] ) && $boolDirectSet ) $this->set( 'm_strTerminationReason', trim( $arrValues['termination_reason'] ) ); elseif( isset( $arrValues['termination_reason'] ) ) $this->setTerminationReason( $arrValues['termination_reason'] );
		if( isset( $arrValues['termination_request_details'] ) && $boolDirectSet ) $this->set( 'm_strTerminationRequestDetails', trim( $arrValues['termination_request_details'] ) ); elseif( isset( $arrValues['termination_request_details'] ) ) $this->setTerminationRequestDetails( $arrValues['termination_request_details'] );
		if( isset( $arrValues['termination_note'] ) && $boolDirectSet ) $this->set( 'm_strTerminationNote', trim( $arrValues['termination_note'] ) ); elseif( isset( $arrValues['termination_note'] ) ) $this->setTerminationNote( $arrValues['termination_note'] );
		if( isset( $arrValues['sales_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intSalesApprovedBy', trim( $arrValues['sales_approved_by'] ) ); elseif( isset( $arrValues['sales_approved_by'] ) ) $this->setSalesApprovedBy( $arrValues['sales_approved_by'] );
		if( isset( $arrValues['sales_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strSalesApprovedOn', trim( $arrValues['sales_approved_on'] ) ); elseif( isset( $arrValues['sales_approved_on'] ) ) $this->setSalesApprovedOn( $arrValues['sales_approved_on'] );
		if( isset( $arrValues['posted_by'] ) && $boolDirectSet ) $this->set( 'm_intPostedBy', trim( $arrValues['posted_by'] ) ); elseif( isset( $arrValues['posted_by'] ) ) $this->setPostedBy( $arrValues['posted_by'] );
		if( isset( $arrValues['posted_on'] ) && $boolDirectSet ) $this->set( 'm_strPostedOn', trim( $arrValues['posted_on'] ) ); elseif( isset( $arrValues['posted_on'] ) ) $this->setPostedOn( $arrValues['posted_on'] );
		if( isset( $arrValues['termination_processed_on'] ) && $boolDirectSet ) $this->set( 'm_strTerminationProcessedOn', trim( $arrValues['termination_processed_on'] ) ); elseif( isset( $arrValues['termination_processed_on'] ) ) $this->setTerminationProcessedOn( $arrValues['termination_processed_on'] );
		if( isset( $arrValues['is_dismissed'] ) && $boolDirectSet ) $this->set( 'm_intIsDismissed', trim( $arrValues['is_dismissed'] ) ); elseif( isset( $arrValues['is_dismissed'] ) ) $this->setIsDismissed( $arrValues['is_dismissed'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['reason_for_deny'] ) && $boolDirectSet ) $this->set( 'm_strReasonForDeny', trim( $arrValues['reason_for_deny'] ) ); elseif( isset( $arrValues['reason_for_deny'] ) ) $this->setReasonForDeny( $arrValues['reason_for_deny'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setContractTerminationReasonId( $intContractTerminationReasonId ) {
		$this->set( 'm_intContractTerminationReasonId', CStrings::strToIntDef( $intContractTerminationReasonId, NULL, false ) );
	}

	public function getContractTerminationReasonId() {
		return $this->m_intContractTerminationReasonId;
	}

	public function sqlContractTerminationReasonId() {
		return ( true == isset( $this->m_intContractTerminationReasonId ) ) ? ( string ) $this->m_intContractTerminationReasonId : 'NULL';
	}

	public function setContractTerminationRequestStatusId( $intContractTerminationRequestStatusId ) {
		$this->set( 'm_intContractTerminationRequestStatusId', CStrings::strToIntDef( $intContractTerminationRequestStatusId, NULL, false ) );
	}

	public function getContractTerminationRequestStatusId() {
		return $this->m_intContractTerminationRequestStatusId;
	}

	public function sqlContractTerminationRequestStatusId() {
		return ( true == isset( $this->m_intContractTerminationRequestStatusId ) ) ? ( string ) $this->m_intContractTerminationRequestStatusId : '9';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setRequestDatetime( $strRequestDatetime ) {
		$this->set( 'm_strRequestDatetime', CStrings::strTrimDef( $strRequestDatetime, -1, NULL, true ) );
	}

	public function getRequestDatetime() {
		return $this->m_strRequestDatetime;
	}

	public function sqlRequestDatetime() {
		return ( true == isset( $this->m_strRequestDatetime ) ) ? '\'' . $this->m_strRequestDatetime . '\'' : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 125, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : '\'NULL\'';
	}

	public function setDeactivationDate( $strDeactivationDate ) {
		$this->set( 'm_strDeactivationDate', CStrings::strTrimDef( $strDeactivationDate, -1, NULL, true ) );
	}

	public function getDeactivationDate() {
		return $this->m_strDeactivationDate;
	}

	public function sqlDeactivationDate() {
		return ( true == isset( $this->m_strDeactivationDate ) ) ? '\'' . $this->m_strDeactivationDate . '\'' : 'NULL';
	}

	public function setTerminationDate( $strTerminationDate ) {
		$this->set( 'm_strTerminationDate', CStrings::strTrimDef( $strTerminationDate, -1, NULL, true ) );
	}

	public function getTerminationDate() {
		return $this->m_strTerminationDate;
	}

	public function sqlTerminationDate() {
		return ( true == isset( $this->m_strTerminationDate ) ) ? '\'' . $this->m_strTerminationDate . '\'' : 'NULL';
	}

	public function setTerminationReason( $strTerminationReason ) {
		$this->set( 'm_strTerminationReason', CStrings::strTrimDef( $strTerminationReason, -1, NULL, true ) );
	}

	public function getTerminationReason() {
		return $this->m_strTerminationReason;
	}

	public function sqlTerminationReason() {
		return ( true == isset( $this->m_strTerminationReason ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTerminationReason ) : '\'' . addslashes( $this->m_strTerminationReason ) . '\'' ) : 'NULL';
	}

	public function setTerminationRequestDetails( $strTerminationRequestDetails ) {
		$this->set( 'm_strTerminationRequestDetails', CStrings::strTrimDef( $strTerminationRequestDetails, -1, NULL, true ) );
	}

	public function getTerminationRequestDetails() {
		return $this->m_strTerminationRequestDetails;
	}

	public function sqlTerminationRequestDetails() {
		return ( true == isset( $this->m_strTerminationRequestDetails ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTerminationRequestDetails ) : '\'' . addslashes( $this->m_strTerminationRequestDetails ) . '\'' ) : 'NULL';
	}

	public function setTerminationNote( $strTerminationNote ) {
		$this->set( 'm_strTerminationNote', CStrings::strTrimDef( $strTerminationNote, -1, NULL, true ) );
	}

	public function getTerminationNote() {
		return $this->m_strTerminationNote;
	}

	public function sqlTerminationNote() {
		return ( true == isset( $this->m_strTerminationNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTerminationNote ) : '\'' . addslashes( $this->m_strTerminationNote ) . '\'' ) : 'NULL';
	}

	public function setSalesApprovedBy( $intSalesApprovedBy ) {
		$this->set( 'm_intSalesApprovedBy', CStrings::strToIntDef( $intSalesApprovedBy, NULL, false ) );
	}

	public function getSalesApprovedBy() {
		return $this->m_intSalesApprovedBy;
	}

	public function sqlSalesApprovedBy() {
		return ( true == isset( $this->m_intSalesApprovedBy ) ) ? ( string ) $this->m_intSalesApprovedBy : 'NULL';
	}

	public function setSalesApprovedOn( $strSalesApprovedOn ) {
		$this->set( 'm_strSalesApprovedOn', CStrings::strTrimDef( $strSalesApprovedOn, -1, NULL, true ) );
	}

	public function getSalesApprovedOn() {
		return $this->m_strSalesApprovedOn;
	}

	public function sqlSalesApprovedOn() {
		return ( true == isset( $this->m_strSalesApprovedOn ) ) ? '\'' . $this->m_strSalesApprovedOn . '\'' : 'NULL';
	}

	public function setPostedBy( $intPostedBy ) {
		$this->set( 'm_intPostedBy', CStrings::strToIntDef( $intPostedBy, NULL, false ) );
	}

	public function getPostedBy() {
		return $this->m_intPostedBy;
	}

	public function sqlPostedBy() {
		return ( true == isset( $this->m_intPostedBy ) ) ? ( string ) $this->m_intPostedBy : 'NULL';
	}

	public function setPostedOn( $strPostedOn ) {
		$this->set( 'm_strPostedOn', CStrings::strTrimDef( $strPostedOn, -1, NULL, true ) );
	}

	public function getPostedOn() {
		return $this->m_strPostedOn;
	}

	public function sqlPostedOn() {
		return ( true == isset( $this->m_strPostedOn ) ) ? '\'' . $this->m_strPostedOn . '\'' : 'NULL';
	}

	public function setTerminationProcessedOn( $strTerminationProcessedOn ) {
		$this->set( 'm_strTerminationProcessedOn', CStrings::strTrimDef( $strTerminationProcessedOn, -1, NULL, true ) );
	}

	public function getTerminationProcessedOn() {
		return $this->m_strTerminationProcessedOn;
	}

	public function sqlTerminationProcessedOn() {
		return ( true == isset( $this->m_strTerminationProcessedOn ) ) ? '\'' . $this->m_strTerminationProcessedOn . '\'' : 'NULL';
	}

	public function setIsDismissed( $intIsDismissed ) {
		$this->set( 'm_intIsDismissed', CStrings::strToIntDef( $intIsDismissed, NULL, false ) );
	}

	public function getIsDismissed() {
		return $this->m_intIsDismissed;
	}

	public function sqlIsDismissed() {
		return ( true == isset( $this->m_intIsDismissed ) ) ? ( string ) $this->m_intIsDismissed : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setReasonForDeny( $strReasonForDeny ) {
		$this->set( 'm_strReasonForDeny', CStrings::strTrimDef( $strReasonForDeny, -1, NULL, true ) );
	}

	public function getReasonForDeny() {
		return $this->m_strReasonForDeny;
	}

	public function sqlReasonForDeny() {
		return ( true == isset( $this->m_strReasonForDeny ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReasonForDeny ) : '\'' . addslashes( $this->m_strReasonForDeny ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, contract_termination_reason_id, contract_termination_request_status_id, company_user_id, request_datetime, ip_address, deactivation_date, termination_date, termination_reason, termination_request_details, termination_note, sales_approved_by, sales_approved_on, posted_by, posted_on, termination_processed_on, is_dismissed, updated_by, updated_on, created_by, created_on, reason_for_deny )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlContractTerminationReasonId() . ', ' .
		          $this->sqlContractTerminationRequestStatusId() . ', ' .
		          $this->sqlCompanyUserId() . ', ' .
		          $this->sqlRequestDatetime() . ', ' .
		          $this->sqlIpAddress() . ', ' .
		          $this->sqlDeactivationDate() . ', ' .
		          $this->sqlTerminationDate() . ', ' .
		          $this->sqlTerminationReason() . ', ' .
		          $this->sqlTerminationRequestDetails() . ', ' .
		          $this->sqlTerminationNote() . ', ' .
		          $this->sqlSalesApprovedBy() . ', ' .
		          $this->sqlSalesApprovedOn() . ', ' .
		          $this->sqlPostedBy() . ', ' .
		          $this->sqlPostedOn() . ', ' .
		          $this->sqlTerminationProcessedOn() . ', ' .
		          $this->sqlIsDismissed() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlReasonForDeny() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_termination_reason_id = ' . $this->sqlContractTerminationReasonId(). ',' ; } elseif( true == array_key_exists( 'ContractTerminationReasonId', $this->getChangedColumns() ) ) { $strSql .= ' contract_termination_reason_id = ' . $this->sqlContractTerminationReasonId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_termination_request_status_id = ' . $this->sqlContractTerminationRequestStatusId(). ',' ; } elseif( true == array_key_exists( 'ContractTerminationRequestStatusId', $this->getChangedColumns() ) ) { $strSql .= ' contract_termination_request_status_id = ' . $this->sqlContractTerminationRequestStatusId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime(). ',' ; } elseif( true == array_key_exists( 'RequestDatetime', $this->getChangedColumns() ) ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deactivation_date = ' . $this->sqlDeactivationDate(). ',' ; } elseif( true == array_key_exists( 'DeactivationDate', $this->getChangedColumns() ) ) { $strSql .= ' deactivation_date = ' . $this->sqlDeactivationDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate(). ',' ; } elseif( true == array_key_exists( 'TerminationDate', $this->getChangedColumns() ) ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_reason = ' . $this->sqlTerminationReason(). ',' ; } elseif( true == array_key_exists( 'TerminationReason', $this->getChangedColumns() ) ) { $strSql .= ' termination_reason = ' . $this->sqlTerminationReason() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_request_details = ' . $this->sqlTerminationRequestDetails(). ',' ; } elseif( true == array_key_exists( 'TerminationRequestDetails', $this->getChangedColumns() ) ) { $strSql .= ' termination_request_details = ' . $this->sqlTerminationRequestDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_note = ' . $this->sqlTerminationNote(). ',' ; } elseif( true == array_key_exists( 'TerminationNote', $this->getChangedColumns() ) ) { $strSql .= ' termination_note = ' . $this->sqlTerminationNote() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_approved_by = ' . $this->sqlSalesApprovedBy(). ',' ; } elseif( true == array_key_exists( 'SalesApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' sales_approved_by = ' . $this->sqlSalesApprovedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_approved_on = ' . $this->sqlSalesApprovedOn(). ',' ; } elseif( true == array_key_exists( 'SalesApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' sales_approved_on = ' . $this->sqlSalesApprovedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_by = ' . $this->sqlPostedBy(). ',' ; } elseif( true == array_key_exists( 'PostedBy', $this->getChangedColumns() ) ) { $strSql .= ' posted_by = ' . $this->sqlPostedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn(). ',' ; } elseif( true == array_key_exists( 'PostedOn', $this->getChangedColumns() ) ) { $strSql .= ' posted_on = ' . $this->sqlPostedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_processed_on = ' . $this->sqlTerminationProcessedOn(). ',' ; } elseif( true == array_key_exists( 'TerminationProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' termination_processed_on = ' . $this->sqlTerminationProcessedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_dismissed = ' . $this->sqlIsDismissed(). ',' ; } elseif( true == array_key_exists( 'IsDismissed', $this->getChangedColumns() ) ) { $strSql .= ' is_dismissed = ' . $this->sqlIsDismissed() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_for_deny = ' . $this->sqlReasonForDeny(). ',' ; } elseif( true == array_key_exists( 'ReasonForDeny', $this->getChangedColumns() ) ) { $strSql .= ' reason_for_deny = ' . $this->sqlReasonForDeny() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'contract_termination_reason_id' => $this->getContractTerminationReasonId(),
			'contract_termination_request_status_id' => $this->getContractTerminationRequestStatusId(),
			'company_user_id' => $this->getCompanyUserId(),
			'request_datetime' => $this->getRequestDatetime(),
			'ip_address' => $this->getIpAddress(),
			'deactivation_date' => $this->getDeactivationDate(),
			'termination_date' => $this->getTerminationDate(),
			'termination_reason' => $this->getTerminationReason(),
			'termination_request_details' => $this->getTerminationRequestDetails(),
			'termination_note' => $this->getTerminationNote(),
			'sales_approved_by' => $this->getSalesApprovedBy(),
			'sales_approved_on' => $this->getSalesApprovedOn(),
			'posted_by' => $this->getPostedBy(),
			'posted_on' => $this->getPostedOn(),
			'termination_processed_on' => $this->getTerminationProcessedOn(),
			'is_dismissed' => $this->getIsDismissed(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'reason_for_deny' => $this->getReasonForDeny()
		);
	}

}
?>