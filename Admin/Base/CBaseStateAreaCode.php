<?php

class CBaseStateAreaCode extends CEosSingularBase {

	const TABLE_NAME = 'public.state_area_codes';

	protected $m_intId;
	protected $m_strStateCode;
	protected $m_strAreaCode;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['area_code'] ) && $boolDirectSet ) $this->set( 'm_strAreaCode', trim( stripcslashes( $arrValues['area_code'] ) ) ); elseif( isset( $arrValues['area_code'] ) ) $this->setAreaCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['area_code'] ) : $arrValues['area_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setAreaCode( $strAreaCode ) {
		$this->set( 'm_strAreaCode', CStrings::strTrimDef( $strAreaCode, 10, NULL, true ) );
	}

	public function getAreaCode() {
		return $this->m_strAreaCode;
	}

	public function sqlAreaCode() {
		return ( true == isset( $this->m_strAreaCode ) ) ? '\'' . addslashes( $this->m_strAreaCode ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'state_code' => $this->getStateCode(),
			'area_code' => $this->getAreaCode()
		);
	}

}
?>