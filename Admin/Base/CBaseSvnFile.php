<?php

class CBaseSvnFile extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.svn_files';

	protected $m_intId;
	protected $m_intDeploymentTypeId;
	protected $m_intDatabaseTypeId;
	protected $m_intTaskId;
	protected $m_strFileName;
	protected $m_intRevision;
	protected $m_strCommittedOn;
	protected $m_intSqlReviewedBy;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intDeploymentTypeId = '3';
		$this->m_intIsPublished = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['deployment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDeploymentTypeId', trim( $arrValues['deployment_type_id'] ) ); elseif( isset( $arrValues['deployment_type_id'] ) ) $this->setDeploymentTypeId( $arrValues['deployment_type_id'] );
		if( isset( $arrValues['database_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseTypeId', trim( $arrValues['database_type_id'] ) ); elseif( isset( $arrValues['database_type_id'] ) ) $this->setDatabaseTypeId( $arrValues['database_type_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( $arrValues['file_name'] ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( $arrValues['file_name'] );
		if( isset( $arrValues['revision'] ) && $boolDirectSet ) $this->set( 'm_intRevision', trim( $arrValues['revision'] ) ); elseif( isset( $arrValues['revision'] ) ) $this->setRevision( $arrValues['revision'] );
		if( isset( $arrValues['committed_on'] ) && $boolDirectSet ) $this->set( 'm_strCommittedOn', trim( $arrValues['committed_on'] ) ); elseif( isset( $arrValues['committed_on'] ) ) $this->setCommittedOn( $arrValues['committed_on'] );
		if( isset( $arrValues['sql_reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intSqlReviewedBy', trim( $arrValues['sql_reviewed_by'] ) ); elseif( isset( $arrValues['sql_reviewed_by'] ) ) $this->setSqlReviewedBy( $arrValues['sql_reviewed_by'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDeploymentTypeId( $intDeploymentTypeId ) {
		$this->set( 'm_intDeploymentTypeId', CStrings::strToIntDef( $intDeploymentTypeId, NULL, false ) );
	}

	public function getDeploymentTypeId() {
		return $this->m_intDeploymentTypeId;
	}

	public function sqlDeploymentTypeId() {
		return ( true == isset( $this->m_intDeploymentTypeId ) ) ? ( string ) $this->m_intDeploymentTypeId : '3';
	}

	public function setDatabaseTypeId( $intDatabaseTypeId ) {
		$this->set( 'm_intDatabaseTypeId', CStrings::strToIntDef( $intDatabaseTypeId, NULL, false ) );
	}

	public function getDatabaseTypeId() {
		return $this->m_intDatabaseTypeId;
	}

	public function sqlDatabaseTypeId() {
		return ( true == isset( $this->m_intDatabaseTypeId ) ) ? ( string ) $this->m_intDatabaseTypeId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 255, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileName ) : '\'' . addslashes( $this->m_strFileName ) . '\'' ) : 'NULL';
	}

	public function setRevision( $intRevision ) {
		$this->set( 'm_intRevision', CStrings::strToIntDef( $intRevision, NULL, false ) );
	}

	public function getRevision() {
		return $this->m_intRevision;
	}

	public function sqlRevision() {
		return ( true == isset( $this->m_intRevision ) ) ? ( string ) $this->m_intRevision : 'NULL';
	}

	public function setCommittedOn( $strCommittedOn ) {
		$this->set( 'm_strCommittedOn', CStrings::strTrimDef( $strCommittedOn, -1, NULL, true ) );
	}

	public function getCommittedOn() {
		return $this->m_strCommittedOn;
	}

	public function sqlCommittedOn() {
		return ( true == isset( $this->m_strCommittedOn ) ) ? '\'' . $this->m_strCommittedOn . '\'' : 'NULL';
	}

	public function setSqlReviewedBy( $intSqlReviewedBy ) {
		$this->set( 'm_intSqlReviewedBy', CStrings::strToIntDef( $intSqlReviewedBy, NULL, false ) );
	}

	public function getSqlReviewedBy() {
		return $this->m_intSqlReviewedBy;
	}

	public function sqlSqlReviewedBy() {
		return ( true == isset( $this->m_intSqlReviewedBy ) ) ? ( string ) $this->m_intSqlReviewedBy : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, deployment_type_id, database_type_id, task_id, file_name, revision, committed_on, sql_reviewed_by, is_published, order_num, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlDeploymentTypeId() . ', ' .
		          $this->sqlDatabaseTypeId() . ', ' .
		          $this->sqlTaskId() . ', ' .
		          $this->sqlFileName() . ', ' .
		          $this->sqlRevision() . ', ' .
		          $this->sqlCommittedOn() . ', ' .
		          $this->sqlSqlReviewedBy() . ', ' .
		          $this->sqlIsPublished() . ', ' .
		          $this->sqlOrderNum() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployment_type_id = ' . $this->sqlDeploymentTypeId(). ',' ; } elseif( true == array_key_exists( 'DeploymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' deployment_type_id = ' . $this->sqlDeploymentTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_type_id = ' . $this->sqlDatabaseTypeId(). ',' ; } elseif( true == array_key_exists( 'DatabaseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' database_type_id = ' . $this->sqlDatabaseTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId(). ',' ; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revision = ' . $this->sqlRevision(). ',' ; } elseif( true == array_key_exists( 'Revision', $this->getChangedColumns() ) ) { $strSql .= ' revision = ' . $this->sqlRevision() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_on = ' . $this->sqlCommittedOn(). ',' ; } elseif( true == array_key_exists( 'CommittedOn', $this->getChangedColumns() ) ) { $strSql .= ' committed_on = ' . $this->sqlCommittedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sql_reviewed_by = ' . $this->sqlSqlReviewedBy(). ',' ; } elseif( true == array_key_exists( 'SqlReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' sql_reviewed_by = ' . $this->sqlSqlReviewedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'deployment_type_id' => $this->getDeploymentTypeId(),
			'database_type_id' => $this->getDatabaseTypeId(),
			'task_id' => $this->getTaskId(),
			'file_name' => $this->getFileName(),
			'revision' => $this->getRevision(),
			'committed_on' => $this->getCommittedOn(),
			'sql_reviewed_by' => $this->getSqlReviewedBy(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>