<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestQuestionTemplates
 * Do not add any new functions to this class.
 */

class CBaseTestQuestionTemplates extends CEosPluralBase {

	/**
	 * @return CTestQuestionTemplate[]
	 */
	public static function fetchTestQuestionTemplates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestQuestionTemplate', $objDatabase );
	}

	/**
	 * @return CTestQuestionTemplate
	 */
	public static function fetchTestQuestionTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestQuestionTemplate', $objDatabase );
	}

	public static function fetchTestQuestionTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_question_templates', $objDatabase );
	}

	public static function fetchTestQuestionTemplateById( $intId, $objDatabase ) {
		return self::fetchTestQuestionTemplate( sprintf( 'SELECT * FROM test_question_templates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestQuestionTemplatesByTestTemplateId( $intTestTemplateId, $objDatabase ) {
		return self::fetchTestQuestionTemplates( sprintf( 'SELECT * FROM test_question_templates WHERE test_template_id = %d', ( int ) $intTestTemplateId ), $objDatabase );
	}

	public static function fetchTestQuestionTemplatesByTestQuestionTypeId( $intTestQuestionTypeId, $objDatabase ) {
		return self::fetchTestQuestionTemplates( sprintf( 'SELECT * FROM test_question_templates WHERE test_question_type_id = %d', ( int ) $intTestQuestionTypeId ), $objDatabase );
	}

	public static function fetchTestQuestionTemplatesByTestQuestionGroupId( $intTestQuestionGroupId, $objDatabase ) {
		return self::fetchTestQuestionTemplates( sprintf( 'SELECT * FROM test_question_templates WHERE test_question_group_id = %d', ( int ) $intTestQuestionGroupId ), $objDatabase );
	}

}
?>