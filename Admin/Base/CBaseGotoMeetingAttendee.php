<?php

class CBaseGotoMeetingAttendee extends CEosSingularBase {

	const TABLE_NAME = 'public.goto_meeting_attendees';

	protected $m_intId;
	protected $m_intGotoMeetingId;
	protected $m_intEmployeeId;
	protected $m_strEmployeeName;
	protected $m_intMinutesActive;
	protected $m_strJointime;
	protected $m_strLeavetime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['goto_meeting_id'] ) && $boolDirectSet ) $this->set( 'm_intGotoMeetingId', trim( $arrValues['goto_meeting_id'] ) ); elseif( isset( $arrValues['goto_meeting_id'] ) ) $this->setGotoMeetingId( $arrValues['goto_meeting_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['employee_name'] ) && $boolDirectSet ) $this->set( 'm_strEmployeeName', trim( stripcslashes( $arrValues['employee_name'] ) ) ); elseif( isset( $arrValues['employee_name'] ) ) $this->setEmployeeName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['employee_name'] ) : $arrValues['employee_name'] );
		if( isset( $arrValues['minutes_active'] ) && $boolDirectSet ) $this->set( 'm_intMinutesActive', trim( $arrValues['minutes_active'] ) ); elseif( isset( $arrValues['minutes_active'] ) ) $this->setMinutesActive( $arrValues['minutes_active'] );
		if( isset( $arrValues['jointime'] ) && $boolDirectSet ) $this->set( 'm_strJointime', trim( $arrValues['jointime'] ) ); elseif( isset( $arrValues['jointime'] ) ) $this->setJointime( $arrValues['jointime'] );
		if( isset( $arrValues['leavetime'] ) && $boolDirectSet ) $this->set( 'm_strLeavetime', trim( $arrValues['leavetime'] ) ); elseif( isset( $arrValues['leavetime'] ) ) $this->setLeavetime( $arrValues['leavetime'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setGotoMeetingId( $intGotoMeetingId ) {
		$this->set( 'm_intGotoMeetingId', CStrings::strToIntDef( $intGotoMeetingId, NULL, false ) );
	}

	public function getGotoMeetingId() {
		return $this->m_intGotoMeetingId;
	}

	public function sqlGotoMeetingId() {
		return ( true == isset( $this->m_intGotoMeetingId ) ) ? ( string ) $this->m_intGotoMeetingId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setEmployeeName( $strEmployeeName ) {
		$this->set( 'm_strEmployeeName', CStrings::strTrimDef( $strEmployeeName, 100, NULL, true ) );
	}

	public function getEmployeeName() {
		return $this->m_strEmployeeName;
	}

	public function sqlEmployeeName() {
		return ( true == isset( $this->m_strEmployeeName ) ) ? '\'' . addslashes( $this->m_strEmployeeName ) . '\'' : 'NULL';
	}

	public function setMinutesActive( $intMinutesActive ) {
		$this->set( 'm_intMinutesActive', CStrings::strToIntDef( $intMinutesActive, NULL, false ) );
	}

	public function getMinutesActive() {
		return $this->m_intMinutesActive;
	}

	public function sqlMinutesActive() {
		return ( true == isset( $this->m_intMinutesActive ) ) ? ( string ) $this->m_intMinutesActive : 'NULL';
	}

	public function setJointime( $strJointime ) {
		$this->set( 'm_strJointime', CStrings::strTrimDef( $strJointime, -1, NULL, true ) );
	}

	public function getJointime() {
		return $this->m_strJointime;
	}

	public function sqlJointime() {
		return ( true == isset( $this->m_strJointime ) ) ? '\'' . $this->m_strJointime . '\'' : 'NOW()';
	}

	public function setLeavetime( $strLeavetime ) {
		$this->set( 'm_strLeavetime', CStrings::strTrimDef( $strLeavetime, -1, NULL, true ) );
	}

	public function getLeavetime() {
		return $this->m_strLeavetime;
	}

	public function sqlLeavetime() {
		return ( true == isset( $this->m_strLeavetime ) ) ? '\'' . $this->m_strLeavetime . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, goto_meeting_id, employee_id, employee_name, minutes_active, jointime, leavetime, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlGotoMeetingId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlEmployeeName() . ', ' .
 						$this->sqlMinutesActive() . ', ' .
 						$this->sqlJointime() . ', ' .
 						$this->sqlLeavetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' goto_meeting_id = ' . $this->sqlGotoMeetingId() . ','; } elseif( true == array_key_exists( 'GotoMeetingId', $this->getChangedColumns() ) ) { $strSql .= ' goto_meeting_id = ' . $this->sqlGotoMeetingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_name = ' . $this->sqlEmployeeName() . ','; } elseif( true == array_key_exists( 'EmployeeName', $this->getChangedColumns() ) ) { $strSql .= ' employee_name = ' . $this->sqlEmployeeName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minutes_active = ' . $this->sqlMinutesActive() . ','; } elseif( true == array_key_exists( 'MinutesActive', $this->getChangedColumns() ) ) { $strSql .= ' minutes_active = ' . $this->sqlMinutesActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' jointime = ' . $this->sqlJointime() . ','; } elseif( true == array_key_exists( 'Jointime', $this->getChangedColumns() ) ) { $strSql .= ' jointime = ' . $this->sqlJointime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leavetime = ' . $this->sqlLeavetime() . ','; } elseif( true == array_key_exists( 'Leavetime', $this->getChangedColumns() ) ) { $strSql .= ' leavetime = ' . $this->sqlLeavetime() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'goto_meeting_id' => $this->getGotoMeetingId(),
			'employee_id' => $this->getEmployeeId(),
			'employee_name' => $this->getEmployeeName(),
			'minutes_active' => $this->getMinutesActive(),
			'jointime' => $this->getJointime(),
			'leavetime' => $this->getLeavetime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>