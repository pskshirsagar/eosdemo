<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CRoles
 * Do not add any new functions to this class.
 */

class CBaseRoles extends CEosPluralBase {

	/**
	 * @return CRole[]
	 */
	public static function fetchRoles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CRole', $objDatabase );
	}

	/**
	 * @return CRole
	 */
	public static function fetchRole( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRole', $objDatabase );
	}

	public static function fetchRoleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'roles', $objDatabase );
	}

	public static function fetchRoleById( $intId, $objDatabase ) {
		return self::fetchRole( sprintf( 'SELECT * FROM roles WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchRolesByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchRoles( sprintf( 'SELECT * FROM roles WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

}
?>