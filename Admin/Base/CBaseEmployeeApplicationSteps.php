<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationSteps
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationSteps extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationStep[]
	 */
	public static function fetchEmployeeApplicationSteps( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeApplicationStep::class, $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationStep
	 */
	public static function fetchEmployeeApplicationStep( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeApplicationStep::class, $objDatabase );
	}

	public static function fetchEmployeeApplicationStepCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_steps', $objDatabase );
	}

	public static function fetchEmployeeApplicationStepById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationStep( sprintf( 'SELECT * FROM employee_application_steps WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationStepsByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchEmployeeApplicationSteps( sprintf( 'SELECT * FROM employee_application_steps WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationStepsByPsJobPostingStepStatusId( $intPsJobPostingStepStatusId, $objDatabase ) {
		return self::fetchEmployeeApplicationSteps( sprintf( 'SELECT * FROM employee_application_steps WHERE ps_job_posting_step_status_id = %d', ( int ) $intPsJobPostingStepStatusId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationStepsByPurchaseRequestId( $intPurchaseRequestId, $objDatabase ) {
		return self::fetchEmployeeApplicationSteps( sprintf( 'SELECT * FROM employee_application_steps WHERE purchase_request_id = %d', ( int ) $intPurchaseRequestId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationStepsByStepUpdatedByEmployeeId( $intStepUpdatedByEmployeeId, $objDatabase ) {
		return self::fetchEmployeeApplicationSteps( sprintf( 'SELECT * FROM employee_application_steps WHERE step_updated_by_employee_id = %d', ( int ) $intStepUpdatedByEmployeeId ), $objDatabase );
	}

}
?>