<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEntityReferenceTypes
 * Do not add any new functions to this class.
 */

class CBaseEntityReferenceTypes extends CEosPluralBase {

	/**
	 * @return CEntityReferenceType[]
	 */
	public static function fetchEntityReferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEntityReferenceType', $objDatabase );
	}

	/**
	 * @return CEntityReferenceType
	 */
	public static function fetchEntityReferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEntityReferenceType', $objDatabase );
	}

	public static function fetchEntityReferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'entity_reference_types', $objDatabase );
	}

	public static function fetchEntityReferenceTypeById( $intId, $objDatabase ) {
		return self::fetchEntityReferenceType( sprintf( 'SELECT * FROM entity_reference_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>