<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskAttachments
 * Do not add any new functions to this class.
 */

class CBaseTaskAttachments extends CEosPluralBase {

	/**
	 * @return CTaskAttachment[]
	 */
	public static function fetchTaskAttachments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskAttachment', $objDatabase );
	}

	/**
	 * @return CTaskAttachment
	 */
	public static function fetchTaskAttachment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskAttachment', $objDatabase );
	}

	public static function fetchTaskAttachmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_attachments', $objDatabase );
	}

	public static function fetchTaskAttachmentById( $intId, $objDatabase ) {
		return self::fetchTaskAttachment( sprintf( 'SELECT * FROM task_attachments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskAttachmentsByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchTaskAttachments( sprintf( 'SELECT * FROM task_attachments WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

	public static function fetchTaskAttachmentsByTaskNoteId( $intTaskNoteId, $objDatabase ) {
		return self::fetchTaskAttachments( sprintf( 'SELECT * FROM task_attachments WHERE task_note_id = %d', ( int ) $intTaskNoteId ), $objDatabase );
	}

	public static function fetchTaskAttachmentsByReleaseNoteId( $intReleaseNoteId, $objDatabase ) {
		return self::fetchTaskAttachments( sprintf( 'SELECT * FROM task_attachments WHERE release_note_id = %d', ( int ) $intReleaseNoteId ), $objDatabase );
	}

}
?>