<?php

class CBaseSalesTaxSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.sales_tax_settings';

	protected $m_intId;
	protected $m_strCountryCode;
	protected $m_strCurrencyCode;
	protected $m_boolIsNonBilledSalesTax;
	protected $m_boolIsBlockedSalesTax;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsNonBilledSalesTax = false;
		$this->m_boolIsBlockedSalesTax = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( $arrValues['currency_code'] ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( $arrValues['currency_code'] );
		if( isset( $arrValues['is_non_billed_sales_tax'] ) && $boolDirectSet ) $this->set( 'm_boolIsNonBilledSalesTax', trim( stripcslashes( $arrValues['is_non_billed_sales_tax'] ) ) ); elseif( isset( $arrValues['is_non_billed_sales_tax'] ) ) $this->setIsNonBilledSalesTax( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_non_billed_sales_tax'] ) : $arrValues['is_non_billed_sales_tax'] );
		if( isset( $arrValues['is_blocked_sales_tax'] ) && $boolDirectSet ) $this->set( 'm_boolIsBlockedSalesTax', trim( stripcslashes( $arrValues['is_blocked_sales_tax'] ) ) ); elseif( isset( $arrValues['is_blocked_sales_tax'] ) ) $this->setIsBlockedSalesTax( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_blocked_sales_tax'] ) : $arrValues['is_blocked_sales_tax'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountryCode ) : '\'' . addslashes( $this->m_strCountryCode ) . '\'' ) : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrencyCode ) : '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' ) : 'NULL';
	}

	public function setIsNonBilledSalesTax( $boolIsNonBilledSalesTax ) {
		$this->set( 'm_boolIsNonBilledSalesTax', CStrings::strToBool( $boolIsNonBilledSalesTax ) );
	}

	public function getIsNonBilledSalesTax() {
		return $this->m_boolIsNonBilledSalesTax;
	}

	public function sqlIsNonBilledSalesTax() {
		return ( true == isset( $this->m_boolIsNonBilledSalesTax ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNonBilledSalesTax ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsBlockedSalesTax( $boolIsBlockedSalesTax ) {
		$this->set( 'm_boolIsBlockedSalesTax', CStrings::strToBool( $boolIsBlockedSalesTax ) );
	}

	public function getIsBlockedSalesTax() {
		return $this->m_boolIsBlockedSalesTax;
	}

	public function sqlIsBlockedSalesTax() {
		return ( true == isset( $this->m_boolIsBlockedSalesTax ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBlockedSalesTax ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, country_code, currency_code, is_non_billed_sales_tax, is_blocked_sales_tax, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlCurrencyCode() . ', ' .
						$this->sqlIsNonBilledSalesTax() . ', ' .
						$this->sqlIsBlockedSalesTax() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_non_billed_sales_tax = ' . $this->sqlIsNonBilledSalesTax(). ',' ; } elseif( true == array_key_exists( 'IsNonBilledSalesTax', $this->getChangedColumns() ) ) { $strSql .= ' is_non_billed_sales_tax = ' . $this->sqlIsNonBilledSalesTax() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_blocked_sales_tax = ' . $this->sqlIsBlockedSalesTax(). ',' ; } elseif( true == array_key_exists( 'IsBlockedSalesTax', $this->getChangedColumns() ) ) { $strSql .= ' is_blocked_sales_tax = ' . $this->sqlIsBlockedSalesTax() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'country_code' => $this->getCountryCode(),
			'currency_code' => $this->getCurrencyCode(),
			'is_non_billed_sales_tax' => $this->getIsNonBilledSalesTax(),
			'is_blocked_sales_tax' => $this->getIsBlockedSalesTax(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>