<?php

class CBaseDocumentTemplateComponent extends CEosSingularBase {

	const TABLE_NAME = 'public.document_template_components';

	protected $m_intId;
	protected $m_intDocumentTemplateId;
	protected $m_intDocumentTemplateComponentId;
	protected $m_intArchivedTemplateComponentId;
	protected $m_strComponentType;
	protected $m_strTemplateComponentKey;
	protected $m_intSize;
	protected $m_intOrderNum;
	protected $m_strSyncedOn;
	protected $m_intRequireSync;
	protected $m_intAllowsUpdates;
	protected $m_intIsModifiable;
	protected $m_intIsRemovable;
	protected $m_intDefaultIsAutoUpdated;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intRequireSync = '0';
		$this->m_intAllowsUpdates = '0';
		$this->m_intDefaultIsAutoUpdated = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['document_template_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentTemplateId', trim( $arrValues['document_template_id'] ) ); elseif( isset( $arrValues['document_template_id'] ) ) $this->setDocumentTemplateId( $arrValues['document_template_id'] );
		if( isset( $arrValues['document_template_component_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentTemplateComponentId', trim( $arrValues['document_template_component_id'] ) ); elseif( isset( $arrValues['document_template_component_id'] ) ) $this->setDocumentTemplateComponentId( $arrValues['document_template_component_id'] );
		if( isset( $arrValues['archived_template_component_id'] ) && $boolDirectSet ) $this->set( 'm_intArchivedTemplateComponentId', trim( $arrValues['archived_template_component_id'] ) ); elseif( isset( $arrValues['archived_template_component_id'] ) ) $this->setArchivedTemplateComponentId( $arrValues['archived_template_component_id'] );
		if( isset( $arrValues['component_type'] ) && $boolDirectSet ) $this->set( 'm_strComponentType', trim( stripcslashes( $arrValues['component_type'] ) ) ); elseif( isset( $arrValues['component_type'] ) ) $this->setComponentType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['component_type'] ) : $arrValues['component_type'] );
		if( isset( $arrValues['template_component_key'] ) && $boolDirectSet ) $this->set( 'm_strTemplateComponentKey', trim( stripcslashes( $arrValues['template_component_key'] ) ) ); elseif( isset( $arrValues['template_component_key'] ) ) $this->setTemplateComponentKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['template_component_key'] ) : $arrValues['template_component_key'] );
		if( isset( $arrValues['size'] ) && $boolDirectSet ) $this->set( 'm_intSize', trim( $arrValues['size'] ) ); elseif( isset( $arrValues['size'] ) ) $this->setSize( $arrValues['size'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['synced_on'] ) && $boolDirectSet ) $this->set( 'm_strSyncedOn', trim( $arrValues['synced_on'] ) ); elseif( isset( $arrValues['synced_on'] ) ) $this->setSyncedOn( $arrValues['synced_on'] );
		if( isset( $arrValues['require_sync'] ) && $boolDirectSet ) $this->set( 'm_intRequireSync', trim( $arrValues['require_sync'] ) ); elseif( isset( $arrValues['require_sync'] ) ) $this->setRequireSync( $arrValues['require_sync'] );
		if( isset( $arrValues['allows_updates'] ) && $boolDirectSet ) $this->set( 'm_intAllowsUpdates', trim( $arrValues['allows_updates'] ) ); elseif( isset( $arrValues['allows_updates'] ) ) $this->setAllowsUpdates( $arrValues['allows_updates'] );
		if( isset( $arrValues['is_modifiable'] ) && $boolDirectSet ) $this->set( 'm_intIsModifiable', trim( $arrValues['is_modifiable'] ) ); elseif( isset( $arrValues['is_modifiable'] ) ) $this->setIsModifiable( $arrValues['is_modifiable'] );
		if( isset( $arrValues['is_removable'] ) && $boolDirectSet ) $this->set( 'm_intIsRemovable', trim( $arrValues['is_removable'] ) ); elseif( isset( $arrValues['is_removable'] ) ) $this->setIsRemovable( $arrValues['is_removable'] );
		if( isset( $arrValues['default_is_auto_updated'] ) && $boolDirectSet ) $this->set( 'm_intDefaultIsAutoUpdated', trim( $arrValues['default_is_auto_updated'] ) ); elseif( isset( $arrValues['default_is_auto_updated'] ) ) $this->setDefaultIsAutoUpdated( $arrValues['default_is_auto_updated'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDocumentTemplateId( $intDocumentTemplateId ) {
		$this->set( 'm_intDocumentTemplateId', CStrings::strToIntDef( $intDocumentTemplateId, NULL, false ) );
	}

	public function getDocumentTemplateId() {
		return $this->m_intDocumentTemplateId;
	}

	public function sqlDocumentTemplateId() {
		return ( true == isset( $this->m_intDocumentTemplateId ) ) ? ( string ) $this->m_intDocumentTemplateId : 'NULL';
	}

	public function setDocumentTemplateComponentId( $intDocumentTemplateComponentId ) {
		$this->set( 'm_intDocumentTemplateComponentId', CStrings::strToIntDef( $intDocumentTemplateComponentId, NULL, false ) );
	}

	public function getDocumentTemplateComponentId() {
		return $this->m_intDocumentTemplateComponentId;
	}

	public function sqlDocumentTemplateComponentId() {
		return ( true == isset( $this->m_intDocumentTemplateComponentId ) ) ? ( string ) $this->m_intDocumentTemplateComponentId : 'NULL';
	}

	public function setArchivedTemplateComponentId( $intArchivedTemplateComponentId ) {
		$this->set( 'm_intArchivedTemplateComponentId', CStrings::strToIntDef( $intArchivedTemplateComponentId, NULL, false ) );
	}

	public function getArchivedTemplateComponentId() {
		return $this->m_intArchivedTemplateComponentId;
	}

	public function sqlArchivedTemplateComponentId() {
		return ( true == isset( $this->m_intArchivedTemplateComponentId ) ) ? ( string ) $this->m_intArchivedTemplateComponentId : 'NULL';
	}

	public function setComponentType( $strComponentType ) {
		$this->set( 'm_strComponentType', CStrings::strTrimDef( $strComponentType, 50, NULL, true ) );
	}

	public function getComponentType() {
		return $this->m_strComponentType;
	}

	public function sqlComponentType() {
		return ( true == isset( $this->m_strComponentType ) ) ? '\'' . addslashes( $this->m_strComponentType ) . '\'' : 'NULL';
	}

	public function setTemplateComponentKey( $strTemplateComponentKey ) {
		$this->set( 'm_strTemplateComponentKey', CStrings::strTrimDef( $strTemplateComponentKey, 64, NULL, true ) );
	}

	public function getTemplateComponentKey() {
		return $this->m_strTemplateComponentKey;
	}

	public function sqlTemplateComponentKey() {
		return ( true == isset( $this->m_strTemplateComponentKey ) ) ? '\'' . addslashes( $this->m_strTemplateComponentKey ) . '\'' : 'NULL';
	}

	public function setSize( $intSize ) {
		$this->set( 'm_intSize', CStrings::strToIntDef( $intSize, NULL, false ) );
	}

	public function getSize() {
		return $this->m_intSize;
	}

	public function sqlSize() {
		return ( true == isset( $this->m_intSize ) ) ? ( string ) $this->m_intSize : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setSyncedOn( $strSyncedOn ) {
		$this->set( 'm_strSyncedOn', CStrings::strTrimDef( $strSyncedOn, -1, NULL, true ) );
	}

	public function getSyncedOn() {
		return $this->m_strSyncedOn;
	}

	public function sqlSyncedOn() {
		return ( true == isset( $this->m_strSyncedOn ) ) ? '\'' . $this->m_strSyncedOn . '\'' : 'NULL';
	}

	public function setRequireSync( $intRequireSync ) {
		$this->set( 'm_intRequireSync', CStrings::strToIntDef( $intRequireSync, NULL, false ) );
	}

	public function getRequireSync() {
		return $this->m_intRequireSync;
	}

	public function sqlRequireSync() {
		return ( true == isset( $this->m_intRequireSync ) ) ? ( string ) $this->m_intRequireSync : '0';
	}

	public function setAllowsUpdates( $intAllowsUpdates ) {
		$this->set( 'm_intAllowsUpdates', CStrings::strToIntDef( $intAllowsUpdates, NULL, false ) );
	}

	public function getAllowsUpdates() {
		return $this->m_intAllowsUpdates;
	}

	public function sqlAllowsUpdates() {
		return ( true == isset( $this->m_intAllowsUpdates ) ) ? ( string ) $this->m_intAllowsUpdates : '0';
	}

	public function setIsModifiable( $intIsModifiable ) {
		$this->set( 'm_intIsModifiable', CStrings::strToIntDef( $intIsModifiable, NULL, false ) );
	}

	public function getIsModifiable() {
		return $this->m_intIsModifiable;
	}

	public function sqlIsModifiable() {
		return ( true == isset( $this->m_intIsModifiable ) ) ? ( string ) $this->m_intIsModifiable : 'NULL';
	}

	public function setIsRemovable( $intIsRemovable ) {
		$this->set( 'm_intIsRemovable', CStrings::strToIntDef( $intIsRemovable, NULL, false ) );
	}

	public function getIsRemovable() {
		return $this->m_intIsRemovable;
	}

	public function sqlIsRemovable() {
		return ( true == isset( $this->m_intIsRemovable ) ) ? ( string ) $this->m_intIsRemovable : 'NULL';
	}

	public function setDefaultIsAutoUpdated( $intDefaultIsAutoUpdated ) {
		$this->set( 'm_intDefaultIsAutoUpdated', CStrings::strToIntDef( $intDefaultIsAutoUpdated, NULL, false ) );
	}

	public function getDefaultIsAutoUpdated() {
		return $this->m_intDefaultIsAutoUpdated;
	}

	public function sqlDefaultIsAutoUpdated() {
		return ( true == isset( $this->m_intDefaultIsAutoUpdated ) ) ? ( string ) $this->m_intDefaultIsAutoUpdated : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, document_template_id, document_template_component_id, archived_template_component_id, component_type, template_component_key, size, order_num, synced_on, require_sync, allows_updates, is_modifiable, is_removable, default_is_auto_updated, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDocumentTemplateId() . ', ' .
 						$this->sqlDocumentTemplateComponentId() . ', ' .
 						$this->sqlArchivedTemplateComponentId() . ', ' .
 						$this->sqlComponentType() . ', ' .
 						$this->sqlTemplateComponentKey() . ', ' .
 						$this->sqlSize() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlSyncedOn() . ', ' .
 						$this->sqlRequireSync() . ', ' .
 						$this->sqlAllowsUpdates() . ', ' .
 						$this->sqlIsModifiable() . ', ' .
 						$this->sqlIsRemovable() . ', ' .
 						$this->sqlDefaultIsAutoUpdated() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_template_id = ' . $this->sqlDocumentTemplateId() . ','; } elseif( true == array_key_exists( 'DocumentTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' document_template_id = ' . $this->sqlDocumentTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_template_component_id = ' . $this->sqlDocumentTemplateComponentId() . ','; } elseif( true == array_key_exists( 'DocumentTemplateComponentId', $this->getChangedColumns() ) ) { $strSql .= ' document_template_component_id = ' . $this->sqlDocumentTemplateComponentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archived_template_component_id = ' . $this->sqlArchivedTemplateComponentId() . ','; } elseif( true == array_key_exists( 'ArchivedTemplateComponentId', $this->getChangedColumns() ) ) { $strSql .= ' archived_template_component_id = ' . $this->sqlArchivedTemplateComponentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' component_type = ' . $this->sqlComponentType() . ','; } elseif( true == array_key_exists( 'ComponentType', $this->getChangedColumns() ) ) { $strSql .= ' component_type = ' . $this->sqlComponentType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_component_key = ' . $this->sqlTemplateComponentKey() . ','; } elseif( true == array_key_exists( 'TemplateComponentKey', $this->getChangedColumns() ) ) { $strSql .= ' template_component_key = ' . $this->sqlTemplateComponentKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' size = ' . $this->sqlSize() . ','; } elseif( true == array_key_exists( 'Size', $this->getChangedColumns() ) ) { $strSql .= ' size = ' . $this->sqlSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' synced_on = ' . $this->sqlSyncedOn() . ','; } elseif( true == array_key_exists( 'SyncedOn', $this->getChangedColumns() ) ) { $strSql .= ' synced_on = ' . $this->sqlSyncedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_sync = ' . $this->sqlRequireSync() . ','; } elseif( true == array_key_exists( 'RequireSync', $this->getChangedColumns() ) ) { $strSql .= ' require_sync = ' . $this->sqlRequireSync() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allows_updates = ' . $this->sqlAllowsUpdates() . ','; } elseif( true == array_key_exists( 'AllowsUpdates', $this->getChangedColumns() ) ) { $strSql .= ' allows_updates = ' . $this->sqlAllowsUpdates() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_modifiable = ' . $this->sqlIsModifiable() . ','; } elseif( true == array_key_exists( 'IsModifiable', $this->getChangedColumns() ) ) { $strSql .= ' is_modifiable = ' . $this->sqlIsModifiable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_removable = ' . $this->sqlIsRemovable() . ','; } elseif( true == array_key_exists( 'IsRemovable', $this->getChangedColumns() ) ) { $strSql .= ' is_removable = ' . $this->sqlIsRemovable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_is_auto_updated = ' . $this->sqlDefaultIsAutoUpdated() . ','; } elseif( true == array_key_exists( 'DefaultIsAutoUpdated', $this->getChangedColumns() ) ) { $strSql .= ' default_is_auto_updated = ' . $this->sqlDefaultIsAutoUpdated() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'document_template_id' => $this->getDocumentTemplateId(),
			'document_template_component_id' => $this->getDocumentTemplateComponentId(),
			'archived_template_component_id' => $this->getArchivedTemplateComponentId(),
			'component_type' => $this->getComponentType(),
			'template_component_key' => $this->getTemplateComponentKey(),
			'size' => $this->getSize(),
			'order_num' => $this->getOrderNum(),
			'synced_on' => $this->getSyncedOn(),
			'require_sync' => $this->getRequireSync(),
			'allows_updates' => $this->getAllowsUpdates(),
			'is_modifiable' => $this->getIsModifiable(),
			'is_removable' => $this->getIsRemovable(),
			'default_is_auto_updated' => $this->getDefaultIsAutoUpdated(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>