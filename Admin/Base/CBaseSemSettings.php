<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemSettings
 * Do not add any new functions to this class.
 */

class CBaseSemSettings extends CEosPluralBase {

	/**
	 * @return CSemSetting[]
	 */
	public static function fetchSemSettings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemSetting', $objDatabase );
	}

	/**
	 * @return CSemSetting
	 */
	public static function fetchSemSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemSetting', $objDatabase );
	}

	public static function fetchSemSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_settings', $objDatabase );
	}

	public static function fetchSemSettingById( $intId, $objDatabase ) {
		return self::fetchSemSetting( sprintf( 'SELECT * FROM sem_settings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemSettingsByCid( $intCid, $objDatabase ) {
		return self::fetchSemSettings( sprintf( 'SELECT * FROM sem_settings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSemSettingsByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchSemSettings( sprintf( 'SELECT * FROM sem_settings WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

	public static function fetchSemSettingsBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemSettings( sprintf( 'SELECT * FROM sem_settings WHERE sem_ad_group_id = %d', ( int ) $intSemAdGroupId ), $objDatabase );
	}

	public static function fetchSemSettingsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSemSettings( sprintf( 'SELECT * FROM sem_settings WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchSemSettingsByFrequencyId( $intFrequencyId, $objDatabase ) {
		return self::fetchSemSettings( sprintf( 'SELECT * FROM sem_settings WHERE frequency_id = %d', ( int ) $intFrequencyId ), $objDatabase );
	}

}
?>