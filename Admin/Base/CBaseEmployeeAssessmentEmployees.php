<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAssessmentEmployees
 * Do not add any new functions to this class.
 */

class CBaseEmployeeAssessmentEmployees extends CEosPluralBase {

	/**
	 * @return CEmployeeAssessmentEmployee[]
	 */
	public static function fetchEmployeeAssessmentEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeAssessmentEmployee', $objDatabase );
	}

	/**
	 * @return CEmployeeAssessmentEmployee
	 */
	public static function fetchEmployeeAssessmentEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeAssessmentEmployee', $objDatabase );
	}

	public static function fetchEmployeeAssessmentEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_assessment_employees', $objDatabase );
	}

	public static function fetchEmployeeAssessmentEmployeeById( $intId, $objDatabase ) {
		return self::fetchEmployeeAssessmentEmployee( sprintf( 'SELECT * FROM employee_assessment_employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentEmployeesByEmployeeAssessmentId( $intEmployeeAssessmentId, $objDatabase ) {
		return self::fetchEmployeeAssessmentEmployees( sprintf( 'SELECT * FROM employee_assessment_employees WHERE employee_assessment_id = %d', ( int ) $intEmployeeAssessmentId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeAssessmentEmployees( sprintf( 'SELECT * FROM employee_assessment_employees WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>