<?php

class CBaseOauthClientScope extends CEosSingularBase {

	const TABLE_NAME = 'public.oauth_client_scopes';

	protected $m_intId;
	protected $m_intOauthClientId;
	protected $m_intScopeId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['oauth_client_id'] ) && $boolDirectSet ) $this->set( 'm_intOauthClientId', trim( $arrValues['oauth_client_id'] ) ); elseif( isset( $arrValues['oauth_client_id'] ) ) $this->setOauthClientId( $arrValues['oauth_client_id'] );
		if( isset( $arrValues['scope_id'] ) && $boolDirectSet ) $this->set( 'm_intScopeId', trim( $arrValues['scope_id'] ) ); elseif( isset( $arrValues['scope_id'] ) ) $this->setScopeId( $arrValues['scope_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOauthClientId( $intOauthClientId ) {
		$this->set( 'm_intOauthClientId', CStrings::strToIntDef( $intOauthClientId, NULL, false ) );
	}

	public function getOauthClientId() {
		return $this->m_intOauthClientId;
	}

	public function sqlOauthClientId() {
		return ( true == isset( $this->m_intOauthClientId ) ) ? ( string ) $this->m_intOauthClientId : 'NULL';
	}

	public function setScopeId( $intScopeId ) {
		$this->set( 'm_intScopeId', CStrings::strToIntDef( $intScopeId, NULL, false ) );
	}

	public function getScopeId() {
		return $this->m_intScopeId;
	}

	public function sqlScopeId() {
		return ( true == isset( $this->m_intScopeId ) ) ? ( string ) $this->m_intScopeId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, oauth_client_id, scope_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlOauthClientId() . ', ' .
 						$this->sqlScopeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' oauth_client_id = ' . $this->sqlOauthClientId() . ','; } elseif( true == array_key_exists( 'OauthClientId', $this->getChangedColumns() ) ) { $strSql .= ' oauth_client_id = ' . $this->sqlOauthClientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scope_id = ' . $this->sqlScopeId() . ','; } elseif( true == array_key_exists( 'ScopeId', $this->getChangedColumns() ) ) { $strSql .= ' scope_id = ' . $this->sqlScopeId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'oauth_client_id' => $this->getOauthClientId(),
			'scope_id' => $this->getScopeId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>