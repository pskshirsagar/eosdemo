<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskNoteTypes
 * Do not add any new functions to this class.
 */

class CBaseTaskNoteTypes extends CEosPluralBase {

	/**
	 * @return CTaskNoteType[]
	 */
	public static function fetchTaskNoteTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskNoteType', $objDatabase );
	}

	/**
	 * @return CTaskNoteType
	 */
	public static function fetchTaskNoteType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskNoteType', $objDatabase );
	}

	public static function fetchTaskNoteTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_note_types', $objDatabase );
	}

	public static function fetchTaskNoteTypeById( $intId, $objDatabase ) {
		return self::fetchTaskNoteType( sprintf( 'SELECT * FROM task_note_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>