<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPurchaseRequestStatusTypes
 * Do not add any new functions to this class.
 */

class CBasePurchaseRequestStatusTypes extends CEosPluralBase {

	/**
	 * @return CPurchaseRequestStatusType[]
	 */
	public static function fetchPurchaseRequestStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPurchaseRequestStatusType', $objDatabase );
	}

	/**
	 * @return CPurchaseRequestStatusType
	 */
	public static function fetchPurchaseRequestStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPurchaseRequestStatusType', $objDatabase );
	}

	public static function fetchPurchaseRequestStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'purchase_request_status_types', $objDatabase );
	}

	public static function fetchPurchaseRequestStatusTypeById( $intId, $objDatabase ) {
		return self::fetchPurchaseRequestStatusType( sprintf( 'SELECT * FROM purchase_request_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>