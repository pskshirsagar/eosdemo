<?php

class CBaseStatsOpenBugCount extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_open_bug_counts';

	protected $m_intId;
	protected $m_intAddEmployeeId;
	protected $m_intQamEmployeeId;
	protected $m_intOpenBugCount;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intOpenBugCount = '0';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['add_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intAddEmployeeId', trim( $arrValues['add_employee_id'] ) ); elseif( isset( $arrValues['add_employee_id'] ) ) $this->setAddEmployeeId( $arrValues['add_employee_id'] );
		if( isset( $arrValues['qam_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQamEmployeeId', trim( $arrValues['qam_employee_id'] ) ); elseif( isset( $arrValues['qam_employee_id'] ) ) $this->setQamEmployeeId( $arrValues['qam_employee_id'] );
		if( isset( $arrValues['open_bug_count'] ) && $boolDirectSet ) $this->set( 'm_intOpenBugCount', trim( $arrValues['open_bug_count'] ) ); elseif( isset( $arrValues['open_bug_count'] ) ) $this->setOpenBugCount( $arrValues['open_bug_count'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAddEmployeeId( $intAddEmployeeId ) {
		$this->set( 'm_intAddEmployeeId', CStrings::strToIntDef( $intAddEmployeeId, NULL, false ) );
	}

	public function getAddEmployeeId() {
		return $this->m_intAddEmployeeId;
	}

	public function sqlAddEmployeeId() {
		return ( true == isset( $this->m_intAddEmployeeId ) ) ? ( string ) $this->m_intAddEmployeeId : 'NULL';
	}

	public function setQamEmployeeId( $intQamEmployeeId ) {
		$this->set( 'm_intQamEmployeeId', CStrings::strToIntDef( $intQamEmployeeId, NULL, false ) );
	}

	public function getQamEmployeeId() {
		return $this->m_intQamEmployeeId;
	}

	public function sqlQamEmployeeId() {
		return ( true == isset( $this->m_intQamEmployeeId ) ) ? ( string ) $this->m_intQamEmployeeId : 'NULL';
	}

	public function setOpenBugCount( $intOpenBugCount ) {
		$this->set( 'm_intOpenBugCount', CStrings::strToIntDef( $intOpenBugCount, NULL, false ) );
	}

	public function getOpenBugCount() {
		return $this->m_intOpenBugCount;
	}

	public function sqlOpenBugCount() {
		return ( true == isset( $this->m_intOpenBugCount ) ) ? ( string ) $this->m_intOpenBugCount : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, add_employee_id, qam_employee_id, open_bug_count, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlAddEmployeeId() . ', ' .
						$this->sqlQamEmployeeId() . ', ' .
						$this->sqlOpenBugCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_employee_id = ' . $this->sqlAddEmployeeId(). ',' ; } elseif( true == array_key_exists( 'AddEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' add_employee_id = ' . $this->sqlAddEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qam_employee_id = ' . $this->sqlQamEmployeeId(). ',' ; } elseif( true == array_key_exists( 'QamEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qam_employee_id = ' . $this->sqlQamEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' open_bug_count = ' . $this->sqlOpenBugCount() ; } elseif( true == array_key_exists( 'OpenBugCount', $this->getChangedColumns() ) ) { $strSql .= ' open_bug_count = ' . $this->sqlOpenBugCount() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'add_employee_id' => $this->getAddEmployeeId(),
			'qam_employee_id' => $this->getQamEmployeeId(),
			'open_bug_count' => $this->getOpenBugCount(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>