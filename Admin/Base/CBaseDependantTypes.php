<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDependantTypes
 * Do not add any new functions to this class.
 */

class CBaseDependantTypes extends CEosPluralBase {

	/**
	 * @return CDependantType[]
	 */
	public static function fetchDependantTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDependantType', $objDatabase );
	}

	/**
	 * @return CDependantType
	 */
	public static function fetchDependantType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDependantType', $objDatabase );
	}

	public static function fetchDependantTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'dependant_types', $objDatabase );
	}

	public static function fetchDependantTypeById( $intId, $objDatabase ) {
		return self::fetchDependantType( sprintf( 'SELECT * FROM dependant_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>