<?php

class CBaseStorageObject extends CEosSingularBase {

	const TABLE_NAME = 'public.storage_objects';

	protected $m_intId;
	protected $m_intStorageTypeId;
	protected $m_intPsProductId;
	protected $m_intTeamId;
	protected $m_intParentStorageObjectId;
	protected $m_intScriptId;
	protected $m_strFolderName;
	protected $m_strFolderPath;
	protected $m_boolIsRequireCleanup;
	protected $m_fltCleanupFrequency;
	protected $m_strCleanupCommand;
	protected $m_strCleanStartDatetime;
	protected $m_strCleanEndDatetime;
	protected $m_strObjectDatetime;
	protected $m_boolIsApproved;
	protected $m_intObjectCleanupCount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsOneTimeCleanup;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsRequireCleanup = false;
		$this->m_boolIsApproved = false;
		$this->m_intObjectCleanupCount = '0';
		$this->m_boolIsOneTimeCleanup = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['storage_type_id'] ) && $boolDirectSet ) $this->set( 'm_intStorageTypeId', trim( $arrValues['storage_type_id'] ) ); elseif( isset( $arrValues['storage_type_id'] ) ) $this->setStorageTypeId( $arrValues['storage_type_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['team_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamId', trim( $arrValues['team_id'] ) ); elseif( isset( $arrValues['team_id'] ) ) $this->setTeamId( $arrValues['team_id'] );
		if( isset( $arrValues['parent_storage_object_id'] ) && $boolDirectSet ) $this->set( 'm_intParentStorageObjectId', trim( $arrValues['parent_storage_object_id'] ) ); elseif( isset( $arrValues['parent_storage_object_id'] ) ) $this->setParentStorageObjectId( $arrValues['parent_storage_object_id'] );
		if( isset( $arrValues['script_id'] ) && $boolDirectSet ) $this->set( 'm_intScriptId', trim( $arrValues['script_id'] ) ); elseif( isset( $arrValues['script_id'] ) ) $this->setScriptId( $arrValues['script_id'] );
		if( isset( $arrValues['folder_name'] ) && $boolDirectSet ) $this->set( 'm_strFolderName', trim( stripcslashes( $arrValues['folder_name'] ) ) ); elseif( isset( $arrValues['folder_name'] ) ) $this->setFolderName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['folder_name'] ) : $arrValues['folder_name'] );
		if( isset( $arrValues['folder_path'] ) && $boolDirectSet ) $this->set( 'm_strFolderPath', trim( stripcslashes( $arrValues['folder_path'] ) ) ); elseif( isset( $arrValues['folder_path'] ) ) $this->setFolderPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['folder_path'] ) : $arrValues['folder_path'] );
		if( isset( $arrValues['is_require_cleanup'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequireCleanup', trim( stripcslashes( $arrValues['is_require_cleanup'] ) ) ); elseif( isset( $arrValues['is_require_cleanup'] ) ) $this->setIsRequireCleanup( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_require_cleanup'] ) : $arrValues['is_require_cleanup'] );
		if( isset( $arrValues['cleanup_frequency'] ) && $boolDirectSet ) $this->set( 'm_fltCleanupFrequency', trim( $arrValues['cleanup_frequency'] ) ); elseif( isset( $arrValues['cleanup_frequency'] ) ) $this->setCleanupFrequency( $arrValues['cleanup_frequency'] );
		if( isset( $arrValues['cleanup_command'] ) && $boolDirectSet ) $this->set( 'm_strCleanupCommand', trim( stripcslashes( $arrValues['cleanup_command'] ) ) ); elseif( isset( $arrValues['cleanup_command'] ) ) $this->setCleanupCommand( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cleanup_command'] ) : $arrValues['cleanup_command'] );
		if( isset( $arrValues['clean_start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCleanStartDatetime', trim( $arrValues['clean_start_datetime'] ) ); elseif( isset( $arrValues['clean_start_datetime'] ) ) $this->setCleanStartDatetime( $arrValues['clean_start_datetime'] );
		if( isset( $arrValues['clean_end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCleanEndDatetime', trim( $arrValues['clean_end_datetime'] ) ); elseif( isset( $arrValues['clean_end_datetime'] ) ) $this->setCleanEndDatetime( $arrValues['clean_end_datetime'] );
		if( isset( $arrValues['object_datetime'] ) && $boolDirectSet ) $this->set( 'm_strObjectDatetime', trim( $arrValues['object_datetime'] ) ); elseif( isset( $arrValues['object_datetime'] ) ) $this->setObjectDatetime( $arrValues['object_datetime'] );
		if( isset( $arrValues['is_approved'] ) && $boolDirectSet ) $this->set( 'm_boolIsApproved', trim( stripcslashes( $arrValues['is_approved'] ) ) ); elseif( isset( $arrValues['is_approved'] ) ) $this->setIsApproved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_approved'] ) : $arrValues['is_approved'] );
		if( isset( $arrValues['object_cleanup_count'] ) && $boolDirectSet ) $this->set( 'm_intObjectCleanupCount', trim( $arrValues['object_cleanup_count'] ) ); elseif( isset( $arrValues['object_cleanup_count'] ) ) $this->setObjectCleanupCount( $arrValues['object_cleanup_count'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_one_time_cleanup'] ) && $boolDirectSet ) $this->set( 'm_boolIsOneTimeCleanup', trim( stripcslashes( $arrValues['is_one_time_cleanup'] ) ) ); elseif( isset( $arrValues['is_one_time_cleanup'] ) ) $this->setIsOneTimeCleanup( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_one_time_cleanup'] ) : $arrValues['is_one_time_cleanup'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setStorageTypeId( $intStorageTypeId ) {
		$this->set( 'm_intStorageTypeId', CStrings::strToIntDef( $intStorageTypeId, NULL, false ) );
	}

	public function getStorageTypeId() {
		return $this->m_intStorageTypeId;
	}

	public function sqlStorageTypeId() {
		return ( true == isset( $this->m_intStorageTypeId ) ) ? ( string ) $this->m_intStorageTypeId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setTeamId( $intTeamId ) {
		$this->set( 'm_intTeamId', CStrings::strToIntDef( $intTeamId, NULL, false ) );
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function sqlTeamId() {
		return ( true == isset( $this->m_intTeamId ) ) ? ( string ) $this->m_intTeamId : 'NULL';
	}

	public function setParentStorageObjectId( $intParentStorageObjectId ) {
		$this->set( 'm_intParentStorageObjectId', CStrings::strToIntDef( $intParentStorageObjectId, NULL, false ) );
	}

	public function getParentStorageObjectId() {
		return $this->m_intParentStorageObjectId;
	}

	public function sqlParentStorageObjectId() {
		return ( true == isset( $this->m_intParentStorageObjectId ) ) ? ( string ) $this->m_intParentStorageObjectId : 'NULL';
	}

	public function setScriptId( $intScriptId ) {
		$this->set( 'm_intScriptId', CStrings::strToIntDef( $intScriptId, NULL, false ) );
	}

	public function getScriptId() {
		return $this->m_intScriptId;
	}

	public function sqlScriptId() {
		return ( true == isset( $this->m_intScriptId ) ) ? ( string ) $this->m_intScriptId : 'NULL';
	}

	public function setFolderName( $strFolderName ) {
		$this->set( 'm_strFolderName', CStrings::strTrimDef( $strFolderName, 100, NULL, true ) );
	}

	public function getFolderName() {
		return $this->m_strFolderName;
	}

	public function sqlFolderName() {
		return ( true == isset( $this->m_strFolderName ) ) ? '\'' . addslashes( $this->m_strFolderName ) . '\'' : 'NULL';
	}

	public function setFolderPath( $strFolderPath ) {
		$this->set( 'm_strFolderPath', CStrings::strTrimDef( $strFolderPath, 100, NULL, true ) );
	}

	public function getFolderPath() {
		return $this->m_strFolderPath;
	}

	public function sqlFolderPath() {
		return ( true == isset( $this->m_strFolderPath ) ) ? '\'' . addslashes( $this->m_strFolderPath ) . '\'' : 'NULL';
	}

	public function setIsRequireCleanup( $boolIsRequireCleanup ) {
		$this->set( 'm_boolIsRequireCleanup', CStrings::strToBool( $boolIsRequireCleanup ) );
	}

	public function getIsRequireCleanup() {
		return $this->m_boolIsRequireCleanup;
	}

	public function sqlIsRequireCleanup() {
		return ( true == isset( $this->m_boolIsRequireCleanup ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequireCleanup ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCleanupFrequency( $fltCleanupFrequency ) {
		$this->set( 'm_fltCleanupFrequency', CStrings::strToFloatDef( $fltCleanupFrequency, NULL, false, 0 ) );
	}

	public function getCleanupFrequency() {
		return $this->m_fltCleanupFrequency;
	}

	public function sqlCleanupFrequency() {
		return ( true == isset( $this->m_fltCleanupFrequency ) ) ? ( string ) $this->m_fltCleanupFrequency : 'NULL';
	}

	public function setCleanupCommand( $strCleanupCommand ) {
		$this->set( 'm_strCleanupCommand', CStrings::strTrimDef( $strCleanupCommand, 255, NULL, true ) );
	}

	public function getCleanupCommand() {
		return $this->m_strCleanupCommand;
	}

	public function sqlCleanupCommand() {
		return ( true == isset( $this->m_strCleanupCommand ) ) ? '\'' . addslashes( $this->m_strCleanupCommand ) . '\'' : 'NULL';
	}

	public function setCleanStartDatetime( $strCleanStartDatetime ) {
		$this->set( 'm_strCleanStartDatetime', CStrings::strTrimDef( $strCleanStartDatetime, -1, NULL, true ) );
	}

	public function getCleanStartDatetime() {
		return $this->m_strCleanStartDatetime;
	}

	public function sqlCleanStartDatetime() {
		return ( true == isset( $this->m_strCleanStartDatetime ) ) ? '\'' . $this->m_strCleanStartDatetime . '\'' : 'NULL';
	}

	public function setCleanEndDatetime( $strCleanEndDatetime ) {
		$this->set( 'm_strCleanEndDatetime', CStrings::strTrimDef( $strCleanEndDatetime, -1, NULL, true ) );
	}

	public function getCleanEndDatetime() {
		return $this->m_strCleanEndDatetime;
	}

	public function sqlCleanEndDatetime() {
		return ( true == isset( $this->m_strCleanEndDatetime ) ) ? '\'' . $this->m_strCleanEndDatetime . '\'' : 'NULL';
	}

	public function setObjectDatetime( $strObjectDatetime ) {
		$this->set( 'm_strObjectDatetime', CStrings::strTrimDef( $strObjectDatetime, -1, NULL, true ) );
	}

	public function getObjectDatetime() {
		return $this->m_strObjectDatetime;
	}

	public function sqlObjectDatetime() {
		return ( true == isset( $this->m_strObjectDatetime ) ) ? '\'' . $this->m_strObjectDatetime . '\'' : 'NULL';
	}

	public function setIsApproved( $boolIsApproved ) {
		$this->set( 'm_boolIsApproved', CStrings::strToBool( $boolIsApproved ) );
	}

	public function getIsApproved() {
		return $this->m_boolIsApproved;
	}

	public function sqlIsApproved() {
		return ( true == isset( $this->m_boolIsApproved ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsApproved ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setObjectCleanupCount( $intObjectCleanupCount ) {
		$this->set( 'm_intObjectCleanupCount', CStrings::strToIntDef( $intObjectCleanupCount, NULL, false ) );
	}

	public function getObjectCleanupCount() {
		return $this->m_intObjectCleanupCount;
	}

	public function sqlObjectCleanupCount() {
		return ( true == isset( $this->m_intObjectCleanupCount ) ) ? ( string ) $this->m_intObjectCleanupCount : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsOneTimeCleanup( $boolIsOneTimeCleanup ) {
		$this->set( 'm_boolIsOneTimeCleanup', CStrings::strToBool( $boolIsOneTimeCleanup ) );
	}

	public function getIsOneTimeCleanup() {
		return $this->m_boolIsOneTimeCleanup;
	}

	public function sqlIsOneTimeCleanup() {
		return ( true == isset( $this->m_boolIsOneTimeCleanup ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOneTimeCleanup ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, storage_type_id, ps_product_id, team_id, parent_storage_object_id, script_id, folder_name, folder_path, is_require_cleanup, cleanup_frequency, cleanup_command, clean_start_datetime, clean_end_datetime, object_datetime, is_approved, object_cleanup_count, updated_by, updated_on, created_by, created_on, is_one_time_cleanup )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlStorageTypeId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlTeamId() . ', ' .
 						$this->sqlParentStorageObjectId() . ', ' .
 						$this->sqlScriptId() . ', ' .
 						$this->sqlFolderName() . ', ' .
 						$this->sqlFolderPath() . ', ' .
 						$this->sqlIsRequireCleanup() . ', ' .
 						$this->sqlCleanupFrequency() . ', ' .
 						$this->sqlCleanupCommand() . ', ' .
 						$this->sqlCleanStartDatetime() . ', ' .
 						$this->sqlCleanEndDatetime() . ', ' .
 						$this->sqlObjectDatetime() . ', ' .
 						$this->sqlIsApproved() . ', ' .
 						$this->sqlObjectCleanupCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlIsOneTimeCleanup() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' storage_type_id = ' . $this->sqlStorageTypeId() . ','; } elseif( true == array_key_exists( 'StorageTypeId', $this->getChangedColumns() ) ) { $strSql .= ' storage_type_id = ' . $this->sqlStorageTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; } elseif( true == array_key_exists( 'TeamId', $this->getChangedColumns() ) ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_storage_object_id = ' . $this->sqlParentStorageObjectId() . ','; } elseif( true == array_key_exists( 'ParentStorageObjectId', $this->getChangedColumns() ) ) { $strSql .= ' parent_storage_object_id = ' . $this->sqlParentStorageObjectId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_id = ' . $this->sqlScriptId() . ','; } elseif( true == array_key_exists( 'ScriptId', $this->getChangedColumns() ) ) { $strSql .= ' script_id = ' . $this->sqlScriptId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' folder_name = ' . $this->sqlFolderName() . ','; } elseif( true == array_key_exists( 'FolderName', $this->getChangedColumns() ) ) { $strSql .= ' folder_name = ' . $this->sqlFolderName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' folder_path = ' . $this->sqlFolderPath() . ','; } elseif( true == array_key_exists( 'FolderPath', $this->getChangedColumns() ) ) { $strSql .= ' folder_path = ' . $this->sqlFolderPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_require_cleanup = ' . $this->sqlIsRequireCleanup() . ','; } elseif( true == array_key_exists( 'IsRequireCleanup', $this->getChangedColumns() ) ) { $strSql .= ' is_require_cleanup = ' . $this->sqlIsRequireCleanup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cleanup_frequency = ' . $this->sqlCleanupFrequency() . ','; } elseif( true == array_key_exists( 'CleanupFrequency', $this->getChangedColumns() ) ) { $strSql .= ' cleanup_frequency = ' . $this->sqlCleanupFrequency() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cleanup_command = ' . $this->sqlCleanupCommand() . ','; } elseif( true == array_key_exists( 'CleanupCommand', $this->getChangedColumns() ) ) { $strSql .= ' cleanup_command = ' . $this->sqlCleanupCommand() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clean_start_datetime = ' . $this->sqlCleanStartDatetime() . ','; } elseif( true == array_key_exists( 'CleanStartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' clean_start_datetime = ' . $this->sqlCleanStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clean_end_datetime = ' . $this->sqlCleanEndDatetime() . ','; } elseif( true == array_key_exists( 'CleanEndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' clean_end_datetime = ' . $this->sqlCleanEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' object_datetime = ' . $this->sqlObjectDatetime() . ','; } elseif( true == array_key_exists( 'ObjectDatetime', $this->getChangedColumns() ) ) { $strSql .= ' object_datetime = ' . $this->sqlObjectDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; } elseif( true == array_key_exists( 'IsApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' object_cleanup_count = ' . $this->sqlObjectCleanupCount() . ','; } elseif( true == array_key_exists( 'ObjectCleanupCount', $this->getChangedColumns() ) ) { $strSql .= ' object_cleanup_count = ' . $this->sqlObjectCleanupCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_one_time_cleanup = ' . $this->sqlIsOneTimeCleanup() . ','; } elseif( true == array_key_exists( 'IsOneTimeCleanup', $this->getChangedColumns() ) ) { $strSql .= ' is_one_time_cleanup = ' . $this->sqlIsOneTimeCleanup() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'storage_type_id' => $this->getStorageTypeId(),
			'ps_product_id' => $this->getPsProductId(),
			'team_id' => $this->getTeamId(),
			'parent_storage_object_id' => $this->getParentStorageObjectId(),
			'script_id' => $this->getScriptId(),
			'folder_name' => $this->getFolderName(),
			'folder_path' => $this->getFolderPath(),
			'is_require_cleanup' => $this->getIsRequireCleanup(),
			'cleanup_frequency' => $this->getCleanupFrequency(),
			'cleanup_command' => $this->getCleanupCommand(),
			'clean_start_datetime' => $this->getCleanStartDatetime(),
			'clean_end_datetime' => $this->getCleanEndDatetime(),
			'object_datetime' => $this->getObjectDatetime(),
			'is_approved' => $this->getIsApproved(),
			'object_cleanup_count' => $this->getObjectCleanupCount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_one_time_cleanup' => $this->getIsOneTimeCleanup()
		);
	}

}
?>