<?php

class CBaseContractPropertyPricing extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.contract_property_pricings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPsProductId;
	protected $m_intContractPropertyId;
	protected $m_intContractDocumentId;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_fltPrice;
	protected $m_fltMonthlyChangeAmount;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsSuspended;
	protected $m_boolIsRealigned;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsMigrated;
	protected $m_intNewSpawnedContractPropertyId;
	protected $m_boolIsDiscountUpdate;

	public function __construct() {
		parent::__construct();

		$this->m_fltPrice = '0';
		$this->m_fltMonthlyChangeAmount = '0';
		$this->m_boolIsSuspended = false;
		$this->m_boolIsRealigned = false;
		$this->m_boolIsMigrated = false;
		$this->m_boolIsDiscountUpdate = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intContractPropertyId', trim( $arrValues['contract_property_id'] ) ); elseif( isset( $arrValues['contract_property_id'] ) ) $this->setContractPropertyId( $arrValues['contract_property_id'] );
		if( isset( $arrValues['contract_document_id'] ) && $boolDirectSet ) $this->set( 'm_intContractDocumentId', trim( $arrValues['contract_document_id'] ) ); elseif( isset( $arrValues['contract_document_id'] ) ) $this->setContractDocumentId( $arrValues['contract_document_id'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['price'] ) && $boolDirectSet ) $this->set( 'm_fltPrice', trim( $arrValues['price'] ) ); elseif( isset( $arrValues['price'] ) ) $this->setPrice( $arrValues['price'] );
		if( isset( $arrValues['monthly_change_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyChangeAmount', trim( $arrValues['monthly_change_amount'] ) ); elseif( isset( $arrValues['monthly_change_amount'] ) ) $this->setMonthlyChangeAmount( $arrValues['monthly_change_amount'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_suspended'] ) && $boolDirectSet ) $this->set( 'm_boolIsSuspended', trim( stripcslashes( $arrValues['is_suspended'] ) ) ); elseif( isset( $arrValues['is_suspended'] ) ) $this->setIsSuspended( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_suspended'] ) : $arrValues['is_suspended'] );
		if( isset( $arrValues['is_realigned'] ) && $boolDirectSet ) $this->set( 'm_boolIsRealigned', trim( stripcslashes( $arrValues['is_realigned'] ) ) ); elseif( isset( $arrValues['is_realigned'] ) ) $this->setIsRealigned( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_realigned'] ) : $arrValues['is_realigned'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_migrated'] ) && $boolDirectSet ) $this->set( 'm_boolIsMigrated', trim( stripcslashes( $arrValues['is_migrated'] ) ) ); elseif( isset( $arrValues['is_migrated'] ) ) $this->setIsMigrated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_migrated'] ) : $arrValues['is_migrated'] );
		if( isset( $arrValues['new_spawned_contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intNewSpawnedContractPropertyId', trim( $arrValues['new_spawned_contract_property_id'] ) ); elseif( isset( $arrValues['new_spawned_contract_property_id'] ) ) $this->setNewSpawnedContractPropertyId( $arrValues['new_spawned_contract_property_id'] );
		if( isset( $arrValues['is_discount_update'] ) && $boolDirectSet ) $this->set( 'm_boolIsDiscountUpdate', trim( stripcslashes( $arrValues['is_discount_update'] ) ) ); elseif( isset( $arrValues['is_discount_update'] ) ) $this->setIsDiscountUpdate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_discount_update'] ) : $arrValues['is_discount_update'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setContractPropertyId( $intContractPropertyId ) {
		$this->set( 'm_intContractPropertyId', CStrings::strToIntDef( $intContractPropertyId, NULL, false ) );
	}

	public function getContractPropertyId() {
		return $this->m_intContractPropertyId;
	}

	public function sqlContractPropertyId() {
		return ( true == isset( $this->m_intContractPropertyId ) ) ? ( string ) $this->m_intContractPropertyId : 'NULL';
	}

	public function setContractDocumentId( $intContractDocumentId ) {
		$this->set( 'm_intContractDocumentId', CStrings::strToIntDef( $intContractDocumentId, NULL, false ) );
	}

	public function getContractDocumentId() {
		return $this->m_intContractDocumentId;
	}

	public function sqlContractDocumentId() {
		return ( true == isset( $this->m_intContractDocumentId ) ) ? ( string ) $this->m_intContractDocumentId : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setPrice( $fltPrice ) {
		$this->set( 'm_fltPrice', CStrings::strToFloatDef( $fltPrice, NULL, false, 2 ) );
	}

	public function getPrice() {
		return $this->m_fltPrice;
	}

	public function sqlPrice() {
		return ( true == isset( $this->m_fltPrice ) ) ? ( string ) $this->m_fltPrice : '0';
	}

	public function setMonthlyChangeAmount( $fltMonthlyChangeAmount ) {
		$this->set( 'm_fltMonthlyChangeAmount', CStrings::strToFloatDef( $fltMonthlyChangeAmount, NULL, false, 2 ) );
	}

	public function getMonthlyChangeAmount() {
		return $this->m_fltMonthlyChangeAmount;
	}

	public function sqlMonthlyChangeAmount() {
		return ( true == isset( $this->m_fltMonthlyChangeAmount ) ) ? ( string ) $this->m_fltMonthlyChangeAmount : '0';
	}

	public function setIsSuspended( $boolIsSuspended ) {
		$this->set( 'm_boolIsSuspended', CStrings::strToBool( $boolIsSuspended ) );
	}

	public function getIsSuspended() {
		return $this->m_boolIsSuspended;
	}

	public function sqlIsSuspended() {
		return ( true == isset( $this->m_boolIsSuspended ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSuspended ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsRealigned( $boolIsRealigned ) {
		$this->set( 'm_boolIsRealigned', CStrings::strToBool( $boolIsRealigned ) );
	}

	public function getIsRealigned() {
		return $this->m_boolIsRealigned;
	}

	public function sqlIsRealigned() {
		return ( true == isset( $this->m_boolIsRealigned ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRealigned ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsMigrated( $boolIsMigrated ) {
		$this->set( 'm_boolIsMigrated', CStrings::strToBool( $boolIsMigrated ) );
	}

	public function getIsMigrated() {
		return $this->m_boolIsMigrated;
	}

	public function sqlIsMigrated() {
		return ( true == isset( $this->m_boolIsMigrated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMigrated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setNewSpawnedContractPropertyId( $intNewSpawnedContractPropertyId ) {
		$this->set( 'm_intNewSpawnedContractPropertyId', CStrings::strToIntDef( $intNewSpawnedContractPropertyId, NULL, false ) );
	}

	public function getNewSpawnedContractPropertyId() {
		return $this->m_intNewSpawnedContractPropertyId;
	}

	public function sqlNewSpawnedContractPropertyId() {
		return ( true == isset( $this->m_intNewSpawnedContractPropertyId ) ) ? ( string ) $this->m_intNewSpawnedContractPropertyId : 'NULL';
	}

	public function setIsDiscountUpdate( $boolIsDiscountUpdate ) {
		$this->set( 'm_boolIsDiscountUpdate', CStrings::strToBool( $boolIsDiscountUpdate ) );
	}

	public function getIsDiscountUpdate() {
		return $this->m_boolIsDiscountUpdate;
	}

	public function sqlIsDiscountUpdate() {
		return ( true == isset( $this->m_boolIsDiscountUpdate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDiscountUpdate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, ps_product_id, contract_property_id, contract_document_id, start_date, end_date, price, monthly_change_amount, details, is_suspended, is_realigned, updated_by, updated_on, created_by, created_on, is_migrated, new_spawned_contract_property_id, is_discount_update )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlContractPropertyId() . ', ' .
						$this->sqlContractDocumentId() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlPrice() . ', ' .
						$this->sqlMonthlyChangeAmount() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsSuspended() . ', ' .
						$this->sqlIsRealigned() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsMigrated() . ', ' .
						$this->sqlNewSpawnedContractPropertyId() . ', ' .
						$this->sqlIsDiscountUpdate() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_property_id = ' . $this->sqlContractPropertyId(). ',' ; } elseif( true == array_key_exists( 'ContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' contract_property_id = ' . $this->sqlContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_document_id = ' . $this->sqlContractDocumentId(). ',' ; } elseif( true == array_key_exists( 'ContractDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' contract_document_id = ' . $this->sqlContractDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' price = ' . $this->sqlPrice(). ',' ; } elseif( true == array_key_exists( 'Price', $this->getChangedColumns() ) ) { $strSql .= ' price = ' . $this->sqlPrice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_change_amount = ' . $this->sqlMonthlyChangeAmount(). ',' ; } elseif( true == array_key_exists( 'MonthlyChangeAmount', $this->getChangedColumns() ) ) { $strSql .= ' monthly_change_amount = ' . $this->sqlMonthlyChangeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_suspended = ' . $this->sqlIsSuspended(). ',' ; } elseif( true == array_key_exists( 'IsSuspended', $this->getChangedColumns() ) ) { $strSql .= ' is_suspended = ' . $this->sqlIsSuspended() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_realigned = ' . $this->sqlIsRealigned(). ',' ; } elseif( true == array_key_exists( 'IsRealigned', $this->getChangedColumns() ) ) { $strSql .= ' is_realigned = ' . $this->sqlIsRealigned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_migrated = ' . $this->sqlIsMigrated(). ',' ; } elseif( true == array_key_exists( 'IsMigrated', $this->getChangedColumns() ) ) { $strSql .= ' is_migrated = ' . $this->sqlIsMigrated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_spawned_contract_property_id = ' . $this->sqlNewSpawnedContractPropertyId(). ',' ; } elseif( true == array_key_exists( 'NewSpawnedContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' new_spawned_contract_property_id = ' . $this->sqlNewSpawnedContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_discount_update = ' . $this->sqlIsDiscountUpdate(). ',' ; } elseif( true == array_key_exists( 'IsDiscountUpdate', $this->getChangedColumns() ) ) { $strSql .= ' is_discount_update = ' . $this->sqlIsDiscountUpdate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'ps_product_id' => $this->getPsProductId(),
			'contract_property_id' => $this->getContractPropertyId(),
			'contract_document_id' => $this->getContractDocumentId(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'price' => $this->getPrice(),
			'monthly_change_amount' => $this->getMonthlyChangeAmount(),
			'details' => $this->getDetails(),
			'is_suspended' => $this->getIsSuspended(),
			'is_realigned' => $this->getIsRealigned(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_migrated' => $this->getIsMigrated(),
			'new_spawned_contract_property_id' => $this->getNewSpawnedContractPropertyId(),
			'is_discount_update' => $this->getIsDiscountUpdate()
		);
	}

}
?>