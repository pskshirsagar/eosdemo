<?php

class CBaseWorkbookCategory extends CEosSingularBase {

	const TABLE_NAME = 'public.workbook_categories';

	protected $m_intId;
	protected $m_intWorkbookTemplateId;
	protected $m_intWorkbookStepId;
	protected $m_intSectionTypeId;
	protected $m_arrintOccupancyTypeIds;
	protected $m_strName;
	protected $m_intTotalColumns;
	protected $m_strCategoryDetails;
	protected $m_jsonCategoryDetails;
	protected $m_strHelperText;
	protected $m_strHelperLink;
	protected $m_intOrderNumber;
	protected $m_boolIsComment;
	protected $m_boolIsEditable;
	protected $m_boolIsPublished;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsComment = false;
		$this->m_boolIsEditable = false;
		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['workbook_template_id'] ) && $boolDirectSet ) $this->set( 'm_intWorkbookTemplateId', trim( $arrValues['workbook_template_id'] ) ); elseif( isset( $arrValues['workbook_template_id'] ) ) $this->setWorkbookTemplateId( $arrValues['workbook_template_id'] );
		if( isset( $arrValues['workbook_step_id'] ) && $boolDirectSet ) $this->set( 'm_intWorkbookStepId', trim( $arrValues['workbook_step_id'] ) ); elseif( isset( $arrValues['workbook_step_id'] ) ) $this->setWorkbookStepId( $arrValues['workbook_step_id'] );
		if( isset( $arrValues['section_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSectionTypeId', trim( $arrValues['section_type_id'] ) ); elseif( isset( $arrValues['section_type_id'] ) ) $this->setSectionTypeId( $arrValues['section_type_id'] );
		if( isset( $arrValues['occupancy_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintOccupancyTypeIds', trim( $arrValues['occupancy_type_ids'] ) ); elseif( isset( $arrValues['occupancy_type_ids'] ) ) $this->setOccupancyTypeIds( $arrValues['occupancy_type_ids'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['total_columns'] ) && $boolDirectSet ) $this->set( 'm_intTotalColumns', trim( $arrValues['total_columns'] ) ); elseif( isset( $arrValues['total_columns'] ) ) $this->setTotalColumns( $arrValues['total_columns'] );
		if( isset( $arrValues['category_details'] ) ) $this->set( 'm_strCategoryDetails', trim( $arrValues['category_details'] ) );
		if( isset( $arrValues['helper_text'] ) && $boolDirectSet ) $this->set( 'm_strHelperText', trim( stripcslashes( $arrValues['helper_text'] ) ) ); elseif( isset( $arrValues['helper_text'] ) ) $this->setHelperText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['helper_text'] ) : $arrValues['helper_text'] );
		if( isset( $arrValues['helper_link'] ) && $boolDirectSet ) $this->set( 'm_strHelperLink', trim( stripcslashes( $arrValues['helper_link'] ) ) ); elseif( isset( $arrValues['helper_link'] ) ) $this->setHelperLink( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['helper_link'] ) : $arrValues['helper_link'] );
		if( isset( $arrValues['order_number'] ) && $boolDirectSet ) $this->set( 'm_intOrderNumber', trim( $arrValues['order_number'] ) ); elseif( isset( $arrValues['order_number'] ) ) $this->setOrderNumber( $arrValues['order_number'] );
		if( isset( $arrValues['is_comment'] ) && $boolDirectSet ) $this->set( 'm_boolIsComment', trim( stripcslashes( $arrValues['is_comment'] ) ) ); elseif( isset( $arrValues['is_comment'] ) ) $this->setIsComment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_comment'] ) : $arrValues['is_comment'] );
		if( isset( $arrValues['is_editable'] ) && $boolDirectSet ) $this->set( 'm_boolIsEditable', trim( stripcslashes( $arrValues['is_editable'] ) ) ); elseif( isset( $arrValues['is_editable'] ) ) $this->setIsEditable( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_editable'] ) : $arrValues['is_editable'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setWorkbookTemplateId( $intWorkbookTemplateId ) {
		$this->set( 'm_intWorkbookTemplateId', CStrings::strToIntDef( $intWorkbookTemplateId, NULL, false ) );
	}

	public function getWorkbookTemplateId() {
		return $this->m_intWorkbookTemplateId;
	}

	public function sqlWorkbookTemplateId() {
		return ( true == isset( $this->m_intWorkbookTemplateId ) ) ? ( string ) $this->m_intWorkbookTemplateId : 'NULL';
	}

	public function setWorkbookStepId( $intWorkbookStepId ) {
		$this->set( 'm_intWorkbookStepId', CStrings::strToIntDef( $intWorkbookStepId, NULL, false ) );
	}

	public function getWorkbookStepId() {
		return $this->m_intWorkbookStepId;
	}

	public function sqlWorkbookStepId() {
		return ( true == isset( $this->m_intWorkbookStepId ) ) ? ( string ) $this->m_intWorkbookStepId : 'NULL';
	}

	public function setSectionTypeId( $intSectionTypeId ) {
		$this->set( 'm_intSectionTypeId', CStrings::strToIntDef( $intSectionTypeId, NULL, false ) );
	}

	public function getSectionTypeId() {
		return $this->m_intSectionTypeId;
	}

	public function sqlSectionTypeId() {
		return ( true == isset( $this->m_intSectionTypeId ) ) ? ( string ) $this->m_intSectionTypeId : 'NULL';
	}

	public function setOccupancyTypeIds( $arrintOccupancyTypeIds ) {
		$this->set( 'm_arrintOccupancyTypeIds', CStrings::strToArrIntDef( $arrintOccupancyTypeIds, NULL ) );
	}

	public function getOccupancyTypeIds() {
		return $this->m_arrintOccupancyTypeIds;
	}

	public function sqlOccupancyTypeIds() {
		return ( true == isset( $this->m_arrintOccupancyTypeIds ) && true == valArr( $this->m_arrintOccupancyTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintOccupancyTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setTotalColumns( $intTotalColumns ) {
		$this->set( 'm_intTotalColumns', CStrings::strToIntDef( $intTotalColumns, NULL, false ) );
	}

	public function getTotalColumns() {
		return $this->m_intTotalColumns;
	}

	public function sqlTotalColumns() {
		return ( true == isset( $this->m_intTotalColumns ) ) ? ( string ) $this->m_intTotalColumns : 'NULL';
	}

	public function setCategoryDetails( $jsonCategoryDetails ) {
		if( true == valObj( $jsonCategoryDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonCategoryDetails', $jsonCategoryDetails );
		} elseif( true == valJsonString( $jsonCategoryDetails ) ) {
			$this->set( 'm_jsonCategoryDetails', CStrings::strToJson( $jsonCategoryDetails ) );
		} else {
			$this->set( 'm_jsonCategoryDetails', NULL ); 
		}
		unset( $this->m_strCategoryDetails );
	}

	public function getCategoryDetails() {
		if( true == isset( $this->m_strCategoryDetails ) ) {
			$this->m_jsonCategoryDetails = CStrings::strToJson( $this->m_strCategoryDetails );
			unset( $this->m_strCategoryDetails );
		}
		return $this->m_jsonCategoryDetails;
	}

	public function sqlCategoryDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getCategoryDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getCategoryDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setHelperText( $strHelperText ) {
		$this->set( 'm_strHelperText', CStrings::strTrimDef( $strHelperText, 500, NULL, true ) );
	}

	public function getHelperText() {
		return $this->m_strHelperText;
	}

	public function sqlHelperText() {
		return ( true == isset( $this->m_strHelperText ) ) ? '\'' . addslashes( $this->m_strHelperText ) . '\'' : 'NULL';
	}

	public function setHelperLink( $strHelperLink ) {
		$this->set( 'm_strHelperLink', CStrings::strTrimDef( $strHelperLink, 500, NULL, true ) );
	}

	public function getHelperLink() {
		return $this->m_strHelperLink;
	}

	public function sqlHelperLink() {
		return ( true == isset( $this->m_strHelperLink ) ) ? '\'' . addslashes( $this->m_strHelperLink ) . '\'' : 'NULL';
	}

	public function setOrderNumber( $intOrderNumber ) {
		$this->set( 'm_intOrderNumber', CStrings::strToIntDef( $intOrderNumber, NULL, false ) );
	}

	public function getOrderNumber() {
		return $this->m_intOrderNumber;
	}

	public function sqlOrderNumber() {
		return ( true == isset( $this->m_intOrderNumber ) ) ? ( string ) $this->m_intOrderNumber : 'NULL';
	}

	public function setIsComment( $boolIsComment ) {
		$this->set( 'm_boolIsComment', CStrings::strToBool( $boolIsComment ) );
	}

	public function getIsComment() {
		return $this->m_boolIsComment;
	}

	public function sqlIsComment() {
		return ( true == isset( $this->m_boolIsComment ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsComment ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsEditable( $boolIsEditable ) {
		$this->set( 'm_boolIsEditable', CStrings::strToBool( $boolIsEditable ) );
	}

	public function getIsEditable() {
		return $this->m_boolIsEditable;
	}

	public function sqlIsEditable() {
		return ( true == isset( $this->m_boolIsEditable ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEditable ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, workbook_template_id, workbook_step_id, section_type_id, occupancy_type_ids, name, total_columns, category_details, helper_text, helper_link, order_number, is_comment, is_editable, is_published, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlWorkbookTemplateId() . ', ' .
						$this->sqlWorkbookStepId() . ', ' .
						$this->sqlSectionTypeId() . ', ' .
						$this->sqlOccupancyTypeIds() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlTotalColumns() . ', ' .
						$this->sqlCategoryDetails() . ', ' .
						$this->sqlHelperText() . ', ' .
						$this->sqlHelperLink() . ', ' .
						$this->sqlOrderNumber() . ', ' .
						$this->sqlIsComment() . ', ' .
						$this->sqlIsEditable() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' workbook_template_id = ' . $this->sqlWorkbookTemplateId(). ',' ; } elseif( true == array_key_exists( 'WorkbookTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' workbook_template_id = ' . $this->sqlWorkbookTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' workbook_step_id = ' . $this->sqlWorkbookStepId(). ',' ; } elseif( true == array_key_exists( 'WorkbookStepId', $this->getChangedColumns() ) ) { $strSql .= ' workbook_step_id = ' . $this->sqlWorkbookStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' section_type_id = ' . $this->sqlSectionTypeId(). ',' ; } elseif( true == array_key_exists( 'SectionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' section_type_id = ' . $this->sqlSectionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupancy_type_ids = ' . $this->sqlOccupancyTypeIds(). ',' ; } elseif( true == array_key_exists( 'OccupancyTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' occupancy_type_ids = ' . $this->sqlOccupancyTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_columns = ' . $this->sqlTotalColumns(). ',' ; } elseif( true == array_key_exists( 'TotalColumns', $this->getChangedColumns() ) ) { $strSql .= ' total_columns = ' . $this->sqlTotalColumns() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' category_details = ' . $this->sqlCategoryDetails(). ',' ; } elseif( true == array_key_exists( 'CategoryDetails', $this->getChangedColumns() ) ) { $strSql .= ' category_details = ' . $this->sqlCategoryDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' helper_text = ' . $this->sqlHelperText(). ',' ; } elseif( true == array_key_exists( 'HelperText', $this->getChangedColumns() ) ) { $strSql .= ' helper_text = ' . $this->sqlHelperText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' helper_link = ' . $this->sqlHelperLink(). ',' ; } elseif( true == array_key_exists( 'HelperLink', $this->getChangedColumns() ) ) { $strSql .= ' helper_link = ' . $this->sqlHelperLink() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_number = ' . $this->sqlOrderNumber(). ',' ; } elseif( true == array_key_exists( 'OrderNumber', $this->getChangedColumns() ) ) { $strSql .= ' order_number = ' . $this->sqlOrderNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_comment = ' . $this->sqlIsComment(). ',' ; } elseif( true == array_key_exists( 'IsComment', $this->getChangedColumns() ) ) { $strSql .= ' is_comment = ' . $this->sqlIsComment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_editable = ' . $this->sqlIsEditable(). ',' ; } elseif( true == array_key_exists( 'IsEditable', $this->getChangedColumns() ) ) { $strSql .= ' is_editable = ' . $this->sqlIsEditable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'workbook_template_id' => $this->getWorkbookTemplateId(),
			'workbook_step_id' => $this->getWorkbookStepId(),
			'section_type_id' => $this->getSectionTypeId(),
			'occupancy_type_ids' => $this->getOccupancyTypeIds(),
			'name' => $this->getName(),
			'total_columns' => $this->getTotalColumns(),
			'category_details' => $this->getCategoryDetails(),
			'helper_text' => $this->getHelperText(),
			'helper_link' => $this->getHelperLink(),
			'order_number' => $this->getOrderNumber(),
			'is_comment' => $this->getIsComment(),
			'is_editable' => $this->getIsEditable(),
			'is_published' => $this->getIsPublished(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>