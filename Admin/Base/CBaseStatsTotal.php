<?php

class CBaseStatsTotal extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_totals';

	protected $m_intId;
	protected $m_strMonth;
	protected $m_intNewClients;
	protected $m_intLostClients;
	protected $m_intNetClientChange;
	protected $m_intCumulativeClients;
	protected $m_intSubscriptionRevenue;
	protected $m_intTransactionRevenue;
	protected $m_intOneTimeRevenue;
	protected $m_intWriteOffRevenue;
	protected $m_intTotalRevenue;
	protected $m_fltQuarterRevenueGrowthPercent;
	protected $m_intLostRevenueUnimplemented;
	protected $m_intLostRevenueUnhappy;
	protected $m_intLostRevenueManagement;
	protected $m_intLostRevenueTransferProperty;
	protected $m_intTotalLostRevenue;
	protected $m_intNewLogoUnits;
	protected $m_intNewProductUnits;
	protected $m_intNewPropertyUnits;
	protected $m_intNewTransferPropertyUnits;
	protected $m_intTotalNewUnits;
	protected $m_intLostUnitsUnhappy;
	protected $m_intLostUnitsManagement;
	protected $m_intLostUnitsUnimplemented;
	protected $m_intLostUnitsTransferProperty;
	protected $m_intTotalLostUnits;
	protected $m_intNetUnitChange;
	protected $m_intCumulativeUnits;
	protected $m_fltRevenuePerUnit;
	protected $m_intNewLogoSubscriptionAcv;
	protected $m_intNewProductSubscriptionAcv;
	protected $m_intNewPropertySubscriptionAcv;
	protected $m_intNewRenewalSubscriptionAcv;
	protected $m_intNewTransferPropertySubscriptionAcv;
	protected $m_intTotalNewSubscriptionAcv;
	protected $m_intTotalNewActiveSubscriptionAcv;
	protected $m_intTotalNewAdjustedSubscriptionAcv;
	protected $m_intTotalNewActiveAdjustedSubscriptionAcv;
	protected $m_intNewLogoTransactionAcv;
	protected $m_intNewProductTransactionAcv;
	protected $m_intNewPropertyTransactionAcv;
	protected $m_intNewRenewalTransactionAcv;
	protected $m_intNewTransferPropertyTransactionAcv;
	protected $m_intTotalNewTransactionAcv;
	protected $m_intTotalNewActiveTransactionAcv;
	protected $m_intTotalNewAdjustedTransactionAcv;
	protected $m_intTotalNewActiveAdjustedTransactionAcv;
	protected $m_intTotalNewAcv;
	protected $m_intTotalNewActiveAcv;
	protected $m_intLostSubscriptionAcvUnhappy;
	protected $m_intLostSubscriptionAcvUnimplemented;
	protected $m_intLostSubscriptionAcvTransferProperty;
	protected $m_intLostSubscriptionAcvManagement;
	protected $m_intTotalLostSubscriptionAcv;
	protected $m_intLostTransactionAcvUnhappy;
	protected $m_intLostTransactionAcvUnimplemented;
	protected $m_intLostTransactionAcvTransferProperty;
	protected $m_intLostTransactionAcvManagement;
	protected $m_intTotalLostTransactionAcv;
	protected $m_intTotalLostAcv;
	protected $m_intLostAcvUnhappy;
	protected $m_intLostAcvUnimplemented;
	protected $m_intLostAcvTransferProperty;
	protected $m_intLostAcvManagement;
	protected $m_intNetAcvChange;
	protected $m_intCumulativeAcv;
	protected $m_fltMcvPerUnit;
	protected $m_intNewLogoImplementation;
	protected $m_intNewProductImplementation;
	protected $m_intNewPropertyImplementation;
	protected $m_intNewRenewalImplementation;
	protected $m_intNewTransferPropertyImplementation;
	protected $m_intPilotNewSubscriptionAcv;
	protected $m_intPilotNewTransactionAcv;
	protected $m_intPilotPendingSubscriptionAcv;
	protected $m_intPilotPendingTransactionAcv;
	protected $m_intPilotWonSubscriptionAcv;
	protected $m_intPilotWonTransactionAcv;
	protected $m_intPilotLostSubscriptionAcv;
	protected $m_intPilotLostTransactionAcv;
	protected $m_intPilotOpenCount;
	protected $m_intUnimplementedOpenDays;
	protected $m_intUnimplementedSubscriptionAcv;
	protected $m_intUnimplementedTransactionAcv;
	protected $m_intUnimplementedAcv;
	protected $m_intUnimplementedUnits;
	protected $m_intImplementedSubscriptionAcv;
	protected $m_intImplementedTransactionAcv;
	protected $m_intImplementedAcv;
	protected $m_intImplementedUnits;
	protected $m_intUnbilledOpenDays;
	protected $m_intUnbilledAcv;
	protected $m_intUnbilledUnits;
	protected $m_intBilledAcv;
	protected $m_intBilledUnits;
	protected $m_intDemosScheduled;
	protected $m_intDemoedUnits;
	protected $m_intDemosCompleted;
	protected $m_intDemosForgotten;
	protected $m_intCommissionsScheduled;
	protected $m_intCommissionsPaid;
	protected $m_intPipelineNewLeads;
	protected $m_intPipelineLeads;
	protected $m_intPipelineWonLeads;
	protected $m_intPipelineLostLeads;
	protected $m_intPipelineNewUnits;
	protected $m_intPipelineUnits;
	protected $m_intPipelineWonUnits;
	protected $m_intPipelineLostUnits;
	protected $m_intPipelineNewAcv;
	protected $m_intPipelineAcv;
	protected $m_intPipelineWonAcv;
	protected $m_intPipelineLostAcv;
	protected $m_intPipelineNewProposals;
	protected $m_intCommittedAcv;
	protected $m_intNewBugSeverity;
	protected $m_intNewBugs;
	protected $m_intClosedBugs;
	protected $m_intOpenBugs;
	protected $m_intNewFeatureRequests;
	protected $m_intClosedFeatureRequests;
	protected $m_intOpenFeatureRequests;
	protected $m_intNewSupportTickets;
	protected $m_intClosedSupportTickets;
	protected $m_intOpenSupportTickets;
	protected $m_intClientImpactfulReleasedTasks;
	protected $m_intReleasedTasksWithoutStoryPoints;
	protected $m_intStoryPointsReleased;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['new_clients'] ) && $boolDirectSet ) $this->set( 'm_intNewClients', trim( $arrValues['new_clients'] ) ); elseif( isset( $arrValues['new_clients'] ) ) $this->setNewClients( $arrValues['new_clients'] );
		if( isset( $arrValues['lost_clients'] ) && $boolDirectSet ) $this->set( 'm_intLostClients', trim( $arrValues['lost_clients'] ) ); elseif( isset( $arrValues['lost_clients'] ) ) $this->setLostClients( $arrValues['lost_clients'] );
		if( isset( $arrValues['net_client_change'] ) && $boolDirectSet ) $this->set( 'm_intNetClientChange', trim( $arrValues['net_client_change'] ) ); elseif( isset( $arrValues['net_client_change'] ) ) $this->setNetClientChange( $arrValues['net_client_change'] );
		if( isset( $arrValues['cumulative_clients'] ) && $boolDirectSet ) $this->set( 'm_intCumulativeClients', trim( $arrValues['cumulative_clients'] ) ); elseif( isset( $arrValues['cumulative_clients'] ) ) $this->setCumulativeClients( $arrValues['cumulative_clients'] );
		if( isset( $arrValues['subscription_revenue'] ) && $boolDirectSet ) $this->set( 'm_intSubscriptionRevenue', trim( $arrValues['subscription_revenue'] ) ); elseif( isset( $arrValues['subscription_revenue'] ) ) $this->setSubscriptionRevenue( $arrValues['subscription_revenue'] );
		if( isset( $arrValues['transaction_revenue'] ) && $boolDirectSet ) $this->set( 'm_intTransactionRevenue', trim( $arrValues['transaction_revenue'] ) ); elseif( isset( $arrValues['transaction_revenue'] ) ) $this->setTransactionRevenue( $arrValues['transaction_revenue'] );
		if( isset( $arrValues['one_time_revenue'] ) && $boolDirectSet ) $this->set( 'm_intOneTimeRevenue', trim( $arrValues['one_time_revenue'] ) ); elseif( isset( $arrValues['one_time_revenue'] ) ) $this->setOneTimeRevenue( $arrValues['one_time_revenue'] );
		if( isset( $arrValues['write_off_revenue'] ) && $boolDirectSet ) $this->set( 'm_intWriteOffRevenue', trim( $arrValues['write_off_revenue'] ) ); elseif( isset( $arrValues['write_off_revenue'] ) ) $this->setWriteOffRevenue( $arrValues['write_off_revenue'] );
		if( isset( $arrValues['total_revenue'] ) && $boolDirectSet ) $this->set( 'm_intTotalRevenue', trim( $arrValues['total_revenue'] ) ); elseif( isset( $arrValues['total_revenue'] ) ) $this->setTotalRevenue( $arrValues['total_revenue'] );
		if( isset( $arrValues['quarter_revenue_growth_percent'] ) && $boolDirectSet ) $this->set( 'm_fltQuarterRevenueGrowthPercent', trim( $arrValues['quarter_revenue_growth_percent'] ) ); elseif( isset( $arrValues['quarter_revenue_growth_percent'] ) ) $this->setQuarterRevenueGrowthPercent( $arrValues['quarter_revenue_growth_percent'] );
		if( isset( $arrValues['lost_revenue_unimplemented'] ) && $boolDirectSet ) $this->set( 'm_intLostRevenueUnimplemented', trim( $arrValues['lost_revenue_unimplemented'] ) ); elseif( isset( $arrValues['lost_revenue_unimplemented'] ) ) $this->setLostRevenueUnimplemented( $arrValues['lost_revenue_unimplemented'] );
		if( isset( $arrValues['lost_revenue_unhappy'] ) && $boolDirectSet ) $this->set( 'm_intLostRevenueUnhappy', trim( $arrValues['lost_revenue_unhappy'] ) ); elseif( isset( $arrValues['lost_revenue_unhappy'] ) ) $this->setLostRevenueUnhappy( $arrValues['lost_revenue_unhappy'] );
		if( isset( $arrValues['lost_revenue_management'] ) && $boolDirectSet ) $this->set( 'm_intLostRevenueManagement', trim( $arrValues['lost_revenue_management'] ) ); elseif( isset( $arrValues['lost_revenue_management'] ) ) $this->setLostRevenueManagement( $arrValues['lost_revenue_management'] );
		if( isset( $arrValues['lost_revenue_transfer_property'] ) && $boolDirectSet ) $this->set( 'm_intLostRevenueTransferProperty', trim( $arrValues['lost_revenue_transfer_property'] ) ); elseif( isset( $arrValues['lost_revenue_transfer_property'] ) ) $this->setLostRevenueTransferProperty( $arrValues['lost_revenue_transfer_property'] );
		if( isset( $arrValues['total_lost_revenue'] ) && $boolDirectSet ) $this->set( 'm_intTotalLostRevenue', trim( $arrValues['total_lost_revenue'] ) ); elseif( isset( $arrValues['total_lost_revenue'] ) ) $this->setTotalLostRevenue( $arrValues['total_lost_revenue'] );
		if( isset( $arrValues['new_logo_units'] ) && $boolDirectSet ) $this->set( 'm_intNewLogoUnits', trim( $arrValues['new_logo_units'] ) ); elseif( isset( $arrValues['new_logo_units'] ) ) $this->setNewLogoUnits( $arrValues['new_logo_units'] );
		if( isset( $arrValues['new_product_units'] ) && $boolDirectSet ) $this->set( 'm_intNewProductUnits', trim( $arrValues['new_product_units'] ) ); elseif( isset( $arrValues['new_product_units'] ) ) $this->setNewProductUnits( $arrValues['new_product_units'] );
		if( isset( $arrValues['new_property_units'] ) && $boolDirectSet ) $this->set( 'm_intNewPropertyUnits', trim( $arrValues['new_property_units'] ) ); elseif( isset( $arrValues['new_property_units'] ) ) $this->setNewPropertyUnits( $arrValues['new_property_units'] );
		if( isset( $arrValues['new_transfer_property_units'] ) && $boolDirectSet ) $this->set( 'm_intNewTransferPropertyUnits', trim( $arrValues['new_transfer_property_units'] ) ); elseif( isset( $arrValues['new_transfer_property_units'] ) ) $this->setNewTransferPropertyUnits( $arrValues['new_transfer_property_units'] );
		if( isset( $arrValues['total_new_units'] ) && $boolDirectSet ) $this->set( 'm_intTotalNewUnits', trim( $arrValues['total_new_units'] ) ); elseif( isset( $arrValues['total_new_units'] ) ) $this->setTotalNewUnits( $arrValues['total_new_units'] );
		if( isset( $arrValues['lost_units_unhappy'] ) && $boolDirectSet ) $this->set( 'm_intLostUnitsUnhappy', trim( $arrValues['lost_units_unhappy'] ) ); elseif( isset( $arrValues['lost_units_unhappy'] ) ) $this->setLostUnitsUnhappy( $arrValues['lost_units_unhappy'] );
		if( isset( $arrValues['lost_units_management'] ) && $boolDirectSet ) $this->set( 'm_intLostUnitsManagement', trim( $arrValues['lost_units_management'] ) ); elseif( isset( $arrValues['lost_units_management'] ) ) $this->setLostUnitsManagement( $arrValues['lost_units_management'] );
		if( isset( $arrValues['lost_units_unimplemented'] ) && $boolDirectSet ) $this->set( 'm_intLostUnitsUnimplemented', trim( $arrValues['lost_units_unimplemented'] ) ); elseif( isset( $arrValues['lost_units_unimplemented'] ) ) $this->setLostUnitsUnimplemented( $arrValues['lost_units_unimplemented'] );
		if( isset( $arrValues['lost_units_transfer_property'] ) && $boolDirectSet ) $this->set( 'm_intLostUnitsTransferProperty', trim( $arrValues['lost_units_transfer_property'] ) ); elseif( isset( $arrValues['lost_units_transfer_property'] ) ) $this->setLostUnitsTransferProperty( $arrValues['lost_units_transfer_property'] );
		if( isset( $arrValues['total_lost_units'] ) && $boolDirectSet ) $this->set( 'm_intTotalLostUnits', trim( $arrValues['total_lost_units'] ) ); elseif( isset( $arrValues['total_lost_units'] ) ) $this->setTotalLostUnits( $arrValues['total_lost_units'] );
		if( isset( $arrValues['net_unit_change'] ) && $boolDirectSet ) $this->set( 'm_intNetUnitChange', trim( $arrValues['net_unit_change'] ) ); elseif( isset( $arrValues['net_unit_change'] ) ) $this->setNetUnitChange( $arrValues['net_unit_change'] );
		if( isset( $arrValues['cumulative_units'] ) && $boolDirectSet ) $this->set( 'm_intCumulativeUnits', trim( $arrValues['cumulative_units'] ) ); elseif( isset( $arrValues['cumulative_units'] ) ) $this->setCumulativeUnits( $arrValues['cumulative_units'] );
		if( isset( $arrValues['revenue_per_unit'] ) && $boolDirectSet ) $this->set( 'm_fltRevenuePerUnit', trim( $arrValues['revenue_per_unit'] ) ); elseif( isset( $arrValues['revenue_per_unit'] ) ) $this->setRevenuePerUnit( $arrValues['revenue_per_unit'] );
		if( isset( $arrValues['new_logo_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewLogoSubscriptionAcv', trim( $arrValues['new_logo_subscription_acv'] ) ); elseif( isset( $arrValues['new_logo_subscription_acv'] ) ) $this->setNewLogoSubscriptionAcv( $arrValues['new_logo_subscription_acv'] );
		if( isset( $arrValues['new_product_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewProductSubscriptionAcv', trim( $arrValues['new_product_subscription_acv'] ) ); elseif( isset( $arrValues['new_product_subscription_acv'] ) ) $this->setNewProductSubscriptionAcv( $arrValues['new_product_subscription_acv'] );
		if( isset( $arrValues['new_property_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewPropertySubscriptionAcv', trim( $arrValues['new_property_subscription_acv'] ) ); elseif( isset( $arrValues['new_property_subscription_acv'] ) ) $this->setNewPropertySubscriptionAcv( $arrValues['new_property_subscription_acv'] );
		if( isset( $arrValues['new_renewal_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewRenewalSubscriptionAcv', trim( $arrValues['new_renewal_subscription_acv'] ) ); elseif( isset( $arrValues['new_renewal_subscription_acv'] ) ) $this->setNewRenewalSubscriptionAcv( $arrValues['new_renewal_subscription_acv'] );
		if( isset( $arrValues['new_transfer_property_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewTransferPropertySubscriptionAcv', trim( $arrValues['new_transfer_property_subscription_acv'] ) ); elseif( isset( $arrValues['new_transfer_property_subscription_acv'] ) ) $this->setNewTransferPropertySubscriptionAcv( $arrValues['new_transfer_property_subscription_acv'] );
		if( isset( $arrValues['total_new_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalNewSubscriptionAcv', trim( $arrValues['total_new_subscription_acv'] ) ); elseif( isset( $arrValues['total_new_subscription_acv'] ) ) $this->setTotalNewSubscriptionAcv( $arrValues['total_new_subscription_acv'] );
		if( isset( $arrValues['total_new_active_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalNewActiveSubscriptionAcv', trim( $arrValues['total_new_active_subscription_acv'] ) ); elseif( isset( $arrValues['total_new_active_subscription_acv'] ) ) $this->setTotalNewActiveSubscriptionAcv( $arrValues['total_new_active_subscription_acv'] );
		if( isset( $arrValues['total_new_adjusted_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalNewAdjustedSubscriptionAcv', trim( $arrValues['total_new_adjusted_subscription_acv'] ) ); elseif( isset( $arrValues['total_new_adjusted_subscription_acv'] ) ) $this->setTotalNewAdjustedSubscriptionAcv( $arrValues['total_new_adjusted_subscription_acv'] );
		if( isset( $arrValues['total_new_active_adjusted_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalNewActiveAdjustedSubscriptionAcv', trim( $arrValues['total_new_active_adjusted_subscription_acv'] ) ); elseif( isset( $arrValues['total_new_active_adjusted_subscription_acv'] ) ) $this->setTotalNewActiveAdjustedSubscriptionAcv( $arrValues['total_new_active_adjusted_subscription_acv'] );
		if( isset( $arrValues['new_logo_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewLogoTransactionAcv', trim( $arrValues['new_logo_transaction_acv'] ) ); elseif( isset( $arrValues['new_logo_transaction_acv'] ) ) $this->setNewLogoTransactionAcv( $arrValues['new_logo_transaction_acv'] );
		if( isset( $arrValues['new_product_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewProductTransactionAcv', trim( $arrValues['new_product_transaction_acv'] ) ); elseif( isset( $arrValues['new_product_transaction_acv'] ) ) $this->setNewProductTransactionAcv( $arrValues['new_product_transaction_acv'] );
		if( isset( $arrValues['new_property_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewPropertyTransactionAcv', trim( $arrValues['new_property_transaction_acv'] ) ); elseif( isset( $arrValues['new_property_transaction_acv'] ) ) $this->setNewPropertyTransactionAcv( $arrValues['new_property_transaction_acv'] );
		if( isset( $arrValues['new_renewal_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewRenewalTransactionAcv', trim( $arrValues['new_renewal_transaction_acv'] ) ); elseif( isset( $arrValues['new_renewal_transaction_acv'] ) ) $this->setNewRenewalTransactionAcv( $arrValues['new_renewal_transaction_acv'] );
		if( isset( $arrValues['new_transfer_property_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewTransferPropertyTransactionAcv', trim( $arrValues['new_transfer_property_transaction_acv'] ) ); elseif( isset( $arrValues['new_transfer_property_transaction_acv'] ) ) $this->setNewTransferPropertyTransactionAcv( $arrValues['new_transfer_property_transaction_acv'] );
		if( isset( $arrValues['total_new_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalNewTransactionAcv', trim( $arrValues['total_new_transaction_acv'] ) ); elseif( isset( $arrValues['total_new_transaction_acv'] ) ) $this->setTotalNewTransactionAcv( $arrValues['total_new_transaction_acv'] );
		if( isset( $arrValues['total_new_active_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalNewActiveTransactionAcv', trim( $arrValues['total_new_active_transaction_acv'] ) ); elseif( isset( $arrValues['total_new_active_transaction_acv'] ) ) $this->setTotalNewActiveTransactionAcv( $arrValues['total_new_active_transaction_acv'] );
		if( isset( $arrValues['total_new_adjusted_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalNewAdjustedTransactionAcv', trim( $arrValues['total_new_adjusted_transaction_acv'] ) ); elseif( isset( $arrValues['total_new_adjusted_transaction_acv'] ) ) $this->setTotalNewAdjustedTransactionAcv( $arrValues['total_new_adjusted_transaction_acv'] );
		if( isset( $arrValues['total_new_active_adjusted_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalNewActiveAdjustedTransactionAcv', trim( $arrValues['total_new_active_adjusted_transaction_acv'] ) ); elseif( isset( $arrValues['total_new_active_adjusted_transaction_acv'] ) ) $this->setTotalNewActiveAdjustedTransactionAcv( $arrValues['total_new_active_adjusted_transaction_acv'] );
		if( isset( $arrValues['total_new_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalNewAcv', trim( $arrValues['total_new_acv'] ) ); elseif( isset( $arrValues['total_new_acv'] ) ) $this->setTotalNewAcv( $arrValues['total_new_acv'] );
		if( isset( $arrValues['total_new_active_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalNewActiveAcv', trim( $arrValues['total_new_active_acv'] ) ); elseif( isset( $arrValues['total_new_active_acv'] ) ) $this->setTotalNewActiveAcv( $arrValues['total_new_active_acv'] );
		if( isset( $arrValues['lost_subscription_acv_unhappy'] ) && $boolDirectSet ) $this->set( 'm_intLostSubscriptionAcvUnhappy', trim( $arrValues['lost_subscription_acv_unhappy'] ) ); elseif( isset( $arrValues['lost_subscription_acv_unhappy'] ) ) $this->setLostSubscriptionAcvUnhappy( $arrValues['lost_subscription_acv_unhappy'] );
		if( isset( $arrValues['lost_subscription_acv_unimplemented'] ) && $boolDirectSet ) $this->set( 'm_intLostSubscriptionAcvUnimplemented', trim( $arrValues['lost_subscription_acv_unimplemented'] ) ); elseif( isset( $arrValues['lost_subscription_acv_unimplemented'] ) ) $this->setLostSubscriptionAcvUnimplemented( $arrValues['lost_subscription_acv_unimplemented'] );
		if( isset( $arrValues['lost_subscription_acv_transfer_property'] ) && $boolDirectSet ) $this->set( 'm_intLostSubscriptionAcvTransferProperty', trim( $arrValues['lost_subscription_acv_transfer_property'] ) ); elseif( isset( $arrValues['lost_subscription_acv_transfer_property'] ) ) $this->setLostSubscriptionAcvTransferProperty( $arrValues['lost_subscription_acv_transfer_property'] );
		if( isset( $arrValues['lost_subscription_acv_management'] ) && $boolDirectSet ) $this->set( 'm_intLostSubscriptionAcvManagement', trim( $arrValues['lost_subscription_acv_management'] ) ); elseif( isset( $arrValues['lost_subscription_acv_management'] ) ) $this->setLostSubscriptionAcvManagement( $arrValues['lost_subscription_acv_management'] );
		if( isset( $arrValues['total_lost_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalLostSubscriptionAcv', trim( $arrValues['total_lost_subscription_acv'] ) ); elseif( isset( $arrValues['total_lost_subscription_acv'] ) ) $this->setTotalLostSubscriptionAcv( $arrValues['total_lost_subscription_acv'] );
		if( isset( $arrValues['lost_transaction_acv_unhappy'] ) && $boolDirectSet ) $this->set( 'm_intLostTransactionAcvUnhappy', trim( $arrValues['lost_transaction_acv_unhappy'] ) ); elseif( isset( $arrValues['lost_transaction_acv_unhappy'] ) ) $this->setLostTransactionAcvUnhappy( $arrValues['lost_transaction_acv_unhappy'] );
		if( isset( $arrValues['lost_transaction_acv_unimplemented'] ) && $boolDirectSet ) $this->set( 'm_intLostTransactionAcvUnimplemented', trim( $arrValues['lost_transaction_acv_unimplemented'] ) ); elseif( isset( $arrValues['lost_transaction_acv_unimplemented'] ) ) $this->setLostTransactionAcvUnimplemented( $arrValues['lost_transaction_acv_unimplemented'] );
		if( isset( $arrValues['lost_transaction_acv_transfer_property'] ) && $boolDirectSet ) $this->set( 'm_intLostTransactionAcvTransferProperty', trim( $arrValues['lost_transaction_acv_transfer_property'] ) ); elseif( isset( $arrValues['lost_transaction_acv_transfer_property'] ) ) $this->setLostTransactionAcvTransferProperty( $arrValues['lost_transaction_acv_transfer_property'] );
		if( isset( $arrValues['lost_transaction_acv_management'] ) && $boolDirectSet ) $this->set( 'm_intLostTransactionAcvManagement', trim( $arrValues['lost_transaction_acv_management'] ) ); elseif( isset( $arrValues['lost_transaction_acv_management'] ) ) $this->setLostTransactionAcvManagement( $arrValues['lost_transaction_acv_management'] );
		if( isset( $arrValues['total_lost_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalLostTransactionAcv', trim( $arrValues['total_lost_transaction_acv'] ) ); elseif( isset( $arrValues['total_lost_transaction_acv'] ) ) $this->setTotalLostTransactionAcv( $arrValues['total_lost_transaction_acv'] );
		if( isset( $arrValues['total_lost_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalLostAcv', trim( $arrValues['total_lost_acv'] ) ); elseif( isset( $arrValues['total_lost_acv'] ) ) $this->setTotalLostAcv( $arrValues['total_lost_acv'] );
		if( isset( $arrValues['lost_acv_unhappy'] ) && $boolDirectSet ) $this->set( 'm_intLostAcvUnhappy', trim( $arrValues['lost_acv_unhappy'] ) ); elseif( isset( $arrValues['lost_acv_unhappy'] ) ) $this->setLostAcvUnhappy( $arrValues['lost_acv_unhappy'] );
		if( isset( $arrValues['lost_acv_unimplemented'] ) && $boolDirectSet ) $this->set( 'm_intLostAcvUnimplemented', trim( $arrValues['lost_acv_unimplemented'] ) ); elseif( isset( $arrValues['lost_acv_unimplemented'] ) ) $this->setLostAcvUnimplemented( $arrValues['lost_acv_unimplemented'] );
		if( isset( $arrValues['lost_acv_transfer_property'] ) && $boolDirectSet ) $this->set( 'm_intLostAcvTransferProperty', trim( $arrValues['lost_acv_transfer_property'] ) ); elseif( isset( $arrValues['lost_acv_transfer_property'] ) ) $this->setLostAcvTransferProperty( $arrValues['lost_acv_transfer_property'] );
		if( isset( $arrValues['lost_acv_management'] ) && $boolDirectSet ) $this->set( 'm_intLostAcvManagement', trim( $arrValues['lost_acv_management'] ) ); elseif( isset( $arrValues['lost_acv_management'] ) ) $this->setLostAcvManagement( $arrValues['lost_acv_management'] );
		if( isset( $arrValues['net_acv_change'] ) && $boolDirectSet ) $this->set( 'm_intNetAcvChange', trim( $arrValues['net_acv_change'] ) ); elseif( isset( $arrValues['net_acv_change'] ) ) $this->setNetAcvChange( $arrValues['net_acv_change'] );
		if( isset( $arrValues['cumulative_acv'] ) && $boolDirectSet ) $this->set( 'm_intCumulativeAcv', trim( $arrValues['cumulative_acv'] ) ); elseif( isset( $arrValues['cumulative_acv'] ) ) $this->setCumulativeAcv( $arrValues['cumulative_acv'] );
		if( isset( $arrValues['mcv_per_unit'] ) && $boolDirectSet ) $this->set( 'm_fltMcvPerUnit', trim( $arrValues['mcv_per_unit'] ) ); elseif( isset( $arrValues['mcv_per_unit'] ) ) $this->setMcvPerUnit( $arrValues['mcv_per_unit'] );
		if( isset( $arrValues['new_logo_implementation'] ) && $boolDirectSet ) $this->set( 'm_intNewLogoImplementation', trim( $arrValues['new_logo_implementation'] ) ); elseif( isset( $arrValues['new_logo_implementation'] ) ) $this->setNewLogoImplementation( $arrValues['new_logo_implementation'] );
		if( isset( $arrValues['new_product_implementation'] ) && $boolDirectSet ) $this->set( 'm_intNewProductImplementation', trim( $arrValues['new_product_implementation'] ) ); elseif( isset( $arrValues['new_product_implementation'] ) ) $this->setNewProductImplementation( $arrValues['new_product_implementation'] );
		if( isset( $arrValues['new_property_implementation'] ) && $boolDirectSet ) $this->set( 'm_intNewPropertyImplementation', trim( $arrValues['new_property_implementation'] ) ); elseif( isset( $arrValues['new_property_implementation'] ) ) $this->setNewPropertyImplementation( $arrValues['new_property_implementation'] );
		if( isset( $arrValues['new_renewal_implementation'] ) && $boolDirectSet ) $this->set( 'm_intNewRenewalImplementation', trim( $arrValues['new_renewal_implementation'] ) ); elseif( isset( $arrValues['new_renewal_implementation'] ) ) $this->setNewRenewalImplementation( $arrValues['new_renewal_implementation'] );
		if( isset( $arrValues['new_transfer_property_implementation'] ) && $boolDirectSet ) $this->set( 'm_intNewTransferPropertyImplementation', trim( $arrValues['new_transfer_property_implementation'] ) ); elseif( isset( $arrValues['new_transfer_property_implementation'] ) ) $this->setNewTransferPropertyImplementation( $arrValues['new_transfer_property_implementation'] );
		if( isset( $arrValues['pilot_new_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotNewSubscriptionAcv', trim( $arrValues['pilot_new_subscription_acv'] ) ); elseif( isset( $arrValues['pilot_new_subscription_acv'] ) ) $this->setPilotNewSubscriptionAcv( $arrValues['pilot_new_subscription_acv'] );
		if( isset( $arrValues['pilot_new_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotNewTransactionAcv', trim( $arrValues['pilot_new_transaction_acv'] ) ); elseif( isset( $arrValues['pilot_new_transaction_acv'] ) ) $this->setPilotNewTransactionAcv( $arrValues['pilot_new_transaction_acv'] );
		if( isset( $arrValues['pilot_pending_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotPendingSubscriptionAcv', trim( $arrValues['pilot_pending_subscription_acv'] ) ); elseif( isset( $arrValues['pilot_pending_subscription_acv'] ) ) $this->setPilotPendingSubscriptionAcv( $arrValues['pilot_pending_subscription_acv'] );
		if( isset( $arrValues['pilot_pending_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotPendingTransactionAcv', trim( $arrValues['pilot_pending_transaction_acv'] ) ); elseif( isset( $arrValues['pilot_pending_transaction_acv'] ) ) $this->setPilotPendingTransactionAcv( $arrValues['pilot_pending_transaction_acv'] );
		if( isset( $arrValues['pilot_won_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotWonSubscriptionAcv', trim( $arrValues['pilot_won_subscription_acv'] ) ); elseif( isset( $arrValues['pilot_won_subscription_acv'] ) ) $this->setPilotWonSubscriptionAcv( $arrValues['pilot_won_subscription_acv'] );
		if( isset( $arrValues['pilot_won_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotWonTransactionAcv', trim( $arrValues['pilot_won_transaction_acv'] ) ); elseif( isset( $arrValues['pilot_won_transaction_acv'] ) ) $this->setPilotWonTransactionAcv( $arrValues['pilot_won_transaction_acv'] );
		if( isset( $arrValues['pilot_lost_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotLostSubscriptionAcv', trim( $arrValues['pilot_lost_subscription_acv'] ) ); elseif( isset( $arrValues['pilot_lost_subscription_acv'] ) ) $this->setPilotLostSubscriptionAcv( $arrValues['pilot_lost_subscription_acv'] );
		if( isset( $arrValues['pilot_lost_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotLostTransactionAcv', trim( $arrValues['pilot_lost_transaction_acv'] ) ); elseif( isset( $arrValues['pilot_lost_transaction_acv'] ) ) $this->setPilotLostTransactionAcv( $arrValues['pilot_lost_transaction_acv'] );
		if( isset( $arrValues['pilot_open_count'] ) && $boolDirectSet ) $this->set( 'm_intPilotOpenCount', trim( $arrValues['pilot_open_count'] ) ); elseif( isset( $arrValues['pilot_open_count'] ) ) $this->setPilotOpenCount( $arrValues['pilot_open_count'] );
		if( isset( $arrValues['unimplemented_open_days'] ) && $boolDirectSet ) $this->set( 'm_intUnimplementedOpenDays', trim( $arrValues['unimplemented_open_days'] ) ); elseif( isset( $arrValues['unimplemented_open_days'] ) ) $this->setUnimplementedOpenDays( $arrValues['unimplemented_open_days'] );
		if( isset( $arrValues['unimplemented_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intUnimplementedSubscriptionAcv', trim( $arrValues['unimplemented_subscription_acv'] ) ); elseif( isset( $arrValues['unimplemented_subscription_acv'] ) ) $this->setUnimplementedSubscriptionAcv( $arrValues['unimplemented_subscription_acv'] );
		if( isset( $arrValues['unimplemented_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intUnimplementedTransactionAcv', trim( $arrValues['unimplemented_transaction_acv'] ) ); elseif( isset( $arrValues['unimplemented_transaction_acv'] ) ) $this->setUnimplementedTransactionAcv( $arrValues['unimplemented_transaction_acv'] );
		if( isset( $arrValues['unimplemented_acv'] ) && $boolDirectSet ) $this->set( 'm_intUnimplementedAcv', trim( $arrValues['unimplemented_acv'] ) ); elseif( isset( $arrValues['unimplemented_acv'] ) ) $this->setUnimplementedAcv( $arrValues['unimplemented_acv'] );
		if( isset( $arrValues['unimplemented_units'] ) && $boolDirectSet ) $this->set( 'm_intUnimplementedUnits', trim( $arrValues['unimplemented_units'] ) ); elseif( isset( $arrValues['unimplemented_units'] ) ) $this->setUnimplementedUnits( $arrValues['unimplemented_units'] );
		if( isset( $arrValues['implemented_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intImplementedSubscriptionAcv', trim( $arrValues['implemented_subscription_acv'] ) ); elseif( isset( $arrValues['implemented_subscription_acv'] ) ) $this->setImplementedSubscriptionAcv( $arrValues['implemented_subscription_acv'] );
		if( isset( $arrValues['implemented_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intImplementedTransactionAcv', trim( $arrValues['implemented_transaction_acv'] ) ); elseif( isset( $arrValues['implemented_transaction_acv'] ) ) $this->setImplementedTransactionAcv( $arrValues['implemented_transaction_acv'] );
		if( isset( $arrValues['implemented_acv'] ) && $boolDirectSet ) $this->set( 'm_intImplementedAcv', trim( $arrValues['implemented_acv'] ) ); elseif( isset( $arrValues['implemented_acv'] ) ) $this->setImplementedAcv( $arrValues['implemented_acv'] );
		if( isset( $arrValues['implemented_units'] ) && $boolDirectSet ) $this->set( 'm_intImplementedUnits', trim( $arrValues['implemented_units'] ) ); elseif( isset( $arrValues['implemented_units'] ) ) $this->setImplementedUnits( $arrValues['implemented_units'] );
		if( isset( $arrValues['unbilled_open_days'] ) && $boolDirectSet ) $this->set( 'm_intUnbilledOpenDays', trim( $arrValues['unbilled_open_days'] ) ); elseif( isset( $arrValues['unbilled_open_days'] ) ) $this->setUnbilledOpenDays( $arrValues['unbilled_open_days'] );
		if( isset( $arrValues['unbilled_acv'] ) && $boolDirectSet ) $this->set( 'm_intUnbilledAcv', trim( $arrValues['unbilled_acv'] ) ); elseif( isset( $arrValues['unbilled_acv'] ) ) $this->setUnbilledAcv( $arrValues['unbilled_acv'] );
		if( isset( $arrValues['unbilled_units'] ) && $boolDirectSet ) $this->set( 'm_intUnbilledUnits', trim( $arrValues['unbilled_units'] ) ); elseif( isset( $arrValues['unbilled_units'] ) ) $this->setUnbilledUnits( $arrValues['unbilled_units'] );
		if( isset( $arrValues['billed_acv'] ) && $boolDirectSet ) $this->set( 'm_intBilledAcv', trim( $arrValues['billed_acv'] ) ); elseif( isset( $arrValues['billed_acv'] ) ) $this->setBilledAcv( $arrValues['billed_acv'] );
		if( isset( $arrValues['billed_units'] ) && $boolDirectSet ) $this->set( 'm_intBilledUnits', trim( $arrValues['billed_units'] ) ); elseif( isset( $arrValues['billed_units'] ) ) $this->setBilledUnits( $arrValues['billed_units'] );
		if( isset( $arrValues['demos_scheduled'] ) && $boolDirectSet ) $this->set( 'm_intDemosScheduled', trim( $arrValues['demos_scheduled'] ) ); elseif( isset( $arrValues['demos_scheduled'] ) ) $this->setDemosScheduled( $arrValues['demos_scheduled'] );
		if( isset( $arrValues['demoed_units'] ) && $boolDirectSet ) $this->set( 'm_intDemoedUnits', trim( $arrValues['demoed_units'] ) ); elseif( isset( $arrValues['demoed_units'] ) ) $this->setDemoedUnits( $arrValues['demoed_units'] );
		if( isset( $arrValues['demos_completed'] ) && $boolDirectSet ) $this->set( 'm_intDemosCompleted', trim( $arrValues['demos_completed'] ) ); elseif( isset( $arrValues['demos_completed'] ) ) $this->setDemosCompleted( $arrValues['demos_completed'] );
		if( isset( $arrValues['demos_forgotten'] ) && $boolDirectSet ) $this->set( 'm_intDemosForgotten', trim( $arrValues['demos_forgotten'] ) ); elseif( isset( $arrValues['demos_forgotten'] ) ) $this->setDemosForgotten( $arrValues['demos_forgotten'] );
		if( isset( $arrValues['commissions_scheduled'] ) && $boolDirectSet ) $this->set( 'm_intCommissionsScheduled', trim( $arrValues['commissions_scheduled'] ) ); elseif( isset( $arrValues['commissions_scheduled'] ) ) $this->setCommissionsScheduled( $arrValues['commissions_scheduled'] );
		if( isset( $arrValues['commissions_paid'] ) && $boolDirectSet ) $this->set( 'm_intCommissionsPaid', trim( $arrValues['commissions_paid'] ) ); elseif( isset( $arrValues['commissions_paid'] ) ) $this->setCommissionsPaid( $arrValues['commissions_paid'] );
		if( isset( $arrValues['pipeline_new_leads'] ) && $boolDirectSet ) $this->set( 'm_intPipelineNewLeads', trim( $arrValues['pipeline_new_leads'] ) ); elseif( isset( $arrValues['pipeline_new_leads'] ) ) $this->setPipelineNewLeads( $arrValues['pipeline_new_leads'] );
		if( isset( $arrValues['pipeline_leads'] ) && $boolDirectSet ) $this->set( 'm_intPipelineLeads', trim( $arrValues['pipeline_leads'] ) ); elseif( isset( $arrValues['pipeline_leads'] ) ) $this->setPipelineLeads( $arrValues['pipeline_leads'] );
		if( isset( $arrValues['pipeline_won_leads'] ) && $boolDirectSet ) $this->set( 'm_intPipelineWonLeads', trim( $arrValues['pipeline_won_leads'] ) ); elseif( isset( $arrValues['pipeline_won_leads'] ) ) $this->setPipelineWonLeads( $arrValues['pipeline_won_leads'] );
		if( isset( $arrValues['pipeline_lost_leads'] ) && $boolDirectSet ) $this->set( 'm_intPipelineLostLeads', trim( $arrValues['pipeline_lost_leads'] ) ); elseif( isset( $arrValues['pipeline_lost_leads'] ) ) $this->setPipelineLostLeads( $arrValues['pipeline_lost_leads'] );
		if( isset( $arrValues['pipeline_new_units'] ) && $boolDirectSet ) $this->set( 'm_intPipelineNewUnits', trim( $arrValues['pipeline_new_units'] ) ); elseif( isset( $arrValues['pipeline_new_units'] ) ) $this->setPipelineNewUnits( $arrValues['pipeline_new_units'] );
		if( isset( $arrValues['pipeline_units'] ) && $boolDirectSet ) $this->set( 'm_intPipelineUnits', trim( $arrValues['pipeline_units'] ) ); elseif( isset( $arrValues['pipeline_units'] ) ) $this->setPipelineUnits( $arrValues['pipeline_units'] );
		if( isset( $arrValues['pipeline_won_units'] ) && $boolDirectSet ) $this->set( 'm_intPipelineWonUnits', trim( $arrValues['pipeline_won_units'] ) ); elseif( isset( $arrValues['pipeline_won_units'] ) ) $this->setPipelineWonUnits( $arrValues['pipeline_won_units'] );
		if( isset( $arrValues['pipeline_lost_units'] ) && $boolDirectSet ) $this->set( 'm_intPipelineLostUnits', trim( $arrValues['pipeline_lost_units'] ) ); elseif( isset( $arrValues['pipeline_lost_units'] ) ) $this->setPipelineLostUnits( $arrValues['pipeline_lost_units'] );
		if( isset( $arrValues['pipeline_new_acv'] ) && $boolDirectSet ) $this->set( 'm_intPipelineNewAcv', trim( $arrValues['pipeline_new_acv'] ) ); elseif( isset( $arrValues['pipeline_new_acv'] ) ) $this->setPipelineNewAcv( $arrValues['pipeline_new_acv'] );
		if( isset( $arrValues['pipeline_acv'] ) && $boolDirectSet ) $this->set( 'm_intPipelineAcv', trim( $arrValues['pipeline_acv'] ) ); elseif( isset( $arrValues['pipeline_acv'] ) ) $this->setPipelineAcv( $arrValues['pipeline_acv'] );
		if( isset( $arrValues['pipeline_won_acv'] ) && $boolDirectSet ) $this->set( 'm_intPipelineWonAcv', trim( $arrValues['pipeline_won_acv'] ) ); elseif( isset( $arrValues['pipeline_won_acv'] ) ) $this->setPipelineWonAcv( $arrValues['pipeline_won_acv'] );
		if( isset( $arrValues['pipeline_lost_acv'] ) && $boolDirectSet ) $this->set( 'm_intPipelineLostAcv', trim( $arrValues['pipeline_lost_acv'] ) ); elseif( isset( $arrValues['pipeline_lost_acv'] ) ) $this->setPipelineLostAcv( $arrValues['pipeline_lost_acv'] );
		if( isset( $arrValues['pipeline_new_proposals'] ) && $boolDirectSet ) $this->set( 'm_intPipelineNewProposals', trim( $arrValues['pipeline_new_proposals'] ) ); elseif( isset( $arrValues['pipeline_new_proposals'] ) ) $this->setPipelineNewProposals( $arrValues['pipeline_new_proposals'] );
		if( isset( $arrValues['committed_acv'] ) && $boolDirectSet ) $this->set( 'm_intCommittedAcv', trim( $arrValues['committed_acv'] ) ); elseif( isset( $arrValues['committed_acv'] ) ) $this->setCommittedAcv( $arrValues['committed_acv'] );
		if( isset( $arrValues['new_bug_severity'] ) && $boolDirectSet ) $this->set( 'm_intNewBugSeverity', trim( $arrValues['new_bug_severity'] ) ); elseif( isset( $arrValues['new_bug_severity'] ) ) $this->setNewBugSeverity( $arrValues['new_bug_severity'] );
		if( isset( $arrValues['new_bugs'] ) && $boolDirectSet ) $this->set( 'm_intNewBugs', trim( $arrValues['new_bugs'] ) ); elseif( isset( $arrValues['new_bugs'] ) ) $this->setNewBugs( $arrValues['new_bugs'] );
		if( isset( $arrValues['closed_bugs'] ) && $boolDirectSet ) $this->set( 'm_intClosedBugs', trim( $arrValues['closed_bugs'] ) ); elseif( isset( $arrValues['closed_bugs'] ) ) $this->setClosedBugs( $arrValues['closed_bugs'] );
		if( isset( $arrValues['open_bugs'] ) && $boolDirectSet ) $this->set( 'm_intOpenBugs', trim( $arrValues['open_bugs'] ) ); elseif( isset( $arrValues['open_bugs'] ) ) $this->setOpenBugs( $arrValues['open_bugs'] );
		if( isset( $arrValues['new_feature_requests'] ) && $boolDirectSet ) $this->set( 'm_intNewFeatureRequests', trim( $arrValues['new_feature_requests'] ) ); elseif( isset( $arrValues['new_feature_requests'] ) ) $this->setNewFeatureRequests( $arrValues['new_feature_requests'] );
		if( isset( $arrValues['closed_feature_requests'] ) && $boolDirectSet ) $this->set( 'm_intClosedFeatureRequests', trim( $arrValues['closed_feature_requests'] ) ); elseif( isset( $arrValues['closed_feature_requests'] ) ) $this->setClosedFeatureRequests( $arrValues['closed_feature_requests'] );
		if( isset( $arrValues['open_feature_requests'] ) && $boolDirectSet ) $this->set( 'm_intOpenFeatureRequests', trim( $arrValues['open_feature_requests'] ) ); elseif( isset( $arrValues['open_feature_requests'] ) ) $this->setOpenFeatureRequests( $arrValues['open_feature_requests'] );
		if( isset( $arrValues['new_support_tickets'] ) && $boolDirectSet ) $this->set( 'm_intNewSupportTickets', trim( $arrValues['new_support_tickets'] ) ); elseif( isset( $arrValues['new_support_tickets'] ) ) $this->setNewSupportTickets( $arrValues['new_support_tickets'] );
		if( isset( $arrValues['closed_support_tickets'] ) && $boolDirectSet ) $this->set( 'm_intClosedSupportTickets', trim( $arrValues['closed_support_tickets'] ) ); elseif( isset( $arrValues['closed_support_tickets'] ) ) $this->setClosedSupportTickets( $arrValues['closed_support_tickets'] );
		if( isset( $arrValues['open_support_tickets'] ) && $boolDirectSet ) $this->set( 'm_intOpenSupportTickets', trim( $arrValues['open_support_tickets'] ) ); elseif( isset( $arrValues['open_support_tickets'] ) ) $this->setOpenSupportTickets( $arrValues['open_support_tickets'] );
		if( isset( $arrValues['client_impactful_released_tasks'] ) && $boolDirectSet ) $this->set( 'm_intClientImpactfulReleasedTasks', trim( $arrValues['client_impactful_released_tasks'] ) ); elseif( isset( $arrValues['client_impactful_released_tasks'] ) ) $this->setClientImpactfulReleasedTasks( $arrValues['client_impactful_released_tasks'] );
		if( isset( $arrValues['released_tasks_without_story_points'] ) && $boolDirectSet ) $this->set( 'm_intReleasedTasksWithoutStoryPoints', trim( $arrValues['released_tasks_without_story_points'] ) ); elseif( isset( $arrValues['released_tasks_without_story_points'] ) ) $this->setReleasedTasksWithoutStoryPoints( $arrValues['released_tasks_without_story_points'] );
		if( isset( $arrValues['story_points_released'] ) && $boolDirectSet ) $this->set( 'm_intStoryPointsReleased', trim( $arrValues['story_points_released'] ) ); elseif( isset( $arrValues['story_points_released'] ) ) $this->setStoryPointsReleased( $arrValues['story_points_released'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NULL';
	}

	public function setNewClients( $intNewClients ) {
		$this->set( 'm_intNewClients', CStrings::strToIntDef( $intNewClients, NULL, false ) );
	}

	public function getNewClients() {
		return $this->m_intNewClients;
	}

	public function sqlNewClients() {
		return ( true == isset( $this->m_intNewClients ) ) ? ( string ) $this->m_intNewClients : 'NULL';
	}

	public function setLostClients( $intLostClients ) {
		$this->set( 'm_intLostClients', CStrings::strToIntDef( $intLostClients, NULL, false ) );
	}

	public function getLostClients() {
		return $this->m_intLostClients;
	}

	public function sqlLostClients() {
		return ( true == isset( $this->m_intLostClients ) ) ? ( string ) $this->m_intLostClients : 'NULL';
	}

	public function setNetClientChange( $intNetClientChange ) {
		$this->set( 'm_intNetClientChange', CStrings::strToIntDef( $intNetClientChange, NULL, false ) );
	}

	public function getNetClientChange() {
		return $this->m_intNetClientChange;
	}

	public function sqlNetClientChange() {
		return ( true == isset( $this->m_intNetClientChange ) ) ? ( string ) $this->m_intNetClientChange : 'NULL';
	}

	public function setCumulativeClients( $intCumulativeClients ) {
		$this->set( 'm_intCumulativeClients', CStrings::strToIntDef( $intCumulativeClients, NULL, false ) );
	}

	public function getCumulativeClients() {
		return $this->m_intCumulativeClients;
	}

	public function sqlCumulativeClients() {
		return ( true == isset( $this->m_intCumulativeClients ) ) ? ( string ) $this->m_intCumulativeClients : 'NULL';
	}

	public function setSubscriptionRevenue( $intSubscriptionRevenue ) {
		$this->set( 'm_intSubscriptionRevenue', CStrings::strToIntDef( $intSubscriptionRevenue, NULL, false ) );
	}

	public function getSubscriptionRevenue() {
		return $this->m_intSubscriptionRevenue;
	}

	public function sqlSubscriptionRevenue() {
		return ( true == isset( $this->m_intSubscriptionRevenue ) ) ? ( string ) $this->m_intSubscriptionRevenue : 'NULL';
	}

	public function setTransactionRevenue( $intTransactionRevenue ) {
		$this->set( 'm_intTransactionRevenue', CStrings::strToIntDef( $intTransactionRevenue, NULL, false ) );
	}

	public function getTransactionRevenue() {
		return $this->m_intTransactionRevenue;
	}

	public function sqlTransactionRevenue() {
		return ( true == isset( $this->m_intTransactionRevenue ) ) ? ( string ) $this->m_intTransactionRevenue : 'NULL';
	}

	public function setOneTimeRevenue( $intOneTimeRevenue ) {
		$this->set( 'm_intOneTimeRevenue', CStrings::strToIntDef( $intOneTimeRevenue, NULL, false ) );
	}

	public function getOneTimeRevenue() {
		return $this->m_intOneTimeRevenue;
	}

	public function sqlOneTimeRevenue() {
		return ( true == isset( $this->m_intOneTimeRevenue ) ) ? ( string ) $this->m_intOneTimeRevenue : 'NULL';
	}

	public function setWriteOffRevenue( $intWriteOffRevenue ) {
		$this->set( 'm_intWriteOffRevenue', CStrings::strToIntDef( $intWriteOffRevenue, NULL, false ) );
	}

	public function getWriteOffRevenue() {
		return $this->m_intWriteOffRevenue;
	}

	public function sqlWriteOffRevenue() {
		return ( true == isset( $this->m_intWriteOffRevenue ) ) ? ( string ) $this->m_intWriteOffRevenue : 'NULL';
	}

	public function setTotalRevenue( $intTotalRevenue ) {
		$this->set( 'm_intTotalRevenue', CStrings::strToIntDef( $intTotalRevenue, NULL, false ) );
	}

	public function getTotalRevenue() {
		return $this->m_intTotalRevenue;
	}

	public function sqlTotalRevenue() {
		return ( true == isset( $this->m_intTotalRevenue ) ) ? ( string ) $this->m_intTotalRevenue : 'NULL';
	}

	public function setQuarterRevenueGrowthPercent( $fltQuarterRevenueGrowthPercent ) {
		$this->set( 'm_fltQuarterRevenueGrowthPercent', CStrings::strToFloatDef( $fltQuarterRevenueGrowthPercent, NULL, false, 0 ) );
	}

	public function getQuarterRevenueGrowthPercent() {
		return $this->m_fltQuarterRevenueGrowthPercent;
	}

	public function sqlQuarterRevenueGrowthPercent() {
		return ( true == isset( $this->m_fltQuarterRevenueGrowthPercent ) ) ? ( string ) $this->m_fltQuarterRevenueGrowthPercent : 'NULL';
	}

	public function setLostRevenueUnimplemented( $intLostRevenueUnimplemented ) {
		$this->set( 'm_intLostRevenueUnimplemented', CStrings::strToIntDef( $intLostRevenueUnimplemented, NULL, false ) );
	}

	public function getLostRevenueUnimplemented() {
		return $this->m_intLostRevenueUnimplemented;
	}

	public function sqlLostRevenueUnimplemented() {
		return ( true == isset( $this->m_intLostRevenueUnimplemented ) ) ? ( string ) $this->m_intLostRevenueUnimplemented : 'NULL';
	}

	public function setLostRevenueUnhappy( $intLostRevenueUnhappy ) {
		$this->set( 'm_intLostRevenueUnhappy', CStrings::strToIntDef( $intLostRevenueUnhappy, NULL, false ) );
	}

	public function getLostRevenueUnhappy() {
		return $this->m_intLostRevenueUnhappy;
	}

	public function sqlLostRevenueUnhappy() {
		return ( true == isset( $this->m_intLostRevenueUnhappy ) ) ? ( string ) $this->m_intLostRevenueUnhappy : 'NULL';
	}

	public function setLostRevenueManagement( $intLostRevenueManagement ) {
		$this->set( 'm_intLostRevenueManagement', CStrings::strToIntDef( $intLostRevenueManagement, NULL, false ) );
	}

	public function getLostRevenueManagement() {
		return $this->m_intLostRevenueManagement;
	}

	public function sqlLostRevenueManagement() {
		return ( true == isset( $this->m_intLostRevenueManagement ) ) ? ( string ) $this->m_intLostRevenueManagement : 'NULL';
	}

	public function setLostRevenueTransferProperty( $intLostRevenueTransferProperty ) {
		$this->set( 'm_intLostRevenueTransferProperty', CStrings::strToIntDef( $intLostRevenueTransferProperty, NULL, false ) );
	}

	public function getLostRevenueTransferProperty() {
		return $this->m_intLostRevenueTransferProperty;
	}

	public function sqlLostRevenueTransferProperty() {
		return ( true == isset( $this->m_intLostRevenueTransferProperty ) ) ? ( string ) $this->m_intLostRevenueTransferProperty : 'NULL';
	}

	public function setTotalLostRevenue( $intTotalLostRevenue ) {
		$this->set( 'm_intTotalLostRevenue', CStrings::strToIntDef( $intTotalLostRevenue, NULL, false ) );
	}

	public function getTotalLostRevenue() {
		return $this->m_intTotalLostRevenue;
	}

	public function sqlTotalLostRevenue() {
		return ( true == isset( $this->m_intTotalLostRevenue ) ) ? ( string ) $this->m_intTotalLostRevenue : 'NULL';
	}

	public function setNewLogoUnits( $intNewLogoUnits ) {
		$this->set( 'm_intNewLogoUnits', CStrings::strToIntDef( $intNewLogoUnits, NULL, false ) );
	}

	public function getNewLogoUnits() {
		return $this->m_intNewLogoUnits;
	}

	public function sqlNewLogoUnits() {
		return ( true == isset( $this->m_intNewLogoUnits ) ) ? ( string ) $this->m_intNewLogoUnits : 'NULL';
	}

	public function setNewProductUnits( $intNewProductUnits ) {
		$this->set( 'm_intNewProductUnits', CStrings::strToIntDef( $intNewProductUnits, NULL, false ) );
	}

	public function getNewProductUnits() {
		return $this->m_intNewProductUnits;
	}

	public function sqlNewProductUnits() {
		return ( true == isset( $this->m_intNewProductUnits ) ) ? ( string ) $this->m_intNewProductUnits : 'NULL';
	}

	public function setNewPropertyUnits( $intNewPropertyUnits ) {
		$this->set( 'm_intNewPropertyUnits', CStrings::strToIntDef( $intNewPropertyUnits, NULL, false ) );
	}

	public function getNewPropertyUnits() {
		return $this->m_intNewPropertyUnits;
	}

	public function sqlNewPropertyUnits() {
		return ( true == isset( $this->m_intNewPropertyUnits ) ) ? ( string ) $this->m_intNewPropertyUnits : 'NULL';
	}

	public function setNewTransferPropertyUnits( $intNewTransferPropertyUnits ) {
		$this->set( 'm_intNewTransferPropertyUnits', CStrings::strToIntDef( $intNewTransferPropertyUnits, NULL, false ) );
	}

	public function getNewTransferPropertyUnits() {
		return $this->m_intNewTransferPropertyUnits;
	}

	public function sqlNewTransferPropertyUnits() {
		return ( true == isset( $this->m_intNewTransferPropertyUnits ) ) ? ( string ) $this->m_intNewTransferPropertyUnits : 'NULL';
	}

	public function setTotalNewUnits( $intTotalNewUnits ) {
		$this->set( 'm_intTotalNewUnits', CStrings::strToIntDef( $intTotalNewUnits, NULL, false ) );
	}

	public function getTotalNewUnits() {
		return $this->m_intTotalNewUnits;
	}

	public function sqlTotalNewUnits() {
		return ( true == isset( $this->m_intTotalNewUnits ) ) ? ( string ) $this->m_intTotalNewUnits : 'NULL';
	}

	public function setLostUnitsUnhappy( $intLostUnitsUnhappy ) {
		$this->set( 'm_intLostUnitsUnhappy', CStrings::strToIntDef( $intLostUnitsUnhappy, NULL, false ) );
	}

	public function getLostUnitsUnhappy() {
		return $this->m_intLostUnitsUnhappy;
	}

	public function sqlLostUnitsUnhappy() {
		return ( true == isset( $this->m_intLostUnitsUnhappy ) ) ? ( string ) $this->m_intLostUnitsUnhappy : 'NULL';
	}

	public function setLostUnitsManagement( $intLostUnitsManagement ) {
		$this->set( 'm_intLostUnitsManagement', CStrings::strToIntDef( $intLostUnitsManagement, NULL, false ) );
	}

	public function getLostUnitsManagement() {
		return $this->m_intLostUnitsManagement;
	}

	public function sqlLostUnitsManagement() {
		return ( true == isset( $this->m_intLostUnitsManagement ) ) ? ( string ) $this->m_intLostUnitsManagement : 'NULL';
	}

	public function setLostUnitsUnimplemented( $intLostUnitsUnimplemented ) {
		$this->set( 'm_intLostUnitsUnimplemented', CStrings::strToIntDef( $intLostUnitsUnimplemented, NULL, false ) );
	}

	public function getLostUnitsUnimplemented() {
		return $this->m_intLostUnitsUnimplemented;
	}

	public function sqlLostUnitsUnimplemented() {
		return ( true == isset( $this->m_intLostUnitsUnimplemented ) ) ? ( string ) $this->m_intLostUnitsUnimplemented : 'NULL';
	}

	public function setLostUnitsTransferProperty( $intLostUnitsTransferProperty ) {
		$this->set( 'm_intLostUnitsTransferProperty', CStrings::strToIntDef( $intLostUnitsTransferProperty, NULL, false ) );
	}

	public function getLostUnitsTransferProperty() {
		return $this->m_intLostUnitsTransferProperty;
	}

	public function sqlLostUnitsTransferProperty() {
		return ( true == isset( $this->m_intLostUnitsTransferProperty ) ) ? ( string ) $this->m_intLostUnitsTransferProperty : 'NULL';
	}

	public function setTotalLostUnits( $intTotalLostUnits ) {
		$this->set( 'm_intTotalLostUnits', CStrings::strToIntDef( $intTotalLostUnits, NULL, false ) );
	}

	public function getTotalLostUnits() {
		return $this->m_intTotalLostUnits;
	}

	public function sqlTotalLostUnits() {
		return ( true == isset( $this->m_intTotalLostUnits ) ) ? ( string ) $this->m_intTotalLostUnits : 'NULL';
	}

	public function setNetUnitChange( $intNetUnitChange ) {
		$this->set( 'm_intNetUnitChange', CStrings::strToIntDef( $intNetUnitChange, NULL, false ) );
	}

	public function getNetUnitChange() {
		return $this->m_intNetUnitChange;
	}

	public function sqlNetUnitChange() {
		return ( true == isset( $this->m_intNetUnitChange ) ) ? ( string ) $this->m_intNetUnitChange : 'NULL';
	}

	public function setCumulativeUnits( $intCumulativeUnits ) {
		$this->set( 'm_intCumulativeUnits', CStrings::strToIntDef( $intCumulativeUnits, NULL, false ) );
	}

	public function getCumulativeUnits() {
		return $this->m_intCumulativeUnits;
	}

	public function sqlCumulativeUnits() {
		return ( true == isset( $this->m_intCumulativeUnits ) ) ? ( string ) $this->m_intCumulativeUnits : 'NULL';
	}

	public function setRevenuePerUnit( $fltRevenuePerUnit ) {
		$this->set( 'm_fltRevenuePerUnit', CStrings::strToFloatDef( $fltRevenuePerUnit, NULL, false, 0 ) );
	}

	public function getRevenuePerUnit() {
		return $this->m_fltRevenuePerUnit;
	}

	public function sqlRevenuePerUnit() {
		return ( true == isset( $this->m_fltRevenuePerUnit ) ) ? ( string ) $this->m_fltRevenuePerUnit : 'NULL';
	}

	public function setNewLogoSubscriptionAcv( $intNewLogoSubscriptionAcv ) {
		$this->set( 'm_intNewLogoSubscriptionAcv', CStrings::strToIntDef( $intNewLogoSubscriptionAcv, NULL, false ) );
	}

	public function getNewLogoSubscriptionAcv() {
		return $this->m_intNewLogoSubscriptionAcv;
	}

	public function sqlNewLogoSubscriptionAcv() {
		return ( true == isset( $this->m_intNewLogoSubscriptionAcv ) ) ? ( string ) $this->m_intNewLogoSubscriptionAcv : 'NULL';
	}

	public function setNewProductSubscriptionAcv( $intNewProductSubscriptionAcv ) {
		$this->set( 'm_intNewProductSubscriptionAcv', CStrings::strToIntDef( $intNewProductSubscriptionAcv, NULL, false ) );
	}

	public function getNewProductSubscriptionAcv() {
		return $this->m_intNewProductSubscriptionAcv;
	}

	public function sqlNewProductSubscriptionAcv() {
		return ( true == isset( $this->m_intNewProductSubscriptionAcv ) ) ? ( string ) $this->m_intNewProductSubscriptionAcv : 'NULL';
	}

	public function setNewPropertySubscriptionAcv( $intNewPropertySubscriptionAcv ) {
		$this->set( 'm_intNewPropertySubscriptionAcv', CStrings::strToIntDef( $intNewPropertySubscriptionAcv, NULL, false ) );
	}

	public function getNewPropertySubscriptionAcv() {
		return $this->m_intNewPropertySubscriptionAcv;
	}

	public function sqlNewPropertySubscriptionAcv() {
		return ( true == isset( $this->m_intNewPropertySubscriptionAcv ) ) ? ( string ) $this->m_intNewPropertySubscriptionAcv : 'NULL';
	}

	public function setNewRenewalSubscriptionAcv( $intNewRenewalSubscriptionAcv ) {
		$this->set( 'm_intNewRenewalSubscriptionAcv', CStrings::strToIntDef( $intNewRenewalSubscriptionAcv, NULL, false ) );
	}

	public function getNewRenewalSubscriptionAcv() {
		return $this->m_intNewRenewalSubscriptionAcv;
	}

	public function sqlNewRenewalSubscriptionAcv() {
		return ( true == isset( $this->m_intNewRenewalSubscriptionAcv ) ) ? ( string ) $this->m_intNewRenewalSubscriptionAcv : 'NULL';
	}

	public function setNewTransferPropertySubscriptionAcv( $intNewTransferPropertySubscriptionAcv ) {
		$this->set( 'm_intNewTransferPropertySubscriptionAcv', CStrings::strToIntDef( $intNewTransferPropertySubscriptionAcv, NULL, false ) );
	}

	public function getNewTransferPropertySubscriptionAcv() {
		return $this->m_intNewTransferPropertySubscriptionAcv;
	}

	public function sqlNewTransferPropertySubscriptionAcv() {
		return ( true == isset( $this->m_intNewTransferPropertySubscriptionAcv ) ) ? ( string ) $this->m_intNewTransferPropertySubscriptionAcv : 'NULL';
	}

	public function setTotalNewSubscriptionAcv( $intTotalNewSubscriptionAcv ) {
		$this->set( 'm_intTotalNewSubscriptionAcv', CStrings::strToIntDef( $intTotalNewSubscriptionAcv, NULL, false ) );
	}

	public function getTotalNewSubscriptionAcv() {
		return $this->m_intTotalNewSubscriptionAcv;
	}

	public function sqlTotalNewSubscriptionAcv() {
		return ( true == isset( $this->m_intTotalNewSubscriptionAcv ) ) ? ( string ) $this->m_intTotalNewSubscriptionAcv : 'NULL';
	}

	public function setTotalNewActiveSubscriptionAcv( $intTotalNewActiveSubscriptionAcv ) {
		$this->set( 'm_intTotalNewActiveSubscriptionAcv', CStrings::strToIntDef( $intTotalNewActiveSubscriptionAcv, NULL, false ) );
	}

	public function getTotalNewActiveSubscriptionAcv() {
		return $this->m_intTotalNewActiveSubscriptionAcv;
	}

	public function sqlTotalNewActiveSubscriptionAcv() {
		return ( true == isset( $this->m_intTotalNewActiveSubscriptionAcv ) ) ? ( string ) $this->m_intTotalNewActiveSubscriptionAcv : 'NULL';
	}

	public function setTotalNewAdjustedSubscriptionAcv( $intTotalNewAdjustedSubscriptionAcv ) {
		$this->set( 'm_intTotalNewAdjustedSubscriptionAcv', CStrings::strToIntDef( $intTotalNewAdjustedSubscriptionAcv, NULL, false ) );
	}

	public function getTotalNewAdjustedSubscriptionAcv() {
		return $this->m_intTotalNewAdjustedSubscriptionAcv;
	}

	public function sqlTotalNewAdjustedSubscriptionAcv() {
		return ( true == isset( $this->m_intTotalNewAdjustedSubscriptionAcv ) ) ? ( string ) $this->m_intTotalNewAdjustedSubscriptionAcv : 'NULL';
	}

	public function setTotalNewActiveAdjustedSubscriptionAcv( $intTotalNewActiveAdjustedSubscriptionAcv ) {
		$this->set( 'm_intTotalNewActiveAdjustedSubscriptionAcv', CStrings::strToIntDef( $intTotalNewActiveAdjustedSubscriptionAcv, NULL, false ) );
	}

	public function getTotalNewActiveAdjustedSubscriptionAcv() {
		return $this->m_intTotalNewActiveAdjustedSubscriptionAcv;
	}

	public function sqlTotalNewActiveAdjustedSubscriptionAcv() {
		return ( true == isset( $this->m_intTotalNewActiveAdjustedSubscriptionAcv ) ) ? ( string ) $this->m_intTotalNewActiveAdjustedSubscriptionAcv : 'NULL';
	}

	public function setNewLogoTransactionAcv( $intNewLogoTransactionAcv ) {
		$this->set( 'm_intNewLogoTransactionAcv', CStrings::strToIntDef( $intNewLogoTransactionAcv, NULL, false ) );
	}

	public function getNewLogoTransactionAcv() {
		return $this->m_intNewLogoTransactionAcv;
	}

	public function sqlNewLogoTransactionAcv() {
		return ( true == isset( $this->m_intNewLogoTransactionAcv ) ) ? ( string ) $this->m_intNewLogoTransactionAcv : 'NULL';
	}

	public function setNewProductTransactionAcv( $intNewProductTransactionAcv ) {
		$this->set( 'm_intNewProductTransactionAcv', CStrings::strToIntDef( $intNewProductTransactionAcv, NULL, false ) );
	}

	public function getNewProductTransactionAcv() {
		return $this->m_intNewProductTransactionAcv;
	}

	public function sqlNewProductTransactionAcv() {
		return ( true == isset( $this->m_intNewProductTransactionAcv ) ) ? ( string ) $this->m_intNewProductTransactionAcv : 'NULL';
	}

	public function setNewPropertyTransactionAcv( $intNewPropertyTransactionAcv ) {
		$this->set( 'm_intNewPropertyTransactionAcv', CStrings::strToIntDef( $intNewPropertyTransactionAcv, NULL, false ) );
	}

	public function getNewPropertyTransactionAcv() {
		return $this->m_intNewPropertyTransactionAcv;
	}

	public function sqlNewPropertyTransactionAcv() {
		return ( true == isset( $this->m_intNewPropertyTransactionAcv ) ) ? ( string ) $this->m_intNewPropertyTransactionAcv : 'NULL';
	}

	public function setNewRenewalTransactionAcv( $intNewRenewalTransactionAcv ) {
		$this->set( 'm_intNewRenewalTransactionAcv', CStrings::strToIntDef( $intNewRenewalTransactionAcv, NULL, false ) );
	}

	public function getNewRenewalTransactionAcv() {
		return $this->m_intNewRenewalTransactionAcv;
	}

	public function sqlNewRenewalTransactionAcv() {
		return ( true == isset( $this->m_intNewRenewalTransactionAcv ) ) ? ( string ) $this->m_intNewRenewalTransactionAcv : 'NULL';
	}

	public function setNewTransferPropertyTransactionAcv( $intNewTransferPropertyTransactionAcv ) {
		$this->set( 'm_intNewTransferPropertyTransactionAcv', CStrings::strToIntDef( $intNewTransferPropertyTransactionAcv, NULL, false ) );
	}

	public function getNewTransferPropertyTransactionAcv() {
		return $this->m_intNewTransferPropertyTransactionAcv;
	}

	public function sqlNewTransferPropertyTransactionAcv() {
		return ( true == isset( $this->m_intNewTransferPropertyTransactionAcv ) ) ? ( string ) $this->m_intNewTransferPropertyTransactionAcv : 'NULL';
	}

	public function setTotalNewTransactionAcv( $intTotalNewTransactionAcv ) {
		$this->set( 'm_intTotalNewTransactionAcv', CStrings::strToIntDef( $intTotalNewTransactionAcv, NULL, false ) );
	}

	public function getTotalNewTransactionAcv() {
		return $this->m_intTotalNewTransactionAcv;
	}

	public function sqlTotalNewTransactionAcv() {
		return ( true == isset( $this->m_intTotalNewTransactionAcv ) ) ? ( string ) $this->m_intTotalNewTransactionAcv : 'NULL';
	}

	public function setTotalNewActiveTransactionAcv( $intTotalNewActiveTransactionAcv ) {
		$this->set( 'm_intTotalNewActiveTransactionAcv', CStrings::strToIntDef( $intTotalNewActiveTransactionAcv, NULL, false ) );
	}

	public function getTotalNewActiveTransactionAcv() {
		return $this->m_intTotalNewActiveTransactionAcv;
	}

	public function sqlTotalNewActiveTransactionAcv() {
		return ( true == isset( $this->m_intTotalNewActiveTransactionAcv ) ) ? ( string ) $this->m_intTotalNewActiveTransactionAcv : 'NULL';
	}

	public function setTotalNewAdjustedTransactionAcv( $intTotalNewAdjustedTransactionAcv ) {
		$this->set( 'm_intTotalNewAdjustedTransactionAcv', CStrings::strToIntDef( $intTotalNewAdjustedTransactionAcv, NULL, false ) );
	}

	public function getTotalNewAdjustedTransactionAcv() {
		return $this->m_intTotalNewAdjustedTransactionAcv;
	}

	public function sqlTotalNewAdjustedTransactionAcv() {
		return ( true == isset( $this->m_intTotalNewAdjustedTransactionAcv ) ) ? ( string ) $this->m_intTotalNewAdjustedTransactionAcv : 'NULL';
	}

	public function setTotalNewActiveAdjustedTransactionAcv( $intTotalNewActiveAdjustedTransactionAcv ) {
		$this->set( 'm_intTotalNewActiveAdjustedTransactionAcv', CStrings::strToIntDef( $intTotalNewActiveAdjustedTransactionAcv, NULL, false ) );
	}

	public function getTotalNewActiveAdjustedTransactionAcv() {
		return $this->m_intTotalNewActiveAdjustedTransactionAcv;
	}

	public function sqlTotalNewActiveAdjustedTransactionAcv() {
		return ( true == isset( $this->m_intTotalNewActiveAdjustedTransactionAcv ) ) ? ( string ) $this->m_intTotalNewActiveAdjustedTransactionAcv : 'NULL';
	}

	public function setTotalNewAcv( $intTotalNewAcv ) {
		$this->set( 'm_intTotalNewAcv', CStrings::strToIntDef( $intTotalNewAcv, NULL, false ) );
	}

	public function getTotalNewAcv() {
		return $this->m_intTotalNewAcv;
	}

	public function sqlTotalNewAcv() {
		return ( true == isset( $this->m_intTotalNewAcv ) ) ? ( string ) $this->m_intTotalNewAcv : 'NULL';
	}

	public function setTotalNewActiveAcv( $intTotalNewActiveAcv ) {
		$this->set( 'm_intTotalNewActiveAcv', CStrings::strToIntDef( $intTotalNewActiveAcv, NULL, false ) );
	}

	public function getTotalNewActiveAcv() {
		return $this->m_intTotalNewActiveAcv;
	}

	public function sqlTotalNewActiveAcv() {
		return ( true == isset( $this->m_intTotalNewActiveAcv ) ) ? ( string ) $this->m_intTotalNewActiveAcv : 'NULL';
	}

	public function setLostSubscriptionAcvUnhappy( $intLostSubscriptionAcvUnhappy ) {
		$this->set( 'm_intLostSubscriptionAcvUnhappy', CStrings::strToIntDef( $intLostSubscriptionAcvUnhappy, NULL, false ) );
	}

	public function getLostSubscriptionAcvUnhappy() {
		return $this->m_intLostSubscriptionAcvUnhappy;
	}

	public function sqlLostSubscriptionAcvUnhappy() {
		return ( true == isset( $this->m_intLostSubscriptionAcvUnhappy ) ) ? ( string ) $this->m_intLostSubscriptionAcvUnhappy : 'NULL';
	}

	public function setLostSubscriptionAcvUnimplemented( $intLostSubscriptionAcvUnimplemented ) {
		$this->set( 'm_intLostSubscriptionAcvUnimplemented', CStrings::strToIntDef( $intLostSubscriptionAcvUnimplemented, NULL, false ) );
	}

	public function getLostSubscriptionAcvUnimplemented() {
		return $this->m_intLostSubscriptionAcvUnimplemented;
	}

	public function sqlLostSubscriptionAcvUnimplemented() {
		return ( true == isset( $this->m_intLostSubscriptionAcvUnimplemented ) ) ? ( string ) $this->m_intLostSubscriptionAcvUnimplemented : 'NULL';
	}

	public function setLostSubscriptionAcvTransferProperty( $intLostSubscriptionAcvTransferProperty ) {
		$this->set( 'm_intLostSubscriptionAcvTransferProperty', CStrings::strToIntDef( $intLostSubscriptionAcvTransferProperty, NULL, false ) );
	}

	public function getLostSubscriptionAcvTransferProperty() {
		return $this->m_intLostSubscriptionAcvTransferProperty;
	}

	public function sqlLostSubscriptionAcvTransferProperty() {
		return ( true == isset( $this->m_intLostSubscriptionAcvTransferProperty ) ) ? ( string ) $this->m_intLostSubscriptionAcvTransferProperty : 'NULL';
	}

	public function setLostSubscriptionAcvManagement( $intLostSubscriptionAcvManagement ) {
		$this->set( 'm_intLostSubscriptionAcvManagement', CStrings::strToIntDef( $intLostSubscriptionAcvManagement, NULL, false ) );
	}

	public function getLostSubscriptionAcvManagement() {
		return $this->m_intLostSubscriptionAcvManagement;
	}

	public function sqlLostSubscriptionAcvManagement() {
		return ( true == isset( $this->m_intLostSubscriptionAcvManagement ) ) ? ( string ) $this->m_intLostSubscriptionAcvManagement : 'NULL';
	}

	public function setTotalLostSubscriptionAcv( $intTotalLostSubscriptionAcv ) {
		$this->set( 'm_intTotalLostSubscriptionAcv', CStrings::strToIntDef( $intTotalLostSubscriptionAcv, NULL, false ) );
	}

	public function getTotalLostSubscriptionAcv() {
		return $this->m_intTotalLostSubscriptionAcv;
	}

	public function sqlTotalLostSubscriptionAcv() {
		return ( true == isset( $this->m_intTotalLostSubscriptionAcv ) ) ? ( string ) $this->m_intTotalLostSubscriptionAcv : 'NULL';
	}

	public function setLostTransactionAcvUnhappy( $intLostTransactionAcvUnhappy ) {
		$this->set( 'm_intLostTransactionAcvUnhappy', CStrings::strToIntDef( $intLostTransactionAcvUnhappy, NULL, false ) );
	}

	public function getLostTransactionAcvUnhappy() {
		return $this->m_intLostTransactionAcvUnhappy;
	}

	public function sqlLostTransactionAcvUnhappy() {
		return ( true == isset( $this->m_intLostTransactionAcvUnhappy ) ) ? ( string ) $this->m_intLostTransactionAcvUnhappy : 'NULL';
	}

	public function setLostTransactionAcvUnimplemented( $intLostTransactionAcvUnimplemented ) {
		$this->set( 'm_intLostTransactionAcvUnimplemented', CStrings::strToIntDef( $intLostTransactionAcvUnimplemented, NULL, false ) );
	}

	public function getLostTransactionAcvUnimplemented() {
		return $this->m_intLostTransactionAcvUnimplemented;
	}

	public function sqlLostTransactionAcvUnimplemented() {
		return ( true == isset( $this->m_intLostTransactionAcvUnimplemented ) ) ? ( string ) $this->m_intLostTransactionAcvUnimplemented : 'NULL';
	}

	public function setLostTransactionAcvTransferProperty( $intLostTransactionAcvTransferProperty ) {
		$this->set( 'm_intLostTransactionAcvTransferProperty', CStrings::strToIntDef( $intLostTransactionAcvTransferProperty, NULL, false ) );
	}

	public function getLostTransactionAcvTransferProperty() {
		return $this->m_intLostTransactionAcvTransferProperty;
	}

	public function sqlLostTransactionAcvTransferProperty() {
		return ( true == isset( $this->m_intLostTransactionAcvTransferProperty ) ) ? ( string ) $this->m_intLostTransactionAcvTransferProperty : 'NULL';
	}

	public function setLostTransactionAcvManagement( $intLostTransactionAcvManagement ) {
		$this->set( 'm_intLostTransactionAcvManagement', CStrings::strToIntDef( $intLostTransactionAcvManagement, NULL, false ) );
	}

	public function getLostTransactionAcvManagement() {
		return $this->m_intLostTransactionAcvManagement;
	}

	public function sqlLostTransactionAcvManagement() {
		return ( true == isset( $this->m_intLostTransactionAcvManagement ) ) ? ( string ) $this->m_intLostTransactionAcvManagement : 'NULL';
	}

	public function setTotalLostTransactionAcv( $intTotalLostTransactionAcv ) {
		$this->set( 'm_intTotalLostTransactionAcv', CStrings::strToIntDef( $intTotalLostTransactionAcv, NULL, false ) );
	}

	public function getTotalLostTransactionAcv() {
		return $this->m_intTotalLostTransactionAcv;
	}

	public function sqlTotalLostTransactionAcv() {
		return ( true == isset( $this->m_intTotalLostTransactionAcv ) ) ? ( string ) $this->m_intTotalLostTransactionAcv : 'NULL';
	}

	public function setTotalLostAcv( $intTotalLostAcv ) {
		$this->set( 'm_intTotalLostAcv', CStrings::strToIntDef( $intTotalLostAcv, NULL, false ) );
	}

	public function getTotalLostAcv() {
		return $this->m_intTotalLostAcv;
	}

	public function sqlTotalLostAcv() {
		return ( true == isset( $this->m_intTotalLostAcv ) ) ? ( string ) $this->m_intTotalLostAcv : 'NULL';
	}

	public function setLostAcvUnhappy( $intLostAcvUnhappy ) {
		$this->set( 'm_intLostAcvUnhappy', CStrings::strToIntDef( $intLostAcvUnhappy, NULL, false ) );
	}

	public function getLostAcvUnhappy() {
		return $this->m_intLostAcvUnhappy;
	}

	public function sqlLostAcvUnhappy() {
		return ( true == isset( $this->m_intLostAcvUnhappy ) ) ? ( string ) $this->m_intLostAcvUnhappy : 'NULL';
	}

	public function setLostAcvUnimplemented( $intLostAcvUnimplemented ) {
		$this->set( 'm_intLostAcvUnimplemented', CStrings::strToIntDef( $intLostAcvUnimplemented, NULL, false ) );
	}

	public function getLostAcvUnimplemented() {
		return $this->m_intLostAcvUnimplemented;
	}

	public function sqlLostAcvUnimplemented() {
		return ( true == isset( $this->m_intLostAcvUnimplemented ) ) ? ( string ) $this->m_intLostAcvUnimplemented : 'NULL';
	}

	public function setLostAcvTransferProperty( $intLostAcvTransferProperty ) {
		$this->set( 'm_intLostAcvTransferProperty', CStrings::strToIntDef( $intLostAcvTransferProperty, NULL, false ) );
	}

	public function getLostAcvTransferProperty() {
		return $this->m_intLostAcvTransferProperty;
	}

	public function sqlLostAcvTransferProperty() {
		return ( true == isset( $this->m_intLostAcvTransferProperty ) ) ? ( string ) $this->m_intLostAcvTransferProperty : 'NULL';
	}

	public function setLostAcvManagement( $intLostAcvManagement ) {
		$this->set( 'm_intLostAcvManagement', CStrings::strToIntDef( $intLostAcvManagement, NULL, false ) );
	}

	public function getLostAcvManagement() {
		return $this->m_intLostAcvManagement;
	}

	public function sqlLostAcvManagement() {
		return ( true == isset( $this->m_intLostAcvManagement ) ) ? ( string ) $this->m_intLostAcvManagement : 'NULL';
	}

	public function setNetAcvChange( $intNetAcvChange ) {
		$this->set( 'm_intNetAcvChange', CStrings::strToIntDef( $intNetAcvChange, NULL, false ) );
	}

	public function getNetAcvChange() {
		return $this->m_intNetAcvChange;
	}

	public function sqlNetAcvChange() {
		return ( true == isset( $this->m_intNetAcvChange ) ) ? ( string ) $this->m_intNetAcvChange : 'NULL';
	}

	public function setCumulativeAcv( $intCumulativeAcv ) {
		$this->set( 'm_intCumulativeAcv', CStrings::strToIntDef( $intCumulativeAcv, NULL, false ) );
	}

	public function getCumulativeAcv() {
		return $this->m_intCumulativeAcv;
	}

	public function sqlCumulativeAcv() {
		return ( true == isset( $this->m_intCumulativeAcv ) ) ? ( string ) $this->m_intCumulativeAcv : 'NULL';
	}

	public function setMcvPerUnit( $fltMcvPerUnit ) {
		$this->set( 'm_fltMcvPerUnit', CStrings::strToFloatDef( $fltMcvPerUnit, NULL, false, 0 ) );
	}

	public function getMcvPerUnit() {
		return $this->m_fltMcvPerUnit;
	}

	public function sqlMcvPerUnit() {
		return ( true == isset( $this->m_fltMcvPerUnit ) ) ? ( string ) $this->m_fltMcvPerUnit : 'NULL';
	}

	public function setNewLogoImplementation( $intNewLogoImplementation ) {
		$this->set( 'm_intNewLogoImplementation', CStrings::strToIntDef( $intNewLogoImplementation, NULL, false ) );
	}

	public function getNewLogoImplementation() {
		return $this->m_intNewLogoImplementation;
	}

	public function sqlNewLogoImplementation() {
		return ( true == isset( $this->m_intNewLogoImplementation ) ) ? ( string ) $this->m_intNewLogoImplementation : 'NULL';
	}

	public function setNewProductImplementation( $intNewProductImplementation ) {
		$this->set( 'm_intNewProductImplementation', CStrings::strToIntDef( $intNewProductImplementation, NULL, false ) );
	}

	public function getNewProductImplementation() {
		return $this->m_intNewProductImplementation;
	}

	public function sqlNewProductImplementation() {
		return ( true == isset( $this->m_intNewProductImplementation ) ) ? ( string ) $this->m_intNewProductImplementation : 'NULL';
	}

	public function setNewPropertyImplementation( $intNewPropertyImplementation ) {
		$this->set( 'm_intNewPropertyImplementation', CStrings::strToIntDef( $intNewPropertyImplementation, NULL, false ) );
	}

	public function getNewPropertyImplementation() {
		return $this->m_intNewPropertyImplementation;
	}

	public function sqlNewPropertyImplementation() {
		return ( true == isset( $this->m_intNewPropertyImplementation ) ) ? ( string ) $this->m_intNewPropertyImplementation : 'NULL';
	}

	public function setNewRenewalImplementation( $intNewRenewalImplementation ) {
		$this->set( 'm_intNewRenewalImplementation', CStrings::strToIntDef( $intNewRenewalImplementation, NULL, false ) );
	}

	public function getNewRenewalImplementation() {
		return $this->m_intNewRenewalImplementation;
	}

	public function sqlNewRenewalImplementation() {
		return ( true == isset( $this->m_intNewRenewalImplementation ) ) ? ( string ) $this->m_intNewRenewalImplementation : 'NULL';
	}

	public function setNewTransferPropertyImplementation( $intNewTransferPropertyImplementation ) {
		$this->set( 'm_intNewTransferPropertyImplementation', CStrings::strToIntDef( $intNewTransferPropertyImplementation, NULL, false ) );
	}

	public function getNewTransferPropertyImplementation() {
		return $this->m_intNewTransferPropertyImplementation;
	}

	public function sqlNewTransferPropertyImplementation() {
		return ( true == isset( $this->m_intNewTransferPropertyImplementation ) ) ? ( string ) $this->m_intNewTransferPropertyImplementation : 'NULL';
	}

	public function setPilotNewSubscriptionAcv( $intPilotNewSubscriptionAcv ) {
		$this->set( 'm_intPilotNewSubscriptionAcv', CStrings::strToIntDef( $intPilotNewSubscriptionAcv, NULL, false ) );
	}

	public function getPilotNewSubscriptionAcv() {
		return $this->m_intPilotNewSubscriptionAcv;
	}

	public function sqlPilotNewSubscriptionAcv() {
		return ( true == isset( $this->m_intPilotNewSubscriptionAcv ) ) ? ( string ) $this->m_intPilotNewSubscriptionAcv : 'NULL';
	}

	public function setPilotNewTransactionAcv( $intPilotNewTransactionAcv ) {
		$this->set( 'm_intPilotNewTransactionAcv', CStrings::strToIntDef( $intPilotNewTransactionAcv, NULL, false ) );
	}

	public function getPilotNewTransactionAcv() {
		return $this->m_intPilotNewTransactionAcv;
	}

	public function sqlPilotNewTransactionAcv() {
		return ( true == isset( $this->m_intPilotNewTransactionAcv ) ) ? ( string ) $this->m_intPilotNewTransactionAcv : 'NULL';
	}

	public function setPilotPendingSubscriptionAcv( $intPilotPendingSubscriptionAcv ) {
		$this->set( 'm_intPilotPendingSubscriptionAcv', CStrings::strToIntDef( $intPilotPendingSubscriptionAcv, NULL, false ) );
	}

	public function getPilotPendingSubscriptionAcv() {
		return $this->m_intPilotPendingSubscriptionAcv;
	}

	public function sqlPilotPendingSubscriptionAcv() {
		return ( true == isset( $this->m_intPilotPendingSubscriptionAcv ) ) ? ( string ) $this->m_intPilotPendingSubscriptionAcv : 'NULL';
	}

	public function setPilotPendingTransactionAcv( $intPilotPendingTransactionAcv ) {
		$this->set( 'm_intPilotPendingTransactionAcv', CStrings::strToIntDef( $intPilotPendingTransactionAcv, NULL, false ) );
	}

	public function getPilotPendingTransactionAcv() {
		return $this->m_intPilotPendingTransactionAcv;
	}

	public function sqlPilotPendingTransactionAcv() {
		return ( true == isset( $this->m_intPilotPendingTransactionAcv ) ) ? ( string ) $this->m_intPilotPendingTransactionAcv : 'NULL';
	}

	public function setPilotWonSubscriptionAcv( $intPilotWonSubscriptionAcv ) {
		$this->set( 'm_intPilotWonSubscriptionAcv', CStrings::strToIntDef( $intPilotWonSubscriptionAcv, NULL, false ) );
	}

	public function getPilotWonSubscriptionAcv() {
		return $this->m_intPilotWonSubscriptionAcv;
	}

	public function sqlPilotWonSubscriptionAcv() {
		return ( true == isset( $this->m_intPilotWonSubscriptionAcv ) ) ? ( string ) $this->m_intPilotWonSubscriptionAcv : 'NULL';
	}

	public function setPilotWonTransactionAcv( $intPilotWonTransactionAcv ) {
		$this->set( 'm_intPilotWonTransactionAcv', CStrings::strToIntDef( $intPilotWonTransactionAcv, NULL, false ) );
	}

	public function getPilotWonTransactionAcv() {
		return $this->m_intPilotWonTransactionAcv;
	}

	public function sqlPilotWonTransactionAcv() {
		return ( true == isset( $this->m_intPilotWonTransactionAcv ) ) ? ( string ) $this->m_intPilotWonTransactionAcv : 'NULL';
	}

	public function setPilotLostSubscriptionAcv( $intPilotLostSubscriptionAcv ) {
		$this->set( 'm_intPilotLostSubscriptionAcv', CStrings::strToIntDef( $intPilotLostSubscriptionAcv, NULL, false ) );
	}

	public function getPilotLostSubscriptionAcv() {
		return $this->m_intPilotLostSubscriptionAcv;
	}

	public function sqlPilotLostSubscriptionAcv() {
		return ( true == isset( $this->m_intPilotLostSubscriptionAcv ) ) ? ( string ) $this->m_intPilotLostSubscriptionAcv : 'NULL';
	}

	public function setPilotLostTransactionAcv( $intPilotLostTransactionAcv ) {
		$this->set( 'm_intPilotLostTransactionAcv', CStrings::strToIntDef( $intPilotLostTransactionAcv, NULL, false ) );
	}

	public function getPilotLostTransactionAcv() {
		return $this->m_intPilotLostTransactionAcv;
	}

	public function sqlPilotLostTransactionAcv() {
		return ( true == isset( $this->m_intPilotLostTransactionAcv ) ) ? ( string ) $this->m_intPilotLostTransactionAcv : 'NULL';
	}

	public function setPilotOpenCount( $intPilotOpenCount ) {
		$this->set( 'm_intPilotOpenCount', CStrings::strToIntDef( $intPilotOpenCount, NULL, false ) );
	}

	public function getPilotOpenCount() {
		return $this->m_intPilotOpenCount;
	}

	public function sqlPilotOpenCount() {
		return ( true == isset( $this->m_intPilotOpenCount ) ) ? ( string ) $this->m_intPilotOpenCount : 'NULL';
	}

	public function setUnimplementedOpenDays( $intUnimplementedOpenDays ) {
		$this->set( 'm_intUnimplementedOpenDays', CStrings::strToIntDef( $intUnimplementedOpenDays, NULL, false ) );
	}

	public function getUnimplementedOpenDays() {
		return $this->m_intUnimplementedOpenDays;
	}

	public function sqlUnimplementedOpenDays() {
		return ( true == isset( $this->m_intUnimplementedOpenDays ) ) ? ( string ) $this->m_intUnimplementedOpenDays : 'NULL';
	}

	public function setUnimplementedSubscriptionAcv( $intUnimplementedSubscriptionAcv ) {
		$this->set( 'm_intUnimplementedSubscriptionAcv', CStrings::strToIntDef( $intUnimplementedSubscriptionAcv, NULL, false ) );
	}

	public function getUnimplementedSubscriptionAcv() {
		return $this->m_intUnimplementedSubscriptionAcv;
	}

	public function sqlUnimplementedSubscriptionAcv() {
		return ( true == isset( $this->m_intUnimplementedSubscriptionAcv ) ) ? ( string ) $this->m_intUnimplementedSubscriptionAcv : 'NULL';
	}

	public function setUnimplementedTransactionAcv( $intUnimplementedTransactionAcv ) {
		$this->set( 'm_intUnimplementedTransactionAcv', CStrings::strToIntDef( $intUnimplementedTransactionAcv, NULL, false ) );
	}

	public function getUnimplementedTransactionAcv() {
		return $this->m_intUnimplementedTransactionAcv;
	}

	public function sqlUnimplementedTransactionAcv() {
		return ( true == isset( $this->m_intUnimplementedTransactionAcv ) ) ? ( string ) $this->m_intUnimplementedTransactionAcv : 'NULL';
	}

	public function setUnimplementedAcv( $intUnimplementedAcv ) {
		$this->set( 'm_intUnimplementedAcv', CStrings::strToIntDef( $intUnimplementedAcv, NULL, false ) );
	}

	public function getUnimplementedAcv() {
		return $this->m_intUnimplementedAcv;
	}

	public function sqlUnimplementedAcv() {
		return ( true == isset( $this->m_intUnimplementedAcv ) ) ? ( string ) $this->m_intUnimplementedAcv : 'NULL';
	}

	public function setUnimplementedUnits( $intUnimplementedUnits ) {
		$this->set( 'm_intUnimplementedUnits', CStrings::strToIntDef( $intUnimplementedUnits, NULL, false ) );
	}

	public function getUnimplementedUnits() {
		return $this->m_intUnimplementedUnits;
	}

	public function sqlUnimplementedUnits() {
		return ( true == isset( $this->m_intUnimplementedUnits ) ) ? ( string ) $this->m_intUnimplementedUnits : 'NULL';
	}

	public function setImplementedSubscriptionAcv( $intImplementedSubscriptionAcv ) {
		$this->set( 'm_intImplementedSubscriptionAcv', CStrings::strToIntDef( $intImplementedSubscriptionAcv, NULL, false ) );
	}

	public function getImplementedSubscriptionAcv() {
		return $this->m_intImplementedSubscriptionAcv;
	}

	public function sqlImplementedSubscriptionAcv() {
		return ( true == isset( $this->m_intImplementedSubscriptionAcv ) ) ? ( string ) $this->m_intImplementedSubscriptionAcv : 'NULL';
	}

	public function setImplementedTransactionAcv( $intImplementedTransactionAcv ) {
		$this->set( 'm_intImplementedTransactionAcv', CStrings::strToIntDef( $intImplementedTransactionAcv, NULL, false ) );
	}

	public function getImplementedTransactionAcv() {
		return $this->m_intImplementedTransactionAcv;
	}

	public function sqlImplementedTransactionAcv() {
		return ( true == isset( $this->m_intImplementedTransactionAcv ) ) ? ( string ) $this->m_intImplementedTransactionAcv : 'NULL';
	}

	public function setImplementedAcv( $intImplementedAcv ) {
		$this->set( 'm_intImplementedAcv', CStrings::strToIntDef( $intImplementedAcv, NULL, false ) );
	}

	public function getImplementedAcv() {
		return $this->m_intImplementedAcv;
	}

	public function sqlImplementedAcv() {
		return ( true == isset( $this->m_intImplementedAcv ) ) ? ( string ) $this->m_intImplementedAcv : 'NULL';
	}

	public function setImplementedUnits( $intImplementedUnits ) {
		$this->set( 'm_intImplementedUnits', CStrings::strToIntDef( $intImplementedUnits, NULL, false ) );
	}

	public function getImplementedUnits() {
		return $this->m_intImplementedUnits;
	}

	public function sqlImplementedUnits() {
		return ( true == isset( $this->m_intImplementedUnits ) ) ? ( string ) $this->m_intImplementedUnits : 'NULL';
	}

	public function setUnbilledOpenDays( $intUnbilledOpenDays ) {
		$this->set( 'm_intUnbilledOpenDays', CStrings::strToIntDef( $intUnbilledOpenDays, NULL, false ) );
	}

	public function getUnbilledOpenDays() {
		return $this->m_intUnbilledOpenDays;
	}

	public function sqlUnbilledOpenDays() {
		return ( true == isset( $this->m_intUnbilledOpenDays ) ) ? ( string ) $this->m_intUnbilledOpenDays : 'NULL';
	}

	public function setUnbilledAcv( $intUnbilledAcv ) {
		$this->set( 'm_intUnbilledAcv', CStrings::strToIntDef( $intUnbilledAcv, NULL, false ) );
	}

	public function getUnbilledAcv() {
		return $this->m_intUnbilledAcv;
	}

	public function sqlUnbilledAcv() {
		return ( true == isset( $this->m_intUnbilledAcv ) ) ? ( string ) $this->m_intUnbilledAcv : 'NULL';
	}

	public function setUnbilledUnits( $intUnbilledUnits ) {
		$this->set( 'm_intUnbilledUnits', CStrings::strToIntDef( $intUnbilledUnits, NULL, false ) );
	}

	public function getUnbilledUnits() {
		return $this->m_intUnbilledUnits;
	}

	public function sqlUnbilledUnits() {
		return ( true == isset( $this->m_intUnbilledUnits ) ) ? ( string ) $this->m_intUnbilledUnits : 'NULL';
	}

	public function setBilledAcv( $intBilledAcv ) {
		$this->set( 'm_intBilledAcv', CStrings::strToIntDef( $intBilledAcv, NULL, false ) );
	}

	public function getBilledAcv() {
		return $this->m_intBilledAcv;
	}

	public function sqlBilledAcv() {
		return ( true == isset( $this->m_intBilledAcv ) ) ? ( string ) $this->m_intBilledAcv : 'NULL';
	}

	public function setBilledUnits( $intBilledUnits ) {
		$this->set( 'm_intBilledUnits', CStrings::strToIntDef( $intBilledUnits, NULL, false ) );
	}

	public function getBilledUnits() {
		return $this->m_intBilledUnits;
	}

	public function sqlBilledUnits() {
		return ( true == isset( $this->m_intBilledUnits ) ) ? ( string ) $this->m_intBilledUnits : 'NULL';
	}

	public function setDemosScheduled( $intDemosScheduled ) {
		$this->set( 'm_intDemosScheduled', CStrings::strToIntDef( $intDemosScheduled, NULL, false ) );
	}

	public function getDemosScheduled() {
		return $this->m_intDemosScheduled;
	}

	public function sqlDemosScheduled() {
		return ( true == isset( $this->m_intDemosScheduled ) ) ? ( string ) $this->m_intDemosScheduled : 'NULL';
	}

	public function setDemoedUnits( $intDemoedUnits ) {
		$this->set( 'm_intDemoedUnits', CStrings::strToIntDef( $intDemoedUnits, NULL, false ) );
	}

	public function getDemoedUnits() {
		return $this->m_intDemoedUnits;
	}

	public function sqlDemoedUnits() {
		return ( true == isset( $this->m_intDemoedUnits ) ) ? ( string ) $this->m_intDemoedUnits : 'NULL';
	}

	public function setDemosCompleted( $intDemosCompleted ) {
		$this->set( 'm_intDemosCompleted', CStrings::strToIntDef( $intDemosCompleted, NULL, false ) );
	}

	public function getDemosCompleted() {
		return $this->m_intDemosCompleted;
	}

	public function sqlDemosCompleted() {
		return ( true == isset( $this->m_intDemosCompleted ) ) ? ( string ) $this->m_intDemosCompleted : 'NULL';
	}

	public function setDemosForgotten( $intDemosForgotten ) {
		$this->set( 'm_intDemosForgotten', CStrings::strToIntDef( $intDemosForgotten, NULL, false ) );
	}

	public function getDemosForgotten() {
		return $this->m_intDemosForgotten;
	}

	public function sqlDemosForgotten() {
		return ( true == isset( $this->m_intDemosForgotten ) ) ? ( string ) $this->m_intDemosForgotten : 'NULL';
	}

	public function setCommissionsScheduled( $intCommissionsScheduled ) {
		$this->set( 'm_intCommissionsScheduled', CStrings::strToIntDef( $intCommissionsScheduled, NULL, false ) );
	}

	public function getCommissionsScheduled() {
		return $this->m_intCommissionsScheduled;
	}

	public function sqlCommissionsScheduled() {
		return ( true == isset( $this->m_intCommissionsScheduled ) ) ? ( string ) $this->m_intCommissionsScheduled : 'NULL';
	}

	public function setCommissionsPaid( $intCommissionsPaid ) {
		$this->set( 'm_intCommissionsPaid', CStrings::strToIntDef( $intCommissionsPaid, NULL, false ) );
	}

	public function getCommissionsPaid() {
		return $this->m_intCommissionsPaid;
	}

	public function sqlCommissionsPaid() {
		return ( true == isset( $this->m_intCommissionsPaid ) ) ? ( string ) $this->m_intCommissionsPaid : 'NULL';
	}

	public function setPipelineNewLeads( $intPipelineNewLeads ) {
		$this->set( 'm_intPipelineNewLeads', CStrings::strToIntDef( $intPipelineNewLeads, NULL, false ) );
	}

	public function getPipelineNewLeads() {
		return $this->m_intPipelineNewLeads;
	}

	public function sqlPipelineNewLeads() {
		return ( true == isset( $this->m_intPipelineNewLeads ) ) ? ( string ) $this->m_intPipelineNewLeads : 'NULL';
	}

	public function setPipelineLeads( $intPipelineLeads ) {
		$this->set( 'm_intPipelineLeads', CStrings::strToIntDef( $intPipelineLeads, NULL, false ) );
	}

	public function getPipelineLeads() {
		return $this->m_intPipelineLeads;
	}

	public function sqlPipelineLeads() {
		return ( true == isset( $this->m_intPipelineLeads ) ) ? ( string ) $this->m_intPipelineLeads : 'NULL';
	}

	public function setPipelineWonLeads( $intPipelineWonLeads ) {
		$this->set( 'm_intPipelineWonLeads', CStrings::strToIntDef( $intPipelineWonLeads, NULL, false ) );
	}

	public function getPipelineWonLeads() {
		return $this->m_intPipelineWonLeads;
	}

	public function sqlPipelineWonLeads() {
		return ( true == isset( $this->m_intPipelineWonLeads ) ) ? ( string ) $this->m_intPipelineWonLeads : 'NULL';
	}

	public function setPipelineLostLeads( $intPipelineLostLeads ) {
		$this->set( 'm_intPipelineLostLeads', CStrings::strToIntDef( $intPipelineLostLeads, NULL, false ) );
	}

	public function getPipelineLostLeads() {
		return $this->m_intPipelineLostLeads;
	}

	public function sqlPipelineLostLeads() {
		return ( true == isset( $this->m_intPipelineLostLeads ) ) ? ( string ) $this->m_intPipelineLostLeads : 'NULL';
	}

	public function setPipelineNewUnits( $intPipelineNewUnits ) {
		$this->set( 'm_intPipelineNewUnits', CStrings::strToIntDef( $intPipelineNewUnits, NULL, false ) );
	}

	public function getPipelineNewUnits() {
		return $this->m_intPipelineNewUnits;
	}

	public function sqlPipelineNewUnits() {
		return ( true == isset( $this->m_intPipelineNewUnits ) ) ? ( string ) $this->m_intPipelineNewUnits : 'NULL';
	}

	public function setPipelineUnits( $intPipelineUnits ) {
		$this->set( 'm_intPipelineUnits', CStrings::strToIntDef( $intPipelineUnits, NULL, false ) );
	}

	public function getPipelineUnits() {
		return $this->m_intPipelineUnits;
	}

	public function sqlPipelineUnits() {
		return ( true == isset( $this->m_intPipelineUnits ) ) ? ( string ) $this->m_intPipelineUnits : 'NULL';
	}

	public function setPipelineWonUnits( $intPipelineWonUnits ) {
		$this->set( 'm_intPipelineWonUnits', CStrings::strToIntDef( $intPipelineWonUnits, NULL, false ) );
	}

	public function getPipelineWonUnits() {
		return $this->m_intPipelineWonUnits;
	}

	public function sqlPipelineWonUnits() {
		return ( true == isset( $this->m_intPipelineWonUnits ) ) ? ( string ) $this->m_intPipelineWonUnits : 'NULL';
	}

	public function setPipelineLostUnits( $intPipelineLostUnits ) {
		$this->set( 'm_intPipelineLostUnits', CStrings::strToIntDef( $intPipelineLostUnits, NULL, false ) );
	}

	public function getPipelineLostUnits() {
		return $this->m_intPipelineLostUnits;
	}

	public function sqlPipelineLostUnits() {
		return ( true == isset( $this->m_intPipelineLostUnits ) ) ? ( string ) $this->m_intPipelineLostUnits : 'NULL';
	}

	public function setPipelineNewAcv( $intPipelineNewAcv ) {
		$this->set( 'm_intPipelineNewAcv', CStrings::strToIntDef( $intPipelineNewAcv, NULL, false ) );
	}

	public function getPipelineNewAcv() {
		return $this->m_intPipelineNewAcv;
	}

	public function sqlPipelineNewAcv() {
		return ( true == isset( $this->m_intPipelineNewAcv ) ) ? ( string ) $this->m_intPipelineNewAcv : 'NULL';
	}

	public function setPipelineAcv( $intPipelineAcv ) {
		$this->set( 'm_intPipelineAcv', CStrings::strToIntDef( $intPipelineAcv, NULL, false ) );
	}

	public function getPipelineAcv() {
		return $this->m_intPipelineAcv;
	}

	public function sqlPipelineAcv() {
		return ( true == isset( $this->m_intPipelineAcv ) ) ? ( string ) $this->m_intPipelineAcv : 'NULL';
	}

	public function setPipelineWonAcv( $intPipelineWonAcv ) {
		$this->set( 'm_intPipelineWonAcv', CStrings::strToIntDef( $intPipelineWonAcv, NULL, false ) );
	}

	public function getPipelineWonAcv() {
		return $this->m_intPipelineWonAcv;
	}

	public function sqlPipelineWonAcv() {
		return ( true == isset( $this->m_intPipelineWonAcv ) ) ? ( string ) $this->m_intPipelineWonAcv : 'NULL';
	}

	public function setPipelineLostAcv( $intPipelineLostAcv ) {
		$this->set( 'm_intPipelineLostAcv', CStrings::strToIntDef( $intPipelineLostAcv, NULL, false ) );
	}

	public function getPipelineLostAcv() {
		return $this->m_intPipelineLostAcv;
	}

	public function sqlPipelineLostAcv() {
		return ( true == isset( $this->m_intPipelineLostAcv ) ) ? ( string ) $this->m_intPipelineLostAcv : 'NULL';
	}

	public function setPipelineNewProposals( $intPipelineNewProposals ) {
		$this->set( 'm_intPipelineNewProposals', CStrings::strToIntDef( $intPipelineNewProposals, NULL, false ) );
	}

	public function getPipelineNewProposals() {
		return $this->m_intPipelineNewProposals;
	}

	public function sqlPipelineNewProposals() {
		return ( true == isset( $this->m_intPipelineNewProposals ) ) ? ( string ) $this->m_intPipelineNewProposals : 'NULL';
	}

	public function setCommittedAcv( $intCommittedAcv ) {
		$this->set( 'm_intCommittedAcv', CStrings::strToIntDef( $intCommittedAcv, NULL, false ) );
	}

	public function getCommittedAcv() {
		return $this->m_intCommittedAcv;
	}

	public function sqlCommittedAcv() {
		return ( true == isset( $this->m_intCommittedAcv ) ) ? ( string ) $this->m_intCommittedAcv : 'NULL';
	}

	public function setNewBugSeverity( $intNewBugSeverity ) {
		$this->set( 'm_intNewBugSeverity', CStrings::strToIntDef( $intNewBugSeverity, NULL, false ) );
	}

	public function getNewBugSeverity() {
		return $this->m_intNewBugSeverity;
	}

	public function sqlNewBugSeverity() {
		return ( true == isset( $this->m_intNewBugSeverity ) ) ? ( string ) $this->m_intNewBugSeverity : 'NULL';
	}

	public function setNewBugs( $intNewBugs ) {
		$this->set( 'm_intNewBugs', CStrings::strToIntDef( $intNewBugs, NULL, false ) );
	}

	public function getNewBugs() {
		return $this->m_intNewBugs;
	}

	public function sqlNewBugs() {
		return ( true == isset( $this->m_intNewBugs ) ) ? ( string ) $this->m_intNewBugs : 'NULL';
	}

	public function setClosedBugs( $intClosedBugs ) {
		$this->set( 'm_intClosedBugs', CStrings::strToIntDef( $intClosedBugs, NULL, false ) );
	}

	public function getClosedBugs() {
		return $this->m_intClosedBugs;
	}

	public function sqlClosedBugs() {
		return ( true == isset( $this->m_intClosedBugs ) ) ? ( string ) $this->m_intClosedBugs : 'NULL';
	}

	public function setOpenBugs( $intOpenBugs ) {
		$this->set( 'm_intOpenBugs', CStrings::strToIntDef( $intOpenBugs, NULL, false ) );
	}

	public function getOpenBugs() {
		return $this->m_intOpenBugs;
	}

	public function sqlOpenBugs() {
		return ( true == isset( $this->m_intOpenBugs ) ) ? ( string ) $this->m_intOpenBugs : 'NULL';
	}

	public function setNewFeatureRequests( $intNewFeatureRequests ) {
		$this->set( 'm_intNewFeatureRequests', CStrings::strToIntDef( $intNewFeatureRequests, NULL, false ) );
	}

	public function getNewFeatureRequests() {
		return $this->m_intNewFeatureRequests;
	}

	public function sqlNewFeatureRequests() {
		return ( true == isset( $this->m_intNewFeatureRequests ) ) ? ( string ) $this->m_intNewFeatureRequests : 'NULL';
	}

	public function setClosedFeatureRequests( $intClosedFeatureRequests ) {
		$this->set( 'm_intClosedFeatureRequests', CStrings::strToIntDef( $intClosedFeatureRequests, NULL, false ) );
	}

	public function getClosedFeatureRequests() {
		return $this->m_intClosedFeatureRequests;
	}

	public function sqlClosedFeatureRequests() {
		return ( true == isset( $this->m_intClosedFeatureRequests ) ) ? ( string ) $this->m_intClosedFeatureRequests : 'NULL';
	}

	public function setOpenFeatureRequests( $intOpenFeatureRequests ) {
		$this->set( 'm_intOpenFeatureRequests', CStrings::strToIntDef( $intOpenFeatureRequests, NULL, false ) );
	}

	public function getOpenFeatureRequests() {
		return $this->m_intOpenFeatureRequests;
	}

	public function sqlOpenFeatureRequests() {
		return ( true == isset( $this->m_intOpenFeatureRequests ) ) ? ( string ) $this->m_intOpenFeatureRequests : 'NULL';
	}

	public function setNewSupportTickets( $intNewSupportTickets ) {
		$this->set( 'm_intNewSupportTickets', CStrings::strToIntDef( $intNewSupportTickets, NULL, false ) );
	}

	public function getNewSupportTickets() {
		return $this->m_intNewSupportTickets;
	}

	public function sqlNewSupportTickets() {
		return ( true == isset( $this->m_intNewSupportTickets ) ) ? ( string ) $this->m_intNewSupportTickets : 'NULL';
	}

	public function setClosedSupportTickets( $intClosedSupportTickets ) {
		$this->set( 'm_intClosedSupportTickets', CStrings::strToIntDef( $intClosedSupportTickets, NULL, false ) );
	}

	public function getClosedSupportTickets() {
		return $this->m_intClosedSupportTickets;
	}

	public function sqlClosedSupportTickets() {
		return ( true == isset( $this->m_intClosedSupportTickets ) ) ? ( string ) $this->m_intClosedSupportTickets : 'NULL';
	}

	public function setOpenSupportTickets( $intOpenSupportTickets ) {
		$this->set( 'm_intOpenSupportTickets', CStrings::strToIntDef( $intOpenSupportTickets, NULL, false ) );
	}

	public function getOpenSupportTickets() {
		return $this->m_intOpenSupportTickets;
	}

	public function sqlOpenSupportTickets() {
		return ( true == isset( $this->m_intOpenSupportTickets ) ) ? ( string ) $this->m_intOpenSupportTickets : 'NULL';
	}

	public function setClientImpactfulReleasedTasks( $intClientImpactfulReleasedTasks ) {
		$this->set( 'm_intClientImpactfulReleasedTasks', CStrings::strToIntDef( $intClientImpactfulReleasedTasks, NULL, false ) );
	}

	public function getClientImpactfulReleasedTasks() {
		return $this->m_intClientImpactfulReleasedTasks;
	}

	public function sqlClientImpactfulReleasedTasks() {
		return ( true == isset( $this->m_intClientImpactfulReleasedTasks ) ) ? ( string ) $this->m_intClientImpactfulReleasedTasks : 'NULL';
	}

	public function setReleasedTasksWithoutStoryPoints( $intReleasedTasksWithoutStoryPoints ) {
		$this->set( 'm_intReleasedTasksWithoutStoryPoints', CStrings::strToIntDef( $intReleasedTasksWithoutStoryPoints, NULL, false ) );
	}

	public function getReleasedTasksWithoutStoryPoints() {
		return $this->m_intReleasedTasksWithoutStoryPoints;
	}

	public function sqlReleasedTasksWithoutStoryPoints() {
		return ( true == isset( $this->m_intReleasedTasksWithoutStoryPoints ) ) ? ( string ) $this->m_intReleasedTasksWithoutStoryPoints : 'NULL';
	}

	public function setStoryPointsReleased( $intStoryPointsReleased ) {
		$this->set( 'm_intStoryPointsReleased', CStrings::strToIntDef( $intStoryPointsReleased, NULL, false ) );
	}

	public function getStoryPointsReleased() {
		return $this->m_intStoryPointsReleased;
	}

	public function sqlStoryPointsReleased() {
		return ( true == isset( $this->m_intStoryPointsReleased ) ) ? ( string ) $this->m_intStoryPointsReleased : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, month, new_clients, lost_clients, net_client_change, cumulative_clients, subscription_revenue, transaction_revenue, one_time_revenue, write_off_revenue, total_revenue, quarter_revenue_growth_percent, lost_revenue_unimplemented, lost_revenue_unhappy, lost_revenue_management, lost_revenue_transfer_property, total_lost_revenue, new_logo_units, new_product_units, new_property_units, new_transfer_property_units, total_new_units, lost_units_unhappy, lost_units_management, lost_units_unimplemented, lost_units_transfer_property, total_lost_units, net_unit_change, cumulative_units, revenue_per_unit, new_logo_subscription_acv, new_product_subscription_acv, new_property_subscription_acv, new_renewal_subscription_acv, new_transfer_property_subscription_acv, total_new_subscription_acv, total_new_active_subscription_acv, total_new_adjusted_subscription_acv, total_new_active_adjusted_subscription_acv, new_logo_transaction_acv, new_product_transaction_acv, new_property_transaction_acv, new_renewal_transaction_acv, new_transfer_property_transaction_acv, total_new_transaction_acv, total_new_active_transaction_acv, total_new_adjusted_transaction_acv, total_new_active_adjusted_transaction_acv, total_new_acv, total_new_active_acv, lost_subscription_acv_unhappy, lost_subscription_acv_unimplemented, lost_subscription_acv_transfer_property, lost_subscription_acv_management, total_lost_subscription_acv, lost_transaction_acv_unhappy, lost_transaction_acv_unimplemented, lost_transaction_acv_transfer_property, lost_transaction_acv_management, total_lost_transaction_acv, total_lost_acv, lost_acv_unhappy, lost_acv_unimplemented, lost_acv_transfer_property, lost_acv_management, net_acv_change, cumulative_acv, mcv_per_unit, new_logo_implementation, new_product_implementation, new_property_implementation, new_renewal_implementation, new_transfer_property_implementation, pilot_new_subscription_acv, pilot_new_transaction_acv, pilot_pending_subscription_acv, pilot_pending_transaction_acv, pilot_won_subscription_acv, pilot_won_transaction_acv, pilot_lost_subscription_acv, pilot_lost_transaction_acv, pilot_open_count, unimplemented_open_days, unimplemented_subscription_acv, unimplemented_transaction_acv, unimplemented_acv, unimplemented_units, implemented_subscription_acv, implemented_transaction_acv, implemented_acv, implemented_units, unbilled_open_days, unbilled_acv, unbilled_units, billed_acv, billed_units, demos_scheduled, demoed_units, demos_completed, demos_forgotten, commissions_scheduled, commissions_paid, pipeline_new_leads, pipeline_leads, pipeline_won_leads, pipeline_lost_leads, pipeline_new_units, pipeline_units, pipeline_won_units, pipeline_lost_units, pipeline_new_acv, pipeline_acv, pipeline_won_acv, pipeline_lost_acv, pipeline_new_proposals, committed_acv, new_bug_severity, new_bugs, closed_bugs, open_bugs, new_feature_requests, closed_feature_requests, open_feature_requests, new_support_tickets, closed_support_tickets, open_support_tickets, client_impactful_released_tasks, released_tasks_without_story_points, story_points_released, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlMonth() . ', ' .
 						$this->sqlNewClients() . ', ' .
 						$this->sqlLostClients() . ', ' .
 						$this->sqlNetClientChange() . ', ' .
 						$this->sqlCumulativeClients() . ', ' .
 						$this->sqlSubscriptionRevenue() . ', ' .
 						$this->sqlTransactionRevenue() . ', ' .
 						$this->sqlOneTimeRevenue() . ', ' .
 						$this->sqlWriteOffRevenue() . ', ' .
 						$this->sqlTotalRevenue() . ', ' .
 						$this->sqlQuarterRevenueGrowthPercent() . ', ' .
 						$this->sqlLostRevenueUnimplemented() . ', ' .
 						$this->sqlLostRevenueUnhappy() . ', ' .
 						$this->sqlLostRevenueManagement() . ', ' .
 						$this->sqlLostRevenueTransferProperty() . ', ' .
 						$this->sqlTotalLostRevenue() . ', ' .
 						$this->sqlNewLogoUnits() . ', ' .
 						$this->sqlNewProductUnits() . ', ' .
 						$this->sqlNewPropertyUnits() . ', ' .
 						$this->sqlNewTransferPropertyUnits() . ', ' .
 						$this->sqlTotalNewUnits() . ', ' .
 						$this->sqlLostUnitsUnhappy() . ', ' .
 						$this->sqlLostUnitsManagement() . ', ' .
 						$this->sqlLostUnitsUnimplemented() . ', ' .
 						$this->sqlLostUnitsTransferProperty() . ', ' .
 						$this->sqlTotalLostUnits() . ', ' .
 						$this->sqlNetUnitChange() . ', ' .
 						$this->sqlCumulativeUnits() . ', ' .
 						$this->sqlRevenuePerUnit() . ', ' .
 						$this->sqlNewLogoSubscriptionAcv() . ', ' .
 						$this->sqlNewProductSubscriptionAcv() . ', ' .
 						$this->sqlNewPropertySubscriptionAcv() . ', ' .
 						$this->sqlNewRenewalSubscriptionAcv() . ', ' .
 						$this->sqlNewTransferPropertySubscriptionAcv() . ', ' .
 						$this->sqlTotalNewSubscriptionAcv() . ', ' .
 						$this->sqlTotalNewActiveSubscriptionAcv() . ', ' .
 						$this->sqlTotalNewAdjustedSubscriptionAcv() . ', ' .
 						$this->sqlTotalNewActiveAdjustedSubscriptionAcv() . ', ' .
 						$this->sqlNewLogoTransactionAcv() . ', ' .
 						$this->sqlNewProductTransactionAcv() . ', ' .
 						$this->sqlNewPropertyTransactionAcv() . ', ' .
 						$this->sqlNewRenewalTransactionAcv() . ', ' .
 						$this->sqlNewTransferPropertyTransactionAcv() . ', ' .
 						$this->sqlTotalNewTransactionAcv() . ', ' .
 						$this->sqlTotalNewActiveTransactionAcv() . ', ' .
 						$this->sqlTotalNewAdjustedTransactionAcv() . ', ' .
 						$this->sqlTotalNewActiveAdjustedTransactionAcv() . ', ' .
 						$this->sqlTotalNewAcv() . ', ' .
 						$this->sqlTotalNewActiveAcv() . ', ' .
 						$this->sqlLostSubscriptionAcvUnhappy() . ', ' .
 						$this->sqlLostSubscriptionAcvUnimplemented() . ', ' .
 						$this->sqlLostSubscriptionAcvTransferProperty() . ', ' .
 						$this->sqlLostSubscriptionAcvManagement() . ', ' .
 						$this->sqlTotalLostSubscriptionAcv() . ', ' .
 						$this->sqlLostTransactionAcvUnhappy() . ', ' .
 						$this->sqlLostTransactionAcvUnimplemented() . ', ' .
 						$this->sqlLostTransactionAcvTransferProperty() . ', ' .
 						$this->sqlLostTransactionAcvManagement() . ', ' .
 						$this->sqlTotalLostTransactionAcv() . ', ' .
 						$this->sqlTotalLostAcv() . ', ' .
 						$this->sqlLostAcvUnhappy() . ', ' .
 						$this->sqlLostAcvUnimplemented() . ', ' .
 						$this->sqlLostAcvTransferProperty() . ', ' .
 						$this->sqlLostAcvManagement() . ', ' .
 						$this->sqlNetAcvChange() . ', ' .
 						$this->sqlCumulativeAcv() . ', ' .
 						$this->sqlMcvPerUnit() . ', ' .
 						$this->sqlNewLogoImplementation() . ', ' .
 						$this->sqlNewProductImplementation() . ', ' .
 						$this->sqlNewPropertyImplementation() . ', ' .
 						$this->sqlNewRenewalImplementation() . ', ' .
 						$this->sqlNewTransferPropertyImplementation() . ', ' .
 						$this->sqlPilotNewSubscriptionAcv() . ', ' .
 						$this->sqlPilotNewTransactionAcv() . ', ' .
 						$this->sqlPilotPendingSubscriptionAcv() . ', ' .
 						$this->sqlPilotPendingTransactionAcv() . ', ' .
 						$this->sqlPilotWonSubscriptionAcv() . ', ' .
 						$this->sqlPilotWonTransactionAcv() . ', ' .
 						$this->sqlPilotLostSubscriptionAcv() . ', ' .
 						$this->sqlPilotLostTransactionAcv() . ', ' .
 						$this->sqlPilotOpenCount() . ', ' .
 						$this->sqlUnimplementedOpenDays() . ', ' .
 						$this->sqlUnimplementedSubscriptionAcv() . ', ' .
 						$this->sqlUnimplementedTransactionAcv() . ', ' .
 						$this->sqlUnimplementedAcv() . ', ' .
 						$this->sqlUnimplementedUnits() . ', ' .
 						$this->sqlImplementedSubscriptionAcv() . ', ' .
 						$this->sqlImplementedTransactionAcv() . ', ' .
 						$this->sqlImplementedAcv() . ', ' .
 						$this->sqlImplementedUnits() . ', ' .
 						$this->sqlUnbilledOpenDays() . ', ' .
 						$this->sqlUnbilledAcv() . ', ' .
 						$this->sqlUnbilledUnits() . ', ' .
 						$this->sqlBilledAcv() . ', ' .
 						$this->sqlBilledUnits() . ', ' .
 						$this->sqlDemosScheduled() . ', ' .
 						$this->sqlDemoedUnits() . ', ' .
 						$this->sqlDemosCompleted() . ', ' .
 						$this->sqlDemosForgotten() . ', ' .
 						$this->sqlCommissionsScheduled() . ', ' .
 						$this->sqlCommissionsPaid() . ', ' .
 						$this->sqlPipelineNewLeads() . ', ' .
 						$this->sqlPipelineLeads() . ', ' .
 						$this->sqlPipelineWonLeads() . ', ' .
 						$this->sqlPipelineLostLeads() . ', ' .
 						$this->sqlPipelineNewUnits() . ', ' .
 						$this->sqlPipelineUnits() . ', ' .
 						$this->sqlPipelineWonUnits() . ', ' .
 						$this->sqlPipelineLostUnits() . ', ' .
 						$this->sqlPipelineNewAcv() . ', ' .
 						$this->sqlPipelineAcv() . ', ' .
 						$this->sqlPipelineWonAcv() . ', ' .
 						$this->sqlPipelineLostAcv() . ', ' .
 						$this->sqlPipelineNewProposals() . ', ' .
 						$this->sqlCommittedAcv() . ', ' .
 						$this->sqlNewBugSeverity() . ', ' .
 						$this->sqlNewBugs() . ', ' .
 						$this->sqlClosedBugs() . ', ' .
 						$this->sqlOpenBugs() . ', ' .
 						$this->sqlNewFeatureRequests() . ', ' .
 						$this->sqlClosedFeatureRequests() . ', ' .
 						$this->sqlOpenFeatureRequests() . ', ' .
 						$this->sqlNewSupportTickets() . ', ' .
 						$this->sqlClosedSupportTickets() . ', ' .
 						$this->sqlOpenSupportTickets() . ', ' .
 						$this->sqlClientImpactfulReleasedTasks() . ', ' .
 						$this->sqlReleasedTasksWithoutStoryPoints() . ', ' .
 						$this->sqlStoryPointsReleased() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_clients = ' . $this->sqlNewClients() . ','; } elseif( true == array_key_exists( 'NewClients', $this->getChangedColumns() ) ) { $strSql .= ' new_clients = ' . $this->sqlNewClients() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_clients = ' . $this->sqlLostClients() . ','; } elseif( true == array_key_exists( 'LostClients', $this->getChangedColumns() ) ) { $strSql .= ' lost_clients = ' . $this->sqlLostClients() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' net_client_change = ' . $this->sqlNetClientChange() . ','; } elseif( true == array_key_exists( 'NetClientChange', $this->getChangedColumns() ) ) { $strSql .= ' net_client_change = ' . $this->sqlNetClientChange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cumulative_clients = ' . $this->sqlCumulativeClients() . ','; } elseif( true == array_key_exists( 'CumulativeClients', $this->getChangedColumns() ) ) { $strSql .= ' cumulative_clients = ' . $this->sqlCumulativeClients() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subscription_revenue = ' . $this->sqlSubscriptionRevenue() . ','; } elseif( true == array_key_exists( 'SubscriptionRevenue', $this->getChangedColumns() ) ) { $strSql .= ' subscription_revenue = ' . $this->sqlSubscriptionRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_revenue = ' . $this->sqlTransactionRevenue() . ','; } elseif( true == array_key_exists( 'TransactionRevenue', $this->getChangedColumns() ) ) { $strSql .= ' transaction_revenue = ' . $this->sqlTransactionRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' one_time_revenue = ' . $this->sqlOneTimeRevenue() . ','; } elseif( true == array_key_exists( 'OneTimeRevenue', $this->getChangedColumns() ) ) { $strSql .= ' one_time_revenue = ' . $this->sqlOneTimeRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' write_off_revenue = ' . $this->sqlWriteOffRevenue() . ','; } elseif( true == array_key_exists( 'WriteOffRevenue', $this->getChangedColumns() ) ) { $strSql .= ' write_off_revenue = ' . $this->sqlWriteOffRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_revenue = ' . $this->sqlTotalRevenue() . ','; } elseif( true == array_key_exists( 'TotalRevenue', $this->getChangedColumns() ) ) { $strSql .= ' total_revenue = ' . $this->sqlTotalRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quarter_revenue_growth_percent = ' . $this->sqlQuarterRevenueGrowthPercent() . ','; } elseif( true == array_key_exists( 'QuarterRevenueGrowthPercent', $this->getChangedColumns() ) ) { $strSql .= ' quarter_revenue_growth_percent = ' . $this->sqlQuarterRevenueGrowthPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_revenue_unimplemented = ' . $this->sqlLostRevenueUnimplemented() . ','; } elseif( true == array_key_exists( 'LostRevenueUnimplemented', $this->getChangedColumns() ) ) { $strSql .= ' lost_revenue_unimplemented = ' . $this->sqlLostRevenueUnimplemented() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_revenue_unhappy = ' . $this->sqlLostRevenueUnhappy() . ','; } elseif( true == array_key_exists( 'LostRevenueUnhappy', $this->getChangedColumns() ) ) { $strSql .= ' lost_revenue_unhappy = ' . $this->sqlLostRevenueUnhappy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_revenue_management = ' . $this->sqlLostRevenueManagement() . ','; } elseif( true == array_key_exists( 'LostRevenueManagement', $this->getChangedColumns() ) ) { $strSql .= ' lost_revenue_management = ' . $this->sqlLostRevenueManagement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_revenue_transfer_property = ' . $this->sqlLostRevenueTransferProperty() . ','; } elseif( true == array_key_exists( 'LostRevenueTransferProperty', $this->getChangedColumns() ) ) { $strSql .= ' lost_revenue_transfer_property = ' . $this->sqlLostRevenueTransferProperty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_lost_revenue = ' . $this->sqlTotalLostRevenue() . ','; } elseif( true == array_key_exists( 'TotalLostRevenue', $this->getChangedColumns() ) ) { $strSql .= ' total_lost_revenue = ' . $this->sqlTotalLostRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_logo_units = ' . $this->sqlNewLogoUnits() . ','; } elseif( true == array_key_exists( 'NewLogoUnits', $this->getChangedColumns() ) ) { $strSql .= ' new_logo_units = ' . $this->sqlNewLogoUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_product_units = ' . $this->sqlNewProductUnits() . ','; } elseif( true == array_key_exists( 'NewProductUnits', $this->getChangedColumns() ) ) { $strSql .= ' new_product_units = ' . $this->sqlNewProductUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_property_units = ' . $this->sqlNewPropertyUnits() . ','; } elseif( true == array_key_exists( 'NewPropertyUnits', $this->getChangedColumns() ) ) { $strSql .= ' new_property_units = ' . $this->sqlNewPropertyUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_transfer_property_units = ' . $this->sqlNewTransferPropertyUnits() . ','; } elseif( true == array_key_exists( 'NewTransferPropertyUnits', $this->getChangedColumns() ) ) { $strSql .= ' new_transfer_property_units = ' . $this->sqlNewTransferPropertyUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_new_units = ' . $this->sqlTotalNewUnits() . ','; } elseif( true == array_key_exists( 'TotalNewUnits', $this->getChangedColumns() ) ) { $strSql .= ' total_new_units = ' . $this->sqlTotalNewUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_units_unhappy = ' . $this->sqlLostUnitsUnhappy() . ','; } elseif( true == array_key_exists( 'LostUnitsUnhappy', $this->getChangedColumns() ) ) { $strSql .= ' lost_units_unhappy = ' . $this->sqlLostUnitsUnhappy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_units_management = ' . $this->sqlLostUnitsManagement() . ','; } elseif( true == array_key_exists( 'LostUnitsManagement', $this->getChangedColumns() ) ) { $strSql .= ' lost_units_management = ' . $this->sqlLostUnitsManagement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_units_unimplemented = ' . $this->sqlLostUnitsUnimplemented() . ','; } elseif( true == array_key_exists( 'LostUnitsUnimplemented', $this->getChangedColumns() ) ) { $strSql .= ' lost_units_unimplemented = ' . $this->sqlLostUnitsUnimplemented() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_units_transfer_property = ' . $this->sqlLostUnitsTransferProperty() . ','; } elseif( true == array_key_exists( 'LostUnitsTransferProperty', $this->getChangedColumns() ) ) { $strSql .= ' lost_units_transfer_property = ' . $this->sqlLostUnitsTransferProperty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_lost_units = ' . $this->sqlTotalLostUnits() . ','; } elseif( true == array_key_exists( 'TotalLostUnits', $this->getChangedColumns() ) ) { $strSql .= ' total_lost_units = ' . $this->sqlTotalLostUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' net_unit_change = ' . $this->sqlNetUnitChange() . ','; } elseif( true == array_key_exists( 'NetUnitChange', $this->getChangedColumns() ) ) { $strSql .= ' net_unit_change = ' . $this->sqlNetUnitChange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cumulative_units = ' . $this->sqlCumulativeUnits() . ','; } elseif( true == array_key_exists( 'CumulativeUnits', $this->getChangedColumns() ) ) { $strSql .= ' cumulative_units = ' . $this->sqlCumulativeUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revenue_per_unit = ' . $this->sqlRevenuePerUnit() . ','; } elseif( true == array_key_exists( 'RevenuePerUnit', $this->getChangedColumns() ) ) { $strSql .= ' revenue_per_unit = ' . $this->sqlRevenuePerUnit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_logo_subscription_acv = ' . $this->sqlNewLogoSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'NewLogoSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_logo_subscription_acv = ' . $this->sqlNewLogoSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_product_subscription_acv = ' . $this->sqlNewProductSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'NewProductSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_product_subscription_acv = ' . $this->sqlNewProductSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_property_subscription_acv = ' . $this->sqlNewPropertySubscriptionAcv() . ','; } elseif( true == array_key_exists( 'NewPropertySubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_property_subscription_acv = ' . $this->sqlNewPropertySubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_renewal_subscription_acv = ' . $this->sqlNewRenewalSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'NewRenewalSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_renewal_subscription_acv = ' . $this->sqlNewRenewalSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_transfer_property_subscription_acv = ' . $this->sqlNewTransferPropertySubscriptionAcv() . ','; } elseif( true == array_key_exists( 'NewTransferPropertySubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_transfer_property_subscription_acv = ' . $this->sqlNewTransferPropertySubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_new_subscription_acv = ' . $this->sqlTotalNewSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'TotalNewSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_new_subscription_acv = ' . $this->sqlTotalNewSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_new_active_subscription_acv = ' . $this->sqlTotalNewActiveSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'TotalNewActiveSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_new_active_subscription_acv = ' . $this->sqlTotalNewActiveSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_new_adjusted_subscription_acv = ' . $this->sqlTotalNewAdjustedSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'TotalNewAdjustedSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_new_adjusted_subscription_acv = ' . $this->sqlTotalNewAdjustedSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_new_active_adjusted_subscription_acv = ' . $this->sqlTotalNewActiveAdjustedSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'TotalNewActiveAdjustedSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_new_active_adjusted_subscription_acv = ' . $this->sqlTotalNewActiveAdjustedSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_logo_transaction_acv = ' . $this->sqlNewLogoTransactionAcv() . ','; } elseif( true == array_key_exists( 'NewLogoTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_logo_transaction_acv = ' . $this->sqlNewLogoTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_product_transaction_acv = ' . $this->sqlNewProductTransactionAcv() . ','; } elseif( true == array_key_exists( 'NewProductTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_product_transaction_acv = ' . $this->sqlNewProductTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_property_transaction_acv = ' . $this->sqlNewPropertyTransactionAcv() . ','; } elseif( true == array_key_exists( 'NewPropertyTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_property_transaction_acv = ' . $this->sqlNewPropertyTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_renewal_transaction_acv = ' . $this->sqlNewRenewalTransactionAcv() . ','; } elseif( true == array_key_exists( 'NewRenewalTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_renewal_transaction_acv = ' . $this->sqlNewRenewalTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_transfer_property_transaction_acv = ' . $this->sqlNewTransferPropertyTransactionAcv() . ','; } elseif( true == array_key_exists( 'NewTransferPropertyTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_transfer_property_transaction_acv = ' . $this->sqlNewTransferPropertyTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_new_transaction_acv = ' . $this->sqlTotalNewTransactionAcv() . ','; } elseif( true == array_key_exists( 'TotalNewTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_new_transaction_acv = ' . $this->sqlTotalNewTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_new_active_transaction_acv = ' . $this->sqlTotalNewActiveTransactionAcv() . ','; } elseif( true == array_key_exists( 'TotalNewActiveTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_new_active_transaction_acv = ' . $this->sqlTotalNewActiveTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_new_adjusted_transaction_acv = ' . $this->sqlTotalNewAdjustedTransactionAcv() . ','; } elseif( true == array_key_exists( 'TotalNewAdjustedTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_new_adjusted_transaction_acv = ' . $this->sqlTotalNewAdjustedTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_new_active_adjusted_transaction_acv = ' . $this->sqlTotalNewActiveAdjustedTransactionAcv() . ','; } elseif( true == array_key_exists( 'TotalNewActiveAdjustedTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_new_active_adjusted_transaction_acv = ' . $this->sqlTotalNewActiveAdjustedTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_new_acv = ' . $this->sqlTotalNewAcv() . ','; } elseif( true == array_key_exists( 'TotalNewAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_new_acv = ' . $this->sqlTotalNewAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_new_active_acv = ' . $this->sqlTotalNewActiveAcv() . ','; } elseif( true == array_key_exists( 'TotalNewActiveAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_new_active_acv = ' . $this->sqlTotalNewActiveAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_subscription_acv_unhappy = ' . $this->sqlLostSubscriptionAcvUnhappy() . ','; } elseif( true == array_key_exists( 'LostSubscriptionAcvUnhappy', $this->getChangedColumns() ) ) { $strSql .= ' lost_subscription_acv_unhappy = ' . $this->sqlLostSubscriptionAcvUnhappy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_subscription_acv_unimplemented = ' . $this->sqlLostSubscriptionAcvUnimplemented() . ','; } elseif( true == array_key_exists( 'LostSubscriptionAcvUnimplemented', $this->getChangedColumns() ) ) { $strSql .= ' lost_subscription_acv_unimplemented = ' . $this->sqlLostSubscriptionAcvUnimplemented() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_subscription_acv_transfer_property = ' . $this->sqlLostSubscriptionAcvTransferProperty() . ','; } elseif( true == array_key_exists( 'LostSubscriptionAcvTransferProperty', $this->getChangedColumns() ) ) { $strSql .= ' lost_subscription_acv_transfer_property = ' . $this->sqlLostSubscriptionAcvTransferProperty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_subscription_acv_management = ' . $this->sqlLostSubscriptionAcvManagement() . ','; } elseif( true == array_key_exists( 'LostSubscriptionAcvManagement', $this->getChangedColumns() ) ) { $strSql .= ' lost_subscription_acv_management = ' . $this->sqlLostSubscriptionAcvManagement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_lost_subscription_acv = ' . $this->sqlTotalLostSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'TotalLostSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_lost_subscription_acv = ' . $this->sqlTotalLostSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_transaction_acv_unhappy = ' . $this->sqlLostTransactionAcvUnhappy() . ','; } elseif( true == array_key_exists( 'LostTransactionAcvUnhappy', $this->getChangedColumns() ) ) { $strSql .= ' lost_transaction_acv_unhappy = ' . $this->sqlLostTransactionAcvUnhappy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_transaction_acv_unimplemented = ' . $this->sqlLostTransactionAcvUnimplemented() . ','; } elseif( true == array_key_exists( 'LostTransactionAcvUnimplemented', $this->getChangedColumns() ) ) { $strSql .= ' lost_transaction_acv_unimplemented = ' . $this->sqlLostTransactionAcvUnimplemented() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_transaction_acv_transfer_property = ' . $this->sqlLostTransactionAcvTransferProperty() . ','; } elseif( true == array_key_exists( 'LostTransactionAcvTransferProperty', $this->getChangedColumns() ) ) { $strSql .= ' lost_transaction_acv_transfer_property = ' . $this->sqlLostTransactionAcvTransferProperty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_transaction_acv_management = ' . $this->sqlLostTransactionAcvManagement() . ','; } elseif( true == array_key_exists( 'LostTransactionAcvManagement', $this->getChangedColumns() ) ) { $strSql .= ' lost_transaction_acv_management = ' . $this->sqlLostTransactionAcvManagement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_lost_transaction_acv = ' . $this->sqlTotalLostTransactionAcv() . ','; } elseif( true == array_key_exists( 'TotalLostTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_lost_transaction_acv = ' . $this->sqlTotalLostTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_lost_acv = ' . $this->sqlTotalLostAcv() . ','; } elseif( true == array_key_exists( 'TotalLostAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_lost_acv = ' . $this->sqlTotalLostAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_acv_unhappy = ' . $this->sqlLostAcvUnhappy() . ','; } elseif( true == array_key_exists( 'LostAcvUnhappy', $this->getChangedColumns() ) ) { $strSql .= ' lost_acv_unhappy = ' . $this->sqlLostAcvUnhappy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_acv_unimplemented = ' . $this->sqlLostAcvUnimplemented() . ','; } elseif( true == array_key_exists( 'LostAcvUnimplemented', $this->getChangedColumns() ) ) { $strSql .= ' lost_acv_unimplemented = ' . $this->sqlLostAcvUnimplemented() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_acv_transfer_property = ' . $this->sqlLostAcvTransferProperty() . ','; } elseif( true == array_key_exists( 'LostAcvTransferProperty', $this->getChangedColumns() ) ) { $strSql .= ' lost_acv_transfer_property = ' . $this->sqlLostAcvTransferProperty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_acv_management = ' . $this->sqlLostAcvManagement() . ','; } elseif( true == array_key_exists( 'LostAcvManagement', $this->getChangedColumns() ) ) { $strSql .= ' lost_acv_management = ' . $this->sqlLostAcvManagement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' net_acv_change = ' . $this->sqlNetAcvChange() . ','; } elseif( true == array_key_exists( 'NetAcvChange', $this->getChangedColumns() ) ) { $strSql .= ' net_acv_change = ' . $this->sqlNetAcvChange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cumulative_acv = ' . $this->sqlCumulativeAcv() . ','; } elseif( true == array_key_exists( 'CumulativeAcv', $this->getChangedColumns() ) ) { $strSql .= ' cumulative_acv = ' . $this->sqlCumulativeAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mcv_per_unit = ' . $this->sqlMcvPerUnit() . ','; } elseif( true == array_key_exists( 'McvPerUnit', $this->getChangedColumns() ) ) { $strSql .= ' mcv_per_unit = ' . $this->sqlMcvPerUnit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_logo_implementation = ' . $this->sqlNewLogoImplementation() . ','; } elseif( true == array_key_exists( 'NewLogoImplementation', $this->getChangedColumns() ) ) { $strSql .= ' new_logo_implementation = ' . $this->sqlNewLogoImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_product_implementation = ' . $this->sqlNewProductImplementation() . ','; } elseif( true == array_key_exists( 'NewProductImplementation', $this->getChangedColumns() ) ) { $strSql .= ' new_product_implementation = ' . $this->sqlNewProductImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_property_implementation = ' . $this->sqlNewPropertyImplementation() . ','; } elseif( true == array_key_exists( 'NewPropertyImplementation', $this->getChangedColumns() ) ) { $strSql .= ' new_property_implementation = ' . $this->sqlNewPropertyImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_renewal_implementation = ' . $this->sqlNewRenewalImplementation() . ','; } elseif( true == array_key_exists( 'NewRenewalImplementation', $this->getChangedColumns() ) ) { $strSql .= ' new_renewal_implementation = ' . $this->sqlNewRenewalImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_transfer_property_implementation = ' . $this->sqlNewTransferPropertyImplementation() . ','; } elseif( true == array_key_exists( 'NewTransferPropertyImplementation', $this->getChangedColumns() ) ) { $strSql .= ' new_transfer_property_implementation = ' . $this->sqlNewTransferPropertyImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_new_subscription_acv = ' . $this->sqlPilotNewSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'PilotNewSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_new_subscription_acv = ' . $this->sqlPilotNewSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_new_transaction_acv = ' . $this->sqlPilotNewTransactionAcv() . ','; } elseif( true == array_key_exists( 'PilotNewTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_new_transaction_acv = ' . $this->sqlPilotNewTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_pending_subscription_acv = ' . $this->sqlPilotPendingSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'PilotPendingSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_pending_subscription_acv = ' . $this->sqlPilotPendingSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_pending_transaction_acv = ' . $this->sqlPilotPendingTransactionAcv() . ','; } elseif( true == array_key_exists( 'PilotPendingTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_pending_transaction_acv = ' . $this->sqlPilotPendingTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_won_subscription_acv = ' . $this->sqlPilotWonSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'PilotWonSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_won_subscription_acv = ' . $this->sqlPilotWonSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_won_transaction_acv = ' . $this->sqlPilotWonTransactionAcv() . ','; } elseif( true == array_key_exists( 'PilotWonTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_won_transaction_acv = ' . $this->sqlPilotWonTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_lost_subscription_acv = ' . $this->sqlPilotLostSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'PilotLostSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_lost_subscription_acv = ' . $this->sqlPilotLostSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_lost_transaction_acv = ' . $this->sqlPilotLostTransactionAcv() . ','; } elseif( true == array_key_exists( 'PilotLostTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_lost_transaction_acv = ' . $this->sqlPilotLostTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_open_count = ' . $this->sqlPilotOpenCount() . ','; } elseif( true == array_key_exists( 'PilotOpenCount', $this->getChangedColumns() ) ) { $strSql .= ' pilot_open_count = ' . $this->sqlPilotOpenCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unimplemented_open_days = ' . $this->sqlUnimplementedOpenDays() . ','; } elseif( true == array_key_exists( 'UnimplementedOpenDays', $this->getChangedColumns() ) ) { $strSql .= ' unimplemented_open_days = ' . $this->sqlUnimplementedOpenDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unimplemented_subscription_acv = ' . $this->sqlUnimplementedSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'UnimplementedSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' unimplemented_subscription_acv = ' . $this->sqlUnimplementedSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unimplemented_transaction_acv = ' . $this->sqlUnimplementedTransactionAcv() . ','; } elseif( true == array_key_exists( 'UnimplementedTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' unimplemented_transaction_acv = ' . $this->sqlUnimplementedTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unimplemented_acv = ' . $this->sqlUnimplementedAcv() . ','; } elseif( true == array_key_exists( 'UnimplementedAcv', $this->getChangedColumns() ) ) { $strSql .= ' unimplemented_acv = ' . $this->sqlUnimplementedAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unimplemented_units = ' . $this->sqlUnimplementedUnits() . ','; } elseif( true == array_key_exists( 'UnimplementedUnits', $this->getChangedColumns() ) ) { $strSql .= ' unimplemented_units = ' . $this->sqlUnimplementedUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implemented_subscription_acv = ' . $this->sqlImplementedSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'ImplementedSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' implemented_subscription_acv = ' . $this->sqlImplementedSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implemented_transaction_acv = ' . $this->sqlImplementedTransactionAcv() . ','; } elseif( true == array_key_exists( 'ImplementedTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' implemented_transaction_acv = ' . $this->sqlImplementedTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implemented_acv = ' . $this->sqlImplementedAcv() . ','; } elseif( true == array_key_exists( 'ImplementedAcv', $this->getChangedColumns() ) ) { $strSql .= ' implemented_acv = ' . $this->sqlImplementedAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implemented_units = ' . $this->sqlImplementedUnits() . ','; } elseif( true == array_key_exists( 'ImplementedUnits', $this->getChangedColumns() ) ) { $strSql .= ' implemented_units = ' . $this->sqlImplementedUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unbilled_open_days = ' . $this->sqlUnbilledOpenDays() . ','; } elseif( true == array_key_exists( 'UnbilledOpenDays', $this->getChangedColumns() ) ) { $strSql .= ' unbilled_open_days = ' . $this->sqlUnbilledOpenDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unbilled_acv = ' . $this->sqlUnbilledAcv() . ','; } elseif( true == array_key_exists( 'UnbilledAcv', $this->getChangedColumns() ) ) { $strSql .= ' unbilled_acv = ' . $this->sqlUnbilledAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unbilled_units = ' . $this->sqlUnbilledUnits() . ','; } elseif( true == array_key_exists( 'UnbilledUnits', $this->getChangedColumns() ) ) { $strSql .= ' unbilled_units = ' . $this->sqlUnbilledUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billed_acv = ' . $this->sqlBilledAcv() . ','; } elseif( true == array_key_exists( 'BilledAcv', $this->getChangedColumns() ) ) { $strSql .= ' billed_acv = ' . $this->sqlBilledAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billed_units = ' . $this->sqlBilledUnits() . ','; } elseif( true == array_key_exists( 'BilledUnits', $this->getChangedColumns() ) ) { $strSql .= ' billed_units = ' . $this->sqlBilledUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demos_scheduled = ' . $this->sqlDemosScheduled() . ','; } elseif( true == array_key_exists( 'DemosScheduled', $this->getChangedColumns() ) ) { $strSql .= ' demos_scheduled = ' . $this->sqlDemosScheduled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demoed_units = ' . $this->sqlDemoedUnits() . ','; } elseif( true == array_key_exists( 'DemoedUnits', $this->getChangedColumns() ) ) { $strSql .= ' demoed_units = ' . $this->sqlDemoedUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demos_completed = ' . $this->sqlDemosCompleted() . ','; } elseif( true == array_key_exists( 'DemosCompleted', $this->getChangedColumns() ) ) { $strSql .= ' demos_completed = ' . $this->sqlDemosCompleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demos_forgotten = ' . $this->sqlDemosForgotten() . ','; } elseif( true == array_key_exists( 'DemosForgotten', $this->getChangedColumns() ) ) { $strSql .= ' demos_forgotten = ' . $this->sqlDemosForgotten() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commissions_scheduled = ' . $this->sqlCommissionsScheduled() . ','; } elseif( true == array_key_exists( 'CommissionsScheduled', $this->getChangedColumns() ) ) { $strSql .= ' commissions_scheduled = ' . $this->sqlCommissionsScheduled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commissions_paid = ' . $this->sqlCommissionsPaid() . ','; } elseif( true == array_key_exists( 'CommissionsPaid', $this->getChangedColumns() ) ) { $strSql .= ' commissions_paid = ' . $this->sqlCommissionsPaid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_new_leads = ' . $this->sqlPipelineNewLeads() . ','; } elseif( true == array_key_exists( 'PipelineNewLeads', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_new_leads = ' . $this->sqlPipelineNewLeads() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_leads = ' . $this->sqlPipelineLeads() . ','; } elseif( true == array_key_exists( 'PipelineLeads', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_leads = ' . $this->sqlPipelineLeads() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_won_leads = ' . $this->sqlPipelineWonLeads() . ','; } elseif( true == array_key_exists( 'PipelineWonLeads', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_won_leads = ' . $this->sqlPipelineWonLeads() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_lost_leads = ' . $this->sqlPipelineLostLeads() . ','; } elseif( true == array_key_exists( 'PipelineLostLeads', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_lost_leads = ' . $this->sqlPipelineLostLeads() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_new_units = ' . $this->sqlPipelineNewUnits() . ','; } elseif( true == array_key_exists( 'PipelineNewUnits', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_new_units = ' . $this->sqlPipelineNewUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_units = ' . $this->sqlPipelineUnits() . ','; } elseif( true == array_key_exists( 'PipelineUnits', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_units = ' . $this->sqlPipelineUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_won_units = ' . $this->sqlPipelineWonUnits() . ','; } elseif( true == array_key_exists( 'PipelineWonUnits', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_won_units = ' . $this->sqlPipelineWonUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_lost_units = ' . $this->sqlPipelineLostUnits() . ','; } elseif( true == array_key_exists( 'PipelineLostUnits', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_lost_units = ' . $this->sqlPipelineLostUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_new_acv = ' . $this->sqlPipelineNewAcv() . ','; } elseif( true == array_key_exists( 'PipelineNewAcv', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_new_acv = ' . $this->sqlPipelineNewAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_acv = ' . $this->sqlPipelineAcv() . ','; } elseif( true == array_key_exists( 'PipelineAcv', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_acv = ' . $this->sqlPipelineAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_won_acv = ' . $this->sqlPipelineWonAcv() . ','; } elseif( true == array_key_exists( 'PipelineWonAcv', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_won_acv = ' . $this->sqlPipelineWonAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_lost_acv = ' . $this->sqlPipelineLostAcv() . ','; } elseif( true == array_key_exists( 'PipelineLostAcv', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_lost_acv = ' . $this->sqlPipelineLostAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_new_proposals = ' . $this->sqlPipelineNewProposals() . ','; } elseif( true == array_key_exists( 'PipelineNewProposals', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_new_proposals = ' . $this->sqlPipelineNewProposals() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_acv = ' . $this->sqlCommittedAcv() . ','; } elseif( true == array_key_exists( 'CommittedAcv', $this->getChangedColumns() ) ) { $strSql .= ' committed_acv = ' . $this->sqlCommittedAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_bug_severity = ' . $this->sqlNewBugSeverity() . ','; } elseif( true == array_key_exists( 'NewBugSeverity', $this->getChangedColumns() ) ) { $strSql .= ' new_bug_severity = ' . $this->sqlNewBugSeverity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_bugs = ' . $this->sqlNewBugs() . ','; } elseif( true == array_key_exists( 'NewBugs', $this->getChangedColumns() ) ) { $strSql .= ' new_bugs = ' . $this->sqlNewBugs() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' closed_bugs = ' . $this->sqlClosedBugs() . ','; } elseif( true == array_key_exists( 'ClosedBugs', $this->getChangedColumns() ) ) { $strSql .= ' closed_bugs = ' . $this->sqlClosedBugs() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' open_bugs = ' . $this->sqlOpenBugs() . ','; } elseif( true == array_key_exists( 'OpenBugs', $this->getChangedColumns() ) ) { $strSql .= ' open_bugs = ' . $this->sqlOpenBugs() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_feature_requests = ' . $this->sqlNewFeatureRequests() . ','; } elseif( true == array_key_exists( 'NewFeatureRequests', $this->getChangedColumns() ) ) { $strSql .= ' new_feature_requests = ' . $this->sqlNewFeatureRequests() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' closed_feature_requests = ' . $this->sqlClosedFeatureRequests() . ','; } elseif( true == array_key_exists( 'ClosedFeatureRequests', $this->getChangedColumns() ) ) { $strSql .= ' closed_feature_requests = ' . $this->sqlClosedFeatureRequests() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' open_feature_requests = ' . $this->sqlOpenFeatureRequests() . ','; } elseif( true == array_key_exists( 'OpenFeatureRequests', $this->getChangedColumns() ) ) { $strSql .= ' open_feature_requests = ' . $this->sqlOpenFeatureRequests() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_support_tickets = ' . $this->sqlNewSupportTickets() . ','; } elseif( true == array_key_exists( 'NewSupportTickets', $this->getChangedColumns() ) ) { $strSql .= ' new_support_tickets = ' . $this->sqlNewSupportTickets() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' closed_support_tickets = ' . $this->sqlClosedSupportTickets() . ','; } elseif( true == array_key_exists( 'ClosedSupportTickets', $this->getChangedColumns() ) ) { $strSql .= ' closed_support_tickets = ' . $this->sqlClosedSupportTickets() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' open_support_tickets = ' . $this->sqlOpenSupportTickets() . ','; } elseif( true == array_key_exists( 'OpenSupportTickets', $this->getChangedColumns() ) ) { $strSql .= ' open_support_tickets = ' . $this->sqlOpenSupportTickets() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_impactful_released_tasks = ' . $this->sqlClientImpactfulReleasedTasks() . ','; } elseif( true == array_key_exists( 'ClientImpactfulReleasedTasks', $this->getChangedColumns() ) ) { $strSql .= ' client_impactful_released_tasks = ' . $this->sqlClientImpactfulReleasedTasks() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' released_tasks_without_story_points = ' . $this->sqlReleasedTasksWithoutStoryPoints() . ','; } elseif( true == array_key_exists( 'ReleasedTasksWithoutStoryPoints', $this->getChangedColumns() ) ) { $strSql .= ' released_tasks_without_story_points = ' . $this->sqlReleasedTasksWithoutStoryPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' story_points_released = ' . $this->sqlStoryPointsReleased() . ','; } elseif( true == array_key_exists( 'StoryPointsReleased', $this->getChangedColumns() ) ) { $strSql .= ' story_points_released = ' . $this->sqlStoryPointsReleased() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'month' => $this->getMonth(),
			'new_clients' => $this->getNewClients(),
			'lost_clients' => $this->getLostClients(),
			'net_client_change' => $this->getNetClientChange(),
			'cumulative_clients' => $this->getCumulativeClients(),
			'subscription_revenue' => $this->getSubscriptionRevenue(),
			'transaction_revenue' => $this->getTransactionRevenue(),
			'one_time_revenue' => $this->getOneTimeRevenue(),
			'write_off_revenue' => $this->getWriteOffRevenue(),
			'total_revenue' => $this->getTotalRevenue(),
			'quarter_revenue_growth_percent' => $this->getQuarterRevenueGrowthPercent(),
			'lost_revenue_unimplemented' => $this->getLostRevenueUnimplemented(),
			'lost_revenue_unhappy' => $this->getLostRevenueUnhappy(),
			'lost_revenue_management' => $this->getLostRevenueManagement(),
			'lost_revenue_transfer_property' => $this->getLostRevenueTransferProperty(),
			'total_lost_revenue' => $this->getTotalLostRevenue(),
			'new_logo_units' => $this->getNewLogoUnits(),
			'new_product_units' => $this->getNewProductUnits(),
			'new_property_units' => $this->getNewPropertyUnits(),
			'new_transfer_property_units' => $this->getNewTransferPropertyUnits(),
			'total_new_units' => $this->getTotalNewUnits(),
			'lost_units_unhappy' => $this->getLostUnitsUnhappy(),
			'lost_units_management' => $this->getLostUnitsManagement(),
			'lost_units_unimplemented' => $this->getLostUnitsUnimplemented(),
			'lost_units_transfer_property' => $this->getLostUnitsTransferProperty(),
			'total_lost_units' => $this->getTotalLostUnits(),
			'net_unit_change' => $this->getNetUnitChange(),
			'cumulative_units' => $this->getCumulativeUnits(),
			'revenue_per_unit' => $this->getRevenuePerUnit(),
			'new_logo_subscription_acv' => $this->getNewLogoSubscriptionAcv(),
			'new_product_subscription_acv' => $this->getNewProductSubscriptionAcv(),
			'new_property_subscription_acv' => $this->getNewPropertySubscriptionAcv(),
			'new_renewal_subscription_acv' => $this->getNewRenewalSubscriptionAcv(),
			'new_transfer_property_subscription_acv' => $this->getNewTransferPropertySubscriptionAcv(),
			'total_new_subscription_acv' => $this->getTotalNewSubscriptionAcv(),
			'total_new_active_subscription_acv' => $this->getTotalNewActiveSubscriptionAcv(),
			'total_new_adjusted_subscription_acv' => $this->getTotalNewAdjustedSubscriptionAcv(),
			'total_new_active_adjusted_subscription_acv' => $this->getTotalNewActiveAdjustedSubscriptionAcv(),
			'new_logo_transaction_acv' => $this->getNewLogoTransactionAcv(),
			'new_product_transaction_acv' => $this->getNewProductTransactionAcv(),
			'new_property_transaction_acv' => $this->getNewPropertyTransactionAcv(),
			'new_renewal_transaction_acv' => $this->getNewRenewalTransactionAcv(),
			'new_transfer_property_transaction_acv' => $this->getNewTransferPropertyTransactionAcv(),
			'total_new_transaction_acv' => $this->getTotalNewTransactionAcv(),
			'total_new_active_transaction_acv' => $this->getTotalNewActiveTransactionAcv(),
			'total_new_adjusted_transaction_acv' => $this->getTotalNewAdjustedTransactionAcv(),
			'total_new_active_adjusted_transaction_acv' => $this->getTotalNewActiveAdjustedTransactionAcv(),
			'total_new_acv' => $this->getTotalNewAcv(),
			'total_new_active_acv' => $this->getTotalNewActiveAcv(),
			'lost_subscription_acv_unhappy' => $this->getLostSubscriptionAcvUnhappy(),
			'lost_subscription_acv_unimplemented' => $this->getLostSubscriptionAcvUnimplemented(),
			'lost_subscription_acv_transfer_property' => $this->getLostSubscriptionAcvTransferProperty(),
			'lost_subscription_acv_management' => $this->getLostSubscriptionAcvManagement(),
			'total_lost_subscription_acv' => $this->getTotalLostSubscriptionAcv(),
			'lost_transaction_acv_unhappy' => $this->getLostTransactionAcvUnhappy(),
			'lost_transaction_acv_unimplemented' => $this->getLostTransactionAcvUnimplemented(),
			'lost_transaction_acv_transfer_property' => $this->getLostTransactionAcvTransferProperty(),
			'lost_transaction_acv_management' => $this->getLostTransactionAcvManagement(),
			'total_lost_transaction_acv' => $this->getTotalLostTransactionAcv(),
			'total_lost_acv' => $this->getTotalLostAcv(),
			'lost_acv_unhappy' => $this->getLostAcvUnhappy(),
			'lost_acv_unimplemented' => $this->getLostAcvUnimplemented(),
			'lost_acv_transfer_property' => $this->getLostAcvTransferProperty(),
			'lost_acv_management' => $this->getLostAcvManagement(),
			'net_acv_change' => $this->getNetAcvChange(),
			'cumulative_acv' => $this->getCumulativeAcv(),
			'mcv_per_unit' => $this->getMcvPerUnit(),
			'new_logo_implementation' => $this->getNewLogoImplementation(),
			'new_product_implementation' => $this->getNewProductImplementation(),
			'new_property_implementation' => $this->getNewPropertyImplementation(),
			'new_renewal_implementation' => $this->getNewRenewalImplementation(),
			'new_transfer_property_implementation' => $this->getNewTransferPropertyImplementation(),
			'pilot_new_subscription_acv' => $this->getPilotNewSubscriptionAcv(),
			'pilot_new_transaction_acv' => $this->getPilotNewTransactionAcv(),
			'pilot_pending_subscription_acv' => $this->getPilotPendingSubscriptionAcv(),
			'pilot_pending_transaction_acv' => $this->getPilotPendingTransactionAcv(),
			'pilot_won_subscription_acv' => $this->getPilotWonSubscriptionAcv(),
			'pilot_won_transaction_acv' => $this->getPilotWonTransactionAcv(),
			'pilot_lost_subscription_acv' => $this->getPilotLostSubscriptionAcv(),
			'pilot_lost_transaction_acv' => $this->getPilotLostTransactionAcv(),
			'pilot_open_count' => $this->getPilotOpenCount(),
			'unimplemented_open_days' => $this->getUnimplementedOpenDays(),
			'unimplemented_subscription_acv' => $this->getUnimplementedSubscriptionAcv(),
			'unimplemented_transaction_acv' => $this->getUnimplementedTransactionAcv(),
			'unimplemented_acv' => $this->getUnimplementedAcv(),
			'unimplemented_units' => $this->getUnimplementedUnits(),
			'implemented_subscription_acv' => $this->getImplementedSubscriptionAcv(),
			'implemented_transaction_acv' => $this->getImplementedTransactionAcv(),
			'implemented_acv' => $this->getImplementedAcv(),
			'implemented_units' => $this->getImplementedUnits(),
			'unbilled_open_days' => $this->getUnbilledOpenDays(),
			'unbilled_acv' => $this->getUnbilledAcv(),
			'unbilled_units' => $this->getUnbilledUnits(),
			'billed_acv' => $this->getBilledAcv(),
			'billed_units' => $this->getBilledUnits(),
			'demos_scheduled' => $this->getDemosScheduled(),
			'demoed_units' => $this->getDemoedUnits(),
			'demos_completed' => $this->getDemosCompleted(),
			'demos_forgotten' => $this->getDemosForgotten(),
			'commissions_scheduled' => $this->getCommissionsScheduled(),
			'commissions_paid' => $this->getCommissionsPaid(),
			'pipeline_new_leads' => $this->getPipelineNewLeads(),
			'pipeline_leads' => $this->getPipelineLeads(),
			'pipeline_won_leads' => $this->getPipelineWonLeads(),
			'pipeline_lost_leads' => $this->getPipelineLostLeads(),
			'pipeline_new_units' => $this->getPipelineNewUnits(),
			'pipeline_units' => $this->getPipelineUnits(),
			'pipeline_won_units' => $this->getPipelineWonUnits(),
			'pipeline_lost_units' => $this->getPipelineLostUnits(),
			'pipeline_new_acv' => $this->getPipelineNewAcv(),
			'pipeline_acv' => $this->getPipelineAcv(),
			'pipeline_won_acv' => $this->getPipelineWonAcv(),
			'pipeline_lost_acv' => $this->getPipelineLostAcv(),
			'pipeline_new_proposals' => $this->getPipelineNewProposals(),
			'committed_acv' => $this->getCommittedAcv(),
			'new_bug_severity' => $this->getNewBugSeverity(),
			'new_bugs' => $this->getNewBugs(),
			'closed_bugs' => $this->getClosedBugs(),
			'open_bugs' => $this->getOpenBugs(),
			'new_feature_requests' => $this->getNewFeatureRequests(),
			'closed_feature_requests' => $this->getClosedFeatureRequests(),
			'open_feature_requests' => $this->getOpenFeatureRequests(),
			'new_support_tickets' => $this->getNewSupportTickets(),
			'closed_support_tickets' => $this->getClosedSupportTickets(),
			'open_support_tickets' => $this->getOpenSupportTickets(),
			'client_impactful_released_tasks' => $this->getClientImpactfulReleasedTasks(),
			'released_tasks_without_story_points' => $this->getReleasedTasksWithoutStoryPoints(),
			'story_points_released' => $this->getStoryPointsReleased(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>