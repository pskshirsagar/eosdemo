<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeCertifications
 * Do not add any new functions to this class.
 */

class CBaseEmployeeCertifications extends CEosPluralBase {

	/**
	 * @return CEmployeeCertification[]
	 */
	public static function fetchEmployeeCertifications( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeCertification', $objDatabase );
	}

	/**
	 * @return CEmployeeCertification
	 */
	public static function fetchEmployeeCertification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeCertification', $objDatabase );
	}

	public static function fetchEmployeeCertificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_certifications', $objDatabase );
	}

	public static function fetchEmployeeCertificationById( $intId, $objDatabase ) {
		return self::fetchEmployeeCertification( sprintf( 'SELECT * FROM employee_certifications WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeCertificationsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeCertifications( sprintf( 'SELECT * FROM employee_certifications WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeCertificationsByCertificationId( $intCertificationId, $objDatabase ) {
		return self::fetchEmployeeCertifications( sprintf( 'SELECT * FROM employee_certifications WHERE certification_id = %d', ( int ) $intCertificationId ), $objDatabase );
	}

	public static function fetchEmployeeCertificationsByPsDocumentId( $intPsDocumentId, $objDatabase ) {
		return self::fetchEmployeeCertifications( sprintf( 'SELECT * FROM employee_certifications WHERE ps_document_id = %d', ( int ) $intPsDocumentId ), $objDatabase );
	}

}
?>