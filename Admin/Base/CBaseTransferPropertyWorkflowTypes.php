<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTransferPropertyWorkflowTypes
 * Do not add any new functions to this class.
 */

class CBaseTransferPropertyWorkflowTypes extends CEosPluralBase {

	/**
	 * @return CTransferPropertyWorkflowType[]
	 */
	public static function fetchTransferPropertyWorkflowTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTransferPropertyWorkflowType', $objDatabase );
	}

	/**
	 * @return CTransferPropertyWorkflowType
	 */
	public static function fetchTransferPropertyWorkflowType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTransferPropertyWorkflowType', $objDatabase );
	}

	public static function fetchTransferPropertyWorkflowTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transfer_property_workflow_types', $objDatabase );
	}

	public static function fetchTransferPropertyWorkflowTypeById( $intId, $objDatabase ) {
		return self::fetchTransferPropertyWorkflowType( sprintf( 'SELECT * FROM transfer_property_workflow_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>