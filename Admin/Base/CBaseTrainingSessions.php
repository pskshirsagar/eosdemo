<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingSessions
 * Do not add any new functions to this class.
 */

class CBaseTrainingSessions extends CEosPluralBase {

	/**
	 * @return CTrainingSession[]
	 */
	public static function fetchTrainingSessions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTrainingSession', $objDatabase );
	}

	/**
	 * @return CTrainingSession
	 */
	public static function fetchTrainingSession( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTrainingSession', $objDatabase );
	}

	public static function fetchTrainingSessionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'training_sessions', $objDatabase );
	}

	public static function fetchTrainingSessionById( $intId, $objDatabase ) {
		return self::fetchTrainingSession( sprintf( 'SELECT * FROM training_sessions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTrainingSessionsByParentTrainingSessionId( $intParentTrainingSessionId, $objDatabase ) {
		return self::fetchTrainingSessions( sprintf( 'SELECT * FROM training_sessions WHERE parent_training_session_id = %d', ( int ) $intParentTrainingSessionId ), $objDatabase );
	}

	public static function fetchTrainingSessionsByTrainingId( $intTrainingId, $objDatabase ) {
		return self::fetchTrainingSessions( sprintf( 'SELECT * FROM training_sessions WHERE training_id = %d', ( int ) $intTrainingId ), $objDatabase );
	}

	public static function fetchTrainingSessionsByInductionSessionId( $intInductionSessionId, $objDatabase ) {
		return self::fetchTrainingSessions( sprintf( 'SELECT * FROM training_sessions WHERE induction_session_id = %d', ( int ) $intInductionSessionId ), $objDatabase );
	}

	public static function fetchTrainingSessionsByTrainingTrainerAssociationId( $intTrainingTrainerAssociationId, $objDatabase ) {
		return self::fetchTrainingSessions( sprintf( 'SELECT * FROM training_sessions WHERE training_trainer_association_id = %d', ( int ) $intTrainingTrainerAssociationId ), $objDatabase );
	}

	public static function fetchTrainingSessionsByOfficeRoomId( $intOfficeRoomId, $objDatabase ) {
		return self::fetchTrainingSessions( sprintf( 'SELECT * FROM training_sessions WHERE office_room_id = %d', ( int ) $intOfficeRoomId ), $objDatabase );
	}

	public static function fetchTrainingSessionsBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		return self::fetchTrainingSessions( sprintf( 'SELECT * FROM training_sessions WHERE survey_template_id = %d', ( int ) $intSurveyTemplateId ), $objDatabase );
	}

	public static function fetchTrainingSessionsByGoogleCalendarEventId( $strGoogleCalendarEventId, $objDatabase ) {
		return self::fetchTrainingSessions( sprintf( 'SELECT * FROM training_sessions WHERE google_calendar_event_id = \'%s\'', $strGoogleCalendarEventId ), $objDatabase );
	}

}
?>