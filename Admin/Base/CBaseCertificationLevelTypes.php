<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCertificationLevelTypes
 * Do not add any new functions to this class.
 */

class CBaseCertificationLevelTypes extends CEosPluralBase {

	/**
	 * @return CCertificationLevelType[]
	 */
	public static function fetchCertificationLevelTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCertificationLevelType', $objDatabase );
	}

	/**
	 * @return CCertificationLevelType
	 */
	public static function fetchCertificationLevelType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCertificationLevelType', $objDatabase );
	}

	public static function fetchCertificationLevelTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'certification_level_types', $objDatabase );
	}

	public static function fetchCertificationLevelTypeById( $intId, $objDatabase ) {
		return self::fetchCertificationLevelType( sprintf( 'SELECT * FROM certification_level_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>