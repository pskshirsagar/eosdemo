<?php

class CBaseMassEmailClick extends CEosSingularBase {

	const TABLE_NAME = 'public.mass_email_clicks';

	protected $m_intId;
	protected $m_intPsLeadId;
	protected $m_intActionId;
	protected $m_intMassEmailId;
	protected $m_intMassEmailLinkId;
	protected $m_strClickDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['action_id'] ) && $boolDirectSet ) $this->set( 'm_intActionId', trim( $arrValues['action_id'] ) ); elseif( isset( $arrValues['action_id'] ) ) $this->setActionId( $arrValues['action_id'] );
		if( isset( $arrValues['mass_email_id'] ) && $boolDirectSet ) $this->set( 'm_intMassEmailId', trim( $arrValues['mass_email_id'] ) ); elseif( isset( $arrValues['mass_email_id'] ) ) $this->setMassEmailId( $arrValues['mass_email_id'] );
		if( isset( $arrValues['mass_email_link_id'] ) && $boolDirectSet ) $this->set( 'm_intMassEmailLinkId', trim( $arrValues['mass_email_link_id'] ) ); elseif( isset( $arrValues['mass_email_link_id'] ) ) $this->setMassEmailLinkId( $arrValues['mass_email_link_id'] );
		if( isset( $arrValues['click_datetime'] ) && $boolDirectSet ) $this->set( 'm_strClickDatetime', trim( $arrValues['click_datetime'] ) ); elseif( isset( $arrValues['click_datetime'] ) ) $this->setClickDatetime( $arrValues['click_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setActionId( $intActionId ) {
		$this->set( 'm_intActionId', CStrings::strToIntDef( $intActionId, NULL, false ) );
	}

	public function getActionId() {
		return $this->m_intActionId;
	}

	public function sqlActionId() {
		return ( true == isset( $this->m_intActionId ) ) ? ( string ) $this->m_intActionId : 'NULL';
	}

	public function setMassEmailId( $intMassEmailId ) {
		$this->set( 'm_intMassEmailId', CStrings::strToIntDef( $intMassEmailId, NULL, false ) );
	}

	public function getMassEmailId() {
		return $this->m_intMassEmailId;
	}

	public function sqlMassEmailId() {
		return ( true == isset( $this->m_intMassEmailId ) ) ? ( string ) $this->m_intMassEmailId : 'NULL';
	}

	public function setMassEmailLinkId( $intMassEmailLinkId ) {
		$this->set( 'm_intMassEmailLinkId', CStrings::strToIntDef( $intMassEmailLinkId, NULL, false ) );
	}

	public function getMassEmailLinkId() {
		return $this->m_intMassEmailLinkId;
	}

	public function sqlMassEmailLinkId() {
		return ( true == isset( $this->m_intMassEmailLinkId ) ) ? ( string ) $this->m_intMassEmailLinkId : 'NULL';
	}

	public function setClickDatetime( $strClickDatetime ) {
		$this->set( 'm_strClickDatetime', CStrings::strTrimDef( $strClickDatetime, -1, NULL, true ) );
	}

	public function getClickDatetime() {
		return $this->m_strClickDatetime;
	}

	public function sqlClickDatetime() {
		return ( true == isset( $this->m_strClickDatetime ) ) ? '\'' . $this->m_strClickDatetime . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_lead_id, action_id, mass_email_id, mass_email_link_id, click_datetime, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsLeadId() . ', ' .
 						$this->sqlActionId() . ', ' .
 						$this->sqlMassEmailId() . ', ' .
 						$this->sqlMassEmailLinkId() . ', ' .
 						$this->sqlClickDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_id = ' . $this->sqlActionId() . ','; } elseif( true == array_key_exists( 'ActionId', $this->getChangedColumns() ) ) { $strSql .= ' action_id = ' . $this->sqlActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mass_email_id = ' . $this->sqlMassEmailId() . ','; } elseif( true == array_key_exists( 'MassEmailId', $this->getChangedColumns() ) ) { $strSql .= ' mass_email_id = ' . $this->sqlMassEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mass_email_link_id = ' . $this->sqlMassEmailLinkId() . ','; } elseif( true == array_key_exists( 'MassEmailLinkId', $this->getChangedColumns() ) ) { $strSql .= ' mass_email_link_id = ' . $this->sqlMassEmailLinkId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' click_datetime = ' . $this->sqlClickDatetime() . ','; } elseif( true == array_key_exists( 'ClickDatetime', $this->getChangedColumns() ) ) { $strSql .= ' click_datetime = ' . $this->sqlClickDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'action_id' => $this->getActionId(),
			'mass_email_id' => $this->getMassEmailId(),
			'mass_email_link_id' => $this->getMassEmailLinkId(),
			'click_datetime' => $this->getClickDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>