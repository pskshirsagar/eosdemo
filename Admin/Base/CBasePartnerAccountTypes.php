<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPartnerAccountTypes
 * Do not add any new functions to this class.
 */

class CBasePartnerAccountTypes extends CEosPluralBase {

	/**
	 * @return CPartnerAccountType[]
	 */
	public static function fetchPartnerAccountTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPartnerAccountType', $objDatabase );
	}

	/**
	 * @return CPartnerAccountType
	 */
	public static function fetchPartnerAccountType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPartnerAccountType', $objDatabase );
	}

	public static function fetchPartnerAccountTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'partner_account_types', $objDatabase );
	}

	public static function fetchPartnerAccountTypeById( $intId, $objDatabase ) {
		return self::fetchPartnerAccountType( sprintf( 'SELECT * FROM partner_account_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>