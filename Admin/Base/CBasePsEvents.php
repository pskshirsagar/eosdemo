<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsEvents
 * Do not add any new functions to this class.
 */

class CBasePsEvents extends CEosPluralBase {

	/**
	 * @return CPsEvent[]
	 */
	public static function fetchPsEvents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsEvent', $objDatabase );
	}

	/**
	 * @return CPsEvent
	 */
	public static function fetchPsEvent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsEvent', $objDatabase );
	}

	public static function fetchPsEventCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_events', $objDatabase );
	}

	public static function fetchPsEventById( $intId, $objDatabase ) {
		return self::fetchPsEvent( sprintf( 'SELECT * FROM ps_events WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsEventsByPsEventTypeId( $intPsEventTypeId, $objDatabase ) {
		return self::fetchPsEvents( sprintf( 'SELECT * FROM ps_events WHERE ps_event_type_id = %d', ( int ) $intPsEventTypeId ), $objDatabase );
	}

}
?>