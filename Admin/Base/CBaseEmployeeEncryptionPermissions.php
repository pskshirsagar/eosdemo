<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeEncryptionPermissions
 * Do not add any new functions to this class.
 */

class CBaseEmployeeEncryptionPermissions extends CEosPluralBase {

	/**
	 * @return CEmployeeEncryptionPermission[]
	 */
	public static function fetchEmployeeEncryptionPermissions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeEncryptionPermission', $objDatabase );
	}

	/**
	 * @return CEmployeeEncryptionPermission
	 */
	public static function fetchEmployeeEncryptionPermission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeEncryptionPermission', $objDatabase );
	}

	public static function fetchEmployeeEncryptionPermissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_encryption_permissions', $objDatabase );
	}

	public static function fetchEmployeeEncryptionPermissionById( $intId, $objDatabase ) {
		return self::fetchEmployeeEncryptionPermission( sprintf( 'SELECT * FROM employee_encryption_permissions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeEncryptionPermissionsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeEncryptionPermissions( sprintf( 'SELECT * FROM employee_encryption_permissions WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeEncryptionPermissionsByReferenceId( $intReferenceId, $objDatabase ) {
		return self::fetchEmployeeEncryptionPermissions( sprintf( 'SELECT * FROM employee_encryption_permissions WHERE reference_id = %d', ( int ) $intReferenceId ), $objDatabase );
	}

	public static function fetchEmployeeEncryptionPermissionsByReferenceTypeId( $intReferenceTypeId, $objDatabase ) {
		return self::fetchEmployeeEncryptionPermissions( sprintf( 'SELECT * FROM employee_encryption_permissions WHERE reference_type_id = %d', ( int ) $intReferenceTypeId ), $objDatabase );
	}

	public static function fetchEmployeeEncryptionPermissionsByEncryptionSystemTypeId( $intEncryptionSystemTypeId, $objDatabase ) {
		return self::fetchEmployeeEncryptionPermissions( sprintf( 'SELECT * FROM employee_encryption_permissions WHERE encryption_system_type_id = %d', ( int ) $intEncryptionSystemTypeId ), $objDatabase );
	}

}
?>