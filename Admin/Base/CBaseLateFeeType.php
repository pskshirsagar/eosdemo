<?php

class CBaseLateFeeType extends CEosSingularBase {

	const TABLE_NAME = 'public.late_fee_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltPercentageFee;
	protected $m_fltFixedFee;
	protected $m_intOrderNum;
	protected $m_intIsPublished;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['percentage_fee'] ) && $boolDirectSet ) $this->set( 'm_fltPercentageFee', trim( $arrValues['percentage_fee'] ) ); elseif( isset( $arrValues['percentage_fee'] ) ) $this->setPercentageFee( $arrValues['percentage_fee'] );
		if( isset( $arrValues['fixed_fee'] ) && $boolDirectSet ) $this->set( 'm_fltFixedFee', trim( $arrValues['fixed_fee'] ) ); elseif( isset( $arrValues['fixed_fee'] ) ) $this->setFixedFee( $arrValues['fixed_fee'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setPercentageFee( $fltPercentageFee ) {
		$this->set( 'm_fltPercentageFee', CStrings::strToFloatDef( $fltPercentageFee, NULL, false, 6 ) );
	}

	public function getPercentageFee() {
		return $this->m_fltPercentageFee;
	}

	public function sqlPercentageFee() {
		return ( true == isset( $this->m_fltPercentageFee ) ) ? ( string ) $this->m_fltPercentageFee : 'NULL';
	}

	public function setFixedFee( $fltFixedFee ) {
		$this->set( 'm_fltFixedFee', CStrings::strToFloatDef( $fltFixedFee, NULL, false, 2 ) );
	}

	public function getFixedFee() {
		return $this->m_fltFixedFee;
	}

	public function sqlFixedFee() {
		return ( true == isset( $this->m_fltFixedFee ) ) ? ( string ) $this->m_fltFixedFee : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'percentage_fee' => $this->getPercentageFee(),
			'fixed_fee' => $this->getFixedFee(),
			'order_num' => $this->getOrderNum(),
			'is_published' => $this->getIsPublished()
		);
	}

}
?>