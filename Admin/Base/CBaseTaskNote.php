<?php

class CBaseTaskNote extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.task_notes';

	protected $m_intId;
	protected $m_intTaskId;
	protected $m_intTaskNoteTypeId;
	protected $m_intUserId;
	protected $m_intCompanyUserId;
	protected $m_intTeamId;
	protected $m_intEmployeeId;
	protected $m_intCallId;
	protected $m_strReferenceNumber;
	protected $m_strContactFullName;
	protected $m_strEmailAddress;
	protected $m_strPhoneNumber;
	protected $m_strPhoneExtension;
	protected $m_strTaskNote;
	protected $m_strTaskNoteDatetime;
	protected $m_strStartTime;
	protected $m_intTimeSpent;
	protected $m_intIsPublished;
	protected $m_intIsClientVisible;
	protected $m_intIsFlagged;
	protected $m_boolIsShared;
	protected $m_intAbandonedBy;
	protected $m_strAbandonedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intBlockerResponseId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strContactFullName = NULL;
		$this->m_intIsPublished = '1';
		$this->m_intIsClientVisible = '0';
		$this->m_intIsFlagged = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['task_note_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskNoteTypeId', trim( $arrValues['task_note_type_id'] ) ); elseif( isset( $arrValues['task_note_type_id'] ) ) $this->setTaskNoteTypeId( $arrValues['task_note_type_id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['team_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamId', trim( $arrValues['team_id'] ) ); elseif( isset( $arrValues['team_id'] ) ) $this->setTeamId( $arrValues['team_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['reference_number'] ) && $boolDirectSet ) $this->set( 'm_strReferenceNumber', trim( $arrValues['reference_number'] ) ); elseif( isset( $arrValues['reference_number'] ) ) $this->setReferenceNumber( $arrValues['reference_number'] );
		if( isset( $arrValues['contact_full_name'] ) && $boolDirectSet ) $this->set( 'm_strContactFullName', trim( $arrValues['contact_full_name'] ) ); elseif( isset( $arrValues['contact_full_name'] ) ) $this->setContactFullName( $arrValues['contact_full_name'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['phone_extension'] ) && $boolDirectSet ) $this->set( 'm_strPhoneExtension', trim( $arrValues['phone_extension'] ) ); elseif( isset( $arrValues['phone_extension'] ) ) $this->setPhoneExtension( $arrValues['phone_extension'] );
		if( isset( $arrValues['task_note'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTaskNote', trim( $arrValues['task_note'] ) ); elseif( isset( $arrValues['task_note'] ) ) $this->setTaskNote( $arrValues['task_note'] );
		if( isset( $arrValues['task_note_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTaskNoteDatetime', trim( $arrValues['task_note_datetime'] ) ); elseif( isset( $arrValues['task_note_datetime'] ) ) $this->setTaskNoteDatetime( $arrValues['task_note_datetime'] );
		if( isset( $arrValues['start_time'] ) && $boolDirectSet ) $this->set( 'm_strStartTime', trim( $arrValues['start_time'] ) ); elseif( isset( $arrValues['start_time'] ) ) $this->setStartTime( $arrValues['start_time'] );
		if( isset( $arrValues['time_spent'] ) && $boolDirectSet ) $this->set( 'm_intTimeSpent', trim( $arrValues['time_spent'] ) ); elseif( isset( $arrValues['time_spent'] ) ) $this->setTimeSpent( $arrValues['time_spent'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_client_visible'] ) && $boolDirectSet ) $this->set( 'm_intIsClientVisible', trim( $arrValues['is_client_visible'] ) ); elseif( isset( $arrValues['is_client_visible'] ) ) $this->setIsClientVisible( $arrValues['is_client_visible'] );
		if( isset( $arrValues['is_flagged'] ) && $boolDirectSet ) $this->set( 'm_intIsFlagged', trim( $arrValues['is_flagged'] ) ); elseif( isset( $arrValues['is_flagged'] ) ) $this->setIsFlagged( $arrValues['is_flagged'] );
		if( isset( $arrValues['is_shared'] ) && $boolDirectSet ) $this->set( 'm_boolIsShared', trim( stripcslashes( $arrValues['is_shared'] ) ) ); elseif( isset( $arrValues['is_shared'] ) ) $this->setIsShared( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_shared'] ) : $arrValues['is_shared'] );
		if( isset( $arrValues['abandoned_by'] ) && $boolDirectSet ) $this->set( 'm_intAbandonedBy', trim( $arrValues['abandoned_by'] ) ); elseif( isset( $arrValues['abandoned_by'] ) ) $this->setAbandonedBy( $arrValues['abandoned_by'] );
		if( isset( $arrValues['abandoned_on'] ) && $boolDirectSet ) $this->set( 'm_strAbandonedOn', trim( $arrValues['abandoned_on'] ) ); elseif( isset( $arrValues['abandoned_on'] ) ) $this->setAbandonedOn( $arrValues['abandoned_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['blocker_response_id'] ) && $boolDirectSet ) $this->set( 'm_intBlockerResponseId', trim( $arrValues['blocker_response_id'] ) ); elseif( isset( $arrValues['blocker_response_id'] ) ) $this->setBlockerResponseId( $arrValues['blocker_response_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setTaskNoteTypeId( $intTaskNoteTypeId ) {
		$this->set( 'm_intTaskNoteTypeId', CStrings::strToIntDef( $intTaskNoteTypeId, NULL, false ) );
	}

	public function getTaskNoteTypeId() {
		return $this->m_intTaskNoteTypeId;
	}

	public function sqlTaskNoteTypeId() {
		return ( true == isset( $this->m_intTaskNoteTypeId ) ) ? ( string ) $this->m_intTaskNoteTypeId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setTeamId( $intTeamId ) {
		$this->set( 'm_intTeamId', CStrings::strToIntDef( $intTeamId, NULL, false ) );
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function sqlTeamId() {
		return ( true == isset( $this->m_intTeamId ) ) ? ( string ) $this->m_intTeamId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setReferenceNumber( $strReferenceNumber ) {
		$this->set( 'm_strReferenceNumber', CStrings::strTrimDef( $strReferenceNumber, 100, NULL, true ) );
	}

	public function getReferenceNumber() {
		return $this->m_strReferenceNumber;
	}

	public function sqlReferenceNumber() {
		return ( true == isset( $this->m_strReferenceNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReferenceNumber ) : '\'' . addslashes( $this->m_strReferenceNumber ) . '\'' ) : 'NULL';
	}

	public function setContactFullName( $strContactFullName ) {
		$this->set( 'm_strContactFullName', CStrings::strTrimDef( $strContactFullName, 120, NULL, true ) );
	}

	public function getContactFullName() {
		return $this->m_strContactFullName;
	}

	public function sqlContactFullName() {
		return ( true == isset( $this->m_strContactFullName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContactFullName ) : '\'' . addslashes( $this->m_strContactFullName ) . '\'' ) : '\'NULL\'';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 50, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 50, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setPhoneExtension( $strPhoneExtension ) {
		$this->set( 'm_strPhoneExtension', CStrings::strTrimDef( $strPhoneExtension, 10, NULL, true ) );
	}

	public function getPhoneExtension() {
		return $this->m_strPhoneExtension;
	}

	public function sqlPhoneExtension() {
		return ( true == isset( $this->m_strPhoneExtension ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneExtension ) : '\'' . addslashes( $this->m_strPhoneExtension ) . '\'' ) : 'NULL';
	}

	public function setTaskNote( $strTaskNote, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTaskNote', CStrings::strTrimDef( $strTaskNote, -1, NULL, true ), $strLocaleCode );
	}

	public function getTaskNote( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTaskNote', $strLocaleCode );
	}

	public function sqlTaskNote() {
		return ( true == isset( $this->m_strTaskNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaskNote ) : '\'' . addslashes( $this->m_strTaskNote ) . '\'' ) : 'NULL';
	}

	public function setTaskNoteDatetime( $strTaskNoteDatetime ) {
		$this->set( 'm_strTaskNoteDatetime', CStrings::strTrimDef( $strTaskNoteDatetime, -1, NULL, true ) );
	}

	public function getTaskNoteDatetime() {
		return $this->m_strTaskNoteDatetime;
	}

	public function sqlTaskNoteDatetime() {
		return ( true == isset( $this->m_strTaskNoteDatetime ) ) ? '\'' . $this->m_strTaskNoteDatetime . '\'' : 'NOW()';
	}

	public function setStartTime( $strStartTime ) {
		$this->set( 'm_strStartTime', CStrings::strTrimDef( $strStartTime, -1, NULL, true ) );
	}

	public function getStartTime() {
		return $this->m_strStartTime;
	}

	public function sqlStartTime() {
		return ( true == isset( $this->m_strStartTime ) ) ? '\'' . $this->m_strStartTime . '\'' : 'NULL';
	}

	public function setTimeSpent( $intTimeSpent ) {
		$this->set( 'm_intTimeSpent', CStrings::strToIntDef( $intTimeSpent, NULL, false ) );
	}

	public function getTimeSpent() {
		return $this->m_intTimeSpent;
	}

	public function sqlTimeSpent() {
		return ( true == isset( $this->m_intTimeSpent ) ) ? ( string ) $this->m_intTimeSpent : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsClientVisible( $intIsClientVisible ) {
		$this->set( 'm_intIsClientVisible', CStrings::strToIntDef( $intIsClientVisible, NULL, false ) );
	}

	public function getIsClientVisible() {
		return $this->m_intIsClientVisible;
	}

	public function sqlIsClientVisible() {
		return ( true == isset( $this->m_intIsClientVisible ) ) ? ( string ) $this->m_intIsClientVisible : '0';
	}

	public function setIsFlagged( $intIsFlagged ) {
		$this->set( 'm_intIsFlagged', CStrings::strToIntDef( $intIsFlagged, NULL, false ) );
	}

	public function getIsFlagged() {
		return $this->m_intIsFlagged;
	}

	public function sqlIsFlagged() {
		return ( true == isset( $this->m_intIsFlagged ) ) ? ( string ) $this->m_intIsFlagged : '0';
	}

	public function setIsShared( $boolIsShared ) {
		$this->set( 'm_boolIsShared', CStrings::strToBool( $boolIsShared ) );
	}

	public function getIsShared() {
		return $this->m_boolIsShared;
	}

	public function sqlIsShared() {
		return ( true == isset( $this->m_boolIsShared ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsShared ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAbandonedBy( $intAbandonedBy ) {
		$this->set( 'm_intAbandonedBy', CStrings::strToIntDef( $intAbandonedBy, NULL, false ) );
	}

	public function getAbandonedBy() {
		return $this->m_intAbandonedBy;
	}

	public function sqlAbandonedBy() {
		return ( true == isset( $this->m_intAbandonedBy ) ) ? ( string ) $this->m_intAbandonedBy : 'NULL';
	}

	public function setAbandonedOn( $strAbandonedOn ) {
		$this->set( 'm_strAbandonedOn', CStrings::strTrimDef( $strAbandonedOn, -1, NULL, true ) );
	}

	public function getAbandonedOn() {
		return $this->m_strAbandonedOn;
	}

	public function sqlAbandonedOn() {
		return ( true == isset( $this->m_strAbandonedOn ) ) ? '\'' . $this->m_strAbandonedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setBlockerResponseId( $intBlockerResponseId ) {
		$this->set( 'm_intBlockerResponseId', CStrings::strToIntDef( $intBlockerResponseId, NULL, false ) );
	}

	public function getBlockerResponseId() {
		return $this->m_intBlockerResponseId;
	}

	public function sqlBlockerResponseId() {
		return ( true == isset( $this->m_intBlockerResponseId ) ) ? ( string ) $this->m_intBlockerResponseId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, task_id, task_note_type_id, user_id, company_user_id, team_id, employee_id, call_id, reference_number, contact_full_name, email_address, phone_number, phone_extension, task_note, task_note_datetime, start_time, time_spent, is_published, is_client_visible, is_flagged, is_shared, abandoned_by, abandoned_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, blocker_response_id, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlTaskId() . ', ' .
						$this->sqlTaskNoteTypeId() . ', ' .
						$this->sqlUserId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlTeamId() . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlCallId() . ', ' .
						$this->sqlReferenceNumber() . ', ' .
						$this->sqlContactFullName() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlPhoneNumber() . ', ' .
						$this->sqlPhoneExtension() . ', ' .
						$this->sqlTaskNote() . ', ' .
						$this->sqlTaskNoteDatetime() . ', ' .
						$this->sqlStartTime() . ', ' .
						$this->sqlTimeSpent() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsClientVisible() . ', ' .
						$this->sqlIsFlagged() . ', ' .
						$this->sqlIsShared() . ', ' .
						$this->sqlAbandonedBy() . ', ' .
						$this->sqlAbandonedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlBlockerResponseId() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId(). ',' ; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_note_type_id = ' . $this->sqlTaskNoteTypeId(). ',' ; } elseif( true == array_key_exists( 'TaskNoteTypeId', $this->getChangedColumns() ) ) { $strSql .= ' task_note_type_id = ' . $this->sqlTaskNoteTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId(). ',' ; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_id = ' . $this->sqlTeamId(). ',' ; } elseif( true == array_key_exists( 'TeamId', $this->getChangedColumns() ) ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId(). ',' ; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'ReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_full_name = ' . $this->sqlContactFullName(). ',' ; } elseif( true == array_key_exists( 'ContactFullName', $this->getChangedColumns() ) ) { $strSql .= ' contact_full_name = ' . $this->sqlContactFullName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_extension = ' . $this->sqlPhoneExtension(). ',' ; } elseif( true == array_key_exists( 'PhoneExtension', $this->getChangedColumns() ) ) { $strSql .= ' phone_extension = ' . $this->sqlPhoneExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_note = ' . $this->sqlTaskNote(). ',' ; } elseif( true == array_key_exists( 'TaskNote', $this->getChangedColumns() ) ) { $strSql .= ' task_note = ' . $this->sqlTaskNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_note_datetime = ' . $this->sqlTaskNoteDatetime(). ',' ; } elseif( true == array_key_exists( 'TaskNoteDatetime', $this->getChangedColumns() ) ) { $strSql .= ' task_note_datetime = ' . $this->sqlTaskNoteDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_time = ' . $this->sqlStartTime(). ',' ; } elseif( true == array_key_exists( 'StartTime', $this->getChangedColumns() ) ) { $strSql .= ' start_time = ' . $this->sqlStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' time_spent = ' . $this->sqlTimeSpent(). ',' ; } elseif( true == array_key_exists( 'TimeSpent', $this->getChangedColumns() ) ) { $strSql .= ' time_spent = ' . $this->sqlTimeSpent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_visible = ' . $this->sqlIsClientVisible(). ',' ; } elseif( true == array_key_exists( 'IsClientVisible', $this->getChangedColumns() ) ) { $strSql .= ' is_client_visible = ' . $this->sqlIsClientVisible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_flagged = ' . $this->sqlIsFlagged(). ',' ; } elseif( true == array_key_exists( 'IsFlagged', $this->getChangedColumns() ) ) { $strSql .= ' is_flagged = ' . $this->sqlIsFlagged() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_shared = ' . $this->sqlIsShared(). ',' ; } elseif( true == array_key_exists( 'IsShared', $this->getChangedColumns() ) ) { $strSql .= ' is_shared = ' . $this->sqlIsShared() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' abandoned_by = ' . $this->sqlAbandonedBy(). ',' ; } elseif( true == array_key_exists( 'AbandonedBy', $this->getChangedColumns() ) ) { $strSql .= ' abandoned_by = ' . $this->sqlAbandonedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' abandoned_on = ' . $this->sqlAbandonedOn(). ',' ; } elseif( true == array_key_exists( 'AbandonedOn', $this->getChangedColumns() ) ) { $strSql .= ' abandoned_on = ' . $this->sqlAbandonedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blocker_response_id = ' . $this->sqlBlockerResponseId(). ',' ; } elseif( true == array_key_exists( 'BlockerResponseId', $this->getChangedColumns() ) ) { $strSql .= ' blocker_response_id = ' . $this->sqlBlockerResponseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'task_id' => $this->getTaskId(),
			'task_note_type_id' => $this->getTaskNoteTypeId(),
			'user_id' => $this->getUserId(),
			'company_user_id' => $this->getCompanyUserId(),
			'team_id' => $this->getTeamId(),
			'employee_id' => $this->getEmployeeId(),
			'call_id' => $this->getCallId(),
			'reference_number' => $this->getReferenceNumber(),
			'contact_full_name' => $this->getContactFullName(),
			'email_address' => $this->getEmailAddress(),
			'phone_number' => $this->getPhoneNumber(),
			'phone_extension' => $this->getPhoneExtension(),
			'task_note' => $this->getTaskNote(),
			'task_note_datetime' => $this->getTaskNoteDatetime(),
			'start_time' => $this->getStartTime(),
			'time_spent' => $this->getTimeSpent(),
			'is_published' => $this->getIsPublished(),
			'is_client_visible' => $this->getIsClientVisible(),
			'is_flagged' => $this->getIsFlagged(),
			'is_shared' => $this->getIsShared(),
			'abandoned_by' => $this->getAbandonedBy(),
			'abandoned_on' => $this->getAbandonedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'blocker_response_id' => $this->getBlockerResponseId(),
			'details' => $this->getDetails()
		);
	}

}
?>