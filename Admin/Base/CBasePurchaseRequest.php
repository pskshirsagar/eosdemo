<?php

class CBasePurchaseRequest extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.purchase_requests';

	protected $m_intId;
	protected $m_intPurchaseRequestTypeId;
	protected $m_intPurchaseRequestStatusTypeId;
	protected $m_intApproverEmployeeId;
	protected $m_intEmployeeId;
	protected $m_intDesignationIdOld;
	protected $m_intDesignationIdNew;
	protected $m_intPsAssetTypeId;
	protected $m_intCurrentSalaryTypeId;
	protected $m_intProposedSalaryTypeId;
	protected $m_intPurchaseRequestId;
	protected $m_intBonusTypeId;
	protected $m_intTeamId;
	protected $m_strBonusTypeDescription;
	protected $m_strBaseAmountDollarsEncrypted;
	protected $m_strNewAmountDollarsEncrypted;
	protected $m_strApprovedAmountDollarsEncrypted;
	protected $m_intEstimatedHoursOld;
	protected $m_intEstimatedHoursNew;
	protected $m_strHourlyRateOldEncrypted;
	protected $m_strHourlyRateNewEncrypted;
	protected $m_strHourlyRateApprovedEncrypted;
	protected $m_intAnnualBonusTypeId;
	protected $m_strAnnualBonusPotentialEncrypted;
	protected $m_strReason;
	protected $m_strNotes;
	protected $m_strTeamLeadComments;
	protected $m_strProjectManagerComments;
	protected $m_strBuHrComments;
	protected $m_strFinalComments;
	protected $m_strReopenedReason;
	protected $m_strEffectiveDate;
	protected $m_strRequestDatetime;
	protected $m_fltDollarToRupeeExchangeRate;
	protected $m_intIsReencrypted;
	protected $m_strBonusRequirement;
	protected $m_intRequestedBy;
	protected $m_strApprovedOn;
	protected $m_strDeniedOn;
	protected $m_intReopenedBy;
	protected $m_strReopenedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_strDestinationCountry;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsInternalHire;
	protected $m_intTechnologyId;

	public function __construct() {
		parent::__construct();

		$this->m_intPurchaseRequestStatusTypeId = '6';
		$this->m_intIsReencrypted = '0';
		$this->m_boolIsInternalHire = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['purchase_request_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchaseRequestTypeId', trim( $arrValues['purchase_request_type_id'] ) ); elseif( isset( $arrValues['purchase_request_type_id'] ) ) $this->setPurchaseRequestTypeId( $arrValues['purchase_request_type_id'] );
		if( isset( $arrValues['purchase_request_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchaseRequestStatusTypeId', trim( $arrValues['purchase_request_status_type_id'] ) ); elseif( isset( $arrValues['purchase_request_status_type_id'] ) ) $this->setPurchaseRequestStatusTypeId( $arrValues['purchase_request_status_type_id'] );
		if( isset( $arrValues['approver_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intApproverEmployeeId', trim( $arrValues['approver_employee_id'] ) ); elseif( isset( $arrValues['approver_employee_id'] ) ) $this->setApproverEmployeeId( $arrValues['approver_employee_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['designation_id_old'] ) && $boolDirectSet ) $this->set( 'm_intDesignationIdOld', trim( $arrValues['designation_id_old'] ) ); elseif( isset( $arrValues['designation_id_old'] ) ) $this->setDesignationIdOld( $arrValues['designation_id_old'] );
		if( isset( $arrValues['designation_id_new'] ) && $boolDirectSet ) $this->set( 'm_intDesignationIdNew', trim( $arrValues['designation_id_new'] ) ); elseif( isset( $arrValues['designation_id_new'] ) ) $this->setDesignationIdNew( $arrValues['designation_id_new'] );
		if( isset( $arrValues['ps_asset_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsAssetTypeId', trim( $arrValues['ps_asset_type_id'] ) ); elseif( isset( $arrValues['ps_asset_type_id'] ) ) $this->setPsAssetTypeId( $arrValues['ps_asset_type_id'] );
		if( isset( $arrValues['current_salary_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCurrentSalaryTypeId', trim( $arrValues['current_salary_type_id'] ) ); elseif( isset( $arrValues['current_salary_type_id'] ) ) $this->setCurrentSalaryTypeId( $arrValues['current_salary_type_id'] );
		if( isset( $arrValues['proposed_salary_type_id'] ) && $boolDirectSet ) $this->set( 'm_intProposedSalaryTypeId', trim( $arrValues['proposed_salary_type_id'] ) ); elseif( isset( $arrValues['proposed_salary_type_id'] ) ) $this->setProposedSalaryTypeId( $arrValues['proposed_salary_type_id'] );
		if( isset( $arrValues['purchase_request_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchaseRequestId', trim( $arrValues['purchase_request_id'] ) ); elseif( isset( $arrValues['purchase_request_id'] ) ) $this->setPurchaseRequestId( $arrValues['purchase_request_id'] );
		if( isset( $arrValues['bonus_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBonusTypeId', trim( $arrValues['bonus_type_id'] ) ); elseif( isset( $arrValues['bonus_type_id'] ) ) $this->setBonusTypeId( $arrValues['bonus_type_id'] );
		if( isset( $arrValues['team_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamId', trim( $arrValues['team_id'] ) ); elseif( isset( $arrValues['team_id'] ) ) $this->setTeamId( $arrValues['team_id'] );
		if( isset( $arrValues['bonus_type_description'] ) && $boolDirectSet ) $this->set( 'm_strBonusTypeDescription', trim( $arrValues['bonus_type_description'] ) ); elseif( isset( $arrValues['bonus_type_description'] ) ) $this->setBonusTypeDescription( $arrValues['bonus_type_description'] );
		if( isset( $arrValues['base_amount_dollars_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBaseAmountDollarsEncrypted', trim( $arrValues['base_amount_dollars_encrypted'] ) ); elseif( isset( $arrValues['base_amount_dollars_encrypted'] ) ) $this->setBaseAmountDollarsEncrypted( $arrValues['base_amount_dollars_encrypted'] );
		if( isset( $arrValues['new_amount_dollars_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strNewAmountDollarsEncrypted', trim( $arrValues['new_amount_dollars_encrypted'] ) ); elseif( isset( $arrValues['new_amount_dollars_encrypted'] ) ) $this->setNewAmountDollarsEncrypted( $arrValues['new_amount_dollars_encrypted'] );
		if( isset( $arrValues['approved_amount_dollars_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strApprovedAmountDollarsEncrypted', trim( $arrValues['approved_amount_dollars_encrypted'] ) ); elseif( isset( $arrValues['approved_amount_dollars_encrypted'] ) ) $this->setApprovedAmountDollarsEncrypted( $arrValues['approved_amount_dollars_encrypted'] );
		if( isset( $arrValues['estimated_hours_old'] ) && $boolDirectSet ) $this->set( 'm_intEstimatedHoursOld', trim( $arrValues['estimated_hours_old'] ) ); elseif( isset( $arrValues['estimated_hours_old'] ) ) $this->setEstimatedHoursOld( $arrValues['estimated_hours_old'] );
		if( isset( $arrValues['estimated_hours_new'] ) && $boolDirectSet ) $this->set( 'm_intEstimatedHoursNew', trim( $arrValues['estimated_hours_new'] ) ); elseif( isset( $arrValues['estimated_hours_new'] ) ) $this->setEstimatedHoursNew( $arrValues['estimated_hours_new'] );
		if( isset( $arrValues['hourly_rate_old_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strHourlyRateOldEncrypted', trim( $arrValues['hourly_rate_old_encrypted'] ) ); elseif( isset( $arrValues['hourly_rate_old_encrypted'] ) ) $this->setHourlyRateOldEncrypted( $arrValues['hourly_rate_old_encrypted'] );
		if( isset( $arrValues['hourly_rate_new_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strHourlyRateNewEncrypted', trim( $arrValues['hourly_rate_new_encrypted'] ) ); elseif( isset( $arrValues['hourly_rate_new_encrypted'] ) ) $this->setHourlyRateNewEncrypted( $arrValues['hourly_rate_new_encrypted'] );
		if( isset( $arrValues['hourly_rate_approved_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strHourlyRateApprovedEncrypted', trim( $arrValues['hourly_rate_approved_encrypted'] ) ); elseif( isset( $arrValues['hourly_rate_approved_encrypted'] ) ) $this->setHourlyRateApprovedEncrypted( $arrValues['hourly_rate_approved_encrypted'] );
		if( isset( $arrValues['annual_bonus_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAnnualBonusTypeId', trim( $arrValues['annual_bonus_type_id'] ) ); elseif( isset( $arrValues['annual_bonus_type_id'] ) ) $this->setAnnualBonusTypeId( $arrValues['annual_bonus_type_id'] );
		if( isset( $arrValues['annual_bonus_potential_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strAnnualBonusPotentialEncrypted', trim( $arrValues['annual_bonus_potential_encrypted'] ) ); elseif( isset( $arrValues['annual_bonus_potential_encrypted'] ) ) $this->setAnnualBonusPotentialEncrypted( $arrValues['annual_bonus_potential_encrypted'] );
		if( isset( $arrValues['reason'] ) && $boolDirectSet ) $this->set( 'm_strReason', trim( $arrValues['reason'] ) ); elseif( isset( $arrValues['reason'] ) ) $this->setReason( $arrValues['reason'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['team_lead_comments'] ) && $boolDirectSet ) $this->set( 'm_strTeamLeadComments', trim( $arrValues['team_lead_comments'] ) ); elseif( isset( $arrValues['team_lead_comments'] ) ) $this->setTeamLeadComments( $arrValues['team_lead_comments'] );
		if( isset( $arrValues['project_manager_comments'] ) && $boolDirectSet ) $this->set( 'm_strProjectManagerComments', trim( $arrValues['project_manager_comments'] ) ); elseif( isset( $arrValues['project_manager_comments'] ) ) $this->setProjectManagerComments( $arrValues['project_manager_comments'] );
		if( isset( $arrValues['bu_hr_comments'] ) && $boolDirectSet ) $this->set( 'm_strBuHrComments', trim( $arrValues['bu_hr_comments'] ) ); elseif( isset( $arrValues['bu_hr_comments'] ) ) $this->setBuHrComments( $arrValues['bu_hr_comments'] );
		if( isset( $arrValues['final_comments'] ) && $boolDirectSet ) $this->set( 'm_strFinalComments', trim( $arrValues['final_comments'] ) ); elseif( isset( $arrValues['final_comments'] ) ) $this->setFinalComments( $arrValues['final_comments'] );
		if( isset( $arrValues['reopened_reason'] ) && $boolDirectSet ) $this->set( 'm_strReopenedReason', trim( $arrValues['reopened_reason'] ) ); elseif( isset( $arrValues['reopened_reason'] ) ) $this->setReopenedReason( $arrValues['reopened_reason'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['request_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequestDatetime', trim( $arrValues['request_datetime'] ) ); elseif( isset( $arrValues['request_datetime'] ) ) $this->setRequestDatetime( $arrValues['request_datetime'] );
		if( isset( $arrValues['dollar_to_rupee_exchange_rate'] ) && $boolDirectSet ) $this->set( 'm_fltDollarToRupeeExchangeRate', trim( $arrValues['dollar_to_rupee_exchange_rate'] ) ); elseif( isset( $arrValues['dollar_to_rupee_exchange_rate'] ) ) $this->setDollarToRupeeExchangeRate( $arrValues['dollar_to_rupee_exchange_rate'] );
		if( isset( $arrValues['is_reencrypted'] ) && $boolDirectSet ) $this->set( 'm_intIsReencrypted', trim( $arrValues['is_reencrypted'] ) ); elseif( isset( $arrValues['is_reencrypted'] ) ) $this->setIsReencrypted( $arrValues['is_reencrypted'] );
		if( isset( $arrValues['bonus_requirement'] ) && $boolDirectSet ) $this->set( 'm_strBonusRequirement', trim( $arrValues['bonus_requirement'] ) ); elseif( isset( $arrValues['bonus_requirement'] ) ) $this->setBonusRequirement( $arrValues['bonus_requirement'] );
		if( isset( $arrValues['requested_by'] ) && $boolDirectSet ) $this->set( 'm_intRequestedBy', trim( $arrValues['requested_by'] ) ); elseif( isset( $arrValues['requested_by'] ) ) $this->setRequestedBy( $arrValues['requested_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['denied_on'] ) && $boolDirectSet ) $this->set( 'm_strDeniedOn', trim( $arrValues['denied_on'] ) ); elseif( isset( $arrValues['denied_on'] ) ) $this->setDeniedOn( $arrValues['denied_on'] );
		if( isset( $arrValues['reopened_by'] ) && $boolDirectSet ) $this->set( 'm_intReopenedBy', trim( $arrValues['reopened_by'] ) ); elseif( isset( $arrValues['reopened_by'] ) ) $this->setReopenedBy( $arrValues['reopened_by'] );
		if( isset( $arrValues['reopened_on'] ) && $boolDirectSet ) $this->set( 'm_strReopenedOn', trim( $arrValues['reopened_on'] ) ); elseif( isset( $arrValues['reopened_on'] ) ) $this->setReopenedOn( $arrValues['reopened_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['destination_country'] ) && $boolDirectSet ) $this->set( 'm_strDestinationCountry', trim( $arrValues['destination_country'] ) ); elseif( isset( $arrValues['destination_country'] ) ) $this->setDestinationCountry( $arrValues['destination_country'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_internal_hire'] ) && $boolDirectSet ) $this->set( 'm_boolIsInternalHire', trim( stripcslashes( $arrValues['is_internal_hire'] ) ) ); elseif( isset( $arrValues['is_internal_hire'] ) ) $this->setIsInternalHire( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_internal_hire'] ) : $arrValues['is_internal_hire'] );
		if( isset( $arrValues['technology_id'] ) && $boolDirectSet ) $this->set( 'm_intTechnologyId', trim( $arrValues['technology_id'] ) ); elseif( isset( $arrValues['technology_id'] ) ) $this->setTechnologyId( $arrValues['technology_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPurchaseRequestTypeId( $intPurchaseRequestTypeId ) {
		$this->set( 'm_intPurchaseRequestTypeId', CStrings::strToIntDef( $intPurchaseRequestTypeId, NULL, false ) );
	}

	public function getPurchaseRequestTypeId() {
		return $this->m_intPurchaseRequestTypeId;
	}

	public function sqlPurchaseRequestTypeId() {
		return ( true == isset( $this->m_intPurchaseRequestTypeId ) ) ? ( string ) $this->m_intPurchaseRequestTypeId : 'NULL';
	}

	public function setPurchaseRequestStatusTypeId( $intPurchaseRequestStatusTypeId ) {
		$this->set( 'm_intPurchaseRequestStatusTypeId', CStrings::strToIntDef( $intPurchaseRequestStatusTypeId, NULL, false ) );
	}

	public function getPurchaseRequestStatusTypeId() {
		return $this->m_intPurchaseRequestStatusTypeId;
	}

	public function sqlPurchaseRequestStatusTypeId() {
		return ( true == isset( $this->m_intPurchaseRequestStatusTypeId ) ) ? ( string ) $this->m_intPurchaseRequestStatusTypeId : '6';
	}

	public function setApproverEmployeeId( $intApproverEmployeeId ) {
		$this->set( 'm_intApproverEmployeeId', CStrings::strToIntDef( $intApproverEmployeeId, NULL, false ) );
	}

	public function getApproverEmployeeId() {
		return $this->m_intApproverEmployeeId;
	}

	public function sqlApproverEmployeeId() {
		return ( true == isset( $this->m_intApproverEmployeeId ) ) ? ( string ) $this->m_intApproverEmployeeId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setDesignationIdOld( $intDesignationIdOld ) {
		$this->set( 'm_intDesignationIdOld', CStrings::strToIntDef( $intDesignationIdOld, NULL, false ) );
	}

	public function getDesignationIdOld() {
		return $this->m_intDesignationIdOld;
	}

	public function sqlDesignationIdOld() {
		return ( true == isset( $this->m_intDesignationIdOld ) ) ? ( string ) $this->m_intDesignationIdOld : 'NULL';
	}

	public function setDesignationIdNew( $intDesignationIdNew ) {
		$this->set( 'm_intDesignationIdNew', CStrings::strToIntDef( $intDesignationIdNew, NULL, false ) );
	}

	public function getDesignationIdNew() {
		return $this->m_intDesignationIdNew;
	}

	public function sqlDesignationIdNew() {
		return ( true == isset( $this->m_intDesignationIdNew ) ) ? ( string ) $this->m_intDesignationIdNew : 'NULL';
	}

	public function setPsAssetTypeId( $intPsAssetTypeId ) {
		$this->set( 'm_intPsAssetTypeId', CStrings::strToIntDef( $intPsAssetTypeId, NULL, false ) );
	}

	public function getPsAssetTypeId() {
		return $this->m_intPsAssetTypeId;
	}

	public function sqlPsAssetTypeId() {
		return ( true == isset( $this->m_intPsAssetTypeId ) ) ? ( string ) $this->m_intPsAssetTypeId : 'NULL';
	}

	public function setCurrentSalaryTypeId( $intCurrentSalaryTypeId ) {
		$this->set( 'm_intCurrentSalaryTypeId', CStrings::strToIntDef( $intCurrentSalaryTypeId, NULL, false ) );
	}

	public function getCurrentSalaryTypeId() {
		return $this->m_intCurrentSalaryTypeId;
	}

	public function sqlCurrentSalaryTypeId() {
		return ( true == isset( $this->m_intCurrentSalaryTypeId ) ) ? ( string ) $this->m_intCurrentSalaryTypeId : 'NULL';
	}

	public function setProposedSalaryTypeId( $intProposedSalaryTypeId ) {
		$this->set( 'm_intProposedSalaryTypeId', CStrings::strToIntDef( $intProposedSalaryTypeId, NULL, false ) );
	}

	public function getProposedSalaryTypeId() {
		return $this->m_intProposedSalaryTypeId;
	}

	public function sqlProposedSalaryTypeId() {
		return ( true == isset( $this->m_intProposedSalaryTypeId ) ) ? ( string ) $this->m_intProposedSalaryTypeId : 'NULL';
	}

	public function setPurchaseRequestId( $intPurchaseRequestId ) {
		$this->set( 'm_intPurchaseRequestId', CStrings::strToIntDef( $intPurchaseRequestId, NULL, false ) );
	}

	public function getPurchaseRequestId() {
		return $this->m_intPurchaseRequestId;
	}

	public function sqlPurchaseRequestId() {
		return ( true == isset( $this->m_intPurchaseRequestId ) ) ? ( string ) $this->m_intPurchaseRequestId : 'NULL';
	}

	public function setBonusTypeId( $intBonusTypeId ) {
		$this->set( 'm_intBonusTypeId', CStrings::strToIntDef( $intBonusTypeId, NULL, false ) );
	}

	public function getBonusTypeId() {
		return $this->m_intBonusTypeId;
	}

	public function sqlBonusTypeId() {
		return ( true == isset( $this->m_intBonusTypeId ) ) ? ( string ) $this->m_intBonusTypeId : 'NULL';
	}

	public function setTeamId( $intTeamId ) {
		$this->set( 'm_intTeamId', CStrings::strToIntDef( $intTeamId, NULL, false ) );
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function sqlTeamId() {
		return ( true == isset( $this->m_intTeamId ) ) ? ( string ) $this->m_intTeamId : 'NULL';
	}

	public function setBonusTypeDescription( $strBonusTypeDescription ) {
		$this->set( 'm_strBonusTypeDescription', CStrings::strTrimDef( $strBonusTypeDescription, 255, NULL, true ) );
	}

	public function getBonusTypeDescription() {
		return $this->m_strBonusTypeDescription;
	}

	public function sqlBonusTypeDescription() {
		return ( true == isset( $this->m_strBonusTypeDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBonusTypeDescription ) : '\'' . addslashes( $this->m_strBonusTypeDescription ) . '\'' ) : 'NULL';
	}

	public function setBaseAmountDollarsEncrypted( $strBaseAmountDollarsEncrypted ) {
		$this->set( 'm_strBaseAmountDollarsEncrypted', CStrings::strTrimDef( $strBaseAmountDollarsEncrypted, -1, NULL, true ) );
	}

	public function getBaseAmountDollarsEncrypted() {
		return $this->m_strBaseAmountDollarsEncrypted;
	}

	public function sqlBaseAmountDollarsEncrypted() {
		return ( true == isset( $this->m_strBaseAmountDollarsEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBaseAmountDollarsEncrypted ) : '\'' . addslashes( $this->m_strBaseAmountDollarsEncrypted ) . '\'' ) : 'NULL';
	}

	public function setNewAmountDollarsEncrypted( $strNewAmountDollarsEncrypted ) {
		$this->set( 'm_strNewAmountDollarsEncrypted', CStrings::strTrimDef( $strNewAmountDollarsEncrypted, -1, NULL, true ) );
	}

	public function getNewAmountDollarsEncrypted() {
		return $this->m_strNewAmountDollarsEncrypted;
	}

	public function sqlNewAmountDollarsEncrypted() {
		return ( true == isset( $this->m_strNewAmountDollarsEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNewAmountDollarsEncrypted ) : '\'' . addslashes( $this->m_strNewAmountDollarsEncrypted ) . '\'' ) : 'NULL';
	}

	public function setApprovedAmountDollarsEncrypted( $strApprovedAmountDollarsEncrypted ) {
		$this->set( 'm_strApprovedAmountDollarsEncrypted', CStrings::strTrimDef( $strApprovedAmountDollarsEncrypted, -1, NULL, true ) );
	}

	public function getApprovedAmountDollarsEncrypted() {
		return $this->m_strApprovedAmountDollarsEncrypted;
	}

	public function sqlApprovedAmountDollarsEncrypted() {
		return ( true == isset( $this->m_strApprovedAmountDollarsEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovedAmountDollarsEncrypted ) : '\'' . addslashes( $this->m_strApprovedAmountDollarsEncrypted ) . '\'' ) : 'NULL';
	}

	public function setEstimatedHoursOld( $intEstimatedHoursOld ) {
		$this->set( 'm_intEstimatedHoursOld', CStrings::strToIntDef( $intEstimatedHoursOld, NULL, false ) );
	}

	public function getEstimatedHoursOld() {
		return $this->m_intEstimatedHoursOld;
	}

	public function sqlEstimatedHoursOld() {
		return ( true == isset( $this->m_intEstimatedHoursOld ) ) ? ( string ) $this->m_intEstimatedHoursOld : 'NULL';
	}

	public function setEstimatedHoursNew( $intEstimatedHoursNew ) {
		$this->set( 'm_intEstimatedHoursNew', CStrings::strToIntDef( $intEstimatedHoursNew, NULL, false ) );
	}

	public function getEstimatedHoursNew() {
		return $this->m_intEstimatedHoursNew;
	}

	public function sqlEstimatedHoursNew() {
		return ( true == isset( $this->m_intEstimatedHoursNew ) ) ? ( string ) $this->m_intEstimatedHoursNew : 'NULL';
	}

	public function setHourlyRateOldEncrypted( $strHourlyRateOldEncrypted ) {
		$this->set( 'm_strHourlyRateOldEncrypted', CStrings::strTrimDef( $strHourlyRateOldEncrypted, -1, NULL, true ) );
	}

	public function getHourlyRateOldEncrypted() {
		return $this->m_strHourlyRateOldEncrypted;
	}

	public function sqlHourlyRateOldEncrypted() {
		return ( true == isset( $this->m_strHourlyRateOldEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHourlyRateOldEncrypted ) : '\'' . addslashes( $this->m_strHourlyRateOldEncrypted ) . '\'' ) : 'NULL';
	}

	public function setHourlyRateNewEncrypted( $strHourlyRateNewEncrypted ) {
		$this->set( 'm_strHourlyRateNewEncrypted', CStrings::strTrimDef( $strHourlyRateNewEncrypted, -1, NULL, true ) );
	}

	public function getHourlyRateNewEncrypted() {
		return $this->m_strHourlyRateNewEncrypted;
	}

	public function sqlHourlyRateNewEncrypted() {
		return ( true == isset( $this->m_strHourlyRateNewEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHourlyRateNewEncrypted ) : '\'' . addslashes( $this->m_strHourlyRateNewEncrypted ) . '\'' ) : 'NULL';
	}

	public function setHourlyRateApprovedEncrypted( $strHourlyRateApprovedEncrypted ) {
		$this->set( 'm_strHourlyRateApprovedEncrypted', CStrings::strTrimDef( $strHourlyRateApprovedEncrypted, -1, NULL, true ) );
	}

	public function getHourlyRateApprovedEncrypted() {
		return $this->m_strHourlyRateApprovedEncrypted;
	}

	public function sqlHourlyRateApprovedEncrypted() {
		return ( true == isset( $this->m_strHourlyRateApprovedEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHourlyRateApprovedEncrypted ) : '\'' . addslashes( $this->m_strHourlyRateApprovedEncrypted ) . '\'' ) : 'NULL';
	}

	public function setAnnualBonusTypeId( $intAnnualBonusTypeId ) {
		$this->set( 'm_intAnnualBonusTypeId', CStrings::strToIntDef( $intAnnualBonusTypeId, NULL, false ) );
	}

	public function getAnnualBonusTypeId() {
		return $this->m_intAnnualBonusTypeId;
	}

	public function sqlAnnualBonusTypeId() {
		return ( true == isset( $this->m_intAnnualBonusTypeId ) ) ? ( string ) $this->m_intAnnualBonusTypeId : 'NULL';
	}

	public function setAnnualBonusPotentialEncrypted( $strAnnualBonusPotentialEncrypted ) {
		$this->set( 'm_strAnnualBonusPotentialEncrypted', CStrings::strTrimDef( $strAnnualBonusPotentialEncrypted, -1, NULL, true ) );
	}

	public function getAnnualBonusPotentialEncrypted() {
		return $this->m_strAnnualBonusPotentialEncrypted;
	}

	public function sqlAnnualBonusPotentialEncrypted() {
		return ( true == isset( $this->m_strAnnualBonusPotentialEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAnnualBonusPotentialEncrypted ) : '\'' . addslashes( $this->m_strAnnualBonusPotentialEncrypted ) . '\'' ) : 'NULL';
	}

	public function setReason( $strReason ) {
		$this->set( 'm_strReason', CStrings::strTrimDef( $strReason, -1, NULL, true ) );
	}

	public function getReason() {
		return $this->m_strReason;
	}

	public function sqlReason() {
		return ( true == isset( $this->m_strReason ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReason ) : '\'' . addslashes( $this->m_strReason ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setTeamLeadComments( $strTeamLeadComments ) {
		$this->set( 'm_strTeamLeadComments', CStrings::strTrimDef( $strTeamLeadComments, 2000, NULL, true ) );
	}

	public function getTeamLeadComments() {
		return $this->m_strTeamLeadComments;
	}

	public function sqlTeamLeadComments() {
		return ( true == isset( $this->m_strTeamLeadComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTeamLeadComments ) : '\'' . addslashes( $this->m_strTeamLeadComments ) . '\'' ) : 'NULL';
	}

	public function setProjectManagerComments( $strProjectManagerComments ) {
		$this->set( 'm_strProjectManagerComments', CStrings::strTrimDef( $strProjectManagerComments, 2000, NULL, true ) );
	}

	public function getProjectManagerComments() {
		return $this->m_strProjectManagerComments;
	}

	public function sqlProjectManagerComments() {
		return ( true == isset( $this->m_strProjectManagerComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProjectManagerComments ) : '\'' . addslashes( $this->m_strProjectManagerComments ) . '\'' ) : 'NULL';
	}

	public function setBuHrComments( $strBuHrComments ) {
		$this->set( 'm_strBuHrComments', CStrings::strTrimDef( $strBuHrComments, 2000, NULL, true ) );
	}

	public function getBuHrComments() {
		return $this->m_strBuHrComments;
	}

	public function sqlBuHrComments() {
		return ( true == isset( $this->m_strBuHrComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBuHrComments ) : '\'' . addslashes( $this->m_strBuHrComments ) . '\'' ) : 'NULL';
	}

	public function setFinalComments( $strFinalComments ) {
		$this->set( 'm_strFinalComments', CStrings::strTrimDef( $strFinalComments, 2000, NULL, true ) );
	}

	public function getFinalComments() {
		return $this->m_strFinalComments;
	}

	public function sqlFinalComments() {
		return ( true == isset( $this->m_strFinalComments ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFinalComments ) : '\'' . addslashes( $this->m_strFinalComments ) . '\'' ) : 'NULL';
	}

	public function setReopenedReason( $strReopenedReason ) {
		$this->set( 'm_strReopenedReason', CStrings::strTrimDef( $strReopenedReason, -1, NULL, true ) );
	}

	public function getReopenedReason() {
		return $this->m_strReopenedReason;
	}

	public function sqlReopenedReason() {
		return ( true == isset( $this->m_strReopenedReason ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReopenedReason ) : '\'' . addslashes( $this->m_strReopenedReason ) . '\'' ) : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NULL';
	}

	public function setRequestDatetime( $strRequestDatetime ) {
		$this->set( 'm_strRequestDatetime', CStrings::strTrimDef( $strRequestDatetime, -1, NULL, true ) );
	}

	public function getRequestDatetime() {
		return $this->m_strRequestDatetime;
	}

	public function sqlRequestDatetime() {
		return ( true == isset( $this->m_strRequestDatetime ) ) ? '\'' . $this->m_strRequestDatetime . '\'' : 'NULL';
	}

	public function setDollarToRupeeExchangeRate( $fltDollarToRupeeExchangeRate ) {
		$this->set( 'm_fltDollarToRupeeExchangeRate', CStrings::strToFloatDef( $fltDollarToRupeeExchangeRate, NULL, false, 2 ) );
	}

	public function getDollarToRupeeExchangeRate() {
		return $this->m_fltDollarToRupeeExchangeRate;
	}

	public function sqlDollarToRupeeExchangeRate() {
		return ( true == isset( $this->m_fltDollarToRupeeExchangeRate ) ) ? ( string ) $this->m_fltDollarToRupeeExchangeRate : 'NULL';
	}

	public function setIsReencrypted( $intIsReencrypted ) {
		$this->set( 'm_intIsReencrypted', CStrings::strToIntDef( $intIsReencrypted, NULL, false ) );
	}

	public function getIsReencrypted() {
		return $this->m_intIsReencrypted;
	}

	public function sqlIsReencrypted() {
		return ( true == isset( $this->m_intIsReencrypted ) ) ? ( string ) $this->m_intIsReencrypted : '0';
	}

	public function setBonusRequirement( $strBonusRequirement ) {
		$this->set( 'm_strBonusRequirement', CStrings::strTrimDef( $strBonusRequirement, -1, NULL, true ) );
	}

	public function getBonusRequirement() {
		return $this->m_strBonusRequirement;
	}

	public function sqlBonusRequirement() {
		return ( true == isset( $this->m_strBonusRequirement ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBonusRequirement ) : '\'' . addslashes( $this->m_strBonusRequirement ) . '\'' ) : 'NULL';
	}

	public function setRequestedBy( $intRequestedBy ) {
		$this->set( 'm_intRequestedBy', CStrings::strToIntDef( $intRequestedBy, NULL, false ) );
	}

	public function getRequestedBy() {
		return $this->m_intRequestedBy;
	}

	public function sqlRequestedBy() {
		return ( true == isset( $this->m_intRequestedBy ) ) ? ( string ) $this->m_intRequestedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeniedOn( $strDeniedOn ) {
		$this->set( 'm_strDeniedOn', CStrings::strTrimDef( $strDeniedOn, -1, NULL, true ) );
	}

	public function getDeniedOn() {
		return $this->m_strDeniedOn;
	}

	public function sqlDeniedOn() {
		return ( true == isset( $this->m_strDeniedOn ) ) ? '\'' . $this->m_strDeniedOn . '\'' : 'NULL';
	}

	public function setReopenedBy( $intReopenedBy ) {
		$this->set( 'm_intReopenedBy', CStrings::strToIntDef( $intReopenedBy, NULL, false ) );
	}

	public function getReopenedBy() {
		return $this->m_intReopenedBy;
	}

	public function sqlReopenedBy() {
		return ( true == isset( $this->m_intReopenedBy ) ) ? ( string ) $this->m_intReopenedBy : 'NULL';
	}

	public function setReopenedOn( $strReopenedOn ) {
		$this->set( 'm_strReopenedOn', CStrings::strTrimDef( $strReopenedOn, -1, NULL, true ) );
	}

	public function getReopenedOn() {
		return $this->m_strReopenedOn;
	}

	public function sqlReopenedOn() {
		return ( true == isset( $this->m_strReopenedOn ) ) ? '\'' . $this->m_strReopenedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setDestinationCountry( $strDestinationCountry ) {
		$this->set( 'm_strDestinationCountry', CStrings::strTrimDef( $strDestinationCountry, 2, NULL, true ) );
	}

	public function getDestinationCountry() {
		return $this->m_strDestinationCountry;
	}

	public function sqlDestinationCountry() {
		return ( true == isset( $this->m_strDestinationCountry ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDestinationCountry ) : '\'' . addslashes( $this->m_strDestinationCountry ) . '\'' ) : 'NULL';
	}

	public function setIsInternalHire( $boolIsInternalHire ) {
		$this->set( 'm_boolIsInternalHire', CStrings::strToBool( $boolIsInternalHire ) );
	}

	public function getIsInternalHire() {
		return $this->m_boolIsInternalHire;
	}

	public function sqlIsInternalHire() {
		return ( true == isset( $this->m_boolIsInternalHire ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInternalHire ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setTechnologyId( $intTechnologyId ) {
		$this->set( 'm_intTechnologyId', CStrings::strToIntDef( $intTechnologyId, NULL, false ) );
	}

	public function getTechnologyId() {
		return $this->m_intTechnologyId;
	}

	public function sqlTechnologyId() {
		return ( true == isset( $this->m_intTechnologyId ) ) ? ( string ) $this->m_intTechnologyId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, purchase_request_type_id, purchase_request_status_type_id, approver_employee_id, employee_id, designation_id_old, designation_id_new, ps_asset_type_id, current_salary_type_id, proposed_salary_type_id, purchase_request_id, bonus_type_id, team_id, bonus_type_description, base_amount_dollars_encrypted, new_amount_dollars_encrypted, approved_amount_dollars_encrypted, estimated_hours_old, estimated_hours_new, hourly_rate_old_encrypted, hourly_rate_new_encrypted, hourly_rate_approved_encrypted, annual_bonus_type_id, annual_bonus_potential_encrypted, reason, notes, team_lead_comments, project_manager_comments, bu_hr_comments, final_comments, reopened_reason, effective_date, request_datetime, dollar_to_rupee_exchange_rate, is_reencrypted, bonus_requirement, requested_by, approved_on, denied_on, reopened_by, reopened_on, created_by, created_on, updated_by, updated_on, deleted_by, deleted_on, destination_country, details, is_internal_hire, technology_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPurchaseRequestTypeId() . ', ' .
						$this->sqlPurchaseRequestStatusTypeId() . ', ' .
						$this->sqlApproverEmployeeId() . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlDesignationIdOld() . ', ' .
						$this->sqlDesignationIdNew() . ', ' .
						$this->sqlPsAssetTypeId() . ', ' .
						$this->sqlCurrentSalaryTypeId() . ', ' .
						$this->sqlProposedSalaryTypeId() . ', ' .
						$this->sqlPurchaseRequestId() . ', ' .
						$this->sqlBonusTypeId() . ', ' .
						$this->sqlTeamId() . ', ' .
						$this->sqlBonusTypeDescription() . ', ' .
						$this->sqlBaseAmountDollarsEncrypted() . ', ' .
						$this->sqlNewAmountDollarsEncrypted() . ', ' .
						$this->sqlApprovedAmountDollarsEncrypted() . ', ' .
						$this->sqlEstimatedHoursOld() . ', ' .
						$this->sqlEstimatedHoursNew() . ', ' .
						$this->sqlHourlyRateOldEncrypted() . ', ' .
						$this->sqlHourlyRateNewEncrypted() . ', ' .
						$this->sqlHourlyRateApprovedEncrypted() . ', ' .
						$this->sqlAnnualBonusTypeId() . ', ' .
						$this->sqlAnnualBonusPotentialEncrypted() . ', ' .
						$this->sqlReason() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlTeamLeadComments() . ', ' .
						$this->sqlProjectManagerComments() . ', ' .
						$this->sqlBuHrComments() . ', ' .
						$this->sqlFinalComments() . ', ' .
						$this->sqlReopenedReason() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						$this->sqlRequestDatetime() . ', ' .
						$this->sqlDollarToRupeeExchangeRate() . ', ' .
						$this->sqlIsReencrypted() . ', ' .
						$this->sqlBonusRequirement() . ', ' .
						$this->sqlRequestedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlDeniedOn() . ', ' .
						$this->sqlReopenedBy() . ', ' .
						$this->sqlReopenedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDestinationCountry() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsInternalHire() . ', ' .
						$this->sqlTechnologyId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchase_request_type_id = ' . $this->sqlPurchaseRequestTypeId(). ',' ; } elseif( true == array_key_exists( 'PurchaseRequestTypeId', $this->getChangedColumns() ) ) { $strSql .= ' purchase_request_type_id = ' . $this->sqlPurchaseRequestTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchase_request_status_type_id = ' . $this->sqlPurchaseRequestStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'PurchaseRequestStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' purchase_request_status_type_id = ' . $this->sqlPurchaseRequestStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approver_employee_id = ' . $this->sqlApproverEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ApproverEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' approver_employee_id = ' . $this->sqlApproverEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id_old = ' . $this->sqlDesignationIdOld(). ',' ; } elseif( true == array_key_exists( 'DesignationIdOld', $this->getChangedColumns() ) ) { $strSql .= ' designation_id_old = ' . $this->sqlDesignationIdOld() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id_new = ' . $this->sqlDesignationIdNew(). ',' ; } elseif( true == array_key_exists( 'DesignationIdNew', $this->getChangedColumns() ) ) { $strSql .= ' designation_id_new = ' . $this->sqlDesignationIdNew() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_asset_type_id = ' . $this->sqlPsAssetTypeId(). ',' ; } elseif( true == array_key_exists( 'PsAssetTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ps_asset_type_id = ' . $this->sqlPsAssetTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_salary_type_id = ' . $this->sqlCurrentSalaryTypeId(). ',' ; } elseif( true == array_key_exists( 'CurrentSalaryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' current_salary_type_id = ' . $this->sqlCurrentSalaryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposed_salary_type_id = ' . $this->sqlProposedSalaryTypeId(). ',' ; } elseif( true == array_key_exists( 'ProposedSalaryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' proposed_salary_type_id = ' . $this->sqlProposedSalaryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId(). ',' ; } elseif( true == array_key_exists( 'PurchaseRequestId', $this->getChangedColumns() ) ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bonus_type_id = ' . $this->sqlBonusTypeId(). ',' ; } elseif( true == array_key_exists( 'BonusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' bonus_type_id = ' . $this->sqlBonusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_id = ' . $this->sqlTeamId(). ',' ; } elseif( true == array_key_exists( 'TeamId', $this->getChangedColumns() ) ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bonus_type_description = ' . $this->sqlBonusTypeDescription(). ',' ; } elseif( true == array_key_exists( 'BonusTypeDescription', $this->getChangedColumns() ) ) { $strSql .= ' bonus_type_description = ' . $this->sqlBonusTypeDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_amount_dollars_encrypted = ' . $this->sqlBaseAmountDollarsEncrypted(). ',' ; } elseif( true == array_key_exists( 'BaseAmountDollarsEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' base_amount_dollars_encrypted = ' . $this->sqlBaseAmountDollarsEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_amount_dollars_encrypted = ' . $this->sqlNewAmountDollarsEncrypted(). ',' ; } elseif( true == array_key_exists( 'NewAmountDollarsEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' new_amount_dollars_encrypted = ' . $this->sqlNewAmountDollarsEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_amount_dollars_encrypted = ' . $this->sqlApprovedAmountDollarsEncrypted(). ',' ; } elseif( true == array_key_exists( 'ApprovedAmountDollarsEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' approved_amount_dollars_encrypted = ' . $this->sqlApprovedAmountDollarsEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimated_hours_old = ' . $this->sqlEstimatedHoursOld(). ',' ; } elseif( true == array_key_exists( 'EstimatedHoursOld', $this->getChangedColumns() ) ) { $strSql .= ' estimated_hours_old = ' . $this->sqlEstimatedHoursOld() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimated_hours_new = ' . $this->sqlEstimatedHoursNew(). ',' ; } elseif( true == array_key_exists( 'EstimatedHoursNew', $this->getChangedColumns() ) ) { $strSql .= ' estimated_hours_new = ' . $this->sqlEstimatedHoursNew() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hourly_rate_old_encrypted = ' . $this->sqlHourlyRateOldEncrypted(). ',' ; } elseif( true == array_key_exists( 'HourlyRateOldEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' hourly_rate_old_encrypted = ' . $this->sqlHourlyRateOldEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hourly_rate_new_encrypted = ' . $this->sqlHourlyRateNewEncrypted(). ',' ; } elseif( true == array_key_exists( 'HourlyRateNewEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' hourly_rate_new_encrypted = ' . $this->sqlHourlyRateNewEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hourly_rate_approved_encrypted = ' . $this->sqlHourlyRateApprovedEncrypted(). ',' ; } elseif( true == array_key_exists( 'HourlyRateApprovedEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' hourly_rate_approved_encrypted = ' . $this->sqlHourlyRateApprovedEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' annual_bonus_type_id = ' . $this->sqlAnnualBonusTypeId(). ',' ; } elseif( true == array_key_exists( 'AnnualBonusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' annual_bonus_type_id = ' . $this->sqlAnnualBonusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' annual_bonus_potential_encrypted = ' . $this->sqlAnnualBonusPotentialEncrypted(). ',' ; } elseif( true == array_key_exists( 'AnnualBonusPotentialEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' annual_bonus_potential_encrypted = ' . $this->sqlAnnualBonusPotentialEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason = ' . $this->sqlReason(). ',' ; } elseif( true == array_key_exists( 'Reason', $this->getChangedColumns() ) ) { $strSql .= ' reason = ' . $this->sqlReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_lead_comments = ' . $this->sqlTeamLeadComments(). ',' ; } elseif( true == array_key_exists( 'TeamLeadComments', $this->getChangedColumns() ) ) { $strSql .= ' team_lead_comments = ' . $this->sqlTeamLeadComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_manager_comments = ' . $this->sqlProjectManagerComments(). ',' ; } elseif( true == array_key_exists( 'ProjectManagerComments', $this->getChangedColumns() ) ) { $strSql .= ' project_manager_comments = ' . $this->sqlProjectManagerComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bu_hr_comments = ' . $this->sqlBuHrComments(). ',' ; } elseif( true == array_key_exists( 'BuHrComments', $this->getChangedColumns() ) ) { $strSql .= ' bu_hr_comments = ' . $this->sqlBuHrComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' final_comments = ' . $this->sqlFinalComments(). ',' ; } elseif( true == array_key_exists( 'FinalComments', $this->getChangedColumns() ) ) { $strSql .= ' final_comments = ' . $this->sqlFinalComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reopened_reason = ' . $this->sqlReopenedReason(). ',' ; } elseif( true == array_key_exists( 'ReopenedReason', $this->getChangedColumns() ) ) { $strSql .= ' reopened_reason = ' . $this->sqlReopenedReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime(). ',' ; } elseif( true == array_key_exists( 'RequestDatetime', $this->getChangedColumns() ) ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dollar_to_rupee_exchange_rate = ' . $this->sqlDollarToRupeeExchangeRate(). ',' ; } elseif( true == array_key_exists( 'DollarToRupeeExchangeRate', $this->getChangedColumns() ) ) { $strSql .= ' dollar_to_rupee_exchange_rate = ' . $this->sqlDollarToRupeeExchangeRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reencrypted = ' . $this->sqlIsReencrypted(). ',' ; } elseif( true == array_key_exists( 'IsReencrypted', $this->getChangedColumns() ) ) { $strSql .= ' is_reencrypted = ' . $this->sqlIsReencrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bonus_requirement = ' . $this->sqlBonusRequirement(). ',' ; } elseif( true == array_key_exists( 'BonusRequirement', $this->getChangedColumns() ) ) { $strSql .= ' bonus_requirement = ' . $this->sqlBonusRequirement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy(). ',' ; } elseif( true == array_key_exists( 'RequestedBy', $this->getChangedColumns() ) ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn(). ',' ; } elseif( true == array_key_exists( 'DeniedOn', $this->getChangedColumns() ) ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reopened_by = ' . $this->sqlReopenedBy(). ',' ; } elseif( true == array_key_exists( 'ReopenedBy', $this->getChangedColumns() ) ) { $strSql .= ' reopened_by = ' . $this->sqlReopenedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reopened_on = ' . $this->sqlReopenedOn(). ',' ; } elseif( true == array_key_exists( 'ReopenedOn', $this->getChangedColumns() ) ) { $strSql .= ' reopened_on = ' . $this->sqlReopenedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_country = ' . $this->sqlDestinationCountry(). ',' ; } elseif( true == array_key_exists( 'DestinationCountry', $this->getChangedColumns() ) ) { $strSql .= ' destination_country = ' . $this->sqlDestinationCountry() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_internal_hire = ' . $this->sqlIsInternalHire(). ',' ; } elseif( true == array_key_exists( 'IsInternalHire', $this->getChangedColumns() ) ) { $strSql .= ' is_internal_hire = ' . $this->sqlIsInternalHire() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' technology_id = ' . $this->sqlTechnologyId(). ',' ; } elseif( true == array_key_exists( 'TechnologyId', $this->getChangedColumns() ) ) { $strSql .= ' technology_id = ' . $this->sqlTechnologyId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'purchase_request_type_id' => $this->getPurchaseRequestTypeId(),
			'purchase_request_status_type_id' => $this->getPurchaseRequestStatusTypeId(),
			'approver_employee_id' => $this->getApproverEmployeeId(),
			'employee_id' => $this->getEmployeeId(),
			'designation_id_old' => $this->getDesignationIdOld(),
			'designation_id_new' => $this->getDesignationIdNew(),
			'ps_asset_type_id' => $this->getPsAssetTypeId(),
			'current_salary_type_id' => $this->getCurrentSalaryTypeId(),
			'proposed_salary_type_id' => $this->getProposedSalaryTypeId(),
			'purchase_request_id' => $this->getPurchaseRequestId(),
			'bonus_type_id' => $this->getBonusTypeId(),
			'team_id' => $this->getTeamId(),
			'bonus_type_description' => $this->getBonusTypeDescription(),
			'base_amount_dollars_encrypted' => $this->getBaseAmountDollarsEncrypted(),
			'new_amount_dollars_encrypted' => $this->getNewAmountDollarsEncrypted(),
			'approved_amount_dollars_encrypted' => $this->getApprovedAmountDollarsEncrypted(),
			'estimated_hours_old' => $this->getEstimatedHoursOld(),
			'estimated_hours_new' => $this->getEstimatedHoursNew(),
			'hourly_rate_old_encrypted' => $this->getHourlyRateOldEncrypted(),
			'hourly_rate_new_encrypted' => $this->getHourlyRateNewEncrypted(),
			'hourly_rate_approved_encrypted' => $this->getHourlyRateApprovedEncrypted(),
			'annual_bonus_type_id' => $this->getAnnualBonusTypeId(),
			'annual_bonus_potential_encrypted' => $this->getAnnualBonusPotentialEncrypted(),
			'reason' => $this->getReason(),
			'notes' => $this->getNotes(),
			'team_lead_comments' => $this->getTeamLeadComments(),
			'project_manager_comments' => $this->getProjectManagerComments(),
			'bu_hr_comments' => $this->getBuHrComments(),
			'final_comments' => $this->getFinalComments(),
			'reopened_reason' => $this->getReopenedReason(),
			'effective_date' => $this->getEffectiveDate(),
			'request_datetime' => $this->getRequestDatetime(),
			'dollar_to_rupee_exchange_rate' => $this->getDollarToRupeeExchangeRate(),
			'is_reencrypted' => $this->getIsReencrypted(),
			'bonus_requirement' => $this->getBonusRequirement(),
			'requested_by' => $this->getRequestedBy(),
			'approved_on' => $this->getApprovedOn(),
			'denied_on' => $this->getDeniedOn(),
			'reopened_by' => $this->getReopenedBy(),
			'reopened_on' => $this->getReopenedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'destination_country' => $this->getDestinationCountry(),
			'details' => $this->getDetails(),
			'is_internal_hire' => $this->getIsInternalHire(),
			'technology_id' => $this->getTechnologyId()
		);
	}

}
?>