<?php

class CBaseStatsSalesCloserEmployee extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_sales_closer_employees';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intDepartmentId;
	protected $m_strMonth;
	protected $m_intCommittedAcv;
	protected $m_intQuarterlyAcvQuota;
	protected $m_intAnnualAcvQuota;
	protected $m_intNewClients;
	protected $m_intLostClients;
	protected $m_intNetClientChange;
	protected $m_intCumulativeClients;
	protected $m_intSubscriptionRevenue;
	protected $m_intTransactionRevenue;
	protected $m_intOneTimeRevenue;
	protected $m_intNewLogoUnits;
	protected $m_intNewProductUnits;
	protected $m_intNewPropertyUnits;
	protected $m_intNewTransferPropertyUnits;
	protected $m_intLostUnitsUnhappy;
	protected $m_intLostUnitsManagement;
	protected $m_intLostUnitsUnimplemented;
	protected $m_intLostUnitsTransferProperty;
	protected $m_intLost12MonthRevenue;
	protected $m_intNetUnitChange;
	protected $m_intCumulativeUnits;
	protected $m_intNewLogoSubscriptionAcv;
	protected $m_intNewLogoTransactionAcv;
	protected $m_intNewLogoImplementation;
	protected $m_intNewProductSubscriptionAcv;
	protected $m_intNewProductTransactionAcv;
	protected $m_intNewProductImplementation;
	protected $m_intNewPropertySubscriptionAcv;
	protected $m_intNewPropertyTransactionAcv;
	protected $m_intNewPropertyImplementation;
	protected $m_intNewRenewalSubscriptionAcv;
	protected $m_intNewRenewalTransactionAcv;
	protected $m_intNewRenewalImplementation;
	protected $m_intNewTransferPropertySubscriptionAcv;
	protected $m_intNewTransferPropertyTransactionAcv;
	protected $m_intNewTransferPropertyImplementation;
	protected $m_intSalesAcv;
	protected $m_intBookedAcv;
	protected $m_intTotalNewAcv;
	protected $m_intExpandedAcv;
	protected $m_intLostAcvUnhappy;
	protected $m_intLostAcvManagement;
	protected $m_intLostAcvUnimplemented;
	protected $m_intLostAcvTransferProperty;
	protected $m_intTotalLostAcv;
	protected $m_intNetAcvChange;
	protected $m_intCumulativeAcv;
	protected $m_intMcvPerUnit;
	protected $m_intRevenuePerUnit;
	protected $m_intNotesSubmitted;
	protected $m_intDemosScheduled;
	protected $m_intCallsMade;
	protected $m_intEmailsSent;
	protected $m_intFollowUpsScheduled;
	protected $m_intTotalActions;
	protected $m_intCallsConnected;
	protected $m_intDemosForgotten;
	protected $m_intDemosCompleted;
	protected $m_intEmailsReturned;
	protected $m_intRemote;
	protected $m_intOffice;
	protected $m_intMeal;
	protected $m_intEvent;
	protected $m_intFollowUpsCompleted;
	protected $m_intPeepEmailsSent;
	protected $m_intPeepEmailsReturned;
	protected $m_intPeepEmailsClicked;
	protected $m_intTotalLeads;
	protected $m_intTotalUnits;
	protected $m_intProposalsSent;
	protected $m_intPipelineAcv;
	protected $m_intLeadsAdded;
	protected $m_intLeadsUpdated;
	protected $m_intPeopleAdded;
	protected $m_intPeopleUpdated;
	protected $m_intVendorsAdded;
	protected $m_intVendorsUpdated;
	protected $m_intOpportunitiesAdded;
	protected $m_intOpportunitiesUpdated;
	protected $m_intCommissionsScheduled;
	protected $m_intProfileCompletionPercent;
	protected $m_intCommissionsPaid;
	protected $m_intPilotNewSubscriptionAcv;
	protected $m_intPilotNewTransactionAcv;
	protected $m_intPilotPendingSubscriptionAcv;
	protected $m_intPilotPendingTransactionAcv;
	protected $m_intPilotWonSubscriptionAcv;
	protected $m_intPilotWonTransactionAcv;
	protected $m_intPilotLostSubscriptionAcv;
	protected $m_intPilotLostTransactionAcv;
	protected $m_intPilotOpenCount;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCommittedAcv = '0';
		$this->m_intQuarterlyAcvQuota = '0';
		$this->m_intAnnualAcvQuota = '0';
		$this->m_intNewClients = '0';
		$this->m_intLostClients = '0';
		$this->m_intNetClientChange = '0';
		$this->m_intCumulativeClients = '0';
		$this->m_intSubscriptionRevenue = '0';
		$this->m_intTransactionRevenue = '0';
		$this->m_intOneTimeRevenue = '0';
		$this->m_intNewLogoUnits = '0';
		$this->m_intNewProductUnits = '0';
		$this->m_intNewPropertyUnits = '0';
		$this->m_intNewTransferPropertyUnits = '0';
		$this->m_intLostUnitsUnhappy = '0';
		$this->m_intLostUnitsManagement = '0';
		$this->m_intLostUnitsUnimplemented = '0';
		$this->m_intLostUnitsTransferProperty = '0';
		$this->m_intLost12MonthRevenue = '0';
		$this->m_intNetUnitChange = '0';
		$this->m_intCumulativeUnits = '0';
		$this->m_intNewLogoSubscriptionAcv = '0';
		$this->m_intNewLogoTransactionAcv = '0';
		$this->m_intNewLogoImplementation = '0';
		$this->m_intNewProductSubscriptionAcv = '0';
		$this->m_intNewProductTransactionAcv = '0';
		$this->m_intNewProductImplementation = '0';
		$this->m_intNewPropertySubscriptionAcv = '0';
		$this->m_intNewPropertyTransactionAcv = '0';
		$this->m_intNewPropertyImplementation = '0';
		$this->m_intNewRenewalSubscriptionAcv = '0';
		$this->m_intNewRenewalTransactionAcv = '0';
		$this->m_intNewRenewalImplementation = '0';
		$this->m_intNewTransferPropertySubscriptionAcv = '0';
		$this->m_intNewTransferPropertyTransactionAcv = '0';
		$this->m_intNewTransferPropertyImplementation = '0';
		$this->m_intSalesAcv = '0';
		$this->m_intBookedAcv = '0';
		$this->m_intTotalNewAcv = '0';
		$this->m_intExpandedAcv = '0';
		$this->m_intLostAcvUnhappy = '0';
		$this->m_intLostAcvManagement = '0';
		$this->m_intLostAcvUnimplemented = '0';
		$this->m_intLostAcvTransferProperty = '0';
		$this->m_intTotalLostAcv = '0';
		$this->m_intNetAcvChange = '0';
		$this->m_intCumulativeAcv = '0';
		$this->m_intMcvPerUnit = '0';
		$this->m_intRevenuePerUnit = '0';
		$this->m_intNotesSubmitted = '0';
		$this->m_intDemosScheduled = '0';
		$this->m_intCallsMade = '0';
		$this->m_intEmailsSent = '0';
		$this->m_intFollowUpsScheduled = '0';
		$this->m_intTotalActions = '0';
		$this->m_intCallsConnected = '0';
		$this->m_intDemosForgotten = '0';
		$this->m_intDemosCompleted = '0';
		$this->m_intEmailsReturned = '0';
		$this->m_intRemote = '0';
		$this->m_intOffice = '0';
		$this->m_intMeal = '0';
		$this->m_intEvent = '0';
		$this->m_intFollowUpsCompleted = '0';
		$this->m_intPeepEmailsSent = '0';
		$this->m_intPeepEmailsReturned = '0';
		$this->m_intPeepEmailsClicked = '0';
		$this->m_intTotalLeads = '0';
		$this->m_intTotalUnits = '0';
		$this->m_intProposalsSent = '0';
		$this->m_intPipelineAcv = '0';
		$this->m_intLeadsAdded = '0';
		$this->m_intLeadsUpdated = '0';
		$this->m_intPeopleAdded = '0';
		$this->m_intPeopleUpdated = '0';
		$this->m_intVendorsAdded = '0';
		$this->m_intVendorsUpdated = '0';
		$this->m_intOpportunitiesAdded = '0';
		$this->m_intOpportunitiesUpdated = '0';
		$this->m_intCommissionsScheduled = '0';
		$this->m_intProfileCompletionPercent = '0';
		$this->m_intCommissionsPaid = '0';
		$this->m_intPilotNewSubscriptionAcv = '0';
		$this->m_intPilotNewTransactionAcv = '0';
		$this->m_intPilotPendingSubscriptionAcv = '0';
		$this->m_intPilotPendingTransactionAcv = '0';
		$this->m_intPilotWonSubscriptionAcv = '0';
		$this->m_intPilotWonTransactionAcv = '0';
		$this->m_intPilotLostSubscriptionAcv = '0';
		$this->m_intPilotLostTransactionAcv = '0';
		$this->m_intPilotOpenCount = '0';
		$this->m_intCreatedBy = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['committed_acv'] ) && $boolDirectSet ) $this->set( 'm_intCommittedAcv', trim( $arrValues['committed_acv'] ) ); elseif( isset( $arrValues['committed_acv'] ) ) $this->setCommittedAcv( $arrValues['committed_acv'] );
		if( isset( $arrValues['quarterly_acv_quota'] ) && $boolDirectSet ) $this->set( 'm_intQuarterlyAcvQuota', trim( $arrValues['quarterly_acv_quota'] ) ); elseif( isset( $arrValues['quarterly_acv_quota'] ) ) $this->setQuarterlyAcvQuota( $arrValues['quarterly_acv_quota'] );
		if( isset( $arrValues['annual_acv_quota'] ) && $boolDirectSet ) $this->set( 'm_intAnnualAcvQuota', trim( $arrValues['annual_acv_quota'] ) ); elseif( isset( $arrValues['annual_acv_quota'] ) ) $this->setAnnualAcvQuota( $arrValues['annual_acv_quota'] );
		if( isset( $arrValues['new_clients'] ) && $boolDirectSet ) $this->set( 'm_intNewClients', trim( $arrValues['new_clients'] ) ); elseif( isset( $arrValues['new_clients'] ) ) $this->setNewClients( $arrValues['new_clients'] );
		if( isset( $arrValues['lost_clients'] ) && $boolDirectSet ) $this->set( 'm_intLostClients', trim( $arrValues['lost_clients'] ) ); elseif( isset( $arrValues['lost_clients'] ) ) $this->setLostClients( $arrValues['lost_clients'] );
		if( isset( $arrValues['net_client_change'] ) && $boolDirectSet ) $this->set( 'm_intNetClientChange', trim( $arrValues['net_client_change'] ) ); elseif( isset( $arrValues['net_client_change'] ) ) $this->setNetClientChange( $arrValues['net_client_change'] );
		if( isset( $arrValues['cumulative_clients'] ) && $boolDirectSet ) $this->set( 'm_intCumulativeClients', trim( $arrValues['cumulative_clients'] ) ); elseif( isset( $arrValues['cumulative_clients'] ) ) $this->setCumulativeClients( $arrValues['cumulative_clients'] );
		if( isset( $arrValues['subscription_revenue'] ) && $boolDirectSet ) $this->set( 'm_intSubscriptionRevenue', trim( $arrValues['subscription_revenue'] ) ); elseif( isset( $arrValues['subscription_revenue'] ) ) $this->setSubscriptionRevenue( $arrValues['subscription_revenue'] );
		if( isset( $arrValues['transaction_revenue'] ) && $boolDirectSet ) $this->set( 'm_intTransactionRevenue', trim( $arrValues['transaction_revenue'] ) ); elseif( isset( $arrValues['transaction_revenue'] ) ) $this->setTransactionRevenue( $arrValues['transaction_revenue'] );
		if( isset( $arrValues['one_time_revenue'] ) && $boolDirectSet ) $this->set( 'm_intOneTimeRevenue', trim( $arrValues['one_time_revenue'] ) ); elseif( isset( $arrValues['one_time_revenue'] ) ) $this->setOneTimeRevenue( $arrValues['one_time_revenue'] );
		if( isset( $arrValues['new_logo_units'] ) && $boolDirectSet ) $this->set( 'm_intNewLogoUnits', trim( $arrValues['new_logo_units'] ) ); elseif( isset( $arrValues['new_logo_units'] ) ) $this->setNewLogoUnits( $arrValues['new_logo_units'] );
		if( isset( $arrValues['new_product_units'] ) && $boolDirectSet ) $this->set( 'm_intNewProductUnits', trim( $arrValues['new_product_units'] ) ); elseif( isset( $arrValues['new_product_units'] ) ) $this->setNewProductUnits( $arrValues['new_product_units'] );
		if( isset( $arrValues['new_property_units'] ) && $boolDirectSet ) $this->set( 'm_intNewPropertyUnits', trim( $arrValues['new_property_units'] ) ); elseif( isset( $arrValues['new_property_units'] ) ) $this->setNewPropertyUnits( $arrValues['new_property_units'] );
		if( isset( $arrValues['new_transfer_property_units'] ) && $boolDirectSet ) $this->set( 'm_intNewTransferPropertyUnits', trim( $arrValues['new_transfer_property_units'] ) ); elseif( isset( $arrValues['new_transfer_property_units'] ) ) $this->setNewTransferPropertyUnits( $arrValues['new_transfer_property_units'] );
		if( isset( $arrValues['lost_units_unhappy'] ) && $boolDirectSet ) $this->set( 'm_intLostUnitsUnhappy', trim( $arrValues['lost_units_unhappy'] ) ); elseif( isset( $arrValues['lost_units_unhappy'] ) ) $this->setLostUnitsUnhappy( $arrValues['lost_units_unhappy'] );
		if( isset( $arrValues['lost_units_management'] ) && $boolDirectSet ) $this->set( 'm_intLostUnitsManagement', trim( $arrValues['lost_units_management'] ) ); elseif( isset( $arrValues['lost_units_management'] ) ) $this->setLostUnitsManagement( $arrValues['lost_units_management'] );
		if( isset( $arrValues['lost_units_unimplemented'] ) && $boolDirectSet ) $this->set( 'm_intLostUnitsUnimplemented', trim( $arrValues['lost_units_unimplemented'] ) ); elseif( isset( $arrValues['lost_units_unimplemented'] ) ) $this->setLostUnitsUnimplemented( $arrValues['lost_units_unimplemented'] );
		if( isset( $arrValues['lost_units_transfer_property'] ) && $boolDirectSet ) $this->set( 'm_intLostUnitsTransferProperty', trim( $arrValues['lost_units_transfer_property'] ) ); elseif( isset( $arrValues['lost_units_transfer_property'] ) ) $this->setLostUnitsTransferProperty( $arrValues['lost_units_transfer_property'] );
		if( isset( $arrValues['lost_12_month_revenue'] ) && $boolDirectSet ) $this->set( 'm_intLost12MonthRevenue', trim( $arrValues['lost_12_month_revenue'] ) ); elseif( isset( $arrValues['lost_12_month_revenue'] ) ) $this->setLost12MonthRevenue( $arrValues['lost_12_month_revenue'] );
		if( isset( $arrValues['net_unit_change'] ) && $boolDirectSet ) $this->set( 'm_intNetUnitChange', trim( $arrValues['net_unit_change'] ) ); elseif( isset( $arrValues['net_unit_change'] ) ) $this->setNetUnitChange( $arrValues['net_unit_change'] );
		if( isset( $arrValues['cumulative_units'] ) && $boolDirectSet ) $this->set( 'm_intCumulativeUnits', trim( $arrValues['cumulative_units'] ) ); elseif( isset( $arrValues['cumulative_units'] ) ) $this->setCumulativeUnits( $arrValues['cumulative_units'] );
		if( isset( $arrValues['new_logo_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewLogoSubscriptionAcv', trim( $arrValues['new_logo_subscription_acv'] ) ); elseif( isset( $arrValues['new_logo_subscription_acv'] ) ) $this->setNewLogoSubscriptionAcv( $arrValues['new_logo_subscription_acv'] );
		if( isset( $arrValues['new_logo_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewLogoTransactionAcv', trim( $arrValues['new_logo_transaction_acv'] ) ); elseif( isset( $arrValues['new_logo_transaction_acv'] ) ) $this->setNewLogoTransactionAcv( $arrValues['new_logo_transaction_acv'] );
		if( isset( $arrValues['new_logo_implementation'] ) && $boolDirectSet ) $this->set( 'm_intNewLogoImplementation', trim( $arrValues['new_logo_implementation'] ) ); elseif( isset( $arrValues['new_logo_implementation'] ) ) $this->setNewLogoImplementation( $arrValues['new_logo_implementation'] );
		if( isset( $arrValues['new_product_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewProductSubscriptionAcv', trim( $arrValues['new_product_subscription_acv'] ) ); elseif( isset( $arrValues['new_product_subscription_acv'] ) ) $this->setNewProductSubscriptionAcv( $arrValues['new_product_subscription_acv'] );
		if( isset( $arrValues['new_product_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewProductTransactionAcv', trim( $arrValues['new_product_transaction_acv'] ) ); elseif( isset( $arrValues['new_product_transaction_acv'] ) ) $this->setNewProductTransactionAcv( $arrValues['new_product_transaction_acv'] );
		if( isset( $arrValues['new_product_implementation'] ) && $boolDirectSet ) $this->set( 'm_intNewProductImplementation', trim( $arrValues['new_product_implementation'] ) ); elseif( isset( $arrValues['new_product_implementation'] ) ) $this->setNewProductImplementation( $arrValues['new_product_implementation'] );
		if( isset( $arrValues['new_property_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewPropertySubscriptionAcv', trim( $arrValues['new_property_subscription_acv'] ) ); elseif( isset( $arrValues['new_property_subscription_acv'] ) ) $this->setNewPropertySubscriptionAcv( $arrValues['new_property_subscription_acv'] );
		if( isset( $arrValues['new_property_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewPropertyTransactionAcv', trim( $arrValues['new_property_transaction_acv'] ) ); elseif( isset( $arrValues['new_property_transaction_acv'] ) ) $this->setNewPropertyTransactionAcv( $arrValues['new_property_transaction_acv'] );
		if( isset( $arrValues['new_property_implementation'] ) && $boolDirectSet ) $this->set( 'm_intNewPropertyImplementation', trim( $arrValues['new_property_implementation'] ) ); elseif( isset( $arrValues['new_property_implementation'] ) ) $this->setNewPropertyImplementation( $arrValues['new_property_implementation'] );
		if( isset( $arrValues['new_renewal_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewRenewalSubscriptionAcv', trim( $arrValues['new_renewal_subscription_acv'] ) ); elseif( isset( $arrValues['new_renewal_subscription_acv'] ) ) $this->setNewRenewalSubscriptionAcv( $arrValues['new_renewal_subscription_acv'] );
		if( isset( $arrValues['new_renewal_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewRenewalTransactionAcv', trim( $arrValues['new_renewal_transaction_acv'] ) ); elseif( isset( $arrValues['new_renewal_transaction_acv'] ) ) $this->setNewRenewalTransactionAcv( $arrValues['new_renewal_transaction_acv'] );
		if( isset( $arrValues['new_renewal_implementation'] ) && $boolDirectSet ) $this->set( 'm_intNewRenewalImplementation', trim( $arrValues['new_renewal_implementation'] ) ); elseif( isset( $arrValues['new_renewal_implementation'] ) ) $this->setNewRenewalImplementation( $arrValues['new_renewal_implementation'] );
		if( isset( $arrValues['new_transfer_property_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewTransferPropertySubscriptionAcv', trim( $arrValues['new_transfer_property_subscription_acv'] ) ); elseif( isset( $arrValues['new_transfer_property_subscription_acv'] ) ) $this->setNewTransferPropertySubscriptionAcv( $arrValues['new_transfer_property_subscription_acv'] );
		if( isset( $arrValues['new_transfer_property_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intNewTransferPropertyTransactionAcv', trim( $arrValues['new_transfer_property_transaction_acv'] ) ); elseif( isset( $arrValues['new_transfer_property_transaction_acv'] ) ) $this->setNewTransferPropertyTransactionAcv( $arrValues['new_transfer_property_transaction_acv'] );
		if( isset( $arrValues['new_transfer_property_implementation'] ) && $boolDirectSet ) $this->set( 'm_intNewTransferPropertyImplementation', trim( $arrValues['new_transfer_property_implementation'] ) ); elseif( isset( $arrValues['new_transfer_property_implementation'] ) ) $this->setNewTransferPropertyImplementation( $arrValues['new_transfer_property_implementation'] );
		if( isset( $arrValues['sales_acv'] ) && $boolDirectSet ) $this->set( 'm_intSalesAcv', trim( $arrValues['sales_acv'] ) ); elseif( isset( $arrValues['sales_acv'] ) ) $this->setSalesAcv( $arrValues['sales_acv'] );
		if( isset( $arrValues['booked_acv'] ) && $boolDirectSet ) $this->set( 'm_intBookedAcv', trim( $arrValues['booked_acv'] ) ); elseif( isset( $arrValues['booked_acv'] ) ) $this->setBookedAcv( $arrValues['booked_acv'] );
		if( isset( $arrValues['total_new_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalNewAcv', trim( $arrValues['total_new_acv'] ) ); elseif( isset( $arrValues['total_new_acv'] ) ) $this->setTotalNewAcv( $arrValues['total_new_acv'] );
		if( isset( $arrValues['expanded_acv'] ) && $boolDirectSet ) $this->set( 'm_intExpandedAcv', trim( $arrValues['expanded_acv'] ) ); elseif( isset( $arrValues['expanded_acv'] ) ) $this->setExpandedAcv( $arrValues['expanded_acv'] );
		if( isset( $arrValues['lost_acv_unhappy'] ) && $boolDirectSet ) $this->set( 'm_intLostAcvUnhappy', trim( $arrValues['lost_acv_unhappy'] ) ); elseif( isset( $arrValues['lost_acv_unhappy'] ) ) $this->setLostAcvUnhappy( $arrValues['lost_acv_unhappy'] );
		if( isset( $arrValues['lost_acv_management'] ) && $boolDirectSet ) $this->set( 'm_intLostAcvManagement', trim( $arrValues['lost_acv_management'] ) ); elseif( isset( $arrValues['lost_acv_management'] ) ) $this->setLostAcvManagement( $arrValues['lost_acv_management'] );
		if( isset( $arrValues['lost_acv_unimplemented'] ) && $boolDirectSet ) $this->set( 'm_intLostAcvUnimplemented', trim( $arrValues['lost_acv_unimplemented'] ) ); elseif( isset( $arrValues['lost_acv_unimplemented'] ) ) $this->setLostAcvUnimplemented( $arrValues['lost_acv_unimplemented'] );
		if( isset( $arrValues['lost_acv_transfer_property'] ) && $boolDirectSet ) $this->set( 'm_intLostAcvTransferProperty', trim( $arrValues['lost_acv_transfer_property'] ) ); elseif( isset( $arrValues['lost_acv_transfer_property'] ) ) $this->setLostAcvTransferProperty( $arrValues['lost_acv_transfer_property'] );
		if( isset( $arrValues['total_lost_acv'] ) && $boolDirectSet ) $this->set( 'm_intTotalLostAcv', trim( $arrValues['total_lost_acv'] ) ); elseif( isset( $arrValues['total_lost_acv'] ) ) $this->setTotalLostAcv( $arrValues['total_lost_acv'] );
		if( isset( $arrValues['net_acv_change'] ) && $boolDirectSet ) $this->set( 'm_intNetAcvChange', trim( $arrValues['net_acv_change'] ) ); elseif( isset( $arrValues['net_acv_change'] ) ) $this->setNetAcvChange( $arrValues['net_acv_change'] );
		if( isset( $arrValues['cumulative_acv'] ) && $boolDirectSet ) $this->set( 'm_intCumulativeAcv', trim( $arrValues['cumulative_acv'] ) ); elseif( isset( $arrValues['cumulative_acv'] ) ) $this->setCumulativeAcv( $arrValues['cumulative_acv'] );
		if( isset( $arrValues['mcv_per_unit'] ) && $boolDirectSet ) $this->set( 'm_intMcvPerUnit', trim( $arrValues['mcv_per_unit'] ) ); elseif( isset( $arrValues['mcv_per_unit'] ) ) $this->setMcvPerUnit( $arrValues['mcv_per_unit'] );
		if( isset( $arrValues['revenue_per_unit'] ) && $boolDirectSet ) $this->set( 'm_intRevenuePerUnit', trim( $arrValues['revenue_per_unit'] ) ); elseif( isset( $arrValues['revenue_per_unit'] ) ) $this->setRevenuePerUnit( $arrValues['revenue_per_unit'] );
		if( isset( $arrValues['notes_submitted'] ) && $boolDirectSet ) $this->set( 'm_intNotesSubmitted', trim( $arrValues['notes_submitted'] ) ); elseif( isset( $arrValues['notes_submitted'] ) ) $this->setNotesSubmitted( $arrValues['notes_submitted'] );
		if( isset( $arrValues['demos_scheduled'] ) && $boolDirectSet ) $this->set( 'm_intDemosScheduled', trim( $arrValues['demos_scheduled'] ) ); elseif( isset( $arrValues['demos_scheduled'] ) ) $this->setDemosScheduled( $arrValues['demos_scheduled'] );
		if( isset( $arrValues['calls_made'] ) && $boolDirectSet ) $this->set( 'm_intCallsMade', trim( $arrValues['calls_made'] ) ); elseif( isset( $arrValues['calls_made'] ) ) $this->setCallsMade( $arrValues['calls_made'] );
		if( isset( $arrValues['emails_sent'] ) && $boolDirectSet ) $this->set( 'm_intEmailsSent', trim( $arrValues['emails_sent'] ) ); elseif( isset( $arrValues['emails_sent'] ) ) $this->setEmailsSent( $arrValues['emails_sent'] );
		if( isset( $arrValues['follow_ups_scheduled'] ) && $boolDirectSet ) $this->set( 'm_intFollowUpsScheduled', trim( $arrValues['follow_ups_scheduled'] ) ); elseif( isset( $arrValues['follow_ups_scheduled'] ) ) $this->setFollowUpsScheduled( $arrValues['follow_ups_scheduled'] );
		if( isset( $arrValues['total_actions'] ) && $boolDirectSet ) $this->set( 'm_intTotalActions', trim( $arrValues['total_actions'] ) ); elseif( isset( $arrValues['total_actions'] ) ) $this->setTotalActions( $arrValues['total_actions'] );
		if( isset( $arrValues['calls_connected'] ) && $boolDirectSet ) $this->set( 'm_intCallsConnected', trim( $arrValues['calls_connected'] ) ); elseif( isset( $arrValues['calls_connected'] ) ) $this->setCallsConnected( $arrValues['calls_connected'] );
		if( isset( $arrValues['demos_forgotten'] ) && $boolDirectSet ) $this->set( 'm_intDemosForgotten', trim( $arrValues['demos_forgotten'] ) ); elseif( isset( $arrValues['demos_forgotten'] ) ) $this->setDemosForgotten( $arrValues['demos_forgotten'] );
		if( isset( $arrValues['demos_completed'] ) && $boolDirectSet ) $this->set( 'm_intDemosCompleted', trim( $arrValues['demos_completed'] ) ); elseif( isset( $arrValues['demos_completed'] ) ) $this->setDemosCompleted( $arrValues['demos_completed'] );
		if( isset( $arrValues['emails_returned'] ) && $boolDirectSet ) $this->set( 'm_intEmailsReturned', trim( $arrValues['emails_returned'] ) ); elseif( isset( $arrValues['emails_returned'] ) ) $this->setEmailsReturned( $arrValues['emails_returned'] );
		if( isset( $arrValues['remote'] ) && $boolDirectSet ) $this->set( 'm_intRemote', trim( $arrValues['remote'] ) ); elseif( isset( $arrValues['remote'] ) ) $this->setRemote( $arrValues['remote'] );
		if( isset( $arrValues['office'] ) && $boolDirectSet ) $this->set( 'm_intOffice', trim( $arrValues['office'] ) ); elseif( isset( $arrValues['office'] ) ) $this->setOffice( $arrValues['office'] );
		if( isset( $arrValues['meal'] ) && $boolDirectSet ) $this->set( 'm_intMeal', trim( $arrValues['meal'] ) ); elseif( isset( $arrValues['meal'] ) ) $this->setMeal( $arrValues['meal'] );
		if( isset( $arrValues['event'] ) && $boolDirectSet ) $this->set( 'm_intEvent', trim( $arrValues['event'] ) ); elseif( isset( $arrValues['event'] ) ) $this->setEvent( $arrValues['event'] );
		if( isset( $arrValues['follow_ups_completed'] ) && $boolDirectSet ) $this->set( 'm_intFollowUpsCompleted', trim( $arrValues['follow_ups_completed'] ) ); elseif( isset( $arrValues['follow_ups_completed'] ) ) $this->setFollowUpsCompleted( $arrValues['follow_ups_completed'] );
		if( isset( $arrValues['peep_emails_sent'] ) && $boolDirectSet ) $this->set( 'm_intPeepEmailsSent', trim( $arrValues['peep_emails_sent'] ) ); elseif( isset( $arrValues['peep_emails_sent'] ) ) $this->setPeepEmailsSent( $arrValues['peep_emails_sent'] );
		if( isset( $arrValues['peep_emails_returned'] ) && $boolDirectSet ) $this->set( 'm_intPeepEmailsReturned', trim( $arrValues['peep_emails_returned'] ) ); elseif( isset( $arrValues['peep_emails_returned'] ) ) $this->setPeepEmailsReturned( $arrValues['peep_emails_returned'] );
		if( isset( $arrValues['peep_emails_clicked'] ) && $boolDirectSet ) $this->set( 'm_intPeepEmailsClicked', trim( $arrValues['peep_emails_clicked'] ) ); elseif( isset( $arrValues['peep_emails_clicked'] ) ) $this->setPeepEmailsClicked( $arrValues['peep_emails_clicked'] );
		if( isset( $arrValues['total_leads'] ) && $boolDirectSet ) $this->set( 'm_intTotalLeads', trim( $arrValues['total_leads'] ) ); elseif( isset( $arrValues['total_leads'] ) ) $this->setTotalLeads( $arrValues['total_leads'] );
		if( isset( $arrValues['total_units'] ) && $boolDirectSet ) $this->set( 'm_intTotalUnits', trim( $arrValues['total_units'] ) ); elseif( isset( $arrValues['total_units'] ) ) $this->setTotalUnits( $arrValues['total_units'] );
		if( isset( $arrValues['proposals_sent'] ) && $boolDirectSet ) $this->set( 'm_intProposalsSent', trim( $arrValues['proposals_sent'] ) ); elseif( isset( $arrValues['proposals_sent'] ) ) $this->setProposalsSent( $arrValues['proposals_sent'] );
		if( isset( $arrValues['pipeline_acv'] ) && $boolDirectSet ) $this->set( 'm_intPipelineAcv', trim( $arrValues['pipeline_acv'] ) ); elseif( isset( $arrValues['pipeline_acv'] ) ) $this->setPipelineAcv( $arrValues['pipeline_acv'] );
		if( isset( $arrValues['leads_added'] ) && $boolDirectSet ) $this->set( 'm_intLeadsAdded', trim( $arrValues['leads_added'] ) ); elseif( isset( $arrValues['leads_added'] ) ) $this->setLeadsAdded( $arrValues['leads_added'] );
		if( isset( $arrValues['leads_updated'] ) && $boolDirectSet ) $this->set( 'm_intLeadsUpdated', trim( $arrValues['leads_updated'] ) ); elseif( isset( $arrValues['leads_updated'] ) ) $this->setLeadsUpdated( $arrValues['leads_updated'] );
		if( isset( $arrValues['people_added'] ) && $boolDirectSet ) $this->set( 'm_intPeopleAdded', trim( $arrValues['people_added'] ) ); elseif( isset( $arrValues['people_added'] ) ) $this->setPeopleAdded( $arrValues['people_added'] );
		if( isset( $arrValues['people_updated'] ) && $boolDirectSet ) $this->set( 'm_intPeopleUpdated', trim( $arrValues['people_updated'] ) ); elseif( isset( $arrValues['people_updated'] ) ) $this->setPeopleUpdated( $arrValues['people_updated'] );
		if( isset( $arrValues['vendors_added'] ) && $boolDirectSet ) $this->set( 'm_intVendorsAdded', trim( $arrValues['vendors_added'] ) ); elseif( isset( $arrValues['vendors_added'] ) ) $this->setVendorsAdded( $arrValues['vendors_added'] );
		if( isset( $arrValues['vendors_updated'] ) && $boolDirectSet ) $this->set( 'm_intVendorsUpdated', trim( $arrValues['vendors_updated'] ) ); elseif( isset( $arrValues['vendors_updated'] ) ) $this->setVendorsUpdated( $arrValues['vendors_updated'] );
		if( isset( $arrValues['opportunities_added'] ) && $boolDirectSet ) $this->set( 'm_intOpportunitiesAdded', trim( $arrValues['opportunities_added'] ) ); elseif( isset( $arrValues['opportunities_added'] ) ) $this->setOpportunitiesAdded( $arrValues['opportunities_added'] );
		if( isset( $arrValues['opportunities_updated'] ) && $boolDirectSet ) $this->set( 'm_intOpportunitiesUpdated', trim( $arrValues['opportunities_updated'] ) ); elseif( isset( $arrValues['opportunities_updated'] ) ) $this->setOpportunitiesUpdated( $arrValues['opportunities_updated'] );
		if( isset( $arrValues['commissions_scheduled'] ) && $boolDirectSet ) $this->set( 'm_intCommissionsScheduled', trim( $arrValues['commissions_scheduled'] ) ); elseif( isset( $arrValues['commissions_scheduled'] ) ) $this->setCommissionsScheduled( $arrValues['commissions_scheduled'] );
		if( isset( $arrValues['profile_completion_percent'] ) && $boolDirectSet ) $this->set( 'm_intProfileCompletionPercent', trim( $arrValues['profile_completion_percent'] ) ); elseif( isset( $arrValues['profile_completion_percent'] ) ) $this->setProfileCompletionPercent( $arrValues['profile_completion_percent'] );
		if( isset( $arrValues['commissions_paid'] ) && $boolDirectSet ) $this->set( 'm_intCommissionsPaid', trim( $arrValues['commissions_paid'] ) ); elseif( isset( $arrValues['commissions_paid'] ) ) $this->setCommissionsPaid( $arrValues['commissions_paid'] );
		if( isset( $arrValues['pilot_new_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotNewSubscriptionAcv', trim( $arrValues['pilot_new_subscription_acv'] ) ); elseif( isset( $arrValues['pilot_new_subscription_acv'] ) ) $this->setPilotNewSubscriptionAcv( $arrValues['pilot_new_subscription_acv'] );
		if( isset( $arrValues['pilot_new_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotNewTransactionAcv', trim( $arrValues['pilot_new_transaction_acv'] ) ); elseif( isset( $arrValues['pilot_new_transaction_acv'] ) ) $this->setPilotNewTransactionAcv( $arrValues['pilot_new_transaction_acv'] );
		if( isset( $arrValues['pilot_pending_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotPendingSubscriptionAcv', trim( $arrValues['pilot_pending_subscription_acv'] ) ); elseif( isset( $arrValues['pilot_pending_subscription_acv'] ) ) $this->setPilotPendingSubscriptionAcv( $arrValues['pilot_pending_subscription_acv'] );
		if( isset( $arrValues['pilot_pending_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotPendingTransactionAcv', trim( $arrValues['pilot_pending_transaction_acv'] ) ); elseif( isset( $arrValues['pilot_pending_transaction_acv'] ) ) $this->setPilotPendingTransactionAcv( $arrValues['pilot_pending_transaction_acv'] );
		if( isset( $arrValues['pilot_won_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotWonSubscriptionAcv', trim( $arrValues['pilot_won_subscription_acv'] ) ); elseif( isset( $arrValues['pilot_won_subscription_acv'] ) ) $this->setPilotWonSubscriptionAcv( $arrValues['pilot_won_subscription_acv'] );
		if( isset( $arrValues['pilot_won_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotWonTransactionAcv', trim( $arrValues['pilot_won_transaction_acv'] ) ); elseif( isset( $arrValues['pilot_won_transaction_acv'] ) ) $this->setPilotWonTransactionAcv( $arrValues['pilot_won_transaction_acv'] );
		if( isset( $arrValues['pilot_lost_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotLostSubscriptionAcv', trim( $arrValues['pilot_lost_subscription_acv'] ) ); elseif( isset( $arrValues['pilot_lost_subscription_acv'] ) ) $this->setPilotLostSubscriptionAcv( $arrValues['pilot_lost_subscription_acv'] );
		if( isset( $arrValues['pilot_lost_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intPilotLostTransactionAcv', trim( $arrValues['pilot_lost_transaction_acv'] ) ); elseif( isset( $arrValues['pilot_lost_transaction_acv'] ) ) $this->setPilotLostTransactionAcv( $arrValues['pilot_lost_transaction_acv'] );
		if( isset( $arrValues['pilot_open_count'] ) && $boolDirectSet ) $this->set( 'm_intPilotOpenCount', trim( $arrValues['pilot_open_count'] ) ); elseif( isset( $arrValues['pilot_open_count'] ) ) $this->setPilotOpenCount( $arrValues['pilot_open_count'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NOW()';
	}

	public function setCommittedAcv( $intCommittedAcv ) {
		$this->set( 'm_intCommittedAcv', CStrings::strToIntDef( $intCommittedAcv, NULL, false ) );
	}

	public function getCommittedAcv() {
		return $this->m_intCommittedAcv;
	}

	public function sqlCommittedAcv() {
		return ( true == isset( $this->m_intCommittedAcv ) ) ? ( string ) $this->m_intCommittedAcv : '0';
	}

	public function setQuarterlyAcvQuota( $intQuarterlyAcvQuota ) {
		$this->set( 'm_intQuarterlyAcvQuota', CStrings::strToIntDef( $intQuarterlyAcvQuota, NULL, false ) );
	}

	public function getQuarterlyAcvQuota() {
		return $this->m_intQuarterlyAcvQuota;
	}

	public function sqlQuarterlyAcvQuota() {
		return ( true == isset( $this->m_intQuarterlyAcvQuota ) ) ? ( string ) $this->m_intQuarterlyAcvQuota : '0';
	}

	public function setAnnualAcvQuota( $intAnnualAcvQuota ) {
		$this->set( 'm_intAnnualAcvQuota', CStrings::strToIntDef( $intAnnualAcvQuota, NULL, false ) );
	}

	public function getAnnualAcvQuota() {
		return $this->m_intAnnualAcvQuota;
	}

	public function sqlAnnualAcvQuota() {
		return ( true == isset( $this->m_intAnnualAcvQuota ) ) ? ( string ) $this->m_intAnnualAcvQuota : '0';
	}

	public function setNewClients( $intNewClients ) {
		$this->set( 'm_intNewClients', CStrings::strToIntDef( $intNewClients, NULL, false ) );
	}

	public function getNewClients() {
		return $this->m_intNewClients;
	}

	public function sqlNewClients() {
		return ( true == isset( $this->m_intNewClients ) ) ? ( string ) $this->m_intNewClients : '0';
	}

	public function setLostClients( $intLostClients ) {
		$this->set( 'm_intLostClients', CStrings::strToIntDef( $intLostClients, NULL, false ) );
	}

	public function getLostClients() {
		return $this->m_intLostClients;
	}

	public function sqlLostClients() {
		return ( true == isset( $this->m_intLostClients ) ) ? ( string ) $this->m_intLostClients : '0';
	}

	public function setNetClientChange( $intNetClientChange ) {
		$this->set( 'm_intNetClientChange', CStrings::strToIntDef( $intNetClientChange, NULL, false ) );
	}

	public function getNetClientChange() {
		return $this->m_intNetClientChange;
	}

	public function sqlNetClientChange() {
		return ( true == isset( $this->m_intNetClientChange ) ) ? ( string ) $this->m_intNetClientChange : '0';
	}

	public function setCumulativeClients( $intCumulativeClients ) {
		$this->set( 'm_intCumulativeClients', CStrings::strToIntDef( $intCumulativeClients, NULL, false ) );
	}

	public function getCumulativeClients() {
		return $this->m_intCumulativeClients;
	}

	public function sqlCumulativeClients() {
		return ( true == isset( $this->m_intCumulativeClients ) ) ? ( string ) $this->m_intCumulativeClients : '0';
	}

	public function setSubscriptionRevenue( $intSubscriptionRevenue ) {
		$this->set( 'm_intSubscriptionRevenue', CStrings::strToIntDef( $intSubscriptionRevenue, NULL, false ) );
	}

	public function getSubscriptionRevenue() {
		return $this->m_intSubscriptionRevenue;
	}

	public function sqlSubscriptionRevenue() {
		return ( true == isset( $this->m_intSubscriptionRevenue ) ) ? ( string ) $this->m_intSubscriptionRevenue : '0';
	}

	public function setTransactionRevenue( $intTransactionRevenue ) {
		$this->set( 'm_intTransactionRevenue', CStrings::strToIntDef( $intTransactionRevenue, NULL, false ) );
	}

	public function getTransactionRevenue() {
		return $this->m_intTransactionRevenue;
	}

	public function sqlTransactionRevenue() {
		return ( true == isset( $this->m_intTransactionRevenue ) ) ? ( string ) $this->m_intTransactionRevenue : '0';
	}

	public function setOneTimeRevenue( $intOneTimeRevenue ) {
		$this->set( 'm_intOneTimeRevenue', CStrings::strToIntDef( $intOneTimeRevenue, NULL, false ) );
	}

	public function getOneTimeRevenue() {
		return $this->m_intOneTimeRevenue;
	}

	public function sqlOneTimeRevenue() {
		return ( true == isset( $this->m_intOneTimeRevenue ) ) ? ( string ) $this->m_intOneTimeRevenue : '0';
	}

	public function setNewLogoUnits( $intNewLogoUnits ) {
		$this->set( 'm_intNewLogoUnits', CStrings::strToIntDef( $intNewLogoUnits, NULL, false ) );
	}

	public function getNewLogoUnits() {
		return $this->m_intNewLogoUnits;
	}

	public function sqlNewLogoUnits() {
		return ( true == isset( $this->m_intNewLogoUnits ) ) ? ( string ) $this->m_intNewLogoUnits : '0';
	}

	public function setNewProductUnits( $intNewProductUnits ) {
		$this->set( 'm_intNewProductUnits', CStrings::strToIntDef( $intNewProductUnits, NULL, false ) );
	}

	public function getNewProductUnits() {
		return $this->m_intNewProductUnits;
	}

	public function sqlNewProductUnits() {
		return ( true == isset( $this->m_intNewProductUnits ) ) ? ( string ) $this->m_intNewProductUnits : '0';
	}

	public function setNewPropertyUnits( $intNewPropertyUnits ) {
		$this->set( 'm_intNewPropertyUnits', CStrings::strToIntDef( $intNewPropertyUnits, NULL, false ) );
	}

	public function getNewPropertyUnits() {
		return $this->m_intNewPropertyUnits;
	}

	public function sqlNewPropertyUnits() {
		return ( true == isset( $this->m_intNewPropertyUnits ) ) ? ( string ) $this->m_intNewPropertyUnits : '0';
	}

	public function setNewTransferPropertyUnits( $intNewTransferPropertyUnits ) {
		$this->set( 'm_intNewTransferPropertyUnits', CStrings::strToIntDef( $intNewTransferPropertyUnits, NULL, false ) );
	}

	public function getNewTransferPropertyUnits() {
		return $this->m_intNewTransferPropertyUnits;
	}

	public function sqlNewTransferPropertyUnits() {
		return ( true == isset( $this->m_intNewTransferPropertyUnits ) ) ? ( string ) $this->m_intNewTransferPropertyUnits : '0';
	}

	public function setLostUnitsUnhappy( $intLostUnitsUnhappy ) {
		$this->set( 'm_intLostUnitsUnhappy', CStrings::strToIntDef( $intLostUnitsUnhappy, NULL, false ) );
	}

	public function getLostUnitsUnhappy() {
		return $this->m_intLostUnitsUnhappy;
	}

	public function sqlLostUnitsUnhappy() {
		return ( true == isset( $this->m_intLostUnitsUnhappy ) ) ? ( string ) $this->m_intLostUnitsUnhappy : '0';
	}

	public function setLostUnitsManagement( $intLostUnitsManagement ) {
		$this->set( 'm_intLostUnitsManagement', CStrings::strToIntDef( $intLostUnitsManagement, NULL, false ) );
	}

	public function getLostUnitsManagement() {
		return $this->m_intLostUnitsManagement;
	}

	public function sqlLostUnitsManagement() {
		return ( true == isset( $this->m_intLostUnitsManagement ) ) ? ( string ) $this->m_intLostUnitsManagement : '0';
	}

	public function setLostUnitsUnimplemented( $intLostUnitsUnimplemented ) {
		$this->set( 'm_intLostUnitsUnimplemented', CStrings::strToIntDef( $intLostUnitsUnimplemented, NULL, false ) );
	}

	public function getLostUnitsUnimplemented() {
		return $this->m_intLostUnitsUnimplemented;
	}

	public function sqlLostUnitsUnimplemented() {
		return ( true == isset( $this->m_intLostUnitsUnimplemented ) ) ? ( string ) $this->m_intLostUnitsUnimplemented : '0';
	}

	public function setLostUnitsTransferProperty( $intLostUnitsTransferProperty ) {
		$this->set( 'm_intLostUnitsTransferProperty', CStrings::strToIntDef( $intLostUnitsTransferProperty, NULL, false ) );
	}

	public function getLostUnitsTransferProperty() {
		return $this->m_intLostUnitsTransferProperty;
	}

	public function sqlLostUnitsTransferProperty() {
		return ( true == isset( $this->m_intLostUnitsTransferProperty ) ) ? ( string ) $this->m_intLostUnitsTransferProperty : '0';
	}

	public function setLost12MonthRevenue( $intLost12MonthRevenue ) {
		$this->set( 'm_intLost12MonthRevenue', CStrings::strToIntDef( $intLost12MonthRevenue, NULL, false ) );
	}

	public function getLost12MonthRevenue() {
		return $this->m_intLost12MonthRevenue;
	}

	public function sqlLost12MonthRevenue() {
		return ( true == isset( $this->m_intLost12MonthRevenue ) ) ? ( string ) $this->m_intLost12MonthRevenue : '0';
	}

	public function setNetUnitChange( $intNetUnitChange ) {
		$this->set( 'm_intNetUnitChange', CStrings::strToIntDef( $intNetUnitChange, NULL, false ) );
	}

	public function getNetUnitChange() {
		return $this->m_intNetUnitChange;
	}

	public function sqlNetUnitChange() {
		return ( true == isset( $this->m_intNetUnitChange ) ) ? ( string ) $this->m_intNetUnitChange : '0';
	}

	public function setCumulativeUnits( $intCumulativeUnits ) {
		$this->set( 'm_intCumulativeUnits', CStrings::strToIntDef( $intCumulativeUnits, NULL, false ) );
	}

	public function getCumulativeUnits() {
		return $this->m_intCumulativeUnits;
	}

	public function sqlCumulativeUnits() {
		return ( true == isset( $this->m_intCumulativeUnits ) ) ? ( string ) $this->m_intCumulativeUnits : '0';
	}

	public function setNewLogoSubscriptionAcv( $intNewLogoSubscriptionAcv ) {
		$this->set( 'm_intNewLogoSubscriptionAcv', CStrings::strToIntDef( $intNewLogoSubscriptionAcv, NULL, false ) );
	}

	public function getNewLogoSubscriptionAcv() {
		return $this->m_intNewLogoSubscriptionAcv;
	}

	public function sqlNewLogoSubscriptionAcv() {
		return ( true == isset( $this->m_intNewLogoSubscriptionAcv ) ) ? ( string ) $this->m_intNewLogoSubscriptionAcv : '0';
	}

	public function setNewLogoTransactionAcv( $intNewLogoTransactionAcv ) {
		$this->set( 'm_intNewLogoTransactionAcv', CStrings::strToIntDef( $intNewLogoTransactionAcv, NULL, false ) );
	}

	public function getNewLogoTransactionAcv() {
		return $this->m_intNewLogoTransactionAcv;
	}

	public function sqlNewLogoTransactionAcv() {
		return ( true == isset( $this->m_intNewLogoTransactionAcv ) ) ? ( string ) $this->m_intNewLogoTransactionAcv : '0';
	}

	public function setNewLogoImplementation( $intNewLogoImplementation ) {
		$this->set( 'm_intNewLogoImplementation', CStrings::strToIntDef( $intNewLogoImplementation, NULL, false ) );
	}

	public function getNewLogoImplementation() {
		return $this->m_intNewLogoImplementation;
	}

	public function sqlNewLogoImplementation() {
		return ( true == isset( $this->m_intNewLogoImplementation ) ) ? ( string ) $this->m_intNewLogoImplementation : '0';
	}

	public function setNewProductSubscriptionAcv( $intNewProductSubscriptionAcv ) {
		$this->set( 'm_intNewProductSubscriptionAcv', CStrings::strToIntDef( $intNewProductSubscriptionAcv, NULL, false ) );
	}

	public function getNewProductSubscriptionAcv() {
		return $this->m_intNewProductSubscriptionAcv;
	}

	public function sqlNewProductSubscriptionAcv() {
		return ( true == isset( $this->m_intNewProductSubscriptionAcv ) ) ? ( string ) $this->m_intNewProductSubscriptionAcv : '0';
	}

	public function setNewProductTransactionAcv( $intNewProductTransactionAcv ) {
		$this->set( 'm_intNewProductTransactionAcv', CStrings::strToIntDef( $intNewProductTransactionAcv, NULL, false ) );
	}

	public function getNewProductTransactionAcv() {
		return $this->m_intNewProductTransactionAcv;
	}

	public function sqlNewProductTransactionAcv() {
		return ( true == isset( $this->m_intNewProductTransactionAcv ) ) ? ( string ) $this->m_intNewProductTransactionAcv : '0';
	}

	public function setNewProductImplementation( $intNewProductImplementation ) {
		$this->set( 'm_intNewProductImplementation', CStrings::strToIntDef( $intNewProductImplementation, NULL, false ) );
	}

	public function getNewProductImplementation() {
		return $this->m_intNewProductImplementation;
	}

	public function sqlNewProductImplementation() {
		return ( true == isset( $this->m_intNewProductImplementation ) ) ? ( string ) $this->m_intNewProductImplementation : '0';
	}

	public function setNewPropertySubscriptionAcv( $intNewPropertySubscriptionAcv ) {
		$this->set( 'm_intNewPropertySubscriptionAcv', CStrings::strToIntDef( $intNewPropertySubscriptionAcv, NULL, false ) );
	}

	public function getNewPropertySubscriptionAcv() {
		return $this->m_intNewPropertySubscriptionAcv;
	}

	public function sqlNewPropertySubscriptionAcv() {
		return ( true == isset( $this->m_intNewPropertySubscriptionAcv ) ) ? ( string ) $this->m_intNewPropertySubscriptionAcv : '0';
	}

	public function setNewPropertyTransactionAcv( $intNewPropertyTransactionAcv ) {
		$this->set( 'm_intNewPropertyTransactionAcv', CStrings::strToIntDef( $intNewPropertyTransactionAcv, NULL, false ) );
	}

	public function getNewPropertyTransactionAcv() {
		return $this->m_intNewPropertyTransactionAcv;
	}

	public function sqlNewPropertyTransactionAcv() {
		return ( true == isset( $this->m_intNewPropertyTransactionAcv ) ) ? ( string ) $this->m_intNewPropertyTransactionAcv : '0';
	}

	public function setNewPropertyImplementation( $intNewPropertyImplementation ) {
		$this->set( 'm_intNewPropertyImplementation', CStrings::strToIntDef( $intNewPropertyImplementation, NULL, false ) );
	}

	public function getNewPropertyImplementation() {
		return $this->m_intNewPropertyImplementation;
	}

	public function sqlNewPropertyImplementation() {
		return ( true == isset( $this->m_intNewPropertyImplementation ) ) ? ( string ) $this->m_intNewPropertyImplementation : '0';
	}

	public function setNewRenewalSubscriptionAcv( $intNewRenewalSubscriptionAcv ) {
		$this->set( 'm_intNewRenewalSubscriptionAcv', CStrings::strToIntDef( $intNewRenewalSubscriptionAcv, NULL, false ) );
	}

	public function getNewRenewalSubscriptionAcv() {
		return $this->m_intNewRenewalSubscriptionAcv;
	}

	public function sqlNewRenewalSubscriptionAcv() {
		return ( true == isset( $this->m_intNewRenewalSubscriptionAcv ) ) ? ( string ) $this->m_intNewRenewalSubscriptionAcv : '0';
	}

	public function setNewRenewalTransactionAcv( $intNewRenewalTransactionAcv ) {
		$this->set( 'm_intNewRenewalTransactionAcv', CStrings::strToIntDef( $intNewRenewalTransactionAcv, NULL, false ) );
	}

	public function getNewRenewalTransactionAcv() {
		return $this->m_intNewRenewalTransactionAcv;
	}

	public function sqlNewRenewalTransactionAcv() {
		return ( true == isset( $this->m_intNewRenewalTransactionAcv ) ) ? ( string ) $this->m_intNewRenewalTransactionAcv : '0';
	}

	public function setNewRenewalImplementation( $intNewRenewalImplementation ) {
		$this->set( 'm_intNewRenewalImplementation', CStrings::strToIntDef( $intNewRenewalImplementation, NULL, false ) );
	}

	public function getNewRenewalImplementation() {
		return $this->m_intNewRenewalImplementation;
	}

	public function sqlNewRenewalImplementation() {
		return ( true == isset( $this->m_intNewRenewalImplementation ) ) ? ( string ) $this->m_intNewRenewalImplementation : '0';
	}

	public function setNewTransferPropertySubscriptionAcv( $intNewTransferPropertySubscriptionAcv ) {
		$this->set( 'm_intNewTransferPropertySubscriptionAcv', CStrings::strToIntDef( $intNewTransferPropertySubscriptionAcv, NULL, false ) );
	}

	public function getNewTransferPropertySubscriptionAcv() {
		return $this->m_intNewTransferPropertySubscriptionAcv;
	}

	public function sqlNewTransferPropertySubscriptionAcv() {
		return ( true == isset( $this->m_intNewTransferPropertySubscriptionAcv ) ) ? ( string ) $this->m_intNewTransferPropertySubscriptionAcv : '0';
	}

	public function setNewTransferPropertyTransactionAcv( $intNewTransferPropertyTransactionAcv ) {
		$this->set( 'm_intNewTransferPropertyTransactionAcv', CStrings::strToIntDef( $intNewTransferPropertyTransactionAcv, NULL, false ) );
	}

	public function getNewTransferPropertyTransactionAcv() {
		return $this->m_intNewTransferPropertyTransactionAcv;
	}

	public function sqlNewTransferPropertyTransactionAcv() {
		return ( true == isset( $this->m_intNewTransferPropertyTransactionAcv ) ) ? ( string ) $this->m_intNewTransferPropertyTransactionAcv : '0';
	}

	public function setNewTransferPropertyImplementation( $intNewTransferPropertyImplementation ) {
		$this->set( 'm_intNewTransferPropertyImplementation', CStrings::strToIntDef( $intNewTransferPropertyImplementation, NULL, false ) );
	}

	public function getNewTransferPropertyImplementation() {
		return $this->m_intNewTransferPropertyImplementation;
	}

	public function sqlNewTransferPropertyImplementation() {
		return ( true == isset( $this->m_intNewTransferPropertyImplementation ) ) ? ( string ) $this->m_intNewTransferPropertyImplementation : '0';
	}

	public function setSalesAcv( $intSalesAcv ) {
		$this->set( 'm_intSalesAcv', CStrings::strToIntDef( $intSalesAcv, NULL, false ) );
	}

	public function getSalesAcv() {
		return $this->m_intSalesAcv;
	}

	public function sqlSalesAcv() {
		return ( true == isset( $this->m_intSalesAcv ) ) ? ( string ) $this->m_intSalesAcv : '0';
	}

	public function setBookedAcv( $intBookedAcv ) {
		$this->set( 'm_intBookedAcv', CStrings::strToIntDef( $intBookedAcv, NULL, false ) );
	}

	public function getBookedAcv() {
		return $this->m_intBookedAcv;
	}

	public function sqlBookedAcv() {
		return ( true == isset( $this->m_intBookedAcv ) ) ? ( string ) $this->m_intBookedAcv : '0';
	}

	public function setTotalNewAcv( $intTotalNewAcv ) {
		$this->set( 'm_intTotalNewAcv', CStrings::strToIntDef( $intTotalNewAcv, NULL, false ) );
	}

	public function getTotalNewAcv() {
		return $this->m_intTotalNewAcv;
	}

	public function sqlTotalNewAcv() {
		return ( true == isset( $this->m_intTotalNewAcv ) ) ? ( string ) $this->m_intTotalNewAcv : '0';
	}

	public function setExpandedAcv( $intExpandedAcv ) {
		$this->set( 'm_intExpandedAcv', CStrings::strToIntDef( $intExpandedAcv, NULL, false ) );
	}

	public function getExpandedAcv() {
		return $this->m_intExpandedAcv;
	}

	public function sqlExpandedAcv() {
		return ( true == isset( $this->m_intExpandedAcv ) ) ? ( string ) $this->m_intExpandedAcv : '0';
	}

	public function setLostAcvUnhappy( $intLostAcvUnhappy ) {
		$this->set( 'm_intLostAcvUnhappy', CStrings::strToIntDef( $intLostAcvUnhappy, NULL, false ) );
	}

	public function getLostAcvUnhappy() {
		return $this->m_intLostAcvUnhappy;
	}

	public function sqlLostAcvUnhappy() {
		return ( true == isset( $this->m_intLostAcvUnhappy ) ) ? ( string ) $this->m_intLostAcvUnhappy : '0';
	}

	public function setLostAcvManagement( $intLostAcvManagement ) {
		$this->set( 'm_intLostAcvManagement', CStrings::strToIntDef( $intLostAcvManagement, NULL, false ) );
	}

	public function getLostAcvManagement() {
		return $this->m_intLostAcvManagement;
	}

	public function sqlLostAcvManagement() {
		return ( true == isset( $this->m_intLostAcvManagement ) ) ? ( string ) $this->m_intLostAcvManagement : '0';
	}

	public function setLostAcvUnimplemented( $intLostAcvUnimplemented ) {
		$this->set( 'm_intLostAcvUnimplemented', CStrings::strToIntDef( $intLostAcvUnimplemented, NULL, false ) );
	}

	public function getLostAcvUnimplemented() {
		return $this->m_intLostAcvUnimplemented;
	}

	public function sqlLostAcvUnimplemented() {
		return ( true == isset( $this->m_intLostAcvUnimplemented ) ) ? ( string ) $this->m_intLostAcvUnimplemented : '0';
	}

	public function setLostAcvTransferProperty( $intLostAcvTransferProperty ) {
		$this->set( 'm_intLostAcvTransferProperty', CStrings::strToIntDef( $intLostAcvTransferProperty, NULL, false ) );
	}

	public function getLostAcvTransferProperty() {
		return $this->m_intLostAcvTransferProperty;
	}

	public function sqlLostAcvTransferProperty() {
		return ( true == isset( $this->m_intLostAcvTransferProperty ) ) ? ( string ) $this->m_intLostAcvTransferProperty : '0';
	}

	public function setTotalLostAcv( $intTotalLostAcv ) {
		$this->set( 'm_intTotalLostAcv', CStrings::strToIntDef( $intTotalLostAcv, NULL, false ) );
	}

	public function getTotalLostAcv() {
		return $this->m_intTotalLostAcv;
	}

	public function sqlTotalLostAcv() {
		return ( true == isset( $this->m_intTotalLostAcv ) ) ? ( string ) $this->m_intTotalLostAcv : '0';
	}

	public function setNetAcvChange( $intNetAcvChange ) {
		$this->set( 'm_intNetAcvChange', CStrings::strToIntDef( $intNetAcvChange, NULL, false ) );
	}

	public function getNetAcvChange() {
		return $this->m_intNetAcvChange;
	}

	public function sqlNetAcvChange() {
		return ( true == isset( $this->m_intNetAcvChange ) ) ? ( string ) $this->m_intNetAcvChange : '0';
	}

	public function setCumulativeAcv( $intCumulativeAcv ) {
		$this->set( 'm_intCumulativeAcv', CStrings::strToIntDef( $intCumulativeAcv, NULL, false ) );
	}

	public function getCumulativeAcv() {
		return $this->m_intCumulativeAcv;
	}

	public function sqlCumulativeAcv() {
		return ( true == isset( $this->m_intCumulativeAcv ) ) ? ( string ) $this->m_intCumulativeAcv : '0';
	}

	public function setMcvPerUnit( $intMcvPerUnit ) {
		$this->set( 'm_intMcvPerUnit', CStrings::strToIntDef( $intMcvPerUnit, NULL, false ) );
	}

	public function getMcvPerUnit() {
		return $this->m_intMcvPerUnit;
	}

	public function sqlMcvPerUnit() {
		return ( true == isset( $this->m_intMcvPerUnit ) ) ? ( string ) $this->m_intMcvPerUnit : '0';
	}

	public function setRevenuePerUnit( $intRevenuePerUnit ) {
		$this->set( 'm_intRevenuePerUnit', CStrings::strToIntDef( $intRevenuePerUnit, NULL, false ) );
	}

	public function getRevenuePerUnit() {
		return $this->m_intRevenuePerUnit;
	}

	public function sqlRevenuePerUnit() {
		return ( true == isset( $this->m_intRevenuePerUnit ) ) ? ( string ) $this->m_intRevenuePerUnit : '0';
	}

	public function setNotesSubmitted( $intNotesSubmitted ) {
		$this->set( 'm_intNotesSubmitted', CStrings::strToIntDef( $intNotesSubmitted, NULL, false ) );
	}

	public function getNotesSubmitted() {
		return $this->m_intNotesSubmitted;
	}

	public function sqlNotesSubmitted() {
		return ( true == isset( $this->m_intNotesSubmitted ) ) ? ( string ) $this->m_intNotesSubmitted : '0';
	}

	public function setDemosScheduled( $intDemosScheduled ) {
		$this->set( 'm_intDemosScheduled', CStrings::strToIntDef( $intDemosScheduled, NULL, false ) );
	}

	public function getDemosScheduled() {
		return $this->m_intDemosScheduled;
	}

	public function sqlDemosScheduled() {
		return ( true == isset( $this->m_intDemosScheduled ) ) ? ( string ) $this->m_intDemosScheduled : '0';
	}

	public function setCallsMade( $intCallsMade ) {
		$this->set( 'm_intCallsMade', CStrings::strToIntDef( $intCallsMade, NULL, false ) );
	}

	public function getCallsMade() {
		return $this->m_intCallsMade;
	}

	public function sqlCallsMade() {
		return ( true == isset( $this->m_intCallsMade ) ) ? ( string ) $this->m_intCallsMade : '0';
	}

	public function setEmailsSent( $intEmailsSent ) {
		$this->set( 'm_intEmailsSent', CStrings::strToIntDef( $intEmailsSent, NULL, false ) );
	}

	public function getEmailsSent() {
		return $this->m_intEmailsSent;
	}

	public function sqlEmailsSent() {
		return ( true == isset( $this->m_intEmailsSent ) ) ? ( string ) $this->m_intEmailsSent : '0';
	}

	public function setFollowUpsScheduled( $intFollowUpsScheduled ) {
		$this->set( 'm_intFollowUpsScheduled', CStrings::strToIntDef( $intFollowUpsScheduled, NULL, false ) );
	}

	public function getFollowUpsScheduled() {
		return $this->m_intFollowUpsScheduled;
	}

	public function sqlFollowUpsScheduled() {
		return ( true == isset( $this->m_intFollowUpsScheduled ) ) ? ( string ) $this->m_intFollowUpsScheduled : '0';
	}

	public function setTotalActions( $intTotalActions ) {
		$this->set( 'm_intTotalActions', CStrings::strToIntDef( $intTotalActions, NULL, false ) );
	}

	public function getTotalActions() {
		return $this->m_intTotalActions;
	}

	public function sqlTotalActions() {
		return ( true == isset( $this->m_intTotalActions ) ) ? ( string ) $this->m_intTotalActions : '0';
	}

	public function setCallsConnected( $intCallsConnected ) {
		$this->set( 'm_intCallsConnected', CStrings::strToIntDef( $intCallsConnected, NULL, false ) );
	}

	public function getCallsConnected() {
		return $this->m_intCallsConnected;
	}

	public function sqlCallsConnected() {
		return ( true == isset( $this->m_intCallsConnected ) ) ? ( string ) $this->m_intCallsConnected : '0';
	}

	public function setDemosForgotten( $intDemosForgotten ) {
		$this->set( 'm_intDemosForgotten', CStrings::strToIntDef( $intDemosForgotten, NULL, false ) );
	}

	public function getDemosForgotten() {
		return $this->m_intDemosForgotten;
	}

	public function sqlDemosForgotten() {
		return ( true == isset( $this->m_intDemosForgotten ) ) ? ( string ) $this->m_intDemosForgotten : '0';
	}

	public function setDemosCompleted( $intDemosCompleted ) {
		$this->set( 'm_intDemosCompleted', CStrings::strToIntDef( $intDemosCompleted, NULL, false ) );
	}

	public function getDemosCompleted() {
		return $this->m_intDemosCompleted;
	}

	public function sqlDemosCompleted() {
		return ( true == isset( $this->m_intDemosCompleted ) ) ? ( string ) $this->m_intDemosCompleted : '0';
	}

	public function setEmailsReturned( $intEmailsReturned ) {
		$this->set( 'm_intEmailsReturned', CStrings::strToIntDef( $intEmailsReturned, NULL, false ) );
	}

	public function getEmailsReturned() {
		return $this->m_intEmailsReturned;
	}

	public function sqlEmailsReturned() {
		return ( true == isset( $this->m_intEmailsReturned ) ) ? ( string ) $this->m_intEmailsReturned : '0';
	}

	public function setRemote( $intRemote ) {
		$this->set( 'm_intRemote', CStrings::strToIntDef( $intRemote, NULL, false ) );
	}

	public function getRemote() {
		return $this->m_intRemote;
	}

	public function sqlRemote() {
		return ( true == isset( $this->m_intRemote ) ) ? ( string ) $this->m_intRemote : '0';
	}

	public function setOffice( $intOffice ) {
		$this->set( 'm_intOffice', CStrings::strToIntDef( $intOffice, NULL, false ) );
	}

	public function getOffice() {
		return $this->m_intOffice;
	}

	public function sqlOffice() {
		return ( true == isset( $this->m_intOffice ) ) ? ( string ) $this->m_intOffice : '0';
	}

	public function setMeal( $intMeal ) {
		$this->set( 'm_intMeal', CStrings::strToIntDef( $intMeal, NULL, false ) );
	}

	public function getMeal() {
		return $this->m_intMeal;
	}

	public function sqlMeal() {
		return ( true == isset( $this->m_intMeal ) ) ? ( string ) $this->m_intMeal : '0';
	}

	public function setEvent( $intEvent ) {
		$this->set( 'm_intEvent', CStrings::strToIntDef( $intEvent, NULL, false ) );
	}

	public function getEvent() {
		return $this->m_intEvent;
	}

	public function sqlEvent() {
		return ( true == isset( $this->m_intEvent ) ) ? ( string ) $this->m_intEvent : '0';
	}

	public function setFollowUpsCompleted( $intFollowUpsCompleted ) {
		$this->set( 'm_intFollowUpsCompleted', CStrings::strToIntDef( $intFollowUpsCompleted, NULL, false ) );
	}

	public function getFollowUpsCompleted() {
		return $this->m_intFollowUpsCompleted;
	}

	public function sqlFollowUpsCompleted() {
		return ( true == isset( $this->m_intFollowUpsCompleted ) ) ? ( string ) $this->m_intFollowUpsCompleted : '0';
	}

	public function setPeepEmailsSent( $intPeepEmailsSent ) {
		$this->set( 'm_intPeepEmailsSent', CStrings::strToIntDef( $intPeepEmailsSent, NULL, false ) );
	}

	public function getPeepEmailsSent() {
		return $this->m_intPeepEmailsSent;
	}

	public function sqlPeepEmailsSent() {
		return ( true == isset( $this->m_intPeepEmailsSent ) ) ? ( string ) $this->m_intPeepEmailsSent : '0';
	}

	public function setPeepEmailsReturned( $intPeepEmailsReturned ) {
		$this->set( 'm_intPeepEmailsReturned', CStrings::strToIntDef( $intPeepEmailsReturned, NULL, false ) );
	}

	public function getPeepEmailsReturned() {
		return $this->m_intPeepEmailsReturned;
	}

	public function sqlPeepEmailsReturned() {
		return ( true == isset( $this->m_intPeepEmailsReturned ) ) ? ( string ) $this->m_intPeepEmailsReturned : '0';
	}

	public function setPeepEmailsClicked( $intPeepEmailsClicked ) {
		$this->set( 'm_intPeepEmailsClicked', CStrings::strToIntDef( $intPeepEmailsClicked, NULL, false ) );
	}

	public function getPeepEmailsClicked() {
		return $this->m_intPeepEmailsClicked;
	}

	public function sqlPeepEmailsClicked() {
		return ( true == isset( $this->m_intPeepEmailsClicked ) ) ? ( string ) $this->m_intPeepEmailsClicked : '0';
	}

	public function setTotalLeads( $intTotalLeads ) {
		$this->set( 'm_intTotalLeads', CStrings::strToIntDef( $intTotalLeads, NULL, false ) );
	}

	public function getTotalLeads() {
		return $this->m_intTotalLeads;
	}

	public function sqlTotalLeads() {
		return ( true == isset( $this->m_intTotalLeads ) ) ? ( string ) $this->m_intTotalLeads : '0';
	}

	public function setTotalUnits( $intTotalUnits ) {
		$this->set( 'm_intTotalUnits', CStrings::strToIntDef( $intTotalUnits, NULL, false ) );
	}

	public function getTotalUnits() {
		return $this->m_intTotalUnits;
	}

	public function sqlTotalUnits() {
		return ( true == isset( $this->m_intTotalUnits ) ) ? ( string ) $this->m_intTotalUnits : '0';
	}

	public function setProposalsSent( $intProposalsSent ) {
		$this->set( 'm_intProposalsSent', CStrings::strToIntDef( $intProposalsSent, NULL, false ) );
	}

	public function getProposalsSent() {
		return $this->m_intProposalsSent;
	}

	public function sqlProposalsSent() {
		return ( true == isset( $this->m_intProposalsSent ) ) ? ( string ) $this->m_intProposalsSent : '0';
	}

	public function setPipelineAcv( $intPipelineAcv ) {
		$this->set( 'm_intPipelineAcv', CStrings::strToIntDef( $intPipelineAcv, NULL, false ) );
	}

	public function getPipelineAcv() {
		return $this->m_intPipelineAcv;
	}

	public function sqlPipelineAcv() {
		return ( true == isset( $this->m_intPipelineAcv ) ) ? ( string ) $this->m_intPipelineAcv : '0';
	}

	public function setLeadsAdded( $intLeadsAdded ) {
		$this->set( 'm_intLeadsAdded', CStrings::strToIntDef( $intLeadsAdded, NULL, false ) );
	}

	public function getLeadsAdded() {
		return $this->m_intLeadsAdded;
	}

	public function sqlLeadsAdded() {
		return ( true == isset( $this->m_intLeadsAdded ) ) ? ( string ) $this->m_intLeadsAdded : '0';
	}

	public function setLeadsUpdated( $intLeadsUpdated ) {
		$this->set( 'm_intLeadsUpdated', CStrings::strToIntDef( $intLeadsUpdated, NULL, false ) );
	}

	public function getLeadsUpdated() {
		return $this->m_intLeadsUpdated;
	}

	public function sqlLeadsUpdated() {
		return ( true == isset( $this->m_intLeadsUpdated ) ) ? ( string ) $this->m_intLeadsUpdated : '0';
	}

	public function setPeopleAdded( $intPeopleAdded ) {
		$this->set( 'm_intPeopleAdded', CStrings::strToIntDef( $intPeopleAdded, NULL, false ) );
	}

	public function getPeopleAdded() {
		return $this->m_intPeopleAdded;
	}

	public function sqlPeopleAdded() {
		return ( true == isset( $this->m_intPeopleAdded ) ) ? ( string ) $this->m_intPeopleAdded : '0';
	}

	public function setPeopleUpdated( $intPeopleUpdated ) {
		$this->set( 'm_intPeopleUpdated', CStrings::strToIntDef( $intPeopleUpdated, NULL, false ) );
	}

	public function getPeopleUpdated() {
		return $this->m_intPeopleUpdated;
	}

	public function sqlPeopleUpdated() {
		return ( true == isset( $this->m_intPeopleUpdated ) ) ? ( string ) $this->m_intPeopleUpdated : '0';
	}

	public function setVendorsAdded( $intVendorsAdded ) {
		$this->set( 'm_intVendorsAdded', CStrings::strToIntDef( $intVendorsAdded, NULL, false ) );
	}

	public function getVendorsAdded() {
		return $this->m_intVendorsAdded;
	}

	public function sqlVendorsAdded() {
		return ( true == isset( $this->m_intVendorsAdded ) ) ? ( string ) $this->m_intVendorsAdded : '0';
	}

	public function setVendorsUpdated( $intVendorsUpdated ) {
		$this->set( 'm_intVendorsUpdated', CStrings::strToIntDef( $intVendorsUpdated, NULL, false ) );
	}

	public function getVendorsUpdated() {
		return $this->m_intVendorsUpdated;
	}

	public function sqlVendorsUpdated() {
		return ( true == isset( $this->m_intVendorsUpdated ) ) ? ( string ) $this->m_intVendorsUpdated : '0';
	}

	public function setOpportunitiesAdded( $intOpportunitiesAdded ) {
		$this->set( 'm_intOpportunitiesAdded', CStrings::strToIntDef( $intOpportunitiesAdded, NULL, false ) );
	}

	public function getOpportunitiesAdded() {
		return $this->m_intOpportunitiesAdded;
	}

	public function sqlOpportunitiesAdded() {
		return ( true == isset( $this->m_intOpportunitiesAdded ) ) ? ( string ) $this->m_intOpportunitiesAdded : '0';
	}

	public function setOpportunitiesUpdated( $intOpportunitiesUpdated ) {
		$this->set( 'm_intOpportunitiesUpdated', CStrings::strToIntDef( $intOpportunitiesUpdated, NULL, false ) );
	}

	public function getOpportunitiesUpdated() {
		return $this->m_intOpportunitiesUpdated;
	}

	public function sqlOpportunitiesUpdated() {
		return ( true == isset( $this->m_intOpportunitiesUpdated ) ) ? ( string ) $this->m_intOpportunitiesUpdated : '0';
	}

	public function setCommissionsScheduled( $intCommissionsScheduled ) {
		$this->set( 'm_intCommissionsScheduled', CStrings::strToIntDef( $intCommissionsScheduled, NULL, false ) );
	}

	public function getCommissionsScheduled() {
		return $this->m_intCommissionsScheduled;
	}

	public function sqlCommissionsScheduled() {
		return ( true == isset( $this->m_intCommissionsScheduled ) ) ? ( string ) $this->m_intCommissionsScheduled : '0';
	}

	public function setProfileCompletionPercent( $intProfileCompletionPercent ) {
		$this->set( 'm_intProfileCompletionPercent', CStrings::strToIntDef( $intProfileCompletionPercent, NULL, false ) );
	}

	public function getProfileCompletionPercent() {
		return $this->m_intProfileCompletionPercent;
	}

	public function sqlProfileCompletionPercent() {
		return ( true == isset( $this->m_intProfileCompletionPercent ) ) ? ( string ) $this->m_intProfileCompletionPercent : '0';
	}

	public function setCommissionsPaid( $intCommissionsPaid ) {
		$this->set( 'm_intCommissionsPaid', CStrings::strToIntDef( $intCommissionsPaid, NULL, false ) );
	}

	public function getCommissionsPaid() {
		return $this->m_intCommissionsPaid;
	}

	public function sqlCommissionsPaid() {
		return ( true == isset( $this->m_intCommissionsPaid ) ) ? ( string ) $this->m_intCommissionsPaid : '0';
	}

	public function setPilotNewSubscriptionAcv( $intPilotNewSubscriptionAcv ) {
		$this->set( 'm_intPilotNewSubscriptionAcv', CStrings::strToIntDef( $intPilotNewSubscriptionAcv, NULL, false ) );
	}

	public function getPilotNewSubscriptionAcv() {
		return $this->m_intPilotNewSubscriptionAcv;
	}

	public function sqlPilotNewSubscriptionAcv() {
		return ( true == isset( $this->m_intPilotNewSubscriptionAcv ) ) ? ( string ) $this->m_intPilotNewSubscriptionAcv : '0';
	}

	public function setPilotNewTransactionAcv( $intPilotNewTransactionAcv ) {
		$this->set( 'm_intPilotNewTransactionAcv', CStrings::strToIntDef( $intPilotNewTransactionAcv, NULL, false ) );
	}

	public function getPilotNewTransactionAcv() {
		return $this->m_intPilotNewTransactionAcv;
	}

	public function sqlPilotNewTransactionAcv() {
		return ( true == isset( $this->m_intPilotNewTransactionAcv ) ) ? ( string ) $this->m_intPilotNewTransactionAcv : '0';
	}

	public function setPilotPendingSubscriptionAcv( $intPilotPendingSubscriptionAcv ) {
		$this->set( 'm_intPilotPendingSubscriptionAcv', CStrings::strToIntDef( $intPilotPendingSubscriptionAcv, NULL, false ) );
	}

	public function getPilotPendingSubscriptionAcv() {
		return $this->m_intPilotPendingSubscriptionAcv;
	}

	public function sqlPilotPendingSubscriptionAcv() {
		return ( true == isset( $this->m_intPilotPendingSubscriptionAcv ) ) ? ( string ) $this->m_intPilotPendingSubscriptionAcv : '0';
	}

	public function setPilotPendingTransactionAcv( $intPilotPendingTransactionAcv ) {
		$this->set( 'm_intPilotPendingTransactionAcv', CStrings::strToIntDef( $intPilotPendingTransactionAcv, NULL, false ) );
	}

	public function getPilotPendingTransactionAcv() {
		return $this->m_intPilotPendingTransactionAcv;
	}

	public function sqlPilotPendingTransactionAcv() {
		return ( true == isset( $this->m_intPilotPendingTransactionAcv ) ) ? ( string ) $this->m_intPilotPendingTransactionAcv : '0';
	}

	public function setPilotWonSubscriptionAcv( $intPilotWonSubscriptionAcv ) {
		$this->set( 'm_intPilotWonSubscriptionAcv', CStrings::strToIntDef( $intPilotWonSubscriptionAcv, NULL, false ) );
	}

	public function getPilotWonSubscriptionAcv() {
		return $this->m_intPilotWonSubscriptionAcv;
	}

	public function sqlPilotWonSubscriptionAcv() {
		return ( true == isset( $this->m_intPilotWonSubscriptionAcv ) ) ? ( string ) $this->m_intPilotWonSubscriptionAcv : '0';
	}

	public function setPilotWonTransactionAcv( $intPilotWonTransactionAcv ) {
		$this->set( 'm_intPilotWonTransactionAcv', CStrings::strToIntDef( $intPilotWonTransactionAcv, NULL, false ) );
	}

	public function getPilotWonTransactionAcv() {
		return $this->m_intPilotWonTransactionAcv;
	}

	public function sqlPilotWonTransactionAcv() {
		return ( true == isset( $this->m_intPilotWonTransactionAcv ) ) ? ( string ) $this->m_intPilotWonTransactionAcv : '0';
	}

	public function setPilotLostSubscriptionAcv( $intPilotLostSubscriptionAcv ) {
		$this->set( 'm_intPilotLostSubscriptionAcv', CStrings::strToIntDef( $intPilotLostSubscriptionAcv, NULL, false ) );
	}

	public function getPilotLostSubscriptionAcv() {
		return $this->m_intPilotLostSubscriptionAcv;
	}

	public function sqlPilotLostSubscriptionAcv() {
		return ( true == isset( $this->m_intPilotLostSubscriptionAcv ) ) ? ( string ) $this->m_intPilotLostSubscriptionAcv : '0';
	}

	public function setPilotLostTransactionAcv( $intPilotLostTransactionAcv ) {
		$this->set( 'm_intPilotLostTransactionAcv', CStrings::strToIntDef( $intPilotLostTransactionAcv, NULL, false ) );
	}

	public function getPilotLostTransactionAcv() {
		return $this->m_intPilotLostTransactionAcv;
	}

	public function sqlPilotLostTransactionAcv() {
		return ( true == isset( $this->m_intPilotLostTransactionAcv ) ) ? ( string ) $this->m_intPilotLostTransactionAcv : '0';
	}

	public function setPilotOpenCount( $intPilotOpenCount ) {
		$this->set( 'm_intPilotOpenCount', CStrings::strToIntDef( $intPilotOpenCount, NULL, false ) );
	}

	public function getPilotOpenCount() {
		return $this->m_intPilotOpenCount;
	}

	public function sqlPilotOpenCount() {
		return ( true == isset( $this->m_intPilotOpenCount ) ) ? ( string ) $this->m_intPilotOpenCount : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '0';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, department_id, month, committed_acv, quarterly_acv_quota, annual_acv_quota, new_clients, lost_clients, net_client_change, cumulative_clients, subscription_revenue, transaction_revenue, one_time_revenue, new_logo_units, new_product_units, new_property_units, new_transfer_property_units, lost_units_unhappy, lost_units_management, lost_units_unimplemented, lost_units_transfer_property, lost_12_month_revenue, net_unit_change, cumulative_units, new_logo_subscription_acv, new_logo_transaction_acv, new_logo_implementation, new_product_subscription_acv, new_product_transaction_acv, new_product_implementation, new_property_subscription_acv, new_property_transaction_acv, new_property_implementation, new_renewal_subscription_acv, new_renewal_transaction_acv, new_renewal_implementation, new_transfer_property_subscription_acv, new_transfer_property_transaction_acv, new_transfer_property_implementation, sales_acv, booked_acv, total_new_acv, expanded_acv, lost_acv_unhappy, lost_acv_management, lost_acv_unimplemented, lost_acv_transfer_property, total_lost_acv, net_acv_change, cumulative_acv, mcv_per_unit, revenue_per_unit, notes_submitted, demos_scheduled, calls_made, emails_sent, follow_ups_scheduled, total_actions, calls_connected, demos_forgotten, demos_completed, emails_returned, remote, office, meal, event, follow_ups_completed, peep_emails_sent, peep_emails_returned, peep_emails_clicked, total_leads, total_units, proposals_sent, pipeline_acv, leads_added, leads_updated, people_added, people_updated, vendors_added, vendors_updated, opportunities_added, opportunities_updated, commissions_scheduled, profile_completion_percent, commissions_paid, pilot_new_subscription_acv, pilot_new_transaction_acv, pilot_pending_subscription_acv, pilot_pending_transaction_acv, pilot_won_subscription_acv, pilot_won_transaction_acv, pilot_lost_subscription_acv, pilot_lost_transaction_acv, pilot_open_count, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlDepartmentId() . ', ' .
 						$this->sqlMonth() . ', ' .
 						$this->sqlCommittedAcv() . ', ' .
 						$this->sqlQuarterlyAcvQuota() . ', ' .
 						$this->sqlAnnualAcvQuota() . ', ' .
 						$this->sqlNewClients() . ', ' .
 						$this->sqlLostClients() . ', ' .
 						$this->sqlNetClientChange() . ', ' .
 						$this->sqlCumulativeClients() . ', ' .
 						$this->sqlSubscriptionRevenue() . ', ' .
 						$this->sqlTransactionRevenue() . ', ' .
 						$this->sqlOneTimeRevenue() . ', ' .
 						$this->sqlNewLogoUnits() . ', ' .
 						$this->sqlNewProductUnits() . ', ' .
 						$this->sqlNewPropertyUnits() . ', ' .
 						$this->sqlNewTransferPropertyUnits() . ', ' .
 						$this->sqlLostUnitsUnhappy() . ', ' .
 						$this->sqlLostUnitsManagement() . ', ' .
 						$this->sqlLostUnitsUnimplemented() . ', ' .
 						$this->sqlLostUnitsTransferProperty() . ', ' .
 						$this->sqlLost12MonthRevenue() . ', ' .
 						$this->sqlNetUnitChange() . ', ' .
 						$this->sqlCumulativeUnits() . ', ' .
 						$this->sqlNewLogoSubscriptionAcv() . ', ' .
 						$this->sqlNewLogoTransactionAcv() . ', ' .
 						$this->sqlNewLogoImplementation() . ', ' .
 						$this->sqlNewProductSubscriptionAcv() . ', ' .
 						$this->sqlNewProductTransactionAcv() . ', ' .
 						$this->sqlNewProductImplementation() . ', ' .
 						$this->sqlNewPropertySubscriptionAcv() . ', ' .
 						$this->sqlNewPropertyTransactionAcv() . ', ' .
 						$this->sqlNewPropertyImplementation() . ', ' .
 						$this->sqlNewRenewalSubscriptionAcv() . ', ' .
 						$this->sqlNewRenewalTransactionAcv() . ', ' .
 						$this->sqlNewRenewalImplementation() . ', ' .
 						$this->sqlNewTransferPropertySubscriptionAcv() . ', ' .
 						$this->sqlNewTransferPropertyTransactionAcv() . ', ' .
 						$this->sqlNewTransferPropertyImplementation() . ', ' .
 						$this->sqlSalesAcv() . ', ' .
 						$this->sqlBookedAcv() . ', ' .
 						$this->sqlTotalNewAcv() . ', ' .
 						$this->sqlExpandedAcv() . ', ' .
 						$this->sqlLostAcvUnhappy() . ', ' .
 						$this->sqlLostAcvManagement() . ', ' .
 						$this->sqlLostAcvUnimplemented() . ', ' .
 						$this->sqlLostAcvTransferProperty() . ', ' .
 						$this->sqlTotalLostAcv() . ', ' .
 						$this->sqlNetAcvChange() . ', ' .
 						$this->sqlCumulativeAcv() . ', ' .
 						$this->sqlMcvPerUnit() . ', ' .
 						$this->sqlRevenuePerUnit() . ', ' .
 						$this->sqlNotesSubmitted() . ', ' .
 						$this->sqlDemosScheduled() . ', ' .
 						$this->sqlCallsMade() . ', ' .
 						$this->sqlEmailsSent() . ', ' .
 						$this->sqlFollowUpsScheduled() . ', ' .
 						$this->sqlTotalActions() . ', ' .
 						$this->sqlCallsConnected() . ', ' .
 						$this->sqlDemosForgotten() . ', ' .
 						$this->sqlDemosCompleted() . ', ' .
 						$this->sqlEmailsReturned() . ', ' .
 						$this->sqlRemote() . ', ' .
 						$this->sqlOffice() . ', ' .
 						$this->sqlMeal() . ', ' .
 						$this->sqlEvent() . ', ' .
 						$this->sqlFollowUpsCompleted() . ', ' .
 						$this->sqlPeepEmailsSent() . ', ' .
 						$this->sqlPeepEmailsReturned() . ', ' .
 						$this->sqlPeepEmailsClicked() . ', ' .
 						$this->sqlTotalLeads() . ', ' .
 						$this->sqlTotalUnits() . ', ' .
 						$this->sqlProposalsSent() . ', ' .
 						$this->sqlPipelineAcv() . ', ' .
 						$this->sqlLeadsAdded() . ', ' .
 						$this->sqlLeadsUpdated() . ', ' .
 						$this->sqlPeopleAdded() . ', ' .
 						$this->sqlPeopleUpdated() . ', ' .
 						$this->sqlVendorsAdded() . ', ' .
 						$this->sqlVendorsUpdated() . ', ' .
 						$this->sqlOpportunitiesAdded() . ', ' .
 						$this->sqlOpportunitiesUpdated() . ', ' .
 						$this->sqlCommissionsScheduled() . ', ' .
 						$this->sqlProfileCompletionPercent() . ', ' .
 						$this->sqlCommissionsPaid() . ', ' .
 						$this->sqlPilotNewSubscriptionAcv() . ', ' .
 						$this->sqlPilotNewTransactionAcv() . ', ' .
 						$this->sqlPilotPendingSubscriptionAcv() . ', ' .
 						$this->sqlPilotPendingTransactionAcv() . ', ' .
 						$this->sqlPilotWonSubscriptionAcv() . ', ' .
 						$this->sqlPilotWonTransactionAcv() . ', ' .
 						$this->sqlPilotLostSubscriptionAcv() . ', ' .
 						$this->sqlPilotLostTransactionAcv() . ', ' .
 						$this->sqlPilotOpenCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_acv = ' . $this->sqlCommittedAcv() . ','; } elseif( true == array_key_exists( 'CommittedAcv', $this->getChangedColumns() ) ) { $strSql .= ' committed_acv = ' . $this->sqlCommittedAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quarterly_acv_quota = ' . $this->sqlQuarterlyAcvQuota() . ','; } elseif( true == array_key_exists( 'QuarterlyAcvQuota', $this->getChangedColumns() ) ) { $strSql .= ' quarterly_acv_quota = ' . $this->sqlQuarterlyAcvQuota() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' annual_acv_quota = ' . $this->sqlAnnualAcvQuota() . ','; } elseif( true == array_key_exists( 'AnnualAcvQuota', $this->getChangedColumns() ) ) { $strSql .= ' annual_acv_quota = ' . $this->sqlAnnualAcvQuota() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_clients = ' . $this->sqlNewClients() . ','; } elseif( true == array_key_exists( 'NewClients', $this->getChangedColumns() ) ) { $strSql .= ' new_clients = ' . $this->sqlNewClients() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_clients = ' . $this->sqlLostClients() . ','; } elseif( true == array_key_exists( 'LostClients', $this->getChangedColumns() ) ) { $strSql .= ' lost_clients = ' . $this->sqlLostClients() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' net_client_change = ' . $this->sqlNetClientChange() . ','; } elseif( true == array_key_exists( 'NetClientChange', $this->getChangedColumns() ) ) { $strSql .= ' net_client_change = ' . $this->sqlNetClientChange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cumulative_clients = ' . $this->sqlCumulativeClients() . ','; } elseif( true == array_key_exists( 'CumulativeClients', $this->getChangedColumns() ) ) { $strSql .= ' cumulative_clients = ' . $this->sqlCumulativeClients() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subscription_revenue = ' . $this->sqlSubscriptionRevenue() . ','; } elseif( true == array_key_exists( 'SubscriptionRevenue', $this->getChangedColumns() ) ) { $strSql .= ' subscription_revenue = ' . $this->sqlSubscriptionRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_revenue = ' . $this->sqlTransactionRevenue() . ','; } elseif( true == array_key_exists( 'TransactionRevenue', $this->getChangedColumns() ) ) { $strSql .= ' transaction_revenue = ' . $this->sqlTransactionRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' one_time_revenue = ' . $this->sqlOneTimeRevenue() . ','; } elseif( true == array_key_exists( 'OneTimeRevenue', $this->getChangedColumns() ) ) { $strSql .= ' one_time_revenue = ' . $this->sqlOneTimeRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_logo_units = ' . $this->sqlNewLogoUnits() . ','; } elseif( true == array_key_exists( 'NewLogoUnits', $this->getChangedColumns() ) ) { $strSql .= ' new_logo_units = ' . $this->sqlNewLogoUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_product_units = ' . $this->sqlNewProductUnits() . ','; } elseif( true == array_key_exists( 'NewProductUnits', $this->getChangedColumns() ) ) { $strSql .= ' new_product_units = ' . $this->sqlNewProductUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_property_units = ' . $this->sqlNewPropertyUnits() . ','; } elseif( true == array_key_exists( 'NewPropertyUnits', $this->getChangedColumns() ) ) { $strSql .= ' new_property_units = ' . $this->sqlNewPropertyUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_transfer_property_units = ' . $this->sqlNewTransferPropertyUnits() . ','; } elseif( true == array_key_exists( 'NewTransferPropertyUnits', $this->getChangedColumns() ) ) { $strSql .= ' new_transfer_property_units = ' . $this->sqlNewTransferPropertyUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_units_unhappy = ' . $this->sqlLostUnitsUnhappy() . ','; } elseif( true == array_key_exists( 'LostUnitsUnhappy', $this->getChangedColumns() ) ) { $strSql .= ' lost_units_unhappy = ' . $this->sqlLostUnitsUnhappy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_units_management = ' . $this->sqlLostUnitsManagement() . ','; } elseif( true == array_key_exists( 'LostUnitsManagement', $this->getChangedColumns() ) ) { $strSql .= ' lost_units_management = ' . $this->sqlLostUnitsManagement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_units_unimplemented = ' . $this->sqlLostUnitsUnimplemented() . ','; } elseif( true == array_key_exists( 'LostUnitsUnimplemented', $this->getChangedColumns() ) ) { $strSql .= ' lost_units_unimplemented = ' . $this->sqlLostUnitsUnimplemented() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_units_transfer_property = ' . $this->sqlLostUnitsTransferProperty() . ','; } elseif( true == array_key_exists( 'LostUnitsTransferProperty', $this->getChangedColumns() ) ) { $strSql .= ' lost_units_transfer_property = ' . $this->sqlLostUnitsTransferProperty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_12_month_revenue = ' . $this->sqlLost12MonthRevenue() . ','; } elseif( true == array_key_exists( 'Lost12MonthRevenue', $this->getChangedColumns() ) ) { $strSql .= ' lost_12_month_revenue = ' . $this->sqlLost12MonthRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' net_unit_change = ' . $this->sqlNetUnitChange() . ','; } elseif( true == array_key_exists( 'NetUnitChange', $this->getChangedColumns() ) ) { $strSql .= ' net_unit_change = ' . $this->sqlNetUnitChange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cumulative_units = ' . $this->sqlCumulativeUnits() . ','; } elseif( true == array_key_exists( 'CumulativeUnits', $this->getChangedColumns() ) ) { $strSql .= ' cumulative_units = ' . $this->sqlCumulativeUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_logo_subscription_acv = ' . $this->sqlNewLogoSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'NewLogoSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_logo_subscription_acv = ' . $this->sqlNewLogoSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_logo_transaction_acv = ' . $this->sqlNewLogoTransactionAcv() . ','; } elseif( true == array_key_exists( 'NewLogoTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_logo_transaction_acv = ' . $this->sqlNewLogoTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_logo_implementation = ' . $this->sqlNewLogoImplementation() . ','; } elseif( true == array_key_exists( 'NewLogoImplementation', $this->getChangedColumns() ) ) { $strSql .= ' new_logo_implementation = ' . $this->sqlNewLogoImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_product_subscription_acv = ' . $this->sqlNewProductSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'NewProductSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_product_subscription_acv = ' . $this->sqlNewProductSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_product_transaction_acv = ' . $this->sqlNewProductTransactionAcv() . ','; } elseif( true == array_key_exists( 'NewProductTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_product_transaction_acv = ' . $this->sqlNewProductTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_product_implementation = ' . $this->sqlNewProductImplementation() . ','; } elseif( true == array_key_exists( 'NewProductImplementation', $this->getChangedColumns() ) ) { $strSql .= ' new_product_implementation = ' . $this->sqlNewProductImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_property_subscription_acv = ' . $this->sqlNewPropertySubscriptionAcv() . ','; } elseif( true == array_key_exists( 'NewPropertySubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_property_subscription_acv = ' . $this->sqlNewPropertySubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_property_transaction_acv = ' . $this->sqlNewPropertyTransactionAcv() . ','; } elseif( true == array_key_exists( 'NewPropertyTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_property_transaction_acv = ' . $this->sqlNewPropertyTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_property_implementation = ' . $this->sqlNewPropertyImplementation() . ','; } elseif( true == array_key_exists( 'NewPropertyImplementation', $this->getChangedColumns() ) ) { $strSql .= ' new_property_implementation = ' . $this->sqlNewPropertyImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_renewal_subscription_acv = ' . $this->sqlNewRenewalSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'NewRenewalSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_renewal_subscription_acv = ' . $this->sqlNewRenewalSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_renewal_transaction_acv = ' . $this->sqlNewRenewalTransactionAcv() . ','; } elseif( true == array_key_exists( 'NewRenewalTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_renewal_transaction_acv = ' . $this->sqlNewRenewalTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_renewal_implementation = ' . $this->sqlNewRenewalImplementation() . ','; } elseif( true == array_key_exists( 'NewRenewalImplementation', $this->getChangedColumns() ) ) { $strSql .= ' new_renewal_implementation = ' . $this->sqlNewRenewalImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_transfer_property_subscription_acv = ' . $this->sqlNewTransferPropertySubscriptionAcv() . ','; } elseif( true == array_key_exists( 'NewTransferPropertySubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_transfer_property_subscription_acv = ' . $this->sqlNewTransferPropertySubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_transfer_property_transaction_acv = ' . $this->sqlNewTransferPropertyTransactionAcv() . ','; } elseif( true == array_key_exists( 'NewTransferPropertyTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' new_transfer_property_transaction_acv = ' . $this->sqlNewTransferPropertyTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_transfer_property_implementation = ' . $this->sqlNewTransferPropertyImplementation() . ','; } elseif( true == array_key_exists( 'NewTransferPropertyImplementation', $this->getChangedColumns() ) ) { $strSql .= ' new_transfer_property_implementation = ' . $this->sqlNewTransferPropertyImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_acv = ' . $this->sqlSalesAcv() . ','; } elseif( true == array_key_exists( 'SalesAcv', $this->getChangedColumns() ) ) { $strSql .= ' sales_acv = ' . $this->sqlSalesAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' booked_acv = ' . $this->sqlBookedAcv() . ','; } elseif( true == array_key_exists( 'BookedAcv', $this->getChangedColumns() ) ) { $strSql .= ' booked_acv = ' . $this->sqlBookedAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_new_acv = ' . $this->sqlTotalNewAcv() . ','; } elseif( true == array_key_exists( 'TotalNewAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_new_acv = ' . $this->sqlTotalNewAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expanded_acv = ' . $this->sqlExpandedAcv() . ','; } elseif( true == array_key_exists( 'ExpandedAcv', $this->getChangedColumns() ) ) { $strSql .= ' expanded_acv = ' . $this->sqlExpandedAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_acv_unhappy = ' . $this->sqlLostAcvUnhappy() . ','; } elseif( true == array_key_exists( 'LostAcvUnhappy', $this->getChangedColumns() ) ) { $strSql .= ' lost_acv_unhappy = ' . $this->sqlLostAcvUnhappy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_acv_management = ' . $this->sqlLostAcvManagement() . ','; } elseif( true == array_key_exists( 'LostAcvManagement', $this->getChangedColumns() ) ) { $strSql .= ' lost_acv_management = ' . $this->sqlLostAcvManagement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_acv_unimplemented = ' . $this->sqlLostAcvUnimplemented() . ','; } elseif( true == array_key_exists( 'LostAcvUnimplemented', $this->getChangedColumns() ) ) { $strSql .= ' lost_acv_unimplemented = ' . $this->sqlLostAcvUnimplemented() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_acv_transfer_property = ' . $this->sqlLostAcvTransferProperty() . ','; } elseif( true == array_key_exists( 'LostAcvTransferProperty', $this->getChangedColumns() ) ) { $strSql .= ' lost_acv_transfer_property = ' . $this->sqlLostAcvTransferProperty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_lost_acv = ' . $this->sqlTotalLostAcv() . ','; } elseif( true == array_key_exists( 'TotalLostAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_lost_acv = ' . $this->sqlTotalLostAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' net_acv_change = ' . $this->sqlNetAcvChange() . ','; } elseif( true == array_key_exists( 'NetAcvChange', $this->getChangedColumns() ) ) { $strSql .= ' net_acv_change = ' . $this->sqlNetAcvChange() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cumulative_acv = ' . $this->sqlCumulativeAcv() . ','; } elseif( true == array_key_exists( 'CumulativeAcv', $this->getChangedColumns() ) ) { $strSql .= ' cumulative_acv = ' . $this->sqlCumulativeAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mcv_per_unit = ' . $this->sqlMcvPerUnit() . ','; } elseif( true == array_key_exists( 'McvPerUnit', $this->getChangedColumns() ) ) { $strSql .= ' mcv_per_unit = ' . $this->sqlMcvPerUnit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revenue_per_unit = ' . $this->sqlRevenuePerUnit() . ','; } elseif( true == array_key_exists( 'RevenuePerUnit', $this->getChangedColumns() ) ) { $strSql .= ' revenue_per_unit = ' . $this->sqlRevenuePerUnit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes_submitted = ' . $this->sqlNotesSubmitted() . ','; } elseif( true == array_key_exists( 'NotesSubmitted', $this->getChangedColumns() ) ) { $strSql .= ' notes_submitted = ' . $this->sqlNotesSubmitted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demos_scheduled = ' . $this->sqlDemosScheduled() . ','; } elseif( true == array_key_exists( 'DemosScheduled', $this->getChangedColumns() ) ) { $strSql .= ' demos_scheduled = ' . $this->sqlDemosScheduled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calls_made = ' . $this->sqlCallsMade() . ','; } elseif( true == array_key_exists( 'CallsMade', $this->getChangedColumns() ) ) { $strSql .= ' calls_made = ' . $this->sqlCallsMade() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emails_sent = ' . $this->sqlEmailsSent() . ','; } elseif( true == array_key_exists( 'EmailsSent', $this->getChangedColumns() ) ) { $strSql .= ' emails_sent = ' . $this->sqlEmailsSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' follow_ups_scheduled = ' . $this->sqlFollowUpsScheduled() . ','; } elseif( true == array_key_exists( 'FollowUpsScheduled', $this->getChangedColumns() ) ) { $strSql .= ' follow_ups_scheduled = ' . $this->sqlFollowUpsScheduled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_actions = ' . $this->sqlTotalActions() . ','; } elseif( true == array_key_exists( 'TotalActions', $this->getChangedColumns() ) ) { $strSql .= ' total_actions = ' . $this->sqlTotalActions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calls_connected = ' . $this->sqlCallsConnected() . ','; } elseif( true == array_key_exists( 'CallsConnected', $this->getChangedColumns() ) ) { $strSql .= ' calls_connected = ' . $this->sqlCallsConnected() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demos_forgotten = ' . $this->sqlDemosForgotten() . ','; } elseif( true == array_key_exists( 'DemosForgotten', $this->getChangedColumns() ) ) { $strSql .= ' demos_forgotten = ' . $this->sqlDemosForgotten() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demos_completed = ' . $this->sqlDemosCompleted() . ','; } elseif( true == array_key_exists( 'DemosCompleted', $this->getChangedColumns() ) ) { $strSql .= ' demos_completed = ' . $this->sqlDemosCompleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emails_returned = ' . $this->sqlEmailsReturned() . ','; } elseif( true == array_key_exists( 'EmailsReturned', $this->getChangedColumns() ) ) { $strSql .= ' emails_returned = ' . $this->sqlEmailsReturned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote = ' . $this->sqlRemote() . ','; } elseif( true == array_key_exists( 'Remote', $this->getChangedColumns() ) ) { $strSql .= ' remote = ' . $this->sqlRemote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office = ' . $this->sqlOffice() . ','; } elseif( true == array_key_exists( 'Office', $this->getChangedColumns() ) ) { $strSql .= ' office = ' . $this->sqlOffice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meal = ' . $this->sqlMeal() . ','; } elseif( true == array_key_exists( 'Meal', $this->getChangedColumns() ) ) { $strSql .= ' meal = ' . $this->sqlMeal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event = ' . $this->sqlEvent() . ','; } elseif( true == array_key_exists( 'Event', $this->getChangedColumns() ) ) { $strSql .= ' event = ' . $this->sqlEvent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' follow_ups_completed = ' . $this->sqlFollowUpsCompleted() . ','; } elseif( true == array_key_exists( 'FollowUpsCompleted', $this->getChangedColumns() ) ) { $strSql .= ' follow_ups_completed = ' . $this->sqlFollowUpsCompleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' peep_emails_sent = ' . $this->sqlPeepEmailsSent() . ','; } elseif( true == array_key_exists( 'PeepEmailsSent', $this->getChangedColumns() ) ) { $strSql .= ' peep_emails_sent = ' . $this->sqlPeepEmailsSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' peep_emails_returned = ' . $this->sqlPeepEmailsReturned() . ','; } elseif( true == array_key_exists( 'PeepEmailsReturned', $this->getChangedColumns() ) ) { $strSql .= ' peep_emails_returned = ' . $this->sqlPeepEmailsReturned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' peep_emails_clicked = ' . $this->sqlPeepEmailsClicked() . ','; } elseif( true == array_key_exists( 'PeepEmailsClicked', $this->getChangedColumns() ) ) { $strSql .= ' peep_emails_clicked = ' . $this->sqlPeepEmailsClicked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_leads = ' . $this->sqlTotalLeads() . ','; } elseif( true == array_key_exists( 'TotalLeads', $this->getChangedColumns() ) ) { $strSql .= ' total_leads = ' . $this->sqlTotalLeads() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_units = ' . $this->sqlTotalUnits() . ','; } elseif( true == array_key_exists( 'TotalUnits', $this->getChangedColumns() ) ) { $strSql .= ' total_units = ' . $this->sqlTotalUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposals_sent = ' . $this->sqlProposalsSent() . ','; } elseif( true == array_key_exists( 'ProposalsSent', $this->getChangedColumns() ) ) { $strSql .= ' proposals_sent = ' . $this->sqlProposalsSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pipeline_acv = ' . $this->sqlPipelineAcv() . ','; } elseif( true == array_key_exists( 'PipelineAcv', $this->getChangedColumns() ) ) { $strSql .= ' pipeline_acv = ' . $this->sqlPipelineAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leads_added = ' . $this->sqlLeadsAdded() . ','; } elseif( true == array_key_exists( 'LeadsAdded', $this->getChangedColumns() ) ) { $strSql .= ' leads_added = ' . $this->sqlLeadsAdded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leads_updated = ' . $this->sqlLeadsUpdated() . ','; } elseif( true == array_key_exists( 'LeadsUpdated', $this->getChangedColumns() ) ) { $strSql .= ' leads_updated = ' . $this->sqlLeadsUpdated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' people_added = ' . $this->sqlPeopleAdded() . ','; } elseif( true == array_key_exists( 'PeopleAdded', $this->getChangedColumns() ) ) { $strSql .= ' people_added = ' . $this->sqlPeopleAdded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' people_updated = ' . $this->sqlPeopleUpdated() . ','; } elseif( true == array_key_exists( 'PeopleUpdated', $this->getChangedColumns() ) ) { $strSql .= ' people_updated = ' . $this->sqlPeopleUpdated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendors_added = ' . $this->sqlVendorsAdded() . ','; } elseif( true == array_key_exists( 'VendorsAdded', $this->getChangedColumns() ) ) { $strSql .= ' vendors_added = ' . $this->sqlVendorsAdded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendors_updated = ' . $this->sqlVendorsUpdated() . ','; } elseif( true == array_key_exists( 'VendorsUpdated', $this->getChangedColumns() ) ) { $strSql .= ' vendors_updated = ' . $this->sqlVendorsUpdated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' opportunities_added = ' . $this->sqlOpportunitiesAdded() . ','; } elseif( true == array_key_exists( 'OpportunitiesAdded', $this->getChangedColumns() ) ) { $strSql .= ' opportunities_added = ' . $this->sqlOpportunitiesAdded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' opportunities_updated = ' . $this->sqlOpportunitiesUpdated() . ','; } elseif( true == array_key_exists( 'OpportunitiesUpdated', $this->getChangedColumns() ) ) { $strSql .= ' opportunities_updated = ' . $this->sqlOpportunitiesUpdated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commissions_scheduled = ' . $this->sqlCommissionsScheduled() . ','; } elseif( true == array_key_exists( 'CommissionsScheduled', $this->getChangedColumns() ) ) { $strSql .= ' commissions_scheduled = ' . $this->sqlCommissionsScheduled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' profile_completion_percent = ' . $this->sqlProfileCompletionPercent() . ','; } elseif( true == array_key_exists( 'ProfileCompletionPercent', $this->getChangedColumns() ) ) { $strSql .= ' profile_completion_percent = ' . $this->sqlProfileCompletionPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commissions_paid = ' . $this->sqlCommissionsPaid() . ','; } elseif( true == array_key_exists( 'CommissionsPaid', $this->getChangedColumns() ) ) { $strSql .= ' commissions_paid = ' . $this->sqlCommissionsPaid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_new_subscription_acv = ' . $this->sqlPilotNewSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'PilotNewSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_new_subscription_acv = ' . $this->sqlPilotNewSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_new_transaction_acv = ' . $this->sqlPilotNewTransactionAcv() . ','; } elseif( true == array_key_exists( 'PilotNewTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_new_transaction_acv = ' . $this->sqlPilotNewTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_pending_subscription_acv = ' . $this->sqlPilotPendingSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'PilotPendingSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_pending_subscription_acv = ' . $this->sqlPilotPendingSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_pending_transaction_acv = ' . $this->sqlPilotPendingTransactionAcv() . ','; } elseif( true == array_key_exists( 'PilotPendingTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_pending_transaction_acv = ' . $this->sqlPilotPendingTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_won_subscription_acv = ' . $this->sqlPilotWonSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'PilotWonSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_won_subscription_acv = ' . $this->sqlPilotWonSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_won_transaction_acv = ' . $this->sqlPilotWonTransactionAcv() . ','; } elseif( true == array_key_exists( 'PilotWonTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_won_transaction_acv = ' . $this->sqlPilotWonTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_lost_subscription_acv = ' . $this->sqlPilotLostSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'PilotLostSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_lost_subscription_acv = ' . $this->sqlPilotLostSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_lost_transaction_acv = ' . $this->sqlPilotLostTransactionAcv() . ','; } elseif( true == array_key_exists( 'PilotLostTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' pilot_lost_transaction_acv = ' . $this->sqlPilotLostTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_open_count = ' . $this->sqlPilotOpenCount() . ','; } elseif( true == array_key_exists( 'PilotOpenCount', $this->getChangedColumns() ) ) { $strSql .= ' pilot_open_count = ' . $this->sqlPilotOpenCount() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'department_id' => $this->getDepartmentId(),
			'month' => $this->getMonth(),
			'committed_acv' => $this->getCommittedAcv(),
			'quarterly_acv_quota' => $this->getQuarterlyAcvQuota(),
			'annual_acv_quota' => $this->getAnnualAcvQuota(),
			'new_clients' => $this->getNewClients(),
			'lost_clients' => $this->getLostClients(),
			'net_client_change' => $this->getNetClientChange(),
			'cumulative_clients' => $this->getCumulativeClients(),
			'subscription_revenue' => $this->getSubscriptionRevenue(),
			'transaction_revenue' => $this->getTransactionRevenue(),
			'one_time_revenue' => $this->getOneTimeRevenue(),
			'new_logo_units' => $this->getNewLogoUnits(),
			'new_product_units' => $this->getNewProductUnits(),
			'new_property_units' => $this->getNewPropertyUnits(),
			'new_transfer_property_units' => $this->getNewTransferPropertyUnits(),
			'lost_units_unhappy' => $this->getLostUnitsUnhappy(),
			'lost_units_management' => $this->getLostUnitsManagement(),
			'lost_units_unimplemented' => $this->getLostUnitsUnimplemented(),
			'lost_units_transfer_property' => $this->getLostUnitsTransferProperty(),
			'lost_12_month_revenue' => $this->getLost12MonthRevenue(),
			'net_unit_change' => $this->getNetUnitChange(),
			'cumulative_units' => $this->getCumulativeUnits(),
			'new_logo_subscription_acv' => $this->getNewLogoSubscriptionAcv(),
			'new_logo_transaction_acv' => $this->getNewLogoTransactionAcv(),
			'new_logo_implementation' => $this->getNewLogoImplementation(),
			'new_product_subscription_acv' => $this->getNewProductSubscriptionAcv(),
			'new_product_transaction_acv' => $this->getNewProductTransactionAcv(),
			'new_product_implementation' => $this->getNewProductImplementation(),
			'new_property_subscription_acv' => $this->getNewPropertySubscriptionAcv(),
			'new_property_transaction_acv' => $this->getNewPropertyTransactionAcv(),
			'new_property_implementation' => $this->getNewPropertyImplementation(),
			'new_renewal_subscription_acv' => $this->getNewRenewalSubscriptionAcv(),
			'new_renewal_transaction_acv' => $this->getNewRenewalTransactionAcv(),
			'new_renewal_implementation' => $this->getNewRenewalImplementation(),
			'new_transfer_property_subscription_acv' => $this->getNewTransferPropertySubscriptionAcv(),
			'new_transfer_property_transaction_acv' => $this->getNewTransferPropertyTransactionAcv(),
			'new_transfer_property_implementation' => $this->getNewTransferPropertyImplementation(),
			'sales_acv' => $this->getSalesAcv(),
			'booked_acv' => $this->getBookedAcv(),
			'total_new_acv' => $this->getTotalNewAcv(),
			'expanded_acv' => $this->getExpandedAcv(),
			'lost_acv_unhappy' => $this->getLostAcvUnhappy(),
			'lost_acv_management' => $this->getLostAcvManagement(),
			'lost_acv_unimplemented' => $this->getLostAcvUnimplemented(),
			'lost_acv_transfer_property' => $this->getLostAcvTransferProperty(),
			'total_lost_acv' => $this->getTotalLostAcv(),
			'net_acv_change' => $this->getNetAcvChange(),
			'cumulative_acv' => $this->getCumulativeAcv(),
			'mcv_per_unit' => $this->getMcvPerUnit(),
			'revenue_per_unit' => $this->getRevenuePerUnit(),
			'notes_submitted' => $this->getNotesSubmitted(),
			'demos_scheduled' => $this->getDemosScheduled(),
			'calls_made' => $this->getCallsMade(),
			'emails_sent' => $this->getEmailsSent(),
			'follow_ups_scheduled' => $this->getFollowUpsScheduled(),
			'total_actions' => $this->getTotalActions(),
			'calls_connected' => $this->getCallsConnected(),
			'demos_forgotten' => $this->getDemosForgotten(),
			'demos_completed' => $this->getDemosCompleted(),
			'emails_returned' => $this->getEmailsReturned(),
			'remote' => $this->getRemote(),
			'office' => $this->getOffice(),
			'meal' => $this->getMeal(),
			'event' => $this->getEvent(),
			'follow_ups_completed' => $this->getFollowUpsCompleted(),
			'peep_emails_sent' => $this->getPeepEmailsSent(),
			'peep_emails_returned' => $this->getPeepEmailsReturned(),
			'peep_emails_clicked' => $this->getPeepEmailsClicked(),
			'total_leads' => $this->getTotalLeads(),
			'total_units' => $this->getTotalUnits(),
			'proposals_sent' => $this->getProposalsSent(),
			'pipeline_acv' => $this->getPipelineAcv(),
			'leads_added' => $this->getLeadsAdded(),
			'leads_updated' => $this->getLeadsUpdated(),
			'people_added' => $this->getPeopleAdded(),
			'people_updated' => $this->getPeopleUpdated(),
			'vendors_added' => $this->getVendorsAdded(),
			'vendors_updated' => $this->getVendorsUpdated(),
			'opportunities_added' => $this->getOpportunitiesAdded(),
			'opportunities_updated' => $this->getOpportunitiesUpdated(),
			'commissions_scheduled' => $this->getCommissionsScheduled(),
			'profile_completion_percent' => $this->getProfileCompletionPercent(),
			'commissions_paid' => $this->getCommissionsPaid(),
			'pilot_new_subscription_acv' => $this->getPilotNewSubscriptionAcv(),
			'pilot_new_transaction_acv' => $this->getPilotNewTransactionAcv(),
			'pilot_pending_subscription_acv' => $this->getPilotPendingSubscriptionAcv(),
			'pilot_pending_transaction_acv' => $this->getPilotPendingTransactionAcv(),
			'pilot_won_subscription_acv' => $this->getPilotWonSubscriptionAcv(),
			'pilot_won_transaction_acv' => $this->getPilotWonTransactionAcv(),
			'pilot_lost_subscription_acv' => $this->getPilotLostSubscriptionAcv(),
			'pilot_lost_transaction_acv' => $this->getPilotLostTransactionAcv(),
			'pilot_open_count' => $this->getPilotOpenCount(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>