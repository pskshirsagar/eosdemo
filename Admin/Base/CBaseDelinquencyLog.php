<?php

class CBaseDelinquencyLog extends CEosSingularBase {

	const TABLE_NAME = 'public.delinquency_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intPropertyId;
	protected $m_boolIsManualUpdate;
	protected $m_intPropertyDelinquencyId;
	protected $m_intSystemEmailId;
	protected $m_intDelinquencyLevelTypeId;
	protected $m_intEligibleDelinquencyLevelTypeId;
	protected $m_intMaxDelinquencyLevelTypeId;
	protected $m_strInvoiceIds;
	protected $m_fltThresholdAmount;
	protected $m_fltOverdueAmount;
	protected $m_strUserNote;
	protected $m_strSystemNote;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsManualUpdate = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['is_manual_update'] ) && $boolDirectSet ) $this->set( 'm_boolIsManualUpdate', trim( stripcslashes( $arrValues['is_manual_update'] ) ) ); elseif( isset( $arrValues['is_manual_update'] ) ) $this->setIsManualUpdate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_manual_update'] ) : $arrValues['is_manual_update'] );
		if( isset( $arrValues['property_delinquency_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyDelinquencyId', trim( $arrValues['property_delinquency_id'] ) ); elseif( isset( $arrValues['property_delinquency_id'] ) ) $this->setPropertyDelinquencyId( $arrValues['property_delinquency_id'] );
		if( isset( $arrValues['system_email_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailId', trim( $arrValues['system_email_id'] ) ); elseif( isset( $arrValues['system_email_id'] ) ) $this->setSystemEmailId( $arrValues['system_email_id'] );
		if( isset( $arrValues['delinquency_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyLevelTypeId', trim( $arrValues['delinquency_level_type_id'] ) ); elseif( isset( $arrValues['delinquency_level_type_id'] ) ) $this->setDelinquencyLevelTypeId( $arrValues['delinquency_level_type_id'] );
		if( isset( $arrValues['eligible_delinquency_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEligibleDelinquencyLevelTypeId', trim( $arrValues['eligible_delinquency_level_type_id'] ) ); elseif( isset( $arrValues['eligible_delinquency_level_type_id'] ) ) $this->setEligibleDelinquencyLevelTypeId( $arrValues['eligible_delinquency_level_type_id'] );
		if( isset( $arrValues['max_delinquency_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMaxDelinquencyLevelTypeId', trim( $arrValues['max_delinquency_level_type_id'] ) ); elseif( isset( $arrValues['max_delinquency_level_type_id'] ) ) $this->setMaxDelinquencyLevelTypeId( $arrValues['max_delinquency_level_type_id'] );
		if( isset( $arrValues['invoice_ids'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceIds', trim( stripcslashes( $arrValues['invoice_ids'] ) ) ); elseif( isset( $arrValues['invoice_ids'] ) ) $this->setInvoiceIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['invoice_ids'] ) : $arrValues['invoice_ids'] );
		if( isset( $arrValues['threshold_amount'] ) && $boolDirectSet ) $this->set( 'm_fltThresholdAmount', trim( $arrValues['threshold_amount'] ) ); elseif( isset( $arrValues['threshold_amount'] ) ) $this->setThresholdAmount( $arrValues['threshold_amount'] );
		if( isset( $arrValues['overdue_amount'] ) && $boolDirectSet ) $this->set( 'm_fltOverdueAmount', trim( $arrValues['overdue_amount'] ) ); elseif( isset( $arrValues['overdue_amount'] ) ) $this->setOverdueAmount( $arrValues['overdue_amount'] );
		if( isset( $arrValues['user_note'] ) && $boolDirectSet ) $this->set( 'm_strUserNote', trim( stripcslashes( $arrValues['user_note'] ) ) ); elseif( isset( $arrValues['user_note'] ) ) $this->setUserNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['user_note'] ) : $arrValues['user_note'] );
		if( isset( $arrValues['system_note'] ) && $boolDirectSet ) $this->set( 'm_strSystemNote', trim( stripcslashes( $arrValues['system_note'] ) ) ); elseif( isset( $arrValues['system_note'] ) ) $this->setSystemNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['system_note'] ) : $arrValues['system_note'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setIsManualUpdate( $boolIsManualUpdate ) {
		$this->set( 'm_boolIsManualUpdate', CStrings::strToBool( $boolIsManualUpdate ) );
	}

	public function getIsManualUpdate() {
		return $this->m_boolIsManualUpdate;
	}

	public function sqlIsManualUpdate() {
		return ( true == isset( $this->m_boolIsManualUpdate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsManualUpdate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPropertyDelinquencyId( $intPropertyDelinquencyId ) {
		$this->set( 'm_intPropertyDelinquencyId', CStrings::strToIntDef( $intPropertyDelinquencyId, NULL, false ) );
	}

	public function getPropertyDelinquencyId() {
		return $this->m_intPropertyDelinquencyId;
	}

	public function sqlPropertyDelinquencyId() {
		return ( true == isset( $this->m_intPropertyDelinquencyId ) ) ? ( string ) $this->m_intPropertyDelinquencyId : 'NULL';
	}

	public function setSystemEmailId( $intSystemEmailId ) {
		$this->set( 'm_intSystemEmailId', CStrings::strToIntDef( $intSystemEmailId, NULL, false ) );
	}

	public function getSystemEmailId() {
		return $this->m_intSystemEmailId;
	}

	public function sqlSystemEmailId() {
		return ( true == isset( $this->m_intSystemEmailId ) ) ? ( string ) $this->m_intSystemEmailId : 'NULL';
	}

	public function setDelinquencyLevelTypeId( $intDelinquencyLevelTypeId ) {
		$this->set( 'm_intDelinquencyLevelTypeId', CStrings::strToIntDef( $intDelinquencyLevelTypeId, NULL, false ) );
	}

	public function getDelinquencyLevelTypeId() {
		return $this->m_intDelinquencyLevelTypeId;
	}

	public function sqlDelinquencyLevelTypeId() {
		return ( true == isset( $this->m_intDelinquencyLevelTypeId ) ) ? ( string ) $this->m_intDelinquencyLevelTypeId : 'NULL';
	}

	public function setEligibleDelinquencyLevelTypeId( $intEligibleDelinquencyLevelTypeId ) {
		$this->set( 'm_intEligibleDelinquencyLevelTypeId', CStrings::strToIntDef( $intEligibleDelinquencyLevelTypeId, NULL, false ) );
	}

	public function getEligibleDelinquencyLevelTypeId() {
		return $this->m_intEligibleDelinquencyLevelTypeId;
	}

	public function sqlEligibleDelinquencyLevelTypeId() {
		return ( true == isset( $this->m_intEligibleDelinquencyLevelTypeId ) ) ? ( string ) $this->m_intEligibleDelinquencyLevelTypeId : 'NULL';
	}

	public function setMaxDelinquencyLevelTypeId( $intMaxDelinquencyLevelTypeId ) {
		$this->set( 'm_intMaxDelinquencyLevelTypeId', CStrings::strToIntDef( $intMaxDelinquencyLevelTypeId, NULL, false ) );
	}

	public function getMaxDelinquencyLevelTypeId() {
		return $this->m_intMaxDelinquencyLevelTypeId;
	}

	public function sqlMaxDelinquencyLevelTypeId() {
		return ( true == isset( $this->m_intMaxDelinquencyLevelTypeId ) ) ? ( string ) $this->m_intMaxDelinquencyLevelTypeId : 'NULL';
	}

	public function setInvoiceIds( $strInvoiceIds ) {
		$this->set( 'm_strInvoiceIds', CStrings::strTrimDef( $strInvoiceIds, -1, NULL, true ) );
	}

	public function getInvoiceIds() {
		return $this->m_strInvoiceIds;
	}

	public function sqlInvoiceIds() {
		return ( true == isset( $this->m_strInvoiceIds ) ) ? '\'' . addslashes( $this->m_strInvoiceIds ) . '\'' : 'NULL';
	}

	public function setThresholdAmount( $fltThresholdAmount ) {
		$this->set( 'm_fltThresholdAmount', CStrings::strToFloatDef( $fltThresholdAmount, NULL, false, 2 ) );
	}

	public function getThresholdAmount() {
		return $this->m_fltThresholdAmount;
	}

	public function sqlThresholdAmount() {
		return ( true == isset( $this->m_fltThresholdAmount ) ) ? ( string ) $this->m_fltThresholdAmount : 'NULL';
	}

	public function setOverdueAmount( $fltOverdueAmount ) {
		$this->set( 'm_fltOverdueAmount', CStrings::strToFloatDef( $fltOverdueAmount, NULL, false, 2 ) );
	}

	public function getOverdueAmount() {
		return $this->m_fltOverdueAmount;
	}

	public function sqlOverdueAmount() {
		return ( true == isset( $this->m_fltOverdueAmount ) ) ? ( string ) $this->m_fltOverdueAmount : 'NULL';
	}

	public function setUserNote( $strUserNote ) {
		$this->set( 'm_strUserNote', CStrings::strTrimDef( $strUserNote, -1, NULL, true ) );
	}

	public function getUserNote() {
		return $this->m_strUserNote;
	}

	public function sqlUserNote() {
		return ( true == isset( $this->m_strUserNote ) ) ? '\'' . addslashes( $this->m_strUserNote ) . '\'' : 'NULL';
	}

	public function setSystemNote( $strSystemNote ) {
		$this->set( 'm_strSystemNote', CStrings::strTrimDef( $strSystemNote, -1, NULL, true ) );
	}

	public function getSystemNote() {
		return $this->m_strSystemNote;
	}

	public function sqlSystemNote() {
		return ( true == isset( $this->m_strSystemNote ) ) ? '\'' . addslashes( $this->m_strSystemNote ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, property_id, is_manual_update, property_delinquency_id, system_email_id, delinquency_level_type_id, eligible_delinquency_level_type_id, max_delinquency_level_type_id, invoice_ids, threshold_amount, overdue_amount, user_note, system_note, approved_by, approved_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlAccountId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlIsManualUpdate() . ', ' .
 						$this->sqlPropertyDelinquencyId() . ', ' .
 						$this->sqlSystemEmailId() . ', ' .
 						$this->sqlDelinquencyLevelTypeId() . ', ' .
 						$this->sqlEligibleDelinquencyLevelTypeId() . ', ' .
 						$this->sqlMaxDelinquencyLevelTypeId() . ', ' .
 						$this->sqlInvoiceIds() . ', ' .
 						$this->sqlThresholdAmount() . ', ' .
 						$this->sqlOverdueAmount() . ', ' .
 						$this->sqlUserNote() . ', ' .
 						$this->sqlSystemNote() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual_update = ' . $this->sqlIsManualUpdate() . ','; } elseif( true == array_key_exists( 'IsManualUpdate', $this->getChangedColumns() ) ) { $strSql .= ' is_manual_update = ' . $this->sqlIsManualUpdate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_delinquency_id = ' . $this->sqlPropertyDelinquencyId() . ','; } elseif( true == array_key_exists( 'PropertyDelinquencyId', $this->getChangedColumns() ) ) { $strSql .= ' property_delinquency_id = ' . $this->sqlPropertyDelinquencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; } elseif( true == array_key_exists( 'SystemEmailId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_level_type_id = ' . $this->sqlDelinquencyLevelTypeId() . ','; } elseif( true == array_key_exists( 'DelinquencyLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_level_type_id = ' . $this->sqlDelinquencyLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eligible_delinquency_level_type_id = ' . $this->sqlEligibleDelinquencyLevelTypeId() . ','; } elseif( true == array_key_exists( 'EligibleDelinquencyLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' eligible_delinquency_level_type_id = ' . $this->sqlEligibleDelinquencyLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_delinquency_level_type_id = ' . $this->sqlMaxDelinquencyLevelTypeId() . ','; } elseif( true == array_key_exists( 'MaxDelinquencyLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' max_delinquency_level_type_id = ' . $this->sqlMaxDelinquencyLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_ids = ' . $this->sqlInvoiceIds() . ','; } elseif( true == array_key_exists( 'InvoiceIds', $this->getChangedColumns() ) ) { $strSql .= ' invoice_ids = ' . $this->sqlInvoiceIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' threshold_amount = ' . $this->sqlThresholdAmount() . ','; } elseif( true == array_key_exists( 'ThresholdAmount', $this->getChangedColumns() ) ) { $strSql .= ' threshold_amount = ' . $this->sqlThresholdAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' overdue_amount = ' . $this->sqlOverdueAmount() . ','; } elseif( true == array_key_exists( 'OverdueAmount', $this->getChangedColumns() ) ) { $strSql .= ' overdue_amount = ' . $this->sqlOverdueAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_note = ' . $this->sqlUserNote() . ','; } elseif( true == array_key_exists( 'UserNote', $this->getChangedColumns() ) ) { $strSql .= ' user_note = ' . $this->sqlUserNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_note = ' . $this->sqlSystemNote() . ','; } elseif( true == array_key_exists( 'SystemNote', $this->getChangedColumns() ) ) { $strSql .= ' system_note = ' . $this->sqlSystemNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'property_id' => $this->getPropertyId(),
			'is_manual_update' => $this->getIsManualUpdate(),
			'property_delinquency_id' => $this->getPropertyDelinquencyId(),
			'system_email_id' => $this->getSystemEmailId(),
			'delinquency_level_type_id' => $this->getDelinquencyLevelTypeId(),
			'eligible_delinquency_level_type_id' => $this->getEligibleDelinquencyLevelTypeId(),
			'max_delinquency_level_type_id' => $this->getMaxDelinquencyLevelTypeId(),
			'invoice_ids' => $this->getInvoiceIds(),
			'threshold_amount' => $this->getThresholdAmount(),
			'overdue_amount' => $this->getOverdueAmount(),
			'user_note' => $this->getUserNote(),
			'system_note' => $this->getSystemNote(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>