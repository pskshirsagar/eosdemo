<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCounties
 * Do not add any new functions to this class.
 */

class CBaseCounties extends CEosPluralBase {

	/**
	 * @return CCounty[]
	 */
	public static function fetchCounties( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCounty', $objDatabase );
	}

	/**
	 * @return CCounty
	 */
	public static function fetchCounty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCounty', $objDatabase );
	}

	public static function fetchCountyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'counties', $objDatabase );
	}

	public static function fetchCountyById( $intId, $objDatabase ) {
		return self::fetchCounty( sprintf( 'SELECT * FROM counties WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>