<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskPriorities
 * Do not add any new functions to this class.
 */

class CBaseTaskPriorities extends CEosPluralBase {

	/**
	 * @return CTaskPriority[]
	 */
	public static function fetchTaskPriorities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTaskPriority::class, $objDatabase );
	}

	/**
	 * @return CTaskPriority
	 */
	public static function fetchTaskPriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaskPriority::class, $objDatabase );
	}

	public static function fetchTaskPriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_priorities', $objDatabase );
	}

	public static function fetchTaskPriorityById( $intId, $objDatabase ) {
		return self::fetchTaskPriority( sprintf( 'SELECT * FROM task_priorities WHERE id = %d', $intId ), $objDatabase );
	}

}
?>