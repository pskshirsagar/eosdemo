<?php

class CBaseContract extends CEosSingularBase {

	const TABLE_NAME = 'public.contracts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intContractTypeId;
	protected $m_strCurrencyCode;
	protected $m_intContractStatusTypeId;
	protected $m_intPsLeadId;
	protected $m_intPersonId;
	protected $m_intPsLeadOriginId;
	protected $m_intContractRejectTypeId;
	protected $m_intSalesEmployeeId;
	protected $m_intSalesDepartmentId;
	protected $m_intSalesManagerEmployeeId;
	protected $m_strContractDatetime;
	protected $m_strContractStartDate;
	protected $m_fltPercentCloseLikelihood;
	protected $m_strTitle;
	protected $m_strNotes;
	protected $m_strAnticipatedCloseDate;
	protected $m_strEnteredPipelineOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDuplicateContractId;
	protected $m_intEnteredPipelineActionId;
	protected $m_intContactedActionId;
	protected $m_intQualifiedActionId;
	protected $m_intDiscoveryActionId;
	protected $m_intProposalSentActionId;
	protected $m_intContractNegotiationActionId;
	protected $m_intContractSentActionId;
	protected $m_intContractApprovedActionId;
	protected $m_intContractTerminatedActionId;
	protected $m_intLeadLostActionId;
	protected $m_intLastActionId;
	protected $m_intOnHoldActionId;
	protected $m_strContactedDate;
	protected $m_strQualifiedDate;
	protected $m_strDiscoveryDate;
	protected $m_strProposalSentDate;
	protected $m_strContractNegotiationDate;
	protected $m_strContractSentDate;
	protected $m_strContractApprovedDate;
	protected $m_strContractTerminatedDate;
	protected $m_strLeadLostDate;
	protected $m_strLastActionDate;
	protected $m_strOnHoldDate;
	protected $m_strCommitMonth;
	protected $m_strMeetingDate;
	protected $m_intDemoActionId;
	protected $m_strDemoDate;
	protected $m_intHighestContractStatusTypeId;
	protected $m_intCachedNumberOfUnits;
	protected $m_strFirstCloseMonth;
	protected $m_strLastTerminationMonth;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';
		$this->m_intContractStatusTypeId = '1';
		$this->m_intHighestContractStatusTypeId = '1';
		$this->m_intCachedNumberOfUnits = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['contract_type_id'] ) && $boolDirectSet ) $this->set( 'm_intContractTypeId', trim( $arrValues['contract_type_id'] ) ); elseif( isset( $arrValues['contract_type_id'] ) ) $this->setContractTypeId( $arrValues['contract_type_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( $arrValues['currency_code'] ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( $arrValues['currency_code'] );
		if( isset( $arrValues['contract_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intContractStatusTypeId', trim( $arrValues['contract_status_type_id'] ) ); elseif( isset( $arrValues['contract_status_type_id'] ) ) $this->setContractStatusTypeId( $arrValues['contract_status_type_id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['person_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonId', trim( $arrValues['person_id'] ) ); elseif( isset( $arrValues['person_id'] ) ) $this->setPersonId( $arrValues['person_id'] );
		if( isset( $arrValues['ps_lead_origin_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadOriginId', trim( $arrValues['ps_lead_origin_id'] ) ); elseif( isset( $arrValues['ps_lead_origin_id'] ) ) $this->setPsLeadOriginId( $arrValues['ps_lead_origin_id'] );
		if( isset( $arrValues['contract_reject_type_id'] ) && $boolDirectSet ) $this->set( 'm_intContractRejectTypeId', trim( $arrValues['contract_reject_type_id'] ) ); elseif( isset( $arrValues['contract_reject_type_id'] ) ) $this->setContractRejectTypeId( $arrValues['contract_reject_type_id'] );
		if( isset( $arrValues['sales_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSalesEmployeeId', trim( $arrValues['sales_employee_id'] ) ); elseif( isset( $arrValues['sales_employee_id'] ) ) $this->setSalesEmployeeId( $arrValues['sales_employee_id'] );
		if( isset( $arrValues['sales_department_id'] ) && $boolDirectSet ) $this->set( 'm_intSalesDepartmentId', trim( $arrValues['sales_department_id'] ) ); elseif( isset( $arrValues['sales_department_id'] ) ) $this->setSalesDepartmentId( $arrValues['sales_department_id'] );
		if( isset( $arrValues['sales_manager_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSalesManagerEmployeeId', trim( $arrValues['sales_manager_employee_id'] ) ); elseif( isset( $arrValues['sales_manager_employee_id'] ) ) $this->setSalesManagerEmployeeId( $arrValues['sales_manager_employee_id'] );
		if( isset( $arrValues['contract_datetime'] ) && $boolDirectSet ) $this->set( 'm_strContractDatetime', trim( $arrValues['contract_datetime'] ) ); elseif( isset( $arrValues['contract_datetime'] ) ) $this->setContractDatetime( $arrValues['contract_datetime'] );
		if( isset( $arrValues['contract_start_date'] ) && $boolDirectSet ) $this->set( 'm_strContractStartDate', trim( $arrValues['contract_start_date'] ) ); elseif( isset( $arrValues['contract_start_date'] ) ) $this->setContractStartDate( $arrValues['contract_start_date'] );
		if( isset( $arrValues['percent_close_likelihood'] ) && $boolDirectSet ) $this->set( 'm_fltPercentCloseLikelihood', trim( $arrValues['percent_close_likelihood'] ) ); elseif( isset( $arrValues['percent_close_likelihood'] ) ) $this->setPercentCloseLikelihood( $arrValues['percent_close_likelihood'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['anticipated_close_date'] ) && $boolDirectSet ) $this->set( 'm_strAnticipatedCloseDate', trim( $arrValues['anticipated_close_date'] ) ); elseif( isset( $arrValues['anticipated_close_date'] ) ) $this->setAnticipatedCloseDate( $arrValues['anticipated_close_date'] );
		if( isset( $arrValues['entered_pipeline_on'] ) && $boolDirectSet ) $this->set( 'm_strEnteredPipelineOn', trim( $arrValues['entered_pipeline_on'] ) ); elseif( isset( $arrValues['entered_pipeline_on'] ) ) $this->setEnteredPipelineOn( $arrValues['entered_pipeline_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['duplicate_contract_id'] ) && $boolDirectSet ) $this->set( 'm_intDuplicateContractId', trim( $arrValues['duplicate_contract_id'] ) ); elseif( isset( $arrValues['duplicate_contract_id'] ) ) $this->setDuplicateContractId( $arrValues['duplicate_contract_id'] );
		if( isset( $arrValues['entered_pipeline_action_id'] ) && $boolDirectSet ) $this->set( 'm_intEnteredPipelineActionId', trim( $arrValues['entered_pipeline_action_id'] ) ); elseif( isset( $arrValues['entered_pipeline_action_id'] ) ) $this->setEnteredPipelineActionId( $arrValues['entered_pipeline_action_id'] );
		if( isset( $arrValues['contacted_action_id'] ) && $boolDirectSet ) $this->set( 'm_intContactedActionId', trim( $arrValues['contacted_action_id'] ) ); elseif( isset( $arrValues['contacted_action_id'] ) ) $this->setContactedActionId( $arrValues['contacted_action_id'] );
		if( isset( $arrValues['qualified_action_id'] ) && $boolDirectSet ) $this->set( 'm_intQualifiedActionId', trim( $arrValues['qualified_action_id'] ) ); elseif( isset( $arrValues['qualified_action_id'] ) ) $this->setQualifiedActionId( $arrValues['qualified_action_id'] );
		if( isset( $arrValues['discovery_action_id'] ) && $boolDirectSet ) $this->set( 'm_intDiscoveryActionId', trim( $arrValues['discovery_action_id'] ) ); elseif( isset( $arrValues['discovery_action_id'] ) ) $this->setDiscoveryActionId( $arrValues['discovery_action_id'] );
		if( isset( $arrValues['proposal_sent_action_id'] ) && $boolDirectSet ) $this->set( 'm_intProposalSentActionId', trim( $arrValues['proposal_sent_action_id'] ) ); elseif( isset( $arrValues['proposal_sent_action_id'] ) ) $this->setProposalSentActionId( $arrValues['proposal_sent_action_id'] );
		if( isset( $arrValues['contract_negotiation_action_id'] ) && $boolDirectSet ) $this->set( 'm_intContractNegotiationActionId', trim( $arrValues['contract_negotiation_action_id'] ) ); elseif( isset( $arrValues['contract_negotiation_action_id'] ) ) $this->setContractNegotiationActionId( $arrValues['contract_negotiation_action_id'] );
		if( isset( $arrValues['contract_sent_action_id'] ) && $boolDirectSet ) $this->set( 'm_intContractSentActionId', trim( $arrValues['contract_sent_action_id'] ) ); elseif( isset( $arrValues['contract_sent_action_id'] ) ) $this->setContractSentActionId( $arrValues['contract_sent_action_id'] );
		if( isset( $arrValues['contract_approved_action_id'] ) && $boolDirectSet ) $this->set( 'm_intContractApprovedActionId', trim( $arrValues['contract_approved_action_id'] ) ); elseif( isset( $arrValues['contract_approved_action_id'] ) ) $this->setContractApprovedActionId( $arrValues['contract_approved_action_id'] );
		if( isset( $arrValues['contract_terminated_action_id'] ) && $boolDirectSet ) $this->set( 'm_intContractTerminatedActionId', trim( $arrValues['contract_terminated_action_id'] ) ); elseif( isset( $arrValues['contract_terminated_action_id'] ) ) $this->setContractTerminatedActionId( $arrValues['contract_terminated_action_id'] );
		if( isset( $arrValues['lead_lost_action_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadLostActionId', trim( $arrValues['lead_lost_action_id'] ) ); elseif( isset( $arrValues['lead_lost_action_id'] ) ) $this->setLeadLostActionId( $arrValues['lead_lost_action_id'] );
		if( isset( $arrValues['last_action_id'] ) && $boolDirectSet ) $this->set( 'm_intLastActionId', trim( $arrValues['last_action_id'] ) ); elseif( isset( $arrValues['last_action_id'] ) ) $this->setLastActionId( $arrValues['last_action_id'] );
		if( isset( $arrValues['on_hold_action_id'] ) && $boolDirectSet ) $this->set( 'm_intOnHoldActionId', trim( $arrValues['on_hold_action_id'] ) ); elseif( isset( $arrValues['on_hold_action_id'] ) ) $this->setOnHoldActionId( $arrValues['on_hold_action_id'] );
		if( isset( $arrValues['contacted_date'] ) && $boolDirectSet ) $this->set( 'm_strContactedDate', trim( $arrValues['contacted_date'] ) ); elseif( isset( $arrValues['contacted_date'] ) ) $this->setContactedDate( $arrValues['contacted_date'] );
		if( isset( $arrValues['qualified_date'] ) && $boolDirectSet ) $this->set( 'm_strQualifiedDate', trim( $arrValues['qualified_date'] ) ); elseif( isset( $arrValues['qualified_date'] ) ) $this->setQualifiedDate( $arrValues['qualified_date'] );
		if( isset( $arrValues['discovery_date'] ) && $boolDirectSet ) $this->set( 'm_strDiscoveryDate', trim( $arrValues['discovery_date'] ) ); elseif( isset( $arrValues['discovery_date'] ) ) $this->setDiscoveryDate( $arrValues['discovery_date'] );
		if( isset( $arrValues['proposal_sent_date'] ) && $boolDirectSet ) $this->set( 'm_strProposalSentDate', trim( $arrValues['proposal_sent_date'] ) ); elseif( isset( $arrValues['proposal_sent_date'] ) ) $this->setProposalSentDate( $arrValues['proposal_sent_date'] );
		if( isset( $arrValues['contract_negotiation_date'] ) && $boolDirectSet ) $this->set( 'm_strContractNegotiationDate', trim( $arrValues['contract_negotiation_date'] ) ); elseif( isset( $arrValues['contract_negotiation_date'] ) ) $this->setContractNegotiationDate( $arrValues['contract_negotiation_date'] );
		if( isset( $arrValues['contract_sent_date'] ) && $boolDirectSet ) $this->set( 'm_strContractSentDate', trim( $arrValues['contract_sent_date'] ) ); elseif( isset( $arrValues['contract_sent_date'] ) ) $this->setContractSentDate( $arrValues['contract_sent_date'] );
		if( isset( $arrValues['contract_approved_date'] ) && $boolDirectSet ) $this->set( 'm_strContractApprovedDate', trim( $arrValues['contract_approved_date'] ) ); elseif( isset( $arrValues['contract_approved_date'] ) ) $this->setContractApprovedDate( $arrValues['contract_approved_date'] );
		if( isset( $arrValues['contract_terminated_date'] ) && $boolDirectSet ) $this->set( 'm_strContractTerminatedDate', trim( $arrValues['contract_terminated_date'] ) ); elseif( isset( $arrValues['contract_terminated_date'] ) ) $this->setContractTerminatedDate( $arrValues['contract_terminated_date'] );
		if( isset( $arrValues['lead_lost_date'] ) && $boolDirectSet ) $this->set( 'm_strLeadLostDate', trim( $arrValues['lead_lost_date'] ) ); elseif( isset( $arrValues['lead_lost_date'] ) ) $this->setLeadLostDate( $arrValues['lead_lost_date'] );
		if( isset( $arrValues['last_action_date'] ) && $boolDirectSet ) $this->set( 'm_strLastActionDate', trim( $arrValues['last_action_date'] ) ); elseif( isset( $arrValues['last_action_date'] ) ) $this->setLastActionDate( $arrValues['last_action_date'] );
		if( isset( $arrValues['on_hold_date'] ) && $boolDirectSet ) $this->set( 'm_strOnHoldDate', trim( $arrValues['on_hold_date'] ) ); elseif( isset( $arrValues['on_hold_date'] ) ) $this->setOnHoldDate( $arrValues['on_hold_date'] );
		if( isset( $arrValues['commit_month'] ) && $boolDirectSet ) $this->set( 'm_strCommitMonth', trim( $arrValues['commit_month'] ) ); elseif( isset( $arrValues['commit_month'] ) ) $this->setCommitMonth( $arrValues['commit_month'] );
		if( isset( $arrValues['meeting_date'] ) && $boolDirectSet ) $this->set( 'm_strMeetingDate', trim( $arrValues['meeting_date'] ) ); elseif( isset( $arrValues['meeting_date'] ) ) $this->setMeetingDate( $arrValues['meeting_date'] );
		if( isset( $arrValues['demo_action_id'] ) && $boolDirectSet ) $this->set( 'm_intDemoActionId', trim( $arrValues['demo_action_id'] ) ); elseif( isset( $arrValues['demo_action_id'] ) ) $this->setDemoActionId( $arrValues['demo_action_id'] );
		if( isset( $arrValues['demo_date'] ) && $boolDirectSet ) $this->set( 'm_strDemoDate', trim( $arrValues['demo_date'] ) ); elseif( isset( $arrValues['demo_date'] ) ) $this->setDemoDate( $arrValues['demo_date'] );
		if( isset( $arrValues['highest_contract_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intHighestContractStatusTypeId', trim( $arrValues['highest_contract_status_type_id'] ) ); elseif( isset( $arrValues['highest_contract_status_type_id'] ) ) $this->setHighestContractStatusTypeId( $arrValues['highest_contract_status_type_id'] );
		if( isset( $arrValues['cached_number_of_units'] ) && $boolDirectSet ) $this->set( 'm_intCachedNumberOfUnits', trim( $arrValues['cached_number_of_units'] ) ); elseif( isset( $arrValues['cached_number_of_units'] ) ) $this->setCachedNumberOfUnits( $arrValues['cached_number_of_units'] );
		if( isset( $arrValues['first_close_month'] ) && $boolDirectSet ) $this->set( 'm_strFirstCloseMonth', trim( $arrValues['first_close_month'] ) ); elseif( isset( $arrValues['first_close_month'] ) ) $this->setFirstCloseMonth( $arrValues['first_close_month'] );
		if( isset( $arrValues['last_termination_month'] ) && $boolDirectSet ) $this->set( 'm_strLastTerminationMonth', trim( $arrValues['last_termination_month'] ) ); elseif( isset( $arrValues['last_termination_month'] ) ) $this->setLastTerminationMonth( $arrValues['last_termination_month'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setContractTypeId( $intContractTypeId ) {
		$this->set( 'm_intContractTypeId', CStrings::strToIntDef( $intContractTypeId, NULL, false ) );
	}

	public function getContractTypeId() {
		return $this->m_intContractTypeId;
	}

	public function sqlContractTypeId() {
		return ( true == isset( $this->m_intContractTypeId ) ) ? ( string ) $this->m_intContractTypeId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrencyCode ) : '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' ) : '\'USD\'';
	}

	public function setContractStatusTypeId( $intContractStatusTypeId ) {
		$this->set( 'm_intContractStatusTypeId', CStrings::strToIntDef( $intContractStatusTypeId, NULL, false ) );
	}

	public function getContractStatusTypeId() {
		return $this->m_intContractStatusTypeId;
	}

	public function sqlContractStatusTypeId() {
		return ( true == isset( $this->m_intContractStatusTypeId ) ) ? ( string ) $this->m_intContractStatusTypeId : '1';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setPersonId( $intPersonId ) {
		$this->set( 'm_intPersonId', CStrings::strToIntDef( $intPersonId, NULL, false ) );
	}

	public function getPersonId() {
		return $this->m_intPersonId;
	}

	public function sqlPersonId() {
		return ( true == isset( $this->m_intPersonId ) ) ? ( string ) $this->m_intPersonId : 'NULL';
	}

	public function setPsLeadOriginId( $intPsLeadOriginId ) {
		$this->set( 'm_intPsLeadOriginId', CStrings::strToIntDef( $intPsLeadOriginId, NULL, false ) );
	}

	public function getPsLeadOriginId() {
		return $this->m_intPsLeadOriginId;
	}

	public function sqlPsLeadOriginId() {
		return ( true == isset( $this->m_intPsLeadOriginId ) ) ? ( string ) $this->m_intPsLeadOriginId : 'NULL';
	}

	public function setContractRejectTypeId( $intContractRejectTypeId ) {
		$this->set( 'm_intContractRejectTypeId', CStrings::strToIntDef( $intContractRejectTypeId, NULL, false ) );
	}

	public function getContractRejectTypeId() {
		return $this->m_intContractRejectTypeId;
	}

	public function sqlContractRejectTypeId() {
		return ( true == isset( $this->m_intContractRejectTypeId ) ) ? ( string ) $this->m_intContractRejectTypeId : 'NULL';
	}

	public function setSalesEmployeeId( $intSalesEmployeeId ) {
		$this->set( 'm_intSalesEmployeeId', CStrings::strToIntDef( $intSalesEmployeeId, NULL, false ) );
	}

	public function getSalesEmployeeId() {
		return $this->m_intSalesEmployeeId;
	}

	public function sqlSalesEmployeeId() {
		return ( true == isset( $this->m_intSalesEmployeeId ) ) ? ( string ) $this->m_intSalesEmployeeId : 'NULL';
	}

	public function setSalesDepartmentId( $intSalesDepartmentId ) {
		$this->set( 'm_intSalesDepartmentId', CStrings::strToIntDef( $intSalesDepartmentId, NULL, false ) );
	}

	public function getSalesDepartmentId() {
		return $this->m_intSalesDepartmentId;
	}

	public function sqlSalesDepartmentId() {
		return ( true == isset( $this->m_intSalesDepartmentId ) ) ? ( string ) $this->m_intSalesDepartmentId : 'NULL';
	}

	public function setSalesManagerEmployeeId( $intSalesManagerEmployeeId ) {
		$this->set( 'm_intSalesManagerEmployeeId', CStrings::strToIntDef( $intSalesManagerEmployeeId, NULL, false ) );
	}

	public function getSalesManagerEmployeeId() {
		return $this->m_intSalesManagerEmployeeId;
	}

	public function sqlSalesManagerEmployeeId() {
		return ( true == isset( $this->m_intSalesManagerEmployeeId ) ) ? ( string ) $this->m_intSalesManagerEmployeeId : 'NULL';
	}

	public function setContractDatetime( $strContractDatetime ) {
		$this->set( 'm_strContractDatetime', CStrings::strTrimDef( $strContractDatetime, -1, NULL, true ) );
	}

	public function getContractDatetime() {
		return $this->m_strContractDatetime;
	}

	public function sqlContractDatetime() {
		return ( true == isset( $this->m_strContractDatetime ) ) ? '\'' . $this->m_strContractDatetime . '\'' : 'NOW()';
	}

	public function setContractStartDate( $strContractStartDate ) {
		$this->set( 'm_strContractStartDate', CStrings::strTrimDef( $strContractStartDate, -1, NULL, true ) );
	}

	public function getContractStartDate() {
		return $this->m_strContractStartDate;
	}

	public function sqlContractStartDate() {
		return ( true == isset( $this->m_strContractStartDate ) ) ? '\'' . $this->m_strContractStartDate . '\'' : 'NULL';
	}

	public function setPercentCloseLikelihood( $fltPercentCloseLikelihood ) {
		$this->set( 'm_fltPercentCloseLikelihood', CStrings::strToFloatDef( $fltPercentCloseLikelihood, NULL, false, 6 ) );
	}

	public function getPercentCloseLikelihood() {
		return $this->m_fltPercentCloseLikelihood;
	}

	public function sqlPercentCloseLikelihood() {
		return ( true == isset( $this->m_fltPercentCloseLikelihood ) ) ? ( string ) $this->m_fltPercentCloseLikelihood : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 250, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 240, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setAnticipatedCloseDate( $strAnticipatedCloseDate ) {
		$this->set( 'm_strAnticipatedCloseDate', CStrings::strTrimDef( $strAnticipatedCloseDate, -1, NULL, true ) );
	}

	public function getAnticipatedCloseDate() {
		return $this->m_strAnticipatedCloseDate;
	}

	public function sqlAnticipatedCloseDate() {
		return ( true == isset( $this->m_strAnticipatedCloseDate ) ) ? '\'' . $this->m_strAnticipatedCloseDate . '\'' : 'NULL';
	}

	public function setEnteredPipelineOn( $strEnteredPipelineOn ) {
		$this->set( 'm_strEnteredPipelineOn', CStrings::strTrimDef( $strEnteredPipelineOn, -1, NULL, true ) );
	}

	public function getEnteredPipelineOn() {
		return $this->m_strEnteredPipelineOn;
	}

	public function sqlEnteredPipelineOn() {
		return ( true == isset( $this->m_strEnteredPipelineOn ) ) ? '\'' . $this->m_strEnteredPipelineOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDuplicateContractId( $intDuplicateContractId ) {
		$this->set( 'm_intDuplicateContractId', CStrings::strToIntDef( $intDuplicateContractId, NULL, false ) );
	}

	public function getDuplicateContractId() {
		return $this->m_intDuplicateContractId;
	}

	public function sqlDuplicateContractId() {
		return ( true == isset( $this->m_intDuplicateContractId ) ) ? ( string ) $this->m_intDuplicateContractId : 'NULL';
	}

	public function setEnteredPipelineActionId( $intEnteredPipelineActionId ) {
		$this->set( 'm_intEnteredPipelineActionId', CStrings::strToIntDef( $intEnteredPipelineActionId, NULL, false ) );
	}

	public function getEnteredPipelineActionId() {
		return $this->m_intEnteredPipelineActionId;
	}

	public function sqlEnteredPipelineActionId() {
		return ( true == isset( $this->m_intEnteredPipelineActionId ) ) ? ( string ) $this->m_intEnteredPipelineActionId : 'NULL';
	}

	public function setContactedActionId( $intContactedActionId ) {
		$this->set( 'm_intContactedActionId', CStrings::strToIntDef( $intContactedActionId, NULL, false ) );
	}

	public function getContactedActionId() {
		return $this->m_intContactedActionId;
	}

	public function sqlContactedActionId() {
		return ( true == isset( $this->m_intContactedActionId ) ) ? ( string ) $this->m_intContactedActionId : 'NULL';
	}

	public function setQualifiedActionId( $intQualifiedActionId ) {
		$this->set( 'm_intQualifiedActionId', CStrings::strToIntDef( $intQualifiedActionId, NULL, false ) );
	}

	public function getQualifiedActionId() {
		return $this->m_intQualifiedActionId;
	}

	public function sqlQualifiedActionId() {
		return ( true == isset( $this->m_intQualifiedActionId ) ) ? ( string ) $this->m_intQualifiedActionId : 'NULL';
	}

	public function setDiscoveryActionId( $intDiscoveryActionId ) {
		$this->set( 'm_intDiscoveryActionId', CStrings::strToIntDef( $intDiscoveryActionId, NULL, false ) );
	}

	public function getDiscoveryActionId() {
		return $this->m_intDiscoveryActionId;
	}

	public function sqlDiscoveryActionId() {
		return ( true == isset( $this->m_intDiscoveryActionId ) ) ? ( string ) $this->m_intDiscoveryActionId : 'NULL';
	}

	public function setProposalSentActionId( $intProposalSentActionId ) {
		$this->set( 'm_intProposalSentActionId', CStrings::strToIntDef( $intProposalSentActionId, NULL, false ) );
	}

	public function getProposalSentActionId() {
		return $this->m_intProposalSentActionId;
	}

	public function sqlProposalSentActionId() {
		return ( true == isset( $this->m_intProposalSentActionId ) ) ? ( string ) $this->m_intProposalSentActionId : 'NULL';
	}

	public function setContractNegotiationActionId( $intContractNegotiationActionId ) {
		$this->set( 'm_intContractNegotiationActionId', CStrings::strToIntDef( $intContractNegotiationActionId, NULL, false ) );
	}

	public function getContractNegotiationActionId() {
		return $this->m_intContractNegotiationActionId;
	}

	public function sqlContractNegotiationActionId() {
		return ( true == isset( $this->m_intContractNegotiationActionId ) ) ? ( string ) $this->m_intContractNegotiationActionId : 'NULL';
	}

	public function setContractSentActionId( $intContractSentActionId ) {
		$this->set( 'm_intContractSentActionId', CStrings::strToIntDef( $intContractSentActionId, NULL, false ) );
	}

	public function getContractSentActionId() {
		return $this->m_intContractSentActionId;
	}

	public function sqlContractSentActionId() {
		return ( true == isset( $this->m_intContractSentActionId ) ) ? ( string ) $this->m_intContractSentActionId : 'NULL';
	}

	public function setContractApprovedActionId( $intContractApprovedActionId ) {
		$this->set( 'm_intContractApprovedActionId', CStrings::strToIntDef( $intContractApprovedActionId, NULL, false ) );
	}

	public function getContractApprovedActionId() {
		return $this->m_intContractApprovedActionId;
	}

	public function sqlContractApprovedActionId() {
		return ( true == isset( $this->m_intContractApprovedActionId ) ) ? ( string ) $this->m_intContractApprovedActionId : 'NULL';
	}

	public function setContractTerminatedActionId( $intContractTerminatedActionId ) {
		$this->set( 'm_intContractTerminatedActionId', CStrings::strToIntDef( $intContractTerminatedActionId, NULL, false ) );
	}

	public function getContractTerminatedActionId() {
		return $this->m_intContractTerminatedActionId;
	}

	public function sqlContractTerminatedActionId() {
		return ( true == isset( $this->m_intContractTerminatedActionId ) ) ? ( string ) $this->m_intContractTerminatedActionId : 'NULL';
	}

	public function setLeadLostActionId( $intLeadLostActionId ) {
		$this->set( 'm_intLeadLostActionId', CStrings::strToIntDef( $intLeadLostActionId, NULL, false ) );
	}

	public function getLeadLostActionId() {
		return $this->m_intLeadLostActionId;
	}

	public function sqlLeadLostActionId() {
		return ( true == isset( $this->m_intLeadLostActionId ) ) ? ( string ) $this->m_intLeadLostActionId : 'NULL';
	}

	public function setLastActionId( $intLastActionId ) {
		$this->set( 'm_intLastActionId', CStrings::strToIntDef( $intLastActionId, NULL, false ) );
	}

	public function getLastActionId() {
		return $this->m_intLastActionId;
	}

	public function sqlLastActionId() {
		return ( true == isset( $this->m_intLastActionId ) ) ? ( string ) $this->m_intLastActionId : 'NULL';
	}

	public function setOnHoldActionId( $intOnHoldActionId ) {
		$this->set( 'm_intOnHoldActionId', CStrings::strToIntDef( $intOnHoldActionId, NULL, false ) );
	}

	public function getOnHoldActionId() {
		return $this->m_intOnHoldActionId;
	}

	public function sqlOnHoldActionId() {
		return ( true == isset( $this->m_intOnHoldActionId ) ) ? ( string ) $this->m_intOnHoldActionId : 'NULL';
	}

	public function setContactedDate( $strContactedDate ) {
		$this->set( 'm_strContactedDate', CStrings::strTrimDef( $strContactedDate, -1, NULL, true ) );
	}

	public function getContactedDate() {
		return $this->m_strContactedDate;
	}

	public function sqlContactedDate() {
		return ( true == isset( $this->m_strContactedDate ) ) ? '\'' . $this->m_strContactedDate . '\'' : 'NULL';
	}

	public function setQualifiedDate( $strQualifiedDate ) {
		$this->set( 'm_strQualifiedDate', CStrings::strTrimDef( $strQualifiedDate, -1, NULL, true ) );
	}

	public function getQualifiedDate() {
		return $this->m_strQualifiedDate;
	}

	public function sqlQualifiedDate() {
		return ( true == isset( $this->m_strQualifiedDate ) ) ? '\'' . $this->m_strQualifiedDate . '\'' : 'NULL';
	}

	public function setDiscoveryDate( $strDiscoveryDate ) {
		$this->set( 'm_strDiscoveryDate', CStrings::strTrimDef( $strDiscoveryDate, -1, NULL, true ) );
	}

	public function getDiscoveryDate() {
		return $this->m_strDiscoveryDate;
	}

	public function sqlDiscoveryDate() {
		return ( true == isset( $this->m_strDiscoveryDate ) ) ? '\'' . $this->m_strDiscoveryDate . '\'' : 'NULL';
	}

	public function setProposalSentDate( $strProposalSentDate ) {
		$this->set( 'm_strProposalSentDate', CStrings::strTrimDef( $strProposalSentDate, -1, NULL, true ) );
	}

	public function getProposalSentDate() {
		return $this->m_strProposalSentDate;
	}

	public function sqlProposalSentDate() {
		return ( true == isset( $this->m_strProposalSentDate ) ) ? '\'' . $this->m_strProposalSentDate . '\'' : 'NULL';
	}

	public function setContractNegotiationDate( $strContractNegotiationDate ) {
		$this->set( 'm_strContractNegotiationDate', CStrings::strTrimDef( $strContractNegotiationDate, -1, NULL, true ) );
	}

	public function getContractNegotiationDate() {
		return $this->m_strContractNegotiationDate;
	}

	public function sqlContractNegotiationDate() {
		return ( true == isset( $this->m_strContractNegotiationDate ) ) ? '\'' . $this->m_strContractNegotiationDate . '\'' : 'NULL';
	}

	public function setContractSentDate( $strContractSentDate ) {
		$this->set( 'm_strContractSentDate', CStrings::strTrimDef( $strContractSentDate, -1, NULL, true ) );
	}

	public function getContractSentDate() {
		return $this->m_strContractSentDate;
	}

	public function sqlContractSentDate() {
		return ( true == isset( $this->m_strContractSentDate ) ) ? '\'' . $this->m_strContractSentDate . '\'' : 'NULL';
	}

	public function setContractApprovedDate( $strContractApprovedDate ) {
		$this->set( 'm_strContractApprovedDate', CStrings::strTrimDef( $strContractApprovedDate, -1, NULL, true ) );
	}

	public function getContractApprovedDate() {
		return $this->m_strContractApprovedDate;
	}

	public function sqlContractApprovedDate() {
		return ( true == isset( $this->m_strContractApprovedDate ) ) ? '\'' . $this->m_strContractApprovedDate . '\'' : 'NULL';
	}

	public function setContractTerminatedDate( $strContractTerminatedDate ) {
		$this->set( 'm_strContractTerminatedDate', CStrings::strTrimDef( $strContractTerminatedDate, -1, NULL, true ) );
	}

	public function getContractTerminatedDate() {
		return $this->m_strContractTerminatedDate;
	}

	public function sqlContractTerminatedDate() {
		return ( true == isset( $this->m_strContractTerminatedDate ) ) ? '\'' . $this->m_strContractTerminatedDate . '\'' : 'NULL';
	}

	public function setLeadLostDate( $strLeadLostDate ) {
		$this->set( 'm_strLeadLostDate', CStrings::strTrimDef( $strLeadLostDate, -1, NULL, true ) );
	}

	public function getLeadLostDate() {
		return $this->m_strLeadLostDate;
	}

	public function sqlLeadLostDate() {
		return ( true == isset( $this->m_strLeadLostDate ) ) ? '\'' . $this->m_strLeadLostDate . '\'' : 'NULL';
	}

	public function setLastActionDate( $strLastActionDate ) {
		$this->set( 'm_strLastActionDate', CStrings::strTrimDef( $strLastActionDate, -1, NULL, true ) );
	}

	public function getLastActionDate() {
		return $this->m_strLastActionDate;
	}

	public function sqlLastActionDate() {
		return ( true == isset( $this->m_strLastActionDate ) ) ? '\'' . $this->m_strLastActionDate . '\'' : 'NULL';
	}

	public function setOnHoldDate( $strOnHoldDate ) {
		$this->set( 'm_strOnHoldDate', CStrings::strTrimDef( $strOnHoldDate, -1, NULL, true ) );
	}

	public function getOnHoldDate() {
		return $this->m_strOnHoldDate;
	}

	public function sqlOnHoldDate() {
		return ( true == isset( $this->m_strOnHoldDate ) ) ? '\'' . $this->m_strOnHoldDate . '\'' : 'NULL';
	}

	public function setCommitMonth( $strCommitMonth ) {
		$this->set( 'm_strCommitMonth', CStrings::strTrimDef( $strCommitMonth, -1, NULL, true ) );
	}

	public function getCommitMonth() {
		return $this->m_strCommitMonth;
	}

	public function sqlCommitMonth() {
		return ( true == isset( $this->m_strCommitMonth ) ) ? '\'' . $this->m_strCommitMonth . '\'' : 'NULL';
	}

	public function setMeetingDate( $strMeetingDate ) {
		$this->set( 'm_strMeetingDate', CStrings::strTrimDef( $strMeetingDate, -1, NULL, true ) );
	}

	public function getMeetingDate() {
		return $this->m_strMeetingDate;
	}

	public function sqlMeetingDate() {
		return ( true == isset( $this->m_strMeetingDate ) ) ? '\'' . $this->m_strMeetingDate . '\'' : 'NULL';
	}

	public function setDemoActionId( $intDemoActionId ) {
		$this->set( 'm_intDemoActionId', CStrings::strToIntDef( $intDemoActionId, NULL, false ) );
	}

	public function getDemoActionId() {
		return $this->m_intDemoActionId;
	}

	public function sqlDemoActionId() {
		return ( true == isset( $this->m_intDemoActionId ) ) ? ( string ) $this->m_intDemoActionId : 'NULL';
	}

	public function setDemoDate( $strDemoDate ) {
		$this->set( 'm_strDemoDate', CStrings::strTrimDef( $strDemoDate, -1, NULL, true ) );
	}

	public function getDemoDate() {
		return $this->m_strDemoDate;
	}

	public function sqlDemoDate() {
		return ( true == isset( $this->m_strDemoDate ) ) ? '\'' . $this->m_strDemoDate . '\'' : 'NULL';
	}

	public function setHighestContractStatusTypeId( $intHighestContractStatusTypeId ) {
		$this->set( 'm_intHighestContractStatusTypeId', CStrings::strToIntDef( $intHighestContractStatusTypeId, NULL, false ) );
	}

	public function getHighestContractStatusTypeId() {
		return $this->m_intHighestContractStatusTypeId;
	}

	public function sqlHighestContractStatusTypeId() {
		return ( true == isset( $this->m_intHighestContractStatusTypeId ) ) ? ( string ) $this->m_intHighestContractStatusTypeId : '1';
	}

	public function setCachedNumberOfUnits( $intCachedNumberOfUnits ) {
		$this->set( 'm_intCachedNumberOfUnits', CStrings::strToIntDef( $intCachedNumberOfUnits, NULL, false ) );
	}

	public function getCachedNumberOfUnits() {
		return $this->m_intCachedNumberOfUnits;
	}

	public function sqlCachedNumberOfUnits() {
		return ( true == isset( $this->m_intCachedNumberOfUnits ) ) ? ( string ) $this->m_intCachedNumberOfUnits : '0';
	}

	public function setFirstCloseMonth( $strFirstCloseMonth ) {
		$this->set( 'm_strFirstCloseMonth', CStrings::strTrimDef( $strFirstCloseMonth, -1, NULL, true ) );
	}

	public function getFirstCloseMonth() {
		return $this->m_strFirstCloseMonth;
	}

	public function sqlFirstCloseMonth() {
		return ( true == isset( $this->m_strFirstCloseMonth ) ) ? '\'' . $this->m_strFirstCloseMonth . '\'' : 'NULL';
	}

	public function setLastTerminationMonth( $strLastTerminationMonth ) {
		$this->set( 'm_strLastTerminationMonth', CStrings::strTrimDef( $strLastTerminationMonth, -1, NULL, true ) );
	}

	public function getLastTerminationMonth() {
		return $this->m_strLastTerminationMonth;
	}

	public function sqlLastTerminationMonth() {
		return ( true == isset( $this->m_strLastTerminationMonth ) ) ? '\'' . $this->m_strLastTerminationMonth . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, contract_type_id, currency_code, contract_status_type_id, ps_lead_id, person_id, ps_lead_origin_id, contract_reject_type_id, sales_employee_id, sales_department_id, sales_manager_employee_id, contract_datetime, contract_start_date, percent_close_likelihood, title, notes, anticipated_close_date, entered_pipeline_on, updated_by, updated_on, created_by, created_on, duplicate_contract_id, entered_pipeline_action_id, contacted_action_id, qualified_action_id, discovery_action_id, proposal_sent_action_id, contract_negotiation_action_id, contract_sent_action_id, contract_approved_action_id, contract_terminated_action_id, lead_lost_action_id, last_action_id, on_hold_action_id, contacted_date, qualified_date, discovery_date, proposal_sent_date, contract_negotiation_date, contract_sent_date, contract_approved_date, contract_terminated_date, lead_lost_date, last_action_date, on_hold_date, commit_month, meeting_date, demo_action_id, demo_date, highest_contract_status_type_id, cached_number_of_units, first_close_month, last_termination_month )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlContractTypeId() . ', ' .
						$this->sqlCurrencyCode() . ', ' .
						$this->sqlContractStatusTypeId() . ', ' .
						$this->sqlPsLeadId() . ', ' .
						$this->sqlPersonId() . ', ' .
						$this->sqlPsLeadOriginId() . ', ' .
						$this->sqlContractRejectTypeId() . ', ' .
						$this->sqlSalesEmployeeId() . ', ' .
						$this->sqlSalesDepartmentId() . ', ' .
						$this->sqlSalesManagerEmployeeId() . ', ' .
						$this->sqlContractDatetime() . ', ' .
						$this->sqlContractStartDate() . ', ' .
						$this->sqlPercentCloseLikelihood() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlAnticipatedCloseDate() . ', ' .
						$this->sqlEnteredPipelineOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDuplicateContractId() . ', ' .
						$this->sqlEnteredPipelineActionId() . ', ' .
						$this->sqlContactedActionId() . ', ' .
						$this->sqlQualifiedActionId() . ', ' .
						$this->sqlDiscoveryActionId() . ', ' .
						$this->sqlProposalSentActionId() . ', ' .
						$this->sqlContractNegotiationActionId() . ', ' .
						$this->sqlContractSentActionId() . ', ' .
						$this->sqlContractApprovedActionId() . ', ' .
						$this->sqlContractTerminatedActionId() . ', ' .
						$this->sqlLeadLostActionId() . ', ' .
						$this->sqlLastActionId() . ', ' .
						$this->sqlOnHoldActionId() . ', ' .
						$this->sqlContactedDate() . ', ' .
						$this->sqlQualifiedDate() . ', ' .
						$this->sqlDiscoveryDate() . ', ' .
						$this->sqlProposalSentDate() . ', ' .
						$this->sqlContractNegotiationDate() . ', ' .
						$this->sqlContractSentDate() . ', ' .
						$this->sqlContractApprovedDate() . ', ' .
						$this->sqlContractTerminatedDate() . ', ' .
						$this->sqlLeadLostDate() . ', ' .
						$this->sqlLastActionDate() . ', ' .
						$this->sqlOnHoldDate() . ', ' .
						$this->sqlCommitMonth() . ', ' .
						$this->sqlMeetingDate() . ', ' .
						$this->sqlDemoActionId() . ', ' .
						$this->sqlDemoDate() . ', ' .
						$this->sqlHighestContractStatusTypeId() . ', ' .
						$this->sqlCachedNumberOfUnits() . ', ' .
						$this->sqlFirstCloseMonth() . ', ' .
						$this->sqlLastTerminationMonth() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_type_id = ' . $this->sqlContractTypeId(). ',' ; } elseif( true == array_key_exists( 'ContractTypeId', $this->getChangedColumns() ) ) { $strSql .= ' contract_type_id = ' . $this->sqlContractTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_status_type_id = ' . $this->sqlContractStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ContractStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' contract_status_type_id = ' . $this->sqlContractStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' person_id = ' . $this->sqlPersonId(). ',' ; } elseif( true == array_key_exists( 'PersonId', $this->getChangedColumns() ) ) { $strSql .= ' person_id = ' . $this->sqlPersonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_origin_id = ' . $this->sqlPsLeadOriginId(). ',' ; } elseif( true == array_key_exists( 'PsLeadOriginId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_origin_id = ' . $this->sqlPsLeadOriginId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_reject_type_id = ' . $this->sqlContractRejectTypeId(). ',' ; } elseif( true == array_key_exists( 'ContractRejectTypeId', $this->getChangedColumns() ) ) { $strSql .= ' contract_reject_type_id = ' . $this->sqlContractRejectTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_employee_id = ' . $this->sqlSalesEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SalesEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sales_employee_id = ' . $this->sqlSalesEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_department_id = ' . $this->sqlSalesDepartmentId(). ',' ; } elseif( true == array_key_exists( 'SalesDepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' sales_department_id = ' . $this->sqlSalesDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_manager_employee_id = ' . $this->sqlSalesManagerEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SalesManagerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sales_manager_employee_id = ' . $this->sqlSalesManagerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_datetime = ' . $this->sqlContractDatetime(). ',' ; } elseif( true == array_key_exists( 'ContractDatetime', $this->getChangedColumns() ) ) { $strSql .= ' contract_datetime = ' . $this->sqlContractDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_start_date = ' . $this->sqlContractStartDate(). ',' ; } elseif( true == array_key_exists( 'ContractStartDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_start_date = ' . $this->sqlContractStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percent_close_likelihood = ' . $this->sqlPercentCloseLikelihood(). ',' ; } elseif( true == array_key_exists( 'PercentCloseLikelihood', $this->getChangedColumns() ) ) { $strSql .= ' percent_close_likelihood = ' . $this->sqlPercentCloseLikelihood() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anticipated_close_date = ' . $this->sqlAnticipatedCloseDate(). ',' ; } elseif( true == array_key_exists( 'AnticipatedCloseDate', $this->getChangedColumns() ) ) { $strSql .= ' anticipated_close_date = ' . $this->sqlAnticipatedCloseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entered_pipeline_on = ' . $this->sqlEnteredPipelineOn(). ',' ; } elseif( true == array_key_exists( 'EnteredPipelineOn', $this->getChangedColumns() ) ) { $strSql .= ' entered_pipeline_on = ' . $this->sqlEnteredPipelineOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' duplicate_contract_id = ' . $this->sqlDuplicateContractId(). ',' ; } elseif( true == array_key_exists( 'DuplicateContractId', $this->getChangedColumns() ) ) { $strSql .= ' duplicate_contract_id = ' . $this->sqlDuplicateContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entered_pipeline_action_id = ' . $this->sqlEnteredPipelineActionId(). ',' ; } elseif( true == array_key_exists( 'EnteredPipelineActionId', $this->getChangedColumns() ) ) { $strSql .= ' entered_pipeline_action_id = ' . $this->sqlEnteredPipelineActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contacted_action_id = ' . $this->sqlContactedActionId(). ',' ; } elseif( true == array_key_exists( 'ContactedActionId', $this->getChangedColumns() ) ) { $strSql .= ' contacted_action_id = ' . $this->sqlContactedActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qualified_action_id = ' . $this->sqlQualifiedActionId(). ',' ; } elseif( true == array_key_exists( 'QualifiedActionId', $this->getChangedColumns() ) ) { $strSql .= ' qualified_action_id = ' . $this->sqlQualifiedActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discovery_action_id = ' . $this->sqlDiscoveryActionId(). ',' ; } elseif( true == array_key_exists( 'DiscoveryActionId', $this->getChangedColumns() ) ) { $strSql .= ' discovery_action_id = ' . $this->sqlDiscoveryActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposal_sent_action_id = ' . $this->sqlProposalSentActionId(). ',' ; } elseif( true == array_key_exists( 'ProposalSentActionId', $this->getChangedColumns() ) ) { $strSql .= ' proposal_sent_action_id = ' . $this->sqlProposalSentActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_negotiation_action_id = ' . $this->sqlContractNegotiationActionId(). ',' ; } elseif( true == array_key_exists( 'ContractNegotiationActionId', $this->getChangedColumns() ) ) { $strSql .= ' contract_negotiation_action_id = ' . $this->sqlContractNegotiationActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_sent_action_id = ' . $this->sqlContractSentActionId(). ',' ; } elseif( true == array_key_exists( 'ContractSentActionId', $this->getChangedColumns() ) ) { $strSql .= ' contract_sent_action_id = ' . $this->sqlContractSentActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_approved_action_id = ' . $this->sqlContractApprovedActionId(). ',' ; } elseif( true == array_key_exists( 'ContractApprovedActionId', $this->getChangedColumns() ) ) { $strSql .= ' contract_approved_action_id = ' . $this->sqlContractApprovedActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_terminated_action_id = ' . $this->sqlContractTerminatedActionId(). ',' ; } elseif( true == array_key_exists( 'ContractTerminatedActionId', $this->getChangedColumns() ) ) { $strSql .= ' contract_terminated_action_id = ' . $this->sqlContractTerminatedActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_lost_action_id = ' . $this->sqlLeadLostActionId(). ',' ; } elseif( true == array_key_exists( 'LeadLostActionId', $this->getChangedColumns() ) ) { $strSql .= ' lead_lost_action_id = ' . $this->sqlLeadLostActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_action_id = ' . $this->sqlLastActionId(). ',' ; } elseif( true == array_key_exists( 'LastActionId', $this->getChangedColumns() ) ) { $strSql .= ' last_action_id = ' . $this->sqlLastActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' on_hold_action_id = ' . $this->sqlOnHoldActionId(). ',' ; } elseif( true == array_key_exists( 'OnHoldActionId', $this->getChangedColumns() ) ) { $strSql .= ' on_hold_action_id = ' . $this->sqlOnHoldActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contacted_date = ' . $this->sqlContactedDate(). ',' ; } elseif( true == array_key_exists( 'ContactedDate', $this->getChangedColumns() ) ) { $strSql .= ' contacted_date = ' . $this->sqlContactedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qualified_date = ' . $this->sqlQualifiedDate(). ',' ; } elseif( true == array_key_exists( 'QualifiedDate', $this->getChangedColumns() ) ) { $strSql .= ' qualified_date = ' . $this->sqlQualifiedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' discovery_date = ' . $this->sqlDiscoveryDate(). ',' ; } elseif( true == array_key_exists( 'DiscoveryDate', $this->getChangedColumns() ) ) { $strSql .= ' discovery_date = ' . $this->sqlDiscoveryDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposal_sent_date = ' . $this->sqlProposalSentDate(). ',' ; } elseif( true == array_key_exists( 'ProposalSentDate', $this->getChangedColumns() ) ) { $strSql .= ' proposal_sent_date = ' . $this->sqlProposalSentDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_negotiation_date = ' . $this->sqlContractNegotiationDate(). ',' ; } elseif( true == array_key_exists( 'ContractNegotiationDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_negotiation_date = ' . $this->sqlContractNegotiationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_sent_date = ' . $this->sqlContractSentDate(). ',' ; } elseif( true == array_key_exists( 'ContractSentDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_sent_date = ' . $this->sqlContractSentDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_approved_date = ' . $this->sqlContractApprovedDate(). ',' ; } elseif( true == array_key_exists( 'ContractApprovedDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_approved_date = ' . $this->sqlContractApprovedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_terminated_date = ' . $this->sqlContractTerminatedDate(). ',' ; } elseif( true == array_key_exists( 'ContractTerminatedDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_terminated_date = ' . $this->sqlContractTerminatedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_lost_date = ' . $this->sqlLeadLostDate(). ',' ; } elseif( true == array_key_exists( 'LeadLostDate', $this->getChangedColumns() ) ) { $strSql .= ' lead_lost_date = ' . $this->sqlLeadLostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_action_date = ' . $this->sqlLastActionDate(). ',' ; } elseif( true == array_key_exists( 'LastActionDate', $this->getChangedColumns() ) ) { $strSql .= ' last_action_date = ' . $this->sqlLastActionDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' on_hold_date = ' . $this->sqlOnHoldDate(). ',' ; } elseif( true == array_key_exists( 'OnHoldDate', $this->getChangedColumns() ) ) { $strSql .= ' on_hold_date = ' . $this->sqlOnHoldDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commit_month = ' . $this->sqlCommitMonth(). ',' ; } elseif( true == array_key_exists( 'CommitMonth', $this->getChangedColumns() ) ) { $strSql .= ' commit_month = ' . $this->sqlCommitMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meeting_date = ' . $this->sqlMeetingDate(). ',' ; } elseif( true == array_key_exists( 'MeetingDate', $this->getChangedColumns() ) ) { $strSql .= ' meeting_date = ' . $this->sqlMeetingDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demo_action_id = ' . $this->sqlDemoActionId(). ',' ; } elseif( true == array_key_exists( 'DemoActionId', $this->getChangedColumns() ) ) { $strSql .= ' demo_action_id = ' . $this->sqlDemoActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demo_date = ' . $this->sqlDemoDate(). ',' ; } elseif( true == array_key_exists( 'DemoDate', $this->getChangedColumns() ) ) { $strSql .= ' demo_date = ' . $this->sqlDemoDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' highest_contract_status_type_id = ' . $this->sqlHighestContractStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'HighestContractStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' highest_contract_status_type_id = ' . $this->sqlHighestContractStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cached_number_of_units = ' . $this->sqlCachedNumberOfUnits(). ',' ; } elseif( true == array_key_exists( 'CachedNumberOfUnits', $this->getChangedColumns() ) ) { $strSql .= ' cached_number_of_units = ' . $this->sqlCachedNumberOfUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_close_month = ' . $this->sqlFirstCloseMonth(). ',' ; } elseif( true == array_key_exists( 'FirstCloseMonth', $this->getChangedColumns() ) ) { $strSql .= ' first_close_month = ' . $this->sqlFirstCloseMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_termination_month = ' . $this->sqlLastTerminationMonth(). ',' ; } elseif( true == array_key_exists( 'LastTerminationMonth', $this->getChangedColumns() ) ) { $strSql .= ' last_termination_month = ' . $this->sqlLastTerminationMonth() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'contract_type_id' => $this->getContractTypeId(),
			'currency_code' => $this->getCurrencyCode(),
			'contract_status_type_id' => $this->getContractStatusTypeId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'person_id' => $this->getPersonId(),
			'ps_lead_origin_id' => $this->getPsLeadOriginId(),
			'contract_reject_type_id' => $this->getContractRejectTypeId(),
			'sales_employee_id' => $this->getSalesEmployeeId(),
			'sales_department_id' => $this->getSalesDepartmentId(),
			'sales_manager_employee_id' => $this->getSalesManagerEmployeeId(),
			'contract_datetime' => $this->getContractDatetime(),
			'contract_start_date' => $this->getContractStartDate(),
			'percent_close_likelihood' => $this->getPercentCloseLikelihood(),
			'title' => $this->getTitle(),
			'notes' => $this->getNotes(),
			'anticipated_close_date' => $this->getAnticipatedCloseDate(),
			'entered_pipeline_on' => $this->getEnteredPipelineOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'duplicate_contract_id' => $this->getDuplicateContractId(),
			'entered_pipeline_action_id' => $this->getEnteredPipelineActionId(),
			'contacted_action_id' => $this->getContactedActionId(),
			'qualified_action_id' => $this->getQualifiedActionId(),
			'discovery_action_id' => $this->getDiscoveryActionId(),
			'proposal_sent_action_id' => $this->getProposalSentActionId(),
			'contract_negotiation_action_id' => $this->getContractNegotiationActionId(),
			'contract_sent_action_id' => $this->getContractSentActionId(),
			'contract_approved_action_id' => $this->getContractApprovedActionId(),
			'contract_terminated_action_id' => $this->getContractTerminatedActionId(),
			'lead_lost_action_id' => $this->getLeadLostActionId(),
			'last_action_id' => $this->getLastActionId(),
			'on_hold_action_id' => $this->getOnHoldActionId(),
			'contacted_date' => $this->getContactedDate(),
			'qualified_date' => $this->getQualifiedDate(),
			'discovery_date' => $this->getDiscoveryDate(),
			'proposal_sent_date' => $this->getProposalSentDate(),
			'contract_negotiation_date' => $this->getContractNegotiationDate(),
			'contract_sent_date' => $this->getContractSentDate(),
			'contract_approved_date' => $this->getContractApprovedDate(),
			'contract_terminated_date' => $this->getContractTerminatedDate(),
			'lead_lost_date' => $this->getLeadLostDate(),
			'last_action_date' => $this->getLastActionDate(),
			'on_hold_date' => $this->getOnHoldDate(),
			'commit_month' => $this->getCommitMonth(),
			'meeting_date' => $this->getMeetingDate(),
			'demo_action_id' => $this->getDemoActionId(),
			'demo_date' => $this->getDemoDate(),
			'highest_contract_status_type_id' => $this->getHighestContractStatusTypeId(),
			'cached_number_of_units' => $this->getCachedNumberOfUnits(),
			'first_close_month' => $this->getFirstCloseMonth(),
			'last_termination_month' => $this->getLastTerminationMonth()
		);
	}

}
?>