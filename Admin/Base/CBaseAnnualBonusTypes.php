<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CAnnualBonusTypes
 * Do not add any new functions to this class.
 */

class CBaseAnnualBonusTypes extends CEosPluralBase {

	/**
	 * @return CAnnualBonusType[]
	 */
	public static function fetchAnnualBonusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CAnnualBonusType', $objDatabase );
	}

	/**
	 * @return CAnnualBonusType
	 */
	public static function fetchAnnualBonusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAnnualBonusType', $objDatabase );
	}

	public static function fetchAnnualBonusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'annual_bonus_types', $objDatabase );
	}

	public static function fetchAnnualBonusTypeById( $intId, $objDatabase ) {
		return self::fetchAnnualBonusType( sprintf( 'SELECT * FROM annual_bonus_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>