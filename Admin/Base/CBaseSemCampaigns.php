<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemCampaigns
 * Do not add any new functions to this class.
 */

class CBaseSemCampaigns extends CEosPluralBase {

	/**
	 * @return CSemCampaign[]
	 */
	public static function fetchSemCampaigns( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemCampaign', $objDatabase );
	}

	/**
	 * @return CSemCampaign
	 */
	public static function fetchSemCampaign( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemCampaign', $objDatabase );
	}

	public static function fetchSemCampaignCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_campaigns', $objDatabase );
	}

	public static function fetchSemCampaignById( $intId, $objDatabase ) {
		return self::fetchSemCampaign( sprintf( 'SELECT * FROM sem_campaigns WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>