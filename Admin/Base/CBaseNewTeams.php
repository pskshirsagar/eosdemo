<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CNewTeams
 * Do not add any new functions to this class.
 */

class CBaseNewTeams extends CEosPluralBase {

	/**
	 * @return CNewTeam[]
	 */
	public static function fetchNewTeams( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNewTeam::class, $objDatabase );
	}

	/**
	 * @return CNewTeam
	 */
	public static function fetchNewTeam( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNewTeam::class, $objDatabase );
	}

	public static function fetchNewTeamCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'new_teams', $objDatabase );
	}

	public static function fetchNewTeamById( $intId, $objDatabase ) {
		return self::fetchNewTeam( sprintf( 'SELECT * FROM new_teams WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchNewTeamsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE manager_employee_id = %d', ( int ) $intManagerEmployeeId ), $objDatabase );
	}

	public static function fetchNewTeamsBySdmEmployeeId( $intSdmEmployeeId, $objDatabase ) {
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE sdm_employee_id = %d', ( int ) $intSdmEmployeeId ), $objDatabase );
	}

	public static function fetchNewTeamsByAddEmployeeId( $intAddEmployeeId, $objDatabase ) {
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE add_employee_id = %d', ( int ) $intAddEmployeeId ), $objDatabase );
	}

	public static function fetchNewTeamsByQamEmployeeId( $intQamEmployeeId, $objDatabase ) {
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE qam_employee_id = %d', ( int ) $intQamEmployeeId ), $objDatabase );
	}

	public static function fetchNewTeamsByQaEmployeeId( $intQaEmployeeId, $objDatabase ) {
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE qa_employee_id = %d', ( int ) $intQaEmployeeId ), $objDatabase );
	}

	public static function fetchNewTeamsByTpmEmployeeId( $intTpmEmployeeId, $objDatabase ) {
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE tpm_employee_id = %d', ( int ) $intTpmEmployeeId ), $objDatabase );
	}

	public static function fetchNewTeamsByVdbaEmployeeId( $intVdbaEmployeeId, $objDatabase ) {
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE vdba_employee_id = %d', ( int ) $intVdbaEmployeeId ), $objDatabase );
	}

	public static function fetchNewTeamsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchNewTeamsByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE ps_product_option_id = %d', ( int ) $intPsProductOptionId ), $objDatabase );
	}

	public static function fetchNewTeamsByHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId, $objDatabase ) {
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE hr_representative_employee_id = %d', ( int ) $intHrRepresentativeEmployeeId ), $objDatabase );
	}

	public static function fetchNewTeamsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchNewTeamsByTeamTypeId( $intTeamTypeId, $objDatabase ) {
		return self::fetchNewTeams( sprintf( 'SELECT * FROM new_teams WHERE team_type_id = %d', ( int ) $intTeamTypeId ), $objDatabase );
	}

}
?>