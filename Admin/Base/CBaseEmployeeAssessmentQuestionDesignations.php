<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAssessmentQuestionDesignations
 * Do not add any new functions to this class.
 */

class CBaseEmployeeAssessmentQuestionDesignations extends CEosPluralBase {

	/**
	 * @return CEmployeeAssessmentQuestionDesignation[]
	 */
	public static function fetchEmployeeAssessmentQuestionDesignations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeAssessmentQuestionDesignation', $objDatabase );
	}

	/**
	 * @return CEmployeeAssessmentQuestionDesignation
	 */
	public static function fetchEmployeeAssessmentQuestionDesignation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeAssessmentQuestionDesignation', $objDatabase );
	}

	public static function fetchEmployeeAssessmentQuestionDesignationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_assessment_question_designations', $objDatabase );
	}

	public static function fetchEmployeeAssessmentQuestionDesignationById( $intId, $objDatabase ) {
		return self::fetchEmployeeAssessmentQuestionDesignation( sprintf( 'SELECT * FROM employee_assessment_question_designations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentQuestionDesignationsByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchEmployeeAssessmentQuestionDesignations( sprintf( 'SELECT * FROM employee_assessment_question_designations WHERE designation_id = %d', ( int ) $intDesignationId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentQuestionDesignationsByEmployeeAssessmentQuestionId( $intEmployeeAssessmentQuestionId, $objDatabase ) {
		return self::fetchEmployeeAssessmentQuestionDesignations( sprintf( 'SELECT * FROM employee_assessment_question_designations WHERE employee_assessment_question_id = %d', ( int ) $intEmployeeAssessmentQuestionId ), $objDatabase );
	}

}
?>