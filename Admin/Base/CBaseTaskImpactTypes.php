<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskImpactTypes
 * Do not add any new functions to this class.
 */

class CBaseTaskImpactTypes extends CEosPluralBase {

	/**
	 * @return CTaskImpactType[]
	 */
	public static function fetchTaskImpactTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskImpactType', $objDatabase );
	}

	/**
	 * @return CTaskImpactType
	 */
	public static function fetchTaskImpactType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskImpactType', $objDatabase );
	}

	public static function fetchTaskImpactTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_impact_types', $objDatabase );
	}

	public static function fetchTaskImpactTypeById( $intId, $objDatabase ) {
		return self::fetchTaskImpactType( sprintf( 'SELECT * FROM task_impact_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>