<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdSources
 * Do not add any new functions to this class.
 */

class CBaseSemAdSources extends CEosPluralBase {

	/**
	 * @return CSemAdSource[]
	 */
	public static function fetchSemAdSources( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemAdSource', $objDatabase );
	}

	/**
	 * @return CSemAdSource
	 */
	public static function fetchSemAdSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemAdSource', $objDatabase );
	}

	public static function fetchSemAdSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_ad_sources', $objDatabase );
	}

	public static function fetchSemAdSourceById( $intId, $objDatabase ) {
		return self::fetchSemAdSource( sprintf( 'SELECT * FROM sem_ad_sources WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemAdSourcesBySemSourceId( $intSemSourceId, $objDatabase ) {
		return self::fetchSemAdSources( sprintf( 'SELECT * FROM sem_ad_sources WHERE sem_source_id = %d', ( int ) $intSemSourceId ), $objDatabase );
	}

	public static function fetchSemAdSourcesBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemAdSources( sprintf( 'SELECT * FROM sem_ad_sources WHERE sem_ad_group_id = %d', ( int ) $intSemAdGroupId ), $objDatabase );
	}

	public static function fetchSemAdSourcesBySemAdId( $intSemAdId, $objDatabase ) {
		return self::fetchSemAdSources( sprintf( 'SELECT * FROM sem_ad_sources WHERE sem_ad_id = %d', ( int ) $intSemAdId ), $objDatabase );
	}

	public static function fetchSemAdSourcesBySemAdStatusTypeId( $intSemAdStatusTypeId, $objDatabase ) {
		return self::fetchSemAdSources( sprintf( 'SELECT * FROM sem_ad_sources WHERE sem_ad_status_type_id = %d', ( int ) $intSemAdStatusTypeId ), $objDatabase );
	}

	public static function fetchSemAdSourcesBySemStatusTypeId( $intSemStatusTypeId, $objDatabase ) {
		return self::fetchSemAdSources( sprintf( 'SELECT * FROM sem_ad_sources WHERE sem_status_type_id = %d', ( int ) $intSemStatusTypeId ), $objDatabase );
	}

}
?>