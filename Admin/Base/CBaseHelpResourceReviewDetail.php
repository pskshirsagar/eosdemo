<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseHelpResourceReviewDetail extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.help_resource_review_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intHelpResourceId;
	protected $m_arrintReviewerEmployeeIds;
	protected $m_intReviewFrequencyDays;
	protected $m_strReviewDueDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intContentReviewStatusId;
	protected $m_intReviewedBy;
	protected $m_strReviewedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intHelpResourceId', trim( $arrValues['help_resource_id'] ) ); elseif( isset( $arrValues['help_resource_id'] ) ) $this->setHelpResourceId( $arrValues['help_resource_id'] );
		if( isset( $arrValues['reviewer_employee_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintReviewerEmployeeIds', trim( $arrValues['reviewer_employee_ids'] ) ); elseif( isset( $arrValues['reviewer_employee_ids'] ) ) $this->setReviewerEmployeeIds( $arrValues['reviewer_employee_ids'] );
		if( isset( $arrValues['review_frequency_days'] ) && $boolDirectSet ) $this->set( 'm_intReviewFrequencyDays', trim( $arrValues['review_frequency_days'] ) ); elseif( isset( $arrValues['review_frequency_days'] ) ) $this->setReviewFrequencyDays( $arrValues['review_frequency_days'] );
		if( isset( $arrValues['review_due_date'] ) && $boolDirectSet ) $this->set( 'm_strReviewDueDate', trim( $arrValues['review_due_date'] ) ); elseif( isset( $arrValues['review_due_date'] ) ) $this->setReviewDueDate( $arrValues['review_due_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['content_review_status_id'] ) && $boolDirectSet ) $this->set( 'm_intContentReviewStatusId', trim( $arrValues['content_review_status_id'] ) ); elseif( isset( $arrValues['content_review_status_id'] ) ) $this->setContentReviewStatusId( $arrValues['content_review_status_id'] );
		if( isset( $arrValues['reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intReviewedBy', trim( $arrValues['reviewed_by'] ) ); elseif( isset( $arrValues['reviewed_by'] ) ) $this->setReviewedBy( $arrValues['reviewed_by'] );
		if( isset( $arrValues['reviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strReviewedOn', trim( $arrValues['reviewed_on'] ) ); elseif( isset( $arrValues['reviewed_on'] ) ) $this->setReviewedOn( $arrValues['reviewed_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setHelpResourceId( $intHelpResourceId ) {
		$this->set( 'm_intHelpResourceId', CStrings::strToIntDef( $intHelpResourceId, NULL, false ) );
	}

	public function getHelpResourceId() {
		return $this->m_intHelpResourceId;
	}

	public function sqlHelpResourceId() {
		return ( true == isset( $this->m_intHelpResourceId ) ) ? ( string ) $this->m_intHelpResourceId : 'NULL';
	}

	public function setReviewerEmployeeIds( $arrintReviewerEmployeeIds ) {
		$this->set( 'm_arrintReviewerEmployeeIds', CStrings::strToArrIntDef( $arrintReviewerEmployeeIds, NULL ) );
	}

	public function getReviewerEmployeeIds() {
		return $this->m_arrintReviewerEmployeeIds;
	}

	public function sqlReviewerEmployeeIds() {
		return ( true == isset( $this->m_arrintReviewerEmployeeIds ) && true == valArr( $this->m_arrintReviewerEmployeeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintReviewerEmployeeIds, NULL ) . '\'' : 'NULL';
	}

	public function setReviewFrequencyDays( $intReviewFrequencyDays ) {
		$this->set( 'm_intReviewFrequencyDays', CStrings::strToIntDef( $intReviewFrequencyDays, NULL, false ) );
	}

	public function getReviewFrequencyDays() {
		return $this->m_intReviewFrequencyDays;
	}

	public function sqlReviewFrequencyDays() {
		return ( true == isset( $this->m_intReviewFrequencyDays ) ) ? ( string ) $this->m_intReviewFrequencyDays : 'NULL';
	}

	public function setReviewDueDate( $strReviewDueDate ) {
		$this->set( 'm_strReviewDueDate', CStrings::strTrimDef( $strReviewDueDate, -1, NULL, true ) );
	}

	public function getReviewDueDate() {
		return $this->m_strReviewDueDate;
	}

	public function sqlReviewDueDate() {
		return ( true == isset( $this->m_strReviewDueDate ) ) ? '\'' . $this->m_strReviewDueDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setContentReviewStatusId( $intContentReviewStatusId ) {
		$this->set( 'm_intContentReviewStatusId', CStrings::strToIntDef( $intContentReviewStatusId, NULL, false ) );
	}

	public function getContentReviewStatusId() {
		return $this->m_intContentReviewStatusId;
	}

	public function sqlContentReviewStatusId() {
		return ( true == isset( $this->m_intContentReviewStatusId ) ) ? ( string ) $this->m_intContentReviewStatusId : 'NULL';
	}

	public function setReviewedBy( $intReviewedBy ) {
		$this->set( 'm_intReviewedBy', CStrings::strToIntDef( $intReviewedBy, NULL, false ) );
	}

	public function getReviewedBy() {
		return $this->m_intReviewedBy;
	}

	public function sqlReviewedBy() {
		return ( true == isset( $this->m_intReviewedBy ) ) ? ( string ) $this->m_intReviewedBy : 'NULL';
	}

	public function setReviewedOn( $strReviewedOn ) {
		$this->set( 'm_strReviewedOn', CStrings::strTrimDef( $strReviewedOn, -1, NULL, true ) );
	}

	public function getReviewedOn() {
		return $this->m_strReviewedOn;
	}

	public function sqlReviewedOn() {
		return ( true == isset( $this->m_strReviewedOn ) ) ? '\'' . $this->m_strReviewedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, help_resource_id, reviewer_employee_ids, review_frequency_days, review_due_date, updated_by, updated_on, created_by, created_on, content_review_status_id, reviewed_by, reviewed_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlHelpResourceId() . ', ' .
						$this->sqlReviewerEmployeeIds() . ', ' .
						$this->sqlReviewFrequencyDays() . ', ' .
						$this->sqlReviewDueDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlContentReviewStatusId() . ', ' .
						$this->sqlReviewedBy() . ', ' .
						$this->sqlReviewedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_resource_id = ' . $this->sqlHelpResourceId(). ',' ; } elseif( true == array_key_exists( 'HelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' help_resource_id = ' . $this->sqlHelpResourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewer_employee_ids = ' . $this->sqlReviewerEmployeeIds(). ',' ; } elseif( true == array_key_exists( 'ReviewerEmployeeIds', $this->getChangedColumns() ) ) { $strSql .= ' reviewer_employee_ids = ' . $this->sqlReviewerEmployeeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_frequency_days = ' . $this->sqlReviewFrequencyDays(). ',' ; } elseif( true == array_key_exists( 'ReviewFrequencyDays', $this->getChangedColumns() ) ) { $strSql .= ' review_frequency_days = ' . $this->sqlReviewFrequencyDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_due_date = ' . $this->sqlReviewDueDate(). ',' ; } elseif( true == array_key_exists( 'ReviewDueDate', $this->getChangedColumns() ) ) { $strSql .= ' review_due_date = ' . $this->sqlReviewDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content_review_status_id = ' . $this->sqlContentReviewStatusId(). ',' ; } elseif( true == array_key_exists( 'ContentReviewStatusId', $this->getChangedColumns() ) ) { $strSql .= ' content_review_status_id = ' . $this->sqlContentReviewStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy(). ',' ; } elseif( true == array_key_exists( 'ReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn(). ',' ; } elseif( true == array_key_exists( 'ReviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'help_resource_id' => $this->getHelpResourceId(),
			'reviewer_employee_ids' => $this->getReviewerEmployeeIds(),
			'review_frequency_days' => $this->getReviewFrequencyDays(),
			'review_due_date' => $this->getReviewDueDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'content_review_status_id' => $this->getContentReviewStatusId(),
			'reviewed_by' => $this->getReviewedBy(),
			'reviewed_on' => $this->getReviewedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>