<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseHelpResourceTranslation extends CEosSingularBase {

	const TABLE_NAME = 'public.help_resource_translations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intHelpResourceId;
	protected $m_intContentTranslationStatusId;
	protected $m_strLocaleCode;
	protected $m_strContent;
	protected $m_strTranslationReturnReason;
	protected $m_strRequestDatetime;
	protected $m_strTranslatedDatetime;
	protected $m_intContentVerifiedBy;
	protected $m_strContentVerifiedOn;
	protected $m_intLayoutVerifiedBy;
	protected $m_strLayoutVerifiedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intHelpResourceId', trim( $arrValues['help_resource_id'] ) ); elseif( isset( $arrValues['help_resource_id'] ) ) $this->setHelpResourceId( $arrValues['help_resource_id'] );
		if( isset( $arrValues['content_translation_status_id'] ) && $boolDirectSet ) $this->set( 'm_intContentTranslationStatusId', trim( $arrValues['content_translation_status_id'] ) ); elseif( isset( $arrValues['content_translation_status_id'] ) ) $this->setContentTranslationStatusId( $arrValues['content_translation_status_id'] );
		if( isset( $arrValues['locale_code'] ) && $boolDirectSet ) $this->set( 'm_strLocaleCode', trim( stripcslashes( $arrValues['locale_code'] ) ) ); elseif( isset( $arrValues['locale_code'] ) ) $this->setLocaleCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['locale_code'] ) : $arrValues['locale_code'] );
		if( isset( $arrValues['content'] ) && $boolDirectSet ) $this->set( 'm_strContent', trim( stripcslashes( $arrValues['content'] ) ) ); elseif( isset( $arrValues['content'] ) ) $this->setContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['content'] ) : $arrValues['content'] );
		if( isset( $arrValues['translation_return_reason'] ) && $boolDirectSet ) $this->set( 'm_strTranslationReturnReason', trim( stripcslashes( $arrValues['translation_return_reason'] ) ) ); elseif( isset( $arrValues['translation_return_reason'] ) ) $this->setTranslationReturnReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['translation_return_reason'] ) : $arrValues['translation_return_reason'] );
		if( isset( $arrValues['request_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequestDatetime', trim( $arrValues['request_datetime'] ) ); elseif( isset( $arrValues['request_datetime'] ) ) $this->setRequestDatetime( $arrValues['request_datetime'] );
		if( isset( $arrValues['translated_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTranslatedDatetime', trim( $arrValues['translated_datetime'] ) ); elseif( isset( $arrValues['translated_datetime'] ) ) $this->setTranslatedDatetime( $arrValues['translated_datetime'] );
		if( isset( $arrValues['content_verified_by'] ) && $boolDirectSet ) $this->set( 'm_intContentVerifiedBy', trim( $arrValues['content_verified_by'] ) ); elseif( isset( $arrValues['content_verified_by'] ) ) $this->setContentVerifiedBy( $arrValues['content_verified_by'] );
		if( isset( $arrValues['content_verified_on'] ) && $boolDirectSet ) $this->set( 'm_strContentVerifiedOn', trim( $arrValues['content_verified_on'] ) ); elseif( isset( $arrValues['content_verified_on'] ) ) $this->setContentVerifiedOn( $arrValues['content_verified_on'] );
		if( isset( $arrValues['layout_verified_by'] ) && $boolDirectSet ) $this->set( 'm_intLayoutVerifiedBy', trim( $arrValues['layout_verified_by'] ) ); elseif( isset( $arrValues['layout_verified_by'] ) ) $this->setLayoutVerifiedBy( $arrValues['layout_verified_by'] );
		if( isset( $arrValues['layout_verified_on'] ) && $boolDirectSet ) $this->set( 'm_strLayoutVerifiedOn', trim( $arrValues['layout_verified_on'] ) ); elseif( isset( $arrValues['layout_verified_on'] ) ) $this->setLayoutVerifiedOn( $arrValues['layout_verified_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setHelpResourceId( $intHelpResourceId ) {
		$this->set( 'm_intHelpResourceId', CStrings::strToIntDef( $intHelpResourceId, NULL, false ) );
	}

	public function getHelpResourceId() {
		return $this->m_intHelpResourceId;
	}

	public function sqlHelpResourceId() {
		return ( true == isset( $this->m_intHelpResourceId ) ) ? ( string ) $this->m_intHelpResourceId : 'NULL';
	}

	public function setContentTranslationStatusId( $intContentTranslationStatusId ) {
		$this->set( 'm_intContentTranslationStatusId', CStrings::strToIntDef( $intContentTranslationStatusId, NULL, false ) );
	}

	public function getContentTranslationStatusId() {
		return $this->m_intContentTranslationStatusId;
	}

	public function sqlContentTranslationStatusId() {
		return ( true == isset( $this->m_intContentTranslationStatusId ) ) ? ( string ) $this->m_intContentTranslationStatusId : 'NULL';
	}

	public function setLocaleCode( $strLocaleCode ) {
		$this->set( 'm_strLocaleCode', CStrings::strTrimDef( $strLocaleCode, 100, NULL, true ) );
	}

	public function getLocaleCode() {
		return $this->m_strLocaleCode;
	}

	public function sqlLocaleCode() {
		return ( true == isset( $this->m_strLocaleCode ) ) ? '\'' . addslashes( $this->m_strLocaleCode ) . '\'' : 'NULL';
	}

	public function setContent( $strContent ) {
		$this->set( 'm_strContent', CStrings::strTrimDef( $strContent, -1, NULL, true ) );
	}

	public function getContent() {
		return $this->m_strContent;
	}

	public function sqlContent() {
		return ( true == isset( $this->m_strContent ) ) ? '\'' . addslashes( $this->m_strContent ) . '\'' : 'NULL';
	}

	public function setTranslationReturnReason( $strTranslationReturnReason ) {
		$this->set( 'm_strTranslationReturnReason', CStrings::strTrimDef( $strTranslationReturnReason, -1, NULL, true ) );
	}

	public function getTranslationReturnReason() {
		return $this->m_strTranslationReturnReason;
	}

	public function sqlTranslationReturnReason() {
		return ( true == isset( $this->m_strTranslationReturnReason ) ) ? '\'' . addslashes( $this->m_strTranslationReturnReason ) . '\'' : 'NULL';
	}

	public function setRequestDatetime( $strRequestDatetime ) {
		$this->set( 'm_strRequestDatetime', CStrings::strTrimDef( $strRequestDatetime, -1, NULL, true ) );
	}

	public function getRequestDatetime() {
		return $this->m_strRequestDatetime;
	}

	public function sqlRequestDatetime() {
		return ( true == isset( $this->m_strRequestDatetime ) ) ? '\'' . $this->m_strRequestDatetime . '\'' : 'NULL';
	}

	public function setTranslatedDatetime( $strTranslatedDatetime ) {
		$this->set( 'm_strTranslatedDatetime', CStrings::strTrimDef( $strTranslatedDatetime, -1, NULL, true ) );
	}

	public function getTranslatedDatetime() {
		return $this->m_strTranslatedDatetime;
	}

	public function sqlTranslatedDatetime() {
		return ( true == isset( $this->m_strTranslatedDatetime ) ) ? '\'' . $this->m_strTranslatedDatetime . '\'' : 'NULL';
	}

	public function setContentVerifiedBy( $intContentVerifiedBy ) {
		$this->set( 'm_intContentVerifiedBy', CStrings::strToIntDef( $intContentVerifiedBy, NULL, false ) );
	}

	public function getContentVerifiedBy() {
		return $this->m_intContentVerifiedBy;
	}

	public function sqlContentVerifiedBy() {
		return ( true == isset( $this->m_intContentVerifiedBy ) ) ? ( string ) $this->m_intContentVerifiedBy : 'NULL';
	}

	public function setContentVerifiedOn( $strContentVerifiedOn ) {
		$this->set( 'm_strContentVerifiedOn', CStrings::strTrimDef( $strContentVerifiedOn, -1, NULL, true ) );
	}

	public function getContentVerifiedOn() {
		return $this->m_strContentVerifiedOn;
	}

	public function sqlContentVerifiedOn() {
		return ( true == isset( $this->m_strContentVerifiedOn ) ) ? '\'' . $this->m_strContentVerifiedOn . '\'' : 'NULL';
	}

	public function setLayoutVerifiedBy( $intLayoutVerifiedBy ) {
		$this->set( 'm_intLayoutVerifiedBy', CStrings::strToIntDef( $intLayoutVerifiedBy, NULL, false ) );
	}

	public function getLayoutVerifiedBy() {
		return $this->m_intLayoutVerifiedBy;
	}

	public function sqlLayoutVerifiedBy() {
		return ( true == isset( $this->m_intLayoutVerifiedBy ) ) ? ( string ) $this->m_intLayoutVerifiedBy : 'NULL';
	}

	public function setLayoutVerifiedOn( $strLayoutVerifiedOn ) {
		$this->set( 'm_strLayoutVerifiedOn', CStrings::strTrimDef( $strLayoutVerifiedOn, -1, NULL, true ) );
	}

	public function getLayoutVerifiedOn() {
		return $this->m_strLayoutVerifiedOn;
	}

	public function sqlLayoutVerifiedOn() {
		return ( true == isset( $this->m_strLayoutVerifiedOn ) ) ? '\'' . $this->m_strLayoutVerifiedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, help_resource_id, content_translation_status_id, locale_code, content, translation_return_reason, request_datetime, translated_datetime, content_verified_by, content_verified_on, layout_verified_by, layout_verified_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlHelpResourceId() . ', ' .
						$this->sqlContentTranslationStatusId() . ', ' .
						$this->sqlLocaleCode() . ', ' .
						$this->sqlContent() . ', ' .
						$this->sqlTranslationReturnReason() . ', ' .
						$this->sqlRequestDatetime() . ', ' .
						$this->sqlTranslatedDatetime() . ', ' .
						$this->sqlContentVerifiedBy() . ', ' .
						$this->sqlContentVerifiedOn() . ', ' .
						$this->sqlLayoutVerifiedBy() . ', ' .
						$this->sqlLayoutVerifiedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_resource_id = ' . $this->sqlHelpResourceId(). ',' ; } elseif( true == array_key_exists( 'HelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' help_resource_id = ' . $this->sqlHelpResourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content_translation_status_id = ' . $this->sqlContentTranslationStatusId(). ',' ; } elseif( true == array_key_exists( 'ContentTranslationStatusId', $this->getChangedColumns() ) ) { $strSql .= ' content_translation_status_id = ' . $this->sqlContentTranslationStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode(). ',' ; } elseif( true == array_key_exists( 'LocaleCode', $this->getChangedColumns() ) ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content = ' . $this->sqlContent(). ',' ; } elseif( true == array_key_exists( 'Content', $this->getChangedColumns() ) ) { $strSql .= ' content = ' . $this->sqlContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' translation_return_reason = ' . $this->sqlTranslationReturnReason(). ',' ; } elseif( true == array_key_exists( 'TranslationReturnReason', $this->getChangedColumns() ) ) { $strSql .= ' translation_return_reason = ' . $this->sqlTranslationReturnReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime(). ',' ; } elseif( true == array_key_exists( 'RequestDatetime', $this->getChangedColumns() ) ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' translated_datetime = ' . $this->sqlTranslatedDatetime(). ',' ; } elseif( true == array_key_exists( 'TranslatedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' translated_datetime = ' . $this->sqlTranslatedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content_verified_by = ' . $this->sqlContentVerifiedBy(). ',' ; } elseif( true == array_key_exists( 'ContentVerifiedBy', $this->getChangedColumns() ) ) { $strSql .= ' content_verified_by = ' . $this->sqlContentVerifiedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content_verified_on = ' . $this->sqlContentVerifiedOn(). ',' ; } elseif( true == array_key_exists( 'ContentVerifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' content_verified_on = ' . $this->sqlContentVerifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' layout_verified_by = ' . $this->sqlLayoutVerifiedBy(). ',' ; } elseif( true == array_key_exists( 'LayoutVerifiedBy', $this->getChangedColumns() ) ) { $strSql .= ' layout_verified_by = ' . $this->sqlLayoutVerifiedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' layout_verified_on = ' . $this->sqlLayoutVerifiedOn(). ',' ; } elseif( true == array_key_exists( 'LayoutVerifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' layout_verified_on = ' . $this->sqlLayoutVerifiedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'help_resource_id' => $this->getHelpResourceId(),
			'content_translation_status_id' => $this->getContentTranslationStatusId(),
			'locale_code' => $this->getLocaleCode(),
			'content' => $this->getContent(),
			'translation_return_reason' => $this->getTranslationReturnReason(),
			'request_datetime' => $this->getRequestDatetime(),
			'translated_datetime' => $this->getTranslatedDatetime(),
			'content_verified_by' => $this->getContentVerifiedBy(),
			'content_verified_on' => $this->getContentVerifiedOn(),
			'layout_verified_by' => $this->getLayoutVerifiedBy(),
			'layout_verified_on' => $this->getLayoutVerifiedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>