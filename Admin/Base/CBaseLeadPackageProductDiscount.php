<?php

class CBaseLeadPackageProductDiscount extends CEosSingularBase {

	const TABLE_NAME = 'public.lead_package_product_discounts';

	protected $m_intId;
	protected $m_intLeadPackageProductId;
	protected $m_intDependentProductId;
	protected $m_fltSetUpDiscountAmount;
	protected $m_fltRecurringDiscountAmount;
	protected $m_fltSetUpDiscountPercentage;
	protected $m_fltRecurringDiscountPercentage;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltSetUpDiscountAmount = '0';
		$this->m_fltRecurringDiscountAmount = '0';
		$this->m_fltSetUpDiscountPercentage = '0';
		$this->m_fltRecurringDiscountPercentage = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['lead_package_product_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadPackageProductId', trim( $arrValues['lead_package_product_id'] ) ); elseif( isset( $arrValues['lead_package_product_id'] ) ) $this->setLeadPackageProductId( $arrValues['lead_package_product_id'] );
		if( isset( $arrValues['dependent_product_id'] ) && $boolDirectSet ) $this->set( 'm_intDependentProductId', trim( $arrValues['dependent_product_id'] ) ); elseif( isset( $arrValues['dependent_product_id'] ) ) $this->setDependentProductId( $arrValues['dependent_product_id'] );
		if( isset( $arrValues['set_up_discount_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSetUpDiscountAmount', trim( $arrValues['set_up_discount_amount'] ) ); elseif( isset( $arrValues['set_up_discount_amount'] ) ) $this->setSetUpDiscountAmount( $arrValues['set_up_discount_amount'] );
		if( isset( $arrValues['recurring_discount_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRecurringDiscountAmount', trim( $arrValues['recurring_discount_amount'] ) ); elseif( isset( $arrValues['recurring_discount_amount'] ) ) $this->setRecurringDiscountAmount( $arrValues['recurring_discount_amount'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['setup_discount_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltSetUpDiscountPercentage', trim( $arrValues['setup_discount_percentage'] ) ); elseif( isset( $arrValues['setup_discount_percentage'] ) ) $this->setSetUpDiscountPercentage( $arrValues['setup_discount_percentage'] );
		if( isset( $arrValues['recurring_discount_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltRecurringDiscountPercentage', trim( $arrValues['recurring_discount_percentage'] ) ); elseif( isset( $arrValues['recurring_discount_percentage'] ) ) $this->setRecurringDiscountPercentage( $arrValues['recurring_discount_percentage'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setLeadPackageProductId( $intLeadPackageProductId ) {
		$this->set( 'm_intLeadPackageProductId', CStrings::strToIntDef( $intLeadPackageProductId, NULL, false ) );
	}

	public function getLeadPackageProductId() {
		return $this->m_intLeadPackageProductId;
	}

	public function sqlLeadPackageProductId() {
		return ( true == isset( $this->m_intLeadPackageProductId ) ) ? ( string ) $this->m_intLeadPackageProductId : 'NULL';
	}

	public function setDependentProductId( $intDependentProductId ) {
		$this->set( 'm_intDependentProductId', CStrings::strToIntDef( $intDependentProductId, NULL, false ) );
	}

	public function getDependentProductId() {
		return $this->m_intDependentProductId;
	}

	public function sqlDependentProductId() {
		return ( true == isset( $this->m_intDependentProductId ) ) ? ( string ) $this->m_intDependentProductId : 'NULL';
	}

	public function setSetUpDiscountAmount( $fltSetUpDiscountAmount ) {
		$this->set( 'm_fltSetUpDiscountAmount', CStrings::strToFloatDef( $fltSetUpDiscountAmount, NULL, false, 2 ) );
	}

	public function getSetUpDiscountAmount() {
		return $this->m_fltSetUpDiscountAmount;
	}

	public function sqlSetUpDiscountAmount() {
		return ( true == isset( $this->m_fltSetUpDiscountAmount ) ) ? ( string ) $this->m_fltSetUpDiscountAmount : '0';
	}

	public function setRecurringDiscountAmount( $fltRecurringDiscountAmount ) {
		$this->set( 'm_fltRecurringDiscountAmount', CStrings::strToFloatDef( $fltRecurringDiscountAmount, NULL, false, 2 ) );
	}

	public function getRecurringDiscountAmount() {
		return $this->m_fltRecurringDiscountAmount;
	}

	public function sqlRecurringDiscountAmount() {
		return ( true == isset( $this->m_fltRecurringDiscountAmount ) ) ? ( string ) $this->m_fltRecurringDiscountAmount : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSetUpDiscountPercentage( $fltSetUpDiscountPercentage ) {
		$this->set( 'm_fltSetUpDiscountPercentage', CStrings::strToFloatDef( $fltSetUpDiscountPercentage, NULL, false, 2 ) );
	}

	public function getSetUpDiscountPercentage() {
		return $this->m_fltSetUpDiscountPercentage;
	}

	public function sqlSetUpDiscountPercentage() {
		return ( true == isset( $this->m_fltSetUpDiscountPercentage ) ) ? ( string ) $this->m_fltSetUpDiscountPercentage : '0';
	}

	public function setRecurringDiscountPercentage( $fltRecurringDiscountPercentage ) {
		$this->set( 'm_fltRecurringDiscountPercentage', CStrings::strToFloatDef( $fltRecurringDiscountPercentage, NULL, false, 2 ) );
	}

	public function getRecurringDiscountPercentage() {
		return $this->m_fltRecurringDiscountPercentage;
	}

	public function sqlRecurringDiscountPercentage() {
		return ( true == isset( $this->m_fltRecurringDiscountPercentage ) ) ? ( string ) $this->m_fltRecurringDiscountPercentage : '0';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, lead_package_product_id, dependent_product_id, set_up_discount_amount, recurring_discount_amount, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, setup_discount_percentage, recurring_discount_percentage )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlLeadPackageProductId() . ', ' .
						$this->sqlDependentProductId() . ', ' .
						$this->sqlSetUpDiscountAmount() . ', ' .
						$this->sqlRecurringDiscountAmount() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlSetUpDiscountPercentage() . ', ' .
						$this->sqlRecurringDiscountPercentage() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_package_product_id = ' . $this->sqlLeadPackageProductId(). ',' ; } elseif( true == array_key_exists( 'LeadPackageProductId', $this->getChangedColumns() ) ) { $strSql .= ' lead_package_product_id = ' . $this->sqlLeadPackageProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dependent_product_id = ' . $this->sqlDependentProductId(). ',' ; } elseif( true == array_key_exists( 'DependentProductId', $this->getChangedColumns() ) ) { $strSql .= ' dependent_product_id = ' . $this->sqlDependentProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' set_up_discount_amount = ' . $this->sqlSetUpDiscountAmount(). ',' ; } elseif( true == array_key_exists( 'SetUpDiscountAmount', $this->getChangedColumns() ) ) { $strSql .= ' set_up_discount_amount = ' . $this->sqlSetUpDiscountAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recurring_discount_amount = ' . $this->sqlRecurringDiscountAmount(). ',' ; } elseif( true == array_key_exists( 'RecurringDiscountAmount', $this->getChangedColumns() ) ) { $strSql .= ' recurring_discount_amount = ' . $this->sqlRecurringDiscountAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' setup_discount_percentage = ' . $this->sqlSetUpDiscountPercentage(). ',' ; } elseif( true == array_key_exists( 'SetUpDiscountPercentage', $this->getChangedColumns() ) ) { $strSql .= ' setup_discount_percentage = ' . $this->sqlSetUpDiscountPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recurring_discount_percentage = ' . $this->sqlRecurringDiscountPercentage(). ',' ; } elseif( true == array_key_exists( 'RecurringDiscountPercentage', $this->getChangedColumns() ) ) { $strSql .= ' recurring_discount_Percentage = ' . $this->sqlRecurringDiscountPercentage() . ','; $boolUpdate = true; };

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'lead_package_product_id' => $this->getLeadPackageProductId(),
			'dependent_product_id' => $this->getDependentProductId(),
			'set_up_discount_amount' => $this->getSetUpDiscountAmount(),
			'recurring_discount_amount' => $this->getRecurringDiscountAmount(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'setup_discount_percentage' => $this->getSetUpDiscountPercentage(),
			'recurring_discount_percentage' => $this->getRecurringDiscountPercentage()
			);
	}

}
?>