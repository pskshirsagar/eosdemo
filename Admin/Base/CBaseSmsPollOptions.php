<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSmsPollOptions
 * Do not add any new functions to this class.
 */

class CBaseSmsPollOptions extends CEosPluralBase {

	/**
	 * @return CSmsPollOption[]
	 */
	public static function fetchSmsPollOptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSmsPollOption', $objDatabase );
	}

	/**
	 * @return CSmsPollOption
	 */
	public static function fetchSmsPollOption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSmsPollOption', $objDatabase );
	}

	public static function fetchSmsPollOptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sms_poll_options', $objDatabase );
	}

	public static function fetchSmsPollOptionById( $intId, $objDatabase ) {
		return self::fetchSmsPollOption( sprintf( 'SELECT * FROM sms_poll_options WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSmsPollOptionsByKeywordId( $intKeywordId, $objDatabase ) {
		return self::fetchSmsPollOptions( sprintf( 'SELECT * FROM sms_poll_options WHERE keyword_id = %d', ( int ) $intKeywordId ), $objDatabase );
	}

}
?>