<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserRoles
 * Do not add any new functions to this class.
 */

class CBaseUserRoles extends CEosPluralBase {

	/**
	 * @return CUserRole[]
	 */
	public static function fetchUserRoles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserRole', $objDatabase );
	}

	/**
	 * @return CUserRole
	 */
	public static function fetchUserRole( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserRole', $objDatabase );
	}

	public static function fetchUserRoleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_roles', $objDatabase );
	}

	public static function fetchUserRoleById( $intId, $objDatabase ) {
		return self::fetchUserRole( sprintf( 'SELECT * FROM user_roles WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserRolesByUserId( $intUserId, $objDatabase ) {
		return self::fetchUserRoles( sprintf( 'SELECT * FROM user_roles WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

	public static function fetchUserRolesByRoleId( $intRoleId, $objDatabase ) {
		return self::fetchUserRoles( sprintf( 'SELECT * FROM user_roles WHERE role_id = %d', ( int ) $intRoleId ), $objDatabase );
	}

}
?>