<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSkillsets
 * Do not add any new functions to this class.
 */

class CBaseSkillsets extends CEosPluralBase {

	/**
	 * @return CSkillset[]
	 */
	public static function fetchSkillsets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSkillset', $objDatabase );
	}

	/**
	 * @return CSkillset
	 */
	public static function fetchSkillset( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSkillset', $objDatabase );
	}

	public static function fetchSkillsetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'skillsets', $objDatabase );
	}

	public static function fetchSkillsetById( $intId, $objDatabase ) {
		return self::fetchSkillset( sprintf( 'SELECT * FROM skillsets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>