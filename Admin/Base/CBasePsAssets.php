<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsAssets
 * Do not add any new functions to this class.
 */

class CBasePsAssets extends CEosPluralBase {

	/**
	 * @return CPsAsset[]
	 */
	public static function fetchPsAssets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsAsset', $objDatabase );
	}

	/**
	 * @return CPsAsset
	 */
	public static function fetchPsAsset( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsAsset', $objDatabase );
	}

	public static function fetchPsAssetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_assets', $objDatabase );
	}

	public static function fetchPsAssetById( $intId, $objDatabase ) {
		return self::fetchPsAsset( sprintf( 'SELECT * FROM ps_assets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsAssetsByPsAssetStatusTypeId( $intPsAssetStatusTypeId, $objDatabase ) {
		return self::fetchPsAssets( sprintf( 'SELECT * FROM ps_assets WHERE ps_asset_status_type_id = %d', ( int ) $intPsAssetStatusTypeId ), $objDatabase );
	}

	public static function fetchPsAssetsByOfficeId( $intOfficeId, $objDatabase ) {
		return self::fetchPsAssets( sprintf( 'SELECT * FROM ps_assets WHERE office_id = %d', ( int ) $intOfficeId ), $objDatabase );
	}

	public static function fetchPsAssetsByPsAssetTypeId( $intPsAssetTypeId, $objDatabase ) {
		return self::fetchPsAssets( sprintf( 'SELECT * FROM ps_assets WHERE ps_asset_type_id = %d', ( int ) $intPsAssetTypeId ), $objDatabase );
	}

	public static function fetchPsAssetsByPsAssetBrandId( $intPsAssetBrandId, $objDatabase ) {
		return self::fetchPsAssets( sprintf( 'SELECT * FROM ps_assets WHERE ps_asset_brand_id = %d', ( int ) $intPsAssetBrandId ), $objDatabase );
	}

	public static function fetchPsAssetsByPsAssetModelId( $intPsAssetModelId, $objDatabase ) {
		return self::fetchPsAssets( sprintf( 'SELECT * FROM ps_assets WHERE ps_asset_model_id = %d', ( int ) $intPsAssetModelId ), $objDatabase );
	}

	public static function fetchPsAssetsByPurchaseRequestId( $intPurchaseRequestId, $objDatabase ) {
		return self::fetchPsAssets( sprintf( 'SELECT * FROM ps_assets WHERE purchase_request_id = %d', ( int ) $intPurchaseRequestId ), $objDatabase );
	}

	public static function fetchPsAssetsByHardwareTypeId( $intHardwareTypeId, $objDatabase ) {
		return self::fetchPsAssets( sprintf( 'SELECT * FROM ps_assets WHERE hardware_type_id = %d', ( int ) $intHardwareTypeId ), $objDatabase );
	}

	public static function fetchPsAssetsByMachineTypeId( $intMachineTypeId, $objDatabase ) {
		return self::fetchPsAssets( sprintf( 'SELECT * FROM ps_assets WHERE machine_type_id = %d', ( int ) $intMachineTypeId ), $objDatabase );
	}

	public static function fetchPsAssetsBySwitchTypeId( $intSwitchTypeId, $objDatabase ) {
		return self::fetchPsAssets( sprintf( 'SELECT * FROM ps_assets WHERE switch_type_id = %d', ( int ) $intSwitchTypeId ), $objDatabase );
	}

	public static function fetchPsAssetsByLocationId( $intLocationId, $objDatabase ) {
		return self::fetchPsAssets( sprintf( 'SELECT * FROM ps_assets WHERE location_id = %d', ( int ) $intLocationId ), $objDatabase );
	}

	public static function fetchPsAssetsByPsDocumentId( $intPsDocumentId, $objDatabase ) {
		return self::fetchPsAssets( sprintf( 'SELECT * FROM ps_assets WHERE ps_document_id = %d', ( int ) $intPsDocumentId ), $objDatabase );
	}

}
?>