<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPayrollPeriods
 * Do not add any new functions to this class.
 */

class CBasePayrollPeriods extends CEosPluralBase {

	/**
	 * @return CPayrollPeriod[]
	 */
	public static function fetchPayrollPeriods( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPayrollPeriod', $objDatabase );
	}

	/**
	 * @return CPayrollPeriod
	 */
	public static function fetchPayrollPeriod( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPayrollPeriod', $objDatabase );
	}

	public static function fetchPayrollPeriodCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'payroll_periods', $objDatabase );
	}

	public static function fetchPayrollPeriodById( $intId, $objDatabase ) {
		return self::fetchPayrollPeriod( sprintf( 'SELECT * FROM payroll_periods WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>