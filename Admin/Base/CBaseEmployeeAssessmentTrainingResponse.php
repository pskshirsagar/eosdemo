<?php

class CBaseEmployeeAssessmentTrainingResponse extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_assessment_training_responses';

	protected $m_intId;
	protected $m_intTrainingCategoryId;
	protected $m_intEmployeeAssessmentId;
	protected $m_intEmployeeId;
	protected $m_intEmployeeAssessmentQuestionId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['training_category_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingCategoryId', trim( $arrValues['training_category_id'] ) ); elseif( isset( $arrValues['training_category_id'] ) ) $this->setTrainingCategoryId( $arrValues['training_category_id'] );
		if( isset( $arrValues['employee_assessment_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeAssessmentId', trim( $arrValues['employee_assessment_id'] ) ); elseif( isset( $arrValues['employee_assessment_id'] ) ) $this->setEmployeeAssessmentId( $arrValues['employee_assessment_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['employee_assessment_question_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeAssessmentQuestionId', trim( $arrValues['employee_assessment_question_id'] ) ); elseif( isset( $arrValues['employee_assessment_question_id'] ) ) $this->setEmployeeAssessmentQuestionId( $arrValues['employee_assessment_question_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTrainingCategoryId( $intTrainingCategoryId ) {
		$this->set( 'm_intTrainingCategoryId', CStrings::strToIntDef( $intTrainingCategoryId, NULL, false ) );
	}

	public function getTrainingCategoryId() {
		return $this->m_intTrainingCategoryId;
	}

	public function sqlTrainingCategoryId() {
		return ( true == isset( $this->m_intTrainingCategoryId ) ) ? ( string ) $this->m_intTrainingCategoryId : 'NULL';
	}

	public function setEmployeeAssessmentId( $intEmployeeAssessmentId ) {
		$this->set( 'm_intEmployeeAssessmentId', CStrings::strToIntDef( $intEmployeeAssessmentId, NULL, false ) );
	}

	public function getEmployeeAssessmentId() {
		return $this->m_intEmployeeAssessmentId;
	}

	public function sqlEmployeeAssessmentId() {
		return ( true == isset( $this->m_intEmployeeAssessmentId ) ) ? ( string ) $this->m_intEmployeeAssessmentId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setEmployeeAssessmentQuestionId( $intEmployeeAssessmentQuestionId ) {
		$this->set( 'm_intEmployeeAssessmentQuestionId', CStrings::strToIntDef( $intEmployeeAssessmentQuestionId, NULL, false ) );
	}

	public function getEmployeeAssessmentQuestionId() {
		return $this->m_intEmployeeAssessmentQuestionId;
	}

	public function sqlEmployeeAssessmentQuestionId() {
		return ( true == isset( $this->m_intEmployeeAssessmentQuestionId ) ) ? ( string ) $this->m_intEmployeeAssessmentQuestionId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, training_category_id, employee_assessment_id, employee_id, employee_assessment_question_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTrainingCategoryId() . ', ' .
 						$this->sqlEmployeeAssessmentId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlEmployeeAssessmentQuestionId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_category_id = ' . $this->sqlTrainingCategoryId() . ','; } elseif( true == array_key_exists( 'TrainingCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' training_category_id = ' . $this->sqlTrainingCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_assessment_id = ' . $this->sqlEmployeeAssessmentId() . ','; } elseif( true == array_key_exists( 'EmployeeAssessmentId', $this->getChangedColumns() ) ) { $strSql .= ' employee_assessment_id = ' . $this->sqlEmployeeAssessmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_assessment_question_id = ' . $this->sqlEmployeeAssessmentQuestionId() . ','; } elseif( true == array_key_exists( 'EmployeeAssessmentQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' employee_assessment_question_id = ' . $this->sqlEmployeeAssessmentQuestionId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'training_category_id' => $this->getTrainingCategoryId(),
			'employee_assessment_id' => $this->getEmployeeAssessmentId(),
			'employee_id' => $this->getEmployeeId(),
			'employee_assessment_question_id' => $this->getEmployeeAssessmentQuestionId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>