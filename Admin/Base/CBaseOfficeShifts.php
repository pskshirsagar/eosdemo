<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\COfficeShifts
 * Do not add any new functions to this class.
 */

class CBaseOfficeShifts extends CEosPluralBase {

	/**
	 * @return COfficeShift[]
	 */
	public static function fetchOfficeShifts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, COfficeShift::class, $objDatabase );
	}

	/**
	 * @return COfficeShift
	 */
	public static function fetchOfficeShift( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COfficeShift::class, $objDatabase );
	}

	public static function fetchOfficeShiftCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'office_shifts', $objDatabase );
	}

	public static function fetchOfficeShiftById( $intId, $objDatabase ) {
		return self::fetchOfficeShift( sprintf( 'SELECT * FROM office_shifts WHERE id = %d', $intId ), $objDatabase );
	}

}
?>