<?php

class CBaseSurveyTemplateEmail extends CEosSingularBase {

	const TABLE_NAME = 'public.survey_template_emails';

	protected $m_intId;
	protected $m_strEmailTitle;
	protected $m_strEmailContent;
	protected $m_strEmailAddresses;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['email_title'] ) && $boolDirectSet ) $this->set( 'm_strEmailTitle', trim( stripcslashes( $arrValues['email_title'] ) ) ); elseif( isset( $arrValues['email_title'] ) ) $this->setEmailTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_title'] ) : $arrValues['email_title'] );
		if( isset( $arrValues['email_content'] ) && $boolDirectSet ) $this->set( 'm_strEmailContent', trim( stripcslashes( $arrValues['email_content'] ) ) ); elseif( isset( $arrValues['email_content'] ) ) $this->setEmailContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_content'] ) : $arrValues['email_content'] );
		if( isset( $arrValues['email_addresses'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddresses', trim( stripcslashes( $arrValues['email_addresses'] ) ) ); elseif( isset( $arrValues['email_addresses'] ) ) $this->setEmailAddresses( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_addresses'] ) : $arrValues['email_addresses'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmailTitle( $strEmailTitle ) {
		$this->set( 'm_strEmailTitle', CStrings::strTrimDef( $strEmailTitle, -1, NULL, true ) );
	}

	public function getEmailTitle() {
		return $this->m_strEmailTitle;
	}

	public function sqlEmailTitle() {
		return ( true == isset( $this->m_strEmailTitle ) ) ? '\'' . addslashes( $this->m_strEmailTitle ) . '\'' : 'NULL';
	}

	public function setEmailContent( $strEmailContent ) {
		$this->set( 'm_strEmailContent', CStrings::strTrimDef( $strEmailContent, -1, NULL, true ) );
	}

	public function getEmailContent() {
		return $this->m_strEmailContent;
	}

	public function sqlEmailContent() {
		return ( true == isset( $this->m_strEmailContent ) ) ? '\'' . addslashes( $this->m_strEmailContent ) . '\'' : 'NULL';
	}

	public function setEmailAddresses( $strEmailAddresses ) {
		$this->set( 'm_strEmailAddresses', CStrings::strTrimDef( $strEmailAddresses, -1, NULL, true ) );
	}

	public function getEmailAddresses() {
		return $this->m_strEmailAddresses;
	}

	public function sqlEmailAddresses() {
		return ( true == isset( $this->m_strEmailAddresses ) ) ? '\'' . addslashes( $this->m_strEmailAddresses ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, email_title, email_content, email_addresses, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmailTitle() . ', ' .
 						$this->sqlEmailContent() . ', ' .
 						$this->sqlEmailAddresses() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_title = ' . $this->sqlEmailTitle() . ','; } elseif( true == array_key_exists( 'EmailTitle', $this->getChangedColumns() ) ) { $strSql .= ' email_title = ' . $this->sqlEmailTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_content = ' . $this->sqlEmailContent() . ','; } elseif( true == array_key_exists( 'EmailContent', $this->getChangedColumns() ) ) { $strSql .= ' email_content = ' . $this->sqlEmailContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_addresses = ' . $this->sqlEmailAddresses() . ','; } elseif( true == array_key_exists( 'EmailAddresses', $this->getChangedColumns() ) ) { $strSql .= ' email_addresses = ' . $this->sqlEmailAddresses() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'email_title' => $this->getEmailTitle(),
			'email_content' => $this->getEmailContent(),
			'email_addresses' => $this->getEmailAddresses(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>