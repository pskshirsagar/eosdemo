<?php

class CBaseVirtualForeignKeyLog extends CEosSingularBase {

	const TABLE_NAME = 'public.virtual_foreign_key_logs';

	protected $m_intId;
	protected $m_intDatabaseId;
	protected $m_intVirtualForeignKeyId;
	protected $m_strLogDatetime;
	protected $m_intErrorCount;
	protected $m_strInvalidValues;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intErrorCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['virtual_foreign_key_id'] ) && $boolDirectSet ) $this->set( 'm_intVirtualForeignKeyId', trim( $arrValues['virtual_foreign_key_id'] ) ); elseif( isset( $arrValues['virtual_foreign_key_id'] ) ) $this->setVirtualForeignKeyId( $arrValues['virtual_foreign_key_id'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['error_count'] ) && $boolDirectSet ) $this->set( 'm_intErrorCount', trim( $arrValues['error_count'] ) ); elseif( isset( $arrValues['error_count'] ) ) $this->setErrorCount( $arrValues['error_count'] );
		if( isset( $arrValues['invalid_values'] ) && $boolDirectSet ) $this->set( 'm_strInvalidValues', trim( stripcslashes( $arrValues['invalid_values'] ) ) ); elseif( isset( $arrValues['invalid_values'] ) ) $this->setInvalidValues( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['invalid_values'] ) : $arrValues['invalid_values'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : 'NULL';
	}

	public function setVirtualForeignKeyId( $intVirtualForeignKeyId ) {
		$this->set( 'm_intVirtualForeignKeyId', CStrings::strToIntDef( $intVirtualForeignKeyId, NULL, false ) );
	}

	public function getVirtualForeignKeyId() {
		return $this->m_intVirtualForeignKeyId;
	}

	public function sqlVirtualForeignKeyId() {
		return ( true == isset( $this->m_intVirtualForeignKeyId ) ) ? ( string ) $this->m_intVirtualForeignKeyId : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setErrorCount( $intErrorCount ) {
		$this->set( 'm_intErrorCount', CStrings::strToIntDef( $intErrorCount, NULL, false ) );
	}

	public function getErrorCount() {
		return $this->m_intErrorCount;
	}

	public function sqlErrorCount() {
		return ( true == isset( $this->m_intErrorCount ) ) ? ( string ) $this->m_intErrorCount : '0';
	}

	public function setInvalidValues( $strInvalidValues ) {
		$this->set( 'm_strInvalidValues', CStrings::strTrimDef( $strInvalidValues, -1, NULL, true ) );
	}

	public function getInvalidValues() {
		return $this->m_strInvalidValues;
	}

	public function sqlInvalidValues() {
		return ( true == isset( $this->m_strInvalidValues ) ) ? '\'' . addslashes( $this->m_strInvalidValues ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, database_id, virtual_foreign_key_id, log_datetime, error_count, invalid_values, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDatabaseId() . ', ' .
 						$this->sqlVirtualForeignKeyId() . ', ' .
 						$this->sqlLogDatetime() . ', ' .
 						$this->sqlErrorCount() . ', ' .
 						$this->sqlInvalidValues() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' virtual_foreign_key_id = ' . $this->sqlVirtualForeignKeyId() . ','; } elseif( true == array_key_exists( 'VirtualForeignKeyId', $this->getChangedColumns() ) ) { $strSql .= ' virtual_foreign_key_id = ' . $this->sqlVirtualForeignKeyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_count = ' . $this->sqlErrorCount() . ','; } elseif( true == array_key_exists( 'ErrorCount', $this->getChangedColumns() ) ) { $strSql .= ' error_count = ' . $this->sqlErrorCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invalid_values = ' . $this->sqlInvalidValues() . ','; } elseif( true == array_key_exists( 'InvalidValues', $this->getChangedColumns() ) ) { $strSql .= ' invalid_values = ' . $this->sqlInvalidValues() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'database_id' => $this->getDatabaseId(),
			'virtual_foreign_key_id' => $this->getVirtualForeignKeyId(),
			'log_datetime' => $this->getLogDatetime(),
			'error_count' => $this->getErrorCount(),
			'invalid_values' => $this->getInvalidValues(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>