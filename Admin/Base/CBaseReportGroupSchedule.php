<?php

class CBaseReportGroupSchedule extends CEosSingularBase {

	const TABLE_NAME = 'public.report_group_schedules';

	protected $m_intId;
	protected $m_intGroupScheduleTypeId;
	protected $m_intGroupId;
	protected $m_intDayOfTheMonth;
	protected $m_strScheduleDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['group_schedule_type_id'] ) && $boolDirectSet ) $this->set( 'm_intGroupScheduleTypeId', trim( $arrValues['group_schedule_type_id'] ) ); elseif( isset( $arrValues['group_schedule_type_id'] ) ) $this->setGroupScheduleTypeId( $arrValues['group_schedule_type_id'] );
		if( isset( $arrValues['group_id'] ) && $boolDirectSet ) $this->set( 'm_intGroupId', trim( $arrValues['group_id'] ) ); elseif( isset( $arrValues['group_id'] ) ) $this->setGroupId( $arrValues['group_id'] );
		if( isset( $arrValues['day_of_the_month'] ) && $boolDirectSet ) $this->set( 'm_intDayOfTheMonth', trim( $arrValues['day_of_the_month'] ) ); elseif( isset( $arrValues['day_of_the_month'] ) ) $this->setDayOfTheMonth( $arrValues['day_of_the_month'] );
		if( isset( $arrValues['schedule_date'] ) && $boolDirectSet ) $this->set( 'm_strScheduleDate', trim( $arrValues['schedule_date'] ) ); elseif( isset( $arrValues['schedule_date'] ) ) $this->setScheduleDate( $arrValues['schedule_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setGroupScheduleTypeId( $intGroupScheduleTypeId ) {
		$this->set( 'm_intGroupScheduleTypeId', CStrings::strToIntDef( $intGroupScheduleTypeId, NULL, false ) );
	}

	public function getGroupScheduleTypeId() {
		return $this->m_intGroupScheduleTypeId;
	}

	public function sqlGroupScheduleTypeId() {
		return ( true == isset( $this->m_intGroupScheduleTypeId ) ) ? ( string ) $this->m_intGroupScheduleTypeId : 'NULL';
	}

	public function setGroupId( $intGroupId ) {
		$this->set( 'm_intGroupId', CStrings::strToIntDef( $intGroupId, NULL, false ) );
	}

	public function getGroupId() {
		return $this->m_intGroupId;
	}

	public function sqlGroupId() {
		return ( true == isset( $this->m_intGroupId ) ) ? ( string ) $this->m_intGroupId : 'NULL';
	}

	public function setDayOfTheMonth( $intDayOfTheMonth ) {
		$this->set( 'm_intDayOfTheMonth', CStrings::strToIntDef( $intDayOfTheMonth, NULL, false ) );
	}

	public function getDayOfTheMonth() {
		return $this->m_intDayOfTheMonth;
	}

	public function sqlDayOfTheMonth() {
		return ( true == isset( $this->m_intDayOfTheMonth ) ) ? ( string ) $this->m_intDayOfTheMonth : 'NULL';
	}

	public function setScheduleDate( $strScheduleDate ) {
		$this->set( 'm_strScheduleDate', CStrings::strTrimDef( $strScheduleDate, -1, NULL, true ) );
	}

	public function getScheduleDate() {
		return $this->m_strScheduleDate;
	}

	public function sqlScheduleDate() {
		return ( true == isset( $this->m_strScheduleDate ) ) ? '\'' . $this->m_strScheduleDate . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, group_schedule_type_id, group_id, day_of_the_month, schedule_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlGroupScheduleTypeId() . ', ' .
 						$this->sqlGroupId() . ', ' .
 						$this->sqlDayOfTheMonth() . ', ' .
 						$this->sqlScheduleDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_schedule_type_id = ' . $this->sqlGroupScheduleTypeId() . ','; } elseif( true == array_key_exists( 'GroupScheduleTypeId', $this->getChangedColumns() ) ) { $strSql .= ' group_schedule_type_id = ' . $this->sqlGroupScheduleTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_id = ' . $this->sqlGroupId() . ','; } elseif( true == array_key_exists( 'GroupId', $this->getChangedColumns() ) ) { $strSql .= ' group_id = ' . $this->sqlGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' day_of_the_month = ' . $this->sqlDayOfTheMonth() . ','; } elseif( true == array_key_exists( 'DayOfTheMonth', $this->getChangedColumns() ) ) { $strSql .= ' day_of_the_month = ' . $this->sqlDayOfTheMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' schedule_date = ' . $this->sqlScheduleDate() . ','; } elseif( true == array_key_exists( 'ScheduleDate', $this->getChangedColumns() ) ) { $strSql .= ' schedule_date = ' . $this->sqlScheduleDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'group_schedule_type_id' => $this->getGroupScheduleTypeId(),
			'group_id' => $this->getGroupId(),
			'day_of_the_month' => $this->getDayOfTheMonth(),
			'schedule_date' => $this->getScheduleDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>