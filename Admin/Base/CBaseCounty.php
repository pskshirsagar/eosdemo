<?php

class CBaseCounty extends CEosSingularBase {

	const TABLE_NAME = 'public.counties';

	protected $m_intId;
	protected $m_strStateCode;
	protected $m_strCountyName;
	protected $m_strCountyCode;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['county_name'] ) && $boolDirectSet ) $this->set( 'm_strCountyName', trim( stripcslashes( $arrValues['county_name'] ) ) ); elseif( isset( $arrValues['county_name'] ) ) $this->setCountyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['county_name'] ) : $arrValues['county_name'] );
		if( isset( $arrValues['county_code'] ) && $boolDirectSet ) $this->set( 'm_strCountyCode', trim( stripcslashes( $arrValues['county_code'] ) ) ); elseif( isset( $arrValues['county_code'] ) ) $this->setCountyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['county_code'] ) : $arrValues['county_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setCountyName( $strCountyName ) {
		$this->set( 'm_strCountyName', CStrings::strTrimDef( $strCountyName, 50, NULL, true ) );
	}

	public function getCountyName() {
		return $this->m_strCountyName;
	}

	public function sqlCountyName() {
		return ( true == isset( $this->m_strCountyName ) ) ? '\'' . addslashes( $this->m_strCountyName ) . '\'' : 'NULL';
	}

	public function setCountyCode( $strCountyCode ) {
		$this->set( 'm_strCountyCode', CStrings::strTrimDef( $strCountyCode, 10, NULL, true ) );
	}

	public function getCountyCode() {
		return $this->m_strCountyCode;
	}

	public function sqlCountyCode() {
		return ( true == isset( $this->m_strCountyCode ) ) ? '\'' . addslashes( $this->m_strCountyCode ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'state_code' => $this->getStateCode(),
			'county_name' => $this->getCountyName(),
			'county_code' => $this->getCountyCode()
		);
	}

}
?>