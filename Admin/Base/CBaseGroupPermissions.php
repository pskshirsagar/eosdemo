<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroupPermissions
 * Do not add any new functions to this class.
 */

class CBaseGroupPermissions extends CEosPluralBase {

	/**
	 * @return CGroupPermission[]
	 */
	public static function fetchGroupPermissions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CGroupPermission', $objDatabase );
	}

	/**
	 * @return CGroupPermission
	 */
	public static function fetchGroupPermission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGroupPermission', $objDatabase );
	}

	public static function fetchGroupPermissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'group_permissions', $objDatabase );
	}

	public static function fetchGroupPermissionById( $intId, $objDatabase ) {
		return self::fetchGroupPermission( sprintf( 'SELECT * FROM group_permissions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchGroupPermissionsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchGroupPermissions( sprintf( 'SELECT * FROM group_permissions WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

	public static function fetchGroupPermissionsByPsModuleId( $intPsModuleId, $objDatabase ) {
		return self::fetchGroupPermissions( sprintf( 'SELECT * FROM group_permissions WHERE ps_module_id = %d', ( int ) $intPsModuleId ), $objDatabase );
	}

}
?>