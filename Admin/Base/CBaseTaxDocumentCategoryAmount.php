<?php

class CBaseTaxDocumentCategoryAmount extends CEosSingularBase {

	const TABLE_NAME = 'public.tax_document_category_amounts';

	protected $m_intId;
	protected $m_intEmployeeDocumentCategoryId;
	protected $m_intFinancialYearId;
	protected $m_fltMaximumAmount;
	protected $m_strSubAmounts;
	protected $m_jsonSubAmounts;
	protected $m_strNotes;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_document_category_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeDocumentCategoryId', trim( $arrValues['employee_document_category_id'] ) ); elseif( isset( $arrValues['employee_document_category_id'] ) ) $this->setEmployeeDocumentCategoryId( $arrValues['employee_document_category_id'] );
		if( isset( $arrValues['financial_year_id'] ) && $boolDirectSet ) $this->set( 'm_intFinancialYearId', trim( $arrValues['financial_year_id'] ) ); elseif( isset( $arrValues['financial_year_id'] ) ) $this->setFinancialYearId( $arrValues['financial_year_id'] );
		if( isset( $arrValues['maximum_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMaximumAmount', trim( $arrValues['maximum_amount'] ) ); elseif( isset( $arrValues['maximum_amount'] ) ) $this->setMaximumAmount( $arrValues['maximum_amount'] );
		if( isset( $arrValues['sub_amounts'] ) ) $this->set( 'm_strSubAmounts', trim( $arrValues['sub_amounts'] ) );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeDocumentCategoryId( $intEmployeeDocumentCategoryId ) {
		$this->set( 'm_intEmployeeDocumentCategoryId', CStrings::strToIntDef( $intEmployeeDocumentCategoryId, NULL, false ) );
	}

	public function getEmployeeDocumentCategoryId() {
		return $this->m_intEmployeeDocumentCategoryId;
	}

	public function sqlEmployeeDocumentCategoryId() {
		return ( true == isset( $this->m_intEmployeeDocumentCategoryId ) ) ? ( string ) $this->m_intEmployeeDocumentCategoryId : 'NULL';
	}

	public function setFinancialYearId( $intFinancialYearId ) {
		$this->set( 'm_intFinancialYearId', CStrings::strToIntDef( $intFinancialYearId, NULL, false ) );
	}

	public function getFinancialYearId() {
		return $this->m_intFinancialYearId;
	}

	public function sqlFinancialYearId() {
		return ( true == isset( $this->m_intFinancialYearId ) ) ? ( string ) $this->m_intFinancialYearId : 'NULL';
	}

	public function setMaximumAmount( $fltMaximumAmount ) {
		$this->set( 'm_fltMaximumAmount', CStrings::strToFloatDef( $fltMaximumAmount, NULL, false, 2 ) );
	}

	public function getMaximumAmount() {
		return $this->m_fltMaximumAmount;
	}

	public function sqlMaximumAmount() {
		return ( true == isset( $this->m_fltMaximumAmount ) ) ? ( string ) $this->m_fltMaximumAmount : 'NULL';
	}

	public function setSubAmounts( $jsonSubAmounts ) {
		if( true == valObj( $jsonSubAmounts, 'stdClass' ) ) {
			$this->set( 'm_jsonSubAmounts', $jsonSubAmounts );
		} elseif( true == valJsonString( $jsonSubAmounts ) ) {
			$this->set( 'm_jsonSubAmounts', CStrings::strToJson( $jsonSubAmounts ) );
		} else {
			$this->set( 'm_jsonSubAmounts', NULL ); 
		}
		unset( $this->m_strSubAmounts );
	}

	public function getSubAmounts() {
		if( true == isset( $this->m_strSubAmounts ) ) {
			$this->m_jsonSubAmounts = CStrings::strToJson( $this->m_strSubAmounts );
			unset( $this->m_strSubAmounts );
		}
		return $this->m_jsonSubAmounts;
	}

	public function sqlSubAmounts() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getSubAmounts() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getSubAmounts() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getSubAmounts() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_document_category_id, financial_year_id, maximum_amount, sub_amounts, notes, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeDocumentCategoryId() . ', ' .
						$this->sqlFinancialYearId() . ', ' .
						$this->sqlMaximumAmount() . ', ' .
						$this->sqlSubAmounts() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_document_category_id = ' . $this->sqlEmployeeDocumentCategoryId(). ',' ; } elseif( true == array_key_exists( 'EmployeeDocumentCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' employee_document_category_id = ' . $this->sqlEmployeeDocumentCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_year_id = ' . $this->sqlFinancialYearId(). ',' ; } elseif( true == array_key_exists( 'FinancialYearId', $this->getChangedColumns() ) ) { $strSql .= ' financial_year_id = ' . $this->sqlFinancialYearId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maximum_amount = ' . $this->sqlMaximumAmount(). ',' ; } elseif( true == array_key_exists( 'MaximumAmount', $this->getChangedColumns() ) ) { $strSql .= ' maximum_amount = ' . $this->sqlMaximumAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sub_amounts = ' . $this->sqlSubAmounts(). ',' ; } elseif( true == array_key_exists( 'SubAmounts', $this->getChangedColumns() ) ) { $strSql .= ' sub_amounts = ' . $this->sqlSubAmounts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_document_category_id' => $this->getEmployeeDocumentCategoryId(),
			'financial_year_id' => $this->getFinancialYearId(),
			'maximum_amount' => $this->getMaximumAmount(),
			'sub_amounts' => $this->getSubAmounts(),
			'notes' => $this->getNotes(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>