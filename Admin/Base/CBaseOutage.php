<?php

class CBaseOutage extends CEosSingularBase {

	const TABLE_NAME = 'public.outages';

	protected $m_intId;
	protected $m_intTaskId;
	protected $m_strOutageDatetime;
	protected $m_strOutageDetails;
	protected $m_jsonOutageDetails;
	protected $m_boolIsCompanyGoalAffected;
	protected $m_boolIsEndUserAffected;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsCompanyGoalAffected = false;
		$this->m_boolIsEndUserAffected = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['outage_datetime'] ) && $boolDirectSet ) $this->set( 'm_strOutageDatetime', trim( $arrValues['outage_datetime'] ) ); elseif( isset( $arrValues['outage_datetime'] ) ) $this->setOutageDatetime( $arrValues['outage_datetime'] );
		if( isset( $arrValues['outage_details'] ) ) $this->set( 'm_strOutageDetails', trim( $arrValues['outage_details'] ) );
		if( isset( $arrValues['is_company_goal_affected'] ) && $boolDirectSet ) $this->set( 'm_boolIsCompanyGoalAffected', trim( stripcslashes( $arrValues['is_company_goal_affected'] ) ) ); elseif( isset( $arrValues['is_company_goal_affected'] ) ) $this->setIsCompanyGoalAffected( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_company_goal_affected'] ) : $arrValues['is_company_goal_affected'] );
		if( isset( $arrValues['is_end_user_affected'] ) && $boolDirectSet ) $this->set( 'm_boolIsEndUserAffected', trim( stripcslashes( $arrValues['is_end_user_affected'] ) ) ); elseif( isset( $arrValues['is_end_user_affected'] ) ) $this->setIsEndUserAffected( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_end_user_affected'] ) : $arrValues['is_end_user_affected'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setOutageDatetime( $strOutageDatetime ) {
		$this->set( 'm_strOutageDatetime', CStrings::strTrimDef( $strOutageDatetime, -1, NULL, true ) );
	}

	public function getOutageDatetime() {
		return $this->m_strOutageDatetime;
	}

	public function sqlOutageDatetime() {
		return ( true == isset( $this->m_strOutageDatetime ) ) ? '\'' . $this->m_strOutageDatetime . '\'' : 'NULL';
	}

	public function setOutageDetails( $jsonOutageDetails ) {
		if( true == valObj( $jsonOutageDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonOutageDetails', $jsonOutageDetails );
		} elseif( true == valJsonString( $jsonOutageDetails ) ) {
			$this->set( 'm_jsonOutageDetails', CStrings::strToJson( $jsonOutageDetails ) );
		} else {
			$this->set( 'm_jsonOutageDetails', NULL ); 
		}
		unset( $this->m_strOutageDetails );
	}

	public function getOutageDetails() {
		if( true == isset( $this->m_strOutageDetails ) ) {
			$this->m_jsonOutageDetails = CStrings::strToJson( $this->m_strOutageDetails );
			unset( $this->m_strOutageDetails );
		}
		return $this->m_jsonOutageDetails;
	}

	public function sqlOutageDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getOutageDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getOutageDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setIsCompanyGoalAffected( $boolIsCompanyGoalAffected ) {
		$this->set( 'm_boolIsCompanyGoalAffected', CStrings::strToBool( $boolIsCompanyGoalAffected ) );
	}

	public function getIsCompanyGoalAffected() {
		return $this->m_boolIsCompanyGoalAffected;
	}

	public function sqlIsCompanyGoalAffected() {
		return ( true == isset( $this->m_boolIsCompanyGoalAffected ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCompanyGoalAffected ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsEndUserAffected( $boolIsEndUserAffected ) {
		$this->set( 'm_boolIsEndUserAffected', CStrings::strToBool( $boolIsEndUserAffected ) );
	}

	public function getIsEndUserAffected() {
		return $this->m_boolIsEndUserAffected;
	}

	public function sqlIsEndUserAffected() {
		return ( true == isset( $this->m_boolIsEndUserAffected ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEndUserAffected ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, task_id, outage_datetime, outage_details, is_company_goal_affected, is_end_user_affected, completed_by, completed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlOutageDatetime() . ', ' .
 						$this->sqlOutageDetails() . ', ' .
 						$this->sqlIsCompanyGoalAffected() . ', ' .
 						$this->sqlIsEndUserAffected() . ', ' .
 						$this->sqlCompletedBy() . ', ' .
 						$this->sqlCompletedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' outage_datetime = ' . $this->sqlOutageDatetime() . ','; } elseif( true == array_key_exists( 'OutageDatetime', $this->getChangedColumns() ) ) { $strSql .= ' outage_datetime = ' . $this->sqlOutageDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' outage_details = ' . $this->sqlOutageDetails() . ','; } elseif( true == array_key_exists( 'OutageDetails', $this->getChangedColumns() ) ) { $strSql .= ' outage_details = ' . $this->sqlOutageDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_company_goal_affected = ' . $this->sqlIsCompanyGoalAffected() . ','; } elseif( true == array_key_exists( 'IsCompanyGoalAffected', $this->getChangedColumns() ) ) { $strSql .= ' is_company_goal_affected = ' . $this->sqlIsCompanyGoalAffected() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_end_user_affected = ' . $this->sqlIsEndUserAffected() . ','; } elseif( true == array_key_exists( 'IsEndUserAffected', $this->getChangedColumns() ) ) { $strSql .= ' is_end_user_affected = ' . $this->sqlIsEndUserAffected() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'task_id' => $this->getTaskId(),
			'outage_datetime' => $this->getOutageDatetime(),
			'outage_details' => $this->getOutageDetails(),
			'is_company_goal_affected' => $this->getIsCompanyGoalAffected(),
			'is_end_user_affected' => $this->getIsEndUserAffected(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>