<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CJobPostingQuestionResponses
 * Do not add any new functions to this class.
 */

class CBaseJobPostingQuestionResponses extends CEosPluralBase {

	/**
	 * @return CJobPostingQuestionResponse[]
	 */
	public static function fetchJobPostingQuestionResponses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CJobPostingQuestionResponse', $objDatabase );
	}

	/**
	 * @return CJobPostingQuestionResponse
	 */
	public static function fetchJobPostingQuestionResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobPostingQuestionResponse', $objDatabase );
	}

	public static function fetchJobPostingQuestionResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_posting_question_responses', $objDatabase );
	}

	public static function fetchJobPostingQuestionResponseById( $intId, $objDatabase ) {
		return self::fetchJobPostingQuestionResponse( sprintf( 'SELECT * FROM job_posting_question_responses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchJobPostingQuestionResponsesByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchJobPostingQuestionResponses( sprintf( 'SELECT * FROM job_posting_question_responses WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchJobPostingQuestionResponsesByQuestionId( $intQuestionId, $objDatabase ) {
		return self::fetchJobPostingQuestionResponses( sprintf( 'SELECT * FROM job_posting_question_responses WHERE question_id = %d', ( int ) $intQuestionId ), $objDatabase );
	}

}
?>