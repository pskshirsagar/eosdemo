<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplateComponents
 * Do not add any new functions to this class.
 */

class CBaseDocumentTemplateComponents extends CEosPluralBase {

	/**
	 * @return CDocumentTemplateComponent[]
	 */
	public static function fetchDocumentTemplateComponents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDocumentTemplateComponent', $objDatabase );
	}

	/**
	 * @return CDocumentTemplateComponent
	 */
	public static function fetchDocumentTemplateComponent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentTemplateComponent', $objDatabase );
	}

	public static function fetchDocumentTemplateComponentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_template_components', $objDatabase );
	}

	public static function fetchDocumentTemplateComponentById( $intId, $objDatabase ) {
		return self::fetchDocumentTemplateComponent( sprintf( 'SELECT * FROM document_template_components WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDocumentTemplateComponentsByDocumentTemplateId( $intDocumentTemplateId, $objDatabase ) {
		return self::fetchDocumentTemplateComponents( sprintf( 'SELECT * FROM document_template_components WHERE document_template_id = %d', ( int ) $intDocumentTemplateId ), $objDatabase );
	}

	public static function fetchDocumentTemplateComponentsByDocumentTemplateComponentId( $intDocumentTemplateComponentId, $objDatabase ) {
		return self::fetchDocumentTemplateComponents( sprintf( 'SELECT * FROM document_template_components WHERE document_template_component_id = %d', ( int ) $intDocumentTemplateComponentId ), $objDatabase );
	}

	public static function fetchDocumentTemplateComponentsByArchivedTemplateComponentId( $intArchivedTemplateComponentId, $objDatabase ) {
		return self::fetchDocumentTemplateComponents( sprintf( 'SELECT * FROM document_template_components WHERE archived_template_component_id = %d', ( int ) $intArchivedTemplateComponentId ), $objDatabase );
	}

}
?>