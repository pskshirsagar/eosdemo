<?php

class CBaseContractProduct extends CEosSingularBase {

	const TABLE_NAME = 'public.contract_products';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intContractId;
	protected $m_strCurrencyCode;
	protected $m_intPsProductId;
	protected $m_intCommissionBucketId;
	protected $m_intTrainingEmployeeId;
	protected $m_intImplementationEmployeeId;
	protected $m_intProjectManagerEmployeeId;
	protected $m_intFrequencyId;
	protected $m_intBundlePsProductId;
	protected $m_strItemDatetime;
	protected $m_fltTrainingAmount;
	protected $m_fltImplementationAmount;
	protected $m_fltRecurringAmount;
	protected $m_fltTransactionalAmount;
	protected $m_intMinUnits;
	protected $m_intMaxUnits;
	protected $m_fltSmallMonthlyPerUnitRecurring;
	protected $m_intEstimatedPropertyCount;
	protected $m_intEstimatedUnitCount;
	protected $m_intOverrideRevenueType;
	protected $m_intTrainingHours;
	protected $m_intImplementationHours;
	protected $m_strRecurringStartDate;
	protected $m_strImplementationPostDate;
	protected $m_strTrainingPostDate;
	protected $m_intUniversalUnitLimit;
	protected $m_intAutoBill;
	protected $m_intIsUniversal;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intPackageProductId;
	protected $m_strDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';
		$this->m_intCommissionBucketId = '1';
		$this->m_fltTrainingAmount = '0';
		$this->m_fltImplementationAmount = '0';
		$this->m_fltRecurringAmount = '0';
		$this->m_fltTransactionalAmount = '0';
		$this->m_intOverrideRevenueType = '0';
		$this->m_intAutoBill = '1';
		$this->m_intIsUniversal = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( $arrValues['currency_code'] ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( $arrValues['currency_code'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['commission_bucket_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionBucketId', trim( $arrValues['commission_bucket_id'] ) ); elseif( isset( $arrValues['commission_bucket_id'] ) ) $this->setCommissionBucketId( $arrValues['commission_bucket_id'] );
		if( isset( $arrValues['training_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingEmployeeId', trim( $arrValues['training_employee_id'] ) ); elseif( isset( $arrValues['training_employee_id'] ) ) $this->setTrainingEmployeeId( $arrValues['training_employee_id'] );
		if( isset( $arrValues['implementation_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationEmployeeId', trim( $arrValues['implementation_employee_id'] ) ); elseif( isset( $arrValues['implementation_employee_id'] ) ) $this->setImplementationEmployeeId( $arrValues['implementation_employee_id'] );
		if( isset( $arrValues['project_manager_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intProjectManagerEmployeeId', trim( $arrValues['project_manager_employee_id'] ) ); elseif( isset( $arrValues['project_manager_employee_id'] ) ) $this->setProjectManagerEmployeeId( $arrValues['project_manager_employee_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['bundle_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intBundlePsProductId', trim( $arrValues['bundle_ps_product_id'] ) ); elseif( isset( $arrValues['bundle_ps_product_id'] ) ) $this->setBundlePsProductId( $arrValues['bundle_ps_product_id'] );
		if( isset( $arrValues['item_datetime'] ) && $boolDirectSet ) $this->set( 'm_strItemDatetime', trim( $arrValues['item_datetime'] ) ); elseif( isset( $arrValues['item_datetime'] ) ) $this->setItemDatetime( $arrValues['item_datetime'] );
		if( isset( $arrValues['training_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTrainingAmount', trim( $arrValues['training_amount'] ) ); elseif( isset( $arrValues['training_amount'] ) ) $this->setTrainingAmount( $arrValues['training_amount'] );
		if( isset( $arrValues['implementation_amount'] ) && $boolDirectSet ) $this->set( 'm_fltImplementationAmount', trim( $arrValues['implementation_amount'] ) ); elseif( isset( $arrValues['implementation_amount'] ) ) $this->setImplementationAmount( $arrValues['implementation_amount'] );
		if( isset( $arrValues['recurring_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRecurringAmount', trim( $arrValues['recurring_amount'] ) ); elseif( isset( $arrValues['recurring_amount'] ) ) $this->setRecurringAmount( $arrValues['recurring_amount'] );
		if( isset( $arrValues['transactional_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionalAmount', trim( $arrValues['transactional_amount'] ) ); elseif( isset( $arrValues['transactional_amount'] ) ) $this->setTransactionalAmount( $arrValues['transactional_amount'] );
		if( isset( $arrValues['min_units'] ) && $boolDirectSet ) $this->set( 'm_intMinUnits', trim( $arrValues['min_units'] ) ); elseif( isset( $arrValues['min_units'] ) ) $this->setMinUnits( $arrValues['min_units'] );
		if( isset( $arrValues['max_units'] ) && $boolDirectSet ) $this->set( 'm_intMaxUnits', trim( $arrValues['max_units'] ) ); elseif( isset( $arrValues['max_units'] ) ) $this->setMaxUnits( $arrValues['max_units'] );
		if( isset( $arrValues['small_monthly_per_unit_recurring'] ) && $boolDirectSet ) $this->set( 'm_fltSmallMonthlyPerUnitRecurring', trim( $arrValues['small_monthly_per_unit_recurring'] ) ); elseif( isset( $arrValues['small_monthly_per_unit_recurring'] ) ) $this->setSmallMonthlyPerUnitRecurring( $arrValues['small_monthly_per_unit_recurring'] );
		if( isset( $arrValues['estimated_property_count'] ) && $boolDirectSet ) $this->set( 'm_intEstimatedPropertyCount', trim( $arrValues['estimated_property_count'] ) ); elseif( isset( $arrValues['estimated_property_count'] ) ) $this->setEstimatedPropertyCount( $arrValues['estimated_property_count'] );
		if( isset( $arrValues['estimated_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intEstimatedUnitCount', trim( $arrValues['estimated_unit_count'] ) ); elseif( isset( $arrValues['estimated_unit_count'] ) ) $this->setEstimatedUnitCount( $arrValues['estimated_unit_count'] );
		if( isset( $arrValues['override_revenue_type'] ) && $boolDirectSet ) $this->set( 'm_intOverrideRevenueType', trim( $arrValues['override_revenue_type'] ) ); elseif( isset( $arrValues['override_revenue_type'] ) ) $this->setOverrideRevenueType( $arrValues['override_revenue_type'] );
		if( isset( $arrValues['training_hours'] ) && $boolDirectSet ) $this->set( 'm_intTrainingHours', trim( $arrValues['training_hours'] ) ); elseif( isset( $arrValues['training_hours'] ) ) $this->setTrainingHours( $arrValues['training_hours'] );
		if( isset( $arrValues['implementation_hours'] ) && $boolDirectSet ) $this->set( 'm_intImplementationHours', trim( $arrValues['implementation_hours'] ) ); elseif( isset( $arrValues['implementation_hours'] ) ) $this->setImplementationHours( $arrValues['implementation_hours'] );
		if( isset( $arrValues['recurring_start_date'] ) && $boolDirectSet ) $this->set( 'm_strRecurringStartDate', trim( $arrValues['recurring_start_date'] ) ); elseif( isset( $arrValues['recurring_start_date'] ) ) $this->setRecurringStartDate( $arrValues['recurring_start_date'] );
		if( isset( $arrValues['implementation_post_date'] ) && $boolDirectSet ) $this->set( 'm_strImplementationPostDate', trim( $arrValues['implementation_post_date'] ) ); elseif( isset( $arrValues['implementation_post_date'] ) ) $this->setImplementationPostDate( $arrValues['implementation_post_date'] );
		if( isset( $arrValues['training_post_date'] ) && $boolDirectSet ) $this->set( 'm_strTrainingPostDate', trim( $arrValues['training_post_date'] ) ); elseif( isset( $arrValues['training_post_date'] ) ) $this->setTrainingPostDate( $arrValues['training_post_date'] );
		if( isset( $arrValues['universal_unit_limit'] ) && $boolDirectSet ) $this->set( 'm_intUniversalUnitLimit', trim( $arrValues['universal_unit_limit'] ) ); elseif( isset( $arrValues['universal_unit_limit'] ) ) $this->setUniversalUnitLimit( $arrValues['universal_unit_limit'] );
		if( isset( $arrValues['auto_bill'] ) && $boolDirectSet ) $this->set( 'm_intAutoBill', trim( $arrValues['auto_bill'] ) ); elseif( isset( $arrValues['auto_bill'] ) ) $this->setAutoBill( $arrValues['auto_bill'] );
		if( isset( $arrValues['is_universal'] ) && $boolDirectSet ) $this->set( 'm_intIsUniversal', trim( $arrValues['is_universal'] ) ); elseif( isset( $arrValues['is_universal'] ) ) $this->setIsUniversal( $arrValues['is_universal'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['package_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPackageProductId', trim( $arrValues['package_product_id'] ) ); elseif( isset( $arrValues['package_product_id'] ) ) $this->setPackageProductId( $arrValues['package_product_id'] );
		if( isset( $arrValues['details'] ) && $boolDirectSet ) $this->set( 'm_strDetails', trim( stripcslashes( $arrValues['details'] ) ) ); elseif( isset( $arrValues['details'] ) ) $this->setDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['details'] ) : $arrValues['details'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrencyCode ) : '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' ) : '\'USD\'';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setCommissionBucketId( $intCommissionBucketId ) {
		$this->set( 'm_intCommissionBucketId', CStrings::strToIntDef( $intCommissionBucketId, NULL, false ) );
	}

	public function getCommissionBucketId() {
		return $this->m_intCommissionBucketId;
	}

	public function sqlCommissionBucketId() {
		return ( true == isset( $this->m_intCommissionBucketId ) ) ? ( string ) $this->m_intCommissionBucketId : '1';
	}

	public function setTrainingEmployeeId( $intTrainingEmployeeId ) {
		$this->set( 'm_intTrainingEmployeeId', CStrings::strToIntDef( $intTrainingEmployeeId, NULL, false ) );
	}

	public function getTrainingEmployeeId() {
		return $this->m_intTrainingEmployeeId;
	}

	public function sqlTrainingEmployeeId() {
		return ( true == isset( $this->m_intTrainingEmployeeId ) ) ? ( string ) $this->m_intTrainingEmployeeId : 'NULL';
	}

	public function setImplementationEmployeeId( $intImplementationEmployeeId ) {
		$this->set( 'm_intImplementationEmployeeId', CStrings::strToIntDef( $intImplementationEmployeeId, NULL, false ) );
	}

	public function getImplementationEmployeeId() {
		return $this->m_intImplementationEmployeeId;
	}

	public function sqlImplementationEmployeeId() {
		return ( true == isset( $this->m_intImplementationEmployeeId ) ) ? ( string ) $this->m_intImplementationEmployeeId : 'NULL';
	}

	public function setProjectManagerEmployeeId( $intProjectManagerEmployeeId ) {
		$this->set( 'm_intProjectManagerEmployeeId', CStrings::strToIntDef( $intProjectManagerEmployeeId, NULL, false ) );
	}

	public function getProjectManagerEmployeeId() {
		return $this->m_intProjectManagerEmployeeId;
	}

	public function sqlProjectManagerEmployeeId() {
		return ( true == isset( $this->m_intProjectManagerEmployeeId ) ) ? ( string ) $this->m_intProjectManagerEmployeeId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setBundlePsProductId( $intBundlePsProductId ) {
		$this->set( 'm_intBundlePsProductId', CStrings::strToIntDef( $intBundlePsProductId, NULL, false ) );
	}

	public function getBundlePsProductId() {
		return $this->m_intBundlePsProductId;
	}

	public function sqlBundlePsProductId() {
		return ( true == isset( $this->m_intBundlePsProductId ) ) ? ( string ) $this->m_intBundlePsProductId : 'NULL';
	}

	public function setItemDatetime( $strItemDatetime ) {
		$this->set( 'm_strItemDatetime', CStrings::strTrimDef( $strItemDatetime, -1, NULL, true ) );
	}

	public function getItemDatetime() {
		return $this->m_strItemDatetime;
	}

	public function sqlItemDatetime() {
		return ( true == isset( $this->m_strItemDatetime ) ) ? '\'' . $this->m_strItemDatetime . '\'' : 'NULL';
	}

	public function setTrainingAmount( $fltTrainingAmount ) {
		$this->set( 'm_fltTrainingAmount', CStrings::strToFloatDef( $fltTrainingAmount, NULL, false, 2 ) );
	}

	public function getTrainingAmount() {
		return $this->m_fltTrainingAmount;
	}

	public function sqlTrainingAmount() {
		return ( true == isset( $this->m_fltTrainingAmount ) ) ? ( string ) $this->m_fltTrainingAmount : '0';
	}

	public function setImplementationAmount( $fltImplementationAmount ) {
		$this->set( 'm_fltImplementationAmount', CStrings::strToFloatDef( $fltImplementationAmount, NULL, false, 2 ) );
	}

	public function getImplementationAmount() {
		return $this->m_fltImplementationAmount;
	}

	public function sqlImplementationAmount() {
		return ( true == isset( $this->m_fltImplementationAmount ) ) ? ( string ) $this->m_fltImplementationAmount : '0';
	}

	public function setRecurringAmount( $fltRecurringAmount ) {
		$this->set( 'm_fltRecurringAmount', CStrings::strToFloatDef( $fltRecurringAmount, NULL, false, 2 ) );
	}

	public function getRecurringAmount() {
		return $this->m_fltRecurringAmount;
	}

	public function sqlRecurringAmount() {
		return ( true == isset( $this->m_fltRecurringAmount ) ) ? ( string ) $this->m_fltRecurringAmount : '0';
	}

	public function setTransactionalAmount( $fltTransactionalAmount ) {
		$this->set( 'm_fltTransactionalAmount', CStrings::strToFloatDef( $fltTransactionalAmount, NULL, false, 2 ) );
	}

	public function getTransactionalAmount() {
		return $this->m_fltTransactionalAmount;
	}

	public function sqlTransactionalAmount() {
		return ( true == isset( $this->m_fltTransactionalAmount ) ) ? ( string ) $this->m_fltTransactionalAmount : '0';
	}

	public function setMinUnits( $intMinUnits ) {
		$this->set( 'm_intMinUnits', CStrings::strToIntDef( $intMinUnits, NULL, false ) );
	}

	public function getMinUnits() {
		return $this->m_intMinUnits;
	}

	public function sqlMinUnits() {
		return ( true == isset( $this->m_intMinUnits ) ) ? ( string ) $this->m_intMinUnits : 'NULL';
	}

	public function setMaxUnits( $intMaxUnits ) {
		$this->set( 'm_intMaxUnits', CStrings::strToIntDef( $intMaxUnits, NULL, false ) );
	}

	public function getMaxUnits() {
		return $this->m_intMaxUnits;
	}

	public function sqlMaxUnits() {
		return ( true == isset( $this->m_intMaxUnits ) ) ? ( string ) $this->m_intMaxUnits : 'NULL';
	}

	public function setSmallMonthlyPerUnitRecurring( $fltSmallMonthlyPerUnitRecurring ) {
		$this->set( 'm_fltSmallMonthlyPerUnitRecurring', CStrings::strToFloatDef( $fltSmallMonthlyPerUnitRecurring, NULL, false, 2 ) );
	}

	public function getSmallMonthlyPerUnitRecurring() {
		return $this->m_fltSmallMonthlyPerUnitRecurring;
	}

	public function sqlSmallMonthlyPerUnitRecurring() {
		return ( true == isset( $this->m_fltSmallMonthlyPerUnitRecurring ) ) ? ( string ) $this->m_fltSmallMonthlyPerUnitRecurring : 'NULL';
	}

	public function setEstimatedPropertyCount( $intEstimatedPropertyCount ) {
		$this->set( 'm_intEstimatedPropertyCount', CStrings::strToIntDef( $intEstimatedPropertyCount, NULL, false ) );
	}

	public function getEstimatedPropertyCount() {
		return $this->m_intEstimatedPropertyCount;
	}

	public function sqlEstimatedPropertyCount() {
		return ( true == isset( $this->m_intEstimatedPropertyCount ) ) ? ( string ) $this->m_intEstimatedPropertyCount : 'NULL';
	}

	public function setEstimatedUnitCount( $intEstimatedUnitCount ) {
		$this->set( 'm_intEstimatedUnitCount', CStrings::strToIntDef( $intEstimatedUnitCount, NULL, false ) );
	}

	public function getEstimatedUnitCount() {
		return $this->m_intEstimatedUnitCount;
	}

	public function sqlEstimatedUnitCount() {
		return ( true == isset( $this->m_intEstimatedUnitCount ) ) ? ( string ) $this->m_intEstimatedUnitCount : 'NULL';
	}

	public function setOverrideRevenueType( $intOverrideRevenueType ) {
		$this->set( 'm_intOverrideRevenueType', CStrings::strToIntDef( $intOverrideRevenueType, NULL, false ) );
	}

	public function getOverrideRevenueType() {
		return $this->m_intOverrideRevenueType;
	}

	public function sqlOverrideRevenueType() {
		return ( true == isset( $this->m_intOverrideRevenueType ) ) ? ( string ) $this->m_intOverrideRevenueType : '0';
	}

	public function setTrainingHours( $intTrainingHours ) {
		$this->set( 'm_intTrainingHours', CStrings::strToIntDef( $intTrainingHours, NULL, false ) );
	}

	public function getTrainingHours() {
		return $this->m_intTrainingHours;
	}

	public function sqlTrainingHours() {
		return ( true == isset( $this->m_intTrainingHours ) ) ? ( string ) $this->m_intTrainingHours : 'NULL';
	}

	public function setImplementationHours( $intImplementationHours ) {
		$this->set( 'm_intImplementationHours', CStrings::strToIntDef( $intImplementationHours, NULL, false ) );
	}

	public function getImplementationHours() {
		return $this->m_intImplementationHours;
	}

	public function sqlImplementationHours() {
		return ( true == isset( $this->m_intImplementationHours ) ) ? ( string ) $this->m_intImplementationHours : 'NULL';
	}

	public function setRecurringStartDate( $strRecurringStartDate ) {
		$this->set( 'm_strRecurringStartDate', CStrings::strTrimDef( $strRecurringStartDate, -1, NULL, true ) );
	}

	public function getRecurringStartDate() {
		return $this->m_strRecurringStartDate;
	}

	public function sqlRecurringStartDate() {
		return ( true == isset( $this->m_strRecurringStartDate ) ) ? '\'' . $this->m_strRecurringStartDate . '\'' : 'NULL';
	}

	public function setImplementationPostDate( $strImplementationPostDate ) {
		$this->set( 'm_strImplementationPostDate', CStrings::strTrimDef( $strImplementationPostDate, -1, NULL, true ) );
	}

	public function getImplementationPostDate() {
		return $this->m_strImplementationPostDate;
	}

	public function sqlImplementationPostDate() {
		return ( true == isset( $this->m_strImplementationPostDate ) ) ? '\'' . $this->m_strImplementationPostDate . '\'' : 'NULL';
	}

	public function setTrainingPostDate( $strTrainingPostDate ) {
		$this->set( 'm_strTrainingPostDate', CStrings::strTrimDef( $strTrainingPostDate, -1, NULL, true ) );
	}

	public function getTrainingPostDate() {
		return $this->m_strTrainingPostDate;
	}

	public function sqlTrainingPostDate() {
		return ( true == isset( $this->m_strTrainingPostDate ) ) ? '\'' . $this->m_strTrainingPostDate . '\'' : 'NULL';
	}

	public function setUniversalUnitLimit( $intUniversalUnitLimit ) {
		$this->set( 'm_intUniversalUnitLimit', CStrings::strToIntDef( $intUniversalUnitLimit, NULL, false ) );
	}

	public function getUniversalUnitLimit() {
		return $this->m_intUniversalUnitLimit;
	}

	public function sqlUniversalUnitLimit() {
		return ( true == isset( $this->m_intUniversalUnitLimit ) ) ? ( string ) $this->m_intUniversalUnitLimit : 'NULL';
	}

	public function setAutoBill( $intAutoBill ) {
		$this->set( 'm_intAutoBill', CStrings::strToIntDef( $intAutoBill, NULL, false ) );
	}

	public function getAutoBill() {
		return $this->m_intAutoBill;
	}

	public function sqlAutoBill() {
		return ( true == isset( $this->m_intAutoBill ) ) ? ( string ) $this->m_intAutoBill : '1';
	}

	public function setIsUniversal( $intIsUniversal ) {
		$this->set( 'm_intIsUniversal', CStrings::strToIntDef( $intIsUniversal, NULL, false ) );
	}

	public function getIsUniversal() {
		return $this->m_intIsUniversal;
	}

	public function sqlIsUniversal() {
		return ( true == isset( $this->m_intIsUniversal ) ) ? ( string ) $this->m_intIsUniversal : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPackageProductId( $intLeadPackageId ) {
		$this->set( 'm_intPackageProductId', CStrings::strToIntDef( $intLeadPackageId, NULL, false ) );
	}

	public function getPackageProductId() {
		return $this->m_intPackageProductId;
	}

	public function sqlPackageProduct() {
		return ( true == isset( $this->m_intPackageProductId ) ) ? ( string ) $this->m_intPackageProductId : 'NULL';
	}

	public function setDetails( $strDetails ) {
		$this->set( 'm_strDetails', CStrings::strTrimDef( $strDetails, -1, NULL, true ) );
	}

	public function getDetails() {
		return $this->m_strDetails;
	}

	public function sqlDetails() {
		return ( true == isset( $this->m_strDetails ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDetails ) : '\'' . addslashes( $this->m_strDetails ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, contract_id, currency_code, ps_product_id, commission_bucket_id, training_employee_id, implementation_employee_id, project_manager_employee_id, frequency_id, bundle_ps_product_id, item_datetime, training_amount, implementation_amount, recurring_amount, transactional_amount, min_units, max_units, small_monthly_per_unit_recurring, estimated_property_count, estimated_unit_count, override_revenue_type, training_hours, implementation_hours, recurring_start_date, implementation_post_date, training_post_date, universal_unit_limit, auto_bill, is_universal, updated_by, updated_on, created_by, created_on, package_product_id, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlContractId() . ', ' .
						$this->sqlCurrencyCode() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlCommissionBucketId() . ', ' .
						$this->sqlTrainingEmployeeId() . ', ' .
						$this->sqlImplementationEmployeeId() . ', ' .
						$this->sqlProjectManagerEmployeeId() . ', ' .
						$this->sqlFrequencyId() . ', ' .
						$this->sqlBundlePsProductId() . ', ' .
						$this->sqlItemDatetime() . ', ' .
						$this->sqlTrainingAmount() . ', ' .
						$this->sqlImplementationAmount() . ', ' .
						$this->sqlRecurringAmount() . ', ' .
						$this->sqlTransactionalAmount() . ', ' .
						$this->sqlMinUnits() . ', ' .
						$this->sqlMaxUnits() . ', ' .
						$this->sqlSmallMonthlyPerUnitRecurring() . ', ' .
						$this->sqlEstimatedPropertyCount() . ', ' .
						$this->sqlEstimatedUnitCount() . ', ' .
						$this->sqlOverrideRevenueType() . ', ' .
						$this->sqlTrainingHours() . ', ' .
						$this->sqlImplementationHours() . ', ' .
						$this->sqlRecurringStartDate() . ', ' .
						$this->sqlImplementationPostDate() . ', ' .
						$this->sqlTrainingPostDate() . ', ' .
						$this->sqlUniversalUnitLimit() . ', ' .
						$this->sqlAutoBill() . ', ' .
						$this->sqlIsUniversal() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlPackageProduct() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId(). ',' ; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_bucket_id = ' . $this->sqlCommissionBucketId(). ',' ; } elseif( true == array_key_exists( 'CommissionBucketId', $this->getChangedColumns() ) ) { $strSql .= ' commission_bucket_id = ' . $this->sqlCommissionBucketId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_employee_id = ' . $this->sqlTrainingEmployeeId(). ',' ; } elseif( true == array_key_exists( 'TrainingEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' training_employee_id = ' . $this->sqlTrainingEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_employee_id = ' . $this->sqlImplementationEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ImplementationEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_employee_id = ' . $this->sqlImplementationEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_manager_employee_id = ' . $this->sqlProjectManagerEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ProjectManagerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' project_manager_employee_id = ' . $this->sqlProjectManagerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bundle_ps_product_id = ' . $this->sqlBundlePsProductId(). ',' ; } elseif( true == array_key_exists( 'BundlePsProductId', $this->getChangedColumns() ) ) { $strSql .= ' bundle_ps_product_id = ' . $this->sqlBundlePsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_datetime = ' . $this->sqlItemDatetime(). ',' ; } elseif( true == array_key_exists( 'ItemDatetime', $this->getChangedColumns() ) ) { $strSql .= ' item_datetime = ' . $this->sqlItemDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_amount = ' . $this->sqlTrainingAmount(). ',' ; } elseif( true == array_key_exists( 'TrainingAmount', $this->getChangedColumns() ) ) { $strSql .= ' training_amount = ' . $this->sqlTrainingAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_amount = ' . $this->sqlImplementationAmount(). ',' ; } elseif( true == array_key_exists( 'ImplementationAmount', $this->getChangedColumns() ) ) { $strSql .= ' implementation_amount = ' . $this->sqlImplementationAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recurring_amount = ' . $this->sqlRecurringAmount(). ',' ; } elseif( true == array_key_exists( 'RecurringAmount', $this->getChangedColumns() ) ) { $strSql .= ' recurring_amount = ' . $this->sqlRecurringAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transactional_amount = ' . $this->sqlTransactionalAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionalAmount', $this->getChangedColumns() ) ) { $strSql .= ' transactional_amount = ' . $this->sqlTransactionalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_units = ' . $this->sqlMinUnits(). ',' ; } elseif( true == array_key_exists( 'MinUnits', $this->getChangedColumns() ) ) { $strSql .= ' min_units = ' . $this->sqlMinUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_units = ' . $this->sqlMaxUnits(). ',' ; } elseif( true == array_key_exists( 'MaxUnits', $this->getChangedColumns() ) ) { $strSql .= ' max_units = ' . $this->sqlMaxUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' small_monthly_per_unit_recurring = ' . $this->sqlSmallMonthlyPerUnitRecurring(). ',' ; } elseif( true == array_key_exists( 'SmallMonthlyPerUnitRecurring', $this->getChangedColumns() ) ) { $strSql .= ' small_monthly_per_unit_recurring = ' . $this->sqlSmallMonthlyPerUnitRecurring() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimated_property_count = ' . $this->sqlEstimatedPropertyCount(). ',' ; } elseif( true == array_key_exists( 'EstimatedPropertyCount', $this->getChangedColumns() ) ) { $strSql .= ' estimated_property_count = ' . $this->sqlEstimatedPropertyCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimated_unit_count = ' . $this->sqlEstimatedUnitCount(). ',' ; } elseif( true == array_key_exists( 'EstimatedUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' estimated_unit_count = ' . $this->sqlEstimatedUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' override_revenue_type = ' . $this->sqlOverrideRevenueType(). ',' ; } elseif( true == array_key_exists( 'OverrideRevenueType', $this->getChangedColumns() ) ) { $strSql .= ' override_revenue_type = ' . $this->sqlOverrideRevenueType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_hours = ' . $this->sqlTrainingHours(). ',' ; } elseif( true == array_key_exists( 'TrainingHours', $this->getChangedColumns() ) ) { $strSql .= ' training_hours = ' . $this->sqlTrainingHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_hours = ' . $this->sqlImplementationHours(). ',' ; } elseif( true == array_key_exists( 'ImplementationHours', $this->getChangedColumns() ) ) { $strSql .= ' implementation_hours = ' . $this->sqlImplementationHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recurring_start_date = ' . $this->sqlRecurringStartDate(). ',' ; } elseif( true == array_key_exists( 'RecurringStartDate', $this->getChangedColumns() ) ) { $strSql .= ' recurring_start_date = ' . $this->sqlRecurringStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_post_date = ' . $this->sqlImplementationPostDate(). ',' ; } elseif( true == array_key_exists( 'ImplementationPostDate', $this->getChangedColumns() ) ) { $strSql .= ' implementation_post_date = ' . $this->sqlImplementationPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_post_date = ' . $this->sqlTrainingPostDate(). ',' ; } elseif( true == array_key_exists( 'TrainingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' training_post_date = ' . $this->sqlTrainingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' universal_unit_limit = ' . $this->sqlUniversalUnitLimit(). ',' ; } elseif( true == array_key_exists( 'UniversalUnitLimit', $this->getChangedColumns() ) ) { $strSql .= ' universal_unit_limit = ' . $this->sqlUniversalUnitLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_bill = ' . $this->sqlAutoBill(). ',' ; } elseif( true == array_key_exists( 'AutoBill', $this->getChangedColumns() ) ) { $strSql .= ' auto_bill = ' . $this->sqlAutoBill() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_universal = ' . $this->sqlIsUniversal(). ',' ; } elseif( true == array_key_exists( 'IsUniversal', $this->getChangedColumns() ) ) { $strSql .= ' is_universal = ' . $this->sqlIsUniversal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' package_product_id = ' . $this->sqlPackageProduct(). ',' ; } elseif( true == array_key_exists( 'PackageProductId', $this->getChangedColumns() ) ) { $strSql .= ' package_product_id = ' . $this->sqlPackageProduct() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'contract_id' => $this->getContractId(),
			'currency_code' => $this->getCurrencyCode(),
			'ps_product_id' => $this->getPsProductId(),
			'commission_bucket_id' => $this->getCommissionBucketId(),
			'training_employee_id' => $this->getTrainingEmployeeId(),
			'implementation_employee_id' => $this->getImplementationEmployeeId(),
			'project_manager_employee_id' => $this->getProjectManagerEmployeeId(),
			'frequency_id' => $this->getFrequencyId(),
			'bundle_ps_product_id' => $this->getBundlePsProductId(),
			'item_datetime' => $this->getItemDatetime(),
			'training_amount' => $this->getTrainingAmount(),
			'implementation_amount' => $this->getImplementationAmount(),
			'recurring_amount' => $this->getRecurringAmount(),
			'transactional_amount' => $this->getTransactionalAmount(),
			'min_units' => $this->getMinUnits(),
			'max_units' => $this->getMaxUnits(),
			'small_monthly_per_unit_recurring' => $this->getSmallMonthlyPerUnitRecurring(),
			'estimated_property_count' => $this->getEstimatedPropertyCount(),
			'estimated_unit_count' => $this->getEstimatedUnitCount(),
			'override_revenue_type' => $this->getOverrideRevenueType(),
			'training_hours' => $this->getTrainingHours(),
			'implementation_hours' => $this->getImplementationHours(),
			'recurring_start_date' => $this->getRecurringStartDate(),
			'implementation_post_date' => $this->getImplementationPostDate(),
			'training_post_date' => $this->getTrainingPostDate(),
			'universal_unit_limit' => $this->getUniversalUnitLimit(),
			'auto_bill' => $this->getAutoBill(),
			'is_universal' => $this->getIsUniversal(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'package_product_id' => $this->getPackageProductId(),
			'details' => $this->getDetails()
		);
	}

}
?>