<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTeamTypes
 * Do not add any new functions to this class.
 */

class CBaseTeamTypes extends CEosPluralBase {

	/**
	 * @return CTeamType[]
	 */
	public static function fetchTeamTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTeamType', $objDatabase );
	}

	/**
	 * @return CTeamType
	 */
	public static function fetchTeamType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTeamType', $objDatabase );
	}

	public static function fetchTeamTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'team_types', $objDatabase );
	}

	public static function fetchTeamTypeById( $intId, $objDatabase ) {
		return self::fetchTeamType( sprintf( 'SELECT * FROM team_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTeamTypesByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchTeamTypes( sprintf( 'SELECT * FROM team_types WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>