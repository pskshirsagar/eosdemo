<?php

class CBaseQuotaRequirement extends CEosSingularBase {

	const TABLE_NAME = 'public.quota_requirements';

	protected $m_intId;
	protected $m_intQuotaId;
	protected $m_intQuotaEmployeeId;
	protected $m_intPsLeadFilterId;
	protected $m_intPsProductId;
	protected $m_fltRequiredImplementationAmount;
	protected $m_fltActualImplementationAmount;
	protected $m_fltRequiredRecurringAmount;
	protected $m_fltActualRecurringAmount;
	protected $m_fltRequiredTransactionalAmount;
	protected $m_fltActualTransactionalAmount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltRequiredImplementationAmount = '0';
		$this->m_fltActualImplementationAmount = '0';
		$this->m_fltRequiredRecurringAmount = '0';
		$this->m_fltActualRecurringAmount = '0';
		$this->m_fltRequiredTransactionalAmount = '0';
		$this->m_fltActualTransactionalAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['quota_id'] ) && $boolDirectSet ) $this->set( 'm_intQuotaId', trim( $arrValues['quota_id'] ) ); elseif( isset( $arrValues['quota_id'] ) ) $this->setQuotaId( $arrValues['quota_id'] );
		if( isset( $arrValues['quota_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQuotaEmployeeId', trim( $arrValues['quota_employee_id'] ) ); elseif( isset( $arrValues['quota_employee_id'] ) ) $this->setQuotaEmployeeId( $arrValues['quota_employee_id'] );
		if( isset( $arrValues['ps_lead_filter_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadFilterId', trim( $arrValues['ps_lead_filter_id'] ) ); elseif( isset( $arrValues['ps_lead_filter_id'] ) ) $this->setPsLeadFilterId( $arrValues['ps_lead_filter_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['required_implementation_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRequiredImplementationAmount', trim( $arrValues['required_implementation_amount'] ) ); elseif( isset( $arrValues['required_implementation_amount'] ) ) $this->setRequiredImplementationAmount( $arrValues['required_implementation_amount'] );
		if( isset( $arrValues['actual_implementation_amount'] ) && $boolDirectSet ) $this->set( 'm_fltActualImplementationAmount', trim( $arrValues['actual_implementation_amount'] ) ); elseif( isset( $arrValues['actual_implementation_amount'] ) ) $this->setActualImplementationAmount( $arrValues['actual_implementation_amount'] );
		if( isset( $arrValues['required_recurring_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRequiredRecurringAmount', trim( $arrValues['required_recurring_amount'] ) ); elseif( isset( $arrValues['required_recurring_amount'] ) ) $this->setRequiredRecurringAmount( $arrValues['required_recurring_amount'] );
		if( isset( $arrValues['actual_recurring_amount'] ) && $boolDirectSet ) $this->set( 'm_fltActualRecurringAmount', trim( $arrValues['actual_recurring_amount'] ) ); elseif( isset( $arrValues['actual_recurring_amount'] ) ) $this->setActualRecurringAmount( $arrValues['actual_recurring_amount'] );
		if( isset( $arrValues['required_transactional_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRequiredTransactionalAmount', trim( $arrValues['required_transactional_amount'] ) ); elseif( isset( $arrValues['required_transactional_amount'] ) ) $this->setRequiredTransactionalAmount( $arrValues['required_transactional_amount'] );
		if( isset( $arrValues['actual_transactional_amount'] ) && $boolDirectSet ) $this->set( 'm_fltActualTransactionalAmount', trim( $arrValues['actual_transactional_amount'] ) ); elseif( isset( $arrValues['actual_transactional_amount'] ) ) $this->setActualTransactionalAmount( $arrValues['actual_transactional_amount'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setQuotaId( $intQuotaId ) {
		$this->set( 'm_intQuotaId', CStrings::strToIntDef( $intQuotaId, NULL, false ) );
	}

	public function getQuotaId() {
		return $this->m_intQuotaId;
	}

	public function sqlQuotaId() {
		return ( true == isset( $this->m_intQuotaId ) ) ? ( string ) $this->m_intQuotaId : 'NULL';
	}

	public function setQuotaEmployeeId( $intQuotaEmployeeId ) {
		$this->set( 'm_intQuotaEmployeeId', CStrings::strToIntDef( $intQuotaEmployeeId, NULL, false ) );
	}

	public function getQuotaEmployeeId() {
		return $this->m_intQuotaEmployeeId;
	}

	public function sqlQuotaEmployeeId() {
		return ( true == isset( $this->m_intQuotaEmployeeId ) ) ? ( string ) $this->m_intQuotaEmployeeId : 'NULL';
	}

	public function setPsLeadFilterId( $intPsLeadFilterId ) {
		$this->set( 'm_intPsLeadFilterId', CStrings::strToIntDef( $intPsLeadFilterId, NULL, false ) );
	}

	public function getPsLeadFilterId() {
		return $this->m_intPsLeadFilterId;
	}

	public function sqlPsLeadFilterId() {
		return ( true == isset( $this->m_intPsLeadFilterId ) ) ? ( string ) $this->m_intPsLeadFilterId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setRequiredImplementationAmount( $fltRequiredImplementationAmount ) {
		$this->set( 'm_fltRequiredImplementationAmount', CStrings::strToFloatDef( $fltRequiredImplementationAmount, NULL, false, 2 ) );
	}

	public function getRequiredImplementationAmount() {
		return $this->m_fltRequiredImplementationAmount;
	}

	public function sqlRequiredImplementationAmount() {
		return ( true == isset( $this->m_fltRequiredImplementationAmount ) ) ? ( string ) $this->m_fltRequiredImplementationAmount : '0';
	}

	public function setActualImplementationAmount( $fltActualImplementationAmount ) {
		$this->set( 'm_fltActualImplementationAmount', CStrings::strToFloatDef( $fltActualImplementationAmount, NULL, false, 2 ) );
	}

	public function getActualImplementationAmount() {
		return $this->m_fltActualImplementationAmount;
	}

	public function sqlActualImplementationAmount() {
		return ( true == isset( $this->m_fltActualImplementationAmount ) ) ? ( string ) $this->m_fltActualImplementationAmount : '0';
	}

	public function setRequiredRecurringAmount( $fltRequiredRecurringAmount ) {
		$this->set( 'm_fltRequiredRecurringAmount', CStrings::strToFloatDef( $fltRequiredRecurringAmount, NULL, false, 2 ) );
	}

	public function getRequiredRecurringAmount() {
		return $this->m_fltRequiredRecurringAmount;
	}

	public function sqlRequiredRecurringAmount() {
		return ( true == isset( $this->m_fltRequiredRecurringAmount ) ) ? ( string ) $this->m_fltRequiredRecurringAmount : '0';
	}

	public function setActualRecurringAmount( $fltActualRecurringAmount ) {
		$this->set( 'm_fltActualRecurringAmount', CStrings::strToFloatDef( $fltActualRecurringAmount, NULL, false, 2 ) );
	}

	public function getActualRecurringAmount() {
		return $this->m_fltActualRecurringAmount;
	}

	public function sqlActualRecurringAmount() {
		return ( true == isset( $this->m_fltActualRecurringAmount ) ) ? ( string ) $this->m_fltActualRecurringAmount : '0';
	}

	public function setRequiredTransactionalAmount( $fltRequiredTransactionalAmount ) {
		$this->set( 'm_fltRequiredTransactionalAmount', CStrings::strToFloatDef( $fltRequiredTransactionalAmount, NULL, false, 2 ) );
	}

	public function getRequiredTransactionalAmount() {
		return $this->m_fltRequiredTransactionalAmount;
	}

	public function sqlRequiredTransactionalAmount() {
		return ( true == isset( $this->m_fltRequiredTransactionalAmount ) ) ? ( string ) $this->m_fltRequiredTransactionalAmount : '0';
	}

	public function setActualTransactionalAmount( $fltActualTransactionalAmount ) {
		$this->set( 'm_fltActualTransactionalAmount', CStrings::strToFloatDef( $fltActualTransactionalAmount, NULL, false, 2 ) );
	}

	public function getActualTransactionalAmount() {
		return $this->m_fltActualTransactionalAmount;
	}

	public function sqlActualTransactionalAmount() {
		return ( true == isset( $this->m_fltActualTransactionalAmount ) ) ? ( string ) $this->m_fltActualTransactionalAmount : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, quota_id, quota_employee_id, ps_lead_filter_id, ps_product_id, required_implementation_amount, actual_implementation_amount, required_recurring_amount, actual_recurring_amount, required_transactional_amount, actual_transactional_amount, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlQuotaId() . ', ' .
 						$this->sqlQuotaEmployeeId() . ', ' .
 						$this->sqlPsLeadFilterId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlRequiredImplementationAmount() . ', ' .
 						$this->sqlActualImplementationAmount() . ', ' .
 						$this->sqlRequiredRecurringAmount() . ', ' .
 						$this->sqlActualRecurringAmount() . ', ' .
 						$this->sqlRequiredTransactionalAmount() . ', ' .
 						$this->sqlActualTransactionalAmount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quota_id = ' . $this->sqlQuotaId() . ','; } elseif( true == array_key_exists( 'QuotaId', $this->getChangedColumns() ) ) { $strSql .= ' quota_id = ' . $this->sqlQuotaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quota_employee_id = ' . $this->sqlQuotaEmployeeId() . ','; } elseif( true == array_key_exists( 'QuotaEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' quota_employee_id = ' . $this->sqlQuotaEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_filter_id = ' . $this->sqlPsLeadFilterId() . ','; } elseif( true == array_key_exists( 'PsLeadFilterId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_filter_id = ' . $this->sqlPsLeadFilterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_implementation_amount = ' . $this->sqlRequiredImplementationAmount() . ','; } elseif( true == array_key_exists( 'RequiredImplementationAmount', $this->getChangedColumns() ) ) { $strSql .= ' required_implementation_amount = ' . $this->sqlRequiredImplementationAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_implementation_amount = ' . $this->sqlActualImplementationAmount() . ','; } elseif( true == array_key_exists( 'ActualImplementationAmount', $this->getChangedColumns() ) ) { $strSql .= ' actual_implementation_amount = ' . $this->sqlActualImplementationAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_recurring_amount = ' . $this->sqlRequiredRecurringAmount() . ','; } elseif( true == array_key_exists( 'RequiredRecurringAmount', $this->getChangedColumns() ) ) { $strSql .= ' required_recurring_amount = ' . $this->sqlRequiredRecurringAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_recurring_amount = ' . $this->sqlActualRecurringAmount() . ','; } elseif( true == array_key_exists( 'ActualRecurringAmount', $this->getChangedColumns() ) ) { $strSql .= ' actual_recurring_amount = ' . $this->sqlActualRecurringAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_transactional_amount = ' . $this->sqlRequiredTransactionalAmount() . ','; } elseif( true == array_key_exists( 'RequiredTransactionalAmount', $this->getChangedColumns() ) ) { $strSql .= ' required_transactional_amount = ' . $this->sqlRequiredTransactionalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_transactional_amount = ' . $this->sqlActualTransactionalAmount() . ','; } elseif( true == array_key_exists( 'ActualTransactionalAmount', $this->getChangedColumns() ) ) { $strSql .= ' actual_transactional_amount = ' . $this->sqlActualTransactionalAmount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'quota_id' => $this->getQuotaId(),
			'quota_employee_id' => $this->getQuotaEmployeeId(),
			'ps_lead_filter_id' => $this->getPsLeadFilterId(),
			'ps_product_id' => $this->getPsProductId(),
			'required_implementation_amount' => $this->getRequiredImplementationAmount(),
			'actual_implementation_amount' => $this->getActualImplementationAmount(),
			'required_recurring_amount' => $this->getRequiredRecurringAmount(),
			'actual_recurring_amount' => $this->getActualRecurringAmount(),
			'required_transactional_amount' => $this->getRequiredTransactionalAmount(),
			'actual_transactional_amount' => $this->getActualTransactionalAmount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>