<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportDepartments
 * Do not add any new functions to this class.
 */

class CBaseReportDepartments extends CEosPluralBase {

	/**
	 * @return CReportDepartment[]
	 */
	public static function fetchReportDepartments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportDepartment::class, $objDatabase );
	}

	/**
	 * @return CReportDepartment
	 */
	public static function fetchReportDepartment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportDepartment::class, $objDatabase );
	}

	public static function fetchReportDepartmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_departments', $objDatabase );
	}

	public static function fetchReportDepartmentById( $intId, $objDatabase ) {
		return self::fetchReportDepartment( sprintf( 'SELECT * FROM report_departments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportDepartmentsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchReportDepartments( sprintf( 'SELECT * FROM report_departments WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>