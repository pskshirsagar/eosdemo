<?php

class CBaseEmployeePayroll extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_payrolls';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intDesignationId;
	protected $m_intPayrollPeriodId;
	protected $m_intPayrollId;
	protected $m_intGrossPayEmployeeEncryptionAssociationId;
	protected $m_intHoursWorked;
	protected $m_intHourlyPayEmployeeEncryptionAssociationId;
	protected $m_fltWorkingDays;
	protected $m_fltPresentDays;
	protected $m_fltPaidDays;
	protected $m_fltUnpaidDays;
	protected $m_fltExtraWorkingDays;
	protected $m_fltAbsentDays;
	protected $m_strSupportingData;
	protected $m_intReviewedBy;
	protected $m_strReviewedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['payroll_period_id'] ) && $boolDirectSet ) $this->set( 'm_intPayrollPeriodId', trim( $arrValues['payroll_period_id'] ) ); elseif( isset( $arrValues['payroll_period_id'] ) ) $this->setPayrollPeriodId( $arrValues['payroll_period_id'] );
		if( isset( $arrValues['payroll_id'] ) && $boolDirectSet ) $this->set( 'm_intPayrollId', trim( $arrValues['payroll_id'] ) ); elseif( isset( $arrValues['payroll_id'] ) ) $this->setPayrollId( $arrValues['payroll_id'] );
		if( isset( $arrValues['gross_pay_employee_encryption_association_id'] ) && $boolDirectSet ) $this->set( 'm_intGrossPayEmployeeEncryptionAssociationId', trim( $arrValues['gross_pay_employee_encryption_association_id'] ) ); elseif( isset( $arrValues['gross_pay_employee_encryption_association_id'] ) ) $this->setGrossPayEmployeeEncryptionAssociationId( $arrValues['gross_pay_employee_encryption_association_id'] );
		if( isset( $arrValues['hours_worked'] ) && $boolDirectSet ) $this->set( 'm_intHoursWorked', trim( $arrValues['hours_worked'] ) ); elseif( isset( $arrValues['hours_worked'] ) ) $this->setHoursWorked( $arrValues['hours_worked'] );
		if( isset( $arrValues['hourly_pay_employee_encryption_association_id'] ) && $boolDirectSet ) $this->set( 'm_intHourlyPayEmployeeEncryptionAssociationId', trim( $arrValues['hourly_pay_employee_encryption_association_id'] ) ); elseif( isset( $arrValues['hourly_pay_employee_encryption_association_id'] ) ) $this->setHourlyPayEmployeeEncryptionAssociationId( $arrValues['hourly_pay_employee_encryption_association_id'] );
		if( isset( $arrValues['working_days'] ) && $boolDirectSet ) $this->set( 'm_fltWorkingDays', trim( $arrValues['working_days'] ) ); elseif( isset( $arrValues['working_days'] ) ) $this->setWorkingDays( $arrValues['working_days'] );
		if( isset( $arrValues['present_days'] ) && $boolDirectSet ) $this->set( 'm_fltPresentDays', trim( $arrValues['present_days'] ) ); elseif( isset( $arrValues['present_days'] ) ) $this->setPresentDays( $arrValues['present_days'] );
		if( isset( $arrValues['paid_days'] ) && $boolDirectSet ) $this->set( 'm_fltPaidDays', trim( $arrValues['paid_days'] ) ); elseif( isset( $arrValues['paid_days'] ) ) $this->setPaidDays( $arrValues['paid_days'] );
		if( isset( $arrValues['unpaid_days'] ) && $boolDirectSet ) $this->set( 'm_fltUnpaidDays', trim( $arrValues['unpaid_days'] ) ); elseif( isset( $arrValues['unpaid_days'] ) ) $this->setUnpaidDays( $arrValues['unpaid_days'] );
		if( isset( $arrValues['extra_working_days'] ) && $boolDirectSet ) $this->set( 'm_fltExtraWorkingDays', trim( $arrValues['extra_working_days'] ) ); elseif( isset( $arrValues['extra_working_days'] ) ) $this->setExtraWorkingDays( $arrValues['extra_working_days'] );
		if( isset( $arrValues['absent_days'] ) && $boolDirectSet ) $this->set( 'm_fltAbsentDays', trim( $arrValues['absent_days'] ) ); elseif( isset( $arrValues['absent_days'] ) ) $this->setAbsentDays( $arrValues['absent_days'] );
		if( isset( $arrValues['supporting_data'] ) && $boolDirectSet ) $this->set( 'm_strSupportingData', trim( stripcslashes( $arrValues['supporting_data'] ) ) ); elseif( isset( $arrValues['supporting_data'] ) ) $this->setSupportingData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['supporting_data'] ) : $arrValues['supporting_data'] );
		if( isset( $arrValues['reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intReviewedBy', trim( $arrValues['reviewed_by'] ) ); elseif( isset( $arrValues['reviewed_by'] ) ) $this->setReviewedBy( $arrValues['reviewed_by'] );
		if( isset( $arrValues['reviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strReviewedOn', trim( $arrValues['reviewed_on'] ) ); elseif( isset( $arrValues['reviewed_on'] ) ) $this->setReviewedOn( $arrValues['reviewed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setPayrollPeriodId( $intPayrollPeriodId ) {
		$this->set( 'm_intPayrollPeriodId', CStrings::strToIntDef( $intPayrollPeriodId, NULL, false ) );
	}

	public function getPayrollPeriodId() {
		return $this->m_intPayrollPeriodId;
	}

	public function sqlPayrollPeriodId() {
		return ( true == isset( $this->m_intPayrollPeriodId ) ) ? ( string ) $this->m_intPayrollPeriodId : 'NULL';
	}

	public function setPayrollId( $intPayrollId ) {
		$this->set( 'm_intPayrollId', CStrings::strToIntDef( $intPayrollId, NULL, false ) );
	}

	public function getPayrollId() {
		return $this->m_intPayrollId;
	}

	public function sqlPayrollId() {
		return ( true == isset( $this->m_intPayrollId ) ) ? ( string ) $this->m_intPayrollId : 'NULL';
	}

	public function setGrossPayEmployeeEncryptionAssociationId( $intGrossPayEmployeeEncryptionAssociationId ) {
		$this->set( 'm_intGrossPayEmployeeEncryptionAssociationId', CStrings::strToIntDef( $intGrossPayEmployeeEncryptionAssociationId, NULL, false ) );
	}

	public function getGrossPayEmployeeEncryptionAssociationId() {
		return $this->m_intGrossPayEmployeeEncryptionAssociationId;
	}

	public function sqlGrossPayEmployeeEncryptionAssociationId() {
		return ( true == isset( $this->m_intGrossPayEmployeeEncryptionAssociationId ) ) ? ( string ) $this->m_intGrossPayEmployeeEncryptionAssociationId : 'NULL';
	}

	public function setHoursWorked( $intHoursWorked ) {
		$this->set( 'm_intHoursWorked', CStrings::strToIntDef( $intHoursWorked, NULL, false ) );
	}

	public function getHoursWorked() {
		return $this->m_intHoursWorked;
	}

	public function sqlHoursWorked() {
		return ( true == isset( $this->m_intHoursWorked ) ) ? ( string ) $this->m_intHoursWorked : 'NULL';
	}

	public function setHourlyPayEmployeeEncryptionAssociationId( $intHourlyPayEmployeeEncryptionAssociationId ) {
		$this->set( 'm_intHourlyPayEmployeeEncryptionAssociationId', CStrings::strToIntDef( $intHourlyPayEmployeeEncryptionAssociationId, NULL, false ) );
	}

	public function getHourlyPayEmployeeEncryptionAssociationId() {
		return $this->m_intHourlyPayEmployeeEncryptionAssociationId;
	}

	public function sqlHourlyPayEmployeeEncryptionAssociationId() {
		return ( true == isset( $this->m_intHourlyPayEmployeeEncryptionAssociationId ) ) ? ( string ) $this->m_intHourlyPayEmployeeEncryptionAssociationId : 'NULL';
	}

	public function setWorkingDays( $fltWorkingDays ) {
		$this->set( 'm_fltWorkingDays', CStrings::strToFloatDef( $fltWorkingDays, NULL, false, 2 ) );
	}

	public function getWorkingDays() {
		return $this->m_fltWorkingDays;
	}

	public function sqlWorkingDays() {
		return ( true == isset( $this->m_fltWorkingDays ) ) ? ( string ) $this->m_fltWorkingDays : 'NULL';
	}

	public function setPresentDays( $fltPresentDays ) {
		$this->set( 'm_fltPresentDays', CStrings::strToFloatDef( $fltPresentDays, NULL, false, 2 ) );
	}

	public function getPresentDays() {
		return $this->m_fltPresentDays;
	}

	public function sqlPresentDays() {
		return ( true == isset( $this->m_fltPresentDays ) ) ? ( string ) $this->m_fltPresentDays : 'NULL';
	}

	public function setPaidDays( $fltPaidDays ) {
		$this->set( 'm_fltPaidDays', CStrings::strToFloatDef( $fltPaidDays, NULL, false, 2 ) );
	}

	public function getPaidDays() {
		return $this->m_fltPaidDays;
	}

	public function sqlPaidDays() {
		return ( true == isset( $this->m_fltPaidDays ) ) ? ( string ) $this->m_fltPaidDays : 'NULL';
	}

	public function setUnpaidDays( $fltUnpaidDays ) {
		$this->set( 'm_fltUnpaidDays', CStrings::strToFloatDef( $fltUnpaidDays, NULL, false, 2 ) );
	}

	public function getUnpaidDays() {
		return $this->m_fltUnpaidDays;
	}

	public function sqlUnpaidDays() {
		return ( true == isset( $this->m_fltUnpaidDays ) ) ? ( string ) $this->m_fltUnpaidDays : 'NULL';
	}

	public function setExtraWorkingDays( $fltExtraWorkingDays ) {
		$this->set( 'm_fltExtraWorkingDays', CStrings::strToFloatDef( $fltExtraWorkingDays, NULL, false, 2 ) );
	}

	public function getExtraWorkingDays() {
		return $this->m_fltExtraWorkingDays;
	}

	public function sqlExtraWorkingDays() {
		return ( true == isset( $this->m_fltExtraWorkingDays ) ) ? ( string ) $this->m_fltExtraWorkingDays : 'NULL';
	}

	public function setAbsentDays( $fltAbsentDays ) {
		$this->set( 'm_fltAbsentDays', CStrings::strToFloatDef( $fltAbsentDays, NULL, false, 2 ) );
	}

	public function getAbsentDays() {
		return $this->m_fltAbsentDays;
	}

	public function sqlAbsentDays() {
		return ( true == isset( $this->m_fltAbsentDays ) ) ? ( string ) $this->m_fltAbsentDays : 'NULL';
	}

	public function setSupportingData( $strSupportingData ) {
		$this->set( 'm_strSupportingData', CStrings::strTrimDef( $strSupportingData, -1, NULL, true ) );
	}

	public function getSupportingData() {
		return $this->m_strSupportingData;
	}

	public function sqlSupportingData() {
		return ( true == isset( $this->m_strSupportingData ) ) ? '\'' . addslashes( $this->m_strSupportingData ) . '\'' : 'NULL';
	}

	public function setReviewedBy( $intReviewedBy ) {
		$this->set( 'm_intReviewedBy', CStrings::strToIntDef( $intReviewedBy, NULL, false ) );
	}

	public function getReviewedBy() {
		return $this->m_intReviewedBy;
	}

	public function sqlReviewedBy() {
		return ( true == isset( $this->m_intReviewedBy ) ) ? ( string ) $this->m_intReviewedBy : 'NULL';
	}

	public function setReviewedOn( $strReviewedOn ) {
		$this->set( 'm_strReviewedOn', CStrings::strTrimDef( $strReviewedOn, -1, NULL, true ) );
	}

	public function getReviewedOn() {
		return $this->m_strReviewedOn;
	}

	public function sqlReviewedOn() {
		return ( true == isset( $this->m_strReviewedOn ) ) ? '\'' . $this->m_strReviewedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, designation_id, payroll_period_id, payroll_id, gross_pay_employee_encryption_association_id, hours_worked, hourly_pay_employee_encryption_association_id, working_days, present_days, paid_days, unpaid_days, extra_working_days, absent_days, supporting_data, reviewed_by, reviewed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlDesignationId() . ', ' .
 						$this->sqlPayrollPeriodId() . ', ' .
 						$this->sqlPayrollId() . ', ' .
 						$this->sqlGrossPayEmployeeEncryptionAssociationId() . ', ' .
 						$this->sqlHoursWorked() . ', ' .
 						$this->sqlHourlyPayEmployeeEncryptionAssociationId() . ', ' .
 						$this->sqlWorkingDays() . ', ' .
 						$this->sqlPresentDays() . ', ' .
 						$this->sqlPaidDays() . ', ' .
 						$this->sqlUnpaidDays() . ', ' .
 						$this->sqlExtraWorkingDays() . ', ' .
 						$this->sqlAbsentDays() . ', ' .
 						$this->sqlSupportingData() . ', ' .
 						$this->sqlReviewedBy() . ', ' .
 						$this->sqlReviewedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payroll_period_id = ' . $this->sqlPayrollPeriodId() . ','; } elseif( true == array_key_exists( 'PayrollPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' payroll_period_id = ' . $this->sqlPayrollPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payroll_id = ' . $this->sqlPayrollId() . ','; } elseif( true == array_key_exists( 'PayrollId', $this->getChangedColumns() ) ) { $strSql .= ' payroll_id = ' . $this->sqlPayrollId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gross_pay_employee_encryption_association_id = ' . $this->sqlGrossPayEmployeeEncryptionAssociationId() . ','; } elseif( true == array_key_exists( 'GrossPayEmployeeEncryptionAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' gross_pay_employee_encryption_association_id = ' . $this->sqlGrossPayEmployeeEncryptionAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hours_worked = ' . $this->sqlHoursWorked() . ','; } elseif( true == array_key_exists( 'HoursWorked', $this->getChangedColumns() ) ) { $strSql .= ' hours_worked = ' . $this->sqlHoursWorked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hourly_pay_employee_encryption_association_id = ' . $this->sqlHourlyPayEmployeeEncryptionAssociationId() . ','; } elseif( true == array_key_exists( 'HourlyPayEmployeeEncryptionAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' hourly_pay_employee_encryption_association_id = ' . $this->sqlHourlyPayEmployeeEncryptionAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' working_days = ' . $this->sqlWorkingDays() . ','; } elseif( true == array_key_exists( 'WorkingDays', $this->getChangedColumns() ) ) { $strSql .= ' working_days = ' . $this->sqlWorkingDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' present_days = ' . $this->sqlPresentDays() . ','; } elseif( true == array_key_exists( 'PresentDays', $this->getChangedColumns() ) ) { $strSql .= ' present_days = ' . $this->sqlPresentDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' paid_days = ' . $this->sqlPaidDays() . ','; } elseif( true == array_key_exists( 'PaidDays', $this->getChangedColumns() ) ) { $strSql .= ' paid_days = ' . $this->sqlPaidDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unpaid_days = ' . $this->sqlUnpaidDays() . ','; } elseif( true == array_key_exists( 'UnpaidDays', $this->getChangedColumns() ) ) { $strSql .= ' unpaid_days = ' . $this->sqlUnpaidDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' extra_working_days = ' . $this->sqlExtraWorkingDays() . ','; } elseif( true == array_key_exists( 'ExtraWorkingDays', $this->getChangedColumns() ) ) { $strSql .= ' extra_working_days = ' . $this->sqlExtraWorkingDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' absent_days = ' . $this->sqlAbsentDays() . ','; } elseif( true == array_key_exists( 'AbsentDays', $this->getChangedColumns() ) ) { $strSql .= ' absent_days = ' . $this->sqlAbsentDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' supporting_data = ' . $this->sqlSupportingData() . ','; } elseif( true == array_key_exists( 'SupportingData', $this->getChangedColumns() ) ) { $strSql .= ' supporting_data = ' . $this->sqlSupportingData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy() . ','; } elseif( true == array_key_exists( 'ReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_by = ' . $this->sqlReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn() . ','; } elseif( true == array_key_exists( 'ReviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' reviewed_on = ' . $this->sqlReviewedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'designation_id' => $this->getDesignationId(),
			'payroll_period_id' => $this->getPayrollPeriodId(),
			'payroll_id' => $this->getPayrollId(),
			'gross_pay_employee_encryption_association_id' => $this->getGrossPayEmployeeEncryptionAssociationId(),
			'hours_worked' => $this->getHoursWorked(),
			'hourly_pay_employee_encryption_association_id' => $this->getHourlyPayEmployeeEncryptionAssociationId(),
			'working_days' => $this->getWorkingDays(),
			'present_days' => $this->getPresentDays(),
			'paid_days' => $this->getPaidDays(),
			'unpaid_days' => $this->getUnpaidDays(),
			'extra_working_days' => $this->getExtraWorkingDays(),
			'absent_days' => $this->getAbsentDays(),
			'supporting_data' => $this->getSupportingData(),
			'reviewed_by' => $this->getReviewedBy(),
			'reviewed_on' => $this->getReviewedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>