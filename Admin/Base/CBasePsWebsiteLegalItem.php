<?php

class CBasePsWebsiteLegalItem extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_website_legal_items';

	protected $m_intId;
	protected $m_strDate;
	protected $m_strTitle;
	protected $m_strSeoHandle;
	protected $m_strContent;
	protected $m_strSource;
	protected $m_strAuthor;
	protected $m_strImageFileName;
	protected $m_strImagePath;
	protected $m_intShowOnHomePage;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intShowOnHomePage = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['date'] ) && $boolDirectSet ) $this->set( 'm_strDate', trim( $arrValues['date'] ) ); elseif( isset( $arrValues['date'] ) ) $this->setDate( $arrValues['date'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['seo_handle'] ) && $boolDirectSet ) $this->set( 'm_strSeoHandle', trim( stripcslashes( $arrValues['seo_handle'] ) ) ); elseif( isset( $arrValues['seo_handle'] ) ) $this->setSeoHandle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['seo_handle'] ) : $arrValues['seo_handle'] );
		if( isset( $arrValues['content'] ) && $boolDirectSet ) $this->set( 'm_strContent', trim( stripcslashes( $arrValues['content'] ) ) ); elseif( isset( $arrValues['content'] ) ) $this->setContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['content'] ) : $arrValues['content'] );
		if( isset( $arrValues['source'] ) && $boolDirectSet ) $this->set( 'm_strSource', trim( stripcslashes( $arrValues['source'] ) ) ); elseif( isset( $arrValues['source'] ) ) $this->setSource( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['source'] ) : $arrValues['source'] );
		if( isset( $arrValues['author'] ) && $boolDirectSet ) $this->set( 'm_strAuthor', trim( stripcslashes( $arrValues['author'] ) ) ); elseif( isset( $arrValues['author'] ) ) $this->setAuthor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['author'] ) : $arrValues['author'] );
		if( isset( $arrValues['image_file_name'] ) && $boolDirectSet ) $this->set( 'm_strImageFileName', trim( stripcslashes( $arrValues['image_file_name'] ) ) ); elseif( isset( $arrValues['image_file_name'] ) ) $this->setImageFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_file_name'] ) : $arrValues['image_file_name'] );
		if( isset( $arrValues['image_path'] ) && $boolDirectSet ) $this->set( 'm_strImagePath', trim( stripcslashes( $arrValues['image_path'] ) ) ); elseif( isset( $arrValues['image_path'] ) ) $this->setImagePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['image_path'] ) : $arrValues['image_path'] );
		if( isset( $arrValues['show_on_home_page'] ) && $boolDirectSet ) $this->set( 'm_intShowOnHomePage', trim( $arrValues['show_on_home_page'] ) ); elseif( isset( $arrValues['show_on_home_page'] ) ) $this->setShowOnHomePage( $arrValues['show_on_home_page'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDate( $strDate ) {
		$this->set( 'm_strDate', CStrings::strTrimDef( $strDate, -1, NULL, true ) );
	}

	public function getDate() {
		return $this->m_strDate;
	}

	public function sqlDate() {
		return ( true == isset( $this->m_strDate ) ) ? '\'' . $this->m_strDate . '\'' : 'NOW()';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setSeoHandle( $strSeoHandle ) {
		$this->set( 'm_strSeoHandle', CStrings::strTrimDef( $strSeoHandle, 50, NULL, true ) );
	}

	public function getSeoHandle() {
		return $this->m_strSeoHandle;
	}

	public function sqlSeoHandle() {
		return ( true == isset( $this->m_strSeoHandle ) ) ? '\'' . addslashes( $this->m_strSeoHandle ) . '\'' : 'NULL';
	}

	public function setContent( $strContent ) {
		$this->set( 'm_strContent', CStrings::strTrimDef( $strContent, -1, NULL, true ) );
	}

	public function getContent() {
		return $this->m_strContent;
	}

	public function sqlContent() {
		return ( true == isset( $this->m_strContent ) ) ? '\'' . addslashes( $this->m_strContent ) . '\'' : 'NULL';
	}

	public function setSource( $strSource ) {
		$this->set( 'm_strSource', CStrings::strTrimDef( $strSource, 50, NULL, true ) );
	}

	public function getSource() {
		return $this->m_strSource;
	}

	public function sqlSource() {
		return ( true == isset( $this->m_strSource ) ) ? '\'' . addslashes( $this->m_strSource ) . '\'' : 'NULL';
	}

	public function setAuthor( $strAuthor ) {
		$this->set( 'm_strAuthor', CStrings::strTrimDef( $strAuthor, 50, NULL, true ) );
	}

	public function getAuthor() {
		return $this->m_strAuthor;
	}

	public function sqlAuthor() {
		return ( true == isset( $this->m_strAuthor ) ) ? '\'' . addslashes( $this->m_strAuthor ) . '\'' : 'NULL';
	}

	public function setImageFileName( $strImageFileName ) {
		$this->set( 'm_strImageFileName', CStrings::strTrimDef( $strImageFileName, 240, NULL, true ) );
	}

	public function getImageFileName() {
		return $this->m_strImageFileName;
	}

	public function sqlImageFileName() {
		return ( true == isset( $this->m_strImageFileName ) ) ? '\'' . addslashes( $this->m_strImageFileName ) . '\'' : 'NULL';
	}

	public function setImagePath( $strImagePath ) {
		$this->set( 'm_strImagePath', CStrings::strTrimDef( $strImagePath, 4096, NULL, true ) );
	}

	public function getImagePath() {
		return $this->m_strImagePath;
	}

	public function sqlImagePath() {
		return ( true == isset( $this->m_strImagePath ) ) ? '\'' . addslashes( $this->m_strImagePath ) . '\'' : 'NULL';
	}

	public function setShowOnHomePage( $intShowOnHomePage ) {
		$this->set( 'm_intShowOnHomePage', CStrings::strToIntDef( $intShowOnHomePage, NULL, false ) );
	}

	public function getShowOnHomePage() {
		return $this->m_intShowOnHomePage;
	}

	public function sqlShowOnHomePage() {
		return ( true == isset( $this->m_intShowOnHomePage ) ) ? ( string ) $this->m_intShowOnHomePage : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, date, title, seo_handle, content, source, author, image_file_name, image_path, show_on_home_page, is_published, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDate() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlSeoHandle() . ', ' .
 						$this->sqlContent() . ', ' .
 						$this->sqlSource() . ', ' .
 						$this->sqlAuthor() . ', ' .
 						$this->sqlImageFileName() . ', ' .
 						$this->sqlImagePath() . ', ' .
 						$this->sqlShowOnHomePage() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date = ' . $this->sqlDate() . ','; } elseif( true == array_key_exists( 'Date', $this->getChangedColumns() ) ) { $strSql .= ' date = ' . $this->sqlDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' seo_handle = ' . $this->sqlSeoHandle() . ','; } elseif( true == array_key_exists( 'SeoHandle', $this->getChangedColumns() ) ) { $strSql .= ' seo_handle = ' . $this->sqlSeoHandle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content = ' . $this->sqlContent() . ','; } elseif( true == array_key_exists( 'Content', $this->getChangedColumns() ) ) { $strSql .= ' content = ' . $this->sqlContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source = ' . $this->sqlSource() . ','; } elseif( true == array_key_exists( 'Source', $this->getChangedColumns() ) ) { $strSql .= ' source = ' . $this->sqlSource() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' author = ' . $this->sqlAuthor() . ','; } elseif( true == array_key_exists( 'Author', $this->getChangedColumns() ) ) { $strSql .= ' author = ' . $this->sqlAuthor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_file_name = ' . $this->sqlImageFileName() . ','; } elseif( true == array_key_exists( 'ImageFileName', $this->getChangedColumns() ) ) { $strSql .= ' image_file_name = ' . $this->sqlImageFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' image_path = ' . $this->sqlImagePath() . ','; } elseif( true == array_key_exists( 'ImagePath', $this->getChangedColumns() ) ) { $strSql .= ' image_path = ' . $this->sqlImagePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_home_page = ' . $this->sqlShowOnHomePage() . ','; } elseif( true == array_key_exists( 'ShowOnHomePage', $this->getChangedColumns() ) ) { $strSql .= ' show_on_home_page = ' . $this->sqlShowOnHomePage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'date' => $this->getDate(),
			'title' => $this->getTitle(),
			'seo_handle' => $this->getSeoHandle(),
			'content' => $this->getContent(),
			'source' => $this->getSource(),
			'author' => $this->getAuthor(),
			'image_file_name' => $this->getImageFileName(),
			'image_path' => $this->getImagePath(),
			'show_on_home_page' => $this->getShowOnHomePage(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>