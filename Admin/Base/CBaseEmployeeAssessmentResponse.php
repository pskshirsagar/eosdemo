<?php

class CBaseEmployeeAssessmentResponse extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_assessment_responses';

	protected $m_intId;
	protected $m_intEmployeeAssessmentId;
	protected $m_intEmployeeAssessmentEmployeeId;
	protected $m_intEmployeeAssessmentQuestionId;
	protected $m_intIsRating;
	protected $m_strQuestion;
	protected $m_strAnswer;
	protected $m_strComments;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsRating = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_assessment_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeAssessmentId', trim( $arrValues['employee_assessment_id'] ) ); elseif( isset( $arrValues['employee_assessment_id'] ) ) $this->setEmployeeAssessmentId( $arrValues['employee_assessment_id'] );
		if( isset( $arrValues['employee_assessment_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeAssessmentEmployeeId', trim( $arrValues['employee_assessment_employee_id'] ) ); elseif( isset( $arrValues['employee_assessment_employee_id'] ) ) $this->setEmployeeAssessmentEmployeeId( $arrValues['employee_assessment_employee_id'] );
		if( isset( $arrValues['employee_assessment_question_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeAssessmentQuestionId', trim( $arrValues['employee_assessment_question_id'] ) ); elseif( isset( $arrValues['employee_assessment_question_id'] ) ) $this->setEmployeeAssessmentQuestionId( $arrValues['employee_assessment_question_id'] );
		if( isset( $arrValues['is_rating'] ) && $boolDirectSet ) $this->set( 'm_intIsRating', trim( $arrValues['is_rating'] ) ); elseif( isset( $arrValues['is_rating'] ) ) $this->setIsRating( $arrValues['is_rating'] );
		if( isset( $arrValues['question'] ) && $boolDirectSet ) $this->set( 'm_strQuestion', trim( stripcslashes( $arrValues['question'] ) ) ); elseif( isset( $arrValues['question'] ) ) $this->setQuestion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['question'] ) : $arrValues['question'] );
		if( isset( $arrValues['answer'] ) && $boolDirectSet ) $this->set( 'm_strAnswer', trim( stripcslashes( $arrValues['answer'] ) ) ); elseif( isset( $arrValues['answer'] ) ) $this->setAnswer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['answer'] ) : $arrValues['answer'] );
		if( isset( $arrValues['comments'] ) && $boolDirectSet ) $this->set( 'm_strComments', trim( stripcslashes( $arrValues['comments'] ) ) ); elseif( isset( $arrValues['comments'] ) ) $this->setComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['comments'] ) : $arrValues['comments'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeAssessmentId( $intEmployeeAssessmentId ) {
		$this->set( 'm_intEmployeeAssessmentId', CStrings::strToIntDef( $intEmployeeAssessmentId, NULL, false ) );
	}

	public function getEmployeeAssessmentId() {
		return $this->m_intEmployeeAssessmentId;
	}

	public function sqlEmployeeAssessmentId() {
		return ( true == isset( $this->m_intEmployeeAssessmentId ) ) ? ( string ) $this->m_intEmployeeAssessmentId : 'NULL';
	}

	public function setEmployeeAssessmentEmployeeId( $intEmployeeAssessmentEmployeeId ) {
		$this->set( 'm_intEmployeeAssessmentEmployeeId', CStrings::strToIntDef( $intEmployeeAssessmentEmployeeId, NULL, false ) );
	}

	public function getEmployeeAssessmentEmployeeId() {
		return $this->m_intEmployeeAssessmentEmployeeId;
	}

	public function sqlEmployeeAssessmentEmployeeId() {
		return ( true == isset( $this->m_intEmployeeAssessmentEmployeeId ) ) ? ( string ) $this->m_intEmployeeAssessmentEmployeeId : 'NULL';
	}

	public function setEmployeeAssessmentQuestionId( $intEmployeeAssessmentQuestionId ) {
		$this->set( 'm_intEmployeeAssessmentQuestionId', CStrings::strToIntDef( $intEmployeeAssessmentQuestionId, NULL, false ) );
	}

	public function getEmployeeAssessmentQuestionId() {
		return $this->m_intEmployeeAssessmentQuestionId;
	}

	public function sqlEmployeeAssessmentQuestionId() {
		return ( true == isset( $this->m_intEmployeeAssessmentQuestionId ) ) ? ( string ) $this->m_intEmployeeAssessmentQuestionId : 'NULL';
	}

	public function setIsRating( $intIsRating ) {
		$this->set( 'm_intIsRating', CStrings::strToIntDef( $intIsRating, NULL, false ) );
	}

	public function getIsRating() {
		return $this->m_intIsRating;
	}

	public function sqlIsRating() {
		return ( true == isset( $this->m_intIsRating ) ) ? ( string ) $this->m_intIsRating : '0';
	}

	public function setQuestion( $strQuestion ) {
		$this->set( 'm_strQuestion', CStrings::strTrimDef( $strQuestion, -1, NULL, true ) );
	}

	public function getQuestion() {
		return $this->m_strQuestion;
	}

	public function sqlQuestion() {
		return ( true == isset( $this->m_strQuestion ) ) ? '\'' . addslashes( $this->m_strQuestion ) . '\'' : 'NULL';
	}

	public function setAnswer( $strAnswer ) {
		$this->set( 'm_strAnswer', CStrings::strTrimDef( $strAnswer, -1, NULL, true ) );
	}

	public function getAnswer() {
		return $this->m_strAnswer;
	}

	public function sqlAnswer() {
		return ( true == isset( $this->m_strAnswer ) ) ? '\'' . addslashes( $this->m_strAnswer ) . '\'' : 'NULL';
	}

	public function setComments( $strComments ) {
		$this->set( 'm_strComments', CStrings::strTrimDef( $strComments, -1, NULL, true ) );
	}

	public function getComments() {
		return $this->m_strComments;
	}

	public function sqlComments() {
		return ( true == isset( $this->m_strComments ) ) ? '\'' . addslashes( $this->m_strComments ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_assessment_id, employee_assessment_employee_id, employee_assessment_question_id, is_rating, question, answer, comments, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeAssessmentId() . ', ' .
 						$this->sqlEmployeeAssessmentEmployeeId() . ', ' .
 						$this->sqlEmployeeAssessmentQuestionId() . ', ' .
 						$this->sqlIsRating() . ', ' .
 						$this->sqlQuestion() . ', ' .
 						$this->sqlAnswer() . ', ' .
 						$this->sqlComments() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_assessment_id = ' . $this->sqlEmployeeAssessmentId() . ','; } elseif( true == array_key_exists( 'EmployeeAssessmentId', $this->getChangedColumns() ) ) { $strSql .= ' employee_assessment_id = ' . $this->sqlEmployeeAssessmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_assessment_employee_id = ' . $this->sqlEmployeeAssessmentEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeAssessmentEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_assessment_employee_id = ' . $this->sqlEmployeeAssessmentEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_assessment_question_id = ' . $this->sqlEmployeeAssessmentQuestionId() . ','; } elseif( true == array_key_exists( 'EmployeeAssessmentQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' employee_assessment_question_id = ' . $this->sqlEmployeeAssessmentQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_rating = ' . $this->sqlIsRating() . ','; } elseif( true == array_key_exists( 'IsRating', $this->getChangedColumns() ) ) { $strSql .= ' is_rating = ' . $this->sqlIsRating() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' question = ' . $this->sqlQuestion() . ','; } elseif( true == array_key_exists( 'Question', $this->getChangedColumns() ) ) { $strSql .= ' question = ' . $this->sqlQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answer = ' . $this->sqlAnswer() . ','; } elseif( true == array_key_exists( 'Answer', $this->getChangedColumns() ) ) { $strSql .= ' answer = ' . $this->sqlAnswer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comments = ' . $this->sqlComments() . ','; } elseif( true == array_key_exists( 'Comments', $this->getChangedColumns() ) ) { $strSql .= ' comments = ' . $this->sqlComments() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_assessment_id' => $this->getEmployeeAssessmentId(),
			'employee_assessment_employee_id' => $this->getEmployeeAssessmentEmployeeId(),
			'employee_assessment_question_id' => $this->getEmployeeAssessmentQuestionId(),
			'is_rating' => $this->getIsRating(),
			'question' => $this->getQuestion(),
			'answer' => $this->getAnswer(),
			'comments' => $this->getComments(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>