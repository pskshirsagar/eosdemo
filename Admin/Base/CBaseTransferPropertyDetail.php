<?php

class CBaseTransferPropertyDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.transfer_property_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTransferPropertyId;
	protected $m_intPropertyId;
	protected $m_intPsProductId;
	protected $m_intContractPropertyId;
	protected $m_intPreviousContractPropertyId;
	protected $m_intPreviousCommissionRateAssociationId;
	protected $m_fltPreviousOutstandingCommissionAmount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['transfer_property_id'] ) && $boolDirectSet ) $this->set( 'm_intTransferPropertyId', trim( $arrValues['transfer_property_id'] ) ); elseif( isset( $arrValues['transfer_property_id'] ) ) $this->setTransferPropertyId( $arrValues['transfer_property_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intContractPropertyId', trim( $arrValues['contract_property_id'] ) ); elseif( isset( $arrValues['contract_property_id'] ) ) $this->setContractPropertyId( $arrValues['contract_property_id'] );
		if( isset( $arrValues['previous_contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intPreviousContractPropertyId', trim( $arrValues['previous_contract_property_id'] ) ); elseif( isset( $arrValues['previous_contract_property_id'] ) ) $this->setPreviousContractPropertyId( $arrValues['previous_contract_property_id'] );
		if( isset( $arrValues['previous_commission_rate_association_id'] ) && $boolDirectSet ) $this->set( 'm_intPreviousCommissionRateAssociationId', trim( $arrValues['previous_commission_rate_association_id'] ) ); elseif( isset( $arrValues['previous_commission_rate_association_id'] ) ) $this->setPreviousCommissionRateAssociationId( $arrValues['previous_commission_rate_association_id'] );
		if( isset( $arrValues['previous_outstanding_commission_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPreviousOutstandingCommissionAmount', trim( $arrValues['previous_outstanding_commission_amount'] ) ); elseif( isset( $arrValues['previous_outstanding_commission_amount'] ) ) $this->setPreviousOutstandingCommissionAmount( $arrValues['previous_outstanding_commission_amount'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setTransferPropertyId( $intTransferPropertyId ) {
		$this->set( 'm_intTransferPropertyId', CStrings::strToIntDef( $intTransferPropertyId, NULL, false ) );
	}

	public function getTransferPropertyId() {
		return $this->m_intTransferPropertyId;
	}

	public function sqlTransferPropertyId() {
		return ( true == isset( $this->m_intTransferPropertyId ) ) ? ( string ) $this->m_intTransferPropertyId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setContractPropertyId( $intContractPropertyId ) {
		$this->set( 'm_intContractPropertyId', CStrings::strToIntDef( $intContractPropertyId, NULL, false ) );
	}

	public function getContractPropertyId() {
		return $this->m_intContractPropertyId;
	}

	public function sqlContractPropertyId() {
		return ( true == isset( $this->m_intContractPropertyId ) ) ? ( string ) $this->m_intContractPropertyId : 'NULL';
	}

	public function setPreviousContractPropertyId( $intPreviousContractPropertyId ) {
		$this->set( 'm_intPreviousContractPropertyId', CStrings::strToIntDef( $intPreviousContractPropertyId, NULL, false ) );
	}

	public function getPreviousContractPropertyId() {
		return $this->m_intPreviousContractPropertyId;
	}

	public function sqlPreviousContractPropertyId() {
		return ( true == isset( $this->m_intPreviousContractPropertyId ) ) ? ( string ) $this->m_intPreviousContractPropertyId : 'NULL';
	}

	public function setPreviousCommissionRateAssociationId( $intPreviousCommissionRateAssociationId ) {
		$this->set( 'm_intPreviousCommissionRateAssociationId', CStrings::strToIntDef( $intPreviousCommissionRateAssociationId, NULL, false ) );
	}

	public function getPreviousCommissionRateAssociationId() {
		return $this->m_intPreviousCommissionRateAssociationId;
	}

	public function sqlPreviousCommissionRateAssociationId() {
		return ( true == isset( $this->m_intPreviousCommissionRateAssociationId ) ) ? ( string ) $this->m_intPreviousCommissionRateAssociationId : 'NULL';
	}

	public function setPreviousOutstandingCommissionAmount( $fltPreviousOutstandingCommissionAmount ) {
		$this->set( 'm_fltPreviousOutstandingCommissionAmount', CStrings::strToFloatDef( $fltPreviousOutstandingCommissionAmount, NULL, false, 2 ) );
	}

	public function getPreviousOutstandingCommissionAmount() {
		return $this->m_fltPreviousOutstandingCommissionAmount;
	}

	public function sqlPreviousOutstandingCommissionAmount() {
		return ( true == isset( $this->m_fltPreviousOutstandingCommissionAmount ) ) ? ( string ) $this->m_fltPreviousOutstandingCommissionAmount : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, transfer_property_id, property_id, ps_product_id, contract_property_id, previous_contract_property_id, previous_commission_rate_association_id, previous_outstanding_commission_amount, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlTransferPropertyId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlContractPropertyId() . ', ' .
 						$this->sqlPreviousContractPropertyId() . ', ' .
 						$this->sqlPreviousCommissionRateAssociationId() . ', ' .
 						$this->sqlPreviousOutstandingCommissionAmount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transfer_property_id = ' . $this->sqlTransferPropertyId() . ','; } elseif( true == array_key_exists( 'TransferPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' transfer_property_id = ' . $this->sqlTransferPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_property_id = ' . $this->sqlContractPropertyId() . ','; } elseif( true == array_key_exists( 'ContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' contract_property_id = ' . $this->sqlContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_contract_property_id = ' . $this->sqlPreviousContractPropertyId() . ','; } elseif( true == array_key_exists( 'PreviousContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' previous_contract_property_id = ' . $this->sqlPreviousContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_commission_rate_association_id = ' . $this->sqlPreviousCommissionRateAssociationId() . ','; } elseif( true == array_key_exists( 'PreviousCommissionRateAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' previous_commission_rate_association_id = ' . $this->sqlPreviousCommissionRateAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_outstanding_commission_amount = ' . $this->sqlPreviousOutstandingCommissionAmount() . ','; } elseif( true == array_key_exists( 'PreviousOutstandingCommissionAmount', $this->getChangedColumns() ) ) { $strSql .= ' previous_outstanding_commission_amount = ' . $this->sqlPreviousOutstandingCommissionAmount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'transfer_property_id' => $this->getTransferPropertyId(),
			'property_id' => $this->getPropertyId(),
			'ps_product_id' => $this->getPsProductId(),
			'contract_property_id' => $this->getContractPropertyId(),
			'previous_contract_property_id' => $this->getPreviousContractPropertyId(),
			'previous_commission_rate_association_id' => $this->getPreviousCommissionRateAssociationId(),
			'previous_outstanding_commission_amount' => $this->getPreviousOutstandingCommissionAmount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>