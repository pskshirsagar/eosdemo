<?php

class CBaseEmployeeSwipe extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_swipes';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intEmployeeHourId;
	protected $m_intCardNumber;
	protected $m_strGateNumber;
	protected $m_intIsIn;
	protected $m_intIsManual;
	protected $m_strSwipeDatetime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsManual = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['employee_hour_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeHourId', trim( $arrValues['employee_hour_id'] ) ); elseif( isset( $arrValues['employee_hour_id'] ) ) $this->setEmployeeHourId( $arrValues['employee_hour_id'] );
		if( isset( $arrValues['card_number'] ) && $boolDirectSet ) $this->set( 'm_intCardNumber', trim( $arrValues['card_number'] ) ); elseif( isset( $arrValues['card_number'] ) ) $this->setCardNumber( $arrValues['card_number'] );
		if( isset( $arrValues['gate_number'] ) && $boolDirectSet ) $this->set( 'm_strGateNumber', trim( stripcslashes( $arrValues['gate_number'] ) ) ); elseif( isset( $arrValues['gate_number'] ) ) $this->setGateNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gate_number'] ) : $arrValues['gate_number'] );
		if( isset( $arrValues['is_in'] ) && $boolDirectSet ) $this->set( 'm_intIsIn', trim( $arrValues['is_in'] ) ); elseif( isset( $arrValues['is_in'] ) ) $this->setIsIn( $arrValues['is_in'] );
		if( isset( $arrValues['is_manual'] ) && $boolDirectSet ) $this->set( 'm_intIsManual', trim( $arrValues['is_manual'] ) ); elseif( isset( $arrValues['is_manual'] ) ) $this->setIsManual( $arrValues['is_manual'] );
		if( isset( $arrValues['swipe_datetime'] ) && $boolDirectSet ) $this->set( 'm_strSwipeDatetime', trim( $arrValues['swipe_datetime'] ) ); elseif( isset( $arrValues['swipe_datetime'] ) ) $this->setSwipeDatetime( $arrValues['swipe_datetime'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setEmployeeHourId( $intEmployeeHourId ) {
		$this->set( 'm_intEmployeeHourId', CStrings::strToIntDef( $intEmployeeHourId, NULL, false ) );
	}

	public function getEmployeeHourId() {
		return $this->m_intEmployeeHourId;
	}

	public function sqlEmployeeHourId() {
		return ( true == isset( $this->m_intEmployeeHourId ) ) ? ( string ) $this->m_intEmployeeHourId : 'NULL';
	}

	public function setCardNumber( $intCardNumber ) {
		$this->set( 'm_intCardNumber', CStrings::strToIntDef( $intCardNumber, NULL, false ) );
	}

	public function getCardNumber() {
		return $this->m_intCardNumber;
	}

	public function sqlCardNumber() {
		return ( true == isset( $this->m_intCardNumber ) ) ? ( string ) $this->m_intCardNumber : 'NULL';
	}

	public function setGateNumber( $strGateNumber ) {
		$this->set( 'm_strGateNumber', CStrings::strTrimDef( $strGateNumber, 10, NULL, true ) );
	}

	public function getGateNumber() {
		return $this->m_strGateNumber;
	}

	public function sqlGateNumber() {
		return ( true == isset( $this->m_strGateNumber ) ) ? '\'' . addslashes( $this->m_strGateNumber ) . '\'' : 'NULL';
	}

	public function setIsIn( $intIsIn ) {
		$this->set( 'm_intIsIn', CStrings::strToIntDef( $intIsIn, NULL, false ) );
	}

	public function getIsIn() {
		return $this->m_intIsIn;
	}

	public function sqlIsIn() {
		return ( true == isset( $this->m_intIsIn ) ) ? ( string ) $this->m_intIsIn : 'NULL';
	}

	public function setIsManual( $intIsManual ) {
		$this->set( 'm_intIsManual', CStrings::strToIntDef( $intIsManual, NULL, false ) );
	}

	public function getIsManual() {
		return $this->m_intIsManual;
	}

	public function sqlIsManual() {
		return ( true == isset( $this->m_intIsManual ) ) ? ( string ) $this->m_intIsManual : '0';
	}

	public function setSwipeDatetime( $strSwipeDatetime ) {
		$this->set( 'm_strSwipeDatetime', CStrings::strTrimDef( $strSwipeDatetime, -1, NULL, true ) );
	}

	public function getSwipeDatetime() {
		return $this->m_strSwipeDatetime;
	}

	public function sqlSwipeDatetime() {
		return ( true == isset( $this->m_strSwipeDatetime ) ) ? '\'' . $this->m_strSwipeDatetime . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, employee_hour_id, card_number, gate_number, is_in, is_manual, swipe_datetime, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlEmployeeHourId() . ', ' .
 						$this->sqlCardNumber() . ', ' .
 						$this->sqlGateNumber() . ', ' .
 						$this->sqlIsIn() . ', ' .
 						$this->sqlIsManual() . ', ' .
 						$this->sqlSwipeDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_hour_id = ' . $this->sqlEmployeeHourId() . ','; } elseif( true == array_key_exists( 'EmployeeHourId', $this->getChangedColumns() ) ) { $strSql .= ' employee_hour_id = ' . $this->sqlEmployeeHourId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' card_number = ' . $this->sqlCardNumber() . ','; } elseif( true == array_key_exists( 'CardNumber', $this->getChangedColumns() ) ) { $strSql .= ' card_number = ' . $this->sqlCardNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gate_number = ' . $this->sqlGateNumber() . ','; } elseif( true == array_key_exists( 'GateNumber', $this->getChangedColumns() ) ) { $strSql .= ' gate_number = ' . $this->sqlGateNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_in = ' . $this->sqlIsIn() . ','; } elseif( true == array_key_exists( 'IsIn', $this->getChangedColumns() ) ) { $strSql .= ' is_in = ' . $this->sqlIsIn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual = ' . $this->sqlIsManual() . ','; } elseif( true == array_key_exists( 'IsManual', $this->getChangedColumns() ) ) { $strSql .= ' is_manual = ' . $this->sqlIsManual() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' swipe_datetime = ' . $this->sqlSwipeDatetime() . ','; } elseif( true == array_key_exists( 'SwipeDatetime', $this->getChangedColumns() ) ) { $strSql .= ' swipe_datetime = ' . $this->sqlSwipeDatetime() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'employee_hour_id' => $this->getEmployeeHourId(),
			'card_number' => $this->getCardNumber(),
			'gate_number' => $this->getGateNumber(),
			'is_in' => $this->getIsIn(),
			'is_manual' => $this->getIsManual(),
			'swipe_datetime' => $this->getSwipeDatetime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>