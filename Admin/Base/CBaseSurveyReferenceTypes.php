<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyReferenceTypes
 * Do not add any new functions to this class.
 */

class CBaseSurveyReferenceTypes extends CEosPluralBase {

	/**
	 * @return CSurveyReferenceType[]
	 */
	public static function fetchSurveyReferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyReferenceType', $objDatabase );
	}

	/**
	 * @return CSurveyReferenceType
	 */
	public static function fetchSurveyReferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyReferenceType', $objDatabase );
	}

	public static function fetchSurveyReferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_reference_types', $objDatabase );
	}

	public static function fetchSurveyReferenceTypeById( $intId, $objDatabase ) {
		return self::fetchSurveyReferenceType( sprintf( 'SELECT * FROM survey_reference_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>