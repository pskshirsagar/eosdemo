<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestSubmissionAnswers
 * Do not add any new functions to this class.
 */

class CBaseTestSubmissionAnswers extends CEosPluralBase {

	/**
	 * @return CTestSubmissionAnswer[]
	 */
	public static function fetchTestSubmissionAnswers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestSubmissionAnswer', $objDatabase );
	}

	/**
	 * @return CTestSubmissionAnswer
	 */
	public static function fetchTestSubmissionAnswer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestSubmissionAnswer', $objDatabase );
	}

	public static function fetchTestSubmissionAnswerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_submission_answers', $objDatabase );
	}

	public static function fetchTestSubmissionAnswerById( $intId, $objDatabase ) {
		return self::fetchTestSubmissionAnswer( sprintf( 'SELECT * FROM test_submission_answers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestSubmissionAnswersByTestSubmissionId( $intTestSubmissionId, $objDatabase ) {
		return self::fetchTestSubmissionAnswers( sprintf( 'SELECT * FROM test_submission_answers WHERE test_submission_id = %d', ( int ) $intTestSubmissionId ), $objDatabase );
	}

	public static function fetchTestSubmissionAnswersByTestQuestionId( $intTestQuestionId, $objDatabase ) {
		return self::fetchTestSubmissionAnswers( sprintf( 'SELECT * FROM test_submission_answers WHERE test_question_id = %d', ( int ) $intTestQuestionId ), $objDatabase );
	}

	public static function fetchTestSubmissionAnswersByTestAnswerOptionId( $intTestAnswerOptionId, $objDatabase ) {
		return self::fetchTestSubmissionAnswers( sprintf( 'SELECT * FROM test_submission_answers WHERE test_answer_option_id = %d', ( int ) $intTestAnswerOptionId ), $objDatabase );
	}

}
?>