<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEntities
 * Do not add any new functions to this class.
 */

class CBaseEntities extends CEosPluralBase {

	/**
	 * @return CEntity[]
	 */
	public static function fetchEntities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEntity', $objDatabase );
	}

	/**
	 * @return CEntity
	 */
	public static function fetchEntity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEntity', $objDatabase );
	}

	public static function fetchEntityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'entities', $objDatabase );
	}

	public static function fetchEntityById( $intId, $objDatabase ) {
		return self::fetchEntity( sprintf( 'SELECT * FROM entities WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEntitiesByLeaseId( $intLeaseId, $objDatabase ) {
		return self::fetchEntities( sprintf( 'SELECT * FROM entities WHERE lease_id = %d', ( int ) $intLeaseId ), $objDatabase );
	}

	public static function fetchEntitiesByCustomerId( $intCustomerId, $objDatabase ) {
		return self::fetchEntities( sprintf( 'SELECT * FROM entities WHERE customer_id = %d', ( int ) $intCustomerId ), $objDatabase );
	}

	public static function fetchEntitiesByApplicantId( $intApplicantId, $objDatabase ) {
		return self::fetchEntities( sprintf( 'SELECT * FROM entities WHERE applicant_id = %d', ( int ) $intApplicantId ), $objDatabase );
	}

	public static function fetchEntitiesByApplicationId( $intApplicationId, $objDatabase ) {
		return self::fetchEntities( sprintf( 'SELECT * FROM entities WHERE application_id = %d', ( int ) $intApplicationId ), $objDatabase );
	}

}
?>