<?php

class CBaseTemplateWidgetType extends CEosSingularBase {

	const TABLE_NAME = 'public.template_widget_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_intSupportedType;
	protected $m_strAllowableParameters;
	protected $m_intWidgetTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intWidgetTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['supported_type'] ) && $boolDirectSet ) $this->set( 'm_intSupportedType', trim( $arrValues['supported_type'] ) ); elseif( isset( $arrValues['supported_type'] ) ) $this->setSupportedType( $arrValues['supported_type'] );
		if( isset( $arrValues['allowable_parameters'] ) && $boolDirectSet ) $this->set( 'm_strAllowableParameters', trim( stripcslashes( $arrValues['allowable_parameters'] ) ) ); elseif( isset( $arrValues['allowable_parameters'] ) ) $this->setAllowableParameters( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['allowable_parameters'] ) : $arrValues['allowable_parameters'] );
		if( isset( $arrValues['widget_type_id'] ) && $boolDirectSet ) $this->set( 'm_intWidgetTypeId', trim( $arrValues['widget_type_id'] ) ); elseif( isset( $arrValues['widget_type_id'] ) ) $this->setWidgetTypeId( $arrValues['widget_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 250, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setSupportedType( $intSupportedType ) {
		$this->set( 'm_intSupportedType', CStrings::strToIntDef( $intSupportedType, NULL, false ) );
	}

	public function getSupportedType() {
		return $this->m_intSupportedType;
	}

	public function sqlSupportedType() {
		return ( true == isset( $this->m_intSupportedType ) ) ? ( string ) $this->m_intSupportedType : 'NULL';
	}

	public function setAllowableParameters( $strAllowableParameters ) {
		$this->set( 'm_strAllowableParameters', CStrings::strTrimDef( $strAllowableParameters, -1, NULL, true ) );
	}

	public function getAllowableParameters() {
		return $this->m_strAllowableParameters;
	}

	public function sqlAllowableParameters() {
		return ( true == isset( $this->m_strAllowableParameters ) ) ? '\'' . addslashes( $this->m_strAllowableParameters ) . '\'' : 'NULL';
	}

	public function setWidgetTypeId( $intWidgetTypeId ) {
		$this->set( 'm_intWidgetTypeId', CStrings::strToIntDef( $intWidgetTypeId, NULL, false ) );
	}

	public function getWidgetTypeId() {
		return $this->m_intWidgetTypeId;
	}

	public function sqlWidgetTypeId() {
		return ( true == isset( $this->m_intWidgetTypeId ) ) ? ( string ) $this->m_intWidgetTypeId : '1';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'supported_type' => $this->getSupportedType(),
			'allowable_parameters' => $this->getAllowableParameters(),
			'widget_type_id' => $this->getWidgetTypeId()
		);
	}

}
?>