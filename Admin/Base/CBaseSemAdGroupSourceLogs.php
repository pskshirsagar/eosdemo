<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdGroupSourceLogs
 * Do not add any new functions to this class.
 */

class CBaseSemAdGroupSourceLogs extends CEosPluralBase {

	/**
	 * @return CSemAdGroupSourceLog[]
	 */
	public static function fetchSemAdGroupSourceLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemAdGroupSourceLog', $objDatabase );
	}

	/**
	 * @return CSemAdGroupSourceLog
	 */
	public static function fetchSemAdGroupSourceLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemAdGroupSourceLog', $objDatabase );
	}

	public static function fetchSemAdGroupSourceLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_ad_group_source_logs', $objDatabase );
	}

	public static function fetchSemAdGroupSourceLogById( $intId, $objDatabase ) {
		return self::fetchSemAdGroupSourceLog( sprintf( 'SELECT * FROM sem_ad_group_source_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemAdGroupSourceLogsBySemSourceId( $intSemSourceId, $objDatabase ) {
		return self::fetchSemAdGroupSourceLogs( sprintf( 'SELECT * FROM sem_ad_group_source_logs WHERE sem_source_id = %d', ( int ) $intSemSourceId ), $objDatabase );
	}

	public static function fetchSemAdGroupSourceLogsBySemAccountId( $intSemAccountId, $objDatabase ) {
		return self::fetchSemAdGroupSourceLogs( sprintf( 'SELECT * FROM sem_ad_group_source_logs WHERE sem_account_id = %d', ( int ) $intSemAccountId ), $objDatabase );
	}

	public static function fetchSemAdGroupSourceLogsBySemCampaignId( $intSemCampaignId, $objDatabase ) {
		return self::fetchSemAdGroupSourceLogs( sprintf( 'SELECT * FROM sem_ad_group_source_logs WHERE sem_campaign_id = %d', ( int ) $intSemCampaignId ), $objDatabase );
	}

	public static function fetchSemAdGroupSourceLogsBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemAdGroupSourceLogs( sprintf( 'SELECT * FROM sem_ad_group_source_logs WHERE sem_ad_group_id = %d', ( int ) $intSemAdGroupId ), $objDatabase );
	}

	public static function fetchSemAdGroupSourceLogsByOldSemStatusTypeId( $intOldSemStatusTypeId, $objDatabase ) {
		return self::fetchSemAdGroupSourceLogs( sprintf( 'SELECT * FROM sem_ad_group_source_logs WHERE old_sem_status_type_id = %d', ( int ) $intOldSemStatusTypeId ), $objDatabase );
	}

	public static function fetchSemAdGroupSourceLogsByNewSemStatusTypeId( $intNewSemStatusTypeId, $objDatabase ) {
		return self::fetchSemAdGroupSourceLogs( sprintf( 'SELECT * FROM sem_ad_group_source_logs WHERE new_sem_status_type_id = %d', ( int ) $intNewSemStatusTypeId ), $objDatabase );
	}

}
?>