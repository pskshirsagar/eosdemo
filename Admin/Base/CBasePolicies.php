<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPolicies
 * Do not add any new functions to this class.
 */

class CBasePolicies extends CEosPluralBase {

	/**
	 * @return CPolicy[]
	 */
	public static function fetchPolicies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPolicy', $objDatabase );
	}

	/**
	 * @return CPolicy
	 */
	public static function fetchPolicy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPolicy', $objDatabase );
	}

	public static function fetchPolicyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'policies', $objDatabase );
	}

	public static function fetchPolicyById( $intId, $objDatabase ) {
		return self::fetchPolicy( sprintf( 'SELECT * FROM policies WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPoliciesByPsDocumentId( $intPsDocumentId, $objDatabase ) {
		return self::fetchPolicies( sprintf( 'SELECT * FROM policies WHERE ps_document_id = %d', ( int ) $intPsDocumentId ), $objDatabase );
	}

}
?>