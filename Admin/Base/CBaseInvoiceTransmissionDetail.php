<?php

class CBaseInvoiceTransmissionDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.invoice_transmission_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intInvoiceId;
	protected $m_intInvoiceTransmissionTypeId;
	protected $m_strInvoiceSentOn;
	protected $m_strInvoiceSentResponse;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceId', trim( $arrValues['invoice_id'] ) ); elseif( isset( $arrValues['invoice_id'] ) ) $this->setInvoiceId( $arrValues['invoice_id'] );
		if( isset( $arrValues['invoice_transmission_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceTransmissionTypeId', trim( $arrValues['invoice_transmission_type_id'] ) ); elseif( isset( $arrValues['invoice_transmission_type_id'] ) ) $this->setInvoiceTransmissionTypeId( $arrValues['invoice_transmission_type_id'] );
		if( isset( $arrValues['invoice_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceSentOn', trim( $arrValues['invoice_sent_on'] ) ); elseif( isset( $arrValues['invoice_sent_on'] ) ) $this->setInvoiceSentOn( $arrValues['invoice_sent_on'] );
		if( isset( $arrValues['invoice_sent_response'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceSentResponse', trim( stripcslashes( $arrValues['invoice_sent_response'] ) ) ); elseif( isset( $arrValues['invoice_sent_response'] ) ) $this->setInvoiceSentResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['invoice_sent_response'] ) : $arrValues['invoice_sent_response'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setInvoiceId( $intInvoiceId ) {
		$this->set( 'm_intInvoiceId', CStrings::strToIntDef( $intInvoiceId, NULL, false ) );
	}

	public function getInvoiceId() {
		return $this->m_intInvoiceId;
	}

	public function sqlInvoiceId() {
		return ( true == isset( $this->m_intInvoiceId ) ) ? ( string ) $this->m_intInvoiceId : 'NULL';
	}

	public function setInvoiceTransmissionTypeId( $intInvoiceTransmissionTypeId ) {
		$this->set( 'm_intInvoiceTransmissionTypeId', CStrings::strToIntDef( $intInvoiceTransmissionTypeId, NULL, false ) );
	}

	public function getInvoiceTransmissionTypeId() {
		return $this->m_intInvoiceTransmissionTypeId;
	}

	public function sqlInvoiceTransmissionTypeId() {
		return ( true == isset( $this->m_intInvoiceTransmissionTypeId ) ) ? ( string ) $this->m_intInvoiceTransmissionTypeId : 'NULL';
	}

	public function setInvoiceSentOn( $strInvoiceSentOn ) {
		$this->set( 'm_strInvoiceSentOn', CStrings::strTrimDef( $strInvoiceSentOn, -1, NULL, true ) );
	}

	public function getInvoiceSentOn() {
		return $this->m_strInvoiceSentOn;
	}

	public function sqlInvoiceSentOn() {
		return ( true == isset( $this->m_strInvoiceSentOn ) ) ? '\'' . $this->m_strInvoiceSentOn . '\'' : 'NULL';
	}

	public function setInvoiceSentResponse( $strInvoiceSentResponse ) {
		$this->set( 'm_strInvoiceSentResponse', CStrings::strTrimDef( $strInvoiceSentResponse, 240, NULL, true ) );
	}

	public function getInvoiceSentResponse() {
		return $this->m_strInvoiceSentResponse;
	}

	public function sqlInvoiceSentResponse() {
		return ( true == isset( $this->m_strInvoiceSentResponse ) ) ? '\'' . addslashes( $this->m_strInvoiceSentResponse ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, invoice_id, invoice_transmission_type_id, invoice_sent_on, invoice_sent_response, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlAccountId() . ', ' .
 						$this->sqlInvoiceId() . ', ' .
 						$this->sqlInvoiceTransmissionTypeId() . ', ' .
 						$this->sqlInvoiceSentOn() . ', ' .
 						$this->sqlInvoiceSentResponse() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_id = ' . $this->sqlInvoiceId() . ','; } elseif( true == array_key_exists( 'InvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_id = ' . $this->sqlInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_transmission_type_id = ' . $this->sqlInvoiceTransmissionTypeId() . ','; } elseif( true == array_key_exists( 'InvoiceTransmissionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_transmission_type_id = ' . $this->sqlInvoiceTransmissionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_sent_on = ' . $this->sqlInvoiceSentOn() . ','; } elseif( true == array_key_exists( 'InvoiceSentOn', $this->getChangedColumns() ) ) { $strSql .= ' invoice_sent_on = ' . $this->sqlInvoiceSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_sent_response = ' . $this->sqlInvoiceSentResponse() . ','; } elseif( true == array_key_exists( 'InvoiceSentResponse', $this->getChangedColumns() ) ) { $strSql .= ' invoice_sent_response = ' . $this->sqlInvoiceSentResponse() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'invoice_id' => $this->getInvoiceId(),
			'invoice_transmission_type_id' => $this->getInvoiceTransmissionTypeId(),
			'invoice_sent_on' => $this->getInvoiceSentOn(),
			'invoice_sent_response' => $this->getInvoiceSentResponse(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>