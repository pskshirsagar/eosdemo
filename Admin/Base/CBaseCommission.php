<?php

class CBaseCommission extends CEosSingularBase {

	const TABLE_NAME = 'public.commissions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intReversedCommissionId;
	protected $m_intTransactionId;
	protected $m_intArPaymentId;
	protected $m_intCommissionRecipientId;
	protected $m_intCommissionStructureId;
	protected $m_intCommissionRateId;
	protected $m_intCommissionFrontLoadId;
	protected $m_intCommissionPaymentId;
	protected $m_intCommissionBatchId;
	protected $m_intChargeCodeId;
	protected $m_intCommissionTypeId;
	protected $m_intCommissionFrontLoadTypeId;
	protected $m_intCommissionRateAssociationId;
	protected $m_intOriginatingTransactionNumber;
	protected $m_intTransactionNumber;
	protected $m_strCommissionDatetime;
	protected $m_fltCommissionAmount;
	protected $m_fltUnallocatedAmount;
	protected $m_strCommissionMemo;
	protected $m_intIsManualAdjustment;
	protected $m_intExportedBy;
	protected $m_strExportedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strCurrencyCode;
	protected $m_intCurrencyRateId;

	public function __construct() {
		parent::__construct();

		$this->m_intCommissionTypeId = '8';
		$this->m_intIsManualAdjustment = '0';
		$this->m_strCurrencyCode = 'USD';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['reversed_commission_id'] ) && $boolDirectSet ) $this->set( 'm_intReversedCommissionId', trim( $arrValues['reversed_commission_id'] ) ); elseif( isset( $arrValues['reversed_commission_id'] ) ) $this->setReversedCommissionId( $arrValues['reversed_commission_id'] );
		if( isset( $arrValues['transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionId', trim( $arrValues['transaction_id'] ) ); elseif( isset( $arrValues['transaction_id'] ) ) $this->setTransactionId( $arrValues['transaction_id'] );
		if( isset( $arrValues['ar_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intArPaymentId', trim( $arrValues['ar_payment_id'] ) ); elseif( isset( $arrValues['ar_payment_id'] ) ) $this->setArPaymentId( $arrValues['ar_payment_id'] );
		if( isset( $arrValues['commission_recipient_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionRecipientId', trim( $arrValues['commission_recipient_id'] ) ); elseif( isset( $arrValues['commission_recipient_id'] ) ) $this->setCommissionRecipientId( $arrValues['commission_recipient_id'] );
		if( isset( $arrValues['commission_structure_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionStructureId', trim( $arrValues['commission_structure_id'] ) ); elseif( isset( $arrValues['commission_structure_id'] ) ) $this->setCommissionStructureId( $arrValues['commission_structure_id'] );
		if( isset( $arrValues['commission_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionRateId', trim( $arrValues['commission_rate_id'] ) ); elseif( isset( $arrValues['commission_rate_id'] ) ) $this->setCommissionRateId( $arrValues['commission_rate_id'] );
		if( isset( $arrValues['commission_front_load_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionFrontLoadId', trim( $arrValues['commission_front_load_id'] ) ); elseif( isset( $arrValues['commission_front_load_id'] ) ) $this->setCommissionFrontLoadId( $arrValues['commission_front_load_id'] );
		if( isset( $arrValues['commission_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionPaymentId', trim( $arrValues['commission_payment_id'] ) ); elseif( isset( $arrValues['commission_payment_id'] ) ) $this->setCommissionPaymentId( $arrValues['commission_payment_id'] );
		if( isset( $arrValues['commission_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionBatchId', trim( $arrValues['commission_batch_id'] ) ); elseif( isset( $arrValues['commission_batch_id'] ) ) $this->setCommissionBatchId( $arrValues['commission_batch_id'] );
		if( isset( $arrValues['charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeCodeId', trim( $arrValues['charge_code_id'] ) ); elseif( isset( $arrValues['charge_code_id'] ) ) $this->setChargeCodeId( $arrValues['charge_code_id'] );
		if( isset( $arrValues['commission_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionTypeId', trim( $arrValues['commission_type_id'] ) ); elseif( isset( $arrValues['commission_type_id'] ) ) $this->setCommissionTypeId( $arrValues['commission_type_id'] );
		if( isset( $arrValues['commission_front_load_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionFrontLoadTypeId', trim( $arrValues['commission_front_load_type_id'] ) ); elseif( isset( $arrValues['commission_front_load_type_id'] ) ) $this->setCommissionFrontLoadTypeId( $arrValues['commission_front_load_type_id'] );
		if( isset( $arrValues['commission_rate_association_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionRateAssociationId', trim( $arrValues['commission_rate_association_id'] ) ); elseif( isset( $arrValues['commission_rate_association_id'] ) ) $this->setCommissionRateAssociationId( $arrValues['commission_rate_association_id'] );
		if( isset( $arrValues['originating_transaction_number'] ) && $boolDirectSet ) $this->set( 'm_intOriginatingTransactionNumber', trim( $arrValues['originating_transaction_number'] ) ); elseif( isset( $arrValues['originating_transaction_number'] ) ) $this->setOriginatingTransactionNumber( $arrValues['originating_transaction_number'] );
		if( isset( $arrValues['transaction_number'] ) && $boolDirectSet ) $this->set( 'm_intTransactionNumber', trim( $arrValues['transaction_number'] ) ); elseif( isset( $arrValues['transaction_number'] ) ) $this->setTransactionNumber( $arrValues['transaction_number'] );
		if( isset( $arrValues['commission_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCommissionDatetime', trim( $arrValues['commission_datetime'] ) ); elseif( isset( $arrValues['commission_datetime'] ) ) $this->setCommissionDatetime( $arrValues['commission_datetime'] );
		if( isset( $arrValues['commission_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCommissionAmount', trim( $arrValues['commission_amount'] ) ); elseif( isset( $arrValues['commission_amount'] ) ) $this->setCommissionAmount( $arrValues['commission_amount'] );
		if( isset( $arrValues['unallocated_amount'] ) && $boolDirectSet ) $this->set( 'm_fltUnallocatedAmount', trim( $arrValues['unallocated_amount'] ) ); elseif( isset( $arrValues['unallocated_amount'] ) ) $this->setUnallocatedAmount( $arrValues['unallocated_amount'] );
		if( isset( $arrValues['commission_memo'] ) && $boolDirectSet ) $this->set( 'm_strCommissionMemo', trim( $arrValues['commission_memo'] ) ); elseif( isset( $arrValues['commission_memo'] ) ) $this->setCommissionMemo( $arrValues['commission_memo'] );
		if( isset( $arrValues['is_manual_adjustment'] ) && $boolDirectSet ) $this->set( 'm_intIsManualAdjustment', trim( $arrValues['is_manual_adjustment'] ) ); elseif( isset( $arrValues['is_manual_adjustment'] ) ) $this->setIsManualAdjustment( $arrValues['is_manual_adjustment'] );
		if( isset( $arrValues['exported_by'] ) && $boolDirectSet ) $this->set( 'm_intExportedBy', trim( $arrValues['exported_by'] ) ); elseif( isset( $arrValues['exported_by'] ) ) $this->setExportedBy( $arrValues['exported_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( $arrValues['currency_code'] ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( $arrValues['currency_code'] );
		if( isset( $arrValues['currency_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intCurrencyRateId', trim( $arrValues['currency_rate_id'] ) ); elseif( isset( $arrValues['currency_rate_id'] ) ) $this->setCurrencyRateId( $arrValues['currency_rate_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setReversedCommissionId( $intReversedCommissionId ) {
		$this->set( 'm_intReversedCommissionId', CStrings::strToIntDef( $intReversedCommissionId, NULL, false ) );
	}

	public function getReversedCommissionId() {
		return $this->m_intReversedCommissionId;
	}

	public function sqlReversedCommissionId() {
		return ( true == isset( $this->m_intReversedCommissionId ) ) ? ( string ) $this->m_intReversedCommissionId : 'NULL';
	}

	public function setTransactionId( $intTransactionId ) {
		$this->set( 'm_intTransactionId', CStrings::strToIntDef( $intTransactionId, NULL, false ) );
	}

	public function getTransactionId() {
		return $this->m_intTransactionId;
	}

	public function sqlTransactionId() {
		return ( true == isset( $this->m_intTransactionId ) ) ? ( string ) $this->m_intTransactionId : 'NULL';
	}

	public function setArPaymentId( $intArPaymentId ) {
		$this->set( 'm_intArPaymentId', CStrings::strToIntDef( $intArPaymentId, NULL, false ) );
	}

	public function getArPaymentId() {
		return $this->m_intArPaymentId;
	}

	public function sqlArPaymentId() {
		return ( true == isset( $this->m_intArPaymentId ) ) ? ( string ) $this->m_intArPaymentId : 'NULL';
	}

	public function setCommissionRecipientId( $intCommissionRecipientId ) {
		$this->set( 'm_intCommissionRecipientId', CStrings::strToIntDef( $intCommissionRecipientId, NULL, false ) );
	}

	public function getCommissionRecipientId() {
		return $this->m_intCommissionRecipientId;
	}

	public function sqlCommissionRecipientId() {
		return ( true == isset( $this->m_intCommissionRecipientId ) ) ? ( string ) $this->m_intCommissionRecipientId : 'NULL';
	}

	public function setCommissionStructureId( $intCommissionStructureId ) {
		$this->set( 'm_intCommissionStructureId', CStrings::strToIntDef( $intCommissionStructureId, NULL, false ) );
	}

	public function getCommissionStructureId() {
		return $this->m_intCommissionStructureId;
	}

	public function sqlCommissionStructureId() {
		return ( true == isset( $this->m_intCommissionStructureId ) ) ? ( string ) $this->m_intCommissionStructureId : 'NULL';
	}

	public function setCommissionRateId( $intCommissionRateId ) {
		$this->set( 'm_intCommissionRateId', CStrings::strToIntDef( $intCommissionRateId, NULL, false ) );
	}

	public function getCommissionRateId() {
		return $this->m_intCommissionRateId;
	}

	public function sqlCommissionRateId() {
		return ( true == isset( $this->m_intCommissionRateId ) ) ? ( string ) $this->m_intCommissionRateId : 'NULL';
	}

	public function setCommissionFrontLoadId( $intCommissionFrontLoadId ) {
		$this->set( 'm_intCommissionFrontLoadId', CStrings::strToIntDef( $intCommissionFrontLoadId, NULL, false ) );
	}

	public function getCommissionFrontLoadId() {
		return $this->m_intCommissionFrontLoadId;
	}

	public function sqlCommissionFrontLoadId() {
		return ( true == isset( $this->m_intCommissionFrontLoadId ) ) ? ( string ) $this->m_intCommissionFrontLoadId : 'NULL';
	}

	public function setCommissionPaymentId( $intCommissionPaymentId ) {
		$this->set( 'm_intCommissionPaymentId', CStrings::strToIntDef( $intCommissionPaymentId, NULL, false ) );
	}

	public function getCommissionPaymentId() {
		return $this->m_intCommissionPaymentId;
	}

	public function sqlCommissionPaymentId() {
		return ( true == isset( $this->m_intCommissionPaymentId ) ) ? ( string ) $this->m_intCommissionPaymentId : 'NULL';
	}

	public function setCommissionBatchId( $intCommissionBatchId ) {
		$this->set( 'm_intCommissionBatchId', CStrings::strToIntDef( $intCommissionBatchId, NULL, false ) );
	}

	public function getCommissionBatchId() {
		return $this->m_intCommissionBatchId;
	}

	public function sqlCommissionBatchId() {
		return ( true == isset( $this->m_intCommissionBatchId ) ) ? ( string ) $this->m_intCommissionBatchId : 'NULL';
	}

	public function setChargeCodeId( $intChargeCodeId ) {
		$this->set( 'm_intChargeCodeId', CStrings::strToIntDef( $intChargeCodeId, NULL, false ) );
	}

	public function getChargeCodeId() {
		return $this->m_intChargeCodeId;
	}

	public function sqlChargeCodeId() {
		return ( true == isset( $this->m_intChargeCodeId ) ) ? ( string ) $this->m_intChargeCodeId : 'NULL';
	}

	public function setCommissionTypeId( $intCommissionTypeId ) {
		$this->set( 'm_intCommissionTypeId', CStrings::strToIntDef( $intCommissionTypeId, NULL, false ) );
	}

	public function getCommissionTypeId() {
		return $this->m_intCommissionTypeId;
	}

	public function sqlCommissionTypeId() {
		return ( true == isset( $this->m_intCommissionTypeId ) ) ? ( string ) $this->m_intCommissionTypeId : '8';
	}

	public function setCommissionFrontLoadTypeId( $intCommissionFrontLoadTypeId ) {
		$this->set( 'm_intCommissionFrontLoadTypeId', CStrings::strToIntDef( $intCommissionFrontLoadTypeId, NULL, false ) );
	}

	public function getCommissionFrontLoadTypeId() {
		return $this->m_intCommissionFrontLoadTypeId;
	}

	public function sqlCommissionFrontLoadTypeId() {
		return ( true == isset( $this->m_intCommissionFrontLoadTypeId ) ) ? ( string ) $this->m_intCommissionFrontLoadTypeId : 'NULL';
	}

	public function setCommissionRateAssociationId( $intCommissionRateAssociationId ) {
		$this->set( 'm_intCommissionRateAssociationId', CStrings::strToIntDef( $intCommissionRateAssociationId, NULL, false ) );
	}

	public function getCommissionRateAssociationId() {
		return $this->m_intCommissionRateAssociationId;
	}

	public function sqlCommissionRateAssociationId() {
		return ( true == isset( $this->m_intCommissionRateAssociationId ) ) ? ( string ) $this->m_intCommissionRateAssociationId : 'NULL';
	}

	public function setOriginatingTransactionNumber( $intOriginatingTransactionNumber ) {
		$this->set( 'm_intOriginatingTransactionNumber', CStrings::strToIntDef( $intOriginatingTransactionNumber, NULL, false ) );
	}

	public function getOriginatingTransactionNumber() {
		return $this->m_intOriginatingTransactionNumber;
	}

	public function sqlOriginatingTransactionNumber() {
		return ( true == isset( $this->m_intOriginatingTransactionNumber ) ) ? ( string ) $this->m_intOriginatingTransactionNumber : 'NULL';
	}

	public function setTransactionNumber( $intTransactionNumber ) {
		$this->set( 'm_intTransactionNumber', CStrings::strToIntDef( $intTransactionNumber, NULL, false ) );
	}

	public function getTransactionNumber() {
		return $this->m_intTransactionNumber;
	}

	public function sqlTransactionNumber() {
		return ( true == isset( $this->m_intTransactionNumber ) ) ? ( string ) $this->m_intTransactionNumber : 'nextval( \'commissions_transaction_number_seq\'::regclass )';
	}

	public function setCommissionDatetime( $strCommissionDatetime ) {
		$this->set( 'm_strCommissionDatetime', CStrings::strTrimDef( $strCommissionDatetime, -1, NULL, true ) );
	}

	public function getCommissionDatetime() {
		return $this->m_strCommissionDatetime;
	}

	public function sqlCommissionDatetime() {
		return ( true == isset( $this->m_strCommissionDatetime ) ) ? '\'' . $this->m_strCommissionDatetime . '\'' : 'NOW()';
	}

	public function setCommissionAmount( $fltCommissionAmount ) {
		$this->set( 'm_fltCommissionAmount', CStrings::strToFloatDef( $fltCommissionAmount, NULL, false, 2 ) );
	}

	public function getCommissionAmount() {
		return $this->m_fltCommissionAmount;
	}

	public function sqlCommissionAmount() {
		return ( true == isset( $this->m_fltCommissionAmount ) ) ? ( string ) $this->m_fltCommissionAmount : 'NULL';
	}

	public function setUnallocatedAmount( $fltUnallocatedAmount ) {
		$this->set( 'm_fltUnallocatedAmount', CStrings::strToFloatDef( $fltUnallocatedAmount, NULL, false, 2 ) );
	}

	public function getUnallocatedAmount() {
		return $this->m_fltUnallocatedAmount;
	}

	public function sqlUnallocatedAmount() {
		return ( true == isset( $this->m_fltUnallocatedAmount ) ) ? ( string ) $this->m_fltUnallocatedAmount : 'NULL';
	}

	public function setCommissionMemo( $strCommissionMemo ) {
		$this->set( 'm_strCommissionMemo', CStrings::strTrimDef( $strCommissionMemo, 2000, NULL, true ) );
	}

	public function getCommissionMemo() {
		return $this->m_strCommissionMemo;
	}

	public function sqlCommissionMemo() {
		return ( true == isset( $this->m_strCommissionMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCommissionMemo ) : '\'' . addslashes( $this->m_strCommissionMemo ) . '\'' ) : 'NULL';
	}

	public function setIsManualAdjustment( $intIsManualAdjustment ) {
		$this->set( 'm_intIsManualAdjustment', CStrings::strToIntDef( $intIsManualAdjustment, NULL, false ) );
	}

	public function getIsManualAdjustment() {
		return $this->m_intIsManualAdjustment;
	}

	public function sqlIsManualAdjustment() {
		return ( true == isset( $this->m_intIsManualAdjustment ) ) ? ( string ) $this->m_intIsManualAdjustment : '0';
	}

	public function setExportedBy( $intExportedBy ) {
		$this->set( 'm_intExportedBy', CStrings::strToIntDef( $intExportedBy, NULL, false ) );
	}

	public function getExportedBy() {
		return $this->m_intExportedBy;
	}

	public function sqlExportedBy() {
		return ( true == isset( $this->m_intExportedBy ) ) ? ( string ) $this->m_intExportedBy : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrencyCode ) : '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' ) : '\'USD\'';
	}

	public function setCurrencyRateId( $intCurrencyRateId ) {
		$this->set( 'm_intCurrencyRateId', CStrings::strToIntDef( $intCurrencyRateId, NULL, false ) );
	}

	public function getCurrencyRateId() {
		return $this->m_intCurrencyRateId;
	}

	public function sqlCurrencyRateId() {
		return ( true == isset( $this->m_intCurrencyRateId ) ) ? ( string ) $this->m_intCurrencyRateId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, reversed_commission_id, transaction_id, ar_payment_id, commission_recipient_id, commission_structure_id, commission_rate_id, commission_front_load_id, commission_payment_id, commission_batch_id, charge_code_id, commission_type_id, commission_front_load_type_id, commission_rate_association_id, originating_transaction_number, transaction_number, commission_datetime, commission_amount, unallocated_amount, commission_memo, is_manual_adjustment, exported_by, exported_on, approved_by, approved_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, currency_code, currency_rate_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlReversedCommissionId() . ', ' .
						$this->sqlTransactionId() . ', ' .
						$this->sqlArPaymentId() . ', ' .
						$this->sqlCommissionRecipientId() . ', ' .
						$this->sqlCommissionStructureId() . ', ' .
						$this->sqlCommissionRateId() . ', ' .
						$this->sqlCommissionFrontLoadId() . ', ' .
						$this->sqlCommissionPaymentId() . ', ' .
						$this->sqlCommissionBatchId() . ', ' .
						$this->sqlChargeCodeId() . ', ' .
						$this->sqlCommissionTypeId() . ', ' .
						$this->sqlCommissionFrontLoadTypeId() . ', ' .
						$this->sqlCommissionRateAssociationId() . ', ' .
						$this->sqlOriginatingTransactionNumber() . ', ' .
						$this->sqlTransactionNumber() . ', ' .
						$this->sqlCommissionDatetime() . ', ' .
						$this->sqlCommissionAmount() . ', ' .
						$this->sqlUnallocatedAmount() . ', ' .
						$this->sqlCommissionMemo() . ', ' .
						$this->sqlIsManualAdjustment() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCurrencyCode() . ', ' .
						$this->sqlCurrencyRateId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reversed_commission_id = ' . $this->sqlReversedCommissionId(). ',' ; } elseif( true == array_key_exists( 'ReversedCommissionId', $this->getChangedColumns() ) ) { $strSql .= ' reversed_commission_id = ' . $this->sqlReversedCommissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId(). ',' ; } elseif( true == array_key_exists( 'TransactionId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId(). ',' ; } elseif( true == array_key_exists( 'ArPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' ar_payment_id = ' . $this->sqlArPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_recipient_id = ' . $this->sqlCommissionRecipientId(). ',' ; } elseif( true == array_key_exists( 'CommissionRecipientId', $this->getChangedColumns() ) ) { $strSql .= ' commission_recipient_id = ' . $this->sqlCommissionRecipientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_structure_id = ' . $this->sqlCommissionStructureId(). ',' ; } elseif( true == array_key_exists( 'CommissionStructureId', $this->getChangedColumns() ) ) { $strSql .= ' commission_structure_id = ' . $this->sqlCommissionStructureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_rate_id = ' . $this->sqlCommissionRateId(). ',' ; } elseif( true == array_key_exists( 'CommissionRateId', $this->getChangedColumns() ) ) { $strSql .= ' commission_rate_id = ' . $this->sqlCommissionRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_front_load_id = ' . $this->sqlCommissionFrontLoadId(). ',' ; } elseif( true == array_key_exists( 'CommissionFrontLoadId', $this->getChangedColumns() ) ) { $strSql .= ' commission_front_load_id = ' . $this->sqlCommissionFrontLoadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_payment_id = ' . $this->sqlCommissionPaymentId(). ',' ; } elseif( true == array_key_exists( 'CommissionPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' commission_payment_id = ' . $this->sqlCommissionPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_batch_id = ' . $this->sqlCommissionBatchId(). ',' ; } elseif( true == array_key_exists( 'CommissionBatchId', $this->getChangedColumns() ) ) { $strSql .= ' commission_batch_id = ' . $this->sqlCommissionBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId(). ',' ; } elseif( true == array_key_exists( 'ChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_type_id = ' . $this->sqlCommissionTypeId(). ',' ; } elseif( true == array_key_exists( 'CommissionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' commission_type_id = ' . $this->sqlCommissionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_front_load_type_id = ' . $this->sqlCommissionFrontLoadTypeId(). ',' ; } elseif( true == array_key_exists( 'CommissionFrontLoadTypeId', $this->getChangedColumns() ) ) { $strSql .= ' commission_front_load_type_id = ' . $this->sqlCommissionFrontLoadTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_rate_association_id = ' . $this->sqlCommissionRateAssociationId(). ',' ; } elseif( true == array_key_exists( 'CommissionRateAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' commission_rate_association_id = ' . $this->sqlCommissionRateAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' originating_transaction_number = ' . $this->sqlOriginatingTransactionNumber(). ',' ; } elseif( true == array_key_exists( 'OriginatingTransactionNumber', $this->getChangedColumns() ) ) { $strSql .= ' originating_transaction_number = ' . $this->sqlOriginatingTransactionNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_number = ' . $this->sqlTransactionNumber(). ',' ; } elseif( true == array_key_exists( 'TransactionNumber', $this->getChangedColumns() ) ) { $strSql .= ' transaction_number = ' . $this->sqlTransactionNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_datetime = ' . $this->sqlCommissionDatetime(). ',' ; } elseif( true == array_key_exists( 'CommissionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' commission_datetime = ' . $this->sqlCommissionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_amount = ' . $this->sqlCommissionAmount(). ',' ; } elseif( true == array_key_exists( 'CommissionAmount', $this->getChangedColumns() ) ) { $strSql .= ' commission_amount = ' . $this->sqlCommissionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unallocated_amount = ' . $this->sqlUnallocatedAmount(). ',' ; } elseif( true == array_key_exists( 'UnallocatedAmount', $this->getChangedColumns() ) ) { $strSql .= ' unallocated_amount = ' . $this->sqlUnallocatedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_memo = ' . $this->sqlCommissionMemo(). ',' ; } elseif( true == array_key_exists( 'CommissionMemo', $this->getChangedColumns() ) ) { $strSql .= ' commission_memo = ' . $this->sqlCommissionMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual_adjustment = ' . $this->sqlIsManualAdjustment(). ',' ; } elseif( true == array_key_exists( 'IsManualAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' is_manual_adjustment = ' . $this->sqlIsManualAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy(). ',' ; } elseif( true == array_key_exists( 'ExportedBy', $this->getChangedColumns() ) ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_rate_id = ' . $this->sqlCurrencyRateId(). ',' ; } elseif( true == array_key_exists( 'CurrencyRateId', $this->getChangedColumns() ) ) { $strSql .= ' currency_rate_id = ' . $this->sqlCurrencyRateId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'reversed_commission_id' => $this->getReversedCommissionId(),
			'transaction_id' => $this->getTransactionId(),
			'ar_payment_id' => $this->getArPaymentId(),
			'commission_recipient_id' => $this->getCommissionRecipientId(),
			'commission_structure_id' => $this->getCommissionStructureId(),
			'commission_rate_id' => $this->getCommissionRateId(),
			'commission_front_load_id' => $this->getCommissionFrontLoadId(),
			'commission_payment_id' => $this->getCommissionPaymentId(),
			'commission_batch_id' => $this->getCommissionBatchId(),
			'charge_code_id' => $this->getChargeCodeId(),
			'commission_type_id' => $this->getCommissionTypeId(),
			'commission_front_load_type_id' => $this->getCommissionFrontLoadTypeId(),
			'commission_rate_association_id' => $this->getCommissionRateAssociationId(),
			'originating_transaction_number' => $this->getOriginatingTransactionNumber(),
			'transaction_number' => $this->getTransactionNumber(),
			'commission_datetime' => $this->getCommissionDatetime(),
			'commission_amount' => $this->getCommissionAmount(),
			'unallocated_amount' => $this->getUnallocatedAmount(),
			'commission_memo' => $this->getCommissionMemo(),
			'is_manual_adjustment' => $this->getIsManualAdjustment(),
			'exported_by' => $this->getExportedBy(),
			'exported_on' => $this->getExportedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'currency_code' => $this->getCurrencyCode(),
			'currency_rate_id' => $this->getCurrencyRateId()
		);
	}

}
?>