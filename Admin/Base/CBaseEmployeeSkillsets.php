<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeSkillsets
 * Do not add any new functions to this class.
 */

class CBaseEmployeeSkillsets extends CEosPluralBase {

	/**
	 * @return CEmployeeSkillset[]
	 */
	public static function fetchEmployeeSkillsets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeSkillset', $objDatabase );
	}

	/**
	 * @return CEmployeeSkillset
	 */
	public static function fetchEmployeeSkillset( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeSkillset', $objDatabase );
	}

	public static function fetchEmployeeSkillsetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_skillsets', $objDatabase );
	}

	public static function fetchEmployeeSkillsetById( $intId, $objDatabase ) {
		return self::fetchEmployeeSkillset( sprintf( 'SELECT * FROM employee_skillsets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeSkillsetsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeSkillsets( sprintf( 'SELECT * FROM employee_skillsets WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeSkillsetsBySkillsetsId( $intSkillsetsId, $objDatabase ) {
		return self::fetchEmployeeSkillsets( sprintf( 'SELECT * FROM employee_skillsets WHERE skillsets_id = %d', ( int ) $intSkillsetsId ), $objDatabase );
	}

	public static function fetchEmployeeSkillsetsByEvaluatorId( $intEvaluatorId, $objDatabase ) {
		return self::fetchEmployeeSkillsets( sprintf( 'SELECT * FROM employee_skillsets WHERE evaluator_id = %d', ( int ) $intEvaluatorId ), $objDatabase );
	}

}
?>