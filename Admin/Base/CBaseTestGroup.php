<?php

class CBaseTestGroup extends CEosSingularBase {

	const TABLE_NAME = 'public.test_groups';

	protected $m_intId;
	protected $m_intTestId;
	protected $m_intGroupId;
	protected $m_intDepartmentId;
	protected $m_intOfficeId;
	protected $m_intTeamId;
	protected $m_intDesignationId;
	protected $m_intPsWebsiteJobPostingId;
	protected $m_intTrainingSessionId;
	protected $m_intCertificationId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['test_id'] ) && $boolDirectSet ) $this->set( 'm_intTestId', trim( $arrValues['test_id'] ) ); elseif( isset( $arrValues['test_id'] ) ) $this->setTestId( $arrValues['test_id'] );
		if( isset( $arrValues['group_id'] ) && $boolDirectSet ) $this->set( 'm_intGroupId', trim( $arrValues['group_id'] ) ); elseif( isset( $arrValues['group_id'] ) ) $this->setGroupId( $arrValues['group_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['office_id'] ) && $boolDirectSet ) $this->set( 'm_intOfficeId', trim( $arrValues['office_id'] ) ); elseif( isset( $arrValues['office_id'] ) ) $this->setOfficeId( $arrValues['office_id'] );
		if( isset( $arrValues['team_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamId', trim( $arrValues['team_id'] ) ); elseif( isset( $arrValues['team_id'] ) ) $this->setTeamId( $arrValues['team_id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['ps_website_job_posting_id'] ) && $boolDirectSet ) $this->set( 'm_intPsWebsiteJobPostingId', trim( $arrValues['ps_website_job_posting_id'] ) ); elseif( isset( $arrValues['ps_website_job_posting_id'] ) ) $this->setPsWebsiteJobPostingId( $arrValues['ps_website_job_posting_id'] );
		if( isset( $arrValues['training_session_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingSessionId', trim( $arrValues['training_session_id'] ) ); elseif( isset( $arrValues['training_session_id'] ) ) $this->setTrainingSessionId( $arrValues['training_session_id'] );
		if( isset( $arrValues['certification_id'] ) && $boolDirectSet ) $this->set( 'm_intCertificationId', trim( $arrValues['certification_id'] ) ); elseif( isset( $arrValues['certification_id'] ) ) $this->setCertificationId( $arrValues['certification_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTestId( $intTestId ) {
		$this->set( 'm_intTestId', CStrings::strToIntDef( $intTestId, NULL, false ) );
	}

	public function getTestId() {
		return $this->m_intTestId;
	}

	public function sqlTestId() {
		return ( true == isset( $this->m_intTestId ) ) ? ( string ) $this->m_intTestId : 'NULL';
	}

	public function setGroupId( $intGroupId ) {
		$this->set( 'm_intGroupId', CStrings::strToIntDef( $intGroupId, NULL, false ) );
	}

	public function getGroupId() {
		return $this->m_intGroupId;
	}

	public function sqlGroupId() {
		return ( true == isset( $this->m_intGroupId ) ) ? ( string ) $this->m_intGroupId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setOfficeId( $intOfficeId ) {
		$this->set( 'm_intOfficeId', CStrings::strToIntDef( $intOfficeId, NULL, false ) );
	}

	public function getOfficeId() {
		return $this->m_intOfficeId;
	}

	public function sqlOfficeId() {
		return ( true == isset( $this->m_intOfficeId ) ) ? ( string ) $this->m_intOfficeId : 'NULL';
	}

	public function setTeamId( $intTeamId ) {
		$this->set( 'm_intTeamId', CStrings::strToIntDef( $intTeamId, NULL, false ) );
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function sqlTeamId() {
		return ( true == isset( $this->m_intTeamId ) ) ? ( string ) $this->m_intTeamId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setPsWebsiteJobPostingId( $intPsWebsiteJobPostingId ) {
		$this->set( 'm_intPsWebsiteJobPostingId', CStrings::strToIntDef( $intPsWebsiteJobPostingId, NULL, false ) );
	}

	public function getPsWebsiteJobPostingId() {
		return $this->m_intPsWebsiteJobPostingId;
	}

	public function sqlPsWebsiteJobPostingId() {
		return ( true == isset( $this->m_intPsWebsiteJobPostingId ) ) ? ( string ) $this->m_intPsWebsiteJobPostingId : 'NULL';
	}

	public function setTrainingSessionId( $intTrainingSessionId ) {
		$this->set( 'm_intTrainingSessionId', CStrings::strToIntDef( $intTrainingSessionId, NULL, false ) );
	}

	public function getTrainingSessionId() {
		return $this->m_intTrainingSessionId;
	}

	public function sqlTrainingSessionId() {
		return ( true == isset( $this->m_intTrainingSessionId ) ) ? ( string ) $this->m_intTrainingSessionId : 'NULL';
	}

	public function setCertificationId( $intCertificationId ) {
		$this->set( 'm_intCertificationId', CStrings::strToIntDef( $intCertificationId, NULL, false ) );
	}

	public function getCertificationId() {
		return $this->m_intCertificationId;
	}

	public function sqlCertificationId() {
		return ( true == isset( $this->m_intCertificationId ) ) ? ( string ) $this->m_intCertificationId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, test_id, group_id, department_id, office_id, team_id, designation_id, ps_website_job_posting_id, training_session_id, certification_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTestId() . ', ' .
 						$this->sqlGroupId() . ', ' .
 						$this->sqlDepartmentId() . ', ' .
 						$this->sqlOfficeId() . ', ' .
 						$this->sqlTeamId() . ', ' .
 						$this->sqlDesignationId() . ', ' .
 						$this->sqlPsWebsiteJobPostingId() . ', ' .
 						$this->sqlTrainingSessionId() . ', ' .
 						$this->sqlCertificationId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_id = ' . $this->sqlTestId() . ','; } elseif( true == array_key_exists( 'TestId', $this->getChangedColumns() ) ) { $strSql .= ' test_id = ' . $this->sqlTestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_id = ' . $this->sqlGroupId() . ','; } elseif( true == array_key_exists( 'GroupId', $this->getChangedColumns() ) ) { $strSql .= ' group_id = ' . $this->sqlGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_id = ' . $this->sqlOfficeId() . ','; } elseif( true == array_key_exists( 'OfficeId', $this->getChangedColumns() ) ) { $strSql .= ' office_id = ' . $this->sqlOfficeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; } elseif( true == array_key_exists( 'TeamId', $this->getChangedColumns() ) ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_website_job_posting_id = ' . $this->sqlPsWebsiteJobPostingId() . ','; } elseif( true == array_key_exists( 'PsWebsiteJobPostingId', $this->getChangedColumns() ) ) { $strSql .= ' ps_website_job_posting_id = ' . $this->sqlPsWebsiteJobPostingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_session_id = ' . $this->sqlTrainingSessionId() . ','; } elseif( true == array_key_exists( 'TrainingSessionId', $this->getChangedColumns() ) ) { $strSql .= ' training_session_id = ' . $this->sqlTrainingSessionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' certification_id = ' . $this->sqlCertificationId() . ','; } elseif( true == array_key_exists( 'CertificationId', $this->getChangedColumns() ) ) { $strSql .= ' certification_id = ' . $this->sqlCertificationId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'test_id' => $this->getTestId(),
			'group_id' => $this->getGroupId(),
			'department_id' => $this->getDepartmentId(),
			'office_id' => $this->getOfficeId(),
			'team_id' => $this->getTeamId(),
			'designation_id' => $this->getDesignationId(),
			'ps_website_job_posting_id' => $this->getPsWebsiteJobPostingId(),
			'training_session_id' => $this->getTrainingSessionId(),
			'certification_id' => $this->getCertificationId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>