<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeOptionAssociations
 * Do not add any new functions to this class.
 */

class CBaseEmployeeOptionAssociations extends CEosPluralBase {

	/**
	 * @return CEmployeeOptionAssociation[]
	 */
	public static function fetchEmployeeOptionAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeOptionAssociation', $objDatabase );
	}

	/**
	 * @return CEmployeeOptionAssociation
	 */
	public static function fetchEmployeeOptionAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeOptionAssociation', $objDatabase );
	}

	public static function fetchEmployeeOptionAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_option_associations', $objDatabase );
	}

	public static function fetchEmployeeOptionAssociationById( $intId, $objDatabase ) {
		return self::fetchEmployeeOptionAssociation( sprintf( 'SELECT * FROM employee_option_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeOptionAssociationsByViewerEmployeeId( $intViewerEmployeeId, $objDatabase ) {
		return self::fetchEmployeeOptionAssociations( sprintf( 'SELECT * FROM employee_option_associations WHERE viewer_employee_id = %d', ( int ) $intViewerEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeOptionAssociationsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeOptionAssociations( sprintf( 'SELECT * FROM employee_option_associations WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>