<?php

class CBasePsWebsiteJobPosting extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.ps_website_job_postings';

	protected $m_intId;
	protected $m_intDepartmentId;
	protected $m_intDesignationId;
	protected $m_intEmploymentApplicationId;
	protected $m_intPsJobTypeId;
	protected $m_intHiringManagerEmployeeId;
	protected $m_intEmployeeApplicationScoreTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strCountryCode;
	protected $m_intReferralBonusAmount;
	protected $m_strDeactivateOn;
	protected $m_intAllowOnlineApplication;
	protected $m_intAutoSendMrab;
	protected $m_boolIsAutoSendTechnicalTest;
	protected $m_boolIsAutoSendCodingTest;
	protected $m_intMrabCutoff;
	protected $m_intRawScoreCutoff;
	protected $m_intTechnicalCutoff;
	protected $m_intIsCampusDrive;
	protected $m_intIsPublished;
	protected $m_boolIsPublishInternal;
	protected $m_boolIsAllowStudent;
	protected $m_boolIsReferenceRequired;
	protected $m_boolIsFollowAllSteps;
	protected $m_strPublishedOn;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strAssessmentUrl;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intTechnologyId;
	protected $m_boolIsApproved;

	public function __construct() {
		parent::__construct();

		$this->m_strCountryCode = 'US';
		$this->m_intAllowOnlineApplication = '0';
		$this->m_intAutoSendMrab = '0';
		$this->m_boolIsAutoSendTechnicalTest = false;
		$this->m_boolIsAutoSendCodingTest = false;
		$this->m_intMrabCutoff = '0';
		$this->m_intRawScoreCutoff = '0';
		$this->m_intTechnicalCutoff = '0';
		$this->m_intIsCampusDrive = '0';
		$this->m_intIsPublished = '1';
		$this->m_boolIsPublishInternal = false;
		$this->m_boolIsAllowStudent = false;
		$this->m_boolIsReferenceRequired = false;
		$this->m_boolIsFollowAllSteps = false;
		$this->m_intOrderNum = '0';
		$this->m_boolIsApproved = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['employment_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmploymentApplicationId', trim( $arrValues['employment_application_id'] ) ); elseif( isset( $arrValues['employment_application_id'] ) ) $this->setEmploymentApplicationId( $arrValues['employment_application_id'] );
		if( isset( $arrValues['ps_job_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsJobTypeId', trim( $arrValues['ps_job_type_id'] ) ); elseif( isset( $arrValues['ps_job_type_id'] ) ) $this->setPsJobTypeId( $arrValues['ps_job_type_id'] );
		if( isset( $arrValues['hiring_manager_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intHiringManagerEmployeeId', trim( $arrValues['hiring_manager_employee_id'] ) ); elseif( isset( $arrValues['hiring_manager_employee_id'] ) ) $this->setHiringManagerEmployeeId( $arrValues['hiring_manager_employee_id'] );
		if( isset( $arrValues['employee_application_score_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationScoreTypeId', trim( $arrValues['employee_application_score_type_id'] ) ); elseif( isset( $arrValues['employee_application_score_type_id'] ) ) $this->setEmployeeApplicationScoreTypeId( $arrValues['employee_application_score_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( $arrValues['city'] ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( $arrValues['state_code'] ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( $arrValues['state_code'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['referral_bonus_amount'] ) && $boolDirectSet ) $this->set( 'm_intReferralBonusAmount', trim( $arrValues['referral_bonus_amount'] ) ); elseif( isset( $arrValues['referral_bonus_amount'] ) ) $this->setReferralBonusAmount( $arrValues['referral_bonus_amount'] );
		if( isset( $arrValues['deactivate_on'] ) && $boolDirectSet ) $this->set( 'm_strDeactivateOn', trim( $arrValues['deactivate_on'] ) ); elseif( isset( $arrValues['deactivate_on'] ) ) $this->setDeactivateOn( $arrValues['deactivate_on'] );
		if( isset( $arrValues['allow_online_application'] ) && $boolDirectSet ) $this->set( 'm_intAllowOnlineApplication', trim( $arrValues['allow_online_application'] ) ); elseif( isset( $arrValues['allow_online_application'] ) ) $this->setAllowOnlineApplication( $arrValues['allow_online_application'] );
		if( isset( $arrValues['auto_send_mrab'] ) && $boolDirectSet ) $this->set( 'm_intAutoSendMrab', trim( $arrValues['auto_send_mrab'] ) ); elseif( isset( $arrValues['auto_send_mrab'] ) ) $this->setAutoSendMrab( $arrValues['auto_send_mrab'] );
		if( isset( $arrValues['is_auto_send_technical_test'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoSendTechnicalTest', trim( stripcslashes( $arrValues['is_auto_send_technical_test'] ) ) ); elseif( isset( $arrValues['is_auto_send_technical_test'] ) ) $this->setIsAutoSendTechnicalTest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_send_technical_test'] ) : $arrValues['is_auto_send_technical_test'] );
		if( isset( $arrValues['is_auto_send_coding_test'] ) && $boolDirectSet ) $this->set( 'm_boolIsAutoSendCodingTest', trim( stripcslashes( $arrValues['is_auto_send_coding_test'] ) ) ); elseif( isset( $arrValues['is_auto_send_coding_test'] ) ) $this->setIsAutoSendCodingTest( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_auto_send_coding_test'] ) : $arrValues['is_auto_send_coding_test'] );
		if( isset( $arrValues['mrab_cutoff'] ) && $boolDirectSet ) $this->set( 'm_intMrabCutoff', trim( $arrValues['mrab_cutoff'] ) ); elseif( isset( $arrValues['mrab_cutoff'] ) ) $this->setMrabCutoff( $arrValues['mrab_cutoff'] );
		if( isset( $arrValues['raw_score_cutoff'] ) && $boolDirectSet ) $this->set( 'm_intRawScoreCutoff', trim( $arrValues['raw_score_cutoff'] ) ); elseif( isset( $arrValues['raw_score_cutoff'] ) ) $this->setRawScoreCutoff( $arrValues['raw_score_cutoff'] );
		if( isset( $arrValues['technical_cutoff'] ) && $boolDirectSet ) $this->set( 'm_intTechnicalCutoff', trim( $arrValues['technical_cutoff'] ) ); elseif( isset( $arrValues['technical_cutoff'] ) ) $this->setTechnicalCutoff( $arrValues['technical_cutoff'] );
		if( isset( $arrValues['is_campus_drive'] ) && $boolDirectSet ) $this->set( 'm_intIsCampusDrive', trim( $arrValues['is_campus_drive'] ) ); elseif( isset( $arrValues['is_campus_drive'] ) ) $this->setIsCampusDrive( $arrValues['is_campus_drive'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_publish_internal'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublishInternal', trim( stripcslashes( $arrValues['is_publish_internal'] ) ) ); elseif( isset( $arrValues['is_publish_internal'] ) ) $this->setIsPublishInternal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_publish_internal'] ) : $arrValues['is_publish_internal'] );
		if( isset( $arrValues['is_allow_student'] ) && $boolDirectSet ) $this->set( 'm_boolIsAllowStudent', trim( stripcslashes( $arrValues['is_allow_student'] ) ) ); elseif( isset( $arrValues['is_allow_student'] ) ) $this->setIsAllowStudent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_allow_student'] ) : $arrValues['is_allow_student'] );
		if( isset( $arrValues['is_reference_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsReferenceRequired', trim( stripcslashes( $arrValues['is_reference_required'] ) ) ); elseif( isset( $arrValues['is_reference_required'] ) ) $this->setIsReferenceRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_reference_required'] ) : $arrValues['is_reference_required'] );
		if( isset( $arrValues['is_follow_all_steps'] ) && $boolDirectSet ) $this->set( 'm_boolIsFollowAllSteps', trim( stripcslashes( $arrValues['is_follow_all_steps'] ) ) ); elseif( isset( $arrValues['is_follow_all_steps'] ) ) $this->setIsFollowAllSteps( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_follow_all_steps'] ) : $arrValues['is_follow_all_steps'] );
		if( isset( $arrValues['published_on'] ) && $boolDirectSet ) $this->set( 'm_strPublishedOn', trim( $arrValues['published_on'] ) ); elseif( isset( $arrValues['published_on'] ) ) $this->setPublishedOn( $arrValues['published_on'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['assessment_url'] ) && $boolDirectSet ) $this->set( 'm_strAssessmentUrl', trim( $arrValues['assessment_url'] ) ); elseif( isset( $arrValues['assessment_url'] ) ) $this->setAssessmentUrl( $arrValues['assessment_url'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['technology_id'] ) && $boolDirectSet ) $this->set( 'm_intTechnologyId', trim( $arrValues['technology_id'] ) ); elseif( isset( $arrValues['technology_id'] ) ) $this->setTechnologyId( $arrValues['technology_id'] );
		if( isset( $arrValues['is_approved'] ) && $boolDirectSet ) $this->set( 'm_boolIsApproved', trim( stripcslashes( $arrValues['is_approved'] ) ) ); elseif( isset( $arrValues['is_approved'] ) ) $this->setIsApproved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_approved'] ) : $arrValues['is_approved'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setEmploymentApplicationId( $intEmploymentApplicationId ) {
		$this->set( 'm_intEmploymentApplicationId', CStrings::strToIntDef( $intEmploymentApplicationId, NULL, false ) );
	}

	public function getEmploymentApplicationId() {
		return $this->m_intEmploymentApplicationId;
	}

	public function sqlEmploymentApplicationId() {
		return ( true == isset( $this->m_intEmploymentApplicationId ) ) ? ( string ) $this->m_intEmploymentApplicationId : 'NULL';
	}

	public function setPsJobTypeId( $intPsJobTypeId ) {
		$this->set( 'm_intPsJobTypeId', CStrings::strToIntDef( $intPsJobTypeId, NULL, false ) );
	}

	public function getPsJobTypeId() {
		return $this->m_intPsJobTypeId;
	}

	public function sqlPsJobTypeId() {
		return ( true == isset( $this->m_intPsJobTypeId ) ) ? ( string ) $this->m_intPsJobTypeId : 'NULL';
	}

	public function setHiringManagerEmployeeId( $intHiringManagerEmployeeId ) {
		$this->set( 'm_intHiringManagerEmployeeId', CStrings::strToIntDef( $intHiringManagerEmployeeId, NULL, false ) );
	}

	public function getHiringManagerEmployeeId() {
		return $this->m_intHiringManagerEmployeeId;
	}

	public function sqlHiringManagerEmployeeId() {
		return ( true == isset( $this->m_intHiringManagerEmployeeId ) ) ? ( string ) $this->m_intHiringManagerEmployeeId : 'NULL';
	}

	public function setEmployeeApplicationScoreTypeId( $intEmployeeApplicationScoreTypeId ) {
		$this->set( 'm_intEmployeeApplicationScoreTypeId', CStrings::strToIntDef( $intEmployeeApplicationScoreTypeId, NULL, false ) );
	}

	public function getEmployeeApplicationScoreTypeId() {
		return $this->m_intEmployeeApplicationScoreTypeId;
	}

	public function sqlEmployeeApplicationScoreTypeId() {
		return ( true == isset( $this->m_intEmployeeApplicationScoreTypeId ) ) ? ( string ) $this->m_intEmployeeApplicationScoreTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCity ) : '\'' . addslashes( $this->m_strCity ) . '\'' ) : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStateCode ) : '\'' . addslashes( $this->m_strStateCode ) . '\'' ) : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountryCode ) : '\'' . addslashes( $this->m_strCountryCode ) . '\'' ) : '\'US\'';
	}

	public function setReferralBonusAmount( $intReferralBonusAmount ) {
		$this->set( 'm_intReferralBonusAmount', CStrings::strToIntDef( $intReferralBonusAmount, NULL, false ) );
	}

	public function getReferralBonusAmount() {
		return $this->m_intReferralBonusAmount;
	}

	public function sqlReferralBonusAmount() {
		return ( true == isset( $this->m_intReferralBonusAmount ) ) ? ( string ) $this->m_intReferralBonusAmount : 'NULL';
	}

	public function setDeactivateOn( $strDeactivateOn ) {
		$this->set( 'm_strDeactivateOn', CStrings::strTrimDef( $strDeactivateOn, -1, NULL, true ) );
	}

	public function getDeactivateOn() {
		return $this->m_strDeactivateOn;
	}

	public function sqlDeactivateOn() {
		return ( true == isset( $this->m_strDeactivateOn ) ) ? '\'' . $this->m_strDeactivateOn . '\'' : 'NULL';
	}

	public function setAllowOnlineApplication( $intAllowOnlineApplication ) {
		$this->set( 'm_intAllowOnlineApplication', CStrings::strToIntDef( $intAllowOnlineApplication, NULL, false ) );
	}

	public function getAllowOnlineApplication() {
		return $this->m_intAllowOnlineApplication;
	}

	public function sqlAllowOnlineApplication() {
		return ( true == isset( $this->m_intAllowOnlineApplication ) ) ? ( string ) $this->m_intAllowOnlineApplication : '0';
	}

	public function setAutoSendMrab( $intAutoSendMrab ) {
		$this->set( 'm_intAutoSendMrab', CStrings::strToIntDef( $intAutoSendMrab, NULL, false ) );
	}

	public function getAutoSendMrab() {
		return $this->m_intAutoSendMrab;
	}

	public function sqlAutoSendMrab() {
		return ( true == isset( $this->m_intAutoSendMrab ) ) ? ( string ) $this->m_intAutoSendMrab : '0';
	}

	public function setIsAutoSendTechnicalTest( $boolIsAutoSendTechnicalTest ) {
		$this->set( 'm_boolIsAutoSendTechnicalTest', CStrings::strToBool( $boolIsAutoSendTechnicalTest ) );
	}

	public function getIsAutoSendTechnicalTest() {
		return $this->m_boolIsAutoSendTechnicalTest;
	}

	public function sqlIsAutoSendTechnicalTest() {
		return ( true == isset( $this->m_boolIsAutoSendTechnicalTest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoSendTechnicalTest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAutoSendCodingTest( $boolIsAutoSendCodingTest ) {
		$this->set( 'm_boolIsAutoSendCodingTest', CStrings::strToBool( $boolIsAutoSendCodingTest ) );
	}

	public function getIsAutoSendCodingTest() {
		return $this->m_boolIsAutoSendCodingTest;
	}

	public function sqlIsAutoSendCodingTest() {
		return ( true == isset( $this->m_boolIsAutoSendCodingTest ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAutoSendCodingTest ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMrabCutoff( $intMrabCutoff ) {
		$this->set( 'm_intMrabCutoff', CStrings::strToIntDef( $intMrabCutoff, NULL, false ) );
	}

	public function getMrabCutoff() {
		return $this->m_intMrabCutoff;
	}

	public function sqlMrabCutoff() {
		return ( true == isset( $this->m_intMrabCutoff ) ) ? ( string ) $this->m_intMrabCutoff : '0';
	}

	public function setRawScoreCutoff( $intRawScoreCutoff ) {
		$this->set( 'm_intRawScoreCutoff', CStrings::strToIntDef( $intRawScoreCutoff, NULL, false ) );
	}

	public function getRawScoreCutoff() {
		return $this->m_intRawScoreCutoff;
	}

	public function sqlRawScoreCutoff() {
		return ( true == isset( $this->m_intRawScoreCutoff ) ) ? ( string ) $this->m_intRawScoreCutoff : '0';
	}

	public function setTechnicalCutoff( $intTechnicalCutoff ) {
		$this->set( 'm_intTechnicalCutoff', CStrings::strToIntDef( $intTechnicalCutoff, NULL, false ) );
	}

	public function getTechnicalCutoff() {
		return $this->m_intTechnicalCutoff;
	}

	public function sqlTechnicalCutoff() {
		return ( true == isset( $this->m_intTechnicalCutoff ) ) ? ( string ) $this->m_intTechnicalCutoff : '0';
	}

	public function setIsCampusDrive( $intIsCampusDrive ) {
		$this->set( 'm_intIsCampusDrive', CStrings::strToIntDef( $intIsCampusDrive, NULL, false ) );
	}

	public function getIsCampusDrive() {
		return $this->m_intIsCampusDrive;
	}

	public function sqlIsCampusDrive() {
		return ( true == isset( $this->m_intIsCampusDrive ) ) ? ( string ) $this->m_intIsCampusDrive : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsPublishInternal( $boolIsPublishInternal ) {
		$this->set( 'm_boolIsPublishInternal', CStrings::strToBool( $boolIsPublishInternal ) );
	}

	public function getIsPublishInternal() {
		return $this->m_boolIsPublishInternal;
	}

	public function sqlIsPublishInternal() {
		return ( true == isset( $this->m_boolIsPublishInternal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublishInternal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsAllowStudent( $boolIsAllowStudent ) {
		$this->set( 'm_boolIsAllowStudent', CStrings::strToBool( $boolIsAllowStudent ) );
	}

	public function getIsAllowStudent() {
		return $this->m_boolIsAllowStudent;
	}

	public function sqlIsAllowStudent() {
		return ( true == isset( $this->m_boolIsAllowStudent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAllowStudent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsReferenceRequired( $boolIsReferenceRequired ) {
		$this->set( 'm_boolIsReferenceRequired', CStrings::strToBool( $boolIsReferenceRequired ) );
	}

	public function getIsReferenceRequired() {
		return $this->m_boolIsReferenceRequired;
	}

	public function sqlIsReferenceRequired() {
		return ( true == isset( $this->m_boolIsReferenceRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReferenceRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFollowAllSteps( $boolIsFollowAllSteps ) {
		$this->set( 'm_boolIsFollowAllSteps', CStrings::strToBool( $boolIsFollowAllSteps ) );
	}

	public function getIsFollowAllSteps() {
		return $this->m_boolIsFollowAllSteps;
	}

	public function sqlIsFollowAllSteps() {
		return ( true == isset( $this->m_boolIsFollowAllSteps ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFollowAllSteps ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPublishedOn( $strPublishedOn ) {
		$this->set( 'm_strPublishedOn', CStrings::strTrimDef( $strPublishedOn, -1, NULL, true ) );
	}

	public function getPublishedOn() {
		return $this->m_strPublishedOn;
	}

	public function sqlPublishedOn() {
		return ( true == isset( $this->m_strPublishedOn ) ) ? '\'' . $this->m_strPublishedOn . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAssessmentUrl( $strAssessmentUrl ) {
		$this->set( 'm_strAssessmentUrl', CStrings::strTrimDef( $strAssessmentUrl, 255, NULL, true ) );
	}

	public function getAssessmentUrl() {
		return $this->m_strAssessmentUrl;
	}

	public function sqlAssessmentUrl() {
		return ( true == isset( $this->m_strAssessmentUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAssessmentUrl ) : '\'' . addslashes( $this->m_strAssessmentUrl ) . '\'' ) : 'NULL';
	}

	public function setTechnologyId( $intTechnologyId ) {
		$this->set( 'm_intTechnologyId', CStrings::strToIntDef( $intTechnologyId, NULL, false ) );
	}

	public function getTechnologyId() {
		return $this->m_intTechnologyId;
	}

	public function sqlTechnologyId() {
		return ( true == isset( $this->m_intTechnologyId ) ) ? ( string ) $this->m_intTechnologyId : 'NULL';
	}

	public function setIsApproved( $boolIsApproved ) {
		$this->set( 'm_boolIsApproved', CStrings::strToBool( $boolIsApproved ) );
	}

	public function getIsApproved() {
		return $this->m_boolIsApproved;
	}

	public function sqlIsApproved() {
		return ( true == isset( $this->m_boolIsApproved ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsApproved ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, department_id, designation_id, employment_application_id, ps_job_type_id, hiring_manager_employee_id, employee_application_score_type_id, name, description, city, state_code, country_code, referral_bonus_amount, deactivate_on, allow_online_application, auto_send_mrab, is_auto_send_technical_test, is_auto_send_coding_test, mrab_cutoff, raw_score_cutoff, technical_cutoff, is_campus_drive, is_published, is_publish_internal, is_allow_student, is_reference_required, is_follow_all_steps, published_on, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, assessment_url, details, technology_id, is_approved )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDepartmentId() . ', ' .
						$this->sqlDesignationId() . ', ' .
						$this->sqlEmploymentApplicationId() . ', ' .
						$this->sqlPsJobTypeId() . ', ' .
						$this->sqlHiringManagerEmployeeId() . ', ' .
						$this->sqlEmployeeApplicationScoreTypeId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlReferralBonusAmount() . ', ' .
						$this->sqlDeactivateOn() . ', ' .
						$this->sqlAllowOnlineApplication() . ', ' .
						$this->sqlAutoSendMrab() . ', ' .
						$this->sqlIsAutoSendTechnicalTest() . ', ' .
						$this->sqlIsAutoSendCodingTest() . ', ' .
						$this->sqlMrabCutoff() . ', ' .
						$this->sqlRawScoreCutoff() . ', ' .
						$this->sqlTechnicalCutoff() . ', ' .
						$this->sqlIsCampusDrive() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsPublishInternal() . ', ' .
						$this->sqlIsAllowStudent() . ', ' .
						$this->sqlIsReferenceRequired() . ', ' .
						$this->sqlIsFollowAllSteps() . ', ' .
						$this->sqlPublishedOn() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAssessmentUrl() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlTechnologyId() . ', ' .
						$this->sqlIsApproved() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId(). ',' ; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId(). ',' ; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employment_application_id = ' . $this->sqlEmploymentApplicationId(). ',' ; } elseif( true == array_key_exists( 'EmploymentApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employment_application_id = ' . $this->sqlEmploymentApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_job_type_id = ' . $this->sqlPsJobTypeId(). ',' ; } elseif( true == array_key_exists( 'PsJobTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ps_job_type_id = ' . $this->sqlPsJobTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hiring_manager_employee_id = ' . $this->sqlHiringManagerEmployeeId(). ',' ; } elseif( true == array_key_exists( 'HiringManagerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' hiring_manager_employee_id = ' . $this->sqlHiringManagerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_score_type_id = ' . $this->sqlEmployeeApplicationScoreTypeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeApplicationScoreTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_score_type_id = ' . $this->sqlEmployeeApplicationScoreTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' referral_bonus_amount = ' . $this->sqlReferralBonusAmount(). ',' ; } elseif( true == array_key_exists( 'ReferralBonusAmount', $this->getChangedColumns() ) ) { $strSql .= ' referral_bonus_amount = ' . $this->sqlReferralBonusAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deactivate_on = ' . $this->sqlDeactivateOn(). ',' ; } elseif( true == array_key_exists( 'DeactivateOn', $this->getChangedColumns() ) ) { $strSql .= ' deactivate_on = ' . $this->sqlDeactivateOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_online_application = ' . $this->sqlAllowOnlineApplication(). ',' ; } elseif( true == array_key_exists( 'AllowOnlineApplication', $this->getChangedColumns() ) ) { $strSql .= ' allow_online_application = ' . $this->sqlAllowOnlineApplication() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_send_mrab = ' . $this->sqlAutoSendMrab(). ',' ; } elseif( true == array_key_exists( 'AutoSendMrab', $this->getChangedColumns() ) ) { $strSql .= ' auto_send_mrab = ' . $this->sqlAutoSendMrab() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_send_technical_test = ' . $this->sqlIsAutoSendTechnicalTest(). ',' ; } elseif( true == array_key_exists( 'IsAutoSendTechnicalTest', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_send_technical_test = ' . $this->sqlIsAutoSendTechnicalTest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_send_coding_test = ' . $this->sqlIsAutoSendCodingTest(). ',' ; } elseif( true == array_key_exists( 'IsAutoSendCodingTest', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_send_coding_test = ' . $this->sqlIsAutoSendCodingTest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mrab_cutoff = ' . $this->sqlMrabCutoff(). ',' ; } elseif( true == array_key_exists( 'MrabCutoff', $this->getChangedColumns() ) ) { $strSql .= ' mrab_cutoff = ' . $this->sqlMrabCutoff() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' raw_score_cutoff = ' . $this->sqlRawScoreCutoff(). ',' ; } elseif( true == array_key_exists( 'RawScoreCutoff', $this->getChangedColumns() ) ) { $strSql .= ' raw_score_cutoff = ' . $this->sqlRawScoreCutoff() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' technical_cutoff = ' . $this->sqlTechnicalCutoff(). ',' ; } elseif( true == array_key_exists( 'TechnicalCutoff', $this->getChangedColumns() ) ) { $strSql .= ' technical_cutoff = ' . $this->sqlTechnicalCutoff() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_campus_drive = ' . $this->sqlIsCampusDrive(). ',' ; } elseif( true == array_key_exists( 'IsCampusDrive', $this->getChangedColumns() ) ) { $strSql .= ' is_campus_drive = ' . $this->sqlIsCampusDrive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_publish_internal = ' . $this->sqlIsPublishInternal(). ',' ; } elseif( true == array_key_exists( 'IsPublishInternal', $this->getChangedColumns() ) ) { $strSql .= ' is_publish_internal = ' . $this->sqlIsPublishInternal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_allow_student = ' . $this->sqlIsAllowStudent(). ',' ; } elseif( true == array_key_exists( 'IsAllowStudent', $this->getChangedColumns() ) ) { $strSql .= ' is_allow_student = ' . $this->sqlIsAllowStudent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reference_required = ' . $this->sqlIsReferenceRequired(). ',' ; } elseif( true == array_key_exists( 'IsReferenceRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_reference_required = ' . $this->sqlIsReferenceRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_follow_all_steps = ' . $this->sqlIsFollowAllSteps(). ',' ; } elseif( true == array_key_exists( 'IsFollowAllSteps', $this->getChangedColumns() ) ) { $strSql .= ' is_follow_all_steps = ' . $this->sqlIsFollowAllSteps() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn(). ',' ; } elseif( true == array_key_exists( 'PublishedOn', $this->getChangedColumns() ) ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assessment_url = ' . $this->sqlAssessmentUrl(). ',' ; } elseif( true == array_key_exists( 'AssessmentUrl', $this->getChangedColumns() ) ) { $strSql .= ' assessment_url = ' . $this->sqlAssessmentUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' technology_id = ' . $this->sqlTechnologyId(). ',' ; } elseif( true == array_key_exists( 'TechnologyId', $this->getChangedColumns() ) ) { $strSql .= ' technology_id = ' . $this->sqlTechnologyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved(). ',' ; } elseif( true == array_key_exists( 'IsApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'department_id' => $this->getDepartmentId(),
			'designation_id' => $this->getDesignationId(),
			'employment_application_id' => $this->getEmploymentApplicationId(),
			'ps_job_type_id' => $this->getPsJobTypeId(),
			'hiring_manager_employee_id' => $this->getHiringManagerEmployeeId(),
			'employee_application_score_type_id' => $this->getEmployeeApplicationScoreTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'country_code' => $this->getCountryCode(),
			'referral_bonus_amount' => $this->getReferralBonusAmount(),
			'deactivate_on' => $this->getDeactivateOn(),
			'allow_online_application' => $this->getAllowOnlineApplication(),
			'auto_send_mrab' => $this->getAutoSendMrab(),
			'is_auto_send_technical_test' => $this->getIsAutoSendTechnicalTest(),
			'is_auto_send_coding_test' => $this->getIsAutoSendCodingTest(),
			'mrab_cutoff' => $this->getMrabCutoff(),
			'raw_score_cutoff' => $this->getRawScoreCutoff(),
			'technical_cutoff' => $this->getTechnicalCutoff(),
			'is_campus_drive' => $this->getIsCampusDrive(),
			'is_published' => $this->getIsPublished(),
			'is_publish_internal' => $this->getIsPublishInternal(),
			'is_allow_student' => $this->getIsAllowStudent(),
			'is_reference_required' => $this->getIsReferenceRequired(),
			'is_follow_all_steps' => $this->getIsFollowAllSteps(),
			'published_on' => $this->getPublishedOn(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'assessment_url' => $this->getAssessmentUrl(),
			'details' => $this->getDetails(),
			'technology_id' => $this->getTechnologyId(),
			'is_approved' => $this->getIsApproved()
		);
	}

}
?>