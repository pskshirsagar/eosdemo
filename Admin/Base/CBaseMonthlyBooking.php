<?php

class CBaseMonthlyBooking extends CEosSingularBase {

	const TABLE_NAME = 'public.monthly_bookings';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intQuotaId;
	protected $m_strMonth;
	protected $m_fltNewBookingValue;
	protected $m_fltAdjustedBookingValue;
	protected $m_fltKpiCommission;
	protected $m_fltTotalCommission;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltNewBookingValue = '0';
		$this->m_fltAdjustedBookingValue = '0';
		$this->m_fltKpiCommission = '0';
		$this->m_fltTotalCommission = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['quota_id'] ) && $boolDirectSet ) $this->set( 'm_intQuotaId', trim( $arrValues['quota_id'] ) ); elseif( isset( $arrValues['quota_id'] ) ) $this->setQuotaId( $arrValues['quota_id'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['new_booking_value'] ) && $boolDirectSet ) $this->set( 'm_fltNewBookingValue', trim( $arrValues['new_booking_value'] ) ); elseif( isset( $arrValues['new_booking_value'] ) ) $this->setNewBookingValue( $arrValues['new_booking_value'] );
		if( isset( $arrValues['adjusted_booking_value'] ) && $boolDirectSet ) $this->set( 'm_fltAdjustedBookingValue', trim( $arrValues['adjusted_booking_value'] ) ); elseif( isset( $arrValues['adjusted_booking_value'] ) ) $this->setAdjustedBookingValue( $arrValues['adjusted_booking_value'] );
		if( isset( $arrValues['kpi_commission'] ) && $boolDirectSet ) $this->set( 'm_fltKpiCommission', trim( $arrValues['kpi_commission'] ) ); elseif( isset( $arrValues['kpi_commission'] ) ) $this->setKpiCommission( $arrValues['kpi_commission'] );
		if( isset( $arrValues['total_commission'] ) && $boolDirectSet ) $this->set( 'm_fltTotalCommission', trim( $arrValues['total_commission'] ) ); elseif( isset( $arrValues['total_commission'] ) ) $this->setTotalCommission( $arrValues['total_commission'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setQuotaId( $intQuotaId ) {
		$this->set( 'm_intQuotaId', CStrings::strToIntDef( $intQuotaId, NULL, false ) );
	}

	public function getQuotaId() {
		return $this->m_intQuotaId;
	}

	public function sqlQuotaId() {
		return ( true == isset( $this->m_intQuotaId ) ) ? ( string ) $this->m_intQuotaId : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NOW()';
	}

	public function setNewBookingValue( $fltNewBookingValue ) {
		$this->set( 'm_fltNewBookingValue', CStrings::strToFloatDef( $fltNewBookingValue, NULL, false, 2 ) );
	}

	public function getNewBookingValue() {
		return $this->m_fltNewBookingValue;
	}

	public function sqlNewBookingValue() {
		return ( true == isset( $this->m_fltNewBookingValue ) ) ? ( string ) $this->m_fltNewBookingValue : '0';
	}

	public function setAdjustedBookingValue( $fltAdjustedBookingValue ) {
		$this->set( 'm_fltAdjustedBookingValue', CStrings::strToFloatDef( $fltAdjustedBookingValue, NULL, false, 2 ) );
	}

	public function getAdjustedBookingValue() {
		return $this->m_fltAdjustedBookingValue;
	}

	public function sqlAdjustedBookingValue() {
		return ( true == isset( $this->m_fltAdjustedBookingValue ) ) ? ( string ) $this->m_fltAdjustedBookingValue : '0';
	}

	public function setKpiCommission( $fltKpiCommission ) {
		$this->set( 'm_fltKpiCommission', CStrings::strToFloatDef( $fltKpiCommission, NULL, false, 2 ) );
	}

	public function getKpiCommission() {
		return $this->m_fltKpiCommission;
	}

	public function sqlKpiCommission() {
		return ( true == isset( $this->m_fltKpiCommission ) ) ? ( string ) $this->m_fltKpiCommission : '0';
	}

	public function setTotalCommission( $fltTotalCommission ) {
		$this->set( 'm_fltTotalCommission', CStrings::strToFloatDef( $fltTotalCommission, NULL, false, 2 ) );
	}

	public function getTotalCommission() {
		return $this->m_fltTotalCommission;
	}

	public function sqlTotalCommission() {
		return ( true == isset( $this->m_fltTotalCommission ) ) ? ( string ) $this->m_fltTotalCommission : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, quota_id, month, new_booking_value, adjusted_booking_value, kpi_commission, total_commission, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlQuotaId() . ', ' .
 						$this->sqlMonth() . ', ' .
 						$this->sqlNewBookingValue() . ', ' .
 						$this->sqlAdjustedBookingValue() . ', ' .
 						$this->sqlKpiCommission() . ', ' .
 						$this->sqlTotalCommission() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' quota_id = ' . $this->sqlQuotaId() . ','; } elseif( true == array_key_exists( 'QuotaId', $this->getChangedColumns() ) ) { $strSql .= ' quota_id = ' . $this->sqlQuotaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_booking_value = ' . $this->sqlNewBookingValue() . ','; } elseif( true == array_key_exists( 'NewBookingValue', $this->getChangedColumns() ) ) { $strSql .= ' new_booking_value = ' . $this->sqlNewBookingValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjusted_booking_value = ' . $this->sqlAdjustedBookingValue() . ','; } elseif( true == array_key_exists( 'AdjustedBookingValue', $this->getChangedColumns() ) ) { $strSql .= ' adjusted_booking_value = ' . $this->sqlAdjustedBookingValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' kpi_commission = ' . $this->sqlKpiCommission() . ','; } elseif( true == array_key_exists( 'KpiCommission', $this->getChangedColumns() ) ) { $strSql .= ' kpi_commission = ' . $this->sqlKpiCommission() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_commission = ' . $this->sqlTotalCommission() . ','; } elseif( true == array_key_exists( 'TotalCommission', $this->getChangedColumns() ) ) { $strSql .= ' total_commission = ' . $this->sqlTotalCommission() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'quota_id' => $this->getQuotaId(),
			'month' => $this->getMonth(),
			'new_booking_value' => $this->getNewBookingValue(),
			'adjusted_booking_value' => $this->getAdjustedBookingValue(),
			'kpi_commission' => $this->getKpiCommission(),
			'total_commission' => $this->getTotalCommission(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>