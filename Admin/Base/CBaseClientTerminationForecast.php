<?php

class CBaseClientTerminationForecast extends CEosSingularBase {

	const TABLE_NAME = 'public.client_termination_forecasts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTerminationReasonId;
	protected $m_intCompetitorId;
	protected $m_intTerminatedPropertyCountPerMonth;
	protected $m_strTerminationNote;
	protected $m_strTerminationStartDate;
	protected $m_strTerminationEndDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['termination_reason_id'] ) && $boolDirectSet ) $this->set( 'm_intTerminationReasonId', trim( $arrValues['termination_reason_id'] ) ); elseif( isset( $arrValues['termination_reason_id'] ) ) $this->setTerminationReasonId( $arrValues['termination_reason_id'] );
		if( isset( $arrValues['competitor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorId', trim( $arrValues['competitor_id'] ) ); elseif( isset( $arrValues['competitor_id'] ) ) $this->setCompetitorId( $arrValues['competitor_id'] );
		if( isset( $arrValues['terminated_property_count_per_month'] ) && $boolDirectSet ) $this->set( 'm_intTerminatedPropertyCountPerMonth', trim( $arrValues['terminated_property_count_per_month'] ) ); elseif( isset( $arrValues['terminated_property_count_per_month'] ) ) $this->setTerminatedPropertyCountPerMonth( $arrValues['terminated_property_count_per_month'] );
		if( isset( $arrValues['termination_note'] ) && $boolDirectSet ) $this->set( 'm_strTerminationNote', trim( stripcslashes( $arrValues['termination_note'] ) ) ); elseif( isset( $arrValues['termination_note'] ) ) $this->setTerminationNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['termination_note'] ) : $arrValues['termination_note'] );
		if( isset( $arrValues['termination_start_date'] ) && $boolDirectSet ) $this->set( 'm_strTerminationStartDate', trim( $arrValues['termination_start_date'] ) ); elseif( isset( $arrValues['termination_start_date'] ) ) $this->setTerminationStartDate( $arrValues['termination_start_date'] );
		if( isset( $arrValues['termination_end_date'] ) && $boolDirectSet ) $this->set( 'm_strTerminationEndDate', trim( $arrValues['termination_end_date'] ) ); elseif( isset( $arrValues['termination_end_date'] ) ) $this->setTerminationEndDate( $arrValues['termination_end_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setTerminationReasonId( $intTerminationReasonId ) {
		$this->set( 'm_intTerminationReasonId', CStrings::strToIntDef( $intTerminationReasonId, NULL, false ) );
	}

	public function getTerminationReasonId() {
		return $this->m_intTerminationReasonId;
	}

	public function sqlTerminationReasonId() {
		return ( true == isset( $this->m_intTerminationReasonId ) ) ? ( string ) $this->m_intTerminationReasonId : 'NULL';
	}

	public function setCompetitorId( $intCompetitorId ) {
		$this->set( 'm_intCompetitorId', CStrings::strToIntDef( $intCompetitorId, NULL, false ) );
	}

	public function getCompetitorId() {
		return $this->m_intCompetitorId;
	}

	public function sqlCompetitorId() {
		return ( true == isset( $this->m_intCompetitorId ) ) ? ( string ) $this->m_intCompetitorId : 'NULL';
	}

	public function setTerminatedPropertyCountPerMonth( $intTerminatedPropertyCountPerMonth ) {
		$this->set( 'm_intTerminatedPropertyCountPerMonth', CStrings::strToIntDef( $intTerminatedPropertyCountPerMonth, NULL, false ) );
	}

	public function getTerminatedPropertyCountPerMonth() {
		return $this->m_intTerminatedPropertyCountPerMonth;
	}

	public function sqlTerminatedPropertyCountPerMonth() {
		return ( true == isset( $this->m_intTerminatedPropertyCountPerMonth ) ) ? ( string ) $this->m_intTerminatedPropertyCountPerMonth : 'NULL';
	}

	public function setTerminationNote( $strTerminationNote ) {
		$this->set( 'm_strTerminationNote', CStrings::strTrimDef( $strTerminationNote, -1, NULL, true ) );
	}

	public function getTerminationNote() {
		return $this->m_strTerminationNote;
	}

	public function sqlTerminationNote() {
		return ( true == isset( $this->m_strTerminationNote ) ) ? '\'' . addslashes( $this->m_strTerminationNote ) . '\'' : 'NULL';
	}

	public function setTerminationStartDate( $strTerminationStartDate ) {
		$this->set( 'm_strTerminationStartDate', CStrings::strTrimDef( $strTerminationStartDate, -1, NULL, true ) );
	}

	public function getTerminationStartDate() {
		return $this->m_strTerminationStartDate;
	}

	public function sqlTerminationStartDate() {
		return ( true == isset( $this->m_strTerminationStartDate ) ) ? '\'' . $this->m_strTerminationStartDate . '\'' : 'NULL';
	}

	public function setTerminationEndDate( $strTerminationEndDate ) {
		$this->set( 'm_strTerminationEndDate', CStrings::strTrimDef( $strTerminationEndDate, -1, NULL, true ) );
	}

	public function getTerminationEndDate() {
		return $this->m_strTerminationEndDate;
	}

	public function sqlTerminationEndDate() {
		return ( true == isset( $this->m_strTerminationEndDate ) ) ? '\'' . $this->m_strTerminationEndDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, termination_reason_id, competitor_id, terminated_property_count_per_month, termination_note, termination_start_date, termination_end_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlTerminationReasonId() . ', ' .
 						$this->sqlCompetitorId() . ', ' .
 						$this->sqlTerminatedPropertyCountPerMonth() . ', ' .
 						$this->sqlTerminationNote() . ', ' .
 						$this->sqlTerminationStartDate() . ', ' .
 						$this->sqlTerminationEndDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_reason_id = ' . $this->sqlTerminationReasonId() . ','; } elseif( true == array_key_exists( 'TerminationReasonId', $this->getChangedColumns() ) ) { $strSql .= ' termination_reason_id = ' . $this->sqlTerminationReasonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId() . ','; } elseif( true == array_key_exists( 'CompetitorId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' terminated_property_count_per_month = ' . $this->sqlTerminatedPropertyCountPerMonth() . ','; } elseif( true == array_key_exists( 'TerminatedPropertyCountPerMonth', $this->getChangedColumns() ) ) { $strSql .= ' terminated_property_count_per_month = ' . $this->sqlTerminatedPropertyCountPerMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_note = ' . $this->sqlTerminationNote() . ','; } elseif( true == array_key_exists( 'TerminationNote', $this->getChangedColumns() ) ) { $strSql .= ' termination_note = ' . $this->sqlTerminationNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_start_date = ' . $this->sqlTerminationStartDate() . ','; } elseif( true == array_key_exists( 'TerminationStartDate', $this->getChangedColumns() ) ) { $strSql .= ' termination_start_date = ' . $this->sqlTerminationStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_end_date = ' . $this->sqlTerminationEndDate() . ','; } elseif( true == array_key_exists( 'TerminationEndDate', $this->getChangedColumns() ) ) { $strSql .= ' termination_end_date = ' . $this->sqlTerminationEndDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'termination_reason_id' => $this->getTerminationReasonId(),
			'competitor_id' => $this->getCompetitorId(),
			'terminated_property_count_per_month' => $this->getTerminatedPropertyCountPerMonth(),
			'termination_note' => $this->getTerminationNote(),
			'termination_start_date' => $this->getTerminationStartDate(),
			'termination_end_date' => $this->getTerminationEndDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>