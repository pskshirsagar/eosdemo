<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyReferences
 * Do not add any new functions to this class.
 */

class CBaseSurveyReferences extends CEosPluralBase {

	/**
	 * @return CSurveyReference[]
	 */
	public static function fetchSurveyReferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyReference', $objDatabase );
	}

	/**
	 * @return CSurveyReference
	 */
	public static function fetchSurveyReference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyReference', $objDatabase );
	}

	public static function fetchSurveyReferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_references', $objDatabase );
	}

	public static function fetchSurveyReferenceById( $intId, $objDatabase ) {
		return self::fetchSurveyReference( sprintf( 'SELECT * FROM survey_references WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveyReferencesBySurveyId( $intSurveyId, $objDatabase ) {
		return self::fetchSurveyReferences( sprintf( 'SELECT * FROM survey_references WHERE survey_id = %d', ( int ) $intSurveyId ), $objDatabase );
	}

	public static function fetchSurveyReferencesBySurveyReferenceTypeId( $intSurveyReferenceTypeId, $objDatabase ) {
		return self::fetchSurveyReferences( sprintf( 'SELECT * FROM survey_references WHERE survey_reference_type_id = %d', ( int ) $intSurveyReferenceTypeId ), $objDatabase );
	}

	public static function fetchSurveyReferencesBySurveyQuestionAnswerId( $intSurveyQuestionAnswerId, $objDatabase ) {
		return self::fetchSurveyReferences( sprintf( 'SELECT * FROM survey_references WHERE survey_question_answer_id = %d', ( int ) $intSurveyQuestionAnswerId ), $objDatabase );
	}

}
?>