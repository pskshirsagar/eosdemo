<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationInterviews
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationInterviews extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationInterview[]
	 */
	public static function fetchEmployeeApplicationInterviews( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeApplicationInterview::class, $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationInterview
	 */
	public static function fetchEmployeeApplicationInterview( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeApplicationInterview::class, $objDatabase );
	}

	public static function fetchEmployeeApplicationInterviewCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_interviews', $objDatabase );
	}

	public static function fetchEmployeeApplicationInterviewById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationInterview( sprintf( 'SELECT * FROM employee_application_interviews WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationInterviewsByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchEmployeeApplicationInterviews( sprintf( 'SELECT * FROM employee_application_interviews WHERE employee_application_id = %d', $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationInterviewsByInterviewTypeId( $intInterviewTypeId, $objDatabase ) {
		return self::fetchEmployeeApplicationInterviews( sprintf( 'SELECT * FROM employee_application_interviews WHERE interview_type_id = %d', $intInterviewTypeId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationInterviewsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeApplicationInterviews( sprintf( 'SELECT * FROM employee_application_interviews WHERE employee_id = %d', $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationInterviewsByPsJobPostingStepId( $intPsJobPostingStepId, $objDatabase ) {
		return self::fetchEmployeeApplicationInterviews( sprintf( 'SELECT * FROM employee_application_interviews WHERE ps_job_posting_step_id = %d', $intPsJobPostingStepId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationInterviewsByGoogleCalendarEventId( $strGoogleCalendarEventId, $objDatabase ) {
		return self::fetchEmployeeApplicationInterviews( sprintf( 'SELECT * FROM employee_application_interviews WHERE google_calendar_event_id = \'%s\'', $strGoogleCalendarEventId ), $objDatabase );
	}

}
?>