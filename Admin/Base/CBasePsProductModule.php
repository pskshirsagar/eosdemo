<?php

class CBasePsProductModule extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_product_modules';

	protected $m_intId;
	protected $m_intPsProductId;
	protected $m_strApplicationLayerName;
	protected $m_intParentModuleId;
	protected $m_intDevUserId;
	protected $m_intQaUserId;
	protected $m_intPsProductOptionId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strFilePath;
	protected $m_intIsCritical;
	protected $m_intIsSeleniumScriptCreated;
	protected $m_intIsPublic;
	protected $m_intIsPublished;
	protected $m_intIsDeprecated;
	protected $m_strDeprecatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsCritical = '0';
		$this->m_intIsSeleniumScriptCreated = '0';
		$this->m_intIsPublic = '0';
		$this->m_intIsPublished = '0';
		$this->m_intIsDeprecated = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['application_layer_name'] ) && $boolDirectSet ) $this->set( 'm_strApplicationLayerName', trim( stripcslashes( $arrValues['application_layer_name'] ) ) ); elseif( isset( $arrValues['application_layer_name'] ) ) $this->setApplicationLayerName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['application_layer_name'] ) : $arrValues['application_layer_name'] );
		if( isset( $arrValues['parent_module_id'] ) && $boolDirectSet ) $this->set( 'm_intParentModuleId', trim( $arrValues['parent_module_id'] ) ); elseif( isset( $arrValues['parent_module_id'] ) ) $this->setParentModuleId( $arrValues['parent_module_id'] );
		if( isset( $arrValues['dev_user_id'] ) && $boolDirectSet ) $this->set( 'm_intDevUserId', trim( $arrValues['dev_user_id'] ) ); elseif( isset( $arrValues['dev_user_id'] ) ) $this->setDevUserId( $arrValues['dev_user_id'] );
		if( isset( $arrValues['qa_user_id'] ) && $boolDirectSet ) $this->set( 'm_intQaUserId', trim( $arrValues['qa_user_id'] ) ); elseif( isset( $arrValues['qa_user_id'] ) ) $this->setQaUserId( $arrValues['qa_user_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['is_critical'] ) && $boolDirectSet ) $this->set( 'm_intIsCritical', trim( $arrValues['is_critical'] ) ); elseif( isset( $arrValues['is_critical'] ) ) $this->setIsCritical( $arrValues['is_critical'] );
		if( isset( $arrValues['is_selenium_script_created'] ) && $boolDirectSet ) $this->set( 'm_intIsSeleniumScriptCreated', trim( $arrValues['is_selenium_script_created'] ) ); elseif( isset( $arrValues['is_selenium_script_created'] ) ) $this->setIsSeleniumScriptCreated( $arrValues['is_selenium_script_created'] );
		if( isset( $arrValues['is_public'] ) && $boolDirectSet ) $this->set( 'm_intIsPublic', trim( $arrValues['is_public'] ) ); elseif( isset( $arrValues['is_public'] ) ) $this->setIsPublic( $arrValues['is_public'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_deprecated'] ) && $boolDirectSet ) $this->set( 'm_intIsDeprecated', trim( $arrValues['is_deprecated'] ) ); elseif( isset( $arrValues['is_deprecated'] ) ) $this->setIsDeprecated( $arrValues['is_deprecated'] );
		if( isset( $arrValues['deprecated_on'] ) && $boolDirectSet ) $this->set( 'm_strDeprecatedOn', trim( $arrValues['deprecated_on'] ) ); elseif( isset( $arrValues['deprecated_on'] ) ) $this->setDeprecatedOn( $arrValues['deprecated_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setApplicationLayerName( $strApplicationLayerName ) {
		$this->set( 'm_strApplicationLayerName', CStrings::strTrimDef( $strApplicationLayerName, 50, NULL, true ) );
	}

	public function getApplicationLayerName() {
		return $this->m_strApplicationLayerName;
	}

	public function sqlApplicationLayerName() {
		return ( true == isset( $this->m_strApplicationLayerName ) ) ? '\'' . addslashes( $this->m_strApplicationLayerName ) . '\'' : 'NULL';
	}

	public function setParentModuleId( $intParentModuleId ) {
		$this->set( 'm_intParentModuleId', CStrings::strToIntDef( $intParentModuleId, NULL, false ) );
	}

	public function getParentModuleId() {
		return $this->m_intParentModuleId;
	}

	public function sqlParentModuleId() {
		return ( true == isset( $this->m_intParentModuleId ) ) ? ( string ) $this->m_intParentModuleId : 'NULL';
	}

	public function setDevUserId( $intDevUserId ) {
		$this->set( 'm_intDevUserId', CStrings::strToIntDef( $intDevUserId, NULL, false ) );
	}

	public function getDevUserId() {
		return $this->m_intDevUserId;
	}

	public function sqlDevUserId() {
		return ( true == isset( $this->m_intDevUserId ) ) ? ( string ) $this->m_intDevUserId : 'NULL';
	}

	public function setQaUserId( $intQaUserId ) {
		$this->set( 'm_intQaUserId', CStrings::strToIntDef( $intQaUserId, NULL, false ) );
	}

	public function getQaUserId() {
		return $this->m_intQaUserId;
	}

	public function sqlQaUserId() {
		return ( true == isset( $this->m_intQaUserId ) ) ? ( string ) $this->m_intQaUserId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 128, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setIsCritical( $intIsCritical ) {
		$this->set( 'm_intIsCritical', CStrings::strToIntDef( $intIsCritical, NULL, false ) );
	}

	public function getIsCritical() {
		return $this->m_intIsCritical;
	}

	public function sqlIsCritical() {
		return ( true == isset( $this->m_intIsCritical ) ) ? ( string ) $this->m_intIsCritical : '0';
	}

	public function setIsSeleniumScriptCreated( $intIsSeleniumScriptCreated ) {
		$this->set( 'm_intIsSeleniumScriptCreated', CStrings::strToIntDef( $intIsSeleniumScriptCreated, NULL, false ) );
	}

	public function getIsSeleniumScriptCreated() {
		return $this->m_intIsSeleniumScriptCreated;
	}

	public function sqlIsSeleniumScriptCreated() {
		return ( true == isset( $this->m_intIsSeleniumScriptCreated ) ) ? ( string ) $this->m_intIsSeleniumScriptCreated : '0';
	}

	public function setIsPublic( $intIsPublic ) {
		$this->set( 'm_intIsPublic', CStrings::strToIntDef( $intIsPublic, NULL, false ) );
	}

	public function getIsPublic() {
		return $this->m_intIsPublic;
	}

	public function sqlIsPublic() {
		return ( true == isset( $this->m_intIsPublic ) ) ? ( string ) $this->m_intIsPublic : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setIsDeprecated( $intIsDeprecated ) {
		$this->set( 'm_intIsDeprecated', CStrings::strToIntDef( $intIsDeprecated, NULL, false ) );
	}

	public function getIsDeprecated() {
		return $this->m_intIsDeprecated;
	}

	public function sqlIsDeprecated() {
		return ( true == isset( $this->m_intIsDeprecated ) ) ? ( string ) $this->m_intIsDeprecated : '0';
	}

	public function setDeprecatedOn( $strDeprecatedOn ) {
		$this->set( 'm_strDeprecatedOn', CStrings::strTrimDef( $strDeprecatedOn, -1, NULL, true ) );
	}

	public function getDeprecatedOn() {
		return $this->m_strDeprecatedOn;
	}

	public function sqlDeprecatedOn() {
		return ( true == isset( $this->m_strDeprecatedOn ) ) ? '\'' . $this->m_strDeprecatedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_product_id, application_layer_name, parent_module_id, dev_user_id, qa_user_id, ps_product_option_id, name, description, file_path, is_critical, is_selenium_script_created, is_public, is_published, is_deprecated, deprecated_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlApplicationLayerName() . ', ' .
 						$this->sqlParentModuleId() . ', ' .
 						$this->sqlDevUserId() . ', ' .
 						$this->sqlQaUserId() . ', ' .
 						$this->sqlPsProductOptionId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlIsCritical() . ', ' .
 						$this->sqlIsSeleniumScriptCreated() . ', ' .
 						$this->sqlIsPublic() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsDeprecated() . ', ' .
 						$this->sqlDeprecatedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' application_layer_name = ' . $this->sqlApplicationLayerName() . ','; } elseif( true == array_key_exists( 'ApplicationLayerName', $this->getChangedColumns() ) ) { $strSql .= ' application_layer_name = ' . $this->sqlApplicationLayerName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_module_id = ' . $this->sqlParentModuleId() . ','; } elseif( true == array_key_exists( 'ParentModuleId', $this->getChangedColumns() ) ) { $strSql .= ' parent_module_id = ' . $this->sqlParentModuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dev_user_id = ' . $this->sqlDevUserId() . ','; } elseif( true == array_key_exists( 'DevUserId', $this->getChangedColumns() ) ) { $strSql .= ' dev_user_id = ' . $this->sqlDevUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_user_id = ' . $this->sqlQaUserId() . ','; } elseif( true == array_key_exists( 'QaUserId', $this->getChangedColumns() ) ) { $strSql .= ' qa_user_id = ' . $this->sqlQaUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; } elseif( true == array_key_exists( 'PsProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_critical = ' . $this->sqlIsCritical() . ','; } elseif( true == array_key_exists( 'IsCritical', $this->getChangedColumns() ) ) { $strSql .= ' is_critical = ' . $this->sqlIsCritical() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_selenium_script_created = ' . $this->sqlIsSeleniumScriptCreated() . ','; } elseif( true == array_key_exists( 'IsSeleniumScriptCreated', $this->getChangedColumns() ) ) { $strSql .= ' is_selenium_script_created = ' . $this->sqlIsSeleniumScriptCreated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_public = ' . $this->sqlIsPublic() . ','; } elseif( true == array_key_exists( 'IsPublic', $this->getChangedColumns() ) ) { $strSql .= ' is_public = ' . $this->sqlIsPublic() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deprecated = ' . $this->sqlIsDeprecated() . ','; } elseif( true == array_key_exists( 'IsDeprecated', $this->getChangedColumns() ) ) { $strSql .= ' is_deprecated = ' . $this->sqlIsDeprecated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deprecated_on = ' . $this->sqlDeprecatedOn() . ','; } elseif( true == array_key_exists( 'DeprecatedOn', $this->getChangedColumns() ) ) { $strSql .= ' deprecated_on = ' . $this->sqlDeprecatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_product_id' => $this->getPsProductId(),
			'application_layer_name' => $this->getApplicationLayerName(),
			'parent_module_id' => $this->getParentModuleId(),
			'dev_user_id' => $this->getDevUserId(),
			'qa_user_id' => $this->getQaUserId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'file_path' => $this->getFilePath(),
			'is_critical' => $this->getIsCritical(),
			'is_selenium_script_created' => $this->getIsSeleniumScriptCreated(),
			'is_public' => $this->getIsPublic(),
			'is_published' => $this->getIsPublished(),
			'is_deprecated' => $this->getIsDeprecated(),
			'deprecated_on' => $this->getDeprecatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>