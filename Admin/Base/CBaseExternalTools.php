<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CExternalTools
 * Do not add any new functions to this class.
 */

class CBaseExternalTools extends CEosPluralBase {

	/**
	 * @return CExternalTool[]
	 */
	public static function fetchExternalTools( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CExternalTool::class, $objDatabase );
	}

	/**
	 * @return CExternalTool
	 */
	public static function fetchExternalTool( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CExternalTool::class, $objDatabase );
	}

	public static function fetchExternalToolCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'external_tools', $objDatabase );
	}

	public static function fetchExternalToolById( $intId, $objDatabase ) {
		return self::fetchExternalTool( sprintf( 'SELECT * FROM external_tools WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>