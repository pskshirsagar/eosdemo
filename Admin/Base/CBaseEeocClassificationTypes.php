<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEeocClassificationTypes
 * Do not add any new functions to this class.
 */

class CBaseEeocClassificationTypes extends CEosPluralBase {

	/**
	 * @return CEeocClassificationType[]
	 */
	public static function fetchEeocClassificationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEeocClassificationType', $objDatabase );
	}

	/**
	 * @return CEeocClassificationType
	 */
	public static function fetchEeocClassificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEeocClassificationType', $objDatabase );
	}

	public static function fetchEeocClassificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'eeoc_classification_types', $objDatabase );
	}

	public static function fetchEeocClassificationTypeById( $intId, $objDatabase ) {
		return self::fetchEeocClassificationType( sprintf( 'SELECT * FROM eeoc_classification_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>