<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestGroups
 * Do not add any new functions to this class.
 */

class CBaseTestGroups extends CEosPluralBase {

	/**
	 * @return CTestGroup[]
	 */
	public static function fetchTestGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestGroup', $objDatabase );
	}

	/**
	 * @return CTestGroup
	 */
	public static function fetchTestGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestGroup', $objDatabase );
	}

	public static function fetchTestGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_groups', $objDatabase );
	}

	public static function fetchTestGroupById( $intId, $objDatabase ) {
		return self::fetchTestGroup( sprintf( 'SELECT * FROM test_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestGroupsByTestId( $intTestId, $objDatabase ) {
		return self::fetchTestGroups( sprintf( 'SELECT * FROM test_groups WHERE test_id = %d', ( int ) $intTestId ), $objDatabase );
	}

	public static function fetchTestGroupsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchTestGroups( sprintf( 'SELECT * FROM test_groups WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

	public static function fetchTestGroupsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchTestGroups( sprintf( 'SELECT * FROM test_groups WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchTestGroupsByOfficeId( $intOfficeId, $objDatabase ) {
		return self::fetchTestGroups( sprintf( 'SELECT * FROM test_groups WHERE office_id = %d', ( int ) $intOfficeId ), $objDatabase );
	}

	public static function fetchTestGroupsByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchTestGroups( sprintf( 'SELECT * FROM test_groups WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

	public static function fetchTestGroupsByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchTestGroups( sprintf( 'SELECT * FROM test_groups WHERE designation_id = %d', ( int ) $intDesignationId ), $objDatabase );
	}

	public static function fetchTestGroupsByPsWebsiteJobPostingId( $intPsWebsiteJobPostingId, $objDatabase ) {
		return self::fetchTestGroups( sprintf( 'SELECT * FROM test_groups WHERE ps_website_job_posting_id = %d', ( int ) $intPsWebsiteJobPostingId ), $objDatabase );
	}

	public static function fetchTestGroupsByTrainingSessionId( $intTrainingSessionId, $objDatabase ) {
		return self::fetchTestGroups( sprintf( 'SELECT * FROM test_groups WHERE training_session_id = %d', ( int ) $intTrainingSessionId ), $objDatabase );
	}

	public static function fetchTestGroupsByCertificationId( $intCertificationId, $objDatabase ) {
		return self::fetchTestGroups( sprintf( 'SELECT * FROM test_groups WHERE certification_id = %d', ( int ) $intCertificationId ), $objDatabase );
	}

}
?>