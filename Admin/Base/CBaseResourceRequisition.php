<?php

class CBaseResourceRequisition extends CEosSingularBase {

	const TABLE_NAME = 'public.resource_requisitions';

	protected $m_intId;
	protected $m_intOriginatingEmployeeId;
	protected $m_intDesignationId;
	protected $m_intJobPostingId;
	protected $m_intProductId;
	protected $m_intProductOptionId;
	protected $m_intNumberOfResources;
	protected $m_intRequirementTypeId;
	protected $m_strRequirementDueDate;
	protected $m_intManagerEmployeeId;
	protected $m_intPurchaseRequestId;
	protected $m_intRecruiterEmployeeId;
	protected $m_strReplacementEmployeeIds;
	protected $m_intApprovedBy;
	protected $m_intRequiredExperience;
	protected $m_intRequiredQualification;
	protected $m_intDeletionRequestedBy;
	protected $m_intDeletionConfirmedBy;
	protected $m_intIsPublished;
	protected $m_strJobDescription;
	protected $m_strAdditionalInfo;
	protected $m_strRequiredCertification;
	protected $m_strRemarks;
	protected $m_strRecruiterAssignedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intNumberOfResources = '0';
		$this->m_intManagerEmployeeId = '0';
		$this->m_intRequiredExperience = '0';
		$this->m_intIsPublished = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['originating_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginatingEmployeeId', trim( $arrValues['originating_employee_id'] ) ); elseif( isset( $arrValues['originating_employee_id'] ) ) $this->setOriginatingEmployeeId( $arrValues['originating_employee_id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['job_posting_id'] ) && $boolDirectSet ) $this->set( 'm_intJobPostingId', trim( $arrValues['job_posting_id'] ) ); elseif( isset( $arrValues['job_posting_id'] ) ) $this->setJobPostingId( $arrValues['job_posting_id'] );
		if( isset( $arrValues['product_id'] ) && $boolDirectSet ) $this->set( 'm_intProductId', trim( $arrValues['product_id'] ) ); elseif( isset( $arrValues['product_id'] ) ) $this->setProductId( $arrValues['product_id'] );
		if( isset( $arrValues['product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intProductOptionId', trim( $arrValues['product_option_id'] ) ); elseif( isset( $arrValues['product_option_id'] ) ) $this->setProductOptionId( $arrValues['product_option_id'] );
		if( isset( $arrValues['number_of_resources'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfResources', trim( $arrValues['number_of_resources'] ) ); elseif( isset( $arrValues['number_of_resources'] ) ) $this->setNumberOfResources( $arrValues['number_of_resources'] );
		if( isset( $arrValues['requirement_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRequirementTypeId', trim( $arrValues['requirement_type_id'] ) ); elseif( isset( $arrValues['requirement_type_id'] ) ) $this->setRequirementTypeId( $arrValues['requirement_type_id'] );
		if( isset( $arrValues['requirement_due_date'] ) && $boolDirectSet ) $this->set( 'm_strRequirementDueDate', trim( $arrValues['requirement_due_date'] ) ); elseif( isset( $arrValues['requirement_due_date'] ) ) $this->setRequirementDueDate( $arrValues['requirement_due_date'] );
		if( isset( $arrValues['manager_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intManagerEmployeeId', trim( $arrValues['manager_employee_id'] ) ); elseif( isset( $arrValues['manager_employee_id'] ) ) $this->setManagerEmployeeId( $arrValues['manager_employee_id'] );
		if( isset( $arrValues['purchase_request_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchaseRequestId', trim( $arrValues['purchase_request_id'] ) ); elseif( isset( $arrValues['purchase_request_id'] ) ) $this->setPurchaseRequestId( $arrValues['purchase_request_id'] );
		if( isset( $arrValues['recruiter_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intRecruiterEmployeeId', trim( $arrValues['recruiter_employee_id'] ) ); elseif( isset( $arrValues['recruiter_employee_id'] ) ) $this->setRecruiterEmployeeId( $arrValues['recruiter_employee_id'] );
		if( isset( $arrValues['replacement_employee_ids'] ) && $boolDirectSet ) $this->set( 'm_strReplacementEmployeeIds', trim( stripcslashes( $arrValues['replacement_employee_ids'] ) ) ); elseif( isset( $arrValues['replacement_employee_ids'] ) ) $this->setReplacementEmployeeIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['replacement_employee_ids'] ) : $arrValues['replacement_employee_ids'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['required_experience'] ) && $boolDirectSet ) $this->set( 'm_intRequiredExperience', trim( $arrValues['required_experience'] ) ); elseif( isset( $arrValues['required_experience'] ) ) $this->setRequiredExperience( $arrValues['required_experience'] );
		if( isset( $arrValues['required_qualification'] ) && $boolDirectSet ) $this->set( 'm_intRequiredQualification', trim( $arrValues['required_qualification'] ) ); elseif( isset( $arrValues['required_qualification'] ) ) $this->setRequiredQualification( $arrValues['required_qualification'] );
		if( isset( $arrValues['deletion_requested_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletionRequestedBy', trim( $arrValues['deletion_requested_by'] ) ); elseif( isset( $arrValues['deletion_requested_by'] ) ) $this->setDeletionRequestedBy( $arrValues['deletion_requested_by'] );
		if( isset( $arrValues['deletion_confirmed_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletionConfirmedBy', trim( $arrValues['deletion_confirmed_by'] ) ); elseif( isset( $arrValues['deletion_confirmed_by'] ) ) $this->setDeletionConfirmedBy( $arrValues['deletion_confirmed_by'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['job_description'] ) && $boolDirectSet ) $this->set( 'm_strJobDescription', trim( stripcslashes( $arrValues['job_description'] ) ) ); elseif( isset( $arrValues['job_description'] ) ) $this->setJobDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['job_description'] ) : $arrValues['job_description'] );
		if( isset( $arrValues['additional_info'] ) && $boolDirectSet ) $this->set( 'm_strAdditionalInfo', trim( stripcslashes( $arrValues['additional_info'] ) ) ); elseif( isset( $arrValues['additional_info'] ) ) $this->setAdditionalInfo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['additional_info'] ) : $arrValues['additional_info'] );
		if( isset( $arrValues['required_certification'] ) && $boolDirectSet ) $this->set( 'm_strRequiredCertification', trim( stripcslashes( $arrValues['required_certification'] ) ) ); elseif( isset( $arrValues['required_certification'] ) ) $this->setRequiredCertification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['required_certification'] ) : $arrValues['required_certification'] );
		if( isset( $arrValues['remarks'] ) && $boolDirectSet ) $this->set( 'm_strRemarks', trim( stripcslashes( $arrValues['remarks'] ) ) ); elseif( isset( $arrValues['remarks'] ) ) $this->setRemarks( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remarks'] ) : $arrValues['remarks'] );
		if( isset( $arrValues['recruiter_assigned_on'] ) && $boolDirectSet ) $this->set( 'm_strRecruiterAssignedOn', trim( $arrValues['recruiter_assigned_on'] ) ); elseif( isset( $arrValues['recruiter_assigned_on'] ) ) $this->setRecruiterAssignedOn( $arrValues['recruiter_assigned_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOriginatingEmployeeId( $intOriginatingEmployeeId ) {
		$this->set( 'm_intOriginatingEmployeeId', CStrings::strToIntDef( $intOriginatingEmployeeId, NULL, false ) );
	}

	public function getOriginatingEmployeeId() {
		return $this->m_intOriginatingEmployeeId;
	}

	public function sqlOriginatingEmployeeId() {
		return ( true == isset( $this->m_intOriginatingEmployeeId ) ) ? ( string ) $this->m_intOriginatingEmployeeId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setJobPostingId( $intJobPostingId ) {
		$this->set( 'm_intJobPostingId', CStrings::strToIntDef( $intJobPostingId, NULL, false ) );
	}

	public function getJobPostingId() {
		return $this->m_intJobPostingId;
	}

	public function sqlJobPostingId() {
		return ( true == isset( $this->m_intJobPostingId ) ) ? ( string ) $this->m_intJobPostingId : 'NULL';
	}

	public function setProductId( $intProductId ) {
		$this->set( 'm_intProductId', CStrings::strToIntDef( $intProductId, NULL, false ) );
	}

	public function getProductId() {
		return $this->m_intProductId;
	}

	public function sqlProductId() {
		return ( true == isset( $this->m_intProductId ) ) ? ( string ) $this->m_intProductId : 'NULL';
	}

	public function setProductOptionId( $intProductOptionId ) {
		$this->set( 'm_intProductOptionId', CStrings::strToIntDef( $intProductOptionId, NULL, false ) );
	}

	public function getProductOptionId() {
		return $this->m_intProductOptionId;
	}

	public function sqlProductOptionId() {
		return ( true == isset( $this->m_intProductOptionId ) ) ? ( string ) $this->m_intProductOptionId : 'NULL';
	}

	public function setNumberOfResources( $intNumberOfResources ) {
		$this->set( 'm_intNumberOfResources', CStrings::strToIntDef( $intNumberOfResources, NULL, false ) );
	}

	public function getNumberOfResources() {
		return $this->m_intNumberOfResources;
	}

	public function sqlNumberOfResources() {
		return ( true == isset( $this->m_intNumberOfResources ) ) ? ( string ) $this->m_intNumberOfResources : '0';
	}

	public function setRequirementTypeId( $intRequirementTypeId ) {
		$this->set( 'm_intRequirementTypeId', CStrings::strToIntDef( $intRequirementTypeId, NULL, false ) );
	}

	public function getRequirementTypeId() {
		return $this->m_intRequirementTypeId;
	}

	public function sqlRequirementTypeId() {
		return ( true == isset( $this->m_intRequirementTypeId ) ) ? ( string ) $this->m_intRequirementTypeId : 'NULL';
	}

	public function setRequirementDueDate( $strRequirementDueDate ) {
		$this->set( 'm_strRequirementDueDate', CStrings::strTrimDef( $strRequirementDueDate, -1, NULL, true ) );
	}

	public function getRequirementDueDate() {
		return $this->m_strRequirementDueDate;
	}

	public function sqlRequirementDueDate() {
		return ( true == isset( $this->m_strRequirementDueDate ) ) ? '\'' . $this->m_strRequirementDueDate . '\'' : 'NOW()';
	}

	public function setManagerEmployeeId( $intManagerEmployeeId ) {
		$this->set( 'm_intManagerEmployeeId', CStrings::strToIntDef( $intManagerEmployeeId, NULL, false ) );
	}

	public function getManagerEmployeeId() {
		return $this->m_intManagerEmployeeId;
	}

	public function sqlManagerEmployeeId() {
		return ( true == isset( $this->m_intManagerEmployeeId ) ) ? ( string ) $this->m_intManagerEmployeeId : '0';
	}

	public function setPurchaseRequestId( $intPurchaseRequestId ) {
		$this->set( 'm_intPurchaseRequestId', CStrings::strToIntDef( $intPurchaseRequestId, NULL, false ) );
	}

	public function getPurchaseRequestId() {
		return $this->m_intPurchaseRequestId;
	}

	public function sqlPurchaseRequestId() {
		return ( true == isset( $this->m_intPurchaseRequestId ) ) ? ( string ) $this->m_intPurchaseRequestId : 'NULL';
	}

	public function setRecruiterEmployeeId( $intRecruiterEmployeeId ) {
		$this->set( 'm_intRecruiterEmployeeId', CStrings::strToIntDef( $intRecruiterEmployeeId, NULL, false ) );
	}

	public function getRecruiterEmployeeId() {
		return $this->m_intRecruiterEmployeeId;
	}

	public function sqlRecruiterEmployeeId() {
		return ( true == isset( $this->m_intRecruiterEmployeeId ) ) ? ( string ) $this->m_intRecruiterEmployeeId : 'NULL';
	}

	public function setReplacementEmployeeIds( $strReplacementEmployeeIds ) {
		$this->set( 'm_strReplacementEmployeeIds', CStrings::strTrimDef( $strReplacementEmployeeIds, 75, NULL, true ) );
	}

	public function getReplacementEmployeeIds() {
		return $this->m_strReplacementEmployeeIds;
	}

	public function sqlReplacementEmployeeIds() {
		return ( true == isset( $this->m_strReplacementEmployeeIds ) ) ? '\'' . addslashes( $this->m_strReplacementEmployeeIds ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setRequiredExperience( $intRequiredExperience ) {
		$this->set( 'm_intRequiredExperience', CStrings::strToIntDef( $intRequiredExperience, NULL, false ) );
	}

	public function getRequiredExperience() {
		return $this->m_intRequiredExperience;
	}

	public function sqlRequiredExperience() {
		return ( true == isset( $this->m_intRequiredExperience ) ) ? ( string ) $this->m_intRequiredExperience : '0';
	}

	public function setRequiredQualification( $intRequiredQualification ) {
		$this->set( 'm_intRequiredQualification', CStrings::strToIntDef( $intRequiredQualification, NULL, false ) );
	}

	public function getRequiredQualification() {
		return $this->m_intRequiredQualification;
	}

	public function sqlRequiredQualification() {
		return ( true == isset( $this->m_intRequiredQualification ) ) ? ( string ) $this->m_intRequiredQualification : 'NULL';
	}

	public function setDeletionRequestedBy( $intDeletionRequestedBy ) {
		$this->set( 'm_intDeletionRequestedBy', CStrings::strToIntDef( $intDeletionRequestedBy, NULL, false ) );
	}

	public function getDeletionRequestedBy() {
		return $this->m_intDeletionRequestedBy;
	}

	public function sqlDeletionRequestedBy() {
		return ( true == isset( $this->m_intDeletionRequestedBy ) ) ? ( string ) $this->m_intDeletionRequestedBy : 'NULL';
	}

	public function setDeletionConfirmedBy( $intDeletionConfirmedBy ) {
		$this->set( 'm_intDeletionConfirmedBy', CStrings::strToIntDef( $intDeletionConfirmedBy, NULL, false ) );
	}

	public function getDeletionConfirmedBy() {
		return $this->m_intDeletionConfirmedBy;
	}

	public function sqlDeletionConfirmedBy() {
		return ( true == isset( $this->m_intDeletionConfirmedBy ) ) ? ( string ) $this->m_intDeletionConfirmedBy : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setJobDescription( $strJobDescription ) {
		$this->set( 'm_strJobDescription', CStrings::strTrimDef( $strJobDescription, -1, NULL, true ) );
	}

	public function getJobDescription() {
		return $this->m_strJobDescription;
	}

	public function sqlJobDescription() {
		return ( true == isset( $this->m_strJobDescription ) ) ? '\'' . addslashes( $this->m_strJobDescription ) . '\'' : 'NULL';
	}

	public function setAdditionalInfo( $strAdditionalInfo ) {
		$this->set( 'm_strAdditionalInfo', CStrings::strTrimDef( $strAdditionalInfo, -1, NULL, true ) );
	}

	public function getAdditionalInfo() {
		return $this->m_strAdditionalInfo;
	}

	public function sqlAdditionalInfo() {
		return ( true == isset( $this->m_strAdditionalInfo ) ) ? '\'' . addslashes( $this->m_strAdditionalInfo ) . '\'' : 'NULL';
	}

	public function setRequiredCertification( $strRequiredCertification ) {
		$this->set( 'm_strRequiredCertification', CStrings::strTrimDef( $strRequiredCertification, 100, NULL, true ) );
	}

	public function getRequiredCertification() {
		return $this->m_strRequiredCertification;
	}

	public function sqlRequiredCertification() {
		return ( true == isset( $this->m_strRequiredCertification ) ) ? '\'' . addslashes( $this->m_strRequiredCertification ) . '\'' : 'NULL';
	}

	public function setRemarks( $strRemarks ) {
		$this->set( 'm_strRemarks', CStrings::strTrimDef( $strRemarks, 200, NULL, true ) );
	}

	public function getRemarks() {
		return $this->m_strRemarks;
	}

	public function sqlRemarks() {
		return ( true == isset( $this->m_strRemarks ) ) ? '\'' . addslashes( $this->m_strRemarks ) . '\'' : 'NULL';
	}

	public function setRecruiterAssignedOn( $strRecruiterAssignedOn ) {
		$this->set( 'm_strRecruiterAssignedOn', CStrings::strTrimDef( $strRecruiterAssignedOn, -1, NULL, true ) );
	}

	public function getRecruiterAssignedOn() {
		return $this->m_strRecruiterAssignedOn;
	}

	public function sqlRecruiterAssignedOn() {
		return ( true == isset( $this->m_strRecruiterAssignedOn ) ) ? '\'' . $this->m_strRecruiterAssignedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, originating_employee_id, designation_id, job_posting_id, product_id, product_option_id, number_of_resources, requirement_type_id, requirement_due_date, manager_employee_id, purchase_request_id, recruiter_employee_id, replacement_employee_ids, approved_by, required_experience, required_qualification, deletion_requested_by, deletion_confirmed_by, is_published, job_description, additional_info, required_certification, remarks, recruiter_assigned_on, updated_by, updated_on, deleted_by, deleted_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlOriginatingEmployeeId() . ', ' .
 						$this->sqlDesignationId() . ', ' .
 						$this->sqlJobPostingId() . ', ' .
 						$this->sqlProductId() . ', ' .
 						$this->sqlProductOptionId() . ', ' .
 						$this->sqlNumberOfResources() . ', ' .
 						$this->sqlRequirementTypeId() . ', ' .
 						$this->sqlRequirementDueDate() . ', ' .
 						$this->sqlManagerEmployeeId() . ', ' .
 						$this->sqlPurchaseRequestId() . ', ' .
 						$this->sqlRecruiterEmployeeId() . ', ' .
 						$this->sqlReplacementEmployeeIds() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlRequiredExperience() . ', ' .
 						$this->sqlRequiredQualification() . ', ' .
 						$this->sqlDeletionRequestedBy() . ', ' .
 						$this->sqlDeletionConfirmedBy() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlJobDescription() . ', ' .
 						$this->sqlAdditionalInfo() . ', ' .
 						$this->sqlRequiredCertification() . ', ' .
 						$this->sqlRemarks() . ', ' .
 						$this->sqlRecruiterAssignedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' originating_employee_id = ' . $this->sqlOriginatingEmployeeId() . ','; } elseif( true == array_key_exists( 'OriginatingEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' originating_employee_id = ' . $this->sqlOriginatingEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_posting_id = ' . $this->sqlJobPostingId() . ','; } elseif( true == array_key_exists( 'JobPostingId', $this->getChangedColumns() ) ) { $strSql .= ' job_posting_id = ' . $this->sqlJobPostingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_id = ' . $this->sqlProductId() . ','; } elseif( true == array_key_exists( 'ProductId', $this->getChangedColumns() ) ) { $strSql .= ' product_id = ' . $this->sqlProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_option_id = ' . $this->sqlProductOptionId() . ','; } elseif( true == array_key_exists( 'ProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' product_option_id = ' . $this->sqlProductOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_resources = ' . $this->sqlNumberOfResources() . ','; } elseif( true == array_key_exists( 'NumberOfResources', $this->getChangedColumns() ) ) { $strSql .= ' number_of_resources = ' . $this->sqlNumberOfResources() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requirement_type_id = ' . $this->sqlRequirementTypeId() . ','; } elseif( true == array_key_exists( 'RequirementTypeId', $this->getChangedColumns() ) ) { $strSql .= ' requirement_type_id = ' . $this->sqlRequirementTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requirement_due_date = ' . $this->sqlRequirementDueDate() . ','; } elseif( true == array_key_exists( 'RequirementDueDate', $this->getChangedColumns() ) ) { $strSql .= ' requirement_due_date = ' . $this->sqlRequirementDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manager_employee_id = ' . $this->sqlManagerEmployeeId() . ','; } elseif( true == array_key_exists( 'ManagerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' manager_employee_id = ' . $this->sqlManagerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; } elseif( true == array_key_exists( 'PurchaseRequestId', $this->getChangedColumns() ) ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recruiter_employee_id = ' . $this->sqlRecruiterEmployeeId() . ','; } elseif( true == array_key_exists( 'RecruiterEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' recruiter_employee_id = ' . $this->sqlRecruiterEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' replacement_employee_ids = ' . $this->sqlReplacementEmployeeIds() . ','; } elseif( true == array_key_exists( 'ReplacementEmployeeIds', $this->getChangedColumns() ) ) { $strSql .= ' replacement_employee_ids = ' . $this->sqlReplacementEmployeeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_experience = ' . $this->sqlRequiredExperience() . ','; } elseif( true == array_key_exists( 'RequiredExperience', $this->getChangedColumns() ) ) { $strSql .= ' required_experience = ' . $this->sqlRequiredExperience() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_qualification = ' . $this->sqlRequiredQualification() . ','; } elseif( true == array_key_exists( 'RequiredQualification', $this->getChangedColumns() ) ) { $strSql .= ' required_qualification = ' . $this->sqlRequiredQualification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deletion_requested_by = ' . $this->sqlDeletionRequestedBy() . ','; } elseif( true == array_key_exists( 'DeletionRequestedBy', $this->getChangedColumns() ) ) { $strSql .= ' deletion_requested_by = ' . $this->sqlDeletionRequestedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deletion_confirmed_by = ' . $this->sqlDeletionConfirmedBy() . ','; } elseif( true == array_key_exists( 'DeletionConfirmedBy', $this->getChangedColumns() ) ) { $strSql .= ' deletion_confirmed_by = ' . $this->sqlDeletionConfirmedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_description = ' . $this->sqlJobDescription() . ','; } elseif( true == array_key_exists( 'JobDescription', $this->getChangedColumns() ) ) { $strSql .= ' job_description = ' . $this->sqlJobDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_info = ' . $this->sqlAdditionalInfo() . ','; } elseif( true == array_key_exists( 'AdditionalInfo', $this->getChangedColumns() ) ) { $strSql .= ' additional_info = ' . $this->sqlAdditionalInfo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_certification = ' . $this->sqlRequiredCertification() . ','; } elseif( true == array_key_exists( 'RequiredCertification', $this->getChangedColumns() ) ) { $strSql .= ' required_certification = ' . $this->sqlRequiredCertification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remarks = ' . $this->sqlRemarks() . ','; } elseif( true == array_key_exists( 'Remarks', $this->getChangedColumns() ) ) { $strSql .= ' remarks = ' . $this->sqlRemarks() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recruiter_assigned_on = ' . $this->sqlRecruiterAssignedOn() . ','; } elseif( true == array_key_exists( 'RecruiterAssignedOn', $this->getChangedColumns() ) ) { $strSql .= ' recruiter_assigned_on = ' . $this->sqlRecruiterAssignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'originating_employee_id' => $this->getOriginatingEmployeeId(),
			'designation_id' => $this->getDesignationId(),
			'job_posting_id' => $this->getJobPostingId(),
			'product_id' => $this->getProductId(),
			'product_option_id' => $this->getProductOptionId(),
			'number_of_resources' => $this->getNumberOfResources(),
			'requirement_type_id' => $this->getRequirementTypeId(),
			'requirement_due_date' => $this->getRequirementDueDate(),
			'manager_employee_id' => $this->getManagerEmployeeId(),
			'purchase_request_id' => $this->getPurchaseRequestId(),
			'recruiter_employee_id' => $this->getRecruiterEmployeeId(),
			'replacement_employee_ids' => $this->getReplacementEmployeeIds(),
			'approved_by' => $this->getApprovedBy(),
			'required_experience' => $this->getRequiredExperience(),
			'required_qualification' => $this->getRequiredQualification(),
			'deletion_requested_by' => $this->getDeletionRequestedBy(),
			'deletion_confirmed_by' => $this->getDeletionConfirmedBy(),
			'is_published' => $this->getIsPublished(),
			'job_description' => $this->getJobDescription(),
			'additional_info' => $this->getAdditionalInfo(),
			'required_certification' => $this->getRequiredCertification(),
			'remarks' => $this->getRemarks(),
			'recruiter_assigned_on' => $this->getRecruiterAssignedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>