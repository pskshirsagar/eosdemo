<?php

class CBaseChargeCode extends CEosSingularBase {

	const TABLE_NAME = 'public.charge_codes';

	protected $m_intId;
	protected $m_intSalesTaxCategoryId;
	protected $m_intChargeCodeTypeId;
	protected $m_intDebitChartOfAccountId;
	protected $m_intCreditChartOfAccountId;
	protected $m_intPsProductId;
	protected $m_intRevenueTypeId;
	protected $m_intInvoiceCategoryId;
	protected $m_strCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltDefaultAmount;
	protected $m_fltRefundableAmount;
	protected $m_fltRefundablePercent;
	protected $m_fltCommissionAmount;
	protected $m_fltCommissionPercent;
	protected $m_fltCostAmount;
	protected $m_fltCostPercent;
	protected $m_intPaysCommissions;
	protected $m_intEstimatedAnnualTransCount;
	protected $m_intDontChargeLateFees;
	protected $m_intIsSystemChargeCode;
	protected $m_intIsRefundable;
	protected $m_intIsCommissioned;
	protected $m_intIsTransactional;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intRecognitionStartDays;
	protected $m_intRecognitionMonths;

	public function __construct() {
		parent::__construct();

		$this->m_intPaysCommissions = '0';
		$this->m_intEstimatedAnnualTransCount = '0';
		$this->m_intDontChargeLateFees = '0';
		$this->m_intIsSystemChargeCode = '0';
		$this->m_intIsRefundable = '0';
		$this->m_intIsCommissioned = '0';
		$this->m_intIsTransactional = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['sales_tax_category_id'] ) && $boolDirectSet ) $this->set( 'm_intSalesTaxCategoryId', trim( $arrValues['sales_tax_category_id'] ) ); elseif( isset( $arrValues['sales_tax_category_id'] ) ) $this->setSalesTaxCategoryId( $arrValues['sales_tax_category_id'] );
		if( isset( $arrValues['charge_code_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeCodeTypeId', trim( $arrValues['charge_code_type_id'] ) ); elseif( isset( $arrValues['charge_code_type_id'] ) ) $this->setChargeCodeTypeId( $arrValues['charge_code_type_id'] );
		if( isset( $arrValues['debit_chart_of_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDebitChartOfAccountId', trim( $arrValues['debit_chart_of_account_id'] ) ); elseif( isset( $arrValues['debit_chart_of_account_id'] ) ) $this->setDebitChartOfAccountId( $arrValues['debit_chart_of_account_id'] );
		if( isset( $arrValues['credit_chart_of_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditChartOfAccountId', trim( $arrValues['credit_chart_of_account_id'] ) ); elseif( isset( $arrValues['credit_chart_of_account_id'] ) ) $this->setCreditChartOfAccountId( $arrValues['credit_chart_of_account_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['revenue_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRevenueTypeId', trim( $arrValues['revenue_type_id'] ) ); elseif( isset( $arrValues['revenue_type_id'] ) ) $this->setRevenueTypeId( $arrValues['revenue_type_id'] );
		if( isset( $arrValues['invoice_category_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceCategoryId', trim( $arrValues['invoice_category_id'] ) ); elseif( isset( $arrValues['invoice_category_id'] ) ) $this->setInvoiceCategoryId( $arrValues['invoice_category_id'] );
		if( isset( $arrValues['code'] ) && $boolDirectSet ) $this->set( 'm_strCode', trim( stripcslashes( $arrValues['code'] ) ) ); elseif( isset( $arrValues['code'] ) ) $this->setCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['code'] ) : $arrValues['code'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['default_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDefaultAmount', trim( $arrValues['default_amount'] ) ); elseif( isset( $arrValues['default_amount'] ) ) $this->setDefaultAmount( $arrValues['default_amount'] );
		if( isset( $arrValues['refundable_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRefundableAmount', trim( $arrValues['refundable_amount'] ) ); elseif( isset( $arrValues['refundable_amount'] ) ) $this->setRefundableAmount( $arrValues['refundable_amount'] );
		if( isset( $arrValues['refundable_percent'] ) && $boolDirectSet ) $this->set( 'm_fltRefundablePercent', trim( $arrValues['refundable_percent'] ) ); elseif( isset( $arrValues['refundable_percent'] ) ) $this->setRefundablePercent( $arrValues['refundable_percent'] );
		if( isset( $arrValues['commission_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCommissionAmount', trim( $arrValues['commission_amount'] ) ); elseif( isset( $arrValues['commission_amount'] ) ) $this->setCommissionAmount( $arrValues['commission_amount'] );
		if( isset( $arrValues['commission_percent'] ) && $boolDirectSet ) $this->set( 'm_fltCommissionPercent', trim( $arrValues['commission_percent'] ) ); elseif( isset( $arrValues['commission_percent'] ) ) $this->setCommissionPercent( $arrValues['commission_percent'] );
		if( isset( $arrValues['cost_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCostAmount', trim( $arrValues['cost_amount'] ) ); elseif( isset( $arrValues['cost_amount'] ) ) $this->setCostAmount( $arrValues['cost_amount'] );
		if( isset( $arrValues['cost_percent'] ) && $boolDirectSet ) $this->set( 'm_fltCostPercent', trim( $arrValues['cost_percent'] ) ); elseif( isset( $arrValues['cost_percent'] ) ) $this->setCostPercent( $arrValues['cost_percent'] );
		if( isset( $arrValues['pays_commissions'] ) && $boolDirectSet ) $this->set( 'm_intPaysCommissions', trim( $arrValues['pays_commissions'] ) ); elseif( isset( $arrValues['pays_commissions'] ) ) $this->setPaysCommissions( $arrValues['pays_commissions'] );
		if( isset( $arrValues['estimated_annual_trans_count'] ) && $boolDirectSet ) $this->set( 'm_intEstimatedAnnualTransCount', trim( $arrValues['estimated_annual_trans_count'] ) ); elseif( isset( $arrValues['estimated_annual_trans_count'] ) ) $this->setEstimatedAnnualTransCount( $arrValues['estimated_annual_trans_count'] );
		if( isset( $arrValues['dont_charge_late_fees'] ) && $boolDirectSet ) $this->set( 'm_intDontChargeLateFees', trim( $arrValues['dont_charge_late_fees'] ) ); elseif( isset( $arrValues['dont_charge_late_fees'] ) ) $this->setDontChargeLateFees( $arrValues['dont_charge_late_fees'] );
		if( isset( $arrValues['is_system_charge_code'] ) && $boolDirectSet ) $this->set( 'm_intIsSystemChargeCode', trim( $arrValues['is_system_charge_code'] ) ); elseif( isset( $arrValues['is_system_charge_code'] ) ) $this->setIsSystemChargeCode( $arrValues['is_system_charge_code'] );
		if( isset( $arrValues['is_refundable'] ) && $boolDirectSet ) $this->set( 'm_intIsRefundable', trim( $arrValues['is_refundable'] ) ); elseif( isset( $arrValues['is_refundable'] ) ) $this->setIsRefundable( $arrValues['is_refundable'] );
		if( isset( $arrValues['is_commissioned'] ) && $boolDirectSet ) $this->set( 'm_intIsCommissioned', trim( $arrValues['is_commissioned'] ) ); elseif( isset( $arrValues['is_commissioned'] ) ) $this->setIsCommissioned( $arrValues['is_commissioned'] );
		if( isset( $arrValues['is_transactional'] ) && $boolDirectSet ) $this->set( 'm_intIsTransactional', trim( $arrValues['is_transactional'] ) ); elseif( isset( $arrValues['is_transactional'] ) ) $this->setIsTransactional( $arrValues['is_transactional'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['recognition_start_days'] ) && $boolDirectSet ) $this->set( 'm_intRecognitionStartDays', trim( $arrValues['recognition_start_days'] ) ); elseif( isset( $arrValues['recognition_start_days'] ) ) $this->setRecognitionStartDays( $arrValues['recognition_start_days'] );
		if( isset( $arrValues['recognition_months'] ) && $boolDirectSet ) $this->set( 'm_intRecognitionMonths', trim( $arrValues['recognition_months'] ) ); elseif( isset( $arrValues['recognition_months'] ) ) $this->setRecognitionMonths( $arrValues['recognition_months'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSalesTaxCategoryId( $intSalesTaxCategoryId ) {
		$this->set( 'm_intSalesTaxCategoryId', CStrings::strToIntDef( $intSalesTaxCategoryId, NULL, false ) );
	}

	public function getSalesTaxCategoryId() {
		return $this->m_intSalesTaxCategoryId;
	}

	public function sqlSalesTaxCategoryId() {
		return ( true == isset( $this->m_intSalesTaxCategoryId ) ) ? ( string ) $this->m_intSalesTaxCategoryId : 'NULL';
	}

	public function setChargeCodeTypeId( $intChargeCodeTypeId ) {
		$this->set( 'm_intChargeCodeTypeId', CStrings::strToIntDef( $intChargeCodeTypeId, NULL, false ) );
	}

	public function getChargeCodeTypeId() {
		return $this->m_intChargeCodeTypeId;
	}

	public function sqlChargeCodeTypeId() {
		return ( true == isset( $this->m_intChargeCodeTypeId ) ) ? ( string ) $this->m_intChargeCodeTypeId : 'NULL';
	}

	public function setDebitChartOfAccountId( $intDebitChartOfAccountId ) {
		$this->set( 'm_intDebitChartOfAccountId', CStrings::strToIntDef( $intDebitChartOfAccountId, NULL, false ) );
	}

	public function getDebitChartOfAccountId() {
		return $this->m_intDebitChartOfAccountId;
	}

	public function sqlDebitChartOfAccountId() {
		return ( true == isset( $this->m_intDebitChartOfAccountId ) ) ? ( string ) $this->m_intDebitChartOfAccountId : 'NULL';
	}

	public function setCreditChartOfAccountId( $intCreditChartOfAccountId ) {
		$this->set( 'm_intCreditChartOfAccountId', CStrings::strToIntDef( $intCreditChartOfAccountId, NULL, false ) );
	}

	public function getCreditChartOfAccountId() {
		return $this->m_intCreditChartOfAccountId;
	}

	public function sqlCreditChartOfAccountId() {
		return ( true == isset( $this->m_intCreditChartOfAccountId ) ) ? ( string ) $this->m_intCreditChartOfAccountId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setRevenueTypeId( $intRevenueTypeId ) {
		$this->set( 'm_intRevenueTypeId', CStrings::strToIntDef( $intRevenueTypeId, NULL, false ) );
	}

	public function getRevenueTypeId() {
		return $this->m_intRevenueTypeId;
	}

	public function sqlRevenueTypeId() {
		return ( true == isset( $this->m_intRevenueTypeId ) ) ? ( string ) $this->m_intRevenueTypeId : 'NULL';
	}

	public function setInvoiceCategoryId( $intInvoiceCategoryId ) {
		$this->set( 'm_intInvoiceCategoryId', CStrings::strToIntDef( $intInvoiceCategoryId, NULL, false ) );
	}

	public function getInvoiceCategoryId() {
		return $this->m_intInvoiceCategoryId;
	}

	public function sqlInvoiceCategoryId() {
		return ( true == isset( $this->m_intInvoiceCategoryId ) ) ? ( string ) $this->m_intInvoiceCategoryId : 'NULL';
	}

	public function setCode( $strCode ) {
		$this->set( 'm_strCode', CStrings::strTrimDef( $strCode, 10, NULL, true ) );
	}

	public function getCode() {
		return $this->m_strCode;
	}

	public function sqlCode() {
		return ( true == isset( $this->m_strCode ) ) ? '\'' . addslashes( $this->m_strCode ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDefaultAmount( $fltDefaultAmount ) {
		$this->set( 'm_fltDefaultAmount', CStrings::strToFloatDef( $fltDefaultAmount, NULL, false, 4 ) );
	}

	public function getDefaultAmount() {
		return $this->m_fltDefaultAmount;
	}

	public function sqlDefaultAmount() {
		return ( true == isset( $this->m_fltDefaultAmount ) ) ? ( string ) $this->m_fltDefaultAmount : 'NULL';
	}

	public function setRefundableAmount( $fltRefundableAmount ) {
		$this->set( 'm_fltRefundableAmount', CStrings::strToFloatDef( $fltRefundableAmount, NULL, false, 4 ) );
	}

	public function getRefundableAmount() {
		return $this->m_fltRefundableAmount;
	}

	public function sqlRefundableAmount() {
		return ( true == isset( $this->m_fltRefundableAmount ) ) ? ( string ) $this->m_fltRefundableAmount : 'NULL';
	}

	public function setRefundablePercent( $fltRefundablePercent ) {
		$this->set( 'm_fltRefundablePercent', CStrings::strToFloatDef( $fltRefundablePercent, NULL, false, 6 ) );
	}

	public function getRefundablePercent() {
		return $this->m_fltRefundablePercent;
	}

	public function sqlRefundablePercent() {
		return ( true == isset( $this->m_fltRefundablePercent ) ) ? ( string ) $this->m_fltRefundablePercent : 'NULL';
	}

	public function setCommissionAmount( $fltCommissionAmount ) {
		$this->set( 'm_fltCommissionAmount', CStrings::strToFloatDef( $fltCommissionAmount, NULL, false, 4 ) );
	}

	public function getCommissionAmount() {
		return $this->m_fltCommissionAmount;
	}

	public function sqlCommissionAmount() {
		return ( true == isset( $this->m_fltCommissionAmount ) ) ? ( string ) $this->m_fltCommissionAmount : 'NULL';
	}

	public function setCommissionPercent( $fltCommissionPercent ) {
		$this->set( 'm_fltCommissionPercent', CStrings::strToFloatDef( $fltCommissionPercent, NULL, false, 6 ) );
	}

	public function getCommissionPercent() {
		return $this->m_fltCommissionPercent;
	}

	public function sqlCommissionPercent() {
		return ( true == isset( $this->m_fltCommissionPercent ) ) ? ( string ) $this->m_fltCommissionPercent : 'NULL';
	}

	public function setCostAmount( $fltCostAmount ) {
		$this->set( 'm_fltCostAmount', CStrings::strToFloatDef( $fltCostAmount, NULL, false, 4 ) );
	}

	public function getCostAmount() {
		return $this->m_fltCostAmount;
	}

	public function sqlCostAmount() {
		return ( true == isset( $this->m_fltCostAmount ) ) ? ( string ) $this->m_fltCostAmount : 'NULL';
	}

	public function setCostPercent( $fltCostPercent ) {
		$this->set( 'm_fltCostPercent', CStrings::strToFloatDef( $fltCostPercent, NULL, false, 6 ) );
	}

	public function getCostPercent() {
		return $this->m_fltCostPercent;
	}

	public function sqlCostPercent() {
		return ( true == isset( $this->m_fltCostPercent ) ) ? ( string ) $this->m_fltCostPercent : 'NULL';
	}

	public function setPaysCommissions( $intPaysCommissions ) {
		$this->set( 'm_intPaysCommissions', CStrings::strToIntDef( $intPaysCommissions, NULL, false ) );
	}

	public function getPaysCommissions() {
		return $this->m_intPaysCommissions;
	}

	public function sqlPaysCommissions() {
		return ( true == isset( $this->m_intPaysCommissions ) ) ? ( string ) $this->m_intPaysCommissions : '0';
	}

	public function setEstimatedAnnualTransCount( $intEstimatedAnnualTransCount ) {
		$this->set( 'm_intEstimatedAnnualTransCount', CStrings::strToIntDef( $intEstimatedAnnualTransCount, NULL, false ) );
	}

	public function getEstimatedAnnualTransCount() {
		return $this->m_intEstimatedAnnualTransCount;
	}

	public function sqlEstimatedAnnualTransCount() {
		return ( true == isset( $this->m_intEstimatedAnnualTransCount ) ) ? ( string ) $this->m_intEstimatedAnnualTransCount : '0';
	}

	public function setDontChargeLateFees( $intDontChargeLateFees ) {
		$this->set( 'm_intDontChargeLateFees', CStrings::strToIntDef( $intDontChargeLateFees, NULL, false ) );
	}

	public function getDontChargeLateFees() {
		return $this->m_intDontChargeLateFees;
	}

	public function sqlDontChargeLateFees() {
		return ( true == isset( $this->m_intDontChargeLateFees ) ) ? ( string ) $this->m_intDontChargeLateFees : '0';
	}

	public function setIsSystemChargeCode( $intIsSystemChargeCode ) {
		$this->set( 'm_intIsSystemChargeCode', CStrings::strToIntDef( $intIsSystemChargeCode, NULL, false ) );
	}

	public function getIsSystemChargeCode() {
		return $this->m_intIsSystemChargeCode;
	}

	public function sqlIsSystemChargeCode() {
		return ( true == isset( $this->m_intIsSystemChargeCode ) ) ? ( string ) $this->m_intIsSystemChargeCode : '0';
	}

	public function setIsRefundable( $intIsRefundable ) {
		$this->set( 'm_intIsRefundable', CStrings::strToIntDef( $intIsRefundable, NULL, false ) );
	}

	public function getIsRefundable() {
		return $this->m_intIsRefundable;
	}

	public function sqlIsRefundable() {
		return ( true == isset( $this->m_intIsRefundable ) ) ? ( string ) $this->m_intIsRefundable : '0';
	}

	public function setIsCommissioned( $intIsCommissioned ) {
		$this->set( 'm_intIsCommissioned', CStrings::strToIntDef( $intIsCommissioned, NULL, false ) );
	}

	public function getIsCommissioned() {
		return $this->m_intIsCommissioned;
	}

	public function sqlIsCommissioned() {
		return ( true == isset( $this->m_intIsCommissioned ) ) ? ( string ) $this->m_intIsCommissioned : '0';
	}

	public function setIsTransactional( $intIsTransactional ) {
		$this->set( 'm_intIsTransactional', CStrings::strToIntDef( $intIsTransactional, NULL, false ) );
	}

	public function getIsTransactional() {
		return $this->m_intIsTransactional;
	}

	public function sqlIsTransactional() {
		return ( true == isset( $this->m_intIsTransactional ) ) ? ( string ) $this->m_intIsTransactional : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRecognitionStartDays( $intRecognitionStartDays ) {
		$this->set( 'm_intRecognitionStartDays', CStrings::strToIntDef( $intRecognitionStartDays, NULL, false ) );
	}

	public function getRecognitionStartDays() {
		return $this->m_intRecognitionStartDays;
	}

	public function sqlRecognitionStartDays() {
		return ( true == isset( $this->m_intRecognitionStartDays ) ) ? ( string ) $this->m_intRecognitionStartDays : 'NULL';
	}

	public function setRecognitionMonths( $intRecognitionMonths ) {
		$this->set( 'm_intRecognitionMonths', CStrings::strToIntDef( $intRecognitionMonths, NULL, false ) );
	}

	public function getRecognitionMonths() {
		return $this->m_intRecognitionMonths;
	}

	public function sqlRecognitionMonths() {
		return ( true == isset( $this->m_intRecognitionMonths ) ) ? ( string ) $this->m_intRecognitionMonths : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, sales_tax_category_id, charge_code_type_id, debit_chart_of_account_id, credit_chart_of_account_id, ps_product_id, revenue_type_id, invoice_category_id, code, name, description, default_amount, refundable_amount, refundable_percent, commission_amount, commission_percent, cost_amount, cost_percent, pays_commissions, estimated_annual_trans_count, dont_charge_late_fees, is_system_charge_code, is_refundable, is_commissioned, is_transactional, is_published, order_num, updated_by, updated_on, created_by, created_on, recognition_start_days, recognition_months )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSalesTaxCategoryId() . ', ' .
 						$this->sqlChargeCodeTypeId() . ', ' .
 						$this->sqlDebitChartOfAccountId() . ', ' .
 						$this->sqlCreditChartOfAccountId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlRevenueTypeId() . ', ' .
 						$this->sqlInvoiceCategoryId() . ', ' .
 						$this->sqlCode() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlDefaultAmount() . ', ' .
 						$this->sqlRefundableAmount() . ', ' .
 						$this->sqlRefundablePercent() . ', ' .
 						$this->sqlCommissionAmount() . ', ' .
 						$this->sqlCommissionPercent() . ', ' .
 						$this->sqlCostAmount() . ', ' .
 						$this->sqlCostPercent() . ', ' .
 						$this->sqlPaysCommissions() . ', ' .
 						$this->sqlEstimatedAnnualTransCount() . ', ' .
 						$this->sqlDontChargeLateFees() . ', ' .
 						$this->sqlIsSystemChargeCode() . ', ' .
 						$this->sqlIsRefundable() . ', ' .
 						$this->sqlIsCommissioned() . ', ' .
 						$this->sqlIsTransactional() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlRecognitionStartDays() . ', ' .
 						$this->sqlRecognitionMonths() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_tax_category_id = ' . $this->sqlSalesTaxCategoryId() . ','; } elseif( true == array_key_exists( 'SalesTaxCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' sales_tax_category_id = ' . $this->sqlSalesTaxCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_code_type_id = ' . $this->sqlChargeCodeTypeId() . ','; } elseif( true == array_key_exists( 'ChargeCodeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' charge_code_type_id = ' . $this->sqlChargeCodeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' debit_chart_of_account_id = ' . $this->sqlDebitChartOfAccountId() . ','; } elseif( true == array_key_exists( 'DebitChartOfAccountId', $this->getChangedColumns() ) ) { $strSql .= ' debit_chart_of_account_id = ' . $this->sqlDebitChartOfAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_chart_of_account_id = ' . $this->sqlCreditChartOfAccountId() . ','; } elseif( true == array_key_exists( 'CreditChartOfAccountId', $this->getChangedColumns() ) ) { $strSql .= ' credit_chart_of_account_id = ' . $this->sqlCreditChartOfAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revenue_type_id = ' . $this->sqlRevenueTypeId() . ','; } elseif( true == array_key_exists( 'RevenueTypeId', $this->getChangedColumns() ) ) { $strSql .= ' revenue_type_id = ' . $this->sqlRevenueTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_category_id = ' . $this->sqlInvoiceCategoryId() . ','; } elseif( true == array_key_exists( 'InvoiceCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_category_id = ' . $this->sqlInvoiceCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' code = ' . $this->sqlCode() . ','; } elseif( true == array_key_exists( 'Code', $this->getChangedColumns() ) ) { $strSql .= ' code = ' . $this->sqlCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_amount = ' . $this->sqlDefaultAmount() . ','; } elseif( true == array_key_exists( 'DefaultAmount', $this->getChangedColumns() ) ) { $strSql .= ' default_amount = ' . $this->sqlDefaultAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refundable_amount = ' . $this->sqlRefundableAmount() . ','; } elseif( true == array_key_exists( 'RefundableAmount', $this->getChangedColumns() ) ) { $strSql .= ' refundable_amount = ' . $this->sqlRefundableAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refundable_percent = ' . $this->sqlRefundablePercent() . ','; } elseif( true == array_key_exists( 'RefundablePercent', $this->getChangedColumns() ) ) { $strSql .= ' refundable_percent = ' . $this->sqlRefundablePercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_amount = ' . $this->sqlCommissionAmount() . ','; } elseif( true == array_key_exists( 'CommissionAmount', $this->getChangedColumns() ) ) { $strSql .= ' commission_amount = ' . $this->sqlCommissionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_percent = ' . $this->sqlCommissionPercent() . ','; } elseif( true == array_key_exists( 'CommissionPercent', $this->getChangedColumns() ) ) { $strSql .= ' commission_percent = ' . $this->sqlCommissionPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cost_amount = ' . $this->sqlCostAmount() . ','; } elseif( true == array_key_exists( 'CostAmount', $this->getChangedColumns() ) ) { $strSql .= ' cost_amount = ' . $this->sqlCostAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cost_percent = ' . $this->sqlCostPercent() . ','; } elseif( true == array_key_exists( 'CostPercent', $this->getChangedColumns() ) ) { $strSql .= ' cost_percent = ' . $this->sqlCostPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pays_commissions = ' . $this->sqlPaysCommissions() . ','; } elseif( true == array_key_exists( 'PaysCommissions', $this->getChangedColumns() ) ) { $strSql .= ' pays_commissions = ' . $this->sqlPaysCommissions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimated_annual_trans_count = ' . $this->sqlEstimatedAnnualTransCount() . ','; } elseif( true == array_key_exists( 'EstimatedAnnualTransCount', $this->getChangedColumns() ) ) { $strSql .= ' estimated_annual_trans_count = ' . $this->sqlEstimatedAnnualTransCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_charge_late_fees = ' . $this->sqlDontChargeLateFees() . ','; } elseif( true == array_key_exists( 'DontChargeLateFees', $this->getChangedColumns() ) ) { $strSql .= ' dont_charge_late_fees = ' . $this->sqlDontChargeLateFees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system_charge_code = ' . $this->sqlIsSystemChargeCode() . ','; } elseif( true == array_key_exists( 'IsSystemChargeCode', $this->getChangedColumns() ) ) { $strSql .= ' is_system_charge_code = ' . $this->sqlIsSystemChargeCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_refundable = ' . $this->sqlIsRefundable() . ','; } elseif( true == array_key_exists( 'IsRefundable', $this->getChangedColumns() ) ) { $strSql .= ' is_refundable = ' . $this->sqlIsRefundable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_commissioned = ' . $this->sqlIsCommissioned() . ','; } elseif( true == array_key_exists( 'IsCommissioned', $this->getChangedColumns() ) ) { $strSql .= ' is_commissioned = ' . $this->sqlIsCommissioned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_transactional = ' . $this->sqlIsTransactional() . ','; } elseif( true == array_key_exists( 'IsTransactional', $this->getChangedColumns() ) ) { $strSql .= ' is_transactional = ' . $this->sqlIsTransactional() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recognition_start_days = ' . $this->sqlRecognitionStartDays() . ','; } elseif( true == array_key_exists( 'RecognitionStartDays', $this->getChangedColumns() ) ) { $strSql .= ' recognition_start_days = ' . $this->sqlRecognitionStartDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recognition_months = ' . $this->sqlRecognitionMonths() . ','; } elseif( true == array_key_exists( 'RecognitionMonths', $this->getChangedColumns() ) ) { $strSql .= ' recognition_months = ' . $this->sqlRecognitionMonths() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'sales_tax_category_id' => $this->getSalesTaxCategoryId(),
			'charge_code_type_id' => $this->getChargeCodeTypeId(),
			'debit_chart_of_account_id' => $this->getDebitChartOfAccountId(),
			'credit_chart_of_account_id' => $this->getCreditChartOfAccountId(),
			'ps_product_id' => $this->getPsProductId(),
			'revenue_type_id' => $this->getRevenueTypeId(),
			'invoice_category_id' => $this->getInvoiceCategoryId(),
			'code' => $this->getCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'default_amount' => $this->getDefaultAmount(),
			'refundable_amount' => $this->getRefundableAmount(),
			'refundable_percent' => $this->getRefundablePercent(),
			'commission_amount' => $this->getCommissionAmount(),
			'commission_percent' => $this->getCommissionPercent(),
			'cost_amount' => $this->getCostAmount(),
			'cost_percent' => $this->getCostPercent(),
			'pays_commissions' => $this->getPaysCommissions(),
			'estimated_annual_trans_count' => $this->getEstimatedAnnualTransCount(),
			'dont_charge_late_fees' => $this->getDontChargeLateFees(),
			'is_system_charge_code' => $this->getIsSystemChargeCode(),
			'is_refundable' => $this->getIsRefundable(),
			'is_commissioned' => $this->getIsCommissioned(),
			'is_transactional' => $this->getIsTransactional(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'recognition_start_days' => $this->getRecognitionStartDays(),
			'recognition_months' => $this->getRecognitionMonths()
		);
	}

}
?>