<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAssessmentResponses
 * Do not add any new functions to this class.
 */

class CBaseEmployeeAssessmentResponses extends CEosPluralBase {

	/**
	 * @return CEmployeeAssessmentResponse[]
	 */
	public static function fetchEmployeeAssessmentResponses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeAssessmentResponse', $objDatabase );
	}

	/**
	 * @return CEmployeeAssessmentResponse
	 */
	public static function fetchEmployeeAssessmentResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeAssessmentResponse', $objDatabase );
	}

	public static function fetchEmployeeAssessmentResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_assessment_responses', $objDatabase );
	}

	public static function fetchEmployeeAssessmentResponseById( $intId, $objDatabase ) {
		return self::fetchEmployeeAssessmentResponse( sprintf( 'SELECT * FROM employee_assessment_responses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentResponsesByEmployeeAssessmentId( $intEmployeeAssessmentId, $objDatabase ) {
		return self::fetchEmployeeAssessmentResponses( sprintf( 'SELECT * FROM employee_assessment_responses WHERE employee_assessment_id = %d', ( int ) $intEmployeeAssessmentId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentResponsesByEmployeeAssessmentEmployeeId( $intEmployeeAssessmentEmployeeId, $objDatabase ) {
		return self::fetchEmployeeAssessmentResponses( sprintf( 'SELECT * FROM employee_assessment_responses WHERE employee_assessment_employee_id = %d', ( int ) $intEmployeeAssessmentEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentResponsesByEmployeeAssessmentQuestionId( $intEmployeeAssessmentQuestionId, $objDatabase ) {
		return self::fetchEmployeeAssessmentResponses( sprintf( 'SELECT * FROM employee_assessment_responses WHERE employee_assessment_question_id = %d', ( int ) $intEmployeeAssessmentQuestionId ), $objDatabase );
	}

}
?>