<?php

class CBaseGotoMeetingAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.goto_meeting_accounts';

	protected $m_intId;
	protected $m_strUsername;
	protected $m_strPasswordEncrypted;
	protected $m_strAccessToken;
	protected $m_strRefreshToken;
	protected $m_intIsPrivate;
	protected $m_strExpiryDate;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPrivate = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['access_token'] ) && $boolDirectSet ) $this->set( 'm_strAccessToken', trim( stripcslashes( $arrValues['access_token'] ) ) ); elseif( isset( $arrValues['access_token'] ) ) $this->setAccessToken( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['access_token'] ) : $arrValues['access_token'] );
		if( isset( $arrValues['refresh_token'] ) && $boolDirectSet ) $this->set( 'm_strRefreshToken', trim( stripcslashes( $arrValues['refresh_token'] ) ) ); elseif( isset( $arrValues['refresh_token'] ) ) $this->setRefreshToken( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['refresh_token'] ) : $arrValues['refresh_token'] );
		if( isset( $arrValues['is_private'] ) && $boolDirectSet ) $this->set( 'm_intIsPrivate', trim( $arrValues['is_private'] ) ); elseif( isset( $arrValues['is_private'] ) ) $this->setIsPrivate( $arrValues['is_private'] );
		if( isset( $arrValues['expiry_date'] ) && $boolDirectSet ) $this->set( 'm_strExpiryDate', trim( $arrValues['expiry_date'] ) ); elseif( isset( $arrValues['expiry_date'] ) ) $this->setExpiryDate( $arrValues['expiry_date'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setAccessToken( $strAccessToken ) {
		$this->set( 'm_strAccessToken', CStrings::strTrimDef( $strAccessToken, 1000, NULL, true ) );
	}

	public function getAccessToken() {
		return $this->m_strAccessToken;
	}

	public function sqlAccessToken() {
		return ( true == isset( $this->m_strAccessToken ) ) ? '\'' . addslashes( $this->m_strAccessToken ) . '\'' : 'NULL';
	}

	public function setRefreshToken( $strRefreshToken ) {
		$this->set( 'm_strRefreshToken', CStrings::strTrimDef( $strRefreshToken, 1000, NULL, true ) );
	}

	public function getRefreshToken() {
		return $this->m_strRefreshToken;
	}

	public function sqlRefreshToken() {
		return ( true == isset( $this->m_strRefreshToken ) ) ? '\'' . addslashes( $this->m_strRefreshToken ) . '\'' : 'NULL';
	}

	public function setIsPrivate( $intIsPrivate ) {
		$this->set( 'm_intIsPrivate', CStrings::strToIntDef( $intIsPrivate, NULL, false ) );
	}

	public function getIsPrivate() {
		return $this->m_intIsPrivate;
	}

	public function sqlIsPrivate() {
		return ( true == isset( $this->m_intIsPrivate ) ) ? ( string ) $this->m_intIsPrivate : '0';
	}

	public function setExpiryDate( $strExpiryDate ) {
		$this->set( 'm_strExpiryDate', CStrings::strTrimDef( $strExpiryDate, -1, NULL, true ) );
	}

	public function getExpiryDate() {
		return $this->m_strExpiryDate;
	}

	public function sqlExpiryDate() {
		return ( true == isset( $this->m_strExpiryDate ) ) ? '\'' . $this->m_strExpiryDate . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, username, password_encrypted, access_token, refresh_token, is_private, expiry_date, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlUsername() . ', ' .
 						$this->sqlPasswordEncrypted() . ', ' .
 						$this->sqlAccessToken() . ', ' .
 						$this->sqlRefreshToken() . ', ' .
 						$this->sqlIsPrivate() . ', ' .
 						$this->sqlExpiryDate() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' access_token = ' . $this->sqlAccessToken() . ','; } elseif( true == array_key_exists( 'AccessToken', $this->getChangedColumns() ) ) { $strSql .= ' access_token = ' . $this->sqlAccessToken() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' refresh_token = ' . $this->sqlRefreshToken() . ','; } elseif( true == array_key_exists( 'RefreshToken', $this->getChangedColumns() ) ) { $strSql .= ' refresh_token = ' . $this->sqlRefreshToken() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_private = ' . $this->sqlIsPrivate() . ','; } elseif( true == array_key_exists( 'IsPrivate', $this->getChangedColumns() ) ) { $strSql .= ' is_private = ' . $this->sqlIsPrivate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expiry_date = ' . $this->sqlExpiryDate() . ','; } elseif( true == array_key_exists( 'ExpiryDate', $this->getChangedColumns() ) ) { $strSql .= ' expiry_date = ' . $this->sqlExpiryDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'username' => $this->getUsername(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'access_token' => $this->getAccessToken(),
			'refresh_token' => $this->getRefreshToken(),
			'is_private' => $this->getIsPrivate(),
			'expiry_date' => $this->getExpiryDate(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>