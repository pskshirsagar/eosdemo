<?php

class CBaseBillingRequestDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.billing_request_details';

	protected $m_intId;
	protected $m_intBillingRequestId;
	protected $m_intPropertyId;
	protected $m_intProductId;
	protected $m_intContractId;
	protected $m_strCurrentBillingDate;
	protected $m_strRequestedBillingDate;
	protected $m_intAcv;
	protected $m_boolIsDelaySetupFees;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intAcv = '0';
		$this->m_boolIsDelaySetupFees = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['billing_request_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingRequestId', trim( $arrValues['billing_request_id'] ) ); elseif( isset( $arrValues['billing_request_id'] ) ) $this->setBillingRequestId( $arrValues['billing_request_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['product_id'] ) && $boolDirectSet ) $this->set( 'm_intProductId', trim( $arrValues['product_id'] ) ); elseif( isset( $arrValues['product_id'] ) ) $this->setProductId( $arrValues['product_id'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['current_billing_date'] ) && $boolDirectSet ) $this->set( 'm_strCurrentBillingDate', trim( $arrValues['current_billing_date'] ) ); elseif( isset( $arrValues['current_billing_date'] ) ) $this->setCurrentBillingDate( $arrValues['current_billing_date'] );
		if( isset( $arrValues['requested_billing_date'] ) && $boolDirectSet ) $this->set( 'm_strRequestedBillingDate', trim( $arrValues['requested_billing_date'] ) ); elseif( isset( $arrValues['requested_billing_date'] ) ) $this->setRequestedBillingDate( $arrValues['requested_billing_date'] );
		if( isset( $arrValues['acv'] ) && $boolDirectSet ) $this->set( 'm_intAcv', trim( $arrValues['acv'] ) ); elseif( isset( $arrValues['acv'] ) ) $this->setAcv( $arrValues['acv'] );
		if( isset( $arrValues['is_delay_setup_fees'] ) && $boolDirectSet ) $this->set( 'm_boolIsDelaySetupFees', trim( stripcslashes( $arrValues['is_delay_setup_fees'] ) ) ); elseif( isset( $arrValues['is_delay_setup_fees'] ) ) $this->setIsDelaySetupFees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_delay_setup_fees'] ) : $arrValues['is_delay_setup_fees'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setBillingRequestId( $intBillingRequestId ) {
		$this->set( 'm_intBillingRequestId', CStrings::strToIntDef( $intBillingRequestId, NULL, false ) );
	}

	public function getBillingRequestId() {
		return $this->m_intBillingRequestId;
	}

	public function sqlBillingRequestId() {
		return ( true == isset( $this->m_intBillingRequestId ) ) ? ( string ) $this->m_intBillingRequestId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setProductId( $intProductId ) {
		$this->set( 'm_intProductId', CStrings::strToIntDef( $intProductId, NULL, false ) );
	}

	public function getProductId() {
		return $this->m_intProductId;
	}

	public function sqlProductId() {
		return ( true == isset( $this->m_intProductId ) ) ? ( string ) $this->m_intProductId : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setCurrentBillingDate( $strCurrentBillingDate ) {
		$this->set( 'm_strCurrentBillingDate', CStrings::strTrimDef( $strCurrentBillingDate, -1, NULL, true ) );
	}

	public function getCurrentBillingDate() {
		return $this->m_strCurrentBillingDate;
	}

	public function sqlCurrentBillingDate() {
		return ( true == isset( $this->m_strCurrentBillingDate ) ) ? '\'' . $this->m_strCurrentBillingDate . '\'' : 'NOW()';
	}

	public function setRequestedBillingDate( $strRequestedBillingDate ) {
		$this->set( 'm_strRequestedBillingDate', CStrings::strTrimDef( $strRequestedBillingDate, -1, NULL, true ) );
	}

	public function getRequestedBillingDate() {
		return $this->m_strRequestedBillingDate;
	}

	public function sqlRequestedBillingDate() {
		return ( true == isset( $this->m_strRequestedBillingDate ) ) ? '\'' . $this->m_strRequestedBillingDate . '\'' : 'NOW()';
	}

	public function setAcv( $intAcv ) {
		$this->set( 'm_intAcv', CStrings::strToIntDef( $intAcv, NULL, false ) );
	}

	public function getAcv() {
		return $this->m_intAcv;
	}

	public function sqlAcv() {
		return ( true == isset( $this->m_intAcv ) ) ? ( string ) $this->m_intAcv : '0';
	}

	public function setIsDelaySetupFees( $boolIsDelaySetupFees ) {
		$this->set( 'm_boolIsDelaySetupFees', CStrings::strToBool( $boolIsDelaySetupFees ) );
	}

	public function getIsDelaySetupFees() {
		return $this->m_boolIsDelaySetupFees;
	}

	public function sqlIsDelaySetupFees() {
		return ( true == isset( $this->m_boolIsDelaySetupFees ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDelaySetupFees ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, billing_request_id, property_id, product_id, contract_id, current_billing_date, requested_billing_date, acv, is_delay_setup_fees, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlBillingRequestId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlProductId() . ', ' .
 						$this->sqlContractId() . ', ' .
 						$this->sqlCurrentBillingDate() . ', ' .
 						$this->sqlRequestedBillingDate() . ', ' .
 						$this->sqlAcv() . ', ' .
 						$this->sqlIsDelaySetupFees() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_request_id = ' . $this->sqlBillingRequestId() . ','; } elseif( true == array_key_exists( 'BillingRequestId', $this->getChangedColumns() ) ) { $strSql .= ' billing_request_id = ' . $this->sqlBillingRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_id = ' . $this->sqlProductId() . ','; } elseif( true == array_key_exists( 'ProductId', $this->getChangedColumns() ) ) { $strSql .= ' product_id = ' . $this->sqlProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_billing_date = ' . $this->sqlCurrentBillingDate() . ','; } elseif( true == array_key_exists( 'CurrentBillingDate', $this->getChangedColumns() ) ) { $strSql .= ' current_billing_date = ' . $this->sqlCurrentBillingDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_billing_date = ' . $this->sqlRequestedBillingDate() . ','; } elseif( true == array_key_exists( 'RequestedBillingDate', $this->getChangedColumns() ) ) { $strSql .= ' requested_billing_date = ' . $this->sqlRequestedBillingDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' acv = ' . $this->sqlAcv() . ','; } elseif( true == array_key_exists( 'Acv', $this->getChangedColumns() ) ) { $strSql .= ' acv = ' . $this->sqlAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_delay_setup_fees = ' . $this->sqlIsDelaySetupFees() . ','; } elseif( true == array_key_exists( 'IsDelaySetupFees', $this->getChangedColumns() ) ) { $strSql .= ' is_delay_setup_fees = ' . $this->sqlIsDelaySetupFees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'billing_request_id' => $this->getBillingRequestId(),
			'property_id' => $this->getPropertyId(),
			'product_id' => $this->getProductId(),
			'contract_id' => $this->getContractId(),
			'current_billing_date' => $this->getCurrentBillingDate(),
			'requested_billing_date' => $this->getRequestedBillingDate(),
			'acv' => $this->getAcv(),
			'is_delay_setup_fees' => $this->getIsDelaySetupFees(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>