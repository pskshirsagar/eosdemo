<?php

class CBaseContractAgreementTemplate extends CEosSingularBase {

	const TABLE_NAME = 'public.contract_agreement_templates';

	protected $m_intId;
	protected $m_intPsLeadId;
	protected $m_intContractId;
	protected $m_intContractDocumentId;
	protected $m_intAgreementTemplateId;
	protected $m_intIsModified;
	protected $m_intDontAllowUpgrades;
	protected $m_intOrderNum;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsModified = '0';
		$this->m_intDontAllowUpgrades = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['contract_document_id'] ) && $boolDirectSet ) $this->set( 'm_intContractDocumentId', trim( $arrValues['contract_document_id'] ) ); elseif( isset( $arrValues['contract_document_id'] ) ) $this->setContractDocumentId( $arrValues['contract_document_id'] );
		if( isset( $arrValues['agreement_template_id'] ) && $boolDirectSet ) $this->set( 'm_intAgreementTemplateId', trim( $arrValues['agreement_template_id'] ) ); elseif( isset( $arrValues['agreement_template_id'] ) ) $this->setAgreementTemplateId( $arrValues['agreement_template_id'] );
		if( isset( $arrValues['is_modified'] ) && $boolDirectSet ) $this->set( 'm_intIsModified', trim( $arrValues['is_modified'] ) ); elseif( isset( $arrValues['is_modified'] ) ) $this->setIsModified( $arrValues['is_modified'] );
		if( isset( $arrValues['dont_allow_upgrades'] ) && $boolDirectSet ) $this->set( 'm_intDontAllowUpgrades', trim( $arrValues['dont_allow_upgrades'] ) ); elseif( isset( $arrValues['dont_allow_upgrades'] ) ) $this->setDontAllowUpgrades( $arrValues['dont_allow_upgrades'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setContractDocumentId( $intContractDocumentId ) {
		$this->set( 'm_intContractDocumentId', CStrings::strToIntDef( $intContractDocumentId, NULL, false ) );
	}

	public function getContractDocumentId() {
		return $this->m_intContractDocumentId;
	}

	public function sqlContractDocumentId() {
		return ( true == isset( $this->m_intContractDocumentId ) ) ? ( string ) $this->m_intContractDocumentId : 'NULL';
	}

	public function setAgreementTemplateId( $intAgreementTemplateId ) {
		$this->set( 'm_intAgreementTemplateId', CStrings::strToIntDef( $intAgreementTemplateId, NULL, false ) );
	}

	public function getAgreementTemplateId() {
		return $this->m_intAgreementTemplateId;
	}

	public function sqlAgreementTemplateId() {
		return ( true == isset( $this->m_intAgreementTemplateId ) ) ? ( string ) $this->m_intAgreementTemplateId : 'NULL';
	}

	public function setIsModified( $intIsModified ) {
		$this->set( 'm_intIsModified', CStrings::strToIntDef( $intIsModified, NULL, false ) );
	}

	public function getIsModified() {
		return $this->m_intIsModified;
	}

	public function sqlIsModified() {
		return ( true == isset( $this->m_intIsModified ) ) ? ( string ) $this->m_intIsModified : '0';
	}

	public function setDontAllowUpgrades( $intDontAllowUpgrades ) {
		$this->set( 'm_intDontAllowUpgrades', CStrings::strToIntDef( $intDontAllowUpgrades, NULL, false ) );
	}

	public function getDontAllowUpgrades() {
		return $this->m_intDontAllowUpgrades;
	}

	public function sqlDontAllowUpgrades() {
		return ( true == isset( $this->m_intDontAllowUpgrades ) ) ? ( string ) $this->m_intDontAllowUpgrades : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_lead_id, contract_id, contract_document_id, agreement_template_id, is_modified, dont_allow_upgrades, order_num, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsLeadId() . ', ' .
 						$this->sqlContractId() . ', ' .
 						$this->sqlContractDocumentId() . ', ' .
 						$this->sqlAgreementTemplateId() . ', ' .
 						$this->sqlIsModified() . ', ' .
 						$this->sqlDontAllowUpgrades() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_document_id = ' . $this->sqlContractDocumentId() . ','; } elseif( true == array_key_exists( 'ContractDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' contract_document_id = ' . $this->sqlContractDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agreement_template_id = ' . $this->sqlAgreementTemplateId() . ','; } elseif( true == array_key_exists( 'AgreementTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' agreement_template_id = ' . $this->sqlAgreementTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_modified = ' . $this->sqlIsModified() . ','; } elseif( true == array_key_exists( 'IsModified', $this->getChangedColumns() ) ) { $strSql .= ' is_modified = ' . $this->sqlIsModified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dont_allow_upgrades = ' . $this->sqlDontAllowUpgrades() . ','; } elseif( true == array_key_exists( 'DontAllowUpgrades', $this->getChangedColumns() ) ) { $strSql .= ' dont_allow_upgrades = ' . $this->sqlDontAllowUpgrades() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'contract_id' => $this->getContractId(),
			'contract_document_id' => $this->getContractDocumentId(),
			'agreement_template_id' => $this->getAgreementTemplateId(),
			'is_modified' => $this->getIsModified(),
			'dont_allow_upgrades' => $this->getDontAllowUpgrades(),
			'order_num' => $this->getOrderNum(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>