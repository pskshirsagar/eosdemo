<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationContactTypes
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationContactTypes extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationContactType[]
	 */
	public static function fetchEmployeeApplicationContactTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationContactType', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationContactType
	 */
	public static function fetchEmployeeApplicationContactType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationContactType', $objDatabase );
	}

	public static function fetchEmployeeApplicationContactTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_contact_types', $objDatabase );
	}

	public static function fetchEmployeeApplicationContactTypeById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationContactType( sprintf( 'SELECT * FROM employee_application_contact_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>