<?php

class CBaseSurvey extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.surveys';

	protected $m_intId;
	protected $m_intScheduledSurveyId;
	protected $m_intSurveyTemplateId;
	protected $m_intSurveyTypeId;
	protected $m_intPsLeadId;
	protected $m_intTriggerReferenceId;
	protected $m_intResponderReferenceId;
	protected $m_intSubjectReferenceId;
	protected $m_strSurveyDatetime;
	protected $m_strResponseDatetime;
	protected $m_strDeclinedDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strSurveyDatetime = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['scheduled_survey_id'] ) && $boolDirectSet ) $this->set( 'm_intScheduledSurveyId', trim( $arrValues['scheduled_survey_id'] ) ); elseif( isset( $arrValues['scheduled_survey_id'] ) ) $this->setScheduledSurveyId( $arrValues['scheduled_survey_id'] );
		if( isset( $arrValues['survey_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateId', trim( $arrValues['survey_template_id'] ) ); elseif( isset( $arrValues['survey_template_id'] ) ) $this->setSurveyTemplateId( $arrValues['survey_template_id'] );
		if( isset( $arrValues['survey_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTypeId', trim( $arrValues['survey_type_id'] ) ); elseif( isset( $arrValues['survey_type_id'] ) ) $this->setSurveyTypeId( $arrValues['survey_type_id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['trigger_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intTriggerReferenceId', trim( $arrValues['trigger_reference_id'] ) ); elseif( isset( $arrValues['trigger_reference_id'] ) ) $this->setTriggerReferenceId( $arrValues['trigger_reference_id'] );
		if( isset( $arrValues['responder_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intResponderReferenceId', trim( $arrValues['responder_reference_id'] ) ); elseif( isset( $arrValues['responder_reference_id'] ) ) $this->setResponderReferenceId( $arrValues['responder_reference_id'] );
		if( isset( $arrValues['subject_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intSubjectReferenceId', trim( $arrValues['subject_reference_id'] ) ); elseif( isset( $arrValues['subject_reference_id'] ) ) $this->setSubjectReferenceId( $arrValues['subject_reference_id'] );
		if( isset( $arrValues['survey_datetime'] ) && $boolDirectSet ) $this->set( 'm_strSurveyDatetime', trim( $arrValues['survey_datetime'] ) ); elseif( isset( $arrValues['survey_datetime'] ) ) $this->setSurveyDatetime( $arrValues['survey_datetime'] );
		if( isset( $arrValues['response_datetime'] ) && $boolDirectSet ) $this->set( 'm_strResponseDatetime', trim( $arrValues['response_datetime'] ) ); elseif( isset( $arrValues['response_datetime'] ) ) $this->setResponseDatetime( $arrValues['response_datetime'] );
		if( isset( $arrValues['declined_datetime'] ) && $boolDirectSet ) $this->set( 'm_strDeclinedDatetime', trim( $arrValues['declined_datetime'] ) ); elseif( isset( $arrValues['declined_datetime'] ) ) $this->setDeclinedDatetime( $arrValues['declined_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScheduledSurveyId( $intScheduledSurveyId ) {
		$this->set( 'm_intScheduledSurveyId', CStrings::strToIntDef( $intScheduledSurveyId, NULL, false ) );
	}

	public function getScheduledSurveyId() {
		return $this->m_intScheduledSurveyId;
	}

	public function sqlScheduledSurveyId() {
		return ( true == isset( $this->m_intScheduledSurveyId ) ) ? ( string ) $this->m_intScheduledSurveyId : 'NULL';
	}

	public function setSurveyTemplateId( $intSurveyTemplateId ) {
		$this->set( 'm_intSurveyTemplateId', CStrings::strToIntDef( $intSurveyTemplateId, NULL, false ) );
	}

	public function getSurveyTemplateId() {
		return $this->m_intSurveyTemplateId;
	}

	public function sqlSurveyTemplateId() {
		return ( true == isset( $this->m_intSurveyTemplateId ) ) ? ( string ) $this->m_intSurveyTemplateId : 'NULL';
	}

	public function setSurveyTypeId( $intSurveyTypeId ) {
		$this->set( 'm_intSurveyTypeId', CStrings::strToIntDef( $intSurveyTypeId, NULL, false ) );
	}

	public function getSurveyTypeId() {
		return $this->m_intSurveyTypeId;
	}

	public function sqlSurveyTypeId() {
		return ( true == isset( $this->m_intSurveyTypeId ) ) ? ( string ) $this->m_intSurveyTypeId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setTriggerReferenceId( $intTriggerReferenceId ) {
		$this->set( 'm_intTriggerReferenceId', CStrings::strToIntDef( $intTriggerReferenceId, NULL, false ) );
	}

	public function getTriggerReferenceId() {
		return $this->m_intTriggerReferenceId;
	}

	public function sqlTriggerReferenceId() {
		return ( true == isset( $this->m_intTriggerReferenceId ) ) ? ( string ) $this->m_intTriggerReferenceId : 'NULL';
	}

	public function setResponderReferenceId( $intResponderReferenceId ) {
		$this->set( 'm_intResponderReferenceId', CStrings::strToIntDef( $intResponderReferenceId, NULL, false ) );
	}

	public function getResponderReferenceId() {
		return $this->m_intResponderReferenceId;
	}

	public function sqlResponderReferenceId() {
		return ( true == isset( $this->m_intResponderReferenceId ) ) ? ( string ) $this->m_intResponderReferenceId : 'NULL';
	}

	public function setSubjectReferenceId( $intSubjectReferenceId ) {
		$this->set( 'm_intSubjectReferenceId', CStrings::strToIntDef( $intSubjectReferenceId, NULL, false ) );
	}

	public function getSubjectReferenceId() {
		return $this->m_intSubjectReferenceId;
	}

	public function sqlSubjectReferenceId() {
		return ( true == isset( $this->m_intSubjectReferenceId ) ) ? ( string ) $this->m_intSubjectReferenceId : 'NULL';
	}

	public function setSurveyDatetime( $strSurveyDatetime ) {
		$this->set( 'm_strSurveyDatetime', CStrings::strTrimDef( $strSurveyDatetime, -1, NULL, true ) );
	}

	public function getSurveyDatetime() {
		return $this->m_strSurveyDatetime;
	}

	public function sqlSurveyDatetime() {
		return ( true == isset( $this->m_strSurveyDatetime ) ) ? '\'' . $this->m_strSurveyDatetime . '\'' : 'NOW()';
	}

	public function setResponseDatetime( $strResponseDatetime ) {
		$this->set( 'm_strResponseDatetime', CStrings::strTrimDef( $strResponseDatetime, -1, NULL, true ) );
	}

	public function getResponseDatetime() {
		return $this->m_strResponseDatetime;
	}

	public function sqlResponseDatetime() {
		return ( true == isset( $this->m_strResponseDatetime ) ) ? '\'' . $this->m_strResponseDatetime . '\'' : 'NULL';
	}

	public function setDeclinedDatetime( $strDeclinedDatetime ) {
		$this->set( 'm_strDeclinedDatetime', CStrings::strTrimDef( $strDeclinedDatetime, -1, NULL, true ) );
	}

	public function getDeclinedDatetime() {
		return $this->m_strDeclinedDatetime;
	}

	public function sqlDeclinedDatetime() {
		return ( true == isset( $this->m_strDeclinedDatetime ) ) ? '\'' . $this->m_strDeclinedDatetime . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, scheduled_survey_id, survey_template_id, survey_type_id, ps_lead_id, trigger_reference_id, responder_reference_id, subject_reference_id, survey_datetime, response_datetime, declined_datetime, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlScheduledSurveyId() . ', ' .
						$this->sqlSurveyTemplateId() . ', ' .
						$this->sqlSurveyTypeId() . ', ' .
						$this->sqlPsLeadId() . ', ' .
						$this->sqlTriggerReferenceId() . ', ' .
						$this->sqlResponderReferenceId() . ', ' .
						$this->sqlSubjectReferenceId() . ', ' .
						$this->sqlSurveyDatetime() . ', ' .
						$this->sqlResponseDatetime() . ', ' .
						$this->sqlDeclinedDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_survey_id = ' . $this->sqlScheduledSurveyId(). ',' ; } elseif( true == array_key_exists( 'ScheduledSurveyId', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_survey_id = ' . $this->sqlScheduledSurveyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId(). ',' ; } elseif( true == array_key_exists( 'SurveyTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_type_id = ' . $this->sqlSurveyTypeId(). ',' ; } elseif( true == array_key_exists( 'SurveyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' survey_type_id = ' . $this->sqlSurveyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trigger_reference_id = ' . $this->sqlTriggerReferenceId(). ',' ; } elseif( true == array_key_exists( 'TriggerReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' trigger_reference_id = ' . $this->sqlTriggerReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' responder_reference_id = ' . $this->sqlResponderReferenceId(). ',' ; } elseif( true == array_key_exists( 'ResponderReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' responder_reference_id = ' . $this->sqlResponderReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject_reference_id = ' . $this->sqlSubjectReferenceId(). ',' ; } elseif( true == array_key_exists( 'SubjectReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' subject_reference_id = ' . $this->sqlSubjectReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_datetime = ' . $this->sqlSurveyDatetime(). ',' ; } elseif( true == array_key_exists( 'SurveyDatetime', $this->getChangedColumns() ) ) { $strSql .= ' survey_datetime = ' . $this->sqlSurveyDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_datetime = ' . $this->sqlResponseDatetime(). ',' ; } elseif( true == array_key_exists( 'ResponseDatetime', $this->getChangedColumns() ) ) { $strSql .= ' response_datetime = ' . $this->sqlResponseDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' declined_datetime = ' . $this->sqlDeclinedDatetime(). ',' ; } elseif( true == array_key_exists( 'DeclinedDatetime', $this->getChangedColumns() ) ) { $strSql .= ' declined_datetime = ' . $this->sqlDeclinedDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'scheduled_survey_id' => $this->getScheduledSurveyId(),
			'survey_template_id' => $this->getSurveyTemplateId(),
			'survey_type_id' => $this->getSurveyTypeId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'trigger_reference_id' => $this->getTriggerReferenceId(),
			'responder_reference_id' => $this->getResponderReferenceId(),
			'subject_reference_id' => $this->getSubjectReferenceId(),
			'survey_datetime' => $this->getSurveyDatetime(),
			'response_datetime' => $this->getResponseDatetime(),
			'declined_datetime' => $this->getDeclinedDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>