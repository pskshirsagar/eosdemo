<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CJobPostingCategories
 * Do not add any new functions to this class.
 */

class CBaseJobPostingCategories extends CEosPluralBase {

	/**
	 * @return CJobPostingCategory[]
	 */
	public static function fetchJobPostingCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CJobPostingCategory', $objDatabase );
	}

	/**
	 * @return CJobPostingCategory
	 */
	public static function fetchJobPostingCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobPostingCategory', $objDatabase );
	}

	public static function fetchJobPostingCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_posting_categories', $objDatabase );
	}

	public static function fetchJobPostingCategoryById( $intId, $objDatabase ) {
		return self::fetchJobPostingCategory( sprintf( 'SELECT * FROM job_posting_categories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>