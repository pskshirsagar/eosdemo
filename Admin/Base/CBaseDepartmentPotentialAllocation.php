<?php

class CBaseDepartmentPotentialAllocation extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.department_potential_allocations';

	protected $m_intId;
	protected $m_intDepartmentId;
	protected $m_strAllocatedPotential;
	protected $m_strPayReviewYear;
	protected $m_strAdditionalPotential;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['allocated_potential'] ) && $boolDirectSet ) $this->set( 'm_strAllocatedPotential', trim( $arrValues['allocated_potential'] ) ); elseif( isset( $arrValues['allocated_potential'] ) ) $this->setAllocatedPotential( $arrValues['allocated_potential'] );
		if( isset( $arrValues['pay_review_year'] ) && $boolDirectSet ) $this->set( 'm_strPayReviewYear', trim( $arrValues['pay_review_year'] ) ); elseif( isset( $arrValues['pay_review_year'] ) ) $this->setPayReviewYear( $arrValues['pay_review_year'] );
		if( isset( $arrValues['additional_potential'] ) && $boolDirectSet ) $this->set( 'm_strAdditionalPotential', trim( $arrValues['additional_potential'] ) ); elseif( isset( $arrValues['additional_potential'] ) ) $this->setAdditionalPotential( $arrValues['additional_potential'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setAllocatedPotential( $strAllocatedPotential ) {
		$this->set( 'm_strAllocatedPotential', CStrings::strTrimDef( $strAllocatedPotential, -1, NULL, true ) );
	}

	public function getAllocatedPotential() {
		return $this->m_strAllocatedPotential;
	}

	public function sqlAllocatedPotential() {
		return ( true == isset( $this->m_strAllocatedPotential ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAllocatedPotential ) : '\'' . addslashes( $this->m_strAllocatedPotential ) . '\'' ) : 'NULL';
	}

	public function setPayReviewYear( $strPayReviewYear ) {
		$this->set( 'm_strPayReviewYear', CStrings::strTrimDef( $strPayReviewYear, -1, NULL, true ) );
	}

	public function getPayReviewYear() {
		return $this->m_strPayReviewYear;
	}

	public function sqlPayReviewYear() {
		return ( true == isset( $this->m_strPayReviewYear ) ) ? '\'' . $this->m_strPayReviewYear . '\'' : 'NULL';
	}

	public function setAdditionalPotential( $strAdditionalPotential ) {
		$this->set( 'm_strAdditionalPotential', CStrings::strTrimDef( $strAdditionalPotential, -1, NULL, true ) );
	}

	public function getAdditionalPotential() {
		return $this->m_strAdditionalPotential;
	}

	public function sqlAdditionalPotential() {
		return ( true == isset( $this->m_strAdditionalPotential ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAdditionalPotential ) : '\'' . addslashes( $this->m_strAdditionalPotential ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, department_id, allocated_potential, pay_review_year, additional_potential, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDepartmentId() . ', ' .
						$this->sqlAllocatedPotential() . ', ' .
						$this->sqlPayReviewYear() . ', ' .
						$this->sqlAdditionalPotential() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId(). ',' ; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocated_potential = ' . $this->sqlAllocatedPotential(). ',' ; } elseif( true == array_key_exists( 'AllocatedPotential', $this->getChangedColumns() ) ) { $strSql .= ' allocated_potential = ' . $this->sqlAllocatedPotential() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pay_review_year = ' . $this->sqlPayReviewYear(). ',' ; } elseif( true == array_key_exists( 'PayReviewYear', $this->getChangedColumns() ) ) { $strSql .= ' pay_review_year = ' . $this->sqlPayReviewYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_potential = ' . $this->sqlAdditionalPotential(). ',' ; } elseif( true == array_key_exists( 'AdditionalPotential', $this->getChangedColumns() ) ) { $strSql .= ' additional_potential = ' . $this->sqlAdditionalPotential() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'department_id' => $this->getDepartmentId(),
			'allocated_potential' => $this->getAllocatedPotential(),
			'pay_review_year' => $this->getPayReviewYear(),
			'additional_potential' => $this->getAdditionalPotential(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>