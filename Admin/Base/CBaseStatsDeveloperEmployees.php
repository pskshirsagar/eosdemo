<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsDeveloperEmployees
 * Do not add any new functions to this class.
 */

class CBaseStatsDeveloperEmployees extends CEosPluralBase {

	/**
	 * @return CStatsDeveloperEmployee[]
	 */
	public static function fetchStatsDeveloperEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStatsDeveloperEmployee', $objDatabase );
	}

	/**
	 * @return CStatsDeveloperEmployee
	 */
	public static function fetchStatsDeveloperEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStatsDeveloperEmployee', $objDatabase );
	}

	public static function fetchStatsDeveloperEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stats_developer_employees', $objDatabase );
	}

	public static function fetchStatsDeveloperEmployeeById( $intId, $objDatabase ) {
		return self::fetchStatsDeveloperEmployee( sprintf( 'SELECT * FROM stats_developer_employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStatsDeveloperEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchStatsDeveloperEmployees( sprintf( 'SELECT * FROM stats_developer_employees WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>