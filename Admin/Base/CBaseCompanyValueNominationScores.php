<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyValueNominationScores
 * Do not add any new functions to this class.
 */

class CBaseCompanyValueNominationScores extends CEosPluralBase {

	/**
	 * @return CCompanyValueNominationScore[]
	 */
	public static function fetchCompanyValueNominationScores( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyValueNominationScore::class, $objDatabase );
	}

	/**
	 * @return CCompanyValueNominationScore
	 */
	public static function fetchCompanyValueNominationScore( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyValueNominationScore::class, $objDatabase );
	}

	public static function fetchCompanyValueNominationScoreCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_value_nomination_scores', $objDatabase );
	}

	public static function fetchCompanyValueNominationScoreById( $intId, $objDatabase ) {
		return self::fetchCompanyValueNominationScore( sprintf( 'SELECT * FROM company_value_nomination_scores WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCompanyValueNominationScoresByCompanyValueNominationId( $intCompanyValueNominationId, $objDatabase ) {
		return self::fetchCompanyValueNominationScores( sprintf( 'SELECT * FROM company_value_nomination_scores WHERE company_value_nomination_id = %d', ( int ) $intCompanyValueNominationId ), $objDatabase );
	}

	public static function fetchCompanyValueNominationScoresByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchCompanyValueNominationScores( sprintf( 'SELECT * FROM company_value_nomination_scores WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>