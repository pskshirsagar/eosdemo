<?php

class CBaseReleaseNote extends CEosSingularBase {

	const TABLE_NAME = 'public.release_notes';

	protected $m_intId;
	protected $m_intTaskId;
	protected $m_intReleaseNoteTypeId;
	protected $m_intEmployeeId;
	protected $m_strPathway;
	protected $m_strInstruction;
	protected $m_strDescription;
	protected $m_strAction;
	protected $m_strInternalNote;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strTitle;
	protected $m_strSettingLocation;
	protected $m_strSourceLocation;
	protected $m_boolIsApproved;
	protected $m_boolIsExternal;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['release_note_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReleaseNoteTypeId', trim( $arrValues['release_note_type_id'] ) ); elseif( isset( $arrValues['release_note_type_id'] ) ) $this->setReleaseNoteTypeId( $arrValues['release_note_type_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['pathway'] ) && $boolDirectSet ) $this->set( 'm_strPathway', trim( $arrValues['pathway'] ) ); elseif( isset( $arrValues['pathway'] ) ) $this->setPathway( $arrValues['pathway'] );
		if( isset( $arrValues['instruction'] ) && $boolDirectSet ) $this->set( 'm_strInstruction', trim( $arrValues['instruction'] ) ); elseif( isset( $arrValues['instruction'] ) ) $this->setInstruction( $arrValues['instruction'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( $arrValues['action'] ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( $arrValues['action'] );
		if( isset( $arrValues['internal_note'] ) && $boolDirectSet ) $this->set( 'm_strInternalNote', trim( $arrValues['internal_note'] ) ); elseif( isset( $arrValues['internal_note'] ) ) $this->setInternalNote( $arrValues['internal_note'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['setting_location'] ) && $boolDirectSet ) $this->set( 'm_strSettingLocation', trim( $arrValues['setting_location'] ) ); elseif( isset( $arrValues['setting_location'] ) ) $this->setSettingLocation( $arrValues['setting_location'] );
		if( isset( $arrValues['source_location'] ) && $boolDirectSet ) $this->set( 'm_strSourceLocation', trim( $arrValues['source_location'] ) ); elseif( isset( $arrValues['source_location'] ) ) $this->setSourceLocation( $arrValues['source_location'] );
		if( isset( $arrValues['is_approved'] ) && $boolDirectSet ) $this->set( 'm_boolIsApproved', trim( stripcslashes( $arrValues['is_approved'] ) ) ); elseif( isset( $arrValues['is_approved'] ) ) $this->setIsApproved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_approved'] ) : $arrValues['is_approved'] );
		if( isset( $arrValues['is_external'] ) && $boolDirectSet ) $this->set( 'm_boolIsExternal', trim( stripcslashes( $arrValues['is_external'] ) ) ); elseif( isset( $arrValues['is_external'] ) ) $this->setIsExternal( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_external'] ) : $arrValues['is_external'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setReleaseNoteTypeId( $intReleaseNoteTypeId ) {
		$this->set( 'm_intReleaseNoteTypeId', CStrings::strToIntDef( $intReleaseNoteTypeId, NULL, false ) );
	}

	public function getReleaseNoteTypeId() {
		return $this->m_intReleaseNoteTypeId;
	}

	public function sqlReleaseNoteTypeId() {
		return ( true == isset( $this->m_intReleaseNoteTypeId ) ) ? ( string ) $this->m_intReleaseNoteTypeId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setPathway( $strPathway ) {
		$this->set( 'm_strPathway', CStrings::strTrimDef( $strPathway, -1, NULL, true ) );
	}

	public function getPathway() {
		return $this->m_strPathway;
	}

	public function sqlPathway() {
		return ( true == isset( $this->m_strPathway ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPathway ) : '\'' . addslashes( $this->m_strPathway ) . '\'' ) : 'NULL';
	}

	public function setInstruction( $strInstruction ) {
		$this->set( 'm_strInstruction', CStrings::strTrimDef( $strInstruction, -1, NULL, true ) );
	}

	public function getInstruction() {
		return $this->m_strInstruction;
	}

	public function sqlInstruction() {
		return ( true == isset( $this->m_strInstruction ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInstruction ) : '\'' . addslashes( $this->m_strInstruction ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, -1, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAction ) : '\'' . addslashes( $this->m_strAction ) . '\'' ) : 'NULL';
	}

	public function setInternalNote( $strInternalNote ) {
		$this->set( 'm_strInternalNote', CStrings::strTrimDef( $strInternalNote, -1, NULL, true ) );
	}

	public function getInternalNote() {
		return $this->m_strInternalNote;
	}

	public function sqlInternalNote() {
		return ( true == isset( $this->m_strInternalNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInternalNote ) : '\'' . addslashes( $this->m_strInternalNote ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 150, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setSettingLocation( $strSettingLocation ) {
		$this->set( 'm_strSettingLocation', CStrings::strTrimDef( $strSettingLocation, 150, NULL, true ) );
	}

	public function getSettingLocation() {
		return $this->m_strSettingLocation;
	}

	public function sqlSettingLocation() {
		return ( true == isset( $this->m_strSettingLocation ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSettingLocation ) : '\'' . addslashes( $this->m_strSettingLocation ) . '\'' ) : 'NULL';
	}

	public function setSourceLocation( $strSourceLocation ) {
		$this->set( 'm_strSourceLocation', CStrings::strTrimDef( $strSourceLocation, 150, NULL, true ) );
	}

	public function getSourceLocation() {
		return $this->m_strSourceLocation;
	}

	public function sqlSourceLocation() {
		return ( true == isset( $this->m_strSourceLocation ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSourceLocation ) : '\'' . addslashes( $this->m_strSourceLocation ) . '\'' ) : 'NULL';
	}

	public function setIsApproved( $boolIsApproved ) {
		$this->set( 'm_boolIsApproved', CStrings::strToBool( $boolIsApproved ) );
	}

	public function getIsApproved() {
		return $this->m_boolIsApproved;
	}

	public function sqlIsApproved() {
		return ( true == isset( $this->m_boolIsApproved ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsApproved ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsExternal( $boolIsExternal ) {
		$this->set( 'm_boolIsExternal', CStrings::strToBool( $boolIsExternal ) );
	}

	public function getIsExternal() {
		return $this->m_boolIsExternal;
	}

	public function sqlIsExternal() {
		return ( true == isset( $this->m_boolIsExternal ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsExternal ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, task_id, release_note_type_id, employee_id, pathway, instruction, description, action, internal_note, updated_by, updated_on, created_by, created_on, title, setting_location, source_location, is_approved, is_external, approved_by, approved_on )
					VALUES ( ' .
				  $strId . ', ' .
				  $this->sqlTaskId() . ', ' .
				  $this->sqlReleaseNoteTypeId() . ', ' .
				  $this->sqlEmployeeId() . ', ' .
				  $this->sqlPathway() . ', ' .
				  $this->sqlInstruction() . ', ' .
				  $this->sqlDescription() . ', ' .
				  $this->sqlAction() . ', ' .
				  $this->sqlInternalNote() . ', ' .
				  ( int ) $intCurrentUserId . ', ' .
				  $this->sqlUpdatedOn() . ', ' .
				  ( int ) $intCurrentUserId . ', ' .
				  $this->sqlCreatedOn() . ', ' .
				  $this->sqlTitle() . ', ' .
				  $this->sqlSettingLocation() . ', ' .
				  $this->sqlSourceLocation() . ', ' .
				  $this->sqlIsApproved() . ', ' .
				  $this->sqlIsExternal() . ', ' .
				  $this->sqlApprovedBy() . ', ' .
				  $this->sqlApprovedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId(). ',' ; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_note_type_id = ' . $this->sqlReleaseNoteTypeId(). ',' ; } elseif( true == array_key_exists( 'ReleaseNoteTypeId', $this->getChangedColumns() ) ) { $strSql .= ' release_note_type_id = ' . $this->sqlReleaseNoteTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pathway = ' . $this->sqlPathway(). ',' ; } elseif( true == array_key_exists( 'Pathway', $this->getChangedColumns() ) ) { $strSql .= ' pathway = ' . $this->sqlPathway() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instruction = ' . $this->sqlInstruction(). ',' ; } elseif( true == array_key_exists( 'Instruction', $this->getChangedColumns() ) ) { $strSql .= ' instruction = ' . $this->sqlInstruction() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action = ' . $this->sqlAction(). ',' ; } elseif( true == array_key_exists( 'Action', $this->getChangedColumns() ) ) { $strSql .= ' action = ' . $this->sqlAction() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_note = ' . $this->sqlInternalNote(). ',' ; } elseif( true == array_key_exists( 'InternalNote', $this->getChangedColumns() ) ) { $strSql .= ' internal_note = ' . $this->sqlInternalNote() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' setting_location = ' . $this->sqlSettingLocation(). ',' ; } elseif( true == array_key_exists( 'SettingLocation', $this->getChangedColumns() ) ) { $strSql .= ' setting_location = ' . $this->sqlSettingLocation() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_location = ' . $this->sqlSourceLocation(). ',' ; } elseif( true == array_key_exists( 'SourceLocation', $this->getChangedColumns() ) ) { $strSql .= ' source_location = ' . $this->sqlSourceLocation() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved(). ',' ; } elseif( true == array_key_exists( 'IsApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_external = ' . $this->sqlIsExternal(). ',' ; } elseif( true == array_key_exists( 'IsExternal', $this->getChangedColumns() ) ) { $strSql .= ' is_external = ' . $this->sqlIsExternal() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'task_id' => $this->getTaskId(),
			'release_note_type_id' => $this->getReleaseNoteTypeId(),
			'employee_id' => $this->getEmployeeId(),
			'pathway' => $this->getPathway(),
			'instruction' => $this->getInstruction(),
			'description' => $this->getDescription(),
			'action' => $this->getAction(),
			'internal_note' => $this->getInternalNote(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'title' => $this->getTitle(),
			'setting_location' => $this->getSettingLocation(),
			'source_location' => $this->getSourceLocation(),
			'is_approved' => $this->getIsApproved(),
			'is_external' => $this->getIsExternal(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn()
		);
	}

}
?>