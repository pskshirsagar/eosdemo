<?php

class CBaseS2NetboxApiRequestLog extends CEosSingularBase {

	const TABLE_NAME = 'public.s2_netbox_api_request_logs';

	protected $m_intId;
	protected $m_strApiCommand;
	protected $m_strRequestUrl;
	protected $m_strRequestData;
	protected $m_strResponseCode;
	protected $m_strResponseData;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['api_command'] ) && $boolDirectSet ) $this->set( 'm_strApiCommand', trim( stripcslashes( $arrValues['api_command'] ) ) ); elseif( isset( $arrValues['api_command'] ) ) $this->setApiCommand( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['api_command'] ) : $arrValues['api_command'] );
		if( isset( $arrValues['request_url'] ) && $boolDirectSet ) $this->set( 'm_strRequestUrl', trim( stripcslashes( $arrValues['request_url'] ) ) ); elseif( isset( $arrValues['request_url'] ) ) $this->setRequestUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['request_url'] ) : $arrValues['request_url'] );
		if( isset( $arrValues['request_data'] ) && $boolDirectSet ) $this->set( 'm_strRequestData', trim( $arrValues['request_data'] ) ); elseif( isset( $arrValues['request_data'] ) ) $this->setRequestData( $arrValues['request_data'] );
		if( isset( $arrValues['response_code'] ) && $boolDirectSet ) $this->set( 'm_strResponseCode', trim( stripcslashes( $arrValues['response_code'] ) ) ); elseif( isset( $arrValues['response_code'] ) ) $this->setResponseCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response_code'] ) : $arrValues['response_code'] );
		if( isset( $arrValues['response_data'] ) && $boolDirectSet ) $this->set( 'm_strResponseData', trim( $arrValues['response_data'] ) ); elseif( isset( $arrValues['response_data'] ) ) $this->setResponseData( $arrValues['response_data'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setApiCommand( $strApiCommand ) {
		$this->set( 'm_strApiCommand', CStrings::strTrimDef( $strApiCommand, 40, NULL, true ) );
	}

	public function getApiCommand() {
		return $this->m_strApiCommand;
	}

	public function sqlApiCommand() {
		return ( true == isset( $this->m_strApiCommand ) ) ? '\'' . addslashes( $this->m_strApiCommand ) . '\'' : 'NULL';
	}

	public function setRequestUrl( $strRequestUrl ) {
		$this->set( 'm_strRequestUrl', CStrings::strTrimDef( $strRequestUrl, 255, NULL, true ) );
	}

	public function getRequestUrl() {
		return $this->m_strRequestUrl;
	}

	public function sqlRequestUrl() {
		return ( true == isset( $this->m_strRequestUrl ) ) ? '\'' . addslashes( $this->m_strRequestUrl ) . '\'' : 'NULL';
	}

	public function setRequestData( $strRequestData ) {
		$this->set( 'm_strRequestData', CStrings::strTrimDef( $strRequestData, NULL, NULL, true ) );
	}

	public function getRequestData() {
		return $this->m_strRequestData;
	}

	public function sqlRequestData() {
		return ( true == isset( $this->m_strRequestData ) ) ? '\'' . addslashes( $this->m_strRequestData ) . '\'' : 'NULL';
	}

	public function setResponseCode( $strResponseCode ) {
		$this->set( 'm_strResponseCode', CStrings::strTrimDef( $strResponseCode, 10, NULL, true ) );
	}

	public function getResponseCode() {
		return $this->m_strResponseCode;
	}

	public function sqlResponseCode() {
		return ( true == isset( $this->m_strResponseCode ) ) ? '\'' . addslashes( $this->m_strResponseCode ) . '\'' : 'NULL';
	}

	public function setResponseData( $strResponseData ) {
		$this->set( 'm_strResponseData', CStrings::strTrimDef( $strResponseData, NULL, NULL, true ) );
	}

	public function getResponseData() {
		return $this->m_strResponseData;
	}

	public function sqlResponseData() {
		return ( true == isset( $this->m_strResponseData ) ) ? '\'' . addslashes( $this->m_strResponseData ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, api_command, request_url, request_data, response_code, response_data, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlApiCommand() . ', ' .
 						$this->sqlRequestUrl() . ', ' .
 						$this->sqlRequestData() . ', ' .
 						$this->sqlResponseCode() . ', ' .
 						$this->sqlResponseData() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' api_command = ' . $this->sqlApiCommand() . ','; } elseif( true == array_key_exists( 'ApiCommand', $this->getChangedColumns() ) ) { $strSql .= ' api_command = ' . $this->sqlApiCommand() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_url = ' . $this->sqlRequestUrl() . ','; } elseif( true == array_key_exists( 'RequestUrl', $this->getChangedColumns() ) ) { $strSql .= ' request_url = ' . $this->sqlRequestUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_data = ' . $this->sqlRequestData() . ','; } elseif( true == array_key_exists( 'RequestData', $this->getChangedColumns() ) ) { $strSql .= ' request_data = ' . $this->sqlRequestData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_code = ' . $this->sqlResponseCode() . ','; } elseif( true == array_key_exists( 'ResponseCode', $this->getChangedColumns() ) ) { $strSql .= ' response_code = ' . $this->sqlResponseCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_data = ' . $this->sqlResponseData() . ','; } elseif( true == array_key_exists( 'ResponseData', $this->getChangedColumns() ) ) { $strSql .= ' response_data = ' . $this->sqlResponseData() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'api_command' => $this->getApiCommand(),
			'request_url' => $this->getRequestUrl(),
			'request_data' => $this->getRequestData(),
			'response_code' => $this->getResponseCode(),
			'response_data' => $this->getResponseData(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>