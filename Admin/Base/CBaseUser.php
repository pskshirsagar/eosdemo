<?php

class CBaseUser extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.users';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_strActiveDirectoryGuid;
	protected $m_strUsername;
	protected $m_strPasswordEncrypted;
	protected $m_strSvnUsername;
	protected $m_strSvnPassword;
	protected $m_intWebServicesOnly;
	protected $m_intIsAdministrator;
	protected $m_intIsSuperUser;
	protected $m_intIsDisabled;
	protected $m_intIsXmppSyncRequired;
	protected $m_strLastLogin;
	protected $m_strLastAccess;
	protected $m_intLoginAttemptCount;
	protected $m_strLastLoginAttemptOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intWebServicesOnly = '0';
		$this->m_intIsAdministrator = '0';
		$this->m_intIsSuperUser = '0';
		$this->m_intIsDisabled = '0';
		$this->m_intIsXmppSyncRequired = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['active_directory_guid'] ) && $boolDirectSet ) $this->set( 'm_strActiveDirectoryGuid', trim( stripcslashes( $arrValues['active_directory_guid'] ) ) ); elseif( isset( $arrValues['active_directory_guid'] ) ) $this->setActiveDirectoryGuid( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['active_directory_guid'] ) : $arrValues['active_directory_guid'] );
		if( isset( $arrValues['username'] ) && $boolDirectSet ) $this->set( 'm_strUsername', trim( stripcslashes( $arrValues['username'] ) ) ); elseif( isset( $arrValues['username'] ) ) $this->setUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['username'] ) : $arrValues['username'] );
		if( isset( $arrValues['password_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPasswordEncrypted', trim( stripcslashes( $arrValues['password_encrypted'] ) ) ); elseif( isset( $arrValues['password_encrypted'] ) ) $this->setPasswordEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['password_encrypted'] ) : $arrValues['password_encrypted'] );
		if( isset( $arrValues['svn_username'] ) && $boolDirectSet ) $this->set( 'm_strSvnUsername', trim( stripcslashes( $arrValues['svn_username'] ) ) ); elseif( isset( $arrValues['svn_username'] ) ) $this->setSvnUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['svn_username'] ) : $arrValues['svn_username'] );
		if( isset( $arrValues['svn_password'] ) && $boolDirectSet ) $this->set( 'm_strSvnPassword', trim( stripcslashes( $arrValues['svn_password'] ) ) ); elseif( isset( $arrValues['svn_password'] ) ) $this->setSvnPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['svn_password'] ) : $arrValues['svn_password'] );
		if( isset( $arrValues['web_services_only'] ) && $boolDirectSet ) $this->set( 'm_intWebServicesOnly', trim( $arrValues['web_services_only'] ) ); elseif( isset( $arrValues['web_services_only'] ) ) $this->setWebServicesOnly( $arrValues['web_services_only'] );
		if( isset( $arrValues['is_administrator'] ) && $boolDirectSet ) $this->set( 'm_intIsAdministrator', trim( $arrValues['is_administrator'] ) ); elseif( isset( $arrValues['is_administrator'] ) ) $this->setIsAdministrator( $arrValues['is_administrator'] );
		if( isset( $arrValues['is_super_user'] ) && $boolDirectSet ) $this->set( 'm_intIsSuperUser', trim( $arrValues['is_super_user'] ) ); elseif( isset( $arrValues['is_super_user'] ) ) $this->setIsSuperUser( $arrValues['is_super_user'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['is_xmpp_sync_required'] ) && $boolDirectSet ) $this->set( 'm_intIsXmppSyncRequired', trim( $arrValues['is_xmpp_sync_required'] ) ); elseif( isset( $arrValues['is_xmpp_sync_required'] ) ) $this->setIsXmppSyncRequired( $arrValues['is_xmpp_sync_required'] );
		if( isset( $arrValues['last_login'] ) && $boolDirectSet ) $this->set( 'm_strLastLogin', trim( $arrValues['last_login'] ) ); elseif( isset( $arrValues['last_login'] ) ) $this->setLastLogin( $arrValues['last_login'] );
		if( isset( $arrValues['last_access'] ) && $boolDirectSet ) $this->set( 'm_strLastAccess', trim( $arrValues['last_access'] ) ); elseif( isset( $arrValues['last_access'] ) ) $this->setLastAccess( $arrValues['last_access'] );
		if( isset( $arrValues['login_attempt_count'] ) && $boolDirectSet ) $this->set( 'm_intLoginAttemptCount', trim( $arrValues['login_attempt_count'] ) ); elseif( isset( $arrValues['login_attempt_count'] ) ) $this->setLoginAttemptCount( $arrValues['login_attempt_count'] );
		if( isset( $arrValues['last_login_attempt_on'] ) && $boolDirectSet ) $this->set( 'm_strLastLoginAttemptOn', trim( $arrValues['last_login_attempt_on'] ) ); elseif( isset( $arrValues['last_login_attempt_on'] ) ) $this->setLastLoginAttemptOn( $arrValues['last_login_attempt_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setActiveDirectoryGuid( $strActiveDirectoryGuid ) {
		$this->set( 'm_strActiveDirectoryGuid', CStrings::strTrimDef( $strActiveDirectoryGuid, 50, NULL, true ) );
	}

	public function getActiveDirectoryGuid() {
		return $this->m_strActiveDirectoryGuid;
	}

	public function sqlActiveDirectoryGuid() {
		return ( true == isset( $this->m_strActiveDirectoryGuid ) ) ? '\'' . addslashes( $this->m_strActiveDirectoryGuid ) . '\'' : 'NULL';
	}

	public function setUsername( $strUsername ) {
		$this->set( 'm_strUsername', CStrings::strTrimDef( $strUsername, 240, NULL, true ) );
	}

	public function getUsername() {
		return $this->m_strUsername;
	}

	public function sqlUsername() {
		return ( true == isset( $this->m_strUsername ) ) ? '\'' . addslashes( $this->m_strUsername ) . '\'' : 'NULL';
	}

	public function setPasswordEncrypted( $strPasswordEncrypted ) {
		$this->set( 'm_strPasswordEncrypted', CStrings::strTrimDef( $strPasswordEncrypted, 240, NULL, true ) );
	}

	public function getPasswordEncrypted() {
		return $this->m_strPasswordEncrypted;
	}

	public function sqlPasswordEncrypted() {
		return ( true == isset( $this->m_strPasswordEncrypted ) ) ? '\'' . addslashes( $this->m_strPasswordEncrypted ) . '\'' : 'NULL';
	}

	public function setSvnUsername( $strSvnUsername ) {
		$this->set( 'm_strSvnUsername', CStrings::strTrimDef( $strSvnUsername, 240, NULL, true ) );
	}

	public function getSvnUsername() {
		return $this->m_strSvnUsername;
	}

	public function sqlSvnUsername() {
		return ( true == isset( $this->m_strSvnUsername ) ) ? '\'' . addslashes( $this->m_strSvnUsername ) . '\'' : 'NULL';
	}

	public function setSvnPassword( $strSvnPassword ) {
		$this->set( 'm_strSvnPassword', CStrings::strTrimDef( $strSvnPassword, 240, NULL, true ) );
	}

	public function getSvnPassword() {
		return $this->m_strSvnPassword;
	}

	public function sqlSvnPassword() {
		return ( true == isset( $this->m_strSvnPassword ) ) ? '\'' . addslashes( $this->m_strSvnPassword ) . '\'' : 'NULL';
	}

	public function setWebServicesOnly( $intWebServicesOnly ) {
		$this->set( 'm_intWebServicesOnly', CStrings::strToIntDef( $intWebServicesOnly, NULL, false ) );
	}

	public function getWebServicesOnly() {
		return $this->m_intWebServicesOnly;
	}

	public function sqlWebServicesOnly() {
		return ( true == isset( $this->m_intWebServicesOnly ) ) ? ( string ) $this->m_intWebServicesOnly : '0';
	}

	public function setIsAdministrator( $intIsAdministrator ) {
		$this->set( 'm_intIsAdministrator', CStrings::strToIntDef( $intIsAdministrator, NULL, false ) );
	}

	public function getIsAdministrator() {
		return $this->m_intIsAdministrator;
	}

	public function sqlIsAdministrator() {
		return ( true == isset( $this->m_intIsAdministrator ) ) ? ( string ) $this->m_intIsAdministrator : '0';
	}

	public function setIsSuperUser( $intIsSuperUser ) {
		$this->set( 'm_intIsSuperUser', CStrings::strToIntDef( $intIsSuperUser, NULL, false ) );
	}

	public function getIsSuperUser() {
		return $this->m_intIsSuperUser;
	}

	public function sqlIsSuperUser() {
		return ( true == isset( $this->m_intIsSuperUser ) ) ? ( string ) $this->m_intIsSuperUser : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setIsXmppSyncRequired( $intIsXmppSyncRequired ) {
		$this->set( 'm_intIsXmppSyncRequired', CStrings::strToIntDef( $intIsXmppSyncRequired, NULL, false ) );
	}

	public function getIsXmppSyncRequired() {
		return $this->m_intIsXmppSyncRequired;
	}

	public function sqlIsXmppSyncRequired() {
		return ( true == isset( $this->m_intIsXmppSyncRequired ) ) ? ( string ) $this->m_intIsXmppSyncRequired : '0';
	}

	public function setLastLogin( $strLastLogin ) {
		$this->set( 'm_strLastLogin', CStrings::strTrimDef( $strLastLogin, -1, NULL, true ) );
	}

	public function getLastLogin() {
		return $this->m_strLastLogin;
	}

	public function sqlLastLogin() {
		return ( true == isset( $this->m_strLastLogin ) ) ? '\'' . $this->m_strLastLogin . '\'' : 'NULL';
	}

	public function setLastAccess( $strLastAccess ) {
		$this->set( 'm_strLastAccess', CStrings::strTrimDef( $strLastAccess, -1, NULL, true ) );
	}

	public function getLastAccess() {
		return $this->m_strLastAccess;
	}

	public function sqlLastAccess() {
		return ( true == isset( $this->m_strLastAccess ) ) ? '\'' . $this->m_strLastAccess . '\'' : 'NULL';
	}

	public function setLoginAttemptCount( $intLoginAttemptCount ) {
		$this->set( 'm_intLoginAttemptCount', CStrings::strToIntDef( $intLoginAttemptCount, NULL, false ) );
	}

	public function getLoginAttemptCount() {
		return $this->m_intLoginAttemptCount;
	}

	public function sqlLoginAttemptCount() {
		return ( true == isset( $this->m_intLoginAttemptCount ) ) ? ( string ) $this->m_intLoginAttemptCount : 'NULL';
	}

	public function setLastLoginAttemptOn( $strLastLoginAttemptOn ) {
		$this->set( 'm_strLastLoginAttemptOn', CStrings::strTrimDef( $strLastLoginAttemptOn, -1, NULL, true ) );
	}

	public function getLastLoginAttemptOn() {
		return $this->m_strLastLoginAttemptOn;
	}

	public function sqlLastLoginAttemptOn() {
		return ( true == isset( $this->m_strLastLoginAttemptOn ) ) ? '\'' . $this->m_strLastLoginAttemptOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, active_directory_guid, username, password_encrypted, svn_username, svn_password, web_services_only, is_administrator, is_super_user, is_disabled, is_xmpp_sync_required, last_login, last_access, login_attempt_count, last_login_attempt_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlActiveDirectoryGuid() . ', ' .
						$this->sqlUsername() . ', ' .
						$this->sqlPasswordEncrypted() . ', ' .
						$this->sqlSvnUsername() . ', ' .
						$this->sqlSvnPassword() . ', ' .
						$this->sqlWebServicesOnly() . ', ' .
						$this->sqlIsAdministrator() . ', ' .
						$this->sqlIsSuperUser() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlIsXmppSyncRequired() . ', ' .
						$this->sqlLastLogin() . ', ' .
						$this->sqlLastAccess() . ', ' .
						$this->sqlLoginAttemptCount() . ', ' .
						$this->sqlLastLoginAttemptOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' active_directory_guid = ' . $this->sqlActiveDirectoryGuid(). ',' ; } elseif( true == array_key_exists( 'ActiveDirectoryGuid', $this->getChangedColumns() ) ) { $strSql .= ' active_directory_guid = ' . $this->sqlActiveDirectoryGuid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' username = ' . $this->sqlUsername(). ',' ; } elseif( true == array_key_exists( 'Username', $this->getChangedColumns() ) ) { $strSql .= ' username = ' . $this->sqlUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted(). ',' ; } elseif( true == array_key_exists( 'PasswordEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' password_encrypted = ' . $this->sqlPasswordEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_username = ' . $this->sqlSvnUsername(). ',' ; } elseif( true == array_key_exists( 'SvnUsername', $this->getChangedColumns() ) ) { $strSql .= ' svn_username = ' . $this->sqlSvnUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_password = ' . $this->sqlSvnPassword(). ',' ; } elseif( true == array_key_exists( 'SvnPassword', $this->getChangedColumns() ) ) { $strSql .= ' svn_password = ' . $this->sqlSvnPassword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_services_only = ' . $this->sqlWebServicesOnly(). ',' ; } elseif( true == array_key_exists( 'WebServicesOnly', $this->getChangedColumns() ) ) { $strSql .= ' web_services_only = ' . $this->sqlWebServicesOnly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_administrator = ' . $this->sqlIsAdministrator(). ',' ; } elseif( true == array_key_exists( 'IsAdministrator', $this->getChangedColumns() ) ) { $strSql .= ' is_administrator = ' . $this->sqlIsAdministrator() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_super_user = ' . $this->sqlIsSuperUser(). ',' ; } elseif( true == array_key_exists( 'IsSuperUser', $this->getChangedColumns() ) ) { $strSql .= ' is_super_user = ' . $this->sqlIsSuperUser() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_xmpp_sync_required = ' . $this->sqlIsXmppSyncRequired(). ',' ; } elseif( true == array_key_exists( 'IsXmppSyncRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_xmpp_sync_required = ' . $this->sqlIsXmppSyncRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_login = ' . $this->sqlLastLogin(). ',' ; } elseif( true == array_key_exists( 'LastLogin', $this->getChangedColumns() ) ) { $strSql .= ' last_login = ' . $this->sqlLastLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_access = ' . $this->sqlLastAccess(). ',' ; } elseif( true == array_key_exists( 'LastAccess', $this->getChangedColumns() ) ) { $strSql .= ' last_access = ' . $this->sqlLastAccess() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_attempt_count = ' . $this->sqlLoginAttemptCount(). ',' ; } elseif( true == array_key_exists( 'LoginAttemptCount', $this->getChangedColumns() ) ) { $strSql .= ' login_attempt_count = ' . $this->sqlLoginAttemptCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_login_attempt_on = ' . $this->sqlLastLoginAttemptOn(). ',' ; } elseif( true == array_key_exists( 'LastLoginAttemptOn', $this->getChangedColumns() ) ) { $strSql .= ' last_login_attempt_on = ' . $this->sqlLastLoginAttemptOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'active_directory_guid' => $this->getActiveDirectoryGuid(),
			'username' => $this->getUsername(),
			'password_encrypted' => $this->getPasswordEncrypted(),
			'svn_username' => $this->getSvnUsername(),
			'svn_password' => $this->getSvnPassword(),
			'web_services_only' => $this->getWebServicesOnly(),
			'is_administrator' => $this->getIsAdministrator(),
			'is_super_user' => $this->getIsSuperUser(),
			'is_disabled' => $this->getIsDisabled(),
			'is_xmpp_sync_required' => $this->getIsXmppSyncRequired(),
			'last_login' => $this->getLastLogin(),
			'last_access' => $this->getLastAccess(),
			'login_attempt_count' => $this->getLoginAttemptCount(),
			'last_login_attempt_on' => $this->getLastLoginAttemptOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>