<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskTypeStatuses
 * Do not add any new functions to this class.
 */

class CBaseTaskTypeStatuses extends CEosPluralBase {

	/**
	 * @return CTaskTypeStatus[]
	 */
	public static function fetchTaskTypeStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskTypeStatus', $objDatabase );
	}

	/**
	 * @return CTaskTypeStatus
	 */
	public static function fetchTaskTypeStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskTypeStatus', $objDatabase );
	}

	public static function fetchTaskTypeStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_type_statuses', $objDatabase );
	}

	public static function fetchTaskTypeStatusById( $intId, $objDatabase ) {
		return self::fetchTaskTypeStatus( sprintf( 'SELECT * FROM task_type_statuses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskTypeStatusesByTaskTypeId( $intTaskTypeId, $objDatabase ) {
		return self::fetchTaskTypeStatuses( sprintf( 'SELECT * FROM task_type_statuses WHERE task_type_id = %d', ( int ) $intTaskTypeId ), $objDatabase );
	}

	public static function fetchTaskTypeStatusesByTaskStatusId( $intTaskStatusId, $objDatabase ) {
		return self::fetchTaskTypeStatuses( sprintf( 'SELECT * FROM task_type_statuses WHERE task_status_id = %d', ( int ) $intTaskStatusId ), $objDatabase );
	}

}
?>