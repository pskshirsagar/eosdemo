<?php

class CBasePsLeadCompetitor extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_lead_competitors';

	protected $m_intId;
	protected $m_intPsLeadId;
	protected $m_intPsProductId;
	protected $m_intCompetitorId;
	protected $m_intPersonId;
	protected $m_intUnitsContracted;
	protected $m_intServiceYears;
	protected $m_fltInvoiceAmount;
	protected $m_strNotes;
	protected $m_strContractExpiresOn;
	protected $m_intIsNotInterested;
	protected $m_boolAllProperties;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltInvoiceAmount = '0';
		$this->m_intIsNotInterested = '0';
		$this->m_boolAllProperties = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['competitor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorId', trim( $arrValues['competitor_id'] ) ); elseif( isset( $arrValues['competitor_id'] ) ) $this->setCompetitorId( $arrValues['competitor_id'] );
		if( isset( $arrValues['person_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonId', trim( $arrValues['person_id'] ) ); elseif( isset( $arrValues['person_id'] ) ) $this->setPersonId( $arrValues['person_id'] );
		if( isset( $arrValues['units_contracted'] ) && $boolDirectSet ) $this->set( 'm_intUnitsContracted', trim( $arrValues['units_contracted'] ) ); elseif( isset( $arrValues['units_contracted'] ) ) $this->setUnitsContracted( $arrValues['units_contracted'] );
		if( isset( $arrValues['service_years'] ) && $boolDirectSet ) $this->set( 'm_intServiceYears', trim( $arrValues['service_years'] ) ); elseif( isset( $arrValues['service_years'] ) ) $this->setServiceYears( $arrValues['service_years'] );
		if( isset( $arrValues['invoice_amount'] ) && $boolDirectSet ) $this->set( 'm_fltInvoiceAmount', trim( $arrValues['invoice_amount'] ) ); elseif( isset( $arrValues['invoice_amount'] ) ) $this->setInvoiceAmount( $arrValues['invoice_amount'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['contract_expires_on'] ) && $boolDirectSet ) $this->set( 'm_strContractExpiresOn', trim( $arrValues['contract_expires_on'] ) ); elseif( isset( $arrValues['contract_expires_on'] ) ) $this->setContractExpiresOn( $arrValues['contract_expires_on'] );
		if( isset( $arrValues['is_not_interested'] ) && $boolDirectSet ) $this->set( 'm_intIsNotInterested', trim( $arrValues['is_not_interested'] ) ); elseif( isset( $arrValues['is_not_interested'] ) ) $this->setIsNotInterested( $arrValues['is_not_interested'] );
		if( isset( $arrValues['all_properties'] ) && $boolDirectSet ) $this->set( 'm_boolAllProperties', trim( stripcslashes( $arrValues['all_properties'] ) ) ); elseif( isset( $arrValues['all_properties'] ) ) $this->setAllProperties( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['all_properties'] ) : $arrValues['all_properties'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setCompetitorId( $intCompetitorId ) {
		$this->set( 'm_intCompetitorId', CStrings::strToIntDef( $intCompetitorId, NULL, false ) );
	}

	public function getCompetitorId() {
		return $this->m_intCompetitorId;
	}

	public function sqlCompetitorId() {
		return ( true == isset( $this->m_intCompetitorId ) ) ? ( string ) $this->m_intCompetitorId : 'NULL';
	}

	public function setPersonId( $intPersonId ) {
		$this->set( 'm_intPersonId', CStrings::strToIntDef( $intPersonId, NULL, false ) );
	}

	public function getPersonId() {
		return $this->m_intPersonId;
	}

	public function sqlPersonId() {
		return ( true == isset( $this->m_intPersonId ) ) ? ( string ) $this->m_intPersonId : 'NULL';
	}

	public function setUnitsContracted( $intUnitsContracted ) {
		$this->set( 'm_intUnitsContracted', CStrings::strToIntDef( $intUnitsContracted, NULL, false ) );
	}

	public function getUnitsContracted() {
		return $this->m_intUnitsContracted;
	}

	public function sqlUnitsContracted() {
		return ( true == isset( $this->m_intUnitsContracted ) ) ? ( string ) $this->m_intUnitsContracted : 'NULL';
	}

	public function setServiceYears( $intServiceYears ) {
		$this->set( 'm_intServiceYears', CStrings::strToIntDef( $intServiceYears, NULL, false ) );
	}

	public function getServiceYears() {
		return $this->m_intServiceYears;
	}

	public function sqlServiceYears() {
		return ( true == isset( $this->m_intServiceYears ) ) ? ( string ) $this->m_intServiceYears : 'NULL';
	}

	public function setInvoiceAmount( $fltInvoiceAmount ) {
		$this->set( 'm_fltInvoiceAmount', CStrings::strToFloatDef( $fltInvoiceAmount, NULL, false, 2 ) );
	}

	public function getInvoiceAmount() {
		return $this->m_fltInvoiceAmount;
	}

	public function sqlInvoiceAmount() {
		return ( true == isset( $this->m_fltInvoiceAmount ) ) ? ( string ) $this->m_fltInvoiceAmount : '0';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setContractExpiresOn( $strContractExpiresOn ) {
		$this->set( 'm_strContractExpiresOn', CStrings::strTrimDef( $strContractExpiresOn, -1, NULL, true ) );
	}

	public function getContractExpiresOn() {
		return $this->m_strContractExpiresOn;
	}

	public function sqlContractExpiresOn() {
		return ( true == isset( $this->m_strContractExpiresOn ) ) ? '\'' . $this->m_strContractExpiresOn . '\'' : 'NULL';
	}

	public function setIsNotInterested( $intIsNotInterested ) {
		$this->set( 'm_intIsNotInterested', CStrings::strToIntDef( $intIsNotInterested, NULL, false ) );
	}

	public function getIsNotInterested() {
		return $this->m_intIsNotInterested;
	}

	public function sqlIsNotInterested() {
		return ( true == isset( $this->m_intIsNotInterested ) ) ? ( string ) $this->m_intIsNotInterested : '0';
	}

	public function setAllProperties( $boolAllProperties ) {
		$this->set( 'm_boolAllProperties', CStrings::strToBool( $boolAllProperties ) );
	}

	public function getAllProperties() {
		return $this->m_boolAllProperties;
	}

	public function sqlAllProperties() {
		return ( true == isset( $this->m_boolAllProperties ) ) ? '\'' . ( true == ( bool ) $this->m_boolAllProperties ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_lead_id, ps_product_id, competitor_id, person_id, units_contracted, service_years, invoice_amount, notes, contract_expires_on, is_not_interested, all_properties, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsLeadId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlCompetitorId() . ', ' .
 						$this->sqlPersonId() . ', ' .
 						$this->sqlUnitsContracted() . ', ' .
 						$this->sqlServiceYears() . ', ' .
 						$this->sqlInvoiceAmount() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlContractExpiresOn() . ', ' .
 						$this->sqlIsNotInterested() . ', ' .
 						$this->sqlAllProperties() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId() . ','; } elseif( true == array_key_exists( 'CompetitorId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' person_id = ' . $this->sqlPersonId() . ','; } elseif( true == array_key_exists( 'PersonId', $this->getChangedColumns() ) ) { $strSql .= ' person_id = ' . $this->sqlPersonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' units_contracted = ' . $this->sqlUnitsContracted() . ','; } elseif( true == array_key_exists( 'UnitsContracted', $this->getChangedColumns() ) ) { $strSql .= ' units_contracted = ' . $this->sqlUnitsContracted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_years = ' . $this->sqlServiceYears() . ','; } elseif( true == array_key_exists( 'ServiceYears', $this->getChangedColumns() ) ) { $strSql .= ' service_years = ' . $this->sqlServiceYears() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_amount = ' . $this->sqlInvoiceAmount() . ','; } elseif( true == array_key_exists( 'InvoiceAmount', $this->getChangedColumns() ) ) { $strSql .= ' invoice_amount = ' . $this->sqlInvoiceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_expires_on = ' . $this->sqlContractExpiresOn() . ','; } elseif( true == array_key_exists( 'ContractExpiresOn', $this->getChangedColumns() ) ) { $strSql .= ' contract_expires_on = ' . $this->sqlContractExpiresOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_not_interested = ' . $this->sqlIsNotInterested() . ','; } elseif( true == array_key_exists( 'IsNotInterested', $this->getChangedColumns() ) ) { $strSql .= ' is_not_interested = ' . $this->sqlIsNotInterested() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' all_properties = ' . $this->sqlAllProperties() . ','; } elseif( true == array_key_exists( 'AllProperties', $this->getChangedColumns() ) ) { $strSql .= ' all_properties = ' . $this->sqlAllProperties() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'ps_product_id' => $this->getPsProductId(),
			'competitor_id' => $this->getCompetitorId(),
			'person_id' => $this->getPersonId(),
			'units_contracted' => $this->getUnitsContracted(),
			'service_years' => $this->getServiceYears(),
			'invoice_amount' => $this->getInvoiceAmount(),
			'notes' => $this->getNotes(),
			'contract_expires_on' => $this->getContractExpiresOn(),
			'is_not_interested' => $this->getIsNotInterested(),
			'all_properties' => $this->getAllProperties(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>