<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsEmployeePerformances
 * Do not add any new functions to this class.
 */

class CBaseStatsEmployeePerformances extends CEosPluralBase {

	/**
	 * @return CStatsEmployeePerformance[]
	 */
	public static function fetchStatsEmployeePerformances( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStatsEmployeePerformance', $objDatabase );
	}

	/**
	 * @return CStatsEmployeePerformance
	 */
	public static function fetchStatsEmployeePerformance( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStatsEmployeePerformance', $objDatabase );
	}

	public static function fetchStatsEmployeePerformanceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stats_employee_performances', $objDatabase );
	}

	public static function fetchStatsEmployeePerformanceById( $intId, $objDatabase ) {
		return self::fetchStatsEmployeePerformance( sprintf( 'SELECT * FROM stats_employee_performances WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStatsEmployeePerformancesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchStatsEmployeePerformances( sprintf( 'SELECT * FROM stats_employee_performances WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>