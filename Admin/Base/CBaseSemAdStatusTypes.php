<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseSemAdStatusTypes extends CEosPluralBase {

	/**
	 * @return CSemAdStatusType[]
	 */
	public static function fetchSemAdStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemAdStatusType', $objDatabase );
	}

	/**
	 * @return CSemAdStatusType
	 */
	public static function fetchSemAdStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemAdStatusType', $objDatabase );
	}

	public static function fetchSemAdStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_ad_status_types', $objDatabase );
	}

	public static function fetchSemAdStatusTypeById( $intId, $objDatabase ) {
		return self::fetchSemAdStatusType( sprintf( 'SELECT * FROM sem_ad_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>