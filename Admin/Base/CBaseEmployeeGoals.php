<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeGoals
 * Do not add any new functions to this class.
 */

class CBaseEmployeeGoals extends CEosPluralBase {

	/**
	 * @return CEmployeeGoal[]
	 */
	public static function fetchEmployeeGoals( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeGoal::class, $objDatabase );
	}

	/**
	 * @return CEmployeeGoal
	 */
	public static function fetchEmployeeGoal( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeGoal::class, $objDatabase );
	}

	public static function fetchEmployeeGoalCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_goals', $objDatabase );
	}

	public static function fetchEmployeeGoalById( $intId, $objDatabase ) {
		return self::fetchEmployeeGoal( sprintf( 'SELECT * FROM employee_goals WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchEmployeeGoalsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeGoals( sprintf( 'SELECT * FROM employee_goals WHERE employee_id = %d', $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeGoalsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchEmployeeGoals( sprintf( 'SELECT * FROM employee_goals WHERE call_agent_id = %d', $intCallAgentId ), $objDatabase );
	}

	public static function fetchEmployeeGoalsByCallAgentEmployeeId( $intCallAgentEmployeeId, $objDatabase ) {
		return self::fetchEmployeeGoals( sprintf( 'SELECT * FROM employee_goals WHERE call_agent_employee_id = %d', $intCallAgentEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeGoalsByEmployeeGoalTypeId( $intEmployeeGoalTypeId, $objDatabase ) {
		return self::fetchEmployeeGoals( sprintf( 'SELECT * FROM employee_goals WHERE employee_goal_type_id = %d', $intEmployeeGoalTypeId ), $objDatabase );
	}

	public static function fetchEmployeeGoalsByEmployeeInteractionId( $intEmployeeInteractionId, $objDatabase ) {
		return self::fetchEmployeeGoals( sprintf( 'SELECT * FROM employee_goals WHERE employee_interaction_id = %d', $intEmployeeInteractionId ), $objDatabase );
	}

}
?>