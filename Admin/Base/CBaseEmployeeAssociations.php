<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAssociations
 * Do not add any new functions to this class.
 */

class CBaseEmployeeAssociations extends CEosPluralBase {

	/**
	 * @return CEmployeeAssociation[]
	 */
	public static function fetchEmployeeAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeAssociation', $objDatabase );
	}

	/**
	 * @return CEmployeeAssociation
	 */
	public static function fetchEmployeeAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeAssociation', $objDatabase );
	}

	public static function fetchEmployeeAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_associations', $objDatabase );
	}

	public static function fetchEmployeeAssociationById( $intId, $objDatabase ) {
		return self::fetchEmployeeAssociation( sprintf( 'SELECT * FROM employee_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeAssociationsByPsLeadId( $intPsLeadId, $objDatabase ) {
		return self::fetchEmployeeAssociations( sprintf( 'SELECT * FROM employee_associations WHERE ps_lead_id = %d', ( int ) $intPsLeadId ), $objDatabase );
	}

	public static function fetchEmployeeAssociationsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeAssociations( sprintf( 'SELECT * FROM employee_associations WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeAssociationsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchEmployeeAssociations( sprintf( 'SELECT * FROM employee_associations WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>