<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyValueNominations
 * Do not add any new functions to this class.
 */

class CBaseCompanyValueNominations extends CEosPluralBase {

	/**
	 * @return CCompanyValueNomination[]
	 */
	public static function fetchCompanyValueNominations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyValueNomination::class, $objDatabase );
	}

	/**
	 * @return CCompanyValueNomination
	 */
	public static function fetchCompanyValueNomination( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyValueNomination::class, $objDatabase );
	}

	public static function fetchCompanyValueNominationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_value_nominations', $objDatabase );
	}

	public static function fetchCompanyValueNominationById( $intId, $objDatabase ) {
		return self::fetchCompanyValueNomination( sprintf( 'SELECT * FROM company_value_nominations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCompanyValueNominationsByCompanyValueId( $strCompanyValueId, $objDatabase ) {
		return self::fetchCompanyValueNominations( sprintf( 'SELECT * FROM company_value_nominations WHERE company_value_id = \'%s\'', $strCompanyValueId ), $objDatabase );
	}

	public static function fetchCompanyValueNominationsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchCompanyValueNominations( sprintf( 'SELECT * FROM company_value_nominations WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchCompanyValueNominationsByNominatingEmployeeId( $intNominatingEmployeeId, $objDatabase ) {
		return self::fetchCompanyValueNominations( sprintf( 'SELECT * FROM company_value_nominations WHERE nominating_employee_id = %d', ( int ) $intNominatingEmployeeId ), $objDatabase );
	}

	public static function fetchCompanyValueNominationsByCompanyValueNominationBatchId( $intCompanyValueNominationBatchId, $objDatabase ) {
		return self::fetchCompanyValueNominations( sprintf( 'SELECT * FROM company_value_nominations WHERE company_value_nomination_batch_id = %d', ( int ) $intCompanyValueNominationBatchId ), $objDatabase );
	}

}
?>