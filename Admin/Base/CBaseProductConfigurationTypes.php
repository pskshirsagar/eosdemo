<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CProductConfigurationTypes
 * Do not add any new functions to this class.
 */

class CBaseProductConfigurationTypes extends CEosPluralBase {

	/**
	 * @return CProductConfigurationType[]
	 */
	public static function fetchProductConfigurationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CProductConfigurationType', $objDatabase );
	}

	/**
	 * @return CProductConfigurationType
	 */
	public static function fetchProductConfigurationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CProductConfigurationType', $objDatabase );
	}

	public static function fetchProductConfigurationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'product_configuration_types', $objDatabase );
	}

	public static function fetchProductConfigurationTypeById( $intId, $objDatabase ) {
		return self::fetchProductConfigurationType( sprintf( 'SELECT * FROM product_configuration_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchProductConfigurationTypesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchProductConfigurationTypes( sprintf( 'SELECT * FROM product_configuration_types WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>