<?php

class CBaseTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.transactions';

	protected $m_intId;
	protected $m_intEntityId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intPropertyId;
	protected $m_intDepositId;
	protected $m_strCurrencyCode;
	protected $m_intCompanyChargeId;
	protected $m_intTransactionId;
	protected $m_intChargeCodeId;
	protected $m_intCompanyPaymentId;
	protected $m_intInvoiceId;
	protected $m_intExportBatchId;
	protected $m_intContractPropertyId;
	protected $m_intBundlePsProductId;
	protected $m_intTransactionReversalTypeId;
	protected $m_intDeferredExportBatchId;
	protected $m_intReferenceNumber;
	protected $m_strTransactionDatetime;
	protected $m_fltTransactionAmount;
	protected $m_fltCostAmount;
	protected $m_fltTransactionDueAmount;
	protected $m_intItemCount;
	protected $m_strMemo;
	protected $m_strInternalNotes;
	protected $m_intIsCommissioned;
	protected $m_strPostMonth;
	protected $m_strTransactionPaidDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intContractChainId;
	protected $m_intPsProductId;
	protected $m_intRevenueTypeId;
	protected $m_strBillingPeriod;
	protected $m_strAccountingPeriod;
	protected $m_fltRevenueAmount;
	protected $m_intReferenceTransactionId;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';
		$this->m_fltCostAmount = '0';
		$this->m_fltTransactionDueAmount = '0';
		$this->m_intItemCount = '0';
		$this->m_intIsCommissioned = '0';
		$this->m_fltRevenueAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['entity_id'] ) && $boolDirectSet ) $this->set( 'm_intEntityId', trim( $arrValues['entity_id'] ) ); elseif( isset( $arrValues['entity_id'] ) ) $this->setEntityId( $arrValues['entity_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['deposit_id'] ) && $boolDirectSet ) $this->set( 'm_intDepositId', trim( $arrValues['deposit_id'] ) ); elseif( isset( $arrValues['deposit_id'] ) ) $this->setDepositId( $arrValues['deposit_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		if( isset( $arrValues['company_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyChargeId', trim( $arrValues['company_charge_id'] ) ); elseif( isset( $arrValues['company_charge_id'] ) ) $this->setCompanyChargeId( $arrValues['company_charge_id'] );
		if( isset( $arrValues['transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionId', trim( $arrValues['transaction_id'] ) ); elseif( isset( $arrValues['transaction_id'] ) ) $this->setTransactionId( $arrValues['transaction_id'] );
		if( isset( $arrValues['charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeCodeId', trim( $arrValues['charge_code_id'] ) ); elseif( isset( $arrValues['charge_code_id'] ) ) $this->setChargeCodeId( $arrValues['charge_code_id'] );
		if( isset( $arrValues['company_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyPaymentId', trim( $arrValues['company_payment_id'] ) ); elseif( isset( $arrValues['company_payment_id'] ) ) $this->setCompanyPaymentId( $arrValues['company_payment_id'] );
		if( isset( $arrValues['invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceId', trim( $arrValues['invoice_id'] ) ); elseif( isset( $arrValues['invoice_id'] ) ) $this->setInvoiceId( $arrValues['invoice_id'] );
		if( isset( $arrValues['export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intExportBatchId', trim( $arrValues['export_batch_id'] ) ); elseif( isset( $arrValues['export_batch_id'] ) ) $this->setExportBatchId( $arrValues['export_batch_id'] );
		if( isset( $arrValues['contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intContractPropertyId', trim( $arrValues['contract_property_id'] ) ); elseif( isset( $arrValues['contract_property_id'] ) ) $this->setContractPropertyId( $arrValues['contract_property_id'] );
		if( isset( $arrValues['bundle_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intBundlePsProductId', trim( $arrValues['bundle_ps_product_id'] ) ); elseif( isset( $arrValues['bundle_ps_product_id'] ) ) $this->setBundlePsProductId( $arrValues['bundle_ps_product_id'] );
		if( isset( $arrValues['transaction_reversal_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionReversalTypeId', trim( $arrValues['transaction_reversal_type_id'] ) ); elseif( isset( $arrValues['transaction_reversal_type_id'] ) ) $this->setTransactionReversalTypeId( $arrValues['transaction_reversal_type_id'] );
		if( isset( $arrValues['deferred_export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intDeferredExportBatchId', trim( $arrValues['deferred_export_batch_id'] ) ); elseif( isset( $arrValues['deferred_export_batch_id'] ) ) $this->setDeferredExportBatchId( $arrValues['deferred_export_batch_id'] );
		if( isset( $arrValues['reference_number'] ) && $boolDirectSet ) $this->set( 'm_intReferenceNumber', trim( $arrValues['reference_number'] ) ); elseif( isset( $arrValues['reference_number'] ) ) $this->setReferenceNumber( $arrValues['reference_number'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmount', trim( $arrValues['transaction_amount'] ) ); elseif( isset( $arrValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrValues['transaction_amount'] );
		if( isset( $arrValues['cost_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCostAmount', trim( $arrValues['cost_amount'] ) ); elseif( isset( $arrValues['cost_amount'] ) ) $this->setCostAmount( $arrValues['cost_amount'] );
		if( isset( $arrValues['transaction_due_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionDueAmount', trim( $arrValues['transaction_due_amount'] ) ); elseif( isset( $arrValues['transaction_due_amount'] ) ) $this->setTransactionDueAmount( $arrValues['transaction_due_amount'] );
		if( isset( $arrValues['item_count'] ) && $boolDirectSet ) $this->set( 'm_intItemCount', trim( $arrValues['item_count'] ) ); elseif( isset( $arrValues['item_count'] ) ) $this->setItemCount( $arrValues['item_count'] );
		if( isset( $arrValues['memo'] ) && $boolDirectSet ) $this->set( 'm_strMemo', trim( stripcslashes( $arrValues['memo'] ) ) ); elseif( isset( $arrValues['memo'] ) ) $this->setMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['memo'] ) : $arrValues['memo'] );
		if( isset( $arrValues['internal_notes'] ) && $boolDirectSet ) $this->set( 'm_strInternalNotes', trim( stripcslashes( $arrValues['internal_notes'] ) ) ); elseif( isset( $arrValues['internal_notes'] ) ) $this->setInternalNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['internal_notes'] ) : $arrValues['internal_notes'] );
		if( isset( $arrValues['is_commissioned'] ) && $boolDirectSet ) $this->set( 'm_intIsCommissioned', trim( $arrValues['is_commissioned'] ) ); elseif( isset( $arrValues['is_commissioned'] ) ) $this->setIsCommissioned( $arrValues['is_commissioned'] );
		if( isset( $arrValues['post_month'] ) && $boolDirectSet ) $this->set( 'm_strPostMonth', trim( $arrValues['post_month'] ) ); elseif( isset( $arrValues['post_month'] ) ) $this->setPostMonth( $arrValues['post_month'] );
		if( isset( $arrValues['transaction_paid_date'] ) && $boolDirectSet ) $this->set( 'm_strTransactionPaidDate', trim( $arrValues['transaction_paid_date'] ) ); elseif( isset( $arrValues['transaction_paid_date'] ) ) $this->setTransactionPaidDate( $arrValues['transaction_paid_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['contract_chain_id'] ) && $boolDirectSet ) $this->set( 'm_intContractChainId', trim( $arrValues['contract_chain_id'] ) ); elseif( isset( $arrValues['contract_chain_id'] ) ) $this->setContractChainId( $arrValues['contract_chain_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['revenue_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRevenueTypeId', trim( $arrValues['revenue_type_id'] ) ); elseif( isset( $arrValues['revenue_type_id'] ) ) $this->setRevenueTypeId( $arrValues['revenue_type_id'] );
		if( isset( $arrValues['billing_period'] ) && $boolDirectSet ) $this->set( 'm_strBillingPeriod', trim( $arrValues['billing_period'] ) ); elseif( isset( $arrValues['billing_period'] ) ) $this->setBillingPeriod( $arrValues['billing_period'] );
		if( isset( $arrValues['accounting_period'] ) && $boolDirectSet ) $this->set( 'm_strAccountingPeriod', trim( $arrValues['accounting_period'] ) ); elseif( isset( $arrValues['accounting_period'] ) ) $this->setAccountingPeriod( $arrValues['accounting_period'] );
		if( isset( $arrValues['revenue_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRevenueAmount', trim( $arrValues['revenue_amount'] ) ); elseif( isset( $arrValues['revenue_amount'] ) ) $this->setRevenueAmount( $arrValues['revenue_amount'] );
		if( isset( $arrValues['reference_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceTransactionId', trim( $arrValues['reference_transaction_id'] ) ); elseif( isset( $arrValues['reference_transaction_id'] ) ) $this->setReferenceTransactionId( $arrValues['reference_transaction_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEntityId( $intEntityId ) {
		$this->set( 'm_intEntityId', CStrings::strToIntDef( $intEntityId, NULL, false ) );
	}

	public function getEntityId() {
		return $this->m_intEntityId;
	}

	public function sqlEntityId() {
		return ( true == isset( $this->m_intEntityId ) ) ? ( string ) $this->m_intEntityId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setDepositId( $intDepositId ) {
		$this->set( 'm_intDepositId', CStrings::strToIntDef( $intDepositId, NULL, false ) );
	}

	public function getDepositId() {
		return $this->m_intDepositId;
	}

	public function sqlDepositId() {
		return ( true == isset( $this->m_intDepositId ) ) ? ( string ) $this->m_intDepositId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : '\'USD\'';
	}

	public function setCompanyChargeId( $intCompanyChargeId ) {
		$this->set( 'm_intCompanyChargeId', CStrings::strToIntDef( $intCompanyChargeId, NULL, false ) );
	}

	public function getCompanyChargeId() {
		return $this->m_intCompanyChargeId;
	}

	public function sqlCompanyChargeId() {
		return ( true == isset( $this->m_intCompanyChargeId ) ) ? ( string ) $this->m_intCompanyChargeId : 'NULL';
	}

	public function setTransactionId( $intTransactionId ) {
		$this->set( 'm_intTransactionId', CStrings::strToIntDef( $intTransactionId, NULL, false ) );
	}

	public function getTransactionId() {
		return $this->m_intTransactionId;
	}

	public function sqlTransactionId() {
		return ( true == isset( $this->m_intTransactionId ) ) ? ( string ) $this->m_intTransactionId : 'NULL';
	}

	public function setChargeCodeId( $intChargeCodeId ) {
		$this->set( 'm_intChargeCodeId', CStrings::strToIntDef( $intChargeCodeId, NULL, false ) );
	}

	public function getChargeCodeId() {
		return $this->m_intChargeCodeId;
	}

	public function sqlChargeCodeId() {
		return ( true == isset( $this->m_intChargeCodeId ) ) ? ( string ) $this->m_intChargeCodeId : 'NULL';
	}

	public function setCompanyPaymentId( $intCompanyPaymentId ) {
		$this->set( 'm_intCompanyPaymentId', CStrings::strToIntDef( $intCompanyPaymentId, NULL, false ) );
	}

	public function getCompanyPaymentId() {
		return $this->m_intCompanyPaymentId;
	}

	public function sqlCompanyPaymentId() {
		return ( true == isset( $this->m_intCompanyPaymentId ) ) ? ( string ) $this->m_intCompanyPaymentId : 'NULL';
	}

	public function setInvoiceId( $intInvoiceId ) {
		$this->set( 'm_intInvoiceId', CStrings::strToIntDef( $intInvoiceId, NULL, false ) );
	}

	public function getInvoiceId() {
		return $this->m_intInvoiceId;
	}

	public function sqlInvoiceId() {
		return ( true == isset( $this->m_intInvoiceId ) ) ? ( string ) $this->m_intInvoiceId : 'NULL';
	}

	public function setExportBatchId( $intExportBatchId ) {
		$this->set( 'm_intExportBatchId', CStrings::strToIntDef( $intExportBatchId, NULL, false ) );
	}

	public function getExportBatchId() {
		return $this->m_intExportBatchId;
	}

	public function sqlExportBatchId() {
		return ( true == isset( $this->m_intExportBatchId ) ) ? ( string ) $this->m_intExportBatchId : 'NULL';
	}

	public function setContractPropertyId( $intContractPropertyId ) {
		$this->set( 'm_intContractPropertyId', CStrings::strToIntDef( $intContractPropertyId, NULL, false ) );
	}

	public function getContractPropertyId() {
		return $this->m_intContractPropertyId;
	}

	public function sqlContractPropertyId() {
		return ( true == isset( $this->m_intContractPropertyId ) ) ? ( string ) $this->m_intContractPropertyId : 'NULL';
	}

	public function setBundlePsProductId( $intBundlePsProductId ) {
		$this->set( 'm_intBundlePsProductId', CStrings::strToIntDef( $intBundlePsProductId, NULL, false ) );
	}

	public function getBundlePsProductId() {
		return $this->m_intBundlePsProductId;
	}

	public function sqlBundlePsProductId() {
		return ( true == isset( $this->m_intBundlePsProductId ) ) ? ( string ) $this->m_intBundlePsProductId : 'NULL';
	}

	public function setTransactionReversalTypeId( $intTransactionReversalTypeId ) {
		$this->set( 'm_intTransactionReversalTypeId', CStrings::strToIntDef( $intTransactionReversalTypeId, NULL, false ) );
	}

	public function getTransactionReversalTypeId() {
		return $this->m_intTransactionReversalTypeId;
	}

	public function sqlTransactionReversalTypeId() {
		return ( true == isset( $this->m_intTransactionReversalTypeId ) ) ? ( string ) $this->m_intTransactionReversalTypeId : 'NULL';
	}

	public function setDeferredExportBatchId( $intDeferredExportBatchId ) {
		$this->set( 'm_intDeferredExportBatchId', CStrings::strToIntDef( $intDeferredExportBatchId, NULL, false ) );
	}

	public function getDeferredExportBatchId() {
		return $this->m_intDeferredExportBatchId;
	}

	public function sqlDeferredExportBatchId() {
		return ( true == isset( $this->m_intDeferredExportBatchId ) ) ? ( string ) $this->m_intDeferredExportBatchId : 'NULL';
	}

	public function setReferenceNumber( $intReferenceNumber ) {
		$this->set( 'm_intReferenceNumber', CStrings::strToIntDef( $intReferenceNumber, NULL, false ) );
	}

	public function getReferenceNumber() {
		return $this->m_intReferenceNumber;
	}

	public function sqlReferenceNumber() {
		return ( true == isset( $this->m_intReferenceNumber ) ) ? ( string ) $this->m_intReferenceNumber : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->set( 'm_fltTransactionAmount', CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 4 ) );
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function sqlTransactionAmount() {
		return ( true == isset( $this->m_fltTransactionAmount ) ) ? ( string ) $this->m_fltTransactionAmount : 'NULL';
	}

	public function setCostAmount( $fltCostAmount ) {
		$this->set( 'm_fltCostAmount', CStrings::strToFloatDef( $fltCostAmount, NULL, false, 4 ) );
	}

	public function getCostAmount() {
		return $this->m_fltCostAmount;
	}

	public function sqlCostAmount() {
		return ( true == isset( $this->m_fltCostAmount ) ) ? ( string ) $this->m_fltCostAmount : '0';
	}

	public function setTransactionDueAmount( $fltTransactionDueAmount ) {
		$this->set( 'm_fltTransactionDueAmount', CStrings::strToFloatDef( $fltTransactionDueAmount, NULL, false, 4 ) );
	}

	public function getTransactionDueAmount() {
		return $this->m_fltTransactionDueAmount;
	}

	public function sqlTransactionDueAmount() {
		return ( true == isset( $this->m_fltTransactionDueAmount ) ) ? ( string ) $this->m_fltTransactionDueAmount : '0';
	}

	public function setItemCount( $intItemCount ) {
		$this->set( 'm_intItemCount', CStrings::strToIntDef( $intItemCount, NULL, false ) );
	}

	public function getItemCount() {
		return $this->m_intItemCount;
	}

	public function sqlItemCount() {
		return ( true == isset( $this->m_intItemCount ) ) ? ( string ) $this->m_intItemCount : '0';
	}

	public function setMemo( $strMemo ) {
		$this->set( 'm_strMemo', CStrings::strTrimDef( $strMemo, 2000, NULL, true ) );
	}

	public function getMemo() {
		return $this->m_strMemo;
	}

	public function sqlMemo() {
		return ( true == isset( $this->m_strMemo ) ) ? '\'' . addslashes( $this->m_strMemo ) . '\'' : 'NULL';
	}

	public function setInternalNotes( $strInternalNotes ) {
		$this->set( 'm_strInternalNotes', CStrings::strTrimDef( $strInternalNotes, 2000, NULL, true ) );
	}

	public function getInternalNotes() {
		return $this->m_strInternalNotes;
	}

	public function sqlInternalNotes() {
		return ( true == isset( $this->m_strInternalNotes ) ) ? '\'' . addslashes( $this->m_strInternalNotes ) . '\'' : 'NULL';
	}

	public function setIsCommissioned( $intIsCommissioned ) {
		$this->set( 'm_intIsCommissioned', CStrings::strToIntDef( $intIsCommissioned, NULL, false ) );
	}

	public function getIsCommissioned() {
		return $this->m_intIsCommissioned;
	}

	public function sqlIsCommissioned() {
		return ( true == isset( $this->m_intIsCommissioned ) ) ? ( string ) $this->m_intIsCommissioned : '0';
	}

	public function setPostMonth( $strPostMonth ) {
		$this->set( 'm_strPostMonth', CStrings::strTrimDef( $strPostMonth, -1, NULL, true ) );
	}

	public function getPostMonth() {
		return $this->m_strPostMonth;
	}

	public function sqlPostMonth() {
		return ( true == isset( $this->m_strPostMonth ) ) ? '\'' . $this->m_strPostMonth . '\'' : 'NULL';
	}

	public function setTransactionPaidDate( $strTransactionPaidDate ) {
		$this->set( 'm_strTransactionPaidDate', CStrings::strTrimDef( $strTransactionPaidDate, -1, NULL, true ) );
	}

	public function getTransactionPaidDate() {
		return $this->m_strTransactionPaidDate;
	}

	public function sqlTransactionPaidDate() {
		return ( true == isset( $this->m_strTransactionPaidDate ) ) ? '\'' . $this->m_strTransactionPaidDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setContractChainId( $intContractChainId ) {
		$this->set( 'm_intContractChainId', CStrings::strToIntDef( $intContractChainId, NULL, false ) );
	}

	public function getContractChainId() {
		return $this->m_intContractChainId;
	}

	public function sqlContractChainId() {
		return ( true == isset( $this->m_intContractChainId ) ) ? ( string ) $this->m_intContractChainId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setRevenueTypeId( $intRevenueTypeId ) {
		$this->set( 'm_intRevenueTypeId', CStrings::strToIntDef( $intRevenueTypeId, NULL, false ) );
	}

	public function getRevenueTypeId() {
		return $this->m_intRevenueTypeId;
	}

	public function sqlRevenueTypeId() {
		return ( true == isset( $this->m_intRevenueTypeId ) ) ? ( string ) $this->m_intRevenueTypeId : 'NULL';
	}

	public function setBillingPeriod( $strBillingPeriod ) {
		$this->set( 'm_strBillingPeriod', CStrings::strTrimDef( $strBillingPeriod, -1, NULL, true ) );
	}

	public function getBillingPeriod() {
		return $this->m_strBillingPeriod;
	}

	public function sqlBillingPeriod() {
		return ( true == isset( $this->m_strBillingPeriod ) ) ? '\'' . $this->m_strBillingPeriod . '\'' : 'NULL';
	}

	public function setAccountingPeriod( $strAccountingPeriod ) {
		$this->set( 'm_strAccountingPeriod', CStrings::strTrimDef( $strAccountingPeriod, -1, NULL, true ) );
	}

	public function getAccountingPeriod() {
		return $this->m_strAccountingPeriod;
	}

	public function sqlAccountingPeriod() {
		return ( true == isset( $this->m_strAccountingPeriod ) ) ? '\'' . $this->m_strAccountingPeriod . '\'' : 'NULL';
	}

	public function setRevenueAmount( $fltRevenueAmount ) {
		$this->set( 'm_fltRevenueAmount', CStrings::strToFloatDef( $fltRevenueAmount, NULL, false, 4 ) );
	}

	public function getRevenueAmount() {
		return $this->m_fltRevenueAmount;
	}

	public function sqlRevenueAmount() {
		return ( true == isset( $this->m_fltRevenueAmount ) ) ? ( string ) $this->m_fltRevenueAmount : '0';
	}

	public function setReferenceTransactionId( $intReferenceTransactionId ) {
		$this->set( 'm_intReferenceTransactionId', CStrings::strToIntDef( $intReferenceTransactionId, NULL, false ) );
	}

	public function getReferenceTransactionId() {
		return $this->m_intReferenceTransactionId;
	}

	public function sqlReferenceTransactionId() {
		return ( true == isset( $this->m_intReferenceTransactionId ) ) ? ( string ) $this->m_intReferenceTransactionId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, entity_id, cid, account_id, property_id, deposit_id, currency_code, company_charge_id, transaction_id, charge_code_id, company_payment_id, invoice_id, export_batch_id, contract_property_id, bundle_ps_product_id, transaction_reversal_type_id, deferred_export_batch_id, reference_number, transaction_datetime, transaction_amount, cost_amount, transaction_due_amount, item_count, memo, internal_notes, is_commissioned, post_month, transaction_paid_date, updated_by, updated_on, created_by, created_on, contract_chain_id, ps_product_id, revenue_type_id, billing_period, accounting_period, revenue_amount, reference_transaction_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEntityId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlDepositId() . ', ' .
						$this->sqlCurrencyCode() . ', ' .
						$this->sqlCompanyChargeId() . ', ' .
						$this->sqlTransactionId() . ', ' .
						$this->sqlChargeCodeId() . ', ' .
						$this->sqlCompanyPaymentId() . ', ' .
						$this->sqlInvoiceId() . ', ' .
						$this->sqlExportBatchId() . ', ' .
						$this->sqlContractPropertyId() . ', ' .
						$this->sqlBundlePsProductId() . ', ' .
						$this->sqlTransactionReversalTypeId() . ', ' .
						$this->sqlDeferredExportBatchId() . ', ' .
						$this->sqlReferenceNumber() . ', ' .
						$this->sqlTransactionDatetime() . ', ' .
						$this->sqlTransactionAmount() . ', ' .
						$this->sqlCostAmount() . ', ' .
						$this->sqlTransactionDueAmount() . ', ' .
						$this->sqlItemCount() . ', ' .
						$this->sqlMemo() . ', ' .
						$this->sqlInternalNotes() . ', ' .
						$this->sqlIsCommissioned() . ', ' .
						$this->sqlPostMonth() . ', ' .
						$this->sqlTransactionPaidDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlContractChainId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlRevenueTypeId() . ', ' .
						$this->sqlBillingPeriod() . ', ' .
						$this->sqlAccountingPeriod() . ', ' .
						$this->sqlRevenueAmount() . ', ' .
						$this->sqlReferenceTransactionId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_id = ' . $this->sqlEntityId(). ',' ; } elseif( true == array_key_exists( 'EntityId', $this->getChangedColumns() ) ) { $strSql .= ' entity_id = ' . $this->sqlEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_id = ' . $this->sqlDepositId(). ',' ; } elseif( true == array_key_exists( 'DepositId', $this->getChangedColumns() ) ) { $strSql .= ' deposit_id = ' . $this->sqlDepositId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_charge_id = ' . $this->sqlCompanyChargeId(). ',' ; } elseif( true == array_key_exists( 'CompanyChargeId', $this->getChangedColumns() ) ) { $strSql .= ' company_charge_id = ' . $this->sqlCompanyChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId(). ',' ; } elseif( true == array_key_exists( 'TransactionId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId(). ',' ; } elseif( true == array_key_exists( 'ChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId(). ',' ; } elseif( true == array_key_exists( 'CompanyPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_id = ' . $this->sqlInvoiceId(). ',' ; } elseif( true == array_key_exists( 'InvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_id = ' . $this->sqlInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_batch_id = ' . $this->sqlExportBatchId(). ',' ; } elseif( true == array_key_exists( 'ExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' export_batch_id = ' . $this->sqlExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_property_id = ' . $this->sqlContractPropertyId(). ',' ; } elseif( true == array_key_exists( 'ContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' contract_property_id = ' . $this->sqlContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bundle_ps_product_id = ' . $this->sqlBundlePsProductId(). ',' ; } elseif( true == array_key_exists( 'BundlePsProductId', $this->getChangedColumns() ) ) { $strSql .= ' bundle_ps_product_id = ' . $this->sqlBundlePsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_reversal_type_id = ' . $this->sqlTransactionReversalTypeId(). ',' ; } elseif( true == array_key_exists( 'TransactionReversalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_reversal_type_id = ' . $this->sqlTransactionReversalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deferred_export_batch_id = ' . $this->sqlDeferredExportBatchId(). ',' ; } elseif( true == array_key_exists( 'DeferredExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' deferred_export_batch_id = ' . $this->sqlDeferredExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'ReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime(). ',' ; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cost_amount = ' . $this->sqlCostAmount(). ',' ; } elseif( true == array_key_exists( 'CostAmount', $this->getChangedColumns() ) ) { $strSql .= ' cost_amount = ' . $this->sqlCostAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_due_amount = ' . $this->sqlTransactionDueAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionDueAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_due_amount = ' . $this->sqlTransactionDueAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' item_count = ' . $this->sqlItemCount(). ',' ; } elseif( true == array_key_exists( 'ItemCount', $this->getChangedColumns() ) ) { $strSql .= ' item_count = ' . $this->sqlItemCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' memo = ' . $this->sqlMemo(). ',' ; } elseif( true == array_key_exists( 'Memo', $this->getChangedColumns() ) ) { $strSql .= ' memo = ' . $this->sqlMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_notes = ' . $this->sqlInternalNotes(). ',' ; } elseif( true == array_key_exists( 'InternalNotes', $this->getChangedColumns() ) ) { $strSql .= ' internal_notes = ' . $this->sqlInternalNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_commissioned = ' . $this->sqlIsCommissioned(). ',' ; } elseif( true == array_key_exists( 'IsCommissioned', $this->getChangedColumns() ) ) { $strSql .= ' is_commissioned = ' . $this->sqlIsCommissioned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_month = ' . $this->sqlPostMonth(). ',' ; } elseif( true == array_key_exists( 'PostMonth', $this->getChangedColumns() ) ) { $strSql .= ' post_month = ' . $this->sqlPostMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_paid_date = ' . $this->sqlTransactionPaidDate(). ',' ; } elseif( true == array_key_exists( 'TransactionPaidDate', $this->getChangedColumns() ) ) { $strSql .= ' transaction_paid_date = ' . $this->sqlTransactionPaidDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_chain_id = ' . $this->sqlContractChainId(). ',' ; } elseif( true == array_key_exists( 'ContractChainId', $this->getChangedColumns() ) ) { $strSql .= ' contract_chain_id = ' . $this->sqlContractChainId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revenue_type_id = ' . $this->sqlRevenueTypeId(). ',' ; } elseif( true == array_key_exists( 'RevenueTypeId', $this->getChangedColumns() ) ) { $strSql .= ' revenue_type_id = ' . $this->sqlRevenueTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_period = ' . $this->sqlBillingPeriod(). ',' ; } elseif( true == array_key_exists( 'BillingPeriod', $this->getChangedColumns() ) ) { $strSql .= ' billing_period = ' . $this->sqlBillingPeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accounting_period = ' . $this->sqlAccountingPeriod(). ',' ; } elseif( true == array_key_exists( 'AccountingPeriod', $this->getChangedColumns() ) ) { $strSql .= ' accounting_period = ' . $this->sqlAccountingPeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revenue_amount = ' . $this->sqlRevenueAmount(). ',' ; } elseif( true == array_key_exists( 'RevenueAmount', $this->getChangedColumns() ) ) { $strSql .= ' revenue_amount = ' . $this->sqlRevenueAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_transaction_id = ' . $this->sqlReferenceTransactionId(). ',' ; } elseif( true == array_key_exists( 'ReferenceTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' reference_transaction_id = ' . $this->sqlReferenceTransactionId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'entity_id' => $this->getEntityId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'property_id' => $this->getPropertyId(),
			'deposit_id' => $this->getDepositId(),
			'currency_code' => $this->getCurrencyCode(),
			'company_charge_id' => $this->getCompanyChargeId(),
			'transaction_id' => $this->getTransactionId(),
			'charge_code_id' => $this->getChargeCodeId(),
			'company_payment_id' => $this->getCompanyPaymentId(),
			'invoice_id' => $this->getInvoiceId(),
			'export_batch_id' => $this->getExportBatchId(),
			'contract_property_id' => $this->getContractPropertyId(),
			'bundle_ps_product_id' => $this->getBundlePsProductId(),
			'transaction_reversal_type_id' => $this->getTransactionReversalTypeId(),
			'deferred_export_batch_id' => $this->getDeferredExportBatchId(),
			'reference_number' => $this->getReferenceNumber(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'transaction_amount' => $this->getTransactionAmount(),
			'cost_amount' => $this->getCostAmount(),
			'transaction_due_amount' => $this->getTransactionDueAmount(),
			'item_count' => $this->getItemCount(),
			'memo' => $this->getMemo(),
			'internal_notes' => $this->getInternalNotes(),
			'is_commissioned' => $this->getIsCommissioned(),
			'post_month' => $this->getPostMonth(),
			'transaction_paid_date' => $this->getTransactionPaidDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'contract_chain_id' => $this->getContractChainId(),
			'ps_product_id' => $this->getPsProductId(),
			'revenue_type_id' => $this->getRevenueTypeId(),
			'billing_period' => $this->getBillingPeriod(),
			'accounting_period' => $this->getAccountingPeriod(),
			'revenue_amount' => $this->getRevenueAmount(),
			'reference_transaction_id' => $this->getReferenceTransactionId()
		);
	}

}
?>