<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CBusinessUnits
 * Do not add any new functions to this class.
 */

class CBaseBusinessUnits extends CEosPluralBase {

	/**
	 * @return CBusinessUnit[]
	 */
	public static function fetchBusinessUnits( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBusinessUnit::class, $objDatabase );
	}

	/**
	 * @return CBusinessUnit
	 */
	public static function fetchBusinessUnit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBusinessUnit::class, $objDatabase );
	}

	public static function fetchBusinessUnitCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'business_units', $objDatabase );
	}

	public static function fetchBusinessUnitById( $intId, $objDatabase ) {
		return self::fetchBusinessUnit( sprintf( 'SELECT * FROM business_units WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchBusinessUnitsByBuHrEmployeeId( $intBuHrEmployeeId, $objDatabase ) {
		return self::fetchBusinessUnits( sprintf( 'SELECT * FROM business_units WHERE bu_hr_employee_id = %d', ( int ) $intBuHrEmployeeId ), $objDatabase );
	}

	public static function fetchBusinessUnitsByDirectorEmployeeId( $intDirectorEmployeeId, $objDatabase ) {
		return self::fetchBusinessUnits( sprintf( 'SELECT * FROM business_units WHERE director_employee_id = %d', ( int ) $intDirectorEmployeeId ), $objDatabase );
	}

	public static function fetchBusinessUnitsByTrainingRepresentativeEmployeeId( $intTrainingRepresentativeEmployeeId, $objDatabase ) {
		return self::fetchBusinessUnits( sprintf( 'SELECT * FROM business_units WHERE training_representative_employee_id = %d', ( int ) $intTrainingRepresentativeEmployeeId ), $objDatabase );
	}

	public static function fetchBusinessUnitsByRecruiterEmployeeId( $intRecruiterEmployeeId, $objDatabase ) {
		return self::fetchBusinessUnits( sprintf( 'SELECT * FROM business_units WHERE recruiter_employee_id = %d', ( int ) $intRecruiterEmployeeId ), $objDatabase );
	}

}
?>