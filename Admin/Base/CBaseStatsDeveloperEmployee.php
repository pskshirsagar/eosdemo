<?php

class CBaseStatsDeveloperEmployee extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_developer_employees';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_strWeek;
	protected $m_intStoryPointsReleased;
	protected $m_intTotalLineInsertions;
	protected $m_intTotalLineDeletions;
	protected $m_intFilesAdded;
	protected $m_intFilesDeleted;
	protected $m_intSvnCommits;
	protected $m_intBugSeverity;
	protected $m_intBugsCaused;
	protected $m_intTasksReleased;
	protected $m_intBugsResolved;
	protected $m_intFeaturesReleased;
	protected $m_fltHoursWorked;
	protected $m_intQaRejections;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intFilesCommitted;

	public function __construct() {
		parent::__construct();

		$this->m_intStoryPointsReleased = '0';
		$this->m_intTotalLineInsertions = '0';
		$this->m_intTotalLineDeletions = '0';
		$this->m_intFilesAdded = '0';
		$this->m_intFilesDeleted = '0';
		$this->m_intSvnCommits = '0';
		$this->m_intBugSeverity = '0';
		$this->m_intBugsCaused = '0';
		$this->m_intTasksReleased = '0';
		$this->m_intBugsResolved = '0';
		$this->m_intFeaturesReleased = '0';
		$this->m_fltHoursWorked = '0';
		$this->m_intQaRejections = '0';
		$this->m_intCreatedBy = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['week'] ) && $boolDirectSet ) $this->set( 'm_strWeek', trim( $arrValues['week'] ) ); elseif( isset( $arrValues['week'] ) ) $this->setWeek( $arrValues['week'] );
		if( isset( $arrValues['story_points_released'] ) && $boolDirectSet ) $this->set( 'm_intStoryPointsReleased', trim( $arrValues['story_points_released'] ) ); elseif( isset( $arrValues['story_points_released'] ) ) $this->setStoryPointsReleased( $arrValues['story_points_released'] );
		if( isset( $arrValues['total_line_insertions'] ) && $boolDirectSet ) $this->set( 'm_intTotalLineInsertions', trim( $arrValues['total_line_insertions'] ) ); elseif( isset( $arrValues['total_line_insertions'] ) ) $this->setTotalLineInsertions( $arrValues['total_line_insertions'] );
		if( isset( $arrValues['total_line_deletions'] ) && $boolDirectSet ) $this->set( 'm_intTotalLineDeletions', trim( $arrValues['total_line_deletions'] ) ); elseif( isset( $arrValues['total_line_deletions'] ) ) $this->setTotalLineDeletions( $arrValues['total_line_deletions'] );
		if( isset( $arrValues['files_added'] ) && $boolDirectSet ) $this->set( 'm_intFilesAdded', trim( $arrValues['files_added'] ) ); elseif( isset( $arrValues['files_added'] ) ) $this->setFilesAdded( $arrValues['files_added'] );
		if( isset( $arrValues['files_deleted'] ) && $boolDirectSet ) $this->set( 'm_intFilesDeleted', trim( $arrValues['files_deleted'] ) ); elseif( isset( $arrValues['files_deleted'] ) ) $this->setFilesDeleted( $arrValues['files_deleted'] );
		if( isset( $arrValues['svn_commits'] ) && $boolDirectSet ) $this->set( 'm_intSvnCommits', trim( $arrValues['svn_commits'] ) ); elseif( isset( $arrValues['svn_commits'] ) ) $this->setSvnCommits( $arrValues['svn_commits'] );
		if( isset( $arrValues['bug_severity'] ) && $boolDirectSet ) $this->set( 'm_intBugSeverity', trim( $arrValues['bug_severity'] ) ); elseif( isset( $arrValues['bug_severity'] ) ) $this->setBugSeverity( $arrValues['bug_severity'] );
		if( isset( $arrValues['bugs_caused'] ) && $boolDirectSet ) $this->set( 'm_intBugsCaused', trim( $arrValues['bugs_caused'] ) ); elseif( isset( $arrValues['bugs_caused'] ) ) $this->setBugsCaused( $arrValues['bugs_caused'] );
		if( isset( $arrValues['tasks_released'] ) && $boolDirectSet ) $this->set( 'm_intTasksReleased', trim( $arrValues['tasks_released'] ) ); elseif( isset( $arrValues['tasks_released'] ) ) $this->setTasksReleased( $arrValues['tasks_released'] );
		if( isset( $arrValues['bugs_resolved'] ) && $boolDirectSet ) $this->set( 'm_intBugsResolved', trim( $arrValues['bugs_resolved'] ) ); elseif( isset( $arrValues['bugs_resolved'] ) ) $this->setBugsResolved( $arrValues['bugs_resolved'] );
		if( isset( $arrValues['features_released'] ) && $boolDirectSet ) $this->set( 'm_intFeaturesReleased', trim( $arrValues['features_released'] ) ); elseif( isset( $arrValues['features_released'] ) ) $this->setFeaturesReleased( $arrValues['features_released'] );
		if( isset( $arrValues['hours_worked'] ) && $boolDirectSet ) $this->set( 'm_fltHoursWorked', trim( $arrValues['hours_worked'] ) ); elseif( isset( $arrValues['hours_worked'] ) ) $this->setHoursWorked( $arrValues['hours_worked'] );
		if( isset( $arrValues['qa_rejections'] ) && $boolDirectSet ) $this->set( 'm_intQaRejections', trim( $arrValues['qa_rejections'] ) ); elseif( isset( $arrValues['qa_rejections'] ) ) $this->setQaRejections( $arrValues['qa_rejections'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['files_committed'] ) && $boolDirectSet ) $this->set( 'm_intFilesCommitted', trim( $arrValues['files_committed'] ) ); elseif( isset( $arrValues['files_committed'] ) ) $this->setFilesCommitted( $arrValues['files_committed'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setWeek( $strWeek ) {
		$this->set( 'm_strWeek', CStrings::strTrimDef( $strWeek, -1, NULL, true ) );
	}

	public function getWeek() {
		return $this->m_strWeek;
	}

	public function sqlWeek() {
		return ( true == isset( $this->m_strWeek ) ) ? '\'' . $this->m_strWeek . '\'' : 'NOW()';
	}

	public function setStoryPointsReleased( $intStoryPointsReleased ) {
		$this->set( 'm_intStoryPointsReleased', CStrings::strToIntDef( $intStoryPointsReleased, NULL, false ) );
	}

	public function getStoryPointsReleased() {
		return $this->m_intStoryPointsReleased;
	}

	public function sqlStoryPointsReleased() {
		return ( true == isset( $this->m_intStoryPointsReleased ) ) ? ( string ) $this->m_intStoryPointsReleased : '0';
	}

	public function setTotalLineInsertions( $intTotalLineInsertions ) {
		$this->set( 'm_intTotalLineInsertions', CStrings::strToIntDef( $intTotalLineInsertions, NULL, false ) );
	}

	public function getTotalLineInsertions() {
		return $this->m_intTotalLineInsertions;
	}

	public function sqlTotalLineInsertions() {
		return ( true == isset( $this->m_intTotalLineInsertions ) ) ? ( string ) $this->m_intTotalLineInsertions : '0';
	}

	public function setTotalLineDeletions( $intTotalLineDeletions ) {
		$this->set( 'm_intTotalLineDeletions', CStrings::strToIntDef( $intTotalLineDeletions, NULL, false ) );
	}

	public function getTotalLineDeletions() {
		return $this->m_intTotalLineDeletions;
	}

	public function sqlTotalLineDeletions() {
		return ( true == isset( $this->m_intTotalLineDeletions ) ) ? ( string ) $this->m_intTotalLineDeletions : '0';
	}

	public function setFilesAdded( $intFilesAdded ) {
		$this->set( 'm_intFilesAdded', CStrings::strToIntDef( $intFilesAdded, NULL, false ) );
	}

	public function getFilesAdded() {
		return $this->m_intFilesAdded;
	}

	public function sqlFilesAdded() {
		return ( true == isset( $this->m_intFilesAdded ) ) ? ( string ) $this->m_intFilesAdded : '0';
	}

	public function setFilesDeleted( $intFilesDeleted ) {
		$this->set( 'm_intFilesDeleted', CStrings::strToIntDef( $intFilesDeleted, NULL, false ) );
	}

	public function getFilesDeleted() {
		return $this->m_intFilesDeleted;
	}

	public function sqlFilesDeleted() {
		return ( true == isset( $this->m_intFilesDeleted ) ) ? ( string ) $this->m_intFilesDeleted : '0';
	}

	public function setSvnCommits( $intSvnCommits ) {
		$this->set( 'm_intSvnCommits', CStrings::strToIntDef( $intSvnCommits, NULL, false ) );
	}

	public function getSvnCommits() {
		return $this->m_intSvnCommits;
	}

	public function sqlSvnCommits() {
		return ( true == isset( $this->m_intSvnCommits ) ) ? ( string ) $this->m_intSvnCommits : '0';
	}

	public function setBugSeverity( $intBugSeverity ) {
		$this->set( 'm_intBugSeverity', CStrings::strToIntDef( $intBugSeverity, NULL, false ) );
	}

	public function getBugSeverity() {
		return $this->m_intBugSeverity;
	}

	public function sqlBugSeverity() {
		return ( true == isset( $this->m_intBugSeverity ) ) ? ( string ) $this->m_intBugSeverity : '0';
	}

	public function setBugsCaused( $intBugsCaused ) {
		$this->set( 'm_intBugsCaused', CStrings::strToIntDef( $intBugsCaused, NULL, false ) );
	}

	public function getBugsCaused() {
		return $this->m_intBugsCaused;
	}

	public function sqlBugsCaused() {
		return ( true == isset( $this->m_intBugsCaused ) ) ? ( string ) $this->m_intBugsCaused : '0';
	}

	public function setTasksReleased( $intTasksReleased ) {
		$this->set( 'm_intTasksReleased', CStrings::strToIntDef( $intTasksReleased, NULL, false ) );
	}

	public function getTasksReleased() {
		return $this->m_intTasksReleased;
	}

	public function sqlTasksReleased() {
		return ( true == isset( $this->m_intTasksReleased ) ) ? ( string ) $this->m_intTasksReleased : '0';
	}

	public function setBugsResolved( $intBugsResolved ) {
		$this->set( 'm_intBugsResolved', CStrings::strToIntDef( $intBugsResolved, NULL, false ) );
	}

	public function getBugsResolved() {
		return $this->m_intBugsResolved;
	}

	public function sqlBugsResolved() {
		return ( true == isset( $this->m_intBugsResolved ) ) ? ( string ) $this->m_intBugsResolved : '0';
	}

	public function setFeaturesReleased( $intFeaturesReleased ) {
		$this->set( 'm_intFeaturesReleased', CStrings::strToIntDef( $intFeaturesReleased, NULL, false ) );
	}

	public function getFeaturesReleased() {
		return $this->m_intFeaturesReleased;
	}

	public function sqlFeaturesReleased() {
		return ( true == isset( $this->m_intFeaturesReleased ) ) ? ( string ) $this->m_intFeaturesReleased : '0';
	}

	public function setHoursWorked( $fltHoursWorked ) {
		$this->set( 'm_fltHoursWorked', CStrings::strToFloatDef( $fltHoursWorked, NULL, false, 2 ) );
	}

	public function getHoursWorked() {
		return $this->m_fltHoursWorked;
	}

	public function sqlHoursWorked() {
		return ( true == isset( $this->m_fltHoursWorked ) ) ? ( string ) $this->m_fltHoursWorked : '0';
	}

	public function setQaRejections( $intQaRejections ) {
		$this->set( 'm_intQaRejections', CStrings::strToIntDef( $intQaRejections, NULL, false ) );
	}

	public function getQaRejections() {
		return $this->m_intQaRejections;
	}

	public function sqlQaRejections() {
		return ( true == isset( $this->m_intQaRejections ) ) ? ( string ) $this->m_intQaRejections : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '0';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setFilesCommitted( $intFilesCommitted ) {
		$this->set( 'm_intFilesCommitted', CStrings::strToIntDef( $intFilesCommitted, NULL, false ) );
	}

	public function getFilesCommitted() {
		return $this->m_intFilesCommitted;
	}

	public function sqlFilesCommitted() {
		return ( true == isset( $this->m_intFilesCommitted ) ) ? ( string ) $this->m_intFilesCommitted : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, week, story_points_released, total_line_insertions, total_line_deletions, files_added, files_deleted, svn_commits, bug_severity, bugs_caused, tasks_released, bugs_resolved, features_released, hours_worked, qa_rejections, created_by, created_on, files_committed )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlEmployeeId() . ', ' .
		          $this->sqlWeek() . ', ' .
		          $this->sqlStoryPointsReleased() . ', ' .
		          $this->sqlTotalLineInsertions() . ', ' .
		          $this->sqlTotalLineDeletions() . ', ' .
		          $this->sqlFilesAdded() . ', ' .
		          $this->sqlFilesDeleted() . ', ' .
		          $this->sqlSvnCommits() . ', ' .
		          $this->sqlBugSeverity() . ', ' .
		          $this->sqlBugsCaused() . ', ' .
		          $this->sqlTasksReleased() . ', ' .
		          $this->sqlBugsResolved() . ', ' .
		          $this->sqlFeaturesReleased() . ', ' .
		          $this->sqlHoursWorked() . ', ' .
		          $this->sqlQaRejections() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlFilesCommitted() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' week = ' . $this->sqlWeek(). ',' ; } elseif( true == array_key_exists( 'Week', $this->getChangedColumns() ) ) { $strSql .= ' week = ' . $this->sqlWeek() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' story_points_released = ' . $this->sqlStoryPointsReleased(). ',' ; } elseif( true == array_key_exists( 'StoryPointsReleased', $this->getChangedColumns() ) ) { $strSql .= ' story_points_released = ' . $this->sqlStoryPointsReleased() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_line_insertions = ' . $this->sqlTotalLineInsertions(). ',' ; } elseif( true == array_key_exists( 'TotalLineInsertions', $this->getChangedColumns() ) ) { $strSql .= ' total_line_insertions = ' . $this->sqlTotalLineInsertions() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_line_deletions = ' . $this->sqlTotalLineDeletions(). ',' ; } elseif( true == array_key_exists( 'TotalLineDeletions', $this->getChangedColumns() ) ) { $strSql .= ' total_line_deletions = ' . $this->sqlTotalLineDeletions() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' files_added = ' . $this->sqlFilesAdded(). ',' ; } elseif( true == array_key_exists( 'FilesAdded', $this->getChangedColumns() ) ) { $strSql .= ' files_added = ' . $this->sqlFilesAdded() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' files_deleted = ' . $this->sqlFilesDeleted(). ',' ; } elseif( true == array_key_exists( 'FilesDeleted', $this->getChangedColumns() ) ) { $strSql .= ' files_deleted = ' . $this->sqlFilesDeleted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_commits = ' . $this->sqlSvnCommits(). ',' ; } elseif( true == array_key_exists( 'SvnCommits', $this->getChangedColumns() ) ) { $strSql .= ' svn_commits = ' . $this->sqlSvnCommits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bug_severity = ' . $this->sqlBugSeverity(). ',' ; } elseif( true == array_key_exists( 'BugSeverity', $this->getChangedColumns() ) ) { $strSql .= ' bug_severity = ' . $this->sqlBugSeverity() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bugs_caused = ' . $this->sqlBugsCaused(). ',' ; } elseif( true == array_key_exists( 'BugsCaused', $this->getChangedColumns() ) ) { $strSql .= ' bugs_caused = ' . $this->sqlBugsCaused() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tasks_released = ' . $this->sqlTasksReleased(). ',' ; } elseif( true == array_key_exists( 'TasksReleased', $this->getChangedColumns() ) ) { $strSql .= ' tasks_released = ' . $this->sqlTasksReleased() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bugs_resolved = ' . $this->sqlBugsResolved(). ',' ; } elseif( true == array_key_exists( 'BugsResolved', $this->getChangedColumns() ) ) { $strSql .= ' bugs_resolved = ' . $this->sqlBugsResolved() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' features_released = ' . $this->sqlFeaturesReleased(). ',' ; } elseif( true == array_key_exists( 'FeaturesReleased', $this->getChangedColumns() ) ) { $strSql .= ' features_released = ' . $this->sqlFeaturesReleased() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hours_worked = ' . $this->sqlHoursWorked(). ',' ; } elseif( true == array_key_exists( 'HoursWorked', $this->getChangedColumns() ) ) { $strSql .= ' hours_worked = ' . $this->sqlHoursWorked() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_rejections = ' . $this->sqlQaRejections(). ',' ; } elseif( true == array_key_exists( 'QaRejections', $this->getChangedColumns() ) ) { $strSql .= ' qa_rejections = ' . $this->sqlQaRejections() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' files_committed = ' . $this->sqlFilesCommitted() ; } elseif( true == array_key_exists( 'FilesCommitted', $this->getChangedColumns() ) ) { $strSql .= ' files_committed = ' . $this->sqlFilesCommitted() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'week' => $this->getWeek(),
			'story_points_released' => $this->getStoryPointsReleased(),
			'total_line_insertions' => $this->getTotalLineInsertions(),
			'total_line_deletions' => $this->getTotalLineDeletions(),
			'files_added' => $this->getFilesAdded(),
			'files_deleted' => $this->getFilesDeleted(),
			'svn_commits' => $this->getSvnCommits(),
			'bug_severity' => $this->getBugSeverity(),
			'bugs_caused' => $this->getBugsCaused(),
			'tasks_released' => $this->getTasksReleased(),
			'bugs_resolved' => $this->getBugsResolved(),
			'features_released' => $this->getFeaturesReleased(),
			'hours_worked' => $this->getHoursWorked(),
			'qa_rejections' => $this->getQaRejections(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'files_committed' => $this->getFilesCommitted()
		);
	}

}
?>