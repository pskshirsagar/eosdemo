<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CInductionSessionAssociations
 * Do not add any new functions to this class.
 */

class CBaseInductionSessionAssociations extends CEosPluralBase {

	/**
	 * @return CInductionSessionAssociation[]
	 */
	public static function fetchInductionSessionAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInductionSessionAssociation', $objDatabase );
	}

	/**
	 * @return CInductionSessionAssociation
	 */
	public static function fetchInductionSessionAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInductionSessionAssociation', $objDatabase );
	}

	public static function fetchInductionSessionAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'induction_session_associations', $objDatabase );
	}

	public static function fetchInductionSessionAssociationById( $intId, $objDatabase ) {
		return self::fetchInductionSessionAssociation( sprintf( 'SELECT * FROM induction_session_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInductionSessionAssociationsByInductionSessionId( $intInductionSessionId, $objDatabase ) {
		return self::fetchInductionSessionAssociations( sprintf( 'SELECT * FROM induction_session_associations WHERE induction_session_id = %d', ( int ) $intInductionSessionId ), $objDatabase );
	}

	public static function fetchInductionSessionAssociationsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchInductionSessionAssociations( sprintf( 'SELECT * FROM induction_session_associations WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchInductionSessionAssociationsByTechnologyId( $intTechnologyId, $objDatabase ) {
		return self::fetchInductionSessionAssociations( sprintf( 'SELECT * FROM induction_session_associations WHERE technology_id = %d', ( int ) $intTechnologyId ), $objDatabase );
	}

	public static function fetchInductionSessionAssociationsByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchInductionSessionAssociations( sprintf( 'SELECT * FROM induction_session_associations WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

}
?>