<?php

class CBaseVirtualForeignKey extends CEosSingularBase {

	const TABLE_NAME = 'public.virtual_foreign_keys';

	protected $m_intId;
	protected $m_strName;
	protected $m_intFrequencyId;
	protected $m_arrintClusterIds;
	protected $m_intSourceDatabaseTypeId;
	protected $m_strSourceTableName;
	protected $m_arrstrSourceFields;
	protected $m_intForeignDatabaseTypeId;
	protected $m_strForeignTableName;
	protected $m_arrstrForeignFields;
	protected $m_intDeveloperEmployeeId;
	protected $m_intQaEmployeeId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intUpdatedBy = '1';
		$this->m_intCreatedBy = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['cluster_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintClusterIds', trim( $arrValues['cluster_ids'] ) ); elseif( isset( $arrValues['cluster_ids'] ) ) $this->setClusterIds( $arrValues['cluster_ids'] );
		if( isset( $arrValues['source_database_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSourceDatabaseTypeId', trim( $arrValues['source_database_type_id'] ) ); elseif( isset( $arrValues['source_database_type_id'] ) ) $this->setSourceDatabaseTypeId( $arrValues['source_database_type_id'] );
		if( isset( $arrValues['source_table_name'] ) && $boolDirectSet ) $this->set( 'm_strSourceTableName', trim( stripcslashes( $arrValues['source_table_name'] ) ) ); elseif( isset( $arrValues['source_table_name'] ) ) $this->setSourceTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['source_table_name'] ) : $arrValues['source_table_name'] );
		if( isset( $arrValues['source_fields'] ) && $boolDirectSet ) $this->set( 'm_arrstrSourceFields', trim( $arrValues['source_fields'] ) ); elseif( isset( $arrValues['source_fields'] ) ) $this->setSourceFields( $arrValues['source_fields'] );
		if( isset( $arrValues['foreign_database_type_id'] ) && $boolDirectSet ) $this->set( 'm_intForeignDatabaseTypeId', trim( $arrValues['foreign_database_type_id'] ) ); elseif( isset( $arrValues['foreign_database_type_id'] ) ) $this->setForeignDatabaseTypeId( $arrValues['foreign_database_type_id'] );
		if( isset( $arrValues['foreign_table_name'] ) && $boolDirectSet ) $this->set( 'm_strForeignTableName', trim( stripcslashes( $arrValues['foreign_table_name'] ) ) ); elseif( isset( $arrValues['foreign_table_name'] ) ) $this->setForeignTableName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['foreign_table_name'] ) : $arrValues['foreign_table_name'] );
		if( isset( $arrValues['foreign_fields'] ) && $boolDirectSet ) $this->set( 'm_arrstrForeignFields', trim( $arrValues['foreign_fields'] ) ); elseif( isset( $arrValues['foreign_fields'] ) ) $this->setForeignFields( $arrValues['foreign_fields'] );
		if( isset( $arrValues['developer_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intDeveloperEmployeeId', trim( $arrValues['developer_employee_id'] ) ); elseif( isset( $arrValues['developer_employee_id'] ) ) $this->setDeveloperEmployeeId( $arrValues['developer_employee_id'] );
		if( isset( $arrValues['qa_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQaEmployeeId', trim( $arrValues['qa_employee_id'] ) ); elseif( isset( $arrValues['qa_employee_id'] ) ) $this->setQaEmployeeId( $arrValues['qa_employee_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setClusterIds( $arrintClusterIds ) {
		$this->set( 'm_arrintClusterIds', CStrings::strToArrIntDef( $arrintClusterIds, NULL ) );
	}

	public function getClusterIds() {
		return $this->m_arrintClusterIds;
	}

	public function sqlClusterIds() {
		return ( true == isset( $this->m_arrintClusterIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintClusterIds, NULL ) . '\'' : 'NULL';
	}

	public function setSourceDatabaseTypeId( $intSourceDatabaseTypeId ) {
		$this->set( 'm_intSourceDatabaseTypeId', CStrings::strToIntDef( $intSourceDatabaseTypeId, NULL, false ) );
	}

	public function getSourceDatabaseTypeId() {
		return $this->m_intSourceDatabaseTypeId;
	}

	public function sqlSourceDatabaseTypeId() {
		return ( true == isset( $this->m_intSourceDatabaseTypeId ) ) ? ( string ) $this->m_intSourceDatabaseTypeId : 'NULL';
	}

	public function setSourceTableName( $strSourceTableName ) {
		$this->set( 'm_strSourceTableName', CStrings::strTrimDef( $strSourceTableName, -1, NULL, true ) );
	}

	public function getSourceTableName() {
		return $this->m_strSourceTableName;
	}

	public function sqlSourceTableName() {
		return ( true == isset( $this->m_strSourceTableName ) ) ? '\'' . addslashes( $this->m_strSourceTableName ) . '\'' : 'NULL';
	}

	public function setSourceFields( $arrstrSourceFields ) {
		$this->set( 'm_arrstrSourceFields', CStrings::strToArrIntDef( $arrstrSourceFields, NULL ) );
	}

	public function getSourceFields() {
		return $this->m_arrstrSourceFields;
	}

	public function sqlSourceFields() {
		return ( true == isset( $this->m_arrstrSourceFields ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrSourceFields, NULL ) . '\'' : 'NULL';
	}

	public function setForeignDatabaseTypeId( $intForeignDatabaseTypeId ) {
		$this->set( 'm_intForeignDatabaseTypeId', CStrings::strToIntDef( $intForeignDatabaseTypeId, NULL, false ) );
	}

	public function getForeignDatabaseTypeId() {
		return $this->m_intForeignDatabaseTypeId;
	}

	public function sqlForeignDatabaseTypeId() {
		return ( true == isset( $this->m_intForeignDatabaseTypeId ) ) ? ( string ) $this->m_intForeignDatabaseTypeId : 'NULL';
	}

	public function setForeignTableName( $strForeignTableName ) {
		$this->set( 'm_strForeignTableName', CStrings::strTrimDef( $strForeignTableName, -1, NULL, true ) );
	}

	public function getForeignTableName() {
		return $this->m_strForeignTableName;
	}

	public function sqlForeignTableName() {
		return ( true == isset( $this->m_strForeignTableName ) ) ? '\'' . addslashes( $this->m_strForeignTableName ) . '\'' : 'NULL';
	}

	public function setForeignFields( $arrstrForeignFields ) {
		$this->set( 'm_arrstrForeignFields', CStrings::strToArrIntDef( $arrstrForeignFields, NULL ) );
	}

	public function getForeignFields() {
		return $this->m_arrstrForeignFields;
	}

	public function sqlForeignFields() {
		return ( true == isset( $this->m_arrstrForeignFields ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrForeignFields, NULL ) . '\'' : 'NULL';
	}

	public function setDeveloperEmployeeId( $intDeveloperEmployeeId ) {
		$this->set( 'm_intDeveloperEmployeeId', CStrings::strToIntDef( $intDeveloperEmployeeId, NULL, false ) );
	}

	public function getDeveloperEmployeeId() {
		return $this->m_intDeveloperEmployeeId;
	}

	public function sqlDeveloperEmployeeId() {
		return ( true == isset( $this->m_intDeveloperEmployeeId ) ) ? ( string ) $this->m_intDeveloperEmployeeId : 'NULL';
	}

	public function setQaEmployeeId( $intQaEmployeeId ) {
		$this->set( 'm_intQaEmployeeId', CStrings::strToIntDef( $intQaEmployeeId, NULL, false ) );
	}

	public function getQaEmployeeId() {
		return $this->m_intQaEmployeeId;
	}

	public function sqlQaEmployeeId() {
		return ( true == isset( $this->m_intQaEmployeeId ) ) ? ( string ) $this->m_intQaEmployeeId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, frequency_id, cluster_ids, source_database_type_id, source_table_name, source_fields, foreign_database_type_id, foreign_table_name, foreign_fields, developer_employee_id, qa_employee_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlFrequencyId() . ', ' .
 						$this->sqlClusterIds() . ', ' .
 						$this->sqlSourceDatabaseTypeId() . ', ' .
 						$this->sqlSourceTableName() . ', ' .
 						$this->sqlSourceFields() . ', ' .
 						$this->sqlForeignDatabaseTypeId() . ', ' .
 						$this->sqlForeignTableName() . ', ' .
 						$this->sqlForeignFields() . ', ' .
 						$this->sqlDeveloperEmployeeId() . ', ' .
 						$this->sqlQaEmployeeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_ids = ' . $this->sqlClusterIds() . ','; } elseif( true == array_key_exists( 'ClusterIds', $this->getChangedColumns() ) ) { $strSql .= ' cluster_ids = ' . $this->sqlClusterIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_database_type_id = ' . $this->sqlSourceDatabaseTypeId() . ','; } elseif( true == array_key_exists( 'SourceDatabaseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' source_database_type_id = ' . $this->sqlSourceDatabaseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_table_name = ' . $this->sqlSourceTableName() . ','; } elseif( true == array_key_exists( 'SourceTableName', $this->getChangedColumns() ) ) { $strSql .= ' source_table_name = ' . $this->sqlSourceTableName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_fields = ' . $this->sqlSourceFields() . ','; } elseif( true == array_key_exists( 'SourceFields', $this->getChangedColumns() ) ) { $strSql .= ' source_fields = ' . $this->sqlSourceFields() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' foreign_database_type_id = ' . $this->sqlForeignDatabaseTypeId() . ','; } elseif( true == array_key_exists( 'ForeignDatabaseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' foreign_database_type_id = ' . $this->sqlForeignDatabaseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' foreign_table_name = ' . $this->sqlForeignTableName() . ','; } elseif( true == array_key_exists( 'ForeignTableName', $this->getChangedColumns() ) ) { $strSql .= ' foreign_table_name = ' . $this->sqlForeignTableName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' foreign_fields = ' . $this->sqlForeignFields() . ','; } elseif( true == array_key_exists( 'ForeignFields', $this->getChangedColumns() ) ) { $strSql .= ' foreign_fields = ' . $this->sqlForeignFields() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' developer_employee_id = ' . $this->sqlDeveloperEmployeeId() . ','; } elseif( true == array_key_exists( 'DeveloperEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' developer_employee_id = ' . $this->sqlDeveloperEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId() . ','; } elseif( true == array_key_exists( 'QaEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'frequency_id' => $this->getFrequencyId(),
			'cluster_ids' => $this->getClusterIds(),
			'source_database_type_id' => $this->getSourceDatabaseTypeId(),
			'source_table_name' => $this->getSourceTableName(),
			'source_fields' => $this->getSourceFields(),
			'foreign_database_type_id' => $this->getForeignDatabaseTypeId(),
			'foreign_table_name' => $this->getForeignTableName(),
			'foreign_fields' => $this->getForeignFields(),
			'developer_employee_id' => $this->getDeveloperEmployeeId(),
			'qa_employee_id' => $this->getQaEmployeeId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>