<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCountryStates
 * Do not add any new functions to this class.
 */

class CBaseCountryStates extends CEosPluralBase {

	/**
	 * @return CCountryState[]
	 */
	public static function fetchCountryStates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCountryState', $objDatabase );
	}

	/**
	 * @return CCountryState
	 */
	public static function fetchCountryState( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCountryState', $objDatabase );
	}

	public static function fetchCountryStateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'country_states', $objDatabase );
	}

	public static function fetchCountryStateById( $intId, $objDatabase ) {
		return self::fetchCountryState( sprintf( 'SELECT * FROM country_states WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>