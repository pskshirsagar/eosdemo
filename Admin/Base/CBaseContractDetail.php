<?php

class CBaseContractDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.contract_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intContractId;
	protected $m_intPilotResultTypeId;
	protected $m_intSignatureCompanyUserId;
	protected $m_intFrequencyId;
	protected $m_strWorkfrontProjectId;
	protected $m_intContractTermMonths;
	protected $m_intImplementationWeightScore;
	protected $m_strForceSignatureOn;
	protected $m_strSignature;
	protected $m_strSignatureTimestamp;
	protected $m_strSignatureIpAddress;
	protected $m_strNewSaleEmailNote;
	protected $m_fltPercentageRenewed;
	protected $m_strPilotSuccessCriteria;
	protected $m_strPilotOpportunityDescription;
	protected $m_strPilotDueDate;
	protected $m_intPilotReviewedBy;
	protected $m_strPilotReviewedOn;
	protected $m_strPilotOutcome;
	protected $m_intIsPilot;
	protected $m_fltContingentAcv;
	protected $m_fltWonAcv;
	protected $m_fltLostAcv;
	protected $m_intIsUniqueCompany;
	protected $m_intHasCustomTerms;
	protected $m_intAutoRenews;
	protected $m_intBillAfterImplementation;
	protected $m_intRequireNewMaster;
	protected $m_intWaivedVariableTerms;
	protected $m_intVerifiedBy;
	protected $m_strVerifiedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intContractTermMonths = '12';
		$this->m_intImplementationWeightScore = '1';
		$this->m_fltPercentageRenewed = '0';
		$this->m_intIsPilot = '0';
		$this->m_fltContingentAcv = '0';
		$this->m_fltWonAcv = '0';
		$this->m_fltLostAcv = '0';
		$this->m_intIsUniqueCompany = '0';
		$this->m_intHasCustomTerms = '0';
		$this->m_intAutoRenews = '1';
		$this->m_intBillAfterImplementation = '0';
		$this->m_intRequireNewMaster = '0';
		$this->m_intWaivedVariableTerms = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['pilot_result_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPilotResultTypeId', trim( $arrValues['pilot_result_type_id'] ) ); elseif( isset( $arrValues['pilot_result_type_id'] ) ) $this->setPilotResultTypeId( $arrValues['pilot_result_type_id'] );
		if( isset( $arrValues['signature_company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intSignatureCompanyUserId', trim( $arrValues['signature_company_user_id'] ) ); elseif( isset( $arrValues['signature_company_user_id'] ) ) $this->setSignatureCompanyUserId( $arrValues['signature_company_user_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['workfront_project_id'] ) && $boolDirectSet ) $this->set( 'm_strWorkfrontProjectId', trim( stripcslashes( $arrValues['workfront_project_id'] ) ) ); elseif( isset( $arrValues['workfront_project_id'] ) ) $this->setWorkfrontProjectId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['workfront_project_id'] ) : $arrValues['workfront_project_id'] );
		if( isset( $arrValues['contract_term_months'] ) && $boolDirectSet ) $this->set( 'm_intContractTermMonths', trim( $arrValues['contract_term_months'] ) ); elseif( isset( $arrValues['contract_term_months'] ) ) $this->setContractTermMonths( $arrValues['contract_term_months'] );
		if( isset( $arrValues['implementation_weight_score'] ) && $boolDirectSet ) $this->set( 'm_intImplementationWeightScore', trim( $arrValues['implementation_weight_score'] ) ); elseif( isset( $arrValues['implementation_weight_score'] ) ) $this->setImplementationWeightScore( $arrValues['implementation_weight_score'] );
		if( isset( $arrValues['force_signature_on'] ) && $boolDirectSet ) $this->set( 'm_strForceSignatureOn', trim( $arrValues['force_signature_on'] ) ); elseif( isset( $arrValues['force_signature_on'] ) ) $this->setForceSignatureOn( $arrValues['force_signature_on'] );
		if( isset( $arrValues['signature'] ) && $boolDirectSet ) $this->set( 'm_strSignature', trim( stripcslashes( $arrValues['signature'] ) ) ); elseif( isset( $arrValues['signature'] ) ) $this->setSignature( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['signature'] ) : $arrValues['signature'] );
		if( isset( $arrValues['signature_timestamp'] ) && $boolDirectSet ) $this->set( 'm_strSignatureTimestamp', trim( $arrValues['signature_timestamp'] ) ); elseif( isset( $arrValues['signature_timestamp'] ) ) $this->setSignatureTimestamp( $arrValues['signature_timestamp'] );
		if( isset( $arrValues['signature_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strSignatureIpAddress', trim( stripcslashes( $arrValues['signature_ip_address'] ) ) ); elseif( isset( $arrValues['signature_ip_address'] ) ) $this->setSignatureIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['signature_ip_address'] ) : $arrValues['signature_ip_address'] );
		if( isset( $arrValues['new_sale_email_note'] ) && $boolDirectSet ) $this->set( 'm_strNewSaleEmailNote', trim( stripcslashes( $arrValues['new_sale_email_note'] ) ) ); elseif( isset( $arrValues['new_sale_email_note'] ) ) $this->setNewSaleEmailNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['new_sale_email_note'] ) : $arrValues['new_sale_email_note'] );
		if( isset( $arrValues['percentage_renewed'] ) && $boolDirectSet ) $this->set( 'm_fltPercentageRenewed', trim( $arrValues['percentage_renewed'] ) ); elseif( isset( $arrValues['percentage_renewed'] ) ) $this->setPercentageRenewed( $arrValues['percentage_renewed'] );
		if( isset( $arrValues['pilot_success_criteria'] ) && $boolDirectSet ) $this->set( 'm_strPilotSuccessCriteria', trim( stripcslashes( $arrValues['pilot_success_criteria'] ) ) ); elseif( isset( $arrValues['pilot_success_criteria'] ) ) $this->setPilotSuccessCriteria( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pilot_success_criteria'] ) : $arrValues['pilot_success_criteria'] );
		if( isset( $arrValues['pilot_opportunity_description'] ) && $boolDirectSet ) $this->set( 'm_strPilotOpportunityDescription', trim( stripcslashes( $arrValues['pilot_opportunity_description'] ) ) ); elseif( isset( $arrValues['pilot_opportunity_description'] ) ) $this->setPilotOpportunityDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pilot_opportunity_description'] ) : $arrValues['pilot_opportunity_description'] );
		if( isset( $arrValues['pilot_due_date'] ) && $boolDirectSet ) $this->set( 'm_strPilotDueDate', trim( $arrValues['pilot_due_date'] ) ); elseif( isset( $arrValues['pilot_due_date'] ) ) $this->setPilotDueDate( $arrValues['pilot_due_date'] );
		if( isset( $arrValues['pilot_reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intPilotReviewedBy', trim( $arrValues['pilot_reviewed_by'] ) ); elseif( isset( $arrValues['pilot_reviewed_by'] ) ) $this->setPilotReviewedBy( $arrValues['pilot_reviewed_by'] );
		if( isset( $arrValues['pilot_reviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strPilotReviewedOn', trim( $arrValues['pilot_reviewed_on'] ) ); elseif( isset( $arrValues['pilot_reviewed_on'] ) ) $this->setPilotReviewedOn( $arrValues['pilot_reviewed_on'] );
		if( isset( $arrValues['pilot_outcome'] ) && $boolDirectSet ) $this->set( 'm_strPilotOutcome', trim( stripcslashes( $arrValues['pilot_outcome'] ) ) ); elseif( isset( $arrValues['pilot_outcome'] ) ) $this->setPilotOutcome( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pilot_outcome'] ) : $arrValues['pilot_outcome'] );
		if( isset( $arrValues['is_pilot'] ) && $boolDirectSet ) $this->set( 'm_intIsPilot', trim( $arrValues['is_pilot'] ) ); elseif( isset( $arrValues['is_pilot'] ) ) $this->setIsPilot( $arrValues['is_pilot'] );
		if( isset( $arrValues['contingent_acv'] ) && $boolDirectSet ) $this->set( 'm_fltContingentAcv', trim( $arrValues['contingent_acv'] ) ); elseif( isset( $arrValues['contingent_acv'] ) ) $this->setContingentAcv( $arrValues['contingent_acv'] );
		if( isset( $arrValues['won_acv'] ) && $boolDirectSet ) $this->set( 'm_fltWonAcv', trim( $arrValues['won_acv'] ) ); elseif( isset( $arrValues['won_acv'] ) ) $this->setWonAcv( $arrValues['won_acv'] );
		if( isset( $arrValues['lost_acv'] ) && $boolDirectSet ) $this->set( 'm_fltLostAcv', trim( $arrValues['lost_acv'] ) ); elseif( isset( $arrValues['lost_acv'] ) ) $this->setLostAcv( $arrValues['lost_acv'] );
		if( isset( $arrValues['is_unique_company'] ) && $boolDirectSet ) $this->set( 'm_intIsUniqueCompany', trim( $arrValues['is_unique_company'] ) ); elseif( isset( $arrValues['is_unique_company'] ) ) $this->setIsUniqueCompany( $arrValues['is_unique_company'] );
		if( isset( $arrValues['has_custom_terms'] ) && $boolDirectSet ) $this->set( 'm_intHasCustomTerms', trim( $arrValues['has_custom_terms'] ) ); elseif( isset( $arrValues['has_custom_terms'] ) ) $this->setHasCustomTerms( $arrValues['has_custom_terms'] );
		if( isset( $arrValues['auto_renews'] ) && $boolDirectSet ) $this->set( 'm_intAutoRenews', trim( $arrValues['auto_renews'] ) ); elseif( isset( $arrValues['auto_renews'] ) ) $this->setAutoRenews( $arrValues['auto_renews'] );
		if( isset( $arrValues['bill_after_implementation'] ) && $boolDirectSet ) $this->set( 'm_intBillAfterImplementation', trim( $arrValues['bill_after_implementation'] ) ); elseif( isset( $arrValues['bill_after_implementation'] ) ) $this->setBillAfterImplementation( $arrValues['bill_after_implementation'] );
		if( isset( $arrValues['require_new_master'] ) && $boolDirectSet ) $this->set( 'm_intRequireNewMaster', trim( $arrValues['require_new_master'] ) ); elseif( isset( $arrValues['require_new_master'] ) ) $this->setRequireNewMaster( $arrValues['require_new_master'] );
		if( isset( $arrValues['waived_variable_terms'] ) && $boolDirectSet ) $this->set( 'm_intWaivedVariableTerms', trim( $arrValues['waived_variable_terms'] ) ); elseif( isset( $arrValues['waived_variable_terms'] ) ) $this->setWaivedVariableTerms( $arrValues['waived_variable_terms'] );
		if( isset( $arrValues['verified_by'] ) && $boolDirectSet ) $this->set( 'm_intVerifiedBy', trim( $arrValues['verified_by'] ) ); elseif( isset( $arrValues['verified_by'] ) ) $this->setVerifiedBy( $arrValues['verified_by'] );
		if( isset( $arrValues['verified_on'] ) && $boolDirectSet ) $this->set( 'm_strVerifiedOn', trim( $arrValues['verified_on'] ) ); elseif( isset( $arrValues['verified_on'] ) ) $this->setVerifiedOn( $arrValues['verified_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setPilotResultTypeId( $intPilotResultTypeId ) {
		$this->set( 'm_intPilotResultTypeId', CStrings::strToIntDef( $intPilotResultTypeId, NULL, false ) );
	}

	public function getPilotResultTypeId() {
		return $this->m_intPilotResultTypeId;
	}

	public function sqlPilotResultTypeId() {
		return ( true == isset( $this->m_intPilotResultTypeId ) ) ? ( string ) $this->m_intPilotResultTypeId : 'NULL';
	}

	public function setSignatureCompanyUserId( $intSignatureCompanyUserId ) {
		$this->set( 'm_intSignatureCompanyUserId', CStrings::strToIntDef( $intSignatureCompanyUserId, NULL, false ) );
	}

	public function getSignatureCompanyUserId() {
		return $this->m_intSignatureCompanyUserId;
	}

	public function sqlSignatureCompanyUserId() {
		return ( true == isset( $this->m_intSignatureCompanyUserId ) ) ? ( string ) $this->m_intSignatureCompanyUserId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setWorkfrontProjectId( $strWorkfrontProjectId ) {
		$this->set( 'm_strWorkfrontProjectId', CStrings::strTrimDef( $strWorkfrontProjectId, 50, NULL, true ) );
	}

	public function getWorkfrontProjectId() {
		return $this->m_strWorkfrontProjectId;
	}

	public function sqlWorkfrontProjectId() {
		return ( true == isset( $this->m_strWorkfrontProjectId ) ) ? '\'' . addslashes( $this->m_strWorkfrontProjectId ) . '\'' : 'NULL';
	}

	public function setContractTermMonths( $intContractTermMonths ) {
		$this->set( 'm_intContractTermMonths', CStrings::strToIntDef( $intContractTermMonths, NULL, false ) );
	}

	public function getContractTermMonths() {
		return $this->m_intContractTermMonths;
	}

	public function sqlContractTermMonths() {
		return ( true == isset( $this->m_intContractTermMonths ) ) ? ( string ) $this->m_intContractTermMonths : '12';
	}

	public function setImplementationWeightScore( $intImplementationWeightScore ) {
		$this->set( 'm_intImplementationWeightScore', CStrings::strToIntDef( $intImplementationWeightScore, NULL, false ) );
	}

	public function getImplementationWeightScore() {
		return $this->m_intImplementationWeightScore;
	}

	public function sqlImplementationWeightScore() {
		return ( true == isset( $this->m_intImplementationWeightScore ) ) ? ( string ) $this->m_intImplementationWeightScore : '1';
	}

	public function setForceSignatureOn( $strForceSignatureOn ) {
		$this->set( 'm_strForceSignatureOn', CStrings::strTrimDef( $strForceSignatureOn, -1, NULL, true ) );
	}

	public function getForceSignatureOn() {
		return $this->m_strForceSignatureOn;
	}

	public function sqlForceSignatureOn() {
		return ( true == isset( $this->m_strForceSignatureOn ) ) ? '\'' . $this->m_strForceSignatureOn . '\'' : 'NULL';
	}

	public function setSignature( $strSignature ) {
		$this->set( 'm_strSignature', CStrings::strTrimDef( $strSignature, 240, NULL, true ) );
	}

	public function getSignature() {
		return $this->m_strSignature;
	}

	public function sqlSignature() {
		return ( true == isset( $this->m_strSignature ) ) ? '\'' . addslashes( $this->m_strSignature ) . '\'' : 'NULL';
	}

	public function setSignatureTimestamp( $strSignatureTimestamp ) {
		$this->set( 'm_strSignatureTimestamp', CStrings::strTrimDef( $strSignatureTimestamp, -1, NULL, true ) );
	}

	public function getSignatureTimestamp() {
		return $this->m_strSignatureTimestamp;
	}

	public function sqlSignatureTimestamp() {
		return ( true == isset( $this->m_strSignatureTimestamp ) ) ? '\'' . $this->m_strSignatureTimestamp . '\'' : 'NULL';
	}

	public function setSignatureIpAddress( $strSignatureIpAddress ) {
		$this->set( 'm_strSignatureIpAddress', CStrings::strTrimDef( $strSignatureIpAddress, 23, NULL, true ) );
	}

	public function getSignatureIpAddress() {
		return $this->m_strSignatureIpAddress;
	}

	public function sqlSignatureIpAddress() {
		return ( true == isset( $this->m_strSignatureIpAddress ) ) ? '\'' . addslashes( $this->m_strSignatureIpAddress ) . '\'' : 'NULL';
	}

	public function setNewSaleEmailNote( $strNewSaleEmailNote ) {
		$this->set( 'm_strNewSaleEmailNote', CStrings::strTrimDef( $strNewSaleEmailNote, -1, NULL, true ) );
	}

	public function getNewSaleEmailNote() {
		return $this->m_strNewSaleEmailNote;
	}

	public function sqlNewSaleEmailNote() {
		return ( true == isset( $this->m_strNewSaleEmailNote ) ) ? '\'' . addslashes( $this->m_strNewSaleEmailNote ) . '\'' : 'NULL';
	}

	public function setPercentageRenewed( $fltPercentageRenewed ) {
		$this->set( 'm_fltPercentageRenewed', CStrings::strToFloatDef( $fltPercentageRenewed, NULL, false, 2 ) );
	}

	public function getPercentageRenewed() {
		return $this->m_fltPercentageRenewed;
	}

	public function sqlPercentageRenewed() {
		return ( true == isset( $this->m_fltPercentageRenewed ) ) ? ( string ) $this->m_fltPercentageRenewed : '0';
	}

	public function setPilotSuccessCriteria( $strPilotSuccessCriteria ) {
		$this->set( 'm_strPilotSuccessCriteria', CStrings::strTrimDef( $strPilotSuccessCriteria, -1, NULL, true ) );
	}

	public function getPilotSuccessCriteria() {
		return $this->m_strPilotSuccessCriteria;
	}

	public function sqlPilotSuccessCriteria() {
		return ( true == isset( $this->m_strPilotSuccessCriteria ) ) ? '\'' . addslashes( $this->m_strPilotSuccessCriteria ) . '\'' : 'NULL';
	}

	public function setPilotOpportunityDescription( $strPilotOpportunityDescription ) {
		$this->set( 'm_strPilotOpportunityDescription', CStrings::strTrimDef( $strPilotOpportunityDescription, -1, NULL, true ) );
	}

	public function getPilotOpportunityDescription() {
		return $this->m_strPilotOpportunityDescription;
	}

	public function sqlPilotOpportunityDescription() {
		return ( true == isset( $this->m_strPilotOpportunityDescription ) ) ? '\'' . addslashes( $this->m_strPilotOpportunityDescription ) . '\'' : 'NULL';
	}

	public function setPilotDueDate( $strPilotDueDate ) {
		$this->set( 'm_strPilotDueDate', CStrings::strTrimDef( $strPilotDueDate, -1, NULL, true ) );
	}

	public function getPilotDueDate() {
		return $this->m_strPilotDueDate;
	}

	public function sqlPilotDueDate() {
		return ( true == isset( $this->m_strPilotDueDate ) ) ? '\'' . $this->m_strPilotDueDate . '\'' : 'NULL';
	}

	public function setPilotReviewedBy( $intPilotReviewedBy ) {
		$this->set( 'm_intPilotReviewedBy', CStrings::strToIntDef( $intPilotReviewedBy, NULL, false ) );
	}

	public function getPilotReviewedBy() {
		return $this->m_intPilotReviewedBy;
	}

	public function sqlPilotReviewedBy() {
		return ( true == isset( $this->m_intPilotReviewedBy ) ) ? ( string ) $this->m_intPilotReviewedBy : 'NULL';
	}

	public function setPilotReviewedOn( $strPilotReviewedOn ) {
		$this->set( 'm_strPilotReviewedOn', CStrings::strTrimDef( $strPilotReviewedOn, -1, NULL, true ) );
	}

	public function getPilotReviewedOn() {
		return $this->m_strPilotReviewedOn;
	}

	public function sqlPilotReviewedOn() {
		return ( true == isset( $this->m_strPilotReviewedOn ) ) ? '\'' . $this->m_strPilotReviewedOn . '\'' : 'NULL';
	}

	public function setPilotOutcome( $strPilotOutcome ) {
		$this->set( 'm_strPilotOutcome', CStrings::strTrimDef( $strPilotOutcome, 240, NULL, true ) );
	}

	public function getPilotOutcome() {
		return $this->m_strPilotOutcome;
	}

	public function sqlPilotOutcome() {
		return ( true == isset( $this->m_strPilotOutcome ) ) ? '\'' . addslashes( $this->m_strPilotOutcome ) . '\'' : 'NULL';
	}

	public function setIsPilot( $intIsPilot ) {
		$this->set( 'm_intIsPilot', CStrings::strToIntDef( $intIsPilot, NULL, false ) );
	}

	public function getIsPilot() {
		return $this->m_intIsPilot;
	}

	public function sqlIsPilot() {
		return ( true == isset( $this->m_intIsPilot ) ) ? ( string ) $this->m_intIsPilot : '0';
	}

	public function setContingentAcv( $fltContingentAcv ) {
		$this->set( 'm_fltContingentAcv', CStrings::strToFloatDef( $fltContingentAcv, NULL, false, 2 ) );
	}

	public function getContingentAcv() {
		return $this->m_fltContingentAcv;
	}

	public function sqlContingentAcv() {
		return ( true == isset( $this->m_fltContingentAcv ) ) ? ( string ) $this->m_fltContingentAcv : '0';
	}

	public function setWonAcv( $fltWonAcv ) {
		$this->set( 'm_fltWonAcv', CStrings::strToFloatDef( $fltWonAcv, NULL, false, 2 ) );
	}

	public function getWonAcv() {
		return $this->m_fltWonAcv;
	}

	public function sqlWonAcv() {
		return ( true == isset( $this->m_fltWonAcv ) ) ? ( string ) $this->m_fltWonAcv : '0';
	}

	public function setLostAcv( $fltLostAcv ) {
		$this->set( 'm_fltLostAcv', CStrings::strToFloatDef( $fltLostAcv, NULL, false, 2 ) );
	}

	public function getLostAcv() {
		return $this->m_fltLostAcv;
	}

	public function sqlLostAcv() {
		return ( true == isset( $this->m_fltLostAcv ) ) ? ( string ) $this->m_fltLostAcv : '0';
	}

	public function setIsUniqueCompany( $intIsUniqueCompany ) {
		$this->set( 'm_intIsUniqueCompany', CStrings::strToIntDef( $intIsUniqueCompany, NULL, false ) );
	}

	public function getIsUniqueCompany() {
		return $this->m_intIsUniqueCompany;
	}

	public function sqlIsUniqueCompany() {
		return ( true == isset( $this->m_intIsUniqueCompany ) ) ? ( string ) $this->m_intIsUniqueCompany : '0';
	}

	public function setHasCustomTerms( $intHasCustomTerms ) {
		$this->set( 'm_intHasCustomTerms', CStrings::strToIntDef( $intHasCustomTerms, NULL, false ) );
	}

	public function getHasCustomTerms() {
		return $this->m_intHasCustomTerms;
	}

	public function sqlHasCustomTerms() {
		return ( true == isset( $this->m_intHasCustomTerms ) ) ? ( string ) $this->m_intHasCustomTerms : '0';
	}

	public function setAutoRenews( $intAutoRenews ) {
		$this->set( 'm_intAutoRenews', CStrings::strToIntDef( $intAutoRenews, NULL, false ) );
	}

	public function getAutoRenews() {
		return $this->m_intAutoRenews;
	}

	public function sqlAutoRenews() {
		return ( true == isset( $this->m_intAutoRenews ) ) ? ( string ) $this->m_intAutoRenews : '1';
	}

	public function setBillAfterImplementation( $intBillAfterImplementation ) {
		$this->set( 'm_intBillAfterImplementation', CStrings::strToIntDef( $intBillAfterImplementation, NULL, false ) );
	}

	public function getBillAfterImplementation() {
		return $this->m_intBillAfterImplementation;
	}

	public function sqlBillAfterImplementation() {
		return ( true == isset( $this->m_intBillAfterImplementation ) ) ? ( string ) $this->m_intBillAfterImplementation : '0';
	}

	public function setRequireNewMaster( $intRequireNewMaster ) {
		$this->set( 'm_intRequireNewMaster', CStrings::strToIntDef( $intRequireNewMaster, NULL, false ) );
	}

	public function getRequireNewMaster() {
		return $this->m_intRequireNewMaster;
	}

	public function sqlRequireNewMaster() {
		return ( true == isset( $this->m_intRequireNewMaster ) ) ? ( string ) $this->m_intRequireNewMaster : '0';
	}

	public function setWaivedVariableTerms( $intWaivedVariableTerms ) {
		$this->set( 'm_intWaivedVariableTerms', CStrings::strToIntDef( $intWaivedVariableTerms, NULL, false ) );
	}

	public function getWaivedVariableTerms() {
		return $this->m_intWaivedVariableTerms;
	}

	public function sqlWaivedVariableTerms() {
		return ( true == isset( $this->m_intWaivedVariableTerms ) ) ? ( string ) $this->m_intWaivedVariableTerms : '0';
	}

	public function setVerifiedBy( $intVerifiedBy ) {
		$this->set( 'm_intVerifiedBy', CStrings::strToIntDef( $intVerifiedBy, NULL, false ) );
	}

	public function getVerifiedBy() {
		return $this->m_intVerifiedBy;
	}

	public function sqlVerifiedBy() {
		return ( true == isset( $this->m_intVerifiedBy ) ) ? ( string ) $this->m_intVerifiedBy : 'NULL';
	}

	public function setVerifiedOn( $strVerifiedOn ) {
		$this->set( 'm_strVerifiedOn', CStrings::strTrimDef( $strVerifiedOn, -1, NULL, true ) );
	}

	public function getVerifiedOn() {
		return $this->m_strVerifiedOn;
	}

	public function sqlVerifiedOn() {
		return ( true == isset( $this->m_strVerifiedOn ) ) ? '\'' . $this->m_strVerifiedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, contract_id, pilot_result_type_id, signature_company_user_id, frequency_id, workfront_project_id, contract_term_months, implementation_weight_score, force_signature_on, signature, signature_timestamp, signature_ip_address, new_sale_email_note, percentage_renewed, pilot_success_criteria, pilot_opportunity_description, pilot_due_date, pilot_reviewed_by, pilot_reviewed_on, pilot_outcome, is_pilot, contingent_acv, won_acv, lost_acv, is_unique_company, has_custom_terms, auto_renews, bill_after_implementation, require_new_master, waived_variable_terms, verified_by, verified_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlContractId() . ', ' .
 						$this->sqlPilotResultTypeId() . ', ' .
 						$this->sqlSignatureCompanyUserId() . ', ' .
 						$this->sqlFrequencyId() . ', ' .
 						$this->sqlWorkfrontProjectId() . ', ' .
 						$this->sqlContractTermMonths() . ', ' .
 						$this->sqlImplementationWeightScore() . ', ' .
 						$this->sqlForceSignatureOn() . ', ' .
 						$this->sqlSignature() . ', ' .
 						$this->sqlSignatureTimestamp() . ', ' .
 						$this->sqlSignatureIpAddress() . ', ' .
 						$this->sqlNewSaleEmailNote() . ', ' .
 						$this->sqlPercentageRenewed() . ', ' .
 						$this->sqlPilotSuccessCriteria() . ', ' .
 						$this->sqlPilotOpportunityDescription() . ', ' .
 						$this->sqlPilotDueDate() . ', ' .
 						$this->sqlPilotReviewedBy() . ', ' .
 						$this->sqlPilotReviewedOn() . ', ' .
 						$this->sqlPilotOutcome() . ', ' .
 						$this->sqlIsPilot() . ', ' .
 						$this->sqlContingentAcv() . ', ' .
 						$this->sqlWonAcv() . ', ' .
 						$this->sqlLostAcv() . ', ' .
 						$this->sqlIsUniqueCompany() . ', ' .
 						$this->sqlHasCustomTerms() . ', ' .
 						$this->sqlAutoRenews() . ', ' .
 						$this->sqlBillAfterImplementation() . ', ' .
 						$this->sqlRequireNewMaster() . ', ' .
 						$this->sqlWaivedVariableTerms() . ', ' .
 						$this->sqlVerifiedBy() . ', ' .
 						$this->sqlVerifiedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_result_type_id = ' . $this->sqlPilotResultTypeId() . ','; } elseif( true == array_key_exists( 'PilotResultTypeId', $this->getChangedColumns() ) ) { $strSql .= ' pilot_result_type_id = ' . $this->sqlPilotResultTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_company_user_id = ' . $this->sqlSignatureCompanyUserId() . ','; } elseif( true == array_key_exists( 'SignatureCompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' signature_company_user_id = ' . $this->sqlSignatureCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' workfront_project_id = ' . $this->sqlWorkfrontProjectId() . ','; } elseif( true == array_key_exists( 'WorkfrontProjectId', $this->getChangedColumns() ) ) { $strSql .= ' workfront_project_id = ' . $this->sqlWorkfrontProjectId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_term_months = ' . $this->sqlContractTermMonths() . ','; } elseif( true == array_key_exists( 'ContractTermMonths', $this->getChangedColumns() ) ) { $strSql .= ' contract_term_months = ' . $this->sqlContractTermMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_weight_score = ' . $this->sqlImplementationWeightScore() . ','; } elseif( true == array_key_exists( 'ImplementationWeightScore', $this->getChangedColumns() ) ) { $strSql .= ' implementation_weight_score = ' . $this->sqlImplementationWeightScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' force_signature_on = ' . $this->sqlForceSignatureOn() . ','; } elseif( true == array_key_exists( 'ForceSignatureOn', $this->getChangedColumns() ) ) { $strSql .= ' force_signature_on = ' . $this->sqlForceSignatureOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature = ' . $this->sqlSignature() . ','; } elseif( true == array_key_exists( 'Signature', $this->getChangedColumns() ) ) { $strSql .= ' signature = ' . $this->sqlSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_timestamp = ' . $this->sqlSignatureTimestamp() . ','; } elseif( true == array_key_exists( 'SignatureTimestamp', $this->getChangedColumns() ) ) { $strSql .= ' signature_timestamp = ' . $this->sqlSignatureTimestamp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature_ip_address = ' . $this->sqlSignatureIpAddress() . ','; } elseif( true == array_key_exists( 'SignatureIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' signature_ip_address = ' . $this->sqlSignatureIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_sale_email_note = ' . $this->sqlNewSaleEmailNote() . ','; } elseif( true == array_key_exists( 'NewSaleEmailNote', $this->getChangedColumns() ) ) { $strSql .= ' new_sale_email_note = ' . $this->sqlNewSaleEmailNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percentage_renewed = ' . $this->sqlPercentageRenewed() . ','; } elseif( true == array_key_exists( 'PercentageRenewed', $this->getChangedColumns() ) ) { $strSql .= ' percentage_renewed = ' . $this->sqlPercentageRenewed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_success_criteria = ' . $this->sqlPilotSuccessCriteria() . ','; } elseif( true == array_key_exists( 'PilotSuccessCriteria', $this->getChangedColumns() ) ) { $strSql .= ' pilot_success_criteria = ' . $this->sqlPilotSuccessCriteria() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_opportunity_description = ' . $this->sqlPilotOpportunityDescription() . ','; } elseif( true == array_key_exists( 'PilotOpportunityDescription', $this->getChangedColumns() ) ) { $strSql .= ' pilot_opportunity_description = ' . $this->sqlPilotOpportunityDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_due_date = ' . $this->sqlPilotDueDate() . ','; } elseif( true == array_key_exists( 'PilotDueDate', $this->getChangedColumns() ) ) { $strSql .= ' pilot_due_date = ' . $this->sqlPilotDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_reviewed_by = ' . $this->sqlPilotReviewedBy() . ','; } elseif( true == array_key_exists( 'PilotReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' pilot_reviewed_by = ' . $this->sqlPilotReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_reviewed_on = ' . $this->sqlPilotReviewedOn() . ','; } elseif( true == array_key_exists( 'PilotReviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' pilot_reviewed_on = ' . $this->sqlPilotReviewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_outcome = ' . $this->sqlPilotOutcome() . ','; } elseif( true == array_key_exists( 'PilotOutcome', $this->getChangedColumns() ) ) { $strSql .= ' pilot_outcome = ' . $this->sqlPilotOutcome() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pilot = ' . $this->sqlIsPilot() . ','; } elseif( true == array_key_exists( 'IsPilot', $this->getChangedColumns() ) ) { $strSql .= ' is_pilot = ' . $this->sqlIsPilot() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contingent_acv = ' . $this->sqlContingentAcv() . ','; } elseif( true == array_key_exists( 'ContingentAcv', $this->getChangedColumns() ) ) { $strSql .= ' contingent_acv = ' . $this->sqlContingentAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' won_acv = ' . $this->sqlWonAcv() . ','; } elseif( true == array_key_exists( 'WonAcv', $this->getChangedColumns() ) ) { $strSql .= ' won_acv = ' . $this->sqlWonAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lost_acv = ' . $this->sqlLostAcv() . ','; } elseif( true == array_key_exists( 'LostAcv', $this->getChangedColumns() ) ) { $strSql .= ' lost_acv = ' . $this->sqlLostAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_unique_company = ' . $this->sqlIsUniqueCompany() . ','; } elseif( true == array_key_exists( 'IsUniqueCompany', $this->getChangedColumns() ) ) { $strSql .= ' is_unique_company = ' . $this->sqlIsUniqueCompany() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_custom_terms = ' . $this->sqlHasCustomTerms() . ','; } elseif( true == array_key_exists( 'HasCustomTerms', $this->getChangedColumns() ) ) { $strSql .= ' has_custom_terms = ' . $this->sqlHasCustomTerms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_renews = ' . $this->sqlAutoRenews() . ','; } elseif( true == array_key_exists( 'AutoRenews', $this->getChangedColumns() ) ) { $strSql .= ' auto_renews = ' . $this->sqlAutoRenews() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bill_after_implementation = ' . $this->sqlBillAfterImplementation() . ','; } elseif( true == array_key_exists( 'BillAfterImplementation', $this->getChangedColumns() ) ) { $strSql .= ' bill_after_implementation = ' . $this->sqlBillAfterImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_new_master = ' . $this->sqlRequireNewMaster() . ','; } elseif( true == array_key_exists( 'RequireNewMaster', $this->getChangedColumns() ) ) { $strSql .= ' require_new_master = ' . $this->sqlRequireNewMaster() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' waived_variable_terms = ' . $this->sqlWaivedVariableTerms() . ','; } elseif( true == array_key_exists( 'WaivedVariableTerms', $this->getChangedColumns() ) ) { $strSql .= ' waived_variable_terms = ' . $this->sqlWaivedVariableTerms() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_by = ' . $this->sqlVerifiedBy() . ','; } elseif( true == array_key_exists( 'VerifiedBy', $this->getChangedColumns() ) ) { $strSql .= ' verified_by = ' . $this->sqlVerifiedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn() . ','; } elseif( true == array_key_exists( 'VerifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' verified_on = ' . $this->sqlVerifiedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'contract_id' => $this->getContractId(),
			'pilot_result_type_id' => $this->getPilotResultTypeId(),
			'signature_company_user_id' => $this->getSignatureCompanyUserId(),
			'frequency_id' => $this->getFrequencyId(),
			'workfront_project_id' => $this->getWorkfrontProjectId(),
			'contract_term_months' => $this->getContractTermMonths(),
			'implementation_weight_score' => $this->getImplementationWeightScore(),
			'force_signature_on' => $this->getForceSignatureOn(),
			'signature' => $this->getSignature(),
			'signature_timestamp' => $this->getSignatureTimestamp(),
			'signature_ip_address' => $this->getSignatureIpAddress(),
			'new_sale_email_note' => $this->getNewSaleEmailNote(),
			'percentage_renewed' => $this->getPercentageRenewed(),
			'pilot_success_criteria' => $this->getPilotSuccessCriteria(),
			'pilot_opportunity_description' => $this->getPilotOpportunityDescription(),
			'pilot_due_date' => $this->getPilotDueDate(),
			'pilot_reviewed_by' => $this->getPilotReviewedBy(),
			'pilot_reviewed_on' => $this->getPilotReviewedOn(),
			'pilot_outcome' => $this->getPilotOutcome(),
			'is_pilot' => $this->getIsPilot(),
			'contingent_acv' => $this->getContingentAcv(),
			'won_acv' => $this->getWonAcv(),
			'lost_acv' => $this->getLostAcv(),
			'is_unique_company' => $this->getIsUniqueCompany(),
			'has_custom_terms' => $this->getHasCustomTerms(),
			'auto_renews' => $this->getAutoRenews(),
			'bill_after_implementation' => $this->getBillAfterImplementation(),
			'require_new_master' => $this->getRequireNewMaster(),
			'waived_variable_terms' => $this->getWaivedVariableTerms(),
			'verified_by' => $this->getVerifiedBy(),
			'verified_on' => $this->getVerifiedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>