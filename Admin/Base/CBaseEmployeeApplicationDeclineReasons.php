<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationDeclineReasons
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationDeclineReasons extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationDeclineReason[]
	 */
	public static function fetchEmployeeApplicationDeclineReasons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeApplicationDeclineReason::class, $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationDeclineReason
	 */
	public static function fetchEmployeeApplicationDeclineReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeApplicationDeclineReason::class, $objDatabase );
	}

	public static function fetchEmployeeApplicationDeclineReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_decline_reasons', $objDatabase );
	}

	public static function fetchEmployeeApplicationDeclineReasonById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationDeclineReason( sprintf( 'SELECT * FROM employee_application_decline_reasons WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>