<?php

class CBaseEmployee extends CEosSingularBase {

	const TABLE_NAME = 'public.employees';

	protected $m_intId;
	protected $m_intEmployeeStatusTypeId;
	protected $m_intEmployeeSalaryTypeId;
	protected $m_intEmployeeApplicationId;
	protected $m_intDepartmentId;
	protected $m_intTechnologyId;
	protected $m_intDesignationId;
	protected $m_intOfficeId;
	protected $m_intOfficeDeskId;
	protected $m_intDeskAllocationOfficeDeskId;
	protected $m_intPhysicalPhoneExtensionId;
	protected $m_intAgentPhoneExtensionId;
	protected $m_strPayrollRemotePrimaryKey;
	protected $m_strNamePrefix;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strPreferredName;
	protected $m_strGender;
	protected $m_strNameSuffix;
	protected $m_strNameFull;
	protected $m_strNameMaiden;
	protected $m_strBirthDate;
	protected $m_strBloodGroup;
	protected $m_strAnniversaryDate;
	protected $m_strTaxNumberMasked;
	protected $m_strTaxNumberEncrypted;
	protected $m_strBankAccountNumberEncrypted;
	protected $m_strPfNumberEncrypted;
	protected $m_strPublicKey;
	protected $m_intEmployeeNumber;
	protected $m_strEmailAddress;
	protected $m_strPermanentEmailAddress;
	protected $m_strGmailUsername;
	protected $m_strBaseSalaryEncrypted;
	protected $m_strDateStarted;
	protected $m_strResignedOn;
	protected $m_strExpectedTerminationDate;
	protected $m_strDateTerminated;
	protected $m_strDateConfirmed;
	protected $m_strMarketingBlurb;
	protected $m_strMarketingBlurbUpdatedOn;
	protected $m_intMaxTaskVotes;
	protected $m_strDnsHandle;
	protected $m_strWorkStationIpAddress;
	protected $m_strVpnIpAddress;
	protected $m_strQaIpAddress;
	protected $m_strNotes;
	protected $m_strEmailSignature;
	protected $m_intIsProjectManager;
	protected $m_intIs360Review;
	protected $m_strLastAssessedOn;
	protected $m_strHandbookDownloadedOn;
	protected $m_strHandbookSignedOn;
	protected $m_strBonusPotentialEncrypted;
	protected $m_intAnnualBonusTypeId;
	protected $m_strBonusRequirement;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strPrivateKey;
	protected $m_intReportingManagerId;
	protected $m_intBusinessUnitId;

	public function __construct() {
		parent::__construct();

		$this->m_intEmployeeSalaryTypeId = '1';
		$this->m_intMaxTaskVotes = '5';
		$this->m_intIs360Review = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeStatusTypeId', trim( $arrValues['employee_status_type_id'] ) ); elseif( isset( $arrValues['employee_status_type_id'] ) ) $this->setEmployeeStatusTypeId( $arrValues['employee_status_type_id'] );
		if( isset( $arrValues['employee_salary_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeSalaryTypeId', trim( $arrValues['employee_salary_type_id'] ) ); elseif( isset( $arrValues['employee_salary_type_id'] ) ) $this->setEmployeeSalaryTypeId( $arrValues['employee_salary_type_id'] );
		if( isset( $arrValues['employee_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationId', trim( $arrValues['employee_application_id'] ) ); elseif( isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['technology_id'] ) && $boolDirectSet ) $this->set( 'm_intTechnologyId', trim( $arrValues['technology_id'] ) ); elseif( isset( $arrValues['technology_id'] ) ) $this->setTechnologyId( $arrValues['technology_id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['office_id'] ) && $boolDirectSet ) $this->set( 'm_intOfficeId', trim( $arrValues['office_id'] ) ); elseif( isset( $arrValues['office_id'] ) ) $this->setOfficeId( $arrValues['office_id'] );
		if( isset( $arrValues['office_desk_id'] ) && $boolDirectSet ) $this->set( 'm_intOfficeDeskId', trim( $arrValues['office_desk_id'] ) ); elseif( isset( $arrValues['office_desk_id'] ) ) $this->setOfficeDeskId( $arrValues['office_desk_id'] );
		if( isset( $arrValues['desk_allocation_office_desk_id'] ) && $boolDirectSet ) $this->set( 'm_intDeskAllocationOfficeDeskId', trim( $arrValues['desk_allocation_office_desk_id'] ) ); elseif( isset( $arrValues['desk_allocation_office_desk_id'] ) ) $this->setDeskAllocationOfficeDeskId( $arrValues['desk_allocation_office_desk_id'] );
		if( isset( $arrValues['physical_phone_extension_id'] ) && $boolDirectSet ) $this->set( 'm_intPhysicalPhoneExtensionId', trim( $arrValues['physical_phone_extension_id'] ) ); elseif( isset( $arrValues['physical_phone_extension_id'] ) ) $this->setPhysicalPhoneExtensionId( $arrValues['physical_phone_extension_id'] );
		if( isset( $arrValues['agent_phone_extension_id'] ) && $boolDirectSet ) $this->set( 'm_intAgentPhoneExtensionId', trim( $arrValues['agent_phone_extension_id'] ) ); elseif( isset( $arrValues['agent_phone_extension_id'] ) ) $this->setAgentPhoneExtensionId( $arrValues['agent_phone_extension_id'] );
		if( isset( $arrValues['payroll_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strPayrollRemotePrimaryKey', trim( $arrValues['payroll_remote_primary_key'] ) ); elseif( isset( $arrValues['payroll_remote_primary_key'] ) ) $this->setPayrollRemotePrimaryKey( $arrValues['payroll_remote_primary_key'] );
		if( isset( $arrValues['name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strNamePrefix', trim( $arrValues['name_prefix'] ) ); elseif( isset( $arrValues['name_prefix'] ) ) $this->setNamePrefix( $arrValues['name_prefix'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( $arrValues['name_middle'] ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['preferred_name'] ) && $boolDirectSet ) $this->set( 'm_strPreferredName', trim( $arrValues['preferred_name'] ) ); elseif( isset( $arrValues['preferred_name'] ) ) $this->setPreferredName( $arrValues['preferred_name'] );
		if( isset( $arrValues['gender'] ) && $boolDirectSet ) $this->set( 'm_strGender', trim( $arrValues['gender'] ) ); elseif( isset( $arrValues['gender'] ) ) $this->setGender( $arrValues['gender'] );
		if( isset( $arrValues['name_suffix'] ) && $boolDirectSet ) $this->set( 'm_strNameSuffix', trim( $arrValues['name_suffix'] ) ); elseif( isset( $arrValues['name_suffix'] ) ) $this->setNameSuffix( $arrValues['name_suffix'] );
		if( isset( $arrValues['name_full'] ) && $boolDirectSet ) $this->set( 'm_strNameFull', trim( $arrValues['name_full'] ) ); elseif( isset( $arrValues['name_full'] ) ) $this->setNameFull( $arrValues['name_full'] );
		if( isset( $arrValues['name_maiden'] ) && $boolDirectSet ) $this->set( 'm_strNameMaiden', trim( $arrValues['name_maiden'] ) ); elseif( isset( $arrValues['name_maiden'] ) ) $this->setNameMaiden( $arrValues['name_maiden'] );
		if( isset( $arrValues['birth_date'] ) && $boolDirectSet ) $this->set( 'm_strBirthDate', trim( $arrValues['birth_date'] ) ); elseif( isset( $arrValues['birth_date'] ) ) $this->setBirthDate( $arrValues['birth_date'] );
		if( isset( $arrValues['blood_group'] ) && $boolDirectSet ) $this->set( 'm_strBloodGroup', trim( $arrValues['blood_group'] ) ); elseif( isset( $arrValues['blood_group'] ) ) $this->setBloodGroup( $arrValues['blood_group'] );
		if( isset( $arrValues['anniversary_date'] ) && $boolDirectSet ) $this->set( 'm_strAnniversaryDate', trim( $arrValues['anniversary_date'] ) ); elseif( isset( $arrValues['anniversary_date'] ) ) $this->setAnniversaryDate( $arrValues['anniversary_date'] );
		if( isset( $arrValues['tax_number_masked'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberMasked', trim( $arrValues['tax_number_masked'] ) ); elseif( isset( $arrValues['tax_number_masked'] ) ) $this->setTaxNumberMasked( $arrValues['tax_number_masked'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( $arrValues['tax_number_encrypted'] ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['bank_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBankAccountNumberEncrypted', trim( $arrValues['bank_account_number_encrypted'] ) ); elseif( isset( $arrValues['bank_account_number_encrypted'] ) ) $this->setBankAccountNumberEncrypted( $arrValues['bank_account_number_encrypted'] );
		if( isset( $arrValues['pf_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPfNumberEncrypted', trim( $arrValues['pf_number_encrypted'] ) ); elseif( isset( $arrValues['pf_number_encrypted'] ) ) $this->setPfNumberEncrypted( $arrValues['pf_number_encrypted'] );
		if( isset( $arrValues['public_key'] ) && $boolDirectSet ) $this->set( 'm_strPublicKey', trim( $arrValues['public_key'] ) ); elseif( isset( $arrValues['public_key'] ) ) $this->setPublicKey( $arrValues['public_key'] );
		if( isset( $arrValues['employee_number'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeNumber', trim( $arrValues['employee_number'] ) ); elseif( isset( $arrValues['employee_number'] ) ) $this->setEmployeeNumber( $arrValues['employee_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['permanent_email_address'] ) && $boolDirectSet ) $this->set( 'm_strPermanentEmailAddress', trim( $arrValues['permanent_email_address'] ) ); elseif( isset( $arrValues['permanent_email_address'] ) ) $this->setPermanentEmailAddress( $arrValues['permanent_email_address'] );
		if( isset( $arrValues['gmail_username'] ) && $boolDirectSet ) $this->set( 'm_strGmailUsername', trim( $arrValues['gmail_username'] ) ); elseif( isset( $arrValues['gmail_username'] ) ) $this->setGmailUsername( $arrValues['gmail_username'] );
		if( isset( $arrValues['base_salary_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBaseSalaryEncrypted', trim( $arrValues['base_salary_encrypted'] ) ); elseif( isset( $arrValues['base_salary_encrypted'] ) ) $this->setBaseSalaryEncrypted( $arrValues['base_salary_encrypted'] );
		if( isset( $arrValues['date_started'] ) && $boolDirectSet ) $this->set( 'm_strDateStarted', trim( $arrValues['date_started'] ) ); elseif( isset( $arrValues['date_started'] ) ) $this->setDateStarted( $arrValues['date_started'] );
		if( isset( $arrValues['resigned_on'] ) && $boolDirectSet ) $this->set( 'm_strResignedOn', trim( $arrValues['resigned_on'] ) ); elseif( isset( $arrValues['resigned_on'] ) ) $this->setResignedOn( $arrValues['resigned_on'] );
		if( isset( $arrValues['expected_termination_date'] ) && $boolDirectSet ) $this->set( 'm_strExpectedTerminationDate', trim( $arrValues['expected_termination_date'] ) ); elseif( isset( $arrValues['expected_termination_date'] ) ) $this->setExpectedTerminationDate( $arrValues['expected_termination_date'] );
		if( isset( $arrValues['date_terminated'] ) && $boolDirectSet ) $this->set( 'm_strDateTerminated', trim( $arrValues['date_terminated'] ) ); elseif( isset( $arrValues['date_terminated'] ) ) $this->setDateTerminated( $arrValues['date_terminated'] );
		if( isset( $arrValues['date_confirmed'] ) && $boolDirectSet ) $this->set( 'm_strDateConfirmed', trim( $arrValues['date_confirmed'] ) ); elseif( isset( $arrValues['date_confirmed'] ) ) $this->setDateConfirmed( $arrValues['date_confirmed'] );
		if( isset( $arrValues['marketing_blurb'] ) && $boolDirectSet ) $this->set( 'm_strMarketingBlurb', trim( $arrValues['marketing_blurb'] ) ); elseif( isset( $arrValues['marketing_blurb'] ) ) $this->setMarketingBlurb( $arrValues['marketing_blurb'] );
		if( isset( $arrValues['marketing_blurb_updated_on'] ) && $boolDirectSet ) $this->set( 'm_strMarketingBlurbUpdatedOn', trim( $arrValues['marketing_blurb_updated_on'] ) ); elseif( isset( $arrValues['marketing_blurb_updated_on'] ) ) $this->setMarketingBlurbUpdatedOn( $arrValues['marketing_blurb_updated_on'] );
		if( isset( $arrValues['max_task_votes'] ) && $boolDirectSet ) $this->set( 'm_intMaxTaskVotes', trim( $arrValues['max_task_votes'] ) ); elseif( isset( $arrValues['max_task_votes'] ) ) $this->setMaxTaskVotes( $arrValues['max_task_votes'] );
		if( isset( $arrValues['dns_handle'] ) && $boolDirectSet ) $this->set( 'm_strDnsHandle', trim( $arrValues['dns_handle'] ) ); elseif( isset( $arrValues['dns_handle'] ) ) $this->setDnsHandle( $arrValues['dns_handle'] );
		if( isset( $arrValues['work_station_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strWorkStationIpAddress', trim( $arrValues['work_station_ip_address'] ) ); elseif( isset( $arrValues['work_station_ip_address'] ) ) $this->setWorkStationIpAddress( $arrValues['work_station_ip_address'] );
		if( isset( $arrValues['vpn_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strVpnIpAddress', trim( $arrValues['vpn_ip_address'] ) ); elseif( isset( $arrValues['vpn_ip_address'] ) ) $this->setVpnIpAddress( $arrValues['vpn_ip_address'] );
		if( isset( $arrValues['qa_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strQaIpAddress', trim( $arrValues['qa_ip_address'] ) ); elseif( isset( $arrValues['qa_ip_address'] ) ) $this->setQaIpAddress( $arrValues['qa_ip_address'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['email_signature'] ) && $boolDirectSet ) $this->set( 'm_strEmailSignature', trim( $arrValues['email_signature'] ) ); elseif( isset( $arrValues['email_signature'] ) ) $this->setEmailSignature( $arrValues['email_signature'] );
		if( isset( $arrValues['is_project_manager'] ) && $boolDirectSet ) $this->set( 'm_intIsProjectManager', trim( $arrValues['is_project_manager'] ) ); elseif( isset( $arrValues['is_project_manager'] ) ) $this->setIsProjectManager( $arrValues['is_project_manager'] );
		if( isset( $arrValues['is_360_review'] ) && $boolDirectSet ) $this->set( 'm_intIs360Review', trim( $arrValues['is_360_review'] ) ); elseif( isset( $arrValues['is_360_review'] ) ) $this->setIs360Review( $arrValues['is_360_review'] );
		if( isset( $arrValues['last_assessed_on'] ) && $boolDirectSet ) $this->set( 'm_strLastAssessedOn', trim( $arrValues['last_assessed_on'] ) ); elseif( isset( $arrValues['last_assessed_on'] ) ) $this->setLastAssessedOn( $arrValues['last_assessed_on'] );
		if( isset( $arrValues['handbook_downloaded_on'] ) && $boolDirectSet ) $this->set( 'm_strHandbookDownloadedOn', trim( $arrValues['handbook_downloaded_on'] ) ); elseif( isset( $arrValues['handbook_downloaded_on'] ) ) $this->setHandbookDownloadedOn( $arrValues['handbook_downloaded_on'] );
		if( isset( $arrValues['handbook_signed_on'] ) && $boolDirectSet ) $this->set( 'm_strHandbookSignedOn', trim( $arrValues['handbook_signed_on'] ) ); elseif( isset( $arrValues['handbook_signed_on'] ) ) $this->setHandbookSignedOn( $arrValues['handbook_signed_on'] );
		if( isset( $arrValues['bonus_potential_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBonusPotentialEncrypted', trim( $arrValues['bonus_potential_encrypted'] ) ); elseif( isset( $arrValues['bonus_potential_encrypted'] ) ) $this->setBonusPotentialEncrypted( $arrValues['bonus_potential_encrypted'] );
		if( isset( $arrValues['annual_bonus_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAnnualBonusTypeId', trim( $arrValues['annual_bonus_type_id'] ) ); elseif( isset( $arrValues['annual_bonus_type_id'] ) ) $this->setAnnualBonusTypeId( $arrValues['annual_bonus_type_id'] );
		if( isset( $arrValues['bonus_requirement'] ) && $boolDirectSet ) $this->set( 'm_strBonusRequirement', trim( $arrValues['bonus_requirement'] ) ); elseif( isset( $arrValues['bonus_requirement'] ) ) $this->setBonusRequirement( $arrValues['bonus_requirement'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['private_key'] ) && $boolDirectSet ) $this->set( 'm_strPrivateKey', trim( $arrValues['private_key'] ) ); elseif( isset( $arrValues['private_key'] ) ) $this->setPrivateKey( $arrValues['private_key'] );
		if( isset( $arrValues['reporting_manager_id'] ) && $boolDirectSet ) $this->set( 'm_intReportingManagerId', trim( $arrValues['reporting_manager_id'] ) ); elseif( isset( $arrValues['reporting_manager_id'] ) ) $this->setReportingManagerId( $arrValues['reporting_manager_id'] );
		if( isset( $arrValues['business_unit_id'] ) && $boolDirectSet ) $this->set( 'm_intBusinessUnitId', trim( $arrValues['business_unit_id'] ) ); elseif( isset( $arrValues['business_unit_id'] ) ) $this->setBusinessUnitId( $arrValues['business_unit_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeStatusTypeId( $intEmployeeStatusTypeId ) {
		$this->set( 'm_intEmployeeStatusTypeId', CStrings::strToIntDef( $intEmployeeStatusTypeId, NULL, false ) );
	}

	public function getEmployeeStatusTypeId() {
		return $this->m_intEmployeeStatusTypeId;
	}

	public function sqlEmployeeStatusTypeId() {
		return ( true == isset( $this->m_intEmployeeStatusTypeId ) ) ? ( string ) $this->m_intEmployeeStatusTypeId : 'NULL';
	}

	public function setEmployeeSalaryTypeId( $intEmployeeSalaryTypeId ) {
		$this->set( 'm_intEmployeeSalaryTypeId', CStrings::strToIntDef( $intEmployeeSalaryTypeId, NULL, false ) );
	}

	public function getEmployeeSalaryTypeId() {
		return $this->m_intEmployeeSalaryTypeId;
	}

	public function sqlEmployeeSalaryTypeId() {
		return ( true == isset( $this->m_intEmployeeSalaryTypeId ) ) ? ( string ) $this->m_intEmployeeSalaryTypeId : '1';
	}

	public function setEmployeeApplicationId( $intEmployeeApplicationId ) {
		$this->set( 'm_intEmployeeApplicationId', CStrings::strToIntDef( $intEmployeeApplicationId, NULL, false ) );
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function sqlEmployeeApplicationId() {
		return ( true == isset( $this->m_intEmployeeApplicationId ) ) ? ( string ) $this->m_intEmployeeApplicationId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setTechnologyId( $intTechnologyId ) {
		$this->set( 'm_intTechnologyId', CStrings::strToIntDef( $intTechnologyId, NULL, false ) );
	}

	public function getTechnologyId() {
		return $this->m_intTechnologyId;
	}

	public function sqlTechnologyId() {
		return ( true == isset( $this->m_intTechnologyId ) ) ? ( string ) $this->m_intTechnologyId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setOfficeId( $intOfficeId ) {
		$this->set( 'm_intOfficeId', CStrings::strToIntDef( $intOfficeId, NULL, false ) );
	}

	public function getOfficeId() {
		return $this->m_intOfficeId;
	}

	public function sqlOfficeId() {
		return ( true == isset( $this->m_intOfficeId ) ) ? ( string ) $this->m_intOfficeId : 'NULL';
	}

	public function setOfficeDeskId( $intOfficeDeskId ) {
		$this->set( 'm_intOfficeDeskId', CStrings::strToIntDef( $intOfficeDeskId, NULL, false ) );
	}

	public function getOfficeDeskId() {
		return $this->m_intOfficeDeskId;
	}

	public function sqlOfficeDeskId() {
		return ( true == isset( $this->m_intOfficeDeskId ) ) ? ( string ) $this->m_intOfficeDeskId : 'NULL';
	}

	public function setDeskAllocationOfficeDeskId( $intDeskAllocationOfficeDeskId ) {
		$this->set( 'm_intDeskAllocationOfficeDeskId', CStrings::strToIntDef( $intDeskAllocationOfficeDeskId, NULL, false ) );
	}

	public function getDeskAllocationOfficeDeskId() {
		return $this->m_intDeskAllocationOfficeDeskId;
	}

	public function sqlDeskAllocationOfficeDeskId() {
		return ( true == isset( $this->m_intDeskAllocationOfficeDeskId ) ) ? ( string ) $this->m_intDeskAllocationOfficeDeskId : 'NULL';
	}

	public function setPhysicalPhoneExtensionId( $intPhysicalPhoneExtensionId ) {
		$this->set( 'm_intPhysicalPhoneExtensionId', CStrings::strToIntDef( $intPhysicalPhoneExtensionId, NULL, false ) );
	}

	public function getPhysicalPhoneExtensionId() {
		return $this->m_intPhysicalPhoneExtensionId;
	}

	public function sqlPhysicalPhoneExtensionId() {
		return ( true == isset( $this->m_intPhysicalPhoneExtensionId ) ) ? ( string ) $this->m_intPhysicalPhoneExtensionId : 'NULL';
	}

	public function setAgentPhoneExtensionId( $intAgentPhoneExtensionId ) {
		$this->set( 'm_intAgentPhoneExtensionId', CStrings::strToIntDef( $intAgentPhoneExtensionId, NULL, false ) );
	}

	public function getAgentPhoneExtensionId() {
		return $this->m_intAgentPhoneExtensionId;
	}

	public function sqlAgentPhoneExtensionId() {
		return ( true == isset( $this->m_intAgentPhoneExtensionId ) ) ? ( string ) $this->m_intAgentPhoneExtensionId : 'NULL';
	}

	public function setPayrollRemotePrimaryKey( $strPayrollRemotePrimaryKey ) {
		$this->set( 'm_strPayrollRemotePrimaryKey', CStrings::strTrimDef( $strPayrollRemotePrimaryKey, 200, NULL, true ) );
	}

	public function getPayrollRemotePrimaryKey() {
		return $this->m_strPayrollRemotePrimaryKey;
	}

	public function sqlPayrollRemotePrimaryKey() {
		return ( true == isset( $this->m_strPayrollRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPayrollRemotePrimaryKey ) : '\'' . addslashes( $this->m_strPayrollRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setNamePrefix( $strNamePrefix ) {
		$this->set( 'm_strNamePrefix', CStrings::strTrimDef( $strNamePrefix, 20, NULL, true ) );
	}

	public function getNamePrefix() {
		return $this->m_strNamePrefix;
	}

	public function sqlNamePrefix() {
		return ( true == isset( $this->m_strNamePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNamePrefix ) : '\'' . addslashes( $this->m_strNamePrefix ) . '\'' ) : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 50, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMiddle ) : '\'' . addslashes( $this->m_strNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setPreferredName( $strPreferredName ) {
		$this->set( 'm_strPreferredName', CStrings::strTrimDef( $strPreferredName, 100, NULL, true ) );
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function sqlPreferredName() {
		return ( true == isset( $this->m_strPreferredName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreferredName ) : '\'' . addslashes( $this->m_strPreferredName ) . '\'' ) : 'NULL';
	}

	public function setGender( $strGender ) {
		$this->set( 'm_strGender', CStrings::strTrimDef( $strGender, 1, NULL, true ) );
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function sqlGender() {
		return ( true == isset( $this->m_strGender ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGender ) : '\'' . addslashes( $this->m_strGender ) . '\'' ) : 'NULL';
	}

	public function setNameSuffix( $strNameSuffix ) {
		$this->set( 'm_strNameSuffix', CStrings::strTrimDef( $strNameSuffix, 20, NULL, true ) );
	}

	public function getNameSuffix() {
		return $this->m_strNameSuffix;
	}

	public function sqlNameSuffix() {
		return ( true == isset( $this->m_strNameSuffix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameSuffix ) : '\'' . addslashes( $this->m_strNameSuffix ) . '\'' ) : 'NULL';
	}

	public function setNameFull( $strNameFull ) {
		$this->set( 'm_strNameFull', CStrings::strTrimDef( $strNameFull, 100, NULL, true ) );
	}

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function sqlNameFull() {
		return ( true == isset( $this->m_strNameFull ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFull ) : '\'' . addslashes( $this->m_strNameFull ) . '\'' ) : 'NULL';
	}

	public function setNameMaiden( $strNameMaiden ) {
		$this->set( 'm_strNameMaiden', CStrings::strTrimDef( $strNameMaiden, 50, NULL, true ) );
	}

	public function getNameMaiden() {
		return $this->m_strNameMaiden;
	}

	public function sqlNameMaiden() {
		return ( true == isset( $this->m_strNameMaiden ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMaiden ) : '\'' . addslashes( $this->m_strNameMaiden ) . '\'' ) : 'NULL';
	}

	public function setBirthDate( $strBirthDate ) {
		$this->set( 'm_strBirthDate', CStrings::strTrimDef( $strBirthDate, -1, NULL, true ) );
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function sqlBirthDate() {
		return ( true == isset( $this->m_strBirthDate ) ) ? '\'' . $this->m_strBirthDate . '\'' : 'NULL';
	}

	public function setBloodGroup( $strBloodGroup ) {
		$this->set( 'm_strBloodGroup', CStrings::strTrimDef( $strBloodGroup, 5, NULL, true ) );
	}

	public function getBloodGroup() {
		return $this->m_strBloodGroup;
	}

	public function sqlBloodGroup() {
		return ( true == isset( $this->m_strBloodGroup ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBloodGroup ) : '\'' . addslashes( $this->m_strBloodGroup ) . '\'' ) : 'NULL';
	}

	public function setAnniversaryDate( $strAnniversaryDate ) {
		$this->set( 'm_strAnniversaryDate', CStrings::strTrimDef( $strAnniversaryDate, -1, NULL, true ) );
	}

	public function getAnniversaryDate() {
		return $this->m_strAnniversaryDate;
	}

	public function sqlAnniversaryDate() {
		return ( true == isset( $this->m_strAnniversaryDate ) ) ? '\'' . $this->m_strAnniversaryDate . '\'' : 'NULL';
	}

	public function setTaxNumberMasked( $strTaxNumberMasked ) {
		$this->set( 'm_strTaxNumberMasked', CStrings::strTrimDef( $strTaxNumberMasked, 240, NULL, true ) );
	}

	public function getTaxNumberMasked() {
		return $this->m_strTaxNumberMasked;
	}

	public function sqlTaxNumberMasked() {
		return ( true == isset( $this->m_strTaxNumberMasked ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberMasked ) : '\'' . addslashes( $this->m_strTaxNumberMasked ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberEncrypted ) : '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setBankAccountNumberEncrypted( $strBankAccountNumberEncrypted ) {
		$this->set( 'm_strBankAccountNumberEncrypted', CStrings::strTrimDef( $strBankAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getBankAccountNumberEncrypted() {
		return $this->m_strBankAccountNumberEncrypted;
	}

	public function sqlBankAccountNumberEncrypted() {
		return ( true == isset( $this->m_strBankAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBankAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strBankAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setPfNumberEncrypted( $strPfNumberEncrypted ) {
		$this->set( 'm_strPfNumberEncrypted', CStrings::strTrimDef( $strPfNumberEncrypted, 240, NULL, true ) );
	}

	public function getPfNumberEncrypted() {
		return $this->m_strPfNumberEncrypted;
	}

	public function sqlPfNumberEncrypted() {
		return ( true == isset( $this->m_strPfNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPfNumberEncrypted ) : '\'' . addslashes( $this->m_strPfNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setPublicKey( $strPublicKey ) {
		$this->set( 'm_strPublicKey', CStrings::strTrimDef( $strPublicKey, -1, NULL, true ) );
	}

	public function getPublicKey() {
		return $this->m_strPublicKey;
	}

	public function sqlPublicKey() {
		return ( true == isset( $this->m_strPublicKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPublicKey ) : '\'' . addslashes( $this->m_strPublicKey ) . '\'' ) : 'NULL';
	}

	public function setEmployeeNumber( $intEmployeeNumber ) {
		$this->set( 'm_intEmployeeNumber', CStrings::strToIntDef( $intEmployeeNumber, NULL, false ) );
	}

	public function getEmployeeNumber() {
		return $this->m_intEmployeeNumber;
	}

	public function sqlEmployeeNumber() {
		return ( true == isset( $this->m_intEmployeeNumber ) ) ? ( string ) $this->m_intEmployeeNumber : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setPermanentEmailAddress( $strPermanentEmailAddress ) {
		$this->set( 'm_strPermanentEmailAddress', CStrings::strTrimDef( $strPermanentEmailAddress, 240, NULL, true ) );
	}

	public function getPermanentEmailAddress() {
		return $this->m_strPermanentEmailAddress;
	}

	public function sqlPermanentEmailAddress() {
		return ( true == isset( $this->m_strPermanentEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPermanentEmailAddress ) : '\'' . addslashes( $this->m_strPermanentEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setGmailUsername( $strGmailUsername ) {
		$this->set( 'm_strGmailUsername', CStrings::strTrimDef( $strGmailUsername, 240, NULL, true ) );
	}

	public function getGmailUsername() {
		return $this->m_strGmailUsername;
	}

	public function sqlGmailUsername() {
		return ( true == isset( $this->m_strGmailUsername ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGmailUsername ) : '\'' . addslashes( $this->m_strGmailUsername ) . '\'' ) : 'NULL';
	}

	public function setBaseSalaryEncrypted( $strBaseSalaryEncrypted ) {
		$this->set( 'm_strBaseSalaryEncrypted', CStrings::strTrimDef( $strBaseSalaryEncrypted, 1000, NULL, true ) );
	}

	public function getBaseSalaryEncrypted() {
		return $this->m_strBaseSalaryEncrypted;
	}

	public function sqlBaseSalaryEncrypted() {
		return ( true == isset( $this->m_strBaseSalaryEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBaseSalaryEncrypted ) : '\'' . addslashes( $this->m_strBaseSalaryEncrypted ) . '\'' ) : 'NULL';
	}

	public function setDateStarted( $strDateStarted ) {
		$this->set( 'm_strDateStarted', CStrings::strTrimDef( $strDateStarted, -1, NULL, true ) );
	}

	public function getDateStarted() {
		return $this->m_strDateStarted;
	}

	public function sqlDateStarted() {
		return ( true == isset( $this->m_strDateStarted ) ) ? '\'' . $this->m_strDateStarted . '\'' : 'NULL';
	}

	public function setResignedOn( $strResignedOn ) {
		$this->set( 'm_strResignedOn', CStrings::strTrimDef( $strResignedOn, -1, NULL, true ) );
	}

	public function getResignedOn() {
		return $this->m_strResignedOn;
	}

	public function sqlResignedOn() {
		return ( true == isset( $this->m_strResignedOn ) ) ? '\'' . $this->m_strResignedOn . '\'' : 'NULL';
	}

	public function setExpectedTerminationDate( $strExpectedTerminationDate ) {
		$this->set( 'm_strExpectedTerminationDate', CStrings::strTrimDef( $strExpectedTerminationDate, -1, NULL, true ) );
	}

	public function getExpectedTerminationDate() {
		return $this->m_strExpectedTerminationDate;
	}

	public function sqlExpectedTerminationDate() {
		return ( true == isset( $this->m_strExpectedTerminationDate ) ) ? '\'' . $this->m_strExpectedTerminationDate . '\'' : 'NULL';
	}

	public function setDateTerminated( $strDateTerminated ) {
		$this->set( 'm_strDateTerminated', CStrings::strTrimDef( $strDateTerminated, -1, NULL, true ) );
	}

	public function getDateTerminated() {
		return $this->m_strDateTerminated;
	}

	public function sqlDateTerminated() {
		return ( true == isset( $this->m_strDateTerminated ) ) ? '\'' . $this->m_strDateTerminated . '\'' : 'NULL';
	}

	public function setDateConfirmed( $strDateConfirmed ) {
		$this->set( 'm_strDateConfirmed', CStrings::strTrimDef( $strDateConfirmed, -1, NULL, true ) );
	}

	public function getDateConfirmed() {
		return $this->m_strDateConfirmed;
	}

	public function sqlDateConfirmed() {
		return ( true == isset( $this->m_strDateConfirmed ) ) ? '\'' . $this->m_strDateConfirmed . '\'' : 'NULL';
	}

	public function setMarketingBlurb( $strMarketingBlurb ) {
		$this->set( 'm_strMarketingBlurb', CStrings::strTrimDef( $strMarketingBlurb, -1, NULL, true ) );
	}

	public function getMarketingBlurb() {
		return $this->m_strMarketingBlurb;
	}

	public function sqlMarketingBlurb() {
		return ( true == isset( $this->m_strMarketingBlurb ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMarketingBlurb ) : '\'' . addslashes( $this->m_strMarketingBlurb ) . '\'' ) : 'NULL';
	}

	public function setMarketingBlurbUpdatedOn( $strMarketingBlurbUpdatedOn ) {
		$this->set( 'm_strMarketingBlurbUpdatedOn', CStrings::strTrimDef( $strMarketingBlurbUpdatedOn, -1, NULL, true ) );
	}

	public function getMarketingBlurbUpdatedOn() {
		return $this->m_strMarketingBlurbUpdatedOn;
	}

	public function sqlMarketingBlurbUpdatedOn() {
		return ( true == isset( $this->m_strMarketingBlurbUpdatedOn ) ) ? '\'' . $this->m_strMarketingBlurbUpdatedOn . '\'' : 'NULL';
	}

	public function setMaxTaskVotes( $intMaxTaskVotes ) {
		$this->set( 'm_intMaxTaskVotes', CStrings::strToIntDef( $intMaxTaskVotes, NULL, false ) );
	}

	public function getMaxTaskVotes() {
		return $this->m_intMaxTaskVotes;
	}

	public function sqlMaxTaskVotes() {
		return ( true == isset( $this->m_intMaxTaskVotes ) ) ? ( string ) $this->m_intMaxTaskVotes : '5';
	}

	public function setDnsHandle( $strDnsHandle ) {
		$this->set( 'm_strDnsHandle', CStrings::strTrimDef( $strDnsHandle, 240, NULL, true ) );
	}

	public function getDnsHandle() {
		return $this->m_strDnsHandle;
	}

	public function sqlDnsHandle() {
		return ( true == isset( $this->m_strDnsHandle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDnsHandle ) : '\'' . addslashes( $this->m_strDnsHandle ) . '\'' ) : 'NULL';
	}

	public function setWorkStationIpAddress( $strWorkStationIpAddress ) {
		$this->set( 'm_strWorkStationIpAddress', CStrings::strTrimDef( $strWorkStationIpAddress, 23, NULL, true ) );
	}

	public function getWorkStationIpAddress() {
		return $this->m_strWorkStationIpAddress;
	}

	public function sqlWorkStationIpAddress() {
		return ( true == isset( $this->m_strWorkStationIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strWorkStationIpAddress ) : '\'' . addslashes( $this->m_strWorkStationIpAddress ) . '\'' ) : 'NULL';
	}

	public function setVpnIpAddress( $strVpnIpAddress ) {
		$this->set( 'm_strVpnIpAddress', CStrings::strTrimDef( $strVpnIpAddress, 23, NULL, true ) );
	}

	public function getVpnIpAddress() {
		return $this->m_strVpnIpAddress;
	}

	public function sqlVpnIpAddress() {
		return ( true == isset( $this->m_strVpnIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVpnIpAddress ) : '\'' . addslashes( $this->m_strVpnIpAddress ) . '\'' ) : 'NULL';
	}

	public function setQaIpAddress( $strQaIpAddress ) {
		$this->set( 'm_strQaIpAddress', CStrings::strTrimDef( $strQaIpAddress, 23, NULL, true ) );
	}

	public function getQaIpAddress() {
		return $this->m_strQaIpAddress;
	}

	public function sqlQaIpAddress() {
		return ( true == isset( $this->m_strQaIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strQaIpAddress ) : '\'' . addslashes( $this->m_strQaIpAddress ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setEmailSignature( $strEmailSignature ) {
		$this->set( 'm_strEmailSignature', CStrings::strTrimDef( $strEmailSignature, -1, NULL, true ) );
	}

	public function getEmailSignature() {
		return $this->m_strEmailSignature;
	}

	public function sqlEmailSignature() {
		return ( true == isset( $this->m_strEmailSignature ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailSignature ) : '\'' . addslashes( $this->m_strEmailSignature ) . '\'' ) : 'NULL';
	}

	public function setIsProjectManager( $intIsProjectManager ) {
		$this->set( 'm_intIsProjectManager', CStrings::strToIntDef( $intIsProjectManager, NULL, false ) );
	}

	public function getIsProjectManager() {
		return $this->m_intIsProjectManager;
	}

	public function sqlIsProjectManager() {
		return ( true == isset( $this->m_intIsProjectManager ) ) ? ( string ) $this->m_intIsProjectManager : 'NULL';
	}

	public function setIs360Review( $intIs360Review ) {
		$this->set( 'm_intIs360Review', CStrings::strToIntDef( $intIs360Review, NULL, false ) );
	}

	public function getIs360Review() {
		return $this->m_intIs360Review;
	}

	public function sqlIs360Review() {
		return ( true == isset( $this->m_intIs360Review ) ) ? ( string ) $this->m_intIs360Review : '0';
	}

	public function setLastAssessedOn( $strLastAssessedOn ) {
		$this->set( 'm_strLastAssessedOn', CStrings::strTrimDef( $strLastAssessedOn, -1, NULL, true ) );
	}

	public function getLastAssessedOn() {
		return $this->m_strLastAssessedOn;
	}

	public function sqlLastAssessedOn() {
		return ( true == isset( $this->m_strLastAssessedOn ) ) ? '\'' . $this->m_strLastAssessedOn . '\'' : 'NULL';
	}

	public function setHandbookDownloadedOn( $strHandbookDownloadedOn ) {
		$this->set( 'm_strHandbookDownloadedOn', CStrings::strTrimDef( $strHandbookDownloadedOn, -1, NULL, true ) );
	}

	public function getHandbookDownloadedOn() {
		return $this->m_strHandbookDownloadedOn;
	}

	public function sqlHandbookDownloadedOn() {
		return ( true == isset( $this->m_strHandbookDownloadedOn ) ) ? '\'' . $this->m_strHandbookDownloadedOn . '\'' : 'NULL';
	}

	public function setHandbookSignedOn( $strHandbookSignedOn ) {
		$this->set( 'm_strHandbookSignedOn', CStrings::strTrimDef( $strHandbookSignedOn, -1, NULL, true ) );
	}

	public function getHandbookSignedOn() {
		return $this->m_strHandbookSignedOn;
	}

	public function sqlHandbookSignedOn() {
		return ( true == isset( $this->m_strHandbookSignedOn ) ) ? '\'' . $this->m_strHandbookSignedOn . '\'' : 'NULL';
	}

	public function setBonusPotentialEncrypted( $strBonusPotentialEncrypted ) {
		$this->set( 'm_strBonusPotentialEncrypted', CStrings::strTrimDef( $strBonusPotentialEncrypted, 240, NULL, true ) );
	}

	public function getBonusPotentialEncrypted() {
		return $this->m_strBonusPotentialEncrypted;
	}

	public function sqlBonusPotentialEncrypted() {
		return ( true == isset( $this->m_strBonusPotentialEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBonusPotentialEncrypted ) : '\'' . addslashes( $this->m_strBonusPotentialEncrypted ) . '\'' ) : 'NULL';
	}

	public function setAnnualBonusTypeId( $intAnnualBonusTypeId ) {
		$this->set( 'm_intAnnualBonusTypeId', CStrings::strToIntDef( $intAnnualBonusTypeId, NULL, false ) );
	}

	public function getAnnualBonusTypeId() {
		return $this->m_intAnnualBonusTypeId;
	}

	public function sqlAnnualBonusTypeId() {
		return ( true == isset( $this->m_intAnnualBonusTypeId ) ) ? ( string ) $this->m_intAnnualBonusTypeId : 'NULL';
	}

	public function setBonusRequirement( $strBonusRequirement ) {
		$this->set( 'm_strBonusRequirement', CStrings::strTrimDef( $strBonusRequirement, -1, NULL, true ) );
	}

	public function getBonusRequirement() {
		return $this->m_strBonusRequirement;
	}

	public function sqlBonusRequirement() {
		return ( true == isset( $this->m_strBonusRequirement ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBonusRequirement ) : '\'' . addslashes( $this->m_strBonusRequirement ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPrivateKey( $strPrivateKey ) {
		$this->set( 'm_strPrivateKey', CStrings::strTrimDef( $strPrivateKey, -1, NULL, true ) );
	}

	public function getPrivateKey() {
		return $this->m_strPrivateKey;
	}

	public function sqlPrivateKey() {
		return ( true == isset( $this->m_strPrivateKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPrivateKey ) : '\'' . addslashes( $this->m_strPrivateKey ) . '\'' ) : 'NULL';
	}

	public function setReportingManagerId( $intReportingManagerId ) {
		$this->set( 'm_intReportingManagerId', CStrings::strToIntDef( $intReportingManagerId, NULL, false ) );
	}

	public function getReportingManagerId() {
		return $this->m_intReportingManagerId;
	}

	public function sqlReportingManagerId() {
		return ( true == isset( $this->m_intReportingManagerId ) ) ? ( string ) $this->m_intReportingManagerId : 'NULL';
	}

	public function setBusinessUnitId( $intBusinessUnitId ) {
		$this->set( 'm_intBusinessUnitId', CStrings::strToIntDef( $intBusinessUnitId, NULL, false ) );
	}

	public function getBusinessUnitId() {
		return $this->m_intBusinessUnitId;
	}

	public function sqlBusinessUnitId() {
		return ( true == isset( $this->m_intBusinessUnitId ) ) ? ( string ) $this->m_intBusinessUnitId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_status_type_id, employee_salary_type_id, employee_application_id, department_id, technology_id, designation_id, office_id, office_desk_id, desk_allocation_office_desk_id, physical_phone_extension_id, agent_phone_extension_id, payroll_remote_primary_key, name_prefix, name_first, name_middle, name_last, preferred_name, gender, name_suffix, name_full, name_maiden, birth_date, blood_group, anniversary_date, tax_number_masked, tax_number_encrypted, bank_account_number_encrypted, pf_number_encrypted, public_key, employee_number, email_address, permanent_email_address, gmail_username, base_salary_encrypted, date_started, resigned_on, expected_termination_date, date_terminated, date_confirmed, marketing_blurb, marketing_blurb_updated_on, max_task_votes, dns_handle, work_station_ip_address, vpn_ip_address, qa_ip_address, notes, email_signature, is_project_manager, is_360_review, last_assessed_on, handbook_downloaded_on, handbook_signed_on, bonus_potential_encrypted, annual_bonus_type_id, bonus_requirement, updated_by, updated_on, created_by, created_on, private_key, reporting_manager_id, business_unit_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeStatusTypeId() . ', ' .
						$this->sqlEmployeeSalaryTypeId() . ', ' .
						$this->sqlEmployeeApplicationId() . ', ' .
						$this->sqlDepartmentId() . ', ' .
						$this->sqlTechnologyId() . ', ' .
						$this->sqlDesignationId() . ', ' .
						$this->sqlOfficeId() . ', ' .
						$this->sqlOfficeDeskId() . ', ' .
						$this->sqlDeskAllocationOfficeDeskId() . ', ' .
						$this->sqlPhysicalPhoneExtensionId() . ', ' .
						$this->sqlAgentPhoneExtensionId() . ', ' .
						$this->sqlPayrollRemotePrimaryKey() . ', ' .
						$this->sqlNamePrefix() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlPreferredName() . ', ' .
						$this->sqlGender() . ', ' .
						$this->sqlNameSuffix() . ', ' .
						$this->sqlNameFull() . ', ' .
						$this->sqlNameMaiden() . ', ' .
						$this->sqlBirthDate() . ', ' .
						$this->sqlBloodGroup() . ', ' .
						$this->sqlAnniversaryDate() . ', ' .
						$this->sqlTaxNumberMasked() . ', ' .
						$this->sqlTaxNumberEncrypted() . ', ' .
						$this->sqlBankAccountNumberEncrypted() . ', ' .
						$this->sqlPfNumberEncrypted() . ', ' .
						$this->sqlPublicKey() . ', ' .
						$this->sqlEmployeeNumber() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlPermanentEmailAddress() . ', ' .
						$this->sqlGmailUsername() . ', ' .
						$this->sqlBaseSalaryEncrypted() . ', ' .
						$this->sqlDateStarted() . ', ' .
						$this->sqlResignedOn() . ', ' .
						$this->sqlExpectedTerminationDate() . ', ' .
						$this->sqlDateTerminated() . ', ' .
						$this->sqlDateConfirmed() . ', ' .
						$this->sqlMarketingBlurb() . ', ' .
						$this->sqlMarketingBlurbUpdatedOn() . ', ' .
						$this->sqlMaxTaskVotes() . ', ' .
						$this->sqlDnsHandle() . ', ' .
						$this->sqlWorkStationIpAddress() . ', ' .
						$this->sqlVpnIpAddress() . ', ' .
						$this->sqlQaIpAddress() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlEmailSignature() . ', ' .
						$this->sqlIsProjectManager() . ', ' .
						$this->sqlIs360Review() . ', ' .
						$this->sqlLastAssessedOn() . ', ' .
						$this->sqlHandbookDownloadedOn() . ', ' .
						$this->sqlHandbookSignedOn() . ', ' .
						$this->sqlBonusPotentialEncrypted() . ', ' .
						$this->sqlAnnualBonusTypeId() . ', ' .
						$this->sqlBonusRequirement() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlPrivateKey() . ', ' .
						$this->sqlReportingManagerId() . ', ' .
						$this->sqlBusinessUnitId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_status_type_id = ' . $this->sqlEmployeeStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_status_type_id = ' . $this->sqlEmployeeStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_salary_type_id = ' . $this->sqlEmployeeSalaryTypeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeSalaryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_salary_type_id = ' . $this->sqlEmployeeSalaryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId(). ',' ; } elseif( true == array_key_exists( 'EmployeeApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId(). ',' ; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' technology_id = ' . $this->sqlTechnologyId(). ',' ; } elseif( true == array_key_exists( 'TechnologyId', $this->getChangedColumns() ) ) { $strSql .= ' technology_id = ' . $this->sqlTechnologyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId(). ',' ; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_id = ' . $this->sqlOfficeId(). ',' ; } elseif( true == array_key_exists( 'OfficeId', $this->getChangedColumns() ) ) { $strSql .= ' office_id = ' . $this->sqlOfficeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_desk_id = ' . $this->sqlOfficeDeskId(). ',' ; } elseif( true == array_key_exists( 'OfficeDeskId', $this->getChangedColumns() ) ) { $strSql .= ' office_desk_id = ' . $this->sqlOfficeDeskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desk_allocation_office_desk_id = ' . $this->sqlDeskAllocationOfficeDeskId(). ',' ; } elseif( true == array_key_exists( 'DeskAllocationOfficeDeskId', $this->getChangedColumns() ) ) { $strSql .= ' desk_allocation_office_desk_id = ' . $this->sqlDeskAllocationOfficeDeskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' physical_phone_extension_id = ' . $this->sqlPhysicalPhoneExtensionId(). ',' ; } elseif( true == array_key_exists( 'PhysicalPhoneExtensionId', $this->getChangedColumns() ) ) { $strSql .= ' physical_phone_extension_id = ' . $this->sqlPhysicalPhoneExtensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agent_phone_extension_id = ' . $this->sqlAgentPhoneExtensionId(). ',' ; } elseif( true == array_key_exists( 'AgentPhoneExtensionId', $this->getChangedColumns() ) ) { $strSql .= ' agent_phone_extension_id = ' . $this->sqlAgentPhoneExtensionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payroll_remote_primary_key = ' . $this->sqlPayrollRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'PayrollRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' payroll_remote_primary_key = ' . $this->sqlPayrollRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix(). ',' ; } elseif( true == array_key_exists( 'NamePrefix', $this->getChangedColumns() ) ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle(). ',' ; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preferred_name = ' . $this->sqlPreferredName(). ',' ; } elseif( true == array_key_exists( 'PreferredName', $this->getChangedColumns() ) ) { $strSql .= ' preferred_name = ' . $this->sqlPreferredName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gender = ' . $this->sqlGender(). ',' ; } elseif( true == array_key_exists( 'Gender', $this->getChangedColumns() ) ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix(). ',' ; } elseif( true == array_key_exists( 'NameSuffix', $this->getChangedColumns() ) ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_full = ' . $this->sqlNameFull(). ',' ; } elseif( true == array_key_exists( 'NameFull', $this->getChangedColumns() ) ) { $strSql .= ' name_full = ' . $this->sqlNameFull() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_maiden = ' . $this->sqlNameMaiden(). ',' ; } elseif( true == array_key_exists( 'NameMaiden', $this->getChangedColumns() ) ) { $strSql .= ' name_maiden = ' . $this->sqlNameMaiden() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate(). ',' ; } elseif( true == array_key_exists( 'BirthDate', $this->getChangedColumns() ) ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blood_group = ' . $this->sqlBloodGroup(). ',' ; } elseif( true == array_key_exists( 'BloodGroup', $this->getChangedColumns() ) ) { $strSql .= ' blood_group = ' . $this->sqlBloodGroup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anniversary_date = ' . $this->sqlAnniversaryDate(). ',' ; } elseif( true == array_key_exists( 'AnniversaryDate', $this->getChangedColumns() ) ) { $strSql .= ' anniversary_date = ' . $this->sqlAnniversaryDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_masked = ' . $this->sqlTaxNumberMasked(). ',' ; } elseif( true == array_key_exists( 'TaxNumberMasked', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_masked = ' . $this->sqlTaxNumberMasked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_number_encrypted = ' . $this->sqlBankAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'BankAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_number_encrypted = ' . $this->sqlBankAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pf_number_encrypted = ' . $this->sqlPfNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'PfNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' pf_number_encrypted = ' . $this->sqlPfNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' public_key = ' . $this->sqlPublicKey(). ',' ; } elseif( true == array_key_exists( 'PublicKey', $this->getChangedColumns() ) ) { $strSql .= ' public_key = ' . $this->sqlPublicKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_number = ' . $this->sqlEmployeeNumber(). ',' ; } elseif( true == array_key_exists( 'EmployeeNumber', $this->getChangedColumns() ) ) { $strSql .= ' employee_number = ' . $this->sqlEmployeeNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' permanent_email_address = ' . $this->sqlPermanentEmailAddress(). ',' ; } elseif( true == array_key_exists( 'PermanentEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' permanent_email_address = ' . $this->sqlPermanentEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gmail_username = ' . $this->sqlGmailUsername(). ',' ; } elseif( true == array_key_exists( 'GmailUsername', $this->getChangedColumns() ) ) { $strSql .= ' gmail_username = ' . $this->sqlGmailUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_salary_encrypted = ' . $this->sqlBaseSalaryEncrypted(). ',' ; } elseif( true == array_key_exists( 'BaseSalaryEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' base_salary_encrypted = ' . $this->sqlBaseSalaryEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_started = ' . $this->sqlDateStarted(). ',' ; } elseif( true == array_key_exists( 'DateStarted', $this->getChangedColumns() ) ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resigned_on = ' . $this->sqlResignedOn(). ',' ; } elseif( true == array_key_exists( 'ResignedOn', $this->getChangedColumns() ) ) { $strSql .= ' resigned_on = ' . $this->sqlResignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_termination_date = ' . $this->sqlExpectedTerminationDate(). ',' ; } elseif( true == array_key_exists( 'ExpectedTerminationDate', $this->getChangedColumns() ) ) { $strSql .= ' expected_termination_date = ' . $this->sqlExpectedTerminationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated(). ',' ; } elseif( true == array_key_exists( 'DateTerminated', $this->getChangedColumns() ) ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_confirmed = ' . $this->sqlDateConfirmed(). ',' ; } elseif( true == array_key_exists( 'DateConfirmed', $this->getChangedColumns() ) ) { $strSql .= ' date_confirmed = ' . $this->sqlDateConfirmed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_blurb = ' . $this->sqlMarketingBlurb(). ',' ; } elseif( true == array_key_exists( 'MarketingBlurb', $this->getChangedColumns() ) ) { $strSql .= ' marketing_blurb = ' . $this->sqlMarketingBlurb() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_blurb_updated_on = ' . $this->sqlMarketingBlurbUpdatedOn(). ',' ; } elseif( true == array_key_exists( 'MarketingBlurbUpdatedOn', $this->getChangedColumns() ) ) { $strSql .= ' marketing_blurb_updated_on = ' . $this->sqlMarketingBlurbUpdatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_task_votes = ' . $this->sqlMaxTaskVotes(). ',' ; } elseif( true == array_key_exists( 'MaxTaskVotes', $this->getChangedColumns() ) ) { $strSql .= ' max_task_votes = ' . $this->sqlMaxTaskVotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dns_handle = ' . $this->sqlDnsHandle(). ',' ; } elseif( true == array_key_exists( 'DnsHandle', $this->getChangedColumns() ) ) { $strSql .= ' dns_handle = ' . $this->sqlDnsHandle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' work_station_ip_address = ' . $this->sqlWorkStationIpAddress(). ',' ; } elseif( true == array_key_exists( 'WorkStationIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' work_station_ip_address = ' . $this->sqlWorkStationIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vpn_ip_address = ' . $this->sqlVpnIpAddress(). ',' ; } elseif( true == array_key_exists( 'VpnIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' vpn_ip_address = ' . $this->sqlVpnIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_ip_address = ' . $this->sqlQaIpAddress(). ',' ; } elseif( true == array_key_exists( 'QaIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' qa_ip_address = ' . $this->sqlQaIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_signature = ' . $this->sqlEmailSignature(). ',' ; } elseif( true == array_key_exists( 'EmailSignature', $this->getChangedColumns() ) ) { $strSql .= ' email_signature = ' . $this->sqlEmailSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_project_manager = ' . $this->sqlIsProjectManager(). ',' ; } elseif( true == array_key_exists( 'IsProjectManager', $this->getChangedColumns() ) ) { $strSql .= ' is_project_manager = ' . $this->sqlIsProjectManager() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_360_review = ' . $this->sqlIs360Review(). ',' ; } elseif( true == array_key_exists( 'Is360Review', $this->getChangedColumns() ) ) { $strSql .= ' is_360_review = ' . $this->sqlIs360Review() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_assessed_on = ' . $this->sqlLastAssessedOn(). ',' ; } elseif( true == array_key_exists( 'LastAssessedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_assessed_on = ' . $this->sqlLastAssessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' handbook_downloaded_on = ' . $this->sqlHandbookDownloadedOn(). ',' ; } elseif( true == array_key_exists( 'HandbookDownloadedOn', $this->getChangedColumns() ) ) { $strSql .= ' handbook_downloaded_on = ' . $this->sqlHandbookDownloadedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' handbook_signed_on = ' . $this->sqlHandbookSignedOn(). ',' ; } elseif( true == array_key_exists( 'HandbookSignedOn', $this->getChangedColumns() ) ) { $strSql .= ' handbook_signed_on = ' . $this->sqlHandbookSignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bonus_potential_encrypted = ' . $this->sqlBonusPotentialEncrypted(). ',' ; } elseif( true == array_key_exists( 'BonusPotentialEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bonus_potential_encrypted = ' . $this->sqlBonusPotentialEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' annual_bonus_type_id = ' . $this->sqlAnnualBonusTypeId(). ',' ; } elseif( true == array_key_exists( 'AnnualBonusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' annual_bonus_type_id = ' . $this->sqlAnnualBonusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bonus_requirement = ' . $this->sqlBonusRequirement(). ',' ; } elseif( true == array_key_exists( 'BonusRequirement', $this->getChangedColumns() ) ) { $strSql .= ' bonus_requirement = ' . $this->sqlBonusRequirement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' private_key = ' . $this->sqlPrivateKey(). ',' ; } elseif( true == array_key_exists( 'PrivateKey', $this->getChangedColumns() ) ) { $strSql .= ' private_key = ' . $this->sqlPrivateKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reporting_manager_id = ' . $this->sqlReportingManagerId(). ',' ; } elseif( true == array_key_exists( 'ReportingManagerId', $this->getChangedColumns() ) ) { $strSql .= ' reporting_manager_id = ' . $this->sqlReportingManagerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' business_unit_id = ' . $this->sqlBusinessUnitId(). ',' ; } elseif( true == array_key_exists( 'BusinessUnitId', $this->getChangedColumns() ) ) { $strSql .= ' business_unit_id = ' . $this->sqlBusinessUnitId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_status_type_id' => $this->getEmployeeStatusTypeId(),
			'employee_salary_type_id' => $this->getEmployeeSalaryTypeId(),
			'employee_application_id' => $this->getEmployeeApplicationId(),
			'department_id' => $this->getDepartmentId(),
			'technology_id' => $this->getTechnologyId(),
			'designation_id' => $this->getDesignationId(),
			'office_id' => $this->getOfficeId(),
			'office_desk_id' => $this->getOfficeDeskId(),
			'desk_allocation_office_desk_id' => $this->getDeskAllocationOfficeDeskId(),
			'physical_phone_extension_id' => $this->getPhysicalPhoneExtensionId(),
			'agent_phone_extension_id' => $this->getAgentPhoneExtensionId(),
			'payroll_remote_primary_key' => $this->getPayrollRemotePrimaryKey(),
			'name_prefix' => $this->getNamePrefix(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'preferred_name' => $this->getPreferredName(),
			'gender' => $this->getGender(),
			'name_suffix' => $this->getNameSuffix(),
			'name_full' => $this->getNameFull(),
			'name_maiden' => $this->getNameMaiden(),
			'birth_date' => $this->getBirthDate(),
			'blood_group' => $this->getBloodGroup(),
			'anniversary_date' => $this->getAnniversaryDate(),
			'tax_number_masked' => $this->getTaxNumberMasked(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'bank_account_number_encrypted' => $this->getBankAccountNumberEncrypted(),
			'pf_number_encrypted' => $this->getPfNumberEncrypted(),
			'public_key' => $this->getPublicKey(),
			'employee_number' => $this->getEmployeeNumber(),
			'email_address' => $this->getEmailAddress(),
			'permanent_email_address' => $this->getPermanentEmailAddress(),
			'gmail_username' => $this->getGmailUsername(),
			'base_salary_encrypted' => $this->getBaseSalaryEncrypted(),
			'date_started' => $this->getDateStarted(),
			'resigned_on' => $this->getResignedOn(),
			'expected_termination_date' => $this->getExpectedTerminationDate(),
			'date_terminated' => $this->getDateTerminated(),
			'date_confirmed' => $this->getDateConfirmed(),
			'marketing_blurb' => $this->getMarketingBlurb(),
			'marketing_blurb_updated_on' => $this->getMarketingBlurbUpdatedOn(),
			'max_task_votes' => $this->getMaxTaskVotes(),
			'dns_handle' => $this->getDnsHandle(),
			'work_station_ip_address' => $this->getWorkStationIpAddress(),
			'vpn_ip_address' => $this->getVpnIpAddress(),
			'qa_ip_address' => $this->getQaIpAddress(),
			'notes' => $this->getNotes(),
			'email_signature' => $this->getEmailSignature(),
			'is_project_manager' => $this->getIsProjectManager(),
			'is_360_review' => $this->getIs360Review(),
			'last_assessed_on' => $this->getLastAssessedOn(),
			'handbook_downloaded_on' => $this->getHandbookDownloadedOn(),
			'handbook_signed_on' => $this->getHandbookSignedOn(),
			'bonus_potential_encrypted' => $this->getBonusPotentialEncrypted(),
			'annual_bonus_type_id' => $this->getAnnualBonusTypeId(),
			'bonus_requirement' => $this->getBonusRequirement(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'private_key' => $this->getPrivateKey(),
			'reporting_manager_id' => $this->getReportingManagerId(),
			'business_unit_id' => $this->getBusinessUnitId()
		);
	}

}
?>