<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationSources
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationSources extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationSource[]
	 */
	public static function fetchEmployeeApplicationSources( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationSource', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationSource
	 */
	public static function fetchEmployeeApplicationSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationSource', $objDatabase );
	}

	public static function fetchEmployeeApplicationSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_sources', $objDatabase );
	}

	public static function fetchEmployeeApplicationSourceById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationSource( sprintf( 'SELECT * FROM employee_application_sources WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>