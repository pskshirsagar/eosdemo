<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\COfficeShiftAssociations
 * Do not add any new functions to this class.
 */

class CBaseOfficeShiftAssociations extends CEosPluralBase {

	/**
	 * @return COfficeShiftAssociation[]
	 */
	public static function fetchOfficeShiftAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, COfficeShiftAssociation::class, $objDatabase );
	}

	/**
	 * @return COfficeShiftAssociation
	 */
	public static function fetchOfficeShiftAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COfficeShiftAssociation::class, $objDatabase );
	}

	public static function fetchOfficeShiftAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'office_shift_associations', $objDatabase );
	}

	public static function fetchOfficeShiftAssociationById( $intId, $objDatabase ) {
		return self::fetchOfficeShiftAssociation( sprintf( 'SELECT * FROM office_shift_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchOfficeShiftAssociationsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchOfficeShiftAssociations( sprintf( 'SELECT * FROM office_shift_associations WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchOfficeShiftAssociationsByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchOfficeShiftAssociations( sprintf( 'SELECT * FROM office_shift_associations WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

	public static function fetchOfficeShiftAssociationsByOfficeShiftId( $intOfficeShiftId, $objDatabase ) {
		return self::fetchOfficeShiftAssociations( sprintf( 'SELECT * FROM office_shift_associations WHERE office_shift_id = %d', ( int ) $intOfficeShiftId ), $objDatabase );
	}

}
?>