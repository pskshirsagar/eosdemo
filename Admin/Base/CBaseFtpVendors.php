<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CFtpVendors
 * Do not add any new functions to this class.
 */

class CBaseFtpVendors extends CEosPluralBase {

	/**
	 * @return CFtpVendor[]
	 */
	public static function fetchFtpVendors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CFtpVendor', $objDatabase );
	}

	/**
	 * @return CFtpVendor
	 */
	public static function fetchFtpVendor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CFtpVendor', $objDatabase );
	}

	public static function fetchFtpVendorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ftp_vendors', $objDatabase );
	}

	public static function fetchFtpVendorById( $intId, $objDatabase ) {
		return self::fetchFtpVendor( sprintf( 'SELECT * FROM ftp_vendors WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>