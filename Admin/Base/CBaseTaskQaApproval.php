<?php

class CBaseTaskQaApproval extends CEosSingularBase {

	const TABLE_NAME = 'public.task_qa_approvals';

	protected $m_intId;
	protected $m_intTaskId;
	protected $m_intDeploymentId;
	protected $m_intRevisionNumber;
	protected $m_strNonQaReason;
	protected $m_intDeployedBy;
	protected $m_strDeployedOn;
	protected $m_intQaApprovedBy;
	protected $m_strQaApprovedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strDeployedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['deployment_id'] ) && $boolDirectSet ) $this->set( 'm_intDeploymentId', trim( $arrValues['deployment_id'] ) ); elseif( isset( $arrValues['deployment_id'] ) ) $this->setDeploymentId( $arrValues['deployment_id'] );
		if( isset( $arrValues['revision_number'] ) && $boolDirectSet ) $this->set( 'm_intRevisionNumber', trim( $arrValues['revision_number'] ) ); elseif( isset( $arrValues['revision_number'] ) ) $this->setRevisionNumber( $arrValues['revision_number'] );
		if( isset( $arrValues['non_qa_reason'] ) && $boolDirectSet ) $this->set( 'm_strNonQaReason', trim( stripcslashes( $arrValues['non_qa_reason'] ) ) ); elseif( isset( $arrValues['non_qa_reason'] ) ) $this->setNonQaReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['non_qa_reason'] ) : $arrValues['non_qa_reason'] );
		if( isset( $arrValues['deployed_by'] ) && $boolDirectSet ) $this->set( 'm_intDeployedBy', trim( $arrValues['deployed_by'] ) ); elseif( isset( $arrValues['deployed_by'] ) ) $this->setDeployedBy( $arrValues['deployed_by'] );
		if( isset( $arrValues['deployed_on'] ) && $boolDirectSet ) $this->set( 'm_strDeployedOn', trim( $arrValues['deployed_on'] ) ); elseif( isset( $arrValues['deployed_on'] ) ) $this->setDeployedOn( $arrValues['deployed_on'] );
		if( isset( $arrValues['qa_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intQaApprovedBy', trim( $arrValues['qa_approved_by'] ) ); elseif( isset( $arrValues['qa_approved_by'] ) ) $this->setQaApprovedBy( $arrValues['qa_approved_by'] );
		if( isset( $arrValues['qa_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strQaApprovedOn', trim( $arrValues['qa_approved_on'] ) ); elseif( isset( $arrValues['qa_approved_on'] ) ) $this->setQaApprovedOn( $arrValues['qa_approved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setDeploymentId( $intDeploymentId ) {
		$this->set( 'm_intDeploymentId', CStrings::strToIntDef( $intDeploymentId, NULL, false ) );
	}

	public function getDeploymentId() {
		return $this->m_intDeploymentId;
	}

	public function sqlDeploymentId() {
		return ( true == isset( $this->m_intDeploymentId ) ) ? ( string ) $this->m_intDeploymentId : 'NULL';
	}

	public function setRevisionNumber( $intRevisionNumber ) {
		$this->set( 'm_intRevisionNumber', CStrings::strToIntDef( $intRevisionNumber, NULL, false ) );
	}

	public function getRevisionNumber() {
		return $this->m_intRevisionNumber;
	}

	public function sqlRevisionNumber() {
		return ( true == isset( $this->m_intRevisionNumber ) ) ? ( string ) $this->m_intRevisionNumber : 'NULL';
	}

	public function setNonQaReason( $strNonQaReason ) {
		$this->set( 'm_strNonQaReason', CStrings::strTrimDef( $strNonQaReason, -1, NULL, true ) );
	}

	public function getNonQaReason() {
		return $this->m_strNonQaReason;
	}

	public function sqlNonQaReason() {
		return ( true == isset( $this->m_strNonQaReason ) ) ? '\'' . addslashes( $this->m_strNonQaReason ) . '\'' : 'NULL';
	}

	public function setDeployedBy( $intDeployedBy ) {
		$this->set( 'm_intDeployedBy', CStrings::strToIntDef( $intDeployedBy, NULL, false ) );
	}

	public function getDeployedBy() {
		return $this->m_intDeployedBy;
	}

	public function sqlDeployedBy() {
		return ( true == isset( $this->m_intDeployedBy ) ) ? ( string ) $this->m_intDeployedBy : 'NULL';
	}

	public function setDeployedOn( $strDeployedOn ) {
		$this->set( 'm_strDeployedOn', CStrings::strTrimDef( $strDeployedOn, -1, NULL, true ) );
	}

	public function getDeployedOn() {
		return $this->m_strDeployedOn;
	}

	public function sqlDeployedOn() {
		return ( true == isset( $this->m_strDeployedOn ) ) ? '\'' . $this->m_strDeployedOn . '\'' : 'NOW()';
	}

	public function setQaApprovedBy( $intQaApprovedBy ) {
		$this->set( 'm_intQaApprovedBy', CStrings::strToIntDef( $intQaApprovedBy, NULL, false ) );
	}

	public function getQaApprovedBy() {
		return $this->m_intQaApprovedBy;
	}

	public function sqlQaApprovedBy() {
		return ( true == isset( $this->m_intQaApprovedBy ) ) ? ( string ) $this->m_intQaApprovedBy : 'NULL';
	}

	public function setQaApprovedOn( $strQaApprovedOn ) {
		$this->set( 'm_strQaApprovedOn', CStrings::strTrimDef( $strQaApprovedOn, -1, NULL, true ) );
	}

	public function getQaApprovedOn() {
		return $this->m_strQaApprovedOn;
	}

	public function sqlQaApprovedOn() {
		return ( true == isset( $this->m_strQaApprovedOn ) ) ? '\'' . $this->m_strQaApprovedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, task_id, deployment_id, revision_number, non_qa_reason, deployed_by, deployed_on, qa_approved_by, qa_approved_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlDeploymentId() . ', ' .
 						$this->sqlRevisionNumber() . ', ' .
 						$this->sqlNonQaReason() . ', ' .
 						$this->sqlDeployedBy() . ', ' .
 						$this->sqlDeployedOn() . ', ' .
 						$this->sqlQaApprovedBy() . ', ' .
 						$this->sqlQaApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployment_id = ' . $this->sqlDeploymentId() . ','; } elseif( true == array_key_exists( 'DeploymentId', $this->getChangedColumns() ) ) { $strSql .= ' deployment_id = ' . $this->sqlDeploymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revision_number = ' . $this->sqlRevisionNumber() . ','; } elseif( true == array_key_exists( 'RevisionNumber', $this->getChangedColumns() ) ) { $strSql .= ' revision_number = ' . $this->sqlRevisionNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_qa_reason = ' . $this->sqlNonQaReason() . ','; } elseif( true == array_key_exists( 'NonQaReason', $this->getChangedColumns() ) ) { $strSql .= ' non_qa_reason = ' . $this->sqlNonQaReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployed_by = ' . $this->sqlDeployedBy() . ','; } elseif( true == array_key_exists( 'DeployedBy', $this->getChangedColumns() ) ) { $strSql .= ' deployed_by = ' . $this->sqlDeployedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deployed_on = ' . $this->sqlDeployedOn() . ','; } elseif( true == array_key_exists( 'DeployedOn', $this->getChangedColumns() ) ) { $strSql .= ' deployed_on = ' . $this->sqlDeployedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_approved_by = ' . $this->sqlQaApprovedBy() . ','; } elseif( true == array_key_exists( 'QaApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' qa_approved_by = ' . $this->sqlQaApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_approved_on = ' . $this->sqlQaApprovedOn() . ','; } elseif( true == array_key_exists( 'QaApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' qa_approved_on = ' . $this->sqlQaApprovedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'task_id' => $this->getTaskId(),
			'deployment_id' => $this->getDeploymentId(),
			'revision_number' => $this->getRevisionNumber(),
			'non_qa_reason' => $this->getNonQaReason(),
			'deployed_by' => $this->getDeployedBy(),
			'deployed_on' => $this->getDeployedOn(),
			'qa_approved_by' => $this->getQaApprovedBy(),
			'qa_approved_on' => $this->getQaApprovedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>