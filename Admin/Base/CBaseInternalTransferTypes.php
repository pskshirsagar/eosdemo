<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CInternalTransferTypes
 * Do not add any new functions to this class.
 */

class CBaseInternalTransferTypes extends CEosPluralBase {

	/**
	 * @return CInternalTransferType[]
	 */
	public static function fetchInternalTransferTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInternalTransferType', $objDatabase );
	}

	/**
	 * @return CInternalTransferType
	 */
	public static function fetchInternalTransferType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInternalTransferType', $objDatabase );
	}

	public static function fetchInternalTransferTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'internal_transfer_types', $objDatabase );
	}

	public static function fetchInternalTransferTypeById( $intId, $objDatabase ) {
		return self::fetchInternalTransferType( sprintf( 'SELECT * FROM internal_transfer_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>