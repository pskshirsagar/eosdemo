<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSvnFiles
 * Do not add any new functions to this class.
 */

class CBaseSvnFiles extends CEosPluralBase {

	/**
	 * @return CSvnFile[]
	 */
	public static function fetchSvnFiles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSvnFile::class, $objDatabase );
	}

	/**
	 * @return CSvnFile
	 */
	public static function fetchSvnFile( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSvnFile::class, $objDatabase );
	}

	public static function fetchSvnFileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'svn_files', $objDatabase );
	}

	public static function fetchSvnFileById( $intId, $objDatabase ) {
		return self::fetchSvnFile( sprintf( 'SELECT * FROM svn_files WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSvnFilesByDeploymentTypeId( $intDeploymentTypeId, $objDatabase ) {
		return self::fetchSvnFiles( sprintf( 'SELECT * FROM svn_files WHERE deployment_type_id = %d', ( int ) $intDeploymentTypeId ), $objDatabase );
	}

	public static function fetchSvnFilesByDatabaseTypeId( $intDatabaseTypeId, $objDatabase ) {
		return self::fetchSvnFiles( sprintf( 'SELECT * FROM svn_files WHERE database_type_id = %d', ( int ) $intDatabaseTypeId ), $objDatabase );
	}

	public static function fetchSvnFilesByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchSvnFiles( sprintf( 'SELECT * FROM svn_files WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

}
?>