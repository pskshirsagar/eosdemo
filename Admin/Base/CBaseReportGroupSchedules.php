<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportGroupSchedules
 * Do not add any new functions to this class.
 */

class CBaseReportGroupSchedules extends CEosPluralBase {

	/**
	 * @return CReportGroupSchedule[]
	 */
	public static function fetchReportGroupSchedules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportGroupSchedule::class, $objDatabase );
	}

	/**
	 * @return CReportGroupSchedule
	 */
	public static function fetchReportGroupSchedule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportGroupSchedule::class, $objDatabase );
	}

	public static function fetchReportGroupScheduleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_group_schedules', $objDatabase );
	}

	public static function fetchReportGroupScheduleById( $intId, $objDatabase ) {
		return self::fetchReportGroupSchedule( sprintf( 'SELECT * FROM report_group_schedules WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportGroupSchedulesByGroupScheduleTypeId( $intGroupScheduleTypeId, $objDatabase ) {
		return self::fetchReportGroupSchedules( sprintf( 'SELECT * FROM report_group_schedules WHERE group_schedule_type_id = %d', ( int ) $intGroupScheduleTypeId ), $objDatabase );
	}

	public static function fetchReportGroupSchedulesByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchReportGroupSchedules( sprintf( 'SELECT * FROM report_group_schedules WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

}
?>