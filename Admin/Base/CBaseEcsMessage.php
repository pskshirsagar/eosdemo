<?php

class CBaseEcsMessage extends CEosSingularBase {

	const TABLE_NAME = 'public.ecs_messages';

	protected $m_intId;
	protected $m_strSid;
	protected $m_strMsgBody;
	protected $m_strReceivedFrom;
	protected $m_strSentTo;
	protected $m_strStatus;
	protected $m_strMessageStatus;
	protected $m_strUpdatedOn;
	protected $m_strCreatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['sid'] ) && $boolDirectSet ) $this->set( 'm_strSid', trim( stripcslashes( $arrValues['sid'] ) ) ); elseif( isset( $arrValues['sid'] ) ) $this->setSid( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sid'] ) : $arrValues['sid'] );
		if( isset( $arrValues['msg_body'] ) && $boolDirectSet ) $this->set( 'm_strMsgBody', trim( stripcslashes( $arrValues['msg_body'] ) ) ); elseif( isset( $arrValues['msg_body'] ) ) $this->setMsgBody( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['msg_body'] ) : $arrValues['msg_body'] );
		if( isset( $arrValues['received_from'] ) && $boolDirectSet ) $this->set( 'm_strReceivedFrom', trim( stripcslashes( $arrValues['received_from'] ) ) ); elseif( isset( $arrValues['received_from'] ) ) $this->setReceivedFrom( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['received_from'] ) : $arrValues['received_from'] );
		if( isset( $arrValues['sent_to'] ) && $boolDirectSet ) $this->set( 'm_strSentTo', trim( stripcslashes( $arrValues['sent_to'] ) ) ); elseif( isset( $arrValues['sent_to'] ) ) $this->setSentTo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sent_to'] ) : $arrValues['sent_to'] );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_strStatus', trim( stripcslashes( $arrValues['status'] ) ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status'] ) : $arrValues['status'] );
		if( isset( $arrValues['message_status'] ) && $boolDirectSet ) $this->set( 'm_strMessageStatus', trim( stripcslashes( $arrValues['message_status'] ) ) ); elseif( isset( $arrValues['message_status'] ) ) $this->setMessageStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['message_status'] ) : $arrValues['message_status'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSid( $strSid ) {
		$this->set( 'm_strSid', CStrings::strTrimDef( $strSid, 50, NULL, true ) );
	}

	public function getSid() {
		return $this->m_strSid;
	}

	public function sqlSid() {
		return ( true == isset( $this->m_strSid ) ) ? '\'' . addslashes( $this->m_strSid ) . '\'' : 'NULL';
	}

	public function setMsgBody( $strMsgBody ) {
		$this->set( 'm_strMsgBody', CStrings::strTrimDef( $strMsgBody, 250, NULL, true ) );
	}

	public function getMsgBody() {
		return $this->m_strMsgBody;
	}

	public function sqlMsgBody() {
		return ( true == isset( $this->m_strMsgBody ) ) ? '\'' . addslashes( $this->m_strMsgBody ) . '\'' : 'NULL';
	}

	public function setReceivedFrom( $strReceivedFrom ) {
		$this->set( 'm_strReceivedFrom', CStrings::strTrimDef( $strReceivedFrom, 30, NULL, true ) );
	}

	public function getReceivedFrom() {
		return $this->m_strReceivedFrom;
	}

	public function sqlReceivedFrom() {
		return ( true == isset( $this->m_strReceivedFrom ) ) ? '\'' . addslashes( $this->m_strReceivedFrom ) . '\'' : 'NULL';
	}

	public function setSentTo( $strSentTo ) {
		$this->set( 'm_strSentTo', CStrings::strTrimDef( $strSentTo, 30, NULL, true ) );
	}

	public function getSentTo() {
		return $this->m_strSentTo;
	}

	public function sqlSentTo() {
		return ( true == isset( $this->m_strSentTo ) ) ? '\'' . addslashes( $this->m_strSentTo ) . '\'' : 'NULL';
	}

	public function setStatus( $strStatus ) {
		$this->set( 'm_strStatus', CStrings::strTrimDef( $strStatus, 20, NULL, true ) );
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_strStatus ) ) ? '\'' . addslashes( $this->m_strStatus ) . '\'' : 'NULL';
	}

	public function setMessageStatus( $strMessageStatus ) {
		$this->set( 'm_strMessageStatus', CStrings::strTrimDef( $strMessageStatus, 20, NULL, true ) );
	}

	public function getMessageStatus() {
		return $this->m_strMessageStatus;
	}

	public function sqlMessageStatus() {
		return ( true == isset( $this->m_strMessageStatus ) ) ? '\'' . addslashes( $this->m_strMessageStatus ) . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, sid, msg_body, received_from, sent_to, status, message_status, updated_on, created_on, deleted_by, deleted_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSid() . ', ' .
 						$this->sqlMsgBody() . ', ' .
 						$this->sqlReceivedFrom() . ', ' .
 						$this->sqlSentTo() . ', ' .
 						$this->sqlStatus() . ', ' .
 						$this->sqlMessageStatus() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sid = ' . $this->sqlSid() . ','; } elseif( true == array_key_exists( 'Sid', $this->getChangedColumns() ) ) { $strSql .= ' sid = ' . $this->sqlSid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' msg_body = ' . $this->sqlMsgBody() . ','; } elseif( true == array_key_exists( 'MsgBody', $this->getChangedColumns() ) ) { $strSql .= ' msg_body = ' . $this->sqlMsgBody() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' received_from = ' . $this->sqlReceivedFrom() . ','; } elseif( true == array_key_exists( 'ReceivedFrom', $this->getChangedColumns() ) ) { $strSql .= ' received_from = ' . $this->sqlReceivedFrom() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_to = ' . $this->sqlSentTo() . ','; } elseif( true == array_key_exists( 'SentTo', $this->getChangedColumns() ) ) { $strSql .= ' sent_to = ' . $this->sqlSentTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_status = ' . $this->sqlMessageStatus() . ','; } elseif( true == array_key_exists( 'MessageStatus', $this->getChangedColumns() ) ) { $strSql .= ' message_status = ' . $this->sqlMessageStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'sid' => $this->getSid(),
			'msg_body' => $this->getMsgBody(),
			'received_from' => $this->getReceivedFrom(),
			'sent_to' => $this->getSentTo(),
			'status' => $this->getStatus(),
			'message_status' => $this->getMessageStatus(),
			'updated_on' => $this->getUpdatedOn(),
			'created_on' => $this->getCreatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>