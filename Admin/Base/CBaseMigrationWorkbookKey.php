<?php

class CBaseMigrationWorkbookKey extends CEosSingularBase {

	const TABLE_NAME = 'public.migration_workbook_keys';

	protected $m_intId;
	protected $m_intParentMigrationWorkbookKeyId;
	protected $m_strKey;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['parent_migration_workbook_key_id'] ) && $boolDirectSet ) $this->set( 'm_intParentMigrationWorkbookKeyId', trim( $arrValues['parent_migration_workbook_key_id'] ) ); elseif( isset( $arrValues['parent_migration_workbook_key_id'] ) ) $this->setParentMigrationWorkbookKeyId( $arrValues['parent_migration_workbook_key_id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( stripcslashes( $arrValues['key'] ) ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setParentMigrationWorkbookKeyId( $intParentMigrationWorkbookKeyId ) {
		$this->set( 'm_intParentMigrationWorkbookKeyId', CStrings::strToIntDef( $intParentMigrationWorkbookKeyId, NULL, false ) );
	}

	public function getParentMigrationWorkbookKeyId() {
		return $this->m_intParentMigrationWorkbookKeyId;
	}

	public function sqlParentMigrationWorkbookKeyId() {
		return ( true == isset( $this->m_intParentMigrationWorkbookKeyId ) ) ? ( string ) $this->m_intParentMigrationWorkbookKeyId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 244, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, parent_migration_workbook_key_id, key, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlParentMigrationWorkbookKeyId() . ', ' .
 						$this->sqlKey() . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_migration_workbook_key_id = ' . $this->sqlParentMigrationWorkbookKeyId() . ','; } elseif( true == array_key_exists( 'ParentMigrationWorkbookKeyId', $this->getChangedColumns() ) ) { $strSql .= ' parent_migration_workbook_key_id = ' . $this->sqlParentMigrationWorkbookKeyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey() . ','; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'parent_migration_workbook_key_id' => $this->getParentMigrationWorkbookKeyId(),
			'key' => $this->getKey(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>