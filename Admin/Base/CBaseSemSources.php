<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemSources
 * Do not add any new functions to this class.
 */

class CBaseSemSources extends CEosPluralBase {

	/**
	 * @return CSemSource[]
	 */
	public static function fetchSemSources( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemSource', $objDatabase );
	}

	/**
	 * @return CSemSource
	 */
	public static function fetchSemSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemSource', $objDatabase );
	}

	public static function fetchSemSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_sources', $objDatabase );
	}

	public static function fetchSemSourceById( $intId, $objDatabase ) {
		return self::fetchSemSource( sprintf( 'SELECT * FROM sem_sources WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>