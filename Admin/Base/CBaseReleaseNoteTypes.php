<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseNoteTypes
 * Do not add any new functions to this class.
 */

class CBaseReleaseNoteTypes extends CEosPluralBase {

	/**
	 * @return CReleaseNoteType[]
	 */
	public static function fetchReleaseNoteTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CReleaseNoteType', $objDatabase );
	}

	/**
	 * @return CReleaseNoteType
	 */
	public static function fetchReleaseNoteType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReleaseNoteType', $objDatabase );
	}

	public static function fetchReleaseNoteTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'release_note_types', $objDatabase );
	}

	public static function fetchReleaseNoteTypeById( $intId, $objDatabase ) {
		return self::fetchReleaseNoteType( sprintf( 'SELECT * FROM release_note_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>