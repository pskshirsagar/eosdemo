<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsAssetStatusTypes
 * Do not add any new functions to this class.
 */

class CBasePsAssetStatusTypes extends CEosPluralBase {

	/**
	 * @return CPsAssetStatusType[]
	 */
	public static function fetchPsAssetStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsAssetStatusType', $objDatabase );
	}

	/**
	 * @return CPsAssetStatusType
	 */
	public static function fetchPsAssetStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsAssetStatusType', $objDatabase );
	}

	public static function fetchPsAssetStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_asset_status_types', $objDatabase );
	}

	public static function fetchPsAssetStatusTypeById( $intId, $objDatabase ) {
		return self::fetchPsAssetStatusType( sprintf( 'SELECT * FROM ps_asset_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>