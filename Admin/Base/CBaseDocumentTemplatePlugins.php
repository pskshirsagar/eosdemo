<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplatePlugins
 * Do not add any new functions to this class.
 */

class CBaseDocumentTemplatePlugins extends CEosPluralBase {

	/**
	 * @return CDocumentTemplatePlugin[]
	 */
	public static function fetchDocumentTemplatePlugins( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDocumentTemplatePlugin', $objDatabase );
	}

	/**
	 * @return CDocumentTemplatePlugin
	 */
	public static function fetchDocumentTemplatePlugin( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentTemplatePlugin', $objDatabase );
	}

	public static function fetchDocumentTemplatePluginCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_template_plugins', $objDatabase );
	}

	public static function fetchDocumentTemplatePluginById( $intId, $objDatabase ) {
		return self::fetchDocumentTemplatePlugin( sprintf( 'SELECT * FROM document_template_plugins WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDocumentTemplatePluginsByDocumentTemplateLibraryId( $intDocumentTemplateLibraryId, $objDatabase ) {
		return self::fetchDocumentTemplatePlugins( sprintf( 'SELECT * FROM document_template_plugins WHERE document_template_library_id = %d', ( int ) $intDocumentTemplateLibraryId ), $objDatabase );
	}

}
?>