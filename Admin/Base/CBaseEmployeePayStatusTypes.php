<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePayStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseEmployeePayStatusTypes extends CEosPluralBase {

	/**
	 * @return CEmployeePayStatusType[]
	 */
	public static function fetchEmployeePayStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeePayStatusType', $objDatabase );
	}

	/**
	 * @return CEmployeePayStatusType
	 */
	public static function fetchEmployeePayStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeePayStatusType', $objDatabase );
	}

	public static function fetchEmployeePayStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_pay_status_types', $objDatabase );
	}

	public static function fetchEmployeePayStatusTypeById( $intId, $objDatabase ) {
		return self::fetchEmployeePayStatusType( sprintf( 'SELECT * FROM employee_pay_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>