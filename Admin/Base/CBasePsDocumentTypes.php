<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsDocumentTypes
 * Do not add any new functions to this class.
 */

class CBasePsDocumentTypes extends CEosPluralBase {

	/**
	 * @return CPsDocumentType[]
	 */
	public static function fetchPsDocumentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsDocumentType', $objDatabase );
	}

	/**
	 * @return CPsDocumentType
	 */
	public static function fetchPsDocumentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsDocumentType', $objDatabase );
	}

	public static function fetchPsDocumentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_document_types', $objDatabase );
	}

	public static function fetchPsDocumentTypeById( $intId, $objDatabase ) {
		return self::fetchPsDocumentType( sprintf( 'SELECT * FROM ps_document_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsDocumentTypesByPsDocumentTypeId( $intPsDocumentTypeId, $objDatabase ) {
		return self::fetchPsDocumentTypes( sprintf( 'SELECT * FROM ps_document_types WHERE ps_document_type_id = %d', ( int ) $intPsDocumentTypeId ), $objDatabase );
	}

}
?>