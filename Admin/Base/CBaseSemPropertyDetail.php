<?php

class CBaseSemPropertyDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.sem_property_details';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSemPropertyStatusTypeId;
	protected $m_intCompanyUserId;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strStreetLine3;
	protected $m_strCity;
	protected $m_strCounty;
	protected $m_strStateCode;
	protected $m_strProvince;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_fltLongitude;
	protected $m_fltLatitude;
	protected $m_strVacancyRedirectUrl;
	protected $m_fltRewardAmount;
	protected $m_strRewardDescription;
	protected $m_intIsAutoPostTestimonials;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intSemPropertyStatusTypeId = '1';
		$this->m_intIsAutoPostTestimonials = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['sem_property_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSemPropertyStatusTypeId', trim( $arrValues['sem_property_status_type_id'] ) ); elseif( isset( $arrValues['sem_property_status_type_id'] ) ) $this->setSemPropertyStatusTypeId( $arrValues['sem_property_status_type_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['street_line1'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine1', trim( stripcslashes( $arrValues['street_line1'] ) ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line1'] ) : $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine2', trim( stripcslashes( $arrValues['street_line2'] ) ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line2'] ) : $arrValues['street_line2'] );
		if( isset( $arrValues['street_line3'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine3', trim( stripcslashes( $arrValues['street_line3'] ) ) ); elseif( isset( $arrValues['street_line3'] ) ) $this->setStreetLine3( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['street_line3'] ) : $arrValues['street_line3'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['county'] ) && $boolDirectSet ) $this->set( 'm_strCounty', trim( stripcslashes( $arrValues['county'] ) ) ); elseif( isset( $arrValues['county'] ) ) $this->setCounty( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['county'] ) : $arrValues['county'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( stripcslashes( $arrValues['province'] ) ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['province'] ) : $arrValues['province'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['longitude'] ) && $boolDirectSet ) $this->set( 'm_fltLongitude', trim( $arrValues['longitude'] ) ); elseif( isset( $arrValues['longitude'] ) ) $this->setLongitude( $arrValues['longitude'] );
		if( isset( $arrValues['latitude'] ) && $boolDirectSet ) $this->set( 'm_fltLatitude', trim( $arrValues['latitude'] ) ); elseif( isset( $arrValues['latitude'] ) ) $this->setLatitude( $arrValues['latitude'] );
		if( isset( $arrValues['vacancy_redirect_url'] ) && $boolDirectSet ) $this->set( 'm_strVacancyRedirectUrl', trim( stripcslashes( $arrValues['vacancy_redirect_url'] ) ) ); elseif( isset( $arrValues['vacancy_redirect_url'] ) ) $this->setVacancyRedirectUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vacancy_redirect_url'] ) : $arrValues['vacancy_redirect_url'] );
		if( isset( $arrValues['reward_amount'] ) && $boolDirectSet ) $this->set( 'm_fltRewardAmount', trim( $arrValues['reward_amount'] ) ); elseif( isset( $arrValues['reward_amount'] ) ) $this->setRewardAmount( $arrValues['reward_amount'] );
		if( isset( $arrValues['reward_description'] ) && $boolDirectSet ) $this->set( 'm_strRewardDescription', trim( stripcslashes( $arrValues['reward_description'] ) ) ); elseif( isset( $arrValues['reward_description'] ) ) $this->setRewardDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reward_description'] ) : $arrValues['reward_description'] );
		if( isset( $arrValues['is_auto_post_testimonials'] ) && $boolDirectSet ) $this->set( 'm_intIsAutoPostTestimonials', trim( $arrValues['is_auto_post_testimonials'] ) ); elseif( isset( $arrValues['is_auto_post_testimonials'] ) ) $this->setIsAutoPostTestimonials( $arrValues['is_auto_post_testimonials'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSemPropertyStatusTypeId( $intSemPropertyStatusTypeId ) {
		$this->set( 'm_intSemPropertyStatusTypeId', CStrings::strToIntDef( $intSemPropertyStatusTypeId, NULL, false ) );
	}

	public function getSemPropertyStatusTypeId() {
		return $this->m_intSemPropertyStatusTypeId;
	}

	public function sqlSemPropertyStatusTypeId() {
		return ( true == isset( $this->m_intSemPropertyStatusTypeId ) ) ? ( string ) $this->m_intSemPropertyStatusTypeId : '1';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->set( 'm_strStreetLine1', CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) );
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->set( 'm_strStreetLine2', CStrings::strTrimDef( $strStreetLine2, 100, NULL, true ) );
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' : 'NULL';
	}

	public function setStreetLine3( $strStreetLine3 ) {
		$this->set( 'm_strStreetLine3', CStrings::strTrimDef( $strStreetLine3, 100, NULL, true ) );
	}

	public function getStreetLine3() {
		return $this->m_strStreetLine3;
	}

	public function sqlStreetLine3() {
		return ( true == isset( $this->m_strStreetLine3 ) ) ? '\'' . addslashes( $this->m_strStreetLine3 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setCounty( $strCounty ) {
		$this->set( 'm_strCounty', CStrings::strTrimDef( $strCounty, 50, NULL, true ) );
	}

	public function getCounty() {
		return $this->m_strCounty;
	}

	public function sqlCounty() {
		return ( true == isset( $this->m_strCounty ) ) ? '\'' . addslashes( $this->m_strCounty ) . '\'' : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? '\'' . addslashes( $this->m_strProvince ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setLongitude( $fltLongitude ) {
		$this->set( 'm_fltLongitude', CStrings::strToFloatDef( $fltLongitude, NULL, false, 0 ) );
	}

	public function getLongitude() {
		return $this->m_fltLongitude;
	}

	public function sqlLongitude() {
		return ( true == isset( $this->m_fltLongitude ) ) ? ( string ) $this->m_fltLongitude : 'NULL';
	}

	public function setLatitude( $fltLatitude ) {
		$this->set( 'm_fltLatitude', CStrings::strToFloatDef( $fltLatitude, NULL, false, 0 ) );
	}

	public function getLatitude() {
		return $this->m_fltLatitude;
	}

	public function sqlLatitude() {
		return ( true == isset( $this->m_fltLatitude ) ) ? ( string ) $this->m_fltLatitude : 'NULL';
	}

	public function setVacancyRedirectUrl( $strVacancyRedirectUrl ) {
		$this->set( 'm_strVacancyRedirectUrl', CStrings::strTrimDef( $strVacancyRedirectUrl, 4096, NULL, true ) );
	}

	public function getVacancyRedirectUrl() {
		return $this->m_strVacancyRedirectUrl;
	}

	public function sqlVacancyRedirectUrl() {
		return ( true == isset( $this->m_strVacancyRedirectUrl ) ) ? '\'' . addslashes( $this->m_strVacancyRedirectUrl ) . '\'' : 'NULL';
	}

	public function setRewardAmount( $fltRewardAmount ) {
		$this->set( 'm_fltRewardAmount', CStrings::strToFloatDef( $fltRewardAmount, NULL, false, 4 ) );
	}

	public function getRewardAmount() {
		return $this->m_fltRewardAmount;
	}

	public function sqlRewardAmount() {
		return ( true == isset( $this->m_fltRewardAmount ) ) ? ( string ) $this->m_fltRewardAmount : 'NULL';
	}

	public function setRewardDescription( $strRewardDescription ) {
		$this->set( 'm_strRewardDescription', CStrings::strTrimDef( $strRewardDescription, 2000, NULL, true ) );
	}

	public function getRewardDescription() {
		return $this->m_strRewardDescription;
	}

	public function sqlRewardDescription() {
		return ( true == isset( $this->m_strRewardDescription ) ) ? '\'' . addslashes( $this->m_strRewardDescription ) . '\'' : 'NULL';
	}

	public function setIsAutoPostTestimonials( $intIsAutoPostTestimonials ) {
		$this->set( 'm_intIsAutoPostTestimonials', CStrings::strToIntDef( $intIsAutoPostTestimonials, NULL, false ) );
	}

	public function getIsAutoPostTestimonials() {
		return $this->m_intIsAutoPostTestimonials;
	}

	public function sqlIsAutoPostTestimonials() {
		return ( true == isset( $this->m_intIsAutoPostTestimonials ) ) ? ( string ) $this->m_intIsAutoPostTestimonials : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, sem_property_status_type_id, company_user_id, street_line1, street_line2, street_line3, city, county, state_code, province, postal_code, country_code, longitude, latitude, vacancy_redirect_url, reward_amount, reward_description, is_auto_post_testimonials, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSemPropertyStatusTypeId() . ', ' .
 						$this->sqlCompanyUserId() . ', ' .
 						$this->sqlStreetLine1() . ', ' .
 						$this->sqlStreetLine2() . ', ' .
 						$this->sqlStreetLine3() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlCounty() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlProvince() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlCountryCode() . ', ' .
 						$this->sqlLongitude() . ', ' .
 						$this->sqlLatitude() . ', ' .
 						$this->sqlVacancyRedirectUrl() . ', ' .
 						$this->sqlRewardAmount() . ', ' .
 						$this->sqlRewardDescription() . ', ' .
 						$this->sqlIsAutoPostTestimonials() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_property_status_type_id = ' . $this->sqlSemPropertyStatusTypeId() . ','; } elseif( true == array_key_exists( 'SemPropertyStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' sem_property_status_type_id = ' . $this->sqlSemPropertyStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; } elseif( true == array_key_exists( 'StreetLine3', $this->getChangedColumns() ) ) { $strSql .= ' street_line3 = ' . $this->sqlStreetLine3() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' county = ' . $this->sqlCounty() . ','; } elseif( true == array_key_exists( 'County', $this->getChangedColumns() ) ) { $strSql .= ' county = ' . $this->sqlCounty() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; } elseif( true == array_key_exists( 'Longitude', $this->getChangedColumns() ) ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; } elseif( true == array_key_exists( 'Latitude', $this->getChangedColumns() ) ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacancy_redirect_url = ' . $this->sqlVacancyRedirectUrl() . ','; } elseif( true == array_key_exists( 'VacancyRedirectUrl', $this->getChangedColumns() ) ) { $strSql .= ' vacancy_redirect_url = ' . $this->sqlVacancyRedirectUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reward_amount = ' . $this->sqlRewardAmount() . ','; } elseif( true == array_key_exists( 'RewardAmount', $this->getChangedColumns() ) ) { $strSql .= ' reward_amount = ' . $this->sqlRewardAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reward_description = ' . $this->sqlRewardDescription() . ','; } elseif( true == array_key_exists( 'RewardDescription', $this->getChangedColumns() ) ) { $strSql .= ' reward_description = ' . $this->sqlRewardDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_post_testimonials = ' . $this->sqlIsAutoPostTestimonials() . ','; } elseif( true == array_key_exists( 'IsAutoPostTestimonials', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_post_testimonials = ' . $this->sqlIsAutoPostTestimonials() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'sem_property_status_type_id' => $this->getSemPropertyStatusTypeId(),
			'company_user_id' => $this->getCompanyUserId(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'street_line3' => $this->getStreetLine3(),
			'city' => $this->getCity(),
			'county' => $this->getCounty(),
			'state_code' => $this->getStateCode(),
			'province' => $this->getProvince(),
			'postal_code' => $this->getPostalCode(),
			'country_code' => $this->getCountryCode(),
			'longitude' => $this->getLongitude(),
			'latitude' => $this->getLatitude(),
			'vacancy_redirect_url' => $this->getVacancyRedirectUrl(),
			'reward_amount' => $this->getRewardAmount(),
			'reward_description' => $this->getRewardDescription(),
			'is_auto_post_testimonials' => $this->getIsAutoPostTestimonials(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>