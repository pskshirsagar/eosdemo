<?php

class CBaseSemAd extends CEosSingularBase {

	const TABLE_NAME = 'public.sem_ads';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSemCampaignId;
	protected $m_intSemAdGroupId;
	protected $m_strTitle;
	protected $m_strDescriptionLine1;
	protected $m_strDescriptionLine2;
	protected $m_strDisplayUrl;
	protected $m_strDestinationUrl;
	protected $m_strAdDatetime;
	protected $m_intIsApproved;
	protected $m_intIsSystem;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsApproved = '0';
		$this->m_intIsSystem = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['sem_campaign_id'] ) && $boolDirectSet ) $this->set( 'm_intSemCampaignId', trim( $arrValues['sem_campaign_id'] ) ); elseif( isset( $arrValues['sem_campaign_id'] ) ) $this->setSemCampaignId( $arrValues['sem_campaign_id'] );
		if( isset( $arrValues['sem_ad_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdGroupId', trim( $arrValues['sem_ad_group_id'] ) ); elseif( isset( $arrValues['sem_ad_group_id'] ) ) $this->setSemAdGroupId( $arrValues['sem_ad_group_id'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description_line1'] ) && $boolDirectSet ) $this->set( 'm_strDescriptionLine1', trim( stripcslashes( $arrValues['description_line1'] ) ) ); elseif( isset( $arrValues['description_line1'] ) ) $this->setDescriptionLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description_line1'] ) : $arrValues['description_line1'] );
		if( isset( $arrValues['description_line2'] ) && $boolDirectSet ) $this->set( 'm_strDescriptionLine2', trim( stripcslashes( $arrValues['description_line2'] ) ) ); elseif( isset( $arrValues['description_line2'] ) ) $this->setDescriptionLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description_line2'] ) : $arrValues['description_line2'] );
		if( isset( $arrValues['display_url'] ) && $boolDirectSet ) $this->set( 'm_strDisplayUrl', trim( stripcslashes( $arrValues['display_url'] ) ) ); elseif( isset( $arrValues['display_url'] ) ) $this->setDisplayUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['display_url'] ) : $arrValues['display_url'] );
		if( isset( $arrValues['destination_url'] ) && $boolDirectSet ) $this->set( 'm_strDestinationUrl', trim( stripcslashes( $arrValues['destination_url'] ) ) ); elseif( isset( $arrValues['destination_url'] ) ) $this->setDestinationUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['destination_url'] ) : $arrValues['destination_url'] );
		if( isset( $arrValues['ad_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAdDatetime', trim( $arrValues['ad_datetime'] ) ); elseif( isset( $arrValues['ad_datetime'] ) ) $this->setAdDatetime( $arrValues['ad_datetime'] );
		if( isset( $arrValues['is_approved'] ) && $boolDirectSet ) $this->set( 'm_intIsApproved', trim( $arrValues['is_approved'] ) ); elseif( isset( $arrValues['is_approved'] ) ) $this->setIsApproved( $arrValues['is_approved'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSemCampaignId( $intSemCampaignId ) {
		$this->set( 'm_intSemCampaignId', CStrings::strToIntDef( $intSemCampaignId, NULL, false ) );
	}

	public function getSemCampaignId() {
		return $this->m_intSemCampaignId;
	}

	public function sqlSemCampaignId() {
		return ( true == isset( $this->m_intSemCampaignId ) ) ? ( string ) $this->m_intSemCampaignId : 'NULL';
	}

	public function setSemAdGroupId( $intSemAdGroupId ) {
		$this->set( 'm_intSemAdGroupId', CStrings::strToIntDef( $intSemAdGroupId, NULL, false ) );
	}

	public function getSemAdGroupId() {
		return $this->m_intSemAdGroupId;
	}

	public function sqlSemAdGroupId() {
		return ( true == isset( $this->m_intSemAdGroupId ) ) ? ( string ) $this->m_intSemAdGroupId : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescriptionLine1( $strDescriptionLine1 ) {
		$this->set( 'm_strDescriptionLine1', CStrings::strTrimDef( $strDescriptionLine1, 240, NULL, true ) );
	}

	public function getDescriptionLine1() {
		return $this->m_strDescriptionLine1;
	}

	public function sqlDescriptionLine1() {
		return ( true == isset( $this->m_strDescriptionLine1 ) ) ? '\'' . addslashes( $this->m_strDescriptionLine1 ) . '\'' : 'NULL';
	}

	public function setDescriptionLine2( $strDescriptionLine2 ) {
		$this->set( 'm_strDescriptionLine2', CStrings::strTrimDef( $strDescriptionLine2, 240, NULL, true ) );
	}

	public function getDescriptionLine2() {
		return $this->m_strDescriptionLine2;
	}

	public function sqlDescriptionLine2() {
		return ( true == isset( $this->m_strDescriptionLine2 ) ) ? '\'' . addslashes( $this->m_strDescriptionLine2 ) . '\'' : 'NULL';
	}

	public function setDisplayUrl( $strDisplayUrl ) {
		$this->set( 'm_strDisplayUrl', CStrings::strTrimDef( $strDisplayUrl, 4096, NULL, true ) );
	}

	public function getDisplayUrl() {
		return $this->m_strDisplayUrl;
	}

	public function sqlDisplayUrl() {
		return ( true == isset( $this->m_strDisplayUrl ) ) ? '\'' . addslashes( $this->m_strDisplayUrl ) . '\'' : 'NULL';
	}

	public function setDestinationUrl( $strDestinationUrl ) {
		$this->set( 'm_strDestinationUrl', CStrings::strTrimDef( $strDestinationUrl, 4096, NULL, true ) );
	}

	public function getDestinationUrl() {
		return $this->m_strDestinationUrl;
	}

	public function sqlDestinationUrl() {
		return ( true == isset( $this->m_strDestinationUrl ) ) ? '\'' . addslashes( $this->m_strDestinationUrl ) . '\'' : 'NULL';
	}

	public function setAdDatetime( $strAdDatetime ) {
		$this->set( 'm_strAdDatetime', CStrings::strTrimDef( $strAdDatetime, -1, NULL, true ) );
	}

	public function getAdDatetime() {
		return $this->m_strAdDatetime;
	}

	public function sqlAdDatetime() {
		return ( true == isset( $this->m_strAdDatetime ) ) ? '\'' . $this->m_strAdDatetime . '\'' : 'NOW()';
	}

	public function setIsApproved( $intIsApproved ) {
		$this->set( 'm_intIsApproved', CStrings::strToIntDef( $intIsApproved, NULL, false ) );
	}

	public function getIsApproved() {
		return $this->m_intIsApproved;
	}

	public function sqlIsApproved() {
		return ( true == isset( $this->m_intIsApproved ) ) ? ( string ) $this->m_intIsApproved : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, sem_campaign_id, sem_ad_group_id, title, description_line1, description_line2, display_url, destination_url, ad_datetime, is_approved, is_system, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSemCampaignId() . ', ' .
 						$this->sqlSemAdGroupId() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlDescriptionLine1() . ', ' .
 						$this->sqlDescriptionLine2() . ', ' .
 						$this->sqlDisplayUrl() . ', ' .
 						$this->sqlDestinationUrl() . ', ' .
 						$this->sqlAdDatetime() . ', ' .
 						$this->sqlIsApproved() . ', ' .
 						$this->sqlIsSystem() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_campaign_id = ' . $this->sqlSemCampaignId() . ','; } elseif( true == array_key_exists( 'SemCampaignId', $this->getChangedColumns() ) ) { $strSql .= ' sem_campaign_id = ' . $this->sqlSemCampaignId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; } elseif( true == array_key_exists( 'SemAdGroupId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description_line1 = ' . $this->sqlDescriptionLine1() . ','; } elseif( true == array_key_exists( 'DescriptionLine1', $this->getChangedColumns() ) ) { $strSql .= ' description_line1 = ' . $this->sqlDescriptionLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description_line2 = ' . $this->sqlDescriptionLine2() . ','; } elseif( true == array_key_exists( 'DescriptionLine2', $this->getChangedColumns() ) ) { $strSql .= ' description_line2 = ' . $this->sqlDescriptionLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' display_url = ' . $this->sqlDisplayUrl() . ','; } elseif( true == array_key_exists( 'DisplayUrl', $this->getChangedColumns() ) ) { $strSql .= ' display_url = ' . $this->sqlDisplayUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_url = ' . $this->sqlDestinationUrl() . ','; } elseif( true == array_key_exists( 'DestinationUrl', $this->getChangedColumns() ) ) { $strSql .= ' destination_url = ' . $this->sqlDestinationUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ad_datetime = ' . $this->sqlAdDatetime() . ','; } elseif( true == array_key_exists( 'AdDatetime', $this->getChangedColumns() ) ) { $strSql .= ' ad_datetime = ' . $this->sqlAdDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; } elseif( true == array_key_exists( 'IsApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'sem_campaign_id' => $this->getSemCampaignId(),
			'sem_ad_group_id' => $this->getSemAdGroupId(),
			'title' => $this->getTitle(),
			'description_line1' => $this->getDescriptionLine1(),
			'description_line2' => $this->getDescriptionLine2(),
			'display_url' => $this->getDisplayUrl(),
			'destination_url' => $this->getDestinationUrl(),
			'ad_datetime' => $this->getAdDatetime(),
			'is_approved' => $this->getIsApproved(),
			'is_system' => $this->getIsSystem(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>