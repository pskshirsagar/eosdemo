<?php

class CBaseEmployeeApplication extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.employee_applications';

	protected $m_intId;
	protected $m_intEmployeeApplicationId;
	protected $m_intEmploymentApplicationId;
	protected $m_intEmployeeApplicationStatusTypeId;
	protected $m_intPsJobPostingStepStatusId;
	protected $m_intEmployeeApplicationGroupId;
	protected $m_intEmployeeId;
	protected $m_intRefereeEmployeeId;
	protected $m_intRecruiterEmployeeId;
	protected $m_intEmployeeApplicationEducationId;
	protected $m_intEmployeeApplicationSourceId;
	protected $m_intPsWebsiteJobPostingId;
	protected $m_intEmploymentOccupationTypeId;
	protected $m_intPsDocumentId;
	protected $m_intDesignationId;
	protected $m_intPurchaseRequestId;
	protected $m_strSubmissionDatetime;
	protected $m_strNamePrefix;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strGender;
	protected $m_strNameLast;
	protected $m_strPreferredName;
	protected $m_strPhoneNumber;
	protected $m_strCellNumber;
	protected $m_strEmailAddress;
	protected $m_strBirthDate;
	protected $m_strBloodGroup;
	protected $m_strDigitalSignature;
	protected $m_strTaxNumberEncrypted;
	protected $m_strStreetLine1;
	protected $m_strStreetLine2;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strState;
	protected $m_strProvince;
	protected $m_strCountryCode;
	protected $m_strPostalCode;
	protected $m_intTestScore1;
	protected $m_intTestScore2;
	protected $m_intTestScore3;
	protected $m_intTestScore4;
	protected $m_strReferredByName;
	protected $m_strEducationDescription;
	protected $m_strAdministrativeNotes;
	protected $m_strReferenceFeedback;
	protected $m_strIpAddress;
	protected $m_strRefereeDetails;
	protected $m_intAllowCreditScreening;
	protected $m_strAppliedOn;
	protected $m_strRejectedOn;
	protected $m_strApplicantDeclinedOn;
	protected $m_strApplicantOfferredOn;
	protected $m_strApplicantScreenedOn;
	protected $m_strScheduledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strSourceUpdatedOn;
	protected $m_boolIsArchived;
	protected $m_boolIsFavorite;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strInterviewUrl;
	protected $m_strCandidateLastUpdatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCountryCode = 'US';
		$this->m_boolIsArchived = false;
		$this->m_boolIsFavorite = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationId', trim( $arrValues['employee_application_id'] ) ); elseif( isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( isset( $arrValues['employment_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmploymentApplicationId', trim( $arrValues['employment_application_id'] ) ); elseif( isset( $arrValues['employment_application_id'] ) ) $this->setEmploymentApplicationId( $arrValues['employment_application_id'] );
		if( isset( $arrValues['employee_application_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationStatusTypeId', trim( $arrValues['employee_application_status_type_id'] ) ); elseif( isset( $arrValues['employee_application_status_type_id'] ) ) $this->setEmployeeApplicationStatusTypeId( $arrValues['employee_application_status_type_id'] );
		if( isset( $arrValues['ps_job_posting_step_status_id'] ) && $boolDirectSet ) $this->set( 'm_intPsJobPostingStepStatusId', trim( $arrValues['ps_job_posting_step_status_id'] ) ); elseif( isset( $arrValues['ps_job_posting_step_status_id'] ) ) $this->setPsJobPostingStepStatusId( $arrValues['ps_job_posting_step_status_id'] );
		if( isset( $arrValues['employee_application_group_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationGroupId', trim( $arrValues['employee_application_group_id'] ) ); elseif( isset( $arrValues['employee_application_group_id'] ) ) $this->setEmployeeApplicationGroupId( $arrValues['employee_application_group_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['referee_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intRefereeEmployeeId', trim( $arrValues['referee_employee_id'] ) ); elseif( isset( $arrValues['referee_employee_id'] ) ) $this->setRefereeEmployeeId( $arrValues['referee_employee_id'] );
		if( isset( $arrValues['recruiter_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intRecruiterEmployeeId', trim( $arrValues['recruiter_employee_id'] ) ); elseif( isset( $arrValues['recruiter_employee_id'] ) ) $this->setRecruiterEmployeeId( $arrValues['recruiter_employee_id'] );
		if( isset( $arrValues['employee_application_education_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationEducationId', trim( $arrValues['employee_application_education_id'] ) ); elseif( isset( $arrValues['employee_application_education_id'] ) ) $this->setEmployeeApplicationEducationId( $arrValues['employee_application_education_id'] );
		if( isset( $arrValues['employee_application_source_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationSourceId', trim( $arrValues['employee_application_source_id'] ) ); elseif( isset( $arrValues['employee_application_source_id'] ) ) $this->setEmployeeApplicationSourceId( $arrValues['employee_application_source_id'] );
		if( isset( $arrValues['ps_website_job_posting_id'] ) && $boolDirectSet ) $this->set( 'm_intPsWebsiteJobPostingId', trim( $arrValues['ps_website_job_posting_id'] ) ); elseif( isset( $arrValues['ps_website_job_posting_id'] ) ) $this->setPsWebsiteJobPostingId( $arrValues['ps_website_job_posting_id'] );
		if( isset( $arrValues['employment_occupation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmploymentOccupationTypeId', trim( $arrValues['employment_occupation_type_id'] ) ); elseif( isset( $arrValues['employment_occupation_type_id'] ) ) $this->setEmploymentOccupationTypeId( $arrValues['employment_occupation_type_id'] );
		if( isset( $arrValues['ps_document_id'] ) && $boolDirectSet ) $this->set( 'm_intPsDocumentId', trim( $arrValues['ps_document_id'] ) ); elseif( isset( $arrValues['ps_document_id'] ) ) $this->setPsDocumentId( $arrValues['ps_document_id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['purchase_request_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchaseRequestId', trim( $arrValues['purchase_request_id'] ) ); elseif( isset( $arrValues['purchase_request_id'] ) ) $this->setPurchaseRequestId( $arrValues['purchase_request_id'] );
		if( isset( $arrValues['submission_datetime'] ) && $boolDirectSet ) $this->set( 'm_strSubmissionDatetime', trim( $arrValues['submission_datetime'] ) ); elseif( isset( $arrValues['submission_datetime'] ) ) $this->setSubmissionDatetime( $arrValues['submission_datetime'] );
		if( isset( $arrValues['name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strNamePrefix', trim( $arrValues['name_prefix'] ) ); elseif( isset( $arrValues['name_prefix'] ) ) $this->setNamePrefix( $arrValues['name_prefix'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( $arrValues['name_middle'] ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( $arrValues['name_middle'] );
		if( isset( $arrValues['gender'] ) && $boolDirectSet ) $this->set( 'm_strGender', trim( $arrValues['gender'] ) ); elseif( isset( $arrValues['gender'] ) ) $this->setGender( $arrValues['gender'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['preferred_name'] ) && $boolDirectSet ) $this->set( 'm_strPreferredName', trim( $arrValues['preferred_name'] ) ); elseif( isset( $arrValues['preferred_name'] ) ) $this->setPreferredName( $arrValues['preferred_name'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( $arrValues['phone_number'] ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( $arrValues['phone_number'] );
		if( isset( $arrValues['cell_number'] ) && $boolDirectSet ) $this->set( 'm_strCellNumber', trim( $arrValues['cell_number'] ) ); elseif( isset( $arrValues['cell_number'] ) ) $this->setCellNumber( $arrValues['cell_number'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['birth_date'] ) && $boolDirectSet ) $this->set( 'm_strBirthDate', trim( $arrValues['birth_date'] ) ); elseif( isset( $arrValues['birth_date'] ) ) $this->setBirthDate( $arrValues['birth_date'] );
		if( isset( $arrValues['blood_group'] ) && $boolDirectSet ) $this->set( 'm_strBloodGroup', trim( $arrValues['blood_group'] ) ); elseif( isset( $arrValues['blood_group'] ) ) $this->setBloodGroup( $arrValues['blood_group'] );
		if( isset( $arrValues['digital_signature'] ) && $boolDirectSet ) $this->set( 'm_strDigitalSignature', trim( $arrValues['digital_signature'] ) ); elseif( isset( $arrValues['digital_signature'] ) ) $this->setDigitalSignature( $arrValues['digital_signature'] );
		if( isset( $arrValues['tax_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTaxNumberEncrypted', trim( $arrValues['tax_number_encrypted'] ) ); elseif( isset( $arrValues['tax_number_encrypted'] ) ) $this->setTaxNumberEncrypted( $arrValues['tax_number_encrypted'] );
		if( isset( $arrValues['street_line1'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine1', trim( $arrValues['street_line1'] ) ); elseif( isset( $arrValues['street_line1'] ) ) $this->setStreetLine1( $arrValues['street_line1'] );
		if( isset( $arrValues['street_line2'] ) && $boolDirectSet ) $this->set( 'm_strStreetLine2', trim( $arrValues['street_line2'] ) ); elseif( isset( $arrValues['street_line2'] ) ) $this->setStreetLine2( $arrValues['street_line2'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( $arrValues['city'] ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( $arrValues['state_code'] ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( $arrValues['state_code'] );
		if( isset( $arrValues['state'] ) && $boolDirectSet ) $this->set( 'm_strState', trim( $arrValues['state'] ) ); elseif( isset( $arrValues['state'] ) ) $this->setState( $arrValues['state'] );
		if( isset( $arrValues['province'] ) && $boolDirectSet ) $this->set( 'm_strProvince', trim( $arrValues['province'] ) ); elseif( isset( $arrValues['province'] ) ) $this->setProvince( $arrValues['province'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( $arrValues['postal_code'] ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( $arrValues['postal_code'] );
		if( isset( $arrValues['test_score_1'] ) && $boolDirectSet ) $this->set( 'm_intTestScore1', trim( $arrValues['test_score_1'] ) ); elseif( isset( $arrValues['test_score_1'] ) ) $this->setTestScore1( $arrValues['test_score_1'] );
		if( isset( $arrValues['test_score_2'] ) && $boolDirectSet ) $this->set( 'm_intTestScore2', trim( $arrValues['test_score_2'] ) ); elseif( isset( $arrValues['test_score_2'] ) ) $this->setTestScore2( $arrValues['test_score_2'] );
		if( isset( $arrValues['test_score_3'] ) && $boolDirectSet ) $this->set( 'm_intTestScore3', trim( $arrValues['test_score_3'] ) ); elseif( isset( $arrValues['test_score_3'] ) ) $this->setTestScore3( $arrValues['test_score_3'] );
		if( isset( $arrValues['test_score_4'] ) && $boolDirectSet ) $this->set( 'm_intTestScore4', trim( $arrValues['test_score_4'] ) ); elseif( isset( $arrValues['test_score_4'] ) ) $this->setTestScore4( $arrValues['test_score_4'] );
		if( isset( $arrValues['referred_by_name'] ) && $boolDirectSet ) $this->set( 'm_strReferredByName', trim( $arrValues['referred_by_name'] ) ); elseif( isset( $arrValues['referred_by_name'] ) ) $this->setReferredByName( $arrValues['referred_by_name'] );
		if( isset( $arrValues['education_description'] ) && $boolDirectSet ) $this->set( 'm_strEducationDescription', trim( $arrValues['education_description'] ) ); elseif( isset( $arrValues['education_description'] ) ) $this->setEducationDescription( $arrValues['education_description'] );
		if( isset( $arrValues['administrative_notes'] ) && $boolDirectSet ) $this->set( 'm_strAdministrativeNotes', trim( $arrValues['administrative_notes'] ) ); elseif( isset( $arrValues['administrative_notes'] ) ) $this->setAdministrativeNotes( $arrValues['administrative_notes'] );
		if( isset( $arrValues['reference_feedback'] ) && $boolDirectSet ) $this->set( 'm_strReferenceFeedback', trim( $arrValues['reference_feedback'] ) ); elseif( isset( $arrValues['reference_feedback'] ) ) $this->setReferenceFeedback( $arrValues['reference_feedback'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['referee_details'] ) && $boolDirectSet ) $this->set( 'm_strRefereeDetails', trim( stripcslashes( $arrValues['referee_details'] ) ) ); elseif( isset( $arrValues['referee_details'] ) ) $this->setRefereeDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['referee_details'] ) : $arrValues['referee_details'] );
		if( isset( $arrValues['allow_credit_screening'] ) && $boolDirectSet ) $this->set( 'm_intAllowCreditScreening', trim( $arrValues['allow_credit_screening'] ) ); elseif( isset( $arrValues['allow_credit_screening'] ) ) $this->setAllowCreditScreening( $arrValues['allow_credit_screening'] );
		if( isset( $arrValues['applied_on'] ) && $boolDirectSet ) $this->set( 'm_strAppliedOn', trim( $arrValues['applied_on'] ) ); elseif( isset( $arrValues['applied_on'] ) ) $this->setAppliedOn( $arrValues['applied_on'] );
		if( isset( $arrValues['rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strRejectedOn', trim( $arrValues['rejected_on'] ) ); elseif( isset( $arrValues['rejected_on'] ) ) $this->setRejectedOn( $arrValues['rejected_on'] );
		if( isset( $arrValues['applicant_declined_on'] ) && $boolDirectSet ) $this->set( 'm_strApplicantDeclinedOn', trim( $arrValues['applicant_declined_on'] ) ); elseif( isset( $arrValues['applicant_declined_on'] ) ) $this->setApplicantDeclinedOn( $arrValues['applicant_declined_on'] );
		if( isset( $arrValues['applicant_offerred_on'] ) && $boolDirectSet ) $this->set( 'm_strApplicantOfferredOn', trim( $arrValues['applicant_offerred_on'] ) ); elseif( isset( $arrValues['applicant_offerred_on'] ) ) $this->setApplicantOfferredOn( $arrValues['applicant_offerred_on'] );
		if( isset( $arrValues['applicant_screened_on'] ) && $boolDirectSet ) $this->set( 'm_strApplicantScreenedOn', trim( $arrValues['applicant_screened_on'] ) ); elseif( isset( $arrValues['applicant_screened_on'] ) ) $this->setApplicantScreenedOn( $arrValues['applicant_screened_on'] );
		if( isset( $arrValues['scheduled_on'] ) && $boolDirectSet ) $this->set( 'm_strScheduledOn', trim( $arrValues['scheduled_on'] ) ); elseif( isset( $arrValues['scheduled_on'] ) ) $this->setScheduledOn( $arrValues['scheduled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['source_updated_on'] ) && $boolDirectSet ) $this->set( 'm_strSourceUpdatedOn', trim( $arrValues['source_updated_on'] ) ); elseif( isset( $arrValues['source_updated_on'] ) ) $this->setSourceUpdatedOn( $arrValues['source_updated_on'] );
		if( isset( $arrValues['is_archived'] ) && $boolDirectSet ) $this->set( 'm_boolIsArchived', trim( stripcslashes( $arrValues['is_archived'] ) ) ); elseif( isset( $arrValues['is_archived'] ) ) $this->setIsArchived( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_archived'] ) : $arrValues['is_archived'] );
		if( isset( $arrValues['is_favorite'] ) && $boolDirectSet ) $this->set( 'm_boolIsFavorite', trim( stripcslashes( $arrValues['is_favorite'] ) ) ); elseif( isset( $arrValues['is_favorite'] ) ) $this->setIsFavorite( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_favorite'] ) : $arrValues['is_favorite'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['interview_url'] ) && $boolDirectSet ) $this->set( 'm_strInterviewUrl', trim( $arrValues['interview_url'] ) ); elseif( isset( $arrValues['interview_url'] ) ) $this->setInterviewUrl( $arrValues['interview_url'] );
		if( isset( $arrValues['candidate_last_updated_on'] ) && $boolDirectSet ) $this->set( 'm_strCandidateLastUpdatedOn', trim( $arrValues['candidate_last_updated_on'] ) ); elseif( isset( $arrValues['candidate_last_updated_on'] ) ) $this->setCandidateLastUpdatedOn( $arrValues['candidate_last_updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeApplicationId( $intEmployeeApplicationId ) {
		$this->set( 'm_intEmployeeApplicationId', CStrings::strToIntDef( $intEmployeeApplicationId, NULL, false ) );
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function sqlEmployeeApplicationId() {
		return ( true == isset( $this->m_intEmployeeApplicationId ) ) ? ( string ) $this->m_intEmployeeApplicationId : 'NULL';
	}

	public function setEmploymentApplicationId( $intEmploymentApplicationId ) {
		$this->set( 'm_intEmploymentApplicationId', CStrings::strToIntDef( $intEmploymentApplicationId, NULL, false ) );
	}

	public function getEmploymentApplicationId() {
		return $this->m_intEmploymentApplicationId;
	}

	public function sqlEmploymentApplicationId() {
		return ( true == isset( $this->m_intEmploymentApplicationId ) ) ? ( string ) $this->m_intEmploymentApplicationId : 'NULL';
	}

	public function setEmployeeApplicationStatusTypeId( $intEmployeeApplicationStatusTypeId ) {
		$this->set( 'm_intEmployeeApplicationStatusTypeId', CStrings::strToIntDef( $intEmployeeApplicationStatusTypeId, NULL, false ) );
	}

	public function getEmployeeApplicationStatusTypeId() {
		return $this->m_intEmployeeApplicationStatusTypeId;
	}

	public function sqlEmployeeApplicationStatusTypeId() {
		return ( true == isset( $this->m_intEmployeeApplicationStatusTypeId ) ) ? ( string ) $this->m_intEmployeeApplicationStatusTypeId : 'NULL';
	}

	public function setPsJobPostingStepStatusId( $intPsJobPostingStepStatusId ) {
		$this->set( 'm_intPsJobPostingStepStatusId', CStrings::strToIntDef( $intPsJobPostingStepStatusId, NULL, false ) );
	}

	public function getPsJobPostingStepStatusId() {
		return $this->m_intPsJobPostingStepStatusId;
	}

	public function sqlPsJobPostingStepStatusId() {
		return ( true == isset( $this->m_intPsJobPostingStepStatusId ) ) ? ( string ) $this->m_intPsJobPostingStepStatusId : 'NULL';
	}

	public function setEmployeeApplicationGroupId( $intEmployeeApplicationGroupId ) {
		$this->set( 'm_intEmployeeApplicationGroupId', CStrings::strToIntDef( $intEmployeeApplicationGroupId, NULL, false ) );
	}

	public function getEmployeeApplicationGroupId() {
		return $this->m_intEmployeeApplicationGroupId;
	}

	public function sqlEmployeeApplicationGroupId() {
		return ( true == isset( $this->m_intEmployeeApplicationGroupId ) ) ? ( string ) $this->m_intEmployeeApplicationGroupId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setRefereeEmployeeId( $intRefereeEmployeeId ) {
		$this->set( 'm_intRefereeEmployeeId', CStrings::strToIntDef( $intRefereeEmployeeId, NULL, false ) );
	}

	public function getRefereeEmployeeId() {
		return $this->m_intRefereeEmployeeId;
	}

	public function sqlRefereeEmployeeId() {
		return ( true == isset( $this->m_intRefereeEmployeeId ) ) ? ( string ) $this->m_intRefereeEmployeeId : 'NULL';
	}

	public function setRecruiterEmployeeId( $intRecruiterEmployeeId ) {
		$this->set( 'm_intRecruiterEmployeeId', CStrings::strToIntDef( $intRecruiterEmployeeId, NULL, false ) );
	}

	public function getRecruiterEmployeeId() {
		return $this->m_intRecruiterEmployeeId;
	}

	public function sqlRecruiterEmployeeId() {
		return ( true == isset( $this->m_intRecruiterEmployeeId ) ) ? ( string ) $this->m_intRecruiterEmployeeId : 'NULL';
	}

	public function setEmployeeApplicationEducationId( $intEmployeeApplicationEducationId ) {
		$this->set( 'm_intEmployeeApplicationEducationId', CStrings::strToIntDef( $intEmployeeApplicationEducationId, NULL, false ) );
	}

	public function getEmployeeApplicationEducationId() {
		return $this->m_intEmployeeApplicationEducationId;
	}

	public function sqlEmployeeApplicationEducationId() {
		return ( true == isset( $this->m_intEmployeeApplicationEducationId ) ) ? ( string ) $this->m_intEmployeeApplicationEducationId : 'NULL';
	}

	public function setEmployeeApplicationSourceId( $intEmployeeApplicationSourceId ) {
		$this->set( 'm_intEmployeeApplicationSourceId', CStrings::strToIntDef( $intEmployeeApplicationSourceId, NULL, false ) );
	}

	public function getEmployeeApplicationSourceId() {
		return $this->m_intEmployeeApplicationSourceId;
	}

	public function sqlEmployeeApplicationSourceId() {
		return ( true == isset( $this->m_intEmployeeApplicationSourceId ) ) ? ( string ) $this->m_intEmployeeApplicationSourceId : 'NULL';
	}

	public function setPsWebsiteJobPostingId( $intPsWebsiteJobPostingId ) {
		$this->set( 'm_intPsWebsiteJobPostingId', CStrings::strToIntDef( $intPsWebsiteJobPostingId, NULL, false ) );
	}

	public function getPsWebsiteJobPostingId() {
		return $this->m_intPsWebsiteJobPostingId;
	}

	public function sqlPsWebsiteJobPostingId() {
		return ( true == isset( $this->m_intPsWebsiteJobPostingId ) ) ? ( string ) $this->m_intPsWebsiteJobPostingId : 'NULL';
	}

	public function setEmploymentOccupationTypeId( $intEmploymentOccupationTypeId ) {
		$this->set( 'm_intEmploymentOccupationTypeId', CStrings::strToIntDef( $intEmploymentOccupationTypeId, NULL, false ) );
	}

	public function getEmploymentOccupationTypeId() {
		return $this->m_intEmploymentOccupationTypeId;
	}

	public function sqlEmploymentOccupationTypeId() {
		return ( true == isset( $this->m_intEmploymentOccupationTypeId ) ) ? ( string ) $this->m_intEmploymentOccupationTypeId : 'NULL';
	}

	public function setPsDocumentId( $intPsDocumentId ) {
		$this->set( 'm_intPsDocumentId', CStrings::strToIntDef( $intPsDocumentId, NULL, false ) );
	}

	public function getPsDocumentId() {
		return $this->m_intPsDocumentId;
	}

	public function sqlPsDocumentId() {
		return ( true == isset( $this->m_intPsDocumentId ) ) ? ( string ) $this->m_intPsDocumentId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setPurchaseRequestId( $intPurchaseRequestId ) {
		$this->set( 'm_intPurchaseRequestId', CStrings::strToIntDef( $intPurchaseRequestId, NULL, false ) );
	}

	public function getPurchaseRequestId() {
		return $this->m_intPurchaseRequestId;
	}

	public function sqlPurchaseRequestId() {
		return ( true == isset( $this->m_intPurchaseRequestId ) ) ? ( string ) $this->m_intPurchaseRequestId : 'NULL';
	}

	public function setSubmissionDatetime( $strSubmissionDatetime ) {
		$this->set( 'm_strSubmissionDatetime', CStrings::strTrimDef( $strSubmissionDatetime, -1, NULL, true ) );
	}

	public function getSubmissionDatetime() {
		return $this->m_strSubmissionDatetime;
	}

	public function sqlSubmissionDatetime() {
		return ( true == isset( $this->m_strSubmissionDatetime ) ) ? '\'' . $this->m_strSubmissionDatetime . '\'' : 'NULL';
	}

	public function setNamePrefix( $strNamePrefix ) {
		$this->set( 'm_strNamePrefix', CStrings::strTrimDef( $strNamePrefix, 20, NULL, true ) );
	}

	public function getNamePrefix() {
		return $this->m_strNamePrefix;
	}

	public function sqlNamePrefix() {
		return ( true == isset( $this->m_strNamePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNamePrefix ) : '\'' . addslashes( $this->m_strNamePrefix ) . '\'' ) : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 50, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMiddle ) : '\'' . addslashes( $this->m_strNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setGender( $strGender ) {
		$this->set( 'm_strGender', CStrings::strTrimDef( $strGender, 1, NULL, true ) );
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function sqlGender() {
		return ( true == isset( $this->m_strGender ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGender ) : '\'' . addslashes( $this->m_strGender ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setPreferredName( $strPreferredName ) {
		$this->set( 'm_strPreferredName', CStrings::strTrimDef( $strPreferredName, 100, NULL, true ) );
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function sqlPreferredName() {
		return ( true == isset( $this->m_strPreferredName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreferredName ) : '\'' . addslashes( $this->m_strPreferredName ) . '\'' ) : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneNumber ) : '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setCellNumber( $strCellNumber ) {
		$this->set( 'm_strCellNumber', CStrings::strTrimDef( $strCellNumber, 30, NULL, true ) );
	}

	public function getCellNumber() {
		return $this->m_strCellNumber;
	}

	public function sqlCellNumber() {
		return ( true == isset( $this->m_strCellNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCellNumber ) : '\'' . addslashes( $this->m_strCellNumber ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setBirthDate( $strBirthDate ) {
		$this->set( 'm_strBirthDate', CStrings::strTrimDef( $strBirthDate, -1, NULL, true ) );
	}

	public function getBirthDate() {
		return $this->m_strBirthDate;
	}

	public function sqlBirthDate() {
		return ( true == isset( $this->m_strBirthDate ) ) ? '\'' . $this->m_strBirthDate . '\'' : 'NULL';
	}

	public function setBloodGroup( $strBloodGroup ) {
		$this->set( 'm_strBloodGroup', CStrings::strTrimDef( $strBloodGroup, 5, NULL, true ) );
	}

	public function getBloodGroup() {
		return $this->m_strBloodGroup;
	}

	public function sqlBloodGroup() {
		return ( true == isset( $this->m_strBloodGroup ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBloodGroup ) : '\'' . addslashes( $this->m_strBloodGroup ) . '\'' ) : 'NULL';
	}

	public function setDigitalSignature( $strDigitalSignature ) {
		$this->set( 'm_strDigitalSignature', CStrings::strTrimDef( $strDigitalSignature, 2000, NULL, true ) );
	}

	public function getDigitalSignature() {
		return $this->m_strDigitalSignature;
	}

	public function sqlDigitalSignature() {
		return ( true == isset( $this->m_strDigitalSignature ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDigitalSignature ) : '\'' . addslashes( $this->m_strDigitalSignature ) . '\'' ) : 'NULL';
	}

	public function setTaxNumberEncrypted( $strTaxNumberEncrypted ) {
		$this->set( 'm_strTaxNumberEncrypted', CStrings::strTrimDef( $strTaxNumberEncrypted, 240, NULL, true ) );
	}

	public function getTaxNumberEncrypted() {
		return $this->m_strTaxNumberEncrypted;
	}

	public function sqlTaxNumberEncrypted() {
		return ( true == isset( $this->m_strTaxNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTaxNumberEncrypted ) : '\'' . addslashes( $this->m_strTaxNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setStreetLine1( $strStreetLine1 ) {
		$this->set( 'm_strStreetLine1', CStrings::strTrimDef( $strStreetLine1, 100, NULL, true ) );
	}

	public function getStreetLine1() {
		return $this->m_strStreetLine1;
	}

	public function sqlStreetLine1() {
		return ( true == isset( $this->m_strStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine1 ) : '\'' . addslashes( $this->m_strStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setStreetLine2( $strStreetLine2 ) {
		$this->set( 'm_strStreetLine2', CStrings::strTrimDef( $strStreetLine2, 100, NULL, true ) );
	}

	public function getStreetLine2() {
		return $this->m_strStreetLine2;
	}

	public function sqlStreetLine2() {
		return ( true == isset( $this->m_strStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStreetLine2 ) : '\'' . addslashes( $this->m_strStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCity ) : '\'' . addslashes( $this->m_strCity ) . '\'' ) : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStateCode ) : '\'' . addslashes( $this->m_strStateCode ) . '\'' ) : 'NULL';
	}

	public function setState( $strState ) {
		$this->set( 'm_strState', CStrings::strTrimDef( $strState, 50, NULL, true ) );
	}

	public function getState() {
		return $this->m_strState;
	}

	public function sqlState() {
		return ( true == isset( $this->m_strState ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strState ) : '\'' . addslashes( $this->m_strState ) . '\'' ) : 'NULL';
	}

	public function setProvince( $strProvince ) {
		$this->set( 'm_strProvince', CStrings::strTrimDef( $strProvince, 50, NULL, true ) );
	}

	public function getProvince() {
		return $this->m_strProvince;
	}

	public function sqlProvince() {
		return ( true == isset( $this->m_strProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProvince ) : '\'' . addslashes( $this->m_strProvince ) . '\'' ) : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountryCode ) : '\'' . addslashes( $this->m_strCountryCode ) . '\'' ) : '\'US\'';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostalCode ) : '\'' . addslashes( $this->m_strPostalCode ) . '\'' ) : 'NULL';
	}

	public function setTestScore1( $intTestScore1 ) {
		$this->set( 'm_intTestScore1', CStrings::strToIntDef( $intTestScore1, NULL, false ) );
	}

	public function getTestScore1() {
		return $this->m_intTestScore1;
	}

	public function sqlTestScore1() {
		return ( true == isset( $this->m_intTestScore1 ) ) ? ( string ) $this->m_intTestScore1 : 'NULL';
	}

	public function setTestScore2( $intTestScore2 ) {
		$this->set( 'm_intTestScore2', CStrings::strToIntDef( $intTestScore2, NULL, false ) );
	}

	public function getTestScore2() {
		return $this->m_intTestScore2;
	}

	public function sqlTestScore2() {
		return ( true == isset( $this->m_intTestScore2 ) ) ? ( string ) $this->m_intTestScore2 : 'NULL';
	}

	public function setTestScore3( $intTestScore3 ) {
		$this->set( 'm_intTestScore3', CStrings::strToIntDef( $intTestScore3, NULL, false ) );
	}

	public function getTestScore3() {
		return $this->m_intTestScore3;
	}

	public function sqlTestScore3() {
		return ( true == isset( $this->m_intTestScore3 ) ) ? ( string ) $this->m_intTestScore3 : 'NULL';
	}

	public function setTestScore4( $intTestScore4 ) {
		$this->set( 'm_intTestScore4', CStrings::strToIntDef( $intTestScore4, NULL, false ) );
	}

	public function getTestScore4() {
		return $this->m_intTestScore4;
	}

	public function sqlTestScore4() {
		return ( true == isset( $this->m_intTestScore4 ) ) ? ( string ) $this->m_intTestScore4 : 'NULL';
	}

	public function setReferredByName( $strReferredByName ) {
		$this->set( 'm_strReferredByName', CStrings::strTrimDef( $strReferredByName, 50, NULL, true ) );
	}

	public function getReferredByName() {
		return $this->m_strReferredByName;
	}

	public function sqlReferredByName() {
		return ( true == isset( $this->m_strReferredByName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReferredByName ) : '\'' . addslashes( $this->m_strReferredByName ) . '\'' ) : 'NULL';
	}

	public function setEducationDescription( $strEducationDescription ) {
		$this->set( 'm_strEducationDescription', CStrings::strTrimDef( $strEducationDescription, -1, NULL, true ) );
	}

	public function getEducationDescription() {
		return $this->m_strEducationDescription;
	}

	public function sqlEducationDescription() {
		return ( true == isset( $this->m_strEducationDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEducationDescription ) : '\'' . addslashes( $this->m_strEducationDescription ) . '\'' ) : 'NULL';
	}

	public function setAdministrativeNotes( $strAdministrativeNotes ) {
		$this->set( 'm_strAdministrativeNotes', CStrings::strTrimDef( $strAdministrativeNotes, -1, NULL, true ) );
	}

	public function getAdministrativeNotes() {
		return $this->m_strAdministrativeNotes;
	}

	public function sqlAdministrativeNotes() {
		return ( true == isset( $this->m_strAdministrativeNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAdministrativeNotes ) : '\'' . addslashes( $this->m_strAdministrativeNotes ) . '\'' ) : 'NULL';
	}

	public function setReferenceFeedback( $strReferenceFeedback ) {
		$this->set( 'm_strReferenceFeedback', CStrings::strTrimDef( $strReferenceFeedback, -1, NULL, true ) );
	}

	public function getReferenceFeedback() {
		return $this->m_strReferenceFeedback;
	}

	public function sqlReferenceFeedback() {
		return ( true == isset( $this->m_strReferenceFeedback ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReferenceFeedback ) : '\'' . addslashes( $this->m_strReferenceFeedback ) . '\'' ) : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 23, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
	}

	public function setRefereeDetails( $strRefereeDetails ) {
		$this->set( 'm_strRefereeDetails', CStrings::strTrimDef( $strRefereeDetails, -1, NULL, true ) );
	}

	public function getRefereeDetails() {
		return $this->m_strRefereeDetails;
	}

	public function sqlRefereeDetails() {
		return ( true == isset( $this->m_strRefereeDetails ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRefereeDetails ) : '\'' . addslashes( $this->m_strRefereeDetails ) . '\'' ) : 'NULL';
	}

	public function setAllowCreditScreening( $intAllowCreditScreening ) {
		$this->set( 'm_intAllowCreditScreening', CStrings::strToIntDef( $intAllowCreditScreening, NULL, false ) );
	}

	public function getAllowCreditScreening() {
		return $this->m_intAllowCreditScreening;
	}

	public function sqlAllowCreditScreening() {
		return ( true == isset( $this->m_intAllowCreditScreening ) ) ? ( string ) $this->m_intAllowCreditScreening : 'NULL';
	}

	public function setAppliedOn( $strAppliedOn ) {
		$this->set( 'm_strAppliedOn', CStrings::strTrimDef( $strAppliedOn, -1, NULL, true ) );
	}

	public function getAppliedOn() {
		return $this->m_strAppliedOn;
	}

	public function sqlAppliedOn() {
		return ( true == isset( $this->m_strAppliedOn ) ) ? '\'' . $this->m_strAppliedOn . '\'' : 'NOW()';
	}

	public function setRejectedOn( $strRejectedOn ) {
		$this->set( 'm_strRejectedOn', CStrings::strTrimDef( $strRejectedOn, -1, NULL, true ) );
	}

	public function getRejectedOn() {
		return $this->m_strRejectedOn;
	}

	public function sqlRejectedOn() {
		return ( true == isset( $this->m_strRejectedOn ) ) ? '\'' . $this->m_strRejectedOn . '\'' : 'NULL';
	}

	public function setApplicantDeclinedOn( $strApplicantDeclinedOn ) {
		$this->set( 'm_strApplicantDeclinedOn', CStrings::strTrimDef( $strApplicantDeclinedOn, -1, NULL, true ) );
	}

	public function getApplicantDeclinedOn() {
		return $this->m_strApplicantDeclinedOn;
	}

	public function sqlApplicantDeclinedOn() {
		return ( true == isset( $this->m_strApplicantDeclinedOn ) ) ? '\'' . $this->m_strApplicantDeclinedOn . '\'' : 'NULL';
	}

	public function setApplicantOfferredOn( $strApplicantOfferredOn ) {
		$this->set( 'm_strApplicantOfferredOn', CStrings::strTrimDef( $strApplicantOfferredOn, -1, NULL, true ) );
	}

	public function getApplicantOfferredOn() {
		return $this->m_strApplicantOfferredOn;
	}

	public function sqlApplicantOfferredOn() {
		return ( true == isset( $this->m_strApplicantOfferredOn ) ) ? '\'' . $this->m_strApplicantOfferredOn . '\'' : 'NULL';
	}

	public function setApplicantScreenedOn( $strApplicantScreenedOn ) {
		$this->set( 'm_strApplicantScreenedOn', CStrings::strTrimDef( $strApplicantScreenedOn, -1, NULL, true ) );
	}

	public function getApplicantScreenedOn() {
		return $this->m_strApplicantScreenedOn;
	}

	public function sqlApplicantScreenedOn() {
		return ( true == isset( $this->m_strApplicantScreenedOn ) ) ? '\'' . $this->m_strApplicantScreenedOn . '\'' : 'NULL';
	}

	public function setScheduledOn( $strScheduledOn ) {
		$this->set( 'm_strScheduledOn', CStrings::strTrimDef( $strScheduledOn, -1, NULL, true ) );
	}

	public function getScheduledOn() {
		return $this->m_strScheduledOn;
	}

	public function sqlScheduledOn() {
		return ( true == isset( $this->m_strScheduledOn ) ) ? '\'' . $this->m_strScheduledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSourceUpdatedOn( $strSourceUpdatedOn ) {
		$this->set( 'm_strSourceUpdatedOn', CStrings::strTrimDef( $strSourceUpdatedOn, -1, NULL, true ) );
	}

	public function getSourceUpdatedOn() {
		return $this->m_strSourceUpdatedOn;
	}

	public function sqlSourceUpdatedOn() {
		return ( true == isset( $this->m_strSourceUpdatedOn ) ) ? '\'' . $this->m_strSourceUpdatedOn . '\'' : 'NULL';
	}

	public function setIsArchived( $boolIsArchived ) {
		$this->set( 'm_boolIsArchived', CStrings::strToBool( $boolIsArchived ) );
	}

	public function getIsArchived() {
		return $this->m_boolIsArchived;
	}

	public function sqlIsArchived() {
		return ( true == isset( $this->m_boolIsArchived ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsArchived ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFavorite( $boolIsFavorite ) {
		$this->set( 'm_boolIsFavorite', CStrings::strToBool( $boolIsFavorite ) );
	}

	public function getIsFavorite() {
		return $this->m_boolIsFavorite;
	}

	public function sqlIsFavorite() {
		return ( true == isset( $this->m_boolIsFavorite ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFavorite ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setInterviewUrl( $strInterviewUrl ) {
		$this->set( 'm_strInterviewUrl', CStrings::strTrimDef( $strInterviewUrl, -1, NULL, true ) );
	}

	public function getInterviewUrl() {
		return $this->m_strInterviewUrl;
	}

	public function sqlInterviewUrl() {
		return ( true == isset( $this->m_strInterviewUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInterviewUrl ) : '\'' . addslashes( $this->m_strInterviewUrl ) . '\'' ) : 'NULL';
	}

	public function setCandidateLastUpdatedOn( $strCandidateLastUpdatedOn ) {
		$this->set( 'm_strCandidateLastUpdatedOn', CStrings::strTrimDef( $strCandidateLastUpdatedOn, -1, NULL, true ) );
	}

	public function getCandidateLastUpdatedOn() {
		return $this->m_strCandidateLastUpdatedOn;
	}

	public function sqlCandidateLastUpdatedOn() {
		return ( true == isset( $this->m_strCandidateLastUpdatedOn ) ) ? '\'' . $this->m_strCandidateLastUpdatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_application_id, employment_application_id, employee_application_status_type_id, ps_job_posting_step_status_id, employee_application_group_id, employee_id, referee_employee_id, recruiter_employee_id, employee_application_education_id, employee_application_source_id, ps_website_job_posting_id, employment_occupation_type_id, ps_document_id, designation_id, purchase_request_id, submission_datetime, name_prefix, name_first, name_middle, gender, name_last, preferred_name, phone_number, cell_number, email_address, birth_date, blood_group, digital_signature, tax_number_encrypted, street_line1, street_line2, city, state_code, state, province, country_code, postal_code, test_score_1, test_score_2, test_score_3, test_score_4, referred_by_name, education_description, administrative_notes, reference_feedback, ip_address, referee_details, allow_credit_screening, applied_on, rejected_on, applicant_declined_on, applicant_offerred_on, applicant_screened_on, scheduled_on, updated_by, updated_on, created_by, created_on, source_updated_on, is_archived, is_favorite, details, interview_url, candidate_last_updated_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlEmployeeApplicationId() . ', ' .
		          $this->sqlEmploymentApplicationId() . ', ' .
		          $this->sqlEmployeeApplicationStatusTypeId() . ', ' .
		          $this->sqlPsJobPostingStepStatusId() . ', ' .
		          $this->sqlEmployeeApplicationGroupId() . ', ' .
		          $this->sqlEmployeeId() . ', ' .
		          $this->sqlRefereeEmployeeId() . ', ' .
		          $this->sqlRecruiterEmployeeId() . ', ' .
		          $this->sqlEmployeeApplicationEducationId() . ', ' .
		          $this->sqlEmployeeApplicationSourceId() . ', ' .
		          $this->sqlPsWebsiteJobPostingId() . ', ' .
		          $this->sqlEmploymentOccupationTypeId() . ', ' .
		          $this->sqlPsDocumentId() . ', ' .
		          $this->sqlDesignationId() . ', ' .
		          $this->sqlPurchaseRequestId() . ', ' .
		          $this->sqlSubmissionDatetime() . ', ' .
		          $this->sqlNamePrefix() . ', ' .
		          $this->sqlNameFirst() . ', ' .
		          $this->sqlNameMiddle() . ', ' .
		          $this->sqlGender() . ', ' .
		          $this->sqlNameLast() . ', ' .
		          $this->sqlPreferredName() . ', ' .
		          $this->sqlPhoneNumber() . ', ' .
		          $this->sqlCellNumber() . ', ' .
		          $this->sqlEmailAddress() . ', ' .
		          $this->sqlBirthDate() . ', ' .
		          $this->sqlBloodGroup() . ', ' .
		          $this->sqlDigitalSignature() . ', ' .
		          $this->sqlTaxNumberEncrypted() . ', ' .
		          $this->sqlStreetLine1() . ', ' .
		          $this->sqlStreetLine2() . ', ' .
		          $this->sqlCity() . ', ' .
		          $this->sqlStateCode() . ', ' .
		          $this->sqlState() . ', ' .
		          $this->sqlProvince() . ', ' .
		          $this->sqlCountryCode() . ', ' .
		          $this->sqlPostalCode() . ', ' .
		          $this->sqlTestScore1() . ', ' .
		          $this->sqlTestScore2() . ', ' .
		          $this->sqlTestScore3() . ', ' .
		          $this->sqlTestScore4() . ', ' .
		          $this->sqlReferredByName() . ', ' .
		          $this->sqlEducationDescription() . ', ' .
		          $this->sqlAdministrativeNotes() . ', ' .
		          $this->sqlReferenceFeedback() . ', ' .
		          $this->sqlIpAddress() . ', ' .
		          $this->sqlRefereeDetails() . ', ' .
		          $this->sqlAllowCreditScreening() . ', ' .
		          $this->sqlAppliedOn() . ', ' .
		          $this->sqlRejectedOn() . ', ' .
		          $this->sqlApplicantDeclinedOn() . ', ' .
		          $this->sqlApplicantOfferredOn() . ', ' .
		          $this->sqlApplicantScreenedOn() . ', ' .
		          $this->sqlScheduledOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlSourceUpdatedOn() . ', ' .
		          $this->sqlIsArchived() . ', ' .
		          $this->sqlIsFavorite() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlInterviewUrl() . ', ' .
		          $this->sqlCandidateLastUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId(). ',' ; } elseif( true == array_key_exists( 'EmployeeApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employment_application_id = ' . $this->sqlEmploymentApplicationId(). ',' ; } elseif( true == array_key_exists( 'EmploymentApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employment_application_id = ' . $this->sqlEmploymentApplicationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_status_type_id = ' . $this->sqlEmployeeApplicationStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeApplicationStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_status_type_id = ' . $this->sqlEmployeeApplicationStatusTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_job_posting_step_status_id = ' . $this->sqlPsJobPostingStepStatusId(). ',' ; } elseif( true == array_key_exists( 'PsJobPostingStepStatusId', $this->getChangedColumns() ) ) { $strSql .= ' ps_job_posting_step_status_id = ' . $this->sqlPsJobPostingStepStatusId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_group_id = ' . $this->sqlEmployeeApplicationGroupId(). ',' ; } elseif( true == array_key_exists( 'EmployeeApplicationGroupId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_group_id = ' . $this->sqlEmployeeApplicationGroupId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' referee_employee_id = ' . $this->sqlRefereeEmployeeId(). ',' ; } elseif( true == array_key_exists( 'RefereeEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' referee_employee_id = ' . $this->sqlRefereeEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recruiter_employee_id = ' . $this->sqlRecruiterEmployeeId(). ',' ; } elseif( true == array_key_exists( 'RecruiterEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' recruiter_employee_id = ' . $this->sqlRecruiterEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_education_id = ' . $this->sqlEmployeeApplicationEducationId(). ',' ; } elseif( true == array_key_exists( 'EmployeeApplicationEducationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_education_id = ' . $this->sqlEmployeeApplicationEducationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_source_id = ' . $this->sqlEmployeeApplicationSourceId(). ',' ; } elseif( true == array_key_exists( 'EmployeeApplicationSourceId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_source_id = ' . $this->sqlEmployeeApplicationSourceId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_website_job_posting_id = ' . $this->sqlPsWebsiteJobPostingId(). ',' ; } elseif( true == array_key_exists( 'PsWebsiteJobPostingId', $this->getChangedColumns() ) ) { $strSql .= ' ps_website_job_posting_id = ' . $this->sqlPsWebsiteJobPostingId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employment_occupation_type_id = ' . $this->sqlEmploymentOccupationTypeId(). ',' ; } elseif( true == array_key_exists( 'EmploymentOccupationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employment_occupation_type_id = ' . $this->sqlEmploymentOccupationTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId(). ',' ; } elseif( true == array_key_exists( 'PsDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId(). ',' ; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId(). ',' ; } elseif( true == array_key_exists( 'PurchaseRequestId', $this->getChangedColumns() ) ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' submission_datetime = ' . $this->sqlSubmissionDatetime(). ',' ; } elseif( true == array_key_exists( 'SubmissionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' submission_datetime = ' . $this->sqlSubmissionDatetime() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix(). ',' ; } elseif( true == array_key_exists( 'NamePrefix', $this->getChangedColumns() ) ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle(). ',' ; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gender = ' . $this->sqlGender(). ',' ; } elseif( true == array_key_exists( 'Gender', $this->getChangedColumns() ) ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preferred_name = ' . $this->sqlPreferredName(). ',' ; } elseif( true == array_key_exists( 'PreferredName', $this->getChangedColumns() ) ) { $strSql .= ' preferred_name = ' . $this->sqlPreferredName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cell_number = ' . $this->sqlCellNumber(). ',' ; } elseif( true == array_key_exists( 'CellNumber', $this->getChangedColumns() ) ) { $strSql .= ' cell_number = ' . $this->sqlCellNumber() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate(). ',' ; } elseif( true == array_key_exists( 'BirthDate', $this->getChangedColumns() ) ) { $strSql .= ' birth_date = ' . $this->sqlBirthDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' blood_group = ' . $this->sqlBloodGroup(). ',' ; } elseif( true == array_key_exists( 'BloodGroup', $this->getChangedColumns() ) ) { $strSql .= ' blood_group = ' . $this->sqlBloodGroup() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' digital_signature = ' . $this->sqlDigitalSignature(). ',' ; } elseif( true == array_key_exists( 'DigitalSignature', $this->getChangedColumns() ) ) { $strSql .= ' digital_signature = ' . $this->sqlDigitalSignature() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'TaxNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' tax_number_encrypted = ' . $this->sqlTaxNumberEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1(). ',' ; } elseif( true == array_key_exists( 'StreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' street_line1 = ' . $this->sqlStreetLine1() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2(). ',' ; } elseif( true == array_key_exists( 'StreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' street_line2 = ' . $this->sqlStreetLine2() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state = ' . $this->sqlState(). ',' ; } elseif( true == array_key_exists( 'State', $this->getChangedColumns() ) ) { $strSql .= ' state = ' . $this->sqlState() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' province = ' . $this->sqlProvince(). ',' ; } elseif( true == array_key_exists( 'Province', $this->getChangedColumns() ) ) { $strSql .= ' province = ' . $this->sqlProvince() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_score_1 = ' . $this->sqlTestScore1(). ',' ; } elseif( true == array_key_exists( 'TestScore1', $this->getChangedColumns() ) ) { $strSql .= ' test_score_1 = ' . $this->sqlTestScore1() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_score_2 = ' . $this->sqlTestScore2(). ',' ; } elseif( true == array_key_exists( 'TestScore2', $this->getChangedColumns() ) ) { $strSql .= ' test_score_2 = ' . $this->sqlTestScore2() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_score_3 = ' . $this->sqlTestScore3(). ',' ; } elseif( true == array_key_exists( 'TestScore3', $this->getChangedColumns() ) ) { $strSql .= ' test_score_3 = ' . $this->sqlTestScore3() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_score_4 = ' . $this->sqlTestScore4(). ',' ; } elseif( true == array_key_exists( 'TestScore4', $this->getChangedColumns() ) ) { $strSql .= ' test_score_4 = ' . $this->sqlTestScore4() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' referred_by_name = ' . $this->sqlReferredByName(). ',' ; } elseif( true == array_key_exists( 'ReferredByName', $this->getChangedColumns() ) ) { $strSql .= ' referred_by_name = ' . $this->sqlReferredByName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' education_description = ' . $this->sqlEducationDescription(). ',' ; } elseif( true == array_key_exists( 'EducationDescription', $this->getChangedColumns() ) ) { $strSql .= ' education_description = ' . $this->sqlEducationDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' administrative_notes = ' . $this->sqlAdministrativeNotes(). ',' ; } elseif( true == array_key_exists( 'AdministrativeNotes', $this->getChangedColumns() ) ) { $strSql .= ' administrative_notes = ' . $this->sqlAdministrativeNotes() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_feedback = ' . $this->sqlReferenceFeedback(). ',' ; } elseif( true == array_key_exists( 'ReferenceFeedback', $this->getChangedColumns() ) ) { $strSql .= ' reference_feedback = ' . $this->sqlReferenceFeedback() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' referee_details = ' . $this->sqlRefereeDetails(). ',' ; } elseif( true == array_key_exists( 'RefereeDetails', $this->getChangedColumns() ) ) { $strSql .= ' referee_details = ' . $this->sqlRefereeDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_credit_screening = ' . $this->sqlAllowCreditScreening(). ',' ; } elseif( true == array_key_exists( 'AllowCreditScreening', $this->getChangedColumns() ) ) { $strSql .= ' allow_credit_screening = ' . $this->sqlAllowCreditScreening() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applied_on = ' . $this->sqlAppliedOn(). ',' ; } elseif( true == array_key_exists( 'AppliedOn', $this->getChangedColumns() ) ) { $strSql .= ' applied_on = ' . $this->sqlAppliedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn(). ',' ; } elseif( true == array_key_exists( 'RejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_declined_on = ' . $this->sqlApplicantDeclinedOn(). ',' ; } elseif( true == array_key_exists( 'ApplicantDeclinedOn', $this->getChangedColumns() ) ) { $strSql .= ' applicant_declined_on = ' . $this->sqlApplicantDeclinedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_offerred_on = ' . $this->sqlApplicantOfferredOn(). ',' ; } elseif( true == array_key_exists( 'ApplicantOfferredOn', $this->getChangedColumns() ) ) { $strSql .= ' applicant_offerred_on = ' . $this->sqlApplicantOfferredOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' applicant_screened_on = ' . $this->sqlApplicantScreenedOn(). ',' ; } elseif( true == array_key_exists( 'ApplicantScreenedOn', $this->getChangedColumns() ) ) { $strSql .= ' applicant_screened_on = ' . $this->sqlApplicantScreenedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn(). ',' ; } elseif( true == array_key_exists( 'ScheduledOn', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_updated_on = ' . $this->sqlSourceUpdatedOn(). ',' ; } elseif( true == array_key_exists( 'SourceUpdatedOn', $this->getChangedColumns() ) ) { $strSql .= ' source_updated_on = ' . $this->sqlSourceUpdatedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived(). ',' ; } elseif( true == array_key_exists( 'IsArchived', $this->getChangedColumns() ) ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_favorite = ' . $this->sqlIsFavorite(). ',' ; } elseif( true == array_key_exists( 'IsFavorite', $this->getChangedColumns() ) ) { $strSql .= ' is_favorite = ' . $this->sqlIsFavorite() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interview_url = ' . $this->sqlInterviewUrl(). ',' ; } elseif( true == array_key_exists( 'InterviewUrl', $this->getChangedColumns() ) ) { $strSql .= ' interview_url = ' . $this->sqlInterviewUrl() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' candidate_last_updated_on = ' . $this->sqlCandidateLastUpdatedOn(). ',' ; } elseif( true == array_key_exists( 'CandidateLastUpdatedOn', $this->getChangedColumns() ) ) { $strSql .= ' candidate_last_updated_on = ' . $this->sqlCandidateLastUpdatedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_application_id' => $this->getEmployeeApplicationId(),
			'employment_application_id' => $this->getEmploymentApplicationId(),
			'employee_application_status_type_id' => $this->getEmployeeApplicationStatusTypeId(),
			'ps_job_posting_step_status_id' => $this->getPsJobPostingStepStatusId(),
			'employee_application_group_id' => $this->getEmployeeApplicationGroupId(),
			'employee_id' => $this->getEmployeeId(),
			'referee_employee_id' => $this->getRefereeEmployeeId(),
			'recruiter_employee_id' => $this->getRecruiterEmployeeId(),
			'employee_application_education_id' => $this->getEmployeeApplicationEducationId(),
			'employee_application_source_id' => $this->getEmployeeApplicationSourceId(),
			'ps_website_job_posting_id' => $this->getPsWebsiteJobPostingId(),
			'employment_occupation_type_id' => $this->getEmploymentOccupationTypeId(),
			'ps_document_id' => $this->getPsDocumentId(),
			'designation_id' => $this->getDesignationId(),
			'purchase_request_id' => $this->getPurchaseRequestId(),
			'submission_datetime' => $this->getSubmissionDatetime(),
			'name_prefix' => $this->getNamePrefix(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'gender' => $this->getGender(),
			'name_last' => $this->getNameLast(),
			'preferred_name' => $this->getPreferredName(),
			'phone_number' => $this->getPhoneNumber(),
			'cell_number' => $this->getCellNumber(),
			'email_address' => $this->getEmailAddress(),
			'birth_date' => $this->getBirthDate(),
			'blood_group' => $this->getBloodGroup(),
			'digital_signature' => $this->getDigitalSignature(),
			'tax_number_encrypted' => $this->getTaxNumberEncrypted(),
			'street_line1' => $this->getStreetLine1(),
			'street_line2' => $this->getStreetLine2(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'state' => $this->getState(),
			'province' => $this->getProvince(),
			'country_code' => $this->getCountryCode(),
			'postal_code' => $this->getPostalCode(),
			'test_score_1' => $this->getTestScore1(),
			'test_score_2' => $this->getTestScore2(),
			'test_score_3' => $this->getTestScore3(),
			'test_score_4' => $this->getTestScore4(),
			'referred_by_name' => $this->getReferredByName(),
			'education_description' => $this->getEducationDescription(),
			'administrative_notes' => $this->getAdministrativeNotes(),
			'reference_feedback' => $this->getReferenceFeedback(),
			'ip_address' => $this->getIpAddress(),
			'referee_details' => $this->getRefereeDetails(),
			'allow_credit_screening' => $this->getAllowCreditScreening(),
			'applied_on' => $this->getAppliedOn(),
			'rejected_on' => $this->getRejectedOn(),
			'applicant_declined_on' => $this->getApplicantDeclinedOn(),
			'applicant_offerred_on' => $this->getApplicantOfferredOn(),
			'applicant_screened_on' => $this->getApplicantScreenedOn(),
			'scheduled_on' => $this->getScheduledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'source_updated_on' => $this->getSourceUpdatedOn(),
			'is_archived' => $this->getIsArchived(),
			'is_favorite' => $this->getIsFavorite(),
			'details' => $this->getDetails(),
			'interview_url' => $this->getInterviewUrl(),
			'candidate_last_updated_on' => $this->getCandidateLastUpdatedOn()
		);
	}

}
?>