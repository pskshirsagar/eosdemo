<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskIssueTypes
 * Do not add any new functions to this class.
 */

class CBaseTaskIssueTypes extends CEosPluralBase {

	/**
	 * @return CTaskIssueType[]
	 */
	public static function fetchTaskIssueTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskIssueType', $objDatabase );
	}

	/**
	 * @return CTaskIssueType
	 */
	public static function fetchTaskIssueType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskIssueType', $objDatabase );
	}

	public static function fetchTaskIssueTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_issue_types', $objDatabase );
	}

	public static function fetchTaskIssueTypeById( $intId, $objDatabase ) {
		return self::fetchTaskIssueType( sprintf( 'SELECT * FROM task_issue_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>