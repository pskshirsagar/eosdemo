<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemBudgets
 * Do not add any new functions to this class.
 */

class CBaseSemBudgets extends CEosPluralBase {

	/**
	 * @return CSemBudget[]
	 */
	public static function fetchSemBudgets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemBudget', $objDatabase );
	}

	/**
	 * @return CSemBudget
	 */
	public static function fetchSemBudget( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemBudget', $objDatabase );
	}

	public static function fetchSemBudgetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_budgets', $objDatabase );
	}

	public static function fetchSemBudgetById( $intId, $objDatabase ) {
		return self::fetchSemBudget( sprintf( 'SELECT * FROM sem_budgets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemBudgetsByCid( $intCid, $objDatabase ) {
		return self::fetchSemBudgets( sprintf( 'SELECT * FROM sem_budgets WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSemBudgetsByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchSemBudgets( sprintf( 'SELECT * FROM sem_budgets WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

	public static function fetchSemBudgetsBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemBudgets( sprintf( 'SELECT * FROM sem_budgets WHERE sem_ad_group_id = %d', ( int ) $intSemAdGroupId ), $objDatabase );
	}

	public static function fetchSemBudgetsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSemBudgets( sprintf( 'SELECT * FROM sem_budgets WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>