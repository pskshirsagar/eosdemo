<?php

class CBaseStatsUnitTransactionalAverage extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_unit_transactional_averages';

	protected $m_intId;
	protected $m_intPsProductId;
	protected $m_strProductName;
	protected $m_strMonth;
	protected $m_intTotalUnits;
	protected $m_intTotalUnitsWeighted;
	protected $m_intTransactionRevenue;
	protected $m_fltUnitTransactionalAverageByMonth;
	protected $m_fltUnitTransactionalAverage;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['product_name'] ) && $boolDirectSet ) $this->set( 'm_strProductName', trim( stripcslashes( $arrValues['product_name'] ) ) ); elseif( isset( $arrValues['product_name'] ) ) $this->setProductName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['product_name'] ) : $arrValues['product_name'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['total_units'] ) && $boolDirectSet ) $this->set( 'm_intTotalUnits', trim( $arrValues['total_units'] ) ); elseif( isset( $arrValues['total_units'] ) ) $this->setTotalUnits( $arrValues['total_units'] );
		if( isset( $arrValues['total_units_weighted'] ) && $boolDirectSet ) $this->set( 'm_intTotalUnitsWeighted', trim( $arrValues['total_units_weighted'] ) ); elseif( isset( $arrValues['total_units_weighted'] ) ) $this->setTotalUnitsWeighted( $arrValues['total_units_weighted'] );
		if( isset( $arrValues['transaction_revenue'] ) && $boolDirectSet ) $this->set( 'm_intTransactionRevenue', trim( $arrValues['transaction_revenue'] ) ); elseif( isset( $arrValues['transaction_revenue'] ) ) $this->setTransactionRevenue( $arrValues['transaction_revenue'] );
		if( isset( $arrValues['unit_transactional_average_by_month'] ) && $boolDirectSet ) $this->set( 'm_fltUnitTransactionalAverageByMonth', trim( $arrValues['unit_transactional_average_by_month'] ) ); elseif( isset( $arrValues['unit_transactional_average_by_month'] ) ) $this->setUnitTransactionalAverageByMonth( $arrValues['unit_transactional_average_by_month'] );
		if( isset( $arrValues['unit_transactional_average'] ) && $boolDirectSet ) $this->set( 'm_fltUnitTransactionalAverage', trim( $arrValues['unit_transactional_average'] ) ); elseif( isset( $arrValues['unit_transactional_average'] ) ) $this->setUnitTransactionalAverage( $arrValues['unit_transactional_average'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setProductName( $strProductName ) {
		$this->set( 'm_strProductName', CStrings::strTrimDef( $strProductName, 50, NULL, true ) );
	}

	public function getProductName() {
		return $this->m_strProductName;
	}

	public function sqlProductName() {
		return ( true == isset( $this->m_strProductName ) ) ? '\'' . addslashes( $this->m_strProductName ) . '\'' : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NULL';
	}

	public function setTotalUnits( $intTotalUnits ) {
		$this->set( 'm_intTotalUnits', CStrings::strToIntDef( $intTotalUnits, NULL, false ) );
	}

	public function getTotalUnits() {
		return $this->m_intTotalUnits;
	}

	public function sqlTotalUnits() {
		return ( true == isset( $this->m_intTotalUnits ) ) ? ( string ) $this->m_intTotalUnits : 'NULL';
	}

	public function setTotalUnitsWeighted( $intTotalUnitsWeighted ) {
		$this->set( 'm_intTotalUnitsWeighted', CStrings::strToIntDef( $intTotalUnitsWeighted, NULL, false ) );
	}

	public function getTotalUnitsWeighted() {
		return $this->m_intTotalUnitsWeighted;
	}

	public function sqlTotalUnitsWeighted() {
		return ( true == isset( $this->m_intTotalUnitsWeighted ) ) ? ( string ) $this->m_intTotalUnitsWeighted : 'NULL';
	}

	public function setTransactionRevenue( $intTransactionRevenue ) {
		$this->set( 'm_intTransactionRevenue', CStrings::strToIntDef( $intTransactionRevenue, NULL, false ) );
	}

	public function getTransactionRevenue() {
		return $this->m_intTransactionRevenue;
	}

	public function sqlTransactionRevenue() {
		return ( true == isset( $this->m_intTransactionRevenue ) ) ? ( string ) $this->m_intTransactionRevenue : 'NULL';
	}

	public function setUnitTransactionalAverageByMonth( $fltUnitTransactionalAverageByMonth ) {
		$this->set( 'm_fltUnitTransactionalAverageByMonth', CStrings::strToFloatDef( $fltUnitTransactionalAverageByMonth, NULL, false, 0 ) );
	}

	public function getUnitTransactionalAverageByMonth() {
		return $this->m_fltUnitTransactionalAverageByMonth;
	}

	public function sqlUnitTransactionalAverageByMonth() {
		return ( true == isset( $this->m_fltUnitTransactionalAverageByMonth ) ) ? ( string ) $this->m_fltUnitTransactionalAverageByMonth : 'NULL';
	}

	public function setUnitTransactionalAverage( $fltUnitTransactionalAverage ) {
		$this->set( 'm_fltUnitTransactionalAverage', CStrings::strToFloatDef( $fltUnitTransactionalAverage, NULL, false, 0 ) );
	}

	public function getUnitTransactionalAverage() {
		return $this->m_fltUnitTransactionalAverage;
	}

	public function sqlUnitTransactionalAverage() {
		return ( true == isset( $this->m_fltUnitTransactionalAverage ) ) ? ( string ) $this->m_fltUnitTransactionalAverage : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_product_id, product_name, month, total_units, total_units_weighted, transaction_revenue, unit_transactional_average_by_month, unit_transactional_average, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlProductName() . ', ' .
 						$this->sqlMonth() . ', ' .
 						$this->sqlTotalUnits() . ', ' .
 						$this->sqlTotalUnitsWeighted() . ', ' .
 						$this->sqlTransactionRevenue() . ', ' .
 						$this->sqlUnitTransactionalAverageByMonth() . ', ' .
 						$this->sqlUnitTransactionalAverage() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_name = ' . $this->sqlProductName() . ','; } elseif( true == array_key_exists( 'ProductName', $this->getChangedColumns() ) ) { $strSql .= ' product_name = ' . $this->sqlProductName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_units = ' . $this->sqlTotalUnits() . ','; } elseif( true == array_key_exists( 'TotalUnits', $this->getChangedColumns() ) ) { $strSql .= ' total_units = ' . $this->sqlTotalUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_units_weighted = ' . $this->sqlTotalUnitsWeighted() . ','; } elseif( true == array_key_exists( 'TotalUnitsWeighted', $this->getChangedColumns() ) ) { $strSql .= ' total_units_weighted = ' . $this->sqlTotalUnitsWeighted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_revenue = ' . $this->sqlTransactionRevenue() . ','; } elseif( true == array_key_exists( 'TransactionRevenue', $this->getChangedColumns() ) ) { $strSql .= ' transaction_revenue = ' . $this->sqlTransactionRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_transactional_average_by_month = ' . $this->sqlUnitTransactionalAverageByMonth() . ','; } elseif( true == array_key_exists( 'UnitTransactionalAverageByMonth', $this->getChangedColumns() ) ) { $strSql .= ' unit_transactional_average_by_month = ' . $this->sqlUnitTransactionalAverageByMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_transactional_average = ' . $this->sqlUnitTransactionalAverage() . ','; } elseif( true == array_key_exists( 'UnitTransactionalAverage', $this->getChangedColumns() ) ) { $strSql .= ' unit_transactional_average = ' . $this->sqlUnitTransactionalAverage() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_product_id' => $this->getPsProductId(),
			'product_name' => $this->getProductName(),
			'month' => $this->getMonth(),
			'total_units' => $this->getTotalUnits(),
			'total_units_weighted' => $this->getTotalUnitsWeighted(),
			'transaction_revenue' => $this->getTransactionRevenue(),
			'unit_transactional_average_by_month' => $this->getUnitTransactionalAverageByMonth(),
			'unit_transactional_average' => $this->getUnitTransactionalAverage(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>