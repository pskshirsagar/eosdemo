<?php

class CBaseDesignationBonusCriteria extends CEosSingularBase {

	const TABLE_NAME = 'public.designation_bonus_criterias';

	protected $m_intId;
	protected $m_intDesignationId;
	protected $m_strEncryptedAnnualBonusPotential;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strBonusCriteria;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeniedBy;
	protected $m_strDeniedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intAnnualBonusTypeId;
	protected $m_boolOverwriteEmployeeBonus;

	public function __construct() {
		parent::__construct();

		$this->m_boolOverwriteEmployeeBonus = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['encrypted_annual_bonus_potential'] ) && $boolDirectSet ) $this->set( 'm_strEncryptedAnnualBonusPotential', trim( $arrValues['encrypted_annual_bonus_potential'] ) ); elseif( isset( $arrValues['encrypted_annual_bonus_potential'] ) ) $this->setEncryptedAnnualBonusPotential( $arrValues['encrypted_annual_bonus_potential'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['bonus_criteria'] ) && $boolDirectSet ) $this->set( 'm_strBonusCriteria', trim( $arrValues['bonus_criteria'] ) ); elseif( isset( $arrValues['bonus_criteria'] ) ) $this->setBonusCriteria( $arrValues['bonus_criteria'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['denied_by'] ) && $boolDirectSet ) $this->set( 'm_intDeniedBy', trim( $arrValues['denied_by'] ) ); elseif( isset( $arrValues['denied_by'] ) ) $this->setDeniedBy( $arrValues['denied_by'] );
		if( isset( $arrValues['denied_on'] ) && $boolDirectSet ) $this->set( 'm_strDeniedOn', trim( $arrValues['denied_on'] ) ); elseif( isset( $arrValues['denied_on'] ) ) $this->setDeniedOn( $arrValues['denied_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['annual_bonus_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAnnualBonusTypeId', trim( $arrValues['annual_bonus_type_id'] ) ); elseif( isset( $arrValues['annual_bonus_type_id'] ) ) $this->setAnnualBonusTypeId( $arrValues['annual_bonus_type_id'] );
		if( isset( $arrValues['overwrite_employee_bonus'] ) && $boolDirectSet ) $this->set( 'm_boolOverwriteEmployeeBonus', trim( stripcslashes( $arrValues['overwrite_employee_bonus'] ) ) ); elseif( isset( $arrValues['overwrite_employee_bonus'] ) ) $this->setOverwriteEmployeeBonus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['overwrite_employee_bonus'] ) : $arrValues['overwrite_employee_bonus'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setEncryptedAnnualBonusPotential( $strEncryptedAnnualBonusPotential ) {
		$this->set( 'm_strEncryptedAnnualBonusPotential', CStrings::strTrimDef( $strEncryptedAnnualBonusPotential, 240, NULL, true ) );
	}

	public function getEncryptedAnnualBonusPotential() {
		return $this->m_strEncryptedAnnualBonusPotential;
	}

	public function sqlEncryptedAnnualBonusPotential() {
		return ( true == isset( $this->m_strEncryptedAnnualBonusPotential ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEncryptedAnnualBonusPotential ) : '\'' . addslashes( $this->m_strEncryptedAnnualBonusPotential ) . '\'' ) : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setBonusCriteria( $strBonusCriteria ) {
		$this->set( 'm_strBonusCriteria', CStrings::strTrimDef( $strBonusCriteria, -1, NULL, true ) );
	}

	public function getBonusCriteria() {
		return $this->m_strBonusCriteria;
	}

	public function sqlBonusCriteria() {
		return ( true == isset( $this->m_strBonusCriteria ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBonusCriteria ) : '\'' . addslashes( $this->m_strBonusCriteria ) . '\'' ) : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeniedBy( $intDeniedBy ) {
		$this->set( 'm_intDeniedBy', CStrings::strToIntDef( $intDeniedBy, NULL, false ) );
	}

	public function getDeniedBy() {
		return $this->m_intDeniedBy;
	}

	public function sqlDeniedBy() {
		return ( true == isset( $this->m_intDeniedBy ) ) ? ( string ) $this->m_intDeniedBy : 'NULL';
	}

	public function setDeniedOn( $strDeniedOn ) {
		$this->set( 'm_strDeniedOn', CStrings::strTrimDef( $strDeniedOn, -1, NULL, true ) );
	}

	public function getDeniedOn() {
		return $this->m_strDeniedOn;
	}

	public function sqlDeniedOn() {
		return ( true == isset( $this->m_strDeniedOn ) ) ? '\'' . $this->m_strDeniedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAnnualBonusTypeId( $intAnnualBonusTypeId ) {
		$this->set( 'm_intAnnualBonusTypeId', CStrings::strToIntDef( $intAnnualBonusTypeId, NULL, false ) );
	}

	public function getAnnualBonusTypeId() {
		return $this->m_intAnnualBonusTypeId;
	}

	public function sqlAnnualBonusTypeId() {
		return ( true == isset( $this->m_intAnnualBonusTypeId ) ) ? ( string ) $this->m_intAnnualBonusTypeId : 'NULL';
	}

	public function setOverwriteEmployeeBonus( $boolOverwriteEmployeeBonus ) {
		$this->set( 'm_boolOverwriteEmployeeBonus', CStrings::strToBool( $boolOverwriteEmployeeBonus ) );
	}

	public function getOverwriteEmployeeBonus() {
		return $this->m_boolOverwriteEmployeeBonus;
	}

	public function sqlOverwriteEmployeeBonus() {
		return ( true == isset( $this->m_boolOverwriteEmployeeBonus ) ) ? '\'' . ( true == ( bool ) $this->m_boolOverwriteEmployeeBonus ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, designation_id, encrypted_annual_bonus_potential, start_date, end_date, bonus_criteria, approved_by, approved_on, denied_by, denied_on, updated_by, updated_on, created_by, created_on, annual_bonus_type_id, overwrite_employee_bonus )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDesignationId() . ', ' .
						$this->sqlEncryptedAnnualBonusPotential() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlBonusCriteria() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlDeniedBy() . ', ' .
						$this->sqlDeniedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAnnualBonusTypeId() . ', ' .
						$this->sqlOverwriteEmployeeBonus() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId(). ',' ; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' encrypted_annual_bonus_potential = ' . $this->sqlEncryptedAnnualBonusPotential(). ',' ; } elseif( true == array_key_exists( 'EncryptedAnnualBonusPotential', $this->getChangedColumns() ) ) { $strSql .= ' encrypted_annual_bonus_potential = ' . $this->sqlEncryptedAnnualBonusPotential() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bonus_criteria = ' . $this->sqlBonusCriteria(). ',' ; } elseif( true == array_key_exists( 'BonusCriteria', $this->getChangedColumns() ) ) { $strSql .= ' bonus_criteria = ' . $this->sqlBonusCriteria() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy(). ',' ; } elseif( true == array_key_exists( 'DeniedBy', $this->getChangedColumns() ) ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn(). ',' ; } elseif( true == array_key_exists( 'DeniedOn', $this->getChangedColumns() ) ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' annual_bonus_type_id = ' . $this->sqlAnnualBonusTypeId(). ',' ; } elseif( true == array_key_exists( 'AnnualBonusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' annual_bonus_type_id = ' . $this->sqlAnnualBonusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' overwrite_employee_bonus = ' . $this->sqlOverwriteEmployeeBonus(). ',' ; } elseif( true == array_key_exists( 'OverwriteEmployeeBonus', $this->getChangedColumns() ) ) { $strSql .= ' overwrite_employee_bonus = ' . $this->sqlOverwriteEmployeeBonus() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'designation_id' => $this->getDesignationId(),
			'encrypted_annual_bonus_potential' => $this->getEncryptedAnnualBonusPotential(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'bonus_criteria' => $this->getBonusCriteria(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'denied_by' => $this->getDeniedBy(),
			'denied_on' => $this->getDeniedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'annual_bonus_type_id' => $this->getAnnualBonusTypeId(),
			'overwrite_employee_bonus' => $this->getOverwriteEmployeeBonus()
		);
	}

}
?>