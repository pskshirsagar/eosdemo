<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseReferences
 * Do not add any new functions to this class.
 */

class CBaseReleaseReferences extends CEosPluralBase {

	/**
	 * @return CReleaseReference[]
	 */
	public static function fetchReleaseReferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CReleaseReference', $objDatabase );
	}

	/**
	 * @return CReleaseReference
	 */
	public static function fetchReleaseReference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReleaseReference', $objDatabase );
	}

	public static function fetchReleaseReferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'release_references', $objDatabase );
	}

	public static function fetchReleaseReferenceById( $intId, $objDatabase ) {
		return self::fetchReleaseReference( sprintf( 'SELECT * FROM release_references WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReleaseReferencesByReleaseTypeId( $intReleaseTypeId, $objDatabase ) {
		return self::fetchReleaseReferences( sprintf( 'SELECT * FROM release_references WHERE release_type_id = %d', ( int ) $intReleaseTypeId ), $objDatabase );
	}

	public static function fetchReleaseReferencesByReleaseReferenceTypeId( $intReleaseReferenceTypeId, $objDatabase ) {
		return self::fetchReleaseReferences( sprintf( 'SELECT * FROM release_references WHERE release_reference_type_id = %d', ( int ) $intReleaseReferenceTypeId ), $objDatabase );
	}

}
?>