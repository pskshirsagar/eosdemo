<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPersonEmployees
 * Do not add any new functions to this class.
 */

class CBasePersonEmployees extends CEosPluralBase {

	/**
	 * @return CPersonEmployee[]
	 */
	public static function fetchPersonEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPersonEmployee', $objDatabase );
	}

	/**
	 * @return CPersonEmployee
	 */
	public static function fetchPersonEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPersonEmployee', $objDatabase );
	}

	public static function fetchPersonEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'person_employees', $objDatabase );
	}

	public static function fetchPersonEmployeeById( $intId, $objDatabase ) {
		return self::fetchPersonEmployee( sprintf( 'SELECT * FROM person_employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPersonEmployeesByPersonId( $intPersonId, $objDatabase ) {
		return self::fetchPersonEmployees( sprintf( 'SELECT * FROM person_employees WHERE person_id = %d', ( int ) $intPersonId ), $objDatabase );
	}

	public static function fetchPersonEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchPersonEmployees( sprintf( 'SELECT * FROM person_employees WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>