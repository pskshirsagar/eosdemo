<?php

class CBasePsProduct extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_products';

	protected $m_intId;
	protected $m_intPsProductId;
	protected $m_intParentPsProductBundleId;
	protected $m_intPsLeadId;
	protected $m_intRecurringChargeCodeId;
	protected $m_intImplementationChargeCodeId;
	protected $m_intTrainingChargeCodeId;
	protected $m_intPsProductTypeId;
	protected $m_intPsProductDisplayTypeId;
	protected $m_intDefaultBillingFrequencyId;
	protected $m_intArchitectEmployeeId;
	protected $m_intSqmEmployeeId;
	protected $m_intQamEmployeeId;
	protected $m_intDoeEmployeeId;
	protected $m_intExecutiveEmployeeId;
	protected $m_intImplementationEmployeeId;
	protected $m_intProductManagerEmployeeId;
	protected $m_intProductOwnerEmployeeId;
	protected $m_intSalesEmployeeId;
	protected $m_intSdmEmployeeId;
	protected $m_intSupportEmployeeId;
	protected $m_intQaEmployeeId;
	protected $m_intTechWriterEmployeeId;
	protected $m_intTlEmployeeId;
	protected $m_intTpmEmployeeId;
	protected $m_intUxEmployeeId;
	protected $m_intBusinessAnalystEmployeeId;
	protected $m_intSmeEmployeeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strShortName;
	protected $m_fltUnitMonthlyRecurring;
	protected $m_fltSmallUnitMonthRecurring;
	protected $m_fltUnitTransactionalAverage;
	protected $m_fltPropertySetup;
	protected $m_fltPropertyTraining;
	protected $m_fltCommissionTriggerPercent;
	protected $m_strHexColor;
	protected $m_intMinUnits;
	protected $m_intMaxUnits;
	protected $m_intAllowSimpleContracts;
	protected $m_intAllowInBundle;
	protected $m_intHasBookedCommissions;
	protected $m_intShowInApplicationFilter;
	protected $m_intTrackCompetitors;
	protected $m_intIsCommissioned;
	protected $m_intIsBundle;
	protected $m_intIsForSale;
	protected $m_intIsPublished;
	protected $m_intIsTransactional;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltStartingUnitTransactionalAverage;
	protected $m_fltPropertyMonthlyRecurring;

	public function __construct() {
		parent::__construct();

		$this->m_intPsProductDisplayTypeId = '2';
		$this->m_fltUnitMonthlyRecurring = '0';
		$this->m_fltSmallUnitMonthRecurring = '0';
		$this->m_fltUnitTransactionalAverage = '0';
		$this->m_fltPropertySetup = '0';
		$this->m_fltPropertyTraining = '0';
		$this->m_fltCommissionTriggerPercent = '0';
		$this->m_strHexColor = NULL;
		$this->m_intAllowSimpleContracts = '0';
		$this->m_intAllowInBundle = '1';
		$this->m_intHasBookedCommissions = '0';
		$this->m_intShowInApplicationFilter = '0';
		$this->m_intTrackCompetitors = '0';
		$this->m_intIsCommissioned = '0';
		$this->m_intIsBundle = '0';
		$this->m_intIsForSale = '0';
		$this->m_intIsPublished = '1';
		$this->m_intIsTransactional = '0';
		$this->m_intOrderNum = '0';
		$this->m_intUpdatedBy = '1';
		$this->m_intCreatedBy = '1';
		$this->m_fltPropertyMonthlyRecurring = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['parent_ps_product_bundle_id'] ) && $boolDirectSet ) $this->set( 'm_intParentPsProductBundleId', trim( $arrValues['parent_ps_product_bundle_id'] ) ); elseif( isset( $arrValues['parent_ps_product_bundle_id'] ) ) $this->setParentPsProductBundleId( $arrValues['parent_ps_product_bundle_id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['recurring_charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intRecurringChargeCodeId', trim( $arrValues['recurring_charge_code_id'] ) ); elseif( isset( $arrValues['recurring_charge_code_id'] ) ) $this->setRecurringChargeCodeId( $arrValues['recurring_charge_code_id'] );
		if( isset( $arrValues['implementation_charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationChargeCodeId', trim( $arrValues['implementation_charge_code_id'] ) ); elseif( isset( $arrValues['implementation_charge_code_id'] ) ) $this->setImplementationChargeCodeId( $arrValues['implementation_charge_code_id'] );
		if( isset( $arrValues['training_charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingChargeCodeId', trim( $arrValues['training_charge_code_id'] ) ); elseif( isset( $arrValues['training_charge_code_id'] ) ) $this->setTrainingChargeCodeId( $arrValues['training_charge_code_id'] );
		if( isset( $arrValues['ps_product_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductTypeId', trim( $arrValues['ps_product_type_id'] ) ); elseif( isset( $arrValues['ps_product_type_id'] ) ) $this->setPsProductTypeId( $arrValues['ps_product_type_id'] );
		if( isset( $arrValues['ps_product_display_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductDisplayTypeId', trim( $arrValues['ps_product_display_type_id'] ) ); elseif( isset( $arrValues['ps_product_display_type_id'] ) ) $this->setPsProductDisplayTypeId( $arrValues['ps_product_display_type_id'] );
		if( isset( $arrValues['default_billing_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultBillingFrequencyId', trim( $arrValues['default_billing_frequency_id'] ) ); elseif( isset( $arrValues['default_billing_frequency_id'] ) ) $this->setDefaultBillingFrequencyId( $arrValues['default_billing_frequency_id'] );
		if( isset( $arrValues['architect_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intArchitectEmployeeId', trim( $arrValues['architect_employee_id'] ) ); elseif( isset( $arrValues['architect_employee_id'] ) ) $this->setArchitectEmployeeId( $arrValues['architect_employee_id'] );
		if( isset( $arrValues['sqm_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSqmEmployeeId', trim( $arrValues['sqm_employee_id'] ) ); elseif( isset( $arrValues['sqm_employee_id'] ) ) $this->setSqmEmployeeId( $arrValues['sqm_employee_id'] );
		if( isset( $arrValues['qam_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQamEmployeeId', trim( $arrValues['qam_employee_id'] ) ); elseif( isset( $arrValues['qam_employee_id'] ) ) $this->setQamEmployeeId( $arrValues['qam_employee_id'] );
		if( isset( $arrValues['doe_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intDoeEmployeeId', trim( $arrValues['doe_employee_id'] ) ); elseif( isset( $arrValues['doe_employee_id'] ) ) $this->setDoeEmployeeId( $arrValues['doe_employee_id'] );
		if( isset( $arrValues['executive_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intExecutiveEmployeeId', trim( $arrValues['executive_employee_id'] ) ); elseif( isset( $arrValues['executive_employee_id'] ) ) $this->setExecutiveEmployeeId( $arrValues['executive_employee_id'] );
		if( isset( $arrValues['implementation_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationEmployeeId', trim( $arrValues['implementation_employee_id'] ) ); elseif( isset( $arrValues['implementation_employee_id'] ) ) $this->setImplementationEmployeeId( $arrValues['implementation_employee_id'] );
		if( isset( $arrValues['product_manager_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intProductManagerEmployeeId', trim( $arrValues['product_manager_employee_id'] ) ); elseif( isset( $arrValues['product_manager_employee_id'] ) ) $this->setProductManagerEmployeeId( $arrValues['product_manager_employee_id'] );
		if( isset( $arrValues['product_owner_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intProductOwnerEmployeeId', trim( $arrValues['product_owner_employee_id'] ) ); elseif( isset( $arrValues['product_owner_employee_id'] ) ) $this->setProductOwnerEmployeeId( $arrValues['product_owner_employee_id'] );
		if( isset( $arrValues['sales_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSalesEmployeeId', trim( $arrValues['sales_employee_id'] ) ); elseif( isset( $arrValues['sales_employee_id'] ) ) $this->setSalesEmployeeId( $arrValues['sales_employee_id'] );
		if( isset( $arrValues['sdm_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSdmEmployeeId', trim( $arrValues['sdm_employee_id'] ) ); elseif( isset( $arrValues['sdm_employee_id'] ) ) $this->setSdmEmployeeId( $arrValues['sdm_employee_id'] );
		if( isset( $arrValues['support_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSupportEmployeeId', trim( $arrValues['support_employee_id'] ) ); elseif( isset( $arrValues['support_employee_id'] ) ) $this->setSupportEmployeeId( $arrValues['support_employee_id'] );
		if( isset( $arrValues['qa_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQaEmployeeId', trim( $arrValues['qa_employee_id'] ) ); elseif( isset( $arrValues['qa_employee_id'] ) ) $this->setQaEmployeeId( $arrValues['qa_employee_id'] );
		if( isset( $arrValues['tech_writer_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intTechWriterEmployeeId', trim( $arrValues['tech_writer_employee_id'] ) ); elseif( isset( $arrValues['tech_writer_employee_id'] ) ) $this->setTechWriterEmployeeId( $arrValues['tech_writer_employee_id'] );
		if( isset( $arrValues['tl_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intTlEmployeeId', trim( $arrValues['tl_employee_id'] ) ); elseif( isset( $arrValues['tl_employee_id'] ) ) $this->setTlEmployeeId( $arrValues['tl_employee_id'] );
		if( isset( $arrValues['tpm_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intTpmEmployeeId', trim( $arrValues['tpm_employee_id'] ) ); elseif( isset( $arrValues['tpm_employee_id'] ) ) $this->setTpmEmployeeId( $arrValues['tpm_employee_id'] );
		if( isset( $arrValues['ux_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intUxEmployeeId', trim( $arrValues['ux_employee_id'] ) ); elseif( isset( $arrValues['ux_employee_id'] ) ) $this->setUxEmployeeId( $arrValues['ux_employee_id'] );
		if( isset( $arrValues['business_analyst_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intBusinessAnalystEmployeeId', trim( $arrValues['business_analyst_employee_id'] ) ); elseif( isset( $arrValues['business_analyst_employee_id'] ) ) $this->setBusinessAnalystEmployeeId( $arrValues['business_analyst_employee_id'] );
		if( isset( $arrValues['sme_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSmeEmployeeId', trim( $arrValues['sme_employee_id'] ) ); elseif( isset( $arrValues['sme_employee_id'] ) ) $this->setSmeEmployeeId( $arrValues['sme_employee_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['short_name'] ) && $boolDirectSet ) $this->set( 'm_strShortName', trim( $arrValues['short_name'] ) ); elseif( isset( $arrValues['short_name'] ) ) $this->setShortName( $arrValues['short_name'] );
		if( isset( $arrValues['unit_monthly_recurring'] ) && $boolDirectSet ) $this->set( 'm_fltUnitMonthlyRecurring', trim( $arrValues['unit_monthly_recurring'] ) ); elseif( isset( $arrValues['unit_monthly_recurring'] ) ) $this->setUnitMonthlyRecurring( $arrValues['unit_monthly_recurring'] );
		if( isset( $arrValues['small_unit_month_recurring'] ) && $boolDirectSet ) $this->set( 'm_fltSmallUnitMonthRecurring', trim( $arrValues['small_unit_month_recurring'] ) ); elseif( isset( $arrValues['small_unit_month_recurring'] ) ) $this->setSmallUnitMonthRecurring( $arrValues['small_unit_month_recurring'] );
		if( isset( $arrValues['unit_transactional_average'] ) && $boolDirectSet ) $this->set( 'm_fltUnitTransactionalAverage', trim( $arrValues['unit_transactional_average'] ) ); elseif( isset( $arrValues['unit_transactional_average'] ) ) $this->setUnitTransactionalAverage( $arrValues['unit_transactional_average'] );
		if( isset( $arrValues['property_setup'] ) && $boolDirectSet ) $this->set( 'm_fltPropertySetup', trim( $arrValues['property_setup'] ) ); elseif( isset( $arrValues['property_setup'] ) ) $this->setPropertySetup( $arrValues['property_setup'] );
		if( isset( $arrValues['property_training'] ) && $boolDirectSet ) $this->set( 'm_fltPropertyTraining', trim( $arrValues['property_training'] ) ); elseif( isset( $arrValues['property_training'] ) ) $this->setPropertyTraining( $arrValues['property_training'] );
		if( isset( $arrValues['commission_trigger_percent'] ) && $boolDirectSet ) $this->set( 'm_fltCommissionTriggerPercent', trim( $arrValues['commission_trigger_percent'] ) ); elseif( isset( $arrValues['commission_trigger_percent'] ) ) $this->setCommissionTriggerPercent( $arrValues['commission_trigger_percent'] );
		if( isset( $arrValues['hex_color'] ) && $boolDirectSet ) $this->set( 'm_strHexColor', trim( $arrValues['hex_color'] ) ); elseif( isset( $arrValues['hex_color'] ) ) $this->setHexColor( $arrValues['hex_color'] );
		if( isset( $arrValues['min_units'] ) && $boolDirectSet ) $this->set( 'm_intMinUnits', trim( $arrValues['min_units'] ) ); elseif( isset( $arrValues['min_units'] ) ) $this->setMinUnits( $arrValues['min_units'] );
		if( isset( $arrValues['max_units'] ) && $boolDirectSet ) $this->set( 'm_intMaxUnits', trim( $arrValues['max_units'] ) ); elseif( isset( $arrValues['max_units'] ) ) $this->setMaxUnits( $arrValues['max_units'] );
		if( isset( $arrValues['allow_simple_contracts'] ) && $boolDirectSet ) $this->set( 'm_intAllowSimpleContracts', trim( $arrValues['allow_simple_contracts'] ) ); elseif( isset( $arrValues['allow_simple_contracts'] ) ) $this->setAllowSimpleContracts( $arrValues['allow_simple_contracts'] );
		if( isset( $arrValues['allow_in_bundle'] ) && $boolDirectSet ) $this->set( 'm_intAllowInBundle', trim( $arrValues['allow_in_bundle'] ) ); elseif( isset( $arrValues['allow_in_bundle'] ) ) $this->setAllowInBundle( $arrValues['allow_in_bundle'] );
		if( isset( $arrValues['has_booked_commissions'] ) && $boolDirectSet ) $this->set( 'm_intHasBookedCommissions', trim( $arrValues['has_booked_commissions'] ) ); elseif( isset( $arrValues['has_booked_commissions'] ) ) $this->setHasBookedCommissions( $arrValues['has_booked_commissions'] );
		if( isset( $arrValues['show_in_application_filter'] ) && $boolDirectSet ) $this->set( 'm_intShowInApplicationFilter', trim( $arrValues['show_in_application_filter'] ) ); elseif( isset( $arrValues['show_in_application_filter'] ) ) $this->setShowInApplicationFilter( $arrValues['show_in_application_filter'] );
		if( isset( $arrValues['track_competitors'] ) && $boolDirectSet ) $this->set( 'm_intTrackCompetitors', trim( $arrValues['track_competitors'] ) ); elseif( isset( $arrValues['track_competitors'] ) ) $this->setTrackCompetitors( $arrValues['track_competitors'] );
		if( isset( $arrValues['is_commissioned'] ) && $boolDirectSet ) $this->set( 'm_intIsCommissioned', trim( $arrValues['is_commissioned'] ) ); elseif( isset( $arrValues['is_commissioned'] ) ) $this->setIsCommissioned( $arrValues['is_commissioned'] );
		if( isset( $arrValues['is_bundle'] ) && $boolDirectSet ) $this->set( 'm_intIsBundle', trim( $arrValues['is_bundle'] ) ); elseif( isset( $arrValues['is_bundle'] ) ) $this->setIsBundle( $arrValues['is_bundle'] );
		if( isset( $arrValues['is_for_sale'] ) && $boolDirectSet ) $this->set( 'm_intIsForSale', trim( $arrValues['is_for_sale'] ) ); elseif( isset( $arrValues['is_for_sale'] ) ) $this->setIsForSale( $arrValues['is_for_sale'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_transactional'] ) && $boolDirectSet ) $this->set( 'm_intIsTransactional', trim( $arrValues['is_transactional'] ) ); elseif( isset( $arrValues['is_transactional'] ) ) $this->setIsTransactional( $arrValues['is_transactional'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['starting_unit_transactional_average'] ) && $boolDirectSet ) $this->set( 'm_fltStartingUnitTransactionalAverage', trim( $arrValues['starting_unit_transactional_average'] ) ); elseif( isset( $arrValues['starting_unit_transactional_average'] ) ) $this->setStartingUnitTransactionalAverage( $arrValues['starting_unit_transactional_average'] );
		if( isset( $arrValues['property_monthly_recurring'] ) && $boolDirectSet ) $this->set( 'm_fltPropertyMonthlyRecurring', trim( $arrValues['property_monthly_recurring'] ) ); elseif( isset( $arrValues['property_monthly_recurring'] ) ) $this->setPropertyMonthlyRecurring( $arrValues['property_monthly_recurring'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setParentPsProductBundleId( $intParentPsProductBundleId ) {
		$this->set( 'm_intParentPsProductBundleId', CStrings::strToIntDef( $intParentPsProductBundleId, NULL, false ) );
	}

	public function getParentPsProductBundleId() {
		return $this->m_intParentPsProductBundleId;
	}

	public function sqlParentPsProductBundleId() {
		return ( true == isset( $this->m_intParentPsProductBundleId ) ) ? ( string ) $this->m_intParentPsProductBundleId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setRecurringChargeCodeId( $intRecurringChargeCodeId ) {
		$this->set( 'm_intRecurringChargeCodeId', CStrings::strToIntDef( $intRecurringChargeCodeId, NULL, false ) );
	}

	public function getRecurringChargeCodeId() {
		return $this->m_intRecurringChargeCodeId;
	}

	public function sqlRecurringChargeCodeId() {
		return ( true == isset( $this->m_intRecurringChargeCodeId ) ) ? ( string ) $this->m_intRecurringChargeCodeId : 'NULL';
	}

	public function setImplementationChargeCodeId( $intImplementationChargeCodeId ) {
		$this->set( 'm_intImplementationChargeCodeId', CStrings::strToIntDef( $intImplementationChargeCodeId, NULL, false ) );
	}

	public function getImplementationChargeCodeId() {
		return $this->m_intImplementationChargeCodeId;
	}

	public function sqlImplementationChargeCodeId() {
		return ( true == isset( $this->m_intImplementationChargeCodeId ) ) ? ( string ) $this->m_intImplementationChargeCodeId : 'NULL';
	}

	public function setTrainingChargeCodeId( $intTrainingChargeCodeId ) {
		$this->set( 'm_intTrainingChargeCodeId', CStrings::strToIntDef( $intTrainingChargeCodeId, NULL, false ) );
	}

	public function getTrainingChargeCodeId() {
		return $this->m_intTrainingChargeCodeId;
	}

	public function sqlTrainingChargeCodeId() {
		return ( true == isset( $this->m_intTrainingChargeCodeId ) ) ? ( string ) $this->m_intTrainingChargeCodeId : 'NULL';
	}

	public function setPsProductTypeId( $intPsProductTypeId ) {
		$this->set( 'm_intPsProductTypeId', CStrings::strToIntDef( $intPsProductTypeId, NULL, false ) );
	}

	public function getPsProductTypeId() {
		return $this->m_intPsProductTypeId;
	}

	public function sqlPsProductTypeId() {
		return ( true == isset( $this->m_intPsProductTypeId ) ) ? ( string ) $this->m_intPsProductTypeId : 'NULL';
	}

	public function setPsProductDisplayTypeId( $intPsProductDisplayTypeId ) {
		$this->set( 'm_intPsProductDisplayTypeId', CStrings::strToIntDef( $intPsProductDisplayTypeId, NULL, false ) );
	}

	public function getPsProductDisplayTypeId() {
		return $this->m_intPsProductDisplayTypeId;
	}

	public function sqlPsProductDisplayTypeId() {
		return ( true == isset( $this->m_intPsProductDisplayTypeId ) ) ? ( string ) $this->m_intPsProductDisplayTypeId : '2';
	}

	public function setDefaultBillingFrequencyId( $intDefaultBillingFrequencyId ) {
		$this->set( 'm_intDefaultBillingFrequencyId', CStrings::strToIntDef( $intDefaultBillingFrequencyId, NULL, false ) );
	}

	public function getDefaultBillingFrequencyId() {
		return $this->m_intDefaultBillingFrequencyId;
	}

	public function sqlDefaultBillingFrequencyId() {
		return ( true == isset( $this->m_intDefaultBillingFrequencyId ) ) ? ( string ) $this->m_intDefaultBillingFrequencyId : 'NULL';
	}

	public function setArchitectEmployeeId( $intArchitectEmployeeId ) {
		$this->set( 'm_intArchitectEmployeeId', CStrings::strToIntDef( $intArchitectEmployeeId, NULL, false ) );
	}

	public function getArchitectEmployeeId() {
		return $this->m_intArchitectEmployeeId;
	}

	public function sqlArchitectEmployeeId() {
		return ( true == isset( $this->m_intArchitectEmployeeId ) ) ? ( string ) $this->m_intArchitectEmployeeId : 'NULL';
	}

	public function setSqmEmployeeId( $intSqmEmployeeId ) {
		$this->set( 'm_intSqmEmployeeId', CStrings::strToIntDef( $intSqmEmployeeId, NULL, false ) );
	}

	public function getSqmEmployeeId() {
		return $this->m_intSqmEmployeeId;
	}

	public function sqlSqmEmployeeId() {
		return ( true == isset( $this->m_intSqmEmployeeId ) ) ? ( string ) $this->m_intSqmEmployeeId : 'NULL';
	}

	public function setQamEmployeeId( $intQamEmployeeId ) {
		$this->set( 'm_intQamEmployeeId', CStrings::strToIntDef( $intQamEmployeeId, NULL, false ) );
	}

	public function getQamEmployeeId() {
		return $this->m_intQamEmployeeId;
	}

	public function sqlQamEmployeeId() {
		return ( true == isset( $this->m_intQamEmployeeId ) ) ? ( string ) $this->m_intQamEmployeeId : 'NULL';
	}

	public function setDoeEmployeeId( $intDoeEmployeeId ) {
		$this->set( 'm_intDoeEmployeeId', CStrings::strToIntDef( $intDoeEmployeeId, NULL, false ) );
	}

	public function getDoeEmployeeId() {
		return $this->m_intDoeEmployeeId;
	}

	public function sqlDoeEmployeeId() {
		return ( true == isset( $this->m_intDoeEmployeeId ) ) ? ( string ) $this->m_intDoeEmployeeId : 'NULL';
	}

	public function setExecutiveEmployeeId( $intExecutiveEmployeeId ) {
		$this->set( 'm_intExecutiveEmployeeId', CStrings::strToIntDef( $intExecutiveEmployeeId, NULL, false ) );
	}

	public function getExecutiveEmployeeId() {
		return $this->m_intExecutiveEmployeeId;
	}

	public function sqlExecutiveEmployeeId() {
		return ( true == isset( $this->m_intExecutiveEmployeeId ) ) ? ( string ) $this->m_intExecutiveEmployeeId : 'NULL';
	}

	public function setImplementationEmployeeId( $intImplementationEmployeeId ) {
		$this->set( 'm_intImplementationEmployeeId', CStrings::strToIntDef( $intImplementationEmployeeId, NULL, false ) );
	}

	public function getImplementationEmployeeId() {
		return $this->m_intImplementationEmployeeId;
	}

	public function sqlImplementationEmployeeId() {
		return ( true == isset( $this->m_intImplementationEmployeeId ) ) ? ( string ) $this->m_intImplementationEmployeeId : 'NULL';
	}

	public function setProductManagerEmployeeId( $intProductManagerEmployeeId ) {
		$this->set( 'm_intProductManagerEmployeeId', CStrings::strToIntDef( $intProductManagerEmployeeId, NULL, false ) );
	}

	public function getProductManagerEmployeeId() {
		return $this->m_intProductManagerEmployeeId;
	}

	public function sqlProductManagerEmployeeId() {
		return ( true == isset( $this->m_intProductManagerEmployeeId ) ) ? ( string ) $this->m_intProductManagerEmployeeId : 'NULL';
	}

	public function setProductOwnerEmployeeId( $intProductOwnerEmployeeId ) {
		$this->set( 'm_intProductOwnerEmployeeId', CStrings::strToIntDef( $intProductOwnerEmployeeId, NULL, false ) );
	}

	public function getProductOwnerEmployeeId() {
		return $this->m_intProductOwnerEmployeeId;
	}

	public function sqlProductOwnerEmployeeId() {
		return ( true == isset( $this->m_intProductOwnerEmployeeId ) ) ? ( string ) $this->m_intProductOwnerEmployeeId : 'NULL';
	}

	public function setSalesEmployeeId( $intSalesEmployeeId ) {
		$this->set( 'm_intSalesEmployeeId', CStrings::strToIntDef( $intSalesEmployeeId, NULL, false ) );
	}

	public function getSalesEmployeeId() {
		return $this->m_intSalesEmployeeId;
	}

	public function sqlSalesEmployeeId() {
		return ( true == isset( $this->m_intSalesEmployeeId ) ) ? ( string ) $this->m_intSalesEmployeeId : 'NULL';
	}

	public function setSdmEmployeeId( $intSdmEmployeeId ) {
		$this->set( 'm_intSdmEmployeeId', CStrings::strToIntDef( $intSdmEmployeeId, NULL, false ) );
	}

	public function getSdmEmployeeId() {
		return $this->m_intSdmEmployeeId;
	}

	public function sqlSdmEmployeeId() {
		return ( true == isset( $this->m_intSdmEmployeeId ) ) ? ( string ) $this->m_intSdmEmployeeId : 'NULL';
	}

	public function setSupportEmployeeId( $intSupportEmployeeId ) {
		$this->set( 'm_intSupportEmployeeId', CStrings::strToIntDef( $intSupportEmployeeId, NULL, false ) );
	}

	public function getSupportEmployeeId() {
		return $this->m_intSupportEmployeeId;
	}

	public function sqlSupportEmployeeId() {
		return ( true == isset( $this->m_intSupportEmployeeId ) ) ? ( string ) $this->m_intSupportEmployeeId : 'NULL';
	}

	public function setQaEmployeeId( $intQaEmployeeId ) {
		$this->set( 'm_intQaEmployeeId', CStrings::strToIntDef( $intQaEmployeeId, NULL, false ) );
	}

	public function getQaEmployeeId() {
		return $this->m_intQaEmployeeId;
	}

	public function sqlQaEmployeeId() {
		return ( true == isset( $this->m_intQaEmployeeId ) ) ? ( string ) $this->m_intQaEmployeeId : 'NULL';
	}

	public function setTechWriterEmployeeId( $intTechWriterEmployeeId ) {
		$this->set( 'm_intTechWriterEmployeeId', CStrings::strToIntDef( $intTechWriterEmployeeId, NULL, false ) );
	}

	public function getTechWriterEmployeeId() {
		return $this->m_intTechWriterEmployeeId;
	}

	public function sqlTechWriterEmployeeId() {
		return ( true == isset( $this->m_intTechWriterEmployeeId ) ) ? ( string ) $this->m_intTechWriterEmployeeId : 'NULL';
	}

	public function setTlEmployeeId( $intTlEmployeeId ) {
		$this->set( 'm_intTlEmployeeId', CStrings::strToIntDef( $intTlEmployeeId, NULL, false ) );
	}

	public function getTlEmployeeId() {
		return $this->m_intTlEmployeeId;
	}

	public function sqlTlEmployeeId() {
		return ( true == isset( $this->m_intTlEmployeeId ) ) ? ( string ) $this->m_intTlEmployeeId : 'NULL';
	}

	public function setTpmEmployeeId( $intTpmEmployeeId ) {
		$this->set( 'm_intTpmEmployeeId', CStrings::strToIntDef( $intTpmEmployeeId, NULL, false ) );
	}

	public function getTpmEmployeeId() {
		return $this->m_intTpmEmployeeId;
	}

	public function sqlTpmEmployeeId() {
		return ( true == isset( $this->m_intTpmEmployeeId ) ) ? ( string ) $this->m_intTpmEmployeeId : 'NULL';
	}

	public function setUxEmployeeId( $intUxEmployeeId ) {
		$this->set( 'm_intUxEmployeeId', CStrings::strToIntDef( $intUxEmployeeId, NULL, false ) );
	}

	public function getUxEmployeeId() {
		return $this->m_intUxEmployeeId;
	}

	public function sqlUxEmployeeId() {
		return ( true == isset( $this->m_intUxEmployeeId ) ) ? ( string ) $this->m_intUxEmployeeId : 'NULL';
	}

	public function setBusinessAnalystEmployeeId( $intBusinessAnalystEmployeeId ) {
		$this->set( 'm_intBusinessAnalystEmployeeId', CStrings::strToIntDef( $intBusinessAnalystEmployeeId, NULL, false ) );
	}

	public function getBusinessAnalystEmployeeId() {
		return $this->m_intBusinessAnalystEmployeeId;
	}

	public function sqlBusinessAnalystEmployeeId() {
		return ( true == isset( $this->m_intBusinessAnalystEmployeeId ) ) ? ( string ) $this->m_intBusinessAnalystEmployeeId : 'NULL';
	}

	public function setSmeEmployeeId( $intSmeEmployeeId ) {
		$this->set( 'm_intSmeEmployeeId', CStrings::strToIntDef( $intSmeEmployeeId, NULL, false ) );
	}

	public function getSmeEmployeeId() {
		return $this->m_intSmeEmployeeId;
	}

	public function sqlSmeEmployeeId() {
		return ( true == isset( $this->m_intSmeEmployeeId ) ) ? ( string ) $this->m_intSmeEmployeeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setShortName( $strShortName ) {
		$this->set( 'm_strShortName', CStrings::strTrimDef( $strShortName, 240, NULL, true ) );
	}

	public function getShortName() {
		return $this->m_strShortName;
	}

	public function sqlShortName() {
		return ( true == isset( $this->m_strShortName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strShortName ) : '\'' . addslashes( $this->m_strShortName ) . '\'' ) : 'NULL';
	}

	public function setUnitMonthlyRecurring( $fltUnitMonthlyRecurring ) {
		$this->set( 'm_fltUnitMonthlyRecurring', CStrings::strToFloatDef( $fltUnitMonthlyRecurring, NULL, false, 2 ) );
	}

	public function getUnitMonthlyRecurring() {
		return $this->m_fltUnitMonthlyRecurring;
	}

	public function sqlUnitMonthlyRecurring() {
		return ( true == isset( $this->m_fltUnitMonthlyRecurring ) ) ? ( string ) $this->m_fltUnitMonthlyRecurring : '0';
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Aug 19 2020.
	 */
	public function setSmallUnitMonthRecurring( $fltSmallUnitMonthRecurring ) {
		$this->set( 'm_fltSmallUnitMonthRecurring', CStrings::strToFloatDef( $fltSmallUnitMonthRecurring, NULL, false, 2 ) );
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Aug 19 2020.
	 */
	public function getSmallUnitMonthRecurring() {
		return $this->m_fltSmallUnitMonthRecurring;
	}

	/**
	 * @deprecated:Database column associated with this function is going to deprecate
	 * on Aug 19 2020.
	 */
	public function sqlSmallUnitMonthRecurring() {
		return ( true == isset( $this->m_fltSmallUnitMonthRecurring ) ) ? ( string ) $this->m_fltSmallUnitMonthRecurring : '0';
	}

	public function setUnitTransactionalAverage( $fltUnitTransactionalAverage ) {
		$this->set( 'm_fltUnitTransactionalAverage', CStrings::strToFloatDef( $fltUnitTransactionalAverage, NULL, false, 2 ) );
	}

	public function getUnitTransactionalAverage() {
		return $this->m_fltUnitTransactionalAverage;
	}

	public function sqlUnitTransactionalAverage() {
		return ( true == isset( $this->m_fltUnitTransactionalAverage ) ) ? ( string ) $this->m_fltUnitTransactionalAverage : '0';
	}

	public function setPropertySetup( $fltPropertySetup ) {
		$this->set( 'm_fltPropertySetup', CStrings::strToFloatDef( $fltPropertySetup, NULL, false, 2 ) );
	}

	public function getPropertySetup() {
		return $this->m_fltPropertySetup;
	}

	public function sqlPropertySetup() {
		return ( true == isset( $this->m_fltPropertySetup ) ) ? ( string ) $this->m_fltPropertySetup : '0';
	}

	public function setPropertyTraining( $fltPropertyTraining ) {
		$this->set( 'm_fltPropertyTraining', CStrings::strToFloatDef( $fltPropertyTraining, NULL, false, 2 ) );
	}

	public function getPropertyTraining() {
		return $this->m_fltPropertyTraining;
	}

	public function sqlPropertyTraining() {
		return ( true == isset( $this->m_fltPropertyTraining ) ) ? ( string ) $this->m_fltPropertyTraining : '0';
	}

	public function setCommissionTriggerPercent( $fltCommissionTriggerPercent ) {
		$this->set( 'm_fltCommissionTriggerPercent', CStrings::strToFloatDef( $fltCommissionTriggerPercent, NULL, false, 2 ) );
	}

	public function getCommissionTriggerPercent() {
		return $this->m_fltCommissionTriggerPercent;
	}

	public function sqlCommissionTriggerPercent() {
		return ( true == isset( $this->m_fltCommissionTriggerPercent ) ) ? ( string ) $this->m_fltCommissionTriggerPercent : '0';
	}

	public function setHexColor( $strHexColor ) {
		$this->set( 'm_strHexColor', CStrings::strTrimDef( $strHexColor, 6, NULL, true ) );
	}

	public function getHexColor() {
		return $this->m_strHexColor;
	}

	public function sqlHexColor() {
		return ( true == isset( $this->m_strHexColor ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHexColor ) : '\'' . addslashes( $this->m_strHexColor ) . '\'' ) : '\'NULL\'';
	}

	public function setMinUnits( $intMinUnits ) {
		$this->set( 'm_intMinUnits', CStrings::strToIntDef( $intMinUnits, NULL, false ) );
	}

	public function getMinUnits() {
		return $this->m_intMinUnits;
	}

	public function sqlMinUnits() {
		return ( true == isset( $this->m_intMinUnits ) ) ? ( string ) $this->m_intMinUnits : 'NULL';
	}

	public function setMaxUnits( $intMaxUnits ) {
		$this->set( 'm_intMaxUnits', CStrings::strToIntDef( $intMaxUnits, NULL, false ) );
	}

	public function getMaxUnits() {
		return $this->m_intMaxUnits;
	}

	public function sqlMaxUnits() {
		return ( true == isset( $this->m_intMaxUnits ) ) ? ( string ) $this->m_intMaxUnits : 'NULL';
	}

	public function setAllowSimpleContracts( $intAllowSimpleContracts ) {
		$this->set( 'm_intAllowSimpleContracts', CStrings::strToIntDef( $intAllowSimpleContracts, NULL, false ) );
	}

	public function getAllowSimpleContracts() {
		return $this->m_intAllowSimpleContracts;
	}

	public function sqlAllowSimpleContracts() {
		return ( true == isset( $this->m_intAllowSimpleContracts ) ) ? ( string ) $this->m_intAllowSimpleContracts : '0';
	}

	public function setAllowInBundle( $intAllowInBundle ) {
		$this->set( 'm_intAllowInBundle', CStrings::strToIntDef( $intAllowInBundle, NULL, false ) );
	}

	public function getAllowInBundle() {
		return $this->m_intAllowInBundle;
	}

	public function sqlAllowInBundle() {
		return ( true == isset( $this->m_intAllowInBundle ) ) ? ( string ) $this->m_intAllowInBundle : '1';
	}

	public function setHasBookedCommissions( $intHasBookedCommissions ) {
		$this->set( 'm_intHasBookedCommissions', CStrings::strToIntDef( $intHasBookedCommissions, NULL, false ) );
	}

	public function getHasBookedCommissions() {
		return $this->m_intHasBookedCommissions;
	}

	public function sqlHasBookedCommissions() {
		return ( true == isset( $this->m_intHasBookedCommissions ) ) ? ( string ) $this->m_intHasBookedCommissions : '0';
	}

	public function setShowInApplicationFilter( $intShowInApplicationFilter ) {
		$this->set( 'm_intShowInApplicationFilter', CStrings::strToIntDef( $intShowInApplicationFilter, NULL, false ) );
	}

	public function getShowInApplicationFilter() {
		return $this->m_intShowInApplicationFilter;
	}

	public function sqlShowInApplicationFilter() {
		return ( true == isset( $this->m_intShowInApplicationFilter ) ) ? ( string ) $this->m_intShowInApplicationFilter : '0';
	}

	public function setTrackCompetitors( $intTrackCompetitors ) {
		$this->set( 'm_intTrackCompetitors', CStrings::strToIntDef( $intTrackCompetitors, NULL, false ) );
	}

	public function getTrackCompetitors() {
		return $this->m_intTrackCompetitors;
	}

	public function sqlTrackCompetitors() {
		return ( true == isset( $this->m_intTrackCompetitors ) ) ? ( string ) $this->m_intTrackCompetitors : '0';
	}

	public function setIsCommissioned( $intIsCommissioned ) {
		$this->set( 'm_intIsCommissioned', CStrings::strToIntDef( $intIsCommissioned, NULL, false ) );
	}

	public function getIsCommissioned() {
		return $this->m_intIsCommissioned;
	}

	public function sqlIsCommissioned() {
		return ( true == isset( $this->m_intIsCommissioned ) ) ? ( string ) $this->m_intIsCommissioned : '0';
	}

	public function setIsBundle( $intIsBundle ) {
		$this->set( 'm_intIsBundle', CStrings::strToIntDef( $intIsBundle, NULL, false ) );
	}

	public function getIsBundle() {
		return $this->m_intIsBundle;
	}

	public function sqlIsBundle() {
		return ( true == isset( $this->m_intIsBundle ) ) ? ( string ) $this->m_intIsBundle : '0';
	}

	public function setIsForSale( $intIsForSale ) {
		$this->set( 'm_intIsForSale', CStrings::strToIntDef( $intIsForSale, NULL, false ) );
	}

	public function getIsForSale() {
		return $this->m_intIsForSale;
	}

	public function sqlIsForSale() {
		return ( true == isset( $this->m_intIsForSale ) ) ? ( string ) $this->m_intIsForSale : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsTransactional( $intIsTransactional ) {
		$this->set( 'm_intIsTransactional', CStrings::strToIntDef( $intIsTransactional, NULL, false ) );
	}

	public function getIsTransactional() {
		return $this->m_intIsTransactional;
	}

	public function sqlIsTransactional() {
		return ( true == isset( $this->m_intIsTransactional ) ) ? ( string ) $this->m_intIsTransactional : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setStartingUnitTransactionalAverage( $fltStartingUnitTransactionalAverage ) {
		$this->set( 'm_fltStartingUnitTransactionalAverage', CStrings::strToFloatDef( $fltStartingUnitTransactionalAverage, NULL, false, 0 ) );
	}

	public function getStartingUnitTransactionalAverage() {
		return $this->m_fltStartingUnitTransactionalAverage;
	}

	public function sqlStartingUnitTransactionalAverage() {
		return ( true == isset( $this->m_fltStartingUnitTransactionalAverage ) ) ? ( string ) $this->m_fltStartingUnitTransactionalAverage : 'NULL';
	}

	public function setPropertyMonthlyRecurring( $fltPropertyMonthlyRecurring ) {
		$this->set( 'm_fltPropertyMonthlyRecurring', CStrings::strToFloatDef( $fltPropertyMonthlyRecurring, NULL, false, 2 ) );
	}

	public function getPropertyMonthlyRecurring() {
		return $this->m_fltPropertyMonthlyRecurring;
	}

	public function sqlPropertyMonthlyRecurring() {
		return ( true == isset( $this->m_fltPropertyMonthlyRecurring ) ) ? ( string ) $this->m_fltPropertyMonthlyRecurring : '0';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_product_id, parent_ps_product_bundle_id, ps_lead_id, recurring_charge_code_id, implementation_charge_code_id, training_charge_code_id, ps_product_type_id, ps_product_display_type_id, default_billing_frequency_id, architect_employee_id, sqm_employee_id, qam_employee_id, doe_employee_id, executive_employee_id, implementation_employee_id, product_manager_employee_id, product_owner_employee_id, sales_employee_id, sdm_employee_id, support_employee_id, qa_employee_id, tech_writer_employee_id, tl_employee_id, tpm_employee_id, ux_employee_id, business_analyst_employee_id, sme_employee_id, name, description, short_name, unit_monthly_recurring, small_unit_month_recurring, unit_transactional_average, property_setup, property_training, commission_trigger_percent, hex_color, min_units, max_units, allow_simple_contracts, allow_in_bundle, has_booked_commissions, show_in_application_filter, track_competitors, is_commissioned, is_bundle, is_for_sale, is_published, is_transactional, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, starting_unit_transactional_average, property_monthly_recurring )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlPsProductId() . ', ' .
		          $this->sqlParentPsProductBundleId() . ', ' .
		          $this->sqlPsLeadId() . ', ' .
		          $this->sqlRecurringChargeCodeId() . ', ' .
		          $this->sqlImplementationChargeCodeId() . ', ' .
		          $this->sqlTrainingChargeCodeId() . ', ' .
		          $this->sqlPsProductTypeId() . ', ' .
		          $this->sqlPsProductDisplayTypeId() . ', ' .
		          $this->sqlDefaultBillingFrequencyId() . ', ' .
		          $this->sqlArchitectEmployeeId() . ', ' .
		          $this->sqlSqmEmployeeId() . ', ' .
		          $this->sqlQamEmployeeId() . ', ' .
		          $this->sqlDoeEmployeeId() . ', ' .
		          $this->sqlExecutiveEmployeeId() . ', ' .
		          $this->sqlImplementationEmployeeId() . ', ' .
		          $this->sqlProductManagerEmployeeId() . ', ' .
		          $this->sqlProductOwnerEmployeeId() . ', ' .
		          $this->sqlSalesEmployeeId() . ', ' .
		          $this->sqlSdmEmployeeId() . ', ' .
		          $this->sqlSupportEmployeeId() . ', ' .
		          $this->sqlQaEmployeeId() . ', ' .
		          $this->sqlTechWriterEmployeeId() . ', ' .
		          $this->sqlTlEmployeeId() . ', ' .
		          $this->sqlTpmEmployeeId() . ', ' .
		          $this->sqlUxEmployeeId() . ', ' .
		          $this->sqlBusinessAnalystEmployeeId() . ', ' .
		          $this->sqlSmeEmployeeId() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlDescription() . ', ' .
		          $this->sqlShortName() . ', ' .
		          $this->sqlUnitMonthlyRecurring() . ', ' .
		          $this->sqlSmallUnitMonthRecurring() . ', ' .
		          $this->sqlUnitTransactionalAverage() . ', ' .
		          $this->sqlPropertySetup() . ', ' .
		          $this->sqlPropertyTraining() . ', ' .
		          $this->sqlCommissionTriggerPercent() . ', ' .
		          $this->sqlHexColor() . ', ' .
		          $this->sqlMinUnits() . ', ' .
		          $this->sqlMaxUnits() . ', ' .
		          $this->sqlAllowSimpleContracts() . ', ' .
		          $this->sqlAllowInBundle() . ', ' .
		          $this->sqlHasBookedCommissions() . ', ' .
		          $this->sqlShowInApplicationFilter() . ', ' .
		          $this->sqlTrackCompetitors() . ', ' .
		          $this->sqlIsCommissioned() . ', ' .
		          $this->sqlIsBundle() . ', ' .
		          $this->sqlIsForSale() . ', ' .
		          $this->sqlIsPublished() . ', ' .
		          $this->sqlIsTransactional() . ', ' .
		          $this->sqlOrderNum() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlStartingUnitTransactionalAverage() . ', ' .
		          $this->sqlPropertyMonthlyRecurring() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_ps_product_bundle_id = ' . $this->sqlParentPsProductBundleId(). ',' ; } elseif( true == array_key_exists( 'ParentPsProductBundleId', $this->getChangedColumns() ) ) { $strSql .= ' parent_ps_product_bundle_id = ' . $this->sqlParentPsProductBundleId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recurring_charge_code_id = ' . $this->sqlRecurringChargeCodeId(). ',' ; } elseif( true == array_key_exists( 'RecurringChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' recurring_charge_code_id = ' . $this->sqlRecurringChargeCodeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_charge_code_id = ' . $this->sqlImplementationChargeCodeId(). ',' ; } elseif( true == array_key_exists( 'ImplementationChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_charge_code_id = ' . $this->sqlImplementationChargeCodeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_charge_code_id = ' . $this->sqlTrainingChargeCodeId(). ',' ; } elseif( true == array_key_exists( 'TrainingChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' training_charge_code_id = ' . $this->sqlTrainingChargeCodeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_type_id = ' . $this->sqlPsProductTypeId(). ',' ; } elseif( true == array_key_exists( 'PsProductTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_type_id = ' . $this->sqlPsProductTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_display_type_id = ' . $this->sqlPsProductDisplayTypeId(). ',' ; } elseif( true == array_key_exists( 'PsProductDisplayTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_display_type_id = ' . $this->sqlPsProductDisplayTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_billing_frequency_id = ' . $this->sqlDefaultBillingFrequencyId(). ',' ; } elseif( true == array_key_exists( 'DefaultBillingFrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' default_billing_frequency_id = ' . $this->sqlDefaultBillingFrequencyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' architect_employee_id = ' . $this->sqlArchitectEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ArchitectEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' architect_employee_id = ' . $this->sqlArchitectEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sqm_employee_id = ' . $this->sqlSqmEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SqmEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sqm_employee_id = ' . $this->sqlSqmEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qam_employee_id = ' . $this->sqlQamEmployeeId(). ',' ; } elseif( true == array_key_exists( 'QamEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qam_employee_id = ' . $this->sqlQamEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' doe_employee_id = ' . $this->sqlDoeEmployeeId(). ',' ; } elseif( true == array_key_exists( 'DoeEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' doe_employee_id = ' . $this->sqlDoeEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executive_employee_id = ' . $this->sqlExecutiveEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ExecutiveEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' executive_employee_id = ' . $this->sqlExecutiveEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_employee_id = ' . $this->sqlImplementationEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ImplementationEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_employee_id = ' . $this->sqlImplementationEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_manager_employee_id = ' . $this->sqlProductManagerEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ProductManagerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' product_manager_employee_id = ' . $this->sqlProductManagerEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_owner_employee_id = ' . $this->sqlProductOwnerEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ProductOwnerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' product_owner_employee_id = ' . $this->sqlProductOwnerEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_employee_id = ' . $this->sqlSalesEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SalesEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sales_employee_id = ' . $this->sqlSalesEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sdm_employee_id = ' . $this->sqlSdmEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SdmEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sdm_employee_id = ' . $this->sqlSdmEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' support_employee_id = ' . $this->sqlSupportEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SupportEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' support_employee_id = ' . $this->sqlSupportEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId(). ',' ; } elseif( true == array_key_exists( 'QaEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tech_writer_employee_id = ' . $this->sqlTechWriterEmployeeId(). ',' ; } elseif( true == array_key_exists( 'TechWriterEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' tech_writer_employee_id = ' . $this->sqlTechWriterEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tl_employee_id = ' . $this->sqlTlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'TlEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' tl_employee_id = ' . $this->sqlTlEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tpm_employee_id = ' . $this->sqlTpmEmployeeId(). ',' ; } elseif( true == array_key_exists( 'TpmEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' tpm_employee_id = ' . $this->sqlTpmEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ux_employee_id = ' . $this->sqlUxEmployeeId(). ',' ; } elseif( true == array_key_exists( 'UxEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' ux_employee_id = ' . $this->sqlUxEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' business_analyst_employee_id = ' . $this->sqlBusinessAnalystEmployeeId(). ',' ; } elseif( true == array_key_exists( 'BusinessAnalystEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' business_analyst_employee_id = ' . $this->sqlBusinessAnalystEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sme_employee_id = ' . $this->sqlSmeEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SmeEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sme_employee_id = ' . $this->sqlSmeEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' short_name = ' . $this->sqlShortName(). ',' ; } elseif( true == array_key_exists( 'ShortName', $this->getChangedColumns() ) ) { $strSql .= ' short_name = ' . $this->sqlShortName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_monthly_recurring = ' . $this->sqlUnitMonthlyRecurring(). ',' ; } elseif( true == array_key_exists( 'UnitMonthlyRecurring', $this->getChangedColumns() ) ) { $strSql .= ' unit_monthly_recurring = ' . $this->sqlUnitMonthlyRecurring() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' small_unit_month_recurring = ' . $this->sqlSmallUnitMonthRecurring(). ',' ; } elseif( true == array_key_exists( 'SmallUnitMonthRecurring', $this->getChangedColumns() ) ) { $strSql .= ' small_unit_month_recurring = ' . $this->sqlSmallUnitMonthRecurring() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_transactional_average = ' . $this->sqlUnitTransactionalAverage(). ',' ; } elseif( true == array_key_exists( 'UnitTransactionalAverage', $this->getChangedColumns() ) ) { $strSql .= ' unit_transactional_average = ' . $this->sqlUnitTransactionalAverage() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_setup = ' . $this->sqlPropertySetup(). ',' ; } elseif( true == array_key_exists( 'PropertySetup', $this->getChangedColumns() ) ) { $strSql .= ' property_setup = ' . $this->sqlPropertySetup() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_training = ' . $this->sqlPropertyTraining(). ',' ; } elseif( true == array_key_exists( 'PropertyTraining', $this->getChangedColumns() ) ) { $strSql .= ' property_training = ' . $this->sqlPropertyTraining() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_trigger_percent = ' . $this->sqlCommissionTriggerPercent(). ',' ; } elseif( true == array_key_exists( 'CommissionTriggerPercent', $this->getChangedColumns() ) ) { $strSql .= ' commission_trigger_percent = ' . $this->sqlCommissionTriggerPercent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hex_color = ' . $this->sqlHexColor(). ',' ; } elseif( true == array_key_exists( 'HexColor', $this->getChangedColumns() ) ) { $strSql .= ' hex_color = ' . $this->sqlHexColor() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_units = ' . $this->sqlMinUnits(). ',' ; } elseif( true == array_key_exists( 'MinUnits', $this->getChangedColumns() ) ) { $strSql .= ' min_units = ' . $this->sqlMinUnits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_units = ' . $this->sqlMaxUnits(). ',' ; } elseif( true == array_key_exists( 'MaxUnits', $this->getChangedColumns() ) ) { $strSql .= ' max_units = ' . $this->sqlMaxUnits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_simple_contracts = ' . $this->sqlAllowSimpleContracts(). ',' ; } elseif( true == array_key_exists( 'AllowSimpleContracts', $this->getChangedColumns() ) ) { $strSql .= ' allow_simple_contracts = ' . $this->sqlAllowSimpleContracts() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allow_in_bundle = ' . $this->sqlAllowInBundle(). ',' ; } elseif( true == array_key_exists( 'AllowInBundle', $this->getChangedColumns() ) ) { $strSql .= ' allow_in_bundle = ' . $this->sqlAllowInBundle() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_booked_commissions = ' . $this->sqlHasBookedCommissions(). ',' ; } elseif( true == array_key_exists( 'HasBookedCommissions', $this->getChangedColumns() ) ) { $strSql .= ' has_booked_commissions = ' . $this->sqlHasBookedCommissions() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_application_filter = ' . $this->sqlShowInApplicationFilter(). ',' ; } elseif( true == array_key_exists( 'ShowInApplicationFilter', $this->getChangedColumns() ) ) { $strSql .= ' show_in_application_filter = ' . $this->sqlShowInApplicationFilter() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track_competitors = ' . $this->sqlTrackCompetitors(). ',' ; } elseif( true == array_key_exists( 'TrackCompetitors', $this->getChangedColumns() ) ) { $strSql .= ' track_competitors = ' . $this->sqlTrackCompetitors() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_commissioned = ' . $this->sqlIsCommissioned(). ',' ; } elseif( true == array_key_exists( 'IsCommissioned', $this->getChangedColumns() ) ) { $strSql .= ' is_commissioned = ' . $this->sqlIsCommissioned() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_bundle = ' . $this->sqlIsBundle(). ',' ; } elseif( true == array_key_exists( 'IsBundle', $this->getChangedColumns() ) ) { $strSql .= ' is_bundle = ' . $this->sqlIsBundle() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_for_sale = ' . $this->sqlIsForSale(). ',' ; } elseif( true == array_key_exists( 'IsForSale', $this->getChangedColumns() ) ) { $strSql .= ' is_for_sale = ' . $this->sqlIsForSale() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_transactional = ' . $this->sqlIsTransactional(). ',' ; } elseif( true == array_key_exists( 'IsTransactional', $this->getChangedColumns() ) ) { $strSql .= ' is_transactional = ' . $this->sqlIsTransactional() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' starting_unit_transactional_average = ' . $this->sqlStartingUnitTransactionalAverage(). ',' ; } elseif( true == array_key_exists( 'StartingUnitTransactionalAverage', $this->getChangedColumns() ) ) { $strSql .= ' starting_unit_transactional_average = ' . $this->sqlStartingUnitTransactionalAverage() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_monthly_recurring = ' . $this->sqlPropertyMonthlyRecurring(). ',' ; } elseif( true == array_key_exists( 'PropertyMonthlyRecurring', $this->getChangedColumns() ) ) { $strSql .= ' property_monthly_recurring = ' . $this->sqlPropertyMonthlyRecurring() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_product_id' => $this->getPsProductId(),
			'parent_ps_product_bundle_id' => $this->getParentPsProductBundleId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'recurring_charge_code_id' => $this->getRecurringChargeCodeId(),
			'implementation_charge_code_id' => $this->getImplementationChargeCodeId(),
			'training_charge_code_id' => $this->getTrainingChargeCodeId(),
			'ps_product_type_id' => $this->getPsProductTypeId(),
			'ps_product_display_type_id' => $this->getPsProductDisplayTypeId(),
			'default_billing_frequency_id' => $this->getDefaultBillingFrequencyId(),
			'architect_employee_id' => $this->getArchitectEmployeeId(),
			'sqm_employee_id' => $this->getSqmEmployeeId(),
			'qam_employee_id' => $this->getQamEmployeeId(),
			'doe_employee_id' => $this->getDoeEmployeeId(),
			'executive_employee_id' => $this->getExecutiveEmployeeId(),
			'implementation_employee_id' => $this->getImplementationEmployeeId(),
			'product_manager_employee_id' => $this->getProductManagerEmployeeId(),
			'product_owner_employee_id' => $this->getProductOwnerEmployeeId(),
			'sales_employee_id' => $this->getSalesEmployeeId(),
			'sdm_employee_id' => $this->getSdmEmployeeId(),
			'support_employee_id' => $this->getSupportEmployeeId(),
			'qa_employee_id' => $this->getQaEmployeeId(),
			'tech_writer_employee_id' => $this->getTechWriterEmployeeId(),
			'tl_employee_id' => $this->getTlEmployeeId(),
			'tpm_employee_id' => $this->getTpmEmployeeId(),
			'ux_employee_id' => $this->getUxEmployeeId(),
			'business_analyst_employee_id' => $this->getBusinessAnalystEmployeeId(),
			'sme_employee_id' => $this->getSmeEmployeeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'short_name' => $this->getShortName(),
			'unit_monthly_recurring' => $this->getUnitMonthlyRecurring(),
			'small_unit_month_recurring' => $this->getSmallUnitMonthRecurring(),
			'unit_transactional_average' => $this->getUnitTransactionalAverage(),
			'property_setup' => $this->getPropertySetup(),
			'property_training' => $this->getPropertyTraining(),
			'commission_trigger_percent' => $this->getCommissionTriggerPercent(),
			'hex_color' => $this->getHexColor(),
			'min_units' => $this->getMinUnits(),
			'max_units' => $this->getMaxUnits(),
			'allow_simple_contracts' => $this->getAllowSimpleContracts(),
			'allow_in_bundle' => $this->getAllowInBundle(),
			'has_booked_commissions' => $this->getHasBookedCommissions(),
			'show_in_application_filter' => $this->getShowInApplicationFilter(),
			'track_competitors' => $this->getTrackCompetitors(),
			'is_commissioned' => $this->getIsCommissioned(),
			'is_bundle' => $this->getIsBundle(),
			'is_for_sale' => $this->getIsForSale(),
			'is_published' => $this->getIsPublished(),
			'is_transactional' => $this->getIsTransactional(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'starting_unit_transactional_average' => $this->getStartingUnitTransactionalAverage(),
			'property_monthly_recurring' => $this->getPropertyMonthlyRecurring()
		);
	}

}
?>