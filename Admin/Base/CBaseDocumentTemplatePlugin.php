<?php

class CBaseDocumentTemplatePlugin extends CEosSingularBase {

	const TABLE_NAME = 'public.document_template_plugins';

	protected $m_intId;
	protected $m_intDocumentTemplateLibraryId;
	protected $m_strName;
	protected $m_strHandle;
	protected $m_strDescription;
	protected $m_intIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['document_template_library_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentTemplateLibraryId', trim( $arrValues['document_template_library_id'] ) ); elseif( isset( $arrValues['document_template_library_id'] ) ) $this->setDocumentTemplateLibraryId( $arrValues['document_template_library_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['handle'] ) && $boolDirectSet ) $this->set( 'm_strHandle', trim( stripcslashes( $arrValues['handle'] ) ) ); elseif( isset( $arrValues['handle'] ) ) $this->setHandle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['handle'] ) : $arrValues['handle'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDocumentTemplateLibraryId( $intDocumentTemplateLibraryId ) {
		$this->set( 'm_intDocumentTemplateLibraryId', CStrings::strToIntDef( $intDocumentTemplateLibraryId, NULL, false ) );
	}

	public function getDocumentTemplateLibraryId() {
		return $this->m_intDocumentTemplateLibraryId;
	}

	public function sqlDocumentTemplateLibraryId() {
		return ( true == isset( $this->m_intDocumentTemplateLibraryId ) ) ? ( string ) $this->m_intDocumentTemplateLibraryId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setHandle( $strHandle ) {
		$this->set( 'm_strHandle', CStrings::strTrimDef( $strHandle, 50, NULL, true ) );
	}

	public function getHandle() {
		return $this->m_strHandle;
	}

	public function sqlHandle() {
		return ( true == isset( $this->m_strHandle ) ) ? '\'' . addslashes( $this->m_strHandle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'document_template_library_id' => $this->getDocumentTemplateLibraryId(),
			'name' => $this->getName(),
			'handle' => $this->getHandle(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>