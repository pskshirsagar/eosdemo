<?php

class CBaseEmployeeCertificationTest extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_certification_tests';

	protected $m_intId;
	protected $m_intEmployeeCertificationId;
	protected $m_intCertificationTestId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_certification_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeCertificationId', trim( $arrValues['employee_certification_id'] ) ); elseif( isset( $arrValues['employee_certification_id'] ) ) $this->setEmployeeCertificationId( $arrValues['employee_certification_id'] );
		if( isset( $arrValues['certification_test_id'] ) && $boolDirectSet ) $this->set( 'm_intCertificationTestId', trim( $arrValues['certification_test_id'] ) ); elseif( isset( $arrValues['certification_test_id'] ) ) $this->setCertificationTestId( $arrValues['certification_test_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeCertificationId( $intEmployeeCertificationId ) {
		$this->set( 'm_intEmployeeCertificationId', CStrings::strToIntDef( $intEmployeeCertificationId, NULL, false ) );
	}

	public function getEmployeeCertificationId() {
		return $this->m_intEmployeeCertificationId;
	}

	public function sqlEmployeeCertificationId() {
		return ( true == isset( $this->m_intEmployeeCertificationId ) ) ? ( string ) $this->m_intEmployeeCertificationId : 'NULL';
	}

	public function setCertificationTestId( $intCertificationTestId ) {
		$this->set( 'm_intCertificationTestId', CStrings::strToIntDef( $intCertificationTestId, NULL, false ) );
	}

	public function getCertificationTestId() {
		return $this->m_intCertificationTestId;
	}

	public function sqlCertificationTestId() {
		return ( true == isset( $this->m_intCertificationTestId ) ) ? ( string ) $this->m_intCertificationTestId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_certification_id, certification_test_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeCertificationId() . ', ' .
 						$this->sqlCertificationTestId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_certification_id = ' . $this->sqlEmployeeCertificationId() . ','; } elseif( true == array_key_exists( 'EmployeeCertificationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_certification_id = ' . $this->sqlEmployeeCertificationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' certification_test_id = ' . $this->sqlCertificationTestId() . ','; } elseif( true == array_key_exists( 'CertificationTestId', $this->getChangedColumns() ) ) { $strSql .= ' certification_test_id = ' . $this->sqlCertificationTestId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_certification_id' => $this->getEmployeeCertificationId(),
			'certification_test_id' => $this->getCertificationTestId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>