<?php

class CBaseLeadPackageProduct extends CEosSingularBase {

	const TABLE_NAME = 'public.lead_package_products';

	protected $m_intId;
	protected $m_intLeadPackageId;
	protected $m_intPsProductId;
	protected $m_intBundlePsProductId;
	protected $m_strDetails;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_fltListRecurringPrice;
	protected $m_fltListSetupPrice;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['lead_package_id'] ) && $boolDirectSet ) $this->set( 'm_intLeadPackageId', trim( $arrValues['lead_package_id'] ) ); elseif( isset( $arrValues['lead_package_id'] ) ) $this->setLeadPackageId( $arrValues['lead_package_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['bundle_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intBundlePsProductId', trim( $arrValues['bundle_ps_product_id'] ) ); elseif( isset( $arrValues['bundle_ps_product_id'] ) ) $this->setBundlePsProductId( $arrValues['bundle_ps_product_id'] );
		if( isset( $arrValues['details'] ) && $boolDirectSet ) $this->set( 'm_strDetails', trim( stripcslashes( $arrValues['details'] ) ) ); elseif( isset( $arrValues['details'] ) ) $this->setDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['details'] ) : $arrValues['details'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['list_recurring_price'] ) && $boolDirectSet ) $this->set( 'm_fltListRecurringPrice', trim( $arrValues['list_recurring_price'] ) ); elseif( isset( $arrValues['list_recurring_price'] ) ) $this->setListRecurringPrice( $arrValues['list_recurring_price'] );
		if( isset( $arrValues['list_setup_price'] ) && $boolDirectSet ) $this->set( 'm_fltListSetupPrice', trim( $arrValues['list_setup_price'] ) ); elseif( isset( $arrValues['list_setup_price'] ) ) $this->setListSetupPrice( $arrValues['list_setup_price'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setLeadPackageId( $intLeadPackageId ) {
		$this->set( 'm_intLeadPackageId', CStrings::strToIntDef( $intLeadPackageId, NULL, false ) );
	}

	public function getLeadPackageId() {
		return $this->m_intLeadPackageId;
	}

	public function sqlLeadPackageId() {
		return ( true == isset( $this->m_intLeadPackageId ) ) ? ( string ) $this->m_intLeadPackageId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setBundlePsProductId( $intBundlePsProductId ) {
		$this->set( 'm_intBundlePsProductId', CStrings::strToIntDef( $intBundlePsProductId, NULL, false ) );
	}

	public function getBundlePsProductId() {
		return $this->m_intBundlePsProductId;
	}

	public function sqlBundlePsProductId() {
		return ( true == isset( $this->m_intBundlePsProductId ) ) ? ( string ) $this->m_intBundlePsProductId : 'NULL';
	}

	public function setDetails( $strDetails ) {
		$this->set( 'm_strDetails', CStrings::strTrimDef( $strDetails, -1, NULL, true ) );
	}

	public function getDetails() {
		return $this->m_strDetails;
	}

	public function sqlDetails() {
		return ( true == isset( $this->m_strDetails ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDetails ) : '\'' . addslashes( $this->m_strDetails ) . '\'' ) : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setListRecurringPrice( $fltListRecurringPrice ) {
		$this->set( 'm_fltListRecurringPrice', CStrings::strToFloatDef( $fltListRecurringPrice, NULL, false, 2 ) );
	}

	public function getListRecurringPrice() {
		return $this->m_fltListRecurringPrice;
	}

	public function sqlListRecurringPrice() {
		return ( true == isset( $this->m_fltListRecurringPrice ) ) ? ( string ) $this->m_fltListRecurringPrice : 'NULL';
	}

	public function setListSetupPrice( $fltListSetupPrice ) {
		$this->set( 'm_fltListSetupPrice', CStrings::strToFloatDef( $fltListSetupPrice, NULL, false, 2 ) );
	}

	public function getListSetupPrice() {
		return $this->m_fltListSetupPrice;
	}

	public function sqlListSetupPrice() {
		return ( true == isset( $this->m_fltListSetupPrice ) ) ? ( string ) $this->m_fltListSetupPrice : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, lead_package_id, ps_product_id, bundle_ps_product_id, details, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, list_recurring_price, list_setup_price )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlLeadPackageId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlBundlePsProductId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' , ' .
						$this->sqlListRecurringPrice() . ', ' .
						$this->sqlListSetupPrice() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lead_package_id = ' . $this->sqlLeadPackageId(). ',' ; } elseif( true == array_key_exists( 'LeadPackageId', $this->getChangedColumns() ) ) { $strSql .= ' lead_package_id = ' . $this->sqlLeadPackageId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bundle_ps_product_id = ' . $this->sqlBundlePsProductId(). ',' ; } elseif( true == array_key_exists( 'BundlePsProductId', $this->getChangedColumns() ) ) { $strSql .= ' bundle_ps_product_id = ' . $this->sqlBundlePsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' list_recurring_price = ' . $this->sqlListRecurringPrice(). ',' ; } elseif( true == array_key_exists( 'ListRecurringPrice', $this->getChangedColumns() ) ) { $strSql .= ' list_recurring_price = ' . $this->sqlListRecurringPrice() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' list_setup_price = ' . $this->sqlListSetupPrice(). ',' ; } elseif( true == array_key_exists( 'ListSetupPrice', $this->getChangedColumns() ) ) { $strSql .= ' list_setup_price = ' . $this->sqlListSetupPrice() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'lead_package_id' => $this->getLeadPackageId(),
			'ps_product_id' => $this->getPsProductId(),
			'bundle_ps_product_id' => $this->getBundlePsProductId(),
			'details' => $this->getDetails(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'list_recurring_price' => $this->getListRecurringPrice(),
			'list_setup_price' => $this->getListSetupPrice()
		);
	}

}
?>