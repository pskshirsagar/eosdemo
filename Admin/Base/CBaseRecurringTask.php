<?php

class CBaseRecurringTask extends CEosSingularBase {

	const TABLE_NAME = 'public.recurring_tasks';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intParentRecurringTaskId;
	protected $m_intTaskFrequencyId;
	protected $m_intDepartmentId;
	protected $m_intUserId;
	protected $m_intTaskTypeId;
	protected $m_intTaskStatusId;
	protected $m_intTaskPriorityId;
	protected $m_intWebsiteId;
	protected $m_intPropertyId;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_intClusterId;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_fltDeveloperStoryPoints;
	protected $m_strUrl;
	protected $m_strBeginDate;
	protected $m_strEndDate;
	protected $m_intDaysAllotted;
	protected $m_strLastPostedOn;
	protected $m_intIsDisabled;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltDeveloperStoryPoints = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['parent_recurring_task_id'] ) && $boolDirectSet ) $this->set( 'm_intParentRecurringTaskId', trim( $arrValues['parent_recurring_task_id'] ) ); elseif( isset( $arrValues['parent_recurring_task_id'] ) ) $this->setParentRecurringTaskId( $arrValues['parent_recurring_task_id'] );
		if( isset( $arrValues['task_frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskFrequencyId', trim( $arrValues['task_frequency_id'] ) ); elseif( isset( $arrValues['task_frequency_id'] ) ) $this->setTaskFrequencyId( $arrValues['task_frequency_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['task_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskTypeId', trim( $arrValues['task_type_id'] ) ); elseif( isset( $arrValues['task_type_id'] ) ) $this->setTaskTypeId( $arrValues['task_type_id'] );
		if( isset( $arrValues['task_status_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskStatusId', trim( $arrValues['task_status_id'] ) ); elseif( isset( $arrValues['task_status_id'] ) ) $this->setTaskStatusId( $arrValues['task_status_id'] );
		if( isset( $arrValues['task_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskPriorityId', trim( $arrValues['task_priority_id'] ) ); elseif( isset( $arrValues['task_priority_id'] ) ) $this->setTaskPriorityId( $arrValues['task_priority_id'] );
		if( isset( $arrValues['website_id'] ) && $boolDirectSet ) $this->set( 'm_intWebsiteId', trim( $arrValues['website_id'] ) ); elseif( isset( $arrValues['website_id'] ) ) $this->setWebsiteId( $arrValues['website_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['developer_story_points'] ) && $boolDirectSet ) $this->set( 'm_fltDeveloperStoryPoints', trim( $arrValues['developer_story_points'] ) ); elseif( isset( $arrValues['developer_story_points'] ) ) $this->setDeveloperStoryPoints( $arrValues['developer_story_points'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['begin_date'] ) && $boolDirectSet ) $this->set( 'm_strBeginDate', trim( $arrValues['begin_date'] ) ); elseif( isset( $arrValues['begin_date'] ) ) $this->setBeginDate( $arrValues['begin_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['days_allotted'] ) && $boolDirectSet ) $this->set( 'm_intDaysAllotted', trim( $arrValues['days_allotted'] ) ); elseif( isset( $arrValues['days_allotted'] ) ) $this->setDaysAllotted( $arrValues['days_allotted'] );
		if( isset( $arrValues['last_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedOn', trim( $arrValues['last_posted_on'] ) ); elseif( isset( $arrValues['last_posted_on'] ) ) $this->setLastPostedOn( $arrValues['last_posted_on'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setParentRecurringTaskId( $intParentRecurringTaskId ) {
		$this->set( 'm_intParentRecurringTaskId', CStrings::strToIntDef( $intParentRecurringTaskId, NULL, false ) );
	}

	public function getParentRecurringTaskId() {
		return $this->m_intParentRecurringTaskId;
	}

	public function sqlParentRecurringTaskId() {
		return ( true == isset( $this->m_intParentRecurringTaskId ) ) ? ( string ) $this->m_intParentRecurringTaskId : 'NULL';
	}

	public function setTaskFrequencyId( $intTaskFrequencyId ) {
		$this->set( 'm_intTaskFrequencyId', CStrings::strToIntDef( $intTaskFrequencyId, NULL, false ) );
	}

	public function getTaskFrequencyId() {
		return $this->m_intTaskFrequencyId;
	}

	public function sqlTaskFrequencyId() {
		return ( true == isset( $this->m_intTaskFrequencyId ) ) ? ( string ) $this->m_intTaskFrequencyId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setTaskTypeId( $intTaskTypeId ) {
		$this->set( 'm_intTaskTypeId', CStrings::strToIntDef( $intTaskTypeId, NULL, false ) );
	}

	public function getTaskTypeId() {
		return $this->m_intTaskTypeId;
	}

	public function sqlTaskTypeId() {
		return ( true == isset( $this->m_intTaskTypeId ) ) ? ( string ) $this->m_intTaskTypeId : 'NULL';
	}

	public function setTaskStatusId( $intTaskStatusId ) {
		$this->set( 'm_intTaskStatusId', CStrings::strToIntDef( $intTaskStatusId, NULL, false ) );
	}

	public function getTaskStatusId() {
		return $this->m_intTaskStatusId;
	}

	public function sqlTaskStatusId() {
		return ( true == isset( $this->m_intTaskStatusId ) ) ? ( string ) $this->m_intTaskStatusId : 'NULL';
	}

	public function setTaskPriorityId( $intTaskPriorityId ) {
		$this->set( 'm_intTaskPriorityId', CStrings::strToIntDef( $intTaskPriorityId, NULL, false ) );
	}

	public function getTaskPriorityId() {
		return $this->m_intTaskPriorityId;
	}

	public function sqlTaskPriorityId() {
		return ( true == isset( $this->m_intTaskPriorityId ) ) ? ( string ) $this->m_intTaskPriorityId : 'NULL';
	}

	public function setWebsiteId( $intWebsiteId ) {
		$this->set( 'm_intWebsiteId', CStrings::strToIntDef( $intWebsiteId, NULL, false ) );
	}

	public function getWebsiteId() {
		return $this->m_intWebsiteId;
	}

	public function sqlWebsiteId() {
		return ( true == isset( $this->m_intWebsiteId ) ) ? ( string ) $this->m_intWebsiteId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDeveloperStoryPoints( $fltDeveloperStoryPoints ) {
		$this->set( 'm_fltDeveloperStoryPoints', CStrings::strToFloatDef( $fltDeveloperStoryPoints, NULL, false, 2 ) );
	}

	public function getDeveloperStoryPoints() {
		return $this->m_fltDeveloperStoryPoints;
	}

	public function sqlDeveloperStoryPoints() {
		return ( true == isset( $this->m_fltDeveloperStoryPoints ) ) ? ( string ) $this->m_fltDeveloperStoryPoints : '0';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, -1, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : 'NULL';
	}

	public function setBeginDate( $strBeginDate ) {
		$this->set( 'm_strBeginDate', CStrings::strTrimDef( $strBeginDate, -1, NULL, true ) );
	}

	public function getBeginDate() {
		return $this->m_strBeginDate;
	}

	public function sqlBeginDate() {
		return ( true == isset( $this->m_strBeginDate ) ) ? '\'' . $this->m_strBeginDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setDaysAllotted( $intDaysAllotted ) {
		$this->set( 'm_intDaysAllotted', CStrings::strToIntDef( $intDaysAllotted, NULL, false ) );
	}

	public function getDaysAllotted() {
		return $this->m_intDaysAllotted;
	}

	public function sqlDaysAllotted() {
		return ( true == isset( $this->m_intDaysAllotted ) ) ? ( string ) $this->m_intDaysAllotted : 'NULL';
	}

	public function setLastPostedOn( $strLastPostedOn ) {
		$this->set( 'm_strLastPostedOn', CStrings::strTrimDef( $strLastPostedOn, -1, NULL, true ) );
	}

	public function getLastPostedOn() {
		return $this->m_strLastPostedOn;
	}

	public function sqlLastPostedOn() {
		return ( true == isset( $this->m_strLastPostedOn ) ) ? '\'' . $this->m_strLastPostedOn . '\'' : 'NULL';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, parent_recurring_task_id, task_frequency_id, department_id, user_id, task_type_id, task_status_id, task_priority_id, website_id, property_id, ps_product_id, ps_product_option_id, cluster_id, title, description, developer_story_points, url, begin_date, end_date, days_allotted, last_posted_on, is_disabled, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlParentRecurringTaskId() . ', ' .
 						$this->sqlTaskFrequencyId() . ', ' .
 						$this->sqlDepartmentId() . ', ' .
 						$this->sqlUserId() . ', ' .
 						$this->sqlTaskTypeId() . ', ' .
 						$this->sqlTaskStatusId() . ', ' .
 						$this->sqlTaskPriorityId() . ', ' .
 						$this->sqlWebsiteId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPsProductOptionId() . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlDeveloperStoryPoints() . ', ' .
 						$this->sqlUrl() . ', ' .
 						$this->sqlBeginDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlDaysAllotted() . ', ' .
 						$this->sqlLastPostedOn() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_recurring_task_id = ' . $this->sqlParentRecurringTaskId() . ','; } elseif( true == array_key_exists( 'ParentRecurringTaskId', $this->getChangedColumns() ) ) { $strSql .= ' parent_recurring_task_id = ' . $this->sqlParentRecurringTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_frequency_id = ' . $this->sqlTaskFrequencyId() . ','; } elseif( true == array_key_exists( 'TaskFrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' task_frequency_id = ' . $this->sqlTaskFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_type_id = ' . $this->sqlTaskTypeId() . ','; } elseif( true == array_key_exists( 'TaskTypeId', $this->getChangedColumns() ) ) { $strSql .= ' task_type_id = ' . $this->sqlTaskTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_status_id = ' . $this->sqlTaskStatusId() . ','; } elseif( true == array_key_exists( 'TaskStatusId', $this->getChangedColumns() ) ) { $strSql .= ' task_status_id = ' . $this->sqlTaskStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_priority_id = ' . $this->sqlTaskPriorityId() . ','; } elseif( true == array_key_exists( 'TaskPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' task_priority_id = ' . $this->sqlTaskPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; } elseif( true == array_key_exists( 'WebsiteId', $this->getChangedColumns() ) ) { $strSql .= ' website_id = ' . $this->sqlWebsiteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; } elseif( true == array_key_exists( 'PsProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' developer_story_points = ' . $this->sqlDeveloperStoryPoints() . ','; } elseif( true == array_key_exists( 'DeveloperStoryPoints', $this->getChangedColumns() ) ) { $strSql .= ' developer_story_points = ' . $this->sqlDeveloperStoryPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_date = ' . $this->sqlBeginDate() . ','; } elseif( true == array_key_exists( 'BeginDate', $this->getChangedColumns() ) ) { $strSql .= ' begin_date = ' . $this->sqlBeginDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' days_allotted = ' . $this->sqlDaysAllotted() . ','; } elseif( true == array_key_exists( 'DaysAllotted', $this->getChangedColumns() ) ) { $strSql .= ' days_allotted = ' . $this->sqlDaysAllotted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; } elseif( true == array_key_exists( 'LastPostedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'parent_recurring_task_id' => $this->getParentRecurringTaskId(),
			'task_frequency_id' => $this->getTaskFrequencyId(),
			'department_id' => $this->getDepartmentId(),
			'user_id' => $this->getUserId(),
			'task_type_id' => $this->getTaskTypeId(),
			'task_status_id' => $this->getTaskStatusId(),
			'task_priority_id' => $this->getTaskPriorityId(),
			'website_id' => $this->getWebsiteId(),
			'property_id' => $this->getPropertyId(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'cluster_id' => $this->getClusterId(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'developer_story_points' => $this->getDeveloperStoryPoints(),
			'url' => $this->getUrl(),
			'begin_date' => $this->getBeginDate(),
			'end_date' => $this->getEndDate(),
			'days_allotted' => $this->getDaysAllotted(),
			'last_posted_on' => $this->getLastPostedOn(),
			'is_disabled' => $this->getIsDisabled(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>