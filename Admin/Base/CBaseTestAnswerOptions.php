<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestAnswerOptions
 * Do not add any new functions to this class.
 */

class CBaseTestAnswerOptions extends CEosPluralBase {

	/**
	 * @return CTestAnswerOption[]
	 */
	public static function fetchTestAnswerOptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestAnswerOption', $objDatabase );
	}

	/**
	 * @return CTestAnswerOption
	 */
	public static function fetchTestAnswerOption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestAnswerOption', $objDatabase );
	}

	public static function fetchTestAnswerOptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_answer_options', $objDatabase );
	}

	public static function fetchTestAnswerOptionById( $intId, $objDatabase ) {
		return self::fetchTestAnswerOption( sprintf( 'SELECT * FROM test_answer_options WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestAnswerOptionsByTestId( $intTestId, $objDatabase ) {
		return self::fetchTestAnswerOptions( sprintf( 'SELECT * FROM test_answer_options WHERE test_id = %d', ( int ) $intTestId ), $objDatabase );
	}

	public static function fetchTestAnswerOptionsByTestQuestionId( $intTestQuestionId, $objDatabase ) {
		return self::fetchTestAnswerOptions( sprintf( 'SELECT * FROM test_answer_options WHERE test_question_id = %d', ( int ) $intTestQuestionId ), $objDatabase );
	}

}
?>