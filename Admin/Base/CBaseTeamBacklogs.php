<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTeamBacklogs
 * Do not add any new functions to this class.
 */

class CBaseTeamBacklogs extends CEosPluralBase {

	/**
	 * @return CTeamBacklog[]
	 */
	public static function fetchTeamBacklogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTeamBacklog::class, $objDatabase );
	}

	/**
	 * @return CTeamBacklog
	 */
	public static function fetchTeamBacklog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTeamBacklog::class, $objDatabase );
	}

	public static function fetchTeamBacklogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'team_backlogs', $objDatabase );
	}

	public static function fetchTeamBacklogById( $intId, $objDatabase ) {
		return self::fetchTeamBacklog( sprintf( 'SELECT * FROM team_backlogs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>