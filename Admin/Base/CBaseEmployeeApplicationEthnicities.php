<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationEthnicities
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationEthnicities extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationEthnicity[]
	 */
	public static function fetchEmployeeApplicationEthnicities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationEthnicity', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationEthnicity
	 */
	public static function fetchEmployeeApplicationEthnicity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationEthnicity', $objDatabase );
	}

	public static function fetchEmployeeApplicationEthnicityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_ethnicities', $objDatabase );
	}

	public static function fetchEmployeeApplicationEthnicityById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationEthnicity( sprintf( 'SELECT * FROM employee_application_ethnicities WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>