<?php

class CBaseImportRequest extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.import_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intDestinationCid;
	protected $m_intPropertyId;
	protected $m_intTemplatePropertyId;
	protected $m_strName;
	protected $m_intImportStatusTypeId;
	protected $m_strToken;
	protected $m_strPassword;
	protected $m_intCompetitorId;
	protected $m_intSettingTemplateId;
	protected $m_intHasResidents;
	protected $m_intIsSimpleCsvUpload;
	protected $m_intInitiatedBy;
	protected $m_strInitiatedOn;
	protected $m_strCompletedOn;
	protected $m_intCompletedBy;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intIntegrationDatabaseId;
	protected $m_intImportTypeId;
	protected $m_strEmail;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDestinationPropertyId;

	public function __construct() {
		parent::__construct();

		$this->m_intIsSimpleCsvUpload = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['destination_cid'] ) && $boolDirectSet ) $this->set( 'm_intDestinationCid', trim( $arrValues['destination_cid'] ) ); elseif( isset( $arrValues['destination_cid'] ) ) $this->setDestinationCid( $arrValues['destination_cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['template_property_id'] ) && $boolDirectSet ) $this->set( 'm_intTemplatePropertyId', trim( $arrValues['template_property_id'] ) ); elseif( isset( $arrValues['template_property_id'] ) ) $this->setTemplatePropertyId( $arrValues['template_property_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['import_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportStatusTypeId', trim( $arrValues['import_status_type_id'] ) ); elseif( isset( $arrValues['import_status_type_id'] ) ) $this->setImportStatusTypeId( $arrValues['import_status_type_id'] );
		if( isset( $arrValues['token'] ) && $boolDirectSet ) $this->set( 'm_strToken', trim( $arrValues['token'] ) ); elseif( isset( $arrValues['token'] ) ) $this->setToken( $arrValues['token'] );
		if( isset( $arrValues['password'] ) && $boolDirectSet ) $this->set( 'm_strPassword', trim( $arrValues['password'] ) ); elseif( isset( $arrValues['password'] ) ) $this->setPassword( $arrValues['password'] );
		if( isset( $arrValues['competitor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorId', trim( $arrValues['competitor_id'] ) ); elseif( isset( $arrValues['competitor_id'] ) ) $this->setCompetitorId( $arrValues['competitor_id'] );
		if( isset( $arrValues['setting_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSettingTemplateId', trim( $arrValues['setting_template_id'] ) ); elseif( isset( $arrValues['setting_template_id'] ) ) $this->setSettingTemplateId( $arrValues['setting_template_id'] );
		if( isset( $arrValues['has_residents'] ) && $boolDirectSet ) $this->set( 'm_intHasResidents', trim( $arrValues['has_residents'] ) ); elseif( isset( $arrValues['has_residents'] ) ) $this->setHasResidents( $arrValues['has_residents'] );
		if( isset( $arrValues['is_simple_csv_upload'] ) && $boolDirectSet ) $this->set( 'm_intIsSimpleCsvUpload', trim( $arrValues['is_simple_csv_upload'] ) ); elseif( isset( $arrValues['is_simple_csv_upload'] ) ) $this->setIsSimpleCsvUpload( $arrValues['is_simple_csv_upload'] );
		if( isset( $arrValues['initiated_by'] ) && $boolDirectSet ) $this->set( 'm_intInitiatedBy', trim( $arrValues['initiated_by'] ) ); elseif( isset( $arrValues['initiated_by'] ) ) $this->setInitiatedBy( $arrValues['initiated_by'] );
		if( isset( $arrValues['initiated_on'] ) && $boolDirectSet ) $this->set( 'm_strInitiatedOn', trim( $arrValues['initiated_on'] ) ); elseif( isset( $arrValues['initiated_on'] ) ) $this->setInitiatedOn( $arrValues['initiated_on'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['integration_database_id'] ) && $boolDirectSet ) $this->set( 'm_intIntegrationDatabaseId', trim( $arrValues['integration_database_id'] ) ); elseif( isset( $arrValues['integration_database_id'] ) ) $this->setIntegrationDatabaseId( $arrValues['integration_database_id'] );
		if( isset( $arrValues['import_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportTypeId', trim( $arrValues['import_type_id'] ) ); elseif( isset( $arrValues['import_type_id'] ) ) $this->setImportTypeId( $arrValues['import_type_id'] );
		if( isset( $arrValues['email'] ) && $boolDirectSet ) $this->set( 'm_strEmail', trim( $arrValues['email'] ) ); elseif( isset( $arrValues['email'] ) ) $this->setEmail( $arrValues['email'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['destination_property_id'] ) && $boolDirectSet ) $this->set( 'm_intDestinationPropertyId', trim( $arrValues['destination_property_id'] ) ); elseif( isset( $arrValues['destination_property_id'] ) ) $this->setDestinationPropertyId( $arrValues['destination_property_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDestinationCid( $intDestinationCid ) {
		$this->set( 'm_intDestinationCid', CStrings::strToIntDef( $intDestinationCid, NULL, false ) );
	}

	public function getDestinationCid() {
		return $this->m_intDestinationCid;
	}

	public function sqlDestinationCid() {
		return ( true == isset( $this->m_intDestinationCid ) ) ? ( string ) $this->m_intDestinationCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setTemplatePropertyId( $intTemplatePropertyId ) {
		$this->set( 'm_intTemplatePropertyId', CStrings::strToIntDef( $intTemplatePropertyId, NULL, false ) );
	}

	public function getTemplatePropertyId() {
		return $this->m_intTemplatePropertyId;
	}

	public function sqlTemplatePropertyId() {
		return ( true == isset( $this->m_intTemplatePropertyId ) ) ? ( string ) $this->m_intTemplatePropertyId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setImportStatusTypeId( $intImportStatusTypeId ) {
		$this->set( 'm_intImportStatusTypeId', CStrings::strToIntDef( $intImportStatusTypeId, NULL, false ) );
	}

	public function getImportStatusTypeId() {
		return $this->m_intImportStatusTypeId;
	}

	public function sqlImportStatusTypeId() {
		return ( true == isset( $this->m_intImportStatusTypeId ) ) ? ( string ) $this->m_intImportStatusTypeId : 'NULL';
	}

	public function setToken( $strToken ) {
		$this->set( 'm_strToken', CStrings::strTrimDef( $strToken, 244, NULL, true ) );
	}

	public function getToken() {
		return $this->m_strToken;
	}

	public function sqlToken() {
		return ( true == isset( $this->m_strToken ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strToken ) : '\'' . addslashes( $this->m_strToken ) . '\'' ) : 'NULL';
	}

	public function setPassword( $strPassword ) {
		$this->set( 'm_strPassword', CStrings::strTrimDef( $strPassword, 244, NULL, true ) );
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function sqlPassword() {
		return ( true == isset( $this->m_strPassword ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPassword ) : '\'' . addslashes( $this->m_strPassword ) . '\'' ) : 'NULL';
	}

	public function setCompetitorId( $intCompetitorId ) {
		$this->set( 'm_intCompetitorId', CStrings::strToIntDef( $intCompetitorId, NULL, false ) );
	}

	public function getCompetitorId() {
		return $this->m_intCompetitorId;
	}

	public function sqlCompetitorId() {
		return ( true == isset( $this->m_intCompetitorId ) ) ? ( string ) $this->m_intCompetitorId : 'NULL';
	}

	public function setSettingTemplateId( $intSettingTemplateId ) {
		$this->set( 'm_intSettingTemplateId', CStrings::strToIntDef( $intSettingTemplateId, NULL, false ) );
	}

	public function getSettingTemplateId() {
		return $this->m_intSettingTemplateId;
	}

	public function sqlSettingTemplateId() {
		return ( true == isset( $this->m_intSettingTemplateId ) ) ? ( string ) $this->m_intSettingTemplateId : 'NULL';
	}

	public function setHasResidents( $intHasResidents ) {
		$this->set( 'm_intHasResidents', CStrings::strToIntDef( $intHasResidents, NULL, false ) );
	}

	public function getHasResidents() {
		return $this->m_intHasResidents;
	}

	public function sqlHasResidents() {
		return ( true == isset( $this->m_intHasResidents ) ) ? ( string ) $this->m_intHasResidents : 'NULL';
	}

	public function setIsSimpleCsvUpload( $intIsSimpleCsvUpload ) {
		$this->set( 'm_intIsSimpleCsvUpload', CStrings::strToIntDef( $intIsSimpleCsvUpload, NULL, false ) );
	}

	public function getIsSimpleCsvUpload() {
		return $this->m_intIsSimpleCsvUpload;
	}

	public function sqlIsSimpleCsvUpload() {
		return ( true == isset( $this->m_intIsSimpleCsvUpload ) ) ? ( string ) $this->m_intIsSimpleCsvUpload : '0';
	}

	public function setInitiatedBy( $intInitiatedBy ) {
		$this->set( 'm_intInitiatedBy', CStrings::strToIntDef( $intInitiatedBy, NULL, false ) );
	}

	public function getInitiatedBy() {
		return $this->m_intInitiatedBy;
	}

	public function sqlInitiatedBy() {
		return ( true == isset( $this->m_intInitiatedBy ) ) ? ( string ) $this->m_intInitiatedBy : 'NULL';
	}

	public function setInitiatedOn( $strInitiatedOn ) {
		$this->set( 'm_strInitiatedOn', CStrings::strTrimDef( $strInitiatedOn, -1, NULL, true ) );
	}

	public function getInitiatedOn() {
		return $this->m_strInitiatedOn;
	}

	public function sqlInitiatedOn() {
		return ( true == isset( $this->m_strInitiatedOn ) ) ? '\'' . $this->m_strInitiatedOn . '\'' : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIntegrationDatabaseId( $intIntegrationDatabaseId ) {
		$this->set( 'm_intIntegrationDatabaseId', CStrings::strToIntDef( $intIntegrationDatabaseId, NULL, false ) );
	}

	public function getIntegrationDatabaseId() {
		return $this->m_intIntegrationDatabaseId;
	}

	public function sqlIntegrationDatabaseId() {
		return ( true == isset( $this->m_intIntegrationDatabaseId ) ) ? ( string ) $this->m_intIntegrationDatabaseId : 'NULL';
	}

	public function setImportTypeId( $intImportTypeId ) {
		$this->set( 'm_intImportTypeId', CStrings::strToIntDef( $intImportTypeId, NULL, false ) );
	}

	public function getImportTypeId() {
		return $this->m_intImportTypeId;
	}

	public function sqlImportTypeId() {
		return ( true == isset( $this->m_intImportTypeId ) ) ? ( string ) $this->m_intImportTypeId : 'NULL';
	}

	public function setEmail( $strEmail ) {
		$this->set( 'm_strEmail', CStrings::strTrimDef( $strEmail, 250, NULL, true ) );
	}

	public function getEmail() {
		return $this->m_strEmail;
	}

	public function sqlEmail() {
		return ( true == isset( $this->m_strEmail ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmail ) : '\'' . addslashes( $this->m_strEmail ) . '\'' ) : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDestinationPropertyId( $intDestinationPropertyId ) {
		$this->set( 'm_intDestinationPropertyId', CStrings::strToIntDef( $intDestinationPropertyId, NULL, false ) );
	}

	public function getDestinationPropertyId() {
		return $this->m_intDestinationPropertyId;
	}

	public function sqlDestinationPropertyId() {
		return ( true == isset( $this->m_intDestinationPropertyId ) ) ? ( string ) $this->m_intDestinationPropertyId : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, destination_cid, property_id, template_property_id, name, import_status_type_id, token, password, competitor_id, setting_template_id, has_residents, is_simple_csv_upload, initiated_by, initiated_on, completed_on, updated_by, updated_on, created_by, created_on, details, integration_database_id, import_type_id, email, approved_by, approved_on, destination_property_id, completed_by, deleted_on, deleted_by )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlDestinationCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlTemplatePropertyId() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlImportStatusTypeId() . ', ' .
		          $this->sqlToken() . ', ' .
		          $this->sqlPassword() . ', ' .
		          $this->sqlCompetitorId() . ', ' .
		          $this->sqlSettingTemplateId() . ', ' .
		          $this->sqlHasResidents() . ', ' .
		          $this->sqlIsSimpleCsvUpload() . ', ' .
		          $this->sqlInitiatedBy() . ', ' .
		          $this->sqlInitiatedOn() . ', ' .
		          $this->sqlCompletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlIntegrationDatabaseId() . ', ' .
		          $this->sqlImportTypeId() . ', ' .
		          $this->sqlEmail() . ', ' .
		          $this->sqlApprovedBy() . ', ' .
		          $this->sqlApprovedOn() . ', ' .
		          $this->sqlDestinationPropertyId() . ', ' .
		          $this->sqlCompletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          $this->sqlDeletedBy() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_cid = ' . $this->sqlDestinationCid(). ',' ; } elseif( true == array_key_exists( 'DestinationCid', $this->getChangedColumns() ) ) { $strSql .= ' destination_cid = ' . $this->sqlDestinationCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_property_id = ' . $this->sqlTemplatePropertyId(). ',' ; } elseif( true == array_key_exists( 'TemplatePropertyId', $this->getChangedColumns() ) ) { $strSql .= ' template_property_id = ' . $this->sqlTemplatePropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_status_type_id = ' . $this->sqlImportStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ImportStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' import_status_type_id = ' . $this->sqlImportStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' token = ' . $this->sqlToken(). ',' ; } elseif( true == array_key_exists( 'Token', $this->getChangedColumns() ) ) { $strSql .= ' token = ' . $this->sqlToken() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' password = ' . $this->sqlPassword(). ',' ; } elseif( true == array_key_exists( 'Password', $this->getChangedColumns() ) ) { $strSql .= ' password = ' . $this->sqlPassword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId(). ',' ; } elseif( true == array_key_exists( 'CompetitorId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' setting_template_id = ' . $this->sqlSettingTemplateId(). ',' ; } elseif( true == array_key_exists( 'SettingTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' setting_template_id = ' . $this->sqlSettingTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_residents = ' . $this->sqlHasResidents(). ',' ; } elseif( true == array_key_exists( 'HasResidents', $this->getChangedColumns() ) ) { $strSql .= ' has_residents = ' . $this->sqlHasResidents() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_simple_csv_upload = ' . $this->sqlIsSimpleCsvUpload(). ',' ; } elseif( true == array_key_exists( 'IsSimpleCsvUpload', $this->getChangedColumns() ) ) { $strSql .= ' is_simple_csv_upload = ' . $this->sqlIsSimpleCsvUpload() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initiated_by = ' . $this->sqlInitiatedBy(). ',' ; } elseif( true == array_key_exists( 'InitiatedBy', $this->getChangedColumns() ) ) { $strSql .= ' initiated_by = ' . $this->sqlInitiatedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' initiated_on = ' . $this->sqlInitiatedOn(). ',' ; } elseif( true == array_key_exists( 'InitiatedOn', $this->getChangedColumns() ) ) { $strSql .= ' initiated_on = ' . $this->sqlInitiatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId(). ',' ; } elseif( true == array_key_exists( 'IntegrationDatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' integration_database_id = ' . $this->sqlIntegrationDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_type_id = ' . $this->sqlImportTypeId(). ',' ; } elseif( true == array_key_exists( 'ImportTypeId', $this->getChangedColumns() ) ) { $strSql .= ' import_type_id = ' . $this->sqlImportTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email = ' . $this->sqlEmail(). ',' ; } elseif( true == array_key_exists( 'Email', $this->getChangedColumns() ) ) { $strSql .= ' email = ' . $this->sqlEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_property_id = ' . $this->sqlDestinationPropertyId(). ',' ; } elseif( true == array_key_exists( 'DestinationPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' destination_property_id = ' . $this->sqlDestinationPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'destination_cid' => $this->getDestinationCid(),
			'property_id' => $this->getPropertyId(),
			'template_property_id' => $this->getTemplatePropertyId(),
			'name' => $this->getName(),
			'import_status_type_id' => $this->getImportStatusTypeId(),
			'token' => $this->getToken(),
			'password' => $this->getPassword(),
			'competitor_id' => $this->getCompetitorId(),
			'setting_template_id' => $this->getSettingTemplateId(),
			'has_residents' => $this->getHasResidents(),
			'is_simple_csv_upload' => $this->getIsSimpleCsvUpload(),
			'initiated_by' => $this->getInitiatedBy(),
			'initiated_on' => $this->getInitiatedOn(),
			'completed_on' => $this->getCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'integration_database_id' => $this->getIntegrationDatabaseId(),
			'import_type_id' => $this->getImportTypeId(),
			'email' => $this->getEmail(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'destination_property_id' => $this->getDestinationPropertyId(),
			'completed_by' => $this->getCompletedBy(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>