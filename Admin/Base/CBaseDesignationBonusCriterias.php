<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDesignationBonusCriterias
 * Do not add any new functions to this class.
 */

class CBaseDesignationBonusCriterias extends CEosPluralBase {

	/**
	 * @return CDesignationBonusCriteria[]
	 */
	public static function fetchDesignationBonusCriterias( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDesignationBonusCriteria::class, $objDatabase );
	}

	/**
	 * @return CDesignationBonusCriteria
	 */
	public static function fetchDesignationBonusCriteria( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDesignationBonusCriteria::class, $objDatabase );
	}

	public static function fetchDesignationBonusCriteriaCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'designation_bonus_criterias', $objDatabase );
	}

	public static function fetchDesignationBonusCriteriaById( $intId, $objDatabase ) {
		return self::fetchDesignationBonusCriteria( sprintf( 'SELECT * FROM designation_bonus_criterias WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDesignationBonusCriteriasByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchDesignationBonusCriterias( sprintf( 'SELECT * FROM designation_bonus_criterias WHERE designation_id = %d', $intDesignationId ), $objDatabase );
	}

}
?>