<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeSalaryTypes
 * Do not add any new functions to this class.
 */

class CBaseEmployeeSalaryTypes extends CEosPluralBase {

	/**
	 * @return CEmployeeSalaryType[]
	 */
	public static function fetchEmployeeSalaryTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeSalaryType', $objDatabase );
	}

	/**
	 * @return CEmployeeSalaryType
	 */
	public static function fetchEmployeeSalaryType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeSalaryType', $objDatabase );
	}

	public static function fetchEmployeeSalaryTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_salary_types', $objDatabase );
	}

	public static function fetchEmployeeSalaryTypeById( $intId, $objDatabase ) {
		return self::fetchEmployeeSalaryType( sprintf( 'SELECT * FROM employee_salary_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>