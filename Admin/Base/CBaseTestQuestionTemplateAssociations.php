<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestQuestionTemplateAssociations
 * Do not add any new functions to this class.
 */

class CBaseTestQuestionTemplateAssociations extends CEosPluralBase {

	/**
	 * @return CTestQuestionTemplateAssociation[]
	 */
	public static function fetchTestQuestionTemplateAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestQuestionTemplateAssociation', $objDatabase );
	}

	/**
	 * @return CTestQuestionTemplateAssociation
	 */
	public static function fetchTestQuestionTemplateAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestQuestionTemplateAssociation', $objDatabase );
	}

	public static function fetchTestQuestionTemplateAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_question_template_associations', $objDatabase );
	}

	public static function fetchTestQuestionTemplateAssociationById( $intId, $objDatabase ) {
		return self::fetchTestQuestionTemplateAssociation( sprintf( 'SELECT * FROM test_question_template_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestQuestionTemplateAssociationsByTestId( $intTestId, $objDatabase ) {
		return self::fetchTestQuestionTemplateAssociations( sprintf( 'SELECT * FROM test_question_template_associations WHERE test_id = %d', ( int ) $intTestId ), $objDatabase );
	}

	public static function fetchTestQuestionTemplateAssociationsByTestTemplateId( $intTestTemplateId, $objDatabase ) {
		return self::fetchTestQuestionTemplateAssociations( sprintf( 'SELECT * FROM test_question_template_associations WHERE test_template_id = %d', ( int ) $intTestTemplateId ), $objDatabase );
	}

}
?>