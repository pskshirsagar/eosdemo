<?php

class CBaseTestQuestion extends CEosSingularBase {

	const TABLE_NAME = 'public.test_questions';

	protected $m_intId;
	protected $m_intTestQuestionTypeId;
	protected $m_intTestQuestionGroupId;
	protected $m_intTestLevelTypeId;
	protected $m_strQuestion;
	protected $m_strAnswer;
	protected $m_strAnswerExplanation;
	protected $m_intMaximumPoints;
	protected $m_intMaximumTime;
	protected $m_strAdditionalFields;
	protected $m_intIsOptional;
	protected $m_intIsPublic;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsRenderHtml;

	public function __construct() {
		parent::__construct();

		$this->m_intIsOptional = '0';
		$this->m_intIsPublic = '1';
		$this->m_intIsPublished = '0';
		$this->m_intOrderNum = '0';
		$this->m_boolIsRenderHtml = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['test_question_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTestQuestionTypeId', trim( $arrValues['test_question_type_id'] ) ); elseif( isset( $arrValues['test_question_type_id'] ) ) $this->setTestQuestionTypeId( $arrValues['test_question_type_id'] );
		if( isset( $arrValues['test_question_group_id'] ) && $boolDirectSet ) $this->set( 'm_intTestQuestionGroupId', trim( $arrValues['test_question_group_id'] ) ); elseif( isset( $arrValues['test_question_group_id'] ) ) $this->setTestQuestionGroupId( $arrValues['test_question_group_id'] );
		if( isset( $arrValues['test_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTestLevelTypeId', trim( $arrValues['test_level_type_id'] ) ); elseif( isset( $arrValues['test_level_type_id'] ) ) $this->setTestLevelTypeId( $arrValues['test_level_type_id'] );
		if( isset( $arrValues['question'] ) && $boolDirectSet ) $this->set( 'm_strQuestion', trim( $arrValues['question'] ) ); elseif( isset( $arrValues['question'] ) ) $this->setQuestion( $arrValues['question'] );
		if( isset( $arrValues['answer'] ) && $boolDirectSet ) $this->set( 'm_strAnswer', trim( $arrValues['answer'] ) ); elseif( isset( $arrValues['answer'] ) ) $this->setAnswer( $arrValues['answer'] );
		if( isset( $arrValues['answer_explanation'] ) && $boolDirectSet ) $this->set( 'm_strAnswerExplanation', trim( $arrValues['answer_explanation'] ) ); elseif( isset( $arrValues['answer_explanation'] ) ) $this->setAnswerExplanation( $arrValues['answer_explanation'] );
		if( isset( $arrValues['maximum_points'] ) && $boolDirectSet ) $this->set( 'm_intMaximumPoints', trim( $arrValues['maximum_points'] ) ); elseif( isset( $arrValues['maximum_points'] ) ) $this->setMaximumPoints( $arrValues['maximum_points'] );
		if( isset( $arrValues['maximum_time'] ) && $boolDirectSet ) $this->set( 'm_intMaximumTime', trim( $arrValues['maximum_time'] ) ); elseif( isset( $arrValues['maximum_time'] ) ) $this->setMaximumTime( $arrValues['maximum_time'] );
		if( isset( $arrValues['additional_fields'] ) && $boolDirectSet ) $this->set( 'm_strAdditionalFields', trim( stripcslashes( $arrValues['additional_fields'] ) ) ); elseif( isset( $arrValues['additional_fields'] ) ) $this->setAdditionalFields( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['additional_fields'] ) : $arrValues['additional_fields'] );
		if( isset( $arrValues['is_optional'] ) && $boolDirectSet ) $this->set( 'm_intIsOptional', trim( $arrValues['is_optional'] ) ); elseif( isset( $arrValues['is_optional'] ) ) $this->setIsOptional( $arrValues['is_optional'] );
		if( isset( $arrValues['is_public'] ) && $boolDirectSet ) $this->set( 'm_intIsPublic', trim( $arrValues['is_public'] ) ); elseif( isset( $arrValues['is_public'] ) ) $this->setIsPublic( $arrValues['is_public'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_render_html'] ) && $boolDirectSet ) $this->set( 'm_boolIsRenderHtml', trim( stripcslashes( $arrValues['is_render_html'] ) ) ); elseif( isset( $arrValues['is_render_html'] ) ) $this->setIsRenderHtml( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_render_html'] ) : $arrValues['is_render_html'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTestQuestionTypeId( $intTestQuestionTypeId ) {
		$this->set( 'm_intTestQuestionTypeId', CStrings::strToIntDef( $intTestQuestionTypeId, NULL, false ) );
	}

	public function getTestQuestionTypeId() {
		return $this->m_intTestQuestionTypeId;
	}

	public function sqlTestQuestionTypeId() {
		return ( true == isset( $this->m_intTestQuestionTypeId ) ) ? ( string ) $this->m_intTestQuestionTypeId : 'NULL';
	}

	public function setTestQuestionGroupId( $intTestQuestionGroupId ) {
		$this->set( 'm_intTestQuestionGroupId', CStrings::strToIntDef( $intTestQuestionGroupId, NULL, false ) );
	}

	public function getTestQuestionGroupId() {
		return $this->m_intTestQuestionGroupId;
	}

	public function sqlTestQuestionGroupId() {
		return ( true == isset( $this->m_intTestQuestionGroupId ) ) ? ( string ) $this->m_intTestQuestionGroupId : 'NULL';
	}

	public function setTestLevelTypeId( $intTestLevelTypeId ) {
		$this->set( 'm_intTestLevelTypeId', CStrings::strToIntDef( $intTestLevelTypeId, NULL, false ) );
	}

	public function getTestLevelTypeId() {
		return $this->m_intTestLevelTypeId;
	}

	public function sqlTestLevelTypeId() {
		return ( true == isset( $this->m_intTestLevelTypeId ) ) ? ( string ) $this->m_intTestLevelTypeId : 'NULL';
	}

	public function setQuestion( $strQuestion ) {
		$this->set( 'm_strQuestion', CStrings::strTrimDef( $strQuestion, -1, NULL, true ) );
	}

	public function getQuestion() {
		return $this->m_strQuestion;
	}

	public function sqlQuestion() {
		return ( true == isset( $this->m_strQuestion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strQuestion ) : '\'' . addslashes( $this->m_strQuestion ) . '\'' ) : 'NULL';
	}

	public function setAnswer( $strAnswer ) {
		$this->set( 'm_strAnswer', CStrings::strTrimDef( $strAnswer, -1, NULL, true ) );
	}

	public function getAnswer() {
		return $this->m_strAnswer;
	}

	public function sqlAnswer() {
		return ( true == isset( $this->m_strAnswer ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAnswer ) : '\'' . addslashes( $this->m_strAnswer ) . '\'' ) : 'NULL';
	}

	public function setAnswerExplanation( $strAnswerExplanation ) {
		$this->set( 'm_strAnswerExplanation', CStrings::strTrimDef( $strAnswerExplanation, -1, NULL, true ) );
	}

	public function getAnswerExplanation() {
		return $this->m_strAnswerExplanation;
	}

	public function sqlAnswerExplanation() {
		return ( true == isset( $this->m_strAnswerExplanation ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAnswerExplanation ) : '\'' . addslashes( $this->m_strAnswerExplanation ) . '\'' ) : 'NULL';
	}

	public function setMaximumPoints( $intMaximumPoints ) {
		$this->set( 'm_intMaximumPoints', CStrings::strToIntDef( $intMaximumPoints, NULL, false ) );
	}

	public function getMaximumPoints() {
		return $this->m_intMaximumPoints;
	}

	public function sqlMaximumPoints() {
		return ( true == isset( $this->m_intMaximumPoints ) ) ? ( string ) $this->m_intMaximumPoints : 'NULL';
	}

	public function setMaximumTime( $intMaximumTime ) {
		$this->set( 'm_intMaximumTime', CStrings::strToIntDef( $intMaximumTime, NULL, false ) );
	}

	public function getMaximumTime() {
		return $this->m_intMaximumTime;
	}

	public function sqlMaximumTime() {
		return ( true == isset( $this->m_intMaximumTime ) ) ? ( string ) $this->m_intMaximumTime : 'NULL';
	}

	public function setAdditionalFields( $strAdditionalFields ) {
		$this->set( 'm_strAdditionalFields', CStrings::strTrimDef( $strAdditionalFields, -1, NULL, true ) );
	}

	public function getAdditionalFields() {
		return $this->m_strAdditionalFields;
	}

	public function sqlAdditionalFields() {
		return ( true == isset( $this->m_strAdditionalFields ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAdditionalFields ) : '\'' . addslashes( $this->m_strAdditionalFields ) . '\'' ) : 'NULL';
	}

	public function setIsOptional( $intIsOptional ) {
		$this->set( 'm_intIsOptional', CStrings::strToIntDef( $intIsOptional, NULL, false ) );
	}

	public function getIsOptional() {
		return $this->m_intIsOptional;
	}

	public function sqlIsOptional() {
		return ( true == isset( $this->m_intIsOptional ) ) ? ( string ) $this->m_intIsOptional : '0';
	}

	public function setIsPublic( $intIsPublic ) {
		$this->set( 'm_intIsPublic', CStrings::strToIntDef( $intIsPublic, NULL, false ) );
	}

	public function getIsPublic() {
		return $this->m_intIsPublic;
	}

	public function sqlIsPublic() {
		return ( true == isset( $this->m_intIsPublic ) ) ? ( string ) $this->m_intIsPublic : '1';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsRenderHtml( $boolIsRenderHtml ) {
		$this->set( 'm_boolIsRenderHtml', CStrings::strToBool( $boolIsRenderHtml ) );
	}

	public function getIsRenderHtml() {
		return $this->m_boolIsRenderHtml;
	}

	public function sqlIsRenderHtml() {
		return ( true == isset( $this->m_boolIsRenderHtml ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRenderHtml ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, test_question_type_id, test_question_group_id, test_level_type_id, question, answer, answer_explanation, maximum_points, maximum_time, additional_fields, is_optional, is_public, is_published, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, is_render_html )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlTestQuestionTypeId() . ', ' .
						$this->sqlTestQuestionGroupId() . ', ' .
						$this->sqlTestLevelTypeId() . ', ' .
						$this->sqlQuestion() . ', ' .
						$this->sqlAnswer() . ', ' .
						$this->sqlAnswerExplanation() . ', ' .
						$this->sqlMaximumPoints() . ', ' .
						$this->sqlMaximumTime() . ', ' .
						$this->sqlAdditionalFields() . ', ' .
						$this->sqlIsOptional() . ', ' .
						$this->sqlIsPublic() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsRenderHtml() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_question_type_id = ' . $this->sqlTestQuestionTypeId(). ',' ; } elseif( true == array_key_exists( 'TestQuestionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' test_question_type_id = ' . $this->sqlTestQuestionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_question_group_id = ' . $this->sqlTestQuestionGroupId(). ',' ; } elseif( true == array_key_exists( 'TestQuestionGroupId', $this->getChangedColumns() ) ) { $strSql .= ' test_question_group_id = ' . $this->sqlTestQuestionGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_level_type_id = ' . $this->sqlTestLevelTypeId(). ',' ; } elseif( true == array_key_exists( 'TestLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' test_level_type_id = ' . $this->sqlTestLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' question = ' . $this->sqlQuestion(). ',' ; } elseif( true == array_key_exists( 'Question', $this->getChangedColumns() ) ) { $strSql .= ' question = ' . $this->sqlQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answer = ' . $this->sqlAnswer(). ',' ; } elseif( true == array_key_exists( 'Answer', $this->getChangedColumns() ) ) { $strSql .= ' answer = ' . $this->sqlAnswer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answer_explanation = ' . $this->sqlAnswerExplanation(). ',' ; } elseif( true == array_key_exists( 'AnswerExplanation', $this->getChangedColumns() ) ) { $strSql .= ' answer_explanation = ' . $this->sqlAnswerExplanation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maximum_points = ' . $this->sqlMaximumPoints(). ',' ; } elseif( true == array_key_exists( 'MaximumPoints', $this->getChangedColumns() ) ) { $strSql .= ' maximum_points = ' . $this->sqlMaximumPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maximum_time = ' . $this->sqlMaximumTime(). ',' ; } elseif( true == array_key_exists( 'MaximumTime', $this->getChangedColumns() ) ) { $strSql .= ' maximum_time = ' . $this->sqlMaximumTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_fields = ' . $this->sqlAdditionalFields(). ',' ; } elseif( true == array_key_exists( 'AdditionalFields', $this->getChangedColumns() ) ) { $strSql .= ' additional_fields = ' . $this->sqlAdditionalFields() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional(). ',' ; } elseif( true == array_key_exists( 'IsOptional', $this->getChangedColumns() ) ) { $strSql .= ' is_optional = ' . $this->sqlIsOptional() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_public = ' . $this->sqlIsPublic(). ',' ; } elseif( true == array_key_exists( 'IsPublic', $this->getChangedColumns() ) ) { $strSql .= ' is_public = ' . $this->sqlIsPublic() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_render_html = ' . $this->sqlIsRenderHtml(). ',' ; } elseif( true == array_key_exists( 'IsRenderHtml', $this->getChangedColumns() ) ) { $strSql .= ' is_render_html = ' . $this->sqlIsRenderHtml() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'test_question_type_id' => $this->getTestQuestionTypeId(),
			'test_question_group_id' => $this->getTestQuestionGroupId(),
			'test_level_type_id' => $this->getTestLevelTypeId(),
			'question' => $this->getQuestion(),
			'answer' => $this->getAnswer(),
			'answer_explanation' => $this->getAnswerExplanation(),
			'maximum_points' => $this->getMaximumPoints(),
			'maximum_time' => $this->getMaximumTime(),
			'additional_fields' => $this->getAdditionalFields(),
			'is_optional' => $this->getIsOptional(),
			'is_public' => $this->getIsPublic(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_render_html' => $this->getIsRenderHtml()
		);
	}

}
?>