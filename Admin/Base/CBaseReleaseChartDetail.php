<?php

class CBaseReleaseChartDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.release_chart_details';

	protected $m_intId;
	protected $m_intReleaseReportDetailId;
	protected $m_intReleaseChartReportTemplateId;
	protected $m_strChartData;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['release_report_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intReleaseReportDetailId', trim( $arrValues['release_report_detail_id'] ) ); elseif( isset( $arrValues['release_report_detail_id'] ) ) $this->setReleaseReportDetailId( $arrValues['release_report_detail_id'] );
		if( isset( $arrValues['release_chart_report_template_id'] ) && $boolDirectSet ) $this->set( 'm_intReleaseChartReportTemplateId', trim( $arrValues['release_chart_report_template_id'] ) ); elseif( isset( $arrValues['release_chart_report_template_id'] ) ) $this->setReleaseChartReportTemplateId( $arrValues['release_chart_report_template_id'] );
		if( isset( $arrValues['chart_data'] ) && $boolDirectSet ) $this->set( 'm_strChartData', trim( stripcslashes( $arrValues['chart_data'] ) ) ); elseif( isset( $arrValues['chart_data'] ) ) $this->setChartData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['chart_data'] ) : $arrValues['chart_data'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setReleaseReportDetailId( $intReleaseReportDetailId ) {
		$this->set( 'm_intReleaseReportDetailId', CStrings::strToIntDef( $intReleaseReportDetailId, NULL, false ) );
	}

	public function getReleaseReportDetailId() {
		return $this->m_intReleaseReportDetailId;
	}

	public function sqlReleaseReportDetailId() {
		return ( true == isset( $this->m_intReleaseReportDetailId ) ) ? ( string ) $this->m_intReleaseReportDetailId : 'NULL';
	}

	public function setReleaseChartReportTemplateId( $intReleaseChartReportTemplateId ) {
		$this->set( 'm_intReleaseChartReportTemplateId', CStrings::strToIntDef( $intReleaseChartReportTemplateId, NULL, false ) );
	}

	public function getReleaseChartReportTemplateId() {
		return $this->m_intReleaseChartReportTemplateId;
	}

	public function sqlReleaseChartReportTemplateId() {
		return ( true == isset( $this->m_intReleaseChartReportTemplateId ) ) ? ( string ) $this->m_intReleaseChartReportTemplateId : 'NULL';
	}

	public function setChartData( $strChartData ) {
		$this->set( 'm_strChartData', CStrings::strTrimDef( $strChartData, -1, NULL, true ) );
	}

	public function getChartData() {
		return $this->m_strChartData;
	}

	public function sqlChartData() {
		return ( true == isset( $this->m_strChartData ) ) ? '\'' . addslashes( $this->m_strChartData ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, release_report_detail_id, release_chart_report_template_id, chart_data, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlReleaseReportDetailId() . ', ' .
 						$this->sqlReleaseChartReportTemplateId() . ', ' .
 						$this->sqlChartData() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_report_detail_id = ' . $this->sqlReleaseReportDetailId() . ','; } elseif( true == array_key_exists( 'ReleaseReportDetailId', $this->getChangedColumns() ) ) { $strSql .= ' release_report_detail_id = ' . $this->sqlReleaseReportDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_chart_report_template_id = ' . $this->sqlReleaseChartReportTemplateId() . ','; } elseif( true == array_key_exists( 'ReleaseChartReportTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' release_chart_report_template_id = ' . $this->sqlReleaseChartReportTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chart_data = ' . $this->sqlChartData() . ','; } elseif( true == array_key_exists( 'ChartData', $this->getChangedColumns() ) ) { $strSql .= ' chart_data = ' . $this->sqlChartData() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'release_report_detail_id' => $this->getReleaseReportDetailId(),
			'release_chart_report_template_id' => $this->getReleaseChartReportTemplateId(),
			'chart_data' => $this->getChartData(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>