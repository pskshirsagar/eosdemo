<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserSettings
 * Do not add any new functions to this class.
 */

class CBaseUserSettings extends CEosPluralBase {

	/**
	 * @return CUserSetting[]
	 */
	public static function fetchUserSettings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserSetting', $objDatabase );
	}

	/**
	 * @return CUserSetting
	 */
	public static function fetchUserSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserSetting', $objDatabase );
	}

	public static function fetchUserSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_settings', $objDatabase );
	}

	public static function fetchUserSettingById( $intId, $objDatabase ) {
		return self::fetchUserSetting( sprintf( 'SELECT * FROM user_settings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserSettingsByUserId( $intUserId, $objDatabase ) {
		return self::fetchUserSettings( sprintf( 'SELECT * FROM user_settings WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

}
?>