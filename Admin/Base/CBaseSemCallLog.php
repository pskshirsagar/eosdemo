<?php

class CBaseSemCallLog extends CEosSingularBase {

	const TABLE_NAME = 'public.sem_call_logs';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intCallId;
	protected $m_intSemSourceId;
	protected $m_intSemAdGroupId;
	protected $m_intSemKeywordId;
	protected $m_intCallPhoneNumberId;
	protected $m_strCallerPhoneNumber;
	protected $m_strDestinationPhoneNumber;
	protected $m_strCallDatetime;
	protected $m_intCallDurationSeconds;
	protected $m_intIsAutoDetected;
	protected $m_intIsUnanswered;
	protected $m_intStatsUpdated;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsAutoDetected = '0';
		$this->m_intIsUnanswered = '0';
		$this->m_intStatsUpdated = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['sem_source_id'] ) && $boolDirectSet ) $this->set( 'm_intSemSourceId', trim( $arrValues['sem_source_id'] ) ); elseif( isset( $arrValues['sem_source_id'] ) ) $this->setSemSourceId( $arrValues['sem_source_id'] );
		if( isset( $arrValues['sem_ad_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdGroupId', trim( $arrValues['sem_ad_group_id'] ) ); elseif( isset( $arrValues['sem_ad_group_id'] ) ) $this->setSemAdGroupId( $arrValues['sem_ad_group_id'] );
		if( isset( $arrValues['sem_keyword_id'] ) && $boolDirectSet ) $this->set( 'm_intSemKeywordId', trim( $arrValues['sem_keyword_id'] ) ); elseif( isset( $arrValues['sem_keyword_id'] ) ) $this->setSemKeywordId( $arrValues['sem_keyword_id'] );
		if( isset( $arrValues['call_phone_number_id'] ) && $boolDirectSet ) $this->set( 'm_intCallPhoneNumberId', trim( $arrValues['call_phone_number_id'] ) ); elseif( isset( $arrValues['call_phone_number_id'] ) ) $this->setCallPhoneNumberId( $arrValues['call_phone_number_id'] );
		if( isset( $arrValues['caller_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strCallerPhoneNumber', trim( stripcslashes( $arrValues['caller_phone_number'] ) ) ); elseif( isset( $arrValues['caller_phone_number'] ) ) $this->setCallerPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['caller_phone_number'] ) : $arrValues['caller_phone_number'] );
		if( isset( $arrValues['destination_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strDestinationPhoneNumber', trim( stripcslashes( $arrValues['destination_phone_number'] ) ) ); elseif( isset( $arrValues['destination_phone_number'] ) ) $this->setDestinationPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['destination_phone_number'] ) : $arrValues['destination_phone_number'] );
		if( isset( $arrValues['call_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCallDatetime', trim( $arrValues['call_datetime'] ) ); elseif( isset( $arrValues['call_datetime'] ) ) $this->setCallDatetime( $arrValues['call_datetime'] );
		if( isset( $arrValues['call_duration_seconds'] ) && $boolDirectSet ) $this->set( 'm_intCallDurationSeconds', trim( $arrValues['call_duration_seconds'] ) ); elseif( isset( $arrValues['call_duration_seconds'] ) ) $this->setCallDurationSeconds( $arrValues['call_duration_seconds'] );
		if( isset( $arrValues['is_auto_detected'] ) && $boolDirectSet ) $this->set( 'm_intIsAutoDetected', trim( $arrValues['is_auto_detected'] ) ); elseif( isset( $arrValues['is_auto_detected'] ) ) $this->setIsAutoDetected( $arrValues['is_auto_detected'] );
		if( isset( $arrValues['is_unanswered'] ) && $boolDirectSet ) $this->set( 'm_intIsUnanswered', trim( $arrValues['is_unanswered'] ) ); elseif( isset( $arrValues['is_unanswered'] ) ) $this->setIsUnanswered( $arrValues['is_unanswered'] );
		if( isset( $arrValues['stats_updated'] ) && $boolDirectSet ) $this->set( 'm_intStatsUpdated', trim( $arrValues['stats_updated'] ) ); elseif( isset( $arrValues['stats_updated'] ) ) $this->setStatsUpdated( $arrValues['stats_updated'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setSemSourceId( $intSemSourceId ) {
		$this->set( 'm_intSemSourceId', CStrings::strToIntDef( $intSemSourceId, NULL, false ) );
	}

	public function getSemSourceId() {
		return $this->m_intSemSourceId;
	}

	public function sqlSemSourceId() {
		return ( true == isset( $this->m_intSemSourceId ) ) ? ( string ) $this->m_intSemSourceId : 'NULL';
	}

	public function setSemAdGroupId( $intSemAdGroupId ) {
		$this->set( 'm_intSemAdGroupId', CStrings::strToIntDef( $intSemAdGroupId, NULL, false ) );
	}

	public function getSemAdGroupId() {
		return $this->m_intSemAdGroupId;
	}

	public function sqlSemAdGroupId() {
		return ( true == isset( $this->m_intSemAdGroupId ) ) ? ( string ) $this->m_intSemAdGroupId : 'NULL';
	}

	public function setSemKeywordId( $intSemKeywordId ) {
		$this->set( 'm_intSemKeywordId', CStrings::strToIntDef( $intSemKeywordId, NULL, false ) );
	}

	public function getSemKeywordId() {
		return $this->m_intSemKeywordId;
	}

	public function sqlSemKeywordId() {
		return ( true == isset( $this->m_intSemKeywordId ) ) ? ( string ) $this->m_intSemKeywordId : 'NULL';
	}

	public function setCallPhoneNumberId( $intCallPhoneNumberId ) {
		$this->set( 'm_intCallPhoneNumberId', CStrings::strToIntDef( $intCallPhoneNumberId, NULL, false ) );
	}

	public function getCallPhoneNumberId() {
		return $this->m_intCallPhoneNumberId;
	}

	public function sqlCallPhoneNumberId() {
		return ( true == isset( $this->m_intCallPhoneNumberId ) ) ? ( string ) $this->m_intCallPhoneNumberId : 'NULL';
	}

	public function setCallerPhoneNumber( $strCallerPhoneNumber ) {
		$this->set( 'm_strCallerPhoneNumber', CStrings::strTrimDef( $strCallerPhoneNumber, 30, NULL, true ) );
	}

	public function getCallerPhoneNumber() {
		return $this->m_strCallerPhoneNumber;
	}

	public function sqlCallerPhoneNumber() {
		return ( true == isset( $this->m_strCallerPhoneNumber ) ) ? '\'' . addslashes( $this->m_strCallerPhoneNumber ) . '\'' : 'NULL';
	}

	public function setDestinationPhoneNumber( $strDestinationPhoneNumber ) {
		$this->set( 'm_strDestinationPhoneNumber', CStrings::strTrimDef( $strDestinationPhoneNumber, 30, NULL, true ) );
	}

	public function getDestinationPhoneNumber() {
		return $this->m_strDestinationPhoneNumber;
	}

	public function sqlDestinationPhoneNumber() {
		return ( true == isset( $this->m_strDestinationPhoneNumber ) ) ? '\'' . addslashes( $this->m_strDestinationPhoneNumber ) . '\'' : 'NULL';
	}

	public function setCallDatetime( $strCallDatetime ) {
		$this->set( 'm_strCallDatetime', CStrings::strTrimDef( $strCallDatetime, -1, NULL, true ) );
	}

	public function getCallDatetime() {
		return $this->m_strCallDatetime;
	}

	public function sqlCallDatetime() {
		return ( true == isset( $this->m_strCallDatetime ) ) ? '\'' . $this->m_strCallDatetime . '\'' : 'NULL';
	}

	public function setCallDurationSeconds( $intCallDurationSeconds ) {
		$this->set( 'm_intCallDurationSeconds', CStrings::strToIntDef( $intCallDurationSeconds, NULL, false ) );
	}

	public function getCallDurationSeconds() {
		return $this->m_intCallDurationSeconds;
	}

	public function sqlCallDurationSeconds() {
		return ( true == isset( $this->m_intCallDurationSeconds ) ) ? ( string ) $this->m_intCallDurationSeconds : 'NULL';
	}

	public function setIsAutoDetected( $intIsAutoDetected ) {
		$this->set( 'm_intIsAutoDetected', CStrings::strToIntDef( $intIsAutoDetected, NULL, false ) );
	}

	public function getIsAutoDetected() {
		return $this->m_intIsAutoDetected;
	}

	public function sqlIsAutoDetected() {
		return ( true == isset( $this->m_intIsAutoDetected ) ) ? ( string ) $this->m_intIsAutoDetected : '0';
	}

	public function setIsUnanswered( $intIsUnanswered ) {
		$this->set( 'm_intIsUnanswered', CStrings::strToIntDef( $intIsUnanswered, NULL, false ) );
	}

	public function getIsUnanswered() {
		return $this->m_intIsUnanswered;
	}

	public function sqlIsUnanswered() {
		return ( true == isset( $this->m_intIsUnanswered ) ) ? ( string ) $this->m_intIsUnanswered : '0';
	}

	public function setStatsUpdated( $intStatsUpdated ) {
		$this->set( 'm_intStatsUpdated', CStrings::strToIntDef( $intStatsUpdated, NULL, false ) );
	}

	public function getStatsUpdated() {
		return $this->m_intStatsUpdated;
	}

	public function sqlStatsUpdated() {
		return ( true == isset( $this->m_intStatsUpdated ) ) ? ( string ) $this->m_intStatsUpdated : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, call_id, sem_source_id, sem_ad_group_id, sem_keyword_id, call_phone_number_id, caller_phone_number, destination_phone_number, call_datetime, call_duration_seconds, is_auto_detected, is_unanswered, stats_updated, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCallId() . ', ' .
 						$this->sqlSemSourceId() . ', ' .
 						$this->sqlSemAdGroupId() . ', ' .
 						$this->sqlSemKeywordId() . ', ' .
 						$this->sqlCallPhoneNumberId() . ', ' .
 						$this->sqlCallerPhoneNumber() . ', ' .
 						$this->sqlDestinationPhoneNumber() . ', ' .
 						$this->sqlCallDatetime() . ', ' .
 						$this->sqlCallDurationSeconds() . ', ' .
 						$this->sqlIsAutoDetected() . ', ' .
 						$this->sqlIsUnanswered() . ', ' .
 						$this->sqlStatsUpdated() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; } elseif( true == array_key_exists( 'SemSourceId', $this->getChangedColumns() ) ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; } elseif( true == array_key_exists( 'SemAdGroupId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId() . ','; } elseif( true == array_key_exists( 'SemKeywordId', $this->getChangedColumns() ) ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_phone_number_id = ' . $this->sqlCallPhoneNumberId() . ','; } elseif( true == array_key_exists( 'CallPhoneNumberId', $this->getChangedColumns() ) ) { $strSql .= ' call_phone_number_id = ' . $this->sqlCallPhoneNumberId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' caller_phone_number = ' . $this->sqlCallerPhoneNumber() . ','; } elseif( true == array_key_exists( 'CallerPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' caller_phone_number = ' . $this->sqlCallerPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_phone_number = ' . $this->sqlDestinationPhoneNumber() . ','; } elseif( true == array_key_exists( 'DestinationPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' destination_phone_number = ' . $this->sqlDestinationPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_datetime = ' . $this->sqlCallDatetime() . ','; } elseif( true == array_key_exists( 'CallDatetime', $this->getChangedColumns() ) ) { $strSql .= ' call_datetime = ' . $this->sqlCallDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_duration_seconds = ' . $this->sqlCallDurationSeconds() . ','; } elseif( true == array_key_exists( 'CallDurationSeconds', $this->getChangedColumns() ) ) { $strSql .= ' call_duration_seconds = ' . $this->sqlCallDurationSeconds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_auto_detected = ' . $this->sqlIsAutoDetected() . ','; } elseif( true == array_key_exists( 'IsAutoDetected', $this->getChangedColumns() ) ) { $strSql .= ' is_auto_detected = ' . $this->sqlIsAutoDetected() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_unanswered = ' . $this->sqlIsUnanswered() . ','; } elseif( true == array_key_exists( 'IsUnanswered', $this->getChangedColumns() ) ) { $strSql .= ' is_unanswered = ' . $this->sqlIsUnanswered() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' stats_updated = ' . $this->sqlStatsUpdated() . ','; } elseif( true == array_key_exists( 'StatsUpdated', $this->getChangedColumns() ) ) { $strSql .= ' stats_updated = ' . $this->sqlStatsUpdated() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'call_id' => $this->getCallId(),
			'sem_source_id' => $this->getSemSourceId(),
			'sem_ad_group_id' => $this->getSemAdGroupId(),
			'sem_keyword_id' => $this->getSemKeywordId(),
			'call_phone_number_id' => $this->getCallPhoneNumberId(),
			'caller_phone_number' => $this->getCallerPhoneNumber(),
			'destination_phone_number' => $this->getDestinationPhoneNumber(),
			'call_datetime' => $this->getCallDatetime(),
			'call_duration_seconds' => $this->getCallDurationSeconds(),
			'is_auto_detected' => $this->getIsAutoDetected(),
			'is_unanswered' => $this->getIsUnanswered(),
			'stats_updated' => $this->getStatsUpdated(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>