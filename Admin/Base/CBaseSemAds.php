<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAds
 * Do not add any new functions to this class.
 */

class CBaseSemAds extends CEosPluralBase {

	/**
	 * @return CSemAd[]
	 */
	public static function fetchSemAds( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemAd', $objDatabase );
	}

	/**
	 * @return CSemAd
	 */
	public static function fetchSemAd( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemAd', $objDatabase );
	}

	public static function fetchSemAdCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_ads', $objDatabase );
	}

	public static function fetchSemAdById( $intId, $objDatabase ) {
		return self::fetchSemAd( sprintf( 'SELECT * FROM sem_ads WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemAdsByCid( $intCid, $objDatabase ) {
		return self::fetchSemAds( sprintf( 'SELECT * FROM sem_ads WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSemAdsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSemAds( sprintf( 'SELECT * FROM sem_ads WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchSemAdsBySemCampaignId( $intSemCampaignId, $objDatabase ) {
		return self::fetchSemAds( sprintf( 'SELECT * FROM sem_ads WHERE sem_campaign_id = %d', ( int ) $intSemCampaignId ), $objDatabase );
	}

	public static function fetchSemAdsBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemAds( sprintf( 'SELECT * FROM sem_ads WHERE sem_ad_group_id = %d', ( int ) $intSemAdGroupId ), $objDatabase );
	}

}
?>