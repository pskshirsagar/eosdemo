<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CHonourBadges
 * Do not add any new functions to this class.
 */

class CBaseHonourBadges extends CEosPluralBase {

	/**
	 * @return CHonourBadge[]
	 */
	public static function fetchHonourBadges( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CHonourBadge', $objDatabase );
	}

	/**
	 * @return CHonourBadge
	 */
	public static function fetchHonourBadge( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHonourBadge', $objDatabase );
	}

	public static function fetchHonourBadgeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'honour_badges', $objDatabase );
	}

	public static function fetchHonourBadgeById( $intId, $objDatabase ) {
		return self::fetchHonourBadge( sprintf( 'SELECT * FROM honour_badges WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchHonourBadgesByHonourBadgeTypeId( $intHonourBadgeTypeId, $objDatabase ) {
		return self::fetchHonourBadges( sprintf( 'SELECT * FROM honour_badges WHERE honour_badge_type_id = %d', ( int ) $intHonourBadgeTypeId ), $objDatabase );
	}

	public static function fetchHonourBadgesByHonourBadgeSemesterId( $intHonourBadgeSemesterId, $objDatabase ) {
		return self::fetchHonourBadges( sprintf( 'SELECT * FROM honour_badges WHERE honour_badge_semester_id = %d', ( int ) $intHonourBadgeSemesterId ), $objDatabase );
	}

	public static function fetchHonourBadgesByHonourBadgeReasonId( $intHonourBadgeReasonId, $objDatabase ) {
		return self::fetchHonourBadges( sprintf( 'SELECT * FROM honour_badges WHERE honour_badge_reason_id = %d', ( int ) $intHonourBadgeReasonId ), $objDatabase );
	}

	public static function fetchHonourBadgesByTargetEmployeeId( $intTargetEmployeeId, $objDatabase ) {
		return self::fetchHonourBadges( sprintf( 'SELECT * FROM honour_badges WHERE target_employee_id = %d', ( int ) $intTargetEmployeeId ), $objDatabase );
	}

	public static function fetchHonourBadgesBySourceEmployeeId( $intSourceEmployeeId, $objDatabase ) {
		return self::fetchHonourBadges( sprintf( 'SELECT * FROM honour_badges WHERE source_employee_id = %d', ( int ) $intSourceEmployeeId ), $objDatabase );
	}

}
?>