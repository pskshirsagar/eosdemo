<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationContacts
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationContacts extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationContact[]
	 */
	public static function fetchEmployeeApplicationContacts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationContact', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationContact
	 */
	public static function fetchEmployeeApplicationContact( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationContact', $objDatabase );
	}

	public static function fetchEmployeeApplicationContactCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_contacts', $objDatabase );
	}

	public static function fetchEmployeeApplicationContactById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationContact( sprintf( 'SELECT * FROM employee_application_contacts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationContactsByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchEmployeeApplicationContacts( sprintf( 'SELECT * FROM employee_application_contacts WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationContactsByEmployeeApplicationContactTypeId( $intEmployeeApplicationContactTypeId, $objDatabase ) {
		return self::fetchEmployeeApplicationContacts( sprintf( 'SELECT * FROM employee_application_contacts WHERE employee_application_contact_type_id = %d', ( int ) $intEmployeeApplicationContactTypeId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationContactsByMassEmailId( $intMassEmailId, $objDatabase ) {
		return self::fetchEmployeeApplicationContacts( sprintf( 'SELECT * FROM employee_application_contacts WHERE mass_email_id = %d', ( int ) $intMassEmailId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationContactsBySystemEmailId( $intSystemEmailId, $objDatabase ) {
		return self::fetchEmployeeApplicationContacts( sprintf( 'SELECT * FROM employee_application_contacts WHERE system_email_id = %d', ( int ) $intSystemEmailId ), $objDatabase );
	}

}
?>