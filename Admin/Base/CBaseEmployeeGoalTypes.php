<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeGoalTypes
 * Do not add any new functions to this class.
 */

class CBaseEmployeeGoalTypes extends CEosPluralBase {

	/**
	 * @return CEmployeeGoalType[]
	 */
	public static function fetchEmployeeGoalTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeGoalType::class, $objDatabase );
	}

	/**
	 * @return CEmployeeGoalType
	 */
	public static function fetchEmployeeGoalType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeGoalType::class, $objDatabase );
	}

	public static function fetchEmployeeGoalTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_goal_types', $objDatabase );
	}

	public static function fetchEmployeeGoalTypeById( $intId, $objDatabase ) {
		return self::fetchEmployeeGoalType( sprintf( 'SELECT * FROM employee_goal_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>