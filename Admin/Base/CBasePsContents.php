<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsContents
 * Do not add any new functions to this class.
 */

class CBasePsContents extends CEosPluralBase {

	/**
	 * @return CPsContent[]
	 */
	public static function fetchPsContents( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsContent', $objDatabase );
	}

	/**
	 * @return CPsContent
	 */
	public static function fetchPsContent( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsContent', $objDatabase );
	}

	public static function fetchPsContentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_contents', $objDatabase );
	}

	public static function fetchPsContentById( $intId, $objDatabase ) {
		return self::fetchPsContent( sprintf( 'SELECT * FROM ps_contents WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>