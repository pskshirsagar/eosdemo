<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDbWatchPriorities
 * Do not add any new functions to this class.
 */

class CBaseDbWatchPriorities extends CEosPluralBase {

	/**
	 * @return CDbWatchPriority[]
	 */
	public static function fetchDbWatchPriorities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDbWatchPriority', $objDatabase );
	}

	/**
	 * @return CDbWatchPriority
	 */
	public static function fetchDbWatchPriority( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDbWatchPriority', $objDatabase );
	}

	public static function fetchDbWatchPriorityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'db_watch_priorities', $objDatabase );
	}

	public static function fetchDbWatchPriorityById( $intId, $objDatabase ) {
		return self::fetchDbWatchPriority( sprintf( 'SELECT * FROM db_watch_priorities WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>