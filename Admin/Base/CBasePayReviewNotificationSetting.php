<?php

class CBasePayReviewNotificationSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.pay_review_notification_settings';

	protected $m_intId;
	protected $m_strPayReviewYear;
	protected $m_strNotificationStartDate;
	protected $m_strNotificationEndDate;
	protected $m_intApproverLevel;
	protected $m_strNotificationScheduleDetails;
	protected $m_jsonNotificationScheduleDetails;
	protected $m_strCountryCode;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['pay_review_year'] ) && $boolDirectSet ) $this->set( 'm_strPayReviewYear', trim( $arrValues['pay_review_year'] ) ); elseif( isset( $arrValues['pay_review_year'] ) ) $this->setPayReviewYear( $arrValues['pay_review_year'] );
		if( isset( $arrValues['notification_start_date'] ) && $boolDirectSet ) $this->set( 'm_strNotificationStartDate', trim( $arrValues['notification_start_date'] ) ); elseif( isset( $arrValues['notification_start_date'] ) ) $this->setNotificationStartDate( $arrValues['notification_start_date'] );
		if( isset( $arrValues['notification_end_date'] ) && $boolDirectSet ) $this->set( 'm_strNotificationEndDate', trim( $arrValues['notification_end_date'] ) ); elseif( isset( $arrValues['notification_end_date'] ) ) $this->setNotificationEndDate( $arrValues['notification_end_date'] );
		if( isset( $arrValues['approver_level'] ) && $boolDirectSet ) $this->set( 'm_intApproverLevel', trim( $arrValues['approver_level'] ) ); elseif( isset( $arrValues['approver_level'] ) ) $this->setApproverLevel( $arrValues['approver_level'] );
		if( isset( $arrValues['notification_schedule_details'] ) ) $this->set( 'm_strNotificationScheduleDetails', trim( $arrValues['notification_schedule_details'] ) );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPayReviewYear( $strPayReviewYear ) {
		$this->set( 'm_strPayReviewYear', CStrings::strTrimDef( $strPayReviewYear, -1, NULL, true ) );
	}

	public function getPayReviewYear() {
		return $this->m_strPayReviewYear;
	}

	public function sqlPayReviewYear() {
		return ( true == isset( $this->m_strPayReviewYear ) ) ? '\'' . $this->m_strPayReviewYear . '\'' : 'NOW()';
	}

	public function setNotificationStartDate( $strNotificationStartDate ) {
		$this->set( 'm_strNotificationStartDate', CStrings::strTrimDef( $strNotificationStartDate, -1, NULL, true ) );
	}

	public function getNotificationStartDate() {
		return $this->m_strNotificationStartDate;
	}

	public function sqlNotificationStartDate() {
		return ( true == isset( $this->m_strNotificationStartDate ) ) ? '\'' . $this->m_strNotificationStartDate . '\'' : 'NOW()';
	}

	public function setNotificationEndDate( $strNotificationEndDate ) {
		$this->set( 'm_strNotificationEndDate', CStrings::strTrimDef( $strNotificationEndDate, -1, NULL, true ) );
	}

	public function getNotificationEndDate() {
		return $this->m_strNotificationEndDate;
	}

	public function sqlNotificationEndDate() {
		return ( true == isset( $this->m_strNotificationEndDate ) ) ? '\'' . $this->m_strNotificationEndDate . '\'' : 'NOW()';
	}

	public function setApproverLevel( $intApproverLevel ) {
		$this->set( 'm_intApproverLevel', CStrings::strToIntDef( $intApproverLevel, NULL, false ) );
	}

	public function getApproverLevel() {
		return $this->m_intApproverLevel;
	}

	public function sqlApproverLevel() {
		return ( true == isset( $this->m_intApproverLevel ) ) ? ( string ) $this->m_intApproverLevel : 'NULL';
	}

	public function setNotificationScheduleDetails( $jsonNotificationScheduleDetails ) {
		if( true == valObj( $jsonNotificationScheduleDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonNotificationScheduleDetails', $jsonNotificationScheduleDetails );
		} elseif( true == valJsonString( $jsonNotificationScheduleDetails ) ) {
			$this->set( 'm_jsonNotificationScheduleDetails', CStrings::strToJson( $jsonNotificationScheduleDetails ) );
		} else {
			$this->set( 'm_jsonNotificationScheduleDetails', NULL ); 
		}
		unset( $this->m_strNotificationScheduleDetails );
	}

	public function getNotificationScheduleDetails() {
		if( true == isset( $this->m_strNotificationScheduleDetails ) ) {
			$this->m_jsonNotificationScheduleDetails = CStrings::strToJson( $this->m_strNotificationScheduleDetails );
			unset( $this->m_strNotificationScheduleDetails );
		}
		return $this->m_jsonNotificationScheduleDetails;
	}

	public function sqlNotificationScheduleDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getNotificationScheduleDetails() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getNotificationScheduleDetails() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getNotificationScheduleDetails() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountryCode ) : '\'' . addslashes( $this->m_strCountryCode ) . '\'' ) : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, pay_review_year, notification_start_date, notification_end_date, approver_level, notification_schedule_details, country_code, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPayReviewYear() . ', ' .
						$this->sqlNotificationStartDate() . ', ' .
						$this->sqlNotificationEndDate() . ', ' .
						$this->sqlApproverLevel() . ', ' .
						$this->sqlNotificationScheduleDetails() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pay_review_year = ' . $this->sqlPayReviewYear(). ',' ; } elseif( true == array_key_exists( 'PayReviewYear', $this->getChangedColumns() ) ) { $strSql .= ' pay_review_year = ' . $this->sqlPayReviewYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notification_start_date = ' . $this->sqlNotificationStartDate(). ',' ; } elseif( true == array_key_exists( 'NotificationStartDate', $this->getChangedColumns() ) ) { $strSql .= ' notification_start_date = ' . $this->sqlNotificationStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notification_end_date = ' . $this->sqlNotificationEndDate(). ',' ; } elseif( true == array_key_exists( 'NotificationEndDate', $this->getChangedColumns() ) ) { $strSql .= ' notification_end_date = ' . $this->sqlNotificationEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approver_level = ' . $this->sqlApproverLevel(). ',' ; } elseif( true == array_key_exists( 'ApproverLevel', $this->getChangedColumns() ) ) { $strSql .= ' approver_level = ' . $this->sqlApproverLevel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notification_schedule_details = ' . $this->sqlNotificationScheduleDetails(). ',' ; } elseif( true == array_key_exists( 'NotificationScheduleDetails', $this->getChangedColumns() ) ) { $strSql .= ' notification_schedule_details = ' . $this->sqlNotificationScheduleDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'pay_review_year' => $this->getPayReviewYear(),
			'notification_start_date' => $this->getNotificationStartDate(),
			'notification_end_date' => $this->getNotificationEndDate(),
			'approver_level' => $this->getApproverLevel(),
			'notification_schedule_details' => $this->getNotificationScheduleDetails(),
			'country_code' => $this->getCountryCode(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>