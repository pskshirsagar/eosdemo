<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplateEmails
 * Do not add any new functions to this class.
 */

class CBaseSurveyTemplateEmails extends CEosPluralBase {

	/**
	 * @return CSurveyTemplateEmail[]
	 */
	public static function fetchSurveyTemplateEmails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyTemplateEmail', $objDatabase );
	}

	/**
	 * @return CSurveyTemplateEmail
	 */
	public static function fetchSurveyTemplateEmail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyTemplateEmail', $objDatabase );
	}

	public static function fetchSurveyTemplateEmailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_template_emails', $objDatabase );
	}

	public static function fetchSurveyTemplateEmailById( $intId, $objDatabase ) {
		return self::fetchSurveyTemplateEmail( sprintf( 'SELECT * FROM survey_template_emails WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>