<?php

class CBaseStatsItLog extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_it_logs';

	protected $m_intId;
	protected $m_intStatsTypeId;
	protected $m_intCount;
	protected $m_strLogDate;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['stats_type_id'] ) && $boolDirectSet ) $this->set( 'm_intStatsTypeId', trim( $arrValues['stats_type_id'] ) ); elseif( isset( $arrValues['stats_type_id'] ) ) $this->setStatsTypeId( $arrValues['stats_type_id'] );
		if( isset( $arrValues['count'] ) && $boolDirectSet ) $this->set( 'm_intCount', trim( $arrValues['count'] ) ); elseif( isset( $arrValues['count'] ) ) $this->setCount( $arrValues['count'] );
		if( isset( $arrValues['log_date'] ) && $boolDirectSet ) $this->set( 'm_strLogDate', trim( $arrValues['log_date'] ) ); elseif( isset( $arrValues['log_date'] ) ) $this->setLogDate( $arrValues['log_date'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setStatsTypeId( $intStatsTypeId ) {
		$this->set( 'm_intStatsTypeId', CStrings::strToIntDef( $intStatsTypeId, NULL, false ) );
	}

	public function getStatsTypeId() {
		return $this->m_intStatsTypeId;
	}

	public function sqlStatsTypeId() {
		return ( true == isset( $this->m_intStatsTypeId ) ) ? ( string ) $this->m_intStatsTypeId : 'NULL';
	}

	public function setCount( $intCount ) {
		$this->set( 'm_intCount', CStrings::strToIntDef( $intCount, NULL, false ) );
	}

	public function getCount() {
		return $this->m_intCount;
	}

	public function sqlCount() {
		return ( true == isset( $this->m_intCount ) ) ? ( string ) $this->m_intCount : 'NULL';
	}

	public function setLogDate( $strLogDate ) {
		$this->set( 'm_strLogDate', CStrings::strTrimDef( $strLogDate, -1, NULL, true ) );
	}

	public function getLogDate() {
		return $this->m_strLogDate;
	}

	public function sqlLogDate() {
		return ( true == isset( $this->m_strLogDate ) ) ? '\'' . $this->m_strLogDate . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, stats_type_id, count, log_date, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlStatsTypeId() . ', ' .
 						$this->sqlCount() . ', ' .
 						$this->sqlLogDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' stats_type_id = ' . $this->sqlStatsTypeId() . ','; } elseif( true == array_key_exists( 'StatsTypeId', $this->getChangedColumns() ) ) { $strSql .= ' stats_type_id = ' . $this->sqlStatsTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' count = ' . $this->sqlCount() . ','; } elseif( true == array_key_exists( 'Count', $this->getChangedColumns() ) ) { $strSql .= ' count = ' . $this->sqlCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_date = ' . $this->sqlLogDate() . ','; } elseif( true == array_key_exists( 'LogDate', $this->getChangedColumns() ) ) { $strSql .= ' log_date = ' . $this->sqlLogDate() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'stats_type_id' => $this->getStatsTypeId(),
			'count' => $this->getCount(),
			'log_date' => $this->getLogDate(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>