<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeCompensationLogs
 * Do not add any new functions to this class.
 */

class CBaseEmployeeCompensationLogs extends CEosPluralBase {

	/**
	 * @return CEmployeeCompensationLog[]
	 */
	public static function fetchEmployeeCompensationLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeCompensationLog', $objDatabase );
	}

	/**
	 * @return CEmployeeCompensationLog
	 */
	public static function fetchEmployeeCompensationLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeCompensationLog', $objDatabase );
	}

	public static function fetchEmployeeCompensationLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_compensation_logs', $objDatabase );
	}

	public static function fetchEmployeeCompensationLogById( $intId, $objDatabase ) {
		return self::fetchEmployeeCompensationLog( sprintf( 'SELECT * FROM employee_compensation_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeCompensationLogsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeCompensationLogs( sprintf( 'SELECT * FROM employee_compensation_logs WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeCompensationLogsByPurchaseRequestId( $intPurchaseRequestId, $objDatabase ) {
		return self::fetchEmployeeCompensationLogs( sprintf( 'SELECT * FROM employee_compensation_logs WHERE purchase_request_id = %d', ( int ) $intPurchaseRequestId ), $objDatabase );
	}

	public static function fetchEmployeeCompensationLogsByPreviousEmployeeEncryptionAssociationId( $intPreviousEmployeeEncryptionAssociationId, $objDatabase ) {
		return self::fetchEmployeeCompensationLogs( sprintf( 'SELECT * FROM employee_compensation_logs WHERE previous_employee_encryption_association_id = %d', ( int ) $intPreviousEmployeeEncryptionAssociationId ), $objDatabase );
	}

	public static function fetchEmployeeCompensationLogsByNewEmployeeEncryptionAssociationId( $intNewEmployeeEncryptionAssociationId, $objDatabase ) {
		return self::fetchEmployeeCompensationLogs( sprintf( 'SELECT * FROM employee_compensation_logs WHERE new_employee_encryption_association_id = %d', ( int ) $intNewEmployeeEncryptionAssociationId ), $objDatabase );
	}

	public static function fetchEmployeeCompensationLogsByBonusEmployeeEncryptionAssociationId( $intBonusEmployeeEncryptionAssociationId, $objDatabase ) {
		return self::fetchEmployeeCompensationLogs( sprintf( 'SELECT * FROM employee_compensation_logs WHERE bonus_employee_encryption_association_id = %d', ( int ) $intBonusEmployeeEncryptionAssociationId ), $objDatabase );
	}

	public static function fetchEmployeeCompensationLogsByHourlyRateEmployeeEncryptionAssociationId( $intHourlyRateEmployeeEncryptionAssociationId, $objDatabase ) {
		return self::fetchEmployeeCompensationLogs( sprintf( 'SELECT * FROM employee_compensation_logs WHERE hourly_rate_employee_encryption_association_id = %d', ( int ) $intHourlyRateEmployeeEncryptionAssociationId ), $objDatabase );
	}

}
?>