<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePotentialAllocations
 * Do not add any new functions to this class.
 */

class CBaseEmployeePotentialAllocations extends CEosPluralBase {

	/**
	 * @return CEmployeePotentialAllocation[]
	 */
	public static function fetchEmployeePotentialAllocations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeePotentialAllocation', $objDatabase );
	}

	/**
	 * @return CEmployeePotentialAllocation
	 */
	public static function fetchEmployeePotentialAllocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeePotentialAllocation', $objDatabase );
	}

	public static function fetchEmployeePotentialAllocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_potential_allocations', $objDatabase );
	}

	public static function fetchEmployeePotentialAllocationById( $intId, $objDatabase ) {
		return self::fetchEmployeePotentialAllocation( sprintf( 'SELECT * FROM employee_potential_allocations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeePotentialAllocationsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeePotentialAllocations( sprintf( 'SELECT * FROM employee_potential_allocations WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeePotentialAllocationsByEmployeePayStatusTypeId( $intEmployeePayStatusTypeId, $objDatabase ) {
		return self::fetchEmployeePotentialAllocations( sprintf( 'SELECT * FROM employee_potential_allocations WHERE employee_pay_status_type_id = %d', ( int ) $intEmployeePayStatusTypeId ), $objDatabase );
	}

	public static function fetchEmployeePotentialAllocationsByPotentialIncreaseEncryptionAssociationId( $intPotentialIncreaseEncryptionAssociationId, $objDatabase ) {
		return self::fetchEmployeePotentialAllocations( sprintf( 'SELECT * FROM employee_potential_allocations WHERE potential_increase_encryption_association_id = %d', ( int ) $intPotentialIncreaseEncryptionAssociationId ), $objDatabase );
	}

}
?>