<?php

class CBaseSurveyQuestionCondition extends CEosSingularBase {

	const TABLE_NAME = 'public.survey_question_conditions';

	protected $m_intId;
	protected $m_intSurveyQuestionTypeId;
	protected $m_intSurveyConditionTypeId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['survey_question_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyQuestionTypeId', trim( $arrValues['survey_question_type_id'] ) ); elseif( isset( $arrValues['survey_question_type_id'] ) ) $this->setSurveyQuestionTypeId( $arrValues['survey_question_type_id'] );
		if( isset( $arrValues['survey_condition_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyConditionTypeId', trim( $arrValues['survey_condition_type_id'] ) ); elseif( isset( $arrValues['survey_condition_type_id'] ) ) $this->setSurveyConditionTypeId( $arrValues['survey_condition_type_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSurveyQuestionTypeId( $intSurveyQuestionTypeId ) {
		$this->set( 'm_intSurveyQuestionTypeId', CStrings::strToIntDef( $intSurveyQuestionTypeId, NULL, false ) );
	}

	public function getSurveyQuestionTypeId() {
		return $this->m_intSurveyQuestionTypeId;
	}

	public function sqlSurveyQuestionTypeId() {
		return ( true == isset( $this->m_intSurveyQuestionTypeId ) ) ? ( string ) $this->m_intSurveyQuestionTypeId : 'NULL';
	}

	public function setSurveyConditionTypeId( $intSurveyConditionTypeId ) {
		$this->set( 'm_intSurveyConditionTypeId', CStrings::strToIntDef( $intSurveyConditionTypeId, NULL, false ) );
	}

	public function getSurveyConditionTypeId() {
		return $this->m_intSurveyConditionTypeId;
	}

	public function sqlSurveyConditionTypeId() {
		return ( true == isset( $this->m_intSurveyConditionTypeId ) ) ? ( string ) $this->m_intSurveyConditionTypeId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, survey_question_type_id, survey_condition_type_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSurveyQuestionTypeId() . ', ' .
 						$this->sqlSurveyConditionTypeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_question_type_id = ' . $this->sqlSurveyQuestionTypeId() . ','; } elseif( true == array_key_exists( 'SurveyQuestionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' survey_question_type_id = ' . $this->sqlSurveyQuestionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_condition_type_id = ' . $this->sqlSurveyConditionTypeId() . ','; } elseif( true == array_key_exists( 'SurveyConditionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' survey_condition_type_id = ' . $this->sqlSurveyConditionTypeId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'survey_question_type_id' => $this->getSurveyQuestionTypeId(),
			'survey_condition_type_id' => $this->getSurveyConditionTypeId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>