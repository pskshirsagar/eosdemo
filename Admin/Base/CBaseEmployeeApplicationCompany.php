<?php

class CBaseEmployeeApplicationCompany extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_application_companies';

	protected $m_intId;
	protected $m_strCompanyName;
	protected $m_strCity;
	protected $m_strLocation;
	protected $m_strTotalStrength;
	protected $m_strPhpStrength;
	protected $m_strQaStrength;
	protected $m_strComment;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrValues['company_name'] ) ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_name'] ) : $arrValues['company_name'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['location'] ) && $boolDirectSet ) $this->set( 'm_strLocation', trim( stripcslashes( $arrValues['location'] ) ) ); elseif( isset( $arrValues['location'] ) ) $this->setLocation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['location'] ) : $arrValues['location'] );
		if( isset( $arrValues['total_strength'] ) && $boolDirectSet ) $this->set( 'm_strTotalStrength', trim( stripcslashes( $arrValues['total_strength'] ) ) ); elseif( isset( $arrValues['total_strength'] ) ) $this->setTotalStrength( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['total_strength'] ) : $arrValues['total_strength'] );
		if( isset( $arrValues['php_strength'] ) && $boolDirectSet ) $this->set( 'm_strPhpStrength', trim( stripcslashes( $arrValues['php_strength'] ) ) ); elseif( isset( $arrValues['php_strength'] ) ) $this->setPhpStrength( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['php_strength'] ) : $arrValues['php_strength'] );
		if( isset( $arrValues['qa_strength'] ) && $boolDirectSet ) $this->set( 'm_strQaStrength', trim( stripcslashes( $arrValues['qa_strength'] ) ) ); elseif( isset( $arrValues['qa_strength'] ) ) $this->setQaStrength( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['qa_strength'] ) : $arrValues['qa_strength'] );
		if( isset( $arrValues['comment'] ) && $boolDirectSet ) $this->set( 'm_strComment', trim( stripcslashes( $arrValues['comment'] ) ) ); elseif( isset( $arrValues['comment'] ) ) $this->setComment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['comment'] ) : $arrValues['comment'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, -1, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, -1, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setLocation( $strLocation ) {
		$this->set( 'm_strLocation', CStrings::strTrimDef( $strLocation, -1, NULL, true ) );
	}

	public function getLocation() {
		return $this->m_strLocation;
	}

	public function sqlLocation() {
		return ( true == isset( $this->m_strLocation ) ) ? '\'' . addslashes( $this->m_strLocation ) . '\'' : 'NULL';
	}

	public function setTotalStrength( $strTotalStrength ) {
		$this->set( 'm_strTotalStrength', CStrings::strTrimDef( $strTotalStrength, -1, NULL, true ) );
	}

	public function getTotalStrength() {
		return $this->m_strTotalStrength;
	}

	public function sqlTotalStrength() {
		return ( true == isset( $this->m_strTotalStrength ) ) ? '\'' . addslashes( $this->m_strTotalStrength ) . '\'' : 'NULL';
	}

	public function setPhpStrength( $strPhpStrength ) {
		$this->set( 'm_strPhpStrength', CStrings::strTrimDef( $strPhpStrength, -1, NULL, true ) );
	}

	public function getPhpStrength() {
		return $this->m_strPhpStrength;
	}

	public function sqlPhpStrength() {
		return ( true == isset( $this->m_strPhpStrength ) ) ? '\'' . addslashes( $this->m_strPhpStrength ) . '\'' : 'NULL';
	}

	public function setQaStrength( $strQaStrength ) {
		$this->set( 'm_strQaStrength', CStrings::strTrimDef( $strQaStrength, -1, NULL, true ) );
	}

	public function getQaStrength() {
		return $this->m_strQaStrength;
	}

	public function sqlQaStrength() {
		return ( true == isset( $this->m_strQaStrength ) ) ? '\'' . addslashes( $this->m_strQaStrength ) . '\'' : 'NULL';
	}

	public function setComment( $strComment ) {
		$this->set( 'm_strComment', CStrings::strTrimDef( $strComment, -1, NULL, true ) );
	}

	public function getComment() {
		return $this->m_strComment;
	}

	public function sqlComment() {
		return ( true == isset( $this->m_strComment ) ) ? '\'' . addslashes( $this->m_strComment ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, company_name, city, location, total_strength, php_strength, qa_strength, comment, is_published, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCompanyName() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlLocation() . ', ' .
 						$this->sqlTotalStrength() . ', ' .
 						$this->sqlPhpStrength() . ', ' .
 						$this->sqlQaStrength() . ', ' .
 						$this->sqlComment() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' location = ' . $this->sqlLocation() . ','; } elseif( true == array_key_exists( 'Location', $this->getChangedColumns() ) ) { $strSql .= ' location = ' . $this->sqlLocation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_strength = ' . $this->sqlTotalStrength() . ','; } elseif( true == array_key_exists( 'TotalStrength', $this->getChangedColumns() ) ) { $strSql .= ' total_strength = ' . $this->sqlTotalStrength() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' php_strength = ' . $this->sqlPhpStrength() . ','; } elseif( true == array_key_exists( 'PhpStrength', $this->getChangedColumns() ) ) { $strSql .= ' php_strength = ' . $this->sqlPhpStrength() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_strength = ' . $this->sqlQaStrength() . ','; } elseif( true == array_key_exists( 'QaStrength', $this->getChangedColumns() ) ) { $strSql .= ' qa_strength = ' . $this->sqlQaStrength() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comment = ' . $this->sqlComment() . ','; } elseif( true == array_key_exists( 'Comment', $this->getChangedColumns() ) ) { $strSql .= ' comment = ' . $this->sqlComment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'company_name' => $this->getCompanyName(),
			'city' => $this->getCity(),
			'location' => $this->getLocation(),
			'total_strength' => $this->getTotalStrength(),
			'php_strength' => $this->getPhpStrength(),
			'qa_strength' => $this->getQaStrength(),
			'comment' => $this->getComment(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>