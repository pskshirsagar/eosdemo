<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CIlsServiceRestrictions
 * Do not add any new functions to this class.
 */

class CBaseIlsServiceRestrictions extends CEosPluralBase {

	/**
	 * @return CIlsServiceRestriction[]
	 */
	public static function fetchIlsServiceRestrictions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIlsServiceRestriction::class, $objDatabase );
	}

	/**
	 * @return CIlsServiceRestriction
	 */
	public static function fetchIlsServiceRestriction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIlsServiceRestriction::class, $objDatabase );
	}

	public static function fetchIlsServiceRestrictionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ils_service_restrictions', $objDatabase );
	}

	public static function fetchIlsServiceRestrictionById( $intId, $objDatabase ) {
		return self::fetchIlsServiceRestriction( sprintf( 'SELECT * FROM ils_service_restrictions WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchIlsServiceRestrictionsByInternetListingServiceId( $intInternetListingServiceId, $objDatabase ) {
		return self::fetchIlsServiceRestrictions( sprintf( 'SELECT * FROM ils_service_restrictions WHERE internet_listing_service_id = %d', $intInternetListingServiceId ), $objDatabase );
	}

	public static function fetchIlsServiceRestrictionsByIlsServiceRestrictionTypeId( $intIlsServiceRestrictionTypeId, $objDatabase ) {
		return self::fetchIlsServiceRestrictions( sprintf( 'SELECT * FROM ils_service_restrictions WHERE ils_service_restriction_type_id = %d', $intIlsServiceRestrictionTypeId ), $objDatabase );
	}

}
?>