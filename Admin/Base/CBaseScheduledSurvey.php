<?php

class CBaseScheduledSurvey extends CEosSingularBase {

	const TABLE_NAME = 'public.scheduled_surveys';

	protected $m_intId;
	protected $m_intSurveyTypeId;
	protected $m_intSurveyTemplateId;
	protected $m_intFrequencyId;
	protected $m_intResponderReferenceId;
	protected $m_intSubjectReferenceId;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strLastPostedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strStartDate = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['survey_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTypeId', trim( $arrValues['survey_type_id'] ) ); elseif( isset( $arrValues['survey_type_id'] ) ) $this->setSurveyTypeId( $arrValues['survey_type_id'] );
		if( isset( $arrValues['survey_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateId', trim( $arrValues['survey_template_id'] ) ); elseif( isset( $arrValues['survey_template_id'] ) ) $this->setSurveyTemplateId( $arrValues['survey_template_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['responder_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intResponderReferenceId', trim( $arrValues['responder_reference_id'] ) ); elseif( isset( $arrValues['responder_reference_id'] ) ) $this->setResponderReferenceId( $arrValues['responder_reference_id'] );
		if( isset( $arrValues['subject_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intSubjectReferenceId', trim( $arrValues['subject_reference_id'] ) ); elseif( isset( $arrValues['subject_reference_id'] ) ) $this->setSubjectReferenceId( $arrValues['subject_reference_id'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['last_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedOn', trim( $arrValues['last_posted_on'] ) ); elseif( isset( $arrValues['last_posted_on'] ) ) $this->setLastPostedOn( $arrValues['last_posted_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSurveyTypeId( $intSurveyTypeId ) {
		$this->set( 'm_intSurveyTypeId', CStrings::strToIntDef( $intSurveyTypeId, NULL, false ) );
	}

	public function getSurveyTypeId() {
		return $this->m_intSurveyTypeId;
	}

	public function sqlSurveyTypeId() {
		return ( true == isset( $this->m_intSurveyTypeId ) ) ? ( string ) $this->m_intSurveyTypeId : 'NULL';
	}

	public function setSurveyTemplateId( $intSurveyTemplateId ) {
		$this->set( 'm_intSurveyTemplateId', CStrings::strToIntDef( $intSurveyTemplateId, NULL, false ) );
	}

	public function getSurveyTemplateId() {
		return $this->m_intSurveyTemplateId;
	}

	public function sqlSurveyTemplateId() {
		return ( true == isset( $this->m_intSurveyTemplateId ) ) ? ( string ) $this->m_intSurveyTemplateId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setResponderReferenceId( $intResponderReferenceId ) {
		$this->set( 'm_intResponderReferenceId', CStrings::strToIntDef( $intResponderReferenceId, NULL, false ) );
	}

	public function getResponderReferenceId() {
		return $this->m_intResponderReferenceId;
	}

	public function sqlResponderReferenceId() {
		return ( true == isset( $this->m_intResponderReferenceId ) ) ? ( string ) $this->m_intResponderReferenceId : 'NULL';
	}

	public function setSubjectReferenceId( $intSubjectReferenceId ) {
		$this->set( 'm_intSubjectReferenceId', CStrings::strToIntDef( $intSubjectReferenceId, NULL, false ) );
	}

	public function getSubjectReferenceId() {
		return $this->m_intSubjectReferenceId;
	}

	public function sqlSubjectReferenceId() {
		return ( true == isset( $this->m_intSubjectReferenceId ) ) ? ( string ) $this->m_intSubjectReferenceId : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setLastPostedOn( $strLastPostedOn ) {
		$this->set( 'm_strLastPostedOn', CStrings::strTrimDef( $strLastPostedOn, -1, NULL, true ) );
	}

	public function getLastPostedOn() {
		return $this->m_strLastPostedOn;
	}

	public function sqlLastPostedOn() {
		return ( true == isset( $this->m_strLastPostedOn ) ) ? '\'' . $this->m_strLastPostedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, survey_type_id, survey_template_id, frequency_id, responder_reference_id, subject_reference_id, start_date, end_date, last_posted_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSurveyTypeId() . ', ' .
 						$this->sqlSurveyTemplateId() . ', ' .
 						$this->sqlFrequencyId() . ', ' .
 						$this->sqlResponderReferenceId() . ', ' .
 						$this->sqlSubjectReferenceId() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlLastPostedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_type_id = ' . $this->sqlSurveyTypeId() . ','; } elseif( true == array_key_exists( 'SurveyTypeId', $this->getChangedColumns() ) ) { $strSql .= ' survey_type_id = ' . $this->sqlSurveyTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' responder_reference_id = ' . $this->sqlResponderReferenceId() . ','; } elseif( true == array_key_exists( 'ResponderReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' responder_reference_id = ' . $this->sqlResponderReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject_reference_id = ' . $this->sqlSubjectReferenceId() . ','; } elseif( true == array_key_exists( 'SubjectReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' subject_reference_id = ' . $this->sqlSubjectReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; } elseif( true == array_key_exists( 'LastPostedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'survey_type_id' => $this->getSurveyTypeId(),
			'survey_template_id' => $this->getSurveyTemplateId(),
			'frequency_id' => $this->getFrequencyId(),
			'responder_reference_id' => $this->getResponderReferenceId(),
			'subject_reference_id' => $this->getSubjectReferenceId(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'last_posted_on' => $this->getLastPostedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>