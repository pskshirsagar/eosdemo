<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplateTypes
 * Do not add any new functions to this class.
 */

class CBaseDocumentTemplateTypes extends CEosPluralBase {

	/**
	 * @return CDocumentTemplateType[]
	 */
	public static function fetchDocumentTemplateTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDocumentTemplateType', $objDatabase );
	}

	/**
	 * @return CDocumentTemplateType
	 */
	public static function fetchDocumentTemplateType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentTemplateType', $objDatabase );
	}

	public static function fetchDocumentTemplateTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_template_types', $objDatabase );
	}

	public static function fetchDocumentTemplateTypeById( $intId, $objDatabase ) {
		return self::fetchDocumentTemplateType( sprintf( 'SELECT * FROM document_template_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>