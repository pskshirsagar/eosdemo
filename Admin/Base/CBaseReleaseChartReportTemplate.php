<?php

class CBaseReleaseChartReportTemplate extends CEosSingularBase {

	const TABLE_NAME = 'public.release_chart_report_templates';

	protected $m_intId;
	protected $m_strName;
	protected $m_intReleaseChartTypeId;
	protected $m_intReleaseChartCategoriesId;
	protected $m_strChartDataset;
	protected $m_strChartParameters;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['release_chart_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReleaseChartTypeId', trim( $arrValues['release_chart_type_id'] ) ); elseif( isset( $arrValues['release_chart_type_id'] ) ) $this->setReleaseChartTypeId( $arrValues['release_chart_type_id'] );
		if( isset( $arrValues['release_chart_categories_id'] ) && $boolDirectSet ) $this->set( 'm_intReleaseChartCategoriesId', trim( $arrValues['release_chart_categories_id'] ) ); elseif( isset( $arrValues['release_chart_categories_id'] ) ) $this->setReleaseChartCategoriesId( $arrValues['release_chart_categories_id'] );
		if( isset( $arrValues['chart_dataset'] ) && $boolDirectSet ) $this->set( 'm_strChartDataset', trim( stripcslashes( $arrValues['chart_dataset'] ) ) ); elseif( isset( $arrValues['chart_dataset'] ) ) $this->setChartDataset( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['chart_dataset'] ) : $arrValues['chart_dataset'] );
		if( isset( $arrValues['chart_parameters'] ) && $boolDirectSet ) $this->set( 'm_strChartParameters', trim( stripcslashes( $arrValues['chart_parameters'] ) ) ); elseif( isset( $arrValues['chart_parameters'] ) ) $this->setChartParameters( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['chart_parameters'] ) : $arrValues['chart_parameters'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 200, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setReleaseChartTypeId( $intReleaseChartTypeId ) {
		$this->set( 'm_intReleaseChartTypeId', CStrings::strToIntDef( $intReleaseChartTypeId, NULL, false ) );
	}

	public function getReleaseChartTypeId() {
		return $this->m_intReleaseChartTypeId;
	}

	public function sqlReleaseChartTypeId() {
		return ( true == isset( $this->m_intReleaseChartTypeId ) ) ? ( string ) $this->m_intReleaseChartTypeId : 'NULL';
	}

	public function setReleaseChartCategoriesId( $intReleaseChartCategoriesId ) {
		$this->set( 'm_intReleaseChartCategoriesId', CStrings::strToIntDef( $intReleaseChartCategoriesId, NULL, false ) );
	}

	public function getReleaseChartCategoriesId() {
		return $this->m_intReleaseChartCategoriesId;
	}

	public function sqlReleaseChartCategoriesId() {
		return ( true == isset( $this->m_intReleaseChartCategoriesId ) ) ? ( string ) $this->m_intReleaseChartCategoriesId : 'NULL';
	}

	public function setChartDataset( $strChartDataset ) {
		$this->set( 'm_strChartDataset', CStrings::strTrimDef( $strChartDataset, 100, NULL, true ) );
	}

	public function getChartDataset() {
		return $this->m_strChartDataset;
	}

	public function sqlChartDataset() {
		return ( true == isset( $this->m_strChartDataset ) ) ? '\'' . addslashes( $this->m_strChartDataset ) . '\'' : 'NULL';
	}

	public function setChartParameters( $strChartParameters ) {
		$this->set( 'm_strChartParameters', CStrings::strTrimDef( $strChartParameters, -1, NULL, true ) );
	}

	public function getChartParameters() {
		return $this->m_strChartParameters;
	}

	public function sqlChartParameters() {
		return ( true == isset( $this->m_strChartParameters ) ) ? '\'' . addslashes( $this->m_strChartParameters ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, release_chart_type_id, release_chart_categories_id, chart_dataset, chart_parameters, created_by, created_on, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlReleaseChartTypeId() . ', ' .
 						$this->sqlReleaseChartCategoriesId() . ', ' .
 						$this->sqlChartDataset() . ', ' .
 						$this->sqlChartParameters() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_chart_type_id = ' . $this->sqlReleaseChartTypeId() . ','; } elseif( true == array_key_exists( 'ReleaseChartTypeId', $this->getChangedColumns() ) ) { $strSql .= ' release_chart_type_id = ' . $this->sqlReleaseChartTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_chart_categories_id = ' . $this->sqlReleaseChartCategoriesId() . ','; } elseif( true == array_key_exists( 'ReleaseChartCategoriesId', $this->getChangedColumns() ) ) { $strSql .= ' release_chart_categories_id = ' . $this->sqlReleaseChartCategoriesId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chart_dataset = ' . $this->sqlChartDataset() . ','; } elseif( true == array_key_exists( 'ChartDataset', $this->getChangedColumns() ) ) { $strSql .= ' chart_dataset = ' . $this->sqlChartDataset() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chart_parameters = ' . $this->sqlChartParameters() . ','; } elseif( true == array_key_exists( 'ChartParameters', $this->getChangedColumns() ) ) { $strSql .= ' chart_parameters = ' . $this->sqlChartParameters() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'release_chart_type_id' => $this->getReleaseChartTypeId(),
			'release_chart_categories_id' => $this->getReleaseChartCategoriesId(),
			'chart_dataset' => $this->getChartDataset(),
			'chart_parameters' => $this->getChartParameters(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>