<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemKeywordAssociations
 * Do not add any new functions to this class.
 */

class CBaseSemKeywordAssociations extends CEosPluralBase {

	/**
	 * @return CSemKeywordAssociation[]
	 */
	public static function fetchSemKeywordAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemKeywordAssociation', $objDatabase );
	}

	/**
	 * @return CSemKeywordAssociation
	 */
	public static function fetchSemKeywordAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemKeywordAssociation', $objDatabase );
	}

	public static function fetchSemKeywordAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_keyword_associations', $objDatabase );
	}

	public static function fetchSemKeywordAssociationById( $intId, $objDatabase ) {
		return self::fetchSemKeywordAssociation( sprintf( 'SELECT * FROM sem_keyword_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemKeywordAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchSemKeywordAssociations( sprintf( 'SELECT * FROM sem_keyword_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSemKeywordAssociationsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSemKeywordAssociations( sprintf( 'SELECT * FROM sem_keyword_associations WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchSemKeywordAssociationsBySemAccountId( $intSemAccountId, $objDatabase ) {
		return self::fetchSemKeywordAssociations( sprintf( 'SELECT * FROM sem_keyword_associations WHERE sem_account_id = %d', ( int ) $intSemAccountId ), $objDatabase );
	}

	public static function fetchSemKeywordAssociationsBySemCampaignId( $intSemCampaignId, $objDatabase ) {
		return self::fetchSemKeywordAssociations( sprintf( 'SELECT * FROM sem_keyword_associations WHERE sem_campaign_id = %d', ( int ) $intSemCampaignId ), $objDatabase );
	}

	public static function fetchSemKeywordAssociationsBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemKeywordAssociations( sprintf( 'SELECT * FROM sem_keyword_associations WHERE sem_ad_group_id = %d', ( int ) $intSemAdGroupId ), $objDatabase );
	}

	public static function fetchSemKeywordAssociationsBySemKeywordId( $intSemKeywordId, $objDatabase ) {
		return self::fetchSemKeywordAssociations( sprintf( 'SELECT * FROM sem_keyword_associations WHERE sem_keyword_id = %d', ( int ) $intSemKeywordId ), $objDatabase );
	}

}
?>