<?php

class CBaseSalesTaxTransactionDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.sales_tax_transaction_details';

	protected $m_intId;
	protected $m_intSalesTaxTransactionId;
	protected $m_strAuthorityName;
	protected $m_strAuthorityType;
	protected $m_strTaxName;
	protected $m_fltTaxApplied;
	protected $m_fltFeeApplied;
	protected $m_fltTaxableQuantity;
	protected $m_fltTaxableAmount;
	protected $m_fltExemptQuantity;
	protected $m_fltExemptAmount;
	protected $m_fltTaxRate;
	protected $m_strBaseType;
	protected $m_strPassFlag;
	protected $m_strPassType;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltTaxApplied = '0';
		$this->m_fltFeeApplied = '0';
		$this->m_fltTaxableQuantity = '0';
		$this->m_fltTaxableAmount = '0';
		$this->m_fltExemptQuantity = '0';
		$this->m_fltExemptAmount = '0';
		$this->m_fltTaxRate = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['sales_tax_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intSalesTaxTransactionId', trim( $arrValues['sales_tax_transaction_id'] ) ); elseif( isset( $arrValues['sales_tax_transaction_id'] ) ) $this->setSalesTaxTransactionId( $arrValues['sales_tax_transaction_id'] );
		if( isset( $arrValues['authority_name'] ) && $boolDirectSet ) $this->set( 'm_strAuthorityName', trim( stripcslashes( $arrValues['authority_name'] ) ) ); elseif( isset( $arrValues['authority_name'] ) ) $this->setAuthorityName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['authority_name'] ) : $arrValues['authority_name'] );
		if( isset( $arrValues['authority_type'] ) && $boolDirectSet ) $this->set( 'm_strAuthorityType', trim( stripcslashes( $arrValues['authority_type'] ) ) ); elseif( isset( $arrValues['authority_type'] ) ) $this->setAuthorityType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['authority_type'] ) : $arrValues['authority_type'] );
		if( isset( $arrValues['tax_name'] ) && $boolDirectSet ) $this->set( 'm_strTaxName', trim( stripcslashes( $arrValues['tax_name'] ) ) ); elseif( isset( $arrValues['tax_name'] ) ) $this->setTaxName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tax_name'] ) : $arrValues['tax_name'] );
		if( isset( $arrValues['tax_applied'] ) && $boolDirectSet ) $this->set( 'm_fltTaxApplied', trim( $arrValues['tax_applied'] ) ); elseif( isset( $arrValues['tax_applied'] ) ) $this->setTaxApplied( $arrValues['tax_applied'] );
		if( isset( $arrValues['fee_applied'] ) && $boolDirectSet ) $this->set( 'm_fltFeeApplied', trim( $arrValues['fee_applied'] ) ); elseif( isset( $arrValues['fee_applied'] ) ) $this->setFeeApplied( $arrValues['fee_applied'] );
		if( isset( $arrValues['taxable_quantity'] ) && $boolDirectSet ) $this->set( 'm_fltTaxableQuantity', trim( $arrValues['taxable_quantity'] ) ); elseif( isset( $arrValues['taxable_quantity'] ) ) $this->setTaxableQuantity( $arrValues['taxable_quantity'] );
		if( isset( $arrValues['taxable_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTaxableAmount', trim( $arrValues['taxable_amount'] ) ); elseif( isset( $arrValues['taxable_amount'] ) ) $this->setTaxableAmount( $arrValues['taxable_amount'] );
		if( isset( $arrValues['exempt_quantity'] ) && $boolDirectSet ) $this->set( 'm_fltExemptQuantity', trim( $arrValues['exempt_quantity'] ) ); elseif( isset( $arrValues['exempt_quantity'] ) ) $this->setExemptQuantity( $arrValues['exempt_quantity'] );
		if( isset( $arrValues['exempt_amount'] ) && $boolDirectSet ) $this->set( 'm_fltExemptAmount', trim( $arrValues['exempt_amount'] ) ); elseif( isset( $arrValues['exempt_amount'] ) ) $this->setExemptAmount( $arrValues['exempt_amount'] );
		if( isset( $arrValues['tax_rate'] ) && $boolDirectSet ) $this->set( 'm_fltTaxRate', trim( $arrValues['tax_rate'] ) ); elseif( isset( $arrValues['tax_rate'] ) ) $this->setTaxRate( $arrValues['tax_rate'] );
		if( isset( $arrValues['base_type'] ) && $boolDirectSet ) $this->set( 'm_strBaseType', trim( stripcslashes( $arrValues['base_type'] ) ) ); elseif( isset( $arrValues['base_type'] ) ) $this->setBaseType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['base_type'] ) : $arrValues['base_type'] );
		if( isset( $arrValues['pass_flag'] ) && $boolDirectSet ) $this->set( 'm_strPassFlag', trim( stripcslashes( $arrValues['pass_flag'] ) ) ); elseif( isset( $arrValues['pass_flag'] ) ) $this->setPassFlag( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pass_flag'] ) : $arrValues['pass_flag'] );
		if( isset( $arrValues['pass_type'] ) && $boolDirectSet ) $this->set( 'm_strPassType', trim( stripcslashes( $arrValues['pass_type'] ) ) ); elseif( isset( $arrValues['pass_type'] ) ) $this->setPassType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pass_type'] ) : $arrValues['pass_type'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSalesTaxTransactionId( $intSalesTaxTransactionId ) {
		$this->set( 'm_intSalesTaxTransactionId', CStrings::strToIntDef( $intSalesTaxTransactionId, NULL, false ) );
	}

	public function getSalesTaxTransactionId() {
		return $this->m_intSalesTaxTransactionId;
	}

	public function sqlSalesTaxTransactionId() {
		return ( true == isset( $this->m_intSalesTaxTransactionId ) ) ? ( string ) $this->m_intSalesTaxTransactionId : 'NULL';
	}

	public function setAuthorityName( $strAuthorityName ) {
		$this->set( 'm_strAuthorityName', CStrings::strTrimDef( $strAuthorityName, 200, NULL, true ) );
	}

	public function getAuthorityName() {
		return $this->m_strAuthorityName;
	}

	public function sqlAuthorityName() {
		return ( true == isset( $this->m_strAuthorityName ) ) ? '\'' . addslashes( $this->m_strAuthorityName ) . '\'' : 'NULL';
	}

	public function setAuthorityType( $strAuthorityType ) {
		$this->set( 'm_strAuthorityType', CStrings::strTrimDef( $strAuthorityType, 10, NULL, true ) );
	}

	public function getAuthorityType() {
		return $this->m_strAuthorityType;
	}

	public function sqlAuthorityType() {
		return ( true == isset( $this->m_strAuthorityType ) ) ? '\'' . addslashes( $this->m_strAuthorityType ) . '\'' : 'NULL';
	}

	public function setTaxName( $strTaxName ) {
		$this->set( 'm_strTaxName', CStrings::strTrimDef( $strTaxName, 200, NULL, true ) );
	}

	public function getTaxName() {
		return $this->m_strTaxName;
	}

	public function sqlTaxName() {
		return ( true == isset( $this->m_strTaxName ) ) ? '\'' . addslashes( $this->m_strTaxName ) . '\'' : 'NULL';
	}

	public function setTaxApplied( $fltTaxApplied ) {
		$this->set( 'm_fltTaxApplied', CStrings::strToFloatDef( $fltTaxApplied, NULL, false, 4 ) );
	}

	public function getTaxApplied() {
		return $this->m_fltTaxApplied;
	}

	public function sqlTaxApplied() {
		return ( true == isset( $this->m_fltTaxApplied ) ) ? ( string ) $this->m_fltTaxApplied : '0';
	}

	public function setFeeApplied( $fltFeeApplied ) {
		$this->set( 'm_fltFeeApplied', CStrings::strToFloatDef( $fltFeeApplied, NULL, false, 4 ) );
	}

	public function getFeeApplied() {
		return $this->m_fltFeeApplied;
	}

	public function sqlFeeApplied() {
		return ( true == isset( $this->m_fltFeeApplied ) ) ? ( string ) $this->m_fltFeeApplied : '0';
	}

	public function setTaxableQuantity( $fltTaxableQuantity ) {
		$this->set( 'm_fltTaxableQuantity', CStrings::strToFloatDef( $fltTaxableQuantity, NULL, false, 4 ) );
	}

	public function getTaxableQuantity() {
		return $this->m_fltTaxableQuantity;
	}

	public function sqlTaxableQuantity() {
		return ( true == isset( $this->m_fltTaxableQuantity ) ) ? ( string ) $this->m_fltTaxableQuantity : '0';
	}

	public function setTaxableAmount( $fltTaxableAmount ) {
		$this->set( 'm_fltTaxableAmount', CStrings::strToFloatDef( $fltTaxableAmount, NULL, false, 4 ) );
	}

	public function getTaxableAmount() {
		return $this->m_fltTaxableAmount;
	}

	public function sqlTaxableAmount() {
		return ( true == isset( $this->m_fltTaxableAmount ) ) ? ( string ) $this->m_fltTaxableAmount : '0';
	}

	public function setExemptQuantity( $fltExemptQuantity ) {
		$this->set( 'm_fltExemptQuantity', CStrings::strToFloatDef( $fltExemptQuantity, NULL, false, 4 ) );
	}

	public function getExemptQuantity() {
		return $this->m_fltExemptQuantity;
	}

	public function sqlExemptQuantity() {
		return ( true == isset( $this->m_fltExemptQuantity ) ) ? ( string ) $this->m_fltExemptQuantity : '0';
	}

	public function setExemptAmount( $fltExemptAmount ) {
		$this->set( 'm_fltExemptAmount', CStrings::strToFloatDef( $fltExemptAmount, NULL, false, 4 ) );
	}

	public function getExemptAmount() {
		return $this->m_fltExemptAmount;
	}

	public function sqlExemptAmount() {
		return ( true == isset( $this->m_fltExemptAmount ) ) ? ( string ) $this->m_fltExemptAmount : '0';
	}

	public function setTaxRate( $fltTaxRate ) {
		$this->set( 'm_fltTaxRate', CStrings::strToFloatDef( $fltTaxRate, NULL, false, 4 ) );
	}

	public function getTaxRate() {
		return $this->m_fltTaxRate;
	}

	public function sqlTaxRate() {
		return ( true == isset( $this->m_fltTaxRate ) ) ? ( string ) $this->m_fltTaxRate : '0';
	}

	public function setBaseType( $strBaseType ) {
		$this->set( 'm_strBaseType', CStrings::strTrimDef( $strBaseType, 200, NULL, true ) );
	}

	public function getBaseType() {
		return $this->m_strBaseType;
	}

	public function sqlBaseType() {
		return ( true == isset( $this->m_strBaseType ) ) ? '\'' . addslashes( $this->m_strBaseType ) . '\'' : 'NULL';
	}

	public function setPassFlag( $strPassFlag ) {
		$this->set( 'm_strPassFlag', CStrings::strTrimDef( $strPassFlag, 200, NULL, true ) );
	}

	public function getPassFlag() {
		return $this->m_strPassFlag;
	}

	public function sqlPassFlag() {
		return ( true == isset( $this->m_strPassFlag ) ) ? '\'' . addslashes( $this->m_strPassFlag ) . '\'' : 'NULL';
	}

	public function setPassType( $strPassType ) {
		$this->set( 'm_strPassType', CStrings::strTrimDef( $strPassType, 200, NULL, true ) );
	}

	public function getPassType() {
		return $this->m_strPassType;
	}

	public function sqlPassType() {
		return ( true == isset( $this->m_strPassType ) ) ? '\'' . addslashes( $this->m_strPassType ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, sales_tax_transaction_id, authority_name, authority_type, tax_name, tax_applied, fee_applied, taxable_quantity, taxable_amount, exempt_quantity, exempt_amount, tax_rate, base_type, pass_flag, pass_type, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSalesTaxTransactionId() . ', ' .
 						$this->sqlAuthorityName() . ', ' .
 						$this->sqlAuthorityType() . ', ' .
 						$this->sqlTaxName() . ', ' .
 						$this->sqlTaxApplied() . ', ' .
 						$this->sqlFeeApplied() . ', ' .
 						$this->sqlTaxableQuantity() . ', ' .
 						$this->sqlTaxableAmount() . ', ' .
 						$this->sqlExemptQuantity() . ', ' .
 						$this->sqlExemptAmount() . ', ' .
 						$this->sqlTaxRate() . ', ' .
 						$this->sqlBaseType() . ', ' .
 						$this->sqlPassFlag() . ', ' .
 						$this->sqlPassType() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_tax_transaction_id = ' . $this->sqlSalesTaxTransactionId() . ','; } elseif( true == array_key_exists( 'SalesTaxTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' sales_tax_transaction_id = ' . $this->sqlSalesTaxTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' authority_name = ' . $this->sqlAuthorityName() . ','; } elseif( true == array_key_exists( 'AuthorityName', $this->getChangedColumns() ) ) { $strSql .= ' authority_name = ' . $this->sqlAuthorityName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' authority_type = ' . $this->sqlAuthorityType() . ','; } elseif( true == array_key_exists( 'AuthorityType', $this->getChangedColumns() ) ) { $strSql .= ' authority_type = ' . $this->sqlAuthorityType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_name = ' . $this->sqlTaxName() . ','; } elseif( true == array_key_exists( 'TaxName', $this->getChangedColumns() ) ) { $strSql .= ' tax_name = ' . $this->sqlTaxName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_applied = ' . $this->sqlTaxApplied() . ','; } elseif( true == array_key_exists( 'TaxApplied', $this->getChangedColumns() ) ) { $strSql .= ' tax_applied = ' . $this->sqlTaxApplied() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fee_applied = ' . $this->sqlFeeApplied() . ','; } elseif( true == array_key_exists( 'FeeApplied', $this->getChangedColumns() ) ) { $strSql .= ' fee_applied = ' . $this->sqlFeeApplied() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' taxable_quantity = ' . $this->sqlTaxableQuantity() . ','; } elseif( true == array_key_exists( 'TaxableQuantity', $this->getChangedColumns() ) ) { $strSql .= ' taxable_quantity = ' . $this->sqlTaxableQuantity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' taxable_amount = ' . $this->sqlTaxableAmount() . ','; } elseif( true == array_key_exists( 'TaxableAmount', $this->getChangedColumns() ) ) { $strSql .= ' taxable_amount = ' . $this->sqlTaxableAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exempt_quantity = ' . $this->sqlExemptQuantity() . ','; } elseif( true == array_key_exists( 'ExemptQuantity', $this->getChangedColumns() ) ) { $strSql .= ' exempt_quantity = ' . $this->sqlExemptQuantity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exempt_amount = ' . $this->sqlExemptAmount() . ','; } elseif( true == array_key_exists( 'ExemptAmount', $this->getChangedColumns() ) ) { $strSql .= ' exempt_amount = ' . $this->sqlExemptAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tax_rate = ' . $this->sqlTaxRate() . ','; } elseif( true == array_key_exists( 'TaxRate', $this->getChangedColumns() ) ) { $strSql .= ' tax_rate = ' . $this->sqlTaxRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_type = ' . $this->sqlBaseType() . ','; } elseif( true == array_key_exists( 'BaseType', $this->getChangedColumns() ) ) { $strSql .= ' base_type = ' . $this->sqlBaseType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pass_flag = ' . $this->sqlPassFlag() . ','; } elseif( true == array_key_exists( 'PassFlag', $this->getChangedColumns() ) ) { $strSql .= ' pass_flag = ' . $this->sqlPassFlag() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pass_type = ' . $this->sqlPassType() . ','; } elseif( true == array_key_exists( 'PassType', $this->getChangedColumns() ) ) { $strSql .= ' pass_type = ' . $this->sqlPassType() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'sales_tax_transaction_id' => $this->getSalesTaxTransactionId(),
			'authority_name' => $this->getAuthorityName(),
			'authority_type' => $this->getAuthorityType(),
			'tax_name' => $this->getTaxName(),
			'tax_applied' => $this->getTaxApplied(),
			'fee_applied' => $this->getFeeApplied(),
			'taxable_quantity' => $this->getTaxableQuantity(),
			'taxable_amount' => $this->getTaxableAmount(),
			'exempt_quantity' => $this->getExemptQuantity(),
			'exempt_amount' => $this->getExemptAmount(),
			'tax_rate' => $this->getTaxRate(),
			'base_type' => $this->getBaseType(),
			'pass_flag' => $this->getPassFlag(),
			'pass_type' => $this->getPassType(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>