<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTypes
 * Do not add any new functions to this class.
 */

class CBaseSurveyTypes extends CEosPluralBase {

	/**
	 * @return CSurveyType[]
	 */
	public static function fetchSurveyTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyType', $objDatabase );
	}

	/**
	 * @return CSurveyType
	 */
	public static function fetchSurveyType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyType', $objDatabase );
	}

	public static function fetchSurveyTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_types', $objDatabase );
	}

	public static function fetchSurveyTypeById( $intId, $objDatabase ) {
		return self::fetchSurveyType( sprintf( 'SELECT * FROM survey_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>