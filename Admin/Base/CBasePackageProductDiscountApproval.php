<?php

class CBasePackageProductDiscountApproval extends CEosSingularBase {

	const TABLE_NAME = 'public.package_product_discount_approvals';

	protected $m_intId;
	protected $m_intContractId;
	protected $m_intPackageProductId;
	protected $m_intPsProductId;
	protected $m_intBundlePsProductId;
	protected $m_intApproverEmployeeId;
	protected $m_fltSetupDiscountPercentage;
	protected $m_fltRecurringDiscountPercentage;
	protected $m_strReasonForDiscount;
	protected $m_strReasonForDeny;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeniedBy;
	protected $m_strDeniedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intDeletedBy;
	protected $m_strNote;
	protected $m_strDeletedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltSetupDiscountPercentage = '0';
		$this->m_fltRecurringDiscountPercentage = '0';
		$this->m_strCreatedOn = 'now()';
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['package_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPackageProductId', trim( $arrValues['package_product_id'] ) ); elseif( isset( $arrValues['package_product_id'] ) ) $this->setPackageProductId( $arrValues['package_product_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['bundle_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intBundlePsProductId', trim( $arrValues['bundle_ps_product_id'] ) ); elseif( isset( $arrValues['bundle_ps_product_id'] ) ) $this->setBundlePsProductId( $arrValues['bundle_ps_product_id'] );
		if( isset( $arrValues['approver_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intApproverEmployeeId', trim( $arrValues['approver_employee_id'] ) ); elseif( isset( $arrValues['approver_employee_id'] ) ) $this->setApproverEmployeeId( $arrValues['approver_employee_id'] );
		if( isset( $arrValues['setup_discount_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltSetupDiscountPercentage', trim( $arrValues['setup_discount_percentage'] ) ); elseif( isset( $arrValues['setup_discount_percentage'] ) ) $this->setSetupDiscountPercentage( $arrValues['setup_discount_percentage'] );
		if( isset( $arrValues['recurring_discount_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltRecurringDiscountPercentage', trim( $arrValues['recurring_discount_percentage'] ) ); elseif( isset( $arrValues['recurring_discount_percentage'] ) ) $this->setRecurringDiscountPercentage( $arrValues['recurring_discount_percentage'] );
		if( isset( $arrValues['reason_for_discount'] ) && $boolDirectSet ) $this->set( 'm_strReasonForDiscount', trim( $arrValues['reason_for_discount'] ) ); elseif( isset( $arrValues['reason_for_discount'] ) ) $this->setReasonForDiscount( $arrValues['reason_for_discount'] );
		if( isset( $arrValues['reason_for_deny'] ) && $boolDirectSet ) $this->set( 'm_strReasonForDeny', trim( $arrValues['reason_for_deny'] ) ); elseif( isset( $arrValues['reason_for_deny'] ) ) $this->setReasonForDeny( $arrValues['reason_for_deny'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['denied_by'] ) && $boolDirectSet ) $this->set( 'm_intDeniedBy', trim( $arrValues['denied_by'] ) ); elseif( isset( $arrValues['denied_by'] ) ) $this->setDeniedBy( $arrValues['denied_by'] );
		if( isset( $arrValues['denied_on'] ) && $boolDirectSet ) $this->set( 'm_strDeniedOn', trim( $arrValues['denied_on'] ) ); elseif( isset( $arrValues['denied_on'] ) ) $this->setDeniedOn( $arrValues['denied_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( $arrValues['note'] ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( $arrValues['note'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setPackageProductId( $intPackageProductId ) {
		$this->set( 'm_intPackageProductId', CStrings::strToIntDef( $intPackageProductId, NULL, false ) );
	}

	public function getPackageProductId() {
		return $this->m_intPackageProductId;
	}

	public function sqlPackageProductId() {
		return ( true == isset( $this->m_intPackageProductId ) ) ? ( string ) $this->m_intPackageProductId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setBundlePsProductId( $intBundlePsProductId ) {
		$this->set( 'm_intBundlePsProductId', CStrings::strToIntDef( $intBundlePsProductId, NULL, false ) );
	}

	public function getBundlePsProductId() {
		return $this->m_intBundlePsProductId;
	}

	public function sqlBundlePsProductId() {
		return ( true == isset( $this->m_intBundlePsProductId ) ) ? ( string ) $this->m_intBundlePsProductId : 'NULL';
	}

	public function setApproverEmployeeId( $intApproverEmployeeId ) {
		$this->set( 'm_intApproverEmployeeId', CStrings::strToIntDef( $intApproverEmployeeId, NULL, false ) );
	}

	public function getApproverEmployeeId() {
		return $this->m_intApproverEmployeeId;
	}

	public function sqlApproverEmployeeId() {
		return ( true == isset( $this->m_intApproverEmployeeId ) ) ? ( string ) $this->m_intApproverEmployeeId : 'NULL';
	}

	public function setSetupDiscountPercentage( $fltSetupDiscountPercentage ) {
		$this->set( 'm_fltSetupDiscountPercentage', CStrings::strToFloatDef( $fltSetupDiscountPercentage, NULL, false, 2 ) );
	}

	public function getSetupDiscountPercentage() {
		return $this->m_fltSetupDiscountPercentage;
	}

	public function sqlSetupDiscountPercentage() {
		return ( true == isset( $this->m_fltSetupDiscountPercentage ) ) ? ( string ) $this->m_fltSetupDiscountPercentage : '0';
	}

	public function setRecurringDiscountPercentage( $fltRecurringDiscountPercentage ) {
		$this->set( 'm_fltRecurringDiscountPercentage', CStrings::strToFloatDef( $fltRecurringDiscountPercentage, NULL, false, 2 ) );
	}

	public function getRecurringDiscountPercentage() {
		return $this->m_fltRecurringDiscountPercentage;
	}

	public function sqlRecurringDiscountPercentage() {
		return ( true == isset( $this->m_fltRecurringDiscountPercentage ) ) ? ( string ) $this->m_fltRecurringDiscountPercentage : '0';
	}

	public function setReasonForDiscount( $strReasonForDiscount ) {
		$this->set( 'm_strReasonForDiscount', CStrings::strTrimDef( $strReasonForDiscount, -1, NULL, true ) );
	}

	public function getReasonForDiscount() {
		return $this->m_strReasonForDiscount;
	}

	public function sqlReasonForDiscount() {
		return ( true == isset( $this->m_strReasonForDiscount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReasonForDiscount ) : '\'' . addslashes( $this->m_strReasonForDiscount ) . '\'' ) : 'NULL';
	}

	public function setReasonForDeny( $strReasonForDeny ) {
		$this->set( 'm_strReasonForDeny', CStrings::strTrimDef( $strReasonForDeny, -1, NULL, true ) );
	}

	public function getReasonForDeny() {
		return $this->m_strReasonForDeny;
	}

	public function sqlReasonForDeny() {
		return ( true == isset( $this->m_strReasonForDeny ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReasonForDeny ) : '\'' . addslashes( $this->m_strReasonForDeny ) . '\'' ) : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeniedBy( $intDeniedBy ) {
		$this->set( 'm_intDeniedBy', CStrings::strToIntDef( $intDeniedBy, NULL, false ) );
	}

	public function getDeniedBy() {
		return $this->m_intDeniedBy;
	}

	public function sqlDeniedBy() {
		return ( true == isset( $this->m_intDeniedBy ) ) ? ( string ) $this->m_intDeniedBy : 'NULL';
	}

	public function setDeniedOn( $strDeniedOn ) {
		$this->set( 'm_strDeniedOn', CStrings::strTrimDef( $strDeniedOn, -1, NULL, true ) );
	}

	public function getDeniedOn() {
		return $this->m_strDeniedOn;
	}

	public function sqlDeniedOn() {
		return ( true == isset( $this->m_strDeniedOn ) ) ? '\'' . $this->m_strDeniedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, -1, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNote ) : '\'' . addslashes( $this->m_strNote ) . '\'' ) : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, contract_id, package_product_id, ps_product_id, bundle_ps_product_id, approver_employee_id, setup_discount_percentage, recurring_discount_percentage, reason_for_discount, reason_for_deny, approved_by, approved_on, denied_by, denied_on, created_by, created_on, updated_by, updated_on, deleted_by, note, deleted_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlContractId() . ', ' .
						$this->sqlPackageProductId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlBundlePsProductId() . ', ' .
						$this->sqlApproverEmployeeId() . ', ' .
						$this->sqlSetupDiscountPercentage() . ', ' .
						$this->sqlRecurringDiscountPercentage() . ', ' .
						$this->sqlReasonForDiscount() . ', ' .
						$this->sqlReasonForDeny() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlDeniedBy() . ', ' .
						$this->sqlDeniedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlNote() . ', ' .
						$this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId(). ',' ; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' package_product_id = ' . $this->sqlPackageProductId(). ',' ; } elseif( true == array_key_exists( 'PackageProductId', $this->getChangedColumns() ) ) { $strSql .= ' package_product_id = ' . $this->sqlPackageProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bundle_ps_product_id = ' . $this->sqlBundlePsProductId(). ',' ; } elseif( true == array_key_exists( 'BundlePsProductId', $this->getChangedColumns() ) ) { $strSql .= ' bundle_ps_product_id = ' . $this->sqlBundlePsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approver_employee_id = ' . $this->sqlApproverEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ApproverEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' approver_employee_id = ' . $this->sqlApproverEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' setup_discount_percentage = ' . $this->sqlSetupDiscountPercentage(). ',' ; } elseif( true == array_key_exists( 'SetupDiscountPercentage', $this->getChangedColumns() ) ) { $strSql .= ' setup_discount_percentage = ' . $this->sqlSetupDiscountPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recurring_discount_percentage = ' . $this->sqlRecurringDiscountPercentage(). ',' ; } elseif( true == array_key_exists( 'RecurringDiscountPercentage', $this->getChangedColumns() ) ) { $strSql .= ' recurring_discount_percentage = ' . $this->sqlRecurringDiscountPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_for_discount = ' . $this->sqlReasonForDiscount(). ',' ; } elseif( true == array_key_exists( 'ReasonForDiscount', $this->getChangedColumns() ) ) { $strSql .= ' reason_for_discount = ' . $this->sqlReasonForDiscount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_for_deny = ' . $this->sqlReasonForDeny(). ',' ; } elseif( true == array_key_exists( 'ReasonForDeny', $this->getChangedColumns() ) ) { $strSql .= ' reason_for_deny = ' . $this->sqlReasonForDeny() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy(). ',' ; } elseif( true == array_key_exists( 'DeniedBy', $this->getChangedColumns() ) ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn(). ',' ; } elseif( true == array_key_exists( 'DeniedOn', $this->getChangedColumns() ) ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote(). ',' ; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'contract_id' => $this->getContractId(),
			'package_product_id' => $this->getPackageProductId(),
			'ps_product_id' => $this->getPsProductId(),
			'bundle_ps_product_id' => $this->getBundlePsProductId(),
			'approver_employee_id' => $this->getApproverEmployeeId(),
			'setup_discount_percentage' => $this->getSetupDiscountPercentage(),
			'recurring_discount_percentage' => $this->getRecurringDiscountPercentage(),
			'reason_for_discount' => $this->getReasonForDiscount(),
			'reason_for_deny' => $this->getReasonForDeny(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'denied_by' => $this->getDeniedBy(),
			'denied_on' => $this->getDeniedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'note' => $this->getNote(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>