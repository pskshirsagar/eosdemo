<?php

class CBaseSemTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.sem_transactions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSemAdGroupId;
	protected $m_intSemSourceId;
	protected $m_intSemKeywordId;
	protected $m_intCompanyPaymentId;
	protected $m_intSemBudgetId;
	protected $m_strRemotePrimaryKey;
	protected $m_strTransactionDatetime;
	protected $m_fltCostPerClick;
	protected $m_fltEstimatedAmount;
	protected $m_fltTransactionAmount;
	protected $m_strIpAddress;
	protected $m_strReferringUrl;
	protected $m_intIsInternal;
	protected $m_intIsAdjustment;
	protected $m_intIsReconciled;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsInternal = '0';
		$this->m_intIsAdjustment = '0';
		$this->m_intIsReconciled = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['sem_ad_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdGroupId', trim( $arrValues['sem_ad_group_id'] ) ); elseif( isset( $arrValues['sem_ad_group_id'] ) ) $this->setSemAdGroupId( $arrValues['sem_ad_group_id'] );
		if( isset( $arrValues['sem_source_id'] ) && $boolDirectSet ) $this->set( 'm_intSemSourceId', trim( $arrValues['sem_source_id'] ) ); elseif( isset( $arrValues['sem_source_id'] ) ) $this->setSemSourceId( $arrValues['sem_source_id'] );
		if( isset( $arrValues['sem_keyword_id'] ) && $boolDirectSet ) $this->set( 'm_intSemKeywordId', trim( $arrValues['sem_keyword_id'] ) ); elseif( isset( $arrValues['sem_keyword_id'] ) ) $this->setSemKeywordId( $arrValues['sem_keyword_id'] );
		if( isset( $arrValues['company_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyPaymentId', trim( $arrValues['company_payment_id'] ) ); elseif( isset( $arrValues['company_payment_id'] ) ) $this->setCompanyPaymentId( $arrValues['company_payment_id'] );
		if( isset( $arrValues['sem_budget_id'] ) && $boolDirectSet ) $this->set( 'm_intSemBudgetId', trim( $arrValues['sem_budget_id'] ) ); elseif( isset( $arrValues['sem_budget_id'] ) ) $this->setSemBudgetId( $arrValues['sem_budget_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['cost_per_click'] ) && $boolDirectSet ) $this->set( 'm_fltCostPerClick', trim( $arrValues['cost_per_click'] ) ); elseif( isset( $arrValues['cost_per_click'] ) ) $this->setCostPerClick( $arrValues['cost_per_click'] );
		if( isset( $arrValues['estimated_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEstimatedAmount', trim( $arrValues['estimated_amount'] ) ); elseif( isset( $arrValues['estimated_amount'] ) ) $this->setEstimatedAmount( $arrValues['estimated_amount'] );
		if( isset( $arrValues['transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmount', trim( $arrValues['transaction_amount'] ) ); elseif( isset( $arrValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrValues['transaction_amount'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['referring_url'] ) && $boolDirectSet ) $this->set( 'm_strReferringUrl', trim( stripcslashes( $arrValues['referring_url'] ) ) ); elseif( isset( $arrValues['referring_url'] ) ) $this->setReferringUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['referring_url'] ) : $arrValues['referring_url'] );
		if( isset( $arrValues['is_internal'] ) && $boolDirectSet ) $this->set( 'm_intIsInternal', trim( $arrValues['is_internal'] ) ); elseif( isset( $arrValues['is_internal'] ) ) $this->setIsInternal( $arrValues['is_internal'] );
		if( isset( $arrValues['is_adjustment'] ) && $boolDirectSet ) $this->set( 'm_intIsAdjustment', trim( $arrValues['is_adjustment'] ) ); elseif( isset( $arrValues['is_adjustment'] ) ) $this->setIsAdjustment( $arrValues['is_adjustment'] );
		if( isset( $arrValues['is_reconciled'] ) && $boolDirectSet ) $this->set( 'm_intIsReconciled', trim( $arrValues['is_reconciled'] ) ); elseif( isset( $arrValues['is_reconciled'] ) ) $this->setIsReconciled( $arrValues['is_reconciled'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSemAdGroupId( $intSemAdGroupId ) {
		$this->set( 'm_intSemAdGroupId', CStrings::strToIntDef( $intSemAdGroupId, NULL, false ) );
	}

	public function getSemAdGroupId() {
		return $this->m_intSemAdGroupId;
	}

	public function sqlSemAdGroupId() {
		return ( true == isset( $this->m_intSemAdGroupId ) ) ? ( string ) $this->m_intSemAdGroupId : 'NULL';
	}

	public function setSemSourceId( $intSemSourceId ) {
		$this->set( 'm_intSemSourceId', CStrings::strToIntDef( $intSemSourceId, NULL, false ) );
	}

	public function getSemSourceId() {
		return $this->m_intSemSourceId;
	}

	public function sqlSemSourceId() {
		return ( true == isset( $this->m_intSemSourceId ) ) ? ( string ) $this->m_intSemSourceId : 'NULL';
	}

	public function setSemKeywordId( $intSemKeywordId ) {
		$this->set( 'm_intSemKeywordId', CStrings::strToIntDef( $intSemKeywordId, NULL, false ) );
	}

	public function getSemKeywordId() {
		return $this->m_intSemKeywordId;
	}

	public function sqlSemKeywordId() {
		return ( true == isset( $this->m_intSemKeywordId ) ) ? ( string ) $this->m_intSemKeywordId : 'NULL';
	}

	public function setCompanyPaymentId( $intCompanyPaymentId ) {
		$this->set( 'm_intCompanyPaymentId', CStrings::strToIntDef( $intCompanyPaymentId, NULL, false ) );
	}

	public function getCompanyPaymentId() {
		return $this->m_intCompanyPaymentId;
	}

	public function sqlCompanyPaymentId() {
		return ( true == isset( $this->m_intCompanyPaymentId ) ) ? ( string ) $this->m_intCompanyPaymentId : 'NULL';
	}

	public function setSemBudgetId( $intSemBudgetId ) {
		$this->set( 'm_intSemBudgetId', CStrings::strToIntDef( $intSemBudgetId, NULL, false ) );
	}

	public function getSemBudgetId() {
		return $this->m_intSemBudgetId;
	}

	public function sqlSemBudgetId() {
		return ( true == isset( $this->m_intSemBudgetId ) ) ? ( string ) $this->m_intSemBudgetId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setCostPerClick( $fltCostPerClick ) {
		$this->set( 'm_fltCostPerClick', CStrings::strToFloatDef( $fltCostPerClick, NULL, false, 2 ) );
	}

	public function getCostPerClick() {
		return $this->m_fltCostPerClick;
	}

	public function sqlCostPerClick() {
		return ( true == isset( $this->m_fltCostPerClick ) ) ? ( string ) $this->m_fltCostPerClick : 'NULL';
	}

	public function setEstimatedAmount( $fltEstimatedAmount ) {
		$this->set( 'm_fltEstimatedAmount', CStrings::strToFloatDef( $fltEstimatedAmount, NULL, false, 2 ) );
	}

	public function getEstimatedAmount() {
		return $this->m_fltEstimatedAmount;
	}

	public function sqlEstimatedAmount() {
		return ( true == isset( $this->m_fltEstimatedAmount ) ) ? ( string ) $this->m_fltEstimatedAmount : 'NULL';
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->set( 'm_fltTransactionAmount', CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 2 ) );
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function sqlTransactionAmount() {
		return ( true == isset( $this->m_fltTransactionAmount ) ) ? ( string ) $this->m_fltTransactionAmount : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 23, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
	}

	public function setReferringUrl( $strReferringUrl ) {
		$this->set( 'm_strReferringUrl', CStrings::strTrimDef( $strReferringUrl, 4096, NULL, true ) );
	}

	public function getReferringUrl() {
		return $this->m_strReferringUrl;
	}

	public function sqlReferringUrl() {
		return ( true == isset( $this->m_strReferringUrl ) ) ? '\'' . addslashes( $this->m_strReferringUrl ) . '\'' : 'NULL';
	}

	public function setIsInternal( $intIsInternal ) {
		$this->set( 'm_intIsInternal', CStrings::strToIntDef( $intIsInternal, NULL, false ) );
	}

	public function getIsInternal() {
		return $this->m_intIsInternal;
	}

	public function sqlIsInternal() {
		return ( true == isset( $this->m_intIsInternal ) ) ? ( string ) $this->m_intIsInternal : '0';
	}

	public function setIsAdjustment( $intIsAdjustment ) {
		$this->set( 'm_intIsAdjustment', CStrings::strToIntDef( $intIsAdjustment, NULL, false ) );
	}

	public function getIsAdjustment() {
		return $this->m_intIsAdjustment;
	}

	public function sqlIsAdjustment() {
		return ( true == isset( $this->m_intIsAdjustment ) ) ? ( string ) $this->m_intIsAdjustment : '0';
	}

	public function setIsReconciled( $intIsReconciled ) {
		$this->set( 'm_intIsReconciled', CStrings::strToIntDef( $intIsReconciled, NULL, false ) );
	}

	public function getIsReconciled() {
		return $this->m_intIsReconciled;
	}

	public function sqlIsReconciled() {
		return ( true == isset( $this->m_intIsReconciled ) ) ? ( string ) $this->m_intIsReconciled : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, sem_ad_group_id, sem_source_id, sem_keyword_id, company_payment_id, sem_budget_id, remote_primary_key, transaction_datetime, cost_per_click, estimated_amount, transaction_amount, ip_address, referring_url, is_internal, is_adjustment, is_reconciled, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSemAdGroupId() . ', ' .
 						$this->sqlSemSourceId() . ', ' .
 						$this->sqlSemKeywordId() . ', ' .
 						$this->sqlCompanyPaymentId() . ', ' .
 						$this->sqlSemBudgetId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlTransactionDatetime() . ', ' .
 						$this->sqlCostPerClick() . ', ' .
 						$this->sqlEstimatedAmount() . ', ' .
 						$this->sqlTransactionAmount() . ', ' .
 						$this->sqlIpAddress() . ', ' .
 						$this->sqlReferringUrl() . ', ' .
 						$this->sqlIsInternal() . ', ' .
 						$this->sqlIsAdjustment() . ', ' .
 						$this->sqlIsReconciled() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; } elseif( true == array_key_exists( 'SemAdGroupId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; } elseif( true == array_key_exists( 'SemSourceId', $this->getChangedColumns() ) ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId() . ','; } elseif( true == array_key_exists( 'SemKeywordId', $this->getChangedColumns() ) ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; } elseif( true == array_key_exists( 'CompanyPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_budget_id = ' . $this->sqlSemBudgetId() . ','; } elseif( true == array_key_exists( 'SemBudgetId', $this->getChangedColumns() ) ) { $strSql .= ' sem_budget_id = ' . $this->sqlSemBudgetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cost_per_click = ' . $this->sqlCostPerClick() . ','; } elseif( true == array_key_exists( 'CostPerClick', $this->getChangedColumns() ) ) { $strSql .= ' cost_per_click = ' . $this->sqlCostPerClick() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimated_amount = ' . $this->sqlEstimatedAmount() . ','; } elseif( true == array_key_exists( 'EstimatedAmount', $this->getChangedColumns() ) ) { $strSql .= ' estimated_amount = ' . $this->sqlEstimatedAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; } elseif( true == array_key_exists( 'TransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' referring_url = ' . $this->sqlReferringUrl() . ','; } elseif( true == array_key_exists( 'ReferringUrl', $this->getChangedColumns() ) ) { $strSql .= ' referring_url = ' . $this->sqlReferringUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_internal = ' . $this->sqlIsInternal() . ','; } elseif( true == array_key_exists( 'IsInternal', $this->getChangedColumns() ) ) { $strSql .= ' is_internal = ' . $this->sqlIsInternal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_adjustment = ' . $this->sqlIsAdjustment() . ','; } elseif( true == array_key_exists( 'IsAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' is_adjustment = ' . $this->sqlIsAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reconciled = ' . $this->sqlIsReconciled() . ','; } elseif( true == array_key_exists( 'IsReconciled', $this->getChangedColumns() ) ) { $strSql .= ' is_reconciled = ' . $this->sqlIsReconciled() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'sem_ad_group_id' => $this->getSemAdGroupId(),
			'sem_source_id' => $this->getSemSourceId(),
			'sem_keyword_id' => $this->getSemKeywordId(),
			'company_payment_id' => $this->getCompanyPaymentId(),
			'sem_budget_id' => $this->getSemBudgetId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'cost_per_click' => $this->getCostPerClick(),
			'estimated_amount' => $this->getEstimatedAmount(),
			'transaction_amount' => $this->getTransactionAmount(),
			'ip_address' => $this->getIpAddress(),
			'referring_url' => $this->getReferringUrl(),
			'is_internal' => $this->getIsInternal(),
			'is_adjustment' => $this->getIsAdjustment(),
			'is_reconciled' => $this->getIsReconciled(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>