<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportDataTypes
 * Do not add any new functions to this class.
 */

class CBaseReportDataTypes extends CEosPluralBase {

	/**
	 * @return CReportDataType[]
	 */
	public static function fetchReportDataTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportDataType::class, $objDatabase );
	}

	/**
	 * @return CReportDataType
	 */
	public static function fetchReportDataType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportDataType::class, $objDatabase );
	}

	public static function fetchReportDataTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_data_types', $objDatabase );
	}

	public static function fetchReportDataTypeById( $intId, $objDatabase ) {
		return self::fetchReportDataType( sprintf( 'SELECT * FROM report_data_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>