<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseHelpResourceMediaAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.help_resource_media_associations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intParentHelpResourceId;
	protected $m_intTranslatedHelpResourceId;
	protected $m_strParentMediaTitle;
	protected $m_strTranslatedMediaTitle;
	protected $m_strLocaleCode;
	protected $m_intOrderNum;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['parent_help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intParentHelpResourceId', trim( $arrValues['parent_help_resource_id'] ) ); elseif( isset( $arrValues['parent_help_resource_id'] ) ) $this->setParentHelpResourceId( $arrValues['parent_help_resource_id'] );
		if( isset( $arrValues['translated_help_resource_id'] ) && $boolDirectSet ) $this->set( 'm_intTranslatedHelpResourceId', trim( $arrValues['translated_help_resource_id'] ) ); elseif( isset( $arrValues['translated_help_resource_id'] ) ) $this->setTranslatedHelpResourceId( $arrValues['translated_help_resource_id'] );
		if( isset( $arrValues['parent_media_title'] ) && $boolDirectSet ) $this->set( 'm_strParentMediaTitle', trim( $arrValues['parent_media_title'] ) ); elseif( isset( $arrValues['parent_media_title'] ) ) $this->setParentMediaTitle( $arrValues['parent_media_title'] );
		if( isset( $arrValues['translated_media_title'] ) && $boolDirectSet ) $this->set( 'm_strTranslatedMediaTitle', trim( $arrValues['translated_media_title'] ) ); elseif( isset( $arrValues['translated_media_title'] ) ) $this->setTranslatedMediaTitle( $arrValues['translated_media_title'] );
		if( isset( $arrValues['locale_code'] ) && $boolDirectSet ) $this->set( 'm_strLocaleCode', trim( $arrValues['locale_code'] ) ); elseif( isset( $arrValues['locale_code'] ) ) $this->setLocaleCode( $arrValues['locale_code'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setParentHelpResourceId( $intParentHelpResourceId ) {
		$this->set( 'm_intParentHelpResourceId', CStrings::strToIntDef( $intParentHelpResourceId, NULL, false ) );
	}

	public function getParentHelpResourceId() {
		return $this->m_intParentHelpResourceId;
	}

	public function sqlParentHelpResourceId() {
		return ( true == isset( $this->m_intParentHelpResourceId ) ) ? ( string ) $this->m_intParentHelpResourceId : 'NULL';
	}

	public function setTranslatedHelpResourceId( $intTranslatedHelpResourceId ) {
		$this->set( 'm_intTranslatedHelpResourceId', CStrings::strToIntDef( $intTranslatedHelpResourceId, NULL, false ) );
	}

	public function getTranslatedHelpResourceId() {
		return $this->m_intTranslatedHelpResourceId;
	}

	public function sqlTranslatedHelpResourceId() {
		return ( true == isset( $this->m_intTranslatedHelpResourceId ) ) ? ( string ) $this->m_intTranslatedHelpResourceId : 'NULL';
	}

	public function setParentMediaTitle( $strParentMediaTitle ) {
		$this->set( 'm_strParentMediaTitle', CStrings::strTrimDef( $strParentMediaTitle, -1, NULL, true ) );
	}

	public function getParentMediaTitle() {
		return $this->m_strParentMediaTitle;
	}

	public function sqlParentMediaTitle() {
		return ( true == isset( $this->m_strParentMediaTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strParentMediaTitle ) : '\'' . addslashes( $this->m_strParentMediaTitle ) . '\'' ) : 'NULL';
	}

	public function setTranslatedMediaTitle( $strTranslatedMediaTitle ) {
		$this->set( 'm_strTranslatedMediaTitle', CStrings::strTrimDef( $strTranslatedMediaTitle, -1, NULL, true ) );
	}

	public function getTranslatedMediaTitle() {
		return $this->m_strTranslatedMediaTitle;
	}

	public function sqlTranslatedMediaTitle() {
		return ( true == isset( $this->m_strTranslatedMediaTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTranslatedMediaTitle ) : '\'' . addslashes( $this->m_strTranslatedMediaTitle ) . '\'' ) : 'NULL';
	}

	public function setLocaleCode( $strLocaleCode ) {
		$this->set( 'm_strLocaleCode', CStrings::strTrimDef( $strLocaleCode, -1, NULL, true ) );
	}

	public function getLocaleCode() {
		return $this->m_strLocaleCode;
	}

	public function sqlLocaleCode() {
		return ( true == isset( $this->m_strLocaleCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLocaleCode ) : '\'' . addslashes( $this->m_strLocaleCode ) . '\'' ) : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, parent_help_resource_id, translated_help_resource_id, parent_media_title, translated_media_title, locale_code, order_num, approved_by, approved_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlParentHelpResourceId() . ', ' .
						$this->sqlTranslatedHelpResourceId() . ', ' .
						$this->sqlParentMediaTitle() . ', ' .
						$this->sqlTranslatedMediaTitle() . ', ' .
						$this->sqlLocaleCode() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_help_resource_id = ' . $this->sqlParentHelpResourceId(). ',' ; } elseif( true == array_key_exists( 'ParentHelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' parent_help_resource_id = ' . $this->sqlParentHelpResourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' translated_help_resource_id = ' . $this->sqlTranslatedHelpResourceId(). ',' ; } elseif( true == array_key_exists( 'TranslatedHelpResourceId', $this->getChangedColumns() ) ) { $strSql .= ' translated_help_resource_id = ' . $this->sqlTranslatedHelpResourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_media_title = ' . $this->sqlParentMediaTitle(). ',' ; } elseif( true == array_key_exists( 'ParentMediaTitle', $this->getChangedColumns() ) ) { $strSql .= ' parent_media_title = ' . $this->sqlParentMediaTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' translated_media_title = ' . $this->sqlTranslatedMediaTitle(). ',' ; } elseif( true == array_key_exists( 'TranslatedMediaTitle', $this->getChangedColumns() ) ) { $strSql .= ' translated_media_title = ' . $this->sqlTranslatedMediaTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode(). ',' ; } elseif( true == array_key_exists( 'LocaleCode', $this->getChangedColumns() ) ) { $strSql .= ' locale_code = ' . $this->sqlLocaleCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'parent_help_resource_id' => $this->getParentHelpResourceId(),
			'translated_help_resource_id' => $this->getTranslatedHelpResourceId(),
			'parent_media_title' => $this->getParentMediaTitle(),
			'translated_media_title' => $this->getTranslatedMediaTitle(),
			'locale_code' => $this->getLocaleCode(),
			'order_num' => $this->getOrderNum(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>