<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskAssignments
 * Do not add any new functions to this class.
 */

class CBaseTaskAssignments extends CEosPluralBase {

	/**
	 * @return CTaskAssignment[]
	 */
	public static function fetchTaskAssignments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskAssignment', $objDatabase );
	}

	/**
	 * @return CTaskAssignment
	 */
	public static function fetchTaskAssignment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskAssignment', $objDatabase );
	}

	public static function fetchTaskAssignmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_assignments', $objDatabase );
	}

	public static function fetchTaskAssignmentById( $intId, $objDatabase ) {
		return self::fetchTaskAssignment( sprintf( 'SELECT * FROM task_assignments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskAssignmentsByCid( $intCid, $objDatabase ) {
		return self::fetchTaskAssignments( sprintf( 'SELECT * FROM task_assignments WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTaskAssignmentsByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchTaskAssignments( sprintf( 'SELECT * FROM task_assignments WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

	public static function fetchTaskAssignmentsByTaskStatusId( $intTaskStatusId, $objDatabase ) {
		return self::fetchTaskAssignments( sprintf( 'SELECT * FROM task_assignments WHERE task_status_id = %d', ( int ) $intTaskStatusId ), $objDatabase );
	}

	public static function fetchTaskAssignmentsByTaskPriorityId( $intTaskPriorityId, $objDatabase ) {
		return self::fetchTaskAssignments( sprintf( 'SELECT * FROM task_assignments WHERE task_priority_id = %d', ( int ) $intTaskPriorityId ), $objDatabase );
	}

	public static function fetchTaskAssignmentsByTaskReleaseId( $intTaskReleaseId, $objDatabase ) {
		return self::fetchTaskAssignments( sprintf( 'SELECT * FROM task_assignments WHERE task_release_id = %d', ( int ) $intTaskReleaseId ), $objDatabase );
	}

	public static function fetchTaskAssignmentsByTaskTypeId( $intTaskTypeId, $objDatabase ) {
		return self::fetchTaskAssignments( sprintf( 'SELECT * FROM task_assignments WHERE task_type_id = %d', ( int ) $intTaskTypeId ), $objDatabase );
	}

	public static function fetchTaskAssignmentsByUserId( $intUserId, $objDatabase ) {
		return self::fetchTaskAssignments( sprintf( 'SELECT * FROM task_assignments WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

	public static function fetchTaskAssignmentsBySdmEmployeeId( $intSdmEmployeeId, $objDatabase ) {
		return self::fetchTaskAssignments( sprintf( 'SELECT * FROM task_assignments WHERE sdm_employee_id = %d', ( int ) $intSdmEmployeeId ), $objDatabase );
	}

	public static function fetchTaskAssignmentsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchTaskAssignments( sprintf( 'SELECT * FROM task_assignments WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchTaskAssignmentsByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchTaskAssignments( sprintf( 'SELECT * FROM task_assignments WHERE ps_product_option_id = %d', ( int ) $intPsProductOptionId ), $objDatabase );
	}

}
?>