<?php

class CBaseSemAndOrganicStat extends CEosSingularBase {

    protected $m_intId;
    protected $m_intCid;
    protected $m_intPropertyId;
    protected $m_intSemSourceId;
    protected $m_intSemAdGroupId;
    protected $m_intSemKeywordId;
    protected $m_strTrafficDate;
    protected $m_intListImpressions;
    protected $m_intListFeaturedImpressions;
    protected $m_intFullImpressions;
    protected $m_intWebsiteVisits;
    protected $m_intClickThroughs;
    protected $m_intUniqueListImpressions;
    protected $m_intUniqueListFeaturedImpressions;
    protected $m_intUniqueFullImpressions;
    protected $m_intUniqueWebsiteVisits;
    protected $m_intUniqueClickThroughs;
    protected $m_intAveragePosition;
    protected $m_fltMaximumCostPerClick;
    protected $m_intGuestCards;
    protected $m_intOrganicCalls;
    protected $m_intPaidCalls;
    protected $m_intUpdatedBy;
    protected $m_strUpdatedOn;
    protected $m_intCreatedBy;
    protected $m_strCreatedOn;

    public function __construct() {
        parent::__construct();

        $this->m_intListImpressions = '0';
        $this->m_intListFeaturedImpressions = '0';
        $this->m_intFullImpressions = '0';
        $this->m_intWebsiteVisits = '0';
        $this->m_intClickThroughs = '0';
        $this->m_intUniqueListImpressions = '0';
        $this->m_intUniqueListFeaturedImpressions = '0';
        $this->m_intUniqueFullImpressions = '0';
        $this->m_intUniqueWebsiteVisits = '0';
        $this->m_intUniqueClickThroughs = '0';
        $this->m_intGuestCards = '0';
        $this->m_intOrganicCalls = '0';
        $this->m_intPaidCalls = '0';

        return;
    }

    public function setDefaults() {
        return;
    }

    public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
        if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->m_intCid = trim( $arrValues['cid'] ); else if( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
        if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->m_intPropertyId = trim( $arrValues['property_id'] ); else if( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
        if( isset( $arrValues['sem_source_id'] ) && $boolDirectSet ) $this->m_intSemSourceId = trim( $arrValues['sem_source_id'] ); else if( isset( $arrValues['sem_source_id'] ) ) $this->setSemSourceId( $arrValues['sem_source_id'] );
        if( isset( $arrValues['sem_ad_group_id'] ) && $boolDirectSet ) $this->m_intSemAdGroupId = trim( $arrValues['sem_ad_group_id'] ); else if( isset( $arrValues['sem_ad_group_id'] ) ) $this->setSemAdGroupId( $arrValues['sem_ad_group_id'] );
        if( isset( $arrValues['sem_keyword_id'] ) && $boolDirectSet ) $this->m_intSemKeywordId = trim( $arrValues['sem_keyword_id'] ); else if( isset( $arrValues['sem_keyword_id'] ) ) $this->setSemKeywordId( $arrValues['sem_keyword_id'] );
        if( isset( $arrValues['traffic_date'] ) && $boolDirectSet ) $this->m_strTrafficDate = trim( $arrValues['traffic_date'] ); else if( isset( $arrValues['traffic_date'] ) ) $this->setTrafficDate( $arrValues['traffic_date'] );
        if( isset( $arrValues['list_impressions'] ) && $boolDirectSet ) $this->m_intListImpressions = trim( $arrValues['list_impressions'] ); else if( isset( $arrValues['list_impressions'] ) ) $this->setListImpressions( $arrValues['list_impressions'] );
        if( isset( $arrValues['list_featured_impressions'] ) && $boolDirectSet ) $this->m_intListFeaturedImpressions = trim( $arrValues['list_featured_impressions'] ); else if( isset( $arrValues['list_featured_impressions'] ) ) $this->setListFeaturedImpressions( $arrValues['list_featured_impressions'] );
        if( isset( $arrValues['full_impressions'] ) && $boolDirectSet ) $this->m_intFullImpressions = trim( $arrValues['full_impressions'] ); else if( isset( $arrValues['full_impressions'] ) ) $this->setFullImpressions( $arrValues['full_impressions'] );
        if( isset( $arrValues['website_visits'] ) && $boolDirectSet ) $this->m_intWebsiteVisits = trim( $arrValues['website_visits'] ); else if( isset( $arrValues['website_visits'] ) ) $this->setWebsiteVisits( $arrValues['website_visits'] );
        if( isset( $arrValues['click_throughs'] ) && $boolDirectSet ) $this->m_intClickThroughs = trim( $arrValues['click_throughs'] ); else if( isset( $arrValues['click_throughs'] ) ) $this->setClickThroughs( $arrValues['click_throughs'] );
        if( isset( $arrValues['unique_list_impressions'] ) && $boolDirectSet ) $this->m_intUniqueListImpressions = trim( $arrValues['unique_list_impressions'] ); else if( isset( $arrValues['unique_list_impressions'] ) ) $this->setUniqueListImpressions( $arrValues['unique_list_impressions'] );
        if( isset( $arrValues['unique_list_featured_impressions'] ) && $boolDirectSet ) $this->m_intUniqueListFeaturedImpressions = trim( $arrValues['unique_list_featured_impressions'] ); else if( isset( $arrValues['unique_list_featured_impressions'] ) ) $this->setUniqueListFeaturedImpressions( $arrValues['unique_list_featured_impressions'] );
        if( isset( $arrValues['unique_full_impressions'] ) && $boolDirectSet ) $this->m_intUniqueFullImpressions = trim( $arrValues['unique_full_impressions'] ); else if( isset( $arrValues['unique_full_impressions'] ) ) $this->setUniqueFullImpressions( $arrValues['unique_full_impressions'] );
        if( isset( $arrValues['unique_website_visits'] ) && $boolDirectSet ) $this->m_intUniqueWebsiteVisits = trim( $arrValues['unique_website_visits'] ); else if( isset( $arrValues['unique_website_visits'] ) ) $this->setUniqueWebsiteVisits( $arrValues['unique_website_visits'] );
        if( isset( $arrValues['unique_click_throughs'] ) && $boolDirectSet ) $this->m_intUniqueClickThroughs = trim( $arrValues['unique_click_throughs'] ); else if( isset( $arrValues['unique_click_throughs'] ) ) $this->setUniqueClickThroughs( $arrValues['unique_click_throughs'] );
        if( isset( $arrValues['average_position'] ) && $boolDirectSet ) $this->m_intAveragePosition = trim( $arrValues['average_position'] ); else if( isset( $arrValues['average_position'] ) ) $this->setAveragePosition( $arrValues['average_position'] );
        if( isset( $arrValues['maximum_cost_per_click'] ) && $boolDirectSet ) $this->m_fltMaximumCostPerClick = trim( $arrValues['maximum_cost_per_click'] ); else if( isset( $arrValues['maximum_cost_per_click'] ) ) $this->setMaximumCostPerClick( $arrValues['maximum_cost_per_click'] );
        if( isset( $arrValues['guest_cards'] ) && $boolDirectSet ) $this->m_intGuestCards = trim( $arrValues['guest_cards'] ); else if( isset( $arrValues['guest_cards'] ) ) $this->setGuestCards( $arrValues['guest_cards'] );
        if( isset( $arrValues['organic_calls'] ) && $boolDirectSet ) $this->m_intOrganicCalls = trim( $arrValues['organic_calls'] ); else if( isset( $arrValues['organic_calls'] ) ) $this->setOrganicCalls( $arrValues['organic_calls'] );
        if( isset( $arrValues['paid_calls'] ) && $boolDirectSet ) $this->m_intPaidCalls = trim( $arrValues['paid_calls'] ); else if( isset( $arrValues['paid_calls'] ) ) $this->setPaidCalls( $arrValues['paid_calls'] );
        if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
        if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
        if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
        if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

        return;
    }

    public function setId( $intId ) {
        $this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
    }

    public function getId() {
        return $this->m_intId;
    }

    public function sqlId() {
        return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
    }

    public function setCid( $intCid ) {
        $this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
    }

    public function getCid() {
        return $this->m_intCid;
    }

    public function sqlCid() {
        return ( true == isset( $this->m_intCid ) ) ? (string) $this->m_intCid : 'NULL';
    }

    public function setPropertyId( $intPropertyId ) {
        $this->m_intPropertyId = CStrings::strToIntDef( $intPropertyId, NULL, false );
    }

    public function getPropertyId() {
        return $this->m_intPropertyId;
    }

    public function sqlPropertyId() {
        return ( true == isset( $this->m_intPropertyId ) ) ? (string) $this->m_intPropertyId : 'NULL';
    }

    public function setSemSourceId( $intSemSourceId ) {
        $this->m_intSemSourceId = CStrings::strToIntDef( $intSemSourceId, NULL, false );
    }

    public function getSemSourceId() {
        return $this->m_intSemSourceId;
    }

    public function sqlSemSourceId() {
        return ( true == isset( $this->m_intSemSourceId ) ) ? (string) $this->m_intSemSourceId : 'NULL';
    }

    public function setSemAdGroupId( $intSemAdGroupId ) {
        $this->m_intSemAdGroupId = CStrings::strToIntDef( $intSemAdGroupId, NULL, false );
    }

    public function getSemAdGroupId() {
        return $this->m_intSemAdGroupId;
    }

    public function sqlSemAdGroupId() {
        return ( true == isset( $this->m_intSemAdGroupId ) ) ? (string) $this->m_intSemAdGroupId : 'NULL';
    }

    public function setSemKeywordId( $intSemKeywordId ) {
        $this->m_intSemKeywordId = CStrings::strToIntDef( $intSemKeywordId, NULL, false );
    }

    public function getSemKeywordId() {
        return $this->m_intSemKeywordId;
    }

    public function sqlSemKeywordId() {
        return ( true == isset( $this->m_intSemKeywordId ) ) ? (string) $this->m_intSemKeywordId : 'NULL';
    }

    public function setTrafficDate( $strTrafficDate ) {
        $this->m_strTrafficDate = CStrings::strTrimDef( $strTrafficDate, -1, NULL, true );
    }

    public function getTrafficDate() {
        return $this->m_strTrafficDate;
    }

    public function sqlTrafficDate() {
        return ( true == isset( $this->m_strTrafficDate ) ) ? '\'' . $this->m_strTrafficDate . '\'' : 'NOW()';
    }

    public function setListImpressions( $intListImpressions ) {
        $this->m_intListImpressions = CStrings::strToIntDef( $intListImpressions, NULL, false );
    }

    public function getListImpressions() {
        return $this->m_intListImpressions;
    }

    public function sqlListImpressions() {
        return ( true == isset( $this->m_intListImpressions ) ) ? (string) $this->m_intListImpressions : '0';
    }

    public function setListFeaturedImpressions( $intListFeaturedImpressions ) {
        $this->m_intListFeaturedImpressions = CStrings::strToIntDef( $intListFeaturedImpressions, NULL, false );
    }

    public function getListFeaturedImpressions() {
        return $this->m_intListFeaturedImpressions;
    }

    public function sqlListFeaturedImpressions() {
        return ( true == isset( $this->m_intListFeaturedImpressions ) ) ? (string) $this->m_intListFeaturedImpressions : '0';
    }

    public function setFullImpressions( $intFullImpressions ) {
        $this->m_intFullImpressions = CStrings::strToIntDef( $intFullImpressions, NULL, false );
    }

    public function getFullImpressions() {
        return $this->m_intFullImpressions;
    }

    public function sqlFullImpressions() {
        return ( true == isset( $this->m_intFullImpressions ) ) ? (string) $this->m_intFullImpressions : '0';
    }

    public function setWebsiteVisits( $intWebsiteVisits ) {
        $this->m_intWebsiteVisits = CStrings::strToIntDef( $intWebsiteVisits, NULL, false );
    }

    public function getWebsiteVisits() {
        return $this->m_intWebsiteVisits;
    }

    public function sqlWebsiteVisits() {
        return ( true == isset( $this->m_intWebsiteVisits ) ) ? (string) $this->m_intWebsiteVisits : '0';
    }

    public function setClickThroughs( $intClickThroughs ) {
        $this->m_intClickThroughs = CStrings::strToIntDef( $intClickThroughs, NULL, false );
    }

    public function getClickThroughs() {
        return $this->m_intClickThroughs;
    }

    public function sqlClickThroughs() {
        return ( true == isset( $this->m_intClickThroughs ) ) ? (string) $this->m_intClickThroughs : '0';
    }

    public function setUniqueListImpressions( $intUniqueListImpressions ) {
        $this->m_intUniqueListImpressions = CStrings::strToIntDef( $intUniqueListImpressions, NULL, false );
    }

    public function getUniqueListImpressions() {
        return $this->m_intUniqueListImpressions;
    }

    public function sqlUniqueListImpressions() {
        return ( true == isset( $this->m_intUniqueListImpressions ) ) ? (string) $this->m_intUniqueListImpressions : '0';
    }

    public function setUniqueListFeaturedImpressions( $intUniqueListFeaturedImpressions ) {
        $this->m_intUniqueListFeaturedImpressions = CStrings::strToIntDef( $intUniqueListFeaturedImpressions, NULL, false );
    }

    public function getUniqueListFeaturedImpressions() {
        return $this->m_intUniqueListFeaturedImpressions;
    }

    public function sqlUniqueListFeaturedImpressions() {
        return ( true == isset( $this->m_intUniqueListFeaturedImpressions ) ) ? (string) $this->m_intUniqueListFeaturedImpressions : '0';
    }

    public function setUniqueFullImpressions( $intUniqueFullImpressions ) {
        $this->m_intUniqueFullImpressions = CStrings::strToIntDef( $intUniqueFullImpressions, NULL, false );
    }

    public function getUniqueFullImpressions() {
        return $this->m_intUniqueFullImpressions;
    }

    public function sqlUniqueFullImpressions() {
        return ( true == isset( $this->m_intUniqueFullImpressions ) ) ? (string) $this->m_intUniqueFullImpressions : '0';
    }

    public function setUniqueWebsiteVisits( $intUniqueWebsiteVisits ) {
        $this->m_intUniqueWebsiteVisits = CStrings::strToIntDef( $intUniqueWebsiteVisits, NULL, false );
    }

    public function getUniqueWebsiteVisits() {
        return $this->m_intUniqueWebsiteVisits;
    }

    public function sqlUniqueWebsiteVisits() {
        return ( true == isset( $this->m_intUniqueWebsiteVisits ) ) ? (string) $this->m_intUniqueWebsiteVisits : '0';
    }

    public function setUniqueClickThroughs( $intUniqueClickThroughs ) {
        $this->m_intUniqueClickThroughs = CStrings::strToIntDef( $intUniqueClickThroughs, NULL, false );
    }

    public function getUniqueClickThroughs() {
        return $this->m_intUniqueClickThroughs;
    }

    public function sqlUniqueClickThroughs() {
        return ( true == isset( $this->m_intUniqueClickThroughs ) ) ? (string) $this->m_intUniqueClickThroughs : '0';
    }

    public function setAveragePosition( $intAveragePosition ) {
        $this->m_intAveragePosition = CStrings::strToIntDef( $intAveragePosition, NULL, false );
    }

    public function getAveragePosition() {
        return $this->m_intAveragePosition;
    }

    public function sqlAveragePosition() {
        return ( true == isset( $this->m_intAveragePosition ) ) ? (string) $this->m_intAveragePosition : 'NULL';
    }

    public function setMaximumCostPerClick( $fltMaximumCostPerClick ) {
        $this->m_fltMaximumCostPerClick = CStrings::strToFloatDef( $fltMaximumCostPerClick, NULL, false, 2 );
    }

    public function getMaximumCostPerClick() {
        return $this->m_fltMaximumCostPerClick;
    }

    public function sqlMaximumCostPerClick() {
        return ( true == isset( $this->m_fltMaximumCostPerClick ) ) ? (string) $this->m_fltMaximumCostPerClick : 'NULL';
    }

    public function setGuestCards( $intGuestCards ) {
        $this->m_intGuestCards = CStrings::strToIntDef( $intGuestCards, NULL, false );
    }

    public function getGuestCards() {
        return $this->m_intGuestCards;
    }

    public function sqlGuestCards() {
        return ( true == isset( $this->m_intGuestCards ) ) ? (string) $this->m_intGuestCards : '0';
    }

    public function setOrganicCalls( $intOrganicCalls ) {
        $this->m_intOrganicCalls = CStrings::strToIntDef( $intOrganicCalls, NULL, false );
    }

    public function getOrganicCalls() {
        return $this->m_intOrganicCalls;
    }

    public function sqlOrganicCalls() {
        return ( true == isset( $this->m_intOrganicCalls ) ) ? (string) $this->m_intOrganicCalls : '0';
    }

    public function setPaidCalls( $intPaidCalls ) {
        $this->m_intPaidCalls = CStrings::strToIntDef( $intPaidCalls, NULL, false );
    }

    public function getPaidCalls() {
        return $this->m_intPaidCalls;
    }

    public function sqlPaidCalls() {
        return ( true == isset( $this->m_intPaidCalls ) ) ? (string) $this->m_intPaidCalls : '0';
    }

    public function setUpdatedBy( $intUpdatedBy ) {
        $this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
    }

    public function getUpdatedBy() {
        return $this->m_intUpdatedBy;
    }

    public function sqlUpdatedBy() {
        return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
    }

    public function setUpdatedOn( $strUpdatedOn ) {
        $this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
    }

    public function getUpdatedOn() {
        return $this->m_strUpdatedOn;
    }

    public function sqlUpdatedOn() {
        return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
    }

    public function setCreatedBy( $intCreatedBy ) {
        $this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
    }

    public function getCreatedBy() {
        return $this->m_intCreatedBy;
    }

    public function sqlCreatedBy() {
        return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
    }

    public function setCreatedOn( $strCreatedOn ) {
        $this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
    }

    public function getCreatedOn() {
        return $this->m_strCreatedOn;
    }

    public function sqlCreatedOn() {
        return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
    }

    public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strId = ( true == is_null( $this->getId() ) ) ?  'nextval( \'public.sem_and_organic_stats_id_seq\' )' : (int) $this->m_intId;

        $strSql = 'INSERT INTO					  public.sem_and_organic_stats					VALUES ( ' .	                    $strId . ', ' . 		                $this->sqlCid() . ', ' . 		                $this->sqlPropertyId() . ', ' . 		                $this->sqlSemSourceId() . ', ' . 		                $this->sqlSemAdGroupId() . ', ' . 		                $this->sqlSemKeywordId() . ', ' . 		                $this->sqlTrafficDate() . ', ' . 		                $this->sqlListImpressions() . ', ' . 		                $this->sqlListFeaturedImpressions() . ', ' . 		                $this->sqlFullImpressions() . ', ' . 		                $this->sqlWebsiteVisits() . ', ' . 		                $this->sqlClickThroughs() . ', ' . 		                $this->sqlUniqueListImpressions() . ', ' . 		                $this->sqlUniqueListFeaturedImpressions() . ', ' . 		                $this->sqlUniqueFullImpressions() . ', ' . 		                $this->sqlUniqueWebsiteVisits() . ', ' . 		                $this->sqlUniqueClickThroughs() . ', ' . 		                $this->sqlAveragePosition() . ', ' . 		                $this->sqlMaximumCostPerClick() . ', ' . 		                $this->sqlGuestCards() . ', ' . 		                $this->sqlOrganicCalls() . ', ' . 		                $this->sqlPaidCalls() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlUpdatedOn() . ', ' .                    	(int) $intCurrentUserId . ', ' . 		                $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $boolUpdate = true;

        if( true == $this->getAllowDifferentialUpdate() ) {
        	$this->unSerializeAndSetOriginalValues();
        	$arrstrOriginalValueChanges = array();
        	$boolUpdate = false;
        }
        $strSql = 'UPDATE
                      public.sem_and_organic_stats
                    SET ';
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName ( 'cid' ) ) { $arrstrOriginalValueChanges['cid'] = $this->sqlCid(); $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPropertyId() ) != $this->getOriginalValueByFieldName ( 'property_id' ) ) { $arrstrOriginalValueChanges['property_id'] = $this->sqlPropertyId(); $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSemSourceId() ) != $this->getOriginalValueByFieldName ( 'sem_source_id' ) ) { $arrstrOriginalValueChanges['sem_source_id'] = $this->sqlSemSourceId(); $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSemAdGroupId() ) != $this->getOriginalValueByFieldName ( 'sem_ad_group_id' ) ) { $arrstrOriginalValueChanges['sem_ad_group_id'] = $this->sqlSemAdGroupId(); $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlSemKeywordId() ) != $this->getOriginalValueByFieldName ( 'sem_keyword_id' ) ) { $arrstrOriginalValueChanges['sem_keyword_id'] = $this->sqlSemKeywordId(); $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' traffic_date = ' . $this->sqlTrafficDate() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTrafficDate() ) != $this->getOriginalValueByFieldName ( 'traffic_date' ) ) { $arrstrOriginalValueChanges['traffic_date'] = $this->sqlTrafficDate(); $strSql .= ' traffic_date = ' . $this->sqlTrafficDate() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' list_impressions = ' . $this->sqlListImpressions() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlListImpressions() ) != $this->getOriginalValueByFieldName ( 'list_impressions' ) ) { $arrstrOriginalValueChanges['list_impressions'] = $this->sqlListImpressions(); $strSql .= ' list_impressions = ' . $this->sqlListImpressions() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' list_featured_impressions = ' . $this->sqlListFeaturedImpressions() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlListFeaturedImpressions() ) != $this->getOriginalValueByFieldName ( 'list_featured_impressions' ) ) { $arrstrOriginalValueChanges['list_featured_impressions'] = $this->sqlListFeaturedImpressions(); $strSql .= ' list_featured_impressions = ' . $this->sqlListFeaturedImpressions() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' full_impressions = ' . $this->sqlFullImpressions() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlFullImpressions() ) != $this->getOriginalValueByFieldName ( 'full_impressions' ) ) { $arrstrOriginalValueChanges['full_impressions'] = $this->sqlFullImpressions(); $strSql .= ' full_impressions = ' . $this->sqlFullImpressions() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_visits = ' . $this->sqlWebsiteVisits() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlWebsiteVisits() ) != $this->getOriginalValueByFieldName ( 'website_visits' ) ) { $arrstrOriginalValueChanges['website_visits'] = $this->sqlWebsiteVisits(); $strSql .= ' website_visits = ' . $this->sqlWebsiteVisits() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' click_throughs = ' . $this->sqlClickThroughs() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlClickThroughs() ) != $this->getOriginalValueByFieldName ( 'click_throughs' ) ) { $arrstrOriginalValueChanges['click_throughs'] = $this->sqlClickThroughs(); $strSql .= ' click_throughs = ' . $this->sqlClickThroughs() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unique_list_impressions = ' . $this->sqlUniqueListImpressions() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUniqueListImpressions() ) != $this->getOriginalValueByFieldName ( 'unique_list_impressions' ) ) { $arrstrOriginalValueChanges['unique_list_impressions'] = $this->sqlUniqueListImpressions(); $strSql .= ' unique_list_impressions = ' . $this->sqlUniqueListImpressions() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unique_list_featured_impressions = ' . $this->sqlUniqueListFeaturedImpressions() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUniqueListFeaturedImpressions() ) != $this->getOriginalValueByFieldName ( 'unique_list_featured_impressions' ) ) { $arrstrOriginalValueChanges['unique_list_featured_impressions'] = $this->sqlUniqueListFeaturedImpressions(); $strSql .= ' unique_list_featured_impressions = ' . $this->sqlUniqueListFeaturedImpressions() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unique_full_impressions = ' . $this->sqlUniqueFullImpressions() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUniqueFullImpressions() ) != $this->getOriginalValueByFieldName ( 'unique_full_impressions' ) ) { $arrstrOriginalValueChanges['unique_full_impressions'] = $this->sqlUniqueFullImpressions(); $strSql .= ' unique_full_impressions = ' . $this->sqlUniqueFullImpressions() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unique_website_visits = ' . $this->sqlUniqueWebsiteVisits() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUniqueWebsiteVisits() ) != $this->getOriginalValueByFieldName ( 'unique_website_visits' ) ) { $arrstrOriginalValueChanges['unique_website_visits'] = $this->sqlUniqueWebsiteVisits(); $strSql .= ' unique_website_visits = ' . $this->sqlUniqueWebsiteVisits() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unique_click_throughs = ' . $this->sqlUniqueClickThroughs() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlUniqueClickThroughs() ) != $this->getOriginalValueByFieldName ( 'unique_click_throughs' ) ) { $arrstrOriginalValueChanges['unique_click_throughs'] = $this->sqlUniqueClickThroughs(); $strSql .= ' unique_click_throughs = ' . $this->sqlUniqueClickThroughs() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_position = ' . $this->sqlAveragePosition() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAveragePosition() ) != $this->getOriginalValueByFieldName ( 'average_position' ) ) { $arrstrOriginalValueChanges['average_position'] = $this->sqlAveragePosition(); $strSql .= ' average_position = ' . $this->sqlAveragePosition() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' maximum_cost_per_click = ' . $this->sqlMaximumCostPerClick() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlMaximumCostPerClick() ), $this->getOriginalValueByFieldName ( 'maximum_cost_per_click' ), 2 ) ) ) { $arrstrOriginalValueChanges['maximum_cost_per_click'] = $this->sqlMaximumCostPerClick(); $strSql .= ' maximum_cost_per_click = ' . $this->sqlMaximumCostPerClick() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' guest_cards = ' . $this->sqlGuestCards() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlGuestCards() ) != $this->getOriginalValueByFieldName ( 'guest_cards' ) ) { $arrstrOriginalValueChanges['guest_cards'] = $this->sqlGuestCards(); $strSql .= ' guest_cards = ' . $this->sqlGuestCards() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organic_calls = ' . $this->sqlOrganicCalls() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlOrganicCalls() ) != $this->getOriginalValueByFieldName ( 'organic_calls' ) ) { $arrstrOriginalValueChanges['organic_calls'] = $this->sqlOrganicCalls(); $strSql .= ' organic_calls = ' . $this->sqlOrganicCalls() . ','; $boolUpdate = true; }
                      if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' paid_calls = ' . $this->sqlPaidCalls() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPaidCalls() ) != $this->getOriginalValueByFieldName ( 'paid_calls' ) ) { $arrstrOriginalValueChanges['paid_calls'] = $this->sqlPaidCalls(); $strSql .= ' paid_calls = ' . $this->sqlPaidCalls() . ','; $boolUpdate = true; }
                      $strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
                      $strSql .= ' updated_on = \'NOW()\'					WHERE					    id = ' . (int) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {			return ( true == $boolUpdate ) ? $strSql : false;
        } else {			if( true == $boolUpdate ) {			    if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {				    if( true == $this->getAllowDifferentialUpdate() ) {				    	$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );						return true;				    }			    } else {			        return false;				}			}			return true;		}
    }

    public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

        $strSql = 'DELETE FROM public.sem_and_organic_stats WHERE id = ' . (int) $this->sqlId() . ';';

        if( true == $boolReturnSqlOnly ) {
        	return $strSql;
        } else {
        	return $this->executeSql( $strSql, $this, $objDatabase );
        }
    }

    public function fetchNextId( $objDatabase, $strTableName = NULL ) {
        return parent::fetchNextId( 'public.sem_and_organic_stats_id_seq', $objDatabase );
    }

}
?>