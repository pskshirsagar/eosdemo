<?php

class CBaseContractDocument extends CEosSingularBase {

	const TABLE_NAME = 'public.contract_documents';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intContractDocumentTypeId;
	protected $m_intContractId;
	protected $m_intPsDocumentId;
	protected $m_intIsTemplate;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intContractDraftId;
	protected $m_strClientSignedOn;
	protected $m_intClientSignedBy;
	protected $m_strCounterSignedOn;
	protected $m_intCounterSignedBy;
	protected $m_boolIsDocumentRejected;

	public function __construct() {
		parent::__construct();

		$this->m_intIsTemplate = '0';
		$this->m_boolIsDocumentRejected = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['contract_document_type_id'] ) && $boolDirectSet ) $this->set( 'm_intContractDocumentTypeId', trim( $arrValues['contract_document_type_id'] ) ); elseif( isset( $arrValues['contract_document_type_id'] ) ) $this->setContractDocumentTypeId( $arrValues['contract_document_type_id'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['ps_document_id'] ) && $boolDirectSet ) $this->set( 'm_intPsDocumentId', trim( $arrValues['ps_document_id'] ) ); elseif( isset( $arrValues['ps_document_id'] ) ) $this->setPsDocumentId( $arrValues['ps_document_id'] );
		if( isset( $arrValues['is_template'] ) && $boolDirectSet ) $this->set( 'm_intIsTemplate', trim( $arrValues['is_template'] ) ); elseif( isset( $arrValues['is_template'] ) ) $this->setIsTemplate( $arrValues['is_template'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['contract_draft_id'] ) && $boolDirectSet ) $this->set( 'm_intContractDraftId', trim( $arrValues['contract_draft_id'] ) ); elseif( isset( $arrValues['contract_draft_id'] ) ) $this->setContractDraftId( $arrValues['contract_draft_id'] );
		if( isset( $arrValues['client_signed_on'] ) && $boolDirectSet ) $this->set( 'm_strClientSignedOn', trim( $arrValues['client_signed_on'] ) ); elseif( isset( $arrValues['client_signed_on'] ) ) $this->setClientSignedOn( $arrValues['client_signed_on'] );
		if( isset( $arrValues['client_signed_by'] ) && $boolDirectSet ) $this->set( 'm_intClientSignedBy', trim( $arrValues['client_signed_by'] ) ); elseif( isset( $arrValues['client_signed_by'] ) ) $this->setClientSignedBy( $arrValues['client_signed_by'] );
		if( isset( $arrValues['counter_signed_on'] ) && $boolDirectSet ) $this->set( 'm_strCounterSignedOn', trim( $arrValues['counter_signed_on'] ) ); elseif( isset( $arrValues['counter_signed_on'] ) ) $this->setCounterSignedOn( $arrValues['counter_signed_on'] );
		if( isset( $arrValues['counter_signed_by'] ) && $boolDirectSet ) $this->set( 'm_intCounterSignedBy', trim( $arrValues['counter_signed_by'] ) ); elseif( isset( $arrValues['counter_signed_by'] ) ) $this->setCounterSignedBy( $arrValues['counter_signed_by'] );
		if( isset( $arrValues['is_document_rejected'] ) && $boolDirectSet ) $this->set( 'm_boolIsDocumentRejected', trim( stripcslashes( $arrValues['is_document_rejected'] ) ) ); elseif( isset( $arrValues['is_document_rejected'] ) ) $this->setIsDocumentRejected( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_document_rejected'] ) : $arrValues['is_document_rejected'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setContractDocumentTypeId( $intContractDocumentTypeId ) {
		$this->set( 'm_intContractDocumentTypeId', CStrings::strToIntDef( $intContractDocumentTypeId, NULL, false ) );
	}

	public function getContractDocumentTypeId() {
		return $this->m_intContractDocumentTypeId;
	}

	public function sqlContractDocumentTypeId() {
		return ( true == isset( $this->m_intContractDocumentTypeId ) ) ? ( string ) $this->m_intContractDocumentTypeId : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setPsDocumentId( $intPsDocumentId ) {
		$this->set( 'm_intPsDocumentId', CStrings::strToIntDef( $intPsDocumentId, NULL, false ) );
	}

	public function getPsDocumentId() {
		return $this->m_intPsDocumentId;
	}

	public function sqlPsDocumentId() {
		return ( true == isset( $this->m_intPsDocumentId ) ) ? ( string ) $this->m_intPsDocumentId : 'NULL';
	}

	public function setIsTemplate( $intIsTemplate ) {
		$this->set( 'm_intIsTemplate', CStrings::strToIntDef( $intIsTemplate, NULL, false ) );
	}

	public function getIsTemplate() {
		return $this->m_intIsTemplate;
	}

	public function sqlIsTemplate() {
		return ( true == isset( $this->m_intIsTemplate ) ) ? ( string ) $this->m_intIsTemplate : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setContractDraftId( $intContractDraftId ) {
		$this->set( 'm_intContractDraftId', CStrings::strToIntDef( $intContractDraftId, NULL, false ) );
	}

	public function getContractDraftId() {
		return $this->m_intContractDraftId;
	}

	public function sqlContractDraftId() {
		return ( true == isset( $this->m_intContractDraftId ) ) ? ( string ) $this->m_intContractDraftId : 'NULL';
	}

	public function setClientSignedOn( $strClientSignedOn ) {
		$this->set( 'm_strClientSignedOn', CStrings::strTrimDef( $strClientSignedOn, -1, NULL, true ) );
	}

	public function getClientSignedOn() {
		return $this->m_strClientSignedOn;
	}

	public function sqlClientSignedOn() {
		return ( true == isset( $this->m_strClientSignedOn ) ) ? '\'' . $this->m_strClientSignedOn . '\'' : 'NULL';
	}

	public function setClientSignedBy( $intClientSignedBy ) {
		$this->set( 'm_intClientSignedBy', CStrings::strToIntDef( $intClientSignedBy, NULL, false ) );
	}

	public function getClientSignedBy() {
		return $this->m_intClientSignedBy;
	}

	public function sqlClientSignedBy() {
		return ( true == isset( $this->m_intClientSignedBy ) ) ? ( string ) $this->m_intClientSignedBy : 'NULL';
	}

	public function setCounterSignedOn( $strCounterSignedOn ) {
		$this->set( 'm_strCounterSignedOn', CStrings::strTrimDef( $strCounterSignedOn, -1, NULL, true ) );
	}

	public function getCounterSignedOn() {
		return $this->m_strCounterSignedOn;
	}

	public function sqlCounterSignedOn() {
		return ( true == isset( $this->m_strCounterSignedOn ) ) ? '\'' . $this->m_strCounterSignedOn . '\'' : 'NULL';
	}

	public function setCounterSignedBy( $intCounterSignedBy ) {
		$this->set( 'm_intCounterSignedBy', CStrings::strToIntDef( $intCounterSignedBy, NULL, false ) );
	}

	public function getCounterSignedBy() {
		return $this->m_intCounterSignedBy;
	}

	public function sqlCounterSignedBy() {
		return ( true == isset( $this->m_intCounterSignedBy ) ) ? ( string ) $this->m_intCounterSignedBy : 'NULL';
	}

	public function setIsDocumentRejected( $boolIsDocumentRejected ) {
		$this->set( 'm_boolIsDocumentRejected', CStrings::strToBool( $boolIsDocumentRejected ) );
	}

	public function getIsDocumentRejected() {
		return $this->m_boolIsDocumentRejected;
	}

	public function sqlIsDocumentRejected() {
		return ( true == isset( $this->m_boolIsDocumentRejected ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDocumentRejected ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, contract_document_type_id, contract_id, ps_document_id, is_template, deleted_by, deleted_on, created_by, created_on, contract_draft_id, client_signed_on, client_signed_by, counter_signed_on, counter_signed_by, is_document_rejected )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlContractDocumentTypeId() . ', ' .
						$this->sqlContractId() . ', ' .
						$this->sqlPsDocumentId() . ', ' .
						$this->sqlIsTemplate() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlContractDraftId() . ', ' .
						$this->sqlClientSignedOn() . ', ' .
						$this->sqlClientSignedBy() . ', ' .
						$this->sqlCounterSignedOn() . ', ' .
						$this->sqlCounterSignedBy() . ', ' .
						$this->sqlIsDocumentRejected() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_document_type_id = ' . $this->sqlContractDocumentTypeId(). ',' ; } elseif( true == array_key_exists( 'ContractDocumentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' contract_document_type_id = ' . $this->sqlContractDocumentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId(). ',' ; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId(). ',' ; } elseif( true == array_key_exists( 'PsDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate(). ',' ; } elseif( true == array_key_exists( 'IsTemplate', $this->getChangedColumns() ) ) { $strSql .= ' is_template = ' . $this->sqlIsTemplate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_draft_id = ' . $this->sqlContractDraftId(). ',' ; } elseif( true == array_key_exists( 'ContractDraftId', $this->getChangedColumns() ) ) { $strSql .= ' contract_draft_id = ' . $this->sqlContractDraftId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_signed_on = ' . $this->sqlClientSignedOn(). ',' ; } elseif( true == array_key_exists( 'ClientSignedOn', $this->getChangedColumns() ) ) { $strSql .= ' client_signed_on = ' . $this->sqlClientSignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_signed_by = ' . $this->sqlClientSignedBy(). ',' ; } elseif( true == array_key_exists( 'ClientSignedBy', $this->getChangedColumns() ) ) { $strSql .= ' client_signed_by = ' . $this->sqlClientSignedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' counter_signed_on = ' . $this->sqlCounterSignedOn(). ',' ; } elseif( true == array_key_exists( 'CounterSignedOn', $this->getChangedColumns() ) ) { $strSql .= ' counter_signed_on = ' . $this->sqlCounterSignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' counter_signed_by = ' . $this->sqlCounterSignedBy(). ',' ; } elseif( true == array_key_exists( 'CounterSignedBy', $this->getChangedColumns() ) ) { $strSql .= ' counter_signed_by = ' . $this->sqlCounterSignedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_document_rejected = ' . $this->sqlIsDocumentRejected() ; } elseif( true == array_key_exists( 'IsDocumentRejected', $this->getChangedColumns() ) ) { $strSql .= ' is_document_rejected = ' . $this->sqlIsDocumentRejected() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'contract_document_type_id' => $this->getContractDocumentTypeId(),
			'contract_id' => $this->getContractId(),
			'ps_document_id' => $this->getPsDocumentId(),
			'is_template' => $this->getIsTemplate(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'contract_draft_id' => $this->getContractDraftId(),
			'client_signed_on' => $this->getClientSignedOn(),
			'client_signed_by' => $this->getClientSignedBy(),
			'counter_signed_on' => $this->getCounterSignedOn(),
			'counter_signed_by' => $this->getCounterSignedBy(),
			'is_document_rejected' => $this->getIsDocumentRejected()
		);
	}

}
?>