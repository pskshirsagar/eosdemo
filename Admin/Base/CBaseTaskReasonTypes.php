<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskReasonTypes
 * Do not add any new functions to this class.
 */

class CBaseTaskReasonTypes extends CEosPluralBase {

	/**
	 * @return CTaskReasonType[]
	 */
	public static function fetchTaskReasonTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskReasonType', $objDatabase );
	}

	/**
	 * @return CTaskReasonType
	 */
	public static function fetchTaskReasonType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskReasonType', $objDatabase );
	}

	public static function fetchTaskReasonTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_reason_types', $objDatabase );
	}

	public static function fetchTaskReasonTypeById( $intId, $objDatabase ) {
		return self::fetchTaskReasonType( sprintf( 'SELECT * FROM task_reason_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>