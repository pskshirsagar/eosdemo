<?php

class CBaseIlsErrorFixLog extends CEosSingularBase {

	const TABLE_NAME = 'public.ils_error_fix_logs';

	protected $m_intId;
	protected $m_intIlsEmailErrorTypeId;
	protected $m_strFixDescription;
	protected $m_strReleaseDate;
	protected $m_intFixedBy;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ils_email_error_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIlsEmailErrorTypeId', trim( $arrValues['ils_email_error_type_id'] ) ); elseif( isset( $arrValues['ils_email_error_type_id'] ) ) $this->setIlsEmailErrorTypeId( $arrValues['ils_email_error_type_id'] );
		if( isset( $arrValues['fix_description'] ) && $boolDirectSet ) $this->set( 'm_strFixDescription', trim( stripcslashes( $arrValues['fix_description'] ) ) ); elseif( isset( $arrValues['fix_description'] ) ) $this->setFixDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['fix_description'] ) : $arrValues['fix_description'] );
		if( isset( $arrValues['release_date'] ) && $boolDirectSet ) $this->set( 'm_strReleaseDate', trim( $arrValues['release_date'] ) ); elseif( isset( $arrValues['release_date'] ) ) $this->setReleaseDate( $arrValues['release_date'] );
		if( isset( $arrValues['fixed_by'] ) && $boolDirectSet ) $this->set( 'm_intFixedBy', trim( $arrValues['fixed_by'] ) ); elseif( isset( $arrValues['fixed_by'] ) ) $this->setFixedBy( $arrValues['fixed_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setIlsEmailErrorTypeId( $intIlsEmailErrorTypeId ) {
		$this->set( 'm_intIlsEmailErrorTypeId', CStrings::strToIntDef( $intIlsEmailErrorTypeId, NULL, false ) );
	}

	public function getIlsEmailErrorTypeId() {
		return $this->m_intIlsEmailErrorTypeId;
	}

	public function sqlIlsEmailErrorTypeId() {
		return ( true == isset( $this->m_intIlsEmailErrorTypeId ) ) ? ( string ) $this->m_intIlsEmailErrorTypeId : 'NULL';
	}

	public function setFixDescription( $strFixDescription ) {
		$this->set( 'm_strFixDescription', CStrings::strTrimDef( $strFixDescription, 240, NULL, true ) );
	}

	public function getFixDescription() {
		return $this->m_strFixDescription;
	}

	public function sqlFixDescription() {
		return ( true == isset( $this->m_strFixDescription ) ) ? '\'' . addslashes( $this->m_strFixDescription ) . '\'' : 'NULL';
	}

	public function setReleaseDate( $strReleaseDate ) {
		$this->set( 'm_strReleaseDate', CStrings::strTrimDef( $strReleaseDate, -1, NULL, true ) );
	}

	public function getReleaseDate() {
		return $this->m_strReleaseDate;
	}

	public function sqlReleaseDate() {
		return ( true == isset( $this->m_strReleaseDate ) ) ? '\'' . $this->m_strReleaseDate . '\'' : 'NULL';
	}

	public function setFixedBy( $intFixedBy ) {
		$this->set( 'm_intFixedBy', CStrings::strToIntDef( $intFixedBy, NULL, false ) );
	}

	public function getFixedBy() {
		return $this->m_intFixedBy;
	}

	public function sqlFixedBy() {
		return ( true == isset( $this->m_intFixedBy ) ) ? ( string ) $this->m_intFixedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ils_email_error_type_id, fix_description, release_date, fixed_by, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlIlsEmailErrorTypeId() . ', ' .
 						$this->sqlFixDescription() . ', ' .
 						$this->sqlReleaseDate() . ', ' .
 						$this->sqlFixedBy() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ils_email_error_type_id = ' . $this->sqlIlsEmailErrorTypeId() . ','; } elseif( true == array_key_exists( 'IlsEmailErrorTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ils_email_error_type_id = ' . $this->sqlIlsEmailErrorTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fix_description = ' . $this->sqlFixDescription() . ','; } elseif( true == array_key_exists( 'FixDescription', $this->getChangedColumns() ) ) { $strSql .= ' fix_description = ' . $this->sqlFixDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_date = ' . $this->sqlReleaseDate() . ','; } elseif( true == array_key_exists( 'ReleaseDate', $this->getChangedColumns() ) ) { $strSql .= ' release_date = ' . $this->sqlReleaseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fixed_by = ' . $this->sqlFixedBy() . ','; } elseif( true == array_key_exists( 'FixedBy', $this->getChangedColumns() ) ) { $strSql .= ' fixed_by = ' . $this->sqlFixedBy() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ils_email_error_type_id' => $this->getIlsEmailErrorTypeId(),
			'fix_description' => $this->getFixDescription(),
			'release_date' => $this->getReleaseDate(),
			'fixed_by' => $this->getFixedBy(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>