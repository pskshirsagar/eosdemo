<?php

class CBaseEmployeeEncryptionPermission extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_encryption_permissions';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intReferenceId;
	protected $m_intReferenceTypeId;
	protected $m_intEncryptionSystemTypeId;
	protected $m_intHasReadAccess;
	protected $m_intHasWriteAccess;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intHasReadAccess = '0';
		$this->m_intHasWriteAccess = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['reference_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceTypeId', trim( $arrValues['reference_type_id'] ) ); elseif( isset( $arrValues['reference_type_id'] ) ) $this->setReferenceTypeId( $arrValues['reference_type_id'] );
		if( isset( $arrValues['encryption_system_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEncryptionSystemTypeId', trim( $arrValues['encryption_system_type_id'] ) ); elseif( isset( $arrValues['encryption_system_type_id'] ) ) $this->setEncryptionSystemTypeId( $arrValues['encryption_system_type_id'] );
		if( isset( $arrValues['has_read_access'] ) && $boolDirectSet ) $this->set( 'm_intHasReadAccess', trim( $arrValues['has_read_access'] ) ); elseif( isset( $arrValues['has_read_access'] ) ) $this->setHasReadAccess( $arrValues['has_read_access'] );
		if( isset( $arrValues['has_write_access'] ) && $boolDirectSet ) $this->set( 'm_intHasWriteAccess', trim( $arrValues['has_write_access'] ) ); elseif( isset( $arrValues['has_write_access'] ) ) $this->setHasWriteAccess( $arrValues['has_write_access'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setReferenceTypeId( $intReferenceTypeId ) {
		$this->set( 'm_intReferenceTypeId', CStrings::strToIntDef( $intReferenceTypeId, NULL, false ) );
	}

	public function getReferenceTypeId() {
		return $this->m_intReferenceTypeId;
	}

	public function sqlReferenceTypeId() {
		return ( true == isset( $this->m_intReferenceTypeId ) ) ? ( string ) $this->m_intReferenceTypeId : 'NULL';
	}

	public function setEncryptionSystemTypeId( $intEncryptionSystemTypeId ) {
		$this->set( 'm_intEncryptionSystemTypeId', CStrings::strToIntDef( $intEncryptionSystemTypeId, NULL, false ) );
	}

	public function getEncryptionSystemTypeId() {
		return $this->m_intEncryptionSystemTypeId;
	}

	public function sqlEncryptionSystemTypeId() {
		return ( true == isset( $this->m_intEncryptionSystemTypeId ) ) ? ( string ) $this->m_intEncryptionSystemTypeId : 'NULL';
	}

	public function setHasReadAccess( $intHasReadAccess ) {
		$this->set( 'm_intHasReadAccess', CStrings::strToIntDef( $intHasReadAccess, NULL, false ) );
	}

	public function getHasReadAccess() {
		return $this->m_intHasReadAccess;
	}

	public function sqlHasReadAccess() {
		return ( true == isset( $this->m_intHasReadAccess ) ) ? ( string ) $this->m_intHasReadAccess : '0';
	}

	public function setHasWriteAccess( $intHasWriteAccess ) {
		$this->set( 'm_intHasWriteAccess', CStrings::strToIntDef( $intHasWriteAccess, NULL, false ) );
	}

	public function getHasWriteAccess() {
		return $this->m_intHasWriteAccess;
	}

	public function sqlHasWriteAccess() {
		return ( true == isset( $this->m_intHasWriteAccess ) ) ? ( string ) $this->m_intHasWriteAccess : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, reference_id, reference_type_id, encryption_system_type_id, has_read_access, has_write_access, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlReferenceId() . ', ' .
 						$this->sqlReferenceTypeId() . ', ' .
 						$this->sqlEncryptionSystemTypeId() . ', ' .
 						$this->sqlHasReadAccess() . ', ' .
 						$this->sqlHasWriteAccess() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_type_id = ' . $this->sqlReferenceTypeId() . ','; } elseif( true == array_key_exists( 'ReferenceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' reference_type_id = ' . $this->sqlReferenceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' encryption_system_type_id = ' . $this->sqlEncryptionSystemTypeId() . ','; } elseif( true == array_key_exists( 'EncryptionSystemTypeId', $this->getChangedColumns() ) ) { $strSql .= ' encryption_system_type_id = ' . $this->sqlEncryptionSystemTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_read_access = ' . $this->sqlHasReadAccess() . ','; } elseif( true == array_key_exists( 'HasReadAccess', $this->getChangedColumns() ) ) { $strSql .= ' has_read_access = ' . $this->sqlHasReadAccess() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_write_access = ' . $this->sqlHasWriteAccess() . ','; } elseif( true == array_key_exists( 'HasWriteAccess', $this->getChangedColumns() ) ) { $strSql .= ' has_write_access = ' . $this->sqlHasWriteAccess() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'reference_id' => $this->getReferenceId(),
			'reference_type_id' => $this->getReferenceTypeId(),
			'encryption_system_type_id' => $this->getEncryptionSystemTypeId(),
			'has_read_access' => $this->getHasReadAccess(),
			'has_write_access' => $this->getHasWriteAccess(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>