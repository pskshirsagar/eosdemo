<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsAssetModels
 * Do not add any new functions to this class.
 */

class CBasePsAssetModels extends CEosPluralBase {

	/**
	 * @return CPsAssetModel[]
	 */
	public static function fetchPsAssetModels( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsAssetModel', $objDatabase );
	}

	/**
	 * @return CPsAssetModel
	 */
	public static function fetchPsAssetModel( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsAssetModel', $objDatabase );
	}

	public static function fetchPsAssetModelCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_asset_models', $objDatabase );
	}

	public static function fetchPsAssetModelById( $intId, $objDatabase ) {
		return self::fetchPsAssetModel( sprintf( 'SELECT * FROM ps_asset_models WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsAssetModelsByPsAssetTypeId( $intPsAssetTypeId, $objDatabase ) {
		return self::fetchPsAssetModels( sprintf( 'SELECT * FROM ps_asset_models WHERE ps_asset_type_id = %d', ( int ) $intPsAssetTypeId ), $objDatabase );
	}

	public static function fetchPsAssetModelsByPsAssetBrandId( $intPsAssetBrandId, $objDatabase ) {
		return self::fetchPsAssetModels( sprintf( 'SELECT * FROM ps_asset_models WHERE ps_asset_brand_id = %d', ( int ) $intPsAssetBrandId ), $objDatabase );
	}

}
?>