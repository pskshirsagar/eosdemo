<?php

class CBaseMassEmail extends CEosSingularBase {

	const TABLE_NAME = 'public.mass_emails';

	protected $m_intId;
	protected $m_intMassEmailTypeId;
	protected $m_strMassEmailSegmentIds;
	protected $m_intFrequencyId;
	protected $m_intEmployeeId;
	protected $m_strName;
	protected $m_strSubject;
	protected $m_strFromNameFull;
	protected $m_strFromEmailAddress;
	protected $m_strAdditionalRecipients;
	protected $m_strTextContent;
	protected $m_strHtmlContent;
	protected $m_strParsedHtmlContent;
	protected $m_strBeginDatetime;
	protected $m_intSendingTimezone;
	protected $m_strLastSentOn;
	protected $m_boolIsScheduled;
	protected $m_boolIsDisabled;
	protected $m_boolIsHtmlContent;
	protected $m_boolIsNewMassEmail;
	protected $m_boolIsListExcluded;
	protected $m_intSendCount;
	protected $m_intOpenCount;
	protected $m_intOrderNum;
	protected $m_intPreviewedBy;
	protected $m_strPreviewedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsScheduled = true;
		$this->m_boolIsDisabled = false;
		$this->m_boolIsHtmlContent = true;
		$this->m_boolIsNewMassEmail = false;
		$this->m_boolIsListExcluded = false;
		$this->m_intSendCount = '0';
		$this->m_intOpenCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['mass_email_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMassEmailTypeId', trim( $arrValues['mass_email_type_id'] ) ); elseif( isset( $arrValues['mass_email_type_id'] ) ) $this->setMassEmailTypeId( $arrValues['mass_email_type_id'] );
		if( isset( $arrValues['mass_email_segment_ids'] ) && $boolDirectSet ) $this->set( 'm_strMassEmailSegmentIds', trim( stripcslashes( $arrValues['mass_email_segment_ids'] ) ) ); elseif( isset( $arrValues['mass_email_segment_ids'] ) ) $this->setMassEmailSegmentIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mass_email_segment_ids'] ) : $arrValues['mass_email_segment_ids'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['subject'] ) && $boolDirectSet ) $this->set( 'm_strSubject', trim( stripcslashes( $arrValues['subject'] ) ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['subject'] ) : $arrValues['subject'] );
		if( isset( $arrValues['from_name_full'] ) && $boolDirectSet ) $this->set( 'm_strFromNameFull', trim( stripcslashes( $arrValues['from_name_full'] ) ) ); elseif( isset( $arrValues['from_name_full'] ) ) $this->setFromNameFull( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['from_name_full'] ) : $arrValues['from_name_full'] );
		if( isset( $arrValues['from_email_address'] ) && $boolDirectSet ) $this->set( 'm_strFromEmailAddress', trim( stripcslashes( $arrValues['from_email_address'] ) ) ); elseif( isset( $arrValues['from_email_address'] ) ) $this->setFromEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['from_email_address'] ) : $arrValues['from_email_address'] );
		if( isset( $arrValues['additional_recipients'] ) && $boolDirectSet ) $this->set( 'm_strAdditionalRecipients', trim( stripcslashes( $arrValues['additional_recipients'] ) ) ); elseif( isset( $arrValues['additional_recipients'] ) ) $this->setAdditionalRecipients( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['additional_recipients'] ) : $arrValues['additional_recipients'] );
		if( isset( $arrValues['text_content'] ) && $boolDirectSet ) $this->set( 'm_strTextContent', trim( stripcslashes( $arrValues['text_content'] ) ) ); elseif( isset( $arrValues['text_content'] ) ) $this->setTextContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['text_content'] ) : $arrValues['text_content'] );
		if( isset( $arrValues['html_content'] ) && $boolDirectSet ) $this->set( 'm_strHtmlContent', trim( stripcslashes( $arrValues['html_content'] ) ) ); elseif( isset( $arrValues['html_content'] ) ) $this->setHtmlContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['html_content'] ) : $arrValues['html_content'] );
		if( isset( $arrValues['parsed_html_content'] ) && $boolDirectSet ) $this->set( 'm_strParsedHtmlContent', trim( stripcslashes( $arrValues['parsed_html_content'] ) ) ); elseif( isset( $arrValues['parsed_html_content'] ) ) $this->setParsedHtmlContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['parsed_html_content'] ) : $arrValues['parsed_html_content'] );
		if( isset( $arrValues['begin_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBeginDatetime', trim( $arrValues['begin_datetime'] ) ); elseif( isset( $arrValues['begin_datetime'] ) ) $this->setBeginDatetime( $arrValues['begin_datetime'] );
		if( isset( $arrValues['sending_timezone'] ) && $boolDirectSet ) $this->set( 'm_intSendingTimezone', trim( $arrValues['sending_timezone'] ) ); elseif( isset( $arrValues['sending_timezone'] ) ) $this->setSendingTimezone( $arrValues['sending_timezone'] );
		if( isset( $arrValues['last_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strLastSentOn', trim( $arrValues['last_sent_on'] ) ); elseif( isset( $arrValues['last_sent_on'] ) ) $this->setLastSentOn( $arrValues['last_sent_on'] );
		if( isset( $arrValues['is_scheduled'] ) && $boolDirectSet ) $this->set( 'm_boolIsScheduled', trim( stripcslashes( $arrValues['is_scheduled'] ) ) ); elseif( isset( $arrValues['is_scheduled'] ) ) $this->setIsScheduled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_scheduled'] ) : $arrValues['is_scheduled'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_boolIsDisabled', trim( stripcslashes( $arrValues['is_disabled'] ) ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_disabled'] ) : $arrValues['is_disabled'] );
		if( isset( $arrValues['is_html_content'] ) && $boolDirectSet ) $this->set( 'm_boolIsHtmlContent', trim( stripcslashes( $arrValues['is_html_content'] ) ) ); elseif( isset( $arrValues['is_html_content'] ) ) $this->setIsHtmlContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_html_content'] ) : $arrValues['is_html_content'] );
		if( isset( $arrValues['is_new_mass_email'] ) && $boolDirectSet ) $this->set( 'm_boolIsNewMassEmail', trim( stripcslashes( $arrValues['is_new_mass_email'] ) ) ); elseif( isset( $arrValues['is_new_mass_email'] ) ) $this->setIsNewMassEmail( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_new_mass_email'] ) : $arrValues['is_new_mass_email'] );
		if( isset( $arrValues['is_list_excluded'] ) && $boolDirectSet ) $this->set( 'm_boolIsListExcluded', trim( stripcslashes( $arrValues['is_list_excluded'] ) ) ); elseif( isset( $arrValues['is_list_excluded'] ) ) $this->setIsListExcluded( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_list_excluded'] ) : $arrValues['is_list_excluded'] );
		if( isset( $arrValues['send_count'] ) && $boolDirectSet ) $this->set( 'm_intSendCount', trim( $arrValues['send_count'] ) ); elseif( isset( $arrValues['send_count'] ) ) $this->setSendCount( $arrValues['send_count'] );
		if( isset( $arrValues['open_count'] ) && $boolDirectSet ) $this->set( 'm_intOpenCount', trim( $arrValues['open_count'] ) ); elseif( isset( $arrValues['open_count'] ) ) $this->setOpenCount( $arrValues['open_count'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['previewed_by'] ) && $boolDirectSet ) $this->set( 'm_intPreviewedBy', trim( $arrValues['previewed_by'] ) ); elseif( isset( $arrValues['previewed_by'] ) ) $this->setPreviewedBy( $arrValues['previewed_by'] );
		if( isset( $arrValues['previewed_on'] ) && $boolDirectSet ) $this->set( 'm_strPreviewedOn', trim( $arrValues['previewed_on'] ) ); elseif( isset( $arrValues['previewed_on'] ) ) $this->setPreviewedOn( $arrValues['previewed_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMassEmailTypeId( $intMassEmailTypeId ) {
		$this->set( 'm_intMassEmailTypeId', CStrings::strToIntDef( $intMassEmailTypeId, NULL, false ) );
	}

	public function getMassEmailTypeId() {
		return $this->m_intMassEmailTypeId;
	}

	public function sqlMassEmailTypeId() {
		return ( true == isset( $this->m_intMassEmailTypeId ) ) ? ( string ) $this->m_intMassEmailTypeId : 'NULL';
	}

	public function setMassEmailSegmentIds( $strMassEmailSegmentIds ) {
		$this->set( 'm_strMassEmailSegmentIds', CStrings::strTrimDef( $strMassEmailSegmentIds, -1, NULL, true ) );
	}

	public function getMassEmailSegmentIds() {
		return $this->m_strMassEmailSegmentIds;
	}

	public function sqlMassEmailSegmentIds() {
		return ( true == isset( $this->m_strMassEmailSegmentIds ) ) ? '\'' . addslashes( $this->m_strMassEmailSegmentIds ) . '\'' : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setSubject( $strSubject ) {
		$this->set( 'm_strSubject', CStrings::strTrimDef( $strSubject, 240, NULL, true ) );
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? '\'' . addslashes( $this->m_strSubject ) . '\'' : 'NULL';
	}

	public function setFromNameFull( $strFromNameFull ) {
		$this->set( 'm_strFromNameFull', CStrings::strTrimDef( $strFromNameFull, 240, NULL, true ) );
	}

	public function getFromNameFull() {
		return $this->m_strFromNameFull;
	}

	public function sqlFromNameFull() {
		return ( true == isset( $this->m_strFromNameFull ) ) ? '\'' . addslashes( $this->m_strFromNameFull ) . '\'' : 'NULL';
	}

	public function setFromEmailAddress( $strFromEmailAddress ) {
		$this->set( 'm_strFromEmailAddress', CStrings::strTrimDef( $strFromEmailAddress, 240, NULL, true ) );
	}

	public function getFromEmailAddress() {
		return $this->m_strFromEmailAddress;
	}

	public function sqlFromEmailAddress() {
		return ( true == isset( $this->m_strFromEmailAddress ) ) ? '\'' . addslashes( $this->m_strFromEmailAddress ) . '\'' : 'NULL';
	}

	public function setAdditionalRecipients( $strAdditionalRecipients ) {
		$this->set( 'm_strAdditionalRecipients', CStrings::strTrimDef( $strAdditionalRecipients, -1, NULL, true ) );
	}

	public function getAdditionalRecipients() {
		return $this->m_strAdditionalRecipients;
	}

	public function sqlAdditionalRecipients() {
		return ( true == isset( $this->m_strAdditionalRecipients ) ) ? '\'' . addslashes( $this->m_strAdditionalRecipients ) . '\'' : 'NULL';
	}

	public function setTextContent( $strTextContent ) {
		$this->set( 'm_strTextContent', CStrings::strTrimDef( $strTextContent, -1, NULL, true ) );
	}

	public function getTextContent() {
		return $this->m_strTextContent;
	}

	public function sqlTextContent() {
		return ( true == isset( $this->m_strTextContent ) ) ? '\'' . addslashes( $this->m_strTextContent ) . '\'' : 'NULL';
	}

	public function setHtmlContent( $strHtmlContent ) {
		$this->set( 'm_strHtmlContent', CStrings::strTrimDef( $strHtmlContent, -1, NULL, true ) );
	}

	public function getHtmlContent() {
		return $this->m_strHtmlContent;
	}

	public function sqlHtmlContent() {
		return ( true == isset( $this->m_strHtmlContent ) ) ? '\'' . addslashes( $this->m_strHtmlContent ) . '\'' : 'NULL';
	}

	public function setParsedHtmlContent( $strParsedHtmlContent ) {
		$this->set( 'm_strParsedHtmlContent', CStrings::strTrimDef( $strParsedHtmlContent, -1, NULL, true ) );
	}

	public function getParsedHtmlContent() {
		return $this->m_strParsedHtmlContent;
	}

	public function sqlParsedHtmlContent() {
		return ( true == isset( $this->m_strParsedHtmlContent ) ) ? '\'' . addslashes( $this->m_strParsedHtmlContent ) . '\'' : 'NULL';
	}

	public function setBeginDatetime( $strBeginDatetime ) {
		$this->set( 'm_strBeginDatetime', CStrings::strTrimDef( $strBeginDatetime, -1, NULL, true ) );
	}

	public function getBeginDatetime() {
		return $this->m_strBeginDatetime;
	}

	public function sqlBeginDatetime() {
		return ( true == isset( $this->m_strBeginDatetime ) ) ? '\'' . $this->m_strBeginDatetime . '\'' : 'NULL';
	}

	public function setSendingTimezone( $intSendingTimezone ) {
		$this->set( 'm_intSendingTimezone', CStrings::strToIntDef( $intSendingTimezone, NULL, false ) );
	}

	public function getSendingTimezone() {
		return $this->m_intSendingTimezone;
	}

	public function sqlSendingTimezone() {
		return ( true == isset( $this->m_intSendingTimezone ) ) ? ( string ) $this->m_intSendingTimezone : 'NULL';
	}

	public function setLastSentOn( $strLastSentOn ) {
		$this->set( 'm_strLastSentOn', CStrings::strTrimDef( $strLastSentOn, -1, NULL, true ) );
	}

	public function getLastSentOn() {
		return $this->m_strLastSentOn;
	}

	public function sqlLastSentOn() {
		return ( true == isset( $this->m_strLastSentOn ) ) ? '\'' . $this->m_strLastSentOn . '\'' : 'NULL';
	}

	public function setIsScheduled( $boolIsScheduled ) {
		$this->set( 'm_boolIsScheduled', CStrings::strToBool( $boolIsScheduled ) );
	}

	public function getIsScheduled() {
		return $this->m_boolIsScheduled;
	}

	public function sqlIsScheduled() {
		return ( true == isset( $this->m_boolIsScheduled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsScheduled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $boolIsDisabled ) {
		$this->set( 'm_boolIsDisabled', CStrings::strToBool( $boolIsDisabled ) );
	}

	public function getIsDisabled() {
		return $this->m_boolIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_boolIsDisabled ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDisabled ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsHtmlContent( $boolIsHtmlContent ) {
		$this->set( 'm_boolIsHtmlContent', CStrings::strToBool( $boolIsHtmlContent ) );
	}

	public function getIsHtmlContent() {
		return $this->m_boolIsHtmlContent;
	}

	public function sqlIsHtmlContent() {
		return ( true == isset( $this->m_boolIsHtmlContent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHtmlContent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsNewMassEmail( $boolIsNewMassEmail ) {
		$this->set( 'm_boolIsNewMassEmail', CStrings::strToBool( $boolIsNewMassEmail ) );
	}

	public function getIsNewMassEmail() {
		return $this->m_boolIsNewMassEmail;
	}

	public function sqlIsNewMassEmail() {
		return ( true == isset( $this->m_boolIsNewMassEmail ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNewMassEmail ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsListExcluded( $boolIsListExcluded ) {
		$this->set( 'm_boolIsListExcluded', CStrings::strToBool( $boolIsListExcluded ) );
	}

	public function getIsListExcluded() {
		return $this->m_boolIsListExcluded;
	}

	public function sqlIsListExcluded() {
		return ( true == isset( $this->m_boolIsListExcluded ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsListExcluded ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSendCount( $intSendCount ) {
		$this->set( 'm_intSendCount', CStrings::strToIntDef( $intSendCount, NULL, false ) );
	}

	public function getSendCount() {
		return $this->m_intSendCount;
	}

	public function sqlSendCount() {
		return ( true == isset( $this->m_intSendCount ) ) ? ( string ) $this->m_intSendCount : '0';
	}

	public function setOpenCount( $intOpenCount ) {
		$this->set( 'm_intOpenCount', CStrings::strToIntDef( $intOpenCount, NULL, false ) );
	}

	public function getOpenCount() {
		return $this->m_intOpenCount;
	}

	public function sqlOpenCount() {
		return ( true == isset( $this->m_intOpenCount ) ) ? ( string ) $this->m_intOpenCount : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setPreviewedBy( $intPreviewedBy ) {
		$this->set( 'm_intPreviewedBy', CStrings::strToIntDef( $intPreviewedBy, NULL, false ) );
	}

	public function getPreviewedBy() {
		return $this->m_intPreviewedBy;
	}

	public function sqlPreviewedBy() {
		return ( true == isset( $this->m_intPreviewedBy ) ) ? ( string ) $this->m_intPreviewedBy : 'NULL';
	}

	public function setPreviewedOn( $strPreviewedOn ) {
		$this->set( 'm_strPreviewedOn', CStrings::strTrimDef( $strPreviewedOn, -1, NULL, true ) );
	}

	public function getPreviewedOn() {
		return $this->m_strPreviewedOn;
	}

	public function sqlPreviewedOn() {
		return ( true == isset( $this->m_strPreviewedOn ) ) ? '\'' . $this->m_strPreviewedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, mass_email_type_id, mass_email_segment_ids, frequency_id, employee_id, name, subject, from_name_full, from_email_address, additional_recipients, text_content, html_content, parsed_html_content, begin_datetime, sending_timezone, last_sent_on, is_scheduled, is_disabled, is_html_content, is_new_mass_email, is_list_excluded, send_count, open_count, order_num, previewed_by, previewed_on, approved_by, approved_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlMassEmailTypeId() . ', ' .
 						$this->sqlMassEmailSegmentIds() . ', ' .
 						$this->sqlFrequencyId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlSubject() . ', ' .
 						$this->sqlFromNameFull() . ', ' .
 						$this->sqlFromEmailAddress() . ', ' .
 						$this->sqlAdditionalRecipients() . ', ' .
 						$this->sqlTextContent() . ', ' .
 						$this->sqlHtmlContent() . ', ' .
 						$this->sqlParsedHtmlContent() . ', ' .
 						$this->sqlBeginDatetime() . ', ' .
 						$this->sqlSendingTimezone() . ', ' .
 						$this->sqlLastSentOn() . ', ' .
 						$this->sqlIsScheduled() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
 						$this->sqlIsHtmlContent() . ', ' .
 						$this->sqlIsNewMassEmail() . ', ' .
 						$this->sqlIsListExcluded() . ', ' .
 						$this->sqlSendCount() . ', ' .
 						$this->sqlOpenCount() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlPreviewedBy() . ', ' .
 						$this->sqlPreviewedOn() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mass_email_type_id = ' . $this->sqlMassEmailTypeId() . ','; } elseif( true == array_key_exists( 'MassEmailTypeId', $this->getChangedColumns() ) ) { $strSql .= ' mass_email_type_id = ' . $this->sqlMassEmailTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mass_email_segment_ids = ' . $this->sqlMassEmailSegmentIds() . ','; } elseif( true == array_key_exists( 'MassEmailSegmentIds', $this->getChangedColumns() ) ) { $strSql .= ' mass_email_segment_ids = ' . $this->sqlMassEmailSegmentIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_name_full = ' . $this->sqlFromNameFull() . ','; } elseif( true == array_key_exists( 'FromNameFull', $this->getChangedColumns() ) ) { $strSql .= ' from_name_full = ' . $this->sqlFromNameFull() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_email_address = ' . $this->sqlFromEmailAddress() . ','; } elseif( true == array_key_exists( 'FromEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' from_email_address = ' . $this->sqlFromEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_recipients = ' . $this->sqlAdditionalRecipients() . ','; } elseif( true == array_key_exists( 'AdditionalRecipients', $this->getChangedColumns() ) ) { $strSql .= ' additional_recipients = ' . $this->sqlAdditionalRecipients() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' text_content = ' . $this->sqlTextContent() . ','; } elseif( true == array_key_exists( 'TextContent', $this->getChangedColumns() ) ) { $strSql .= ' text_content = ' . $this->sqlTextContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' html_content = ' . $this->sqlHtmlContent() . ','; } elseif( true == array_key_exists( 'HtmlContent', $this->getChangedColumns() ) ) { $strSql .= ' html_content = ' . $this->sqlHtmlContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parsed_html_content = ' . $this->sqlParsedHtmlContent() . ','; } elseif( true == array_key_exists( 'ParsedHtmlContent', $this->getChangedColumns() ) ) { $strSql .= ' parsed_html_content = ' . $this->sqlParsedHtmlContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_datetime = ' . $this->sqlBeginDatetime() . ','; } elseif( true == array_key_exists( 'BeginDatetime', $this->getChangedColumns() ) ) { $strSql .= ' begin_datetime = ' . $this->sqlBeginDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sending_timezone = ' . $this->sqlSendingTimezone() . ','; } elseif( true == array_key_exists( 'SendingTimezone', $this->getChangedColumns() ) ) { $strSql .= ' sending_timezone = ' . $this->sqlSendingTimezone() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_sent_on = ' . $this->sqlLastSentOn() . ','; } elseif( true == array_key_exists( 'LastSentOn', $this->getChangedColumns() ) ) { $strSql .= ' last_sent_on = ' . $this->sqlLastSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_scheduled = ' . $this->sqlIsScheduled() . ','; } elseif( true == array_key_exists( 'IsScheduled', $this->getChangedColumns() ) ) { $strSql .= ' is_scheduled = ' . $this->sqlIsScheduled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_html_content = ' . $this->sqlIsHtmlContent() . ','; } elseif( true == array_key_exists( 'IsHtmlContent', $this->getChangedColumns() ) ) { $strSql .= ' is_html_content = ' . $this->sqlIsHtmlContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_new_mass_email = ' . $this->sqlIsNewMassEmail() . ','; } elseif( true == array_key_exists( 'IsNewMassEmail', $this->getChangedColumns() ) ) { $strSql .= ' is_new_mass_email = ' . $this->sqlIsNewMassEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_list_excluded = ' . $this->sqlIsListExcluded() . ','; } elseif( true == array_key_exists( 'IsListExcluded', $this->getChangedColumns() ) ) { $strSql .= ' is_list_excluded = ' . $this->sqlIsListExcluded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' send_count = ' . $this->sqlSendCount() . ','; } elseif( true == array_key_exists( 'SendCount', $this->getChangedColumns() ) ) { $strSql .= ' send_count = ' . $this->sqlSendCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' open_count = ' . $this->sqlOpenCount() . ','; } elseif( true == array_key_exists( 'OpenCount', $this->getChangedColumns() ) ) { $strSql .= ' open_count = ' . $this->sqlOpenCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previewed_by = ' . $this->sqlPreviewedBy() . ','; } elseif( true == array_key_exists( 'PreviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' previewed_by = ' . $this->sqlPreviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previewed_on = ' . $this->sqlPreviewedOn() . ','; } elseif( true == array_key_exists( 'PreviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' previewed_on = ' . $this->sqlPreviewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'mass_email_type_id' => $this->getMassEmailTypeId(),
			'mass_email_segment_ids' => $this->getMassEmailSegmentIds(),
			'frequency_id' => $this->getFrequencyId(),
			'employee_id' => $this->getEmployeeId(),
			'name' => $this->getName(),
			'subject' => $this->getSubject(),
			'from_name_full' => $this->getFromNameFull(),
			'from_email_address' => $this->getFromEmailAddress(),
			'additional_recipients' => $this->getAdditionalRecipients(),
			'text_content' => $this->getTextContent(),
			'html_content' => $this->getHtmlContent(),
			'parsed_html_content' => $this->getParsedHtmlContent(),
			'begin_datetime' => $this->getBeginDatetime(),
			'sending_timezone' => $this->getSendingTimezone(),
			'last_sent_on' => $this->getLastSentOn(),
			'is_scheduled' => $this->getIsScheduled(),
			'is_disabled' => $this->getIsDisabled(),
			'is_html_content' => $this->getIsHtmlContent(),
			'is_new_mass_email' => $this->getIsNewMassEmail(),
			'is_list_excluded' => $this->getIsListExcluded(),
			'send_count' => $this->getSendCount(),
			'open_count' => $this->getOpenCount(),
			'order_num' => $this->getOrderNum(),
			'previewed_by' => $this->getPreviewedBy(),
			'previewed_on' => $this->getPreviewedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>