<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveys
 * Do not add any new functions to this class.
 */

class CBaseSurveys extends CEosPluralBase {

	/**
	 * @return CSurvey[]
	 */
	public static function fetchSurveys( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurvey', $objDatabase );
	}

	/**
	 * @return CSurvey
	 */
	public static function fetchSurvey( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurvey', $objDatabase );
	}

	public static function fetchSurveyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'surveys', $objDatabase );
	}

	public static function fetchSurveyById( $intId, $objDatabase ) {
		return self::fetchSurvey( sprintf( 'SELECT * FROM surveys WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveysByScheduledSurveyId( $intScheduledSurveyId, $objDatabase ) {
		return self::fetchSurveys( sprintf( 'SELECT * FROM surveys WHERE scheduled_survey_id = %d', ( int ) $intScheduledSurveyId ), $objDatabase );
	}

	public static function fetchSurveysBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		return self::fetchSurveys( sprintf( 'SELECT * FROM surveys WHERE survey_template_id = %d', ( int ) $intSurveyTemplateId ), $objDatabase );
	}

	public static function fetchSurveysBySurveyTypeId( $intSurveyTypeId, $objDatabase ) {
		return self::fetchSurveys( sprintf( 'SELECT * FROM surveys WHERE survey_type_id = %d', ( int ) $intSurveyTypeId ), $objDatabase );
	}

	public static function fetchSurveysByPsLeadId( $intPsLeadId, $objDatabase ) {
		return self::fetchSurveys( sprintf( 'SELECT * FROM surveys WHERE ps_lead_id = %d', ( int ) $intPsLeadId ), $objDatabase );
	}

	public static function fetchSurveysByTriggerReferenceId( $intTriggerReferenceId, $objDatabase ) {
		return self::fetchSurveys( sprintf( 'SELECT * FROM surveys WHERE trigger_reference_id = %d', ( int ) $intTriggerReferenceId ), $objDatabase );
	}

	public static function fetchSurveysByResponderReferenceId( $intResponderReferenceId, $objDatabase ) {
		return self::fetchSurveys( sprintf( 'SELECT * FROM surveys WHERE responder_reference_id = %d', ( int ) $intResponderReferenceId ), $objDatabase );
	}

	public static function fetchSurveysBySubjectReferenceId( $intSubjectReferenceId, $objDatabase ) {
		return self::fetchSurveys( sprintf( 'SELECT * FROM surveys WHERE subject_reference_id = %d', ( int ) $intSubjectReferenceId ), $objDatabase );
	}

}
?>