<?php

class CBaseContractTerminationEvent extends CEosSingularBase {

	const TABLE_NAME = 'public.contract_termination_events';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intContractTerminationRequestId;
	protected $m_intContractTerminationEventTypeId;
	protected $m_intContractTerminationEventStatusId;
	protected $m_strEffectiveDate;
	protected $m_intContractPropertyId;
	protected $m_strEventParameters;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intContractTerminationEventStatusId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['contract_termination_request_id'] ) && $boolDirectSet ) $this->set( 'm_intContractTerminationRequestId', trim( $arrValues['contract_termination_request_id'] ) ); elseif( isset( $arrValues['contract_termination_request_id'] ) ) $this->setContractTerminationRequestId( $arrValues['contract_termination_request_id'] );
		if( isset( $arrValues['contract_termination_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intContractTerminationEventTypeId', trim( $arrValues['contract_termination_event_type_id'] ) ); elseif( isset( $arrValues['contract_termination_event_type_id'] ) ) $this->setContractTerminationEventTypeId( $arrValues['contract_termination_event_type_id'] );
		if( isset( $arrValues['contract_termination_event_status_id'] ) && $boolDirectSet ) $this->set( 'm_intContractTerminationEventStatusId', trim( $arrValues['contract_termination_event_status_id'] ) ); elseif( isset( $arrValues['contract_termination_event_status_id'] ) ) $this->setContractTerminationEventStatusId( $arrValues['contract_termination_event_status_id'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intContractPropertyId', trim( $arrValues['contract_property_id'] ) ); elseif( isset( $arrValues['contract_property_id'] ) ) $this->setContractPropertyId( $arrValues['contract_property_id'] );
		if( isset( $arrValues['event_parameters'] ) && $boolDirectSet ) $this->set( 'm_strEventParameters', trim( stripcslashes( $arrValues['event_parameters'] ) ) ); elseif( isset( $arrValues['event_parameters'] ) ) $this->setEventParameters( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['event_parameters'] ) : $arrValues['event_parameters'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setContractTerminationRequestId( $intContractTerminationRequestId ) {
		$this->set( 'm_intContractTerminationRequestId', CStrings::strToIntDef( $intContractTerminationRequestId, NULL, false ) );
	}

	public function getContractTerminationRequestId() {
		return $this->m_intContractTerminationRequestId;
	}

	public function sqlContractTerminationRequestId() {
		return ( true == isset( $this->m_intContractTerminationRequestId ) ) ? ( string ) $this->m_intContractTerminationRequestId : 'NULL';
	}

	public function setContractTerminationEventTypeId( $intContractTerminationEventTypeId ) {
		$this->set( 'm_intContractTerminationEventTypeId', CStrings::strToIntDef( $intContractTerminationEventTypeId, NULL, false ) );
	}

	public function getContractTerminationEventTypeId() {
		return $this->m_intContractTerminationEventTypeId;
	}

	public function sqlContractTerminationEventTypeId() {
		return ( true == isset( $this->m_intContractTerminationEventTypeId ) ) ? ( string ) $this->m_intContractTerminationEventTypeId : 'NULL';
	}

	public function setContractTerminationEventStatusId( $intContractTerminationEventStatusId ) {
		$this->set( 'm_intContractTerminationEventStatusId', CStrings::strToIntDef( $intContractTerminationEventStatusId, NULL, false ) );
	}

	public function getContractTerminationEventStatusId() {
		return $this->m_intContractTerminationEventStatusId;
	}

	public function sqlContractTerminationEventStatusId() {
		return ( true == isset( $this->m_intContractTerminationEventStatusId ) ) ? ( string ) $this->m_intContractTerminationEventStatusId : '1';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NOW()';
	}

	public function setContractPropertyId( $intContractPropertyId ) {
		$this->set( 'm_intContractPropertyId', CStrings::strToIntDef( $intContractPropertyId, NULL, false ) );
	}

	public function getContractPropertyId() {
		return $this->m_intContractPropertyId;
	}

	public function sqlContractPropertyId() {
		return ( true == isset( $this->m_intContractPropertyId ) ) ? ( string ) $this->m_intContractPropertyId : 'NULL';
	}

	public function setEventParameters( $strEventParameters ) {
		$this->set( 'm_strEventParameters', CStrings::strTrimDef( $strEventParameters, -1, NULL, true ) );
	}

	public function getEventParameters() {
		return $this->m_strEventParameters;
	}

	public function sqlEventParameters() {
		return ( true == isset( $this->m_strEventParameters ) ) ? '\'' . addslashes( $this->m_strEventParameters ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, contract_termination_request_id, contract_termination_event_type_id, contract_termination_event_status_id, effective_date, contract_property_id, event_parameters, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlContractTerminationRequestId() . ', ' .
 						$this->sqlContractTerminationEventTypeId() . ', ' .
 						$this->sqlContractTerminationEventStatusId() . ', ' .
 						$this->sqlEffectiveDate() . ', ' .
 						$this->sqlContractPropertyId() . ', ' .
 						$this->sqlEventParameters() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_termination_request_id = ' . $this->sqlContractTerminationRequestId() . ','; } elseif( true == array_key_exists( 'ContractTerminationRequestId', $this->getChangedColumns() ) ) { $strSql .= ' contract_termination_request_id = ' . $this->sqlContractTerminationRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_termination_event_type_id = ' . $this->sqlContractTerminationEventTypeId() . ','; } elseif( true == array_key_exists( 'ContractTerminationEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' contract_termination_event_type_id = ' . $this->sqlContractTerminationEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_termination_event_status_id = ' . $this->sqlContractTerminationEventStatusId() . ','; } elseif( true == array_key_exists( 'ContractTerminationEventStatusId', $this->getChangedColumns() ) ) { $strSql .= ' contract_termination_event_status_id = ' . $this->sqlContractTerminationEventStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_property_id = ' . $this->sqlContractPropertyId() . ','; } elseif( true == array_key_exists( 'ContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' contract_property_id = ' . $this->sqlContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' event_parameters = ' . $this->sqlEventParameters() . ','; } elseif( true == array_key_exists( 'EventParameters', $this->getChangedColumns() ) ) { $strSql .= ' event_parameters = ' . $this->sqlEventParameters() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'contract_termination_request_id' => $this->getContractTerminationRequestId(),
			'contract_termination_event_type_id' => $this->getContractTerminationEventTypeId(),
			'contract_termination_event_status_id' => $this->getContractTerminationEventStatusId(),
			'effective_date' => $this->getEffectiveDate(),
			'contract_property_id' => $this->getContractPropertyId(),
			'event_parameters' => $this->getEventParameters(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>