<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationReferences
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationReferences extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationReference[]
	 */
	public static function fetchEmployeeApplicationReferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationReference', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationReference
	 */
	public static function fetchEmployeeApplicationReference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationReference', $objDatabase );
	}

	public static function fetchEmployeeApplicationReferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_references', $objDatabase );
	}

	public static function fetchEmployeeApplicationReferenceById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationReference( sprintf( 'SELECT * FROM employee_application_references WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationReferencesByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchEmployeeApplicationReferences( sprintf( 'SELECT * FROM employee_application_references WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationReferencesByContactedEmployeeId( $intContactedEmployeeId, $objDatabase ) {
		return self::fetchEmployeeApplicationReferences( sprintf( 'SELECT * FROM employee_application_references WHERE contacted_employee_id = %d', ( int ) $intContactedEmployeeId ), $objDatabase );
	}

}
?>