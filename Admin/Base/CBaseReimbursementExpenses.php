<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReimbursementExpenses
 * Do not add any new functions to this class.
 */

class CBaseReimbursementExpenses extends CEosPluralBase {

	/**
	 * @return CReimbursementExpense[]
	 */
	public static function fetchReimbursementExpenses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CReimbursementExpense', $objDatabase );
	}

	/**
	 * @return CReimbursementExpense
	 */
	public static function fetchReimbursementExpense( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReimbursementExpense', $objDatabase );
	}

	public static function fetchReimbursementExpenseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reimbursement_expenses', $objDatabase );
	}

	public static function fetchReimbursementExpenseById( $intId, $objDatabase ) {
		return self::fetchReimbursementExpense( sprintf( 'SELECT * FROM reimbursement_expenses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReimbursementExpensesByReimbursementRequestId( $intReimbursementRequestId, $objDatabase ) {
		return self::fetchReimbursementExpenses( sprintf( 'SELECT * FROM reimbursement_expenses WHERE reimbursement_request_id = %d', ( int ) $intReimbursementRequestId ), $objDatabase );
	}

	public static function fetchReimbursementExpensesByEmployeeExpenseTypeId( $intEmployeeExpenseTypeId, $objDatabase ) {
		return self::fetchReimbursementExpenses( sprintf( 'SELECT * FROM reimbursement_expenses WHERE employee_expense_type_id = %d', ( int ) $intEmployeeExpenseTypeId ), $objDatabase );
	}

	public static function fetchReimbursementExpensesByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchReimbursementExpenses( sprintf( 'SELECT * FROM reimbursement_expenses WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchReimbursementExpensesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchReimbursementExpenses( sprintf( 'SELECT * FROM reimbursement_expenses WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>