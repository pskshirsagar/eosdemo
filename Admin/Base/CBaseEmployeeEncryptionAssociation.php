<?php

class CBaseEmployeeEncryptionAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_encryption_associations';

	protected $m_intId;
	protected $m_intViewerEmployeeId;
	protected $m_intReferenceId;
	protected $m_intEncryptionSystemTypeId;
	protected $m_strEncryptedAmount;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['viewer_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intViewerEmployeeId', trim( $arrValues['viewer_employee_id'] ) ); elseif( isset( $arrValues['viewer_employee_id'] ) ) $this->setViewerEmployeeId( $arrValues['viewer_employee_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['encryption_system_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEncryptionSystemTypeId', trim( $arrValues['encryption_system_type_id'] ) ); elseif( isset( $arrValues['encryption_system_type_id'] ) ) $this->setEncryptionSystemTypeId( $arrValues['encryption_system_type_id'] );
		if( isset( $arrValues['encrypted_amount'] ) && $boolDirectSet ) $this->set( 'm_strEncryptedAmount', trim( stripcslashes( $arrValues['encrypted_amount'] ) ) ); elseif( isset( $arrValues['encrypted_amount'] ) ) $this->setEncryptedAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['encrypted_amount'] ) : $arrValues['encrypted_amount'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setViewerEmployeeId( $intViewerEmployeeId ) {
		$this->set( 'm_intViewerEmployeeId', CStrings::strToIntDef( $intViewerEmployeeId, NULL, false ) );
	}

	public function getViewerEmployeeId() {
		return $this->m_intViewerEmployeeId;
	}

	public function sqlViewerEmployeeId() {
		return ( true == isset( $this->m_intViewerEmployeeId ) ) ? ( string ) $this->m_intViewerEmployeeId : 'NULL';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setEncryptionSystemTypeId( $intEncryptionSystemTypeId ) {
		$this->set( 'm_intEncryptionSystemTypeId', CStrings::strToIntDef( $intEncryptionSystemTypeId, NULL, false ) );
	}

	public function getEncryptionSystemTypeId() {
		return $this->m_intEncryptionSystemTypeId;
	}

	public function sqlEncryptionSystemTypeId() {
		return ( true == isset( $this->m_intEncryptionSystemTypeId ) ) ? ( string ) $this->m_intEncryptionSystemTypeId : 'NULL';
	}

	public function setEncryptedAmount( $strEncryptedAmount ) {
		$this->set( 'm_strEncryptedAmount', CStrings::strTrimDef( $strEncryptedAmount, -1, NULL, true ) );
	}

	public function getEncryptedAmount() {
		return $this->m_strEncryptedAmount;
	}

	public function sqlEncryptedAmount() {
		return ( true == isset( $this->m_strEncryptedAmount ) ) ? '\'' . addslashes( $this->m_strEncryptedAmount ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, viewer_employee_id, reference_id, encryption_system_type_id, encrypted_amount, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlViewerEmployeeId() . ', ' .
 						$this->sqlReferenceId() . ', ' .
 						$this->sqlEncryptionSystemTypeId() . ', ' .
 						$this->sqlEncryptedAmount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewer_employee_id = ' . $this->sqlViewerEmployeeId() . ','; } elseif( true == array_key_exists( 'ViewerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' viewer_employee_id = ' . $this->sqlViewerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' encryption_system_type_id = ' . $this->sqlEncryptionSystemTypeId() . ','; } elseif( true == array_key_exists( 'EncryptionSystemTypeId', $this->getChangedColumns() ) ) { $strSql .= ' encryption_system_type_id = ' . $this->sqlEncryptionSystemTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' encrypted_amount = ' . $this->sqlEncryptedAmount() . ','; } elseif( true == array_key_exists( 'EncryptedAmount', $this->getChangedColumns() ) ) { $strSql .= ' encrypted_amount = ' . $this->sqlEncryptedAmount() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'viewer_employee_id' => $this->getViewerEmployeeId(),
			'reference_id' => $this->getReferenceId(),
			'encryption_system_type_id' => $this->getEncryptionSystemTypeId(),
			'encrypted_amount' => $this->getEncryptedAmount(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>