<?php

class CBaseApiToken extends CEosSingularBase {

	const TABLE_NAME = 'public.api_tokens';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intApiTokenTypeId;
	protected $m_intEntityReferenceTypeId;
	protected $m_intEntityReferenceId;
	protected $m_strDeviceIdentifier;
	protected $m_intOauthClientId;
	protected $m_intOauthClientTypeId;
	protected $m_strToken;
	protected $m_strExpires;
	protected $m_strNotes;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intApiTokenTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['api_token_type_id'] ) && $boolDirectSet ) $this->set( 'm_intApiTokenTypeId', trim( $arrValues['api_token_type_id'] ) ); elseif( isset( $arrValues['api_token_type_id'] ) ) $this->setApiTokenTypeId( $arrValues['api_token_type_id'] );
		if( isset( $arrValues['entity_reference_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEntityReferenceTypeId', trim( $arrValues['entity_reference_type_id'] ) ); elseif( isset( $arrValues['entity_reference_type_id'] ) ) $this->setEntityReferenceTypeId( $arrValues['entity_reference_type_id'] );
		if( isset( $arrValues['entity_reference_id'] ) && $boolDirectSet ) $this->set( 'm_intEntityReferenceId', trim( $arrValues['entity_reference_id'] ) ); elseif( isset( $arrValues['entity_reference_id'] ) ) $this->setEntityReferenceId( $arrValues['entity_reference_id'] );
		if( isset( $arrValues['device_identifier'] ) && $boolDirectSet ) $this->set( 'm_strDeviceIdentifier', trim( stripcslashes( $arrValues['device_identifier'] ) ) ); elseif( isset( $arrValues['device_identifier'] ) ) $this->setDeviceIdentifier( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['device_identifier'] ) : $arrValues['device_identifier'] );
		if( isset( $arrValues['oauth_client_id'] ) && $boolDirectSet ) $this->set( 'm_intOauthClientId', trim( $arrValues['oauth_client_id'] ) ); elseif( isset( $arrValues['oauth_client_id'] ) ) $this->setOauthClientId( $arrValues['oauth_client_id'] );
		if( isset( $arrValues['oauth_client_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOauthClientTypeId', trim( $arrValues['oauth_client_type_id'] ) ); elseif( isset( $arrValues['oauth_client_type_id'] ) ) $this->setOauthClientTypeId( $arrValues['oauth_client_type_id'] );
		if( isset( $arrValues['token'] ) && $boolDirectSet ) $this->set( 'm_strToken', trim( stripcslashes( $arrValues['token'] ) ) ); elseif( isset( $arrValues['token'] ) ) $this->setToken( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['token'] ) : $arrValues['token'] );
		if( isset( $arrValues['expires'] ) && $boolDirectSet ) $this->set( 'm_strExpires', trim( $arrValues['expires'] ) ); elseif( isset( $arrValues['expires'] ) ) $this->setExpires( $arrValues['expires'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setApiTokenTypeId( $intApiTokenTypeId ) {
		$this->set( 'm_intApiTokenTypeId', CStrings::strToIntDef( $intApiTokenTypeId, NULL, false ) );
	}

	public function getApiTokenTypeId() {
		return $this->m_intApiTokenTypeId;
	}

	public function sqlApiTokenTypeId() {
		return ( true == isset( $this->m_intApiTokenTypeId ) ) ? ( string ) $this->m_intApiTokenTypeId : '1';
	}

	public function setEntityReferenceTypeId( $intEntityReferenceTypeId ) {
		$this->set( 'm_intEntityReferenceTypeId', CStrings::strToIntDef( $intEntityReferenceTypeId, NULL, false ) );
	}

	public function getEntityReferenceTypeId() {
		return $this->m_intEntityReferenceTypeId;
	}

	public function sqlEntityReferenceTypeId() {
		return ( true == isset( $this->m_intEntityReferenceTypeId ) ) ? ( string ) $this->m_intEntityReferenceTypeId : 'NULL';
	}

	public function setEntityReferenceId( $intEntityReferenceId ) {
		$this->set( 'm_intEntityReferenceId', CStrings::strToIntDef( $intEntityReferenceId, NULL, false ) );
	}

	public function getEntityReferenceId() {
		return $this->m_intEntityReferenceId;
	}

	public function sqlEntityReferenceId() {
		return ( true == isset( $this->m_intEntityReferenceId ) ) ? ( string ) $this->m_intEntityReferenceId : 'NULL';
	}

	public function setDeviceIdentifier( $strDeviceIdentifier ) {
		$this->set( 'm_strDeviceIdentifier', CStrings::strTrimDef( $strDeviceIdentifier, 100, NULL, true ) );
	}

	public function getDeviceIdentifier() {
		return $this->m_strDeviceIdentifier;
	}

	public function sqlDeviceIdentifier() {
		return ( true == isset( $this->m_strDeviceIdentifier ) ) ? '\'' . addslashes( $this->m_strDeviceIdentifier ) . '\'' : 'NULL';
	}

	public function setOauthClientId( $intOauthClientId ) {
		$this->set( 'm_intOauthClientId', CStrings::strToIntDef( $intOauthClientId, NULL, false ) );
	}

	public function getOauthClientId() {
		return $this->m_intOauthClientId;
	}

	public function sqlOauthClientId() {
		return ( true == isset( $this->m_intOauthClientId ) ) ? ( string ) $this->m_intOauthClientId : 'NULL';
	}

	public function setOauthClientTypeId( $intOauthClientTypeId ) {
		$this->set( 'm_intOauthClientTypeId', CStrings::strToIntDef( $intOauthClientTypeId, NULL, false ) );
	}

	public function getOauthClientTypeId() {
		return $this->m_intOauthClientTypeId;
	}

	public function sqlOauthClientTypeId() {
		return ( true == isset( $this->m_intOauthClientTypeId ) ) ? ( string ) $this->m_intOauthClientTypeId : 'NULL';
	}

	public function setToken( $strToken ) {
		$this->set( 'm_strToken', CStrings::strTrimDef( $strToken, 255, NULL, true ) );
	}

	public function getToken() {
		return $this->m_strToken;
	}

	public function sqlToken() {
		return ( true == isset( $this->m_strToken ) ) ? '\'' . addslashes( $this->m_strToken ) . '\'' : 'NULL';
	}

	public function setExpires( $strExpires ) {
		$this->set( 'm_strExpires', CStrings::strTrimDef( $strExpires, -1, NULL, true ) );
	}

	public function getExpires() {
		return $this->m_strExpires;
	}

	public function sqlExpires() {
		return ( true == isset( $this->m_strExpires ) ) ? '\'' . $this->m_strExpires . '\'' : 'NOW()';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, api_token_type_id, entity_reference_type_id, entity_reference_id, device_identifier, oauth_client_id, oauth_client_type_id, token, expires, notes, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlApiTokenTypeId() . ', ' .
 						$this->sqlEntityReferenceTypeId() . ', ' .
 						$this->sqlEntityReferenceId() . ', ' .
 						$this->sqlDeviceIdentifier() . ', ' .
 						$this->sqlOauthClientId() . ', ' .
 						$this->sqlOauthClientTypeId() . ', ' .
 						$this->sqlToken() . ', ' .
 						$this->sqlExpires() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' api_token_type_id = ' . $this->sqlApiTokenTypeId() . ','; } elseif( true == array_key_exists( 'ApiTokenTypeId', $this->getChangedColumns() ) ) { $strSql .= ' api_token_type_id = ' . $this->sqlApiTokenTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_reference_type_id = ' . $this->sqlEntityReferenceTypeId() . ','; } elseif( true == array_key_exists( 'EntityReferenceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' entity_reference_type_id = ' . $this->sqlEntityReferenceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_reference_id = ' . $this->sqlEntityReferenceId() . ','; } elseif( true == array_key_exists( 'EntityReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' entity_reference_id = ' . $this->sqlEntityReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' device_identifier = ' . $this->sqlDeviceIdentifier() . ','; } elseif( true == array_key_exists( 'DeviceIdentifier', $this->getChangedColumns() ) ) { $strSql .= ' device_identifier = ' . $this->sqlDeviceIdentifier() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' oauth_client_id = ' . $this->sqlOauthClientId() . ','; } elseif( true == array_key_exists( 'OauthClientId', $this->getChangedColumns() ) ) { $strSql .= ' oauth_client_id = ' . $this->sqlOauthClientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' oauth_client_type_id = ' . $this->sqlOauthClientTypeId() . ','; } elseif( true == array_key_exists( 'OauthClientTypeId', $this->getChangedColumns() ) ) { $strSql .= ' oauth_client_type_id = ' . $this->sqlOauthClientTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' token = ' . $this->sqlToken() . ','; } elseif( true == array_key_exists( 'Token', $this->getChangedColumns() ) ) { $strSql .= ' token = ' . $this->sqlToken() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expires = ' . $this->sqlExpires() . ','; } elseif( true == array_key_exists( 'Expires', $this->getChangedColumns() ) ) { $strSql .= ' expires = ' . $this->sqlExpires() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'api_token_type_id' => $this->getApiTokenTypeId(),
			'entity_reference_type_id' => $this->getEntityReferenceTypeId(),
			'entity_reference_id' => $this->getEntityReferenceId(),
			'device_identifier' => $this->getDeviceIdentifier(),
			'oauth_client_id' => $this->getOauthClientId(),
			'oauth_client_type_id' => $this->getOauthClientTypeId(),
			'token' => $this->getToken(),
			'expires' => $this->getExpires(),
			'notes' => $this->getNotes(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>