<?php

class CBaseEmployeeLearningGoal extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_learning_goals';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intSurveyId;
	protected $m_intLearningGoalId;
	protected $m_intLearningGoalProficiencyId;
	protected $m_strNote;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['survey_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyId', trim( $arrValues['survey_id'] ) ); elseif( isset( $arrValues['survey_id'] ) ) $this->setSurveyId( $arrValues['survey_id'] );
		if( isset( $arrValues['learning_goal_id'] ) && $boolDirectSet ) $this->set( 'm_intLearningGoalId', trim( $arrValues['learning_goal_id'] ) ); elseif( isset( $arrValues['learning_goal_id'] ) ) $this->setLearningGoalId( $arrValues['learning_goal_id'] );
		if( isset( $arrValues['learning_goal_proficiency_id'] ) && $boolDirectSet ) $this->set( 'm_intLearningGoalProficiencyId', trim( $arrValues['learning_goal_proficiency_id'] ) ); elseif( isset( $arrValues['learning_goal_proficiency_id'] ) ) $this->setLearningGoalProficiencyId( $arrValues['learning_goal_proficiency_id'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( $arrValues['note'] ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( $arrValues['note'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setSurveyId( $intSurveyId ) {
		$this->set( 'm_intSurveyId', CStrings::strToIntDef( $intSurveyId, NULL, false ) );
	}

	public function getSurveyId() {
		return $this->m_intSurveyId;
	}

	public function sqlSurveyId() {
		return ( true == isset( $this->m_intSurveyId ) ) ? ( string ) $this->m_intSurveyId : 'NULL';
	}

	public function setLearningGoalId( $intLearningGoalId ) {
		$this->set( 'm_intLearningGoalId', CStrings::strToIntDef( $intLearningGoalId, NULL, false ) );
	}

	public function getLearningGoalId() {
		return $this->m_intLearningGoalId;
	}

	public function sqlLearningGoalId() {
		return ( true == isset( $this->m_intLearningGoalId ) ) ? ( string ) $this->m_intLearningGoalId : 'NULL';
	}

	public function setLearningGoalProficiencyId( $intLearningGoalProficiencyId ) {
		$this->set( 'm_intLearningGoalProficiencyId', CStrings::strToIntDef( $intLearningGoalProficiencyId, NULL, false ) );
	}

	public function getLearningGoalProficiencyId() {
		return $this->m_intLearningGoalProficiencyId;
	}

	public function sqlLearningGoalProficiencyId() {
		return ( true == isset( $this->m_intLearningGoalProficiencyId ) ) ? ( string ) $this->m_intLearningGoalProficiencyId : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, -1, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNote ) : '\'' . addslashes( $this->m_strNote ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, survey_id, learning_goal_id, learning_goal_proficiency_id, note, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlSurveyId() . ', ' .
						$this->sqlLearningGoalId() . ', ' .
						$this->sqlLearningGoalProficiencyId() . ', ' .
						$this->sqlNote() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_id = ' . $this->sqlSurveyId(). ',' ; } elseif( true == array_key_exists( 'SurveyId', $this->getChangedColumns() ) ) { $strSql .= ' survey_id = ' . $this->sqlSurveyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' learning_goal_id = ' . $this->sqlLearningGoalId(). ',' ; } elseif( true == array_key_exists( 'LearningGoalId', $this->getChangedColumns() ) ) { $strSql .= ' learning_goal_id = ' . $this->sqlLearningGoalId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' learning_goal_proficiency_id = ' . $this->sqlLearningGoalProficiencyId(). ',' ; } elseif( true == array_key_exists( 'LearningGoalProficiencyId', $this->getChangedColumns() ) ) { $strSql .= ' learning_goal_proficiency_id = ' . $this->sqlLearningGoalProficiencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote(). ',' ; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'survey_id' => $this->getSurveyId(),
			'learning_goal_id' => $this->getLearningGoalId(),
			'learning_goal_proficiency_id' => $this->getLearningGoalProficiencyId(),
			'note' => $this->getNote(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>