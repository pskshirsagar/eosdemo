<?php

class CBaseCommissionRateAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.commission_rate_associations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intCommissionRecipientId;
	protected $m_intCommissionStructureId;
	protected $m_intCommissionRateId;
	protected $m_intContractId;
	protected $m_intPropertyId;
	protected $m_intOriginCommissionRateAssociationId;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strTransactionalEndDate;
	protected $m_fltEffectiveCommissionAmount;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strCurrencyCode;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['commission_recipient_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionRecipientId', trim( $arrValues['commission_recipient_id'] ) ); elseif( isset( $arrValues['commission_recipient_id'] ) ) $this->setCommissionRecipientId( $arrValues['commission_recipient_id'] );
		if( isset( $arrValues['commission_structure_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionStructureId', trim( $arrValues['commission_structure_id'] ) ); elseif( isset( $arrValues['commission_structure_id'] ) ) $this->setCommissionStructureId( $arrValues['commission_structure_id'] );
		if( isset( $arrValues['commission_rate_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionRateId', trim( $arrValues['commission_rate_id'] ) ); elseif( isset( $arrValues['commission_rate_id'] ) ) $this->setCommissionRateId( $arrValues['commission_rate_id'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['origin_commission_rate_association_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginCommissionRateAssociationId', trim( $arrValues['origin_commission_rate_association_id'] ) ); elseif( isset( $arrValues['origin_commission_rate_association_id'] ) ) $this->setOriginCommissionRateAssociationId( $arrValues['origin_commission_rate_association_id'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['transactional_end_date'] ) && $boolDirectSet ) $this->set( 'm_strTransactionalEndDate', trim( $arrValues['transactional_end_date'] ) ); elseif( isset( $arrValues['transactional_end_date'] ) ) $this->setTransactionalEndDate( $arrValues['transactional_end_date'] );
		if( isset( $arrValues['effective_commission_amount'] ) && $boolDirectSet ) $this->set( 'm_fltEffectiveCommissionAmount', trim( $arrValues['effective_commission_amount'] ) ); elseif( isset( $arrValues['effective_commission_amount'] ) ) $this->setEffectiveCommissionAmount( $arrValues['effective_commission_amount'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( $arrValues['currency_code'] ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( $arrValues['currency_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setCommissionRecipientId( $intCommissionRecipientId ) {
		$this->set( 'm_intCommissionRecipientId', CStrings::strToIntDef( $intCommissionRecipientId, NULL, false ) );
	}

	public function getCommissionRecipientId() {
		return $this->m_intCommissionRecipientId;
	}

	public function sqlCommissionRecipientId() {
		return ( true == isset( $this->m_intCommissionRecipientId ) ) ? ( string ) $this->m_intCommissionRecipientId : 'NULL';
	}

	public function setCommissionStructureId( $intCommissionStructureId ) {
		$this->set( 'm_intCommissionStructureId', CStrings::strToIntDef( $intCommissionStructureId, NULL, false ) );
	}

	public function getCommissionStructureId() {
		return $this->m_intCommissionStructureId;
	}

	public function sqlCommissionStructureId() {
		return ( true == isset( $this->m_intCommissionStructureId ) ) ? ( string ) $this->m_intCommissionStructureId : 'NULL';
	}

	public function setCommissionRateId( $intCommissionRateId ) {
		$this->set( 'm_intCommissionRateId', CStrings::strToIntDef( $intCommissionRateId, NULL, false ) );
	}

	public function getCommissionRateId() {
		return $this->m_intCommissionRateId;
	}

	public function sqlCommissionRateId() {
		return ( true == isset( $this->m_intCommissionRateId ) ) ? ( string ) $this->m_intCommissionRateId : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setOriginCommissionRateAssociationId( $intOriginCommissionRateAssociationId ) {
		$this->set( 'm_intOriginCommissionRateAssociationId', CStrings::strToIntDef( $intOriginCommissionRateAssociationId, NULL, false ) );
	}

	public function getOriginCommissionRateAssociationId() {
		return $this->m_intOriginCommissionRateAssociationId;
	}

	public function sqlOriginCommissionRateAssociationId() {
		return ( true == isset( $this->m_intOriginCommissionRateAssociationId ) ) ? ( string ) $this->m_intOriginCommissionRateAssociationId : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setTransactionalEndDate( $strTransactionalEndDate ) {
		$this->set( 'm_strTransactionalEndDate', CStrings::strTrimDef( $strTransactionalEndDate, -1, NULL, true ) );
	}

	public function getTransactionalEndDate() {
		return $this->m_strTransactionalEndDate;
	}

	public function sqlTransactionalEndDate() {
		return ( true == isset( $this->m_strTransactionalEndDate ) ) ? '\'' . $this->m_strTransactionalEndDate . '\'' : 'NULL';
	}

	public function setEffectiveCommissionAmount( $fltEffectiveCommissionAmount ) {
		$this->set( 'm_fltEffectiveCommissionAmount', CStrings::strToFloatDef( $fltEffectiveCommissionAmount, NULL, false, 4 ) );
	}

	public function getEffectiveCommissionAmount() {
		return $this->m_fltEffectiveCommissionAmount;
	}

	public function sqlEffectiveCommissionAmount() {
		return ( true == isset( $this->m_fltEffectiveCommissionAmount ) ) ? ( string ) $this->m_fltEffectiveCommissionAmount : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrencyCode ) : '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' ) : '\'USD\'';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, commission_recipient_id, commission_structure_id, commission_rate_id, contract_id, property_id, origin_commission_rate_association_id, start_date, end_date, transactional_end_date, effective_commission_amount, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, currency_code )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlCommissionRecipientId() . ', ' .
						$this->sqlCommissionStructureId() . ', ' .
						$this->sqlCommissionRateId() . ', ' .
						$this->sqlContractId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlOriginCommissionRateAssociationId() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlTransactionalEndDate() . ', ' .
						$this->sqlEffectiveCommissionAmount() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlCurrencyCode() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_recipient_id = ' . $this->sqlCommissionRecipientId(). ',' ; } elseif( true == array_key_exists( 'CommissionRecipientId', $this->getChangedColumns() ) ) { $strSql .= ' commission_recipient_id = ' . $this->sqlCommissionRecipientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_structure_id = ' . $this->sqlCommissionStructureId(). ',' ; } elseif( true == array_key_exists( 'CommissionStructureId', $this->getChangedColumns() ) ) { $strSql .= ' commission_structure_id = ' . $this->sqlCommissionStructureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_rate_id = ' . $this->sqlCommissionRateId(). ',' ; } elseif( true == array_key_exists( 'CommissionRateId', $this->getChangedColumns() ) ) { $strSql .= ' commission_rate_id = ' . $this->sqlCommissionRateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId(). ',' ; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' origin_commission_rate_association_id = ' . $this->sqlOriginCommissionRateAssociationId(). ',' ; } elseif( true == array_key_exists( 'OriginCommissionRateAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' origin_commission_rate_association_id = ' . $this->sqlOriginCommissionRateAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transactional_end_date = ' . $this->sqlTransactionalEndDate(). ',' ; } elseif( true == array_key_exists( 'TransactionalEndDate', $this->getChangedColumns() ) ) { $strSql .= ' transactional_end_date = ' . $this->sqlTransactionalEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_commission_amount = ' . $this->sqlEffectiveCommissionAmount(). ',' ; } elseif( true == array_key_exists( 'EffectiveCommissionAmount', $this->getChangedColumns() ) ) { $strSql .= ' effective_commission_amount = ' . $this->sqlEffectiveCommissionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'commission_recipient_id' => $this->getCommissionRecipientId(),
			'commission_structure_id' => $this->getCommissionStructureId(),
			'commission_rate_id' => $this->getCommissionRateId(),
			'contract_id' => $this->getContractId(),
			'property_id' => $this->getPropertyId(),
			'origin_commission_rate_association_id' => $this->getOriginCommissionRateAssociationId(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'transactional_end_date' => $this->getTransactionalEndDate(),
			'effective_commission_amount' => $this->getEffectiveCommissionAmount(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'currency_code' => $this->getCurrencyCode()
		);
	}

}
?>