<?php

class CBaseContractProperty extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.contract_properties';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intContractId;
	protected $m_strCurrencyCode;
	protected $m_intCommissionBucketId;
	protected $m_intPropertyId;
	protected $m_intPsProductId;
	protected $m_intBundlePsProductId;
	protected $m_intOriginalContractPropertyId;
	protected $m_intRenewalContractPropertyId;
	protected $m_intTempRenewalContractPropertyId;
	protected $m_intRecurringCompanyChargeId;
	protected $m_intTrainingEmployeeId;
	protected $m_intImplementationEmployeeId;
	protected $m_intImplementationDelayTypeId;
	protected $m_intTrainingTransactionId;
	protected $m_intImplementationTransactionId;
	protected $m_intContractTerminationRequestId;
	protected $m_intContractProductId;
	protected $m_intRecurringAccountId;
	protected $m_intSetupAccountId;
	protected $m_intTrainingAccountId;
	protected $m_intFrequencyId;
	protected $m_fltTrainingRate;
	protected $m_strChargeDatetime;
	protected $m_fltImplementationAmount;
	protected $m_fltMonthlyRecurringAmount;
	protected $m_fltTransactionalAmount;
	protected $m_fltMonthlyChangeAmount;
	protected $m_fltTransactionalChangeAmount;
	protected $m_fltPerItemAmount;
	protected $m_intImplementationHours;
	protected $m_strDeactivationDate;
	protected $m_strTerminationDate;
	protected $m_strBillingStartDate;
	protected $m_strNewBillingStartDate;
	protected $m_strImplementationPostDate;
	protected $m_strTrainingPostDate;
	protected $m_strCommissionTimerStart;
	protected $m_strCloseDate;
	protected $m_strPilotDueDate;
	protected $m_intIsUnlimited;
	protected $m_intIsException;
	protected $m_intIsImplementationContingent;
	protected $m_intImplementedBy;
	protected $m_strImplementedOn;
	protected $m_strImplementationEndDate;
	protected $m_intImplementationRevertedBy;
	protected $m_strImplementationRevertedOn;
	protected $m_intReimplementedBy;
	protected $m_strReimplementedOn;
	protected $m_strLastPostedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIgnoreInForecasting;
	protected $m_boolIsDuplicated;
	protected $m_strSoftTerminationDate;
	protected $m_strFirstSubscriptionMonth;
	protected $m_strFirstTransactionalMonth;
	protected $m_strOriginalBillingStartDate;
	protected $m_boolIsStaging;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolIsTransferredOut;
	protected $m_boolIsTransferredIn;
	protected $m_strPreFixCloseDate;
	protected $m_strPreFixBillingStartDate;
	protected $m_strLastSubscriptionMonth;
	protected $m_strLastTransactionalMonth;
	protected $m_strEffectiveDate;
	protected $m_intContractChainId;
	protected $m_intContractChainRank;
	protected $m_boolIsLastContractRecord;
	protected $m_intCachedNumberOfUnits;
	protected $m_boolHasNonContiguousContracts;
	protected $m_intTerminationBucketId;
	protected $m_strFirstConsistentSubscriptionMonth;
	protected $m_strLastConsistentSubscriptionMonth;
	protected $m_intLeadPackageId;
	protected $m_boolIsFirstClientContract;
	protected $m_boolIsFirstPropertyContract;
	protected $m_boolIsFirstProductContract;
	protected $m_boolIsLastClientTermination;
	protected $m_boolIsLastPropertyTermination;
	protected $m_boolIsLastProductTermination;
	protected $m_boolIsLastClientImplementation;
	protected $m_boolIsLastPropertyImplementation;
	protected $m_boolIsLastProductImplementation;
	protected $m_boolIsLastClientBilling;
	protected $m_boolIsLastPropertyBilling;
	protected $m_boolIsLastProductBilling;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';
		$this->m_intCommissionBucketId = '3';
		$this->m_intFrequencyId = '4';
		$this->m_fltTrainingRate = '0';
		$this->m_fltImplementationAmount = '0';
		$this->m_fltMonthlyRecurringAmount = '0';
		$this->m_fltTransactionalAmount = '0';
		$this->m_fltMonthlyChangeAmount = '0';
		$this->m_fltTransactionalChangeAmount = '0';
		$this->m_fltPerItemAmount = '0';
		$this->m_intImplementationHours = '0';
		$this->m_intIsUnlimited = '0';
		$this->m_intIsException = '0';
		$this->m_intIsImplementationContingent = '0';
		$this->m_boolIgnoreInForecasting = false;
		$this->m_boolIsDuplicated = false;
		$this->m_boolIsStaging = false;
		$this->m_boolIsTransferredOut = false;
		$this->m_boolIsTransferredIn = false;
		$this->m_boolIsLastContractRecord = false;
		$this->m_intCachedNumberOfUnits = '0';
		$this->m_boolHasNonContiguousContracts = false;
		$this->m_boolIsFirstClientContract = false;
		$this->m_boolIsFirstPropertyContract = false;
		$this->m_boolIsFirstProductContract = false;
		$this->m_boolIsLastClientTermination = false;
		$this->m_boolIsLastPropertyTermination = false;
		$this->m_boolIsLastProductTermination = false;
		$this->m_boolIsLastClientImplementation = false;
		$this->m_boolIsLastPropertyImplementation = false;
		$this->m_boolIsLastProductImplementation = false;
		$this->m_boolIsLastClientBilling = false;
		$this->m_boolIsLastPropertyBilling = false;
		$this->m_boolIsLastProductBilling = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( $arrValues['currency_code'] ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( $arrValues['currency_code'] );
		if( isset( $arrValues['commission_bucket_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionBucketId', trim( $arrValues['commission_bucket_id'] ) ); elseif( isset( $arrValues['commission_bucket_id'] ) ) $this->setCommissionBucketId( $arrValues['commission_bucket_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['bundle_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intBundlePsProductId', trim( $arrValues['bundle_ps_product_id'] ) ); elseif( isset( $arrValues['bundle_ps_product_id'] ) ) $this->setBundlePsProductId( $arrValues['bundle_ps_product_id'] );
		if( isset( $arrValues['original_contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalContractPropertyId', trim( $arrValues['original_contract_property_id'] ) ); elseif( isset( $arrValues['original_contract_property_id'] ) ) $this->setOriginalContractPropertyId( $arrValues['original_contract_property_id'] );
		if( isset( $arrValues['renewal_contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intRenewalContractPropertyId', trim( $arrValues['renewal_contract_property_id'] ) ); elseif( isset( $arrValues['renewal_contract_property_id'] ) ) $this->setRenewalContractPropertyId( $arrValues['renewal_contract_property_id'] );
		if( isset( $arrValues['temp_renewal_contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intTempRenewalContractPropertyId', trim( $arrValues['temp_renewal_contract_property_id'] ) ); elseif( isset( $arrValues['temp_renewal_contract_property_id'] ) ) $this->setTempRenewalContractPropertyId( $arrValues['temp_renewal_contract_property_id'] );
		if( isset( $arrValues['recurring_company_charge_id'] ) && $boolDirectSet ) $this->set( 'm_intRecurringCompanyChargeId', trim( $arrValues['recurring_company_charge_id'] ) ); elseif( isset( $arrValues['recurring_company_charge_id'] ) ) $this->setRecurringCompanyChargeId( $arrValues['recurring_company_charge_id'] );
		if( isset( $arrValues['training_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingEmployeeId', trim( $arrValues['training_employee_id'] ) ); elseif( isset( $arrValues['training_employee_id'] ) ) $this->setTrainingEmployeeId( $arrValues['training_employee_id'] );
		if( isset( $arrValues['implementation_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationEmployeeId', trim( $arrValues['implementation_employee_id'] ) ); elseif( isset( $arrValues['implementation_employee_id'] ) ) $this->setImplementationEmployeeId( $arrValues['implementation_employee_id'] );
		if( isset( $arrValues['implementation_delay_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationDelayTypeId', trim( $arrValues['implementation_delay_type_id'] ) ); elseif( isset( $arrValues['implementation_delay_type_id'] ) ) $this->setImplementationDelayTypeId( $arrValues['implementation_delay_type_id'] );
		if( isset( $arrValues['training_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingTransactionId', trim( $arrValues['training_transaction_id'] ) ); elseif( isset( $arrValues['training_transaction_id'] ) ) $this->setTrainingTransactionId( $arrValues['training_transaction_id'] );
		if( isset( $arrValues['implementation_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationTransactionId', trim( $arrValues['implementation_transaction_id'] ) ); elseif( isset( $arrValues['implementation_transaction_id'] ) ) $this->setImplementationTransactionId( $arrValues['implementation_transaction_id'] );
		if( isset( $arrValues['contract_termination_request_id'] ) && $boolDirectSet ) $this->set( 'm_intContractTerminationRequestId', trim( $arrValues['contract_termination_request_id'] ) ); elseif( isset( $arrValues['contract_termination_request_id'] ) ) $this->setContractTerminationRequestId( $arrValues['contract_termination_request_id'] );
		if( isset( $arrValues['contract_product_id'] ) && $boolDirectSet ) $this->set( 'm_intContractProductId', trim( $arrValues['contract_product_id'] ) ); elseif( isset( $arrValues['contract_product_id'] ) ) $this->setContractProductId( $arrValues['contract_product_id'] );
		if( isset( $arrValues['recurring_account_id'] ) && $boolDirectSet ) $this->set( 'm_intRecurringAccountId', trim( $arrValues['recurring_account_id'] ) ); elseif( isset( $arrValues['recurring_account_id'] ) ) $this->setRecurringAccountId( $arrValues['recurring_account_id'] );
		if( isset( $arrValues['setup_account_id'] ) && $boolDirectSet ) $this->set( 'm_intSetupAccountId', trim( $arrValues['setup_account_id'] ) ); elseif( isset( $arrValues['setup_account_id'] ) ) $this->setSetupAccountId( $arrValues['setup_account_id'] );
		if( isset( $arrValues['training_account_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingAccountId', trim( $arrValues['training_account_id'] ) ); elseif( isset( $arrValues['training_account_id'] ) ) $this->setTrainingAccountId( $arrValues['training_account_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['training_rate'] ) && $boolDirectSet ) $this->set( 'm_fltTrainingRate', trim( $arrValues['training_rate'] ) ); elseif( isset( $arrValues['training_rate'] ) ) $this->setTrainingRate( $arrValues['training_rate'] );
		if( isset( $arrValues['charge_datetime'] ) && $boolDirectSet ) $this->set( 'm_strChargeDatetime', trim( $arrValues['charge_datetime'] ) ); elseif( isset( $arrValues['charge_datetime'] ) ) $this->setChargeDatetime( $arrValues['charge_datetime'] );
		if( isset( $arrValues['implementation_amount'] ) && $boolDirectSet ) $this->set( 'm_fltImplementationAmount', trim( $arrValues['implementation_amount'] ) ); elseif( isset( $arrValues['implementation_amount'] ) ) $this->setImplementationAmount( $arrValues['implementation_amount'] );
		if( isset( $arrValues['monthly_recurring_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyRecurringAmount', trim( $arrValues['monthly_recurring_amount'] ) ); elseif( isset( $arrValues['monthly_recurring_amount'] ) ) $this->setMonthlyRecurringAmount( $arrValues['monthly_recurring_amount'] );
		if( isset( $arrValues['transactional_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionalAmount', trim( $arrValues['transactional_amount'] ) ); elseif( isset( $arrValues['transactional_amount'] ) ) $this->setTransactionalAmount( $arrValues['transactional_amount'] );
		if( isset( $arrValues['monthly_change_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyChangeAmount', trim( $arrValues['monthly_change_amount'] ) ); elseif( isset( $arrValues['monthly_change_amount'] ) ) $this->setMonthlyChangeAmount( $arrValues['monthly_change_amount'] );
		if( isset( $arrValues['transactional_change_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionalChangeAmount', trim( $arrValues['transactional_change_amount'] ) ); elseif( isset( $arrValues['transactional_change_amount'] ) ) $this->setTransactionalChangeAmount( $arrValues['transactional_change_amount'] );
		if( isset( $arrValues['per_item_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPerItemAmount', trim( $arrValues['per_item_amount'] ) ); elseif( isset( $arrValues['per_item_amount'] ) ) $this->setPerItemAmount( $arrValues['per_item_amount'] );
		if( isset( $arrValues['implementation_hours'] ) && $boolDirectSet ) $this->set( 'm_intImplementationHours', trim( $arrValues['implementation_hours'] ) ); elseif( isset( $arrValues['implementation_hours'] ) ) $this->setImplementationHours( $arrValues['implementation_hours'] );
		if( isset( $arrValues['deactivation_date'] ) && $boolDirectSet ) $this->set( 'm_strDeactivationDate', trim( $arrValues['deactivation_date'] ) ); elseif( isset( $arrValues['deactivation_date'] ) ) $this->setDeactivationDate( $arrValues['deactivation_date'] );
		if( isset( $arrValues['termination_date'] ) && $boolDirectSet ) $this->set( 'm_strTerminationDate', trim( $arrValues['termination_date'] ) ); elseif( isset( $arrValues['termination_date'] ) ) $this->setTerminationDate( $arrValues['termination_date'] );
		if( isset( $arrValues['billing_start_date'] ) && $boolDirectSet ) $this->set( 'm_strBillingStartDate', trim( $arrValues['billing_start_date'] ) ); elseif( isset( $arrValues['billing_start_date'] ) ) $this->setBillingStartDate( $arrValues['billing_start_date'] );
		if( isset( $arrValues['new_billing_start_date'] ) && $boolDirectSet ) $this->set( 'm_strNewBillingStartDate', trim( $arrValues['new_billing_start_date'] ) ); elseif( isset( $arrValues['new_billing_start_date'] ) ) $this->setNewBillingStartDate( $arrValues['new_billing_start_date'] );
		if( isset( $arrValues['implementation_post_date'] ) && $boolDirectSet ) $this->set( 'm_strImplementationPostDate', trim( $arrValues['implementation_post_date'] ) ); elseif( isset( $arrValues['implementation_post_date'] ) ) $this->setImplementationPostDate( $arrValues['implementation_post_date'] );
		if( isset( $arrValues['training_post_date'] ) && $boolDirectSet ) $this->set( 'm_strTrainingPostDate', trim( $arrValues['training_post_date'] ) ); elseif( isset( $arrValues['training_post_date'] ) ) $this->setTrainingPostDate( $arrValues['training_post_date'] );
		if( isset( $arrValues['commission_timer_start'] ) && $boolDirectSet ) $this->set( 'm_strCommissionTimerStart', trim( $arrValues['commission_timer_start'] ) ); elseif( isset( $arrValues['commission_timer_start'] ) ) $this->setCommissionTimerStart( $arrValues['commission_timer_start'] );
		if( isset( $arrValues['close_date'] ) && $boolDirectSet ) $this->set( 'm_strCloseDate', trim( $arrValues['close_date'] ) ); elseif( isset( $arrValues['close_date'] ) ) $this->setCloseDate( $arrValues['close_date'] );
		if( isset( $arrValues['pilot_due_date'] ) && $boolDirectSet ) $this->set( 'm_strPilotDueDate', trim( $arrValues['pilot_due_date'] ) ); elseif( isset( $arrValues['pilot_due_date'] ) ) $this->setPilotDueDate( $arrValues['pilot_due_date'] );
		if( isset( $arrValues['is_unlimited'] ) && $boolDirectSet ) $this->set( 'm_intIsUnlimited', trim( $arrValues['is_unlimited'] ) ); elseif( isset( $arrValues['is_unlimited'] ) ) $this->setIsUnlimited( $arrValues['is_unlimited'] );
		if( isset( $arrValues['is_exception'] ) && $boolDirectSet ) $this->set( 'm_intIsException', trim( $arrValues['is_exception'] ) ); elseif( isset( $arrValues['is_exception'] ) ) $this->setIsException( $arrValues['is_exception'] );
		if( isset( $arrValues['is_implementation_contingent'] ) && $boolDirectSet ) $this->set( 'm_intIsImplementationContingent', trim( $arrValues['is_implementation_contingent'] ) ); elseif( isset( $arrValues['is_implementation_contingent'] ) ) $this->setIsImplementationContingent( $arrValues['is_implementation_contingent'] );
		if( isset( $arrValues['implemented_by'] ) && $boolDirectSet ) $this->set( 'm_intImplementedBy', trim( $arrValues['implemented_by'] ) ); elseif( isset( $arrValues['implemented_by'] ) ) $this->setImplementedBy( $arrValues['implemented_by'] );
		if( isset( $arrValues['implemented_on'] ) && $boolDirectSet ) $this->set( 'm_strImplementedOn', trim( $arrValues['implemented_on'] ) ); elseif( isset( $arrValues['implemented_on'] ) ) $this->setImplementedOn( $arrValues['implemented_on'] );
		if( isset( $arrValues['implementation_end_date'] ) && $boolDirectSet ) $this->set( 'm_strImplementationEndDate', trim( $arrValues['implementation_end_date'] ) ); elseif( isset( $arrValues['implementation_end_date'] ) ) $this->setImplementationEndDate( $arrValues['implementation_end_date'] );
		if( isset( $arrValues['implementation_reverted_by'] ) && $boolDirectSet ) $this->set( 'm_intImplementationRevertedBy', trim( $arrValues['implementation_reverted_by'] ) ); elseif( isset( $arrValues['implementation_reverted_by'] ) ) $this->setImplementationRevertedBy( $arrValues['implementation_reverted_by'] );
		if( isset( $arrValues['implementation_reverted_on'] ) && $boolDirectSet ) $this->set( 'm_strImplementationRevertedOn', trim( $arrValues['implementation_reverted_on'] ) ); elseif( isset( $arrValues['implementation_reverted_on'] ) ) $this->setImplementationRevertedOn( $arrValues['implementation_reverted_on'] );
		if( isset( $arrValues['reimplemented_by'] ) && $boolDirectSet ) $this->set( 'm_intReimplementedBy', trim( $arrValues['reimplemented_by'] ) ); elseif( isset( $arrValues['reimplemented_by'] ) ) $this->setReimplementedBy( $arrValues['reimplemented_by'] );
		if( isset( $arrValues['reimplemented_on'] ) && $boolDirectSet ) $this->set( 'm_strReimplementedOn', trim( $arrValues['reimplemented_on'] ) ); elseif( isset( $arrValues['reimplemented_on'] ) ) $this->setReimplementedOn( $arrValues['reimplemented_on'] );
		if( isset( $arrValues['last_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedOn', trim( $arrValues['last_posted_on'] ) ); elseif( isset( $arrValues['last_posted_on'] ) ) $this->setLastPostedOn( $arrValues['last_posted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ignore_in_forecasting'] ) && $boolDirectSet ) $this->set( 'm_boolIgnoreInForecasting', trim( stripcslashes( $arrValues['ignore_in_forecasting'] ) ) ); elseif( isset( $arrValues['ignore_in_forecasting'] ) ) $this->setIgnoreInForecasting( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ignore_in_forecasting'] ) : $arrValues['ignore_in_forecasting'] );
		if( isset( $arrValues['is_duplicated'] ) && $boolDirectSet ) $this->set( 'm_boolIsDuplicated', trim( stripcslashes( $arrValues['is_duplicated'] ) ) ); elseif( isset( $arrValues['is_duplicated'] ) ) $this->setIsDuplicated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_duplicated'] ) : $arrValues['is_duplicated'] );
		if( isset( $arrValues['soft_termination_date'] ) && $boolDirectSet ) $this->set( 'm_strSoftTerminationDate', trim( $arrValues['soft_termination_date'] ) ); elseif( isset( $arrValues['soft_termination_date'] ) ) $this->setSoftTerminationDate( $arrValues['soft_termination_date'] );
		if( isset( $arrValues['first_subscription_month'] ) && $boolDirectSet ) $this->set( 'm_strFirstSubscriptionMonth', trim( $arrValues['first_subscription_month'] ) ); elseif( isset( $arrValues['first_subscription_month'] ) ) $this->setFirstSubscriptionMonth( $arrValues['first_subscription_month'] );
		if( isset( $arrValues['first_transactional_month'] ) && $boolDirectSet ) $this->set( 'm_strFirstTransactionalMonth', trim( $arrValues['first_transactional_month'] ) ); elseif( isset( $arrValues['first_transactional_month'] ) ) $this->setFirstTransactionalMonth( $arrValues['first_transactional_month'] );
		if( isset( $arrValues['original_billing_start_date'] ) && $boolDirectSet ) $this->set( 'm_strOriginalBillingStartDate', trim( $arrValues['original_billing_start_date'] ) ); elseif( isset( $arrValues['original_billing_start_date'] ) ) $this->setOriginalBillingStartDate( $arrValues['original_billing_start_date'] );
		if( isset( $arrValues['is_staging'] ) && $boolDirectSet ) $this->set( 'm_boolIsStaging', trim( stripcslashes( $arrValues['is_staging'] ) ) ); elseif( isset( $arrValues['is_staging'] ) ) $this->setIsStaging( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_staging'] ) : $arrValues['is_staging'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_transferred_out'] ) && $boolDirectSet ) $this->set( 'm_boolIsTransferredOut', trim( stripcslashes( $arrValues['is_transferred_out'] ) ) ); elseif( isset( $arrValues['is_transferred_out'] ) ) $this->setIsTransferredOut( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_transferred_out'] ) : $arrValues['is_transferred_out'] );
		if( isset( $arrValues['is_transferred_in'] ) && $boolDirectSet ) $this->set( 'm_boolIsTransferredIn', trim( stripcslashes( $arrValues['is_transferred_in'] ) ) ); elseif( isset( $arrValues['is_transferred_in'] ) ) $this->setIsTransferredIn( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_transferred_in'] ) : $arrValues['is_transferred_in'] );
		if( isset( $arrValues['pre_fix_close_date'] ) && $boolDirectSet ) $this->set( 'm_strPreFixCloseDate', trim( $arrValues['pre_fix_close_date'] ) ); elseif( isset( $arrValues['pre_fix_close_date'] ) ) $this->setPreFixCloseDate( $arrValues['pre_fix_close_date'] );
		if( isset( $arrValues['pre_fix_billing_start_date'] ) && $boolDirectSet ) $this->set( 'm_strPreFixBillingStartDate', trim( $arrValues['pre_fix_billing_start_date'] ) ); elseif( isset( $arrValues['pre_fix_billing_start_date'] ) ) $this->setPreFixBillingStartDate( $arrValues['pre_fix_billing_start_date'] );
		if( isset( $arrValues['last_subscription_month'] ) && $boolDirectSet ) $this->set( 'm_strLastSubscriptionMonth', trim( $arrValues['last_subscription_month'] ) ); elseif( isset( $arrValues['last_subscription_month'] ) ) $this->setLastSubscriptionMonth( $arrValues['last_subscription_month'] );
		if( isset( $arrValues['last_transactional_month'] ) && $boolDirectSet ) $this->set( 'm_strLastTransactionalMonth', trim( $arrValues['last_transactional_month'] ) ); elseif( isset( $arrValues['last_transactional_month'] ) ) $this->setLastTransactionalMonth( $arrValues['last_transactional_month'] );
		if( isset( $arrValues['effective_date'] ) && $boolDirectSet ) $this->set( 'm_strEffectiveDate', trim( $arrValues['effective_date'] ) ); elseif( isset( $arrValues['effective_date'] ) ) $this->setEffectiveDate( $arrValues['effective_date'] );
		if( isset( $arrValues['contract_chain_id'] ) && $boolDirectSet ) $this->set( 'm_intContractChainId', trim( $arrValues['contract_chain_id'] ) ); elseif( isset( $arrValues['contract_chain_id'] ) ) $this->setContractChainId( $arrValues['contract_chain_id'] );
		if( isset( $arrValues['contract_chain_rank'] ) && $boolDirectSet ) $this->set( 'm_intContractChainRank', trim( $arrValues['contract_chain_rank'] ) ); elseif( isset( $arrValues['contract_chain_rank'] ) ) $this->setContractChainRank( $arrValues['contract_chain_rank'] );
		if( isset( $arrValues['is_last_contract_record'] ) && $boolDirectSet ) $this->set( 'm_boolIsLastContractRecord', trim( stripcslashes( $arrValues['is_last_contract_record'] ) ) ); elseif( isset( $arrValues['is_last_contract_record'] ) ) $this->setIsLastContractRecord( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_last_contract_record'] ) : $arrValues['is_last_contract_record'] );
		if( isset( $arrValues['cached_number_of_units'] ) && $boolDirectSet ) $this->set( 'm_intCachedNumberOfUnits', trim( $arrValues['cached_number_of_units'] ) ); elseif( isset( $arrValues['cached_number_of_units'] ) ) $this->setCachedNumberOfUnits( $arrValues['cached_number_of_units'] );
		if( isset( $arrValues['has_non_contiguous_contracts'] ) && $boolDirectSet ) $this->set( 'm_boolHasNonContiguousContracts', trim( stripcslashes( $arrValues['has_non_contiguous_contracts'] ) ) ); elseif( isset( $arrValues['has_non_contiguous_contracts'] ) ) $this->setHasNonContiguousContracts( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['has_non_contiguous_contracts'] ) : $arrValues['has_non_contiguous_contracts'] );
		if( isset( $arrValues['termination_bucket_id'] ) && $boolDirectSet ) $this->set( 'm_intTerminationBucketId', trim( $arrValues['termination_bucket_id'] ) ); elseif( isset( $arrValues['termination_bucket_id'] ) ) $this->setTerminationBucketId( $arrValues['termination_bucket_id'] );
		if( isset( $arrValues['first_consistent_subscription_month'] ) && $boolDirectSet ) $this->set( 'm_strFirstConsistentSubscriptionMonth', trim( $arrValues['first_consistent_subscription_month'] ) ); elseif( isset( $arrValues['first_consistent_subscription_month'] ) ) $this->setFirstConsistentSubscriptionMonth( $arrValues['first_consistent_subscription_month'] );
		if( isset( $arrValues['last_consistent_subscription_month'] ) && $boolDirectSet ) $this->set( 'm_strLastConsistentSubscriptionMonth', trim( $arrValues['last_consistent_subscription_month'] ) ); elseif( isset( $arrValues['last_consistent_subscription_month'] ) ) $this->setLastConsistentSubscriptionMonth( $arrValues['last_consistent_subscription_month'] );
		if( isset( $arrValues['package_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPackageProductId', trim( $arrValues['package_product_id'] ) ); elseif( isset( $arrValues['package_product_id'] ) ) $this->setPackageProductId( $arrValues['package_product_id'] );
		if( isset( $arrValues['is_first_client_contract'] ) && $boolDirectSet ) $this->set( 'm_boolIsFirstClientContract', trim( stripcslashes( $arrValues['is_first_client_contract'] ) ) ); elseif( isset( $arrValues['is_first_client_contract'] ) ) $this->setIsFirstClientContract( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_first_client_contract'] ) : $arrValues['is_first_client_contract'] );
		if( isset( $arrValues['is_first_property_contract'] ) && $boolDirectSet ) $this->set( 'm_boolIsFirstPropertyContract', trim( stripcslashes( $arrValues['is_first_property_contract'] ) ) ); elseif( isset( $arrValues['is_first_property_contract'] ) ) $this->setIsFirstPropertyContract( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_first_property_contract'] ) : $arrValues['is_first_property_contract'] );
		if( isset( $arrValues['is_first_product_contract'] ) && $boolDirectSet ) $this->set( 'm_boolIsFirstProductContract', trim( stripcslashes( $arrValues['is_first_product_contract'] ) ) ); elseif( isset( $arrValues['is_first_product_contract'] ) ) $this->setIsFirstProductContract( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_first_product_contract'] ) : $arrValues['is_first_product_contract'] );
		if( isset( $arrValues['is_last_client_termination'] ) && $boolDirectSet ) $this->set( 'm_boolIsLastClientTermination', trim( stripcslashes( $arrValues['is_last_client_termination'] ) ) ); elseif( isset( $arrValues['is_last_client_termination'] ) ) $this->setIsLastClientTermination( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_last_client_termination'] ) : $arrValues['is_last_client_termination'] );
		if( isset( $arrValues['is_last_property_termination'] ) && $boolDirectSet ) $this->set( 'm_boolIsLastPropertyTermination', trim( stripcslashes( $arrValues['is_last_property_termination'] ) ) ); elseif( isset( $arrValues['is_last_property_termination'] ) ) $this->setIsLastPropertyTermination( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_last_property_termination'] ) : $arrValues['is_last_property_termination'] );
		if( isset( $arrValues['is_last_product_termination'] ) && $boolDirectSet ) $this->set( 'm_boolIsLastProductTermination', trim( stripcslashes( $arrValues['is_last_product_termination'] ) ) ); elseif( isset( $arrValues['is_last_product_termination'] ) ) $this->setIsLastProductTermination( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_last_product_termination'] ) : $arrValues['is_last_product_termination'] );
		if( isset( $arrValues['is_last_client_implementation'] ) && $boolDirectSet ) $this->set( 'm_boolIsLastClientImplementation', trim( stripcslashes( $arrValues['is_last_client_implementation'] ) ) ); elseif( isset( $arrValues['is_last_client_implementation'] ) ) $this->setIsLastClientImplementation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_last_client_implementation'] ) : $arrValues['is_last_client_implementation'] );
		if( isset( $arrValues['is_last_property_implementation'] ) && $boolDirectSet ) $this->set( 'm_boolIsLastPropertyImplementation', trim( stripcslashes( $arrValues['is_last_property_implementation'] ) ) ); elseif( isset( $arrValues['is_last_property_implementation'] ) ) $this->setIsLastPropertyImplementation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_last_property_implementation'] ) : $arrValues['is_last_property_implementation'] );
		if( isset( $arrValues['is_last_product_implementation'] ) && $boolDirectSet ) $this->set( 'm_boolIsLastProductImplementation', trim( stripcslashes( $arrValues['is_last_product_implementation'] ) ) ); elseif( isset( $arrValues['is_last_product_implementation'] ) ) $this->setIsLastProductImplementation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_last_product_implementation'] ) : $arrValues['is_last_product_implementation'] );
		if( isset( $arrValues['is_last_client_billing'] ) && $boolDirectSet ) $this->set( 'm_boolIsLastClientBilling', trim( stripcslashes( $arrValues['is_last_client_billing'] ) ) ); elseif( isset( $arrValues['is_last_client_billing'] ) ) $this->setIsLastClientBilling( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_last_client_billing'] ) : $arrValues['is_last_client_billing'] );
		if( isset( $arrValues['is_last_property_billing'] ) && $boolDirectSet ) $this->set( 'm_boolIsLastPropertyBilling', trim( stripcslashes( $arrValues['is_last_property_billing'] ) ) ); elseif( isset( $arrValues['is_last_property_billing'] ) ) $this->setIsLastPropertyBilling( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_last_property_billing'] ) : $arrValues['is_last_property_billing'] );
		if( isset( $arrValues['is_last_product_billing'] ) && $boolDirectSet ) $this->set( 'm_boolIsLastProductBilling', trim( stripcslashes( $arrValues['is_last_product_billing'] ) ) ); elseif( isset( $arrValues['is_last_product_billing'] ) ) $this->setIsLastProductBilling( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_last_product_billing'] ) : $arrValues['is_last_product_billing'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrencyCode ) : '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' ) : '\'USD\'';
	}

	public function setCommissionBucketId( $intCommissionBucketId ) {
		$this->set( 'm_intCommissionBucketId', CStrings::strToIntDef( $intCommissionBucketId, NULL, false ) );
	}

	public function getCommissionBucketId() {
		return $this->m_intCommissionBucketId;
	}

	public function sqlCommissionBucketId() {
		return ( true == isset( $this->m_intCommissionBucketId ) ) ? ( string ) $this->m_intCommissionBucketId : '3';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setBundlePsProductId( $intBundlePsProductId ) {
		$this->set( 'm_intBundlePsProductId', CStrings::strToIntDef( $intBundlePsProductId, NULL, false ) );
	}

	public function getBundlePsProductId() {
		return $this->m_intBundlePsProductId;
	}

	public function sqlBundlePsProductId() {
		return ( true == isset( $this->m_intBundlePsProductId ) ) ? ( string ) $this->m_intBundlePsProductId : 'NULL';
	}

	public function setOriginalContractPropertyId( $intOriginalContractPropertyId ) {
		$this->set( 'm_intOriginalContractPropertyId', CStrings::strToIntDef( $intOriginalContractPropertyId, NULL, false ) );
	}

	public function getOriginalContractPropertyId() {
		return $this->m_intOriginalContractPropertyId;
	}

	public function sqlOriginalContractPropertyId() {
		return ( true == isset( $this->m_intOriginalContractPropertyId ) ) ? ( string ) $this->m_intOriginalContractPropertyId : 'NULL';
	}

	public function setRenewalContractPropertyId( $intRenewalContractPropertyId ) {
		$this->set( 'm_intRenewalContractPropertyId', CStrings::strToIntDef( $intRenewalContractPropertyId, NULL, false ) );
	}

	public function getRenewalContractPropertyId() {
		return $this->m_intRenewalContractPropertyId;
	}

	public function sqlRenewalContractPropertyId() {
		return ( true == isset( $this->m_intRenewalContractPropertyId ) ) ? ( string ) $this->m_intRenewalContractPropertyId : 'NULL';
	}

	public function setTempRenewalContractPropertyId( $intTempRenewalContractPropertyId ) {
		$this->set( 'm_intTempRenewalContractPropertyId', CStrings::strToIntDef( $intTempRenewalContractPropertyId, NULL, false ) );
	}

	public function getTempRenewalContractPropertyId() {
		return $this->m_intTempRenewalContractPropertyId;
	}

	public function sqlTempRenewalContractPropertyId() {
		return ( true == isset( $this->m_intTempRenewalContractPropertyId ) ) ? ( string ) $this->m_intTempRenewalContractPropertyId : 'NULL';
	}

	public function setRecurringCompanyChargeId( $intRecurringCompanyChargeId ) {
		$this->set( 'm_intRecurringCompanyChargeId', CStrings::strToIntDef( $intRecurringCompanyChargeId, NULL, false ) );
	}

	public function getRecurringCompanyChargeId() {
		return $this->m_intRecurringCompanyChargeId;
	}

	public function sqlRecurringCompanyChargeId() {
		return ( true == isset( $this->m_intRecurringCompanyChargeId ) ) ? ( string ) $this->m_intRecurringCompanyChargeId : 'NULL';
	}

	public function setTrainingEmployeeId( $intTrainingEmployeeId ) {
		$this->set( 'm_intTrainingEmployeeId', CStrings::strToIntDef( $intTrainingEmployeeId, NULL, false ) );
	}

	public function getTrainingEmployeeId() {
		return $this->m_intTrainingEmployeeId;
	}

	public function sqlTrainingEmployeeId() {
		return ( true == isset( $this->m_intTrainingEmployeeId ) ) ? ( string ) $this->m_intTrainingEmployeeId : 'NULL';
	}

	public function setImplementationEmployeeId( $intImplementationEmployeeId ) {
		$this->set( 'm_intImplementationEmployeeId', CStrings::strToIntDef( $intImplementationEmployeeId, NULL, false ) );
	}

	public function getImplementationEmployeeId() {
		return $this->m_intImplementationEmployeeId;
	}

	public function sqlImplementationEmployeeId() {
		return ( true == isset( $this->m_intImplementationEmployeeId ) ) ? ( string ) $this->m_intImplementationEmployeeId : 'NULL';
	}

	public function setImplementationDelayTypeId( $intImplementationDelayTypeId ) {
		$this->set( 'm_intImplementationDelayTypeId', CStrings::strToIntDef( $intImplementationDelayTypeId, NULL, false ) );
	}

	public function getImplementationDelayTypeId() {
		return $this->m_intImplementationDelayTypeId;
	}

	public function sqlImplementationDelayTypeId() {
		return ( true == isset( $this->m_intImplementationDelayTypeId ) ) ? ( string ) $this->m_intImplementationDelayTypeId : 'NULL';
	}

	public function setTrainingTransactionId( $intTrainingTransactionId ) {
		$this->set( 'm_intTrainingTransactionId', CStrings::strToIntDef( $intTrainingTransactionId, NULL, false ) );
	}

	public function getTrainingTransactionId() {
		return $this->m_intTrainingTransactionId;
	}

	public function sqlTrainingTransactionId() {
		return ( true == isset( $this->m_intTrainingTransactionId ) ) ? ( string ) $this->m_intTrainingTransactionId : 'NULL';
	}

	public function setImplementationTransactionId( $intImplementationTransactionId ) {
		$this->set( 'm_intImplementationTransactionId', CStrings::strToIntDef( $intImplementationTransactionId, NULL, false ) );
	}

	public function getImplementationTransactionId() {
		return $this->m_intImplementationTransactionId;
	}

	public function sqlImplementationTransactionId() {
		return ( true == isset( $this->m_intImplementationTransactionId ) ) ? ( string ) $this->m_intImplementationTransactionId : 'NULL';
	}

	public function setContractTerminationRequestId( $intContractTerminationRequestId ) {
		$this->set( 'm_intContractTerminationRequestId', CStrings::strToIntDef( $intContractTerminationRequestId, NULL, false ) );
	}

	public function getContractTerminationRequestId() {
		return $this->m_intContractTerminationRequestId;
	}

	public function sqlContractTerminationRequestId() {
		return ( true == isset( $this->m_intContractTerminationRequestId ) ) ? ( string ) $this->m_intContractTerminationRequestId : 'NULL';
	}

	public function setContractProductId( $intContractProductId ) {
		$this->set( 'm_intContractProductId', CStrings::strToIntDef( $intContractProductId, NULL, false ) );
	}

	public function getContractProductId() {
		return $this->m_intContractProductId;
	}

	public function sqlContractProductId() {
		return ( true == isset( $this->m_intContractProductId ) ) ? ( string ) $this->m_intContractProductId : 'NULL';
	}

	public function setRecurringAccountId( $intRecurringAccountId ) {
		$this->set( 'm_intRecurringAccountId', CStrings::strToIntDef( $intRecurringAccountId, NULL, false ) );
	}

	public function getRecurringAccountId() {
		return $this->m_intRecurringAccountId;
	}

	public function sqlRecurringAccountId() {
		return ( true == isset( $this->m_intRecurringAccountId ) ) ? ( string ) $this->m_intRecurringAccountId : 'NULL';
	}

	public function setSetupAccountId( $intSetupAccountId ) {
		$this->set( 'm_intSetupAccountId', CStrings::strToIntDef( $intSetupAccountId, NULL, false ) );
	}

	public function getSetupAccountId() {
		return $this->m_intSetupAccountId;
	}

	public function sqlSetupAccountId() {
		return ( true == isset( $this->m_intSetupAccountId ) ) ? ( string ) $this->m_intSetupAccountId : 'NULL';
	}

	public function setTrainingAccountId( $intTrainingAccountId ) {
		$this->set( 'm_intTrainingAccountId', CStrings::strToIntDef( $intTrainingAccountId, NULL, false ) );
	}

	public function getTrainingAccountId() {
		return $this->m_intTrainingAccountId;
	}

	public function sqlTrainingAccountId() {
		return ( true == isset( $this->m_intTrainingAccountId ) ) ? ( string ) $this->m_intTrainingAccountId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : '4';
	}

	public function setTrainingRate( $fltTrainingRate ) {
		$this->set( 'm_fltTrainingRate', CStrings::strToFloatDef( $fltTrainingRate, NULL, false, 2 ) );
	}

	public function getTrainingRate() {
		return $this->m_fltTrainingRate;
	}

	public function sqlTrainingRate() {
		return ( true == isset( $this->m_fltTrainingRate ) ) ? ( string ) $this->m_fltTrainingRate : '0';
	}

	public function setChargeDatetime( $strChargeDatetime ) {
		$this->set( 'm_strChargeDatetime', CStrings::strTrimDef( $strChargeDatetime, -1, NULL, true ) );
	}

	public function getChargeDatetime() {
		return $this->m_strChargeDatetime;
	}

	public function sqlChargeDatetime() {
		return ( true == isset( $this->m_strChargeDatetime ) ) ? '\'' . $this->m_strChargeDatetime . '\'' : 'NOW()';
	}

	public function setImplementationAmount( $fltImplementationAmount ) {
		$this->set( 'm_fltImplementationAmount', CStrings::strToFloatDef( $fltImplementationAmount, NULL, false, 2 ) );
	}

	public function getImplementationAmount() {
		return $this->m_fltImplementationAmount;
	}

	public function sqlImplementationAmount() {
		return ( true == isset( $this->m_fltImplementationAmount ) ) ? ( string ) $this->m_fltImplementationAmount : '0';
	}

	public function setMonthlyRecurringAmount( $fltMonthlyRecurringAmount ) {
		$this->set( 'm_fltMonthlyRecurringAmount', CStrings::strToFloatDef( $fltMonthlyRecurringAmount, NULL, false, 2 ) );
	}

	public function getMonthlyRecurringAmount() {
		return $this->m_fltMonthlyRecurringAmount;
	}

	public function sqlMonthlyRecurringAmount() {
		return ( true == isset( $this->m_fltMonthlyRecurringAmount ) ) ? ( string ) $this->m_fltMonthlyRecurringAmount : '0';
	}

	public function setTransactionalAmount( $fltTransactionalAmount ) {
		$this->set( 'm_fltTransactionalAmount', CStrings::strToFloatDef( $fltTransactionalAmount, NULL, false, 2 ) );
	}

	public function getTransactionalAmount() {
		return $this->m_fltTransactionalAmount;
	}

	public function sqlTransactionalAmount() {
		return ( true == isset( $this->m_fltTransactionalAmount ) ) ? ( string ) $this->m_fltTransactionalAmount : '0';
	}

	public function setMonthlyChangeAmount( $fltMonthlyChangeAmount ) {
		$this->set( 'm_fltMonthlyChangeAmount', CStrings::strToFloatDef( $fltMonthlyChangeAmount, NULL, false, 2 ) );
	}

	public function getMonthlyChangeAmount() {
		return $this->m_fltMonthlyChangeAmount;
	}

	public function sqlMonthlyChangeAmount() {
		return ( true == isset( $this->m_fltMonthlyChangeAmount ) ) ? ( string ) $this->m_fltMonthlyChangeAmount : '0';
	}

	public function setTransactionalChangeAmount( $fltTransactionalChangeAmount ) {
		$this->set( 'm_fltTransactionalChangeAmount', CStrings::strToFloatDef( $fltTransactionalChangeAmount, NULL, false, 2 ) );
	}

	public function getTransactionalChangeAmount() {
		return $this->m_fltTransactionalChangeAmount;
	}

	public function sqlTransactionalChangeAmount() {
		return ( true == isset( $this->m_fltTransactionalChangeAmount ) ) ? ( string ) $this->m_fltTransactionalChangeAmount : '0';
	}

	public function setPerItemAmount( $fltPerItemAmount ) {
		$this->set( 'm_fltPerItemAmount', CStrings::strToFloatDef( $fltPerItemAmount, NULL, false, 4 ) );
	}

	public function getPerItemAmount() {
		return $this->m_fltPerItemAmount;
	}

	public function sqlPerItemAmount() {
		return ( true == isset( $this->m_fltPerItemAmount ) ) ? ( string ) $this->m_fltPerItemAmount : '0';
	}

	public function setImplementationHours( $intImplementationHours ) {
		$this->set( 'm_intImplementationHours', CStrings::strToIntDef( $intImplementationHours, NULL, false ) );
	}

	public function getImplementationHours() {
		return $this->m_intImplementationHours;
	}

	public function sqlImplementationHours() {
		return ( true == isset( $this->m_intImplementationHours ) ) ? ( string ) $this->m_intImplementationHours : '0';
	}

	public function setDeactivationDate( $strDeactivationDate ) {
		$this->set( 'm_strDeactivationDate', CStrings::strTrimDef( $strDeactivationDate, -1, NULL, true ) );
	}

	public function getDeactivationDate() {
		return $this->m_strDeactivationDate;
	}

	public function sqlDeactivationDate() {
		return ( true == isset( $this->m_strDeactivationDate ) ) ? '\'' . $this->m_strDeactivationDate . '\'' : 'NULL';
	}

	public function setTerminationDate( $strTerminationDate ) {
		$this->set( 'm_strTerminationDate', CStrings::strTrimDef( $strTerminationDate, -1, NULL, true ) );
	}

	public function getTerminationDate() {
		return $this->m_strTerminationDate;
	}

	public function sqlTerminationDate() {
		return ( true == isset( $this->m_strTerminationDate ) ) ? '\'' . $this->m_strTerminationDate . '\'' : 'NULL';
	}

	public function setBillingStartDate( $strBillingStartDate ) {
		$this->set( 'm_strBillingStartDate', CStrings::strTrimDef( $strBillingStartDate, -1, NULL, true ) );
	}

	public function getBillingStartDate() {
		return $this->m_strBillingStartDate;
	}

	public function sqlBillingStartDate() {
		return ( true == isset( $this->m_strBillingStartDate ) ) ? '\'' . $this->m_strBillingStartDate . '\'' : 'NULL';
	}

	public function setNewBillingStartDate( $strNewBillingStartDate ) {
		$this->set( 'm_strNewBillingStartDate', CStrings::strTrimDef( $strNewBillingStartDate, -1, NULL, true ) );
	}

	public function getNewBillingStartDate() {
		return $this->m_strNewBillingStartDate;
	}

	public function sqlNewBillingStartDate() {
		return ( true == isset( $this->m_strNewBillingStartDate ) ) ? '\'' . $this->m_strNewBillingStartDate . '\'' : 'NULL';
	}

	public function setImplementationPostDate( $strImplementationPostDate ) {
		$this->set( 'm_strImplementationPostDate', CStrings::strTrimDef( $strImplementationPostDate, -1, NULL, true ) );
	}

	public function getImplementationPostDate() {
		return $this->m_strImplementationPostDate;
	}

	public function sqlImplementationPostDate() {
		return ( true == isset( $this->m_strImplementationPostDate ) ) ? '\'' . $this->m_strImplementationPostDate . '\'' : 'NULL';
	}

	public function setTrainingPostDate( $strTrainingPostDate ) {
		$this->set( 'm_strTrainingPostDate', CStrings::strTrimDef( $strTrainingPostDate, -1, NULL, true ) );
	}

	public function getTrainingPostDate() {
		return $this->m_strTrainingPostDate;
	}

	public function sqlTrainingPostDate() {
		return ( true == isset( $this->m_strTrainingPostDate ) ) ? '\'' . $this->m_strTrainingPostDate . '\'' : 'NULL';
	}

	public function setCommissionTimerStart( $strCommissionTimerStart ) {
		$this->set( 'm_strCommissionTimerStart', CStrings::strTrimDef( $strCommissionTimerStart, -1, NULL, true ) );
	}

	public function getCommissionTimerStart() {
		return $this->m_strCommissionTimerStart;
	}

	public function sqlCommissionTimerStart() {
		return ( true == isset( $this->m_strCommissionTimerStart ) ) ? '\'' . $this->m_strCommissionTimerStart . '\'' : 'NULL';
	}

	public function setCloseDate( $strCloseDate ) {
		$this->set( 'm_strCloseDate', CStrings::strTrimDef( $strCloseDate, -1, NULL, true ) );
	}

	public function getCloseDate() {
		return $this->m_strCloseDate;
	}

	public function sqlCloseDate() {
		return ( true == isset( $this->m_strCloseDate ) ) ? '\'' . $this->m_strCloseDate . '\'' : 'NULL';
	}

	public function setPilotDueDate( $strPilotDueDate ) {
		$this->set( 'm_strPilotDueDate', CStrings::strTrimDef( $strPilotDueDate, -1, NULL, true ) );
	}

	public function getPilotDueDate() {
		return $this->m_strPilotDueDate;
	}

	public function sqlPilotDueDate() {
		return ( true == isset( $this->m_strPilotDueDate ) ) ? '\'' . $this->m_strPilotDueDate . '\'' : 'NULL';
	}

	public function setIsUnlimited( $intIsUnlimited ) {
		$this->set( 'm_intIsUnlimited', CStrings::strToIntDef( $intIsUnlimited, NULL, false ) );
	}

	public function getIsUnlimited() {
		return $this->m_intIsUnlimited;
	}

	public function sqlIsUnlimited() {
		return ( true == isset( $this->m_intIsUnlimited ) ) ? ( string ) $this->m_intIsUnlimited : '0';
	}

	public function setIsException( $intIsException ) {
		$this->set( 'm_intIsException', CStrings::strToIntDef( $intIsException, NULL, false ) );
	}

	public function getIsException() {
		return $this->m_intIsException;
	}

	public function sqlIsException() {
		return ( true == isset( $this->m_intIsException ) ) ? ( string ) $this->m_intIsException : '0';
	}

	public function setIsImplementationContingent( $intIsImplementationContingent ) {
		$this->set( 'm_intIsImplementationContingent', CStrings::strToIntDef( $intIsImplementationContingent, NULL, false ) );
	}

	public function getIsImplementationContingent() {
		return $this->m_intIsImplementationContingent;
	}

	public function sqlIsImplementationContingent() {
		return ( true == isset( $this->m_intIsImplementationContingent ) ) ? ( string ) $this->m_intIsImplementationContingent : '0';
	}

	public function setImplementedBy( $intImplementedBy ) {
		$this->set( 'm_intImplementedBy', CStrings::strToIntDef( $intImplementedBy, NULL, false ) );
	}

	public function getImplementedBy() {
		return $this->m_intImplementedBy;
	}

	public function sqlImplementedBy() {
		return ( true == isset( $this->m_intImplementedBy ) ) ? ( string ) $this->m_intImplementedBy : 'NULL';
	}

	public function setImplementedOn( $strImplementedOn ) {
		$this->set( 'm_strImplementedOn', CStrings::strTrimDef( $strImplementedOn, -1, NULL, true ) );
	}

	public function getImplementedOn() {
		return $this->m_strImplementedOn;
	}

	public function sqlImplementedOn() {
		return ( true == isset( $this->m_strImplementedOn ) ) ? '\'' . $this->m_strImplementedOn . '\'' : 'NULL';
	}

	public function setImplementationEndDate( $strImplementationEndDate ) {
		$this->set( 'm_strImplementationEndDate', CStrings::strTrimDef( $strImplementationEndDate, -1, NULL, true ) );
	}

	public function getImplementationEndDate() {
		return $this->m_strImplementationEndDate;
	}

	public function sqlImplementationEndDate() {
		return ( true == isset( $this->m_strImplementationEndDate ) ) ? '\'' . $this->m_strImplementationEndDate . '\'' : 'NULL';
	}

	public function setImplementationRevertedBy( $intImplementationRevertedBy ) {
		$this->set( 'm_intImplementationRevertedBy', CStrings::strToIntDef( $intImplementationRevertedBy, NULL, false ) );
	}

	public function getImplementationRevertedBy() {
		return $this->m_intImplementationRevertedBy;
	}

	public function sqlImplementationRevertedBy() {
		return ( true == isset( $this->m_intImplementationRevertedBy ) ) ? ( string ) $this->m_intImplementationRevertedBy : 'NULL';
	}

	public function setImplementationRevertedOn( $strImplementationRevertedOn ) {
		$this->set( 'm_strImplementationRevertedOn', CStrings::strTrimDef( $strImplementationRevertedOn, -1, NULL, true ) );
	}

	public function getImplementationRevertedOn() {
		return $this->m_strImplementationRevertedOn;
	}

	public function sqlImplementationRevertedOn() {
		return ( true == isset( $this->m_strImplementationRevertedOn ) ) ? '\'' . $this->m_strImplementationRevertedOn . '\'' : 'NULL';
	}

	public function setReimplementedBy( $intReimplementedBy ) {
		$this->set( 'm_intReimplementedBy', CStrings::strToIntDef( $intReimplementedBy, NULL, false ) );
	}

	public function getReimplementedBy() {
		return $this->m_intReimplementedBy;
	}

	public function sqlReimplementedBy() {
		return ( true == isset( $this->m_intReimplementedBy ) ) ? ( string ) $this->m_intReimplementedBy : 'NULL';
	}

	public function setReimplementedOn( $strReimplementedOn ) {
		$this->set( 'm_strReimplementedOn', CStrings::strTrimDef( $strReimplementedOn, -1, NULL, true ) );
	}

	public function getReimplementedOn() {
		return $this->m_strReimplementedOn;
	}

	public function sqlReimplementedOn() {
		return ( true == isset( $this->m_strReimplementedOn ) ) ? '\'' . $this->m_strReimplementedOn . '\'' : 'NULL';
	}

	public function setLastPostedOn( $strLastPostedOn ) {
		$this->set( 'm_strLastPostedOn', CStrings::strTrimDef( $strLastPostedOn, -1, NULL, true ) );
	}

	public function getLastPostedOn() {
		return $this->m_strLastPostedOn;
	}

	public function sqlLastPostedOn() {
		return ( true == isset( $this->m_strLastPostedOn ) ) ? '\'' . $this->m_strLastPostedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIgnoreInForecasting( $boolIgnoreInForecasting ) {
		$this->set( 'm_boolIgnoreInForecasting', CStrings::strToBool( $boolIgnoreInForecasting ) );
	}

	public function getIgnoreInForecasting() {
		return $this->m_boolIgnoreInForecasting;
	}

	public function sqlIgnoreInForecasting() {
		return ( true == isset( $this->m_boolIgnoreInForecasting ) ) ? '\'' . ( true == ( bool ) $this->m_boolIgnoreInForecasting ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDuplicated( $boolIsDuplicated ) {
		$this->set( 'm_boolIsDuplicated', CStrings::strToBool( $boolIsDuplicated ) );
	}

	public function getIsDuplicated() {
		return $this->m_boolIsDuplicated;
	}

	public function sqlIsDuplicated() {
		return ( true == isset( $this->m_boolIsDuplicated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDuplicated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSoftTerminationDate( $strSoftTerminationDate ) {
		$this->set( 'm_strSoftTerminationDate', CStrings::strTrimDef( $strSoftTerminationDate, -1, NULL, true ) );
	}

	public function getSoftTerminationDate() {
		return $this->m_strSoftTerminationDate;
	}

	public function sqlSoftTerminationDate() {
		return ( true == isset( $this->m_strSoftTerminationDate ) ) ? '\'' . $this->m_strSoftTerminationDate . '\'' : 'NULL';
	}

	public function setFirstSubscriptionMonth( $strFirstSubscriptionMonth ) {
		$this->set( 'm_strFirstSubscriptionMonth', CStrings::strTrimDef( $strFirstSubscriptionMonth, -1, NULL, true ) );
	}

	public function getFirstSubscriptionMonth() {
		return $this->m_strFirstSubscriptionMonth;
	}

	public function sqlFirstSubscriptionMonth() {
		return ( true == isset( $this->m_strFirstSubscriptionMonth ) ) ? '\'' . $this->m_strFirstSubscriptionMonth . '\'' : 'NULL';
	}

	public function setFirstTransactionalMonth( $strFirstTransactionalMonth ) {
		$this->set( 'm_strFirstTransactionalMonth', CStrings::strTrimDef( $strFirstTransactionalMonth, -1, NULL, true ) );
	}

	public function getFirstTransactionalMonth() {
		return $this->m_strFirstTransactionalMonth;
	}

	public function sqlFirstTransactionalMonth() {
		return ( true == isset( $this->m_strFirstTransactionalMonth ) ) ? '\'' . $this->m_strFirstTransactionalMonth . '\'' : 'NULL';
	}

	public function setOriginalBillingStartDate( $strOriginalBillingStartDate ) {
		$this->set( 'm_strOriginalBillingStartDate', CStrings::strTrimDef( $strOriginalBillingStartDate, -1, NULL, true ) );
	}

	public function getOriginalBillingStartDate() {
		return $this->m_strOriginalBillingStartDate;
	}

	public function sqlOriginalBillingStartDate() {
		return ( true == isset( $this->m_strOriginalBillingStartDate ) ) ? '\'' . $this->m_strOriginalBillingStartDate . '\'' : 'NULL';
	}

	public function setIsStaging( $boolIsStaging ) {
		$this->set( 'm_boolIsStaging', CStrings::strToBool( $boolIsStaging ) );
	}

	public function getIsStaging() {
		return $this->m_boolIsStaging;
	}

	public function sqlIsStaging() {
		return ( true == isset( $this->m_boolIsStaging ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsStaging ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTransferredOut( $boolIsTransferredOut ) {
		$this->set( 'm_boolIsTransferredOut', CStrings::strToBool( $boolIsTransferredOut ) );
	}

	public function getIsTransferredOut() {
		return $this->m_boolIsTransferredOut;
	}

	public function sqlIsTransferredOut() {
		return ( true == isset( $this->m_boolIsTransferredOut ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTransferredOut ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTransferredIn( $boolIsTransferredIn ) {
		$this->set( 'm_boolIsTransferredIn', CStrings::strToBool( $boolIsTransferredIn ) );
	}

	public function getIsTransferredIn() {
		return $this->m_boolIsTransferredIn;
	}

	public function sqlIsTransferredIn() {
		return ( true == isset( $this->m_boolIsTransferredIn ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTransferredIn ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPreFixCloseDate( $strPreFixCloseDate ) {
		$this->set( 'm_strPreFixCloseDate', CStrings::strTrimDef( $strPreFixCloseDate, -1, NULL, true ) );
	}

	public function getPreFixCloseDate() {
		return $this->m_strPreFixCloseDate;
	}

	public function sqlPreFixCloseDate() {
		return ( true == isset( $this->m_strPreFixCloseDate ) ) ? '\'' . $this->m_strPreFixCloseDate . '\'' : 'NULL';
	}

	public function setPreFixBillingStartDate( $strPreFixBillingStartDate ) {
		$this->set( 'm_strPreFixBillingStartDate', CStrings::strTrimDef( $strPreFixBillingStartDate, -1, NULL, true ) );
	}

	public function getPreFixBillingStartDate() {
		return $this->m_strPreFixBillingStartDate;
	}

	public function sqlPreFixBillingStartDate() {
		return ( true == isset( $this->m_strPreFixBillingStartDate ) ) ? '\'' . $this->m_strPreFixBillingStartDate . '\'' : 'NULL';
	}

	public function setLastSubscriptionMonth( $strLastSubscriptionMonth ) {
		$this->set( 'm_strLastSubscriptionMonth', CStrings::strTrimDef( $strLastSubscriptionMonth, -1, NULL, true ) );
	}

	public function getLastSubscriptionMonth() {
		return $this->m_strLastSubscriptionMonth;
	}

	public function sqlLastSubscriptionMonth() {
		return ( true == isset( $this->m_strLastSubscriptionMonth ) ) ? '\'' . $this->m_strLastSubscriptionMonth . '\'' : 'NULL';
	}

	public function setLastTransactionalMonth( $strLastTransactionalMonth ) {
		$this->set( 'm_strLastTransactionalMonth', CStrings::strTrimDef( $strLastTransactionalMonth, -1, NULL, true ) );
	}

	public function getLastTransactionalMonth() {
		return $this->m_strLastTransactionalMonth;
	}

	public function sqlLastTransactionalMonth() {
		return ( true == isset( $this->m_strLastTransactionalMonth ) ) ? '\'' . $this->m_strLastTransactionalMonth . '\'' : 'NULL';
	}

	public function setEffectiveDate( $strEffectiveDate ) {
		$this->set( 'm_strEffectiveDate', CStrings::strTrimDef( $strEffectiveDate, -1, NULL, true ) );
	}

	public function getEffectiveDate() {
		return $this->m_strEffectiveDate;
	}

	public function sqlEffectiveDate() {
		return ( true == isset( $this->m_strEffectiveDate ) ) ? '\'' . $this->m_strEffectiveDate . '\'' : 'NULL';
	}

	public function setContractChainId( $intContractChainId ) {
		$this->set( 'm_intContractChainId', CStrings::strToIntDef( $intContractChainId, NULL, false ) );
	}

	public function getContractChainId() {
		return $this->m_intContractChainId;
	}

	public function sqlContractChainId() {
		return ( true == isset( $this->m_intContractChainId ) ) ? ( string ) $this->m_intContractChainId : 'NULL';
	}

	public function setContractChainRank( $intContractChainRank ) {
		$this->set( 'm_intContractChainRank', CStrings::strToIntDef( $intContractChainRank, NULL, false ) );
	}

	public function getContractChainRank() {
		return $this->m_intContractChainRank;
	}

	public function sqlContractChainRank() {
		return ( true == isset( $this->m_intContractChainRank ) ) ? ( string ) $this->m_intContractChainRank : 'NULL';
	}

	public function setIsLastContractRecord( $boolIsLastContractRecord ) {
		$this->set( 'm_boolIsLastContractRecord', CStrings::strToBool( $boolIsLastContractRecord ) );
	}

	public function getIsLastContractRecord() {
		return $this->m_boolIsLastContractRecord;
	}

	public function sqlIsLastContractRecord() {
		return ( true == isset( $this->m_boolIsLastContractRecord ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLastContractRecord ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCachedNumberOfUnits( $intCachedNumberOfUnits ) {
		$this->set( 'm_intCachedNumberOfUnits', CStrings::strToIntDef( $intCachedNumberOfUnits, NULL, false ) );
	}

	public function getCachedNumberOfUnits() {
		return $this->m_intCachedNumberOfUnits;
	}

	public function sqlCachedNumberOfUnits() {
		return ( true == isset( $this->m_intCachedNumberOfUnits ) ) ? ( string ) $this->m_intCachedNumberOfUnits : '0';
	}

	public function setHasNonContiguousContracts( $boolHasNonContiguousContracts ) {
		$this->set( 'm_boolHasNonContiguousContracts', CStrings::strToBool( $boolHasNonContiguousContracts ) );
	}

	public function getHasNonContiguousContracts() {
		return $this->m_boolHasNonContiguousContracts;
	}

	public function sqlHasNonContiguousContracts() {
		return ( true == isset( $this->m_boolHasNonContiguousContracts ) ) ? '\'' . ( true == ( bool ) $this->m_boolHasNonContiguousContracts ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setTerminationBucketId( $intTerminationBucketId ) {
		$this->set( 'm_intTerminationBucketId', CStrings::strToIntDef( $intTerminationBucketId, NULL, false ) );
	}

	public function getTerminationBucketId() {
		return $this->m_intTerminationBucketId;
	}

	public function sqlTerminationBucketId() {
		return ( true == isset( $this->m_intTerminationBucketId ) ) ? ( string ) $this->m_intTerminationBucketId : 'NULL';
	}

	public function setFirstConsistentSubscriptionMonth( $strFirstConsistentSubscriptionMonth ) {
		$this->set( 'm_strFirstConsistentSubscriptionMonth', CStrings::strTrimDef( $strFirstConsistentSubscriptionMonth, -1, NULL, true ) );
	}

	public function getFirstConsistentSubscriptionMonth() {
		return $this->m_strFirstConsistentSubscriptionMonth;
	}

	public function sqlFirstConsistentSubscriptionMonth() {
		return ( true == isset( $this->m_strFirstConsistentSubscriptionMonth ) ) ? '\'' . $this->m_strFirstConsistentSubscriptionMonth . '\'' : 'NULL';
	}

	public function setLastConsistentSubscriptionMonth( $strLastConsistentSubscriptionMonth ) {
		$this->set( 'm_strLastConsistentSubscriptionMonth', CStrings::strTrimDef( $strLastConsistentSubscriptionMonth, -1, NULL, true ) );
	}

	public function getLastConsistentSubscriptionMonth() {
		return $this->m_strLastConsistentSubscriptionMonth;
	}

	public function sqlLastConsistentSubscriptionMonth() {
		return ( true == isset( $this->m_strLastConsistentSubscriptionMonth ) ) ? '\'' . $this->m_strLastConsistentSubscriptionMonth . '\'' : 'NULL';
	}

	public function setPackageProductId( $intLeadPackageId ) {
		$this->set( 'm_intPackageProductId', CStrings::strToIntDef( $intLeadPackageId, NULL, false ) );
	}

	public function getPackageProductId() {
		return $this->m_intPackageProductId;
	}

	public function sqlPackageProductId() {
		return ( true == isset( $this->m_intPackageProductId ) ) ? ( string ) $this->m_intPackageProductId : 'NULL';
	}

	public function setIsFirstClientContract( $boolIsFirstClientContract ) {
		$this->set( 'm_boolIsFirstClientContract', CStrings::strToBool( $boolIsFirstClientContract ) );
	}

	public function getIsFirstClientContract() {
		return $this->m_boolIsFirstClientContract;
	}

	public function sqlIsFirstClientContract() {
		return ( true == isset( $this->m_boolIsFirstClientContract ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFirstClientContract ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFirstPropertyContract( $boolIsFirstPropertyContract ) {
		$this->set( 'm_boolIsFirstPropertyContract', CStrings::strToBool( $boolIsFirstPropertyContract ) );
	}

	public function getIsFirstPropertyContract() {
		return $this->m_boolIsFirstPropertyContract;
	}

	public function sqlIsFirstPropertyContract() {
		return ( true == isset( $this->m_boolIsFirstPropertyContract ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFirstPropertyContract ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFirstProductContract( $boolIsFirstProductContract ) {
		$this->set( 'm_boolIsFirstProductContract', CStrings::strToBool( $boolIsFirstProductContract ) );
	}

	public function getIsFirstProductContract() {
		return $this->m_boolIsFirstProductContract;
	}

	public function sqlIsFirstProductContract() {
		return ( true == isset( $this->m_boolIsFirstProductContract ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFirstProductContract ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLastClientTermination( $boolIsLastClientTermination ) {
		$this->set( 'm_boolIsLastClientTermination', CStrings::strToBool( $boolIsLastClientTermination ) );
	}

	public function getIsLastClientTermination() {
		return $this->m_boolIsLastClientTermination;
	}

	public function sqlIsLastClientTermination() {
		return ( true == isset( $this->m_boolIsLastClientTermination ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLastClientTermination ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLastPropertyTermination( $boolIsLastPropertyTermination ) {
		$this->set( 'm_boolIsLastPropertyTermination', CStrings::strToBool( $boolIsLastPropertyTermination ) );
	}

	public function getIsLastPropertyTermination() {
		return $this->m_boolIsLastPropertyTermination;
	}

	public function sqlIsLastPropertyTermination() {
		return ( true == isset( $this->m_boolIsLastPropertyTermination ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLastPropertyTermination ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLastProductTermination( $boolIsLastProductTermination ) {
		$this->set( 'm_boolIsLastProductTermination', CStrings::strToBool( $boolIsLastProductTermination ) );
	}

	public function getIsLastProductTermination() {
		return $this->m_boolIsLastProductTermination;
	}

	public function sqlIsLastProductTermination() {
		return ( true == isset( $this->m_boolIsLastProductTermination ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLastProductTermination ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLastClientImplementation( $boolIsLastClientImplementation ) {
		$this->set( 'm_boolIsLastClientImplementation', CStrings::strToBool( $boolIsLastClientImplementation ) );
	}

	public function getIsLastClientImplementation() {
		return $this->m_boolIsLastClientImplementation;
	}

	public function sqlIsLastClientImplementation() {
		return ( true == isset( $this->m_boolIsLastClientImplementation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLastClientImplementation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLastPropertyImplementation( $boolIsLastPropertyImplementation ) {
		$this->set( 'm_boolIsLastPropertyImplementation', CStrings::strToBool( $boolIsLastPropertyImplementation ) );
	}

	public function getIsLastPropertyImplementation() {
		return $this->m_boolIsLastPropertyImplementation;
	}

	public function sqlIsLastPropertyImplementation() {
		return ( true == isset( $this->m_boolIsLastPropertyImplementation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLastPropertyImplementation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLastProductImplementation( $boolIsLastProductImplementation ) {
		$this->set( 'm_boolIsLastProductImplementation', CStrings::strToBool( $boolIsLastProductImplementation ) );
	}

	public function getIsLastProductImplementation() {
		return $this->m_boolIsLastProductImplementation;
	}

	public function sqlIsLastProductImplementation() {
		return ( true == isset( $this->m_boolIsLastProductImplementation ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLastProductImplementation ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLastClientBilling( $boolIsLastClientBilling ) {
		$this->set( 'm_boolIsLastClientBilling', CStrings::strToBool( $boolIsLastClientBilling ) );
	}

	public function getIsLastClientBilling() {
		return $this->m_boolIsLastClientBilling;
	}

	public function sqlIsLastClientBilling() {
		return ( true == isset( $this->m_boolIsLastClientBilling ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLastClientBilling ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLastPropertyBilling( $boolIsLastPropertyBilling ) {
		$this->set( 'm_boolIsLastPropertyBilling', CStrings::strToBool( $boolIsLastPropertyBilling ) );
	}

	public function getIsLastPropertyBilling() {
		return $this->m_boolIsLastPropertyBilling;
	}

	public function sqlIsLastPropertyBilling() {
		return ( true == isset( $this->m_boolIsLastPropertyBilling ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLastPropertyBilling ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLastProductBilling( $boolIsLastProductBilling ) {
		$this->set( 'm_boolIsLastProductBilling', CStrings::strToBool( $boolIsLastProductBilling ) );
	}

	public function getIsLastProductBilling() {
		return $this->m_boolIsLastProductBilling;
	}

	public function sqlIsLastProductBilling() {
		return ( true == isset( $this->m_boolIsLastProductBilling ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLastProductBilling ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, contract_id, currency_code, commission_bucket_id, property_id, ps_product_id, bundle_ps_product_id, original_contract_property_id, renewal_contract_property_id, temp_renewal_contract_property_id, recurring_company_charge_id, training_employee_id, implementation_employee_id, implementation_delay_type_id, training_transaction_id, implementation_transaction_id, contract_termination_request_id, contract_product_id, recurring_account_id, setup_account_id, training_account_id, frequency_id, training_rate, charge_datetime, implementation_amount, monthly_recurring_amount, transactional_amount, monthly_change_amount, transactional_change_amount, per_item_amount, implementation_hours, deactivation_date, termination_date, billing_start_date, new_billing_start_date, implementation_post_date, training_post_date, commission_timer_start, close_date, pilot_due_date, is_unlimited, is_exception, is_implementation_contingent, implemented_by, implemented_on, implementation_end_date, implementation_reverted_by, implementation_reverted_on, reimplemented_by, reimplemented_on, last_posted_on, updated_by, updated_on, created_by, created_on, ignore_in_forecasting, is_duplicated, soft_termination_date, first_subscription_month, first_transactional_month, original_billing_start_date, is_staging, details, is_transferred_out, is_transferred_in, pre_fix_close_date, pre_fix_billing_start_date, last_subscription_month, last_transactional_month, effective_date, contract_chain_id, contract_chain_rank, is_last_contract_record, cached_number_of_units, has_non_contiguous_contracts, termination_bucket_id, first_consistent_subscription_month, last_consistent_subscription_month, package_product_id, is_first_client_contract, is_first_property_contract, is_first_product_contract, is_last_client_termination, is_last_property_termination, is_last_product_termination, is_last_client_implementation, is_last_property_implementation, is_last_product_implementation, is_last_client_billing, is_last_property_billing, is_last_product_billing )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlContractId() . ', ' .
						$this->sqlCurrencyCode() . ', ' .
						$this->sqlCommissionBucketId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlBundlePsProductId() . ', ' .
						$this->sqlOriginalContractPropertyId() . ', ' .
						$this->sqlRenewalContractPropertyId() . ', ' .
						$this->sqlTempRenewalContractPropertyId() . ', ' .
						$this->sqlRecurringCompanyChargeId() . ', ' .
						$this->sqlTrainingEmployeeId() . ', ' .
						$this->sqlImplementationEmployeeId() . ', ' .
						$this->sqlImplementationDelayTypeId() . ', ' .
						$this->sqlTrainingTransactionId() . ', ' .
						$this->sqlImplementationTransactionId() . ', ' .
						$this->sqlContractTerminationRequestId() . ', ' .
						$this->sqlContractProductId() . ', ' .
						$this->sqlRecurringAccountId() . ', ' .
						$this->sqlSetupAccountId() . ', ' .
						$this->sqlTrainingAccountId() . ', ' .
						$this->sqlFrequencyId() . ', ' .
						$this->sqlTrainingRate() . ', ' .
						$this->sqlChargeDatetime() . ', ' .
						$this->sqlImplementationAmount() . ', ' .
						$this->sqlMonthlyRecurringAmount() . ', ' .
						$this->sqlTransactionalAmount() . ', ' .
						$this->sqlMonthlyChangeAmount() . ', ' .
						$this->sqlTransactionalChangeAmount() . ', ' .
						$this->sqlPerItemAmount() . ', ' .
						$this->sqlImplementationHours() . ', ' .
						$this->sqlDeactivationDate() . ', ' .
						$this->sqlTerminationDate() . ', ' .
						$this->sqlBillingStartDate() . ', ' .
						$this->sqlNewBillingStartDate() . ', ' .
						$this->sqlImplementationPostDate() . ', ' .
						$this->sqlTrainingPostDate() . ', ' .
						$this->sqlCommissionTimerStart() . ', ' .
						$this->sqlCloseDate() . ', ' .
						$this->sqlPilotDueDate() . ', ' .
						$this->sqlIsUnlimited() . ', ' .
						$this->sqlIsException() . ', ' .
						$this->sqlIsImplementationContingent() . ', ' .
						$this->sqlImplementedBy() . ', ' .
						$this->sqlImplementedOn() . ', ' .
						$this->sqlImplementationEndDate() . ', ' .
						$this->sqlImplementationRevertedBy() . ', ' .
						$this->sqlImplementationRevertedOn() . ', ' .
						$this->sqlReimplementedBy() . ', ' .
						$this->sqlReimplementedOn() . ', ' .
						$this->sqlLastPostedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIgnoreInForecasting() . ', ' .
						$this->sqlIsDuplicated() . ', ' .
						$this->sqlSoftTerminationDate() . ', ' .
						$this->sqlFirstSubscriptionMonth() . ', ' .
						$this->sqlFirstTransactionalMonth() . ', ' .
						$this->sqlOriginalBillingStartDate() . ', ' .
						$this->sqlIsStaging() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsTransferredOut() . ', ' .
						$this->sqlIsTransferredIn() . ', ' .
						$this->sqlPreFixCloseDate() . ', ' .
						$this->sqlPreFixBillingStartDate() . ', ' .
						$this->sqlLastSubscriptionMonth() . ', ' .
						$this->sqlLastTransactionalMonth() . ', ' .
						$this->sqlEffectiveDate() . ', ' .
						$this->sqlContractChainId() . ', ' .
						$this->sqlContractChainRank() . ', ' .
						$this->sqlIsLastContractRecord() . ', ' .
						$this->sqlCachedNumberOfUnits() . ', ' .
						$this->sqlHasNonContiguousContracts() . ', ' .
						$this->sqlTerminationBucketId() . ', ' .
						$this->sqlFirstConsistentSubscriptionMonth() . ', ' .
						$this->sqlLastConsistentSubscriptionMonth() . ', ' .
						$this->sqlPackageProductId() . ', ' .
						$this->sqlIsFirstClientContract() . ', ' .
						$this->sqlIsFirstPropertyContract() . ', ' .
						$this->sqlIsFirstProductContract() . ', ' .
						$this->sqlIsLastClientTermination() . ', ' .
						$this->sqlIsLastPropertyTermination() . ', ' .
						$this->sqlIsLastProductTermination() . ', ' .
						$this->sqlIsLastClientImplementation() . ', ' .
						$this->sqlIsLastPropertyImplementation() . ', ' .
						$this->sqlIsLastProductImplementation() . ', ' .
						$this->sqlIsLastClientBilling() . ', ' .
						$this->sqlIsLastPropertyBilling() . ', ' .
						$this->sqlIsLastProductBilling() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId(). ',' ; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_bucket_id = ' . $this->sqlCommissionBucketId(). ',' ; } elseif( true == array_key_exists( 'CommissionBucketId', $this->getChangedColumns() ) ) { $strSql .= ' commission_bucket_id = ' . $this->sqlCommissionBucketId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bundle_ps_product_id = ' . $this->sqlBundlePsProductId(). ',' ; } elseif( true == array_key_exists( 'BundlePsProductId', $this->getChangedColumns() ) ) { $strSql .= ' bundle_ps_product_id = ' . $this->sqlBundlePsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_contract_property_id = ' . $this->sqlOriginalContractPropertyId(). ',' ; } elseif( true == array_key_exists( 'OriginalContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' original_contract_property_id = ' . $this->sqlOriginalContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_contract_property_id = ' . $this->sqlRenewalContractPropertyId(). ',' ; } elseif( true == array_key_exists( 'RenewalContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' renewal_contract_property_id = ' . $this->sqlRenewalContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' temp_renewal_contract_property_id = ' . $this->sqlTempRenewalContractPropertyId(). ',' ; } elseif( true == array_key_exists( 'TempRenewalContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' temp_renewal_contract_property_id = ' . $this->sqlTempRenewalContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recurring_company_charge_id = ' . $this->sqlRecurringCompanyChargeId(). ',' ; } elseif( true == array_key_exists( 'RecurringCompanyChargeId', $this->getChangedColumns() ) ) { $strSql .= ' recurring_company_charge_id = ' . $this->sqlRecurringCompanyChargeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_employee_id = ' . $this->sqlTrainingEmployeeId(). ',' ; } elseif( true == array_key_exists( 'TrainingEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' training_employee_id = ' . $this->sqlTrainingEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_employee_id = ' . $this->sqlImplementationEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ImplementationEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_employee_id = ' . $this->sqlImplementationEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_delay_type_id = ' . $this->sqlImplementationDelayTypeId(). ',' ; } elseif( true == array_key_exists( 'ImplementationDelayTypeId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_delay_type_id = ' . $this->sqlImplementationDelayTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_transaction_id = ' . $this->sqlTrainingTransactionId(). ',' ; } elseif( true == array_key_exists( 'TrainingTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' training_transaction_id = ' . $this->sqlTrainingTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_transaction_id = ' . $this->sqlImplementationTransactionId(). ',' ; } elseif( true == array_key_exists( 'ImplementationTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_transaction_id = ' . $this->sqlImplementationTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_termination_request_id = ' . $this->sqlContractTerminationRequestId(). ',' ; } elseif( true == array_key_exists( 'ContractTerminationRequestId', $this->getChangedColumns() ) ) { $strSql .= ' contract_termination_request_id = ' . $this->sqlContractTerminationRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_product_id = ' . $this->sqlContractProductId(). ',' ; } elseif( true == array_key_exists( 'ContractProductId', $this->getChangedColumns() ) ) { $strSql .= ' contract_product_id = ' . $this->sqlContractProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recurring_account_id = ' . $this->sqlRecurringAccountId(). ',' ; } elseif( true == array_key_exists( 'RecurringAccountId', $this->getChangedColumns() ) ) { $strSql .= ' recurring_account_id = ' . $this->sqlRecurringAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' setup_account_id = ' . $this->sqlSetupAccountId(). ',' ; } elseif( true == array_key_exists( 'SetupAccountId', $this->getChangedColumns() ) ) { $strSql .= ' setup_account_id = ' . $this->sqlSetupAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_account_id = ' . $this->sqlTrainingAccountId(). ',' ; } elseif( true == array_key_exists( 'TrainingAccountId', $this->getChangedColumns() ) ) { $strSql .= ' training_account_id = ' . $this->sqlTrainingAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId(). ',' ; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_rate = ' . $this->sqlTrainingRate(). ',' ; } elseif( true == array_key_exists( 'TrainingRate', $this->getChangedColumns() ) ) { $strSql .= ' training_rate = ' . $this->sqlTrainingRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_datetime = ' . $this->sqlChargeDatetime(). ',' ; } elseif( true == array_key_exists( 'ChargeDatetime', $this->getChangedColumns() ) ) { $strSql .= ' charge_datetime = ' . $this->sqlChargeDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_amount = ' . $this->sqlImplementationAmount(). ',' ; } elseif( true == array_key_exists( 'ImplementationAmount', $this->getChangedColumns() ) ) { $strSql .= ' implementation_amount = ' . $this->sqlImplementationAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_recurring_amount = ' . $this->sqlMonthlyRecurringAmount(). ',' ; } elseif( true == array_key_exists( 'MonthlyRecurringAmount', $this->getChangedColumns() ) ) { $strSql .= ' monthly_recurring_amount = ' . $this->sqlMonthlyRecurringAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transactional_amount = ' . $this->sqlTransactionalAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionalAmount', $this->getChangedColumns() ) ) { $strSql .= ' transactional_amount = ' . $this->sqlTransactionalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_change_amount = ' . $this->sqlMonthlyChangeAmount(). ',' ; } elseif( true == array_key_exists( 'MonthlyChangeAmount', $this->getChangedColumns() ) ) { $strSql .= ' monthly_change_amount = ' . $this->sqlMonthlyChangeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transactional_change_amount = ' . $this->sqlTransactionalChangeAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionalChangeAmount', $this->getChangedColumns() ) ) { $strSql .= ' transactional_change_amount = ' . $this->sqlTransactionalChangeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' per_item_amount = ' . $this->sqlPerItemAmount(). ',' ; } elseif( true == array_key_exists( 'PerItemAmount', $this->getChangedColumns() ) ) { $strSql .= ' per_item_amount = ' . $this->sqlPerItemAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_hours = ' . $this->sqlImplementationHours(). ',' ; } elseif( true == array_key_exists( 'ImplementationHours', $this->getChangedColumns() ) ) { $strSql .= ' implementation_hours = ' . $this->sqlImplementationHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deactivation_date = ' . $this->sqlDeactivationDate(). ',' ; } elseif( true == array_key_exists( 'DeactivationDate', $this->getChangedColumns() ) ) { $strSql .= ' deactivation_date = ' . $this->sqlDeactivationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate(). ',' ; } elseif( true == array_key_exists( 'TerminationDate', $this->getChangedColumns() ) ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_start_date = ' . $this->sqlBillingStartDate(). ',' ; } elseif( true == array_key_exists( 'BillingStartDate', $this->getChangedColumns() ) ) { $strSql .= ' billing_start_date = ' . $this->sqlBillingStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_billing_start_date = ' . $this->sqlNewBillingStartDate(). ',' ; } elseif( true == array_key_exists( 'NewBillingStartDate', $this->getChangedColumns() ) ) { $strSql .= ' new_billing_start_date = ' . $this->sqlNewBillingStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_post_date = ' . $this->sqlImplementationPostDate(). ',' ; } elseif( true == array_key_exists( 'ImplementationPostDate', $this->getChangedColumns() ) ) { $strSql .= ' implementation_post_date = ' . $this->sqlImplementationPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_post_date = ' . $this->sqlTrainingPostDate(). ',' ; } elseif( true == array_key_exists( 'TrainingPostDate', $this->getChangedColumns() ) ) { $strSql .= ' training_post_date = ' . $this->sqlTrainingPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_timer_start = ' . $this->sqlCommissionTimerStart(). ',' ; } elseif( true == array_key_exists( 'CommissionTimerStart', $this->getChangedColumns() ) ) { $strSql .= ' commission_timer_start = ' . $this->sqlCommissionTimerStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' close_date = ' . $this->sqlCloseDate(). ',' ; } elseif( true == array_key_exists( 'CloseDate', $this->getChangedColumns() ) ) { $strSql .= ' close_date = ' . $this->sqlCloseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_due_date = ' . $this->sqlPilotDueDate(). ',' ; } elseif( true == array_key_exists( 'PilotDueDate', $this->getChangedColumns() ) ) { $strSql .= ' pilot_due_date = ' . $this->sqlPilotDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_unlimited = ' . $this->sqlIsUnlimited(). ',' ; } elseif( true == array_key_exists( 'IsUnlimited', $this->getChangedColumns() ) ) { $strSql .= ' is_unlimited = ' . $this->sqlIsUnlimited() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_exception = ' . $this->sqlIsException(). ',' ; } elseif( true == array_key_exists( 'IsException', $this->getChangedColumns() ) ) { $strSql .= ' is_exception = ' . $this->sqlIsException() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_implementation_contingent = ' . $this->sqlIsImplementationContingent(). ',' ; } elseif( true == array_key_exists( 'IsImplementationContingent', $this->getChangedColumns() ) ) { $strSql .= ' is_implementation_contingent = ' . $this->sqlIsImplementationContingent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implemented_by = ' . $this->sqlImplementedBy(). ',' ; } elseif( true == array_key_exists( 'ImplementedBy', $this->getChangedColumns() ) ) { $strSql .= ' implemented_by = ' . $this->sqlImplementedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implemented_on = ' . $this->sqlImplementedOn(). ',' ; } elseif( true == array_key_exists( 'ImplementedOn', $this->getChangedColumns() ) ) { $strSql .= ' implemented_on = ' . $this->sqlImplementedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_end_date = ' . $this->sqlImplementationEndDate(). ',' ; } elseif( true == array_key_exists( 'ImplementationEndDate', $this->getChangedColumns() ) ) { $strSql .= ' implementation_end_date = ' . $this->sqlImplementationEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_reverted_by = ' . $this->sqlImplementationRevertedBy(). ',' ; } elseif( true == array_key_exists( 'ImplementationRevertedBy', $this->getChangedColumns() ) ) { $strSql .= ' implementation_reverted_by = ' . $this->sqlImplementationRevertedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_reverted_on = ' . $this->sqlImplementationRevertedOn(). ',' ; } elseif( true == array_key_exists( 'ImplementationRevertedOn', $this->getChangedColumns() ) ) { $strSql .= ' implementation_reverted_on = ' . $this->sqlImplementationRevertedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimplemented_by = ' . $this->sqlReimplementedBy(). ',' ; } elseif( true == array_key_exists( 'ReimplementedBy', $this->getChangedColumns() ) ) { $strSql .= ' reimplemented_by = ' . $this->sqlReimplementedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimplemented_on = ' . $this->sqlReimplementedOn(). ',' ; } elseif( true == array_key_exists( 'ReimplementedOn', $this->getChangedColumns() ) ) { $strSql .= ' reimplemented_on = ' . $this->sqlReimplementedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn(). ',' ; } elseif( true == array_key_exists( 'LastPostedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ignore_in_forecasting = ' . $this->sqlIgnoreInForecasting(). ',' ; } elseif( true == array_key_exists( 'IgnoreInForecasting', $this->getChangedColumns() ) ) { $strSql .= ' ignore_in_forecasting = ' . $this->sqlIgnoreInForecasting() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_duplicated = ' . $this->sqlIsDuplicated(). ',' ; } elseif( true == array_key_exists( 'IsDuplicated', $this->getChangedColumns() ) ) { $strSql .= ' is_duplicated = ' . $this->sqlIsDuplicated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' soft_termination_date = ' . $this->sqlSoftTerminationDate(). ',' ; } elseif( true == array_key_exists( 'SoftTerminationDate', $this->getChangedColumns() ) ) { $strSql .= ' soft_termination_date = ' . $this->sqlSoftTerminationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_subscription_month = ' . $this->sqlFirstSubscriptionMonth(). ',' ; } elseif( true == array_key_exists( 'FirstSubscriptionMonth', $this->getChangedColumns() ) ) { $strSql .= ' first_subscription_month = ' . $this->sqlFirstSubscriptionMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_transactional_month = ' . $this->sqlFirstTransactionalMonth(). ',' ; } elseif( true == array_key_exists( 'FirstTransactionalMonth', $this->getChangedColumns() ) ) { $strSql .= ' first_transactional_month = ' . $this->sqlFirstTransactionalMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_billing_start_date = ' . $this->sqlOriginalBillingStartDate(). ',' ; } elseif( true == array_key_exists( 'OriginalBillingStartDate', $this->getChangedColumns() ) ) { $strSql .= ' original_billing_start_date = ' . $this->sqlOriginalBillingStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_staging = ' . $this->sqlIsStaging(). ',' ; } elseif( true == array_key_exists( 'IsStaging', $this->getChangedColumns() ) ) { $strSql .= ' is_staging = ' . $this->sqlIsStaging() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_transferred_out = ' . $this->sqlIsTransferredOut(). ',' ; } elseif( true == array_key_exists( 'IsTransferredOut', $this->getChangedColumns() ) ) { $strSql .= ' is_transferred_out = ' . $this->sqlIsTransferredOut() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_transferred_in = ' . $this->sqlIsTransferredIn(). ',' ; } elseif( true == array_key_exists( 'IsTransferredIn', $this->getChangedColumns() ) ) { $strSql .= ' is_transferred_in = ' . $this->sqlIsTransferredIn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pre_fix_close_date = ' . $this->sqlPreFixCloseDate(). ',' ; } elseif( true == array_key_exists( 'PreFixCloseDate', $this->getChangedColumns() ) ) { $strSql .= ' pre_fix_close_date = ' . $this->sqlPreFixCloseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pre_fix_billing_start_date = ' . $this->sqlPreFixBillingStartDate(). ',' ; } elseif( true == array_key_exists( 'PreFixBillingStartDate', $this->getChangedColumns() ) ) { $strSql .= ' pre_fix_billing_start_date = ' . $this->sqlPreFixBillingStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_subscription_month = ' . $this->sqlLastSubscriptionMonth(). ',' ; } elseif( true == array_key_exists( 'LastSubscriptionMonth', $this->getChangedColumns() ) ) { $strSql .= ' last_subscription_month = ' . $this->sqlLastSubscriptionMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_transactional_month = ' . $this->sqlLastTransactionalMonth(). ',' ; } elseif( true == array_key_exists( 'LastTransactionalMonth', $this->getChangedColumns() ) ) { $strSql .= ' last_transactional_month = ' . $this->sqlLastTransactionalMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate(). ',' ; } elseif( true == array_key_exists( 'EffectiveDate', $this->getChangedColumns() ) ) { $strSql .= ' effective_date = ' . $this->sqlEffectiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_chain_id = ' . $this->sqlContractChainId(). ',' ; } elseif( true == array_key_exists( 'ContractChainId', $this->getChangedColumns() ) ) { $strSql .= ' contract_chain_id = ' . $this->sqlContractChainId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_chain_rank = ' . $this->sqlContractChainRank(). ',' ; } elseif( true == array_key_exists( 'ContractChainRank', $this->getChangedColumns() ) ) { $strSql .= ' contract_chain_rank = ' . $this->sqlContractChainRank() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_last_contract_record = ' . $this->sqlIsLastContractRecord(). ',' ; } elseif( true == array_key_exists( 'IsLastContractRecord', $this->getChangedColumns() ) ) { $strSql .= ' is_last_contract_record = ' . $this->sqlIsLastContractRecord() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cached_number_of_units = ' . $this->sqlCachedNumberOfUnits(). ',' ; } elseif( true == array_key_exists( 'CachedNumberOfUnits', $this->getChangedColumns() ) ) { $strSql .= ' cached_number_of_units = ' . $this->sqlCachedNumberOfUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_non_contiguous_contracts = ' . $this->sqlHasNonContiguousContracts(). ',' ; } elseif( true == array_key_exists( 'HasNonContiguousContracts', $this->getChangedColumns() ) ) { $strSql .= ' has_non_contiguous_contracts = ' . $this->sqlHasNonContiguousContracts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_bucket_id = ' . $this->sqlTerminationBucketId(). ',' ; } elseif( true == array_key_exists( 'TerminationBucketId', $this->getChangedColumns() ) ) { $strSql .= ' termination_bucket_id = ' . $this->sqlTerminationBucketId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_consistent_subscription_month = ' . $this->sqlFirstConsistentSubscriptionMonth(). ',' ; } elseif( true == array_key_exists( 'FirstConsistentSubscriptionMonth', $this->getChangedColumns() ) ) { $strSql .= ' first_consistent_subscription_month = ' . $this->sqlFirstConsistentSubscriptionMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_consistent_subscription_month = ' . $this->sqlLastConsistentSubscriptionMonth(). ',' ; } elseif( true == array_key_exists( 'LastConsistentSubscriptionMonth', $this->getChangedColumns() ) ) { $strSql .= ' last_consistent_subscription_month = ' . $this->sqlLastConsistentSubscriptionMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' package_product_id = ' . $this->sqlPackageProductId(). ',' ; } elseif( true == array_key_exists( 'PackageProductId', $this->getChangedColumns() ) ) { $strSql .= ' package_product_id = ' . $this->sqlPackageProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_first_client_contract = ' . $this->sqlIsFirstClientContract(). ',' ; } elseif( true == array_key_exists( 'IsFirstClientContract', $this->getChangedColumns() ) ) { $strSql .= ' is_first_client_contract = ' . $this->sqlIsFirstClientContract() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_first_property_contract = ' . $this->sqlIsFirstPropertyContract(). ',' ; } elseif( true == array_key_exists( 'IsFirstPropertyContract', $this->getChangedColumns() ) ) { $strSql .= ' is_first_property_contract = ' . $this->sqlIsFirstPropertyContract() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_first_product_contract = ' . $this->sqlIsFirstProductContract(). ',' ; } elseif( true == array_key_exists( 'IsFirstProductContract', $this->getChangedColumns() ) ) { $strSql .= ' is_first_product_contract = ' . $this->sqlIsFirstProductContract() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_last_client_termination = ' . $this->sqlIsLastClientTermination(). ',' ; } elseif( true == array_key_exists( 'IsLastClientTermination', $this->getChangedColumns() ) ) { $strSql .= ' is_last_client_termination = ' . $this->sqlIsLastClientTermination() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_last_property_termination = ' . $this->sqlIsLastPropertyTermination(). ',' ; } elseif( true == array_key_exists( 'IsLastPropertyTermination', $this->getChangedColumns() ) ) { $strSql .= ' is_last_property_termination = ' . $this->sqlIsLastPropertyTermination() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_last_product_termination = ' . $this->sqlIsLastProductTermination(). ',' ; } elseif( true == array_key_exists( 'IsLastProductTermination', $this->getChangedColumns() ) ) { $strSql .= ' is_last_product_termination = ' . $this->sqlIsLastProductTermination() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_last_client_implementation = ' . $this->sqlIsLastClientImplementation(). ',' ; } elseif( true == array_key_exists( 'IsLastClientImplementation', $this->getChangedColumns() ) ) { $strSql .= ' is_last_client_implementation = ' . $this->sqlIsLastClientImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_last_property_implementation = ' . $this->sqlIsLastPropertyImplementation(). ',' ; } elseif( true == array_key_exists( 'IsLastPropertyImplementation', $this->getChangedColumns() ) ) { $strSql .= ' is_last_property_implementation = ' . $this->sqlIsLastPropertyImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_last_product_implementation = ' . $this->sqlIsLastProductImplementation(). ',' ; } elseif( true == array_key_exists( 'IsLastProductImplementation', $this->getChangedColumns() ) ) { $strSql .= ' is_last_product_implementation = ' . $this->sqlIsLastProductImplementation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_last_client_billing = ' . $this->sqlIsLastClientBilling(). ',' ; } elseif( true == array_key_exists( 'IsLastClientBilling', $this->getChangedColumns() ) ) { $strSql .= ' is_last_client_billing = ' . $this->sqlIsLastClientBilling() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_last_property_billing = ' . $this->sqlIsLastPropertyBilling(). ',' ; } elseif( true == array_key_exists( 'IsLastPropertyBilling', $this->getChangedColumns() ) ) { $strSql .= ' is_last_property_billing = ' . $this->sqlIsLastPropertyBilling() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_last_product_billing = ' . $this->sqlIsLastProductBilling(). ',' ; } elseif( true == array_key_exists( 'IsLastProductBilling', $this->getChangedColumns() ) ) { $strSql .= ' is_last_product_billing = ' . $this->sqlIsLastProductBilling() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'contract_id' => $this->getContractId(),
			'currency_code' => $this->getCurrencyCode(),
			'commission_bucket_id' => $this->getCommissionBucketId(),
			'property_id' => $this->getPropertyId(),
			'ps_product_id' => $this->getPsProductId(),
			'bundle_ps_product_id' => $this->getBundlePsProductId(),
			'original_contract_property_id' => $this->getOriginalContractPropertyId(),
			'renewal_contract_property_id' => $this->getRenewalContractPropertyId(),
			'temp_renewal_contract_property_id' => $this->getTempRenewalContractPropertyId(),
			'recurring_company_charge_id' => $this->getRecurringCompanyChargeId(),
			'training_employee_id' => $this->getTrainingEmployeeId(),
			'implementation_employee_id' => $this->getImplementationEmployeeId(),
			'implementation_delay_type_id' => $this->getImplementationDelayTypeId(),
			'training_transaction_id' => $this->getTrainingTransactionId(),
			'implementation_transaction_id' => $this->getImplementationTransactionId(),
			'contract_termination_request_id' => $this->getContractTerminationRequestId(),
			'contract_product_id' => $this->getContractProductId(),
			'recurring_account_id' => $this->getRecurringAccountId(),
			'setup_account_id' => $this->getSetupAccountId(),
			'training_account_id' => $this->getTrainingAccountId(),
			'frequency_id' => $this->getFrequencyId(),
			'training_rate' => $this->getTrainingRate(),
			'charge_datetime' => $this->getChargeDatetime(),
			'implementation_amount' => $this->getImplementationAmount(),
			'monthly_recurring_amount' => $this->getMonthlyRecurringAmount(),
			'transactional_amount' => $this->getTransactionalAmount(),
			'monthly_change_amount' => $this->getMonthlyChangeAmount(),
			'transactional_change_amount' => $this->getTransactionalChangeAmount(),
			'per_item_amount' => $this->getPerItemAmount(),
			'implementation_hours' => $this->getImplementationHours(),
			'deactivation_date' => $this->getDeactivationDate(),
			'termination_date' => $this->getTerminationDate(),
			'billing_start_date' => $this->getBillingStartDate(),
			'new_billing_start_date' => $this->getNewBillingStartDate(),
			'implementation_post_date' => $this->getImplementationPostDate(),
			'training_post_date' => $this->getTrainingPostDate(),
			'commission_timer_start' => $this->getCommissionTimerStart(),
			'close_date' => $this->getCloseDate(),
			'pilot_due_date' => $this->getPilotDueDate(),
			'is_unlimited' => $this->getIsUnlimited(),
			'is_exception' => $this->getIsException(),
			'is_implementation_contingent' => $this->getIsImplementationContingent(),
			'implemented_by' => $this->getImplementedBy(),
			'implemented_on' => $this->getImplementedOn(),
			'implementation_end_date' => $this->getImplementationEndDate(),
			'implementation_reverted_by' => $this->getImplementationRevertedBy(),
			'implementation_reverted_on' => $this->getImplementationRevertedOn(),
			'reimplemented_by' => $this->getReimplementedBy(),
			'reimplemented_on' => $this->getReimplementedOn(),
			'last_posted_on' => $this->getLastPostedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ignore_in_forecasting' => $this->getIgnoreInForecasting(),
			'is_duplicated' => $this->getIsDuplicated(),
			'soft_termination_date' => $this->getSoftTerminationDate(),
			'first_subscription_month' => $this->getFirstSubscriptionMonth(),
			'first_transactional_month' => $this->getFirstTransactionalMonth(),
			'original_billing_start_date' => $this->getOriginalBillingStartDate(),
			'is_staging' => $this->getIsStaging(),
			'details' => $this->getDetails(),
			'is_transferred_out' => $this->getIsTransferredOut(),
			'is_transferred_in' => $this->getIsTransferredIn(),
			'pre_fix_close_date' => $this->getPreFixCloseDate(),
			'pre_fix_billing_start_date' => $this->getPreFixBillingStartDate(),
			'last_subscription_month' => $this->getLastSubscriptionMonth(),
			'last_transactional_month' => $this->getLastTransactionalMonth(),
			'effective_date' => $this->getEffectiveDate(),
			'contract_chain_id' => $this->getContractChainId(),
			'contract_chain_rank' => $this->getContractChainRank(),
			'is_last_contract_record' => $this->getIsLastContractRecord(),
			'cached_number_of_units' => $this->getCachedNumberOfUnits(),
			'has_non_contiguous_contracts' => $this->getHasNonContiguousContracts(),
			'termination_bucket_id' => $this->getTerminationBucketId(),
			'first_consistent_subscription_month' => $this->getFirstConsistentSubscriptionMonth(),
			'last_consistent_subscription_month' => $this->getLastConsistentSubscriptionMonth(),
			'package_product_id' => $this->getPackageProductId(),
			'is_first_client_contract' => $this->getIsFirstClientContract(),
			'is_first_property_contract' => $this->getIsFirstPropertyContract(),
			'is_first_product_contract' => $this->getIsFirstProductContract(),
			'is_last_client_termination' => $this->getIsLastClientTermination(),
			'is_last_property_termination' => $this->getIsLastPropertyTermination(),
			'is_last_product_termination' => $this->getIsLastProductTermination(),
			'is_last_client_implementation' => $this->getIsLastClientImplementation(),
			'is_last_property_implementation' => $this->getIsLastPropertyImplementation(),
			'is_last_product_implementation' => $this->getIsLastProductImplementation(),
			'is_last_client_billing' => $this->getIsLastClientBilling(),
			'is_last_property_billing' => $this->getIsLastPropertyBilling(),
			'is_last_product_billing' => $this->getIsLastProductBilling()
		);
	}

}
?>