<?php

class CBaseContactSearchDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.contact_search_details';

	protected $m_intId;
	protected $m_intContactSearchTypeId;
	protected $m_intReferenceId;
	protected $m_strSearchString;
	protected $m_strName;
	protected $m_strLocation;
	protected $m_strTwitterUrl;
	protected $m_strLinkedinUrl;
	protected $m_strFacebookUrl;
	protected $m_strWebsiteUrl;
	protected $m_strBiography;
	protected $m_strProfilePicUrl;
	protected $m_strPersonTitle;
	protected $m_strPersonOrganization;
	protected $m_strCompanyFoundedOn;
	protected $m_intCompanyTotalEmployees;
	protected $m_strAdvancedDetails;
	protected $m_jsonAdvancedDetails;
	protected $m_strFailedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intContactSearchTypeId = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['contact_search_type_id'] ) && $boolDirectSet ) $this->set( 'm_intContactSearchTypeId', trim( $arrValues['contact_search_type_id'] ) ); elseif( isset( $arrValues['contact_search_type_id'] ) ) $this->setContactSearchTypeId( $arrValues['contact_search_type_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_intReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['search_string'] ) && $boolDirectSet ) $this->set( 'm_strSearchString', trim( stripcslashes( $arrValues['search_string'] ) ) ); elseif( isset( $arrValues['search_string'] ) ) $this->setSearchString( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['search_string'] ) : $arrValues['search_string'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['location'] ) && $boolDirectSet ) $this->set( 'm_strLocation', trim( stripcslashes( $arrValues['location'] ) ) ); elseif( isset( $arrValues['location'] ) ) $this->setLocation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['location'] ) : $arrValues['location'] );
		if( isset( $arrValues['twitter_url'] ) && $boolDirectSet ) $this->set( 'm_strTwitterUrl', trim( stripcslashes( $arrValues['twitter_url'] ) ) ); elseif( isset( $arrValues['twitter_url'] ) ) $this->setTwitterUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['twitter_url'] ) : $arrValues['twitter_url'] );
		if( isset( $arrValues['linkedin_url'] ) && $boolDirectSet ) $this->set( 'm_strLinkedinUrl', trim( stripcslashes( $arrValues['linkedin_url'] ) ) ); elseif( isset( $arrValues['linkedin_url'] ) ) $this->setLinkedinUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['linkedin_url'] ) : $arrValues['linkedin_url'] );
		if( isset( $arrValues['facebook_url'] ) && $boolDirectSet ) $this->set( 'm_strFacebookUrl', trim( stripcslashes( $arrValues['facebook_url'] ) ) ); elseif( isset( $arrValues['facebook_url'] ) ) $this->setFacebookUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['facebook_url'] ) : $arrValues['facebook_url'] );
		if( isset( $arrValues['website_url'] ) && $boolDirectSet ) $this->set( 'm_strWebsiteUrl', trim( stripcslashes( $arrValues['website_url'] ) ) ); elseif( isset( $arrValues['website_url'] ) ) $this->setWebsiteUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['website_url'] ) : $arrValues['website_url'] );
		if( isset( $arrValues['biography'] ) && $boolDirectSet ) $this->set( 'm_strBiography', trim( stripcslashes( $arrValues['biography'] ) ) ); elseif( isset( $arrValues['biography'] ) ) $this->setBiography( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['biography'] ) : $arrValues['biography'] );
		if( isset( $arrValues['profile_pic_url'] ) && $boolDirectSet ) $this->set( 'm_strProfilePicUrl', trim( stripcslashes( $arrValues['profile_pic_url'] ) ) ); elseif( isset( $arrValues['profile_pic_url'] ) ) $this->setProfilePicUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['profile_pic_url'] ) : $arrValues['profile_pic_url'] );
		if( isset( $arrValues['person_title'] ) && $boolDirectSet ) $this->set( 'm_strPersonTitle', trim( stripcslashes( $arrValues['person_title'] ) ) ); elseif( isset( $arrValues['person_title'] ) ) $this->setPersonTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['person_title'] ) : $arrValues['person_title'] );
		if( isset( $arrValues['person_organization'] ) && $boolDirectSet ) $this->set( 'm_strPersonOrganization', trim( stripcslashes( $arrValues['person_organization'] ) ) ); elseif( isset( $arrValues['person_organization'] ) ) $this->setPersonOrganization( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['person_organization'] ) : $arrValues['person_organization'] );
		if( isset( $arrValues['company_founded_on'] ) && $boolDirectSet ) $this->set( 'm_strCompanyFoundedOn', trim( $arrValues['company_founded_on'] ) ); elseif( isset( $arrValues['company_founded_on'] ) ) $this->setCompanyFoundedOn( $arrValues['company_founded_on'] );
		if( isset( $arrValues['company_total_employees'] ) && $boolDirectSet ) $this->set( 'm_intCompanyTotalEmployees', trim( $arrValues['company_total_employees'] ) ); elseif( isset( $arrValues['company_total_employees'] ) ) $this->setCompanyTotalEmployees( $arrValues['company_total_employees'] );
		if( isset( $arrValues['advanced_details'] ) ) $this->set( 'm_strAdvancedDetails', trim( $arrValues['advanced_details'] ) );
		if( isset( $arrValues['failed_on'] ) && $boolDirectSet ) $this->set( 'm_strFailedOn', trim( $arrValues['failed_on'] ) ); elseif( isset( $arrValues['failed_on'] ) ) $this->setFailedOn( $arrValues['failed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setContactSearchTypeId( $intContactSearchTypeId ) {
		$this->set( 'm_intContactSearchTypeId', CStrings::strToIntDef( $intContactSearchTypeId, NULL, false ) );
	}

	public function getContactSearchTypeId() {
		return $this->m_intContactSearchTypeId;
	}

	public function sqlContactSearchTypeId() {
		return ( true == isset( $this->m_intContactSearchTypeId ) ) ? ( string ) $this->m_intContactSearchTypeId : '1';
	}

	public function setReferenceId( $intReferenceId ) {
		$this->set( 'm_intReferenceId', CStrings::strToIntDef( $intReferenceId, NULL, false ) );
	}

	public function getReferenceId() {
		return $this->m_intReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_intReferenceId ) ) ? ( string ) $this->m_intReferenceId : 'NULL';
	}

	public function setSearchString( $strSearchString ) {
		$this->set( 'm_strSearchString', CStrings::strTrimDef( $strSearchString, 120, NULL, true ) );
	}

	public function getSearchString() {
		return $this->m_strSearchString;
	}

	public function sqlSearchString() {
		return ( true == isset( $this->m_strSearchString ) ) ? '\'' . addslashes( $this->m_strSearchString ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 120, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setLocation( $strLocation ) {
		$this->set( 'm_strLocation', CStrings::strTrimDef( $strLocation, 240, NULL, true ) );
	}

	public function getLocation() {
		return $this->m_strLocation;
	}

	public function sqlLocation() {
		return ( true == isset( $this->m_strLocation ) ) ? '\'' . addslashes( $this->m_strLocation ) . '\'' : 'NULL';
	}

	public function setTwitterUrl( $strTwitterUrl ) {
		$this->set( 'm_strTwitterUrl', CStrings::strTrimDef( $strTwitterUrl, 240, NULL, true ) );
	}

	public function getTwitterUrl() {
		return $this->m_strTwitterUrl;
	}

	public function sqlTwitterUrl() {
		return ( true == isset( $this->m_strTwitterUrl ) ) ? '\'' . addslashes( $this->m_strTwitterUrl ) . '\'' : 'NULL';
	}

	public function setLinkedinUrl( $strLinkedinUrl ) {
		$this->set( 'm_strLinkedinUrl', CStrings::strTrimDef( $strLinkedinUrl, 240, NULL, true ) );
	}

	public function getLinkedinUrl() {
		return $this->m_strLinkedinUrl;
	}

	public function sqlLinkedinUrl() {
		return ( true == isset( $this->m_strLinkedinUrl ) ) ? '\'' . addslashes( $this->m_strLinkedinUrl ) . '\'' : 'NULL';
	}

	public function setFacebookUrl( $strFacebookUrl ) {
		$this->set( 'm_strFacebookUrl', CStrings::strTrimDef( $strFacebookUrl, 240, NULL, true ) );
	}

	public function getFacebookUrl() {
		return $this->m_strFacebookUrl;
	}

	public function sqlFacebookUrl() {
		return ( true == isset( $this->m_strFacebookUrl ) ) ? '\'' . addslashes( $this->m_strFacebookUrl ) . '\'' : 'NULL';
	}

	public function setWebsiteUrl( $strWebsiteUrl ) {
		$this->set( 'm_strWebsiteUrl', CStrings::strTrimDef( $strWebsiteUrl, 240, NULL, true ) );
	}

	public function getWebsiteUrl() {
		return $this->m_strWebsiteUrl;
	}

	public function sqlWebsiteUrl() {
		return ( true == isset( $this->m_strWebsiteUrl ) ) ? '\'' . addslashes( $this->m_strWebsiteUrl ) . '\'' : 'NULL';
	}

	public function setBiography( $strBiography ) {
		$this->set( 'm_strBiography', CStrings::strTrimDef( $strBiography, 240, NULL, true ) );
	}

	public function getBiography() {
		return $this->m_strBiography;
	}

	public function sqlBiography() {
		return ( true == isset( $this->m_strBiography ) ) ? '\'' . addslashes( $this->m_strBiography ) . '\'' : 'NULL';
	}

	public function setProfilePicUrl( $strProfilePicUrl ) {
		$this->set( 'm_strProfilePicUrl', CStrings::strTrimDef( $strProfilePicUrl, 240, NULL, true ) );
	}

	public function getProfilePicUrl() {
		return $this->m_strProfilePicUrl;
	}

	public function sqlProfilePicUrl() {
		return ( true == isset( $this->m_strProfilePicUrl ) ) ? '\'' . addslashes( $this->m_strProfilePicUrl ) . '\'' : 'NULL';
	}

	public function setPersonTitle( $strPersonTitle ) {
		$this->set( 'm_strPersonTitle', CStrings::strTrimDef( $strPersonTitle, 240, NULL, true ) );
	}

	public function getPersonTitle() {
		return $this->m_strPersonTitle;
	}

	public function sqlPersonTitle() {
		return ( true == isset( $this->m_strPersonTitle ) ) ? '\'' . addslashes( $this->m_strPersonTitle ) . '\'' : 'NULL';
	}

	public function setPersonOrganization( $strPersonOrganization ) {
		$this->set( 'm_strPersonOrganization', CStrings::strTrimDef( $strPersonOrganization, 240, NULL, true ) );
	}

	public function getPersonOrganization() {
		return $this->m_strPersonOrganization;
	}

	public function sqlPersonOrganization() {
		return ( true == isset( $this->m_strPersonOrganization ) ) ? '\'' . addslashes( $this->m_strPersonOrganization ) . '\'' : 'NULL';
	}

	public function setCompanyFoundedOn( $strCompanyFoundedOn ) {
		$this->set( 'm_strCompanyFoundedOn', CStrings::strTrimDef( $strCompanyFoundedOn, -1, NULL, true ) );
	}

	public function getCompanyFoundedOn() {
		return $this->m_strCompanyFoundedOn;
	}

	public function sqlCompanyFoundedOn() {
		return ( true == isset( $this->m_strCompanyFoundedOn ) ) ? '\'' . $this->m_strCompanyFoundedOn . '\'' : 'NULL';
	}

	public function setCompanyTotalEmployees( $intCompanyTotalEmployees ) {
		$this->set( 'm_intCompanyTotalEmployees', CStrings::strToIntDef( $intCompanyTotalEmployees, NULL, false ) );
	}

	public function getCompanyTotalEmployees() {
		return $this->m_intCompanyTotalEmployees;
	}

	public function sqlCompanyTotalEmployees() {
		return ( true == isset( $this->m_intCompanyTotalEmployees ) ) ? ( string ) $this->m_intCompanyTotalEmployees : 'NULL';
	}

	public function setAdvancedDetails( $jsonAdvancedDetails ) {
		if( true == valObj( $jsonAdvancedDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonAdvancedDetails', $jsonAdvancedDetails );
		} elseif( true == valJsonString( $jsonAdvancedDetails ) ) {
			$this->set( 'm_jsonAdvancedDetails', CStrings::strToJson( $jsonAdvancedDetails ) );
		} else {
			$this->set( 'm_jsonAdvancedDetails', NULL ); 
		}
		unset( $this->m_strAdvancedDetails );
	}

	public function getAdvancedDetails() {
		if( true == isset( $this->m_strAdvancedDetails ) ) {
			$this->m_jsonAdvancedDetails = CStrings::strToJson( $this->m_strAdvancedDetails );
			unset( $this->m_strAdvancedDetails );
		}
		return $this->m_jsonAdvancedDetails;
	}

	public function sqlAdvancedDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getAdvancedDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getAdvancedDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setFailedOn( $strFailedOn ) {
		$this->set( 'm_strFailedOn', CStrings::strTrimDef( $strFailedOn, -1, NULL, true ) );
	}

	public function getFailedOn() {
		return $this->m_strFailedOn;
	}

	public function sqlFailedOn() {
		return ( true == isset( $this->m_strFailedOn ) ) ? '\'' . $this->m_strFailedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, contact_search_type_id, reference_id, search_string, name, location, twitter_url, linkedin_url, facebook_url, website_url, biography, profile_pic_url, person_title, person_organization, company_founded_on, company_total_employees, advanced_details, failed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlContactSearchTypeId() . ', ' .
 						$this->sqlReferenceId() . ', ' .
 						$this->sqlSearchString() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlLocation() . ', ' .
 						$this->sqlTwitterUrl() . ', ' .
 						$this->sqlLinkedinUrl() . ', ' .
 						$this->sqlFacebookUrl() . ', ' .
 						$this->sqlWebsiteUrl() . ', ' .
 						$this->sqlBiography() . ', ' .
 						$this->sqlProfilePicUrl() . ', ' .
 						$this->sqlPersonTitle() . ', ' .
 						$this->sqlPersonOrganization() . ', ' .
 						$this->sqlCompanyFoundedOn() . ', ' .
 						$this->sqlCompanyTotalEmployees() . ', ' .
 						$this->sqlAdvancedDetails() . ', ' .
 						$this->sqlFailedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_search_type_id = ' . $this->sqlContactSearchTypeId() . ','; } elseif( true == array_key_exists( 'ContactSearchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' contact_search_type_id = ' . $this->sqlContactSearchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' search_string = ' . $this->sqlSearchString() . ','; } elseif( true == array_key_exists( 'SearchString', $this->getChangedColumns() ) ) { $strSql .= ' search_string = ' . $this->sqlSearchString() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' location = ' . $this->sqlLocation() . ','; } elseif( true == array_key_exists( 'Location', $this->getChangedColumns() ) ) { $strSql .= ' location = ' . $this->sqlLocation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' twitter_url = ' . $this->sqlTwitterUrl() . ','; } elseif( true == array_key_exists( 'TwitterUrl', $this->getChangedColumns() ) ) { $strSql .= ' twitter_url = ' . $this->sqlTwitterUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' linkedin_url = ' . $this->sqlLinkedinUrl() . ','; } elseif( true == array_key_exists( 'LinkedinUrl', $this->getChangedColumns() ) ) { $strSql .= ' linkedin_url = ' . $this->sqlLinkedinUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' facebook_url = ' . $this->sqlFacebookUrl() . ','; } elseif( true == array_key_exists( 'FacebookUrl', $this->getChangedColumns() ) ) { $strSql .= ' facebook_url = ' . $this->sqlFacebookUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl() . ','; } elseif( true == array_key_exists( 'WebsiteUrl', $this->getChangedColumns() ) ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' biography = ' . $this->sqlBiography() . ','; } elseif( true == array_key_exists( 'Biography', $this->getChangedColumns() ) ) { $strSql .= ' biography = ' . $this->sqlBiography() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' profile_pic_url = ' . $this->sqlProfilePicUrl() . ','; } elseif( true == array_key_exists( 'ProfilePicUrl', $this->getChangedColumns() ) ) { $strSql .= ' profile_pic_url = ' . $this->sqlProfilePicUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' person_title = ' . $this->sqlPersonTitle() . ','; } elseif( true == array_key_exists( 'PersonTitle', $this->getChangedColumns() ) ) { $strSql .= ' person_title = ' . $this->sqlPersonTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' person_organization = ' . $this->sqlPersonOrganization() . ','; } elseif( true == array_key_exists( 'PersonOrganization', $this->getChangedColumns() ) ) { $strSql .= ' person_organization = ' . $this->sqlPersonOrganization() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_founded_on = ' . $this->sqlCompanyFoundedOn() . ','; } elseif( true == array_key_exists( 'CompanyFoundedOn', $this->getChangedColumns() ) ) { $strSql .= ' company_founded_on = ' . $this->sqlCompanyFoundedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_total_employees = ' . $this->sqlCompanyTotalEmployees() . ','; } elseif( true == array_key_exists( 'CompanyTotalEmployees', $this->getChangedColumns() ) ) { $strSql .= ' company_total_employees = ' . $this->sqlCompanyTotalEmployees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' advanced_details = ' . $this->sqlAdvancedDetails() . ','; } elseif( true == array_key_exists( 'AdvancedDetails', $this->getChangedColumns() ) ) { $strSql .= ' advanced_details = ' . $this->sqlAdvancedDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; } elseif( true == array_key_exists( 'FailedOn', $this->getChangedColumns() ) ) { $strSql .= ' failed_on = ' . $this->sqlFailedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'contact_search_type_id' => $this->getContactSearchTypeId(),
			'reference_id' => $this->getReferenceId(),
			'search_string' => $this->getSearchString(),
			'name' => $this->getName(),
			'location' => $this->getLocation(),
			'twitter_url' => $this->getTwitterUrl(),
			'linkedin_url' => $this->getLinkedinUrl(),
			'facebook_url' => $this->getFacebookUrl(),
			'website_url' => $this->getWebsiteUrl(),
			'biography' => $this->getBiography(),
			'profile_pic_url' => $this->getProfilePicUrl(),
			'person_title' => $this->getPersonTitle(),
			'person_organization' => $this->getPersonOrganization(),
			'company_founded_on' => $this->getCompanyFoundedOn(),
			'company_total_employees' => $this->getCompanyTotalEmployees(),
			'advanced_details' => $this->getAdvancedDetails(),
			'failed_on' => $this->getFailedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>