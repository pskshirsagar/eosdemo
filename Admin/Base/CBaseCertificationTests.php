<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCertificationTests
 * Do not add any new functions to this class.
 */

class CBaseCertificationTests extends CEosPluralBase {

	/**
	 * @return CCertificationTest[]
	 */
	public static function fetchCertificationTests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCertificationTest', $objDatabase );
	}

	/**
	 * @return CCertificationTest
	 */
	public static function fetchCertificationTest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCertificationTest', $objDatabase );
	}

	public static function fetchCertificationTestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'certification_tests', $objDatabase );
	}

	public static function fetchCertificationTestById( $intId, $objDatabase ) {
		return self::fetchCertificationTest( sprintf( 'SELECT * FROM certification_tests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCertificationTestsByCertificationId( $intCertificationId, $objDatabase ) {
		return self::fetchCertificationTests( sprintf( 'SELECT * FROM certification_tests WHERE certification_id = %d', ( int ) $intCertificationId ), $objDatabase );
	}

	public static function fetchCertificationTestsByTestId( $intTestId, $objDatabase ) {
		return self::fetchCertificationTests( sprintf( 'SELECT * FROM certification_tests WHERE test_id = %d', ( int ) $intTestId ), $objDatabase );
	}

}
?>