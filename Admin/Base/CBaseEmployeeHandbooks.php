<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeHandbooks
 * Do not add any new functions to this class.
 */

class CBaseEmployeeHandbooks extends CEosPluralBase {

	/**
	 * @return CEmployeeHandbook[]
	 */
	public static function fetchEmployeeHandbooks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeHandbook', $objDatabase );
	}

	/**
	 * @return CEmployeeHandbook
	 */
	public static function fetchEmployeeHandbook( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeHandbook', $objDatabase );
	}

	public static function fetchEmployeeHandbookCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_handbooks', $objDatabase );
	}

	public static function fetchEmployeeHandbookById( $intId, $objDatabase ) {
		return self::fetchEmployeeHandbook( sprintf( 'SELECT * FROM employee_handbooks WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeHandbooksByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeHandbooks( sprintf( 'SELECT * FROM employee_handbooks WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeHandbooksByHandbookId( $intHandbookId, $objDatabase ) {
		return self::fetchEmployeeHandbooks( sprintf( 'SELECT * FROM employee_handbooks WHERE handbook_id = %d', ( int ) $intHandbookId ), $objDatabase );
	}

}
?>