<?php

class CBaseReportChangeHistory extends CEosSingularBase {

	const TABLE_NAME = 'public.report_change_histories';

	protected $m_intId;
	protected $m_intCompanyReportId;
	protected $m_strReportChangeDetails;
	protected $m_strLastPostedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['company_report_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyReportId', trim( $arrValues['company_report_id'] ) ); elseif( isset( $arrValues['company_report_id'] ) ) $this->setCompanyReportId( $arrValues['company_report_id'] );
		if( isset( $arrValues['report_change_details'] ) && $boolDirectSet ) $this->set( 'm_strReportChangeDetails', trim( stripcslashes( $arrValues['report_change_details'] ) ) ); elseif( isset( $arrValues['report_change_details'] ) ) $this->setReportChangeDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['report_change_details'] ) : $arrValues['report_change_details'] );
		if( isset( $arrValues['last_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedOn', trim( $arrValues['last_posted_on'] ) ); elseif( isset( $arrValues['last_posted_on'] ) ) $this->setLastPostedOn( $arrValues['last_posted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCompanyReportId( $intCompanyReportId ) {
		$this->set( 'm_intCompanyReportId', CStrings::strToIntDef( $intCompanyReportId, NULL, false ) );
	}

	public function getCompanyReportId() {
		return $this->m_intCompanyReportId;
	}

	public function sqlCompanyReportId() {
		return ( true == isset( $this->m_intCompanyReportId ) ) ? ( string ) $this->m_intCompanyReportId : 'NULL';
	}

	public function setReportChangeDetails( $strReportChangeDetails ) {
		$this->set( 'm_strReportChangeDetails', CStrings::strTrimDef( $strReportChangeDetails, -1, NULL, true ) );
	}

	public function getReportChangeDetails() {
		return $this->m_strReportChangeDetails;
	}

	public function sqlReportChangeDetails() {
		return ( true == isset( $this->m_strReportChangeDetails ) ) ? '\'' . addslashes( $this->m_strReportChangeDetails ) . '\'' : 'NULL';
	}

	public function setLastPostedOn( $strLastPostedOn ) {
		$this->set( 'm_strLastPostedOn', CStrings::strTrimDef( $strLastPostedOn, -1, NULL, true ) );
	}

	public function getLastPostedOn() {
		return $this->m_strLastPostedOn;
	}

	public function sqlLastPostedOn() {
		return ( true == isset( $this->m_strLastPostedOn ) ) ? '\'' . $this->m_strLastPostedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, company_report_id, report_change_details, last_posted_on, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCompanyReportId() . ', ' .
 						$this->sqlReportChangeDetails() . ', ' .
 						$this->sqlLastPostedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_report_id = ' . $this->sqlCompanyReportId() . ','; } elseif( true == array_key_exists( 'CompanyReportId', $this->getChangedColumns() ) ) { $strSql .= ' company_report_id = ' . $this->sqlCompanyReportId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_change_details = ' . $this->sqlReportChangeDetails() . ','; } elseif( true == array_key_exists( 'ReportChangeDetails', $this->getChangedColumns() ) ) { $strSql .= ' report_change_details = ' . $this->sqlReportChangeDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; } elseif( true == array_key_exists( 'LastPostedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'company_report_id' => $this->getCompanyReportId(),
			'report_change_details' => $this->getReportChangeDetails(),
			'last_posted_on' => $this->getLastPostedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>