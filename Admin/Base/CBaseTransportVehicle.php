<?php

class CBaseTransportVehicle extends CEosSingularBase {

	const TABLE_NAME = 'public.transport_vehicles';

	protected $m_intId;
	protected $m_strVehicleNumber;
	protected $m_strDriverName;
	protected $m_strDriverContactNumber;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intNumberOfSeats;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['vehicle_number'] ) && $boolDirectSet ) $this->set( 'm_strVehicleNumber', trim( stripcslashes( $arrValues['vehicle_number'] ) ) ); elseif( isset( $arrValues['vehicle_number'] ) ) $this->setVehicleNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vehicle_number'] ) : $arrValues['vehicle_number'] );
		if( isset( $arrValues['driver_name'] ) && $boolDirectSet ) $this->set( 'm_strDriverName', trim( stripcslashes( $arrValues['driver_name'] ) ) ); elseif( isset( $arrValues['driver_name'] ) ) $this->setDriverName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['driver_name'] ) : $arrValues['driver_name'] );
		if( isset( $arrValues['driver_contact_number'] ) && $boolDirectSet ) $this->set( 'm_strDriverContactNumber', trim( stripcslashes( $arrValues['driver_contact_number'] ) ) ); elseif( isset( $arrValues['driver_contact_number'] ) ) $this->setDriverContactNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['driver_contact_number'] ) : $arrValues['driver_contact_number'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['number_of_seats'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfSeats', trim( $arrValues['number_of_seats'] ) ); elseif( isset( $arrValues['number_of_seats'] ) ) $this->setNumberOfSeats( $arrValues['number_of_seats'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setVehicleNumber( $strVehicleNumber ) {
		$this->set( 'm_strVehicleNumber', CStrings::strTrimDef( $strVehicleNumber, 15, NULL, true ) );
	}

	public function getVehicleNumber() {
		return $this->m_strVehicleNumber;
	}

	public function sqlVehicleNumber() {
		return ( true == isset( $this->m_strVehicleNumber ) ) ? '\'' . addslashes( $this->m_strVehicleNumber ) . '\'' : 'NULL';
	}

	public function setDriverName( $strDriverName ) {
		$this->set( 'm_strDriverName', CStrings::strTrimDef( $strDriverName, 50, NULL, true ) );
	}

	public function getDriverName() {
		return $this->m_strDriverName;
	}

	public function sqlDriverName() {
		return ( true == isset( $this->m_strDriverName ) ) ? '\'' . addslashes( $this->m_strDriverName ) . '\'' : 'NULL';
	}

	public function setDriverContactNumber( $strDriverContactNumber ) {
		$this->set( 'm_strDriverContactNumber', CStrings::strTrimDef( $strDriverContactNumber, 30, NULL, true ) );
	}

	public function getDriverContactNumber() {
		return $this->m_strDriverContactNumber;
	}

	public function sqlDriverContactNumber() {
		return ( true == isset( $this->m_strDriverContactNumber ) ) ? '\'' . addslashes( $this->m_strDriverContactNumber ) . '\'' : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setNumberOfSeats( $intNumberOfSeats ) {
		$this->set( 'm_intNumberOfSeats', CStrings::strToIntDef( $intNumberOfSeats, NULL, false ) );
	}

	public function getNumberOfSeats() {
		return $this->m_intNumberOfSeats;
	}

	public function sqlNumberOfSeats() {
		return ( true == isset( $this->m_intNumberOfSeats ) ) ? ( string ) $this->m_intNumberOfSeats : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, vehicle_number, driver_name, driver_contact_number, is_published, updated_by, updated_on, created_by, created_on, number_of_seats )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlVehicleNumber() . ', ' .
 						$this->sqlDriverName() . ', ' .
 						$this->sqlDriverContactNumber() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlNumberOfSeats() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vehicle_number = ' . $this->sqlVehicleNumber() . ','; } elseif( true == array_key_exists( 'VehicleNumber', $this->getChangedColumns() ) ) { $strSql .= ' vehicle_number = ' . $this->sqlVehicleNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' driver_name = ' . $this->sqlDriverName() . ','; } elseif( true == array_key_exists( 'DriverName', $this->getChangedColumns() ) ) { $strSql .= ' driver_name = ' . $this->sqlDriverName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' driver_contact_number = ' . $this->sqlDriverContactNumber() . ','; } elseif( true == array_key_exists( 'DriverContactNumber', $this->getChangedColumns() ) ) { $strSql .= ' driver_contact_number = ' . $this->sqlDriverContactNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_seats = ' . $this->sqlNumberOfSeats() . ','; } elseif( true == array_key_exists( 'NumberOfSeats', $this->getChangedColumns() ) ) { $strSql .= ' number_of_seats = ' . $this->sqlNumberOfSeats() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'vehicle_number' => $this->getVehicleNumber(),
			'driver_name' => $this->getDriverName(),
			'driver_contact_number' => $this->getDriverContactNumber(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'number_of_seats' => $this->getNumberOfSeats()
		);
	}

}
?>