<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyKeyValues
 * Do not add any new functions to this class.
 */

class CBaseCompanyKeyValues extends CEosPluralBase {

	/**
	 * @return CCompanyKeyValue[]
	 */
	public static function fetchCompanyKeyValues( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCompanyKeyValue', $objDatabase );
	}

	/**
	 * @return CCompanyKeyValue
	 */
	public static function fetchCompanyKeyValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyKeyValue', $objDatabase );
	}

	public static function fetchCompanyKeyValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_key_values', $objDatabase );
	}

	public static function fetchCompanyKeyValueById( $intId, $objDatabase ) {
		return self::fetchCompanyKeyValue( sprintf( 'SELECT * FROM company_key_values WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCompanyKeyValuesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyKeyValues( sprintf( 'SELECT * FROM company_key_values WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>