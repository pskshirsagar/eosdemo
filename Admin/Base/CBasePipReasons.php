<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPipReasons
 * Do not add any new functions to this class.
 */

class CBasePipReasons extends CEosPluralBase {

	/**
	 * @return CPipReason[]
	 */
	public static function fetchPipReasons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPipReason', $objDatabase );
	}

	/**
	 * @return CPipReason
	 */
	public static function fetchPipReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPipReason', $objDatabase );
	}

	public static function fetchPipReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'pip_reasons', $objDatabase );
	}

	public static function fetchPipReasonById( $intId, $objDatabase ) {
		return self::fetchPipReason( sprintf( 'SELECT * FROM pip_reasons WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPipReasonsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchPipReasons( sprintf( 'SELECT * FROM pip_reasons WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>