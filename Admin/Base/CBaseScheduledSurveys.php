<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CScheduledSurveys
 * Do not add any new functions to this class.
 */

class CBaseScheduledSurveys extends CEosPluralBase {

	/**
	 * @return CScheduledSurvey[]
	 */
	public static function fetchScheduledSurveys( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScheduledSurvey', $objDatabase );
	}

	/**
	 * @return CScheduledSurvey
	 */
	public static function fetchScheduledSurvey( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScheduledSurvey', $objDatabase );
	}

	public static function fetchScheduledSurveyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'scheduled_surveys', $objDatabase );
	}

	public static function fetchScheduledSurveyById( $intId, $objDatabase ) {
		return self::fetchScheduledSurvey( sprintf( 'SELECT * FROM scheduled_surveys WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScheduledSurveysBySurveyTypeId( $intSurveyTypeId, $objDatabase ) {
		return self::fetchScheduledSurveys( sprintf( 'SELECT * FROM scheduled_surveys WHERE survey_type_id = %d', ( int ) $intSurveyTypeId ), $objDatabase );
	}

	public static function fetchScheduledSurveysBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		return self::fetchScheduledSurveys( sprintf( 'SELECT * FROM scheduled_surveys WHERE survey_template_id = %d', ( int ) $intSurveyTemplateId ), $objDatabase );
	}

	public static function fetchScheduledSurveysByFrequencyId( $intFrequencyId, $objDatabase ) {
		return self::fetchScheduledSurveys( sprintf( 'SELECT * FROM scheduled_surveys WHERE frequency_id = %d', ( int ) $intFrequencyId ), $objDatabase );
	}

	public static function fetchScheduledSurveysByResponderReferenceId( $intResponderReferenceId, $objDatabase ) {
		return self::fetchScheduledSurveys( sprintf( 'SELECT * FROM scheduled_surveys WHERE responder_reference_id = %d', ( int ) $intResponderReferenceId ), $objDatabase );
	}

	public static function fetchScheduledSurveysBySubjectReferenceId( $intSubjectReferenceId, $objDatabase ) {
		return self::fetchScheduledSurveys( sprintf( 'SELECT * FROM scheduled_surveys WHERE subject_reference_id = %d', ( int ) $intSubjectReferenceId ), $objDatabase );
	}

}
?>