<?php

class CBaseStatsEmployeePerformance extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_employee_performances';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_strIntimatedDate;
	protected $m_intPoints;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['intimated_date'] ) && $boolDirectSet ) $this->set( 'm_strIntimatedDate', trim( $arrValues['intimated_date'] ) ); elseif( isset( $arrValues['intimated_date'] ) ) $this->setIntimatedDate( $arrValues['intimated_date'] );
		if( isset( $arrValues['points'] ) && $boolDirectSet ) $this->set( 'm_intPoints', trim( $arrValues['points'] ) ); elseif( isset( $arrValues['points'] ) ) $this->setPoints( $arrValues['points'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setIntimatedDate( $strIntimatedDate ) {
		$this->set( 'm_strIntimatedDate', CStrings::strTrimDef( $strIntimatedDate, -1, NULL, true ) );
	}

	public function getIntimatedDate() {
		return $this->m_strIntimatedDate;
	}

	public function sqlIntimatedDate() {
		return ( true == isset( $this->m_strIntimatedDate ) ) ? '\'' . $this->m_strIntimatedDate . '\'' : 'NOW()';
	}

	public function setPoints( $intPoints ) {
		$this->set( 'm_intPoints', CStrings::strToIntDef( $intPoints, NULL, false ) );
	}

	public function getPoints() {
		return $this->m_intPoints;
	}

	public function sqlPoints() {
		return ( true == isset( $this->m_intPoints ) ) ? ( string ) $this->m_intPoints : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'intimated_date' => $this->getIntimatedDate(),
			'points' => $this->getPoints()
		);
	}

}
?>