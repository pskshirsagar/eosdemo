<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskEstimations
 * Do not add any new functions to this class.
 */

class CBaseTaskEstimations extends CEosPluralBase {

	/**
	 * @return CTaskEstimation[]
	 */
	public static function fetchTaskEstimations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskEstimation', $objDatabase );
	}

	/**
	 * @return CTaskEstimation
	 */
	public static function fetchTaskEstimation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskEstimation', $objDatabase );
	}

	public static function fetchTaskEstimationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_estimations', $objDatabase );
	}

	public static function fetchTaskEstimationById( $intId, $objDatabase ) {
		return self::fetchTaskEstimation( sprintf( 'SELECT * FROM task_estimations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskEstimationsByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchTaskEstimations( sprintf( 'SELECT * FROM task_estimations WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

}
?>