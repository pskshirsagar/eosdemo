<?php

class CBaseTestQuestionTemplateAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.test_question_template_associations';

	protected $m_intId;
	protected $m_intTestId;
	protected $m_intTestTemplateId;
	protected $m_intNoOfQuestions;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['test_id'] ) && $boolDirectSet ) $this->set( 'm_intTestId', trim( $arrValues['test_id'] ) ); elseif( isset( $arrValues['test_id'] ) ) $this->setTestId( $arrValues['test_id'] );
		if( isset( $arrValues['test_template_id'] ) && $boolDirectSet ) $this->set( 'm_intTestTemplateId', trim( $arrValues['test_template_id'] ) ); elseif( isset( $arrValues['test_template_id'] ) ) $this->setTestTemplateId( $arrValues['test_template_id'] );
		if( isset( $arrValues['no_of_questions'] ) && $boolDirectSet ) $this->set( 'm_intNoOfQuestions', trim( $arrValues['no_of_questions'] ) ); elseif( isset( $arrValues['no_of_questions'] ) ) $this->setNoOfQuestions( $arrValues['no_of_questions'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTestId( $intTestId ) {
		$this->set( 'm_intTestId', CStrings::strToIntDef( $intTestId, NULL, false ) );
	}

	public function getTestId() {
		return $this->m_intTestId;
	}

	public function sqlTestId() {
		return ( true == isset( $this->m_intTestId ) ) ? ( string ) $this->m_intTestId : 'NULL';
	}

	public function setTestTemplateId( $intTestTemplateId ) {
		$this->set( 'm_intTestTemplateId', CStrings::strToIntDef( $intTestTemplateId, NULL, false ) );
	}

	public function getTestTemplateId() {
		return $this->m_intTestTemplateId;
	}

	public function sqlTestTemplateId() {
		return ( true == isset( $this->m_intTestTemplateId ) ) ? ( string ) $this->m_intTestTemplateId : 'NULL';
	}

	public function setNoOfQuestions( $intNoOfQuestions ) {
		$this->set( 'm_intNoOfQuestions', CStrings::strToIntDef( $intNoOfQuestions, NULL, false ) );
	}

	public function getNoOfQuestions() {
		return $this->m_intNoOfQuestions;
	}

	public function sqlNoOfQuestions() {
		return ( true == isset( $this->m_intNoOfQuestions ) ) ? ( string ) $this->m_intNoOfQuestions : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, test_id, test_template_id, no_of_questions, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTestId() . ', ' .
 						$this->sqlTestTemplateId() . ', ' .
 						$this->sqlNoOfQuestions() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_id = ' . $this->sqlTestId() . ','; } elseif( true == array_key_exists( 'TestId', $this->getChangedColumns() ) ) { $strSql .= ' test_id = ' . $this->sqlTestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_template_id = ' . $this->sqlTestTemplateId() . ','; } elseif( true == array_key_exists( 'TestTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' test_template_id = ' . $this->sqlTestTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' no_of_questions = ' . $this->sqlNoOfQuestions() . ','; } elseif( true == array_key_exists( 'NoOfQuestions', $this->getChangedColumns() ) ) { $strSql .= ' no_of_questions = ' . $this->sqlNoOfQuestions() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'test_id' => $this->getTestId(),
			'test_template_id' => $this->getTestTemplateId(),
			'no_of_questions' => $this->getNoOfQuestions(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>