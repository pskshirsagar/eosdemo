<?php

class CBaseStatsPilot extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_pilots';

	protected $m_intId;
	protected $m_strMonth;
	protected $m_intContractId;
	protected $m_intPsLeadId;
	protected $m_intCid;
	protected $m_intPsProductId;
	protected $m_intPropertyId;
	protected $m_strCompanyName;
	protected $m_strProductName;
	protected $m_strPropertyName;
	protected $m_strPropertyType;
	protected $m_intPropertyUnitCount;
	protected $m_fltUnitTransactionalAverage;
	protected $m_strContractStartDate;
	protected $m_strCloseDate;
	protected $m_strContractEnteredPipelineOn;
	protected $m_strContractWonOn;
	protected $m_strContractLostOn;
	protected $m_strContractTerminatedOn;
	protected $m_strPilotStartDate;
	protected $m_strPilotLastActiveDate;
	protected $m_strTerminationDate;
	protected $m_intSubscriptionAcv;
	protected $m_intTransactionAcv;
	protected $m_intSubscriptionAcvUsd;
	protected $m_intTransactionAcvUsd;
	protected $m_intIsWon;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrValues['company_name'] ) ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_name'] ) : $arrValues['company_name'] );
		if( isset( $arrValues['product_name'] ) && $boolDirectSet ) $this->set( 'm_strProductName', trim( stripcslashes( $arrValues['product_name'] ) ) ); elseif( isset( $arrValues['product_name'] ) ) $this->setProductName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['product_name'] ) : $arrValues['product_name'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['property_type'] ) && $boolDirectSet ) $this->set( 'm_strPropertyType', trim( stripcslashes( $arrValues['property_type'] ) ) ); elseif( isset( $arrValues['property_type'] ) ) $this->setPropertyType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_type'] ) : $arrValues['property_type'] );
		if( isset( $arrValues['property_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitCount', trim( $arrValues['property_unit_count'] ) ); elseif( isset( $arrValues['property_unit_count'] ) ) $this->setPropertyUnitCount( $arrValues['property_unit_count'] );
		if( isset( $arrValues['unit_transactional_average'] ) && $boolDirectSet ) $this->set( 'm_fltUnitTransactionalAverage', trim( $arrValues['unit_transactional_average'] ) ); elseif( isset( $arrValues['unit_transactional_average'] ) ) $this->setUnitTransactionalAverage( $arrValues['unit_transactional_average'] );
		if( isset( $arrValues['contract_start_date'] ) && $boolDirectSet ) $this->set( 'm_strContractStartDate', trim( $arrValues['contract_start_date'] ) ); elseif( isset( $arrValues['contract_start_date'] ) ) $this->setContractStartDate( $arrValues['contract_start_date'] );
		if( isset( $arrValues['close_date'] ) && $boolDirectSet ) $this->set( 'm_strCloseDate', trim( $arrValues['close_date'] ) ); elseif( isset( $arrValues['close_date'] ) ) $this->setCloseDate( $arrValues['close_date'] );
		if( isset( $arrValues['contract_entered_pipeline_on'] ) && $boolDirectSet ) $this->set( 'm_strContractEnteredPipelineOn', trim( $arrValues['contract_entered_pipeline_on'] ) ); elseif( isset( $arrValues['contract_entered_pipeline_on'] ) ) $this->setContractEnteredPipelineOn( $arrValues['contract_entered_pipeline_on'] );
		if( isset( $arrValues['contract_won_on'] ) && $boolDirectSet ) $this->set( 'm_strContractWonOn', trim( $arrValues['contract_won_on'] ) ); elseif( isset( $arrValues['contract_won_on'] ) ) $this->setContractWonOn( $arrValues['contract_won_on'] );
		if( isset( $arrValues['contract_lost_on'] ) && $boolDirectSet ) $this->set( 'm_strContractLostOn', trim( $arrValues['contract_lost_on'] ) ); elseif( isset( $arrValues['contract_lost_on'] ) ) $this->setContractLostOn( $arrValues['contract_lost_on'] );
		if( isset( $arrValues['contract_terminated_on'] ) && $boolDirectSet ) $this->set( 'm_strContractTerminatedOn', trim( $arrValues['contract_terminated_on'] ) ); elseif( isset( $arrValues['contract_terminated_on'] ) ) $this->setContractTerminatedOn( $arrValues['contract_terminated_on'] );
		if( isset( $arrValues['pilot_start_date'] ) && $boolDirectSet ) $this->set( 'm_strPilotStartDate', trim( $arrValues['pilot_start_date'] ) ); elseif( isset( $arrValues['pilot_start_date'] ) ) $this->setPilotStartDate( $arrValues['pilot_start_date'] );
		if( isset( $arrValues['pilot_last_active_date'] ) && $boolDirectSet ) $this->set( 'm_strPilotLastActiveDate', trim( $arrValues['pilot_last_active_date'] ) ); elseif( isset( $arrValues['pilot_last_active_date'] ) ) $this->setPilotLastActiveDate( $arrValues['pilot_last_active_date'] );
		if( isset( $arrValues['termination_date'] ) && $boolDirectSet ) $this->set( 'm_strTerminationDate', trim( $arrValues['termination_date'] ) ); elseif( isset( $arrValues['termination_date'] ) ) $this->setTerminationDate( $arrValues['termination_date'] );
		if( isset( $arrValues['subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intSubscriptionAcv', trim( $arrValues['subscription_acv'] ) ); elseif( isset( $arrValues['subscription_acv'] ) ) $this->setSubscriptionAcv( $arrValues['subscription_acv'] );
		if( isset( $arrValues['transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intTransactionAcv', trim( $arrValues['transaction_acv'] ) ); elseif( isset( $arrValues['transaction_acv'] ) ) $this->setTransactionAcv( $arrValues['transaction_acv'] );
		if( isset( $arrValues['subscription_acv_usd'] ) && $boolDirectSet ) $this->set( 'm_intSubscriptionAcvUsd', trim( $arrValues['subscription_acv_usd'] ) ); elseif( isset( $arrValues['subscription_acv_usd'] ) ) $this->setSubscriptionAcvUsd( $arrValues['subscription_acv_usd'] );
		if( isset( $arrValues['transaction_acv_usd'] ) && $boolDirectSet ) $this->set( 'm_intTransactionAcvUsd', trim( $arrValues['transaction_acv_usd'] ) ); elseif( isset( $arrValues['transaction_acv_usd'] ) ) $this->setTransactionAcvUsd( $arrValues['transaction_acv_usd'] );
		if( isset( $arrValues['is_won'] ) && $boolDirectSet ) $this->set( 'm_intIsWon', trim( $arrValues['is_won'] ) ); elseif( isset( $arrValues['is_won'] ) ) $this->setIsWon( $arrValues['is_won'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

	public function setProductName( $strProductName ) {
		$this->set( 'm_strProductName', CStrings::strTrimDef( $strProductName, 50, NULL, true ) );
	}

	public function getProductName() {
		return $this->m_strProductName;
	}

	public function sqlProductName() {
		return ( true == isset( $this->m_strProductName ) ) ? '\'' . addslashes( $this->m_strProductName ) . '\'' : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setPropertyType( $strPropertyType ) {
		$this->set( 'm_strPropertyType', CStrings::strTrimDef( $strPropertyType, 50, NULL, true ) );
	}

	public function getPropertyType() {
		return $this->m_strPropertyType;
	}

	public function sqlPropertyType() {
		return ( true == isset( $this->m_strPropertyType ) ) ? '\'' . addslashes( $this->m_strPropertyType ) . '\'' : 'NULL';
	}

	public function setPropertyUnitCount( $intPropertyUnitCount ) {
		$this->set( 'm_intPropertyUnitCount', CStrings::strToIntDef( $intPropertyUnitCount, NULL, false ) );
	}

	public function getPropertyUnitCount() {
		return $this->m_intPropertyUnitCount;
	}

	public function sqlPropertyUnitCount() {
		return ( true == isset( $this->m_intPropertyUnitCount ) ) ? ( string ) $this->m_intPropertyUnitCount : 'NULL';
	}

	public function setUnitTransactionalAverage( $fltUnitTransactionalAverage ) {
		$this->set( 'm_fltUnitTransactionalAverage', CStrings::strToFloatDef( $fltUnitTransactionalAverage, NULL, false, 0 ) );
	}

	public function getUnitTransactionalAverage() {
		return $this->m_fltUnitTransactionalAverage;
	}

	public function sqlUnitTransactionalAverage() {
		return ( true == isset( $this->m_fltUnitTransactionalAverage ) ) ? ( string ) $this->m_fltUnitTransactionalAverage : 'NULL';
	}

	public function setContractStartDate( $strContractStartDate ) {
		$this->set( 'm_strContractStartDate', CStrings::strTrimDef( $strContractStartDate, -1, NULL, true ) );
	}

	public function getContractStartDate() {
		return $this->m_strContractStartDate;
	}

	public function sqlContractStartDate() {
		return ( true == isset( $this->m_strContractStartDate ) ) ? '\'' . $this->m_strContractStartDate . '\'' : 'NULL';
	}

	public function setCloseDate( $strCloseDate ) {
		$this->set( 'm_strCloseDate', CStrings::strTrimDef( $strCloseDate, -1, NULL, true ) );
	}

	public function getCloseDate() {
		return $this->m_strCloseDate;
	}

	public function sqlCloseDate() {
		return ( true == isset( $this->m_strCloseDate ) ) ? '\'' . $this->m_strCloseDate . '\'' : 'NULL';
	}

	public function setContractEnteredPipelineOn( $strContractEnteredPipelineOn ) {
		$this->set( 'm_strContractEnteredPipelineOn', CStrings::strTrimDef( $strContractEnteredPipelineOn, -1, NULL, true ) );
	}

	public function getContractEnteredPipelineOn() {
		return $this->m_strContractEnteredPipelineOn;
	}

	public function sqlContractEnteredPipelineOn() {
		return ( true == isset( $this->m_strContractEnteredPipelineOn ) ) ? '\'' . $this->m_strContractEnteredPipelineOn . '\'' : 'NULL';
	}

	public function setContractWonOn( $strContractWonOn ) {
		$this->set( 'm_strContractWonOn', CStrings::strTrimDef( $strContractWonOn, -1, NULL, true ) );
	}

	public function getContractWonOn() {
		return $this->m_strContractWonOn;
	}

	public function sqlContractWonOn() {
		return ( true == isset( $this->m_strContractWonOn ) ) ? '\'' . $this->m_strContractWonOn . '\'' : 'NULL';
	}

	public function setContractLostOn( $strContractLostOn ) {
		$this->set( 'm_strContractLostOn', CStrings::strTrimDef( $strContractLostOn, -1, NULL, true ) );
	}

	public function getContractLostOn() {
		return $this->m_strContractLostOn;
	}

	public function sqlContractLostOn() {
		return ( true == isset( $this->m_strContractLostOn ) ) ? '\'' . $this->m_strContractLostOn . '\'' : 'NULL';
	}

	public function setContractTerminatedOn( $strContractTerminatedOn ) {
		$this->set( 'm_strContractTerminatedOn', CStrings::strTrimDef( $strContractTerminatedOn, -1, NULL, true ) );
	}

	public function getContractTerminatedOn() {
		return $this->m_strContractTerminatedOn;
	}

	public function sqlContractTerminatedOn() {
		return ( true == isset( $this->m_strContractTerminatedOn ) ) ? '\'' . $this->m_strContractTerminatedOn . '\'' : 'NULL';
	}

	public function setPilotStartDate( $strPilotStartDate ) {
		$this->set( 'm_strPilotStartDate', CStrings::strTrimDef( $strPilotStartDate, -1, NULL, true ) );
	}

	public function getPilotStartDate() {
		return $this->m_strPilotStartDate;
	}

	public function sqlPilotStartDate() {
		return ( true == isset( $this->m_strPilotStartDate ) ) ? '\'' . $this->m_strPilotStartDate . '\'' : 'NULL';
	}

	public function setPilotLastActiveDate( $strPilotLastActiveDate ) {
		$this->set( 'm_strPilotLastActiveDate', CStrings::strTrimDef( $strPilotLastActiveDate, -1, NULL, true ) );
	}

	public function getPilotLastActiveDate() {
		return $this->m_strPilotLastActiveDate;
	}

	public function sqlPilotLastActiveDate() {
		return ( true == isset( $this->m_strPilotLastActiveDate ) ) ? '\'' . $this->m_strPilotLastActiveDate . '\'' : 'NULL';
	}

	public function setTerminationDate( $strTerminationDate ) {
		$this->set( 'm_strTerminationDate', CStrings::strTrimDef( $strTerminationDate, -1, NULL, true ) );
	}

	public function getTerminationDate() {
		return $this->m_strTerminationDate;
	}

	public function sqlTerminationDate() {
		return ( true == isset( $this->m_strTerminationDate ) ) ? '\'' . $this->m_strTerminationDate . '\'' : 'NULL';
	}

	public function setSubscriptionAcv( $intSubscriptionAcv ) {
		$this->set( 'm_intSubscriptionAcv', CStrings::strToIntDef( $intSubscriptionAcv, NULL, false ) );
	}

	public function getSubscriptionAcv() {
		return $this->m_intSubscriptionAcv;
	}

	public function sqlSubscriptionAcv() {
		return ( true == isset( $this->m_intSubscriptionAcv ) ) ? ( string ) $this->m_intSubscriptionAcv : 'NULL';
	}

	public function setTransactionAcv( $intTransactionAcv ) {
		$this->set( 'm_intTransactionAcv', CStrings::strToIntDef( $intTransactionAcv, NULL, false ) );
	}

	public function getTransactionAcv() {
		return $this->m_intTransactionAcv;
	}

	public function sqlTransactionAcv() {
		return ( true == isset( $this->m_intTransactionAcv ) ) ? ( string ) $this->m_intTransactionAcv : 'NULL';
	}

	public function setSubscriptionAcvUsd( $intSubscriptionAcvUsd ) {
		$this->set( 'm_intSubscriptionAcvUsd', CStrings::strToIntDef( $intSubscriptionAcvUsd, NULL, false ) );
	}

	public function getSubscriptionAcvUsd() {
		return $this->m_intSubscriptionAcvUsd;
	}

	public function sqlSubscriptionAcvUsd() {
		return ( true == isset( $this->m_intSubscriptionAcvUsd ) ) ? ( string ) $this->m_intSubscriptionAcvUsd : 'NULL';
	}

	public function setTransactionAcvUsd( $intTransactionAcvUsd ) {
		$this->set( 'm_intTransactionAcvUsd', CStrings::strToIntDef( $intTransactionAcvUsd, NULL, false ) );
	}

	public function getTransactionAcvUsd() {
		return $this->m_intTransactionAcvUsd;
	}

	public function sqlTransactionAcvUsd() {
		return ( true == isset( $this->m_intTransactionAcvUsd ) ) ? ( string ) $this->m_intTransactionAcvUsd : 'NULL';
	}

	public function setIsWon( $intIsWon ) {
		$this->set( 'm_intIsWon', CStrings::strToIntDef( $intIsWon, NULL, false ) );
	}

	public function getIsWon() {
		return $this->m_intIsWon;
	}

	public function sqlIsWon() {
		return ( true == isset( $this->m_intIsWon ) ) ? ( string ) $this->m_intIsWon : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, month, contract_id, ps_lead_id, cid, ps_product_id, property_id, company_name, product_name, property_name, property_type, property_unit_count, unit_transactional_average, contract_start_date, close_date, contract_entered_pipeline_on, contract_won_on, contract_lost_on, contract_terminated_on, pilot_start_date, pilot_last_active_date, termination_date, subscription_acv, transaction_acv, subscription_acv_usd, transaction_acv_usd, is_won, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlMonth() . ', ' .
 						$this->sqlContractId() . ', ' .
 						$this->sqlPsLeadId() . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlCompanyName() . ', ' .
 						$this->sqlProductName() . ', ' .
 						$this->sqlPropertyName() . ', ' .
 						$this->sqlPropertyType() . ', ' .
 						$this->sqlPropertyUnitCount() . ', ' .
 						$this->sqlUnitTransactionalAverage() . ', ' .
 						$this->sqlContractStartDate() . ', ' .
 						$this->sqlCloseDate() . ', ' .
 						$this->sqlContractEnteredPipelineOn() . ', ' .
 						$this->sqlContractWonOn() . ', ' .
 						$this->sqlContractLostOn() . ', ' .
 						$this->sqlContractTerminatedOn() . ', ' .
 						$this->sqlPilotStartDate() . ', ' .
 						$this->sqlPilotLastActiveDate() . ', ' .
 						$this->sqlTerminationDate() . ', ' .
 						$this->sqlSubscriptionAcv() . ', ' .
 						$this->sqlTransactionAcv() . ', ' .
 						$this->sqlSubscriptionAcvUsd() . ', ' .
 						$this->sqlTransactionAcvUsd() . ', ' .
 						$this->sqlIsWon() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_name = ' . $this->sqlProductName() . ','; } elseif( true == array_key_exists( 'ProductName', $this->getChangedColumns() ) ) { $strSql .= ' product_name = ' . $this->sqlProductName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_type = ' . $this->sqlPropertyType() . ','; } elseif( true == array_key_exists( 'PropertyType', $this->getChangedColumns() ) ) { $strSql .= ' property_type = ' . $this->sqlPropertyType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_count = ' . $this->sqlPropertyUnitCount() . ','; } elseif( true == array_key_exists( 'PropertyUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_count = ' . $this->sqlPropertyUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_transactional_average = ' . $this->sqlUnitTransactionalAverage() . ','; } elseif( true == array_key_exists( 'UnitTransactionalAverage', $this->getChangedColumns() ) ) { $strSql .= ' unit_transactional_average = ' . $this->sqlUnitTransactionalAverage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_start_date = ' . $this->sqlContractStartDate() . ','; } elseif( true == array_key_exists( 'ContractStartDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_start_date = ' . $this->sqlContractStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' close_date = ' . $this->sqlCloseDate() . ','; } elseif( true == array_key_exists( 'CloseDate', $this->getChangedColumns() ) ) { $strSql .= ' close_date = ' . $this->sqlCloseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_entered_pipeline_on = ' . $this->sqlContractEnteredPipelineOn() . ','; } elseif( true == array_key_exists( 'ContractEnteredPipelineOn', $this->getChangedColumns() ) ) { $strSql .= ' contract_entered_pipeline_on = ' . $this->sqlContractEnteredPipelineOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_won_on = ' . $this->sqlContractWonOn() . ','; } elseif( true == array_key_exists( 'ContractWonOn', $this->getChangedColumns() ) ) { $strSql .= ' contract_won_on = ' . $this->sqlContractWonOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_lost_on = ' . $this->sqlContractLostOn() . ','; } elseif( true == array_key_exists( 'ContractLostOn', $this->getChangedColumns() ) ) { $strSql .= ' contract_lost_on = ' . $this->sqlContractLostOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_terminated_on = ' . $this->sqlContractTerminatedOn() . ','; } elseif( true == array_key_exists( 'ContractTerminatedOn', $this->getChangedColumns() ) ) { $strSql .= ' contract_terminated_on = ' . $this->sqlContractTerminatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_start_date = ' . $this->sqlPilotStartDate() . ','; } elseif( true == array_key_exists( 'PilotStartDate', $this->getChangedColumns() ) ) { $strSql .= ' pilot_start_date = ' . $this->sqlPilotStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pilot_last_active_date = ' . $this->sqlPilotLastActiveDate() . ','; } elseif( true == array_key_exists( 'PilotLastActiveDate', $this->getChangedColumns() ) ) { $strSql .= ' pilot_last_active_date = ' . $this->sqlPilotLastActiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate() . ','; } elseif( true == array_key_exists( 'TerminationDate', $this->getChangedColumns() ) ) { $strSql .= ' termination_date = ' . $this->sqlTerminationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subscription_acv = ' . $this->sqlSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'SubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' subscription_acv = ' . $this->sqlSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_acv = ' . $this->sqlTransactionAcv() . ','; } elseif( true == array_key_exists( 'TransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' transaction_acv = ' . $this->sqlTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subscription_acv_usd = ' . $this->sqlSubscriptionAcvUsd() . ','; } elseif( true == array_key_exists( 'SubscriptionAcvUsd', $this->getChangedColumns() ) ) { $strSql .= ' subscription_acv_usd = ' . $this->sqlSubscriptionAcvUsd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_acv_usd = ' . $this->sqlTransactionAcvUsd() . ','; } elseif( true == array_key_exists( 'TransactionAcvUsd', $this->getChangedColumns() ) ) { $strSql .= ' transaction_acv_usd = ' . $this->sqlTransactionAcvUsd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_won = ' . $this->sqlIsWon() . ','; } elseif( true == array_key_exists( 'IsWon', $this->getChangedColumns() ) ) { $strSql .= ' is_won = ' . $this->sqlIsWon() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'month' => $this->getMonth(),
			'contract_id' => $this->getContractId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'cid' => $this->getCid(),
			'ps_product_id' => $this->getPsProductId(),
			'property_id' => $this->getPropertyId(),
			'company_name' => $this->getCompanyName(),
			'product_name' => $this->getProductName(),
			'property_name' => $this->getPropertyName(),
			'property_type' => $this->getPropertyType(),
			'property_unit_count' => $this->getPropertyUnitCount(),
			'unit_transactional_average' => $this->getUnitTransactionalAverage(),
			'contract_start_date' => $this->getContractStartDate(),
			'close_date' => $this->getCloseDate(),
			'contract_entered_pipeline_on' => $this->getContractEnteredPipelineOn(),
			'contract_won_on' => $this->getContractWonOn(),
			'contract_lost_on' => $this->getContractLostOn(),
			'contract_terminated_on' => $this->getContractTerminatedOn(),
			'pilot_start_date' => $this->getPilotStartDate(),
			'pilot_last_active_date' => $this->getPilotLastActiveDate(),
			'termination_date' => $this->getTerminationDate(),
			'subscription_acv' => $this->getSubscriptionAcv(),
			'transaction_acv' => $this->getTransactionAcv(),
			'subscription_acv_usd' => $this->getSubscriptionAcvUsd(),
			'transaction_acv_usd' => $this->getTransactionAcvUsd(),
			'is_won' => $this->getIsWon(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>