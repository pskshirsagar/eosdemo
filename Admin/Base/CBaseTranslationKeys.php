<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTranslationKeys
 * Do not add any new functions to this class.
 */

class CBaseTranslationKeys extends CEosPluralBase {

	/**
	 * @return CTranslationKey[]
	 */
	public static function fetchTranslationKeys( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTranslationKey::class, $objDatabase );
	}

	/**
	 * @return CTranslationKey
	 */
	public static function fetchTranslationKey( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTranslationKey::class, $objDatabase );
	}

	public static function fetchTranslationKeyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'translation_keys', $objDatabase );
	}

	public static function fetchTranslationKeyById( $intId, $objDatabase ) {
		return self::fetchTranslationKey( sprintf( 'SELECT * FROM translation_keys WHERE id = %d', $intId ), $objDatabase );
	}

}
?>