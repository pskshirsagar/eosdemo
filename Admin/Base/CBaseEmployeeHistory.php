<?php

class CBaseEmployeeHistory extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_histories';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intDesignationId;
	protected $m_intDepartmentId;
	protected $m_intTeamId;
	protected $m_intManagerEmployeeId;
	protected $m_strPipDetails;
	protected $m_strDatetime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strHistoryData;
	protected $m_jsonHistoryData;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['team_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamId', trim( $arrValues['team_id'] ) ); elseif( isset( $arrValues['team_id'] ) ) $this->setTeamId( $arrValues['team_id'] );
		if( isset( $arrValues['manager_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intManagerEmployeeId', trim( $arrValues['manager_employee_id'] ) ); elseif( isset( $arrValues['manager_employee_id'] ) ) $this->setManagerEmployeeId( $arrValues['manager_employee_id'] );
		if( isset( $arrValues['pip_details'] ) && $boolDirectSet ) $this->set( 'm_strPipDetails', trim( stripcslashes( $arrValues['pip_details'] ) ) ); elseif( isset( $arrValues['pip_details'] ) ) $this->setPipDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pip_details'] ) : $arrValues['pip_details'] );
		if( isset( $arrValues['datetime'] ) && $boolDirectSet ) $this->set( 'm_strDatetime', trim( $arrValues['datetime'] ) ); elseif( isset( $arrValues['datetime'] ) ) $this->setDatetime( $arrValues['datetime'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['history_data'] ) ) $this->set( 'm_strHistoryData', trim( $arrValues['history_data'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setTeamId( $intTeamId ) {
		$this->set( 'm_intTeamId', CStrings::strToIntDef( $intTeamId, NULL, false ) );
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function sqlTeamId() {
		return ( true == isset( $this->m_intTeamId ) ) ? ( string ) $this->m_intTeamId : 'NULL';
	}

	public function setManagerEmployeeId( $intManagerEmployeeId ) {
		$this->set( 'm_intManagerEmployeeId', CStrings::strToIntDef( $intManagerEmployeeId, NULL, false ) );
	}

	public function getManagerEmployeeId() {
		return $this->m_intManagerEmployeeId;
	}

	public function sqlManagerEmployeeId() {
		return ( true == isset( $this->m_intManagerEmployeeId ) ) ? ( string ) $this->m_intManagerEmployeeId : 'NULL';
	}

	public function setPipDetails( $strPipDetails ) {
		$this->set( 'm_strPipDetails', CStrings::strTrimDef( $strPipDetails, -1, NULL, true ) );
	}

	public function getPipDetails() {
		return $this->m_strPipDetails;
	}

	public function sqlPipDetails() {
		return ( true == isset( $this->m_strPipDetails ) ) ? '\'' . addslashes( $this->m_strPipDetails ) . '\'' : 'NULL';
	}

	public function setDatetime( $strDatetime ) {
		$this->set( 'm_strDatetime', CStrings::strTrimDef( $strDatetime, -1, NULL, true ) );
	}

	public function getDatetime() {
		return $this->m_strDatetime;
	}

	public function sqlDatetime() {
		return ( true == isset( $this->m_strDatetime ) ) ? '\'' . $this->m_strDatetime . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setHistoryData( $jsonHistoryData ) {
		if( true == valObj( $jsonHistoryData, 'stdClass' ) ) {
			$this->set( 'm_jsonHistoryData', $jsonHistoryData );
		} elseif( true == valJsonString( $jsonHistoryData ) ) {
			$this->set( 'm_jsonHistoryData', CStrings::strToJson( $jsonHistoryData ) );
		} else {
			$this->set( 'm_jsonHistoryData', NULL ); 
		}
		unset( $this->m_strHistoryData );
	}

	public function getHistoryData() {
		if( true == isset( $this->m_strHistoryData ) ) {
			$this->m_jsonHistoryData = CStrings::strToJson( $this->m_strHistoryData );
			unset( $this->m_strHistoryData );
		}
		return $this->m_jsonHistoryData;
	}

	public function sqlHistoryData() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getHistoryData() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getHistoryData() ) ) . '\'';
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, designation_id, department_id, team_id, manager_employee_id, pip_details, datetime, created_by, created_on, history_data )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlDesignationId() . ', ' .
 						$this->sqlDepartmentId() . ', ' .
 						$this->sqlTeamId() . ', ' .
 						$this->sqlManagerEmployeeId() . ', ' .
 						$this->sqlPipDetails() . ', ' .
 						$this->sqlDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlHistoryData() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; } elseif( true == array_key_exists( 'TeamId', $this->getChangedColumns() ) ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manager_employee_id = ' . $this->sqlManagerEmployeeId() . ','; } elseif( true == array_key_exists( 'ManagerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' manager_employee_id = ' . $this->sqlManagerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pip_details = ' . $this->sqlPipDetails() . ','; } elseif( true == array_key_exists( 'PipDetails', $this->getChangedColumns() ) ) { $strSql .= ' pip_details = ' . $this->sqlPipDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' datetime = ' . $this->sqlDatetime() . ','; } elseif( true == array_key_exists( 'Datetime', $this->getChangedColumns() ) ) { $strSql .= ' datetime = ' . $this->sqlDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' history_data = ' . $this->sqlHistoryData() . ','; } elseif( true == array_key_exists( 'HistoryData', $this->getChangedColumns() ) ) { $strSql .= ' history_data = ' . $this->sqlHistoryData() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'designation_id' => $this->getDesignationId(),
			'department_id' => $this->getDepartmentId(),
			'team_id' => $this->getTeamId(),
			'manager_employee_id' => $this->getManagerEmployeeId(),
			'pip_details' => $this->getPipDetails(),
			'datetime' => $this->getDatetime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'history_data' => $this->getHistoryData()
		);
	}

}
?>