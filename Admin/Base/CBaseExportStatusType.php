<?php

class CBaseExportStatusType extends CEosSingularBase {

	const TABLE_NAME = 'public.export_status_types';

	protected $m_intId;
	protected $m_intExportPartnerId;
	protected $m_strName;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['export_partner_id'] ) && $boolDirectSet ) $this->set( 'm_intExportPartnerId', trim( $arrValues['export_partner_id'] ) ); elseif( isset( $arrValues['export_partner_id'] ) ) $this->setExportPartnerId( $arrValues['export_partner_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setExportPartnerId( $intExportPartnerId ) {
		$this->set( 'm_intExportPartnerId', CStrings::strToIntDef( $intExportPartnerId, NULL, false ) );
	}

	public function getExportPartnerId() {
		return $this->m_intExportPartnerId;
	}

	public function sqlExportPartnerId() {
		return ( true == isset( $this->m_intExportPartnerId ) ) ? ( string ) $this->m_intExportPartnerId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'export_partner_id' => $this->getExportPartnerId(),
			'name' => $this->getName()
		);
	}

}
?>