<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskQaApprovals
 * Do not add any new functions to this class.
 */

class CBaseTaskQaApprovals extends CEosPluralBase {

	/**
	 * @return CTaskQaApproval[]
	 */
	public static function fetchTaskQaApprovals( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTaskQaApproval::class, $objDatabase );
	}

	/**
	 * @return CTaskQaApproval
	 */
	public static function fetchTaskQaApproval( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaskQaApproval::class, $objDatabase );
	}

	public static function fetchTaskQaApprovalCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_qa_approvals', $objDatabase );
	}

	public static function fetchTaskQaApprovalById( $intId, $objDatabase ) {
		return self::fetchTaskQaApproval( sprintf( 'SELECT * FROM task_qa_approvals WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskQaApprovalsByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchTaskQaApprovals( sprintf( 'SELECT * FROM task_qa_approvals WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

	public static function fetchTaskQaApprovalsByDeploymentId( $intDeploymentId, $objDatabase ) {
		return self::fetchTaskQaApprovals( sprintf( 'SELECT * FROM task_qa_approvals WHERE deployment_id = %d', ( int ) $intDeploymentId ), $objDatabase );
	}

}
?>