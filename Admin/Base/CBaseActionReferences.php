<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CActionReferences
 * Do not add any new functions to this class.
 */

class CBaseActionReferences extends CEosPluralBase {

	/**
	 * @return CActionReference[]
	 */
	public static function fetchActionReferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CActionReference', $objDatabase );
	}

	/**
	 * @return CActionReference
	 */
	public static function fetchActionReference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CActionReference', $objDatabase );
	}

	public static function fetchActionReferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'action_references', $objDatabase );
	}

	public static function fetchActionReferenceById( $intId, $objDatabase ) {
		return self::fetchActionReference( sprintf( 'SELECT * FROM action_references WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchActionReferencesByActionReferenceTypeId( $intActionReferenceTypeId, $objDatabase ) {
		return self::fetchActionReferences( sprintf( 'SELECT * FROM action_references WHERE action_reference_type_id = %d', ( int ) $intActionReferenceTypeId ), $objDatabase );
	}

	public static function fetchActionReferencesByActionId( $intActionId, $objDatabase ) {
		return self::fetchActionReferences( sprintf( 'SELECT * FROM action_references WHERE action_id = %d', ( int ) $intActionId ), $objDatabase );
	}

}
?>