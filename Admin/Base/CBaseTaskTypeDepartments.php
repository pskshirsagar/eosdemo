<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskTypeDepartments
 * Do not add any new functions to this class.
 */

class CBaseTaskTypeDepartments extends CEosPluralBase {

	/**
	 * @return CTaskTypeDepartment[]
	 */
	public static function fetchTaskTypeDepartments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskTypeDepartment', $objDatabase );
	}

	/**
	 * @return CTaskTypeDepartment
	 */
	public static function fetchTaskTypeDepartment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskTypeDepartment', $objDatabase );
	}

	public static function fetchTaskTypeDepartmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_type_departments', $objDatabase );
	}

	public static function fetchTaskTypeDepartmentById( $intId, $objDatabase ) {
		return self::fetchTaskTypeDepartment( sprintf( 'SELECT * FROM task_type_departments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskTypeDepartmentsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchTaskTypeDepartments( sprintf( 'SELECT * FROM task_type_departments WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchTaskTypeDepartmentsByTaskTypeId( $intTaskTypeId, $objDatabase ) {
		return self::fetchTaskTypeDepartments( sprintf( 'SELECT * FROM task_type_departments WHERE task_type_id = %d', ( int ) $intTaskTypeId ), $objDatabase );
	}

}
?>