<?php

class CBaseSurveyTemplateManagerQuestionAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.survey_template_manager_question_associations';

	protected $m_intId;
	protected $m_intSurveyTemplateManagerQuestionId;
	protected $m_intSurveyTemplateId;
	protected $m_intSurveyTemplateQuestionId;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['survey_template_manager_question_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateManagerQuestionId', trim( $arrValues['survey_template_manager_question_id'] ) ); elseif( isset( $arrValues['survey_template_manager_question_id'] ) ) $this->setSurveyTemplateManagerQuestionId( $arrValues['survey_template_manager_question_id'] );
		if( isset( $arrValues['survey_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateId', trim( $arrValues['survey_template_id'] ) ); elseif( isset( $arrValues['survey_template_id'] ) ) $this->setSurveyTemplateId( $arrValues['survey_template_id'] );
		if( isset( $arrValues['survey_template_question_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateQuestionId', trim( $arrValues['survey_template_question_id'] ) ); elseif( isset( $arrValues['survey_template_question_id'] ) ) $this->setSurveyTemplateQuestionId( $arrValues['survey_template_question_id'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSurveyTemplateManagerQuestionId( $intSurveyTemplateManagerQuestionId ) {
		$this->set( 'm_intSurveyTemplateManagerQuestionId', CStrings::strToIntDef( $intSurveyTemplateManagerQuestionId, NULL, false ) );
	}

	public function getSurveyTemplateManagerQuestionId() {
		return $this->m_intSurveyTemplateManagerQuestionId;
	}

	public function sqlSurveyTemplateManagerQuestionId() {
		return ( true == isset( $this->m_intSurveyTemplateManagerQuestionId ) ) ? ( string ) $this->m_intSurveyTemplateManagerQuestionId : 'NULL';
	}

	public function setSurveyTemplateId( $intSurveyTemplateId ) {
		$this->set( 'm_intSurveyTemplateId', CStrings::strToIntDef( $intSurveyTemplateId, NULL, false ) );
	}

	public function getSurveyTemplateId() {
		return $this->m_intSurveyTemplateId;
	}

	public function sqlSurveyTemplateId() {
		return ( true == isset( $this->m_intSurveyTemplateId ) ) ? ( string ) $this->m_intSurveyTemplateId : 'NULL';
	}

	public function setSurveyTemplateQuestionId( $intSurveyTemplateQuestionId ) {
		$this->set( 'm_intSurveyTemplateQuestionId', CStrings::strToIntDef( $intSurveyTemplateQuestionId, NULL, false ) );
	}

	public function getSurveyTemplateQuestionId() {
		return $this->m_intSurveyTemplateQuestionId;
	}

	public function sqlSurveyTemplateQuestionId() {
		return ( true == isset( $this->m_intSurveyTemplateQuestionId ) ) ? ( string ) $this->m_intSurveyTemplateQuestionId : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, survey_template_manager_question_id, survey_template_id, survey_template_question_id, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSurveyTemplateManagerQuestionId() . ', ' .
 						$this->sqlSurveyTemplateId() . ', ' .
 						$this->sqlSurveyTemplateQuestionId() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_manager_question_id = ' . $this->sqlSurveyTemplateManagerQuestionId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateManagerQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_manager_question_id = ' . $this->sqlSurveyTemplateManagerQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_question_id = ' . $this->sqlSurveyTemplateQuestionId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_question_id = ' . $this->sqlSurveyTemplateQuestionId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'survey_template_manager_question_id' => $this->getSurveyTemplateManagerQuestionId(),
			'survey_template_id' => $this->getSurveyTemplateId(),
			'survey_template_question_id' => $this->getSurveyTemplateQuestionId(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>