<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskAcceptanceCriterias
 * Do not add any new functions to this class.
 */

class CBaseTaskAcceptanceCriterias extends CEosPluralBase {

	/**
	 * @return CTaskAcceptanceCriteria[]
	 */
	public static function fetchTaskAcceptanceCriterias( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskAcceptanceCriteria', $objDatabase );
	}

	/**
	 * @return CTaskAcceptanceCriteria
	 */
	public static function fetchTaskAcceptanceCriteria( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskAcceptanceCriteria', $objDatabase );
	}

	public static function fetchTaskAcceptanceCriteriaCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_acceptance_criterias', $objDatabase );
	}

	public static function fetchTaskAcceptanceCriteriaById( $intId, $objDatabase ) {
		return self::fetchTaskAcceptanceCriteria( sprintf( 'SELECT * FROM task_acceptance_criterias WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskAcceptanceCriteriasByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchTaskAcceptanceCriterias( sprintf( 'SELECT * FROM task_acceptance_criterias WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

}
?>