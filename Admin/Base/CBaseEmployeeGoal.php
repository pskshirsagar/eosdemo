<?php

class CBaseEmployeeGoal extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.employee_goals';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intCallAgentId;
	protected $m_intCallAgentEmployeeId;
	protected $m_intEmployeeGoalTypeId;
	protected $m_intEmployeeInteractionId;
	protected $m_strTitle;
	protected $m_strNote;
	protected $m_strFeedback;
	protected $m_strDueDate;
	protected $m_fltStartGoalValue;
	protected $m_fltEndGoalValue;
	protected $m_intAssignedBy;
	protected $m_strAssignedOn;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strTitle = NULL;
		$this->m_strNote = NULL;
		$this->m_strFeedback = NULL;
		$this->m_fltStartGoalValue = NULL;
		$this->m_fltEndGoalValue = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['call_agent_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentEmployeeId', trim( $arrValues['call_agent_employee_id'] ) ); elseif( isset( $arrValues['call_agent_employee_id'] ) ) $this->setCallAgentEmployeeId( $arrValues['call_agent_employee_id'] );
		if( isset( $arrValues['employee_goal_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeGoalTypeId', trim( $arrValues['employee_goal_type_id'] ) ); elseif( isset( $arrValues['employee_goal_type_id'] ) ) $this->setEmployeeGoalTypeId( $arrValues['employee_goal_type_id'] );
		if( isset( $arrValues['employee_interaction_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeInteractionId', trim( $arrValues['employee_interaction_id'] ) ); elseif( isset( $arrValues['employee_interaction_id'] ) ) $this->setEmployeeInteractionId( $arrValues['employee_interaction_id'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['feedback'] ) && $boolDirectSet ) $this->set( 'm_strFeedback', trim( stripcslashes( $arrValues['feedback'] ) ) ); elseif( isset( $arrValues['feedback'] ) ) $this->setFeedback( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['feedback'] ) : $arrValues['feedback'] );
		if( isset( $arrValues['due_date'] ) && $boolDirectSet ) $this->set( 'm_strDueDate', trim( $arrValues['due_date'] ) ); elseif( isset( $arrValues['due_date'] ) ) $this->setDueDate( $arrValues['due_date'] );
		if( isset( $arrValues['start_goal_value'] ) && $boolDirectSet ) $this->set( 'm_fltStartGoalValue', trim( $arrValues['start_goal_value'] ) ); elseif( isset( $arrValues['start_goal_value'] ) ) $this->setStartGoalValue( $arrValues['start_goal_value'] );
		if( isset( $arrValues['end_goal_value'] ) && $boolDirectSet ) $this->set( 'm_fltEndGoalValue', trim( $arrValues['end_goal_value'] ) ); elseif( isset( $arrValues['end_goal_value'] ) ) $this->setEndGoalValue( $arrValues['end_goal_value'] );
		if( isset( $arrValues['assigned_by'] ) && $boolDirectSet ) $this->set( 'm_intAssignedBy', trim( $arrValues['assigned_by'] ) ); elseif( isset( $arrValues['assigned_by'] ) ) $this->setAssignedBy( $arrValues['assigned_by'] );
		if( isset( $arrValues['assigned_on'] ) && $boolDirectSet ) $this->set( 'm_strAssignedOn', trim( $arrValues['assigned_on'] ) ); elseif( isset( $arrValues['assigned_on'] ) ) $this->setAssignedOn( $arrValues['assigned_on'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setCallAgentEmployeeId( $intCallAgentEmployeeId ) {
		$this->set( 'm_intCallAgentEmployeeId', CStrings::strToIntDef( $intCallAgentEmployeeId, NULL, false ) );
	}

	public function getCallAgentEmployeeId() {
		return $this->m_intCallAgentEmployeeId;
	}

	public function sqlCallAgentEmployeeId() {
		return ( true == isset( $this->m_intCallAgentEmployeeId ) ) ? ( string ) $this->m_intCallAgentEmployeeId : 'NULL';
	}

	public function setEmployeeGoalTypeId( $intEmployeeGoalTypeId ) {
		$this->set( 'm_intEmployeeGoalTypeId', CStrings::strToIntDef( $intEmployeeGoalTypeId, NULL, false ) );
	}

	public function getEmployeeGoalTypeId() {
		return $this->m_intEmployeeGoalTypeId;
	}

	public function sqlEmployeeGoalTypeId() {
		return ( true == isset( $this->m_intEmployeeGoalTypeId ) ) ? ( string ) $this->m_intEmployeeGoalTypeId : 'NULL';
	}

	public function setEmployeeInteractionId( $intEmployeeInteractionId ) {
		$this->set( 'm_intEmployeeInteractionId', CStrings::strToIntDef( $intEmployeeInteractionId, NULL, false ) );
	}

	public function getEmployeeInteractionId() {
		return $this->m_intEmployeeInteractionId;
	}

	public function sqlEmployeeInteractionId() {
		return ( true == isset( $this->m_intEmployeeInteractionId ) ) ? ( string ) $this->m_intEmployeeInteractionId : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 500, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : '\'NULL\'';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, 500, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : '\'NULL\'';
	}

	public function setFeedback( $strFeedback ) {
		$this->set( 'm_strFeedback', CStrings::strTrimDef( $strFeedback, 500, NULL, true ) );
	}

	public function getFeedback() {
		return $this->m_strFeedback;
	}

	public function sqlFeedback() {
		return ( true == isset( $this->m_strFeedback ) ) ? '\'' . addslashes( $this->m_strFeedback ) . '\'' : '\'NULL\'';
	}

	public function setDueDate( $strDueDate ) {
		$this->set( 'm_strDueDate', CStrings::strTrimDef( $strDueDate, -1, NULL, true ) );
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function sqlDueDate() {
		return ( true == isset( $this->m_strDueDate ) ) ? '\'' . $this->m_strDueDate . '\'' : 'NULL';
	}

	public function setStartGoalValue( $fltStartGoalValue ) {
		$this->set( 'm_fltStartGoalValue', CStrings::strToFloatDef( $fltStartGoalValue, NULL, false, 1 ) );
	}

	public function getStartGoalValue() {
		return $this->m_fltStartGoalValue;
	}

	public function sqlStartGoalValue() {
		return ( true == isset( $this->m_fltStartGoalValue ) ) ? ( string ) $this->m_fltStartGoalValue : 'NULL::numeric';
	}

	public function setEndGoalValue( $fltEndGoalValue ) {
		$this->set( 'm_fltEndGoalValue', CStrings::strToFloatDef( $fltEndGoalValue, NULL, false, 1 ) );
	}

	public function getEndGoalValue() {
		return $this->m_fltEndGoalValue;
	}

	public function sqlEndGoalValue() {
		return ( true == isset( $this->m_fltEndGoalValue ) ) ? ( string ) $this->m_fltEndGoalValue : 'NULL::numeric';
	}

	public function setAssignedBy( $intAssignedBy ) {
		$this->set( 'm_intAssignedBy', CStrings::strToIntDef( $intAssignedBy, NULL, false ) );
	}

	public function getAssignedBy() {
		return $this->m_intAssignedBy;
	}

	public function sqlAssignedBy() {
		return ( true == isset( $this->m_intAssignedBy ) ) ? ( string ) $this->m_intAssignedBy : 'NULL';
	}

	public function setAssignedOn( $strAssignedOn ) {
		$this->set( 'm_strAssignedOn', CStrings::strTrimDef( $strAssignedOn, -1, NULL, true ) );
	}

	public function getAssignedOn() {
		return $this->m_strAssignedOn;
	}

	public function sqlAssignedOn() {
		return ( true == isset( $this->m_strAssignedOn ) ) ? '\'' . $this->m_strAssignedOn . '\'' : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, call_agent_id, call_agent_employee_id, employee_goal_type_id, employee_interaction_id, title, note, feedback, due_date, start_goal_value, end_goal_value, assigned_by, assigned_on, completed_by, completed_on, approved_by, approved_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlCallAgentId() . ', ' .
						$this->sqlCallAgentEmployeeId() . ', ' .
						$this->sqlEmployeeGoalTypeId() . ', ' .
						$this->sqlEmployeeInteractionId() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlNote() . ', ' .
						$this->sqlFeedback() . ', ' .
						$this->sqlDueDate() . ', ' .
						$this->sqlStartGoalValue() . ', ' .
						$this->sqlEndGoalValue() . ', ' .
						$this->sqlAssignedBy() . ', ' .
						$this->sqlAssignedOn() . ', ' .
						$this->sqlCompletedBy() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId(). ',' ; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_employee_id = ' . $this->sqlCallAgentEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CallAgentEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_employee_id = ' . $this->sqlCallAgentEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_goal_type_id = ' . $this->sqlEmployeeGoalTypeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeGoalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_goal_type_id = ' . $this->sqlEmployeeGoalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_interaction_id = ' . $this->sqlEmployeeInteractionId(). ',' ; } elseif( true == array_key_exists( 'EmployeeInteractionId', $this->getChangedColumns() ) ) { $strSql .= ' employee_interaction_id = ' . $this->sqlEmployeeInteractionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote(). ',' ; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' feedback = ' . $this->sqlFeedback(). ',' ; } elseif( true == array_key_exists( 'Feedback', $this->getChangedColumns() ) ) { $strSql .= ' feedback = ' . $this->sqlFeedback() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date = ' . $this->sqlDueDate(). ',' ; } elseif( true == array_key_exists( 'DueDate', $this->getChangedColumns() ) ) { $strSql .= ' due_date = ' . $this->sqlDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_goal_value = ' . $this->sqlStartGoalValue(). ',' ; } elseif( true == array_key_exists( 'StartGoalValue', $this->getChangedColumns() ) ) { $strSql .= ' start_goal_value = ' . $this->sqlStartGoalValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_goal_value = ' . $this->sqlEndGoalValue(). ',' ; } elseif( true == array_key_exists( 'EndGoalValue', $this->getChangedColumns() ) ) { $strSql .= ' end_goal_value = ' . $this->sqlEndGoalValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assigned_by = ' . $this->sqlAssignedBy(). ',' ; } elseif( true == array_key_exists( 'AssignedBy', $this->getChangedColumns() ) ) { $strSql .= ' assigned_by = ' . $this->sqlAssignedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assigned_on = ' . $this->sqlAssignedOn(). ',' ; } elseif( true == array_key_exists( 'AssignedOn', $this->getChangedColumns() ) ) { $strSql .= ' assigned_on = ' . $this->sqlAssignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'call_agent_id' => $this->getCallAgentId(),
			'call_agent_employee_id' => $this->getCallAgentEmployeeId(),
			'employee_goal_type_id' => $this->getEmployeeGoalTypeId(),
			'employee_interaction_id' => $this->getEmployeeInteractionId(),
			'title' => $this->getTitle(),
			'note' => $this->getNote(),
			'feedback' => $this->getFeedback(),
			'due_date' => $this->getDueDate(),
			'start_goal_value' => $this->getStartGoalValue(),
			'end_goal_value' => $this->getEndGoalValue(),
			'assigned_by' => $this->getAssignedBy(),
			'assigned_on' => $this->getAssignedOn(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>