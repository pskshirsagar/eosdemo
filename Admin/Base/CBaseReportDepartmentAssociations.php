<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportDepartmentAssociations
 * Do not add any new functions to this class.
 */

class CBaseReportDepartmentAssociations extends CEosPluralBase {

	/**
	 * @return CReportDepartmentAssociation[]
	 */
	public static function fetchReportDepartmentAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportDepartmentAssociation::class, $objDatabase );
	}

	/**
	 * @return CReportDepartmentAssociation
	 */
	public static function fetchReportDepartmentAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportDepartmentAssociation::class, $objDatabase );
	}

	public static function fetchReportDepartmentAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_department_associations', $objDatabase );
	}

	public static function fetchReportDepartmentAssociationById( $intId, $objDatabase ) {
		return self::fetchReportDepartmentAssociation( sprintf( 'SELECT * FROM report_department_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportDepartmentAssociationsByCompanyReportId( $intCompanyReportId, $objDatabase ) {
		return self::fetchReportDepartmentAssociations( sprintf( 'SELECT * FROM report_department_associations WHERE company_report_id = %d', ( int ) $intCompanyReportId ), $objDatabase );
	}

	public static function fetchReportDepartmentAssociationsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchReportDepartmentAssociations( sprintf( 'SELECT * FROM report_department_associations WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>