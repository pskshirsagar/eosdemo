<?php

class CBaseEmployeeSupportSchedule extends CEosSingularBase {

	protected $m_intId;
	protected $m_intDevEmployeeId;
	protected $m_intQaEmployeeId;
	protected $m_strBeginDatetime;
	protected $m_strEndDatetime;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['dev_employee_id'] ) && $boolDirectSet ) $this->m_intDevEmployeeId = trim( $arrValues['dev_employee_id'] ); else if( isset( $arrValues['dev_employee_id'] ) ) $this->setDevEmployeeId( $arrValues['dev_employee_id'] );
		if( isset( $arrValues['qa_employee_id'] ) && $boolDirectSet ) $this->m_intQaEmployeeId = trim( $arrValues['qa_employee_id'] ); else if( isset( $arrValues['qa_employee_id'] ) ) $this->setQaEmployeeId( $arrValues['qa_employee_id'] );
		if( isset( $arrValues['begin_datetime'] ) && $boolDirectSet ) $this->m_strBeginDatetime = trim( $arrValues['begin_datetime'] ); else if( isset( $arrValues['begin_datetime'] ) ) $this->setBeginDatetime( $arrValues['begin_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->m_strEndDatetime = trim( $arrValues['end_datetime'] ); else if( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->m_intPsProductId = trim( $arrValues['ps_product_id'] ); else if( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->m_intPsProductOptionId = trim( $arrValues['ps_product_option_id'] ); else if( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->m_intDeletedBy = trim( $arrValues['deleted_by'] ); else if( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->m_strDeletedOn = trim( $arrValues['deleted_on'] ); else if( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->m_intUpdatedBy = trim( $arrValues['updated_by'] ); else if( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->m_strUpdatedOn = trim( $arrValues['updated_on'] ); else if( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? (string) $this->m_intId : 'NULL';
	}

	public function setDevEmployeeId( $intDevEmployeeId ) {
		$this->m_intDevEmployeeId = CStrings::strToIntDef( $intDevEmployeeId, NULL, false );
	}

	public function getDevEmployeeId() {
		return $this->m_intDevEmployeeId;
	}

	public function sqlDevEmployeeId() {
		return ( true == isset( $this->m_intDevEmployeeId ) ) ? (string) $this->m_intDevEmployeeId : 'NULL';
	}

	public function setQaEmployeeId( $intQaEmployeeId ) {
		$this->m_intQaEmployeeId = CStrings::strToIntDef( $intQaEmployeeId, NULL, false );
	}

	public function getQaEmployeeId() {
		return $this->m_intQaEmployeeId;
	}

	public function sqlQaEmployeeId() {
		return ( true == isset( $this->m_intQaEmployeeId ) ) ? (string) $this->m_intQaEmployeeId : 'NULL';
	}

	public function setBeginDatetime( $strBeginDatetime ) {
		$this->m_strBeginDatetime = CStrings::strTrimDef( $strBeginDatetime, -1, NULL, true );
	}

	public function getBeginDatetime() {
		return $this->m_strBeginDatetime;
	}

	public function sqlBeginDatetime() {
		return ( true == isset( $this->m_strBeginDatetime ) ) ? '\'' . $this->m_strBeginDatetime . '\'' : 'NULL';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->m_strEndDatetime = CStrings::strTrimDef( $strEndDatetime, -1, NULL, true );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = CStrings::strToIntDef( $intPsProductId, NULL, false );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? (string) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->m_intPsProductOptionId = CStrings::strToIntDef( $intPsProductOptionId, NULL, false );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? (string) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->m_intDeletedBy = CStrings::strToIntDef( $intDeletedBy, NULL, false );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? (string) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->m_strDeletedOn = CStrings::strTrimDef( $strDeletedOn, -1, NULL, true );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->m_intUpdatedBy = CStrings::strToIntDef( $intUpdatedBy, NULL, false );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? (string) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->m_strUpdatedOn = CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? (string) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'public.employee_support_schedules_id_seq\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						public.employee_support_schedules
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDevEmployeeId() . ', ' .
 						$this->sqlQaEmployeeId() . ', ' .
 						$this->sqlBeginDatetime() . ', ' .
 						$this->sqlEndDatetime() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPsProductOptionId() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						(int) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						(int) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						public.employee_support_schedules
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dev_employee_id = ' . $this->sqlDevEmployeeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDevEmployeeId() ) != $this->getOriginalValueByFieldName ( 'dev_employee_id' ) ) { $arrstrOriginalValueChanges['dev_employee_id'] = $this->sqlDevEmployeeId(); $strSql .= ' dev_employee_id = ' . $this->sqlDevEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlQaEmployeeId() ) != $this->getOriginalValueByFieldName ( 'qa_employee_id' ) ) { $arrstrOriginalValueChanges['qa_employee_id'] = $this->sqlQaEmployeeId(); $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_datetime = ' . $this->sqlBeginDatetime() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlBeginDatetime() ) != $this->getOriginalValueByFieldName ( 'begin_datetime' ) ) { $arrstrOriginalValueChanges['begin_datetime'] = $this->sqlBeginDatetime(); $strSql .= ' begin_datetime = ' . $this->sqlBeginDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEndDatetime() ) != $this->getOriginalValueByFieldName ( 'end_datetime' ) ) { $arrstrOriginalValueChanges['end_datetime'] = $this->sqlEndDatetime(); $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPsProductId() ) != $this->getOriginalValueByFieldName ( 'ps_product_id' ) ) { $arrstrOriginalValueChanges['ps_product_id'] = $this->sqlPsProductId(); $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPsProductOptionId() ) != $this->getOriginalValueByFieldName ( 'ps_product_option_id' ) ) { $arrstrOriginalValueChanges['ps_product_option_id'] = $this->sqlPsProductOptionId(); $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeletedBy() ) != $this->getOriginalValueByFieldName ( 'deleted_by' ) ) { $arrstrOriginalValueChanges['deleted_by'] = $this->sqlDeletedBy(); $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlDeletedOn() ) != $this->getOriginalValueByFieldName ( 'deleted_on' ) ) { $arrstrOriginalValueChanges['deleted_on'] = $this->sqlDeletedOn(); $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . (int) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . (int) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM public.employee_support_schedules WHERE id = ' . (int) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function fetchNextId( $objDatabase, $strTableName = NULL ) {
		return parent::fetchNextId( 'public.employee_support_schedules_id_seq', $objDatabase );
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'dev_employee_id' => $this->getDevEmployeeId(),
			'qa_employee_id' => $this->getQaEmployeeId(),
			'begin_datetime' => $this->getBeginDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>