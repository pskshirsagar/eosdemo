<?php

class CBaseInvoiceBatch extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.invoice_batches';

	protected $m_intId;
	protected $m_intInvoiceBatchStepId;
	protected $m_intExportBatchId;
	protected $m_strBatchDatetime;
	protected $m_fltTrack1ChargesInPeriod;
	protected $m_fltTrack1PaymentsInPeriod;
	protected $m_intTrack1InvoicesMailed;
	protected $m_intTrack1InvoicesEmailed;
	protected $m_intTrack1InvoicesFtpd;
	protected $m_intTrack1InvoiceCount;
	protected $m_fltTrack1AmountUnsuccessful;
	protected $m_fltTrack2ChargesInPeriod;
	protected $m_fltTrack2PaymentsInPeriod;
	protected $m_intTrack2InvoicesMailed;
	protected $m_intTrack2InvoicesEmailed;
	protected $m_intTrack2InvoicesFtpd;
	protected $m_intTrack2InvoiceCount;
	protected $m_fltTrack2AmountUnsuccessful;
	protected $m_intExportedBy;
	protected $m_strExportedOn;
	protected $m_strInvoiceBatchProcessDetails;
	protected $m_jsonInvoiceBatchProcessDetails;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_fltTrack1ChargesInPeriod = '0';
		$this->m_fltTrack1PaymentsInPeriod = '0';
		$this->m_intTrack1InvoicesMailed = '0';
		$this->m_intTrack1InvoicesEmailed = '0';
		$this->m_intTrack1InvoicesFtpd = '0';
		$this->m_intTrack1InvoiceCount = '0';
		$this->m_fltTrack1AmountUnsuccessful = '0';
		$this->m_fltTrack2ChargesInPeriod = '0';
		$this->m_fltTrack2PaymentsInPeriod = '0';
		$this->m_intTrack2InvoicesMailed = '0';
		$this->m_intTrack2InvoicesEmailed = '0';
		$this->m_intTrack2InvoicesFtpd = '0';
		$this->m_intTrack2InvoiceCount = '0';
		$this->m_fltTrack2AmountUnsuccessful = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['invoice_batch_step_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceBatchStepId', trim( $arrValues['invoice_batch_step_id'] ) ); elseif( isset( $arrValues['invoice_batch_step_id'] ) ) $this->setInvoiceBatchStepId( $arrValues['invoice_batch_step_id'] );
		if( isset( $arrValues['export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intExportBatchId', trim( $arrValues['export_batch_id'] ) ); elseif( isset( $arrValues['export_batch_id'] ) ) $this->setExportBatchId( $arrValues['export_batch_id'] );
		if( isset( $arrValues['batch_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBatchDatetime', trim( $arrValues['batch_datetime'] ) ); elseif( isset( $arrValues['batch_datetime'] ) ) $this->setBatchDatetime( $arrValues['batch_datetime'] );
		if( isset( $arrValues['track1_charges_in_period'] ) && $boolDirectSet ) $this->set( 'm_fltTrack1ChargesInPeriod', trim( $arrValues['track1_charges_in_period'] ) ); elseif( isset( $arrValues['track1_charges_in_period'] ) ) $this->setTrack1ChargesInPeriod( $arrValues['track1_charges_in_period'] );
		if( isset( $arrValues['track1_payments_in_period'] ) && $boolDirectSet ) $this->set( 'm_fltTrack1PaymentsInPeriod', trim( $arrValues['track1_payments_in_period'] ) ); elseif( isset( $arrValues['track1_payments_in_period'] ) ) $this->setTrack1PaymentsInPeriod( $arrValues['track1_payments_in_period'] );
		if( isset( $arrValues['track1_invoices_mailed'] ) && $boolDirectSet ) $this->set( 'm_intTrack1InvoicesMailed', trim( $arrValues['track1_invoices_mailed'] ) ); elseif( isset( $arrValues['track1_invoices_mailed'] ) ) $this->setTrack1InvoicesMailed( $arrValues['track1_invoices_mailed'] );
		if( isset( $arrValues['track1_invoices_emailed'] ) && $boolDirectSet ) $this->set( 'm_intTrack1InvoicesEmailed', trim( $arrValues['track1_invoices_emailed'] ) ); elseif( isset( $arrValues['track1_invoices_emailed'] ) ) $this->setTrack1InvoicesEmailed( $arrValues['track1_invoices_emailed'] );
		if( isset( $arrValues['track1_invoices_ftpd'] ) && $boolDirectSet ) $this->set( 'm_intTrack1InvoicesFtpd', trim( $arrValues['track1_invoices_ftpd'] ) ); elseif( isset( $arrValues['track1_invoices_ftpd'] ) ) $this->setTrack1InvoicesFtpd( $arrValues['track1_invoices_ftpd'] );
		if( isset( $arrValues['track1_invoice_count'] ) && $boolDirectSet ) $this->set( 'm_intTrack1InvoiceCount', trim( $arrValues['track1_invoice_count'] ) ); elseif( isset( $arrValues['track1_invoice_count'] ) ) $this->setTrack1InvoiceCount( $arrValues['track1_invoice_count'] );
		if( isset( $arrValues['track1_amount_unsuccessful'] ) && $boolDirectSet ) $this->set( 'm_fltTrack1AmountUnsuccessful', trim( $arrValues['track1_amount_unsuccessful'] ) ); elseif( isset( $arrValues['track1_amount_unsuccessful'] ) ) $this->setTrack1AmountUnsuccessful( $arrValues['track1_amount_unsuccessful'] );
		if( isset( $arrValues['track2_charges_in_period'] ) && $boolDirectSet ) $this->set( 'm_fltTrack2ChargesInPeriod', trim( $arrValues['track2_charges_in_period'] ) ); elseif( isset( $arrValues['track2_charges_in_period'] ) ) $this->setTrack2ChargesInPeriod( $arrValues['track2_charges_in_period'] );
		if( isset( $arrValues['track2_payments_in_period'] ) && $boolDirectSet ) $this->set( 'm_fltTrack2PaymentsInPeriod', trim( $arrValues['track2_payments_in_period'] ) ); elseif( isset( $arrValues['track2_payments_in_period'] ) ) $this->setTrack2PaymentsInPeriod( $arrValues['track2_payments_in_period'] );
		if( isset( $arrValues['track2_invoices_mailed'] ) && $boolDirectSet ) $this->set( 'm_intTrack2InvoicesMailed', trim( $arrValues['track2_invoices_mailed'] ) ); elseif( isset( $arrValues['track2_invoices_mailed'] ) ) $this->setTrack2InvoicesMailed( $arrValues['track2_invoices_mailed'] );
		if( isset( $arrValues['track2_invoices_emailed'] ) && $boolDirectSet ) $this->set( 'm_intTrack2InvoicesEmailed', trim( $arrValues['track2_invoices_emailed'] ) ); elseif( isset( $arrValues['track2_invoices_emailed'] ) ) $this->setTrack2InvoicesEmailed( $arrValues['track2_invoices_emailed'] );
		if( isset( $arrValues['track2_invoices_ftpd'] ) && $boolDirectSet ) $this->set( 'm_intTrack2InvoicesFtpd', trim( $arrValues['track2_invoices_ftpd'] ) ); elseif( isset( $arrValues['track2_invoices_ftpd'] ) ) $this->setTrack2InvoicesFtpd( $arrValues['track2_invoices_ftpd'] );
		if( isset( $arrValues['track2_invoice_count'] ) && $boolDirectSet ) $this->set( 'm_intTrack2InvoiceCount', trim( $arrValues['track2_invoice_count'] ) ); elseif( isset( $arrValues['track2_invoice_count'] ) ) $this->setTrack2InvoiceCount( $arrValues['track2_invoice_count'] );
		if( isset( $arrValues['track2_amount_unsuccessful'] ) && $boolDirectSet ) $this->set( 'm_fltTrack2AmountUnsuccessful', trim( $arrValues['track2_amount_unsuccessful'] ) ); elseif( isset( $arrValues['track2_amount_unsuccessful'] ) ) $this->setTrack2AmountUnsuccessful( $arrValues['track2_amount_unsuccessful'] );
		if( isset( $arrValues['exported_by'] ) && $boolDirectSet ) $this->set( 'm_intExportedBy', trim( $arrValues['exported_by'] ) ); elseif( isset( $arrValues['exported_by'] ) ) $this->setExportedBy( $arrValues['exported_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['invoice_batch_process_details'] ) ) $this->set( 'm_strInvoiceBatchProcessDetails', trim( $arrValues['invoice_batch_process_details'] ) );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInvoiceBatchStepId( $intInvoiceBatchStepId ) {
		$this->set( 'm_intInvoiceBatchStepId', CStrings::strToIntDef( $intInvoiceBatchStepId, NULL, false ) );
	}

	public function getInvoiceBatchStepId() {
		return $this->m_intInvoiceBatchStepId;
	}

	public function sqlInvoiceBatchStepId() {
		return ( true == isset( $this->m_intInvoiceBatchStepId ) ) ? ( string ) $this->m_intInvoiceBatchStepId : 'NULL';
	}

	public function setExportBatchId( $intExportBatchId ) {
		$this->set( 'm_intExportBatchId', CStrings::strToIntDef( $intExportBatchId, NULL, false ) );
	}

	public function getExportBatchId() {
		return $this->m_intExportBatchId;
	}

	public function sqlExportBatchId() {
		return ( true == isset( $this->m_intExportBatchId ) ) ? ( string ) $this->m_intExportBatchId : 'NULL';
	}

	public function setBatchDatetime( $strBatchDatetime ) {
		$this->set( 'm_strBatchDatetime', CStrings::strTrimDef( $strBatchDatetime, -1, NULL, true ) );
	}

	public function getBatchDatetime() {
		return $this->m_strBatchDatetime;
	}

	public function sqlBatchDatetime() {
		return ( true == isset( $this->m_strBatchDatetime ) ) ? '\'' . $this->m_strBatchDatetime . '\'' : 'NOW()';
	}

	public function setTrack1ChargesInPeriod( $fltTrack1ChargesInPeriod ) {
		$this->set( 'm_fltTrack1ChargesInPeriod', CStrings::strToFloatDef( $fltTrack1ChargesInPeriod, NULL, false, 2 ) );
	}

	public function getTrack1ChargesInPeriod() {
		return $this->m_fltTrack1ChargesInPeriod;
	}

	public function sqlTrack1ChargesInPeriod() {
		return ( true == isset( $this->m_fltTrack1ChargesInPeriod ) ) ? ( string ) $this->m_fltTrack1ChargesInPeriod : '0';
	}

	public function setTrack1PaymentsInPeriod( $fltTrack1PaymentsInPeriod ) {
		$this->set( 'm_fltTrack1PaymentsInPeriod', CStrings::strToFloatDef( $fltTrack1PaymentsInPeriod, NULL, false, 2 ) );
	}

	public function getTrack1PaymentsInPeriod() {
		return $this->m_fltTrack1PaymentsInPeriod;
	}

	public function sqlTrack1PaymentsInPeriod() {
		return ( true == isset( $this->m_fltTrack1PaymentsInPeriod ) ) ? ( string ) $this->m_fltTrack1PaymentsInPeriod : '0';
	}

	public function setTrack1InvoicesMailed( $intTrack1InvoicesMailed ) {
		$this->set( 'm_intTrack1InvoicesMailed', CStrings::strToIntDef( $intTrack1InvoicesMailed, NULL, false ) );
	}

	public function getTrack1InvoicesMailed() {
		return $this->m_intTrack1InvoicesMailed;
	}

	public function sqlTrack1InvoicesMailed() {
		return ( true == isset( $this->m_intTrack1InvoicesMailed ) ) ? ( string ) $this->m_intTrack1InvoicesMailed : '0';
	}

	public function setTrack1InvoicesEmailed( $intTrack1InvoicesEmailed ) {
		$this->set( 'm_intTrack1InvoicesEmailed', CStrings::strToIntDef( $intTrack1InvoicesEmailed, NULL, false ) );
	}

	public function getTrack1InvoicesEmailed() {
		return $this->m_intTrack1InvoicesEmailed;
	}

	public function sqlTrack1InvoicesEmailed() {
		return ( true == isset( $this->m_intTrack1InvoicesEmailed ) ) ? ( string ) $this->m_intTrack1InvoicesEmailed : '0';
	}

	public function setTrack1InvoicesFtpd( $intTrack1InvoicesFtpd ) {
		$this->set( 'm_intTrack1InvoicesFtpd', CStrings::strToIntDef( $intTrack1InvoicesFtpd, NULL, false ) );
	}

	public function getTrack1InvoicesFtpd() {
		return $this->m_intTrack1InvoicesFtpd;
	}

	public function sqlTrack1InvoicesFtpd() {
		return ( true == isset( $this->m_intTrack1InvoicesFtpd ) ) ? ( string ) $this->m_intTrack1InvoicesFtpd : '0';
	}

	public function setTrack1InvoiceCount( $intTrack1InvoiceCount ) {
		$this->set( 'm_intTrack1InvoiceCount', CStrings::strToIntDef( $intTrack1InvoiceCount, NULL, false ) );
	}

	public function getTrack1InvoiceCount() {
		return $this->m_intTrack1InvoiceCount;
	}

	public function sqlTrack1InvoiceCount() {
		return ( true == isset( $this->m_intTrack1InvoiceCount ) ) ? ( string ) $this->m_intTrack1InvoiceCount : '0';
	}

	public function setTrack1AmountUnsuccessful( $fltTrack1AmountUnsuccessful ) {
		$this->set( 'm_fltTrack1AmountUnsuccessful', CStrings::strToFloatDef( $fltTrack1AmountUnsuccessful, NULL, false, 2 ) );
	}

	public function getTrack1AmountUnsuccessful() {
		return $this->m_fltTrack1AmountUnsuccessful;
	}

	public function sqlTrack1AmountUnsuccessful() {
		return ( true == isset( $this->m_fltTrack1AmountUnsuccessful ) ) ? ( string ) $this->m_fltTrack1AmountUnsuccessful : '0';
	}

	public function setTrack2ChargesInPeriod( $fltTrack2ChargesInPeriod ) {
		$this->set( 'm_fltTrack2ChargesInPeriod', CStrings::strToFloatDef( $fltTrack2ChargesInPeriod, NULL, false, 2 ) );
	}

	public function getTrack2ChargesInPeriod() {
		return $this->m_fltTrack2ChargesInPeriod;
	}

	public function sqlTrack2ChargesInPeriod() {
		return ( true == isset( $this->m_fltTrack2ChargesInPeriod ) ) ? ( string ) $this->m_fltTrack2ChargesInPeriod : '0';
	}

	public function setTrack2PaymentsInPeriod( $fltTrack2PaymentsInPeriod ) {
		$this->set( 'm_fltTrack2PaymentsInPeriod', CStrings::strToFloatDef( $fltTrack2PaymentsInPeriod, NULL, false, 2 ) );
	}

	public function getTrack2PaymentsInPeriod() {
		return $this->m_fltTrack2PaymentsInPeriod;
	}

	public function sqlTrack2PaymentsInPeriod() {
		return ( true == isset( $this->m_fltTrack2PaymentsInPeriod ) ) ? ( string ) $this->m_fltTrack2PaymentsInPeriod : '0';
	}

	public function setTrack2InvoicesMailed( $intTrack2InvoicesMailed ) {
		$this->set( 'm_intTrack2InvoicesMailed', CStrings::strToIntDef( $intTrack2InvoicesMailed, NULL, false ) );
	}

	public function getTrack2InvoicesMailed() {
		return $this->m_intTrack2InvoicesMailed;
	}

	public function sqlTrack2InvoicesMailed() {
		return ( true == isset( $this->m_intTrack2InvoicesMailed ) ) ? ( string ) $this->m_intTrack2InvoicesMailed : '0';
	}

	public function setTrack2InvoicesEmailed( $intTrack2InvoicesEmailed ) {
		$this->set( 'm_intTrack2InvoicesEmailed', CStrings::strToIntDef( $intTrack2InvoicesEmailed, NULL, false ) );
	}

	public function getTrack2InvoicesEmailed() {
		return $this->m_intTrack2InvoicesEmailed;
	}

	public function sqlTrack2InvoicesEmailed() {
		return ( true == isset( $this->m_intTrack2InvoicesEmailed ) ) ? ( string ) $this->m_intTrack2InvoicesEmailed : '0';
	}

	public function setTrack2InvoicesFtpd( $intTrack2InvoicesFtpd ) {
		$this->set( 'm_intTrack2InvoicesFtpd', CStrings::strToIntDef( $intTrack2InvoicesFtpd, NULL, false ) );
	}

	public function getTrack2InvoicesFtpd() {
		return $this->m_intTrack2InvoicesFtpd;
	}

	public function sqlTrack2InvoicesFtpd() {
		return ( true == isset( $this->m_intTrack2InvoicesFtpd ) ) ? ( string ) $this->m_intTrack2InvoicesFtpd : '0';
	}

	public function setTrack2InvoiceCount( $intTrack2InvoiceCount ) {
		$this->set( 'm_intTrack2InvoiceCount', CStrings::strToIntDef( $intTrack2InvoiceCount, NULL, false ) );
	}

	public function getTrack2InvoiceCount() {
		return $this->m_intTrack2InvoiceCount;
	}

	public function sqlTrack2InvoiceCount() {
		return ( true == isset( $this->m_intTrack2InvoiceCount ) ) ? ( string ) $this->m_intTrack2InvoiceCount : '0';
	}

	public function setTrack2AmountUnsuccessful( $fltTrack2AmountUnsuccessful ) {
		$this->set( 'm_fltTrack2AmountUnsuccessful', CStrings::strToFloatDef( $fltTrack2AmountUnsuccessful, NULL, false, 2 ) );
	}

	public function getTrack2AmountUnsuccessful() {
		return $this->m_fltTrack2AmountUnsuccessful;
	}

	public function sqlTrack2AmountUnsuccessful() {
		return ( true == isset( $this->m_fltTrack2AmountUnsuccessful ) ) ? ( string ) $this->m_fltTrack2AmountUnsuccessful : '0';
	}

	public function setExportedBy( $intExportedBy ) {
		$this->set( 'm_intExportedBy', CStrings::strToIntDef( $intExportedBy, NULL, false ) );
	}

	public function getExportedBy() {
		return $this->m_intExportedBy;
	}

	public function sqlExportedBy() {
		return ( true == isset( $this->m_intExportedBy ) ) ? ( string ) $this->m_intExportedBy : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setInvoiceBatchProcessDetails( $jsonInvoiceBatchProcessDetails ) {
		if( true == valObj( $jsonInvoiceBatchProcessDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonInvoiceBatchProcessDetails', $jsonInvoiceBatchProcessDetails );
		} elseif( true == valJsonString( $jsonInvoiceBatchProcessDetails ) ) {
			$this->set( 'm_jsonInvoiceBatchProcessDetails', CStrings::strToJson( $jsonInvoiceBatchProcessDetails ) );
		} else {
			$this->set( 'm_jsonInvoiceBatchProcessDetails', NULL ); 
		}
		unset( $this->m_strInvoiceBatchProcessDetails );
	}

	public function getInvoiceBatchProcessDetails() {
		if( true == isset( $this->m_strInvoiceBatchProcessDetails ) ) {
			$this->m_jsonInvoiceBatchProcessDetails = CStrings::strToJson( $this->m_strInvoiceBatchProcessDetails );
			unset( $this->m_strInvoiceBatchProcessDetails );
		}
		return $this->m_jsonInvoiceBatchProcessDetails;
	}

	public function sqlInvoiceBatchProcessDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getInvoiceBatchProcessDetails() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getInvoiceBatchProcessDetails() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getInvoiceBatchProcessDetails() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, invoice_batch_step_id, export_batch_id, batch_datetime, track1_charges_in_period, track1_payments_in_period, track1_invoices_mailed, track1_invoices_emailed, track1_invoices_ftpd, track1_invoice_count, track1_amount_unsuccessful, track2_charges_in_period, track2_payments_in_period, track2_invoices_mailed, track2_invoices_emailed, track2_invoices_ftpd, track2_invoice_count, track2_amount_unsuccessful, exported_by, exported_on, invoice_batch_process_details, completed_by, completed_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlInvoiceBatchStepId() . ', ' .
						$this->sqlExportBatchId() . ', ' .
						$this->sqlBatchDatetime() . ', ' .
						$this->sqlTrack1ChargesInPeriod() . ', ' .
						$this->sqlTrack1PaymentsInPeriod() . ', ' .
						$this->sqlTrack1InvoicesMailed() . ', ' .
						$this->sqlTrack1InvoicesEmailed() . ', ' .
						$this->sqlTrack1InvoicesFtpd() . ', ' .
						$this->sqlTrack1InvoiceCount() . ', ' .
						$this->sqlTrack1AmountUnsuccessful() . ', ' .
						$this->sqlTrack2ChargesInPeriod() . ', ' .
						$this->sqlTrack2PaymentsInPeriod() . ', ' .
						$this->sqlTrack2InvoicesMailed() . ', ' .
						$this->sqlTrack2InvoicesEmailed() . ', ' .
						$this->sqlTrack2InvoicesFtpd() . ', ' .
						$this->sqlTrack2InvoiceCount() . ', ' .
						$this->sqlTrack2AmountUnsuccessful() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlInvoiceBatchProcessDetails() . ', ' .
						$this->sqlCompletedBy() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_batch_step_id = ' . $this->sqlInvoiceBatchStepId(). ',' ; } elseif( true == array_key_exists( 'InvoiceBatchStepId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_batch_step_id = ' . $this->sqlInvoiceBatchStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_batch_id = ' . $this->sqlExportBatchId(). ',' ; } elseif( true == array_key_exists( 'ExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' export_batch_id = ' . $this->sqlExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime(). ',' ; } elseif( true == array_key_exists( 'BatchDatetime', $this->getChangedColumns() ) ) { $strSql .= ' batch_datetime = ' . $this->sqlBatchDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track1_charges_in_period = ' . $this->sqlTrack1ChargesInPeriod(). ',' ; } elseif( true == array_key_exists( 'Track1ChargesInPeriod', $this->getChangedColumns() ) ) { $strSql .= ' track1_charges_in_period = ' . $this->sqlTrack1ChargesInPeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track1_payments_in_period = ' . $this->sqlTrack1PaymentsInPeriod(). ',' ; } elseif( true == array_key_exists( 'Track1PaymentsInPeriod', $this->getChangedColumns() ) ) { $strSql .= ' track1_payments_in_period = ' . $this->sqlTrack1PaymentsInPeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track1_invoices_mailed = ' . $this->sqlTrack1InvoicesMailed(). ',' ; } elseif( true == array_key_exists( 'Track1InvoicesMailed', $this->getChangedColumns() ) ) { $strSql .= ' track1_invoices_mailed = ' . $this->sqlTrack1InvoicesMailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track1_invoices_emailed = ' . $this->sqlTrack1InvoicesEmailed(). ',' ; } elseif( true == array_key_exists( 'Track1InvoicesEmailed', $this->getChangedColumns() ) ) { $strSql .= ' track1_invoices_emailed = ' . $this->sqlTrack1InvoicesEmailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track1_invoices_ftpd = ' . $this->sqlTrack1InvoicesFtpd(). ',' ; } elseif( true == array_key_exists( 'Track1InvoicesFtpd', $this->getChangedColumns() ) ) { $strSql .= ' track1_invoices_ftpd = ' . $this->sqlTrack1InvoicesFtpd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track1_invoice_count = ' . $this->sqlTrack1InvoiceCount(). ',' ; } elseif( true == array_key_exists( 'Track1InvoiceCount', $this->getChangedColumns() ) ) { $strSql .= ' track1_invoice_count = ' . $this->sqlTrack1InvoiceCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track1_amount_unsuccessful = ' . $this->sqlTrack1AmountUnsuccessful(). ',' ; } elseif( true == array_key_exists( 'Track1AmountUnsuccessful', $this->getChangedColumns() ) ) { $strSql .= ' track1_amount_unsuccessful = ' . $this->sqlTrack1AmountUnsuccessful() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track2_charges_in_period = ' . $this->sqlTrack2ChargesInPeriod(). ',' ; } elseif( true == array_key_exists( 'Track2ChargesInPeriod', $this->getChangedColumns() ) ) { $strSql .= ' track2_charges_in_period = ' . $this->sqlTrack2ChargesInPeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track2_payments_in_period = ' . $this->sqlTrack2PaymentsInPeriod(). ',' ; } elseif( true == array_key_exists( 'Track2PaymentsInPeriod', $this->getChangedColumns() ) ) { $strSql .= ' track2_payments_in_period = ' . $this->sqlTrack2PaymentsInPeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track2_invoices_mailed = ' . $this->sqlTrack2InvoicesMailed(). ',' ; } elseif( true == array_key_exists( 'Track2InvoicesMailed', $this->getChangedColumns() ) ) { $strSql .= ' track2_invoices_mailed = ' . $this->sqlTrack2InvoicesMailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track2_invoices_emailed = ' . $this->sqlTrack2InvoicesEmailed(). ',' ; } elseif( true == array_key_exists( 'Track2InvoicesEmailed', $this->getChangedColumns() ) ) { $strSql .= ' track2_invoices_emailed = ' . $this->sqlTrack2InvoicesEmailed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track2_invoices_ftpd = ' . $this->sqlTrack2InvoicesFtpd(). ',' ; } elseif( true == array_key_exists( 'Track2InvoicesFtpd', $this->getChangedColumns() ) ) { $strSql .= ' track2_invoices_ftpd = ' . $this->sqlTrack2InvoicesFtpd() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track2_invoice_count = ' . $this->sqlTrack2InvoiceCount(). ',' ; } elseif( true == array_key_exists( 'Track2InvoiceCount', $this->getChangedColumns() ) ) { $strSql .= ' track2_invoice_count = ' . $this->sqlTrack2InvoiceCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track2_amount_unsuccessful = ' . $this->sqlTrack2AmountUnsuccessful(). ',' ; } elseif( true == array_key_exists( 'Track2AmountUnsuccessful', $this->getChangedColumns() ) ) { $strSql .= ' track2_amount_unsuccessful = ' . $this->sqlTrack2AmountUnsuccessful() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy(). ',' ; } elseif( true == array_key_exists( 'ExportedBy', $this->getChangedColumns() ) ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_batch_process_details = ' . $this->sqlInvoiceBatchProcessDetails(). ',' ; } elseif( true == array_key_exists( 'InvoiceBatchProcessDetails', $this->getChangedColumns() ) ) { $strSql .= ' invoice_batch_process_details = ' . $this->sqlInvoiceBatchProcessDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'invoice_batch_step_id' => $this->getInvoiceBatchStepId(),
			'export_batch_id' => $this->getExportBatchId(),
			'batch_datetime' => $this->getBatchDatetime(),
			'track1_charges_in_period' => $this->getTrack1ChargesInPeriod(),
			'track1_payments_in_period' => $this->getTrack1PaymentsInPeriod(),
			'track1_invoices_mailed' => $this->getTrack1InvoicesMailed(),
			'track1_invoices_emailed' => $this->getTrack1InvoicesEmailed(),
			'track1_invoices_ftpd' => $this->getTrack1InvoicesFtpd(),
			'track1_invoice_count' => $this->getTrack1InvoiceCount(),
			'track1_amount_unsuccessful' => $this->getTrack1AmountUnsuccessful(),
			'track2_charges_in_period' => $this->getTrack2ChargesInPeriod(),
			'track2_payments_in_period' => $this->getTrack2PaymentsInPeriod(),
			'track2_invoices_mailed' => $this->getTrack2InvoicesMailed(),
			'track2_invoices_emailed' => $this->getTrack2InvoicesEmailed(),
			'track2_invoices_ftpd' => $this->getTrack2InvoicesFtpd(),
			'track2_invoice_count' => $this->getTrack2InvoiceCount(),
			'track2_amount_unsuccessful' => $this->getTrack2AmountUnsuccessful(),
			'exported_by' => $this->getExportedBy(),
			'exported_on' => $this->getExportedOn(),
			'invoice_batch_process_details' => $this->getInvoiceBatchProcessDetails(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>