<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingSessionNotes
 * Do not add any new functions to this class.
 */

class CBaseTrainingSessionNotes extends CEosPluralBase {

	/**
	 * @return CTrainingSessionNote[]
	 */
	public static function fetchTrainingSessionNotes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTrainingSessionNote', $objDatabase );
	}

	/**
	 * @return CTrainingSessionNote
	 */
	public static function fetchTrainingSessionNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTrainingSessionNote', $objDatabase );
	}

	public static function fetchTrainingSessionNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'training_session_notes', $objDatabase );
	}

	public static function fetchTrainingSessionNoteById( $intId, $objDatabase ) {
		return self::fetchTrainingSessionNote( sprintf( 'SELECT * FROM training_session_notes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTrainingSessionNotesByTrainingSessionId( $intTrainingSessionId, $objDatabase ) {
		return self::fetchTrainingSessionNotes( sprintf( 'SELECT * FROM training_session_notes WHERE training_session_id = %d', ( int ) $intTrainingSessionId ), $objDatabase );
	}

}
?>