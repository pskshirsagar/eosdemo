<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskUserSettings
 * Do not add any new functions to this class.
 */

class CBaseTaskUserSettings extends CEosPluralBase {

	/**
	 * @return CTaskUserSetting[]
	 */
	public static function fetchTaskUserSettings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTaskUserSetting::class, $objDatabase );
	}

	/**
	 * @return CTaskUserSetting
	 */
	public static function fetchTaskUserSetting( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaskUserSetting::class, $objDatabase );
	}

	public static function fetchTaskUserSettingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_user_settings', $objDatabase );
	}

	public static function fetchTaskUserSettingById( $intId, $objDatabase ) {
		return self::fetchTaskUserSetting( sprintf( 'SELECT * FROM task_user_settings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskUserSettingsByUserId( $intUserId, $objDatabase ) {
		return self::fetchTaskUserSettings( sprintf( 'SELECT * FROM task_user_settings WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

}
?>