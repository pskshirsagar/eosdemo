<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CInductionTypes
 * Do not add any new functions to this class.
 */

class CBaseInductionTypes extends CEosPluralBase {

	/**
	 * @return CInductionType[]
	 */
	public static function fetchInductionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInductionType', $objDatabase );
	}

	/**
	 * @return CInductionType
	 */
	public static function fetchInductionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInductionType', $objDatabase );
	}

	public static function fetchInductionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'induction_types', $objDatabase );
	}

	public static function fetchInductionTypeById( $intId, $objDatabase ) {
		return self::fetchInductionType( sprintf( 'SELECT * FROM induction_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>