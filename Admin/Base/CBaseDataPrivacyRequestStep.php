<?php

class CBaseDataPrivacyRequestStep extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.data_privacy_request_steps';

	protected $m_intId;
	protected $m_intDataPrivacyRequestId;
	protected $m_intDataPrivacyStepId;
	protected $m_strNotes;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intOrderNumber;
	protected $m_strCategory;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['data_privacy_request_id'] ) && $boolDirectSet ) $this->set( 'm_intDataPrivacyRequestId', trim( $arrValues['data_privacy_request_id'] ) ); elseif( isset( $arrValues['data_privacy_request_id'] ) ) $this->setDataPrivacyRequestId( $arrValues['data_privacy_request_id'] );
		if( isset( $arrValues['data_privacy_step_id'] ) && $boolDirectSet ) $this->set( 'm_intDataPrivacyStepId', trim( $arrValues['data_privacy_step_id'] ) ); elseif( isset( $arrValues['data_privacy_step_id'] ) ) $this->setDataPrivacyStepId( $arrValues['data_privacy_step_id'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['order_number'] ) && $boolDirectSet ) $this->set( 'm_intOrderNumber', trim( $arrValues['order_number'] ) ); elseif( isset( $arrValues['order_number'] ) ) $this->setOrderNumber( $arrValues['order_number'] );
		if( isset( $arrValues['category'] ) && $boolDirectSet ) $this->set( 'm_strCategory', trim( $arrValues['category'] ) ); elseif( isset( $arrValues['category'] ) ) $this->setCategory( $arrValues['category'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDataPrivacyRequestId( $intDataPrivacyRequestId ) {
		$this->set( 'm_intDataPrivacyRequestId', CStrings::strToIntDef( $intDataPrivacyRequestId, NULL, false ) );
	}

	public function getDataPrivacyRequestId() {
		return $this->m_intDataPrivacyRequestId;
	}

	public function sqlDataPrivacyRequestId() {
		return ( true == isset( $this->m_intDataPrivacyRequestId ) ) ? ( string ) $this->m_intDataPrivacyRequestId : 'NULL';
	}

	public function setDataPrivacyStepId( $intDataPrivacyStepId ) {
		$this->set( 'm_intDataPrivacyStepId', CStrings::strToIntDef( $intDataPrivacyStepId, NULL, false ) );
	}

	public function getDataPrivacyStepId() {
		return $this->m_intDataPrivacyStepId;
	}

	public function sqlDataPrivacyStepId() {
		return ( true == isset( $this->m_intDataPrivacyStepId ) ) ? ( string ) $this->m_intDataPrivacyStepId : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOrderNumber( $intOrderNumber ) {
		$this->set( 'm_intOrderNumber', CStrings::strToIntDef( $intOrderNumber, NULL, false ) );
	}

	public function getOrderNumber() {
		return $this->m_intOrderNumber;
	}

	public function sqlOrderNumber() {
		return ( true == isset( $this->m_intOrderNumber ) ) ? ( string ) $this->m_intOrderNumber : 'NULL';
	}

	public function setCategory( $strCategory ) {
		$this->set( 'm_strCategory', CStrings::strTrimDef( $strCategory, 50, NULL, true ) );
	}

	public function getCategory() {
		return $this->m_strCategory;
	}

	public function sqlCategory() {
		return ( true == isset( $this->m_strCategory ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCategory ) : '\'' . addslashes( $this->m_strCategory ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, data_privacy_request_id, data_privacy_step_id, notes, details, completed_by, completed_on, updated_by, updated_on, created_by, created_on, order_number, category )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDataPrivacyRequestId() . ', ' .
						$this->sqlDataPrivacyStepId() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCompletedBy() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlOrderNumber() . ', ' .
						$this->sqlCategory() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_privacy_request_id = ' . $this->sqlDataPrivacyRequestId(). ',' ; } elseif( true == array_key_exists( 'DataPrivacyRequestId', $this->getChangedColumns() ) ) { $strSql .= ' data_privacy_request_id = ' . $this->sqlDataPrivacyRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_privacy_step_id = ' . $this->sqlDataPrivacyStepId(). ',' ; } elseif( true == array_key_exists( 'DataPrivacyStepId', $this->getChangedColumns() ) ) { $strSql .= ' data_privacy_step_id = ' . $this->sqlDataPrivacyStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_number = ' . $this->sqlOrderNumber(). ',' ; } elseif( true == array_key_exists( 'OrderNumber', $this->getChangedColumns() ) ) { $strSql .= ' order_number = ' . $this->sqlOrderNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' category = ' . $this->sqlCategory(). ',' ; } elseif( true == array_key_exists( 'Category', $this->getChangedColumns() ) ) { $strSql .= ' category = ' . $this->sqlCategory() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'data_privacy_request_id' => $this->getDataPrivacyRequestId(),
			'data_privacy_step_id' => $this->getDataPrivacyStepId(),
			'notes' => $this->getNotes(),
			'details' => $this->getDetails(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'order_number' => $this->getOrderNumber(),
			'category' => $this->getCategory()
		);
	}

}
?>