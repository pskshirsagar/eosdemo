<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeEncryptionAssociations
 * Do not add any new functions to this class.
 */

class CBaseEmployeeEncryptionAssociations extends CEosPluralBase {

	/**
	 * @return CEmployeeEncryptionAssociation[]
	 */
	public static function fetchEmployeeEncryptionAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeEncryptionAssociation', $objDatabase );
	}

	/**
	 * @return CEmployeeEncryptionAssociation
	 */
	public static function fetchEmployeeEncryptionAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeEncryptionAssociation', $objDatabase );
	}

	public static function fetchEmployeeEncryptionAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_encryption_associations', $objDatabase );
	}

	public static function fetchEmployeeEncryptionAssociationById( $intId, $objDatabase ) {
		return self::fetchEmployeeEncryptionAssociation( sprintf( 'SELECT * FROM employee_encryption_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeEncryptionAssociationsByViewerEmployeeId( $intViewerEmployeeId, $objDatabase ) {
		return self::fetchEmployeeEncryptionAssociations( sprintf( 'SELECT * FROM employee_encryption_associations WHERE viewer_employee_id = %d', ( int ) $intViewerEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeEncryptionAssociationsByReferenceId( $intReferenceId, $objDatabase ) {
		return self::fetchEmployeeEncryptionAssociations( sprintf( 'SELECT * FROM employee_encryption_associations WHERE reference_id = %d', ( int ) $intReferenceId ), $objDatabase );
	}

	public static function fetchEmployeeEncryptionAssociationsByEncryptionSystemTypeId( $intEncryptionSystemTypeId, $objDatabase ) {
		return self::fetchEmployeeEncryptionAssociations( sprintf( 'SELECT * FROM employee_encryption_associations WHERE encryption_system_type_id = %d', ( int ) $intEncryptionSystemTypeId ), $objDatabase );
	}

}
?>