<?php

class CBaseUserRepositoryPermission extends CEosSingularBase {

	const TABLE_NAME = 'public.user_repository_permissions';

	protected $m_intId;
	protected $m_intUserId;
	protected $m_intRepositoryId;
	protected $m_intIsRead;
	protected $m_intIsWrite;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsRead = '0';
		$this->m_intIsWrite = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['repository_id'] ) && $boolDirectSet ) $this->set( 'm_intRepositoryId', trim( $arrValues['repository_id'] ) ); elseif( isset( $arrValues['repository_id'] ) ) $this->setRepositoryId( $arrValues['repository_id'] );
		if( isset( $arrValues['is_read'] ) && $boolDirectSet ) $this->set( 'm_intIsRead', trim( $arrValues['is_read'] ) ); elseif( isset( $arrValues['is_read'] ) ) $this->setIsRead( $arrValues['is_read'] );
		if( isset( $arrValues['is_write'] ) && $boolDirectSet ) $this->set( 'm_intIsWrite', trim( $arrValues['is_write'] ) ); elseif( isset( $arrValues['is_write'] ) ) $this->setIsWrite( $arrValues['is_write'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setRepositoryId( $intRepositoryId ) {
		$this->set( 'm_intRepositoryId', CStrings::strToIntDef( $intRepositoryId, NULL, false ) );
	}

	public function getRepositoryId() {
		return $this->m_intRepositoryId;
	}

	public function sqlRepositoryId() {
		return ( true == isset( $this->m_intRepositoryId ) ) ? ( string ) $this->m_intRepositoryId : 'NULL';
	}

	public function setIsRead( $intIsRead ) {
		$this->set( 'm_intIsRead', CStrings::strToIntDef( $intIsRead, NULL, false ) );
	}

	public function getIsRead() {
		return $this->m_intIsRead;
	}

	public function sqlIsRead() {
		return ( true == isset( $this->m_intIsRead ) ) ? ( string ) $this->m_intIsRead : '0';
	}

	public function setIsWrite( $intIsWrite ) {
		$this->set( 'm_intIsWrite', CStrings::strToIntDef( $intIsWrite, NULL, false ) );
	}

	public function getIsWrite() {
		return $this->m_intIsWrite;
	}

	public function sqlIsWrite() {
		return ( true == isset( $this->m_intIsWrite ) ) ? ( string ) $this->m_intIsWrite : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, user_id, repository_id, is_read, is_write, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlUserId() . ', ' .
 						$this->sqlRepositoryId() . ', ' .
 						$this->sqlIsRead() . ', ' .
 						$this->sqlIsWrite() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' repository_id = ' . $this->sqlRepositoryId() . ','; } elseif( true == array_key_exists( 'RepositoryId', $this->getChangedColumns() ) ) { $strSql .= ' repository_id = ' . $this->sqlRepositoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_read = ' . $this->sqlIsRead() . ','; } elseif( true == array_key_exists( 'IsRead', $this->getChangedColumns() ) ) { $strSql .= ' is_read = ' . $this->sqlIsRead() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_write = ' . $this->sqlIsWrite() . ','; } elseif( true == array_key_exists( 'IsWrite', $this->getChangedColumns() ) ) { $strSql .= ' is_write = ' . $this->sqlIsWrite() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'user_id' => $this->getUserId(),
			'repository_id' => $this->getRepositoryId(),
			'is_read' => $this->getIsRead(),
			'is_write' => $this->getIsWrite(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>