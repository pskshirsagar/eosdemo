<?php

class CBaseMigrationWorkbookComment extends CEosSingularBase {

	const TABLE_NAME = 'public.migration_workbook_comments';

	protected $m_intId;
	protected $m_intMigrationWorkbookId;
	protected $m_intMigrationWorkbookKeyId;
	protected $m_intCompanyUserId;
	protected $m_strComment;
	protected $m_boolIsClientVisible;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strStepName;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsClientVisible = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['migration_workbook_id'] ) && $boolDirectSet ) $this->set( 'm_intMigrationWorkbookId', trim( $arrValues['migration_workbook_id'] ) ); elseif( isset( $arrValues['migration_workbook_id'] ) ) $this->setMigrationWorkbookId( $arrValues['migration_workbook_id'] );
		if( isset( $arrValues['migration_workbook_key_id'] ) && $boolDirectSet ) $this->set( 'm_intMigrationWorkbookKeyId', trim( $arrValues['migration_workbook_key_id'] ) ); elseif( isset( $arrValues['migration_workbook_key_id'] ) ) $this->setMigrationWorkbookKeyId( $arrValues['migration_workbook_key_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['comment'] ) && $boolDirectSet ) $this->set( 'm_strComment', trim( stripcslashes( $arrValues['comment'] ) ) ); elseif( isset( $arrValues['comment'] ) ) $this->setComment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['comment'] ) : $arrValues['comment'] );
		if( isset( $arrValues['is_client_visible'] ) && $boolDirectSet ) $this->set( 'm_boolIsClientVisible', trim( stripcslashes( $arrValues['is_client_visible'] ) ) ); elseif( isset( $arrValues['is_client_visible'] ) ) $this->setIsClientVisible( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_client_visible'] ) : $arrValues['is_client_visible'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['step_name'] ) && $boolDirectSet ) $this->set( 'm_strStepName', trim( stripcslashes( $arrValues['step_name'] ) ) ); elseif( isset( $arrValues['step_name'] ) ) $this->setStepName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['step_name'] ) : $arrValues['step_name'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMigrationWorkbookId( $intMigrationWorkbookId ) {
		$this->set( 'm_intMigrationWorkbookId', CStrings::strToIntDef( $intMigrationWorkbookId, NULL, false ) );
	}

	public function getMigrationWorkbookId() {
		return $this->m_intMigrationWorkbookId;
	}

	public function sqlMigrationWorkbookId() {
		return ( true == isset( $this->m_intMigrationWorkbookId ) ) ? ( string ) $this->m_intMigrationWorkbookId : 'NULL';
	}

	public function setMigrationWorkbookKeyId( $intMigrationWorkbookKeyId ) {
		$this->set( 'm_intMigrationWorkbookKeyId', CStrings::strToIntDef( $intMigrationWorkbookKeyId, NULL, false ) );
	}

	public function getMigrationWorkbookKeyId() {
		return $this->m_intMigrationWorkbookKeyId;
	}

	public function sqlMigrationWorkbookKeyId() {
		return ( true == isset( $this->m_intMigrationWorkbookKeyId ) ) ? ( string ) $this->m_intMigrationWorkbookKeyId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setComment( $strComment ) {
		$this->set( 'm_strComment', CStrings::strTrimDef( $strComment, -1, NULL, true ) );
	}

	public function getComment() {
		return $this->m_strComment;
	}

	public function sqlComment() {
		return ( true == isset( $this->m_strComment ) ) ? '\'' . addslashes( $this->m_strComment ) . '\'' : 'NULL';
	}

	public function setIsClientVisible( $boolIsClientVisible ) {
		$this->set( 'm_boolIsClientVisible', CStrings::strToBool( $boolIsClientVisible ) );
	}

	public function getIsClientVisible() {
		return $this->m_boolIsClientVisible;
	}

	public function sqlIsClientVisible() {
		return ( true == isset( $this->m_boolIsClientVisible ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsClientVisible ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setStepName( $strStepName ) {
		$this->set( 'm_strStepName', CStrings::strTrimDef( $strStepName, 100, NULL, true ) );
	}

	public function getStepName() {
		return $this->m_strStepName;
	}

	public function sqlStepName() {
		return ( true == isset( $this->m_strStepName ) ) ? '\'' . addslashes( $this->m_strStepName ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, migration_workbook_id, migration_workbook_key_id, company_user_id, comment, is_client_visible, updated_by, updated_on, created_by, created_on, step_name )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlMigrationWorkbookId() . ', ' .
						$this->sqlMigrationWorkbookKeyId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlComment() . ', ' .
						$this->sqlIsClientVisible() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlStepName() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' migration_workbook_id = ' . $this->sqlMigrationWorkbookId(). ',' ; } elseif( true == array_key_exists( 'MigrationWorkbookId', $this->getChangedColumns() ) ) { $strSql .= ' migration_workbook_id = ' . $this->sqlMigrationWorkbookId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' migration_workbook_key_id = ' . $this->sqlMigrationWorkbookKeyId(). ',' ; } elseif( true == array_key_exists( 'MigrationWorkbookKeyId', $this->getChangedColumns() ) ) { $strSql .= ' migration_workbook_key_id = ' . $this->sqlMigrationWorkbookKeyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comment = ' . $this->sqlComment(). ',' ; } elseif( true == array_key_exists( 'Comment', $this->getChangedColumns() ) ) { $strSql .= ' comment = ' . $this->sqlComment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_visible = ' . $this->sqlIsClientVisible(). ',' ; } elseif( true == array_key_exists( 'IsClientVisible', $this->getChangedColumns() ) ) { $strSql .= ' is_client_visible = ' . $this->sqlIsClientVisible() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' step_name = ' . $this->sqlStepName(). ',' ; } elseif( true == array_key_exists( 'StepName', $this->getChangedColumns() ) ) { $strSql .= ' step_name = ' . $this->sqlStepName() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'migration_workbook_id' => $this->getMigrationWorkbookId(),
			'migration_workbook_key_id' => $this->getMigrationWorkbookKeyId(),
			'company_user_id' => $this->getCompanyUserId(),
			'comment' => $this->getComment(),
			'is_client_visible' => $this->getIsClientVisible(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'step_name' => $this->getStepName()
		);
	}

}
?>