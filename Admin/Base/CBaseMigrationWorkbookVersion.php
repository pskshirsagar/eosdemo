<?php

class CBaseMigrationWorkbookVersion extends CEosSingularBase {

	const TABLE_NAME = 'public.migration_workbook_versions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intMigrationWorkbookId;
	protected $m_strStepName;
	protected $m_strVersionDataValues;
	protected $m_jsonVersionDataValues;
	protected $m_strVersionCustomMapping;
	protected $m_jsonVersionCustomMapping;
	protected $m_boolIsActive;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['migration_workbook_id'] ) && $boolDirectSet ) $this->set( 'm_intMigrationWorkbookId', trim( $arrValues['migration_workbook_id'] ) ); elseif( isset( $arrValues['migration_workbook_id'] ) ) $this->setMigrationWorkbookId( $arrValues['migration_workbook_id'] );
		if( isset( $arrValues['step_name'] ) && $boolDirectSet ) $this->set( 'm_strStepName', trim( stripcslashes( $arrValues['step_name'] ) ) ); elseif( isset( $arrValues['step_name'] ) ) $this->setStepName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['step_name'] ) : $arrValues['step_name'] );
		if( isset( $arrValues['version_data_values'] ) ) $this->set( 'm_strVersionDataValues', trim( $arrValues['version_data_values'] ) );
		if( isset( $arrValues['version_custom_mapping'] ) ) $this->set( 'm_strVersionCustomMapping', trim( $arrValues['version_custom_mapping'] ) );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setMigrationWorkbookId( $intMigrationWorkbookId ) {
		$this->set( 'm_intMigrationWorkbookId', CStrings::strToIntDef( $intMigrationWorkbookId, NULL, false ) );
	}

	public function getMigrationWorkbookId() {
		return $this->m_intMigrationWorkbookId;
	}

	public function sqlMigrationWorkbookId() {
		return ( true == isset( $this->m_intMigrationWorkbookId ) ) ? ( string ) $this->m_intMigrationWorkbookId : 'NULL';
	}

	public function setStepName( $strStepName ) {
		$this->set( 'm_strStepName', CStrings::strTrimDef( $strStepName, 100, NULL, true ) );
	}

	public function getStepName() {
		return $this->m_strStepName;
	}

	public function sqlStepName() {
		return ( true == isset( $this->m_strStepName ) ) ? '\'' . addslashes( $this->m_strStepName ) . '\'' : 'NULL';
	}

	public function setVersionDataValues( $jsonVersionDataValues ) {
		if( true == valObj( $jsonVersionDataValues, 'stdClass' ) ) {
			$this->set( 'm_jsonVersionDataValues', $jsonVersionDataValues );
		} elseif( true == valJsonString( $jsonVersionDataValues ) ) {
			$this->set( 'm_jsonVersionDataValues', CStrings::strToJson( $jsonVersionDataValues ) );
		} else {
			$this->set( 'm_jsonVersionDataValues', NULL ); 
		}
		unset( $this->m_strVersionDataValues );
	}

	public function getVersionDataValues() {
		if( true == isset( $this->m_strVersionDataValues ) ) {
			$this->m_jsonVersionDataValues = CStrings::strToJson( $this->m_strVersionDataValues );
			unset( $this->m_strVersionDataValues );
		}
		return $this->m_jsonVersionDataValues;
	}

	public function sqlVersionDataValues() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getVersionDataValues() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getVersionDataValues() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setVersionCustomMapping( $jsonVersionCustomMapping ) {
		if( true == valObj( $jsonVersionCustomMapping, 'stdClass' ) ) {
			$this->set( 'm_jsonVersionCustomMapping', $jsonVersionCustomMapping );
		} elseif( true == valJsonString( $jsonVersionCustomMapping ) ) {
			$this->set( 'm_jsonVersionCustomMapping', CStrings::strToJson( $jsonVersionCustomMapping ) );
		} else {
			$this->set( 'm_jsonVersionCustomMapping', NULL ); 
		}
		unset( $this->m_strVersionCustomMapping );
	}

	public function getVersionCustomMapping() {
		if( true == isset( $this->m_strVersionCustomMapping ) ) {
			$this->m_jsonVersionCustomMapping = CStrings::strToJson( $this->m_strVersionCustomMapping );
			unset( $this->m_strVersionCustomMapping );
		}
		return $this->m_jsonVersionCustomMapping;
	}

	public function sqlVersionCustomMapping() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getVersionCustomMapping() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getVersionCustomMapping() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, migration_workbook_id, step_name, version_data_values, version_custom_mapping, is_active, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlMigrationWorkbookId() . ', ' .
 						$this->sqlStepName() . ', ' .
 						$this->sqlVersionDataValues() . ', ' .
 						$this->sqlVersionCustomMapping() . ', ' .
 						$this->sqlIsActive() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' migration_workbook_id = ' . $this->sqlMigrationWorkbookId() . ','; } elseif( true == array_key_exists( 'MigrationWorkbookId', $this->getChangedColumns() ) ) { $strSql .= ' migration_workbook_id = ' . $this->sqlMigrationWorkbookId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' step_name = ' . $this->sqlStepName() . ','; } elseif( true == array_key_exists( 'StepName', $this->getChangedColumns() ) ) { $strSql .= ' step_name = ' . $this->sqlStepName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' version_data_values = ' . $this->sqlVersionDataValues() . ','; } elseif( true == array_key_exists( 'VersionDataValues', $this->getChangedColumns() ) ) { $strSql .= ' version_data_values = ' . $this->sqlVersionDataValues() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' version_custom_mapping = ' . $this->sqlVersionCustomMapping() . ','; } elseif( true == array_key_exists( 'VersionCustomMapping', $this->getChangedColumns() ) ) { $strSql .= ' version_custom_mapping = ' . $this->sqlVersionCustomMapping() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'migration_workbook_id' => $this->getMigrationWorkbookId(),
			'step_name' => $this->getStepName(),
			'version_data_values' => $this->getVersionDataValues(),
			'version_custom_mapping' => $this->getVersionCustomMapping(),
			'is_active' => $this->getIsActive(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>