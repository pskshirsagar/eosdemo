<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CRequirementTypes
 * Do not add any new functions to this class.
 */

class CBaseRequirementTypes extends CEosPluralBase {

	/**
	 * @return CRequirementType[]
	 */
	public static function fetchRequirementTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CRequirementType', $objDatabase );
	}

	/**
	 * @return CRequirementType
	 */
	public static function fetchRequirementType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRequirementType', $objDatabase );
	}

	public static function fetchRequirementTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'requirement_types', $objDatabase );
	}

	public static function fetchRequirementTypeById( $intId, $objDatabase ) {
		return self::fetchRequirementType( sprintf( 'SELECT * FROM requirement_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>