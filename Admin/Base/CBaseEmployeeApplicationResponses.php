<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationResponses
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationResponses extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationResponse[]
	 */
	public static function fetchEmployeeApplicationResponses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationResponse', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationResponse
	 */
	public static function fetchEmployeeApplicationResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationResponse', $objDatabase );
	}

	public static function fetchEmployeeApplicationResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_responses', $objDatabase );
	}

	public static function fetchEmployeeApplicationResponseById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationResponse( sprintf( 'SELECT * FROM employee_application_responses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationResponsesByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchEmployeeApplicationResponses( sprintf( 'SELECT * FROM employee_application_responses WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationResponsesByEmploymentApplicationQuestionId( $intEmploymentApplicationQuestionId, $objDatabase ) {
		return self::fetchEmployeeApplicationResponses( sprintf( 'SELECT * FROM employee_application_responses WHERE employment_application_question_id = %d', ( int ) $intEmploymentApplicationQuestionId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationResponsesByEmploymentApplicationId( $intEmploymentApplicationId, $objDatabase ) {
		return self::fetchEmployeeApplicationResponses( sprintf( 'SELECT * FROM employee_application_responses WHERE employment_application_id = %d', ( int ) $intEmploymentApplicationId ), $objDatabase );
	}

}
?>