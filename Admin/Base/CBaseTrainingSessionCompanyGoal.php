<?php

class CBaseTrainingSessionCompanyGoal extends CEosSingularBase {

	const TABLE_NAME = 'public.training_session_company_goals';

	protected $m_intId;
	protected $m_intTrainingSessionId;
	protected $m_intTrainingId;
	protected $m_intCompanyGoalId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['training_session_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingSessionId', trim( $arrValues['training_session_id'] ) ); elseif( isset( $arrValues['training_session_id'] ) ) $this->setTrainingSessionId( $arrValues['training_session_id'] );
		if( isset( $arrValues['training_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingId', trim( $arrValues['training_id'] ) ); elseif( isset( $arrValues['training_id'] ) ) $this->setTrainingId( $arrValues['training_id'] );
		if( isset( $arrValues['company_goal_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyGoalId', trim( $arrValues['company_goal_id'] ) ); elseif( isset( $arrValues['company_goal_id'] ) ) $this->setCompanyGoalId( $arrValues['company_goal_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTrainingSessionId( $intTrainingSessionId ) {
		$this->set( 'm_intTrainingSessionId', CStrings::strToIntDef( $intTrainingSessionId, NULL, false ) );
	}

	public function getTrainingSessionId() {
		return $this->m_intTrainingSessionId;
	}

	public function sqlTrainingSessionId() {
		return ( true == isset( $this->m_intTrainingSessionId ) ) ? ( string ) $this->m_intTrainingSessionId : 'NULL';
	}

	public function setTrainingId( $intTrainingId ) {
		$this->set( 'm_intTrainingId', CStrings::strToIntDef( $intTrainingId, NULL, false ) );
	}

	public function getTrainingId() {
		return $this->m_intTrainingId;
	}

	public function sqlTrainingId() {
		return ( true == isset( $this->m_intTrainingId ) ) ? ( string ) $this->m_intTrainingId : 'NULL';
	}

	public function setCompanyGoalId( $intCompanyGoalId ) {
		$this->set( 'm_intCompanyGoalId', CStrings::strToIntDef( $intCompanyGoalId, NULL, false ) );
	}

	public function getCompanyGoalId() {
		return $this->m_intCompanyGoalId;
	}

	public function sqlCompanyGoalId() {
		return ( true == isset( $this->m_intCompanyGoalId ) ) ? ( string ) $this->m_intCompanyGoalId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, training_session_id, training_id, company_goal_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTrainingSessionId() . ', ' .
 						$this->sqlTrainingId() . ', ' .
 						$this->sqlCompanyGoalId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_session_id = ' . $this->sqlTrainingSessionId() . ','; } elseif( true == array_key_exists( 'TrainingSessionId', $this->getChangedColumns() ) ) { $strSql .= ' training_session_id = ' . $this->sqlTrainingSessionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_id = ' . $this->sqlTrainingId() . ','; } elseif( true == array_key_exists( 'TrainingId', $this->getChangedColumns() ) ) { $strSql .= ' training_id = ' . $this->sqlTrainingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_goal_id = ' . $this->sqlCompanyGoalId() . ','; } elseif( true == array_key_exists( 'CompanyGoalId', $this->getChangedColumns() ) ) { $strSql .= ' company_goal_id = ' . $this->sqlCompanyGoalId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'training_session_id' => $this->getTrainingSessionId(),
			'training_id' => $this->getTrainingId(),
			'company_goal_id' => $this->getCompanyGoalId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>