<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseSemStatusTypes extends CEosPluralBase {

	/**
	 * @return CSemStatusType[]
	 */
	public static function fetchSemStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemStatusType', $objDatabase );
	}

	/**
	 * @return CSemStatusType
	 */
	public static function fetchSemStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemStatusType', $objDatabase );
	}

	public static function fetchSemStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_status_types', $objDatabase );
	}

	public static function fetchSemStatusTypeById( $intId, $objDatabase ) {
		return self::fetchSemStatusType( sprintf( 'SELECT * FROM sem_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>