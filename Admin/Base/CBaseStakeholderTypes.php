<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStakeholderTypes
 * Do not add any new functions to this class.
 */

class CBaseStakeholderTypes extends CEosPluralBase {

	/**
	 * @return CStakeholderType[]
	 */
	public static function fetchStakeholderTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStakeholderType', $objDatabase );
	}

	/**
	 * @return CStakeholderType
	 */
	public static function fetchStakeholderType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStakeholderType', $objDatabase );
	}

	public static function fetchStakeholderTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stakeholder_types', $objDatabase );
	}

	public static function fetchStakeholderTypeById( $intId, $objDatabase ) {
		return self::fetchStakeholderType( sprintf( 'SELECT * FROM stakeholder_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>