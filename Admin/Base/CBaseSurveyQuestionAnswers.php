<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyQuestionAnswers
 * Do not add any new functions to this class.
 */

class CBaseSurveyQuestionAnswers extends CEosPluralBase {

	/**
	 * @return CSurveyQuestionAnswer[]
	 */
	public static function fetchSurveyQuestionAnswers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyQuestionAnswer', $objDatabase );
	}

	/**
	 * @return CSurveyQuestionAnswer
	 */
	public static function fetchSurveyQuestionAnswer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyQuestionAnswer', $objDatabase );
	}

	public static function fetchSurveyQuestionAnswerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_question_answers', $objDatabase );
	}

	public static function fetchSurveyQuestionAnswerById( $intId, $objDatabase ) {
		return self::fetchSurveyQuestionAnswer( sprintf( 'SELECT * FROM survey_question_answers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveyQuestionAnswersBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		return self::fetchSurveyQuestionAnswers( sprintf( 'SELECT * FROM survey_question_answers WHERE survey_template_id = %d', ( int ) $intSurveyTemplateId ), $objDatabase );
	}

	public static function fetchSurveyQuestionAnswersBySurveyId( $intSurveyId, $objDatabase ) {
		return self::fetchSurveyQuestionAnswers( sprintf( 'SELECT * FROM survey_question_answers WHERE survey_id = %d', ( int ) $intSurveyId ), $objDatabase );
	}

	public static function fetchSurveyQuestionAnswersBySurveyTemplateQuestionGroupId( $intSurveyTemplateQuestionGroupId, $objDatabase ) {
		return self::fetchSurveyQuestionAnswers( sprintf( 'SELECT * FROM survey_question_answers WHERE survey_template_question_group_id = %d', ( int ) $intSurveyTemplateQuestionGroupId ), $objDatabase );
	}

	public static function fetchSurveyQuestionAnswersBySurveyQuestionTypeId( $intSurveyQuestionTypeId, $objDatabase ) {
		return self::fetchSurveyQuestionAnswers( sprintf( 'SELECT * FROM survey_question_answers WHERE survey_question_type_id = %d', ( int ) $intSurveyQuestionTypeId ), $objDatabase );
	}

	public static function fetchSurveyQuestionAnswersBySurveyTemplateQuestionId( $intSurveyTemplateQuestionId, $objDatabase ) {
		return self::fetchSurveyQuestionAnswers( sprintf( 'SELECT * FROM survey_question_answers WHERE survey_template_question_id = %d', ( int ) $intSurveyTemplateQuestionId ), $objDatabase );
	}

	public static function fetchSurveyQuestionAnswersBySurveyTemplateOptionId( $intSurveyTemplateOptionId, $objDatabase ) {
		return self::fetchSurveyQuestionAnswers( sprintf( 'SELECT * FROM survey_question_answers WHERE survey_template_option_id = %d', ( int ) $intSurveyTemplateOptionId ), $objDatabase );
	}

}
?>