<?php

class CBaseCompanyValueNominationDocument extends CEosSingularBase {

	const TABLE_NAME = 'public.company_value_nomination_documents';

	protected $m_intId;
	protected $m_intCompanyValueNominationId;
	protected $m_intPsDocumentId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['company_value_nomination_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyValueNominationId', trim( $arrValues['company_value_nomination_id'] ) ); elseif( isset( $arrValues['company_value_nomination_id'] ) ) $this->setCompanyValueNominationId( $arrValues['company_value_nomination_id'] );
		if( isset( $arrValues['ps_document_id'] ) && $boolDirectSet ) $this->set( 'm_intPsDocumentId', trim( $arrValues['ps_document_id'] ) ); elseif( isset( $arrValues['ps_document_id'] ) ) $this->setPsDocumentId( $arrValues['ps_document_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCompanyValueNominationId( $intCompanyValueNominationId ) {
		$this->set( 'm_intCompanyValueNominationId', CStrings::strToIntDef( $intCompanyValueNominationId, NULL, false ) );
	}

	public function getCompanyValueNominationId() {
		return $this->m_intCompanyValueNominationId;
	}

	public function sqlCompanyValueNominationId() {
		return ( true == isset( $this->m_intCompanyValueNominationId ) ) ? ( string ) $this->m_intCompanyValueNominationId : 'NULL';
	}

	public function setPsDocumentId( $intPsDocumentId ) {
		$this->set( 'm_intPsDocumentId', CStrings::strToIntDef( $intPsDocumentId, NULL, false ) );
	}

	public function getPsDocumentId() {
		return $this->m_intPsDocumentId;
	}

	public function sqlPsDocumentId() {
		return ( true == isset( $this->m_intPsDocumentId ) ) ? ( string ) $this->m_intPsDocumentId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, company_value_nomination_id, ps_document_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCompanyValueNominationId() . ', ' .
 						$this->sqlPsDocumentId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_value_nomination_id = ' . $this->sqlCompanyValueNominationId() . ','; } elseif( true == array_key_exists( 'CompanyValueNominationId', $this->getChangedColumns() ) ) { $strSql .= ' company_value_nomination_id = ' . $this->sqlCompanyValueNominationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; } elseif( true == array_key_exists( 'PsDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'company_value_nomination_id' => $this->getCompanyValueNominationId(),
			'ps_document_id' => $this->getPsDocumentId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>