<?php

class CBaseSurveyReference extends CEosSingularBase {

	const TABLE_NAME = 'public.survey_references';

	protected $m_intId;
	protected $m_intSurveyId;
	protected $m_intSurveyReferenceTypeId;
	protected $m_intSurveyQuestionAnswerId;
	protected $m_strValue;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['survey_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyId', trim( $arrValues['survey_id'] ) ); elseif( isset( $arrValues['survey_id'] ) ) $this->setSurveyId( $arrValues['survey_id'] );
		if( isset( $arrValues['survey_reference_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyReferenceTypeId', trim( $arrValues['survey_reference_type_id'] ) ); elseif( isset( $arrValues['survey_reference_type_id'] ) ) $this->setSurveyReferenceTypeId( $arrValues['survey_reference_type_id'] );
		if( isset( $arrValues['survey_question_answer_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyQuestionAnswerId', trim( $arrValues['survey_question_answer_id'] ) ); elseif( isset( $arrValues['survey_question_answer_id'] ) ) $this->setSurveyQuestionAnswerId( $arrValues['survey_question_answer_id'] );
		if( isset( $arrValues['value'] ) && $boolDirectSet ) $this->set( 'm_strValue', trim( stripcslashes( $arrValues['value'] ) ) ); elseif( isset( $arrValues['value'] ) ) $this->setValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['value'] ) : $arrValues['value'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSurveyId( $intSurveyId ) {
		$this->set( 'm_intSurveyId', CStrings::strToIntDef( $intSurveyId, NULL, false ) );
	}

	public function getSurveyId() {
		return $this->m_intSurveyId;
	}

	public function sqlSurveyId() {
		return ( true == isset( $this->m_intSurveyId ) ) ? ( string ) $this->m_intSurveyId : 'NULL';
	}

	public function setSurveyReferenceTypeId( $intSurveyReferenceTypeId ) {
		$this->set( 'm_intSurveyReferenceTypeId', CStrings::strToIntDef( $intSurveyReferenceTypeId, NULL, false ) );
	}

	public function getSurveyReferenceTypeId() {
		return $this->m_intSurveyReferenceTypeId;
	}

	public function sqlSurveyReferenceTypeId() {
		return ( true == isset( $this->m_intSurveyReferenceTypeId ) ) ? ( string ) $this->m_intSurveyReferenceTypeId : 'NULL';
	}

	public function setSurveyQuestionAnswerId( $intSurveyQuestionAnswerId ) {
		$this->set( 'm_intSurveyQuestionAnswerId', CStrings::strToIntDef( $intSurveyQuestionAnswerId, NULL, false ) );
	}

	public function getSurveyQuestionAnswerId() {
		return $this->m_intSurveyQuestionAnswerId;
	}

	public function sqlSurveyQuestionAnswerId() {
		return ( true == isset( $this->m_intSurveyQuestionAnswerId ) ) ? ( string ) $this->m_intSurveyQuestionAnswerId : 'NULL';
	}

	public function setValue( $strValue ) {
		$this->set( 'm_strValue', CStrings::strTrimDef( $strValue, -1, NULL, true ) );
	}

	public function getValue() {
		return $this->m_strValue;
	}

	public function sqlValue() {
		return ( true == isset( $this->m_strValue ) ) ? '\'' . addslashes( $this->m_strValue ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, survey_id, survey_reference_type_id, survey_question_answer_id, value, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSurveyId() . ', ' .
 						$this->sqlSurveyReferenceTypeId() . ', ' .
 						$this->sqlSurveyQuestionAnswerId() . ', ' .
 						$this->sqlValue() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_id = ' . $this->sqlSurveyId() . ','; } elseif( true == array_key_exists( 'SurveyId', $this->getChangedColumns() ) ) { $strSql .= ' survey_id = ' . $this->sqlSurveyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_reference_type_id = ' . $this->sqlSurveyReferenceTypeId() . ','; } elseif( true == array_key_exists( 'SurveyReferenceTypeId', $this->getChangedColumns() ) ) { $strSql .= ' survey_reference_type_id = ' . $this->sqlSurveyReferenceTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_question_answer_id = ' . $this->sqlSurveyQuestionAnswerId() . ','; } elseif( true == array_key_exists( 'SurveyQuestionAnswerId', $this->getChangedColumns() ) ) { $strSql .= ' survey_question_answer_id = ' . $this->sqlSurveyQuestionAnswerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value = ' . $this->sqlValue() . ','; } elseif( true == array_key_exists( 'Value', $this->getChangedColumns() ) ) { $strSql .= ' value = ' . $this->sqlValue() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'survey_id' => $this->getSurveyId(),
			'survey_reference_type_id' => $this->getSurveyReferenceTypeId(),
			'survey_question_answer_id' => $this->getSurveyQuestionAnswerId(),
			'value' => $this->getValue(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>