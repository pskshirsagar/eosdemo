<?php

class CBaseDealDeskMeetingAttendee extends CEosSingularBase {

	const TABLE_NAME = 'public.deal_desk_meeting_attendees';

	protected $m_intId;
	protected $m_intDealDeskMeetingId;
	protected $m_intAttendeeId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['deal_desk_meeting_id'] ) && $boolDirectSet ) $this->set( 'm_intDealDeskMeetingId', trim( $arrValues['deal_desk_meeting_id'] ) ); elseif( isset( $arrValues['deal_desk_meeting_id'] ) ) $this->setDealDeskMeetingId( $arrValues['deal_desk_meeting_id'] );
		if( isset( $arrValues['attendee_id'] ) && $boolDirectSet ) $this->set( 'm_intAttendeeId', trim( $arrValues['attendee_id'] ) ); elseif( isset( $arrValues['attendee_id'] ) ) $this->setAttendeeId( $arrValues['attendee_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDealDeskMeetingId( $intDealDeskMeetingId ) {
		$this->set( 'm_intDealDeskMeetingId', CStrings::strToIntDef( $intDealDeskMeetingId, NULL, false ) );
	}

	public function getDealDeskMeetingId() {
		return $this->m_intDealDeskMeetingId;
	}

	public function sqlDealDeskMeetingId() {
		return ( true == isset( $this->m_intDealDeskMeetingId ) ) ? ( string ) $this->m_intDealDeskMeetingId : 'NULL';
	}

	public function setAttendeeId( $intAttendeeId ) {
		$this->set( 'm_intAttendeeId', CStrings::strToIntDef( $intAttendeeId, NULL, false ) );
	}

	public function getAttendeeId() {
		return $this->m_intAttendeeId;
	}

	public function sqlAttendeeId() {
		return ( true == isset( $this->m_intAttendeeId ) ) ? ( string ) $this->m_intAttendeeId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, deal_desk_meeting_id, attendee_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDealDeskMeetingId() . ', ' .
						$this->sqlAttendeeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deal_desk_meeting_id = ' . $this->sqlDealDeskMeetingId(). ',' ; } elseif( true == array_key_exists( 'DealDeskMeetingId', $this->getChangedColumns() ) ) { $strSql .= ' deal_desk_meeting_id = ' . $this->sqlDealDeskMeetingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attendee_id = ' . $this->sqlAttendeeId() ; } elseif( true == array_key_exists( 'AttendeeId', $this->getChangedColumns() ) ) { $strSql .= ' attendee_id = ' . $this->sqlAttendeeId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'deal_desk_meeting_id' => $this->getDealDeskMeetingId(),
			'attendee_id' => $this->getAttendeeId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>