<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserCommitErrors
 * Do not add any new functions to this class.
 */

class CBaseUserCommitErrors extends CEosPluralBase {

	/**
	 * @return CUserCommitError[]
	 */
	public static function fetchUserCommitErrors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserCommitError', $objDatabase );
	}

	/**
	 * @return CUserCommitError
	 */
	public static function fetchUserCommitError( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserCommitError', $objDatabase );
	}

	public static function fetchUserCommitErrorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_commit_errors', $objDatabase );
	}

	public static function fetchUserCommitErrorById( $intId, $objDatabase ) {
		return self::fetchUserCommitError( sprintf( 'SELECT * FROM user_commit_errors WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserCommitErrorsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchUserCommitErrors( sprintf( 'SELECT * FROM user_commit_errors WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchUserCommitErrorsByUserCommitCheckTypeId( $intUserCommitCheckTypeId, $objDatabase ) {
		return self::fetchUserCommitErrors( sprintf( 'SELECT * FROM user_commit_errors WHERE user_commit_check_type_id = %d', ( int ) $intUserCommitCheckTypeId ), $objDatabase );
	}

}
?>