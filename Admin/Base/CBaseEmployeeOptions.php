<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeOptions
 * Do not add any new functions to this class.
 */

class CBaseEmployeeOptions extends CEosPluralBase {

	/**
	 * @return CEmployeeOption[]
	 */
	public static function fetchEmployeeOptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeOption', $objDatabase );
	}

	/**
	 * @return CEmployeeOption
	 */
	public static function fetchEmployeeOption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeOption', $objDatabase );
	}

	public static function fetchEmployeeOptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_options', $objDatabase );
	}

	public static function fetchEmployeeOptionById( $intId, $objDatabase ) {
		return self::fetchEmployeeOption( sprintf( 'SELECT * FROM employee_options WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeOptionsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeOptions( sprintf( 'SELECT * FROM employee_options WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeOptionsByVestingScheduleId( $intVestingScheduleId, $objDatabase ) {
		return self::fetchEmployeeOptions( sprintf( 'SELECT * FROM employee_options WHERE vesting_schedule_id = %d', ( int ) $intVestingScheduleId ), $objDatabase );
	}

	public static function fetchEmployeeOptionsByEmployeeOptionAssociationId( $intEmployeeOptionAssociationId, $objDatabase ) {
		return self::fetchEmployeeOptions( sprintf( 'SELECT * FROM employee_options WHERE employee_option_association_id = %d', ( int ) $intEmployeeOptionAssociationId ), $objDatabase );
	}

	public static function fetchEmployeeOptionsByExercisePriceAssociationId( $intExercisePriceAssociationId, $objDatabase ) {
		return self::fetchEmployeeOptions( sprintf( 'SELECT * FROM employee_options WHERE exercise_price_association_id = %d', ( int ) $intExercisePriceAssociationId ), $objDatabase );
	}

}
?>