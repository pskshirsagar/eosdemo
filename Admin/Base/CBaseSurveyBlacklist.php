<?php

class CBaseSurveyBlacklist extends CEosSingularBase {

	const TABLE_NAME = 'public.survey_blacklists';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intSurveyTemplateId;
	protected $m_intPersonRoleId;
	protected $m_intPersonId;
	protected $m_intEmployeeId;
	protected $m_intDesignationId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['survey_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateId', trim( $arrValues['survey_template_id'] ) ); elseif( isset( $arrValues['survey_template_id'] ) ) $this->setSurveyTemplateId( $arrValues['survey_template_id'] );
		if( isset( $arrValues['person_role_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonRoleId', trim( $arrValues['person_role_id'] ) ); elseif( isset( $arrValues['person_role_id'] ) ) $this->setPersonRoleId( $arrValues['person_role_id'] );
		if( isset( $arrValues['person_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonId', trim( $arrValues['person_id'] ) ); elseif( isset( $arrValues['person_id'] ) ) $this->setPersonId( $arrValues['person_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setSurveyTemplateId( $intSurveyTemplateId ) {
		$this->set( 'm_intSurveyTemplateId', CStrings::strToIntDef( $intSurveyTemplateId, NULL, false ) );
	}

	public function getSurveyTemplateId() {
		return $this->m_intSurveyTemplateId;
	}

	public function sqlSurveyTemplateId() {
		return ( true == isset( $this->m_intSurveyTemplateId ) ) ? ( string ) $this->m_intSurveyTemplateId : 'NULL';
	}

	public function setPersonRoleId( $intPersonRoleId ) {
		$this->set( 'm_intPersonRoleId', CStrings::strToIntDef( $intPersonRoleId, NULL, false ) );
	}

	public function getPersonRoleId() {
		return $this->m_intPersonRoleId;
	}

	public function sqlPersonRoleId() {
		return ( true == isset( $this->m_intPersonRoleId ) ) ? ( string ) $this->m_intPersonRoleId : 'NULL';
	}

	public function setPersonId( $intPersonId ) {
		$this->set( 'm_intPersonId', CStrings::strToIntDef( $intPersonId, NULL, false ) );
	}

	public function getPersonId() {
		return $this->m_intPersonId;
	}

	public function sqlPersonId() {
		return ( true == isset( $this->m_intPersonId ) ) ? ( string ) $this->m_intPersonId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, survey_template_id, person_role_id, person_id, employee_id, designation_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlSurveyTemplateId() . ', ' .
 						$this->sqlPersonRoleId() . ', ' .
 						$this->sqlPersonId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlDesignationId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' person_role_id = ' . $this->sqlPersonRoleId() . ','; } elseif( true == array_key_exists( 'PersonRoleId', $this->getChangedColumns() ) ) { $strSql .= ' person_role_id = ' . $this->sqlPersonRoleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' person_id = ' . $this->sqlPersonId() . ','; } elseif( true == array_key_exists( 'PersonId', $this->getChangedColumns() ) ) { $strSql .= ' person_id = ' . $this->sqlPersonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'survey_template_id' => $this->getSurveyTemplateId(),
			'person_role_id' => $this->getPersonRoleId(),
			'person_id' => $this->getPersonId(),
			'employee_id' => $this->getEmployeeId(),
			'designation_id' => $this->getDesignationId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>