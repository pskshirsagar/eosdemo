<?php

class CBaseInvoice extends CEosSingularBase {

	const TABLE_NAME = 'public.invoices';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intInvoiceBatchId;
	protected $m_intCompanyPaymentId;
	protected $m_strCurrencyCode;
	protected $m_intInvoiceStatusTypeId;
	protected $m_intUtilityBatchId;
	protected $m_intInvoiceNumber;
	protected $m_strInvoiceDate;
	protected $m_strInvoiceDueDate;
	protected $m_fltInvoiceAmount;
	protected $m_strFileData;
	protected $m_intIsCreditMemo;
	protected $m_strMailedOn;
	protected $m_strEmailedOn;
	protected $m_strFtpdOn;
	protected $m_intExportedBy;
	protected $m_strExportedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsNonBilledSalesTax;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';
		$this->m_intIsCreditMemo = '0';
		$this->m_boolIsNonBilledSalesTax = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['invoice_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceBatchId', trim( $arrValues['invoice_batch_id'] ) ); elseif( isset( $arrValues['invoice_batch_id'] ) ) $this->setInvoiceBatchId( $arrValues['invoice_batch_id'] );
		if( isset( $arrValues['company_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyPaymentId', trim( $arrValues['company_payment_id'] ) ); elseif( isset( $arrValues['company_payment_id'] ) ) $this->setCompanyPaymentId( $arrValues['company_payment_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( $arrValues['currency_code'] ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( $arrValues['currency_code'] );
		if( isset( $arrValues['invoice_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceStatusTypeId', trim( $arrValues['invoice_status_type_id'] ) ); elseif( isset( $arrValues['invoice_status_type_id'] ) ) $this->setInvoiceStatusTypeId( $arrValues['invoice_status_type_id'] );
		if( isset( $arrValues['utility_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intUtilityBatchId', trim( $arrValues['utility_batch_id'] ) ); elseif( isset( $arrValues['utility_batch_id'] ) ) $this->setUtilityBatchId( $arrValues['utility_batch_id'] );
		if( isset( $arrValues['invoice_number'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceNumber', trim( $arrValues['invoice_number'] ) ); elseif( isset( $arrValues['invoice_number'] ) ) $this->setInvoiceNumber( $arrValues['invoice_number'] );
		if( isset( $arrValues['invoice_date'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceDate', trim( $arrValues['invoice_date'] ) ); elseif( isset( $arrValues['invoice_date'] ) ) $this->setInvoiceDate( $arrValues['invoice_date'] );
		if( isset( $arrValues['invoice_due_date'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceDueDate', trim( $arrValues['invoice_due_date'] ) ); elseif( isset( $arrValues['invoice_due_date'] ) ) $this->setInvoiceDueDate( $arrValues['invoice_due_date'] );
		if( isset( $arrValues['invoice_amount'] ) && $boolDirectSet ) $this->set( 'm_fltInvoiceAmount', trim( $arrValues['invoice_amount'] ) ); elseif( isset( $arrValues['invoice_amount'] ) ) $this->setInvoiceAmount( $arrValues['invoice_amount'] );
		if( isset( $arrValues['file_data'] ) && $boolDirectSet ) $this->set( 'm_strFileData', trim( $arrValues['file_data'] ) ); elseif( isset( $arrValues['file_data'] ) ) $this->setFileData( $arrValues['file_data'] );
		if( isset( $arrValues['is_credit_memo'] ) && $boolDirectSet ) $this->set( 'm_intIsCreditMemo', trim( $arrValues['is_credit_memo'] ) ); elseif( isset( $arrValues['is_credit_memo'] ) ) $this->setIsCreditMemo( $arrValues['is_credit_memo'] );
		if( isset( $arrValues['mailed_on'] ) && $boolDirectSet ) $this->set( 'm_strMailedOn', trim( $arrValues['mailed_on'] ) ); elseif( isset( $arrValues['mailed_on'] ) ) $this->setMailedOn( $arrValues['mailed_on'] );
		if( isset( $arrValues['emailed_on'] ) && $boolDirectSet ) $this->set( 'm_strEmailedOn', trim( $arrValues['emailed_on'] ) ); elseif( isset( $arrValues['emailed_on'] ) ) $this->setEmailedOn( $arrValues['emailed_on'] );
		if( isset( $arrValues['ftpd_on'] ) && $boolDirectSet ) $this->set( 'm_strFtpdOn', trim( $arrValues['ftpd_on'] ) ); elseif( isset( $arrValues['ftpd_on'] ) ) $this->setFtpdOn( $arrValues['ftpd_on'] );
		if( isset( $arrValues['exported_by'] ) && $boolDirectSet ) $this->set( 'm_intExportedBy', trim( $arrValues['exported_by'] ) ); elseif( isset( $arrValues['exported_by'] ) ) $this->setExportedBy( $arrValues['exported_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_non_billed_sales_tax'] ) && $boolDirectSet ) $this->set( 'm_boolIsNonBilledSalesTax', trim( stripcslashes( $arrValues['is_non_billed_sales_tax'] ) ) ); elseif( isset( $arrValues['is_non_billed_sales_tax'] ) ) $this->setIsNonBilledSalesTax( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_non_billed_sales_tax'] ) : $arrValues['is_non_billed_sales_tax'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setInvoiceBatchId( $intInvoiceBatchId ) {
		$this->set( 'm_intInvoiceBatchId', CStrings::strToIntDef( $intInvoiceBatchId, NULL, false ) );
	}

	public function getInvoiceBatchId() {
		return $this->m_intInvoiceBatchId;
	}

	public function sqlInvoiceBatchId() {
		return ( true == isset( $this->m_intInvoiceBatchId ) ) ? ( string ) $this->m_intInvoiceBatchId : 'NULL';
	}

	public function setCompanyPaymentId( $intCompanyPaymentId ) {
		$this->set( 'm_intCompanyPaymentId', CStrings::strToIntDef( $intCompanyPaymentId, NULL, false ) );
	}

	public function getCompanyPaymentId() {
		return $this->m_intCompanyPaymentId;
	}

	public function sqlCompanyPaymentId() {
		return ( true == isset( $this->m_intCompanyPaymentId ) ) ? ( string ) $this->m_intCompanyPaymentId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrencyCode ) : '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' ) : '\'USD\'';
	}

	public function setInvoiceStatusTypeId( $intInvoiceStatusTypeId ) {
		$this->set( 'm_intInvoiceStatusTypeId', CStrings::strToIntDef( $intInvoiceStatusTypeId, NULL, false ) );
	}

	public function getInvoiceStatusTypeId() {
		return $this->m_intInvoiceStatusTypeId;
	}

	public function sqlInvoiceStatusTypeId() {
		return ( true == isset( $this->m_intInvoiceStatusTypeId ) ) ? ( string ) $this->m_intInvoiceStatusTypeId : 'NULL';
	}

	public function setUtilityBatchId( $intUtilityBatchId ) {
		$this->set( 'm_intUtilityBatchId', CStrings::strToIntDef( $intUtilityBatchId, NULL, false ) );
	}

	public function getUtilityBatchId() {
		return $this->m_intUtilityBatchId;
	}

	public function sqlUtilityBatchId() {
		return ( true == isset( $this->m_intUtilityBatchId ) ) ? ( string ) $this->m_intUtilityBatchId : 'NULL';
	}

	public function setInvoiceNumber( $intInvoiceNumber ) {
		$this->set( 'm_intInvoiceNumber', CStrings::strToIntDef( $intInvoiceNumber, NULL, false ) );
	}

	public function getInvoiceNumber() {
		return $this->m_intInvoiceNumber;
	}

	public function sqlInvoiceNumber() {
		return ( true == isset( $this->m_intInvoiceNumber ) ) ? ( string ) $this->m_intInvoiceNumber : 'NULL';
	}

	public function setInvoiceDate( $strInvoiceDate ) {
		$this->set( 'm_strInvoiceDate', CStrings::strTrimDef( $strInvoiceDate, -1, NULL, true ) );
	}

	public function getInvoiceDate() {
		return $this->m_strInvoiceDate;
	}

	public function sqlInvoiceDate() {
		return ( true == isset( $this->m_strInvoiceDate ) ) ? '\'' . $this->m_strInvoiceDate . '\'' : 'NOW()';
	}

	public function setInvoiceDueDate( $strInvoiceDueDate ) {
		$this->set( 'm_strInvoiceDueDate', CStrings::strTrimDef( $strInvoiceDueDate, -1, NULL, true ) );
	}

	public function getInvoiceDueDate() {
		return $this->m_strInvoiceDueDate;
	}

	public function sqlInvoiceDueDate() {
		return ( true == isset( $this->m_strInvoiceDueDate ) ) ? '\'' . $this->m_strInvoiceDueDate . '\'' : 'NULL';
	}

	public function setInvoiceAmount( $fltInvoiceAmount ) {
		$this->set( 'm_fltInvoiceAmount', CStrings::strToFloatDef( $fltInvoiceAmount, NULL, false, 4 ) );
	}

	public function getInvoiceAmount() {
		return $this->m_fltInvoiceAmount;
	}

	public function sqlInvoiceAmount() {
		return ( true == isset( $this->m_fltInvoiceAmount ) ) ? ( string ) $this->m_fltInvoiceAmount : 'NULL';
	}

	public function setFileData( $strFileData ) {
		$this->set( 'm_strFileData', CStrings::strTrimDef( $strFileData, -1, NULL, true ) );
	}

	public function getFileData() {
		return $this->m_strFileData;
	}

	public function sqlFileData() {
		return ( true == isset( $this->m_strFileData ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileData ) : '\'' . addslashes( $this->m_strFileData ) . '\'' ) : 'NULL';
	}

	public function setIsCreditMemo( $intIsCreditMemo ) {
		$this->set( 'm_intIsCreditMemo', CStrings::strToIntDef( $intIsCreditMemo, NULL, false ) );
	}

	public function getIsCreditMemo() {
		return $this->m_intIsCreditMemo;
	}

	public function sqlIsCreditMemo() {
		return ( true == isset( $this->m_intIsCreditMemo ) ) ? ( string ) $this->m_intIsCreditMemo : '0';
	}

	public function setMailedOn( $strMailedOn ) {
		$this->set( 'm_strMailedOn', CStrings::strTrimDef( $strMailedOn, -1, NULL, true ) );
	}

	public function getMailedOn() {
		return $this->m_strMailedOn;
	}

	public function sqlMailedOn() {
		return ( true == isset( $this->m_strMailedOn ) ) ? '\'' . $this->m_strMailedOn . '\'' : 'NULL';
	}

	public function setEmailedOn( $strEmailedOn ) {
		$this->set( 'm_strEmailedOn', CStrings::strTrimDef( $strEmailedOn, -1, NULL, true ) );
	}

	public function getEmailedOn() {
		return $this->m_strEmailedOn;
	}

	public function sqlEmailedOn() {
		return ( true == isset( $this->m_strEmailedOn ) ) ? '\'' . $this->m_strEmailedOn . '\'' : 'NULL';
	}

	public function setFtpdOn( $strFtpdOn ) {
		$this->set( 'm_strFtpdOn', CStrings::strTrimDef( $strFtpdOn, -1, NULL, true ) );
	}

	public function getFtpdOn() {
		return $this->m_strFtpdOn;
	}

	public function sqlFtpdOn() {
		return ( true == isset( $this->m_strFtpdOn ) ) ? '\'' . $this->m_strFtpdOn . '\'' : 'NULL';
	}

	public function setExportedBy( $intExportedBy ) {
		$this->set( 'm_intExportedBy', CStrings::strToIntDef( $intExportedBy, NULL, false ) );
	}

	public function getExportedBy() {
		return $this->m_intExportedBy;
	}

	public function sqlExportedBy() {
		return ( true == isset( $this->m_intExportedBy ) ) ? ( string ) $this->m_intExportedBy : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsNonBilledSalesTax( $boolIsNonBilledSalesTax ) {
		$this->set( 'm_boolIsNonBilledSalesTax', CStrings::strToBool( $boolIsNonBilledSalesTax ) );
	}

	public function getIsNonBilledSalesTax() {
		return $this->m_boolIsNonBilledSalesTax;
	}

	public function sqlIsNonBilledSalesTax() {
		return ( true == isset( $this->m_boolIsNonBilledSalesTax ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsNonBilledSalesTax ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, invoice_batch_id, company_payment_id, currency_code, invoice_status_type_id, utility_batch_id, invoice_number, invoice_date, invoice_due_date, invoice_amount, file_data, is_credit_memo, mailed_on, emailed_on, ftpd_on, exported_by, exported_on, deleted_by, deleted_on, created_by, created_on, is_non_billed_sales_tax )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlInvoiceBatchId() . ', ' .
						$this->sqlCompanyPaymentId() . ', ' .
						$this->sqlCurrencyCode() . ', ' .
						$this->sqlInvoiceStatusTypeId() . ', ' .
						$this->sqlUtilityBatchId() . ', ' .
						$this->sqlInvoiceNumber() . ', ' .
						$this->sqlInvoiceDate() . ', ' .
						$this->sqlInvoiceDueDate() . ', ' .
						$this->sqlInvoiceAmount() . ', ' .
						$this->sqlFileData() . ', ' .
						$this->sqlIsCreditMemo() . ', ' .
						$this->sqlMailedOn() . ', ' .
						$this->sqlEmailedOn() . ', ' .
						$this->sqlFtpdOn() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsNonBilledSalesTax() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_batch_id = ' . $this->sqlInvoiceBatchId(). ',' ; } elseif( true == array_key_exists( 'InvoiceBatchId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_batch_id = ' . $this->sqlInvoiceBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId(). ',' ; } elseif( true == array_key_exists( 'CompanyPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_status_type_id = ' . $this->sqlInvoiceStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'InvoiceStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_status_type_id = ' . $this->sqlInvoiceStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId(). ',' ; } elseif( true == array_key_exists( 'UtilityBatchId', $this->getChangedColumns() ) ) { $strSql .= ' utility_batch_id = ' . $this->sqlUtilityBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_number = ' . $this->sqlInvoiceNumber(). ',' ; } elseif( true == array_key_exists( 'InvoiceNumber', $this->getChangedColumns() ) ) { $strSql .= ' invoice_number = ' . $this->sqlInvoiceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_date = ' . $this->sqlInvoiceDate(). ',' ; } elseif( true == array_key_exists( 'InvoiceDate', $this->getChangedColumns() ) ) { $strSql .= ' invoice_date = ' . $this->sqlInvoiceDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_due_date = ' . $this->sqlInvoiceDueDate(). ',' ; } elseif( true == array_key_exists( 'InvoiceDueDate', $this->getChangedColumns() ) ) { $strSql .= ' invoice_due_date = ' . $this->sqlInvoiceDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_amount = ' . $this->sqlInvoiceAmount(). ',' ; } elseif( true == array_key_exists( 'InvoiceAmount', $this->getChangedColumns() ) ) { $strSql .= ' invoice_amount = ' . $this->sqlInvoiceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_data = ' . $this->sqlFileData(). ',' ; } elseif( true == array_key_exists( 'FileData', $this->getChangedColumns() ) ) { $strSql .= ' file_data = ' . $this->sqlFileData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_credit_memo = ' . $this->sqlIsCreditMemo(). ',' ; } elseif( true == array_key_exists( 'IsCreditMemo', $this->getChangedColumns() ) ) { $strSql .= ' is_credit_memo = ' . $this->sqlIsCreditMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mailed_on = ' . $this->sqlMailedOn(). ',' ; } elseif( true == array_key_exists( 'MailedOn', $this->getChangedColumns() ) ) { $strSql .= ' mailed_on = ' . $this->sqlMailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn(). ',' ; } elseif( true == array_key_exists( 'EmailedOn', $this->getChangedColumns() ) ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ftpd_on = ' . $this->sqlFtpdOn(). ',' ; } elseif( true == array_key_exists( 'FtpdOn', $this->getChangedColumns() ) ) { $strSql .= ' ftpd_on = ' . $this->sqlFtpdOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy(). ',' ; } elseif( true == array_key_exists( 'ExportedBy', $this->getChangedColumns() ) ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_non_billed_sales_tax = ' . $this->sqlIsNonBilledSalesTax() ; } elseif( true == array_key_exists( 'IsNonBilledSalesTax', $this->getChangedColumns() ) ) { $strSql .= ' is_non_billed_sales_tax = ' . $this->sqlIsNonBilledSalesTax() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'invoice_batch_id' => $this->getInvoiceBatchId(),
			'company_payment_id' => $this->getCompanyPaymentId(),
			'currency_code' => $this->getCurrencyCode(),
			'invoice_status_type_id' => $this->getInvoiceStatusTypeId(),
			'utility_batch_id' => $this->getUtilityBatchId(),
			'invoice_number' => $this->getInvoiceNumber(),
			'invoice_date' => $this->getInvoiceDate(),
			'invoice_due_date' => $this->getInvoiceDueDate(),
			'invoice_amount' => $this->getInvoiceAmount(),
			'file_data' => $this->getFileData(),
			'is_credit_memo' => $this->getIsCreditMemo(),
			'mailed_on' => $this->getMailedOn(),
			'emailed_on' => $this->getEmailedOn(),
			'ftpd_on' => $this->getFtpdOn(),
			'exported_by' => $this->getExportedBy(),
			'exported_on' => $this->getExportedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_non_billed_sales_tax' => $this->getIsNonBilledSalesTax()
		);
	}

}
?>