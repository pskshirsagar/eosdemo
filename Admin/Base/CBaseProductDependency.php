<?php

class CBaseProductDependency extends CEosSingularBase {

	const TABLE_NAME = 'public.product_dependencies';

	protected $m_intId;
	protected $m_intPsProductId;
	protected $m_intDependentPsProductId;
	protected $m_boolIsBundleDependency;
	protected $m_boolIsActive;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsBundleDependency = false;
		$this->m_boolIsActive = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['dependent_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intDependentPsProductId', trim( $arrValues['dependent_ps_product_id'] ) ); elseif( isset( $arrValues['dependent_ps_product_id'] ) ) $this->setDependentPsProductId( $arrValues['dependent_ps_product_id'] );
		if( isset( $arrValues['is_bundle_dependency'] ) && $boolDirectSet ) $this->set( 'm_boolIsBundleDependency', trim( stripcslashes( $arrValues['is_bundle_dependency'] ) ) ); elseif( isset( $arrValues['is_bundle_dependency'] ) ) $this->setIsBundleDependency( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_bundle_dependency'] ) : $arrValues['is_bundle_dependency'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setDependentPsProductId( $intDependentPsProductId ) {
		$this->set( 'm_intDependentPsProductId', CStrings::strToIntDef( $intDependentPsProductId, NULL, false ) );
	}

	public function getDependentPsProductId() {
		return $this->m_intDependentPsProductId;
	}

	public function sqlDependentPsProductId() {
		return ( true == isset( $this->m_intDependentPsProductId ) ) ? ( string ) $this->m_intDependentPsProductId : 'NULL';
	}

	public function setIsBundleDependency( $boolIsBundleDependency ) {
		$this->set( 'm_boolIsBundleDependency', CStrings::strToBool( $boolIsBundleDependency ) );
	}

	public function getIsBundleDependency() {
		return $this->m_boolIsBundleDependency;
	}

	public function sqlIsBundleDependency() {
		return ( true == isset( $this->m_boolIsBundleDependency ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBundleDependency ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_product_id, dependent_ps_product_id, is_bundle_dependency, is_active, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlDependentPsProductId() . ', ' .
 						$this->sqlIsBundleDependency() . ', ' .
 						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dependent_ps_product_id = ' . $this->sqlDependentPsProductId() . ','; } elseif( true == array_key_exists( 'DependentPsProductId', $this->getChangedColumns() ) ) { $strSql .= ' dependent_ps_product_id = ' . $this->sqlDependentPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_bundle_dependency = ' . $this->sqlIsBundleDependency() . ','; } elseif( true == array_key_exists( 'IsBundleDependency', $this->getChangedColumns() ) ) { $strSql .= ' is_bundle_dependency = ' . $this->sqlIsBundleDependency() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_product_id' => $this->getPsProductId(),
			'dependent_ps_product_id' => $this->getDependentPsProductId(),
			'is_bundle_dependency' => $this->getIsBundleDependency(),
			'is_active' => $this->getIsActive(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>