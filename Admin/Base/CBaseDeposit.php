<?php

class CBaseDeposit extends CEosSingularBase {

	const TABLE_NAME = 'public.deposits';

	protected $m_intId;
	protected $m_intDepositTypeId;
	protected $m_strCurrencyCode;
	protected $m_intChartOfAccountId;
	protected $m_intExportBatchTypeId;
	protected $m_intExportBatchId;
	protected $m_strRemotePrimaryKey;
	protected $m_strDepositDate;
	protected $m_fltDepositAmount;
	protected $m_strDepositMemo;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['deposit_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDepositTypeId', trim( $arrValues['deposit_type_id'] ) ); elseif( isset( $arrValues['deposit_type_id'] ) ) $this->setDepositTypeId( $arrValues['deposit_type_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		if( isset( $arrValues['chart_of_account_id'] ) && $boolDirectSet ) $this->set( 'm_intChartOfAccountId', trim( $arrValues['chart_of_account_id'] ) ); elseif( isset( $arrValues['chart_of_account_id'] ) ) $this->setChartOfAccountId( $arrValues['chart_of_account_id'] );
		if( isset( $arrValues['export_batch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intExportBatchTypeId', trim( $arrValues['export_batch_type_id'] ) ); elseif( isset( $arrValues['export_batch_type_id'] ) ) $this->setExportBatchTypeId( $arrValues['export_batch_type_id'] );
		if( isset( $arrValues['export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intExportBatchId', trim( $arrValues['export_batch_id'] ) ); elseif( isset( $arrValues['export_batch_id'] ) ) $this->setExportBatchId( $arrValues['export_batch_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['deposit_date'] ) && $boolDirectSet ) $this->set( 'm_strDepositDate', trim( $arrValues['deposit_date'] ) ); elseif( isset( $arrValues['deposit_date'] ) ) $this->setDepositDate( $arrValues['deposit_date'] );
		if( isset( $arrValues['deposit_amount'] ) && $boolDirectSet ) $this->set( 'm_fltDepositAmount', trim( $arrValues['deposit_amount'] ) ); elseif( isset( $arrValues['deposit_amount'] ) ) $this->setDepositAmount( $arrValues['deposit_amount'] );
		if( isset( $arrValues['deposit_memo'] ) && $boolDirectSet ) $this->set( 'm_strDepositMemo', trim( stripcslashes( $arrValues['deposit_memo'] ) ) ); elseif( isset( $arrValues['deposit_memo'] ) ) $this->setDepositMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['deposit_memo'] ) : $arrValues['deposit_memo'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDepositTypeId( $intDepositTypeId ) {
		$this->set( 'm_intDepositTypeId', CStrings::strToIntDef( $intDepositTypeId, NULL, false ) );
	}

	public function getDepositTypeId() {
		return $this->m_intDepositTypeId;
	}

	public function sqlDepositTypeId() {
		return ( true == isset( $this->m_intDepositTypeId ) ) ? ( string ) $this->m_intDepositTypeId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : '\'USD\'';
	}

	public function setChartOfAccountId( $intChartOfAccountId ) {
		$this->set( 'm_intChartOfAccountId', CStrings::strToIntDef( $intChartOfAccountId, NULL, false ) );
	}

	public function getChartOfAccountId() {
		return $this->m_intChartOfAccountId;
	}

	public function sqlChartOfAccountId() {
		return ( true == isset( $this->m_intChartOfAccountId ) ) ? ( string ) $this->m_intChartOfAccountId : 'NULL';
	}

	public function setExportBatchTypeId( $intExportBatchTypeId ) {
		$this->set( 'm_intExportBatchTypeId', CStrings::strToIntDef( $intExportBatchTypeId, NULL, false ) );
	}

	public function getExportBatchTypeId() {
		return $this->m_intExportBatchTypeId;
	}

	public function sqlExportBatchTypeId() {
		return ( true == isset( $this->m_intExportBatchTypeId ) ) ? ( string ) $this->m_intExportBatchTypeId : 'NULL';
	}

	public function setExportBatchId( $intExportBatchId ) {
		$this->set( 'm_intExportBatchId', CStrings::strToIntDef( $intExportBatchId, NULL, false ) );
	}

	public function getExportBatchId() {
		return $this->m_intExportBatchId;
	}

	public function sqlExportBatchId() {
		return ( true == isset( $this->m_intExportBatchId ) ) ? ( string ) $this->m_intExportBatchId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setDepositDate( $strDepositDate ) {
		$this->set( 'm_strDepositDate', CStrings::strTrimDef( $strDepositDate, -1, NULL, true ) );
	}

	public function getDepositDate() {
		return $this->m_strDepositDate;
	}

	public function sqlDepositDate() {
		return ( true == isset( $this->m_strDepositDate ) ) ? '\'' . $this->m_strDepositDate . '\'' : 'NOW()';
	}

	public function setDepositAmount( $fltDepositAmount ) {
		$this->set( 'm_fltDepositAmount', CStrings::strToFloatDef( $fltDepositAmount, NULL, false, 4 ) );
	}

	public function getDepositAmount() {
		return $this->m_fltDepositAmount;
	}

	public function sqlDepositAmount() {
		return ( true == isset( $this->m_fltDepositAmount ) ) ? ( string ) $this->m_fltDepositAmount : 'NULL';
	}

	public function setDepositMemo( $strDepositMemo ) {
		$this->set( 'm_strDepositMemo', CStrings::strTrimDef( $strDepositMemo, 2000, NULL, true ) );
	}

	public function getDepositMemo() {
		return $this->m_strDepositMemo;
	}

	public function sqlDepositMemo() {
		return ( true == isset( $this->m_strDepositMemo ) ) ? '\'' . addslashes( $this->m_strDepositMemo ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, deposit_type_id, currency_code, chart_of_account_id, export_batch_type_id, export_batch_id, remote_primary_key, deposit_date, deposit_amount, deposit_memo, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDepositTypeId() . ', ' .
 						$this->sqlCurrencyCode() . ', ' .
 						$this->sqlChartOfAccountId() . ', ' .
 						$this->sqlExportBatchTypeId() . ', ' .
 						$this->sqlExportBatchId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlDepositDate() . ', ' .
 						$this->sqlDepositAmount() . ', ' .
 						$this->sqlDepositMemo() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_type_id = ' . $this->sqlDepositTypeId() . ','; } elseif( true == array_key_exists( 'DepositTypeId', $this->getChangedColumns() ) ) { $strSql .= ' deposit_type_id = ' . $this->sqlDepositTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chart_of_account_id = ' . $this->sqlChartOfAccountId() . ','; } elseif( true == array_key_exists( 'ChartOfAccountId', $this->getChangedColumns() ) ) { $strSql .= ' chart_of_account_id = ' . $this->sqlChartOfAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_batch_type_id = ' . $this->sqlExportBatchTypeId() . ','; } elseif( true == array_key_exists( 'ExportBatchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' export_batch_type_id = ' . $this->sqlExportBatchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_batch_id = ' . $this->sqlExportBatchId() . ','; } elseif( true == array_key_exists( 'ExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' export_batch_id = ' . $this->sqlExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_date = ' . $this->sqlDepositDate() . ','; } elseif( true == array_key_exists( 'DepositDate', $this->getChangedColumns() ) ) { $strSql .= ' deposit_date = ' . $this->sqlDepositDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_amount = ' . $this->sqlDepositAmount() . ','; } elseif( true == array_key_exists( 'DepositAmount', $this->getChangedColumns() ) ) { $strSql .= ' deposit_amount = ' . $this->sqlDepositAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_memo = ' . $this->sqlDepositMemo() . ','; } elseif( true == array_key_exists( 'DepositMemo', $this->getChangedColumns() ) ) { $strSql .= ' deposit_memo = ' . $this->sqlDepositMemo() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'deposit_type_id' => $this->getDepositTypeId(),
			'currency_code' => $this->getCurrencyCode(),
			'chart_of_account_id' => $this->getChartOfAccountId(),
			'export_batch_type_id' => $this->getExportBatchTypeId(),
			'export_batch_id' => $this->getExportBatchId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'deposit_date' => $this->getDepositDate(),
			'deposit_amount' => $this->getDepositAmount(),
			'deposit_memo' => $this->getDepositMemo(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>