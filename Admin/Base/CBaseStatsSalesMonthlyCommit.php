<?php

class CBaseStatsSalesMonthlyCommit extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_sales_monthly_commits';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intPsProductId;
	protected $m_intPsLeadId;
	protected $m_intContractId;
	protected $m_strMonth;
	protected $m_fltPercentCloseLikelihood;
	protected $m_strAnticipatedCloseDate;
	protected $m_intCommittedNewUnits;
	protected $m_intCommittedLogoSubscriptionAcv;
	protected $m_intCommittedLogoTransactionAcv;
	protected $m_intCommittedProductSubscriptionAcv;
	protected $m_intCommittedProductTransactionAcv;
	protected $m_intCommittedPropertySubscriptionAcv;
	protected $m_intCommittedPropertyTransactionAcv;
	protected $m_intCommittedRenewalSubscriptionAcv;
	protected $m_intCommittedRenewalTransactionAcv;
	protected $m_fltTotalCommittedAcv;
	protected $m_intActualNewUnits;
	protected $m_intActualLogoSubscriptionAcv;
	protected $m_intActualLogoTransactionAcv;
	protected $m_intActualProductSubscriptionAcv;
	protected $m_intActualProductTransactionAcv;
	protected $m_intActualPropertySubscriptionAcv;
	protected $m_intActualPropertyTransactionAcv;
	protected $m_intActualRenewalSubscriptionAcv;
	protected $m_intActualRenewalTransactionAcv;
	protected $m_intActualTransferPropertySubscriptionAcv;
	protected $m_intActualTransferPropertyTransactionAcv;
	protected $m_fltActualSalesAcv;
	protected $m_fltActualBookedAcv;
	protected $m_fltActualTotalNewAcv;
	protected $m_fltActualExpandedAcv;
	protected $m_intIsCommit;
	protected $m_intCommittedBy;
	protected $m_strCommittedOn;
	protected $m_intLockedBy;
	protected $m_strLockedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltPercentCloseLikelihood = '0';
		$this->m_intCommittedNewUnits = '0';
		$this->m_intCommittedLogoSubscriptionAcv = '0';
		$this->m_intCommittedLogoTransactionAcv = '0';
		$this->m_intCommittedProductSubscriptionAcv = '0';
		$this->m_intCommittedProductTransactionAcv = '0';
		$this->m_intCommittedPropertySubscriptionAcv = '0';
		$this->m_intCommittedPropertyTransactionAcv = '0';
		$this->m_intCommittedRenewalSubscriptionAcv = '0';
		$this->m_intCommittedRenewalTransactionAcv = '0';
		$this->m_fltTotalCommittedAcv = '0';
		$this->m_intActualNewUnits = '0';
		$this->m_intActualLogoSubscriptionAcv = '0';
		$this->m_intActualLogoTransactionAcv = '0';
		$this->m_intActualProductSubscriptionAcv = '0';
		$this->m_intActualProductTransactionAcv = '0';
		$this->m_intActualPropertySubscriptionAcv = '0';
		$this->m_intActualPropertyTransactionAcv = '0';
		$this->m_intActualRenewalSubscriptionAcv = '0';
		$this->m_intActualRenewalTransactionAcv = '0';
		$this->m_intActualTransferPropertySubscriptionAcv = '0';
		$this->m_intActualTransferPropertyTransactionAcv = '0';
		$this->m_fltActualSalesAcv = '0';
		$this->m_fltActualBookedAcv = '0';
		$this->m_fltActualTotalNewAcv = '0';
		$this->m_fltActualExpandedAcv = '0';
		$this->m_intIsCommit = '0';
		$this->m_intCreatedBy = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['percent_close_likelihood'] ) && $boolDirectSet ) $this->set( 'm_fltPercentCloseLikelihood', trim( $arrValues['percent_close_likelihood'] ) ); elseif( isset( $arrValues['percent_close_likelihood'] ) ) $this->setPercentCloseLikelihood( $arrValues['percent_close_likelihood'] );
		if( isset( $arrValues['anticipated_close_date'] ) && $boolDirectSet ) $this->set( 'm_strAnticipatedCloseDate', trim( $arrValues['anticipated_close_date'] ) ); elseif( isset( $arrValues['anticipated_close_date'] ) ) $this->setAnticipatedCloseDate( $arrValues['anticipated_close_date'] );
		if( isset( $arrValues['committed_new_units'] ) && $boolDirectSet ) $this->set( 'm_intCommittedNewUnits', trim( $arrValues['committed_new_units'] ) ); elseif( isset( $arrValues['committed_new_units'] ) ) $this->setCommittedNewUnits( $arrValues['committed_new_units'] );
		if( isset( $arrValues['committed_logo_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intCommittedLogoSubscriptionAcv', trim( $arrValues['committed_logo_subscription_acv'] ) ); elseif( isset( $arrValues['committed_logo_subscription_acv'] ) ) $this->setCommittedLogoSubscriptionAcv( $arrValues['committed_logo_subscription_acv'] );
		if( isset( $arrValues['committed_logo_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intCommittedLogoTransactionAcv', trim( $arrValues['committed_logo_transaction_acv'] ) ); elseif( isset( $arrValues['committed_logo_transaction_acv'] ) ) $this->setCommittedLogoTransactionAcv( $arrValues['committed_logo_transaction_acv'] );
		if( isset( $arrValues['committed_product_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intCommittedProductSubscriptionAcv', trim( $arrValues['committed_product_subscription_acv'] ) ); elseif( isset( $arrValues['committed_product_subscription_acv'] ) ) $this->setCommittedProductSubscriptionAcv( $arrValues['committed_product_subscription_acv'] );
		if( isset( $arrValues['committed_product_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intCommittedProductTransactionAcv', trim( $arrValues['committed_product_transaction_acv'] ) ); elseif( isset( $arrValues['committed_product_transaction_acv'] ) ) $this->setCommittedProductTransactionAcv( $arrValues['committed_product_transaction_acv'] );
		if( isset( $arrValues['committed_property_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intCommittedPropertySubscriptionAcv', trim( $arrValues['committed_property_subscription_acv'] ) ); elseif( isset( $arrValues['committed_property_subscription_acv'] ) ) $this->setCommittedPropertySubscriptionAcv( $arrValues['committed_property_subscription_acv'] );
		if( isset( $arrValues['committed_property_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intCommittedPropertyTransactionAcv', trim( $arrValues['committed_property_transaction_acv'] ) ); elseif( isset( $arrValues['committed_property_transaction_acv'] ) ) $this->setCommittedPropertyTransactionAcv( $arrValues['committed_property_transaction_acv'] );
		if( isset( $arrValues['committed_renewal_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intCommittedRenewalSubscriptionAcv', trim( $arrValues['committed_renewal_subscription_acv'] ) ); elseif( isset( $arrValues['committed_renewal_subscription_acv'] ) ) $this->setCommittedRenewalSubscriptionAcv( $arrValues['committed_renewal_subscription_acv'] );
		if( isset( $arrValues['committed_renewal_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intCommittedRenewalTransactionAcv', trim( $arrValues['committed_renewal_transaction_acv'] ) ); elseif( isset( $arrValues['committed_renewal_transaction_acv'] ) ) $this->setCommittedRenewalTransactionAcv( $arrValues['committed_renewal_transaction_acv'] );
		if( isset( $arrValues['total_committed_acv'] ) && $boolDirectSet ) $this->set( 'm_fltTotalCommittedAcv', trim( $arrValues['total_committed_acv'] ) ); elseif( isset( $arrValues['total_committed_acv'] ) ) $this->setTotalCommittedAcv( $arrValues['total_committed_acv'] );
		if( isset( $arrValues['actual_new_units'] ) && $boolDirectSet ) $this->set( 'm_intActualNewUnits', trim( $arrValues['actual_new_units'] ) ); elseif( isset( $arrValues['actual_new_units'] ) ) $this->setActualNewUnits( $arrValues['actual_new_units'] );
		if( isset( $arrValues['actual_logo_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intActualLogoSubscriptionAcv', trim( $arrValues['actual_logo_subscription_acv'] ) ); elseif( isset( $arrValues['actual_logo_subscription_acv'] ) ) $this->setActualLogoSubscriptionAcv( $arrValues['actual_logo_subscription_acv'] );
		if( isset( $arrValues['actual_logo_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intActualLogoTransactionAcv', trim( $arrValues['actual_logo_transaction_acv'] ) ); elseif( isset( $arrValues['actual_logo_transaction_acv'] ) ) $this->setActualLogoTransactionAcv( $arrValues['actual_logo_transaction_acv'] );
		if( isset( $arrValues['actual_product_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intActualProductSubscriptionAcv', trim( $arrValues['actual_product_subscription_acv'] ) ); elseif( isset( $arrValues['actual_product_subscription_acv'] ) ) $this->setActualProductSubscriptionAcv( $arrValues['actual_product_subscription_acv'] );
		if( isset( $arrValues['actual_product_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intActualProductTransactionAcv', trim( $arrValues['actual_product_transaction_acv'] ) ); elseif( isset( $arrValues['actual_product_transaction_acv'] ) ) $this->setActualProductTransactionAcv( $arrValues['actual_product_transaction_acv'] );
		if( isset( $arrValues['actual_property_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intActualPropertySubscriptionAcv', trim( $arrValues['actual_property_subscription_acv'] ) ); elseif( isset( $arrValues['actual_property_subscription_acv'] ) ) $this->setActualPropertySubscriptionAcv( $arrValues['actual_property_subscription_acv'] );
		if( isset( $arrValues['actual_property_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intActualPropertyTransactionAcv', trim( $arrValues['actual_property_transaction_acv'] ) ); elseif( isset( $arrValues['actual_property_transaction_acv'] ) ) $this->setActualPropertyTransactionAcv( $arrValues['actual_property_transaction_acv'] );
		if( isset( $arrValues['actual_renewal_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intActualRenewalSubscriptionAcv', trim( $arrValues['actual_renewal_subscription_acv'] ) ); elseif( isset( $arrValues['actual_renewal_subscription_acv'] ) ) $this->setActualRenewalSubscriptionAcv( $arrValues['actual_renewal_subscription_acv'] );
		if( isset( $arrValues['actual_renewal_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intActualRenewalTransactionAcv', trim( $arrValues['actual_renewal_transaction_acv'] ) ); elseif( isset( $arrValues['actual_renewal_transaction_acv'] ) ) $this->setActualRenewalTransactionAcv( $arrValues['actual_renewal_transaction_acv'] );
		if( isset( $arrValues['actual_transfer_property_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intActualTransferPropertySubscriptionAcv', trim( $arrValues['actual_transfer_property_subscription_acv'] ) ); elseif( isset( $arrValues['actual_transfer_property_subscription_acv'] ) ) $this->setActualTransferPropertySubscriptionAcv( $arrValues['actual_transfer_property_subscription_acv'] );
		if( isset( $arrValues['actual_transfer_property_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intActualTransferPropertyTransactionAcv', trim( $arrValues['actual_transfer_property_transaction_acv'] ) ); elseif( isset( $arrValues['actual_transfer_property_transaction_acv'] ) ) $this->setActualTransferPropertyTransactionAcv( $arrValues['actual_transfer_property_transaction_acv'] );
		if( isset( $arrValues['actual_sales_acv'] ) && $boolDirectSet ) $this->set( 'm_fltActualSalesAcv', trim( $arrValues['actual_sales_acv'] ) ); elseif( isset( $arrValues['actual_sales_acv'] ) ) $this->setActualSalesAcv( $arrValues['actual_sales_acv'] );
		if( isset( $arrValues['actual_booked_acv'] ) && $boolDirectSet ) $this->set( 'm_fltActualBookedAcv', trim( $arrValues['actual_booked_acv'] ) ); elseif( isset( $arrValues['actual_booked_acv'] ) ) $this->setActualBookedAcv( $arrValues['actual_booked_acv'] );
		if( isset( $arrValues['actual_total_new_acv'] ) && $boolDirectSet ) $this->set( 'm_fltActualTotalNewAcv', trim( $arrValues['actual_total_new_acv'] ) ); elseif( isset( $arrValues['actual_total_new_acv'] ) ) $this->setActualTotalNewAcv( $arrValues['actual_total_new_acv'] );
		if( isset( $arrValues['actual_expanded_acv'] ) && $boolDirectSet ) $this->set( 'm_fltActualExpandedAcv', trim( $arrValues['actual_expanded_acv'] ) ); elseif( isset( $arrValues['actual_expanded_acv'] ) ) $this->setActualExpandedAcv( $arrValues['actual_expanded_acv'] );
		if( isset( $arrValues['is_commit'] ) && $boolDirectSet ) $this->set( 'm_intIsCommit', trim( $arrValues['is_commit'] ) ); elseif( isset( $arrValues['is_commit'] ) ) $this->setIsCommit( $arrValues['is_commit'] );
		if( isset( $arrValues['committed_by'] ) && $boolDirectSet ) $this->set( 'm_intCommittedBy', trim( $arrValues['committed_by'] ) ); elseif( isset( $arrValues['committed_by'] ) ) $this->setCommittedBy( $arrValues['committed_by'] );
		if( isset( $arrValues['committed_on'] ) && $boolDirectSet ) $this->set( 'm_strCommittedOn', trim( $arrValues['committed_on'] ) ); elseif( isset( $arrValues['committed_on'] ) ) $this->setCommittedOn( $arrValues['committed_on'] );
		if( isset( $arrValues['locked_by'] ) && $boolDirectSet ) $this->set( 'm_intLockedBy', trim( $arrValues['locked_by'] ) ); elseif( isset( $arrValues['locked_by'] ) ) $this->setLockedBy( $arrValues['locked_by'] );
		if( isset( $arrValues['locked_on'] ) && $boolDirectSet ) $this->set( 'm_strLockedOn', trim( $arrValues['locked_on'] ) ); elseif( isset( $arrValues['locked_on'] ) ) $this->setLockedOn( $arrValues['locked_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NOW()';
	}

	public function setPercentCloseLikelihood( $fltPercentCloseLikelihood ) {
		$this->set( 'm_fltPercentCloseLikelihood', CStrings::strToFloatDef( $fltPercentCloseLikelihood, NULL, false, 6 ) );
	}

	public function getPercentCloseLikelihood() {
		return $this->m_fltPercentCloseLikelihood;
	}

	public function sqlPercentCloseLikelihood() {
		return ( true == isset( $this->m_fltPercentCloseLikelihood ) ) ? ( string ) $this->m_fltPercentCloseLikelihood : '0';
	}

	public function setAnticipatedCloseDate( $strAnticipatedCloseDate ) {
		$this->set( 'm_strAnticipatedCloseDate', CStrings::strTrimDef( $strAnticipatedCloseDate, -1, NULL, true ) );
	}

	public function getAnticipatedCloseDate() {
		return $this->m_strAnticipatedCloseDate;
	}

	public function sqlAnticipatedCloseDate() {
		return ( true == isset( $this->m_strAnticipatedCloseDate ) ) ? '\'' . $this->m_strAnticipatedCloseDate . '\'' : 'NULL';
	}

	public function setCommittedNewUnits( $intCommittedNewUnits ) {
		$this->set( 'm_intCommittedNewUnits', CStrings::strToIntDef( $intCommittedNewUnits, NULL, false ) );
	}

	public function getCommittedNewUnits() {
		return $this->m_intCommittedNewUnits;
	}

	public function sqlCommittedNewUnits() {
		return ( true == isset( $this->m_intCommittedNewUnits ) ) ? ( string ) $this->m_intCommittedNewUnits : '0';
	}

	public function setCommittedLogoSubscriptionAcv( $intCommittedLogoSubscriptionAcv ) {
		$this->set( 'm_intCommittedLogoSubscriptionAcv', CStrings::strToIntDef( $intCommittedLogoSubscriptionAcv, NULL, false ) );
	}

	public function getCommittedLogoSubscriptionAcv() {
		return $this->m_intCommittedLogoSubscriptionAcv;
	}

	public function sqlCommittedLogoSubscriptionAcv() {
		return ( true == isset( $this->m_intCommittedLogoSubscriptionAcv ) ) ? ( string ) $this->m_intCommittedLogoSubscriptionAcv : '0';
	}

	public function setCommittedLogoTransactionAcv( $intCommittedLogoTransactionAcv ) {
		$this->set( 'm_intCommittedLogoTransactionAcv', CStrings::strToIntDef( $intCommittedLogoTransactionAcv, NULL, false ) );
	}

	public function getCommittedLogoTransactionAcv() {
		return $this->m_intCommittedLogoTransactionAcv;
	}

	public function sqlCommittedLogoTransactionAcv() {
		return ( true == isset( $this->m_intCommittedLogoTransactionAcv ) ) ? ( string ) $this->m_intCommittedLogoTransactionAcv : '0';
	}

	public function setCommittedProductSubscriptionAcv( $intCommittedProductSubscriptionAcv ) {
		$this->set( 'm_intCommittedProductSubscriptionAcv', CStrings::strToIntDef( $intCommittedProductSubscriptionAcv, NULL, false ) );
	}

	public function getCommittedProductSubscriptionAcv() {
		return $this->m_intCommittedProductSubscriptionAcv;
	}

	public function sqlCommittedProductSubscriptionAcv() {
		return ( true == isset( $this->m_intCommittedProductSubscriptionAcv ) ) ? ( string ) $this->m_intCommittedProductSubscriptionAcv : '0';
	}

	public function setCommittedProductTransactionAcv( $intCommittedProductTransactionAcv ) {
		$this->set( 'm_intCommittedProductTransactionAcv', CStrings::strToIntDef( $intCommittedProductTransactionAcv, NULL, false ) );
	}

	public function getCommittedProductTransactionAcv() {
		return $this->m_intCommittedProductTransactionAcv;
	}

	public function sqlCommittedProductTransactionAcv() {
		return ( true == isset( $this->m_intCommittedProductTransactionAcv ) ) ? ( string ) $this->m_intCommittedProductTransactionAcv : '0';
	}

	public function setCommittedPropertySubscriptionAcv( $intCommittedPropertySubscriptionAcv ) {
		$this->set( 'm_intCommittedPropertySubscriptionAcv', CStrings::strToIntDef( $intCommittedPropertySubscriptionAcv, NULL, false ) );
	}

	public function getCommittedPropertySubscriptionAcv() {
		return $this->m_intCommittedPropertySubscriptionAcv;
	}

	public function sqlCommittedPropertySubscriptionAcv() {
		return ( true == isset( $this->m_intCommittedPropertySubscriptionAcv ) ) ? ( string ) $this->m_intCommittedPropertySubscriptionAcv : '0';
	}

	public function setCommittedPropertyTransactionAcv( $intCommittedPropertyTransactionAcv ) {
		$this->set( 'm_intCommittedPropertyTransactionAcv', CStrings::strToIntDef( $intCommittedPropertyTransactionAcv, NULL, false ) );
	}

	public function getCommittedPropertyTransactionAcv() {
		return $this->m_intCommittedPropertyTransactionAcv;
	}

	public function sqlCommittedPropertyTransactionAcv() {
		return ( true == isset( $this->m_intCommittedPropertyTransactionAcv ) ) ? ( string ) $this->m_intCommittedPropertyTransactionAcv : '0';
	}

	public function setCommittedRenewalSubscriptionAcv( $intCommittedRenewalSubscriptionAcv ) {
		$this->set( 'm_intCommittedRenewalSubscriptionAcv', CStrings::strToIntDef( $intCommittedRenewalSubscriptionAcv, NULL, false ) );
	}

	public function getCommittedRenewalSubscriptionAcv() {
		return $this->m_intCommittedRenewalSubscriptionAcv;
	}

	public function sqlCommittedRenewalSubscriptionAcv() {
		return ( true == isset( $this->m_intCommittedRenewalSubscriptionAcv ) ) ? ( string ) $this->m_intCommittedRenewalSubscriptionAcv : '0';
	}

	public function setCommittedRenewalTransactionAcv( $intCommittedRenewalTransactionAcv ) {
		$this->set( 'm_intCommittedRenewalTransactionAcv', CStrings::strToIntDef( $intCommittedRenewalTransactionAcv, NULL, false ) );
	}

	public function getCommittedRenewalTransactionAcv() {
		return $this->m_intCommittedRenewalTransactionAcv;
	}

	public function sqlCommittedRenewalTransactionAcv() {
		return ( true == isset( $this->m_intCommittedRenewalTransactionAcv ) ) ? ( string ) $this->m_intCommittedRenewalTransactionAcv : '0';
	}

	public function setTotalCommittedAcv( $fltTotalCommittedAcv ) {
		$this->set( 'm_fltTotalCommittedAcv', CStrings::strToFloatDef( $fltTotalCommittedAcv, NULL, false, 2 ) );
	}

	public function getTotalCommittedAcv() {
		return $this->m_fltTotalCommittedAcv;
	}

	public function sqlTotalCommittedAcv() {
		return ( true == isset( $this->m_fltTotalCommittedAcv ) ) ? ( string ) $this->m_fltTotalCommittedAcv : '0';
	}

	public function setActualNewUnits( $intActualNewUnits ) {
		$this->set( 'm_intActualNewUnits', CStrings::strToIntDef( $intActualNewUnits, NULL, false ) );
	}

	public function getActualNewUnits() {
		return $this->m_intActualNewUnits;
	}

	public function sqlActualNewUnits() {
		return ( true == isset( $this->m_intActualNewUnits ) ) ? ( string ) $this->m_intActualNewUnits : '0';
	}

	public function setActualLogoSubscriptionAcv( $intActualLogoSubscriptionAcv ) {
		$this->set( 'm_intActualLogoSubscriptionAcv', CStrings::strToIntDef( $intActualLogoSubscriptionAcv, NULL, false ) );
	}

	public function getActualLogoSubscriptionAcv() {
		return $this->m_intActualLogoSubscriptionAcv;
	}

	public function sqlActualLogoSubscriptionAcv() {
		return ( true == isset( $this->m_intActualLogoSubscriptionAcv ) ) ? ( string ) $this->m_intActualLogoSubscriptionAcv : '0';
	}

	public function setActualLogoTransactionAcv( $intActualLogoTransactionAcv ) {
		$this->set( 'm_intActualLogoTransactionAcv', CStrings::strToIntDef( $intActualLogoTransactionAcv, NULL, false ) );
	}

	public function getActualLogoTransactionAcv() {
		return $this->m_intActualLogoTransactionAcv;
	}

	public function sqlActualLogoTransactionAcv() {
		return ( true == isset( $this->m_intActualLogoTransactionAcv ) ) ? ( string ) $this->m_intActualLogoTransactionAcv : '0';
	}

	public function setActualProductSubscriptionAcv( $intActualProductSubscriptionAcv ) {
		$this->set( 'm_intActualProductSubscriptionAcv', CStrings::strToIntDef( $intActualProductSubscriptionAcv, NULL, false ) );
	}

	public function getActualProductSubscriptionAcv() {
		return $this->m_intActualProductSubscriptionAcv;
	}

	public function sqlActualProductSubscriptionAcv() {
		return ( true == isset( $this->m_intActualProductSubscriptionAcv ) ) ? ( string ) $this->m_intActualProductSubscriptionAcv : '0';
	}

	public function setActualProductTransactionAcv( $intActualProductTransactionAcv ) {
		$this->set( 'm_intActualProductTransactionAcv', CStrings::strToIntDef( $intActualProductTransactionAcv, NULL, false ) );
	}

	public function getActualProductTransactionAcv() {
		return $this->m_intActualProductTransactionAcv;
	}

	public function sqlActualProductTransactionAcv() {
		return ( true == isset( $this->m_intActualProductTransactionAcv ) ) ? ( string ) $this->m_intActualProductTransactionAcv : '0';
	}

	public function setActualPropertySubscriptionAcv( $intActualPropertySubscriptionAcv ) {
		$this->set( 'm_intActualPropertySubscriptionAcv', CStrings::strToIntDef( $intActualPropertySubscriptionAcv, NULL, false ) );
	}

	public function getActualPropertySubscriptionAcv() {
		return $this->m_intActualPropertySubscriptionAcv;
	}

	public function sqlActualPropertySubscriptionAcv() {
		return ( true == isset( $this->m_intActualPropertySubscriptionAcv ) ) ? ( string ) $this->m_intActualPropertySubscriptionAcv : '0';
	}

	public function setActualPropertyTransactionAcv( $intActualPropertyTransactionAcv ) {
		$this->set( 'm_intActualPropertyTransactionAcv', CStrings::strToIntDef( $intActualPropertyTransactionAcv, NULL, false ) );
	}

	public function getActualPropertyTransactionAcv() {
		return $this->m_intActualPropertyTransactionAcv;
	}

	public function sqlActualPropertyTransactionAcv() {
		return ( true == isset( $this->m_intActualPropertyTransactionAcv ) ) ? ( string ) $this->m_intActualPropertyTransactionAcv : '0';
	}

	public function setActualRenewalSubscriptionAcv( $intActualRenewalSubscriptionAcv ) {
		$this->set( 'm_intActualRenewalSubscriptionAcv', CStrings::strToIntDef( $intActualRenewalSubscriptionAcv, NULL, false ) );
	}

	public function getActualRenewalSubscriptionAcv() {
		return $this->m_intActualRenewalSubscriptionAcv;
	}

	public function sqlActualRenewalSubscriptionAcv() {
		return ( true == isset( $this->m_intActualRenewalSubscriptionAcv ) ) ? ( string ) $this->m_intActualRenewalSubscriptionAcv : '0';
	}

	public function setActualRenewalTransactionAcv( $intActualRenewalTransactionAcv ) {
		$this->set( 'm_intActualRenewalTransactionAcv', CStrings::strToIntDef( $intActualRenewalTransactionAcv, NULL, false ) );
	}

	public function getActualRenewalTransactionAcv() {
		return $this->m_intActualRenewalTransactionAcv;
	}

	public function sqlActualRenewalTransactionAcv() {
		return ( true == isset( $this->m_intActualRenewalTransactionAcv ) ) ? ( string ) $this->m_intActualRenewalTransactionAcv : '0';
	}

	public function setActualTransferPropertySubscriptionAcv( $intActualTransferPropertySubscriptionAcv ) {
		$this->set( 'm_intActualTransferPropertySubscriptionAcv', CStrings::strToIntDef( $intActualTransferPropertySubscriptionAcv, NULL, false ) );
	}

	public function getActualTransferPropertySubscriptionAcv() {
		return $this->m_intActualTransferPropertySubscriptionAcv;
	}

	public function sqlActualTransferPropertySubscriptionAcv() {
		return ( true == isset( $this->m_intActualTransferPropertySubscriptionAcv ) ) ? ( string ) $this->m_intActualTransferPropertySubscriptionAcv : '0';
	}

	public function setActualTransferPropertyTransactionAcv( $intActualTransferPropertyTransactionAcv ) {
		$this->set( 'm_intActualTransferPropertyTransactionAcv', CStrings::strToIntDef( $intActualTransferPropertyTransactionAcv, NULL, false ) );
	}

	public function getActualTransferPropertyTransactionAcv() {
		return $this->m_intActualTransferPropertyTransactionAcv;
	}

	public function sqlActualTransferPropertyTransactionAcv() {
		return ( true == isset( $this->m_intActualTransferPropertyTransactionAcv ) ) ? ( string ) $this->m_intActualTransferPropertyTransactionAcv : '0';
	}

	public function setActualSalesAcv( $fltActualSalesAcv ) {
		$this->set( 'm_fltActualSalesAcv', CStrings::strToFloatDef( $fltActualSalesAcv, NULL, false, 2 ) );
	}

	public function getActualSalesAcv() {
		return $this->m_fltActualSalesAcv;
	}

	public function sqlActualSalesAcv() {
		return ( true == isset( $this->m_fltActualSalesAcv ) ) ? ( string ) $this->m_fltActualSalesAcv : '0';
	}

	public function setActualBookedAcv( $fltActualBookedAcv ) {
		$this->set( 'm_fltActualBookedAcv', CStrings::strToFloatDef( $fltActualBookedAcv, NULL, false, 2 ) );
	}

	public function getActualBookedAcv() {
		return $this->m_fltActualBookedAcv;
	}

	public function sqlActualBookedAcv() {
		return ( true == isset( $this->m_fltActualBookedAcv ) ) ? ( string ) $this->m_fltActualBookedAcv : '0';
	}

	public function setActualTotalNewAcv( $fltActualTotalNewAcv ) {
		$this->set( 'm_fltActualTotalNewAcv', CStrings::strToFloatDef( $fltActualTotalNewAcv, NULL, false, 2 ) );
	}

	public function getActualTotalNewAcv() {
		return $this->m_fltActualTotalNewAcv;
	}

	public function sqlActualTotalNewAcv() {
		return ( true == isset( $this->m_fltActualTotalNewAcv ) ) ? ( string ) $this->m_fltActualTotalNewAcv : '0';
	}

	public function setActualExpandedAcv( $fltActualExpandedAcv ) {
		$this->set( 'm_fltActualExpandedAcv', CStrings::strToFloatDef( $fltActualExpandedAcv, NULL, false, 2 ) );
	}

	public function getActualExpandedAcv() {
		return $this->m_fltActualExpandedAcv;
	}

	public function sqlActualExpandedAcv() {
		return ( true == isset( $this->m_fltActualExpandedAcv ) ) ? ( string ) $this->m_fltActualExpandedAcv : '0';
	}

	public function setIsCommit( $intIsCommit ) {
		$this->set( 'm_intIsCommit', CStrings::strToIntDef( $intIsCommit, NULL, false ) );
	}

	public function getIsCommit() {
		return $this->m_intIsCommit;
	}

	public function sqlIsCommit() {
		return ( true == isset( $this->m_intIsCommit ) ) ? ( string ) $this->m_intIsCommit : '0';
	}

	public function setCommittedBy( $intCommittedBy ) {
		$this->set( 'm_intCommittedBy', CStrings::strToIntDef( $intCommittedBy, NULL, false ) );
	}

	public function getCommittedBy() {
		return $this->m_intCommittedBy;
	}

	public function sqlCommittedBy() {
		return ( true == isset( $this->m_intCommittedBy ) ) ? ( string ) $this->m_intCommittedBy : 'NULL';
	}

	public function setCommittedOn( $strCommittedOn ) {
		$this->set( 'm_strCommittedOn', CStrings::strTrimDef( $strCommittedOn, -1, NULL, true ) );
	}

	public function getCommittedOn() {
		return $this->m_strCommittedOn;
	}

	public function sqlCommittedOn() {
		return ( true == isset( $this->m_strCommittedOn ) ) ? '\'' . $this->m_strCommittedOn . '\'' : 'NULL';
	}

	public function setLockedBy( $intLockedBy ) {
		$this->set( 'm_intLockedBy', CStrings::strToIntDef( $intLockedBy, NULL, false ) );
	}

	public function getLockedBy() {
		return $this->m_intLockedBy;
	}

	public function sqlLockedBy() {
		return ( true == isset( $this->m_intLockedBy ) ) ? ( string ) $this->m_intLockedBy : 'NULL';
	}

	public function setLockedOn( $strLockedOn ) {
		$this->set( 'm_strLockedOn', CStrings::strTrimDef( $strLockedOn, -1, NULL, true ) );
	}

	public function getLockedOn() {
		return $this->m_strLockedOn;
	}

	public function sqlLockedOn() {
		return ( true == isset( $this->m_strLockedOn ) ) ? '\'' . $this->m_strLockedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '0';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, ps_product_id, ps_lead_id, contract_id, month, percent_close_likelihood, anticipated_close_date, committed_new_units, committed_logo_subscription_acv, committed_logo_transaction_acv, committed_product_subscription_acv, committed_product_transaction_acv, committed_property_subscription_acv, committed_property_transaction_acv, committed_renewal_subscription_acv, committed_renewal_transaction_acv, total_committed_acv, actual_new_units, actual_logo_subscription_acv, actual_logo_transaction_acv, actual_product_subscription_acv, actual_product_transaction_acv, actual_property_subscription_acv, actual_property_transaction_acv, actual_renewal_subscription_acv, actual_renewal_transaction_acv, actual_transfer_property_subscription_acv, actual_transfer_property_transaction_acv, actual_sales_acv, actual_booked_acv, actual_total_new_acv, actual_expanded_acv, is_commit, committed_by, committed_on, locked_by, locked_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPsLeadId() . ', ' .
 						$this->sqlContractId() . ', ' .
 						$this->sqlMonth() . ', ' .
 						$this->sqlPercentCloseLikelihood() . ', ' .
 						$this->sqlAnticipatedCloseDate() . ', ' .
 						$this->sqlCommittedNewUnits() . ', ' .
 						$this->sqlCommittedLogoSubscriptionAcv() . ', ' .
 						$this->sqlCommittedLogoTransactionAcv() . ', ' .
 						$this->sqlCommittedProductSubscriptionAcv() . ', ' .
 						$this->sqlCommittedProductTransactionAcv() . ', ' .
 						$this->sqlCommittedPropertySubscriptionAcv() . ', ' .
 						$this->sqlCommittedPropertyTransactionAcv() . ', ' .
 						$this->sqlCommittedRenewalSubscriptionAcv() . ', ' .
 						$this->sqlCommittedRenewalTransactionAcv() . ', ' .
 						$this->sqlTotalCommittedAcv() . ', ' .
 						$this->sqlActualNewUnits() . ', ' .
 						$this->sqlActualLogoSubscriptionAcv() . ', ' .
 						$this->sqlActualLogoTransactionAcv() . ', ' .
 						$this->sqlActualProductSubscriptionAcv() . ', ' .
 						$this->sqlActualProductTransactionAcv() . ', ' .
 						$this->sqlActualPropertySubscriptionAcv() . ', ' .
 						$this->sqlActualPropertyTransactionAcv() . ', ' .
 						$this->sqlActualRenewalSubscriptionAcv() . ', ' .
 						$this->sqlActualRenewalTransactionAcv() . ', ' .
 						$this->sqlActualTransferPropertySubscriptionAcv() . ', ' .
 						$this->sqlActualTransferPropertyTransactionAcv() . ', ' .
 						$this->sqlActualSalesAcv() . ', ' .
 						$this->sqlActualBookedAcv() . ', ' .
 						$this->sqlActualTotalNewAcv() . ', ' .
 						$this->sqlActualExpandedAcv() . ', ' .
 						$this->sqlIsCommit() . ', ' .
 						$this->sqlCommittedBy() . ', ' .
 						$this->sqlCommittedOn() . ', ' .
 						$this->sqlLockedBy() . ', ' .
 						$this->sqlLockedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percent_close_likelihood = ' . $this->sqlPercentCloseLikelihood() . ','; } elseif( true == array_key_exists( 'PercentCloseLikelihood', $this->getChangedColumns() ) ) { $strSql .= ' percent_close_likelihood = ' . $this->sqlPercentCloseLikelihood() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' anticipated_close_date = ' . $this->sqlAnticipatedCloseDate() . ','; } elseif( true == array_key_exists( 'AnticipatedCloseDate', $this->getChangedColumns() ) ) { $strSql .= ' anticipated_close_date = ' . $this->sqlAnticipatedCloseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_new_units = ' . $this->sqlCommittedNewUnits() . ','; } elseif( true == array_key_exists( 'CommittedNewUnits', $this->getChangedColumns() ) ) { $strSql .= ' committed_new_units = ' . $this->sqlCommittedNewUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_logo_subscription_acv = ' . $this->sqlCommittedLogoSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'CommittedLogoSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' committed_logo_subscription_acv = ' . $this->sqlCommittedLogoSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_logo_transaction_acv = ' . $this->sqlCommittedLogoTransactionAcv() . ','; } elseif( true == array_key_exists( 'CommittedLogoTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' committed_logo_transaction_acv = ' . $this->sqlCommittedLogoTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_product_subscription_acv = ' . $this->sqlCommittedProductSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'CommittedProductSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' committed_product_subscription_acv = ' . $this->sqlCommittedProductSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_product_transaction_acv = ' . $this->sqlCommittedProductTransactionAcv() . ','; } elseif( true == array_key_exists( 'CommittedProductTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' committed_product_transaction_acv = ' . $this->sqlCommittedProductTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_property_subscription_acv = ' . $this->sqlCommittedPropertySubscriptionAcv() . ','; } elseif( true == array_key_exists( 'CommittedPropertySubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' committed_property_subscription_acv = ' . $this->sqlCommittedPropertySubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_property_transaction_acv = ' . $this->sqlCommittedPropertyTransactionAcv() . ','; } elseif( true == array_key_exists( 'CommittedPropertyTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' committed_property_transaction_acv = ' . $this->sqlCommittedPropertyTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_renewal_subscription_acv = ' . $this->sqlCommittedRenewalSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'CommittedRenewalSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' committed_renewal_subscription_acv = ' . $this->sqlCommittedRenewalSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_renewal_transaction_acv = ' . $this->sqlCommittedRenewalTransactionAcv() . ','; } elseif( true == array_key_exists( 'CommittedRenewalTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' committed_renewal_transaction_acv = ' . $this->sqlCommittedRenewalTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_committed_acv = ' . $this->sqlTotalCommittedAcv() . ','; } elseif( true == array_key_exists( 'TotalCommittedAcv', $this->getChangedColumns() ) ) { $strSql .= ' total_committed_acv = ' . $this->sqlTotalCommittedAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_new_units = ' . $this->sqlActualNewUnits() . ','; } elseif( true == array_key_exists( 'ActualNewUnits', $this->getChangedColumns() ) ) { $strSql .= ' actual_new_units = ' . $this->sqlActualNewUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_logo_subscription_acv = ' . $this->sqlActualLogoSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'ActualLogoSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_logo_subscription_acv = ' . $this->sqlActualLogoSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_logo_transaction_acv = ' . $this->sqlActualLogoTransactionAcv() . ','; } elseif( true == array_key_exists( 'ActualLogoTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_logo_transaction_acv = ' . $this->sqlActualLogoTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_product_subscription_acv = ' . $this->sqlActualProductSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'ActualProductSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_product_subscription_acv = ' . $this->sqlActualProductSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_product_transaction_acv = ' . $this->sqlActualProductTransactionAcv() . ','; } elseif( true == array_key_exists( 'ActualProductTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_product_transaction_acv = ' . $this->sqlActualProductTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_property_subscription_acv = ' . $this->sqlActualPropertySubscriptionAcv() . ','; } elseif( true == array_key_exists( 'ActualPropertySubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_property_subscription_acv = ' . $this->sqlActualPropertySubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_property_transaction_acv = ' . $this->sqlActualPropertyTransactionAcv() . ','; } elseif( true == array_key_exists( 'ActualPropertyTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_property_transaction_acv = ' . $this->sqlActualPropertyTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_renewal_subscription_acv = ' . $this->sqlActualRenewalSubscriptionAcv() . ','; } elseif( true == array_key_exists( 'ActualRenewalSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_renewal_subscription_acv = ' . $this->sqlActualRenewalSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_renewal_transaction_acv = ' . $this->sqlActualRenewalTransactionAcv() . ','; } elseif( true == array_key_exists( 'ActualRenewalTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_renewal_transaction_acv = ' . $this->sqlActualRenewalTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_transfer_property_subscription_acv = ' . $this->sqlActualTransferPropertySubscriptionAcv() . ','; } elseif( true == array_key_exists( 'ActualTransferPropertySubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_transfer_property_subscription_acv = ' . $this->sqlActualTransferPropertySubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_transfer_property_transaction_acv = ' . $this->sqlActualTransferPropertyTransactionAcv() . ','; } elseif( true == array_key_exists( 'ActualTransferPropertyTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_transfer_property_transaction_acv = ' . $this->sqlActualTransferPropertyTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_sales_acv = ' . $this->sqlActualSalesAcv() . ','; } elseif( true == array_key_exists( 'ActualSalesAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_sales_acv = ' . $this->sqlActualSalesAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_booked_acv = ' . $this->sqlActualBookedAcv() . ','; } elseif( true == array_key_exists( 'ActualBookedAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_booked_acv = ' . $this->sqlActualBookedAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_total_new_acv = ' . $this->sqlActualTotalNewAcv() . ','; } elseif( true == array_key_exists( 'ActualTotalNewAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_total_new_acv = ' . $this->sqlActualTotalNewAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_expanded_acv = ' . $this->sqlActualExpandedAcv() . ','; } elseif( true == array_key_exists( 'ActualExpandedAcv', $this->getChangedColumns() ) ) { $strSql .= ' actual_expanded_acv = ' . $this->sqlActualExpandedAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_commit = ' . $this->sqlIsCommit() . ','; } elseif( true == array_key_exists( 'IsCommit', $this->getChangedColumns() ) ) { $strSql .= ' is_commit = ' . $this->sqlIsCommit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_by = ' . $this->sqlCommittedBy() . ','; } elseif( true == array_key_exists( 'CommittedBy', $this->getChangedColumns() ) ) { $strSql .= ' committed_by = ' . $this->sqlCommittedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' committed_on = ' . $this->sqlCommittedOn() . ','; } elseif( true == array_key_exists( 'CommittedOn', $this->getChangedColumns() ) ) { $strSql .= ' committed_on = ' . $this->sqlCommittedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_by = ' . $this->sqlLockedBy() . ','; } elseif( true == array_key_exists( 'LockedBy', $this->getChangedColumns() ) ) { $strSql .= ' locked_by = ' . $this->sqlLockedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; } elseif( true == array_key_exists( 'LockedOn', $this->getChangedColumns() ) ) { $strSql .= ' locked_on = ' . $this->sqlLockedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'contract_id' => $this->getContractId(),
			'month' => $this->getMonth(),
			'percent_close_likelihood' => $this->getPercentCloseLikelihood(),
			'anticipated_close_date' => $this->getAnticipatedCloseDate(),
			'committed_new_units' => $this->getCommittedNewUnits(),
			'committed_logo_subscription_acv' => $this->getCommittedLogoSubscriptionAcv(),
			'committed_logo_transaction_acv' => $this->getCommittedLogoTransactionAcv(),
			'committed_product_subscription_acv' => $this->getCommittedProductSubscriptionAcv(),
			'committed_product_transaction_acv' => $this->getCommittedProductTransactionAcv(),
			'committed_property_subscription_acv' => $this->getCommittedPropertySubscriptionAcv(),
			'committed_property_transaction_acv' => $this->getCommittedPropertyTransactionAcv(),
			'committed_renewal_subscription_acv' => $this->getCommittedRenewalSubscriptionAcv(),
			'committed_renewal_transaction_acv' => $this->getCommittedRenewalTransactionAcv(),
			'total_committed_acv' => $this->getTotalCommittedAcv(),
			'actual_new_units' => $this->getActualNewUnits(),
			'actual_logo_subscription_acv' => $this->getActualLogoSubscriptionAcv(),
			'actual_logo_transaction_acv' => $this->getActualLogoTransactionAcv(),
			'actual_product_subscription_acv' => $this->getActualProductSubscriptionAcv(),
			'actual_product_transaction_acv' => $this->getActualProductTransactionAcv(),
			'actual_property_subscription_acv' => $this->getActualPropertySubscriptionAcv(),
			'actual_property_transaction_acv' => $this->getActualPropertyTransactionAcv(),
			'actual_renewal_subscription_acv' => $this->getActualRenewalSubscriptionAcv(),
			'actual_renewal_transaction_acv' => $this->getActualRenewalTransactionAcv(),
			'actual_transfer_property_subscription_acv' => $this->getActualTransferPropertySubscriptionAcv(),
			'actual_transfer_property_transaction_acv' => $this->getActualTransferPropertyTransactionAcv(),
			'actual_sales_acv' => $this->getActualSalesAcv(),
			'actual_booked_acv' => $this->getActualBookedAcv(),
			'actual_total_new_acv' => $this->getActualTotalNewAcv(),
			'actual_expanded_acv' => $this->getActualExpandedAcv(),
			'is_commit' => $this->getIsCommit(),
			'committed_by' => $this->getCommittedBy(),
			'committed_on' => $this->getCommittedOn(),
			'locked_by' => $this->getLockedBy(),
			'locked_on' => $this->getLockedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>