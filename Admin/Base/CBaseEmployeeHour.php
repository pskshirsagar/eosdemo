<?php

class CBaseEmployeeHour extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_hours';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intPayrollPeriodId;
	protected $m_intEmployeeVacationRequestId;
	protected $m_intEmployeeHourTypeId;
	protected $m_strDate;
	protected $m_strBeginDatetime;
	protected $m_strEndDatetime;
	protected $m_strProposedTime;
	protected $m_intIsManuallyAdded;
	protected $m_intIsManagerApproved;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intEmployeeHourTypeId = '1';
		$this->m_intIsManuallyAdded = '0';
		$this->m_intIsManagerApproved = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['payroll_period_id'] ) && $boolDirectSet ) $this->set( 'm_intPayrollPeriodId', trim( $arrValues['payroll_period_id'] ) ); elseif( isset( $arrValues['payroll_period_id'] ) ) $this->setPayrollPeriodId( $arrValues['payroll_period_id'] );
		if( isset( $arrValues['employee_vacation_request_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeVacationRequestId', trim( $arrValues['employee_vacation_request_id'] ) ); elseif( isset( $arrValues['employee_vacation_request_id'] ) ) $this->setEmployeeVacationRequestId( $arrValues['employee_vacation_request_id'] );
		if( isset( $arrValues['employee_hour_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeHourTypeId', trim( $arrValues['employee_hour_type_id'] ) ); elseif( isset( $arrValues['employee_hour_type_id'] ) ) $this->setEmployeeHourTypeId( $arrValues['employee_hour_type_id'] );
		if( isset( $arrValues['date'] ) && $boolDirectSet ) $this->set( 'm_strDate', trim( $arrValues['date'] ) ); elseif( isset( $arrValues['date'] ) ) $this->setDate( $arrValues['date'] );
		if( isset( $arrValues['begin_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBeginDatetime', trim( $arrValues['begin_datetime'] ) ); elseif( isset( $arrValues['begin_datetime'] ) ) $this->setBeginDatetime( $arrValues['begin_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['proposed_time'] ) && $boolDirectSet ) $this->set( 'm_strProposedTime', trim( $arrValues['proposed_time'] ) ); elseif( isset( $arrValues['proposed_time'] ) ) $this->setProposedTime( $arrValues['proposed_time'] );
		if( isset( $arrValues['is_manually_added'] ) && $boolDirectSet ) $this->set( 'm_intIsManuallyAdded', trim( $arrValues['is_manually_added'] ) ); elseif( isset( $arrValues['is_manually_added'] ) ) $this->setIsManuallyAdded( $arrValues['is_manually_added'] );
		if( isset( $arrValues['is_manager_approved'] ) && $boolDirectSet ) $this->set( 'm_intIsManagerApproved', trim( $arrValues['is_manager_approved'] ) ); elseif( isset( $arrValues['is_manager_approved'] ) ) $this->setIsManagerApproved( $arrValues['is_manager_approved'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setPayrollPeriodId( $intPayrollPeriodId ) {
		$this->set( 'm_intPayrollPeriodId', CStrings::strToIntDef( $intPayrollPeriodId, NULL, false ) );
	}

	public function getPayrollPeriodId() {
		return $this->m_intPayrollPeriodId;
	}

	public function sqlPayrollPeriodId() {
		return ( true == isset( $this->m_intPayrollPeriodId ) ) ? ( string ) $this->m_intPayrollPeriodId : 'NULL';
	}

	public function setEmployeeVacationRequestId( $intEmployeeVacationRequestId ) {
		$this->set( 'm_intEmployeeVacationRequestId', CStrings::strToIntDef( $intEmployeeVacationRequestId, NULL, false ) );
	}

	public function getEmployeeVacationRequestId() {
		return $this->m_intEmployeeVacationRequestId;
	}

	public function sqlEmployeeVacationRequestId() {
		return ( true == isset( $this->m_intEmployeeVacationRequestId ) ) ? ( string ) $this->m_intEmployeeVacationRequestId : 'NULL';
	}

	public function setEmployeeHourTypeId( $intEmployeeHourTypeId ) {
		$this->set( 'm_intEmployeeHourTypeId', CStrings::strToIntDef( $intEmployeeHourTypeId, NULL, false ) );
	}

	public function getEmployeeHourTypeId() {
		return $this->m_intEmployeeHourTypeId;
	}

	public function sqlEmployeeHourTypeId() {
		return ( true == isset( $this->m_intEmployeeHourTypeId ) ) ? ( string ) $this->m_intEmployeeHourTypeId : '1';
	}

	public function setDate( $strDate ) {
		$this->set( 'm_strDate', CStrings::strTrimDef( $strDate, -1, NULL, true ) );
	}

	public function getDate() {
		return $this->m_strDate;
	}

	public function sqlDate() {
		return ( true == isset( $this->m_strDate ) ) ? '\'' . $this->m_strDate . '\'' : 'NOW()';
	}

	public function setBeginDatetime( $strBeginDatetime ) {
		$this->set( 'm_strBeginDatetime', CStrings::strTrimDef( $strBeginDatetime, -1, NULL, true ) );
	}

	public function getBeginDatetime() {
		return $this->m_strBeginDatetime;
	}

	public function sqlBeginDatetime() {
		return ( true == isset( $this->m_strBeginDatetime ) ) ? '\'' . $this->m_strBeginDatetime . '\'' : 'NULL';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setProposedTime( $strProposedTime ) {
		$this->set( 'm_strProposedTime', CStrings::strTrimDef( $strProposedTime, -1, NULL, true ) );
	}

	public function getProposedTime() {
		return $this->m_strProposedTime;
	}

	public function sqlProposedTime() {
		return ( true == isset( $this->m_strProposedTime ) ) ? '\'' . $this->m_strProposedTime . '\'' : 'NULL';
	}

	public function setIsManuallyAdded( $intIsManuallyAdded ) {
		$this->set( 'm_intIsManuallyAdded', CStrings::strToIntDef( $intIsManuallyAdded, NULL, false ) );
	}

	public function getIsManuallyAdded() {
		return $this->m_intIsManuallyAdded;
	}

	public function sqlIsManuallyAdded() {
		return ( true == isset( $this->m_intIsManuallyAdded ) ) ? ( string ) $this->m_intIsManuallyAdded : '0';
	}

	public function setIsManagerApproved( $intIsManagerApproved ) {
		$this->set( 'm_intIsManagerApproved', CStrings::strToIntDef( $intIsManagerApproved, NULL, false ) );
	}

	public function getIsManagerApproved() {
		return $this->m_intIsManagerApproved;
	}

	public function sqlIsManagerApproved() {
		return ( true == isset( $this->m_intIsManagerApproved ) ) ? ( string ) $this->m_intIsManagerApproved : '1';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, payroll_period_id, employee_vacation_request_id, employee_hour_type_id, date, begin_datetime, end_datetime, proposed_time, is_manually_added, is_manager_approved, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlPayrollPeriodId() . ', ' .
 						$this->sqlEmployeeVacationRequestId() . ', ' .
 						$this->sqlEmployeeHourTypeId() . ', ' .
 						$this->sqlDate() . ', ' .
 						$this->sqlBeginDatetime() . ', ' .
 						$this->sqlEndDatetime() . ', ' .
 						$this->sqlProposedTime() . ', ' .
 						$this->sqlIsManuallyAdded() . ', ' .
 						$this->sqlIsManagerApproved() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payroll_period_id = ' . $this->sqlPayrollPeriodId() . ','; } elseif( true == array_key_exists( 'PayrollPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' payroll_period_id = ' . $this->sqlPayrollPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_vacation_request_id = ' . $this->sqlEmployeeVacationRequestId() . ','; } elseif( true == array_key_exists( 'EmployeeVacationRequestId', $this->getChangedColumns() ) ) { $strSql .= ' employee_vacation_request_id = ' . $this->sqlEmployeeVacationRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_hour_type_id = ' . $this->sqlEmployeeHourTypeId() . ','; } elseif( true == array_key_exists( 'EmployeeHourTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_hour_type_id = ' . $this->sqlEmployeeHourTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date = ' . $this->sqlDate() . ','; } elseif( true == array_key_exists( 'Date', $this->getChangedColumns() ) ) { $strSql .= ' date = ' . $this->sqlDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_datetime = ' . $this->sqlBeginDatetime() . ','; } elseif( true == array_key_exists( 'BeginDatetime', $this->getChangedColumns() ) ) { $strSql .= ' begin_datetime = ' . $this->sqlBeginDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposed_time = ' . $this->sqlProposedTime() . ','; } elseif( true == array_key_exists( 'ProposedTime', $this->getChangedColumns() ) ) { $strSql .= ' proposed_time = ' . $this->sqlProposedTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manually_added = ' . $this->sqlIsManuallyAdded() . ','; } elseif( true == array_key_exists( 'IsManuallyAdded', $this->getChangedColumns() ) ) { $strSql .= ' is_manually_added = ' . $this->sqlIsManuallyAdded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manager_approved = ' . $this->sqlIsManagerApproved() . ','; } elseif( true == array_key_exists( 'IsManagerApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_manager_approved = ' . $this->sqlIsManagerApproved() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'payroll_period_id' => $this->getPayrollPeriodId(),
			'employee_vacation_request_id' => $this->getEmployeeVacationRequestId(),
			'employee_hour_type_id' => $this->getEmployeeHourTypeId(),
			'date' => $this->getDate(),
			'begin_datetime' => $this->getBeginDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'proposed_time' => $this->getProposedTime(),
			'is_manually_added' => $this->getIsManuallyAdded(),
			'is_manager_approved' => $this->getIsManagerApproved(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>