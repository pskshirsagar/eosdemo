<?php

class CBaseEmployeeApplicationStep extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_application_steps';

	protected $m_intId;
	protected $m_intEmployeeApplicationId;
	protected $m_intPsJobPostingStepStatusId;
	protected $m_intPurchaseRequestId;
	protected $m_strComments;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intStepUpdatedByEmployeeId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationId', trim( $arrValues['employee_application_id'] ) ); elseif( isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( isset( $arrValues['ps_job_posting_step_status_id'] ) && $boolDirectSet ) $this->set( 'm_intPsJobPostingStepStatusId', trim( $arrValues['ps_job_posting_step_status_id'] ) ); elseif( isset( $arrValues['ps_job_posting_step_status_id'] ) ) $this->setPsJobPostingStepStatusId( $arrValues['ps_job_posting_step_status_id'] );
		if( isset( $arrValues['purchase_request_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchaseRequestId', trim( $arrValues['purchase_request_id'] ) ); elseif( isset( $arrValues['purchase_request_id'] ) ) $this->setPurchaseRequestId( $arrValues['purchase_request_id'] );
		if( isset( $arrValues['comments'] ) && $boolDirectSet ) $this->set( 'm_strComments', trim( stripcslashes( $arrValues['comments'] ) ) ); elseif( isset( $arrValues['comments'] ) ) $this->setComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['comments'] ) : $arrValues['comments'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['step_updated_by_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intStepUpdatedByEmployeeId', trim( $arrValues['step_updated_by_employee_id'] ) ); elseif( isset( $arrValues['step_updated_by_employee_id'] ) ) $this->setStepUpdatedByEmployeeId( $arrValues['step_updated_by_employee_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeApplicationId( $intEmployeeApplicationId ) {
		$this->set( 'm_intEmployeeApplicationId', CStrings::strToIntDef( $intEmployeeApplicationId, NULL, false ) );
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function sqlEmployeeApplicationId() {
		return ( true == isset( $this->m_intEmployeeApplicationId ) ) ? ( string ) $this->m_intEmployeeApplicationId : 'NULL';
	}

	public function setPsJobPostingStepStatusId( $intPsJobPostingStepStatusId ) {
		$this->set( 'm_intPsJobPostingStepStatusId', CStrings::strToIntDef( $intPsJobPostingStepStatusId, NULL, false ) );
	}

	public function getPsJobPostingStepStatusId() {
		return $this->m_intPsJobPostingStepStatusId;
	}

	public function sqlPsJobPostingStepStatusId() {
		return ( true == isset( $this->m_intPsJobPostingStepStatusId ) ) ? ( string ) $this->m_intPsJobPostingStepStatusId : 'NULL';
	}

	public function setPurchaseRequestId( $intPurchaseRequestId ) {
		$this->set( 'm_intPurchaseRequestId', CStrings::strToIntDef( $intPurchaseRequestId, NULL, false ) );
	}

	public function getPurchaseRequestId() {
		return $this->m_intPurchaseRequestId;
	}

	public function sqlPurchaseRequestId() {
		return ( true == isset( $this->m_intPurchaseRequestId ) ) ? ( string ) $this->m_intPurchaseRequestId : 'NULL';
	}

	public function setComments( $strComments ) {
		$this->set( 'm_strComments', CStrings::strTrimDef( $strComments, -1, NULL, true ) );
	}

	public function getComments() {
		return $this->m_strComments;
	}

	public function sqlComments() {
		return ( true == isset( $this->m_strComments ) ) ? '\'' . addslashes( $this->m_strComments ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setStepUpdatedByEmployeeId( $intStepUpdatedByEmployeeId ) {
		$this->set( 'm_intStepUpdatedByEmployeeId', CStrings::strToIntDef( $intStepUpdatedByEmployeeId, NULL, false ) );
	}

	public function getStepUpdatedByEmployeeId() {
		return $this->m_intStepUpdatedByEmployeeId;
	}

	public function sqlStepUpdatedByEmployeeId() {
		return ( true == isset( $this->m_intStepUpdatedByEmployeeId ) ) ? ( string ) $this->m_intStepUpdatedByEmployeeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_application_id, ps_job_posting_step_status_id, purchase_request_id, comments, created_by, created_on, step_updated_by_employee_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeApplicationId() . ', ' .
 						$this->sqlPsJobPostingStepStatusId() . ', ' .
 						$this->sqlPurchaseRequestId() . ', ' .
 						$this->sqlComments() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlStepUpdatedByEmployeeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_job_posting_step_status_id = ' . $this->sqlPsJobPostingStepStatusId() . ','; } elseif( true == array_key_exists( 'PsJobPostingStepStatusId', $this->getChangedColumns() ) ) { $strSql .= ' ps_job_posting_step_status_id = ' . $this->sqlPsJobPostingStepStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; } elseif( true == array_key_exists( 'PurchaseRequestId', $this->getChangedColumns() ) ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comments = ' . $this->sqlComments() . ','; } elseif( true == array_key_exists( 'Comments', $this->getChangedColumns() ) ) { $strSql .= ' comments = ' . $this->sqlComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' step_updated_by_employee_id = ' . $this->sqlStepUpdatedByEmployeeId() . ','; } elseif( true == array_key_exists( 'StepUpdatedByEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' step_updated_by_employee_id = ' . $this->sqlStepUpdatedByEmployeeId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_application_id' => $this->getEmployeeApplicationId(),
			'ps_job_posting_step_status_id' => $this->getPsJobPostingStepStatusId(),
			'purchase_request_id' => $this->getPurchaseRequestId(),
			'comments' => $this->getComments(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'step_updated_by_employee_id' => $this->getStepUpdatedByEmployeeId()
		);
	}

}
?>