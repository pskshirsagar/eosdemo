<?php

class CBaseCompanyPayment extends CEosSingularBase {

	use TEosDetails;

	use TEosPostalAddresses;

	const TABLE_NAME = 'public.company_payments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intDepositId;
	protected $m_strCurrencyCode;
	protected $m_intPsProcessingBankAccountId;
	protected $m_intIntermediaryProcessingBankAccountId;
	protected $m_intMerchantGatewayId;
	protected $m_intPaymentTypeId;
	protected $m_intPaymentStatusTypeId;
	protected $m_intPsProductId;
	protected $m_intInvoiceId;
	protected $m_intReturnTypeId;
	protected $m_intNachaReturnAddendaDetailRecordId;
	protected $m_intCompanyUserId;
	protected $m_intReferenceNumber;
	protected $m_strPaymentDatetime;
	protected $m_fltPaymentAmount;
	protected $m_strPaymentMemo;
	protected $m_strCcNameOnCard;
	protected $m_intSecureReferenceNumber;
	protected $m_strCcCardNumberEncrypted;
	protected $m_strCcExpDateMonth;
	protected $m_strCcExpDateYear;
	protected $m_intCheckAccountTypeId;
	protected $m_strCheckNameOnAccount;
	protected $m_strCheckDate;
	protected $m_strCheckNumber;
	protected $m_strCheckPayableTo;
	protected $m_strCheckBankName;
	protected $m_strCheckRoutingNumber;
	protected $m_strCheckAccountNumberEncrypted;
	protected $m_strBilltoIpAddress;
	protected $m_strBilltoStreetLine1;
	protected $m_strBilltoStreetLine2;
	protected $m_strBilltoCity;
	protected $m_strBilltoStateCode;
	protected $m_strBilltoProvince;
	protected $m_strBilltoPostalCode;
	protected $m_strBilltoCountryCode;
	protected $m_strReturnedOn;
	protected $m_strBatchedOn;
	protected $m_strEftBatchedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intInvoiceBatchId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strCheckAccountNumberBindex;
	protected $m_intCardTypeId;
	protected $m_boolIsBillingInfoStored;
	private $m_arrstrPostalAddressFields;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';
		$this->m_boolIsBillingInfoStored = false;
		$this->m_arrstrPostalAddressFields = [
			 'default' => [
				'm_strBilltoStreetLine1' => 'addressLine1',
				'm_strBilltoStreetLine2' => 'addressLine2',
				'm_strBilltoCity' => 'locality',
				'm_strBilltoStateCode' => 'administrativeArea',
				'm_strBilltoPostalCode' => 'postalCode',
				'm_strBilltoCountryCode' => 'country'
			]
		 ];

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['deposit_id'] ) && $boolDirectSet ) $this->set( 'm_intDepositId', trim( $arrValues['deposit_id'] ) ); elseif( isset( $arrValues['deposit_id'] ) ) $this->setDepositId( $arrValues['deposit_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( $arrValues['currency_code'] ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( $arrValues['currency_code'] );
		if( isset( $arrValues['ps_processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProcessingBankAccountId', trim( $arrValues['ps_processing_bank_account_id'] ) ); elseif( isset( $arrValues['ps_processing_bank_account_id'] ) ) $this->setPsProcessingBankAccountId( $arrValues['ps_processing_bank_account_id'] );
		if( isset( $arrValues['intermediary_processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intIntermediaryProcessingBankAccountId', trim( $arrValues['intermediary_processing_bank_account_id'] ) ); elseif( isset( $arrValues['intermediary_processing_bank_account_id'] ) ) $this->setIntermediaryProcessingBankAccountId( $arrValues['intermediary_processing_bank_account_id'] );
		if( isset( $arrValues['merchant_gateway_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantGatewayId', trim( $arrValues['merchant_gateway_id'] ) ); elseif( isset( $arrValues['merchant_gateway_id'] ) ) $this->setMerchantGatewayId( $arrValues['merchant_gateway_id'] );
		if( isset( $arrValues['payment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTypeId', trim( $arrValues['payment_type_id'] ) ); elseif( isset( $arrValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrValues['payment_type_id'] );
		if( isset( $arrValues['payment_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentStatusTypeId', trim( $arrValues['payment_status_type_id'] ) ); elseif( isset( $arrValues['payment_status_type_id'] ) ) $this->setPaymentStatusTypeId( $arrValues['payment_status_type_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceId', trim( $arrValues['invoice_id'] ) ); elseif( isset( $arrValues['invoice_id'] ) ) $this->setInvoiceId( $arrValues['invoice_id'] );
		if( isset( $arrValues['return_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReturnTypeId', trim( $arrValues['return_type_id'] ) ); elseif( isset( $arrValues['return_type_id'] ) ) $this->setReturnTypeId( $arrValues['return_type_id'] );
		if( isset( $arrValues['nacha_return_addenda_detail_record_id'] ) && $boolDirectSet ) $this->set( 'm_intNachaReturnAddendaDetailRecordId', trim( $arrValues['nacha_return_addenda_detail_record_id'] ) ); elseif( isset( $arrValues['nacha_return_addenda_detail_record_id'] ) ) $this->setNachaReturnAddendaDetailRecordId( $arrValues['nacha_return_addenda_detail_record_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['reference_number'] ) && $boolDirectSet ) $this->set( 'm_intReferenceNumber', trim( $arrValues['reference_number'] ) ); elseif( isset( $arrValues['reference_number'] ) ) $this->setReferenceNumber( $arrValues['reference_number'] );
		if( isset( $arrValues['payment_datetime'] ) && $boolDirectSet ) $this->set( 'm_strPaymentDatetime', trim( $arrValues['payment_datetime'] ) ); elseif( isset( $arrValues['payment_datetime'] ) ) $this->setPaymentDatetime( $arrValues['payment_datetime'] );
		if( isset( $arrValues['payment_amount'] ) && $boolDirectSet ) $this->set( 'm_fltPaymentAmount', trim( $arrValues['payment_amount'] ) ); elseif( isset( $arrValues['payment_amount'] ) ) $this->setPaymentAmount( $arrValues['payment_amount'] );
		if( isset( $arrValues['payment_memo'] ) && $boolDirectSet ) $this->set( 'm_strPaymentMemo', trim( $arrValues['payment_memo'] ) ); elseif( isset( $arrValues['payment_memo'] ) ) $this->setPaymentMemo( $arrValues['payment_memo'] );
		if( isset( $arrValues['cc_name_on_card'] ) && $boolDirectSet ) $this->set( 'm_strCcNameOnCard', trim( $arrValues['cc_name_on_card'] ) ); elseif( isset( $arrValues['cc_name_on_card'] ) ) $this->setCcNameOnCard( $arrValues['cc_name_on_card'] );
		if( isset( $arrValues['secure_reference_number'] ) && $boolDirectSet ) $this->set( 'm_intSecureReferenceNumber', trim( $arrValues['secure_reference_number'] ) ); elseif( isset( $arrValues['secure_reference_number'] ) ) $this->setSecureReferenceNumber( $arrValues['secure_reference_number'] );
		if( isset( $arrValues['cc_card_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCcCardNumberEncrypted', trim( $arrValues['cc_card_number_encrypted'] ) ); elseif( isset( $arrValues['cc_card_number_encrypted'] ) ) $this->setCcCardNumberEncrypted( $arrValues['cc_card_number_encrypted'] );
		if( isset( $arrValues['cc_exp_date_month'] ) && $boolDirectSet ) $this->set( 'm_strCcExpDateMonth', trim( $arrValues['cc_exp_date_month'] ) ); elseif( isset( $arrValues['cc_exp_date_month'] ) ) $this->setCcExpDateMonth( $arrValues['cc_exp_date_month'] );
		if( isset( $arrValues['cc_exp_date_year'] ) && $boolDirectSet ) $this->set( 'm_strCcExpDateYear', trim( $arrValues['cc_exp_date_year'] ) ); elseif( isset( $arrValues['cc_exp_date_year'] ) ) $this->setCcExpDateYear( $arrValues['cc_exp_date_year'] );
		if( isset( $arrValues['check_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCheckAccountTypeId', trim( $arrValues['check_account_type_id'] ) ); elseif( isset( $arrValues['check_account_type_id'] ) ) $this->setCheckAccountTypeId( $arrValues['check_account_type_id'] );
		if( isset( $arrValues['check_name_on_account'] ) && $boolDirectSet ) $this->set( 'm_strCheckNameOnAccount', trim( $arrValues['check_name_on_account'] ) ); elseif( isset( $arrValues['check_name_on_account'] ) ) $this->setCheckNameOnAccount( $arrValues['check_name_on_account'] );
		if( isset( $arrValues['check_date'] ) && $boolDirectSet ) $this->set( 'm_strCheckDate', trim( $arrValues['check_date'] ) ); elseif( isset( $arrValues['check_date'] ) ) $this->setCheckDate( $arrValues['check_date'] );
		if( isset( $arrValues['check_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckNumber', trim( $arrValues['check_number'] ) ); elseif( isset( $arrValues['check_number'] ) ) $this->setCheckNumber( $arrValues['check_number'] );
		if( isset( $arrValues['check_payable_to'] ) && $boolDirectSet ) $this->set( 'm_strCheckPayableTo', trim( $arrValues['check_payable_to'] ) ); elseif( isset( $arrValues['check_payable_to'] ) ) $this->setCheckPayableTo( $arrValues['check_payable_to'] );
		if( isset( $arrValues['check_bank_name'] ) && $boolDirectSet ) $this->set( 'm_strCheckBankName', trim( $arrValues['check_bank_name'] ) ); elseif( isset( $arrValues['check_bank_name'] ) ) $this->setCheckBankName( $arrValues['check_bank_name'] );
		if( isset( $arrValues['check_routing_number'] ) && $boolDirectSet ) $this->set( 'm_strCheckRoutingNumber', trim( $arrValues['check_routing_number'] ) ); elseif( isset( $arrValues['check_routing_number'] ) ) $this->setCheckRoutingNumber( $arrValues['check_routing_number'] );
		if( isset( $arrValues['check_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberEncrypted', trim( $arrValues['check_account_number_encrypted'] ) ); elseif( isset( $arrValues['check_account_number_encrypted'] ) ) $this->setCheckAccountNumberEncrypted( $arrValues['check_account_number_encrypted'] );
		if( isset( $arrValues['billto_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strBilltoIpAddress', trim( $arrValues['billto_ip_address'] ) ); elseif( isset( $arrValues['billto_ip_address'] ) ) $this->setBilltoIpAddress( $arrValues['billto_ip_address'] );
		if( isset( $arrValues['billto_street_line1'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoStreetLine1', trim( $arrValues['billto_street_line1'] ) ); elseif( isset( $arrValues['billto_street_line1'] ) ) $this->setBilltoStreetLine1( $arrValues['billto_street_line1'] );
		if( isset( $arrValues['billto_street_line2'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoStreetLine2', trim( $arrValues['billto_street_line2'] ) ); elseif( isset( $arrValues['billto_street_line2'] ) ) $this->setBilltoStreetLine2( $arrValues['billto_street_line2'] );
		if( isset( $arrValues['billto_city'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoCity', trim( $arrValues['billto_city'] ) ); elseif( isset( $arrValues['billto_city'] ) ) $this->setBilltoCity( $arrValues['billto_city'] );
		if( isset( $arrValues['billto_state_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoStateCode', trim( $arrValues['billto_state_code'] ) ); elseif( isset( $arrValues['billto_state_code'] ) ) $this->setBilltoStateCode( $arrValues['billto_state_code'] );
		if( isset( $arrValues['billto_province'] ) && $boolDirectSet ) $this->set( 'm_strBilltoProvince', trim( $arrValues['billto_province'] ) ); elseif( isset( $arrValues['billto_province'] ) ) $this->setBilltoProvince( $arrValues['billto_province'] );
		if( isset( $arrValues['billto_postal_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoPostalCode', trim( $arrValues['billto_postal_code'] ) ); elseif( isset( $arrValues['billto_postal_code'] ) ) $this->setBilltoPostalCode( $arrValues['billto_postal_code'] );
		if( isset( $arrValues['billto_country_code'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strBilltoCountryCode', trim( $arrValues['billto_country_code'] ) ); elseif( isset( $arrValues['billto_country_code'] ) ) $this->setBilltoCountryCode( $arrValues['billto_country_code'] );
		if( isset( $arrValues['returned_on'] ) && $boolDirectSet ) $this->set( 'm_strReturnedOn', trim( $arrValues['returned_on'] ) ); elseif( isset( $arrValues['returned_on'] ) ) $this->setReturnedOn( $arrValues['returned_on'] );
		if( isset( $arrValues['batched_on'] ) && $boolDirectSet ) $this->set( 'm_strBatchedOn', trim( $arrValues['batched_on'] ) ); elseif( isset( $arrValues['batched_on'] ) ) $this->setBatchedOn( $arrValues['batched_on'] );
		if( isset( $arrValues['eft_batched_on'] ) && $boolDirectSet ) $this->set( 'm_strEftBatchedOn', trim( $arrValues['eft_batched_on'] ) ); elseif( isset( $arrValues['eft_batched_on'] ) ) $this->setEftBatchedOn( $arrValues['eft_batched_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['invoice_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceBatchId', trim( $arrValues['invoice_batch_id'] ) ); elseif( isset( $arrValues['invoice_batch_id'] ) ) $this->setInvoiceBatchId( $arrValues['invoice_batch_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['check_account_number_bindex'] ) && $boolDirectSet ) $this->set( 'm_strCheckAccountNumberBindex', trim( $arrValues['check_account_number_bindex'] ) ); elseif( isset( $arrValues['check_account_number_bindex'] ) ) $this->setCheckAccountNumberBindex( $arrValues['check_account_number_bindex'] );
		if( isset( $arrValues['card_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCardTypeId', trim( $arrValues['card_type_id'] ) ); elseif( isset( $arrValues['card_type_id'] ) ) $this->setCardTypeId( $arrValues['card_type_id'] );
		if( isset( $arrValues['is_billing_info_stored'] ) && $boolDirectSet ) $this->set( 'm_boolIsBillingInfoStored', trim( stripcslashes( $arrValues['is_billing_info_stored'] ) ) ); elseif( isset( $arrValues['is_billing_info_stored'] ) ) $this->setIsBillingInfoStored( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_billing_info_stored'] ) : $arrValues['is_billing_info_stored'] );
		if( $this->m_boolInitialized ) {
			$this->setPostalAddressValues( $arrValues, $this->m_arrstrPostalAddressFields );
		}

		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setDepositId( $intDepositId ) {
		$this->set( 'm_intDepositId', CStrings::strToIntDef( $intDepositId, NULL, false ) );
	}

	public function getDepositId() {
		return $this->m_intDepositId;
	}

	public function sqlDepositId() {
		return ( true == isset( $this->m_intDepositId ) ) ? ( string ) $this->m_intDepositId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCurrencyCode ) : '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' ) : '\'USD\'';
	}

	public function setPsProcessingBankAccountId( $intPsProcessingBankAccountId ) {
		$this->set( 'm_intPsProcessingBankAccountId', CStrings::strToIntDef( $intPsProcessingBankAccountId, NULL, false ) );
	}

	public function getPsProcessingBankAccountId() {
		return $this->m_intPsProcessingBankAccountId;
	}

	public function sqlPsProcessingBankAccountId() {
		return ( true == isset( $this->m_intPsProcessingBankAccountId ) ) ? ( string ) $this->m_intPsProcessingBankAccountId : 'NULL';
	}

	public function setIntermediaryProcessingBankAccountId( $intIntermediaryProcessingBankAccountId ) {
		$this->set( 'm_intIntermediaryProcessingBankAccountId', CStrings::strToIntDef( $intIntermediaryProcessingBankAccountId, NULL, false ) );
	}

	public function getIntermediaryProcessingBankAccountId() {
		return $this->m_intIntermediaryProcessingBankAccountId;
	}

	public function sqlIntermediaryProcessingBankAccountId() {
		return ( true == isset( $this->m_intIntermediaryProcessingBankAccountId ) ) ? ( string ) $this->m_intIntermediaryProcessingBankAccountId : 'NULL';
	}

	public function setMerchantGatewayId( $intMerchantGatewayId ) {
		$this->set( 'm_intMerchantGatewayId', CStrings::strToIntDef( $intMerchantGatewayId, NULL, false ) );
	}

	public function getMerchantGatewayId() {
		return $this->m_intMerchantGatewayId;
	}

	public function sqlMerchantGatewayId() {
		return ( true == isset( $this->m_intMerchantGatewayId ) ) ? ( string ) $this->m_intMerchantGatewayId : 'NULL';
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->set( 'm_intPaymentTypeId', CStrings::strToIntDef( $intPaymentTypeId, NULL, false ) );
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	public function sqlPaymentTypeId() {
		return ( true == isset( $this->m_intPaymentTypeId ) ) ? ( string ) $this->m_intPaymentTypeId : 'NULL';
	}

	public function setPaymentStatusTypeId( $intPaymentStatusTypeId ) {
		$this->set( 'm_intPaymentStatusTypeId', CStrings::strToIntDef( $intPaymentStatusTypeId, NULL, false ) );
	}

	public function getPaymentStatusTypeId() {
		return $this->m_intPaymentStatusTypeId;
	}

	public function sqlPaymentStatusTypeId() {
		return ( true == isset( $this->m_intPaymentStatusTypeId ) ) ? ( string ) $this->m_intPaymentStatusTypeId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setInvoiceId( $intInvoiceId ) {
		$this->set( 'm_intInvoiceId', CStrings::strToIntDef( $intInvoiceId, NULL, false ) );
	}

	public function getInvoiceId() {
		return $this->m_intInvoiceId;
	}

	public function sqlInvoiceId() {
		return ( true == isset( $this->m_intInvoiceId ) ) ? ( string ) $this->m_intInvoiceId : 'NULL';
	}

	public function setReturnTypeId( $intReturnTypeId ) {
		$this->set( 'm_intReturnTypeId', CStrings::strToIntDef( $intReturnTypeId, NULL, false ) );
	}

	public function getReturnTypeId() {
		return $this->m_intReturnTypeId;
	}

	public function sqlReturnTypeId() {
		return ( true == isset( $this->m_intReturnTypeId ) ) ? ( string ) $this->m_intReturnTypeId : 'NULL';
	}

	public function setNachaReturnAddendaDetailRecordId( $intNachaReturnAddendaDetailRecordId ) {
		$this->set( 'm_intNachaReturnAddendaDetailRecordId', CStrings::strToIntDef( $intNachaReturnAddendaDetailRecordId, NULL, false ) );
	}

	public function getNachaReturnAddendaDetailRecordId() {
		return $this->m_intNachaReturnAddendaDetailRecordId;
	}

	public function sqlNachaReturnAddendaDetailRecordId() {
		return ( true == isset( $this->m_intNachaReturnAddendaDetailRecordId ) ) ? ( string ) $this->m_intNachaReturnAddendaDetailRecordId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setReferenceNumber( $intReferenceNumber ) {
		$this->set( 'm_intReferenceNumber', CStrings::strToIntDef( $intReferenceNumber, NULL, false ) );
	}

	public function getReferenceNumber() {
		return $this->m_intReferenceNumber;
	}

	public function sqlReferenceNumber() {
		return ( true == isset( $this->m_intReferenceNumber ) ) ? ( string ) $this->m_intReferenceNumber : 'NULL';
	}

	public function setPaymentDatetime( $strPaymentDatetime ) {
		$this->set( 'm_strPaymentDatetime', CStrings::strTrimDef( $strPaymentDatetime, -1, NULL, true ) );
	}

	public function getPaymentDatetime() {
		return $this->m_strPaymentDatetime;
	}

	public function sqlPaymentDatetime() {
		return ( true == isset( $this->m_strPaymentDatetime ) ) ? '\'' . $this->m_strPaymentDatetime . '\'' : 'NOW()';
	}

	public function setPaymentAmount( $fltPaymentAmount ) {
		$this->set( 'm_fltPaymentAmount', CStrings::strToFloatDef( $fltPaymentAmount, NULL, false, 2 ) );
	}

	public function getPaymentAmount() {
		return $this->m_fltPaymentAmount;
	}

	public function sqlPaymentAmount() {
		return ( true == isset( $this->m_fltPaymentAmount ) ) ? ( string ) $this->m_fltPaymentAmount : 'NULL';
	}

	public function setPaymentMemo( $strPaymentMemo ) {
		$this->set( 'm_strPaymentMemo', CStrings::strTrimDef( $strPaymentMemo, 2000, NULL, true ) );
	}

	public function getPaymentMemo() {
		return $this->m_strPaymentMemo;
	}

	public function sqlPaymentMemo() {
		return ( true == isset( $this->m_strPaymentMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPaymentMemo ) : '\'' . addslashes( $this->m_strPaymentMemo ) . '\'' ) : 'NULL';
	}

	public function setCcNameOnCard( $strCcNameOnCard ) {
		$this->set( 'm_strCcNameOnCard', CStrings::strTrimDef( $strCcNameOnCard, 50, NULL, true ) );
	}

	public function getCcNameOnCard() {
		return $this->m_strCcNameOnCard;
	}

	public function sqlCcNameOnCard() {
		return ( true == isset( $this->m_strCcNameOnCard ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcNameOnCard ) : '\'' . addslashes( $this->m_strCcNameOnCard ) . '\'' ) : 'NULL';
	}

	public function setSecureReferenceNumber( $intSecureReferenceNumber ) {
		$this->set( 'm_intSecureReferenceNumber', CStrings::strToIntDef( $intSecureReferenceNumber, NULL, false ) );
	}

	public function getSecureReferenceNumber() {
		return $this->m_intSecureReferenceNumber;
	}

	public function sqlSecureReferenceNumber() {
		return ( true == isset( $this->m_intSecureReferenceNumber ) ) ? ( string ) $this->m_intSecureReferenceNumber : 'NULL';
	}

	public function setCcCardNumberEncrypted( $strCcCardNumberEncrypted ) {
		$this->set( 'm_strCcCardNumberEncrypted', CStrings::strTrimDef( $strCcCardNumberEncrypted, 240, NULL, true ) );
	}

	public function getCcCardNumberEncrypted() {
		return $this->m_strCcCardNumberEncrypted;
	}

	public function sqlCcCardNumberEncrypted() {
		return ( true == isset( $this->m_strCcCardNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcCardNumberEncrypted ) : '\'' . addslashes( $this->m_strCcCardNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setCcExpDateMonth( $strCcExpDateMonth ) {
		$this->set( 'm_strCcExpDateMonth', CStrings::strTrimDef( $strCcExpDateMonth, 2, NULL, true ) );
	}

	public function getCcExpDateMonth() {
		return $this->m_strCcExpDateMonth;
	}

	public function sqlCcExpDateMonth() {
		return ( true == isset( $this->m_strCcExpDateMonth ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcExpDateMonth ) : '\'' . addslashes( $this->m_strCcExpDateMonth ) . '\'' ) : 'NULL';
	}

	public function setCcExpDateYear( $strCcExpDateYear ) {
		$this->set( 'm_strCcExpDateYear', CStrings::strTrimDef( $strCcExpDateYear, 4, NULL, true ) );
	}

	public function getCcExpDateYear() {
		return $this->m_strCcExpDateYear;
	}

	public function sqlCcExpDateYear() {
		return ( true == isset( $this->m_strCcExpDateYear ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCcExpDateYear ) : '\'' . addslashes( $this->m_strCcExpDateYear ) . '\'' ) : 'NULL';
	}

	public function setCheckAccountTypeId( $intCheckAccountTypeId ) {
		$this->set( 'm_intCheckAccountTypeId', CStrings::strToIntDef( $intCheckAccountTypeId, NULL, false ) );
	}

	public function getCheckAccountTypeId() {
		return $this->m_intCheckAccountTypeId;
	}

	public function sqlCheckAccountTypeId() {
		return ( true == isset( $this->m_intCheckAccountTypeId ) ) ? ( string ) $this->m_intCheckAccountTypeId : 'NULL';
	}

	public function setCheckNameOnAccount( $strCheckNameOnAccount ) {
		$this->set( 'm_strCheckNameOnAccount', CStrings::strTrimDef( $strCheckNameOnAccount, 50, NULL, true ) );
	}

	public function getCheckNameOnAccount() {
		return $this->m_strCheckNameOnAccount;
	}

	public function sqlCheckNameOnAccount() {
		return ( true == isset( $this->m_strCheckNameOnAccount ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckNameOnAccount ) : '\'' . addslashes( $this->m_strCheckNameOnAccount ) . '\'' ) : 'NULL';
	}

	public function setCheckDate( $strCheckDate ) {
		$this->set( 'm_strCheckDate', CStrings::strTrimDef( $strCheckDate, -1, NULL, true ) );
	}

	public function getCheckDate() {
		return $this->m_strCheckDate;
	}

	public function sqlCheckDate() {
		return ( true == isset( $this->m_strCheckDate ) ) ? '\'' . $this->m_strCheckDate . '\'' : 'NULL';
	}

	public function setCheckNumber( $strCheckNumber ) {
		$this->set( 'm_strCheckNumber', CStrings::strTrimDef( $strCheckNumber, 20, NULL, true ) );
	}

	public function getCheckNumber() {
		return $this->m_strCheckNumber;
	}

	public function sqlCheckNumber() {
		return ( true == isset( $this->m_strCheckNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckNumber ) : '\'' . addslashes( $this->m_strCheckNumber ) . '\'' ) : 'NULL';
	}

	public function setCheckPayableTo( $strCheckPayableTo ) {
		$this->set( 'm_strCheckPayableTo', CStrings::strTrimDef( $strCheckPayableTo, 240, NULL, true ) );
	}

	public function getCheckPayableTo() {
		return $this->m_strCheckPayableTo;
	}

	public function sqlCheckPayableTo() {
		return ( true == isset( $this->m_strCheckPayableTo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckPayableTo ) : '\'' . addslashes( $this->m_strCheckPayableTo ) . '\'' ) : 'NULL';
	}

	public function setCheckBankName( $strCheckBankName ) {
		$this->set( 'm_strCheckBankName', CStrings::strTrimDef( $strCheckBankName, 100, NULL, true ) );
	}

	public function getCheckBankName() {
		return $this->m_strCheckBankName;
	}

	public function sqlCheckBankName() {
		return ( true == isset( $this->m_strCheckBankName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckBankName ) : '\'' . addslashes( $this->m_strCheckBankName ) . '\'' ) : 'NULL';
	}

	public function setCheckRoutingNumber( $strCheckRoutingNumber ) {
		$this->set( 'm_strCheckRoutingNumber', CStrings::strTrimDef( $strCheckRoutingNumber, 240, NULL, true ) );
	}

	public function getCheckRoutingNumber() {
		return $this->m_strCheckRoutingNumber;
	}

	public function sqlCheckRoutingNumber() {
		return ( true == isset( $this->m_strCheckRoutingNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckRoutingNumber ) : '\'' . addslashes( $this->m_strCheckRoutingNumber ) . '\'' ) : 'NULL';
	}

	public function setCheckAccountNumberEncrypted( $strCheckAccountNumberEncrypted ) {
		$this->set( 'm_strCheckAccountNumberEncrypted', CStrings::strTrimDef( $strCheckAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getCheckAccountNumberEncrypted() {
		return $this->m_strCheckAccountNumberEncrypted;
	}

	public function sqlCheckAccountNumberEncrypted() {
		return ( true == isset( $this->m_strCheckAccountNumberEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckAccountNumberEncrypted ) : '\'' . addslashes( $this->m_strCheckAccountNumberEncrypted ) . '\'' ) : 'NULL';
	}

	public function setBilltoIpAddress( $strBilltoIpAddress ) {
		$this->set( 'm_strBilltoIpAddress', CStrings::strTrimDef( $strBilltoIpAddress, 23, NULL, true ) );
	}

	public function getBilltoIpAddress() {
		return $this->m_strBilltoIpAddress;
	}

	public function sqlBilltoIpAddress() {
		return ( true == isset( $this->m_strBilltoIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoIpAddress ) : '\'' . addslashes( $this->m_strBilltoIpAddress ) . '\'' ) : 'NULL';
	}

	public function setBilltoStreetLine1( $strBilltoStreetLine1 ) {
		$this->setPostalAddressField( 'addressLine1', $strBilltoStreetLine1, $strAddressKey = 'default', 'm_strBilltoStreetLine1' );
	}

	public function getBilltoStreetLine1() {
		return $this->getPostalAddressField( 'addressLine1', 'default', 'm_strBilltoStreetLine1' );
	}

	public function sqlBilltoStreetLine1() {
		return ( true == isset( $this->m_strBilltoStreetLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoStreetLine1 ) : '\'' . addslashes( $this->m_strBilltoStreetLine1 ) . '\'' ) : 'NULL';
	}

	public function setBilltoStreetLine2( $strBilltoStreetLine2 ) {
		$this->setPostalAddressField( 'addressLine2', $strBilltoStreetLine2, $strAddressKey = 'default', 'm_strBilltoStreetLine2' );
	}

	public function getBilltoStreetLine2() {
		return $this->getPostalAddressField( 'addressLine2', 'default', 'm_strBilltoStreetLine2' );
	}

	public function sqlBilltoStreetLine2() {
		return ( true == isset( $this->m_strBilltoStreetLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoStreetLine2 ) : '\'' . addslashes( $this->m_strBilltoStreetLine2 ) . '\'' ) : 'NULL';
	}

	public function setBilltoCity( $strBilltoCity ) {
		$this->setPostalAddressField( 'locality', $strBilltoCity, $strAddressKey = 'default', 'm_strBilltoCity' );
	}

	public function getBilltoCity() {
		return $this->getPostalAddressField( 'locality', 'default', 'm_strBilltoCity' );
	}

	public function sqlBilltoCity() {
		return ( true == isset( $this->m_strBilltoCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoCity ) : '\'' . addslashes( $this->m_strBilltoCity ) . '\'' ) : 'NULL';
	}

	public function setBilltoStateCode( $strBilltoStateCode ) {
		$this->setPostalAddressField( 'administrativeArea', $strBilltoStateCode, $strAddressKey = 'default', 'm_strBilltoStateCode' );
	}

	public function getBilltoStateCode() {
		return $this->getPostalAddressField( 'administrativeArea', 'default', 'm_strBilltoStateCode' );
	}

	public function sqlBilltoStateCode() {
		return ( true == isset( $this->m_strBilltoStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoStateCode ) : '\'' . addslashes( $this->m_strBilltoStateCode ) . '\'' ) : 'NULL';
	}

	public function setBilltoProvince( $strBilltoProvince ) {
		$this->set( 'm_strBilltoProvince', CStrings::strTrimDef( $strBilltoProvince, 50, NULL, true ) );
	}

	public function getBilltoProvince() {
		return $this->m_strBilltoProvince;
	}

	public function sqlBilltoProvince() {
		return ( true == isset( $this->m_strBilltoProvince ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoProvince ) : '\'' . addslashes( $this->m_strBilltoProvince ) . '\'' ) : 'NULL';
	}

	public function setBilltoPostalCode( $strBilltoPostalCode ) {
		$this->setPostalAddressField( 'postalCode', $strBilltoPostalCode, $strAddressKey = 'default', 'm_strBilltoPostalCode' );
	}

	public function getBilltoPostalCode() {
		return $this->getPostalAddressField( 'postalCode', 'default', 'm_strBilltoPostalCode' );
	}

	public function sqlBilltoPostalCode() {
		return ( true == isset( $this->m_strBilltoPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoPostalCode ) : '\'' . addslashes( $this->m_strBilltoPostalCode ) . '\'' ) : 'NULL';
	}

	public function setBilltoCountryCode( $strBilltoCountryCode ) {
		$this->setPostalAddressField( 'country', $strBilltoCountryCode, $strAddressKey = 'default', 'm_strBilltoCountryCode' );
	}

	public function getBilltoCountryCode() {
		return $this->getPostalAddressField( 'country', 'default', 'm_strBilltoCountryCode' );
	}

	public function sqlBilltoCountryCode() {
		return ( true == isset( $this->m_strBilltoCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strBilltoCountryCode ) : '\'' . addslashes( $this->m_strBilltoCountryCode ) . '\'' ) : 'NULL';
	}

	public function setReturnedOn( $strReturnedOn ) {
		$this->set( 'm_strReturnedOn', CStrings::strTrimDef( $strReturnedOn, -1, NULL, true ) );
	}

	public function getReturnedOn() {
		return $this->m_strReturnedOn;
	}

	public function sqlReturnedOn() {
		return ( true == isset( $this->m_strReturnedOn ) ) ? '\'' . $this->m_strReturnedOn . '\'' : 'NULL';
	}

	public function setBatchedOn( $strBatchedOn ) {
		$this->set( 'm_strBatchedOn', CStrings::strTrimDef( $strBatchedOn, -1, NULL, true ) );
	}

	public function getBatchedOn() {
		return $this->m_strBatchedOn;
	}

	public function sqlBatchedOn() {
		return ( true == isset( $this->m_strBatchedOn ) ) ? '\'' . $this->m_strBatchedOn . '\'' : 'NULL';
	}

	public function setEftBatchedOn( $strEftBatchedOn ) {
		$this->set( 'm_strEftBatchedOn', CStrings::strTrimDef( $strEftBatchedOn, -1, NULL, true ) );
	}

	public function getEftBatchedOn() {
		return $this->m_strEftBatchedOn;
	}

	public function sqlEftBatchedOn() {
		return ( true == isset( $this->m_strEftBatchedOn ) ) ? '\'' . $this->m_strEftBatchedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setInvoiceBatchId( $intInvoiceBatchId ) {
		$this->set( 'm_intInvoiceBatchId', CStrings::strToIntDef( $intInvoiceBatchId, NULL, false ) );
	}

	public function getInvoiceBatchId() {
		return $this->m_intInvoiceBatchId;
	}

	public function sqlInvoiceBatchId() {
		return ( true == isset( $this->m_intInvoiceBatchId ) ) ? ( string ) $this->m_intInvoiceBatchId : 'NULL';
	}

	public function setCheckAccountNumberBindex( $strCheckAccountNumberBindex ) {
		$this->set( 'm_strCheckAccountNumberBindex', CStrings::strTrimDef( $strCheckAccountNumberBindex, 240, NULL, true ) );
	}

	public function getCheckAccountNumberBindex() {
		return $this->m_strCheckAccountNumberBindex;
	}

	public function sqlCheckAccountNumberBindex() {
		return ( true == isset( $this->m_strCheckAccountNumberBindex ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCheckAccountNumberBindex ) : '\'' . addslashes( $this->m_strCheckAccountNumberBindex ) . '\'' ) : 'NULL';
	}

	public function setCardTypeId( $intCardTypeId ) {
		$this->set( 'm_intCardTypeId', CStrings::strToIntDef( $intCardTypeId, NULL, false ) );
	}

	public function getCardTypeId() {
		return $this->m_intCardTypeId;
	}

	public function sqlCardTypeId() {
		return ( true == isset( $this->m_intCardTypeId ) ) ? ( string ) $this->m_intCardTypeId : 'NULL';
	}

	public function setIsBillingInfoStored( $boolIsBillingInfoStored ) {
		$this->set( 'm_boolIsBillingInfoStored', CStrings::strToBool( $boolIsBillingInfoStored ) );
	}

	public function getIsBillingInfoStored() {
		return $this->m_boolIsBillingInfoStored;
	}

	public function sqlIsBillingInfoStored() {
		return ( true == isset( $this->m_boolIsBillingInfoStored ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsBillingInfoStored ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, deposit_id, currency_code, ps_processing_bank_account_id, intermediary_processing_bank_account_id, merchant_gateway_id, payment_type_id, payment_status_type_id, ps_product_id, invoice_id, return_type_id, nacha_return_addenda_detail_record_id, company_user_id, reference_number, payment_datetime, payment_amount, payment_memo, cc_name_on_card, secure_reference_number, cc_card_number_encrypted, cc_exp_date_month, cc_exp_date_year, check_account_type_id, check_name_on_account, check_date, check_number, check_payable_to, check_bank_name, check_routing_number, check_account_number_encrypted, billto_ip_address, billto_street_line1, billto_street_line2, billto_city, billto_state_code, billto_province, billto_postal_code, billto_country_code, returned_on, batched_on, eft_batched_on, updated_by, updated_on, created_by, created_on, invoice_batch_id, details, check_account_number_bindex, card_type_id, is_billing_info_stored )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlDepositId() . ', ' .
						$this->sqlCurrencyCode() . ', ' .
						$this->sqlPsProcessingBankAccountId() . ', ' .
						$this->sqlIntermediaryProcessingBankAccountId() . ', ' .
						$this->sqlMerchantGatewayId() . ', ' .
						$this->sqlPaymentTypeId() . ', ' .
						$this->sqlPaymentStatusTypeId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlInvoiceId() . ', ' .
						$this->sqlReturnTypeId() . ', ' .
						$this->sqlNachaReturnAddendaDetailRecordId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlReferenceNumber() . ', ' .
						$this->sqlPaymentDatetime() . ', ' .
						$this->sqlPaymentAmount() . ', ' .
						$this->sqlPaymentMemo() . ', ' .
						$this->sqlCcNameOnCard() . ', ' .
						$this->sqlSecureReferenceNumber() . ', ' .
						$this->sqlCcCardNumberEncrypted() . ', ' .
						$this->sqlCcExpDateMonth() . ', ' .
						$this->sqlCcExpDateYear() . ', ' .
						$this->sqlCheckAccountTypeId() . ', ' .
						$this->sqlCheckNameOnAccount() . ', ' .
						$this->sqlCheckDate() . ', ' .
						$this->sqlCheckNumber() . ', ' .
						$this->sqlCheckPayableTo() . ', ' .
						$this->sqlCheckBankName() . ', ' .
						$this->sqlCheckRoutingNumber() . ', ' .
						$this->sqlCheckAccountNumberEncrypted() . ', ' .
						$this->sqlBilltoIpAddress() . ', ' .
						$this->sqlBilltoStreetLine1() . ', ' .
						$this->sqlBilltoStreetLine2() . ', ' .
						$this->sqlBilltoCity() . ', ' .
						$this->sqlBilltoStateCode() . ', ' .
						$this->sqlBilltoProvince() . ', ' .
						$this->sqlBilltoPostalCode() . ', ' .
						$this->sqlBilltoCountryCode() . ', ' .
						$this->sqlReturnedOn() . ', ' .
						$this->sqlBatchedOn() . ', ' .
						$this->sqlEftBatchedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlInvoiceBatchId() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlCheckAccountNumberBindex() . ', ' .
						$this->sqlCardTypeId() . ', ' .
						$this->sqlIsBillingInfoStored() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deposit_id = ' . $this->sqlDepositId(). ',' ; } elseif( true == array_key_exists( 'DepositId', $this->getChangedColumns() ) ) { $strSql .= ' deposit_id = ' . $this->sqlDepositId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_processing_bank_account_id = ' . $this->sqlPsProcessingBankAccountId(). ',' ; } elseif( true == array_key_exists( 'PsProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' ps_processing_bank_account_id = ' . $this->sqlPsProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' intermediary_processing_bank_account_id = ' . $this->sqlIntermediaryProcessingBankAccountId(). ',' ; } elseif( true == array_key_exists( 'IntermediaryProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' intermediary_processing_bank_account_id = ' . $this->sqlIntermediaryProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId(). ',' ; } elseif( true == array_key_exists( 'MerchantGatewayId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_gateway_id = ' . $this->sqlMerchantGatewayId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_type_id = ' . $this->sqlPaymentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_status_type_id = ' . $this->sqlPaymentStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'PaymentStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_status_type_id = ' . $this->sqlPaymentStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_id = ' . $this->sqlInvoiceId(). ',' ; } elseif( true == array_key_exists( 'InvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_id = ' . $this->sqlInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId(). ',' ; } elseif( true == array_key_exists( 'ReturnTypeId', $this->getChangedColumns() ) ) { $strSql .= ' return_type_id = ' . $this->sqlReturnTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nacha_return_addenda_detail_record_id = ' . $this->sqlNachaReturnAddendaDetailRecordId(). ',' ; } elseif( true == array_key_exists( 'NachaReturnAddendaDetailRecordId', $this->getChangedColumns() ) ) { $strSql .= ' nacha_return_addenda_detail_record_id = ' . $this->sqlNachaReturnAddendaDetailRecordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'ReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' reference_number = ' . $this->sqlReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_datetime = ' . $this->sqlPaymentDatetime(). ',' ; } elseif( true == array_key_exists( 'PaymentDatetime', $this->getChangedColumns() ) ) { $strSql .= ' payment_datetime = ' . $this->sqlPaymentDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount(). ',' ; } elseif( true == array_key_exists( 'PaymentAmount', $this->getChangedColumns() ) ) { $strSql .= ' payment_amount = ' . $this->sqlPaymentAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_memo = ' . $this->sqlPaymentMemo(). ',' ; } elseif( true == array_key_exists( 'PaymentMemo', $this->getChangedColumns() ) ) { $strSql .= ' payment_memo = ' . $this->sqlPaymentMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_name_on_card = ' . $this->sqlCcNameOnCard(). ',' ; } elseif( true == array_key_exists( 'CcNameOnCard', $this->getChangedColumns() ) ) { $strSql .= ' cc_name_on_card = ' . $this->sqlCcNameOnCard() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secure_reference_number = ' . $this->sqlSecureReferenceNumber(). ',' ; } elseif( true == array_key_exists( 'SecureReferenceNumber', $this->getChangedColumns() ) ) { $strSql .= ' secure_reference_number = ' . $this->sqlSecureReferenceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_card_number_encrypted = ' . $this->sqlCcCardNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CcCardNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' cc_card_number_encrypted = ' . $this->sqlCcCardNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_exp_date_month = ' . $this->sqlCcExpDateMonth(). ',' ; } elseif( true == array_key_exists( 'CcExpDateMonth', $this->getChangedColumns() ) ) { $strSql .= ' cc_exp_date_month = ' . $this->sqlCcExpDateMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cc_exp_date_year = ' . $this->sqlCcExpDateYear(). ',' ; } elseif( true == array_key_exists( 'CcExpDateYear', $this->getChangedColumns() ) ) { $strSql .= ' cc_exp_date_year = ' . $this->sqlCcExpDateYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId(). ',' ; } elseif( true == array_key_exists( 'CheckAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' check_account_type_id = ' . $this->sqlCheckAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount(). ',' ; } elseif( true == array_key_exists( 'CheckNameOnAccount', $this->getChangedColumns() ) ) { $strSql .= ' check_name_on_account = ' . $this->sqlCheckNameOnAccount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_date = ' . $this->sqlCheckDate(). ',' ; } elseif( true == array_key_exists( 'CheckDate', $this->getChangedColumns() ) ) { $strSql .= ' check_date = ' . $this->sqlCheckDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_number = ' . $this->sqlCheckNumber(). ',' ; } elseif( true == array_key_exists( 'CheckNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_number = ' . $this->sqlCheckNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_payable_to = ' . $this->sqlCheckPayableTo(). ',' ; } elseif( true == array_key_exists( 'CheckPayableTo', $this->getChangedColumns() ) ) { $strSql .= ' check_payable_to = ' . $this->sqlCheckPayableTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName(). ',' ; } elseif( true == array_key_exists( 'CheckBankName', $this->getChangedColumns() ) ) { $strSql .= ' check_bank_name = ' . $this->sqlCheckBankName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber(). ',' ; } elseif( true == array_key_exists( 'CheckRoutingNumber', $this->getChangedColumns() ) ) { $strSql .= ' check_routing_number = ' . $this->sqlCheckRoutingNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_encrypted = ' . $this->sqlCheckAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_ip_address = ' . $this->sqlBilltoIpAddress(). ',' ; } elseif( true == array_key_exists( 'BilltoIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' billto_ip_address = ' . $this->sqlBilltoIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine1', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line1 = ' . $this->sqlBilltoStreetLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_street_line2 = ' . $this->sqlBilltoStreetLine2(). ',' ; } elseif( true == array_key_exists( 'BilltoStreetLine2', $this->getChangedColumns() ) ) { $strSql .= ' billto_street_line2 = ' . $this->sqlBilltoStreetLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity(). ',' ; } elseif( true == array_key_exists( 'BilltoCity', $this->getChangedColumns() ) ) { $strSql .= ' billto_city = ' . $this->sqlBilltoCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode(). ',' ; } elseif( true == array_key_exists( 'BilltoStateCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_state_code = ' . $this->sqlBilltoStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_province = ' . $this->sqlBilltoProvince(). ',' ; } elseif( true == array_key_exists( 'BilltoProvince', $this->getChangedColumns() ) ) { $strSql .= ' billto_province = ' . $this->sqlBilltoProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode(). ',' ; } elseif( true == array_key_exists( 'BilltoPostalCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_postal_code = ' . $this->sqlBilltoPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode(). ',' ; } elseif( true == array_key_exists( 'BilltoCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' billto_country_code = ' . $this->sqlBilltoCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn(). ',' ; } elseif( true == array_key_exists( 'ReturnedOn', $this->getChangedColumns() ) ) { $strSql .= ' returned_on = ' . $this->sqlReturnedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batched_on = ' . $this->sqlBatchedOn(). ',' ; } elseif( true == array_key_exists( 'BatchedOn', $this->getChangedColumns() ) ) { $strSql .= ' batched_on = ' . $this->sqlBatchedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eft_batched_on = ' . $this->sqlEftBatchedOn(). ',' ; } elseif( true == array_key_exists( 'EftBatchedOn', $this->getChangedColumns() ) ) { $strSql .= ' eft_batched_on = ' . $this->sqlEftBatchedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_batch_id = ' . $this->sqlInvoiceBatchId(). ',' ; } elseif( true == array_key_exists( 'InvoiceBatchId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_batch_id = ' . $this->sqlInvoiceBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' check_account_number_bindex = ' . $this->sqlCheckAccountNumberBindex(). ',' ; } elseif( true == array_key_exists( 'CheckAccountNumberBindex', $this->getChangedColumns() ) ) { $strSql .= ' check_account_number_bindex = ' . $this->sqlCheckAccountNumberBindex() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' card_type_id = ' . $this->sqlCardTypeId(). ',' ; } elseif( true == array_key_exists( 'CardTypeId', $this->getChangedColumns() ) ) { $strSql .= ' card_type_id = ' . $this->sqlCardTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_billing_info_stored = ' . $this->sqlIsBillingInfoStored(). ',' ; } elseif( true == array_key_exists( 'IsBillingInfoStored', $this->getChangedColumns() ) ) { $strSql .= ' is_billing_info_stored = ' . $this->sqlIsBillingInfoStored() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'deposit_id' => $this->getDepositId(),
			'currency_code' => $this->getCurrencyCode(),
			'ps_processing_bank_account_id' => $this->getPsProcessingBankAccountId(),
			'intermediary_processing_bank_account_id' => $this->getIntermediaryProcessingBankAccountId(),
			'merchant_gateway_id' => $this->getMerchantGatewayId(),
			'payment_type_id' => $this->getPaymentTypeId(),
			'payment_status_type_id' => $this->getPaymentStatusTypeId(),
			'ps_product_id' => $this->getPsProductId(),
			'invoice_id' => $this->getInvoiceId(),
			'return_type_id' => $this->getReturnTypeId(),
			'nacha_return_addenda_detail_record_id' => $this->getNachaReturnAddendaDetailRecordId(),
			'company_user_id' => $this->getCompanyUserId(),
			'reference_number' => $this->getReferenceNumber(),
			'payment_datetime' => $this->getPaymentDatetime(),
			'payment_amount' => $this->getPaymentAmount(),
			'payment_memo' => $this->getPaymentMemo(),
			'cc_name_on_card' => $this->getCcNameOnCard(),
			'secure_reference_number' => $this->getSecureReferenceNumber(),
			'cc_card_number_encrypted' => $this->getCcCardNumberEncrypted(),
			'cc_exp_date_month' => $this->getCcExpDateMonth(),
			'cc_exp_date_year' => $this->getCcExpDateYear(),
			'check_account_type_id' => $this->getCheckAccountTypeId(),
			'check_name_on_account' => $this->getCheckNameOnAccount(),
			'check_date' => $this->getCheckDate(),
			'check_number' => $this->getCheckNumber(),
			'check_payable_to' => $this->getCheckPayableTo(),
			'check_bank_name' => $this->getCheckBankName(),
			'check_routing_number' => $this->getCheckRoutingNumber(),
			'check_account_number_encrypted' => $this->getCheckAccountNumberEncrypted(),
			'billto_ip_address' => $this->getBilltoIpAddress(),
			'billto_street_line1' => $this->getBilltoStreetLine1(),
			'billto_street_line2' => $this->getBilltoStreetLine2(),
			'billto_city' => $this->getBilltoCity(),
			'billto_state_code' => $this->getBilltoStateCode(),
			'billto_province' => $this->getBilltoProvince(),
			'billto_postal_code' => $this->getBilltoPostalCode(),
			'billto_country_code' => $this->getBilltoCountryCode(),
			'returned_on' => $this->getReturnedOn(),
			'batched_on' => $this->getBatchedOn(),
			'eft_batched_on' => $this->getEftBatchedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'invoice_batch_id' => $this->getInvoiceBatchId(),
			'details' => $this->getDetails(),
			'check_account_number_bindex' => $this->getCheckAccountNumberBindex(),
			'card_type_id' => $this->getCardTypeId(),
			'is_billing_info_stored' => $this->getIsBillingInfoStored()
		);
	}

}
?>