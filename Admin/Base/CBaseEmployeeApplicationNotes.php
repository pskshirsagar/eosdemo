<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationNotes
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationNotes extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationNote[]
	 */
	public static function fetchEmployeeApplicationNotes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeApplicationNote::class, $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationNote
	 */
	public static function fetchEmployeeApplicationNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeApplicationNote::class, $objDatabase );
	}

	public static function fetchEmployeeApplicationNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_notes', $objDatabase );
	}

	public static function fetchEmployeeApplicationNoteById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationNote( sprintf( 'SELECT * FROM employee_application_notes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationNotesByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchEmployeeApplicationNotes( sprintf( 'SELECT * FROM employee_application_notes WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationNotesByNoteTypeId( $intNoteTypeId, $objDatabase ) {
		return self::fetchEmployeeApplicationNotes( sprintf( 'SELECT * FROM employee_application_notes WHERE note_type_id = %d', ( int ) $intNoteTypeId ), $objDatabase );
	}

}
?>