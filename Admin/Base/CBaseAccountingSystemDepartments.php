<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CAccountingSystemDepartments
 * Do not add any new functions to this class.
 */

class CBaseAccountingSystemDepartments extends CEosPluralBase {

	/**
	 * @return CAccountingSystemDepartment[]
	 */
	public static function fetchAccountingSystemDepartments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CAccountingSystemDepartment', $objDatabase );
	}

	/**
	 * @return CAccountingSystemDepartment
	 */
	public static function fetchAccountingSystemDepartment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAccountingSystemDepartment', $objDatabase );
	}

	public static function fetchAccountingSystemDepartmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'accounting_system_departments', $objDatabase );
	}

	public static function fetchAccountingSystemDepartmentById( $intId, $objDatabase ) {
		return self::fetchAccountingSystemDepartment( sprintf( 'SELECT * FROM accounting_system_departments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchAccountingSystemDepartmentsByAccountingSystemTypeId( $intAccountingSystemTypeId, $objDatabase ) {
		return self::fetchAccountingSystemDepartments( sprintf( 'SELECT * FROM accounting_system_departments WHERE accounting_system_type_id = %d', ( int ) $intAccountingSystemTypeId ), $objDatabase );
	}

	public static function fetchAccountingSystemDepartmentsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchAccountingSystemDepartments( sprintf( 'SELECT * FROM accounting_system_departments WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>