<?php

class CBaseAllocation extends CEosSingularBase {

	const TABLE_NAME = 'public.allocations';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intChargeTransactionId;
	protected $m_intCreditTransactionId;
	protected $m_strAllocationDatetime;
	protected $m_fltAllocationAmount;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intAllocationId;
	protected $m_fltChargeTransactionDueAmount;
	protected $m_fltCreditTransactionDueAmount;
	protected $m_intExportBatchId;
	protected $m_strDeletedOn;
	protected $m_intDeletedBy;

	public function __construct() {
		parent::__construct();

		$this->m_fltChargeTransactionDueAmount = '0';
		$this->m_fltCreditTransactionDueAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['charge_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeTransactionId', trim( $arrValues['charge_transaction_id'] ) ); elseif( isset( $arrValues['charge_transaction_id'] ) ) $this->setChargeTransactionId( $arrValues['charge_transaction_id'] );
		if( isset( $arrValues['credit_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditTransactionId', trim( $arrValues['credit_transaction_id'] ) ); elseif( isset( $arrValues['credit_transaction_id'] ) ) $this->setCreditTransactionId( $arrValues['credit_transaction_id'] );
		if( isset( $arrValues['allocation_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAllocationDatetime', trim( $arrValues['allocation_datetime'] ) ); elseif( isset( $arrValues['allocation_datetime'] ) ) $this->setAllocationDatetime( $arrValues['allocation_datetime'] );
		if( isset( $arrValues['allocation_amount'] ) && $boolDirectSet ) $this->set( 'm_fltAllocationAmount', trim( $arrValues['allocation_amount'] ) ); elseif( isset( $arrValues['allocation_amount'] ) ) $this->setAllocationAmount( $arrValues['allocation_amount'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['allocation_id'] ) && $boolDirectSet ) $this->set( 'm_intAllocationId', trim( $arrValues['allocation_id'] ) ); elseif( isset( $arrValues['allocation_id'] ) ) $this->setAllocationId( $arrValues['allocation_id'] );
		if( isset( $arrValues['charge_transaction_due_amount'] ) && $boolDirectSet ) $this->set( 'm_fltChargeTransactionDueAmount', trim( $arrValues['charge_transaction_due_amount'] ) ); elseif( isset( $arrValues['charge_transaction_due_amount'] ) ) $this->setChargeTransactionDueAmount( $arrValues['charge_transaction_due_amount'] );
		if( isset( $arrValues['credit_transaction_due_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCreditTransactionDueAmount', trim( $arrValues['credit_transaction_due_amount'] ) ); elseif( isset( $arrValues['credit_transaction_due_amount'] ) ) $this->setCreditTransactionDueAmount( $arrValues['credit_transaction_due_amount'] );
		if( isset( $arrValues['export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intExportBatchId', trim( $arrValues['export_batch_id'] ) ); elseif( isset( $arrValues['export_batch_id'] ) ) $this->setExportBatchId( $arrValues['export_batch_id'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setChargeTransactionId( $intChargeTransactionId ) {
		$this->set( 'm_intChargeTransactionId', CStrings::strToIntDef( $intChargeTransactionId, NULL, false ) );
	}

	public function getChargeTransactionId() {
		return $this->m_intChargeTransactionId;
	}

	public function sqlChargeTransactionId() {
		return ( true == isset( $this->m_intChargeTransactionId ) ) ? ( string ) $this->m_intChargeTransactionId : 'NULL';
	}

	public function setCreditTransactionId( $intCreditTransactionId ) {
		$this->set( 'm_intCreditTransactionId', CStrings::strToIntDef( $intCreditTransactionId, NULL, false ) );
	}

	public function getCreditTransactionId() {
		return $this->m_intCreditTransactionId;
	}

	public function sqlCreditTransactionId() {
		return ( true == isset( $this->m_intCreditTransactionId ) ) ? ( string ) $this->m_intCreditTransactionId : 'NULL';
	}

	public function setAllocationDatetime( $strAllocationDatetime ) {
		$this->set( 'm_strAllocationDatetime', CStrings::strTrimDef( $strAllocationDatetime, -1, NULL, true ) );
	}

	public function getAllocationDatetime() {
		return $this->m_strAllocationDatetime;
	}

	public function sqlAllocationDatetime() {
		return ( true == isset( $this->m_strAllocationDatetime ) ) ? '\'' . $this->m_strAllocationDatetime . '\'' : 'NOW()';
	}

	public function setAllocationAmount( $fltAllocationAmount ) {
		$this->set( 'm_fltAllocationAmount', CStrings::strToFloatDef( $fltAllocationAmount, NULL, false, 2 ) );
	}

	public function getAllocationAmount() {
		return $this->m_fltAllocationAmount;
	}

	public function sqlAllocationAmount() {
		return ( true == isset( $this->m_fltAllocationAmount ) ) ? ( string ) $this->m_fltAllocationAmount : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAllocationId( $intAllocationId ) {
		$this->set( 'm_intAllocationId', CStrings::strToIntDef( $intAllocationId, NULL, false ) );
	}

	public function getAllocationId() {
		return $this->m_intAllocationId;
	}

	public function sqlAllocationId() {
		return ( true == isset( $this->m_intAllocationId ) ) ? ( string ) $this->m_intAllocationId : 'NULL';
	}

	public function setChargeTransactionDueAmount( $fltChargeTransactionDueAmount ) {
		$this->set( 'm_fltChargeTransactionDueAmount', CStrings::strToFloatDef( $fltChargeTransactionDueAmount, NULL, false, 2 ) );
	}

	public function getChargeTransactionDueAmount() {
		return $this->m_fltChargeTransactionDueAmount;
	}

	public function sqlChargeTransactionDueAmount() {
		return ( true == isset( $this->m_fltChargeTransactionDueAmount ) ) ? ( string ) $this->m_fltChargeTransactionDueAmount : '0';
	}

	public function setCreditTransactionDueAmount( $fltCreditTransactionDueAmount ) {
		$this->set( 'm_fltCreditTransactionDueAmount', CStrings::strToFloatDef( $fltCreditTransactionDueAmount, NULL, false, 2 ) );
	}

	public function getCreditTransactionDueAmount() {
		return $this->m_fltCreditTransactionDueAmount;
	}

	public function sqlCreditTransactionDueAmount() {
		return ( true == isset( $this->m_fltCreditTransactionDueAmount ) ) ? ( string ) $this->m_fltCreditTransactionDueAmount : '0';
	}

	public function setExportBatchId( $intExportBatchId ) {
		$this->set( 'm_intExportBatchId', CStrings::strToIntDef( $intExportBatchId, NULL, false ) );
	}

	public function getExportBatchId() {
		return $this->m_intExportBatchId;
	}

	public function sqlExportBatchId() {
		return ( true == isset( $this->m_intExportBatchId ) ) ? ( string ) $this->m_intExportBatchId : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, charge_transaction_id, credit_transaction_id, allocation_datetime, allocation_amount, created_by, created_on, allocation_id, charge_transaction_due_amount, credit_transaction_due_amount, export_batch_id, deleted_on, deleted_by )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlChargeTransactionId() . ', ' .
						$this->sqlCreditTransactionId() . ', ' .
						$this->sqlAllocationDatetime() . ', ' .
						$this->sqlAllocationAmount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAllocationId() . ', ' .
						$this->sqlChargeTransactionDueAmount() . ', ' .
						$this->sqlCreditTransactionDueAmount() . ', ' .
						$this->sqlExportBatchId() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDeletedBy() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_transaction_id = ' . $this->sqlChargeTransactionId(). ',' ; } elseif( true == array_key_exists( 'ChargeTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' charge_transaction_id = ' . $this->sqlChargeTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_transaction_id = ' . $this->sqlCreditTransactionId(). ',' ; } elseif( true == array_key_exists( 'CreditTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' credit_transaction_id = ' . $this->sqlCreditTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocation_datetime = ' . $this->sqlAllocationDatetime(). ',' ; } elseif( true == array_key_exists( 'AllocationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' allocation_datetime = ' . $this->sqlAllocationDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocation_amount = ' . $this->sqlAllocationAmount(). ',' ; } elseif( true == array_key_exists( 'AllocationAmount', $this->getChangedColumns() ) ) { $strSql .= ' allocation_amount = ' . $this->sqlAllocationAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' allocation_id = ' . $this->sqlAllocationId(). ',' ; } elseif( true == array_key_exists( 'AllocationId', $this->getChangedColumns() ) ) { $strSql .= ' allocation_id = ' . $this->sqlAllocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_transaction_due_amount = ' . $this->sqlChargeTransactionDueAmount(). ',' ; } elseif( true == array_key_exists( 'ChargeTransactionDueAmount', $this->getChangedColumns() ) ) { $strSql .= ' charge_transaction_due_amount = ' . $this->sqlChargeTransactionDueAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_transaction_due_amount = ' . $this->sqlCreditTransactionDueAmount(). ',' ; } elseif( true == array_key_exists( 'CreditTransactionDueAmount', $this->getChangedColumns() ) ) { $strSql .= ' credit_transaction_due_amount = ' . $this->sqlCreditTransactionDueAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_batch_id = ' . $this->sqlExportBatchId(). ',' ; } elseif( true == array_key_exists( 'ExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' export_batch_id = ' . $this->sqlExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'charge_transaction_id' => $this->getChargeTransactionId(),
			'credit_transaction_id' => $this->getCreditTransactionId(),
			'allocation_datetime' => $this->getAllocationDatetime(),
			'allocation_amount' => $this->getAllocationAmount(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'allocation_id' => $this->getAllocationId(),
			'charge_transaction_due_amount' => $this->getChargeTransactionDueAmount(),
			'credit_transaction_due_amount' => $this->getCreditTransactionDueAmount(),
			'export_batch_id' => $this->getExportBatchId(),
			'deleted_on' => $this->getDeletedOn(),
			'deleted_by' => $this->getDeletedBy()
		);
	}

}
?>