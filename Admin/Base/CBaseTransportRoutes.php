<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTransportRoutes
 * Do not add any new functions to this class.
 */

class CBaseTransportRoutes extends CEosPluralBase {

	/**
	 * @return CTransportRoute[]
	 */
	public static function fetchTransportRoutes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTransportRoute', $objDatabase );
	}

	/**
	 * @return CTransportRoute
	 */
	public static function fetchTransportRoute( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTransportRoute', $objDatabase );
	}

	public static function fetchTransportRouteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transport_routes', $objDatabase );
	}

	public static function fetchTransportRouteById( $intId, $objDatabase ) {
		return self::fetchTransportRoute( sprintf( 'SELECT * FROM transport_routes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTransportRoutesByTransportVehicleId( $intTransportVehicleId, $objDatabase ) {
		return self::fetchTransportRoutes( sprintf( 'SELECT * FROM transport_routes WHERE transport_vehicle_id = %d', ( int ) $intTransportVehicleId ), $objDatabase );
	}

}
?>