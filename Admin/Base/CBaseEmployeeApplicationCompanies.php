<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationCompanies
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationCompanies extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationCompany[]
	 */
	public static function fetchEmployeeApplicationCompanies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationCompany', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationCompany
	 */
	public static function fetchEmployeeApplicationCompany( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationCompany', $objDatabase );
	}

	public static function fetchEmployeeApplicationCompanyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_companies', $objDatabase );
	}

	public static function fetchEmployeeApplicationCompanyById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationCompany( sprintf( 'SELECT * FROM employee_application_companies WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>