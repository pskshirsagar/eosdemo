<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAttendanceRemarks
 * Do not add any new functions to this class.
 */

class CBaseEmployeeAttendanceRemarks extends CEosPluralBase {

	/**
	 * @return CEmployeeAttendanceRemark[]
	 */
	public static function fetchEmployeeAttendanceRemarks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeAttendanceRemark', $objDatabase );
	}

	/**
	 * @return CEmployeeAttendanceRemark
	 */
	public static function fetchEmployeeAttendanceRemark( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeAttendanceRemark', $objDatabase );
	}

	public static function fetchEmployeeAttendanceRemarkCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_attendance_remarks', $objDatabase );
	}

	public static function fetchEmployeeAttendanceRemarkById( $intId, $objDatabase ) {
		return self::fetchEmployeeAttendanceRemark( sprintf( 'SELECT * FROM employee_attendance_remarks WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeAttendanceRemarksByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeAttendanceRemarks( sprintf( 'SELECT * FROM employee_attendance_remarks WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>