<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeSalesAreas
 * Do not add any new functions to this class.
 */

class CBaseEmployeeSalesAreas extends CEosPluralBase {

	/**
	 * @return CEmployeeSalesArea[]
	 */
	public static function fetchEmployeeSalesAreas( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeSalesArea', $objDatabase );
	}

	/**
	 * @return CEmployeeSalesArea
	 */
	public static function fetchEmployeeSalesArea( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeSalesArea', $objDatabase );
	}

	public static function fetchEmployeeSalesAreaCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_sales_areas', $objDatabase );
	}

	public static function fetchEmployeeSalesAreaById( $intId, $objDatabase ) {
		return self::fetchEmployeeSalesArea( sprintf( 'SELECT * FROM employee_sales_areas WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeSalesAreasByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeSalesAreas( sprintf( 'SELECT * FROM employee_sales_areas WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeSalesAreasBySalesAreaId( $intSalesAreaId, $objDatabase ) {
		return self::fetchEmployeeSalesAreas( sprintf( 'SELECT * FROM employee_sales_areas WHERE sales_area_id = %d', ( int ) $intSalesAreaId ), $objDatabase );
	}

}
?>