<?php

class CBaseExpensesStatusTypes extends CEosPluralBase {

    public static function fetchExpensesStatusTypes( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CExpensesStatusType', $objDatabase );
    }

    public static function fetchExpensesStatusType( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CExpensesStatusType', $objDatabase );
    }

    public static function fetchExpensesStatusTypeCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'expenses_status_types', $objDatabase );
    }

    public static function fetchExpensesStatusTypeById( $intId, $objDatabase ) {
        return self::fetchExpensesStatusType( sprintf( 'SELECT * FROM expenses_status_types WHERE id = %d', (int) $intId ), $objDatabase );
    }

}
?>