<?php

class CBaseConversation extends CEosSingularBase {

	const TABLE_NAME = 'public.conversations';

	protected $m_intId;
	protected $m_intProjectId;
	protected $m_intProjectAttributeId;
	protected $m_intConversationId;
	protected $m_strMessage;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intFeedbackId;
	protected $m_intActionItemId;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['project_id'] ) && $boolDirectSet ) $this->set( 'm_intProjectId', trim( $arrValues['project_id'] ) ); elseif( isset( $arrValues['project_id'] ) ) $this->setProjectId( $arrValues['project_id'] );
		if( isset( $arrValues['project_attribute_id'] ) && $boolDirectSet ) $this->set( 'm_intProjectAttributeId', trim( $arrValues['project_attribute_id'] ) ); elseif( isset( $arrValues['project_attribute_id'] ) ) $this->setProjectAttributeId( $arrValues['project_attribute_id'] );
		if( isset( $arrValues['conversation_id'] ) && $boolDirectSet ) $this->set( 'm_intConversationId', trim( $arrValues['conversation_id'] ) ); elseif( isset( $arrValues['conversation_id'] ) ) $this->setConversationId( $arrValues['conversation_id'] );
		if( isset( $arrValues['message'] ) && $boolDirectSet ) $this->set( 'm_strMessage', trim( $arrValues['message'] ) ); elseif( isset( $arrValues['message'] ) ) $this->setMessage( $arrValues['message'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['feedback_id'] ) && $boolDirectSet ) $this->set( 'm_intFeedbackId', trim( $arrValues['feedback_id'] ) ); elseif( isset( $arrValues['feedback_id'] ) ) $this->setFeedbackId( $arrValues['feedback_id'] );
		if( isset( $arrValues['action_item_id'] ) && $boolDirectSet ) $this->set( 'm_intActionItemId', trim( $arrValues['action_item_id'] ) ); elseif( isset( $arrValues['action_item_id'] ) ) $this->setActionItemId( $arrValues['action_item_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setProjectId( $intProjectId ) {
		$this->set( 'm_intProjectId', CStrings::strToIntDef( $intProjectId, NULL, false ) );
	}

	public function getProjectId() {
		return $this->m_intProjectId;
	}

	public function sqlProjectId() {
		return ( true == isset( $this->m_intProjectId ) ) ? ( string ) $this->m_intProjectId : 'NULL';
	}

	public function setProjectAttributeId( $intProjectAttributeId ) {
		$this->set( 'm_intProjectAttributeId', CStrings::strToIntDef( $intProjectAttributeId, NULL, false ) );
	}

	public function getProjectAttributeId() {
		return $this->m_intProjectAttributeId;
	}

	public function sqlProjectAttributeId() {
		return ( true == isset( $this->m_intProjectAttributeId ) ) ? ( string ) $this->m_intProjectAttributeId : 'NULL';
	}

	public function setConversationId( $intConversationId ) {
		$this->set( 'm_intConversationId', CStrings::strToIntDef( $intConversationId, NULL, false ) );
	}

	public function getConversationId() {
		return $this->m_intConversationId;
	}

	public function sqlConversationId() {
		return ( true == isset( $this->m_intConversationId ) ) ? ( string ) $this->m_intConversationId : 'NULL';
	}

	public function setMessage( $strMessage ) {
		$this->set( 'm_strMessage', CStrings::strTrimDef( $strMessage, -1, NULL, true ) );
	}

	public function getMessage() {
		return $this->m_strMessage;
	}

	public function sqlMessage() {
		return ( true == isset( $this->m_strMessage ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMessage ) : '\'' . addslashes( $this->m_strMessage ) . '\'' ) : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setFeedbackId( $intFeedbackId ) {
		$this->set( 'm_intFeedbackId', CStrings::strToIntDef( $intFeedbackId, NULL, false ) );
	}

	public function getFeedbackId() {
		return $this->m_intFeedbackId;
	}

	public function sqlFeedbackId() {
		return ( true == isset( $this->m_intFeedbackId ) ) ? ( string ) $this->m_intFeedbackId : 'NULL';
	}

	public function setActionItemId( $intActionItemId ) {
		$this->set( 'm_intActionItemId', CStrings::strToIntDef( $intActionItemId, NULL, false ) );
	}

	public function getActionItemId() {
		return $this->m_intActionItemId;
	}

	public function sqlActionItemId() {
		return ( true == isset( $this->m_intActionItemId ) ) ? ( string ) $this->m_intActionItemId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, project_id, project_attribute_id, conversation_id, message, deleted_by, deleted_on, created_by, created_on, feedback_id, action_item_id )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlProjectId() . ', ' .
		          $this->sqlProjectAttributeId() . ', ' .
		          $this->sqlConversationId() . ', ' .
		          $this->sqlMessage() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlFeedbackId() . ', ' .
		          $this->sqlActionItemId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_id = ' . $this->sqlProjectId(). ',' ; } elseif( true == array_key_exists( 'ProjectId', $this->getChangedColumns() ) ) { $strSql .= ' project_id = ' . $this->sqlProjectId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_attribute_id = ' . $this->sqlProjectAttributeId(). ',' ; } elseif( true == array_key_exists( 'ProjectAttributeId', $this->getChangedColumns() ) ) { $strSql .= ' project_attribute_id = ' . $this->sqlProjectAttributeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' conversation_id = ' . $this->sqlConversationId(). ',' ; } elseif( true == array_key_exists( 'ConversationId', $this->getChangedColumns() ) ) { $strSql .= ' conversation_id = ' . $this->sqlConversationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message = ' . $this->sqlMessage(). ',' ; } elseif( true == array_key_exists( 'Message', $this->getChangedColumns() ) ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' feedback_id = ' . $this->sqlFeedbackId(). ',' ; } elseif( true == array_key_exists( 'FeedbackId', $this->getChangedColumns() ) ) { $strSql .= ' feedback_id = ' . $this->sqlFeedbackId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_item_id = ' . $this->sqlActionItemId() ; } elseif( true == array_key_exists( 'ActionItemId', $this->getChangedColumns() ) ) { $strSql .= ' action_item_id = ' . $this->sqlActionItemId() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'project_id' => $this->getProjectId(),
			'project_attribute_id' => $this->getProjectAttributeId(),
			'conversation_id' => $this->getConversationId(),
			'message' => $this->getMessage(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'feedback_id' => $this->getFeedbackId(),
			'action_item_id' => $this->getActionItemId()
		);
	}

}
?>