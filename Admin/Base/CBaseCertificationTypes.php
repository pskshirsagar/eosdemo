<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCertificationTypes
 * Do not add any new functions to this class.
 */

class CBaseCertificationTypes extends CEosPluralBase {

	/**
	 * @return CCertificationType[]
	 */
	public static function fetchCertificationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCertificationType::class, $objDatabase );
	}

	/**
	 * @return CCertificationType
	 */
	public static function fetchCertificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCertificationType::class, $objDatabase );
	}

	public static function fetchCertificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'certification_types', $objDatabase );
	}

	public static function fetchCertificationTypeById( $intId, $objDatabase ) {
		return self::fetchCertificationType( sprintf( 'SELECT * FROM certification_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>