<?php

class CBaseTaskAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.task_associations';

	protected $m_intId;
	protected $m_intTaskId;
	protected $m_intParentTaskId;
	protected $m_boolIsEscalated;
	protected $m_boolIsDependent;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsEscalated = false;
		$this->m_boolIsDependent = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['parent_task_id'] ) && $boolDirectSet ) $this->set( 'm_intParentTaskId', trim( $arrValues['parent_task_id'] ) ); elseif( isset( $arrValues['parent_task_id'] ) ) $this->setParentTaskId( $arrValues['parent_task_id'] );
		if( isset( $arrValues['is_escalated'] ) && $boolDirectSet ) $this->set( 'm_boolIsEscalated', trim( stripcslashes( $arrValues['is_escalated'] ) ) ); elseif( isset( $arrValues['is_escalated'] ) ) $this->setIsEscalated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_escalated'] ) : $arrValues['is_escalated'] );
		if( isset( $arrValues['is_dependent'] ) && $boolDirectSet ) $this->set( 'm_boolIsDependent', trim( stripcslashes( $arrValues['is_dependent'] ) ) ); elseif( isset( $arrValues['is_dependent'] ) ) $this->setIsDependent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_dependent'] ) : $arrValues['is_dependent'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setParentTaskId( $intParentTaskId ) {
		$this->set( 'm_intParentTaskId', CStrings::strToIntDef( $intParentTaskId, NULL, false ) );
	}

	public function getParentTaskId() {
		return $this->m_intParentTaskId;
	}

	public function sqlParentTaskId() {
		return ( true == isset( $this->m_intParentTaskId ) ) ? ( string ) $this->m_intParentTaskId : 'NULL';
	}

	public function setIsEscalated( $boolIsEscalated ) {
		$this->set( 'm_boolIsEscalated', CStrings::strToBool( $boolIsEscalated ) );
	}

	public function getIsEscalated() {
		return $this->m_boolIsEscalated;
	}

	public function sqlIsEscalated() {
		return ( true == isset( $this->m_boolIsEscalated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEscalated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsDependent( $boolIsDependent ) {
		$this->set( 'm_boolIsDependent', CStrings::strToBool( $boolIsDependent ) );
	}

	public function getIsDependent() {
		return $this->m_boolIsDependent;
	}

	public function sqlIsDependent() {
		return ( true == isset( $this->m_boolIsDependent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDependent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, task_id, parent_task_id, is_escalated, is_dependent, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlParentTaskId() . ', ' .
 						$this->sqlIsEscalated() . ', ' .
 						$this->sqlIsDependent() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_task_id = ' . $this->sqlParentTaskId() . ','; } elseif( true == array_key_exists( 'ParentTaskId', $this->getChangedColumns() ) ) { $strSql .= ' parent_task_id = ' . $this->sqlParentTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_escalated = ' . $this->sqlIsEscalated() . ','; } elseif( true == array_key_exists( 'IsEscalated', $this->getChangedColumns() ) ) { $strSql .= ' is_escalated = ' . $this->sqlIsEscalated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_dependent = ' . $this->sqlIsDependent() . ','; } elseif( true == array_key_exists( 'IsDependent', $this->getChangedColumns() ) ) { $strSql .= ' is_dependent = ' . $this->sqlIsDependent() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'task_id' => $this->getTaskId(),
			'parent_task_id' => $this->getParentTaskId(),
			'is_escalated' => $this->getIsEscalated(),
			'is_dependent' => $this->getIsDependent(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>