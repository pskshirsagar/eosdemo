<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseTypes
 * Do not add any new functions to this class.
 */

class CBaseReleaseTypes extends CEosPluralBase {

	/**
	 * @return CReleaseType[]
	 */
	public static function fetchReleaseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CReleaseType', $objDatabase );
	}

	/**
	 * @return CReleaseType
	 */
	public static function fetchReleaseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReleaseType', $objDatabase );
	}

	public static function fetchReleaseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'release_types', $objDatabase );
	}

	public static function fetchReleaseTypeById( $intId, $objDatabase ) {
		return self::fetchReleaseType( sprintf( 'SELECT * FROM release_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>