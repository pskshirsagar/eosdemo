<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CNoteTypes
 * Do not add any new functions to this class.
 */

class CBaseNoteTypes extends CEosPluralBase {

	/**
	 * @return CNoteType[]
	 */
	public static function fetchNoteTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CNoteType', $objDatabase );
	}

	/**
	 * @return CNoteType
	 */
	public static function fetchNoteType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CNoteType', $objDatabase );
	}

	public static function fetchNoteTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'note_types', $objDatabase );
	}

	public static function fetchNoteTypeById( $intId, $objDatabase ) {
		return self::fetchNoteType( sprintf( 'SELECT * FROM note_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>