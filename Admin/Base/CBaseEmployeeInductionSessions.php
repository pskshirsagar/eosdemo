<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeInductionSessions
 * Do not add any new functions to this class.
 */

class CBaseEmployeeInductionSessions extends CEosPluralBase {

	/**
	 * @return CEmployeeInductionSession[]
	 */
	public static function fetchEmployeeInductionSessions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeInductionSession', $objDatabase );
	}

	/**
	 * @return CEmployeeInductionSession
	 */
	public static function fetchEmployeeInductionSession( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeInductionSession', $objDatabase );
	}

	public static function fetchEmployeeInductionSessionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_induction_sessions', $objDatabase );
	}

	public static function fetchEmployeeInductionSessionById( $intId, $objDatabase ) {
		return self::fetchEmployeeInductionSession( sprintf( 'SELECT * FROM employee_induction_sessions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeInductionSessionsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeInductionSessions( sprintf( 'SELECT * FROM employee_induction_sessions WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeInductionSessionsByInductionSessionId( $intInductionSessionId, $objDatabase ) {
		return self::fetchEmployeeInductionSessions( sprintf( 'SELECT * FROM employee_induction_sessions WHERE induction_session_id = %d', ( int ) $intInductionSessionId ), $objDatabase );
	}

}
?>