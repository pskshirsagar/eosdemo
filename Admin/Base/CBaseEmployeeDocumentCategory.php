<?php

class CBaseEmployeeDocumentCategory extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_document_categories';

	protected $m_intId;
	protected $m_intEmployeeDocumentTypeId;
	protected $m_intEmployeeDocumentCategoryId;
	protected $m_strName;
	protected $m_boolIsDocumentRequired;
	protected $m_boolIsVisibleToEmployee;
	protected $m_strFileExtensionIds;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsDocumentRequired = true;
		$this->m_boolIsVisibleToEmployee = false;
		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_document_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeDocumentTypeId', trim( $arrValues['employee_document_type_id'] ) ); elseif( isset( $arrValues['employee_document_type_id'] ) ) $this->setEmployeeDocumentTypeId( $arrValues['employee_document_type_id'] );
		if( isset( $arrValues['employee_document_category_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeDocumentCategoryId', trim( $arrValues['employee_document_category_id'] ) ); elseif( isset( $arrValues['employee_document_category_id'] ) ) $this->setEmployeeDocumentCategoryId( $arrValues['employee_document_category_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['is_document_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsDocumentRequired', trim( stripcslashes( $arrValues['is_document_required'] ) ) ); elseif( isset( $arrValues['is_document_required'] ) ) $this->setIsDocumentRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_document_required'] ) : $arrValues['is_document_required'] );
		if( isset( $arrValues['is_visible_to_employee'] ) && $boolDirectSet ) $this->set( 'm_boolIsVisibleToEmployee', trim( stripcslashes( $arrValues['is_visible_to_employee'] ) ) ); elseif( isset( $arrValues['is_visible_to_employee'] ) ) $this->setIsVisibleToEmployee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_visible_to_employee'] ) : $arrValues['is_visible_to_employee'] );
		if( isset( $arrValues['file_extension_ids'] ) && $boolDirectSet ) $this->set( 'm_strFileExtensionIds', trim( $arrValues['file_extension_ids'] ) ); elseif( isset( $arrValues['file_extension_ids'] ) ) $this->setFileExtensionIds( $arrValues['file_extension_ids'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeDocumentTypeId( $intEmployeeDocumentTypeId ) {
		$this->set( 'm_intEmployeeDocumentTypeId', CStrings::strToIntDef( $intEmployeeDocumentTypeId, NULL, false ) );
	}

	public function getEmployeeDocumentTypeId() {
		return $this->m_intEmployeeDocumentTypeId;
	}

	public function sqlEmployeeDocumentTypeId() {
		return ( true == isset( $this->m_intEmployeeDocumentTypeId ) ) ? ( string ) $this->m_intEmployeeDocumentTypeId : 'NULL';
	}

	public function setEmployeeDocumentCategoryId( $intEmployeeDocumentCategoryId ) {
		$this->set( 'm_intEmployeeDocumentCategoryId', CStrings::strToIntDef( $intEmployeeDocumentCategoryId, NULL, false ) );
	}

	public function getEmployeeDocumentCategoryId() {
		return $this->m_intEmployeeDocumentCategoryId;
	}

	public function sqlEmployeeDocumentCategoryId() {
		return ( true == isset( $this->m_intEmployeeDocumentCategoryId ) ) ? ( string ) $this->m_intEmployeeDocumentCategoryId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 250, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setIsDocumentRequired( $boolIsDocumentRequired ) {
		$this->set( 'm_boolIsDocumentRequired', CStrings::strToBool( $boolIsDocumentRequired ) );
	}

	public function getIsDocumentRequired() {
		return $this->m_boolIsDocumentRequired;
	}

	public function sqlIsDocumentRequired() {
		return ( true == isset( $this->m_boolIsDocumentRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDocumentRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsVisibleToEmployee( $boolIsVisibleToEmployee ) {
		$this->set( 'm_boolIsVisibleToEmployee', CStrings::strToBool( $boolIsVisibleToEmployee ) );
	}

	public function getIsVisibleToEmployee() {
		return $this->m_boolIsVisibleToEmployee;
	}

	public function sqlIsVisibleToEmployee() {
		return ( true == isset( $this->m_boolIsVisibleToEmployee ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsVisibleToEmployee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setFileExtensionIds( $strFileExtensionIds ) {
		$this->set( 'm_strFileExtensionIds', CStrings::strTrimDef( $strFileExtensionIds, 150, NULL, true ) );
	}

	public function getFileExtensionIds() {
		return $this->m_strFileExtensionIds;
	}

	public function sqlFileExtensionIds() {
		return ( true == isset( $this->m_strFileExtensionIds ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileExtensionIds ) : '\'' . addslashes( $this->m_strFileExtensionIds ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_document_type_id, employee_document_category_id, name, is_document_required, is_visible_to_employee, file_extension_ids, is_published, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeDocumentTypeId() . ', ' .
						$this->sqlEmployeeDocumentCategoryId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlIsDocumentRequired() . ', ' .
						$this->sqlIsVisibleToEmployee() . ', ' .
						$this->sqlFileExtensionIds() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_document_type_id = ' . $this->sqlEmployeeDocumentTypeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeDocumentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_document_type_id = ' . $this->sqlEmployeeDocumentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_document_category_id = ' . $this->sqlEmployeeDocumentCategoryId(). ',' ; } elseif( true == array_key_exists( 'EmployeeDocumentCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' employee_document_category_id = ' . $this->sqlEmployeeDocumentCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_document_required = ' . $this->sqlIsDocumentRequired(). ',' ; } elseif( true == array_key_exists( 'IsDocumentRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_document_required = ' . $this->sqlIsDocumentRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_visible_to_employee = ' . $this->sqlIsVisibleToEmployee(). ',' ; } elseif( true == array_key_exists( 'IsVisibleToEmployee', $this->getChangedColumns() ) ) { $strSql .= ' is_visible_to_employee = ' . $this->sqlIsVisibleToEmployee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_extension_ids = ' . $this->sqlFileExtensionIds(). ',' ; } elseif( true == array_key_exists( 'FileExtensionIds', $this->getChangedColumns() ) ) { $strSql .= ' file_extension_ids = ' . $this->sqlFileExtensionIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_document_type_id' => $this->getEmployeeDocumentTypeId(),
			'employee_document_category_id' => $this->getEmployeeDocumentCategoryId(),
			'name' => $this->getName(),
			'is_document_required' => $this->getIsDocumentRequired(),
			'is_visible_to_employee' => $this->getIsVisibleToEmployee(),
			'file_extension_ids' => $this->getFileExtensionIds(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>