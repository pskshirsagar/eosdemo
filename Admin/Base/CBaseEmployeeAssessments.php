<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAssessments
 * Do not add any new functions to this class.
 */

class CBaseEmployeeAssessments extends CEosPluralBase {

	/**
	 * @return CEmployeeAssessment[]
	 */
	public static function fetchEmployeeAssessments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeAssessment::class, $objDatabase );
	}

	/**
	 * @return CEmployeeAssessment
	 */
	public static function fetchEmployeeAssessment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeAssessment::class, $objDatabase );
	}

	public static function fetchEmployeeAssessmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_assessments', $objDatabase );
	}

	public static function fetchEmployeeAssessmentById( $intId, $objDatabase ) {
		return self::fetchEmployeeAssessment( sprintf( 'SELECT * FROM employee_assessments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		return self::fetchEmployeeAssessments( sprintf( 'SELECT * FROM employee_assessments WHERE manager_employee_id = %d', ( int ) $intManagerEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeAssessments( sprintf( 'SELECT * FROM employee_assessments WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentsByEmployeeAssessmentTypeId( $intEmployeeAssessmentTypeId, $objDatabase ) {
		return self::fetchEmployeeAssessments( sprintf( 'SELECT * FROM employee_assessments WHERE employee_assessment_type_id = %d', ( int ) $intEmployeeAssessmentTypeId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchEmployeeAssessments( sprintf( 'SELECT * FROM employee_assessments WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentsByPsDocumentId( $intPsDocumentId, $objDatabase ) {
		return self::fetchEmployeeAssessments( sprintf( 'SELECT * FROM employee_assessments WHERE ps_document_id = %d', ( int ) $intPsDocumentId ), $objDatabase );
	}

}
?>