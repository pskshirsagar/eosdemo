<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskServers
 * Do not add any new functions to this class.
 */

class CBaseTaskServers extends CEosPluralBase {

	/**
	 * @return CTaskServer[]
	 */
	public static function fetchTaskServers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskServer', $objDatabase );
	}

	/**
	 * @return CTaskServer
	 */
	public static function fetchTaskServer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskServer', $objDatabase );
	}

	public static function fetchTaskServerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_servers', $objDatabase );
	}

	public static function fetchTaskServerById( $intId, $objDatabase ) {
		return self::fetchTaskServer( sprintf( 'SELECT * FROM task_servers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskServersByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchTaskServers( sprintf( 'SELECT * FROM task_servers WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

	public static function fetchTaskServersByHardwareId( $intHardwareId, $objDatabase ) {
		return self::fetchTaskServers( sprintf( 'SELECT * FROM task_servers WHERE hardware_id = %d', ( int ) $intHardwareId ), $objDatabase );
	}

	public static function fetchTaskServersByUserId( $intUserId, $objDatabase ) {
		return self::fetchTaskServers( sprintf( 'SELECT * FROM task_servers WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

}
?>