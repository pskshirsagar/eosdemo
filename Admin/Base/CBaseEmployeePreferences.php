<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePreferences
 * Do not add any new functions to this class.
 */

class CBaseEmployeePreferences extends CEosPluralBase {

	/**
	 * @return CEmployeePreference[]
	 */
	public static function fetchEmployeePreferences( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeePreference', $objDatabase );
	}

	/**
	 * @return CEmployeePreference
	 */
	public static function fetchEmployeePreference( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeePreference', $objDatabase );
	}

	public static function fetchEmployeePreferenceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_preferences', $objDatabase );
	}

	public static function fetchEmployeePreferenceById( $intId, $objDatabase ) {
		return self::fetchEmployeePreference( sprintf( 'SELECT * FROM employee_preferences WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeePreferencesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeePreferences( sprintf( 'SELECT * FROM employee_preferences WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>