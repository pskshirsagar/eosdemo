<?php

class CBaseEmployeeOption extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_options';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intVestingScheduleId;
	protected $m_intEmployeeOptionAssociationId;
	protected $m_intExercisePriceAssociationId;
	protected $m_strOptionDatetime;
	protected $m_strGrantDate;
	protected $m_strVestingCommencementDate;
	protected $m_strFinalVestingDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['vesting_schedule_id'] ) && $boolDirectSet ) $this->set( 'm_intVestingScheduleId', trim( $arrValues['vesting_schedule_id'] ) ); elseif( isset( $arrValues['vesting_schedule_id'] ) ) $this->setVestingScheduleId( $arrValues['vesting_schedule_id'] );
		if( isset( $arrValues['employee_option_association_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeOptionAssociationId', trim( $arrValues['employee_option_association_id'] ) ); elseif( isset( $arrValues['employee_option_association_id'] ) ) $this->setEmployeeOptionAssociationId( $arrValues['employee_option_association_id'] );
		if( isset( $arrValues['exercise_price_association_id'] ) && $boolDirectSet ) $this->set( 'm_intExercisePriceAssociationId', trim( $arrValues['exercise_price_association_id'] ) ); elseif( isset( $arrValues['exercise_price_association_id'] ) ) $this->setExercisePriceAssociationId( $arrValues['exercise_price_association_id'] );
		if( isset( $arrValues['option_datetime'] ) && $boolDirectSet ) $this->set( 'm_strOptionDatetime', trim( $arrValues['option_datetime'] ) ); elseif( isset( $arrValues['option_datetime'] ) ) $this->setOptionDatetime( $arrValues['option_datetime'] );
		if( isset( $arrValues['grant_date'] ) && $boolDirectSet ) $this->set( 'm_strGrantDate', trim( $arrValues['grant_date'] ) ); elseif( isset( $arrValues['grant_date'] ) ) $this->setGrantDate( $arrValues['grant_date'] );
		if( isset( $arrValues['vesting_commencement_date'] ) && $boolDirectSet ) $this->set( 'm_strVestingCommencementDate', trim( $arrValues['vesting_commencement_date'] ) ); elseif( isset( $arrValues['vesting_commencement_date'] ) ) $this->setVestingCommencementDate( $arrValues['vesting_commencement_date'] );
		if( isset( $arrValues['final_vesting_date'] ) && $boolDirectSet ) $this->set( 'm_strFinalVestingDate', trim( $arrValues['final_vesting_date'] ) ); elseif( isset( $arrValues['final_vesting_date'] ) ) $this->setFinalVestingDate( $arrValues['final_vesting_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setVestingScheduleId( $intVestingScheduleId ) {
		$this->set( 'm_intVestingScheduleId', CStrings::strToIntDef( $intVestingScheduleId, NULL, false ) );
	}

	public function getVestingScheduleId() {
		return $this->m_intVestingScheduleId;
	}

	public function sqlVestingScheduleId() {
		return ( true == isset( $this->m_intVestingScheduleId ) ) ? ( string ) $this->m_intVestingScheduleId : 'NULL';
	}

	public function setEmployeeOptionAssociationId( $intEmployeeOptionAssociationId ) {
		$this->set( 'm_intEmployeeOptionAssociationId', CStrings::strToIntDef( $intEmployeeOptionAssociationId, NULL, false ) );
	}

	public function getEmployeeOptionAssociationId() {
		return $this->m_intEmployeeOptionAssociationId;
	}

	public function sqlEmployeeOptionAssociationId() {
		return ( true == isset( $this->m_intEmployeeOptionAssociationId ) ) ? ( string ) $this->m_intEmployeeOptionAssociationId : 'NULL';
	}

	public function setExercisePriceAssociationId( $intExercisePriceAssociationId ) {
		$this->set( 'm_intExercisePriceAssociationId', CStrings::strToIntDef( $intExercisePriceAssociationId, NULL, false ) );
	}

	public function getExercisePriceAssociationId() {
		return $this->m_intExercisePriceAssociationId;
	}

	public function sqlExercisePriceAssociationId() {
		return ( true == isset( $this->m_intExercisePriceAssociationId ) ) ? ( string ) $this->m_intExercisePriceAssociationId : 'NULL';
	}

	public function setOptionDatetime( $strOptionDatetime ) {
		$this->set( 'm_strOptionDatetime', CStrings::strTrimDef( $strOptionDatetime, -1, NULL, true ) );
	}

	public function getOptionDatetime() {
		return $this->m_strOptionDatetime;
	}

	public function sqlOptionDatetime() {
		return ( true == isset( $this->m_strOptionDatetime ) ) ? '\'' . $this->m_strOptionDatetime . '\'' : 'NULL';
	}

	public function setGrantDate( $strGrantDate ) {
		$this->set( 'm_strGrantDate', CStrings::strTrimDef( $strGrantDate, -1, NULL, true ) );
	}

	public function getGrantDate() {
		return $this->m_strGrantDate;
	}

	public function sqlGrantDate() {
		return ( true == isset( $this->m_strGrantDate ) ) ? '\'' . $this->m_strGrantDate . '\'' : 'NULL';
	}

	public function setVestingCommencementDate( $strVestingCommencementDate ) {
		$this->set( 'm_strVestingCommencementDate', CStrings::strTrimDef( $strVestingCommencementDate, -1, NULL, true ) );
	}

	public function getVestingCommencementDate() {
		return $this->m_strVestingCommencementDate;
	}

	public function sqlVestingCommencementDate() {
		return ( true == isset( $this->m_strVestingCommencementDate ) ) ? '\'' . $this->m_strVestingCommencementDate . '\'' : 'NULL';
	}

	public function setFinalVestingDate( $strFinalVestingDate ) {
		$this->set( 'm_strFinalVestingDate', CStrings::strTrimDef( $strFinalVestingDate, -1, NULL, true ) );
	}

	public function getFinalVestingDate() {
		return $this->m_strFinalVestingDate;
	}

	public function sqlFinalVestingDate() {
		return ( true == isset( $this->m_strFinalVestingDate ) ) ? '\'' . $this->m_strFinalVestingDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, vesting_schedule_id, employee_option_association_id, exercise_price_association_id, option_datetime, grant_date, vesting_commencement_date, final_vesting_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlVestingScheduleId() . ', ' .
 						$this->sqlEmployeeOptionAssociationId() . ', ' .
 						$this->sqlExercisePriceAssociationId() . ', ' .
 						$this->sqlOptionDatetime() . ', ' .
 						$this->sqlGrantDate() . ', ' .
 						$this->sqlVestingCommencementDate() . ', ' .
 						$this->sqlFinalVestingDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vesting_schedule_id = ' . $this->sqlVestingScheduleId() . ','; } elseif( true == array_key_exists( 'VestingScheduleId', $this->getChangedColumns() ) ) { $strSql .= ' vesting_schedule_id = ' . $this->sqlVestingScheduleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_option_association_id = ' . $this->sqlEmployeeOptionAssociationId() . ','; } elseif( true == array_key_exists( 'EmployeeOptionAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_option_association_id = ' . $this->sqlEmployeeOptionAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exercise_price_association_id = ' . $this->sqlExercisePriceAssociationId() . ','; } elseif( true == array_key_exists( 'ExercisePriceAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' exercise_price_association_id = ' . $this->sqlExercisePriceAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' option_datetime = ' . $this->sqlOptionDatetime() . ','; } elseif( true == array_key_exists( 'OptionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' option_datetime = ' . $this->sqlOptionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' grant_date = ' . $this->sqlGrantDate() . ','; } elseif( true == array_key_exists( 'GrantDate', $this->getChangedColumns() ) ) { $strSql .= ' grant_date = ' . $this->sqlGrantDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vesting_commencement_date = ' . $this->sqlVestingCommencementDate() . ','; } elseif( true == array_key_exists( 'VestingCommencementDate', $this->getChangedColumns() ) ) { $strSql .= ' vesting_commencement_date = ' . $this->sqlVestingCommencementDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' final_vesting_date = ' . $this->sqlFinalVestingDate() . ','; } elseif( true == array_key_exists( 'FinalVestingDate', $this->getChangedColumns() ) ) { $strSql .= ' final_vesting_date = ' . $this->sqlFinalVestingDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'vesting_schedule_id' => $this->getVestingScheduleId(),
			'employee_option_association_id' => $this->getEmployeeOptionAssociationId(),
			'exercise_price_association_id' => $this->getExercisePriceAssociationId(),
			'option_datetime' => $this->getOptionDatetime(),
			'grant_date' => $this->getGrantDate(),
			'vesting_commencement_date' => $this->getVestingCommencementDate(),
			'final_vesting_date' => $this->getFinalVestingDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>