<?php

class CBasePsLead extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_leads';

	protected $m_intId;
	protected $m_intCompanyStatusTypeId;
	protected $m_intPsLeadOriginId;
	protected $m_intPsLeadSourceId;
	protected $m_intPsLeadId;
	protected $m_intPsLeadEventId;
	protected $m_intBluemoonRemotePrimaryKey;
	protected $m_strCompanyName;
	protected $m_intNumberOfUnits;
	protected $m_intPropertyCount;
	protected $m_strRequestDatetime;
	protected $m_strRequest;
	protected $m_strIpAddress;
	protected $m_strNotes;
	protected $m_fltProfilePercent;
	protected $m_intNeedsFollowUp;
	protected $m_intPriorityNumber;
	protected $m_strKeywords;
	protected $m_strReferralUrl;
	protected $m_strReferralDomain;
	protected $m_strDedupedOn;
	protected $m_intClosedBy;
	protected $m_strClosedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strUtmSource;
	protected $m_strUtmMedium;
	protected $m_strUtmCampaign;
	protected $m_strUtmTerm;
	protected $m_strUtmContent;
	protected $m_intPsLeadStageId;

	public function __construct() {
		parent::__construct();

		$this->m_fltProfilePercent = '0';
		$this->m_intNeedsFollowUp = '0';
		$this->m_intPriorityNumber = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['company_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyStatusTypeId', trim( $arrValues['company_status_type_id'] ) ); elseif( isset( $arrValues['company_status_type_id'] ) ) $this->setCompanyStatusTypeId( $arrValues['company_status_type_id'] );
		if( isset( $arrValues['ps_lead_origin_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadOriginId', trim( $arrValues['ps_lead_origin_id'] ) ); elseif( isset( $arrValues['ps_lead_origin_id'] ) ) $this->setPsLeadOriginId( $arrValues['ps_lead_origin_id'] );
		if( isset( $arrValues['ps_lead_source_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadSourceId', trim( $arrValues['ps_lead_source_id'] ) ); elseif( isset( $arrValues['ps_lead_source_id'] ) ) $this->setPsLeadSourceId( $arrValues['ps_lead_source_id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['ps_lead_event_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadEventId', trim( $arrValues['ps_lead_event_id'] ) ); elseif( isset( $arrValues['ps_lead_event_id'] ) ) $this->setPsLeadEventId( $arrValues['ps_lead_event_id'] );
		if( isset( $arrValues['bluemoon_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_intBluemoonRemotePrimaryKey', trim( $arrValues['bluemoon_remote_primary_key'] ) ); elseif( isset( $arrValues['bluemoon_remote_primary_key'] ) ) $this->setBluemoonRemotePrimaryKey( $arrValues['bluemoon_remote_primary_key'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['number_of_units'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfUnits', trim( $arrValues['number_of_units'] ) ); elseif( isset( $arrValues['number_of_units'] ) ) $this->setNumberOfUnits( $arrValues['number_of_units'] );
		if( isset( $arrValues['property_count'] ) && $boolDirectSet ) $this->set( 'm_intPropertyCount', trim( $arrValues['property_count'] ) ); elseif( isset( $arrValues['property_count'] ) ) $this->setPropertyCount( $arrValues['property_count'] );
		if( isset( $arrValues['request_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequestDatetime', trim( $arrValues['request_datetime'] ) ); elseif( isset( $arrValues['request_datetime'] ) ) $this->setRequestDatetime( $arrValues['request_datetime'] );
		if( isset( $arrValues['request'] ) && $boolDirectSet ) $this->set( 'm_strRequest', trim( $arrValues['request'] ) ); elseif( isset( $arrValues['request'] ) ) $this->setRequest( $arrValues['request'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['profile_percent'] ) && $boolDirectSet ) $this->set( 'm_fltProfilePercent', trim( $arrValues['profile_percent'] ) ); elseif( isset( $arrValues['profile_percent'] ) ) $this->setProfilePercent( $arrValues['profile_percent'] );
		if( isset( $arrValues['needs_follow_up'] ) && $boolDirectSet ) $this->set( 'm_intNeedsFollowUp', trim( $arrValues['needs_follow_up'] ) ); elseif( isset( $arrValues['needs_follow_up'] ) ) $this->setNeedsFollowUp( $arrValues['needs_follow_up'] );
		if( isset( $arrValues['priority_number'] ) && $boolDirectSet ) $this->set( 'm_intPriorityNumber', trim( $arrValues['priority_number'] ) ); elseif( isset( $arrValues['priority_number'] ) ) $this->setPriorityNumber( $arrValues['priority_number'] );
		if( isset( $arrValues['keywords'] ) && $boolDirectSet ) $this->set( 'm_strKeywords', trim( $arrValues['keywords'] ) ); elseif( isset( $arrValues['keywords'] ) ) $this->setKeywords( $arrValues['keywords'] );
		if( isset( $arrValues['referral_url'] ) && $boolDirectSet ) $this->set( 'm_strReferralUrl', trim( $arrValues['referral_url'] ) ); elseif( isset( $arrValues['referral_url'] ) ) $this->setReferralUrl( $arrValues['referral_url'] );
		if( isset( $arrValues['referral_domain'] ) && $boolDirectSet ) $this->set( 'm_strReferralDomain', trim( $arrValues['referral_domain'] ) ); elseif( isset( $arrValues['referral_domain'] ) ) $this->setReferralDomain( $arrValues['referral_domain'] );
		if( isset( $arrValues['deduped_on'] ) && $boolDirectSet ) $this->set( 'm_strDedupedOn', trim( $arrValues['deduped_on'] ) ); elseif( isset( $arrValues['deduped_on'] ) ) $this->setDedupedOn( $arrValues['deduped_on'] );
		if( isset( $arrValues['closed_by'] ) && $boolDirectSet ) $this->set( 'm_intClosedBy', trim( $arrValues['closed_by'] ) ); elseif( isset( $arrValues['closed_by'] ) ) $this->setClosedBy( $arrValues['closed_by'] );
		if( isset( $arrValues['closed_on'] ) && $boolDirectSet ) $this->set( 'm_strClosedOn', trim( $arrValues['closed_on'] ) ); elseif( isset( $arrValues['closed_on'] ) ) $this->setClosedOn( $arrValues['closed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['utm_source'] ) && $boolDirectSet ) $this->set( 'm_strUtmSource', trim( $arrValues['utm_source'] ) ); elseif( isset( $arrValues['utm_source'] ) ) $this->setUtmSource( $arrValues['utm_source'] );
		if( isset( $arrValues['utm_medium'] ) && $boolDirectSet ) $this->set( 'm_strUtmMedium', trim( $arrValues['utm_medium'] ) ); elseif( isset( $arrValues['utm_medium'] ) ) $this->setUtmMedium( $arrValues['utm_medium'] );
		if( isset( $arrValues['utm_campaign'] ) && $boolDirectSet ) $this->set( 'm_strUtmCampaign', trim( $arrValues['utm_campaign'] ) ); elseif( isset( $arrValues['utm_campaign'] ) ) $this->setUtmCampaign( $arrValues['utm_campaign'] );
		if( isset( $arrValues['utm_term'] ) && $boolDirectSet ) $this->set( 'm_strUtmTerm', trim( $arrValues['utm_term'] ) ); elseif( isset( $arrValues['utm_term'] ) ) $this->setUtmTerm( $arrValues['utm_term'] );
		if( isset( $arrValues['utm_content'] ) && $boolDirectSet ) $this->set( 'm_strUtmContent', trim( $arrValues['utm_content'] ) ); elseif( isset( $arrValues['utm_content'] ) ) $this->setUtmContent( $arrValues['utm_content'] );
		if( isset( $arrValues['ps_lead_stage_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadStageId', trim( $arrValues['ps_lead_stage_id'] ) ); elseif( isset( $arrValues['ps_lead_stage_id'] ) ) $this->setPsLeadStageId( $arrValues['ps_lead_stage_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCompanyStatusTypeId( $intCompanyStatusTypeId ) {
		$this->set( 'm_intCompanyStatusTypeId', CStrings::strToIntDef( $intCompanyStatusTypeId, NULL, false ) );
	}

	public function getCompanyStatusTypeId() {
		return $this->m_intCompanyStatusTypeId;
	}

	public function sqlCompanyStatusTypeId() {
		return ( true == isset( $this->m_intCompanyStatusTypeId ) ) ? ( string ) $this->m_intCompanyStatusTypeId : 'NULL';
	}

	public function setPsLeadOriginId( $intPsLeadOriginId ) {
		$this->set( 'm_intPsLeadOriginId', CStrings::strToIntDef( $intPsLeadOriginId, NULL, false ) );
	}

	public function getPsLeadOriginId() {
		return $this->m_intPsLeadOriginId;
	}

	public function sqlPsLeadOriginId() {
		return ( true == isset( $this->m_intPsLeadOriginId ) ) ? ( string ) $this->m_intPsLeadOriginId : 'NULL';
	}

	public function setPsLeadSourceId( $intPsLeadSourceId ) {
		$this->set( 'm_intPsLeadSourceId', CStrings::strToIntDef( $intPsLeadSourceId, NULL, false ) );
	}

	public function getPsLeadSourceId() {
		return $this->m_intPsLeadSourceId;
	}

	public function sqlPsLeadSourceId() {
		return ( true == isset( $this->m_intPsLeadSourceId ) ) ? ( string ) $this->m_intPsLeadSourceId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setPsLeadEventId( $intPsLeadEventId ) {
		$this->set( 'm_intPsLeadEventId', CStrings::strToIntDef( $intPsLeadEventId, NULL, false ) );
	}

	public function getPsLeadEventId() {
		return $this->m_intPsLeadEventId;
	}

	public function sqlPsLeadEventId() {
		return ( true == isset( $this->m_intPsLeadEventId ) ) ? ( string ) $this->m_intPsLeadEventId : 'NULL';
	}

	public function setBluemoonRemotePrimaryKey( $intBluemoonRemotePrimaryKey ) {
		$this->set( 'm_intBluemoonRemotePrimaryKey', CStrings::strToIntDef( $intBluemoonRemotePrimaryKey, NULL, false ) );
	}

	public function getBluemoonRemotePrimaryKey() {
		return $this->m_intBluemoonRemotePrimaryKey;
	}

	public function sqlBluemoonRemotePrimaryKey() {
		return ( true == isset( $this->m_intBluemoonRemotePrimaryKey ) ) ? ( string ) $this->m_intBluemoonRemotePrimaryKey : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->set( 'm_intNumberOfUnits', CStrings::strToIntDef( $intNumberOfUnits, NULL, false ) );
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function sqlNumberOfUnits() {
		return ( true == isset( $this->m_intNumberOfUnits ) ) ? ( string ) $this->m_intNumberOfUnits : 'NULL';
	}

	public function setPropertyCount( $intPropertyCount ) {
		$this->set( 'm_intPropertyCount', CStrings::strToIntDef( $intPropertyCount, NULL, false ) );
	}

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function sqlPropertyCount() {
		return ( true == isset( $this->m_intPropertyCount ) ) ? ( string ) $this->m_intPropertyCount : 'NULL';
	}

	public function setRequestDatetime( $strRequestDatetime ) {
		$this->set( 'm_strRequestDatetime', CStrings::strTrimDef( $strRequestDatetime, -1, NULL, true ) );
	}

	public function getRequestDatetime() {
		return $this->m_strRequestDatetime;
	}

	public function sqlRequestDatetime() {
		return ( true == isset( $this->m_strRequestDatetime ) ) ? '\'' . $this->m_strRequestDatetime . '\'' : 'NOW()';
	}

	public function setRequest( $strRequest ) {
		$this->set( 'm_strRequest', CStrings::strTrimDef( $strRequest, -1, NULL, true ) );
	}

	public function getRequest() {
		return $this->m_strRequest;
	}

	public function sqlRequest() {
		return ( true == isset( $this->m_strRequest ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRequest ) : '\'' . addslashes( $this->m_strRequest ) . '\'' ) : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 23, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setProfilePercent( $fltProfilePercent ) {
		$this->set( 'm_fltProfilePercent', CStrings::strToFloatDef( $fltProfilePercent, NULL, false, 2 ) );
	}

	public function getProfilePercent() {
		return $this->m_fltProfilePercent;
	}

	public function sqlProfilePercent() {
		return ( true == isset( $this->m_fltProfilePercent ) ) ? ( string ) $this->m_fltProfilePercent : '0';
	}

	public function setNeedsFollowUp( $intNeedsFollowUp ) {
		$this->set( 'm_intNeedsFollowUp', CStrings::strToIntDef( $intNeedsFollowUp, NULL, false ) );
	}

	public function getNeedsFollowUp() {
		return $this->m_intNeedsFollowUp;
	}

	public function sqlNeedsFollowUp() {
		return ( true == isset( $this->m_intNeedsFollowUp ) ) ? ( string ) $this->m_intNeedsFollowUp : '0';
	}

	public function setPriorityNumber( $intPriorityNumber ) {
		$this->set( 'm_intPriorityNumber', CStrings::strToIntDef( $intPriorityNumber, NULL, false ) );
	}

	public function getPriorityNumber() {
		return $this->m_intPriorityNumber;
	}

	public function sqlPriorityNumber() {
		return ( true == isset( $this->m_intPriorityNumber ) ) ? ( string ) $this->m_intPriorityNumber : '1';
	}

	public function setKeywords( $strKeywords ) {
		$this->set( 'm_strKeywords', CStrings::strTrimDef( $strKeywords, 240, NULL, true ) );
	}

	public function getKeywords() {
		return $this->m_strKeywords;
	}

	public function sqlKeywords() {
		return ( true == isset( $this->m_strKeywords ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strKeywords ) : '\'' . addslashes( $this->m_strKeywords ) . '\'' ) : 'NULL';
	}

	public function setReferralUrl( $strReferralUrl ) {
		$this->set( 'm_strReferralUrl', CStrings::strTrimDef( $strReferralUrl, 1000, NULL, true ) );
	}

	public function getReferralUrl() {
		return $this->m_strReferralUrl;
	}

	public function sqlReferralUrl() {
		return ( true == isset( $this->m_strReferralUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReferralUrl ) : '\'' . addslashes( $this->m_strReferralUrl ) . '\'' ) : 'NULL';
	}

	public function setReferralDomain( $strReferralDomain ) {
		$this->set( 'm_strReferralDomain', CStrings::strTrimDef( $strReferralDomain, 240, NULL, true ) );
	}

	public function getReferralDomain() {
		return $this->m_strReferralDomain;
	}

	public function sqlReferralDomain() {
		return ( true == isset( $this->m_strReferralDomain ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReferralDomain ) : '\'' . addslashes( $this->m_strReferralDomain ) . '\'' ) : 'NULL';
	}

	public function setDedupedOn( $strDedupedOn ) {
		$this->set( 'm_strDedupedOn', CStrings::strTrimDef( $strDedupedOn, -1, NULL, true ) );
	}

	public function getDedupedOn() {
		return $this->m_strDedupedOn;
	}

	public function sqlDedupedOn() {
		return ( true == isset( $this->m_strDedupedOn ) ) ? '\'' . $this->m_strDedupedOn . '\'' : 'NULL';
	}

	public function setClosedBy( $intClosedBy ) {
		$this->set( 'm_intClosedBy', CStrings::strToIntDef( $intClosedBy, NULL, false ) );
	}

	public function getClosedBy() {
		return $this->m_intClosedBy;
	}

	public function sqlClosedBy() {
		return ( true == isset( $this->m_intClosedBy ) ) ? ( string ) $this->m_intClosedBy : 'NULL';
	}

	public function setClosedOn( $strClosedOn ) {
		$this->set( 'm_strClosedOn', CStrings::strTrimDef( $strClosedOn, -1, NULL, true ) );
	}

	public function getClosedOn() {
		return $this->m_strClosedOn;
	}

	public function sqlClosedOn() {
		return ( true == isset( $this->m_strClosedOn ) ) ? '\'' . $this->m_strClosedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUtmSource( $strUtmSource ) {
		$this->set( 'm_strUtmSource', CStrings::strTrimDef( $strUtmSource, 100, NULL, true ) );
	}

	public function getUtmSource() {
		return $this->m_strUtmSource;
	}

	public function sqlUtmSource() {
		return ( true == isset( $this->m_strUtmSource ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUtmSource ) : '\'' . addslashes( $this->m_strUtmSource ) . '\'' ) : 'NULL';
	}

	public function setUtmMedium( $strUtmMedium ) {
		$this->set( 'm_strUtmMedium', CStrings::strTrimDef( $strUtmMedium, 100, NULL, true ) );
	}

	public function getUtmMedium() {
		return $this->m_strUtmMedium;
	}

	public function sqlUtmMedium() {
		return ( true == isset( $this->m_strUtmMedium ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUtmMedium ) : '\'' . addslashes( $this->m_strUtmMedium ) . '\'' ) : 'NULL';
	}

	public function setUtmCampaign( $strUtmCampaign ) {
		$this->set( 'm_strUtmCampaign', CStrings::strTrimDef( $strUtmCampaign, 100, NULL, true ) );
	}

	public function getUtmCampaign() {
		return $this->m_strUtmCampaign;
	}

	public function sqlUtmCampaign() {
		return ( true == isset( $this->m_strUtmCampaign ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUtmCampaign ) : '\'' . addslashes( $this->m_strUtmCampaign ) . '\'' ) : 'NULL';
	}

	public function setUtmTerm( $strUtmTerm ) {
		$this->set( 'm_strUtmTerm', CStrings::strTrimDef( $strUtmTerm, 100, NULL, true ) );
	}

	public function getUtmTerm() {
		return $this->m_strUtmTerm;
	}

	public function sqlUtmTerm() {
		return ( true == isset( $this->m_strUtmTerm ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUtmTerm ) : '\'' . addslashes( $this->m_strUtmTerm ) . '\'' ) : 'NULL';
	}

	public function setUtmContent( $strUtmContent ) {
		$this->set( 'm_strUtmContent', CStrings::strTrimDef( $strUtmContent, 100, NULL, true ) );
	}

	public function getUtmContent() {
		return $this->m_strUtmContent;
	}

	public function sqlUtmContent() {
		return ( true == isset( $this->m_strUtmContent ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUtmContent ) : '\'' . addslashes( $this->m_strUtmContent ) . '\'' ) : 'NULL';
	}

	public function setPsLeadStageId( $intPsLeadStageId ) {
		$this->set( 'm_intPsLeadStageId', CStrings::strToIntDef( $intPsLeadStageId, NULL, false ) );
	}

	public function getPsLeadStageId() {
		return $this->m_intPsLeadStageId;
	}

	public function sqlPsLeadStageId() {
		return ( true == isset( $this->m_intPsLeadStageId ) ) ? ( string ) $this->m_intPsLeadStageId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, company_status_type_id, ps_lead_origin_id, ps_lead_source_id, ps_lead_id, ps_lead_event_id, bluemoon_remote_primary_key, company_name, number_of_units, property_count, request_datetime, request, ip_address, notes, profile_percent, needs_follow_up, priority_number, keywords, referral_url, referral_domain, deduped_on, closed_by, closed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, utm_source, utm_medium, utm_campaign, utm_term, utm_content, ps_lead_stage_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCompanyStatusTypeId() . ', ' .
						$this->sqlPsLeadOriginId() . ', ' .
						$this->sqlPsLeadSourceId() . ', ' .
						$this->sqlPsLeadId() . ', ' .
						$this->sqlPsLeadEventId() . ', ' .
						$this->sqlBluemoonRemotePrimaryKey() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlNumberOfUnits() . ', ' .
						$this->sqlPropertyCount() . ', ' .
						$this->sqlRequestDatetime() . ', ' .
						$this->sqlRequest() . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlProfilePercent() . ', ' .
						$this->sqlNeedsFollowUp() . ', ' .
						$this->sqlPriorityNumber() . ', ' .
						$this->sqlKeywords() . ', ' .
						$this->sqlReferralUrl() . ', ' .
						$this->sqlReferralDomain() . ', ' .
						$this->sqlDedupedOn() . ', ' .
						$this->sqlClosedBy() . ', ' .
						$this->sqlClosedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlUtmSource() . ', ' .
						$this->sqlUtmMedium() . ', ' .
						$this->sqlUtmCampaign() . ', ' .
						$this->sqlUtmTerm() . ', ' .
						$this->sqlUtmContent() . ', ' .
						$this->sqlPsLeadStageId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_status_type_id = ' . $this->sqlCompanyStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'CompanyStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_status_type_id = ' . $this->sqlCompanyStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_origin_id = ' . $this->sqlPsLeadOriginId(). ',' ; } elseif( true == array_key_exists( 'PsLeadOriginId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_origin_id = ' . $this->sqlPsLeadOriginId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_source_id = ' . $this->sqlPsLeadSourceId(). ',' ; } elseif( true == array_key_exists( 'PsLeadSourceId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_source_id = ' . $this->sqlPsLeadSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_event_id = ' . $this->sqlPsLeadEventId(). ',' ; } elseif( true == array_key_exists( 'PsLeadEventId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_event_id = ' . $this->sqlPsLeadEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bluemoon_remote_primary_key = ' . $this->sqlBluemoonRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'BluemoonRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' bluemoon_remote_primary_key = ' . $this->sqlBluemoonRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits(). ',' ; } elseif( true == array_key_exists( 'NumberOfUnits', $this->getChangedColumns() ) ) { $strSql .= ' number_of_units = ' . $this->sqlNumberOfUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_count = ' . $this->sqlPropertyCount(). ',' ; } elseif( true == array_key_exists( 'PropertyCount', $this->getChangedColumns() ) ) { $strSql .= ' property_count = ' . $this->sqlPropertyCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime(). ',' ; } elseif( true == array_key_exists( 'RequestDatetime', $this->getChangedColumns() ) ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request = ' . $this->sqlRequest(). ',' ; } elseif( true == array_key_exists( 'Request', $this->getChangedColumns() ) ) { $strSql .= ' request = ' . $this->sqlRequest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' profile_percent = ' . $this->sqlProfilePercent(). ',' ; } elseif( true == array_key_exists( 'ProfilePercent', $this->getChangedColumns() ) ) { $strSql .= ' profile_percent = ' . $this->sqlProfilePercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' needs_follow_up = ' . $this->sqlNeedsFollowUp(). ',' ; } elseif( true == array_key_exists( 'NeedsFollowUp', $this->getChangedColumns() ) ) { $strSql .= ' needs_follow_up = ' . $this->sqlNeedsFollowUp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' priority_number = ' . $this->sqlPriorityNumber(). ',' ; } elseif( true == array_key_exists( 'PriorityNumber', $this->getChangedColumns() ) ) { $strSql .= ' priority_number = ' . $this->sqlPriorityNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keywords = ' . $this->sqlKeywords(). ',' ; } elseif( true == array_key_exists( 'Keywords', $this->getChangedColumns() ) ) { $strSql .= ' keywords = ' . $this->sqlKeywords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' referral_url = ' . $this->sqlReferralUrl(). ',' ; } elseif( true == array_key_exists( 'ReferralUrl', $this->getChangedColumns() ) ) { $strSql .= ' referral_url = ' . $this->sqlReferralUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' referral_domain = ' . $this->sqlReferralDomain(). ',' ; } elseif( true == array_key_exists( 'ReferralDomain', $this->getChangedColumns() ) ) { $strSql .= ' referral_domain = ' . $this->sqlReferralDomain() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deduped_on = ' . $this->sqlDedupedOn(). ',' ; } elseif( true == array_key_exists( 'DedupedOn', $this->getChangedColumns() ) ) { $strSql .= ' deduped_on = ' . $this->sqlDedupedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' closed_by = ' . $this->sqlClosedBy(). ',' ; } elseif( true == array_key_exists( 'ClosedBy', $this->getChangedColumns() ) ) { $strSql .= ' closed_by = ' . $this->sqlClosedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' closed_on = ' . $this->sqlClosedOn(). ',' ; } elseif( true == array_key_exists( 'ClosedOn', $this->getChangedColumns() ) ) { $strSql .= ' closed_on = ' . $this->sqlClosedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utm_source = ' . $this->sqlUtmSource(). ',' ; } elseif( true == array_key_exists( 'UtmSource', $this->getChangedColumns() ) ) { $strSql .= ' utm_source = ' . $this->sqlUtmSource() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utm_medium = ' . $this->sqlUtmMedium(). ',' ; } elseif( true == array_key_exists( 'UtmMedium', $this->getChangedColumns() ) ) { $strSql .= ' utm_medium = ' . $this->sqlUtmMedium() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utm_campaign = ' . $this->sqlUtmCampaign(). ',' ; } elseif( true == array_key_exists( 'UtmCampaign', $this->getChangedColumns() ) ) { $strSql .= ' utm_campaign = ' . $this->sqlUtmCampaign() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utm_term = ' . $this->sqlUtmTerm(). ',' ; } elseif( true == array_key_exists( 'UtmTerm', $this->getChangedColumns() ) ) { $strSql .= ' utm_term = ' . $this->sqlUtmTerm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' utm_content = ' . $this->sqlUtmContent(). ',' ; } elseif( true == array_key_exists( 'UtmContent', $this->getChangedColumns() ) ) { $strSql .= ' utm_content = ' . $this->sqlUtmContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_stage_id = ' . $this->sqlPsLeadStageId(). ',' ; } elseif( true == array_key_exists( 'PsLeadStageId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_stage_id = ' . $this->sqlPsLeadStageId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'company_status_type_id' => $this->getCompanyStatusTypeId(),
			'ps_lead_origin_id' => $this->getPsLeadOriginId(),
			'ps_lead_source_id' => $this->getPsLeadSourceId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'ps_lead_event_id' => $this->getPsLeadEventId(),
			'bluemoon_remote_primary_key' => $this->getBluemoonRemotePrimaryKey(),
			'company_name' => $this->getCompanyName(),
			'number_of_units' => $this->getNumberOfUnits(),
			'property_count' => $this->getPropertyCount(),
			'request_datetime' => $this->getRequestDatetime(),
			'request' => $this->getRequest(),
			'ip_address' => $this->getIpAddress(),
			'notes' => $this->getNotes(),
			'profile_percent' => $this->getProfilePercent(),
			'needs_follow_up' => $this->getNeedsFollowUp(),
			'priority_number' => $this->getPriorityNumber(),
			'keywords' => $this->getKeywords(),
			'referral_url' => $this->getReferralUrl(),
			'referral_domain' => $this->getReferralDomain(),
			'deduped_on' => $this->getDedupedOn(),
			'closed_by' => $this->getClosedBy(),
			'closed_on' => $this->getClosedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'utm_source' => $this->getUtmSource(),
			'utm_medium' => $this->getUtmMedium(),
			'utm_campaign' => $this->getUtmCampaign(),
			'utm_term' => $this->getUtmTerm(),
			'utm_content' => $this->getUtmContent(),
			'ps_lead_stage_id' => $this->getPsLeadStageId()
		);
	}

}
?>