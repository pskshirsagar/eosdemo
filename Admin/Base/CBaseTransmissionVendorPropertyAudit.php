<?php

/**
 * Warning : This is a composite key based class. If you are regenerating the base
 * class again, please do so by checking the composite key checkbox.
 */
class CBaseTransmissionVendorPropertyAudit extends CEosSingularBase {

	const TABLE_NAME = 'public.transmission_vendor_property_audits';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intTransmissionVendorId;
	protected $m_strPropertyName;
	protected $m_strDescription;
	protected $m_strCategoryIds;
	protected $m_strEmailAddress;
	protected $m_strPhoneNumber;
	protected $m_strAlternatePhoneNumber;
	protected $m_strFaxNumber;
	protected $m_strTollfreeNumber;
	protected $m_strYearEstablished;
	protected $m_strBusinessHours;
	protected $m_strSpecialOffer;
	protected $m_strLogo;
	protected $m_strFacebookPageUrl;
	protected $m_strTwitterHandle;
	protected $m_strPhoto;
	protected $m_strWebsiteUrl;
	protected $m_strVideoUrl;
	protected $m_strSpecialOfferUrl;
	protected $m_strAddress1;
	protected $m_strAddress2;
	protected $m_strCity;
	protected $m_strState;
	protected $m_strPostalCode;
	protected $m_strLatitude;
	protected $m_strLongitude;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['transmission_vendor_id'] ) && $boolDirectSet ) $this->set( 'm_intTransmissionVendorId', trim( $arrValues['transmission_vendor_id'] ) ); elseif( isset( $arrValues['transmission_vendor_id'] ) ) $this->setTransmissionVendorId( $arrValues['transmission_vendor_id'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['category_ids'] ) && $boolDirectSet ) $this->set( 'm_strCategoryIds', trim( stripcslashes( $arrValues['category_ids'] ) ) ); elseif( isset( $arrValues['category_ids'] ) ) $this->setCategoryIds( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['category_ids'] ) : $arrValues['category_ids'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['alternate_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strAlternatePhoneNumber', trim( stripcslashes( $arrValues['alternate_phone_number'] ) ) ); elseif( isset( $arrValues['alternate_phone_number'] ) ) $this->setAlternatePhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['alternate_phone_number'] ) : $arrValues['alternate_phone_number'] );
		if( isset( $arrValues['fax_number'] ) && $boolDirectSet ) $this->set( 'm_strFaxNumber', trim( stripcslashes( $arrValues['fax_number'] ) ) ); elseif( isset( $arrValues['fax_number'] ) ) $this->setFaxNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['fax_number'] ) : $arrValues['fax_number'] );
		if( isset( $arrValues['tollfree_number'] ) && $boolDirectSet ) $this->set( 'm_strTollfreeNumber', trim( stripcslashes( $arrValues['tollfree_number'] ) ) ); elseif( isset( $arrValues['tollfree_number'] ) ) $this->setTollfreeNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['tollfree_number'] ) : $arrValues['tollfree_number'] );
		if( isset( $arrValues['year_established'] ) && $boolDirectSet ) $this->set( 'm_strYearEstablished', trim( $arrValues['year_established'] ) ); elseif( isset( $arrValues['year_established'] ) ) $this->setYearEstablished( $arrValues['year_established'] );
		if( isset( $arrValues['business_hours'] ) && $boolDirectSet ) $this->set( 'm_strBusinessHours', trim( stripcslashes( $arrValues['business_hours'] ) ) ); elseif( isset( $arrValues['business_hours'] ) ) $this->setBusinessHours( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['business_hours'] ) : $arrValues['business_hours'] );
		if( isset( $arrValues['special_offer'] ) && $boolDirectSet ) $this->set( 'm_strSpecialOffer', trim( stripcslashes( $arrValues['special_offer'] ) ) ); elseif( isset( $arrValues['special_offer'] ) ) $this->setSpecialOffer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['special_offer'] ) : $arrValues['special_offer'] );
		if( isset( $arrValues['logo'] ) && $boolDirectSet ) $this->set( 'm_strLogo', trim( stripcslashes( $arrValues['logo'] ) ) ); elseif( isset( $arrValues['logo'] ) ) $this->setLogo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['logo'] ) : $arrValues['logo'] );
		if( isset( $arrValues['facebook_page_url'] ) && $boolDirectSet ) $this->set( 'm_strFacebookPageUrl', trim( stripcslashes( $arrValues['facebook_page_url'] ) ) ); elseif( isset( $arrValues['facebook_page_url'] ) ) $this->setFacebookPageUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['facebook_page_url'] ) : $arrValues['facebook_page_url'] );
		if( isset( $arrValues['twitter_handle'] ) && $boolDirectSet ) $this->set( 'm_strTwitterHandle', trim( stripcslashes( $arrValues['twitter_handle'] ) ) ); elseif( isset( $arrValues['twitter_handle'] ) ) $this->setTwitterHandle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['twitter_handle'] ) : $arrValues['twitter_handle'] );
		if( isset( $arrValues['photo'] ) && $boolDirectSet ) $this->set( 'm_strPhoto', trim( stripcslashes( $arrValues['photo'] ) ) ); elseif( isset( $arrValues['photo'] ) ) $this->setPhoto( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['photo'] ) : $arrValues['photo'] );
		if( isset( $arrValues['website_url'] ) && $boolDirectSet ) $this->set( 'm_strWebsiteUrl', trim( stripcslashes( $arrValues['website_url'] ) ) ); elseif( isset( $arrValues['website_url'] ) ) $this->setWebsiteUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['website_url'] ) : $arrValues['website_url'] );
		if( isset( $arrValues['video_url'] ) && $boolDirectSet ) $this->set( 'm_strVideoUrl', trim( stripcslashes( $arrValues['video_url'] ) ) ); elseif( isset( $arrValues['video_url'] ) ) $this->setVideoUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['video_url'] ) : $arrValues['video_url'] );
		if( isset( $arrValues['special_offer_url'] ) && $boolDirectSet ) $this->set( 'm_strSpecialOfferUrl', trim( stripcslashes( $arrValues['special_offer_url'] ) ) ); elseif( isset( $arrValues['special_offer_url'] ) ) $this->setSpecialOfferUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['special_offer_url'] ) : $arrValues['special_offer_url'] );
		if( isset( $arrValues['address1'] ) && $boolDirectSet ) $this->set( 'm_strAddress1', trim( stripcslashes( $arrValues['address1'] ) ) ); elseif( isset( $arrValues['address1'] ) ) $this->setAddress1( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address1'] ) : $arrValues['address1'] );
		if( isset( $arrValues['address2'] ) && $boolDirectSet ) $this->set( 'm_strAddress2', trim( stripcslashes( $arrValues['address2'] ) ) ); elseif( isset( $arrValues['address2'] ) ) $this->setAddress2( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address2'] ) : $arrValues['address2'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['state'] ) && $boolDirectSet ) $this->set( 'm_strState', trim( stripcslashes( $arrValues['state'] ) ) ); elseif( isset( $arrValues['state'] ) ) $this->setState( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state'] ) : $arrValues['state'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['latitude'] ) && $boolDirectSet ) $this->set( 'm_strLatitude', trim( stripcslashes( $arrValues['latitude'] ) ) ); elseif( isset( $arrValues['latitude'] ) ) $this->setLatitude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['latitude'] ) : $arrValues['latitude'] );
		if( isset( $arrValues['longitude'] ) && $boolDirectSet ) $this->set( 'm_strLongitude', trim( stripcslashes( $arrValues['longitude'] ) ) ); elseif( isset( $arrValues['longitude'] ) ) $this->setLongitude( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['longitude'] ) : $arrValues['longitude'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setTransmissionVendorId( $intTransmissionVendorId ) {
		$this->set( 'm_intTransmissionVendorId', CStrings::strToIntDef( $intTransmissionVendorId, NULL, false ) );
	}

	public function getTransmissionVendorId() {
		return $this->m_intTransmissionVendorId;
	}

	public function sqlTransmissionVendorId() {
		return ( true == isset( $this->m_intTransmissionVendorId ) ) ? ( string ) $this->m_intTransmissionVendorId : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 250, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setCategoryIds( $strCategoryIds ) {
		$this->set( 'm_strCategoryIds', CStrings::strTrimDef( $strCategoryIds, 25, NULL, true ) );
	}

	public function getCategoryIds() {
		return $this->m_strCategoryIds;
	}

	public function sqlCategoryIds() {
		return ( true == isset( $this->m_strCategoryIds ) ) ? '\'' . addslashes( $this->m_strCategoryIds ) . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 100, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setAlternatePhoneNumber( $strAlternatePhoneNumber ) {
		$this->set( 'm_strAlternatePhoneNumber', CStrings::strTrimDef( $strAlternatePhoneNumber, 30, NULL, true ) );
	}

	public function getAlternatePhoneNumber() {
		return $this->m_strAlternatePhoneNumber;
	}

	public function sqlAlternatePhoneNumber() {
		return ( true == isset( $this->m_strAlternatePhoneNumber ) ) ? '\'' . addslashes( $this->m_strAlternatePhoneNumber ) . '\'' : 'NULL';
	}

	public function setFaxNumber( $strFaxNumber ) {
		$this->set( 'm_strFaxNumber', CStrings::strTrimDef( $strFaxNumber, 30, NULL, true ) );
	}

	public function getFaxNumber() {
		return $this->m_strFaxNumber;
	}

	public function sqlFaxNumber() {
		return ( true == isset( $this->m_strFaxNumber ) ) ? '\'' . addslashes( $this->m_strFaxNumber ) . '\'' : 'NULL';
	}

	public function setTollfreeNumber( $strTollfreeNumber ) {
		$this->set( 'm_strTollfreeNumber', CStrings::strTrimDef( $strTollfreeNumber, 30, NULL, true ) );
	}

	public function getTollfreeNumber() {
		return $this->m_strTollfreeNumber;
	}

	public function sqlTollfreeNumber() {
		return ( true == isset( $this->m_strTollfreeNumber ) ) ? '\'' . addslashes( $this->m_strTollfreeNumber ) . '\'' : 'NULL';
	}

	public function setYearEstablished( $strYearEstablished ) {
		$this->set( 'm_strYearEstablished', CStrings::strTrimDef( $strYearEstablished, -1, NULL, true ) );
	}

	public function getYearEstablished() {
		return $this->m_strYearEstablished;
	}

	public function sqlYearEstablished() {
		return ( true == isset( $this->m_strYearEstablished ) ) ? '\'' . $this->m_strYearEstablished . '\'' : 'NULL';
	}

	public function setBusinessHours( $strBusinessHours ) {
		$this->set( 'm_strBusinessHours', CStrings::strTrimDef( $strBusinessHours, 500, NULL, true ) );
	}

	public function getBusinessHours() {
		return $this->m_strBusinessHours;
	}

	public function sqlBusinessHours() {
		return ( true == isset( $this->m_strBusinessHours ) ) ? '\'' . addslashes( $this->m_strBusinessHours ) . '\'' : 'NULL';
	}

	public function setSpecialOffer( $strSpecialOffer ) {
		$this->set( 'm_strSpecialOffer', CStrings::strTrimDef( $strSpecialOffer, 500, NULL, true ) );
	}

	public function getSpecialOffer() {
		return $this->m_strSpecialOffer;
	}

	public function sqlSpecialOffer() {
		return ( true == isset( $this->m_strSpecialOffer ) ) ? '\'' . addslashes( $this->m_strSpecialOffer ) . '\'' : 'NULL';
	}

	public function setLogo( $strLogo ) {
		$this->set( 'm_strLogo', CStrings::strTrimDef( $strLogo, 500, NULL, true ) );
	}

	public function getLogo() {
		return $this->m_strLogo;
	}

	public function sqlLogo() {
		return ( true == isset( $this->m_strLogo ) ) ? '\'' . addslashes( $this->m_strLogo ) . '\'' : 'NULL';
	}

	public function setFacebookPageUrl( $strFacebookPageUrl ) {
		$this->set( 'm_strFacebookPageUrl', CStrings::strTrimDef( $strFacebookPageUrl, 500, NULL, true ) );
	}

	public function getFacebookPageUrl() {
		return $this->m_strFacebookPageUrl;
	}

	public function sqlFacebookPageUrl() {
		return ( true == isset( $this->m_strFacebookPageUrl ) ) ? '\'' . addslashes( $this->m_strFacebookPageUrl ) . '\'' : 'NULL';
	}

	public function setTwitterHandle( $strTwitterHandle ) {
		$this->set( 'm_strTwitterHandle', CStrings::strTrimDef( $strTwitterHandle, 100, NULL, true ) );
	}

	public function getTwitterHandle() {
		return $this->m_strTwitterHandle;
	}

	public function sqlTwitterHandle() {
		return ( true == isset( $this->m_strTwitterHandle ) ) ? '\'' . addslashes( $this->m_strTwitterHandle ) . '\'' : 'NULL';
	}

	public function setPhoto( $strPhoto ) {
		$this->set( 'm_strPhoto', CStrings::strTrimDef( $strPhoto, 500, NULL, true ) );
	}

	public function getPhoto() {
		return $this->m_strPhoto;
	}

	public function sqlPhoto() {
		return ( true == isset( $this->m_strPhoto ) ) ? '\'' . addslashes( $this->m_strPhoto ) . '\'' : 'NULL';
	}

	public function setWebsiteUrl( $strWebsiteUrl ) {
		$this->set( 'm_strWebsiteUrl', CStrings::strTrimDef( $strWebsiteUrl, 2000, NULL, true ) );
	}

	public function getWebsiteUrl() {
		return $this->m_strWebsiteUrl;
	}

	public function sqlWebsiteUrl() {
		return ( true == isset( $this->m_strWebsiteUrl ) ) ? '\'' . addslashes( $this->m_strWebsiteUrl ) . '\'' : 'NULL';
	}

	public function setVideoUrl( $strVideoUrl ) {
		$this->set( 'm_strVideoUrl', CStrings::strTrimDef( $strVideoUrl, 500, NULL, true ) );
	}

	public function getVideoUrl() {
		return $this->m_strVideoUrl;
	}

	public function sqlVideoUrl() {
		return ( true == isset( $this->m_strVideoUrl ) ) ? '\'' . addslashes( $this->m_strVideoUrl ) . '\'' : 'NULL';
	}

	public function setSpecialOfferUrl( $strSpecialOfferUrl ) {
		$this->set( 'm_strSpecialOfferUrl', CStrings::strTrimDef( $strSpecialOfferUrl, 2000, NULL, true ) );
	}

	public function getSpecialOfferUrl() {
		return $this->m_strSpecialOfferUrl;
	}

	public function sqlSpecialOfferUrl() {
		return ( true == isset( $this->m_strSpecialOfferUrl ) ) ? '\'' . addslashes( $this->m_strSpecialOfferUrl ) . '\'' : 'NULL';
	}

	public function setAddress1( $strAddress1 ) {
		$this->set( 'm_strAddress1', CStrings::strTrimDef( $strAddress1, 100, NULL, true ) );
	}

	public function getAddress1() {
		return $this->m_strAddress1;
	}

	public function sqlAddress1() {
		return ( true == isset( $this->m_strAddress1 ) ) ? '\'' . addslashes( $this->m_strAddress1 ) . '\'' : 'NULL';
	}

	public function setAddress2( $strAddress2 ) {
		$this->set( 'm_strAddress2', CStrings::strTrimDef( $strAddress2, 100, NULL, true ) );
	}

	public function getAddress2() {
		return $this->m_strAddress2;
	}

	public function sqlAddress2() {
		return ( true == isset( $this->m_strAddress2 ) ) ? '\'' . addslashes( $this->m_strAddress2 ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setState( $strState ) {
		$this->set( 'm_strState', CStrings::strTrimDef( $strState, 50, NULL, true ) );
	}

	public function getState() {
		return $this->m_strState;
	}

	public function sqlState() {
		return ( true == isset( $this->m_strState ) ) ? '\'' . addslashes( $this->m_strState ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setLatitude( $strLatitude ) {
		$this->set( 'm_strLatitude', CStrings::strTrimDef( $strLatitude, 50, NULL, true ) );
	}

	public function getLatitude() {
		return $this->m_strLatitude;
	}

	public function sqlLatitude() {
		return ( true == isset( $this->m_strLatitude ) ) ? '\'' . addslashes( $this->m_strLatitude ) . '\'' : 'NULL';
	}

	public function setLongitude( $strLongitude ) {
		$this->set( 'm_strLongitude', CStrings::strTrimDef( $strLongitude, 50, NULL, true ) );
	}

	public function getLongitude() {
		return $this->m_strLongitude;
	}

	public function sqlLongitude() {
		return ( true == isset( $this->m_strLongitude ) ) ? '\'' . addslashes( $this->m_strLongitude ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, transmission_vendor_id, property_name, description, category_ids, email_address, phone_number, alternate_phone_number, fax_number, tollfree_number, year_established, business_hours, special_offer, logo, facebook_page_url, twitter_handle, photo, website_url, video_url, special_offer_url, address1, address2, city, state, postal_code, latitude, longitude, approved_by, approved_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlTransmissionVendorId() . ', ' .
 						$this->sqlPropertyName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlCategoryIds() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlAlternatePhoneNumber() . ', ' .
 						$this->sqlFaxNumber() . ', ' .
 						$this->sqlTollfreeNumber() . ', ' .
 						$this->sqlYearEstablished() . ', ' .
 						$this->sqlBusinessHours() . ', ' .
 						$this->sqlSpecialOffer() . ', ' .
 						$this->sqlLogo() . ', ' .
 						$this->sqlFacebookPageUrl() . ', ' .
 						$this->sqlTwitterHandle() . ', ' .
 						$this->sqlPhoto() . ', ' .
 						$this->sqlWebsiteUrl() . ', ' .
 						$this->sqlVideoUrl() . ', ' .
 						$this->sqlSpecialOfferUrl() . ', ' .
 						$this->sqlAddress1() . ', ' .
 						$this->sqlAddress2() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlState() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlLatitude() . ', ' .
 						$this->sqlLongitude() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; } elseif( true == array_key_exists( 'TransmissionVendorId', $this->getChangedColumns() ) ) { $strSql .= ' transmission_vendor_id = ' . $this->sqlTransmissionVendorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' category_ids = ' . $this->sqlCategoryIds() . ','; } elseif( true == array_key_exists( 'CategoryIds', $this->getChangedColumns() ) ) { $strSql .= ' category_ids = ' . $this->sqlCategoryIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' alternate_phone_number = ' . $this->sqlAlternatePhoneNumber() . ','; } elseif( true == array_key_exists( 'AlternatePhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' alternate_phone_number = ' . $this->sqlAlternatePhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber() . ','; } elseif( true == array_key_exists( 'FaxNumber', $this->getChangedColumns() ) ) { $strSql .= ' fax_number = ' . $this->sqlFaxNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tollfree_number = ' . $this->sqlTollfreeNumber() . ','; } elseif( true == array_key_exists( 'TollfreeNumber', $this->getChangedColumns() ) ) { $strSql .= ' tollfree_number = ' . $this->sqlTollfreeNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' year_established = ' . $this->sqlYearEstablished() . ','; } elseif( true == array_key_exists( 'YearEstablished', $this->getChangedColumns() ) ) { $strSql .= ' year_established = ' . $this->sqlYearEstablished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' business_hours = ' . $this->sqlBusinessHours() . ','; } elseif( true == array_key_exists( 'BusinessHours', $this->getChangedColumns() ) ) { $strSql .= ' business_hours = ' . $this->sqlBusinessHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_offer = ' . $this->sqlSpecialOffer() . ','; } elseif( true == array_key_exists( 'SpecialOffer', $this->getChangedColumns() ) ) { $strSql .= ' special_offer = ' . $this->sqlSpecialOffer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' logo = ' . $this->sqlLogo() . ','; } elseif( true == array_key_exists( 'Logo', $this->getChangedColumns() ) ) { $strSql .= ' logo = ' . $this->sqlLogo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' facebook_page_url = ' . $this->sqlFacebookPageUrl() . ','; } elseif( true == array_key_exists( 'FacebookPageUrl', $this->getChangedColumns() ) ) { $strSql .= ' facebook_page_url = ' . $this->sqlFacebookPageUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' twitter_handle = ' . $this->sqlTwitterHandle() . ','; } elseif( true == array_key_exists( 'TwitterHandle', $this->getChangedColumns() ) ) { $strSql .= ' twitter_handle = ' . $this->sqlTwitterHandle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' photo = ' . $this->sqlPhoto() . ','; } elseif( true == array_key_exists( 'Photo', $this->getChangedColumns() ) ) { $strSql .= ' photo = ' . $this->sqlPhoto() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl() . ','; } elseif( true == array_key_exists( 'WebsiteUrl', $this->getChangedColumns() ) ) { $strSql .= ' website_url = ' . $this->sqlWebsiteUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' video_url = ' . $this->sqlVideoUrl() . ','; } elseif( true == array_key_exists( 'VideoUrl', $this->getChangedColumns() ) ) { $strSql .= ' video_url = ' . $this->sqlVideoUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' special_offer_url = ' . $this->sqlSpecialOfferUrl() . ','; } elseif( true == array_key_exists( 'SpecialOfferUrl', $this->getChangedColumns() ) ) { $strSql .= ' special_offer_url = ' . $this->sqlSpecialOfferUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address1 = ' . $this->sqlAddress1() . ','; } elseif( true == array_key_exists( 'Address1', $this->getChangedColumns() ) ) { $strSql .= ' address1 = ' . $this->sqlAddress1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address2 = ' . $this->sqlAddress2() . ','; } elseif( true == array_key_exists( 'Address2', $this->getChangedColumns() ) ) { $strSql .= ' address2 = ' . $this->sqlAddress2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state = ' . $this->sqlState() . ','; } elseif( true == array_key_exists( 'State', $this->getChangedColumns() ) ) { $strSql .= ' state = ' . $this->sqlState() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; } elseif( true == array_key_exists( 'Latitude', $this->getChangedColumns() ) ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; } elseif( true == array_key_exists( 'Longitude', $this->getChangedColumns() ) ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ' AND cid = ' . ( int ) $this->sqlCid() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'transmission_vendor_id' => $this->getTransmissionVendorId(),
			'property_name' => $this->getPropertyName(),
			'description' => $this->getDescription(),
			'category_ids' => $this->getCategoryIds(),
			'email_address' => $this->getEmailAddress(),
			'phone_number' => $this->getPhoneNumber(),
			'alternate_phone_number' => $this->getAlternatePhoneNumber(),
			'fax_number' => $this->getFaxNumber(),
			'tollfree_number' => $this->getTollfreeNumber(),
			'year_established' => $this->getYearEstablished(),
			'business_hours' => $this->getBusinessHours(),
			'special_offer' => $this->getSpecialOffer(),
			'logo' => $this->getLogo(),
			'facebook_page_url' => $this->getFacebookPageUrl(),
			'twitter_handle' => $this->getTwitterHandle(),
			'photo' => $this->getPhoto(),
			'website_url' => $this->getWebsiteUrl(),
			'video_url' => $this->getVideoUrl(),
			'special_offer_url' => $this->getSpecialOfferUrl(),
			'address1' => $this->getAddress1(),
			'address2' => $this->getAddress2(),
			'city' => $this->getCity(),
			'state' => $this->getState(),
			'postal_code' => $this->getPostalCode(),
			'latitude' => $this->getLatitude(),
			'longitude' => $this->getLongitude(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>