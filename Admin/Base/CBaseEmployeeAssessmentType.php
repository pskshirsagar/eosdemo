<?php

class CBaseEmployeeAssessmentType extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_assessment_types';

	protected $m_intId;
	protected $m_intEmployeeAssessmentTypeLevelId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intIsPublished;
	protected $m_intIsVerificationRequired;
	protected $m_intIsTenStarRating;
	protected $m_intIs360Review;
	protected $m_intIsPeerReview;
	protected $m_intIsShowPeerComment;
	protected $m_boolIsShowPeerQuestions;
	protected $m_arrstrCountryCode;
	protected $m_intDepartmentId;
	protected $m_intOrderNum;
	protected $m_strEmployeeDueDate;
	protected $m_strManagerDueDate;
	protected $m_strCompletionDueDate;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '0';
		$this->m_intIsVerificationRequired = '0';
		$this->m_intIsTenStarRating = '1';
		$this->m_intIs360Review = '0';
		$this->m_intIsShowPeerComment = '0';
		$this->m_boolIsShowPeerQuestions = false;
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_assessment_type_level_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeAssessmentTypeLevelId', trim( $arrValues['employee_assessment_type_level_id'] ) ); elseif( isset( $arrValues['employee_assessment_type_level_id'] ) ) $this->setEmployeeAssessmentTypeLevelId( $arrValues['employee_assessment_type_level_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_verification_required'] ) && $boolDirectSet ) $this->set( 'm_intIsVerificationRequired', trim( $arrValues['is_verification_required'] ) ); elseif( isset( $arrValues['is_verification_required'] ) ) $this->setIsVerificationRequired( $arrValues['is_verification_required'] );
		if( isset( $arrValues['is_ten_star_rating'] ) && $boolDirectSet ) $this->set( 'm_intIsTenStarRating', trim( $arrValues['is_ten_star_rating'] ) ); elseif( isset( $arrValues['is_ten_star_rating'] ) ) $this->setIsTenStarRating( $arrValues['is_ten_star_rating'] );
		if( isset( $arrValues['is_360_review'] ) && $boolDirectSet ) $this->set( 'm_intIs360Review', trim( $arrValues['is_360_review'] ) ); elseif( isset( $arrValues['is_360_review'] ) ) $this->setIs360Review( $arrValues['is_360_review'] );
		if( isset( $arrValues['is_peer_review'] ) && $boolDirectSet ) $this->set( 'm_intIsPeerReview', trim( $arrValues['is_peer_review'] ) ); elseif( isset( $arrValues['is_peer_review'] ) ) $this->setIsPeerReview( $arrValues['is_peer_review'] );
		if( isset( $arrValues['is_show_peer_comment'] ) && $boolDirectSet ) $this->set( 'm_intIsShowPeerComment', trim( $arrValues['is_show_peer_comment'] ) ); elseif( isset( $arrValues['is_show_peer_comment'] ) ) $this->setIsShowPeerComment( $arrValues['is_show_peer_comment'] );
		if( isset( $arrValues['is_show_peer_questions'] ) && $boolDirectSet ) $this->set( 'm_boolIsShowPeerQuestions', trim( stripcslashes( $arrValues['is_show_peer_questions'] ) ) ); elseif( isset( $arrValues['is_show_peer_questions'] ) ) $this->setIsShowPeerQuestions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_show_peer_questions'] ) : $arrValues['is_show_peer_questions'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_arrstrCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['employee_due_date'] ) && $boolDirectSet ) $this->set( 'm_strEmployeeDueDate', trim( $arrValues['employee_due_date'] ) ); elseif( isset( $arrValues['employee_due_date'] ) ) $this->setEmployeeDueDate( $arrValues['employee_due_date'] );
		if( isset( $arrValues['manager_due_date'] ) && $boolDirectSet ) $this->set( 'm_strManagerDueDate', trim( $arrValues['manager_due_date'] ) ); elseif( isset( $arrValues['manager_due_date'] ) ) $this->setManagerDueDate( $arrValues['manager_due_date'] );
		if( isset( $arrValues['completion_due_date'] ) && $boolDirectSet ) $this->set( 'm_strCompletionDueDate', trim( $arrValues['completion_due_date'] ) ); elseif( isset( $arrValues['completion_due_date'] ) ) $this->setCompletionDueDate( $arrValues['completion_due_date'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeAssessmentTypeLevelId( $intEmployeeAssessmentTypeLevelId ) {
		$this->set( 'm_intEmployeeAssessmentTypeLevelId', CStrings::strToIntDef( $intEmployeeAssessmentTypeLevelId, NULL, false ) );
	}

	public function getEmployeeAssessmentTypeLevelId() {
		return $this->m_intEmployeeAssessmentTypeLevelId;
	}

	public function sqlEmployeeAssessmentTypeLevelId() {
		return ( true == isset( $this->m_intEmployeeAssessmentTypeLevelId ) ) ? ( string ) $this->m_intEmployeeAssessmentTypeLevelId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setIsVerificationRequired( $intIsVerificationRequired ) {
		$this->set( 'm_intIsVerificationRequired', CStrings::strToIntDef( $intIsVerificationRequired, NULL, false ) );
	}

	public function getIsVerificationRequired() {
		return $this->m_intIsVerificationRequired;
	}

	public function sqlIsVerificationRequired() {
		return ( true == isset( $this->m_intIsVerificationRequired ) ) ? ( string ) $this->m_intIsVerificationRequired : '0';
	}

	public function setIsTenStarRating( $intIsTenStarRating ) {
		$this->set( 'm_intIsTenStarRating', CStrings::strToIntDef( $intIsTenStarRating, NULL, false ) );
	}

	public function getIsTenStarRating() {
		return $this->m_intIsTenStarRating;
	}

	public function sqlIsTenStarRating() {
		return ( true == isset( $this->m_intIsTenStarRating ) ) ? ( string ) $this->m_intIsTenStarRating : '1';
	}

	public function setIs360Review( $intIs360Review ) {
		$this->set( 'm_intIs360Review', CStrings::strToIntDef( $intIs360Review, NULL, false ) );
	}

	public function getIs360Review() {
		return $this->m_intIs360Review;
	}

	public function sqlIs360Review() {
		return ( true == isset( $this->m_intIs360Review ) ) ? ( string ) $this->m_intIs360Review : '0';
	}

	public function setIsPeerReview( $intIsPeerReview ) {
		$this->set( 'm_intIsPeerReview', CStrings::strToIntDef( $intIsPeerReview, NULL, false ) );
	}

	public function getIsPeerReview() {
		return $this->m_intIsPeerReview;
	}

	public function sqlIsPeerReview() {
		return ( true == isset( $this->m_intIsPeerReview ) ) ? ( string ) $this->m_intIsPeerReview : 'NULL';
	}

	public function setIsShowPeerComment( $intIsShowPeerComment ) {
		$this->set( 'm_intIsShowPeerComment', CStrings::strToIntDef( $intIsShowPeerComment, NULL, false ) );
	}

	public function getIsShowPeerComment() {
		return $this->m_intIsShowPeerComment;
	}

	public function sqlIsShowPeerComment() {
		return ( true == isset( $this->m_intIsShowPeerComment ) ) ? ( string ) $this->m_intIsShowPeerComment : '0';
	}

	public function setIsShowPeerQuestions( $boolIsShowPeerQuestions ) {
		$this->set( 'm_boolIsShowPeerQuestions', CStrings::strToBool( $boolIsShowPeerQuestions ) );
	}

	public function getIsShowPeerQuestions() {
		return $this->m_boolIsShowPeerQuestions;
	}

	public function sqlIsShowPeerQuestions() {
		return ( true == isset( $this->m_boolIsShowPeerQuestions ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsShowPeerQuestions ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCountryCode( $arrstrCountryCode ) {
		$this->set( 'm_arrstrCountryCode', CStrings::strToArrIntDef( $arrstrCountryCode, NULL ) );
	}

	public function getCountryCode() {
		return $this->m_arrstrCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_arrstrCountryCode ) && true == valArr( $this->m_arrstrCountryCode ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrCountryCode, NULL ) . '\'' : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setEmployeeDueDate( $strEmployeeDueDate ) {
		$this->set( 'm_strEmployeeDueDate', CStrings::strTrimDef( $strEmployeeDueDate, -1, NULL, true ) );
	}

	public function getEmployeeDueDate() {
		return $this->m_strEmployeeDueDate;
	}

	public function sqlEmployeeDueDate() {
		return ( true == isset( $this->m_strEmployeeDueDate ) ) ? '\'' . $this->m_strEmployeeDueDate . '\'' : 'NULL';
	}

	public function setManagerDueDate( $strManagerDueDate ) {
		$this->set( 'm_strManagerDueDate', CStrings::strTrimDef( $strManagerDueDate, -1, NULL, true ) );
	}

	public function getManagerDueDate() {
		return $this->m_strManagerDueDate;
	}

	public function sqlManagerDueDate() {
		return ( true == isset( $this->m_strManagerDueDate ) ) ? '\'' . $this->m_strManagerDueDate . '\'' : 'NULL';
	}

	public function setCompletionDueDate( $strCompletionDueDate ) {
		$this->set( 'm_strCompletionDueDate', CStrings::strTrimDef( $strCompletionDueDate, -1, NULL, true ) );
	}

	public function getCompletionDueDate() {
		return $this->m_strCompletionDueDate;
	}

	public function sqlCompletionDueDate() {
		return ( true == isset( $this->m_strCompletionDueDate ) ) ? '\'' . $this->m_strCompletionDueDate . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_assessment_type_level_id, name, description, is_published, is_verification_required, is_ten_star_rating, is_360_review, is_peer_review, is_show_peer_comment, is_show_peer_questions, country_code, department_id, order_num, employee_due_date, manager_due_date, completion_due_date, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeAssessmentTypeLevelId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsVerificationRequired() . ', ' .
						$this->sqlIsTenStarRating() . ', ' .
						$this->sqlIs360Review() . ', ' .
						$this->sqlIsPeerReview() . ', ' .
						$this->sqlIsShowPeerComment() . ', ' .
						$this->sqlIsShowPeerQuestions() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlDepartmentId() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlEmployeeDueDate() . ', ' .
						$this->sqlManagerDueDate() . ', ' .
						$this->sqlCompletionDueDate() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_assessment_type_level_id = ' . $this->sqlEmployeeAssessmentTypeLevelId(). ',' ; } elseif( true == array_key_exists( 'EmployeeAssessmentTypeLevelId', $this->getChangedColumns() ) ) { $strSql .= ' employee_assessment_type_level_id = ' . $this->sqlEmployeeAssessmentTypeLevelId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_verification_required = ' . $this->sqlIsVerificationRequired(). ',' ; } elseif( true == array_key_exists( 'IsVerificationRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_verification_required = ' . $this->sqlIsVerificationRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ten_star_rating = ' . $this->sqlIsTenStarRating(). ',' ; } elseif( true == array_key_exists( 'IsTenStarRating', $this->getChangedColumns() ) ) { $strSql .= ' is_ten_star_rating = ' . $this->sqlIsTenStarRating() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_360_review = ' . $this->sqlIs360Review(). ',' ; } elseif( true == array_key_exists( 'Is360Review', $this->getChangedColumns() ) ) { $strSql .= ' is_360_review = ' . $this->sqlIs360Review() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_peer_review = ' . $this->sqlIsPeerReview(). ',' ; } elseif( true == array_key_exists( 'IsPeerReview', $this->getChangedColumns() ) ) { $strSql .= ' is_peer_review = ' . $this->sqlIsPeerReview() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_show_peer_comment = ' . $this->sqlIsShowPeerComment(). ',' ; } elseif( true == array_key_exists( 'IsShowPeerComment', $this->getChangedColumns() ) ) { $strSql .= ' is_show_peer_comment = ' . $this->sqlIsShowPeerComment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_show_peer_questions = ' . $this->sqlIsShowPeerQuestions(). ',' ; } elseif( true == array_key_exists( 'IsShowPeerQuestions', $this->getChangedColumns() ) ) { $strSql .= ' is_show_peer_questions = ' . $this->sqlIsShowPeerQuestions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId(). ',' ; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_due_date = ' . $this->sqlEmployeeDueDate(). ',' ; } elseif( true == array_key_exists( 'EmployeeDueDate', $this->getChangedColumns() ) ) { $strSql .= ' employee_due_date = ' . $this->sqlEmployeeDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manager_due_date = ' . $this->sqlManagerDueDate(). ',' ; } elseif( true == array_key_exists( 'ManagerDueDate', $this->getChangedColumns() ) ) { $strSql .= ' manager_due_date = ' . $this->sqlManagerDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completion_due_date = ' . $this->sqlCompletionDueDate(). ',' ; } elseif( true == array_key_exists( 'CompletionDueDate', $this->getChangedColumns() ) ) { $strSql .= ' completion_due_date = ' . $this->sqlCompletionDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_assessment_type_level_id' => $this->getEmployeeAssessmentTypeLevelId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_published' => $this->getIsPublished(),
			'is_verification_required' => $this->getIsVerificationRequired(),
			'is_ten_star_rating' => $this->getIsTenStarRating(),
			'is_360_review' => $this->getIs360Review(),
			'is_peer_review' => $this->getIsPeerReview(),
			'is_show_peer_comment' => $this->getIsShowPeerComment(),
			'is_show_peer_questions' => $this->getIsShowPeerQuestions(),
			'country_code' => $this->getCountryCode(),
			'department_id' => $this->getDepartmentId(),
			'order_num' => $this->getOrderNum(),
			'employee_due_date' => $this->getEmployeeDueDate(),
			'manager_due_date' => $this->getManagerDueDate(),
			'completion_due_date' => $this->getCompletionDueDate(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>