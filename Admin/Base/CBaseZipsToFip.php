<?php

class CBaseZipsToFip extends CEosSingularBase {

	const TABLE_NAME = 'public.zips_to_fips';

	protected $m_intId;
	protected $m_strZipCode;
	protected $m_strFipsCode;
	protected $m_strMsa;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['zip_code'] ) && $boolDirectSet ) $this->set( 'm_strZipCode', trim( stripcslashes( $arrValues['zip_code'] ) ) ); elseif( isset( $arrValues['zip_code'] ) ) $this->setZipCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['zip_code'] ) : $arrValues['zip_code'] );
		if( isset( $arrValues['fips_code'] ) && $boolDirectSet ) $this->set( 'm_strFipsCode', trim( stripcslashes( $arrValues['fips_code'] ) ) ); elseif( isset( $arrValues['fips_code'] ) ) $this->setFipsCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['fips_code'] ) : $arrValues['fips_code'] );
		if( isset( $arrValues['msa'] ) && $boolDirectSet ) $this->set( 'm_strMsa', trim( stripcslashes( $arrValues['msa'] ) ) ); elseif( isset( $arrValues['msa'] ) ) $this->setMsa( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['msa'] ) : $arrValues['msa'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setZipCode( $strZipCode ) {
		$this->set( 'm_strZipCode', CStrings::strTrimDef( $strZipCode, 10, NULL, true ) );
	}

	public function getZipCode() {
		return $this->m_strZipCode;
	}

	public function sqlZipCode() {
		return ( true == isset( $this->m_strZipCode ) ) ? '\'' . addslashes( $this->m_strZipCode ) . '\'' : 'NULL';
	}

	public function setFipsCode( $strFipsCode ) {
		$this->set( 'm_strFipsCode', CStrings::strTrimDef( $strFipsCode, 10, NULL, true ) );
	}

	public function getFipsCode() {
		return $this->m_strFipsCode;
	}

	public function sqlFipsCode() {
		return ( true == isset( $this->m_strFipsCode ) ) ? '\'' . addslashes( $this->m_strFipsCode ) . '\'' : 'NULL';
	}

	public function setMsa( $strMsa ) {
		$this->set( 'm_strMsa', CStrings::strTrimDef( $strMsa, 100, NULL, true ) );
	}

	public function getMsa() {
		return $this->m_strMsa;
	}

	public function sqlMsa() {
		return ( true == isset( $this->m_strMsa ) ) ? '\'' . addslashes( $this->m_strMsa ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, zip_code, fips_code, msa, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlZipCode() . ', ' .
 						$this->sqlFipsCode() . ', ' .
 						$this->sqlMsa() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' zip_code = ' . $this->sqlZipCode() . ','; } elseif( true == array_key_exists( 'ZipCode', $this->getChangedColumns() ) ) { $strSql .= ' zip_code = ' . $this->sqlZipCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fips_code = ' . $this->sqlFipsCode() . ','; } elseif( true == array_key_exists( 'FipsCode', $this->getChangedColumns() ) ) { $strSql .= ' fips_code = ' . $this->sqlFipsCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' msa = ' . $this->sqlMsa() . ','; } elseif( true == array_key_exists( 'Msa', $this->getChangedColumns() ) ) { $strSql .= ' msa = ' . $this->sqlMsa() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'zip_code' => $this->getZipCode(),
			'fips_code' => $this->getFipsCode(),
			'msa' => $this->getMsa(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>