<?php

class CBaseHonourBadge extends CEosSingularBase {

	const TABLE_NAME = 'public.honour_badges';

	protected $m_intId;
	protected $m_intHonourBadgeTypeId;
	protected $m_intHonourBadgeSemesterId;
	protected $m_intHonourBadgeReasonId;
	protected $m_intTargetEmployeeId;
	protected $m_intSourceEmployeeId;
	protected $m_strRemark;
	protected $m_strHonourBadgeDatetime;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intHonourBadgeSemesterId = '1';
		$this->m_strHonourBadgeDatetime = '06/07/2017 01:05:23.767892';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['honour_badge_type_id'] ) && $boolDirectSet ) $this->set( 'm_intHonourBadgeTypeId', trim( $arrValues['honour_badge_type_id'] ) ); elseif( isset( $arrValues['honour_badge_type_id'] ) ) $this->setHonourBadgeTypeId( $arrValues['honour_badge_type_id'] );
		if( isset( $arrValues['honour_badge_semester_id'] ) && $boolDirectSet ) $this->set( 'm_intHonourBadgeSemesterId', trim( $arrValues['honour_badge_semester_id'] ) ); elseif( isset( $arrValues['honour_badge_semester_id'] ) ) $this->setHonourBadgeSemesterId( $arrValues['honour_badge_semester_id'] );
		if( isset( $arrValues['honour_badge_reason_id'] ) && $boolDirectSet ) $this->set( 'm_intHonourBadgeReasonId', trim( $arrValues['honour_badge_reason_id'] ) ); elseif( isset( $arrValues['honour_badge_reason_id'] ) ) $this->setHonourBadgeReasonId( $arrValues['honour_badge_reason_id'] );
		if( isset( $arrValues['target_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intTargetEmployeeId', trim( $arrValues['target_employee_id'] ) ); elseif( isset( $arrValues['target_employee_id'] ) ) $this->setTargetEmployeeId( $arrValues['target_employee_id'] );
		if( isset( $arrValues['source_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSourceEmployeeId', trim( $arrValues['source_employee_id'] ) ); elseif( isset( $arrValues['source_employee_id'] ) ) $this->setSourceEmployeeId( $arrValues['source_employee_id'] );
		if( isset( $arrValues['remark'] ) && $boolDirectSet ) $this->set( 'm_strRemark', trim( stripcslashes( $arrValues['remark'] ) ) ); elseif( isset( $arrValues['remark'] ) ) $this->setRemark( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remark'] ) : $arrValues['remark'] );
		if( isset( $arrValues['honour_badge_datetime'] ) && $boolDirectSet ) $this->set( 'm_strHonourBadgeDatetime', trim( $arrValues['honour_badge_datetime'] ) ); elseif( isset( $arrValues['honour_badge_datetime'] ) ) $this->setHonourBadgeDatetime( $arrValues['honour_badge_datetime'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setHonourBadgeTypeId( $intHonourBadgeTypeId ) {
		$this->set( 'm_intHonourBadgeTypeId', CStrings::strToIntDef( $intHonourBadgeTypeId, NULL, false ) );
	}

	public function getHonourBadgeTypeId() {
		return $this->m_intHonourBadgeTypeId;
	}

	public function sqlHonourBadgeTypeId() {
		return ( true == isset( $this->m_intHonourBadgeTypeId ) ) ? ( string ) $this->m_intHonourBadgeTypeId : 'NULL';
	}

	public function setHonourBadgeSemesterId( $intHonourBadgeSemesterId ) {
		$this->set( 'm_intHonourBadgeSemesterId', CStrings::strToIntDef( $intHonourBadgeSemesterId, NULL, false ) );
	}

	public function getHonourBadgeSemesterId() {
		return $this->m_intHonourBadgeSemesterId;
	}

	public function sqlHonourBadgeSemesterId() {
		return ( true == isset( $this->m_intHonourBadgeSemesterId ) ) ? ( string ) $this->m_intHonourBadgeSemesterId : '1';
	}

	public function setHonourBadgeReasonId( $intHonourBadgeReasonId ) {
		$this->set( 'm_intHonourBadgeReasonId', CStrings::strToIntDef( $intHonourBadgeReasonId, NULL, false ) );
	}

	public function getHonourBadgeReasonId() {
		return $this->m_intHonourBadgeReasonId;
	}

	public function sqlHonourBadgeReasonId() {
		return ( true == isset( $this->m_intHonourBadgeReasonId ) ) ? ( string ) $this->m_intHonourBadgeReasonId : 'NULL';
	}

	public function setTargetEmployeeId( $intTargetEmployeeId ) {
		$this->set( 'm_intTargetEmployeeId', CStrings::strToIntDef( $intTargetEmployeeId, NULL, false ) );
	}

	public function getTargetEmployeeId() {
		return $this->m_intTargetEmployeeId;
	}

	public function sqlTargetEmployeeId() {
		return ( true == isset( $this->m_intTargetEmployeeId ) ) ? ( string ) $this->m_intTargetEmployeeId : 'NULL';
	}

	public function setSourceEmployeeId( $intSourceEmployeeId ) {
		$this->set( 'm_intSourceEmployeeId', CStrings::strToIntDef( $intSourceEmployeeId, NULL, false ) );
	}

	public function getSourceEmployeeId() {
		return $this->m_intSourceEmployeeId;
	}

	public function sqlSourceEmployeeId() {
		return ( true == isset( $this->m_intSourceEmployeeId ) ) ? ( string ) $this->m_intSourceEmployeeId : 'NULL';
	}

	public function setRemark( $strRemark ) {
		$this->set( 'm_strRemark', CStrings::strTrimDef( $strRemark, -1, NULL, true ) );
	}

	public function getRemark() {
		return $this->m_strRemark;
	}

	public function sqlRemark() {
		return ( true == isset( $this->m_strRemark ) ) ? '\'' . addslashes( $this->m_strRemark ) . '\'' : 'NULL';
	}

	public function setHonourBadgeDatetime( $strHonourBadgeDatetime ) {
		$this->set( 'm_strHonourBadgeDatetime', CStrings::strTrimDef( $strHonourBadgeDatetime, -1, NULL, true ) );
	}

	public function getHonourBadgeDatetime() {
		return $this->m_strHonourBadgeDatetime;
	}

	public function sqlHonourBadgeDatetime() {
		return ( true == isset( $this->m_strHonourBadgeDatetime ) ) ? '\'' . $this->m_strHonourBadgeDatetime . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, honour_badge_type_id, honour_badge_semester_id, honour_badge_reason_id, target_employee_id, source_employee_id, remark, honour_badge_datetime, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlHonourBadgeTypeId() . ', ' .
 						$this->sqlHonourBadgeSemesterId() . ', ' .
 						$this->sqlHonourBadgeReasonId() . ', ' .
 						$this->sqlTargetEmployeeId() . ', ' .
 						$this->sqlSourceEmployeeId() . ', ' .
 						$this->sqlRemark() . ', ' .
 						$this->sqlHonourBadgeDatetime() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' honour_badge_type_id = ' . $this->sqlHonourBadgeTypeId() . ','; } elseif( true == array_key_exists( 'HonourBadgeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' honour_badge_type_id = ' . $this->sqlHonourBadgeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' honour_badge_semester_id = ' . $this->sqlHonourBadgeSemesterId() . ','; } elseif( true == array_key_exists( 'HonourBadgeSemesterId', $this->getChangedColumns() ) ) { $strSql .= ' honour_badge_semester_id = ' . $this->sqlHonourBadgeSemesterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' honour_badge_reason_id = ' . $this->sqlHonourBadgeReasonId() . ','; } elseif( true == array_key_exists( 'HonourBadgeReasonId', $this->getChangedColumns() ) ) { $strSql .= ' honour_badge_reason_id = ' . $this->sqlHonourBadgeReasonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' target_employee_id = ' . $this->sqlTargetEmployeeId() . ','; } elseif( true == array_key_exists( 'TargetEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' target_employee_id = ' . $this->sqlTargetEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' source_employee_id = ' . $this->sqlSourceEmployeeId() . ','; } elseif( true == array_key_exists( 'SourceEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' source_employee_id = ' . $this->sqlSourceEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remark = ' . $this->sqlRemark() . ','; } elseif( true == array_key_exists( 'Remark', $this->getChangedColumns() ) ) { $strSql .= ' remark = ' . $this->sqlRemark() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' honour_badge_datetime = ' . $this->sqlHonourBadgeDatetime() . ','; } elseif( true == array_key_exists( 'HonourBadgeDatetime', $this->getChangedColumns() ) ) { $strSql .= ' honour_badge_datetime = ' . $this->sqlHonourBadgeDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'honour_badge_type_id' => $this->getHonourBadgeTypeId(),
			'honour_badge_semester_id' => $this->getHonourBadgeSemesterId(),
			'honour_badge_reason_id' => $this->getHonourBadgeReasonId(),
			'target_employee_id' => $this->getTargetEmployeeId(),
			'source_employee_id' => $this->getSourceEmployeeId(),
			'remark' => $this->getRemark(),
			'honour_badge_datetime' => $this->getHonourBadgeDatetime(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>