<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CJobPostingNotifications
 * Do not add any new functions to this class.
 */

class CBaseJobPostingNotifications extends CEosPluralBase {

	/**
	 * @return CJobPostingNotification[]
	 */
	public static function fetchJobPostingNotifications( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CJobPostingNotification', $objDatabase );
	}

	/**
	 * @return CJobPostingNotification
	 */
	public static function fetchJobPostingNotification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobPostingNotification', $objDatabase );
	}

	public static function fetchJobPostingNotificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_posting_notifications', $objDatabase );
	}

	public static function fetchJobPostingNotificationById( $intId, $objDatabase ) {
		return self::fetchJobPostingNotification( sprintf( 'SELECT * FROM job_posting_notifications WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchJobPostingNotificationsByJobPostingCategoryId( $intJobPostingCategoryId, $objDatabase ) {
		return self::fetchJobPostingNotifications( sprintf( 'SELECT * FROM job_posting_notifications WHERE job_posting_category_id = %d', ( int ) $intJobPostingCategoryId ), $objDatabase );
	}

}
?>