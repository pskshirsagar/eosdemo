<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserGroups
 * Do not add any new functions to this class.
 */

class CBaseUserGroups extends CEosPluralBase {

	/**
	 * @return CUserGroup[]
	 */
	public static function fetchUserGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserGroup', $objDatabase );
	}

	/**
	 * @return CUserGroup
	 */
	public static function fetchUserGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserGroup', $objDatabase );
	}

	public static function fetchUserGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_groups', $objDatabase );
	}

	public static function fetchUserGroupById( $intId, $objDatabase ) {
		return self::fetchUserGroup( sprintf( 'SELECT * FROM user_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserGroupsByUserId( $intUserId, $objDatabase ) {
		return self::fetchUserGroups( sprintf( 'SELECT * FROM user_groups WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

	public static function fetchUserGroupsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchUserGroups( sprintf( 'SELECT * FROM user_groups WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

}
?>