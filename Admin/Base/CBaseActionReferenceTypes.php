<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CActionReferenceTypes
 * Do not add any new functions to this class.
 */

class CBaseActionReferenceTypes extends CEosPluralBase {

	/**
	 * @return CActionReferenceType[]
	 */
	public static function fetchActionReferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CActionReferenceType', $objDatabase );
	}

	/**
	 * @return CActionReferenceType
	 */
	public static function fetchActionReferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CActionReferenceType', $objDatabase );
	}

	public static function fetchActionReferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'action_reference_types', $objDatabase );
	}

	public static function fetchActionReferenceTypeById( $intId, $objDatabase ) {
		return self::fetchActionReferenceType( sprintf( 'SELECT * FROM action_reference_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>