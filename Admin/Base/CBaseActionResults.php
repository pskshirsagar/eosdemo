<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CActionResults
 * Do not add any new functions to this class.
 */

class CBaseActionResults extends CEosPluralBase {

	/**
	 * @return CActionResult[]
	 */
	public static function fetchActionResults( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CActionResult', $objDatabase );
	}

	/**
	 * @return CActionResult
	 */
	public static function fetchActionResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CActionResult', $objDatabase );
	}

	public static function fetchActionResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'action_results', $objDatabase );
	}

	public static function fetchActionResultById( $intId, $objDatabase ) {
		return self::fetchActionResult( sprintf( 'SELECT * FROM action_results WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>