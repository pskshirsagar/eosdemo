<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStoryPointCommissions
 * Do not add any new functions to this class.
 */

class CBaseStoryPointCommissions extends CEosPluralBase {

	/**
	 * @return CStoryPointCommission[]
	 */
	public static function fetchStoryPointCommissions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStoryPointCommission', $objDatabase );
	}

	/**
	 * @return CStoryPointCommission
	 */
	public static function fetchStoryPointCommission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStoryPointCommission', $objDatabase );
	}

	public static function fetchStoryPointCommissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'story_point_commissions', $objDatabase );
	}

	public static function fetchStoryPointCommissionById( $intId, $objDatabase ) {
		return self::fetchStoryPointCommission( sprintf( 'SELECT * FROM story_point_commissions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStoryPointCommissionsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchStoryPointCommissions( sprintf( 'SELECT * FROM story_point_commissions WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchStoryPointCommissionsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		return self::fetchStoryPointCommissions( sprintf( 'SELECT * FROM story_point_commissions WHERE manager_employee_id = %d', ( int ) $intManagerEmployeeId ), $objDatabase );
	}

	public static function fetchStoryPointCommissionsByStoryPointBudgetId( $intStoryPointBudgetId, $objDatabase ) {
		return self::fetchStoryPointCommissions( sprintf( 'SELECT * FROM story_point_commissions WHERE story_point_budget_id = %d', ( int ) $intStoryPointBudgetId ), $objDatabase );
	}

	public static function fetchStoryPointCommissionsByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchStoryPointCommissions( sprintf( 'SELECT * FROM story_point_commissions WHERE designation_id = %d', ( int ) $intDesignationId ), $objDatabase );
	}

	public static function fetchStoryPointCommissionsByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchStoryPointCommissions( sprintf( 'SELECT * FROM story_point_commissions WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

}
?>