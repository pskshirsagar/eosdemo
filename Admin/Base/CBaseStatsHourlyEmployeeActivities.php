<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsHourlyEmployeeActivities
 * Do not add any new functions to this class.
 */

class CBaseStatsHourlyEmployeeActivities extends CEosPluralBase {

	/**
	 * @return CStatsHourlyEmployeeActivity[]
	 */
	public static function fetchStatsHourlyEmployeeActivities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStatsHourlyEmployeeActivity', $objDatabase );
	}

	/**
	 * @return CStatsHourlyEmployeeActivity
	 */
	public static function fetchStatsHourlyEmployeeActivity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStatsHourlyEmployeeActivity', $objDatabase );
	}

	public static function fetchStatsHourlyEmployeeActivityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stats_hourly_employee_activities', $objDatabase );
	}

	public static function fetchStatsHourlyEmployeeActivityById( $intId, $objDatabase ) {
		return self::fetchStatsHourlyEmployeeActivity( sprintf( 'SELECT * FROM stats_hourly_employee_activities WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStatsHourlyEmployeeActivitiesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchStatsHourlyEmployeeActivities( sprintf( 'SELECT * FROM stats_hourly_employee_activities WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>