<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsSvnCommits
 * Do not add any new functions to this class.
 */

class CBaseStatsSvnCommits extends CEosPluralBase {

	/**
	 * @return CStatsSvnCommit[]
	 */
	public static function fetchStatsSvnCommits( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStatsSvnCommit', $objDatabase );
	}

	/**
	 * @return CStatsSvnCommit
	 */
	public static function fetchStatsSvnCommit( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStatsSvnCommit', $objDatabase );
	}

	public static function fetchStatsSvnCommitCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stats_svn_commits', $objDatabase );
	}

	public static function fetchStatsSvnCommitById( $intId, $objDatabase ) {
		return self::fetchStatsSvnCommit( sprintf( 'SELECT * FROM stats_svn_commits WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStatsSvnCommitsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchStatsSvnCommits( sprintf( 'SELECT * FROM stats_svn_commits WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>