<?php

class CBaseDmPropertyBundleEntityUrl extends CEosSingularBase {

	const TABLE_NAME = 'public.dm_property_bundle_entity_urls';

	protected $m_intId;
	protected $m_intDmBundleEntityId;
	protected $m_intDmPropertyBundleId;
	protected $m_strUrl;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['dm_bundle_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intDmBundleEntityId', trim( $arrValues['dm_bundle_entity_id'] ) ); elseif( isset( $arrValues['dm_bundle_entity_id'] ) ) $this->setDmBundleEntityId( $arrValues['dm_bundle_entity_id'] );
		if( isset( $arrValues['dm_property_bundle_id'] ) && $boolDirectSet ) $this->set( 'm_intDmPropertyBundleId', trim( $arrValues['dm_property_bundle_id'] ) ); elseif( isset( $arrValues['dm_property_bundle_id'] ) ) $this->setDmPropertyBundleId( $arrValues['dm_property_bundle_id'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDmBundleEntityId( $intDmBundleEntityId ) {
		$this->set( 'm_intDmBundleEntityId', CStrings::strToIntDef( $intDmBundleEntityId, NULL, false ) );
	}

	public function getDmBundleEntityId() {
		return $this->m_intDmBundleEntityId;
	}

	public function sqlDmBundleEntityId() {
		return ( true == isset( $this->m_intDmBundleEntityId ) ) ? ( string ) $this->m_intDmBundleEntityId : 'NULL';
	}

	public function setDmPropertyBundleId( $intDmPropertyBundleId ) {
		$this->set( 'm_intDmPropertyBundleId', CStrings::strToIntDef( $intDmPropertyBundleId, NULL, false ) );
	}

	public function getDmPropertyBundleId() {
		return $this->m_intDmPropertyBundleId;
	}

	public function sqlDmPropertyBundleId() {
		return ( true == isset( $this->m_intDmPropertyBundleId ) ) ? ( string ) $this->m_intDmPropertyBundleId : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 240, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, dm_bundle_entity_id, dm_property_bundle_id, url, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDmBundleEntityId() . ', ' .
						$this->sqlDmPropertyBundleId() . ', ' .
						$this->sqlUrl() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dm_bundle_entity_id = ' . $this->sqlDmBundleEntityId(). ',' ; } elseif( true == array_key_exists( 'DmBundleEntityId', $this->getChangedColumns() ) ) { $strSql .= ' dm_bundle_entity_id = ' . $this->sqlDmBundleEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dm_property_bundle_id = ' . $this->sqlDmPropertyBundleId(). ',' ; } elseif( true == array_key_exists( 'DmPropertyBundleId', $this->getChangedColumns() ) ) { $strSql .= ' dm_property_bundle_id = ' . $this->sqlDmPropertyBundleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl(). ',' ; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'dm_bundle_entity_id' => $this->getDmBundleEntityId(),
			'dm_property_bundle_id' => $this->getDmPropertyBundleId(),
			'url' => $this->getUrl(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>