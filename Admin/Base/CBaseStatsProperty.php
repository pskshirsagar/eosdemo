<?php

class CBaseStatsProperty extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_properties';

	protected $m_intId;
	protected $m_intPropertyId;
	protected $m_strMonth;
	protected $m_intActiveLeases;
	protected $m_intActiveUsers;
	protected $m_intPaymentAdoption;
	protected $m_intOnlinePayees;
	protected $m_intOnlineLeads;
	protected $m_intStartedOnlineLeads;
	protected $m_intCompletedOnlineLeads;
	protected $m_intCraigslistPosts;
	protected $m_intCraigslistUniqueVisitors;
	protected $m_intCalls;
	protected $m_intVoicemails;
	protected $m_intMissedCalls;
	protected $m_intChats;
	protected $m_intMissedChats;
	protected $m_intResidentEmailsSent;
	protected $m_intResidentOpenedEmails;
	protected $m_intProspectEmailsSent;
	protected $m_intProspectOpenedEmails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intActiveLeases = '0';
		$this->m_intActiveUsers = '0';
		$this->m_intPaymentAdoption = '0';
		$this->m_intOnlinePayees = '0';
		$this->m_intOnlineLeads = '0';
		$this->m_intStartedOnlineLeads = '0';
		$this->m_intCompletedOnlineLeads = '0';
		$this->m_intCraigslistPosts = '0';
		$this->m_intCraigslistUniqueVisitors = '0';
		$this->m_intCalls = '0';
		$this->m_intVoicemails = '0';
		$this->m_intMissedCalls = '0';
		$this->m_intChats = '0';
		$this->m_intMissedChats = '0';
		$this->m_intResidentEmailsSent = '0';
		$this->m_intResidentOpenedEmails = '0';
		$this->m_intProspectEmailsSent = '0';
		$this->m_intProspectOpenedEmails = '0';
		$this->m_intCreatedBy = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['active_leases'] ) && $boolDirectSet ) $this->set( 'm_intActiveLeases', trim( $arrValues['active_leases'] ) ); elseif( isset( $arrValues['active_leases'] ) ) $this->setActiveLeases( $arrValues['active_leases'] );
		if( isset( $arrValues['active_users'] ) && $boolDirectSet ) $this->set( 'm_intActiveUsers', trim( $arrValues['active_users'] ) ); elseif( isset( $arrValues['active_users'] ) ) $this->setActiveUsers( $arrValues['active_users'] );
		if( isset( $arrValues['payment_adoption'] ) && $boolDirectSet ) $this->set( 'm_intPaymentAdoption', trim( $arrValues['payment_adoption'] ) ); elseif( isset( $arrValues['payment_adoption'] ) ) $this->setPaymentAdoption( $arrValues['payment_adoption'] );
		if( isset( $arrValues['online_payees'] ) && $boolDirectSet ) $this->set( 'm_intOnlinePayees', trim( $arrValues['online_payees'] ) ); elseif( isset( $arrValues['online_payees'] ) ) $this->setOnlinePayees( $arrValues['online_payees'] );
		if( isset( $arrValues['online_leads'] ) && $boolDirectSet ) $this->set( 'm_intOnlineLeads', trim( $arrValues['online_leads'] ) ); elseif( isset( $arrValues['online_leads'] ) ) $this->setOnlineLeads( $arrValues['online_leads'] );
		if( isset( $arrValues['started_online_leads'] ) && $boolDirectSet ) $this->set( 'm_intStartedOnlineLeads', trim( $arrValues['started_online_leads'] ) ); elseif( isset( $arrValues['started_online_leads'] ) ) $this->setStartedOnlineLeads( $arrValues['started_online_leads'] );
		if( isset( $arrValues['completed_online_leads'] ) && $boolDirectSet ) $this->set( 'm_intCompletedOnlineLeads', trim( $arrValues['completed_online_leads'] ) ); elseif( isset( $arrValues['completed_online_leads'] ) ) $this->setCompletedOnlineLeads( $arrValues['completed_online_leads'] );
		if( isset( $arrValues['craigslist_posts'] ) && $boolDirectSet ) $this->set( 'm_intCraigslistPosts', trim( $arrValues['craigslist_posts'] ) ); elseif( isset( $arrValues['craigslist_posts'] ) ) $this->setCraigslistPosts( $arrValues['craigslist_posts'] );
		if( isset( $arrValues['craigslist_unique_visitors'] ) && $boolDirectSet ) $this->set( 'm_intCraigslistUniqueVisitors', trim( $arrValues['craigslist_unique_visitors'] ) ); elseif( isset( $arrValues['craigslist_unique_visitors'] ) ) $this->setCraigslistUniqueVisitors( $arrValues['craigslist_unique_visitors'] );
		if( isset( $arrValues['calls'] ) && $boolDirectSet ) $this->set( 'm_intCalls', trim( $arrValues['calls'] ) ); elseif( isset( $arrValues['calls'] ) ) $this->setCalls( $arrValues['calls'] );
		if( isset( $arrValues['voicemails'] ) && $boolDirectSet ) $this->set( 'm_intVoicemails', trim( $arrValues['voicemails'] ) ); elseif( isset( $arrValues['voicemails'] ) ) $this->setVoicemails( $arrValues['voicemails'] );
		if( isset( $arrValues['missed_calls'] ) && $boolDirectSet ) $this->set( 'm_intMissedCalls', trim( $arrValues['missed_calls'] ) ); elseif( isset( $arrValues['missed_calls'] ) ) $this->setMissedCalls( $arrValues['missed_calls'] );
		if( isset( $arrValues['chats'] ) && $boolDirectSet ) $this->set( 'm_intChats', trim( $arrValues['chats'] ) ); elseif( isset( $arrValues['chats'] ) ) $this->setChats( $arrValues['chats'] );
		if( isset( $arrValues['missed_chats'] ) && $boolDirectSet ) $this->set( 'm_intMissedChats', trim( $arrValues['missed_chats'] ) ); elseif( isset( $arrValues['missed_chats'] ) ) $this->setMissedChats( $arrValues['missed_chats'] );
		if( isset( $arrValues['resident_emails_sent'] ) && $boolDirectSet ) $this->set( 'm_intResidentEmailsSent', trim( $arrValues['resident_emails_sent'] ) ); elseif( isset( $arrValues['resident_emails_sent'] ) ) $this->setResidentEmailsSent( $arrValues['resident_emails_sent'] );
		if( isset( $arrValues['resident_opened_emails'] ) && $boolDirectSet ) $this->set( 'm_intResidentOpenedEmails', trim( $arrValues['resident_opened_emails'] ) ); elseif( isset( $arrValues['resident_opened_emails'] ) ) $this->setResidentOpenedEmails( $arrValues['resident_opened_emails'] );
		if( isset( $arrValues['prospect_emails_sent'] ) && $boolDirectSet ) $this->set( 'm_intProspectEmailsSent', trim( $arrValues['prospect_emails_sent'] ) ); elseif( isset( $arrValues['prospect_emails_sent'] ) ) $this->setProspectEmailsSent( $arrValues['prospect_emails_sent'] );
		if( isset( $arrValues['prospect_opened_emails'] ) && $boolDirectSet ) $this->set( 'm_intProspectOpenedEmails', trim( $arrValues['prospect_opened_emails'] ) ); elseif( isset( $arrValues['prospect_opened_emails'] ) ) $this->setProspectOpenedEmails( $arrValues['prospect_opened_emails'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NOW()';
	}

	public function setActiveLeases( $intActiveLeases ) {
		$this->set( 'm_intActiveLeases', CStrings::strToIntDef( $intActiveLeases, NULL, false ) );
	}

	public function getActiveLeases() {
		return $this->m_intActiveLeases;
	}

	public function sqlActiveLeases() {
		return ( true == isset( $this->m_intActiveLeases ) ) ? ( string ) $this->m_intActiveLeases : '0';
	}

	public function setActiveUsers( $intActiveUsers ) {
		$this->set( 'm_intActiveUsers', CStrings::strToIntDef( $intActiveUsers, NULL, false ) );
	}

	public function getActiveUsers() {
		return $this->m_intActiveUsers;
	}

	public function sqlActiveUsers() {
		return ( true == isset( $this->m_intActiveUsers ) ) ? ( string ) $this->m_intActiveUsers : '0';
	}

	public function setPaymentAdoption( $intPaymentAdoption ) {
		$this->set( 'm_intPaymentAdoption', CStrings::strToIntDef( $intPaymentAdoption, NULL, false ) );
	}

	public function getPaymentAdoption() {
		return $this->m_intPaymentAdoption;
	}

	public function sqlPaymentAdoption() {
		return ( true == isset( $this->m_intPaymentAdoption ) ) ? ( string ) $this->m_intPaymentAdoption : '0';
	}

	public function setOnlinePayees( $intOnlinePayees ) {
		$this->set( 'm_intOnlinePayees', CStrings::strToIntDef( $intOnlinePayees, NULL, false ) );
	}

	public function getOnlinePayees() {
		return $this->m_intOnlinePayees;
	}

	public function sqlOnlinePayees() {
		return ( true == isset( $this->m_intOnlinePayees ) ) ? ( string ) $this->m_intOnlinePayees : '0';
	}

	public function setOnlineLeads( $intOnlineLeads ) {
		$this->set( 'm_intOnlineLeads', CStrings::strToIntDef( $intOnlineLeads, NULL, false ) );
	}

	public function getOnlineLeads() {
		return $this->m_intOnlineLeads;
	}

	public function sqlOnlineLeads() {
		return ( true == isset( $this->m_intOnlineLeads ) ) ? ( string ) $this->m_intOnlineLeads : '0';
	}

	public function setStartedOnlineLeads( $intStartedOnlineLeads ) {
		$this->set( 'm_intStartedOnlineLeads', CStrings::strToIntDef( $intStartedOnlineLeads, NULL, false ) );
	}

	public function getStartedOnlineLeads() {
		return $this->m_intStartedOnlineLeads;
	}

	public function sqlStartedOnlineLeads() {
		return ( true == isset( $this->m_intStartedOnlineLeads ) ) ? ( string ) $this->m_intStartedOnlineLeads : '0';
	}

	public function setCompletedOnlineLeads( $intCompletedOnlineLeads ) {
		$this->set( 'm_intCompletedOnlineLeads', CStrings::strToIntDef( $intCompletedOnlineLeads, NULL, false ) );
	}

	public function getCompletedOnlineLeads() {
		return $this->m_intCompletedOnlineLeads;
	}

	public function sqlCompletedOnlineLeads() {
		return ( true == isset( $this->m_intCompletedOnlineLeads ) ) ? ( string ) $this->m_intCompletedOnlineLeads : '0';
	}

	public function setCraigslistPosts( $intCraigslistPosts ) {
		$this->set( 'm_intCraigslistPosts', CStrings::strToIntDef( $intCraigslistPosts, NULL, false ) );
	}

	public function getCraigslistPosts() {
		return $this->m_intCraigslistPosts;
	}

	public function sqlCraigslistPosts() {
		return ( true == isset( $this->m_intCraigslistPosts ) ) ? ( string ) $this->m_intCraigslistPosts : '0';
	}

	public function setCraigslistUniqueVisitors( $intCraigslistUniqueVisitors ) {
		$this->set( 'm_intCraigslistUniqueVisitors', CStrings::strToIntDef( $intCraigslistUniqueVisitors, NULL, false ) );
	}

	public function getCraigslistUniqueVisitors() {
		return $this->m_intCraigslistUniqueVisitors;
	}

	public function sqlCraigslistUniqueVisitors() {
		return ( true == isset( $this->m_intCraigslistUniqueVisitors ) ) ? ( string ) $this->m_intCraigslistUniqueVisitors : '0';
	}

	public function setCalls( $intCalls ) {
		$this->set( 'm_intCalls', CStrings::strToIntDef( $intCalls, NULL, false ) );
	}

	public function getCalls() {
		return $this->m_intCalls;
	}

	public function sqlCalls() {
		return ( true == isset( $this->m_intCalls ) ) ? ( string ) $this->m_intCalls : '0';
	}

	public function setVoicemails( $intVoicemails ) {
		$this->set( 'm_intVoicemails', CStrings::strToIntDef( $intVoicemails, NULL, false ) );
	}

	public function getVoicemails() {
		return $this->m_intVoicemails;
	}

	public function sqlVoicemails() {
		return ( true == isset( $this->m_intVoicemails ) ) ? ( string ) $this->m_intVoicemails : '0';
	}

	public function setMissedCalls( $intMissedCalls ) {
		$this->set( 'm_intMissedCalls', CStrings::strToIntDef( $intMissedCalls, NULL, false ) );
	}

	public function getMissedCalls() {
		return $this->m_intMissedCalls;
	}

	public function sqlMissedCalls() {
		return ( true == isset( $this->m_intMissedCalls ) ) ? ( string ) $this->m_intMissedCalls : '0';
	}

	public function setChats( $intChats ) {
		$this->set( 'm_intChats', CStrings::strToIntDef( $intChats, NULL, false ) );
	}

	public function getChats() {
		return $this->m_intChats;
	}

	public function sqlChats() {
		return ( true == isset( $this->m_intChats ) ) ? ( string ) $this->m_intChats : '0';
	}

	public function setMissedChats( $intMissedChats ) {
		$this->set( 'm_intMissedChats', CStrings::strToIntDef( $intMissedChats, NULL, false ) );
	}

	public function getMissedChats() {
		return $this->m_intMissedChats;
	}

	public function sqlMissedChats() {
		return ( true == isset( $this->m_intMissedChats ) ) ? ( string ) $this->m_intMissedChats : '0';
	}

	public function setResidentEmailsSent( $intResidentEmailsSent ) {
		$this->set( 'm_intResidentEmailsSent', CStrings::strToIntDef( $intResidentEmailsSent, NULL, false ) );
	}

	public function getResidentEmailsSent() {
		return $this->m_intResidentEmailsSent;
	}

	public function sqlResidentEmailsSent() {
		return ( true == isset( $this->m_intResidentEmailsSent ) ) ? ( string ) $this->m_intResidentEmailsSent : '0';
	}

	public function setResidentOpenedEmails( $intResidentOpenedEmails ) {
		$this->set( 'm_intResidentOpenedEmails', CStrings::strToIntDef( $intResidentOpenedEmails, NULL, false ) );
	}

	public function getResidentOpenedEmails() {
		return $this->m_intResidentOpenedEmails;
	}

	public function sqlResidentOpenedEmails() {
		return ( true == isset( $this->m_intResidentOpenedEmails ) ) ? ( string ) $this->m_intResidentOpenedEmails : '0';
	}

	public function setProspectEmailsSent( $intProspectEmailsSent ) {
		$this->set( 'm_intProspectEmailsSent', CStrings::strToIntDef( $intProspectEmailsSent, NULL, false ) );
	}

	public function getProspectEmailsSent() {
		return $this->m_intProspectEmailsSent;
	}

	public function sqlProspectEmailsSent() {
		return ( true == isset( $this->m_intProspectEmailsSent ) ) ? ( string ) $this->m_intProspectEmailsSent : '0';
	}

	public function setProspectOpenedEmails( $intProspectOpenedEmails ) {
		$this->set( 'm_intProspectOpenedEmails', CStrings::strToIntDef( $intProspectOpenedEmails, NULL, false ) );
	}

	public function getProspectOpenedEmails() {
		return $this->m_intProspectOpenedEmails;
	}

	public function sqlProspectOpenedEmails() {
		return ( true == isset( $this->m_intProspectOpenedEmails ) ) ? ( string ) $this->m_intProspectOpenedEmails : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '0';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, property_id, month, active_leases, active_users, payment_adoption, online_payees, online_leads, started_online_leads, completed_online_leads, craigslist_posts, craigslist_unique_visitors, calls, voicemails, missed_calls, chats, missed_chats, resident_emails_sent, resident_opened_emails, prospect_emails_sent, prospect_opened_emails, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlMonth() . ', ' .
 						$this->sqlActiveLeases() . ', ' .
 						$this->sqlActiveUsers() . ', ' .
 						$this->sqlPaymentAdoption() . ', ' .
 						$this->sqlOnlinePayees() . ', ' .
 						$this->sqlOnlineLeads() . ', ' .
 						$this->sqlStartedOnlineLeads() . ', ' .
 						$this->sqlCompletedOnlineLeads() . ', ' .
 						$this->sqlCraigslistPosts() . ', ' .
 						$this->sqlCraigslistUniqueVisitors() . ', ' .
 						$this->sqlCalls() . ', ' .
 						$this->sqlVoicemails() . ', ' .
 						$this->sqlMissedCalls() . ', ' .
 						$this->sqlChats() . ', ' .
 						$this->sqlMissedChats() . ', ' .
 						$this->sqlResidentEmailsSent() . ', ' .
 						$this->sqlResidentOpenedEmails() . ', ' .
 						$this->sqlProspectEmailsSent() . ', ' .
 						$this->sqlProspectOpenedEmails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' active_leases = ' . $this->sqlActiveLeases() . ','; } elseif( true == array_key_exists( 'ActiveLeases', $this->getChangedColumns() ) ) { $strSql .= ' active_leases = ' . $this->sqlActiveLeases() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' active_users = ' . $this->sqlActiveUsers() . ','; } elseif( true == array_key_exists( 'ActiveUsers', $this->getChangedColumns() ) ) { $strSql .= ' active_users = ' . $this->sqlActiveUsers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_adoption = ' . $this->sqlPaymentAdoption() . ','; } elseif( true == array_key_exists( 'PaymentAdoption', $this->getChangedColumns() ) ) { $strSql .= ' payment_adoption = ' . $this->sqlPaymentAdoption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' online_payees = ' . $this->sqlOnlinePayees() . ','; } elseif( true == array_key_exists( 'OnlinePayees', $this->getChangedColumns() ) ) { $strSql .= ' online_payees = ' . $this->sqlOnlinePayees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' online_leads = ' . $this->sqlOnlineLeads() . ','; } elseif( true == array_key_exists( 'OnlineLeads', $this->getChangedColumns() ) ) { $strSql .= ' online_leads = ' . $this->sqlOnlineLeads() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' started_online_leads = ' . $this->sqlStartedOnlineLeads() . ','; } elseif( true == array_key_exists( 'StartedOnlineLeads', $this->getChangedColumns() ) ) { $strSql .= ' started_online_leads = ' . $this->sqlStartedOnlineLeads() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_online_leads = ' . $this->sqlCompletedOnlineLeads() . ','; } elseif( true == array_key_exists( 'CompletedOnlineLeads', $this->getChangedColumns() ) ) { $strSql .= ' completed_online_leads = ' . $this->sqlCompletedOnlineLeads() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' craigslist_posts = ' . $this->sqlCraigslistPosts() . ','; } elseif( true == array_key_exists( 'CraigslistPosts', $this->getChangedColumns() ) ) { $strSql .= ' craigslist_posts = ' . $this->sqlCraigslistPosts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' craigslist_unique_visitors = ' . $this->sqlCraigslistUniqueVisitors() . ','; } elseif( true == array_key_exists( 'CraigslistUniqueVisitors', $this->getChangedColumns() ) ) { $strSql .= ' craigslist_unique_visitors = ' . $this->sqlCraigslistUniqueVisitors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calls = ' . $this->sqlCalls() . ','; } elseif( true == array_key_exists( 'Calls', $this->getChangedColumns() ) ) { $strSql .= ' calls = ' . $this->sqlCalls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' voicemails = ' . $this->sqlVoicemails() . ','; } elseif( true == array_key_exists( 'Voicemails', $this->getChangedColumns() ) ) { $strSql .= ' voicemails = ' . $this->sqlVoicemails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' missed_calls = ' . $this->sqlMissedCalls() . ','; } elseif( true == array_key_exists( 'MissedCalls', $this->getChangedColumns() ) ) { $strSql .= ' missed_calls = ' . $this->sqlMissedCalls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chats = ' . $this->sqlChats() . ','; } elseif( true == array_key_exists( 'Chats', $this->getChangedColumns() ) ) { $strSql .= ' chats = ' . $this->sqlChats() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' missed_chats = ' . $this->sqlMissedChats() . ','; } elseif( true == array_key_exists( 'MissedChats', $this->getChangedColumns() ) ) { $strSql .= ' missed_chats = ' . $this->sqlMissedChats() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_emails_sent = ' . $this->sqlResidentEmailsSent() . ','; } elseif( true == array_key_exists( 'ResidentEmailsSent', $this->getChangedColumns() ) ) { $strSql .= ' resident_emails_sent = ' . $this->sqlResidentEmailsSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resident_opened_emails = ' . $this->sqlResidentOpenedEmails() . ','; } elseif( true == array_key_exists( 'ResidentOpenedEmails', $this->getChangedColumns() ) ) { $strSql .= ' resident_opened_emails = ' . $this->sqlResidentOpenedEmails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prospect_emails_sent = ' . $this->sqlProspectEmailsSent() . ','; } elseif( true == array_key_exists( 'ProspectEmailsSent', $this->getChangedColumns() ) ) { $strSql .= ' prospect_emails_sent = ' . $this->sqlProspectEmailsSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prospect_opened_emails = ' . $this->sqlProspectOpenedEmails() . ','; } elseif( true == array_key_exists( 'ProspectOpenedEmails', $this->getChangedColumns() ) ) { $strSql .= ' prospect_opened_emails = ' . $this->sqlProspectOpenedEmails() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'property_id' => $this->getPropertyId(),
			'month' => $this->getMonth(),
			'active_leases' => $this->getActiveLeases(),
			'active_users' => $this->getActiveUsers(),
			'payment_adoption' => $this->getPaymentAdoption(),
			'online_payees' => $this->getOnlinePayees(),
			'online_leads' => $this->getOnlineLeads(),
			'started_online_leads' => $this->getStartedOnlineLeads(),
			'completed_online_leads' => $this->getCompletedOnlineLeads(),
			'craigslist_posts' => $this->getCraigslistPosts(),
			'craigslist_unique_visitors' => $this->getCraigslistUniqueVisitors(),
			'calls' => $this->getCalls(),
			'voicemails' => $this->getVoicemails(),
			'missed_calls' => $this->getMissedCalls(),
			'chats' => $this->getChats(),
			'missed_chats' => $this->getMissedChats(),
			'resident_emails_sent' => $this->getResidentEmailsSent(),
			'resident_opened_emails' => $this->getResidentOpenedEmails(),
			'prospect_emails_sent' => $this->getProspectEmailsSent(),
			'prospect_opened_emails' => $this->getProspectOpenedEmails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>