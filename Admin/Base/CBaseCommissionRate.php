<?php

class CBaseCommissionRate extends CEosSingularBase {

	const TABLE_NAME = 'public.commission_rates';

	protected $m_intId;
	protected $m_intCommissionStructureId;
	protected $m_intPsProductId;
	protected $m_intCommissionBucketId;
	protected $m_intChargeCodeId;
	protected $m_intCommissionTierTypeId;
	protected $m_fltCommissionAmount;
	protected $m_fltCommissionPercent;
	protected $m_intFrontLoadPosts;
	protected $m_fltFrontLoadAmount;
	protected $m_fltFrontLoadPercentage;
	protected $m_intOngoingFrontLoadPosts;
	protected $m_fltOngoingFrontLoadAmount;
	protected $m_fltOngoingFrontLoadPercent;
	protected $m_fltSetupCommissionPercent;
	protected $m_intIsBookingBased;
	protected $m_fltUnitCommissionAmount;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intFrontLoadPosts = '0';
		$this->m_fltFrontLoadAmount = '0';
		$this->m_fltFrontLoadPercentage = '0';
		$this->m_intOngoingFrontLoadPosts = '0';
		$this->m_fltOngoingFrontLoadAmount = '0';
		$this->m_fltOngoingFrontLoadPercent = '0';
		$this->m_fltSetupCommissionPercent = '0';
		$this->m_intIsBookingBased = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['commission_structure_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionStructureId', trim( $arrValues['commission_structure_id'] ) ); elseif( isset( $arrValues['commission_structure_id'] ) ) $this->setCommissionStructureId( $arrValues['commission_structure_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['commission_bucket_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionBucketId', trim( $arrValues['commission_bucket_id'] ) ); elseif( isset( $arrValues['commission_bucket_id'] ) ) $this->setCommissionBucketId( $arrValues['commission_bucket_id'] );
		if( isset( $arrValues['charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeCodeId', trim( $arrValues['charge_code_id'] ) ); elseif( isset( $arrValues['charge_code_id'] ) ) $this->setChargeCodeId( $arrValues['charge_code_id'] );
		if( isset( $arrValues['commission_tier_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionTierTypeId', trim( $arrValues['commission_tier_type_id'] ) ); elseif( isset( $arrValues['commission_tier_type_id'] ) ) $this->setCommissionTierTypeId( $arrValues['commission_tier_type_id'] );
		if( isset( $arrValues['commission_amount'] ) && $boolDirectSet ) $this->set( 'm_fltCommissionAmount', trim( $arrValues['commission_amount'] ) ); elseif( isset( $arrValues['commission_amount'] ) ) $this->setCommissionAmount( $arrValues['commission_amount'] );
		if( isset( $arrValues['commission_percent'] ) && $boolDirectSet ) $this->set( 'm_fltCommissionPercent', trim( $arrValues['commission_percent'] ) ); elseif( isset( $arrValues['commission_percent'] ) ) $this->setCommissionPercent( $arrValues['commission_percent'] );
		if( isset( $arrValues['front_load_posts'] ) && $boolDirectSet ) $this->set( 'm_intFrontLoadPosts', trim( $arrValues['front_load_posts'] ) ); elseif( isset( $arrValues['front_load_posts'] ) ) $this->setFrontLoadPosts( $arrValues['front_load_posts'] );
		if( isset( $arrValues['front_load_amount'] ) && $boolDirectSet ) $this->set( 'm_fltFrontLoadAmount', trim( $arrValues['front_load_amount'] ) ); elseif( isset( $arrValues['front_load_amount'] ) ) $this->setFrontLoadAmount( $arrValues['front_load_amount'] );
		if( isset( $arrValues['front_load_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltFrontLoadPercentage', trim( $arrValues['front_load_percentage'] ) ); elseif( isset( $arrValues['front_load_percentage'] ) ) $this->setFrontLoadPercentage( $arrValues['front_load_percentage'] );
		if( isset( $arrValues['ongoing_front_load_posts'] ) && $boolDirectSet ) $this->set( 'm_intOngoingFrontLoadPosts', trim( $arrValues['ongoing_front_load_posts'] ) ); elseif( isset( $arrValues['ongoing_front_load_posts'] ) ) $this->setOngoingFrontLoadPosts( $arrValues['ongoing_front_load_posts'] );
		if( isset( $arrValues['ongoing_front_load_amount'] ) && $boolDirectSet ) $this->set( 'm_fltOngoingFrontLoadAmount', trim( $arrValues['ongoing_front_load_amount'] ) ); elseif( isset( $arrValues['ongoing_front_load_amount'] ) ) $this->setOngoingFrontLoadAmount( $arrValues['ongoing_front_load_amount'] );
		if( isset( $arrValues['ongoing_front_load_percent'] ) && $boolDirectSet ) $this->set( 'm_fltOngoingFrontLoadPercent', trim( $arrValues['ongoing_front_load_percent'] ) ); elseif( isset( $arrValues['ongoing_front_load_percent'] ) ) $this->setOngoingFrontLoadPercent( $arrValues['ongoing_front_load_percent'] );
		if( isset( $arrValues['setup_commission_percent'] ) && $boolDirectSet ) $this->set( 'm_fltSetupCommissionPercent', trim( $arrValues['setup_commission_percent'] ) ); elseif( isset( $arrValues['setup_commission_percent'] ) ) $this->setSetupCommissionPercent( $arrValues['setup_commission_percent'] );
		if( isset( $arrValues['is_booking_based'] ) && $boolDirectSet ) $this->set( 'm_intIsBookingBased', trim( $arrValues['is_booking_based'] ) ); elseif( isset( $arrValues['is_booking_based'] ) ) $this->setIsBookingBased( $arrValues['is_booking_based'] );
		if( isset( $arrValues['unit_commission_amount'] ) && $boolDirectSet ) $this->set( 'm_fltUnitCommissionAmount', trim( $arrValues['unit_commission_amount'] ) ); elseif( isset( $arrValues['unit_commission_amount'] ) ) $this->setUnitCommissionAmount( $arrValues['unit_commission_amount'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCommissionStructureId( $intCommissionStructureId ) {
		$this->set( 'm_intCommissionStructureId', CStrings::strToIntDef( $intCommissionStructureId, NULL, false ) );
	}

	public function getCommissionStructureId() {
		return $this->m_intCommissionStructureId;
	}

	public function sqlCommissionStructureId() {
		return ( true == isset( $this->m_intCommissionStructureId ) ) ? ( string ) $this->m_intCommissionStructureId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setCommissionBucketId( $intCommissionBucketId ) {
		$this->set( 'm_intCommissionBucketId', CStrings::strToIntDef( $intCommissionBucketId, NULL, false ) );
	}

	public function getCommissionBucketId() {
		return $this->m_intCommissionBucketId;
	}

	public function sqlCommissionBucketId() {
		return ( true == isset( $this->m_intCommissionBucketId ) ) ? ( string ) $this->m_intCommissionBucketId : 'NULL';
	}

	public function setChargeCodeId( $intChargeCodeId ) {
		$this->set( 'm_intChargeCodeId', CStrings::strToIntDef( $intChargeCodeId, NULL, false ) );
	}

	public function getChargeCodeId() {
		return $this->m_intChargeCodeId;
	}

	public function sqlChargeCodeId() {
		return ( true == isset( $this->m_intChargeCodeId ) ) ? ( string ) $this->m_intChargeCodeId : 'NULL';
	}

	public function setCommissionTierTypeId( $intCommissionTierTypeId ) {
		$this->set( 'm_intCommissionTierTypeId', CStrings::strToIntDef( $intCommissionTierTypeId, NULL, false ) );
	}

	public function getCommissionTierTypeId() {
		return $this->m_intCommissionTierTypeId;
	}

	public function sqlCommissionTierTypeId() {
		return ( true == isset( $this->m_intCommissionTierTypeId ) ) ? ( string ) $this->m_intCommissionTierTypeId : 'NULL';
	}

	public function setCommissionAmount( $fltCommissionAmount ) {
		$this->set( 'm_fltCommissionAmount', CStrings::strToFloatDef( $fltCommissionAmount, NULL, false, 4 ) );
	}

	public function getCommissionAmount() {
		return $this->m_fltCommissionAmount;
	}

	public function sqlCommissionAmount() {
		return ( true == isset( $this->m_fltCommissionAmount ) ) ? ( string ) $this->m_fltCommissionAmount : 'NULL';
	}

	public function setCommissionPercent( $fltCommissionPercent ) {
		$this->set( 'm_fltCommissionPercent', CStrings::strToFloatDef( $fltCommissionPercent, NULL, false, 6 ) );
	}

	public function getCommissionPercent() {
		return $this->m_fltCommissionPercent;
	}

	public function sqlCommissionPercent() {
		return ( true == isset( $this->m_fltCommissionPercent ) ) ? ( string ) $this->m_fltCommissionPercent : 'NULL';
	}

	public function setFrontLoadPosts( $intFrontLoadPosts ) {
		$this->set( 'm_intFrontLoadPosts', CStrings::strToIntDef( $intFrontLoadPosts, NULL, false ) );
	}

	public function getFrontLoadPosts() {
		return $this->m_intFrontLoadPosts;
	}

	public function sqlFrontLoadPosts() {
		return ( true == isset( $this->m_intFrontLoadPosts ) ) ? ( string ) $this->m_intFrontLoadPosts : '0';
	}

	public function setFrontLoadAmount( $fltFrontLoadAmount ) {
		$this->set( 'm_fltFrontLoadAmount', CStrings::strToFloatDef( $fltFrontLoadAmount, NULL, false, 4 ) );
	}

	public function getFrontLoadAmount() {
		return $this->m_fltFrontLoadAmount;
	}

	public function sqlFrontLoadAmount() {
		return ( true == isset( $this->m_fltFrontLoadAmount ) ) ? ( string ) $this->m_fltFrontLoadAmount : '0';
	}

	public function setFrontLoadPercentage( $fltFrontLoadPercentage ) {
		$this->set( 'm_fltFrontLoadPercentage', CStrings::strToFloatDef( $fltFrontLoadPercentage, NULL, false, 4 ) );
	}

	public function getFrontLoadPercentage() {
		return $this->m_fltFrontLoadPercentage;
	}

	public function sqlFrontLoadPercentage() {
		return ( true == isset( $this->m_fltFrontLoadPercentage ) ) ? ( string ) $this->m_fltFrontLoadPercentage : '0';
	}

	public function setOngoingFrontLoadPosts( $intOngoingFrontLoadPosts ) {
		$this->set( 'm_intOngoingFrontLoadPosts', CStrings::strToIntDef( $intOngoingFrontLoadPosts, NULL, false ) );
	}

	public function getOngoingFrontLoadPosts() {
		return $this->m_intOngoingFrontLoadPosts;
	}

	public function sqlOngoingFrontLoadPosts() {
		return ( true == isset( $this->m_intOngoingFrontLoadPosts ) ) ? ( string ) $this->m_intOngoingFrontLoadPosts : '0';
	}

	public function setOngoingFrontLoadAmount( $fltOngoingFrontLoadAmount ) {
		$this->set( 'm_fltOngoingFrontLoadAmount', CStrings::strToFloatDef( $fltOngoingFrontLoadAmount, NULL, false, 2 ) );
	}

	public function getOngoingFrontLoadAmount() {
		return $this->m_fltOngoingFrontLoadAmount;
	}

	public function sqlOngoingFrontLoadAmount() {
		return ( true == isset( $this->m_fltOngoingFrontLoadAmount ) ) ? ( string ) $this->m_fltOngoingFrontLoadAmount : '0';
	}

	public function setOngoingFrontLoadPercent( $fltOngoingFrontLoadPercent ) {
		$this->set( 'm_fltOngoingFrontLoadPercent', CStrings::strToFloatDef( $fltOngoingFrontLoadPercent, NULL, false, 2 ) );
	}

	public function getOngoingFrontLoadPercent() {
		return $this->m_fltOngoingFrontLoadPercent;
	}

	public function sqlOngoingFrontLoadPercent() {
		return ( true == isset( $this->m_fltOngoingFrontLoadPercent ) ) ? ( string ) $this->m_fltOngoingFrontLoadPercent : '0';
	}

	public function setSetupCommissionPercent( $fltSetupCommissionPercent ) {
		$this->set( 'm_fltSetupCommissionPercent', CStrings::strToFloatDef( $fltSetupCommissionPercent, NULL, false, 4 ) );
	}

	public function getSetupCommissionPercent() {
		return $this->m_fltSetupCommissionPercent;
	}

	public function sqlSetupCommissionPercent() {
		return ( true == isset( $this->m_fltSetupCommissionPercent ) ) ? ( string ) $this->m_fltSetupCommissionPercent : '0';
	}

	public function setIsBookingBased( $intIsBookingBased ) {
		$this->set( 'm_intIsBookingBased', CStrings::strToIntDef( $intIsBookingBased, NULL, false ) );
	}

	public function getIsBookingBased() {
		return $this->m_intIsBookingBased;
	}

	public function sqlIsBookingBased() {
		return ( true == isset( $this->m_intIsBookingBased ) ) ? ( string ) $this->m_intIsBookingBased : '0';
	}

	public function setUnitCommissionAmount( $fltUnitCommissionAmount ) {
		$this->set( 'm_fltUnitCommissionAmount', CStrings::strToFloatDef( $fltUnitCommissionAmount, NULL, false, 4 ) );
	}

	public function getUnitCommissionAmount() {
		return $this->m_fltUnitCommissionAmount;
	}

	public function sqlUnitCommissionAmount() {
		return ( true == isset( $this->m_fltUnitCommissionAmount ) ) ? ( string ) $this->m_fltUnitCommissionAmount : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, commission_structure_id, ps_product_id, commission_bucket_id, charge_code_id, commission_tier_type_id, commission_amount, commission_percent, front_load_posts, front_load_amount, front_load_percentage, ongoing_front_load_posts, ongoing_front_load_amount, ongoing_front_load_percent, setup_commission_percent, is_booking_based, unit_commission_amount, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCommissionStructureId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlCommissionBucketId() . ', ' .
 						$this->sqlChargeCodeId() . ', ' .
 						$this->sqlCommissionTierTypeId() . ', ' .
 						$this->sqlCommissionAmount() . ', ' .
 						$this->sqlCommissionPercent() . ', ' .
 						$this->sqlFrontLoadPosts() . ', ' .
 						$this->sqlFrontLoadAmount() . ', ' .
 						$this->sqlFrontLoadPercentage() . ', ' .
 						$this->sqlOngoingFrontLoadPosts() . ', ' .
 						$this->sqlOngoingFrontLoadAmount() . ', ' .
 						$this->sqlOngoingFrontLoadPercent() . ', ' .
 						$this->sqlSetupCommissionPercent() . ', ' .
 						$this->sqlIsBookingBased() . ', ' .
 						$this->sqlUnitCommissionAmount() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_structure_id = ' . $this->sqlCommissionStructureId() . ','; } elseif( true == array_key_exists( 'CommissionStructureId', $this->getChangedColumns() ) ) { $strSql .= ' commission_structure_id = ' . $this->sqlCommissionStructureId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_bucket_id = ' . $this->sqlCommissionBucketId() . ','; } elseif( true == array_key_exists( 'CommissionBucketId', $this->getChangedColumns() ) ) { $strSql .= ' commission_bucket_id = ' . $this->sqlCommissionBucketId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId() . ','; } elseif( true == array_key_exists( 'ChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_tier_type_id = ' . $this->sqlCommissionTierTypeId() . ','; } elseif( true == array_key_exists( 'CommissionTierTypeId', $this->getChangedColumns() ) ) { $strSql .= ' commission_tier_type_id = ' . $this->sqlCommissionTierTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_amount = ' . $this->sqlCommissionAmount() . ','; } elseif( true == array_key_exists( 'CommissionAmount', $this->getChangedColumns() ) ) { $strSql .= ' commission_amount = ' . $this->sqlCommissionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_percent = ' . $this->sqlCommissionPercent() . ','; } elseif( true == array_key_exists( 'CommissionPercent', $this->getChangedColumns() ) ) { $strSql .= ' commission_percent = ' . $this->sqlCommissionPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' front_load_posts = ' . $this->sqlFrontLoadPosts() . ','; } elseif( true == array_key_exists( 'FrontLoadPosts', $this->getChangedColumns() ) ) { $strSql .= ' front_load_posts = ' . $this->sqlFrontLoadPosts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' front_load_amount = ' . $this->sqlFrontLoadAmount() . ','; } elseif( true == array_key_exists( 'FrontLoadAmount', $this->getChangedColumns() ) ) { $strSql .= ' front_load_amount = ' . $this->sqlFrontLoadAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' front_load_percentage = ' . $this->sqlFrontLoadPercentage() . ','; } elseif( true == array_key_exists( 'FrontLoadPercentage', $this->getChangedColumns() ) ) { $strSql .= ' front_load_percentage = ' . $this->sqlFrontLoadPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ongoing_front_load_posts = ' . $this->sqlOngoingFrontLoadPosts() . ','; } elseif( true == array_key_exists( 'OngoingFrontLoadPosts', $this->getChangedColumns() ) ) { $strSql .= ' ongoing_front_load_posts = ' . $this->sqlOngoingFrontLoadPosts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ongoing_front_load_amount = ' . $this->sqlOngoingFrontLoadAmount() . ','; } elseif( true == array_key_exists( 'OngoingFrontLoadAmount', $this->getChangedColumns() ) ) { $strSql .= ' ongoing_front_load_amount = ' . $this->sqlOngoingFrontLoadAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ongoing_front_load_percent = ' . $this->sqlOngoingFrontLoadPercent() . ','; } elseif( true == array_key_exists( 'OngoingFrontLoadPercent', $this->getChangedColumns() ) ) { $strSql .= ' ongoing_front_load_percent = ' . $this->sqlOngoingFrontLoadPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' setup_commission_percent = ' . $this->sqlSetupCommissionPercent() . ','; } elseif( true == array_key_exists( 'SetupCommissionPercent', $this->getChangedColumns() ) ) { $strSql .= ' setup_commission_percent = ' . $this->sqlSetupCommissionPercent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_booking_based = ' . $this->sqlIsBookingBased() . ','; } elseif( true == array_key_exists( 'IsBookingBased', $this->getChangedColumns() ) ) { $strSql .= ' is_booking_based = ' . $this->sqlIsBookingBased() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_commission_amount = ' . $this->sqlUnitCommissionAmount() . ','; } elseif( true == array_key_exists( 'UnitCommissionAmount', $this->getChangedColumns() ) ) { $strSql .= ' unit_commission_amount = ' . $this->sqlUnitCommissionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'commission_structure_id' => $this->getCommissionStructureId(),
			'ps_product_id' => $this->getPsProductId(),
			'commission_bucket_id' => $this->getCommissionBucketId(),
			'charge_code_id' => $this->getChargeCodeId(),
			'commission_tier_type_id' => $this->getCommissionTierTypeId(),
			'commission_amount' => $this->getCommissionAmount(),
			'commission_percent' => $this->getCommissionPercent(),
			'front_load_posts' => $this->getFrontLoadPosts(),
			'front_load_amount' => $this->getFrontLoadAmount(),
			'front_load_percentage' => $this->getFrontLoadPercentage(),
			'ongoing_front_load_posts' => $this->getOngoingFrontLoadPosts(),
			'ongoing_front_load_amount' => $this->getOngoingFrontLoadAmount(),
			'ongoing_front_load_percent' => $this->getOngoingFrontLoadPercent(),
			'setup_commission_percent' => $this->getSetupCommissionPercent(),
			'is_booking_based' => $this->getIsBookingBased(),
			'unit_commission_amount' => $this->getUnitCommissionAmount(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>