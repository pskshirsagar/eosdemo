<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestGraders
 * Do not add any new functions to this class.
 */

class CBaseTestGraders extends CEosPluralBase {

	/**
	 * @return CTestGrader[]
	 */
	public static function fetchTestGraders( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestGrader', $objDatabase );
	}

	/**
	 * @return CTestGrader
	 */
	public static function fetchTestGrader( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestGrader', $objDatabase );
	}

	public static function fetchTestGraderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_graders', $objDatabase );
	}

	public static function fetchTestGraderById( $intId, $objDatabase ) {
		return self::fetchTestGrader( sprintf( 'SELECT * FROM test_graders WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestGradersByTestId( $intTestId, $objDatabase ) {
		return self::fetchTestGraders( sprintf( 'SELECT * FROM test_graders WHERE test_id = %d', ( int ) $intTestId ), $objDatabase );
	}

	public static function fetchTestGradersByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchTestGraders( sprintf( 'SELECT * FROM test_graders WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>