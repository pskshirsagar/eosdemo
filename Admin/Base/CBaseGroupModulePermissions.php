<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroupModulePermissions
 * Do not add any new functions to this class.
 */

class CBaseGroupModulePermissions extends CEosPluralBase {

	/**
	 * @return CGroupModulePermission[]
	 */
	public static function fetchGroupModulePermissions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CGroupModulePermission', $objDatabase );
	}

	/**
	 * @return CGroupModulePermission
	 */
	public static function fetchGroupModulePermission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGroupModulePermission', $objDatabase );
	}

	public static function fetchGroupModulePermissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'group_module_permissions', $objDatabase );
	}

	public static function fetchGroupModulePermissionById( $intId, $objDatabase ) {
		return self::fetchGroupModulePermission( sprintf( 'SELECT * FROM group_module_permissions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchGroupModulePermissionsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchGroupModulePermissions( sprintf( 'SELECT * FROM group_module_permissions WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

	public static function fetchGroupModulePermissionsByModuleId( $intModuleId, $objDatabase ) {
		return self::fetchGroupModulePermissions( sprintf( 'SELECT * FROM group_module_permissions WHERE module_id = %d', ( int ) $intModuleId ), $objDatabase );
	}

}
?>