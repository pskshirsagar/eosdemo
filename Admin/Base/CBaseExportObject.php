<?php

class CBaseExportObject extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.export_objects';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intExportPartnerId;
	protected $m_strExportObjectType;
	protected $m_strExportObjectTypeKey;
	protected $m_intExportStatusTypeId;
	protected $m_strReferenceId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intExportedBy;
	protected $m_strExportedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['export_partner_id'] ) && $boolDirectSet ) $this->set( 'm_intExportPartnerId', trim( $arrValues['export_partner_id'] ) ); elseif( isset( $arrValues['export_partner_id'] ) ) $this->setExportPartnerId( $arrValues['export_partner_id'] );
		if( isset( $arrValues['export_object_type'] ) && $boolDirectSet ) $this->set( 'm_strExportObjectType', trim( stripcslashes( $arrValues['export_object_type'] ) ) ); elseif( isset( $arrValues['export_object_type'] ) ) $this->setExportObjectType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['export_object_type'] ) : $arrValues['export_object_type'] );
		if( isset( $arrValues['export_object_type_key'] ) && $boolDirectSet ) $this->set( 'm_strExportObjectTypeKey', trim( stripcslashes( $arrValues['export_object_type_key'] ) ) ); elseif( isset( $arrValues['export_object_type_key'] ) ) $this->setExportObjectTypeKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['export_object_type_key'] ) : $arrValues['export_object_type_key'] );
		if( isset( $arrValues['export_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intExportStatusTypeId', trim( $arrValues['export_status_type_id'] ) ); elseif( isset( $arrValues['export_status_type_id'] ) ) $this->setExportStatusTypeId( $arrValues['export_status_type_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_strReferenceId', trim( stripcslashes( $arrValues['reference_id'] ) ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reference_id'] ) : $arrValues['reference_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['exported_by'] ) && $boolDirectSet ) $this->set( 'm_intExportedBy', trim( $arrValues['exported_by'] ) ); elseif( isset( $arrValues['exported_by'] ) ) $this->setExportedBy( $arrValues['exported_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setExportPartnerId( $intExportPartnerId ) {
		$this->set( 'm_intExportPartnerId', CStrings::strToIntDef( $intExportPartnerId, NULL, false ) );
	}

	public function getExportPartnerId() {
		return $this->m_intExportPartnerId;
	}

	public function sqlExportPartnerId() {
		return ( true == isset( $this->m_intExportPartnerId ) ) ? ( string ) $this->m_intExportPartnerId : 'NULL';
	}

	public function setExportObjectType( $strExportObjectType ) {
		$this->set( 'm_strExportObjectType', CStrings::strTrimDef( $strExportObjectType, -1, NULL, true ) );
	}

	public function getExportObjectType() {
		return $this->m_strExportObjectType;
	}

	public function sqlExportObjectType() {
		return ( true == isset( $this->m_strExportObjectType ) ) ? '\'' . addslashes( $this->m_strExportObjectType ) . '\'' : 'NULL';
	}

	public function setExportObjectTypeKey( $strExportObjectTypeKey ) {
		$this->set( 'm_strExportObjectTypeKey', CStrings::strTrimDef( $strExportObjectTypeKey, 250, NULL, true ) );
	}

	public function getExportObjectTypeKey() {
		return $this->m_strExportObjectTypeKey;
	}

	public function sqlExportObjectTypeKey() {
		return ( true == isset( $this->m_strExportObjectTypeKey ) ) ? '\'' . addslashes( $this->m_strExportObjectTypeKey ) . '\'' : 'NULL';
	}

	public function setExportStatusTypeId( $intExportStatusTypeId ) {
		$this->set( 'm_intExportStatusTypeId', CStrings::strToIntDef( $intExportStatusTypeId, NULL, false ) );
	}

	public function getExportStatusTypeId() {
		return $this->m_intExportStatusTypeId;
	}

	public function sqlExportStatusTypeId() {
		return ( true == isset( $this->m_intExportStatusTypeId ) ) ? ( string ) $this->m_intExportStatusTypeId : 'NULL';
	}

	public function setReferenceId( $strReferenceId ) {
		$this->set( 'm_strReferenceId', CStrings::strTrimDef( $strReferenceId, 250, NULL, true ) );
	}

	public function getReferenceId() {
		return $this->m_strReferenceId;
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_strReferenceId ) ) ? '\'' . addslashes( $this->m_strReferenceId ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setExportedBy( $intExportedBy ) {
		$this->set( 'm_intExportedBy', CStrings::strToIntDef( $intExportedBy, NULL, false ) );
	}

	public function getExportedBy() {
		return $this->m_intExportedBy;
	}

	public function sqlExportedBy() {
		return ( true == isset( $this->m_intExportedBy ) ) ? ( string ) $this->m_intExportedBy : 'NULL';
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, export_partner_id, export_object_type, export_object_type_key, export_status_type_id, reference_id, details, updated_by, updated_on, exported_by, exported_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlExportPartnerId() . ', ' .
						$this->sqlExportObjectType() . ', ' .
						$this->sqlExportObjectTypeKey() . ', ' .
						$this->sqlExportStatusTypeId() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_partner_id = ' . $this->sqlExportPartnerId(). ',' ; } elseif( true == array_key_exists( 'ExportPartnerId', $this->getChangedColumns() ) ) { $strSql .= ' export_partner_id = ' . $this->sqlExportPartnerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_object_type = ' . $this->sqlExportObjectType(). ',' ; } elseif( true == array_key_exists( 'ExportObjectType', $this->getChangedColumns() ) ) { $strSql .= ' export_object_type = ' . $this->sqlExportObjectType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_object_type_key = ' . $this->sqlExportObjectTypeKey(). ',' ; } elseif( true == array_key_exists( 'ExportObjectTypeKey', $this->getChangedColumns() ) ) { $strSql .= ' export_object_type_key = ' . $this->sqlExportObjectTypeKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_status_type_id = ' . $this->sqlExportStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ExportStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' export_status_type_id = ' . $this->sqlExportStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy(). ',' ; } elseif( true == array_key_exists( 'ExportedBy', $this->getChangedColumns() ) ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'export_partner_id' => $this->getExportPartnerId(),
			'export_object_type' => $this->getExportObjectType(),
			'export_object_type_key' => $this->getExportObjectTypeKey(),
			'export_status_type_id' => $this->getExportStatusTypeId(),
			'reference_id' => $this->getReferenceId(),
			'details' => $this->getDetails(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'exported_by' => $this->getExportedBy(),
			'exported_on' => $this->getExportedOn()
		);
	}

}
?>