<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyGoals
 * Do not add any new functions to this class.
 */

class CBaseCompanyGoals extends CEosPluralBase {

	/**
	 * @return CCompanyGoal[]
	 */
	public static function fetchCompanyGoals( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCompanyGoal', $objDatabase );
	}

	/**
	 * @return CCompanyGoal
	 */
	public static function fetchCompanyGoal( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyGoal', $objDatabase );
	}

	public static function fetchCompanyGoalCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_goals', $objDatabase );
	}

	public static function fetchCompanyGoalById( $intId, $objDatabase ) {
		return self::fetchCompanyGoal( sprintf( 'SELECT * FROM company_goals WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>