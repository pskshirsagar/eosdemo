<?php

class CBaseContractCommit extends CEosSingularBase {

	const TABLE_NAME = 'public.contract_commits';

	protected $m_intId;
	protected $m_intPsLeadId;
	protected $m_intContractId;
	protected $m_intSalesCloserEmployeeId;
	protected $m_strCloseDate;
	protected $m_fltCloseLiklihood;
	protected $m_strCommitMonth;
	protected $m_fltAcvCommitted;
	protected $m_strScheduledCloseMonth;
	protected $m_strCommitDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltCloseLiklihood = '0';
		$this->m_fltAcvCommitted = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['sales_closer_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSalesCloserEmployeeId', trim( $arrValues['sales_closer_employee_id'] ) ); elseif( isset( $arrValues['sales_closer_employee_id'] ) ) $this->setSalesCloserEmployeeId( $arrValues['sales_closer_employee_id'] );
		if( isset( $arrValues['close_date'] ) && $boolDirectSet ) $this->set( 'm_strCloseDate', trim( $arrValues['close_date'] ) ); elseif( isset( $arrValues['close_date'] ) ) $this->setCloseDate( $arrValues['close_date'] );
		if( isset( $arrValues['close_liklihood'] ) && $boolDirectSet ) $this->set( 'm_fltCloseLiklihood', trim( $arrValues['close_liklihood'] ) ); elseif( isset( $arrValues['close_liklihood'] ) ) $this->setCloseLiklihood( $arrValues['close_liklihood'] );
		if( isset( $arrValues['commit_month'] ) && $boolDirectSet ) $this->set( 'm_strCommitMonth', trim( $arrValues['commit_month'] ) ); elseif( isset( $arrValues['commit_month'] ) ) $this->setCommitMonth( $arrValues['commit_month'] );
		if( isset( $arrValues['acv_committed'] ) && $boolDirectSet ) $this->set( 'm_fltAcvCommitted', trim( $arrValues['acv_committed'] ) ); elseif( isset( $arrValues['acv_committed'] ) ) $this->setAcvCommitted( $arrValues['acv_committed'] );
		if( isset( $arrValues['scheduled_close_month'] ) && $boolDirectSet ) $this->set( 'm_strScheduledCloseMonth', trim( $arrValues['scheduled_close_month'] ) ); elseif( isset( $arrValues['scheduled_close_month'] ) ) $this->setScheduledCloseMonth( $arrValues['scheduled_close_month'] );
		if( isset( $arrValues['commit_datetime'] ) && $boolDirectSet ) $this->set( 'm_strCommitDatetime', trim( $arrValues['commit_datetime'] ) ); elseif( isset( $arrValues['commit_datetime'] ) ) $this->setCommitDatetime( $arrValues['commit_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setSalesCloserEmployeeId( $intSalesCloserEmployeeId ) {
		$this->set( 'm_intSalesCloserEmployeeId', CStrings::strToIntDef( $intSalesCloserEmployeeId, NULL, false ) );
	}

	public function getSalesCloserEmployeeId() {
		return $this->m_intSalesCloserEmployeeId;
	}

	public function sqlSalesCloserEmployeeId() {
		return ( true == isset( $this->m_intSalesCloserEmployeeId ) ) ? ( string ) $this->m_intSalesCloserEmployeeId : 'NULL';
	}

	public function setCloseDate( $strCloseDate ) {
		$this->set( 'm_strCloseDate', CStrings::strTrimDef( $strCloseDate, -1, NULL, true ) );
	}

	public function getCloseDate() {
		return $this->m_strCloseDate;
	}

	public function sqlCloseDate() {
		return ( true == isset( $this->m_strCloseDate ) ) ? '\'' . $this->m_strCloseDate . '\'' : 'NOW()';
	}

	public function setCloseLiklihood( $fltCloseLiklihood ) {
		$this->set( 'm_fltCloseLiklihood', CStrings::strToFloatDef( $fltCloseLiklihood, NULL, false, 2 ) );
	}

	public function getCloseLiklihood() {
		return $this->m_fltCloseLiklihood;
	}

	public function sqlCloseLiklihood() {
		return ( true == isset( $this->m_fltCloseLiklihood ) ) ? ( string ) $this->m_fltCloseLiklihood : '0';
	}

	public function setCommitMonth( $strCommitMonth ) {
		$this->set( 'm_strCommitMonth', CStrings::strTrimDef( $strCommitMonth, -1, NULL, true ) );
	}

	public function getCommitMonth() {
		return $this->m_strCommitMonth;
	}

	public function sqlCommitMonth() {
		return ( true == isset( $this->m_strCommitMonth ) ) ? '\'' . $this->m_strCommitMonth . '\'' : 'NOW()';
	}

	public function setAcvCommitted( $fltAcvCommitted ) {
		$this->set( 'm_fltAcvCommitted', CStrings::strToFloatDef( $fltAcvCommitted, NULL, false, 2 ) );
	}

	public function getAcvCommitted() {
		return $this->m_fltAcvCommitted;
	}

	public function sqlAcvCommitted() {
		return ( true == isset( $this->m_fltAcvCommitted ) ) ? ( string ) $this->m_fltAcvCommitted : '0';
	}

	public function setScheduledCloseMonth( $strScheduledCloseMonth ) {
		$this->set( 'm_strScheduledCloseMonth', CStrings::strTrimDef( $strScheduledCloseMonth, -1, NULL, true ) );
	}

	public function getScheduledCloseMonth() {
		return $this->m_strScheduledCloseMonth;
	}

	public function sqlScheduledCloseMonth() {
		return ( true == isset( $this->m_strScheduledCloseMonth ) ) ? '\'' . $this->m_strScheduledCloseMonth . '\'' : 'NOW()';
	}

	public function setCommitDatetime( $strCommitDatetime ) {
		$this->set( 'm_strCommitDatetime', CStrings::strTrimDef( $strCommitDatetime, -1, NULL, true ) );
	}

	public function getCommitDatetime() {
		return $this->m_strCommitDatetime;
	}

	public function sqlCommitDatetime() {
		return ( true == isset( $this->m_strCommitDatetime ) ) ? '\'' . $this->m_strCommitDatetime . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_lead_id, contract_id, sales_closer_employee_id, close_date, close_liklihood, commit_month, acv_committed, scheduled_close_month, commit_datetime, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPsLeadId() . ', ' .
						$this->sqlContractId() . ', ' .
						$this->sqlSalesCloserEmployeeId() . ', ' .
						$this->sqlCloseDate() . ', ' .
						$this->sqlCloseLiklihood() . ', ' .
						$this->sqlCommitMonth() . ', ' .
						$this->sqlAcvCommitted() . ', ' .
						$this->sqlScheduledCloseMonth() . ', ' .
						$this->sqlCommitDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId(). ',' ; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_closer_employee_id = ' . $this->sqlSalesCloserEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SalesCloserEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sales_closer_employee_id = ' . $this->sqlSalesCloserEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' close_date = ' . $this->sqlCloseDate(). ',' ; } elseif( true == array_key_exists( 'CloseDate', $this->getChangedColumns() ) ) { $strSql .= ' close_date = ' . $this->sqlCloseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' close_liklihood = ' . $this->sqlCloseLiklihood(). ',' ; } elseif( true == array_key_exists( 'CloseLiklihood', $this->getChangedColumns() ) ) { $strSql .= ' close_liklihood = ' . $this->sqlCloseLiklihood() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commit_month = ' . $this->sqlCommitMonth(). ',' ; } elseif( true == array_key_exists( 'CommitMonth', $this->getChangedColumns() ) ) { $strSql .= ' commit_month = ' . $this->sqlCommitMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' acv_committed = ' . $this->sqlAcvCommitted(). ',' ; } elseif( true == array_key_exists( 'AcvCommitted', $this->getChangedColumns() ) ) { $strSql .= ' acv_committed = ' . $this->sqlAcvCommitted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_close_month = ' . $this->sqlScheduledCloseMonth(). ',' ; } elseif( true == array_key_exists( 'ScheduledCloseMonth', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_close_month = ' . $this->sqlScheduledCloseMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commit_datetime = ' . $this->sqlCommitDatetime(). ',' ; } elseif( true == array_key_exists( 'CommitDatetime', $this->getChangedColumns() ) ) { $strSql .= ' commit_datetime = ' . $this->sqlCommitDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'contract_id' => $this->getContractId(),
			'sales_closer_employee_id' => $this->getSalesCloserEmployeeId(),
			'close_date' => $this->getCloseDate(),
			'close_liklihood' => $this->getCloseLiklihood(),
			'commit_month' => $this->getCommitMonth(),
			'acv_committed' => $this->getAcvCommitted(),
			'scheduled_close_month' => $this->getScheduledCloseMonth(),
			'commit_datetime' => $this->getCommitDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>