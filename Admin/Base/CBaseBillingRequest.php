<?php

class CBaseBillingRequest extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.billing_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intBillingRequestTypeId;
	protected $m_intBillingRequestStatusTypeId;
	protected $m_intPsDocumentId;
	protected $m_intPersonId;
	protected $m_intTaskId;
	protected $m_strReasonForDelay;
	protected $m_strDocumentVerbiage;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDocumentApprovedBy;
	protected $m_strDocumentApprovedOn;
	protected $m_intDocumentSignedBy;
	protected $m_strDocumentSignedOn;
	protected $m_intCountersignedBy;
	protected $m_strCountersignedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['billing_request_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingRequestTypeId', trim( $arrValues['billing_request_type_id'] ) ); elseif( isset( $arrValues['billing_request_type_id'] ) ) $this->setBillingRequestTypeId( $arrValues['billing_request_type_id'] );
		if( isset( $arrValues['billing_request_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intBillingRequestStatusTypeId', trim( $arrValues['billing_request_status_type_id'] ) ); elseif( isset( $arrValues['billing_request_status_type_id'] ) ) $this->setBillingRequestStatusTypeId( $arrValues['billing_request_status_type_id'] );
		if( isset( $arrValues['ps_document_id'] ) && $boolDirectSet ) $this->set( 'm_intPsDocumentId', trim( $arrValues['ps_document_id'] ) ); elseif( isset( $arrValues['ps_document_id'] ) ) $this->setPsDocumentId( $arrValues['ps_document_id'] );
		if( isset( $arrValues['person_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonId', trim( $arrValues['person_id'] ) ); elseif( isset( $arrValues['person_id'] ) ) $this->setPersonId( $arrValues['person_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['reason_for_delay'] ) && $boolDirectSet ) $this->set( 'm_strReasonForDelay', trim( $arrValues['reason_for_delay'] ) ); elseif( isset( $arrValues['reason_for_delay'] ) ) $this->setReasonForDelay( $arrValues['reason_for_delay'] );
		if( isset( $arrValues['document_verbiage'] ) && $boolDirectSet ) $this->set( 'm_strDocumentVerbiage', trim( $arrValues['document_verbiage'] ) ); elseif( isset( $arrValues['document_verbiage'] ) ) $this->setDocumentVerbiage( $arrValues['document_verbiage'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['document_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intDocumentApprovedBy', trim( $arrValues['document_approved_by'] ) ); elseif( isset( $arrValues['document_approved_by'] ) ) $this->setDocumentApprovedBy( $arrValues['document_approved_by'] );
		if( isset( $arrValues['document_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strDocumentApprovedOn', trim( $arrValues['document_approved_on'] ) ); elseif( isset( $arrValues['document_approved_on'] ) ) $this->setDocumentApprovedOn( $arrValues['document_approved_on'] );
		if( isset( $arrValues['document_signed_by'] ) && $boolDirectSet ) $this->set( 'm_intDocumentSignedBy', trim( $arrValues['document_signed_by'] ) ); elseif( isset( $arrValues['document_signed_by'] ) ) $this->setDocumentSignedBy( $arrValues['document_signed_by'] );
		if( isset( $arrValues['document_signed_on'] ) && $boolDirectSet ) $this->set( 'm_strDocumentSignedOn', trim( $arrValues['document_signed_on'] ) ); elseif( isset( $arrValues['document_signed_on'] ) ) $this->setDocumentSignedOn( $arrValues['document_signed_on'] );
		if( isset( $arrValues['countersigned_by'] ) && $boolDirectSet ) $this->set( 'm_intCountersignedBy', trim( $arrValues['countersigned_by'] ) ); elseif( isset( $arrValues['countersigned_by'] ) ) $this->setCountersignedBy( $arrValues['countersigned_by'] );
		if( isset( $arrValues['countersigned_on'] ) && $boolDirectSet ) $this->set( 'm_strCountersignedOn', trim( $arrValues['countersigned_on'] ) ); elseif( isset( $arrValues['countersigned_on'] ) ) $this->setCountersignedOn( $arrValues['countersigned_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setBillingRequestTypeId( $intBillingRequestTypeId ) {
		$this->set( 'm_intBillingRequestTypeId', CStrings::strToIntDef( $intBillingRequestTypeId, NULL, false ) );
	}

	public function getBillingRequestTypeId() {
		return $this->m_intBillingRequestTypeId;
	}

	public function sqlBillingRequestTypeId() {
		return ( true == isset( $this->m_intBillingRequestTypeId ) ) ? ( string ) $this->m_intBillingRequestTypeId : 'NULL';
	}

	public function setBillingRequestStatusTypeId( $intBillingRequestStatusTypeId ) {
		$this->set( 'm_intBillingRequestStatusTypeId', CStrings::strToIntDef( $intBillingRequestStatusTypeId, NULL, false ) );
	}

	public function getBillingRequestStatusTypeId() {
		return $this->m_intBillingRequestStatusTypeId;
	}

	public function sqlBillingRequestStatusTypeId() {
		return ( true == isset( $this->m_intBillingRequestStatusTypeId ) ) ? ( string ) $this->m_intBillingRequestStatusTypeId : 'NULL';
	}

	public function setPsDocumentId( $intPsDocumentId ) {
		$this->set( 'm_intPsDocumentId', CStrings::strToIntDef( $intPsDocumentId, NULL, false ) );
	}

	public function getPsDocumentId() {
		return $this->m_intPsDocumentId;
	}

	public function sqlPsDocumentId() {
		return ( true == isset( $this->m_intPsDocumentId ) ) ? ( string ) $this->m_intPsDocumentId : 'NULL';
	}

	public function setPersonId( $intPersonId ) {
		$this->set( 'm_intPersonId', CStrings::strToIntDef( $intPersonId, NULL, false ) );
	}

	public function getPersonId() {
		return $this->m_intPersonId;
	}

	public function sqlPersonId() {
		return ( true == isset( $this->m_intPersonId ) ) ? ( string ) $this->m_intPersonId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setReasonForDelay( $strReasonForDelay ) {
		$this->set( 'm_strReasonForDelay', CStrings::strTrimDef( $strReasonForDelay, 350, NULL, true ) );
	}

	public function getReasonForDelay() {
		return $this->m_strReasonForDelay;
	}

	public function sqlReasonForDelay() {
		return ( true == isset( $this->m_strReasonForDelay ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReasonForDelay ) : '\'' . addslashes( $this->m_strReasonForDelay ) . '\'' ) : 'NULL';
	}

	public function setDocumentVerbiage( $strDocumentVerbiage ) {
		$this->set( 'm_strDocumentVerbiage', CStrings::strTrimDef( $strDocumentVerbiage, -1, NULL, true ) );
	}

	public function getDocumentVerbiage() {
		return $this->m_strDocumentVerbiage;
	}

	public function sqlDocumentVerbiage() {
		return ( true == isset( $this->m_strDocumentVerbiage ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDocumentVerbiage ) : '\'' . addslashes( $this->m_strDocumentVerbiage ) . '\'' ) : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDocumentApprovedBy( $intDocumentApprovedBy ) {
		$this->set( 'm_intDocumentApprovedBy', CStrings::strToIntDef( $intDocumentApprovedBy, NULL, false ) );
	}

	public function getDocumentApprovedBy() {
		return $this->m_intDocumentApprovedBy;
	}

	public function sqlDocumentApprovedBy() {
		return ( true == isset( $this->m_intDocumentApprovedBy ) ) ? ( string ) $this->m_intDocumentApprovedBy : 'NULL';
	}

	public function setDocumentApprovedOn( $strDocumentApprovedOn ) {
		$this->set( 'm_strDocumentApprovedOn', CStrings::strTrimDef( $strDocumentApprovedOn, -1, NULL, true ) );
	}

	public function getDocumentApprovedOn() {
		return $this->m_strDocumentApprovedOn;
	}

	public function sqlDocumentApprovedOn() {
		return ( true == isset( $this->m_strDocumentApprovedOn ) ) ? '\'' . $this->m_strDocumentApprovedOn . '\'' : 'NULL';
	}

	public function setDocumentSignedBy( $intDocumentSignedBy ) {
		$this->set( 'm_intDocumentSignedBy', CStrings::strToIntDef( $intDocumentSignedBy, NULL, false ) );
	}

	public function getDocumentSignedBy() {
		return $this->m_intDocumentSignedBy;
	}

	public function sqlDocumentSignedBy() {
		return ( true == isset( $this->m_intDocumentSignedBy ) ) ? ( string ) $this->m_intDocumentSignedBy : 'NULL';
	}

	public function setDocumentSignedOn( $strDocumentSignedOn ) {
		$this->set( 'm_strDocumentSignedOn', CStrings::strTrimDef( $strDocumentSignedOn, -1, NULL, true ) );
	}

	public function getDocumentSignedOn() {
		return $this->m_strDocumentSignedOn;
	}

	public function sqlDocumentSignedOn() {
		return ( true == isset( $this->m_strDocumentSignedOn ) ) ? '\'' . $this->m_strDocumentSignedOn . '\'' : 'NULL';
	}

	public function setCountersignedBy( $intCountersignedBy ) {
		$this->set( 'm_intCountersignedBy', CStrings::strToIntDef( $intCountersignedBy, NULL, false ) );
	}

	public function getCountersignedBy() {
		return $this->m_intCountersignedBy;
	}

	public function sqlCountersignedBy() {
		return ( true == isset( $this->m_intCountersignedBy ) ) ? ( string ) $this->m_intCountersignedBy : 'NULL';
	}

	public function setCountersignedOn( $strCountersignedOn ) {
		$this->set( 'm_strCountersignedOn', CStrings::strTrimDef( $strCountersignedOn, -1, NULL, true ) );
	}

	public function getCountersignedOn() {
		return $this->m_strCountersignedOn;
	}

	public function sqlCountersignedOn() {
		return ( true == isset( $this->m_strCountersignedOn ) ) ? '\'' . $this->m_strCountersignedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, billing_request_type_id, billing_request_status_type_id, ps_document_id, person_id, task_id, reason_for_delay, document_verbiage, approved_by, approved_on, document_approved_by, document_approved_on, document_signed_by, document_signed_on, countersigned_by, countersigned_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlBillingRequestTypeId() . ', ' .
						$this->sqlBillingRequestStatusTypeId() . ', ' .
						$this->sqlPsDocumentId() . ', ' .
						$this->sqlPersonId() . ', ' .
						$this->sqlTaskId() . ', ' .
						$this->sqlReasonForDelay() . ', ' .
						$this->sqlDocumentVerbiage() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlDocumentApprovedBy() . ', ' .
						$this->sqlDocumentApprovedOn() . ', ' .
						$this->sqlDocumentSignedBy() . ', ' .
						$this->sqlDocumentSignedOn() . ', ' .
						$this->sqlCountersignedBy() . ', ' .
						$this->sqlCountersignedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_request_type_id = ' . $this->sqlBillingRequestTypeId(). ',' ; } elseif( true == array_key_exists( 'BillingRequestTypeId', $this->getChangedColumns() ) ) { $strSql .= ' billing_request_type_id = ' . $this->sqlBillingRequestTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_request_status_type_id = ' . $this->sqlBillingRequestStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'BillingRequestStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' billing_request_status_type_id = ' . $this->sqlBillingRequestStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId(). ',' ; } elseif( true == array_key_exists( 'PsDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' person_id = ' . $this->sqlPersonId(). ',' ; } elseif( true == array_key_exists( 'PersonId', $this->getChangedColumns() ) ) { $strSql .= ' person_id = ' . $this->sqlPersonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId(). ',' ; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_for_delay = ' . $this->sqlReasonForDelay(). ',' ; } elseif( true == array_key_exists( 'ReasonForDelay', $this->getChangedColumns() ) ) { $strSql .= ' reason_for_delay = ' . $this->sqlReasonForDelay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_verbiage = ' . $this->sqlDocumentVerbiage(). ',' ; } elseif( true == array_key_exists( 'DocumentVerbiage', $this->getChangedColumns() ) ) { $strSql .= ' document_verbiage = ' . $this->sqlDocumentVerbiage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_approved_by = ' . $this->sqlDocumentApprovedBy(). ',' ; } elseif( true == array_key_exists( 'DocumentApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' document_approved_by = ' . $this->sqlDocumentApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_approved_on = ' . $this->sqlDocumentApprovedOn(). ',' ; } elseif( true == array_key_exists( 'DocumentApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' document_approved_on = ' . $this->sqlDocumentApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_signed_by = ' . $this->sqlDocumentSignedBy(). ',' ; } elseif( true == array_key_exists( 'DocumentSignedBy', $this->getChangedColumns() ) ) { $strSql .= ' document_signed_by = ' . $this->sqlDocumentSignedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_signed_on = ' . $this->sqlDocumentSignedOn(). ',' ; } elseif( true == array_key_exists( 'DocumentSignedOn', $this->getChangedColumns() ) ) { $strSql .= ' document_signed_on = ' . $this->sqlDocumentSignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' countersigned_by = ' . $this->sqlCountersignedBy(). ',' ; } elseif( true == array_key_exists( 'CountersignedBy', $this->getChangedColumns() ) ) { $strSql .= ' countersigned_by = ' . $this->sqlCountersignedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' countersigned_on = ' . $this->sqlCountersignedOn(). ',' ; } elseif( true == array_key_exists( 'CountersignedOn', $this->getChangedColumns() ) ) { $strSql .= ' countersigned_on = ' . $this->sqlCountersignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'billing_request_type_id' => $this->getBillingRequestTypeId(),
			'billing_request_status_type_id' => $this->getBillingRequestStatusTypeId(),
			'ps_document_id' => $this->getPsDocumentId(),
			'person_id' => $this->getPersonId(),
			'task_id' => $this->getTaskId(),
			'reason_for_delay' => $this->getReasonForDelay(),
			'document_verbiage' => $this->getDocumentVerbiage(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'document_approved_by' => $this->getDocumentApprovedBy(),
			'document_approved_on' => $this->getDocumentApprovedOn(),
			'document_signed_by' => $this->getDocumentSignedBy(),
			'document_signed_on' => $this->getDocumentSignedOn(),
			'countersigned_by' => $this->getCountersignedBy(),
			'countersigned_on' => $this->getCountersignedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>