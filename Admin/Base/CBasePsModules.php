<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsModules
 * Do not add any new functions to this class.
 */

class CBasePsModules extends CEosPluralBase {

	/**
	 * @return CPsModule[]
	 */
	public static function fetchPsModules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPsModule::class, $objDatabase );
	}

	/**
	 * @return CPsModule
	 */
	public static function fetchPsModule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPsModule::class, $objDatabase );
	}

	public static function fetchPsModuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_modules', $objDatabase );
	}

	public static function fetchPsModuleById( $intId, $objDatabase ) {
		return self::fetchPsModule( sprintf( 'SELECT * FROM ps_modules WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchPsModulesByPsModuleId( $intPsModuleId, $objDatabase ) {
		return self::fetchPsModules( sprintf( 'SELECT * FROM ps_modules WHERE ps_module_id = %d', $intPsModuleId ), $objDatabase );
	}

}
?>