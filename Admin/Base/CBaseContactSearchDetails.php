<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CContactSearchDetails
 * Do not add any new functions to this class.
 */

class CBaseContactSearchDetails extends CEosPluralBase {

	/**
	 * @return CContactSearchDetail[]
	 */
	public static function fetchContactSearchDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CContactSearchDetail::class, $objDatabase );
	}

	/**
	 * @return CContactSearchDetail
	 */
	public static function fetchContactSearchDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CContactSearchDetail::class, $objDatabase );
	}

	public static function fetchContactSearchDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'contact_search_details', $objDatabase );
	}

	public static function fetchContactSearchDetailById( $intId, $objDatabase ) {
		return self::fetchContactSearchDetail( sprintf( 'SELECT * FROM contact_search_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchContactSearchDetailsByContactSearchTypeId( $intContactSearchTypeId, $objDatabase ) {
		return self::fetchContactSearchDetails( sprintf( 'SELECT * FROM contact_search_details WHERE contact_search_type_id = %d', ( int ) $intContactSearchTypeId ), $objDatabase );
	}

	public static function fetchContactSearchDetailsByReferenceId( $intReferenceId, $objDatabase ) {
		return self::fetchContactSearchDetails( sprintf( 'SELECT * FROM contact_search_details WHERE reference_id = %d', ( int ) $intReferenceId ), $objDatabase );
	}

}
?>