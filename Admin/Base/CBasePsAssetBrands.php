<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsAssetBrands
 * Do not add any new functions to this class.
 */

class CBasePsAssetBrands extends CEosPluralBase {

	/**
	 * @return CPsAssetBrand[]
	 */
	public static function fetchPsAssetBrands( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsAssetBrand', $objDatabase );
	}

	/**
	 * @return CPsAssetBrand
	 */
	public static function fetchPsAssetBrand( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsAssetBrand', $objDatabase );
	}

	public static function fetchPsAssetBrandCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_asset_brands', $objDatabase );
	}

	public static function fetchPsAssetBrandById( $intId, $objDatabase ) {
		return self::fetchPsAssetBrand( sprintf( 'SELECT * FROM ps_asset_brands WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsAssetBrandsByPsAssetTypeId( $intPsAssetTypeId, $objDatabase ) {
		return self::fetchPsAssetBrands( sprintf( 'SELECT * FROM ps_asset_brands WHERE ps_asset_type_id = %d', ( int ) $intPsAssetTypeId ), $objDatabase );
	}

}
?>