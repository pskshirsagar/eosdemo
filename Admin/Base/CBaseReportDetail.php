<?php

class CBaseReportDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.report_details';

	protected $m_intId;
	protected $m_intCompanyReportId;
	protected $m_intReportDataTypeId;
	protected $m_intReportSqlDetailId;
	protected $m_intReportSpreadsheetId;
	protected $m_fltVersionNumber;
	protected $m_boolIsCurrentVersion;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltVersionNumber = '0';
		$this->m_boolIsCurrentVersion = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['company_report_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyReportId', trim( $arrValues['company_report_id'] ) ); elseif( isset( $arrValues['company_report_id'] ) ) $this->setCompanyReportId( $arrValues['company_report_id'] );
		if( isset( $arrValues['report_data_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReportDataTypeId', trim( $arrValues['report_data_type_id'] ) ); elseif( isset( $arrValues['report_data_type_id'] ) ) $this->setReportDataTypeId( $arrValues['report_data_type_id'] );
		if( isset( $arrValues['report_sql_detail_id'] ) && $boolDirectSet ) $this->set( 'm_intReportSqlDetailId', trim( $arrValues['report_sql_detail_id'] ) ); elseif( isset( $arrValues['report_sql_detail_id'] ) ) $this->setReportSqlDetailId( $arrValues['report_sql_detail_id'] );
		if( isset( $arrValues['report_spreadsheet_id'] ) && $boolDirectSet ) $this->set( 'm_intReportSpreadsheetId', trim( $arrValues['report_spreadsheet_id'] ) ); elseif( isset( $arrValues['report_spreadsheet_id'] ) ) $this->setReportSpreadsheetId( $arrValues['report_spreadsheet_id'] );
		if( isset( $arrValues['version_number'] ) && $boolDirectSet ) $this->set( 'm_fltVersionNumber', trim( $arrValues['version_number'] ) ); elseif( isset( $arrValues['version_number'] ) ) $this->setVersionNumber( $arrValues['version_number'] );
		if( isset( $arrValues['is_current_version'] ) && $boolDirectSet ) $this->set( 'm_boolIsCurrentVersion', trim( stripcslashes( $arrValues['is_current_version'] ) ) ); elseif( isset( $arrValues['is_current_version'] ) ) $this->setIsCurrentVersion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_current_version'] ) : $arrValues['is_current_version'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCompanyReportId( $intCompanyReportId ) {
		$this->set( 'm_intCompanyReportId', CStrings::strToIntDef( $intCompanyReportId, NULL, false ) );
	}

	public function getCompanyReportId() {
		return $this->m_intCompanyReportId;
	}

	public function sqlCompanyReportId() {
		return ( true == isset( $this->m_intCompanyReportId ) ) ? ( string ) $this->m_intCompanyReportId : 'NULL';
	}

	public function setReportDataTypeId( $intReportDataTypeId ) {
		$this->set( 'm_intReportDataTypeId', CStrings::strToIntDef( $intReportDataTypeId, NULL, false ) );
	}

	public function getReportDataTypeId() {
		return $this->m_intReportDataTypeId;
	}

	public function sqlReportDataTypeId() {
		return ( true == isset( $this->m_intReportDataTypeId ) ) ? ( string ) $this->m_intReportDataTypeId : 'NULL';
	}

	public function setReportSqlDetailId( $intReportSqlDetailId ) {
		$this->set( 'm_intReportSqlDetailId', CStrings::strToIntDef( $intReportSqlDetailId, NULL, false ) );
	}

	public function getReportSqlDetailId() {
		return $this->m_intReportSqlDetailId;
	}

	public function sqlReportSqlDetailId() {
		return ( true == isset( $this->m_intReportSqlDetailId ) ) ? ( string ) $this->m_intReportSqlDetailId : 'NULL';
	}

	public function setReportSpreadsheetId( $intReportSpreadsheetId ) {
		$this->set( 'm_intReportSpreadsheetId', CStrings::strToIntDef( $intReportSpreadsheetId, NULL, false ) );
	}

	public function getReportSpreadsheetId() {
		return $this->m_intReportSpreadsheetId;
	}

	public function sqlReportSpreadsheetId() {
		return ( true == isset( $this->m_intReportSpreadsheetId ) ) ? ( string ) $this->m_intReportSpreadsheetId : 'NULL';
	}

	public function setVersionNumber( $fltVersionNumber ) {
		$this->set( 'm_fltVersionNumber', CStrings::strToFloatDef( $fltVersionNumber, NULL, false, 2 ) );
	}

	public function getVersionNumber() {
		return $this->m_fltVersionNumber;
	}

	public function sqlVersionNumber() {
		return ( true == isset( $this->m_fltVersionNumber ) ) ? ( string ) $this->m_fltVersionNumber : '0';
	}

	public function setIsCurrentVersion( $boolIsCurrentVersion ) {
		$this->set( 'm_boolIsCurrentVersion', CStrings::strToBool( $boolIsCurrentVersion ) );
	}

	public function getIsCurrentVersion() {
		return $this->m_boolIsCurrentVersion;
	}

	public function sqlIsCurrentVersion() {
		return ( true == isset( $this->m_boolIsCurrentVersion ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCurrentVersion ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, company_report_id, report_data_type_id, report_sql_detail_id, report_spreadsheet_id, version_number, is_current_version, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCompanyReportId() . ', ' .
 						$this->sqlReportDataTypeId() . ', ' .
 						$this->sqlReportSqlDetailId() . ', ' .
 						$this->sqlReportSpreadsheetId() . ', ' .
 						$this->sqlVersionNumber() . ', ' .
 						$this->sqlIsCurrentVersion() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_report_id = ' . $this->sqlCompanyReportId() . ','; } elseif( true == array_key_exists( 'CompanyReportId', $this->getChangedColumns() ) ) { $strSql .= ' company_report_id = ' . $this->sqlCompanyReportId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_data_type_id = ' . $this->sqlReportDataTypeId() . ','; } elseif( true == array_key_exists( 'ReportDataTypeId', $this->getChangedColumns() ) ) { $strSql .= ' report_data_type_id = ' . $this->sqlReportDataTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_sql_detail_id = ' . $this->sqlReportSqlDetailId() . ','; } elseif( true == array_key_exists( 'ReportSqlDetailId', $this->getChangedColumns() ) ) { $strSql .= ' report_sql_detail_id = ' . $this->sqlReportSqlDetailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_spreadsheet_id = ' . $this->sqlReportSpreadsheetId() . ','; } elseif( true == array_key_exists( 'ReportSpreadsheetId', $this->getChangedColumns() ) ) { $strSql .= ' report_spreadsheet_id = ' . $this->sqlReportSpreadsheetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' version_number = ' . $this->sqlVersionNumber() . ','; } elseif( true == array_key_exists( 'VersionNumber', $this->getChangedColumns() ) ) { $strSql .= ' version_number = ' . $this->sqlVersionNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_current_version = ' . $this->sqlIsCurrentVersion() . ','; } elseif( true == array_key_exists( 'IsCurrentVersion', $this->getChangedColumns() ) ) { $strSql .= ' is_current_version = ' . $this->sqlIsCurrentVersion() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'company_report_id' => $this->getCompanyReportId(),
			'report_data_type_id' => $this->getReportDataTypeId(),
			'report_sql_detail_id' => $this->getReportSqlDetailId(),
			'report_spreadsheet_id' => $this->getReportSpreadsheetId(),
			'version_number' => $this->getVersionNumber(),
			'is_current_version' => $this->getIsCurrentVersion(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>