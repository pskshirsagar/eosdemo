<?php

class CBaseJobPostingPurchaseRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.job_posting_purchase_requests';

	protected $m_intId;
	protected $m_intPsJobPostingId;
	protected $m_intPurchaseRequestId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_job_posting_id'] ) && $boolDirectSet ) $this->set( 'm_intPsJobPostingId', trim( $arrValues['ps_job_posting_id'] ) ); elseif( isset( $arrValues['ps_job_posting_id'] ) ) $this->setPsJobPostingId( $arrValues['ps_job_posting_id'] );
		if( isset( $arrValues['purchase_request_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchaseRequestId', trim( $arrValues['purchase_request_id'] ) ); elseif( isset( $arrValues['purchase_request_id'] ) ) $this->setPurchaseRequestId( $arrValues['purchase_request_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsJobPostingId( $intPsJobPostingId ) {
		$this->set( 'm_intPsJobPostingId', CStrings::strToIntDef( $intPsJobPostingId, NULL, false ) );
	}

	public function getPsJobPostingId() {
		return $this->m_intPsJobPostingId;
	}

	public function sqlPsJobPostingId() {
		return ( true == isset( $this->m_intPsJobPostingId ) ) ? ( string ) $this->m_intPsJobPostingId : 'NULL';
	}

	public function setPurchaseRequestId( $intPurchaseRequestId ) {
		$this->set( 'm_intPurchaseRequestId', CStrings::strToIntDef( $intPurchaseRequestId, NULL, false ) );
	}

	public function getPurchaseRequestId() {
		return $this->m_intPurchaseRequestId;
	}

	public function sqlPurchaseRequestId() {
		return ( true == isset( $this->m_intPurchaseRequestId ) ) ? ( string ) $this->m_intPurchaseRequestId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_job_posting_id, purchase_request_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsJobPostingId() . ', ' .
 						$this->sqlPurchaseRequestId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_job_posting_id = ' . $this->sqlPsJobPostingId() . ','; } elseif( true == array_key_exists( 'PsJobPostingId', $this->getChangedColumns() ) ) { $strSql .= ' ps_job_posting_id = ' . $this->sqlPsJobPostingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; } elseif( true == array_key_exists( 'PurchaseRequestId', $this->getChangedColumns() ) ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_job_posting_id' => $this->getPsJobPostingId(),
			'purchase_request_id' => $this->getPurchaseRequestId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>