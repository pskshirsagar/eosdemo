<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserAccessLogs
 * Do not add any new functions to this class.
 */

class CBaseUserAccessLogs extends CEosPluralBase {

	/**
	 * @return CUserAccessLog[]
	 */
	public static function fetchUserAccessLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserAccessLog', $objDatabase );
	}

	/**
	 * @return CUserAccessLog
	 */
	public static function fetchUserAccessLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserAccessLog', $objDatabase );
	}

	public static function fetchUserAccessLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_access_logs', $objDatabase );
	}

	public static function fetchUserAccessLogById( $intId, $objDatabase ) {
		return self::fetchUserAccessLog( sprintf( 'SELECT * FROM user_access_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserAccessLogsByUserId( $intUserId, $objDatabase ) {
		return self::fetchUserAccessLogs( sprintf( 'SELECT * FROM user_access_logs WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

	public static function fetchUserAccessLogsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchUserAccessLogs( sprintf( 'SELECT * FROM user_access_logs WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>