<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CIntegrationUtilityReleasedVersions
 * Do not add any new functions to this class.
 */

class CBaseIntegrationUtilityReleasedVersions extends CEosPluralBase {

	/**
	 * @return CIntegrationUtilityReleasedVersion[]
	 */
	public static function fetchIntegrationUtilityReleasedVersions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CIntegrationUtilityReleasedVersion', $objDatabase );
	}

	/**
	 * @return CIntegrationUtilityReleasedVersion
	 */
	public static function fetchIntegrationUtilityReleasedVersion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CIntegrationUtilityReleasedVersion', $objDatabase );
	}

	public static function fetchIntegrationUtilityReleasedVersionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'integration_utility_released_versions', $objDatabase );
	}

	public static function fetchIntegrationUtilityReleasedVersionById( $intId, $objDatabase ) {
		return self::fetchIntegrationUtilityReleasedVersion( sprintf( 'SELECT * FROM integration_utility_released_versions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchIntegrationUtilityReleasedVersionsByIntegrationClientTypeId( $intIntegrationClientTypeId, $objDatabase ) {
		return self::fetchIntegrationUtilityReleasedVersions( sprintf( 'SELECT * FROM integration_utility_released_versions WHERE integration_client_type_id = %d', ( int ) $intIntegrationClientTypeId ), $objDatabase );
	}

}
?>