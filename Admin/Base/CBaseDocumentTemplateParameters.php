<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplateParameters
 * Do not add any new functions to this class.
 */

class CBaseDocumentTemplateParameters extends CEosPluralBase {

	/**
	 * @return CDocumentTemplateParameter[]
	 */
	public static function fetchDocumentTemplateParameters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDocumentTemplateParameter', $objDatabase );
	}

	/**
	 * @return CDocumentTemplateParameter
	 */
	public static function fetchDocumentTemplateParameter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentTemplateParameter', $objDatabase );
	}

	public static function fetchDocumentTemplateParameterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_template_parameters', $objDatabase );
	}

	public static function fetchDocumentTemplateParameterById( $intId, $objDatabase ) {
		return self::fetchDocumentTemplateParameter( sprintf( 'SELECT * FROM document_template_parameters WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDocumentTemplateParametersByDocumentTemplateId( $intDocumentTemplateId, $objDatabase ) {
		return self::fetchDocumentTemplateParameters( sprintf( 'SELECT * FROM document_template_parameters WHERE document_template_id = %d', ( int ) $intDocumentTemplateId ), $objDatabase );
	}

	public static function fetchDocumentTemplateParametersByDocumentTemplateComponentId( $intDocumentTemplateComponentId, $objDatabase ) {
		return self::fetchDocumentTemplateParameters( sprintf( 'SELECT * FROM document_template_parameters WHERE document_template_component_id = %d', ( int ) $intDocumentTemplateComponentId ), $objDatabase );
	}

}
?>