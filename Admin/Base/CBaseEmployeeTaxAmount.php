<?php

class CBaseEmployeeTaxAmount extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_tax_amounts';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intEmployeeDocumentId;
	protected $m_strProposedAmountEncrypted;
	protected $m_strApprovedAmountEncrypted;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['employee_document_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeDocumentId', trim( $arrValues['employee_document_id'] ) ); elseif( isset( $arrValues['employee_document_id'] ) ) $this->setEmployeeDocumentId( $arrValues['employee_document_id'] );
		if( isset( $arrValues['proposed_amount_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strProposedAmountEncrypted', trim( $arrValues['proposed_amount_encrypted'] ) ); elseif( isset( $arrValues['proposed_amount_encrypted'] ) ) $this->setProposedAmountEncrypted( $arrValues['proposed_amount_encrypted'] );
		if( isset( $arrValues['approved_amount_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strApprovedAmountEncrypted', trim( $arrValues['approved_amount_encrypted'] ) ); elseif( isset( $arrValues['approved_amount_encrypted'] ) ) $this->setApprovedAmountEncrypted( $arrValues['approved_amount_encrypted'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setEmployeeDocumentId( $intEmployeeDocumentId ) {
		$this->set( 'm_intEmployeeDocumentId', CStrings::strToIntDef( $intEmployeeDocumentId, NULL, false ) );
	}

	public function getEmployeeDocumentId() {
		return $this->m_intEmployeeDocumentId;
	}

	public function sqlEmployeeDocumentId() {
		return ( true == isset( $this->m_intEmployeeDocumentId ) ) ? ( string ) $this->m_intEmployeeDocumentId : 'NULL';
	}

	public function setProposedAmountEncrypted( $strProposedAmountEncrypted ) {
		if( true == $this->m_boolInitialized ) { 
			$strProposedAmountEncrypted = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strProposedAmountEncrypted, CONFIG_SODIUM_KEY_EMPLOYEE_TAX ); 
		 } 
		$this->set( 'm_strProposedAmountEncrypted', CStrings::strTrimDef( $strProposedAmountEncrypted, 250, NULL, true ) );
	}

	public function getProposedAmountEncrypted() {
		return $this->m_strProposedAmountEncrypted;
	}

	public function sqlProposedAmountEncrypted() {
		return ( true == isset( $this->m_strProposedAmountEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProposedAmountEncrypted ) : '\'' . addslashes( $this->m_strProposedAmountEncrypted ) . '\'' ) : 'NULL';
	}

	public function getDecryptedProposedAmountEncrypted() {
		if( false == valStr( $this->getProposedAmountEncrypted() ) ) {
			return NULL;
		 }
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getProposedAmountEncrypted(),CONFIG_SODIUM_KEY_EMPLOYEE_TAX, ['is_base64_encoded '=> true, 'should_rtrim_nulls' => true ]	);
	}

	public function setApprovedAmountEncrypted( $strApprovedAmountEncrypted ) {
		if( true == $this->m_boolInitialized ) { 
			$strApprovedAmountEncrypted = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strApprovedAmountEncrypted, CONFIG_SODIUM_KEY_EMPLOYEE_TAX ); 
		 } 
		$this->set( 'm_strApprovedAmountEncrypted', CStrings::strTrimDef( $strApprovedAmountEncrypted, 250, NULL, true ) );
	}

	public function getApprovedAmountEncrypted() {
		return $this->m_strApprovedAmountEncrypted;
	}

	public function sqlApprovedAmountEncrypted() {
		return ( true == isset( $this->m_strApprovedAmountEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strApprovedAmountEncrypted ) : '\'' . addslashes( $this->m_strApprovedAmountEncrypted ) . '\'' ) : 'NULL';
	}

	public function getDecryptedApprovedAmountEncrypted() {
		if( false == valStr( $this->getApprovedAmountEncrypted() ) ) {
			return NULL;
		 }
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getApprovedAmountEncrypted(),CONFIG_SODIUM_KEY_EMPLOYEE_TAX, ['is_base64_encoded '=> true, 'should_rtrim_nulls' => true ]	);
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, employee_document_id, proposed_amount_encrypted, approved_amount_encrypted, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlEmployeeDocumentId() . ', ' .
						$this->sqlProposedAmountEncrypted() . ', ' .
						$this->sqlApprovedAmountEncrypted() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_document_id = ' . $this->sqlEmployeeDocumentId(). ',' ; } elseif( true == array_key_exists( 'EmployeeDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' employee_document_id = ' . $this->sqlEmployeeDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposed_amount_encrypted = ' . $this->sqlProposedAmountEncrypted(). ',' ; } elseif( true == array_key_exists( 'ProposedAmountEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' proposed_amount_encrypted = ' . $this->sqlProposedAmountEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_amount_encrypted = ' . $this->sqlApprovedAmountEncrypted(). ',' ; } elseif( true == array_key_exists( 'ApprovedAmountEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' approved_amount_encrypted = ' . $this->sqlApprovedAmountEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'employee_document_id' => $this->getEmployeeDocumentId(),
			'proposed_amount_encrypted' => $this->getProposedAmountEncrypted(),
			'approved_amount_encrypted' => $this->getApprovedAmountEncrypted(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>