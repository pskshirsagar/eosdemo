<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CAgreementTemplates
 * Do not add any new functions to this class.
 */

class CBaseAgreementTemplates extends CEosPluralBase {

	/**
	 * @return CAgreementTemplate[]
	 */
	public static function fetchAgreementTemplates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CAgreementTemplate', $objDatabase );
	}

	/**
	 * @return CAgreementTemplate
	 */
	public static function fetchAgreementTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAgreementTemplate', $objDatabase );
	}

	public static function fetchAgreementTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'agreement_templates', $objDatabase );
	}

	public static function fetchAgreementTemplateById( $intId, $objDatabase ) {
		return self::fetchAgreementTemplate( sprintf( 'SELECT * FROM agreement_templates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchAgreementTemplatesByAgreementTemplateTypeId( $intAgreementTemplateTypeId, $objDatabase ) {
		return self::fetchAgreementTemplates( sprintf( 'SELECT * FROM agreement_templates WHERE agreement_template_type_id = %d', ( int ) $intAgreementTemplateTypeId ), $objDatabase );
	}

	public static function fetchAgreementTemplatesByParentAgreementTemplateId( $intParentAgreementTemplateId, $objDatabase ) {
		return self::fetchAgreementTemplates( sprintf( 'SELECT * FROM agreement_templates WHERE parent_agreement_template_id = %d', ( int ) $intParentAgreementTemplateId ), $objDatabase );
	}

	public static function fetchAgreementTemplatesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchAgreementTemplates( sprintf( 'SELECT * FROM agreement_templates WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>