<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsLeadTermsAgreements
 * Do not add any new functions to this class.
 */

class CBasePsLeadTermsAgreements extends CEosPluralBase {

	/**
	 * @return CPsLeadTermsAgreement[]
	 */
	public static function fetchPsLeadTermsAgreements( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsLeadTermsAgreement', $objDatabase );
	}

	/**
	 * @return CPsLeadTermsAgreement
	 */
	public static function fetchPsLeadTermsAgreement( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsLeadTermsAgreement', $objDatabase );
	}

	public static function fetchPsLeadTermsAgreementCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_lead_terms_agreements', $objDatabase );
	}

	public static function fetchPsLeadTermsAgreementById( $intId, $objDatabase ) {
		return self::fetchPsLeadTermsAgreement( sprintf( 'SELECT * FROM ps_lead_terms_agreements WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsLeadTermsAgreementsByPsLeadId( $intPsLeadId, $objDatabase ) {
		return self::fetchPsLeadTermsAgreements( sprintf( 'SELECT * FROM ps_lead_terms_agreements WHERE ps_lead_id = %d', ( int ) $intPsLeadId ), $objDatabase );
	}

	public static function fetchPsLeadTermsAgreementsByCid( $intCid, $objDatabase ) {
		return self::fetchPsLeadTermsAgreements( sprintf( 'SELECT * FROM ps_lead_terms_agreements WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>