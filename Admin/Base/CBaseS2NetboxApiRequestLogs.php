<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CS2NetboxApiRequestLogs
 * Do not add any new functions to this class.
 */

class CBaseS2NetboxApiRequestLogs extends CEosPluralBase {

	/**
	 * @return CS2NetboxApiRequestLog[]
	 */
	public static function fetchS2NetboxApiRequestLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CS2NetboxApiRequestLog', $objDatabase );
	}

	/**
	 * @return CS2NetboxApiRequestLog
	 */
	public static function fetchS2NetboxApiRequestLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CS2NetboxApiRequestLog', $objDatabase );
	}

	public static function fetchS2NetboxApiRequestLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 's2_netbox_api_request_logs', $objDatabase );
	}

	public static function fetchS2NetboxApiRequestLogById( $intId, $objDatabase ) {
		return self::fetchS2NetboxApiRequestLog( sprintf( 'SELECT * FROM s2_netbox_api_request_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>