<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CIlsServiceRestrictionTypes
 * Do not add any new functions to this class.
 */

class CBaseIlsServiceRestrictionTypes extends CEosPluralBase {

	/**
	 * @return CIlsServiceRestrictionType[]
	 */
	public static function fetchIlsServiceRestrictionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CIlsServiceRestrictionType::class, $objDatabase );
	}

	/**
	 * @return CIlsServiceRestrictionType
	 */
	public static function fetchIlsServiceRestrictionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CIlsServiceRestrictionType::class, $objDatabase );
	}

	public static function fetchIlsServiceRestrictionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ils_service_restriction_types', $objDatabase );
	}

	public static function fetchIlsServiceRestrictionTypeById( $intId, $objDatabase ) {
		return self::fetchIlsServiceRestrictionType( sprintf( 'SELECT * FROM ils_service_restriction_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>