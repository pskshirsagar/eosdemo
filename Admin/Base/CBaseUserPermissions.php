<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserPermissions
 * Do not add any new functions to this class.
 */

class CBaseUserPermissions extends CEosPluralBase {

	/**
	 * @return CUserPermission[]
	 */
	public static function fetchUserPermissions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserPermission', $objDatabase );
	}

	/**
	 * @return CUserPermission
	 */
	public static function fetchUserPermission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserPermission', $objDatabase );
	}

	public static function fetchUserPermissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_permissions', $objDatabase );
	}

	public static function fetchUserPermissionById( $intId, $objDatabase ) {
		return self::fetchUserPermission( sprintf( 'SELECT * FROM user_permissions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserPermissionsByUserId( $intUserId, $objDatabase ) {
		return self::fetchUserPermissions( sprintf( 'SELECT * FROM user_permissions WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

	public static function fetchUserPermissionsByPsModuleId( $intPsModuleId, $objDatabase ) {
		return self::fetchUserPermissions( sprintf( 'SELECT * FROM user_permissions WHERE ps_module_id = %d', ( int ) $intPsModuleId ), $objDatabase );
	}

}
?>