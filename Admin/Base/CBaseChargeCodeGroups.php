<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CChargeCodeGroups
 * Do not add any new functions to this class.
 */

class CBaseChargeCodeGroups extends CEosPluralBase {

	/**
	 * @return CChargeCodeGroup[]
	 */
	public static function fetchChargeCodeGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CChargeCodeGroup', $objDatabase );
	}

	/**
	 * @return CChargeCodeGroup
	 */
	public static function fetchChargeCodeGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CChargeCodeGroup', $objDatabase );
	}

	public static function fetchChargeCodeGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'charge_code_groups', $objDatabase );
	}

	public static function fetchChargeCodeGroupById( $intId, $objDatabase ) {
		return self::fetchChargeCodeGroup( sprintf( 'SELECT * FROM charge_code_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchChargeCodeGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchChargeCodeGroups( sprintf( 'SELECT * FROM charge_code_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChargeCodeGroupsByChargeCodeId( $intChargeCodeId, $objDatabase ) {
		return self::fetchChargeCodeGroups( sprintf( 'SELECT * FROM charge_code_groups WHERE charge_code_id = %d', ( int ) $intChargeCodeId ), $objDatabase );
	}

	public static function fetchChargeCodeGroupsByChargeGroupId( $intChargeGroupId, $objDatabase ) {
		return self::fetchChargeCodeGroups( sprintf( 'SELECT * FROM charge_code_groups WHERE charge_group_id = %d', ( int ) $intChargeGroupId ), $objDatabase );
	}

}
?>