<?php

class CBaseDocumentTemplate extends CEosSingularBase {

	const TABLE_NAME = 'public.document_templates';

	protected $m_intId;
	protected $m_intDocumentTemplateTypeId;
	protected $m_intDocumentTypeId;
	protected $m_intDocumentSubTypeId;
	protected $m_intParentDocumentTemplateId;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_strInstructions;
	protected $m_strKeywords;
	protected $m_strContent;
	protected $m_strLinkUri;
	protected $m_strTarget;
	protected $m_strIconFileName;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_intRequirePrintedForm;
	protected $m_strStateCode;
	protected $m_intUsageCount;
	protected $m_intOrderNum;
	protected $m_intIsArchitect;
	protected $m_intIsArchived;
	protected $m_intIsPublished;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intRequirePrintedForm = '0';
		$this->m_intUsageCount = '0';
		$this->m_intIsArchitect = '1';
		$this->m_intIsArchived = '0';
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['document_template_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentTemplateTypeId', trim( $arrValues['document_template_type_id'] ) ); elseif( isset( $arrValues['document_template_type_id'] ) ) $this->setDocumentTemplateTypeId( $arrValues['document_template_type_id'] );
		if( isset( $arrValues['document_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentTypeId', trim( $arrValues['document_type_id'] ) ); elseif( isset( $arrValues['document_type_id'] ) ) $this->setDocumentTypeId( $arrValues['document_type_id'] );
		if( isset( $arrValues['document_sub_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentSubTypeId', trim( $arrValues['document_sub_type_id'] ) ); elseif( isset( $arrValues['document_sub_type_id'] ) ) $this->setDocumentSubTypeId( $arrValues['document_sub_type_id'] );
		if( isset( $arrValues['parent_document_template_id'] ) && $boolDirectSet ) $this->set( 'm_intParentDocumentTemplateId', trim( $arrValues['parent_document_template_id'] ) ); elseif( isset( $arrValues['parent_document_template_id'] ) ) $this->setParentDocumentTemplateId( $arrValues['parent_document_template_id'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['instructions'] ) && $boolDirectSet ) $this->set( 'm_strInstructions', trim( stripcslashes( $arrValues['instructions'] ) ) ); elseif( isset( $arrValues['instructions'] ) ) $this->setInstructions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['instructions'] ) : $arrValues['instructions'] );
		if( isset( $arrValues['keywords'] ) && $boolDirectSet ) $this->set( 'm_strKeywords', trim( stripcslashes( $arrValues['keywords'] ) ) ); elseif( isset( $arrValues['keywords'] ) ) $this->setKeywords( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['keywords'] ) : $arrValues['keywords'] );
		if( isset( $arrValues['content'] ) && $boolDirectSet ) $this->set( 'm_strContent', trim( stripcslashes( $arrValues['content'] ) ) ); elseif( isset( $arrValues['content'] ) ) $this->setContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['content'] ) : $arrValues['content'] );
		if( isset( $arrValues['link_uri'] ) && $boolDirectSet ) $this->set( 'm_strLinkUri', trim( stripcslashes( $arrValues['link_uri'] ) ) ); elseif( isset( $arrValues['link_uri'] ) ) $this->setLinkUri( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['link_uri'] ) : $arrValues['link_uri'] );
		if( isset( $arrValues['target'] ) && $boolDirectSet ) $this->set( 'm_strTarget', trim( stripcslashes( $arrValues['target'] ) ) ); elseif( isset( $arrValues['target'] ) ) $this->setTarget( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['target'] ) : $arrValues['target'] );
		if( isset( $arrValues['icon_file_name'] ) && $boolDirectSet ) $this->set( 'm_strIconFileName', trim( stripcslashes( $arrValues['icon_file_name'] ) ) ); elseif( isset( $arrValues['icon_file_name'] ) ) $this->setIconFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['icon_file_name'] ) : $arrValues['icon_file_name'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['require_printed_form'] ) && $boolDirectSet ) $this->set( 'm_intRequirePrintedForm', trim( $arrValues['require_printed_form'] ) ); elseif( isset( $arrValues['require_printed_form'] ) ) $this->setRequirePrintedForm( $arrValues['require_printed_form'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['usage_count'] ) && $boolDirectSet ) $this->set( 'm_intUsageCount', trim( $arrValues['usage_count'] ) ); elseif( isset( $arrValues['usage_count'] ) ) $this->setUsageCount( $arrValues['usage_count'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_architect'] ) && $boolDirectSet ) $this->set( 'm_intIsArchitect', trim( $arrValues['is_architect'] ) ); elseif( isset( $arrValues['is_architect'] ) ) $this->setIsArchitect( $arrValues['is_architect'] );
		if( isset( $arrValues['is_archived'] ) && $boolDirectSet ) $this->set( 'm_intIsArchived', trim( $arrValues['is_archived'] ) ); elseif( isset( $arrValues['is_archived'] ) ) $this->setIsArchived( $arrValues['is_archived'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDocumentTemplateTypeId( $intDocumentTemplateTypeId ) {
		$this->set( 'm_intDocumentTemplateTypeId', CStrings::strToIntDef( $intDocumentTemplateTypeId, NULL, false ) );
	}

	public function getDocumentTemplateTypeId() {
		return $this->m_intDocumentTemplateTypeId;
	}

	public function sqlDocumentTemplateTypeId() {
		return ( true == isset( $this->m_intDocumentTemplateTypeId ) ) ? ( string ) $this->m_intDocumentTemplateTypeId : 'NULL';
	}

	public function setDocumentTypeId( $intDocumentTypeId ) {
		$this->set( 'm_intDocumentTypeId', CStrings::strToIntDef( $intDocumentTypeId, NULL, false ) );
	}

	public function getDocumentTypeId() {
		return $this->m_intDocumentTypeId;
	}

	public function sqlDocumentTypeId() {
		return ( true == isset( $this->m_intDocumentTypeId ) ) ? ( string ) $this->m_intDocumentTypeId : 'NULL';
	}

	public function setDocumentSubTypeId( $intDocumentSubTypeId ) {
		$this->set( 'm_intDocumentSubTypeId', CStrings::strToIntDef( $intDocumentSubTypeId, NULL, false ) );
	}

	public function getDocumentSubTypeId() {
		return $this->m_intDocumentSubTypeId;
	}

	public function sqlDocumentSubTypeId() {
		return ( true == isset( $this->m_intDocumentSubTypeId ) ) ? ( string ) $this->m_intDocumentSubTypeId : 'NULL';
	}

	public function setParentDocumentTemplateId( $intParentDocumentTemplateId ) {
		$this->set( 'm_intParentDocumentTemplateId', CStrings::strToIntDef( $intParentDocumentTemplateId, NULL, false ) );
	}

	public function getParentDocumentTemplateId() {
		return $this->m_intParentDocumentTemplateId;
	}

	public function sqlParentDocumentTemplateId() {
		return ( true == isset( $this->m_intParentDocumentTemplateId ) ) ? ( string ) $this->m_intParentDocumentTemplateId : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setInstructions( $strInstructions ) {
		$this->set( 'm_strInstructions', CStrings::strTrimDef( $strInstructions, 2000, NULL, true ) );
	}

	public function getInstructions() {
		return $this->m_strInstructions;
	}

	public function sqlInstructions() {
		return ( true == isset( $this->m_strInstructions ) ) ? '\'' . addslashes( $this->m_strInstructions ) . '\'' : 'NULL';
	}

	public function setKeywords( $strKeywords ) {
		$this->set( 'm_strKeywords', CStrings::strTrimDef( $strKeywords, 2000, NULL, true ) );
	}

	public function getKeywords() {
		return $this->m_strKeywords;
	}

	public function sqlKeywords() {
		return ( true == isset( $this->m_strKeywords ) ) ? '\'' . addslashes( $this->m_strKeywords ) . '\'' : 'NULL';
	}

	public function setContent( $strContent ) {
		$this->set( 'm_strContent', CStrings::strTrimDef( $strContent, -1, NULL, true ) );
	}

	public function getContent() {
		return $this->m_strContent;
	}

	public function sqlContent() {
		return ( true == isset( $this->m_strContent ) ) ? '\'' . addslashes( $this->m_strContent ) . '\'' : 'NULL';
	}

	public function setLinkUri( $strLinkUri ) {
		$this->set( 'm_strLinkUri', CStrings::strTrimDef( $strLinkUri, 4096, NULL, true ) );
	}

	public function getLinkUri() {
		return $this->m_strLinkUri;
	}

	public function sqlLinkUri() {
		return ( true == isset( $this->m_strLinkUri ) ) ? '\'' . addslashes( $this->m_strLinkUri ) . '\'' : 'NULL';
	}

	public function setTarget( $strTarget ) {
		$this->set( 'm_strTarget', CStrings::strTrimDef( $strTarget, 240, NULL, true ) );
	}

	public function getTarget() {
		return $this->m_strTarget;
	}

	public function sqlTarget() {
		return ( true == isset( $this->m_strTarget ) ) ? '\'' . addslashes( $this->m_strTarget ) . '\'' : 'NULL';
	}

	public function setIconFileName( $strIconFileName ) {
		$this->set( 'm_strIconFileName', CStrings::strTrimDef( $strIconFileName, 240, NULL, true ) );
	}

	public function getIconFileName() {
		return $this->m_strIconFileName;
	}

	public function sqlIconFileName() {
		return ( true == isset( $this->m_strIconFileName ) ) ? '\'' . addslashes( $this->m_strIconFileName ) . '\'' : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, -1, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setRequirePrintedForm( $intRequirePrintedForm ) {
		$this->set( 'm_intRequirePrintedForm', CStrings::strToIntDef( $intRequirePrintedForm, NULL, false ) );
	}

	public function getRequirePrintedForm() {
		return $this->m_intRequirePrintedForm;
	}

	public function sqlRequirePrintedForm() {
		return ( true == isset( $this->m_intRequirePrintedForm ) ) ? ( string ) $this->m_intRequirePrintedForm : '0';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setUsageCount( $intUsageCount ) {
		$this->set( 'm_intUsageCount', CStrings::strToIntDef( $intUsageCount, NULL, false ) );
	}

	public function getUsageCount() {
		return $this->m_intUsageCount;
	}

	public function sqlUsageCount() {
		return ( true == isset( $this->m_intUsageCount ) ) ? ( string ) $this->m_intUsageCount : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setIsArchitect( $intIsArchitect ) {
		$this->set( 'm_intIsArchitect', CStrings::strToIntDef( $intIsArchitect, NULL, false ) );
	}

	public function getIsArchitect() {
		return $this->m_intIsArchitect;
	}

	public function sqlIsArchitect() {
		return ( true == isset( $this->m_intIsArchitect ) ) ? ( string ) $this->m_intIsArchitect : '1';
	}

	public function setIsArchived( $intIsArchived ) {
		$this->set( 'm_intIsArchived', CStrings::strToIntDef( $intIsArchived, NULL, false ) );
	}

	public function getIsArchived() {
		return $this->m_intIsArchived;
	}

	public function sqlIsArchived() {
		return ( true == isset( $this->m_intIsArchived ) ) ? ( string ) $this->m_intIsArchived : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, document_template_type_id, document_type_id, document_sub_type_id, parent_document_template_id, title, description, instructions, keywords, content, link_uri, target, icon_file_name, file_name, file_path, require_printed_form, state_code, usage_count, order_num, is_architect, is_archived, is_published, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDocumentTemplateTypeId() . ', ' .
 						$this->sqlDocumentTypeId() . ', ' .
 						$this->sqlDocumentSubTypeId() . ', ' .
 						$this->sqlParentDocumentTemplateId() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlInstructions() . ', ' .
 						$this->sqlKeywords() . ', ' .
 						$this->sqlContent() . ', ' .
 						$this->sqlLinkUri() . ', ' .
 						$this->sqlTarget() . ', ' .
 						$this->sqlIconFileName() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlRequirePrintedForm() . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlUsageCount() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlIsArchitect() . ', ' .
 						$this->sqlIsArchived() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_template_type_id = ' . $this->sqlDocumentTemplateTypeId() . ','; } elseif( true == array_key_exists( 'DocumentTemplateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' document_template_type_id = ' . $this->sqlDocumentTemplateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_type_id = ' . $this->sqlDocumentTypeId() . ','; } elseif( true == array_key_exists( 'DocumentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' document_type_id = ' . $this->sqlDocumentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_sub_type_id = ' . $this->sqlDocumentSubTypeId() . ','; } elseif( true == array_key_exists( 'DocumentSubTypeId', $this->getChangedColumns() ) ) { $strSql .= ' document_sub_type_id = ' . $this->sqlDocumentSubTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_document_template_id = ' . $this->sqlParentDocumentTemplateId() . ','; } elseif( true == array_key_exists( 'ParentDocumentTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' parent_document_template_id = ' . $this->sqlParentDocumentTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instructions = ' . $this->sqlInstructions() . ','; } elseif( true == array_key_exists( 'Instructions', $this->getChangedColumns() ) ) { $strSql .= ' instructions = ' . $this->sqlInstructions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keywords = ' . $this->sqlKeywords() . ','; } elseif( true == array_key_exists( 'Keywords', $this->getChangedColumns() ) ) { $strSql .= ' keywords = ' . $this->sqlKeywords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content = ' . $this->sqlContent() . ','; } elseif( true == array_key_exists( 'Content', $this->getChangedColumns() ) ) { $strSql .= ' content = ' . $this->sqlContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' link_uri = ' . $this->sqlLinkUri() . ','; } elseif( true == array_key_exists( 'LinkUri', $this->getChangedColumns() ) ) { $strSql .= ' link_uri = ' . $this->sqlLinkUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' target = ' . $this->sqlTarget() . ','; } elseif( true == array_key_exists( 'Target', $this->getChangedColumns() ) ) { $strSql .= ' target = ' . $this->sqlTarget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' icon_file_name = ' . $this->sqlIconFileName() . ','; } elseif( true == array_key_exists( 'IconFileName', $this->getChangedColumns() ) ) { $strSql .= ' icon_file_name = ' . $this->sqlIconFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_printed_form = ' . $this->sqlRequirePrintedForm() . ','; } elseif( true == array_key_exists( 'RequirePrintedForm', $this->getChangedColumns() ) ) { $strSql .= ' require_printed_form = ' . $this->sqlRequirePrintedForm() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' usage_count = ' . $this->sqlUsageCount() . ','; } elseif( true == array_key_exists( 'UsageCount', $this->getChangedColumns() ) ) { $strSql .= ' usage_count = ' . $this->sqlUsageCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_architect = ' . $this->sqlIsArchitect() . ','; } elseif( true == array_key_exists( 'IsArchitect', $this->getChangedColumns() ) ) { $strSql .= ' is_architect = ' . $this->sqlIsArchitect() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; } elseif( true == array_key_exists( 'IsArchived', $this->getChangedColumns() ) ) { $strSql .= ' is_archived = ' . $this->sqlIsArchived() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'document_template_type_id' => $this->getDocumentTemplateTypeId(),
			'document_type_id' => $this->getDocumentTypeId(),
			'document_sub_type_id' => $this->getDocumentSubTypeId(),
			'parent_document_template_id' => $this->getParentDocumentTemplateId(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'instructions' => $this->getInstructions(),
			'keywords' => $this->getKeywords(),
			'content' => $this->getContent(),
			'link_uri' => $this->getLinkUri(),
			'target' => $this->getTarget(),
			'icon_file_name' => $this->getIconFileName(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'require_printed_form' => $this->getRequirePrintedForm(),
			'state_code' => $this->getStateCode(),
			'usage_count' => $this->getUsageCount(),
			'order_num' => $this->getOrderNum(),
			'is_architect' => $this->getIsArchitect(),
			'is_archived' => $this->getIsArchived(),
			'is_published' => $this->getIsPublished(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>