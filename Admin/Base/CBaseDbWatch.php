<?php

class CBaseDbWatch extends CEosSingularBase {

	const TABLE_NAME = 'public.db_watches';

	protected $m_intId;
	protected $m_intDbWatchTypeId;
	protected $m_intDatabaseId;
	protected $m_intDbWatchPriorityId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strDbWatchSql;
	protected $m_strDbDataSql;
	protected $m_strErrorConditionSql;
	protected $m_strOnFailSql;
	protected $m_strOnSuccessSql;
	protected $m_strOutputTextIntro;
	protected $m_strOutputTextClosing;
	protected $m_strDataLinkUrl;
	protected $m_strCronSchedule;
	protected $m_intIsDisabled;
	protected $m_intIsManual;
	protected $m_intIsPublished;
	protected $m_intIsCustomRoutine;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intDbWatchPriorityId = '5';
		$this->m_intIsDisabled = '0';
		$this->m_intIsManual = '0';
		$this->m_intIsPublished = '1';
		$this->m_intIsCustomRoutine = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['db_watch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDbWatchTypeId', trim( $arrValues['db_watch_type_id'] ) ); elseif( isset( $arrValues['db_watch_type_id'] ) ) $this->setDbWatchTypeId( $arrValues['db_watch_type_id'] );
		if( isset( $arrValues['database_id'] ) && $boolDirectSet ) $this->set( 'm_intDatabaseId', trim( $arrValues['database_id'] ) ); elseif( isset( $arrValues['database_id'] ) ) $this->setDatabaseId( $arrValues['database_id'] );
		if( isset( $arrValues['db_watch_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intDbWatchPriorityId', trim( $arrValues['db_watch_priority_id'] ) ); elseif( isset( $arrValues['db_watch_priority_id'] ) ) $this->setDbWatchPriorityId( $arrValues['db_watch_priority_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['db_watch_sql'] ) && $boolDirectSet ) $this->set( 'm_strDbWatchSql', trim( stripcslashes( $arrValues['db_watch_sql'] ) ) ); elseif( isset( $arrValues['db_watch_sql'] ) ) $this->setDbWatchSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['db_watch_sql'] ) : $arrValues['db_watch_sql'] );
		if( isset( $arrValues['db_data_sql'] ) && $boolDirectSet ) $this->set( 'm_strDbDataSql', trim( stripcslashes( $arrValues['db_data_sql'] ) ) ); elseif( isset( $arrValues['db_data_sql'] ) ) $this->setDbDataSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['db_data_sql'] ) : $arrValues['db_data_sql'] );
		if( isset( $arrValues['error_condition_sql'] ) && $boolDirectSet ) $this->set( 'm_strErrorConditionSql', trim( stripcslashes( $arrValues['error_condition_sql'] ) ) ); elseif( isset( $arrValues['error_condition_sql'] ) ) $this->setErrorConditionSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['error_condition_sql'] ) : $arrValues['error_condition_sql'] );
		if( isset( $arrValues['on_fail_sql'] ) && $boolDirectSet ) $this->set( 'm_strOnFailSql', trim( stripcslashes( $arrValues['on_fail_sql'] ) ) ); elseif( isset( $arrValues['on_fail_sql'] ) ) $this->setOnFailSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['on_fail_sql'] ) : $arrValues['on_fail_sql'] );
		if( isset( $arrValues['on_success_sql'] ) && $boolDirectSet ) $this->set( 'm_strOnSuccessSql', trim( stripcslashes( $arrValues['on_success_sql'] ) ) ); elseif( isset( $arrValues['on_success_sql'] ) ) $this->setOnSuccessSql( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['on_success_sql'] ) : $arrValues['on_success_sql'] );
		if( isset( $arrValues['output_text_intro'] ) && $boolDirectSet ) $this->set( 'm_strOutputTextIntro', trim( stripcslashes( $arrValues['output_text_intro'] ) ) ); elseif( isset( $arrValues['output_text_intro'] ) ) $this->setOutputTextIntro( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['output_text_intro'] ) : $arrValues['output_text_intro'] );
		if( isset( $arrValues['output_text_closing'] ) && $boolDirectSet ) $this->set( 'm_strOutputTextClosing', trim( stripcslashes( $arrValues['output_text_closing'] ) ) ); elseif( isset( $arrValues['output_text_closing'] ) ) $this->setOutputTextClosing( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['output_text_closing'] ) : $arrValues['output_text_closing'] );
		if( isset( $arrValues['data_link_url'] ) && $boolDirectSet ) $this->set( 'm_strDataLinkUrl', trim( stripcslashes( $arrValues['data_link_url'] ) ) ); elseif( isset( $arrValues['data_link_url'] ) ) $this->setDataLinkUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['data_link_url'] ) : $arrValues['data_link_url'] );
		if( isset( $arrValues['cron_schedule'] ) && $boolDirectSet ) $this->set( 'm_strCronSchedule', trim( stripcslashes( $arrValues['cron_schedule'] ) ) ); elseif( isset( $arrValues['cron_schedule'] ) ) $this->setCronSchedule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cron_schedule'] ) : $arrValues['cron_schedule'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['is_manual'] ) && $boolDirectSet ) $this->set( 'm_intIsManual', trim( $arrValues['is_manual'] ) ); elseif( isset( $arrValues['is_manual'] ) ) $this->setIsManual( $arrValues['is_manual'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_custom_routine'] ) && $boolDirectSet ) $this->set( 'm_intIsCustomRoutine', trim( $arrValues['is_custom_routine'] ) ); elseif( isset( $arrValues['is_custom_routine'] ) ) $this->setIsCustomRoutine( $arrValues['is_custom_routine'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDbWatchTypeId( $intDbWatchTypeId ) {
		$this->set( 'm_intDbWatchTypeId', CStrings::strToIntDef( $intDbWatchTypeId, NULL, false ) );
	}

	public function getDbWatchTypeId() {
		return $this->m_intDbWatchTypeId;
	}

	public function sqlDbWatchTypeId() {
		return ( true == isset( $this->m_intDbWatchTypeId ) ) ? ( string ) $this->m_intDbWatchTypeId : 'NULL';
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->set( 'm_intDatabaseId', CStrings::strToIntDef( $intDatabaseId, NULL, false ) );
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function sqlDatabaseId() {
		return ( true == isset( $this->m_intDatabaseId ) ) ? ( string ) $this->m_intDatabaseId : 'NULL';
	}

	public function setDbWatchPriorityId( $intDbWatchPriorityId ) {
		$this->set( 'm_intDbWatchPriorityId', CStrings::strToIntDef( $intDbWatchPriorityId, NULL, false ) );
	}

	public function getDbWatchPriorityId() {
		return $this->m_intDbWatchPriorityId;
	}

	public function sqlDbWatchPriorityId() {
		return ( true == isset( $this->m_intDbWatchPriorityId ) ) ? ( string ) $this->m_intDbWatchPriorityId : '5';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 2000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDbWatchSql( $strDbWatchSql ) {
		$this->set( 'm_strDbWatchSql', CStrings::strTrimDef( $strDbWatchSql, -1, NULL, true ) );
	}

	public function getDbWatchSql() {
		return $this->m_strDbWatchSql;
	}

	public function sqlDbWatchSql() {
		return ( true == isset( $this->m_strDbWatchSql ) ) ? '\'' . addslashes( $this->m_strDbWatchSql ) . '\'' : 'NULL';
	}

	public function setDbDataSql( $strDbDataSql ) {
		$this->set( 'm_strDbDataSql', CStrings::strTrimDef( $strDbDataSql, -1, NULL, true ) );
	}

	public function getDbDataSql() {
		return $this->m_strDbDataSql;
	}

	public function sqlDbDataSql() {
		return ( true == isset( $this->m_strDbDataSql ) ) ? '\'' . addslashes( $this->m_strDbDataSql ) . '\'' : 'NULL';
	}

	public function setErrorConditionSql( $strErrorConditionSql ) {
		$this->set( 'm_strErrorConditionSql', CStrings::strTrimDef( $strErrorConditionSql, -1, NULL, true ) );
	}

	public function getErrorConditionSql() {
		return $this->m_strErrorConditionSql;
	}

	public function sqlErrorConditionSql() {
		return ( true == isset( $this->m_strErrorConditionSql ) ) ? '\'' . addslashes( $this->m_strErrorConditionSql ) . '\'' : 'NULL';
	}

	public function setOnFailSql( $strOnFailSql ) {
		$this->set( 'm_strOnFailSql', CStrings::strTrimDef( $strOnFailSql, -1, NULL, true ) );
	}

	public function getOnFailSql() {
		return $this->m_strOnFailSql;
	}

	public function sqlOnFailSql() {
		return ( true == isset( $this->m_strOnFailSql ) ) ? '\'' . addslashes( $this->m_strOnFailSql ) . '\'' : 'NULL';
	}

	public function setOnSuccessSql( $strOnSuccessSql ) {
		$this->set( 'm_strOnSuccessSql', CStrings::strTrimDef( $strOnSuccessSql, -1, NULL, true ) );
	}

	public function getOnSuccessSql() {
		return $this->m_strOnSuccessSql;
	}

	public function sqlOnSuccessSql() {
		return ( true == isset( $this->m_strOnSuccessSql ) ) ? '\'' . addslashes( $this->m_strOnSuccessSql ) . '\'' : 'NULL';
	}

	public function setOutputTextIntro( $strOutputTextIntro ) {
		$this->set( 'm_strOutputTextIntro', CStrings::strTrimDef( $strOutputTextIntro, -1, NULL, true ) );
	}

	public function getOutputTextIntro() {
		return $this->m_strOutputTextIntro;
	}

	public function sqlOutputTextIntro() {
		return ( true == isset( $this->m_strOutputTextIntro ) ) ? '\'' . addslashes( $this->m_strOutputTextIntro ) . '\'' : 'NULL';
	}

	public function setOutputTextClosing( $strOutputTextClosing ) {
		$this->set( 'm_strOutputTextClosing', CStrings::strTrimDef( $strOutputTextClosing, -1, NULL, true ) );
	}

	public function getOutputTextClosing() {
		return $this->m_strOutputTextClosing;
	}

	public function sqlOutputTextClosing() {
		return ( true == isset( $this->m_strOutputTextClosing ) ) ? '\'' . addslashes( $this->m_strOutputTextClosing ) . '\'' : 'NULL';
	}

	public function setDataLinkUrl( $strDataLinkUrl ) {
		$this->set( 'm_strDataLinkUrl', CStrings::strTrimDef( $strDataLinkUrl, 240, NULL, true ) );
	}

	public function getDataLinkUrl() {
		return $this->m_strDataLinkUrl;
	}

	public function sqlDataLinkUrl() {
		return ( true == isset( $this->m_strDataLinkUrl ) ) ? '\'' . addslashes( $this->m_strDataLinkUrl ) . '\'' : 'NULL';
	}

	public function setCronSchedule( $strCronSchedule ) {
		$this->set( 'm_strCronSchedule', CStrings::strTrimDef( $strCronSchedule, 255, NULL, true ) );
	}

	public function getCronSchedule() {
		return $this->m_strCronSchedule;
	}

	public function sqlCronSchedule() {
		return ( true == isset( $this->m_strCronSchedule ) ) ? '\'' . addslashes( $this->m_strCronSchedule ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setIsManual( $intIsManual ) {
		$this->set( 'm_intIsManual', CStrings::strToIntDef( $intIsManual, NULL, false ) );
	}

	public function getIsManual() {
		return $this->m_intIsManual;
	}

	public function sqlIsManual() {
		return ( true == isset( $this->m_intIsManual ) ) ? ( string ) $this->m_intIsManual : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsCustomRoutine( $intIsCustomRoutine ) {
		$this->set( 'm_intIsCustomRoutine', CStrings::strToIntDef( $intIsCustomRoutine, NULL, false ) );
	}

	public function getIsCustomRoutine() {
		return $this->m_intIsCustomRoutine;
	}

	public function sqlIsCustomRoutine() {
		return ( true == isset( $this->m_intIsCustomRoutine ) ) ? ( string ) $this->m_intIsCustomRoutine : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, db_watch_type_id, database_id, db_watch_priority_id, name, description, db_watch_sql, db_data_sql, error_condition_sql, on_fail_sql, on_success_sql, output_text_intro, output_text_closing, data_link_url, cron_schedule, is_disabled, is_manual, is_published, is_custom_routine, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDbWatchTypeId() . ', ' .
 						$this->sqlDatabaseId() . ', ' .
 						$this->sqlDbWatchPriorityId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlDbWatchSql() . ', ' .
 						$this->sqlDbDataSql() . ', ' .
 						$this->sqlErrorConditionSql() . ', ' .
 						$this->sqlOnFailSql() . ', ' .
 						$this->sqlOnSuccessSql() . ', ' .
 						$this->sqlOutputTextIntro() . ', ' .
 						$this->sqlOutputTextClosing() . ', ' .
 						$this->sqlDataLinkUrl() . ', ' .
 						$this->sqlCronSchedule() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
 						$this->sqlIsManual() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsCustomRoutine() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' db_watch_type_id = ' . $this->sqlDbWatchTypeId() . ','; } elseif( true == array_key_exists( 'DbWatchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' db_watch_type_id = ' . $this->sqlDbWatchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; } elseif( true == array_key_exists( 'DatabaseId', $this->getChangedColumns() ) ) { $strSql .= ' database_id = ' . $this->sqlDatabaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' db_watch_priority_id = ' . $this->sqlDbWatchPriorityId() . ','; } elseif( true == array_key_exists( 'DbWatchPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' db_watch_priority_id = ' . $this->sqlDbWatchPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' db_watch_sql = ' . $this->sqlDbWatchSql() . ','; } elseif( true == array_key_exists( 'DbWatchSql', $this->getChangedColumns() ) ) { $strSql .= ' db_watch_sql = ' . $this->sqlDbWatchSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' db_data_sql = ' . $this->sqlDbDataSql() . ','; } elseif( true == array_key_exists( 'DbDataSql', $this->getChangedColumns() ) ) { $strSql .= ' db_data_sql = ' . $this->sqlDbDataSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_condition_sql = ' . $this->sqlErrorConditionSql() . ','; } elseif( true == array_key_exists( 'ErrorConditionSql', $this->getChangedColumns() ) ) { $strSql .= ' error_condition_sql = ' . $this->sqlErrorConditionSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' on_fail_sql = ' . $this->sqlOnFailSql() . ','; } elseif( true == array_key_exists( 'OnFailSql', $this->getChangedColumns() ) ) { $strSql .= ' on_fail_sql = ' . $this->sqlOnFailSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' on_success_sql = ' . $this->sqlOnSuccessSql() . ','; } elseif( true == array_key_exists( 'OnSuccessSql', $this->getChangedColumns() ) ) { $strSql .= ' on_success_sql = ' . $this->sqlOnSuccessSql() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' output_text_intro = ' . $this->sqlOutputTextIntro() . ','; } elseif( true == array_key_exists( 'OutputTextIntro', $this->getChangedColumns() ) ) { $strSql .= ' output_text_intro = ' . $this->sqlOutputTextIntro() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' output_text_closing = ' . $this->sqlOutputTextClosing() . ','; } elseif( true == array_key_exists( 'OutputTextClosing', $this->getChangedColumns() ) ) { $strSql .= ' output_text_closing = ' . $this->sqlOutputTextClosing() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_link_url = ' . $this->sqlDataLinkUrl() . ','; } elseif( true == array_key_exists( 'DataLinkUrl', $this->getChangedColumns() ) ) { $strSql .= ' data_link_url = ' . $this->sqlDataLinkUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cron_schedule = ' . $this->sqlCronSchedule() . ','; } elseif( true == array_key_exists( 'CronSchedule', $this->getChangedColumns() ) ) { $strSql .= ' cron_schedule = ' . $this->sqlCronSchedule() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual = ' . $this->sqlIsManual() . ','; } elseif( true == array_key_exists( 'IsManual', $this->getChangedColumns() ) ) { $strSql .= ' is_manual = ' . $this->sqlIsManual() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_custom_routine = ' . $this->sqlIsCustomRoutine() . ','; } elseif( true == array_key_exists( 'IsCustomRoutine', $this->getChangedColumns() ) ) { $strSql .= ' is_custom_routine = ' . $this->sqlIsCustomRoutine() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'db_watch_type_id' => $this->getDbWatchTypeId(),
			'database_id' => $this->getDatabaseId(),
			'db_watch_priority_id' => $this->getDbWatchPriorityId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'db_watch_sql' => $this->getDbWatchSql(),
			'db_data_sql' => $this->getDbDataSql(),
			'error_condition_sql' => $this->getErrorConditionSql(),
			'on_fail_sql' => $this->getOnFailSql(),
			'on_success_sql' => $this->getOnSuccessSql(),
			'output_text_intro' => $this->getOutputTextIntro(),
			'output_text_closing' => $this->getOutputTextClosing(),
			'data_link_url' => $this->getDataLinkUrl(),
			'cron_schedule' => $this->getCronSchedule(),
			'is_disabled' => $this->getIsDisabled(),
			'is_manual' => $this->getIsManual(),
			'is_published' => $this->getIsPublished(),
			'is_custom_routine' => $this->getIsCustomRoutine(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>