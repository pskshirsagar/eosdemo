<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsItLogs
 * Do not add any new functions to this class.
 */

class CBaseStatsItLogs extends CEosPluralBase {

	/**
	 * @return CStatsItLog[]
	 */
	public static function fetchStatsItLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStatsItLog', $objDatabase );
	}

	/**
	 * @return CStatsItLog
	 */
	public static function fetchStatsItLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStatsItLog', $objDatabase );
	}

	public static function fetchStatsItLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stats_it_logs', $objDatabase );
	}

	public static function fetchStatsItLogById( $intId, $objDatabase ) {
		return self::fetchStatsItLog( sprintf( 'SELECT * FROM stats_it_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStatsItLogsByStatsTypeId( $intStatsTypeId, $objDatabase ) {
		return self::fetchStatsItLogs( sprintf( 'SELECT * FROM stats_it_logs WHERE stats_type_id = %d', ( int ) $intStatsTypeId ), $objDatabase );
	}

}
?>