<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsProductModules
 * Do not add any new functions to this class.
 */

class CBasePsProductModules extends CEosPluralBase {

	/**
	 * @return CPsProductModule[]
	 */
	public static function fetchPsProductModules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsProductModule', $objDatabase );
	}

	/**
	 * @return CPsProductModule
	 */
	public static function fetchPsProductModule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsProductModule', $objDatabase );
	}

	public static function fetchPsProductModuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_product_modules', $objDatabase );
	}

	public static function fetchPsProductModuleById( $intId, $objDatabase ) {
		return self::fetchPsProductModule( sprintf( 'SELECT * FROM ps_product_modules WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsProductModulesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchPsProductModules( sprintf( 'SELECT * FROM ps_product_modules WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchPsProductModulesByParentModuleId( $intParentModuleId, $objDatabase ) {
		return self::fetchPsProductModules( sprintf( 'SELECT * FROM ps_product_modules WHERE parent_module_id = %d', ( int ) $intParentModuleId ), $objDatabase );
	}

	public static function fetchPsProductModulesByDevUserId( $intDevUserId, $objDatabase ) {
		return self::fetchPsProductModules( sprintf( 'SELECT * FROM ps_product_modules WHERE dev_user_id = %d', ( int ) $intDevUserId ), $objDatabase );
	}

	public static function fetchPsProductModulesByQaUserId( $intQaUserId, $objDatabase ) {
		return self::fetchPsProductModules( sprintf( 'SELECT * FROM ps_product_modules WHERE qa_user_id = %d', ( int ) $intQaUserId ), $objDatabase );
	}

	public static function fetchPsProductModulesByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchPsProductModules( sprintf( 'SELECT * FROM ps_product_modules WHERE ps_product_option_id = %d', ( int ) $intPsProductOptionId ), $objDatabase );
	}

}
?>