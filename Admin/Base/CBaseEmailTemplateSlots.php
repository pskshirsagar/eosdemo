<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmailTemplateSlots
 * Do not add any new functions to this class.
 */

class CBaseEmailTemplateSlots extends CEosPluralBase {

	/**
	 * @return CEmailTemplateSlot[]
	 */
	public static function fetchEmailTemplateSlots( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmailTemplateSlot', $objDatabase );
	}

	/**
	 * @return CEmailTemplateSlot
	 */
	public static function fetchEmailTemplateSlot( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailTemplateSlot', $objDatabase );
	}

	public static function fetchEmailTemplateSlotCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_template_slots', $objDatabase );
	}

	public static function fetchEmailTemplateSlotById( $intId, $objDatabase ) {
		return self::fetchEmailTemplateSlot( sprintf( 'SELECT * FROM email_template_slots WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmailTemplateSlotsByEmailTemplateId( $intEmailTemplateId, $objDatabase ) {
		return self::fetchEmailTemplateSlots( sprintf( 'SELECT * FROM email_template_slots WHERE email_template_id = %d', ( int ) $intEmailTemplateId ), $objDatabase );
	}

}
?>