<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSmsResponsePhoneNumbers
 * Do not add any new functions to this class.
 */

class CBaseSmsResponsePhoneNumbers extends CEosPluralBase {

	/**
	 * @return CSmsResponsePhoneNumber[]
	 */
	public static function fetchSmsResponsePhoneNumbers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSmsResponsePhoneNumber', $objDatabase );
	}

	/**
	 * @return CSmsResponsePhoneNumber
	 */
	public static function fetchSmsResponsePhoneNumber( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSmsResponsePhoneNumber', $objDatabase );
	}

	public static function fetchSmsResponsePhoneNumberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sms_response_phone_numbers', $objDatabase );
	}

	public static function fetchSmsResponsePhoneNumberById( $intId, $objDatabase ) {
		return self::fetchSmsResponsePhoneNumber( sprintf( 'SELECT * FROM sms_response_phone_numbers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSmsResponsePhoneNumbersByKeywordId( $intKeywordId, $objDatabase ) {
		return self::fetchSmsResponsePhoneNumbers( sprintf( 'SELECT * FROM sms_response_phone_numbers WHERE keyword_id = %d', ( int ) $intKeywordId ), $objDatabase );
	}

}
?>