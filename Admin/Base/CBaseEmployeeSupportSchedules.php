<?php

class CBaseEmployeeSupportSchedules extends CEosPluralBase {

	public static function fetchEmployeeSupportSchedules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeSupportSchedule', $objDatabase );
	}

	public static function fetchEmployeeSupportSchedule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeSupportSchedule', $objDatabase );
	}

	public static function fetchEmployeeSupportScheduleCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_support_schedules', $objDatabase );
	}

	public static function fetchEmployeeSupportScheduleById( $intId, $objDatabase ) {
		return self::fetchEmployeeSupportSchedule( sprintf( 'SELECT * FROM employee_support_schedules WHERE id = %d', (int) $intId ), $objDatabase );
	}

	public static function fetchEmployeeSupportSchedulesByDevEmployeeId( $intDevEmployeeId, $objDatabase ) {
		return self::fetchEmployeeSupportSchedules( sprintf( 'SELECT * FROM employee_support_schedules WHERE dev_employee_id = %d', (int) $intDevEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeSupportSchedulesByQaEmployeeId( $intQaEmployeeId, $objDatabase ) {
		return self::fetchEmployeeSupportSchedules( sprintf( 'SELECT * FROM employee_support_schedules WHERE qa_employee_id = %d', (int) $intQaEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeSupportSchedulesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchEmployeeSupportSchedules( sprintf( 'SELECT * FROM employee_support_schedules WHERE ps_product_id = %d', (int) $intPsProductId ), $objDatabase );
	}

	public static function fetchEmployeeSupportSchedulesByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchEmployeeSupportSchedules( sprintf( 'SELECT * FROM employee_support_schedules WHERE ps_product_option_id = %d', (int) $intPsProductOptionId ), $objDatabase );
	}

}
?>