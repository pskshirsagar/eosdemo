<?php

class CBaseEmployeeDesignationGoal extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_designation_goals';

	protected $m_intId;
	protected $m_intEmployeeGoalTypeId;
	protected $m_intDesignationId;
	protected $m_intEmployeeId;
	protected $m_intCallAgentId;
	protected $m_fltMinNumericValue;
	protected $m_fltMaxNumericValue;
	protected $m_fltMinPercentageValue;
	protected $m_fltMaxPercentageValue;
	protected $m_strNotes;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltMinNumericValue = '0.0';
		$this->m_fltMaxNumericValue = '0.0';
		$this->m_fltMinPercentageValue = '0.0';
		$this->m_fltMaxPercentageValue = '0.0';
		$this->m_strNotes = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_goal_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeGoalTypeId', trim( $arrValues['employee_goal_type_id'] ) ); elseif( isset( $arrValues['employee_goal_type_id'] ) ) $this->setEmployeeGoalTypeId( $arrValues['employee_goal_type_id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['min_numeric_value'] ) && $boolDirectSet ) $this->set( 'm_fltMinNumericValue', trim( $arrValues['min_numeric_value'] ) ); elseif( isset( $arrValues['min_numeric_value'] ) ) $this->setMinNumericValue( $arrValues['min_numeric_value'] );
		if( isset( $arrValues['max_numeric_value'] ) && $boolDirectSet ) $this->set( 'm_fltMaxNumericValue', trim( $arrValues['max_numeric_value'] ) ); elseif( isset( $arrValues['max_numeric_value'] ) ) $this->setMaxNumericValue( $arrValues['max_numeric_value'] );
		if( isset( $arrValues['min_percentage_value'] ) && $boolDirectSet ) $this->set( 'm_fltMinPercentageValue', trim( $arrValues['min_percentage_value'] ) ); elseif( isset( $arrValues['min_percentage_value'] ) ) $this->setMinPercentageValue( $arrValues['min_percentage_value'] );
		if( isset( $arrValues['max_percentage_value'] ) && $boolDirectSet ) $this->set( 'm_fltMaxPercentageValue', trim( $arrValues['max_percentage_value'] ) ); elseif( isset( $arrValues['max_percentage_value'] ) ) $this->setMaxPercentageValue( $arrValues['max_percentage_value'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeGoalTypeId( $intEmployeeGoalTypeId ) {
		$this->set( 'm_intEmployeeGoalTypeId', CStrings::strToIntDef( $intEmployeeGoalTypeId, NULL, false ) );
	}

	public function getEmployeeGoalTypeId() {
		return $this->m_intEmployeeGoalTypeId;
	}

	public function sqlEmployeeGoalTypeId() {
		return ( true == isset( $this->m_intEmployeeGoalTypeId ) ) ? ( string ) $this->m_intEmployeeGoalTypeId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setMinNumericValue( $fltMinNumericValue ) {
		$this->set( 'm_fltMinNumericValue', CStrings::strToFloatDef( $fltMinNumericValue, NULL, false, 1 ) );
	}

	public function getMinNumericValue() {
		return $this->m_fltMinNumericValue;
	}

	public function sqlMinNumericValue() {
		return ( true == isset( $this->m_fltMinNumericValue ) ) ? ( string ) $this->m_fltMinNumericValue : '0.0';
	}

	public function setMaxNumericValue( $fltMaxNumericValue ) {
		$this->set( 'm_fltMaxNumericValue', CStrings::strToFloatDef( $fltMaxNumericValue, NULL, false, 1 ) );
	}

	public function getMaxNumericValue() {
		return $this->m_fltMaxNumericValue;
	}

	public function sqlMaxNumericValue() {
		return ( true == isset( $this->m_fltMaxNumericValue ) ) ? ( string ) $this->m_fltMaxNumericValue : '0.0';
	}

	public function setMinPercentageValue( $fltMinPercentageValue ) {
		$this->set( 'm_fltMinPercentageValue', CStrings::strToFloatDef( $fltMinPercentageValue, NULL, false, 1 ) );
	}

	public function getMinPercentageValue() {
		return $this->m_fltMinPercentageValue;
	}

	public function sqlMinPercentageValue() {
		return ( true == isset( $this->m_fltMinPercentageValue ) ) ? ( string ) $this->m_fltMinPercentageValue : '0.0';
	}

	public function setMaxPercentageValue( $fltMaxPercentageValue ) {
		$this->set( 'm_fltMaxPercentageValue', CStrings::strToFloatDef( $fltMaxPercentageValue, NULL, false, 1 ) );
	}

	public function getMaxPercentageValue() {
		return $this->m_fltMaxPercentageValue;
	}

	public function sqlMaxPercentageValue() {
		return ( true == isset( $this->m_fltMaxPercentageValue ) ) ? ( string ) $this->m_fltMaxPercentageValue : '0.0';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 100, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_goal_type_id, designation_id, employee_id, call_agent_id, min_numeric_value, max_numeric_value, min_percentage_value, max_percentage_value, notes, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeGoalTypeId() . ', ' .
 						$this->sqlDesignationId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlMinNumericValue() . ', ' .
 						$this->sqlMaxNumericValue() . ', ' .
 						$this->sqlMinPercentageValue() . ', ' .
 						$this->sqlMaxPercentageValue() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_goal_type_id = ' . $this->sqlEmployeeGoalTypeId() . ','; } elseif( true == array_key_exists( 'EmployeeGoalTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_goal_type_id = ' . $this->sqlEmployeeGoalTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_numeric_value = ' . $this->sqlMinNumericValue() . ','; } elseif( true == array_key_exists( 'MinNumericValue', $this->getChangedColumns() ) ) { $strSql .= ' min_numeric_value = ' . $this->sqlMinNumericValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_numeric_value = ' . $this->sqlMaxNumericValue() . ','; } elseif( true == array_key_exists( 'MaxNumericValue', $this->getChangedColumns() ) ) { $strSql .= ' max_numeric_value = ' . $this->sqlMaxNumericValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_percentage_value = ' . $this->sqlMinPercentageValue() . ','; } elseif( true == array_key_exists( 'MinPercentageValue', $this->getChangedColumns() ) ) { $strSql .= ' min_percentage_value = ' . $this->sqlMinPercentageValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_percentage_value = ' . $this->sqlMaxPercentageValue() . ','; } elseif( true == array_key_exists( 'MaxPercentageValue', $this->getChangedColumns() ) ) { $strSql .= ' max_percentage_value = ' . $this->sqlMaxPercentageValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_goal_type_id' => $this->getEmployeeGoalTypeId(),
			'designation_id' => $this->getDesignationId(),
			'employee_id' => $this->getEmployeeId(),
			'call_agent_id' => $this->getCallAgentId(),
			'min_numeric_value' => $this->getMinNumericValue(),
			'max_numeric_value' => $this->getMaxNumericValue(),
			'min_percentage_value' => $this->getMinPercentageValue(),
			'max_percentage_value' => $this->getMaxPercentageValue(),
			'notes' => $this->getNotes(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>