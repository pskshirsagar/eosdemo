<?php

class CBasePsDocumentType extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_document_types';

	protected $m_intId;
	protected $m_intPsDocumentTypeId;
	protected $m_strName;
	protected $m_strBaseDirectory;
	protected $m_boolIsMounts;
	protected $m_intIsPublished;
	protected $m_intIsCompany;
	protected $m_intIsEmployee;
	protected $m_intIsSystem;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsMounts = false;
		$this->m_intIsPublished = '1';
		$this->m_intIsCompany = '0';
		$this->m_intIsEmployee = '0';
		$this->m_intIsSystem = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_document_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsDocumentTypeId', trim( $arrValues['ps_document_type_id'] ) ); elseif( isset( $arrValues['ps_document_type_id'] ) ) $this->setPsDocumentTypeId( $arrValues['ps_document_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['base_directory'] ) && $boolDirectSet ) $this->set( 'm_strBaseDirectory', trim( stripcslashes( $arrValues['base_directory'] ) ) ); elseif( isset( $arrValues['base_directory'] ) ) $this->setBaseDirectory( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['base_directory'] ) : $arrValues['base_directory'] );
		if( isset( $arrValues['is_mounts'] ) && $boolDirectSet ) $this->set( 'm_boolIsMounts', trim( stripcslashes( $arrValues['is_mounts'] ) ) ); elseif( isset( $arrValues['is_mounts'] ) ) $this->setIsMounts( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_mounts'] ) : $arrValues['is_mounts'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_company'] ) && $boolDirectSet ) $this->set( 'm_intIsCompany', trim( $arrValues['is_company'] ) ); elseif( isset( $arrValues['is_company'] ) ) $this->setIsCompany( $arrValues['is_company'] );
		if( isset( $arrValues['is_employee'] ) && $boolDirectSet ) $this->set( 'm_intIsEmployee', trim( $arrValues['is_employee'] ) ); elseif( isset( $arrValues['is_employee'] ) ) $this->setIsEmployee( $arrValues['is_employee'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsDocumentTypeId( $intPsDocumentTypeId ) {
		$this->set( 'm_intPsDocumentTypeId', CStrings::strToIntDef( $intPsDocumentTypeId, NULL, false ) );
	}

	public function getPsDocumentTypeId() {
		return $this->m_intPsDocumentTypeId;
	}

	public function sqlPsDocumentTypeId() {
		return ( true == isset( $this->m_intPsDocumentTypeId ) ) ? ( string ) $this->m_intPsDocumentTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setBaseDirectory( $strBaseDirectory ) {
		$this->set( 'm_strBaseDirectory', CStrings::strTrimDef( $strBaseDirectory, 200, NULL, true ) );
	}

	public function getBaseDirectory() {
		return $this->m_strBaseDirectory;
	}

	public function sqlBaseDirectory() {
		return ( true == isset( $this->m_strBaseDirectory ) ) ? '\'' . addslashes( $this->m_strBaseDirectory ) . '\'' : 'NULL';
	}

	public function setIsMounts( $boolIsMounts ) {
		$this->set( 'm_boolIsMounts', CStrings::strToBool( $boolIsMounts ) );
	}

	public function getIsMounts() {
		return $this->m_boolIsMounts;
	}

	public function sqlIsMounts() {
		return ( true == isset( $this->m_boolIsMounts ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMounts ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsCompany( $intIsCompany ) {
		$this->set( 'm_intIsCompany', CStrings::strToIntDef( $intIsCompany, NULL, false ) );
	}

	public function getIsCompany() {
		return $this->m_intIsCompany;
	}

	public function sqlIsCompany() {
		return ( true == isset( $this->m_intIsCompany ) ) ? ( string ) $this->m_intIsCompany : '0';
	}

	public function setIsEmployee( $intIsEmployee ) {
		$this->set( 'm_intIsEmployee', CStrings::strToIntDef( $intIsEmployee, NULL, false ) );
	}

	public function getIsEmployee() {
		return $this->m_intIsEmployee;
	}

	public function sqlIsEmployee() {
		return ( true == isset( $this->m_intIsEmployee ) ) ? ( string ) $this->m_intIsEmployee : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_document_type_id' => $this->getPsDocumentTypeId(),
			'name' => $this->getName(),
			'base_directory' => $this->getBaseDirectory(),
			'is_mounts' => $this->getIsMounts(),
			'is_published' => $this->getIsPublished(),
			'is_company' => $this->getIsCompany(),
			'is_employee' => $this->getIsEmployee(),
			'is_system' => $this->getIsSystem(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>