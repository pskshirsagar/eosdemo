<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAccounts
 * Do not add any new functions to this class.
 */

class CBaseSemAccounts extends CEosPluralBase {

	/**
	 * @return CSemAccount[]
	 */
	public static function fetchSemAccounts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemAccount', $objDatabase );
	}

	/**
	 * @return CSemAccount
	 */
	public static function fetchSemAccount( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemAccount', $objDatabase );
	}

	public static function fetchSemAccountCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_accounts', $objDatabase );
	}

	public static function fetchSemAccountById( $intId, $objDatabase ) {
		return self::fetchSemAccount( sprintf( 'SELECT * FROM sem_accounts WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemAccountsBySemSourceId( $intSemSourceId, $objDatabase ) {
		return self::fetchSemAccounts( sprintf( 'SELECT * FROM sem_accounts WHERE sem_source_id = %d', ( int ) $intSemSourceId ), $objDatabase );
	}

}
?>