<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeDesignationGoals
 * Do not add any new functions to this class.
 */

class CBaseEmployeeDesignationGoals extends CEosPluralBase {

	/**
	 * @return CEmployeeDesignationGoal[]
	 */
	public static function fetchEmployeeDesignationGoals( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeDesignationGoal::class, $objDatabase );
	}

	/**
	 * @return CEmployeeDesignationGoal
	 */
	public static function fetchEmployeeDesignationGoal( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeDesignationGoal::class, $objDatabase );
	}

	public static function fetchEmployeeDesignationGoalCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_designation_goals', $objDatabase );
	}

	public static function fetchEmployeeDesignationGoalById( $intId, $objDatabase ) {
		return self::fetchEmployeeDesignationGoal( sprintf( 'SELECT * FROM employee_designation_goals WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeDesignationGoalsByEmployeeGoalTypeId( $intEmployeeGoalTypeId, $objDatabase ) {
		return self::fetchEmployeeDesignationGoals( sprintf( 'SELECT * FROM employee_designation_goals WHERE employee_goal_type_id = %d', ( int ) $intEmployeeGoalTypeId ), $objDatabase );
	}

	public static function fetchEmployeeDesignationGoalsByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchEmployeeDesignationGoals( sprintf( 'SELECT * FROM employee_designation_goals WHERE designation_id = %d', ( int ) $intDesignationId ), $objDatabase );
	}

	public static function fetchEmployeeDesignationGoalsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeDesignationGoals( sprintf( 'SELECT * FROM employee_designation_goals WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeDesignationGoalsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchEmployeeDesignationGoals( sprintf( 'SELECT * FROM employee_designation_goals WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

}
?>