<?php

class CBaseEmployeeApplicationInterview extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.employee_application_interviews';

	protected $m_intId;
	protected $m_intEmployeeApplicationId;
	protected $m_intInterviewTypeId;
	protected $m_intEmployeeId;
	protected $m_intPsJobPostingStepId;
	protected $m_strGoogleCalendarEventId;
	protected $m_strInterviewLocation;
	protected $m_strNote;
	protected $m_strFeedback;
	protected $m_strScheduledOn;
	protected $m_strInterviewedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_strFeedback = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationId', trim( $arrValues['employee_application_id'] ) ); elseif( isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( isset( $arrValues['interview_type_id'] ) && $boolDirectSet ) $this->set( 'm_intInterviewTypeId', trim( $arrValues['interview_type_id'] ) ); elseif( isset( $arrValues['interview_type_id'] ) ) $this->setInterviewTypeId( $arrValues['interview_type_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['ps_job_posting_step_id'] ) && $boolDirectSet ) $this->set( 'm_intPsJobPostingStepId', trim( $arrValues['ps_job_posting_step_id'] ) ); elseif( isset( $arrValues['ps_job_posting_step_id'] ) ) $this->setPsJobPostingStepId( $arrValues['ps_job_posting_step_id'] );
		if( isset( $arrValues['google_calendar_event_id'] ) && $boolDirectSet ) $this->set( 'm_strGoogleCalendarEventId', trim( stripcslashes( $arrValues['google_calendar_event_id'] ) ) ); elseif( isset( $arrValues['google_calendar_event_id'] ) ) $this->setGoogleCalendarEventId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['google_calendar_event_id'] ) : $arrValues['google_calendar_event_id'] );
		if( isset( $arrValues['interview_location'] ) && $boolDirectSet ) $this->set( 'm_strInterviewLocation', trim( stripcslashes( $arrValues['interview_location'] ) ) ); elseif( isset( $arrValues['interview_location'] ) ) $this->setInterviewLocation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['interview_location'] ) : $arrValues['interview_location'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['feedback'] ) && $boolDirectSet ) $this->set( 'm_strFeedback', trim( stripcslashes( $arrValues['feedback'] ) ) ); elseif( isset( $arrValues['feedback'] ) ) $this->setFeedback( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['feedback'] ) : $arrValues['feedback'] );
		if( isset( $arrValues['scheduled_on'] ) && $boolDirectSet ) $this->set( 'm_strScheduledOn', trim( $arrValues['scheduled_on'] ) ); elseif( isset( $arrValues['scheduled_on'] ) ) $this->setScheduledOn( $arrValues['scheduled_on'] );
		if( isset( $arrValues['interviewed_on'] ) && $boolDirectSet ) $this->set( 'm_strInterviewedOn', trim( $arrValues['interviewed_on'] ) ); elseif( isset( $arrValues['interviewed_on'] ) ) $this->setInterviewedOn( $arrValues['interviewed_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeApplicationId( $intEmployeeApplicationId ) {
		$this->set( 'm_intEmployeeApplicationId', CStrings::strToIntDef( $intEmployeeApplicationId, NULL, false ) );
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function sqlEmployeeApplicationId() {
		return ( true == isset( $this->m_intEmployeeApplicationId ) ) ? ( string ) $this->m_intEmployeeApplicationId : 'NULL';
	}

	public function setInterviewTypeId( $intInterviewTypeId ) {
		$this->set( 'm_intInterviewTypeId', CStrings::strToIntDef( $intInterviewTypeId, NULL, false ) );
	}

	public function getInterviewTypeId() {
		return $this->m_intInterviewTypeId;
	}

	public function sqlInterviewTypeId() {
		return ( true == isset( $this->m_intInterviewTypeId ) ) ? ( string ) $this->m_intInterviewTypeId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setPsJobPostingStepId( $intPsJobPostingStepId ) {
		$this->set( 'm_intPsJobPostingStepId', CStrings::strToIntDef( $intPsJobPostingStepId, NULL, false ) );
	}

	public function getPsJobPostingStepId() {
		return $this->m_intPsJobPostingStepId;
	}

	public function sqlPsJobPostingStepId() {
		return ( true == isset( $this->m_intPsJobPostingStepId ) ) ? ( string ) $this->m_intPsJobPostingStepId : 'NULL';
	}

	public function setGoogleCalendarEventId( $strGoogleCalendarEventId ) {
		$this->set( 'm_strGoogleCalendarEventId', CStrings::strTrimDef( $strGoogleCalendarEventId, 100, NULL, true ) );
	}

	public function getGoogleCalendarEventId() {
		return $this->m_strGoogleCalendarEventId;
	}

	public function sqlGoogleCalendarEventId() {
		return ( true == isset( $this->m_strGoogleCalendarEventId ) ) ? '\'' . addslashes( $this->m_strGoogleCalendarEventId ) . '\'' : 'NULL';
	}

	public function setInterviewLocation( $strInterviewLocation ) {
		$this->set( 'm_strInterviewLocation', CStrings::strTrimDef( $strInterviewLocation, 100, NULL, true ) );
	}

	public function getInterviewLocation() {
		return $this->m_strInterviewLocation;
	}

	public function sqlInterviewLocation() {
		return ( true == isset( $this->m_strInterviewLocation ) ) ? '\'' . addslashes( $this->m_strInterviewLocation ) . '\'' : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, -1, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setFeedback( $strFeedback ) {
		$this->set( 'm_strFeedback', CStrings::strTrimDef( $strFeedback, 2000, NULL, true ) );
	}

	public function getFeedback() {
		return $this->m_strFeedback;
	}

	public function sqlFeedback() {
		return ( true == isset( $this->m_strFeedback ) ) ? '\'' . addslashes( $this->m_strFeedback ) . '\'' : '\'NULL\'';
	}

	public function setScheduledOn( $strScheduledOn ) {
		$this->set( 'm_strScheduledOn', CStrings::strTrimDef( $strScheduledOn, -1, NULL, true ) );
	}

	public function getScheduledOn() {
		return $this->m_strScheduledOn;
	}

	public function sqlScheduledOn() {
		return ( true == isset( $this->m_strScheduledOn ) ) ? '\'' . $this->m_strScheduledOn . '\'' : 'NULL';
	}

	public function setInterviewedOn( $strInterviewedOn ) {
		$this->set( 'm_strInterviewedOn', CStrings::strTrimDef( $strInterviewedOn, -1, NULL, true ) );
	}

	public function getInterviewedOn() {
		return $this->m_strInterviewedOn;
	}

	public function sqlInterviewedOn() {
		return ( true == isset( $this->m_strInterviewedOn ) ) ? '\'' . $this->m_strInterviewedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_application_id, interview_type_id, employee_id, ps_job_posting_step_id, google_calendar_event_id, interview_location, note, feedback, scheduled_on, interviewed_on, created_by, created_on, updated_by, updated_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeApplicationId() . ', ' .
						$this->sqlInterviewTypeId() . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlPsJobPostingStepId() . ', ' .
						$this->sqlGoogleCalendarEventId() . ', ' .
						$this->sqlInterviewLocation() . ', ' .
						$this->sqlNote() . ', ' .
						$this->sqlFeedback() . ', ' .
						$this->sqlScheduledOn() . ', ' .
						$this->sqlInterviewedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId(). ',' ; } elseif( true == array_key_exists( 'EmployeeApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interview_type_id = ' . $this->sqlInterviewTypeId(). ',' ; } elseif( true == array_key_exists( 'InterviewTypeId', $this->getChangedColumns() ) ) { $strSql .= ' interview_type_id = ' . $this->sqlInterviewTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_job_posting_step_id = ' . $this->sqlPsJobPostingStepId(). ',' ; } elseif( true == array_key_exists( 'PsJobPostingStepId', $this->getChangedColumns() ) ) { $strSql .= ' ps_job_posting_step_id = ' . $this->sqlPsJobPostingStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' google_calendar_event_id = ' . $this->sqlGoogleCalendarEventId(). ',' ; } elseif( true == array_key_exists( 'GoogleCalendarEventId', $this->getChangedColumns() ) ) { $strSql .= ' google_calendar_event_id = ' . $this->sqlGoogleCalendarEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interview_location = ' . $this->sqlInterviewLocation(). ',' ; } elseif( true == array_key_exists( 'InterviewLocation', $this->getChangedColumns() ) ) { $strSql .= ' interview_location = ' . $this->sqlInterviewLocation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote(). ',' ; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' feedback = ' . $this->sqlFeedback(). ',' ; } elseif( true == array_key_exists( 'Feedback', $this->getChangedColumns() ) ) { $strSql .= ' feedback = ' . $this->sqlFeedback() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn(). ',' ; } elseif( true == array_key_exists( 'ScheduledOn', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interviewed_on = ' . $this->sqlInterviewedOn(). ',' ; } elseif( true == array_key_exists( 'InterviewedOn', $this->getChangedColumns() ) ) { $strSql .= ' interviewed_on = ' . $this->sqlInterviewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_application_id' => $this->getEmployeeApplicationId(),
			'interview_type_id' => $this->getInterviewTypeId(),
			'employee_id' => $this->getEmployeeId(),
			'ps_job_posting_step_id' => $this->getPsJobPostingStepId(),
			'google_calendar_event_id' => $this->getGoogleCalendarEventId(),
			'interview_location' => $this->getInterviewLocation(),
			'note' => $this->getNote(),
			'feedback' => $this->getFeedback(),
			'scheduled_on' => $this->getScheduledOn(),
			'interviewed_on' => $this->getInterviewedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>