<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainings
 * Do not add any new functions to this class.
 */

class CBaseTrainings extends CEosPluralBase {

	/**
	 * @return CTraining[]
	 */
	public static function fetchTrainings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTraining', $objDatabase );
	}

	/**
	 * @return CTraining
	 */
	public static function fetchTraining( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTraining', $objDatabase );
	}

	public static function fetchTrainingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'trainings', $objDatabase );
	}

	public static function fetchTrainingById( $intId, $objDatabase ) {
		return self::fetchTraining( sprintf( 'SELECT * FROM trainings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTrainingsByParentId( $intParentId, $objDatabase ) {
		return self::fetchTrainings( sprintf( 'SELECT * FROM trainings WHERE parent_id = %d', ( int ) $intParentId ), $objDatabase );
	}

	public static function fetchTrainingsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchTrainings( sprintf( 'SELECT * FROM trainings WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>