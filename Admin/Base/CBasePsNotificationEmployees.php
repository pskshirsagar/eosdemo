<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsNotificationEmployees
 * Do not add any new functions to this class.
 */

class CBasePsNotificationEmployees extends CEosPluralBase {

	/**
	 * @return CPsNotificationEmployee[]
	 */
	public static function fetchPsNotificationEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsNotificationEmployee', $objDatabase );
	}

	/**
	 * @return CPsNotificationEmployee
	 */
	public static function fetchPsNotificationEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsNotificationEmployee', $objDatabase );
	}

	public static function fetchPsNotificationEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_notification_employees', $objDatabase );
	}

	public static function fetchPsNotificationEmployeeById( $intId, $objDatabase ) {
		return self::fetchPsNotificationEmployee( sprintf( 'SELECT * FROM ps_notification_employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsNotificationEmployeesByPsNotificationId( $intPsNotificationId, $objDatabase ) {
		return self::fetchPsNotificationEmployees( sprintf( 'SELECT * FROM ps_notification_employees WHERE ps_notification_id = %d', ( int ) $intPsNotificationId ), $objDatabase );
	}

	public static function fetchPsNotificationEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchPsNotificationEmployees( sprintf( 'SELECT * FROM ps_notification_employees WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>