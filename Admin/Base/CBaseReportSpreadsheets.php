<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportSpreadsheets
 * Do not add any new functions to this class.
 */

class CBaseReportSpreadsheets extends CEosPluralBase {

	/**
	 * @return CReportSpreadsheet[]
	 */
	public static function fetchReportSpreadsheets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportSpreadsheet::class, $objDatabase );
	}

	/**
	 * @return CReportSpreadsheet
	 */
	public static function fetchReportSpreadsheet( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportSpreadsheet::class, $objDatabase );
	}

	public static function fetchReportSpreadsheetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_spreadsheets', $objDatabase );
	}

	public static function fetchReportSpreadsheetById( $intId, $objDatabase ) {
		return self::fetchReportSpreadsheet( sprintf( 'SELECT * FROM report_spreadsheets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportSpreadsheetsByPsDocumentId( $intPsDocumentId, $objDatabase ) {
		return self::fetchReportSpreadsheets( sprintf( 'SELECT * FROM report_spreadsheets WHERE ps_document_id = %d', ( int ) $intPsDocumentId ), $objDatabase );
	}

	public static function fetchReportSpreadsheetsByCompanyReportId( $intCompanyReportId, $objDatabase ) {
		return self::fetchReportSpreadsheets( sprintf( 'SELECT * FROM report_spreadsheets WHERE company_report_id = %d', ( int ) $intCompanyReportId ), $objDatabase );
	}

}
?>