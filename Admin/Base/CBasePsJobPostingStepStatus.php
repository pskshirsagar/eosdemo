<?php

class CBasePsJobPostingStepStatus extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_job_posting_step_statuses';

	protected $m_intId;
	protected $m_intPsJobPostingStepId;
	protected $m_intPsJobPostingStatusId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_job_posting_step_id'] ) && $boolDirectSet ) $this->set( 'm_intPsJobPostingStepId', trim( $arrValues['ps_job_posting_step_id'] ) ); elseif( isset( $arrValues['ps_job_posting_step_id'] ) ) $this->setPsJobPostingStepId( $arrValues['ps_job_posting_step_id'] );
		if( isset( $arrValues['ps_job_posting_status_id'] ) && $boolDirectSet ) $this->set( 'm_intPsJobPostingStatusId', trim( $arrValues['ps_job_posting_status_id'] ) ); elseif( isset( $arrValues['ps_job_posting_status_id'] ) ) $this->setPsJobPostingStatusId( $arrValues['ps_job_posting_status_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsJobPostingStepId( $intPsJobPostingStepId ) {
		$this->set( 'm_intPsJobPostingStepId', CStrings::strToIntDef( $intPsJobPostingStepId, NULL, false ) );
	}

	public function getPsJobPostingStepId() {
		return $this->m_intPsJobPostingStepId;
	}

	public function sqlPsJobPostingStepId() {
		return ( true == isset( $this->m_intPsJobPostingStepId ) ) ? ( string ) $this->m_intPsJobPostingStepId : 'NULL';
	}

	public function setPsJobPostingStatusId( $intPsJobPostingStatusId ) {
		$this->set( 'm_intPsJobPostingStatusId', CStrings::strToIntDef( $intPsJobPostingStatusId, NULL, false ) );
	}

	public function getPsJobPostingStatusId() {
		return $this->m_intPsJobPostingStatusId;
	}

	public function sqlPsJobPostingStatusId() {
		return ( true == isset( $this->m_intPsJobPostingStatusId ) ) ? ( string ) $this->m_intPsJobPostingStatusId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_job_posting_step_id, ps_job_posting_status_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsJobPostingStepId() . ', ' .
 						$this->sqlPsJobPostingStatusId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_job_posting_step_id = ' . $this->sqlPsJobPostingStepId() . ','; } elseif( true == array_key_exists( 'PsJobPostingStepId', $this->getChangedColumns() ) ) { $strSql .= ' ps_job_posting_step_id = ' . $this->sqlPsJobPostingStepId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_job_posting_status_id = ' . $this->sqlPsJobPostingStatusId() . ','; } elseif( true == array_key_exists( 'PsJobPostingStatusId', $this->getChangedColumns() ) ) { $strSql .= ' ps_job_posting_status_id = ' . $this->sqlPsJobPostingStatusId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_job_posting_step_id' => $this->getPsJobPostingStepId(),
			'ps_job_posting_status_id' => $this->getPsJobPostingStatusId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>