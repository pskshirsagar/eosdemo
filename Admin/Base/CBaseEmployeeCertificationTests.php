<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeCertificationTests
 * Do not add any new functions to this class.
 */

class CBaseEmployeeCertificationTests extends CEosPluralBase {

	/**
	 * @return CEmployeeCertificationTest[]
	 */
	public static function fetchEmployeeCertificationTests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeCertificationTest', $objDatabase );
	}

	/**
	 * @return CEmployeeCertificationTest
	 */
	public static function fetchEmployeeCertificationTest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeCertificationTest', $objDatabase );
	}

	public static function fetchEmployeeCertificationTestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_certification_tests', $objDatabase );
	}

	public static function fetchEmployeeCertificationTestById( $intId, $objDatabase ) {
		return self::fetchEmployeeCertificationTest( sprintf( 'SELECT * FROM employee_certification_tests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeCertificationTestsByEmployeeCertificationId( $intEmployeeCertificationId, $objDatabase ) {
		return self::fetchEmployeeCertificationTests( sprintf( 'SELECT * FROM employee_certification_tests WHERE employee_certification_id = %d', ( int ) $intEmployeeCertificationId ), $objDatabase );
	}

	public static function fetchEmployeeCertificationTestsByCertificationTestId( $intCertificationTestId, $objDatabase ) {
		return self::fetchEmployeeCertificationTests( sprintf( 'SELECT * FROM employee_certification_tests WHERE certification_test_id = %d', ( int ) $intCertificationTestId ), $objDatabase );
	}

}
?>