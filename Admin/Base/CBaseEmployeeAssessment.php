<?php

class CBaseEmployeeAssessment extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_assessments';

	protected $m_intId;
	protected $m_intManagerEmployeeId;
	protected $m_intEmployeeId;
	protected $m_intEmployeeAssessmentTypeId;
	protected $m_intDepartmentId;
	protected $m_strAssessmentDatetime;
	protected $m_strCurrentTitle;
	protected $m_strAssessmentStartDate;
	protected $m_strAssessmentEndDate;
	protected $m_strCurrentSalary;
	protected $m_strRecommendedNewSalary;
	protected $m_strActualNewSalary;
	protected $m_strSalaryNotes;
	protected $m_strAcceptedOn;
	protected $m_strCompletedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intPsDocumentId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['manager_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intManagerEmployeeId', trim( $arrValues['manager_employee_id'] ) ); elseif( isset( $arrValues['manager_employee_id'] ) ) $this->setManagerEmployeeId( $arrValues['manager_employee_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['employee_assessment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeAssessmentTypeId', trim( $arrValues['employee_assessment_type_id'] ) ); elseif( isset( $arrValues['employee_assessment_type_id'] ) ) $this->setEmployeeAssessmentTypeId( $arrValues['employee_assessment_type_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['assessment_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAssessmentDatetime', trim( $arrValues['assessment_datetime'] ) ); elseif( isset( $arrValues['assessment_datetime'] ) ) $this->setAssessmentDatetime( $arrValues['assessment_datetime'] );
		if( isset( $arrValues['current_title'] ) && $boolDirectSet ) $this->set( 'm_strCurrentTitle', trim( stripcslashes( $arrValues['current_title'] ) ) ); elseif( isset( $arrValues['current_title'] ) ) $this->setCurrentTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['current_title'] ) : $arrValues['current_title'] );
		if( isset( $arrValues['assessment_start_date'] ) && $boolDirectSet ) $this->set( 'm_strAssessmentStartDate', trim( $arrValues['assessment_start_date'] ) ); elseif( isset( $arrValues['assessment_start_date'] ) ) $this->setAssessmentStartDate( $arrValues['assessment_start_date'] );
		if( isset( $arrValues['assessment_end_date'] ) && $boolDirectSet ) $this->set( 'm_strAssessmentEndDate', trim( $arrValues['assessment_end_date'] ) ); elseif( isset( $arrValues['assessment_end_date'] ) ) $this->setAssessmentEndDate( $arrValues['assessment_end_date'] );
		if( isset( $arrValues['current_salary'] ) && $boolDirectSet ) $this->set( 'm_strCurrentSalary', trim( stripcslashes( $arrValues['current_salary'] ) ) ); elseif( isset( $arrValues['current_salary'] ) ) $this->setCurrentSalary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['current_salary'] ) : $arrValues['current_salary'] );
		if( isset( $arrValues['recommended_new_salary'] ) && $boolDirectSet ) $this->set( 'm_strRecommendedNewSalary', trim( stripcslashes( $arrValues['recommended_new_salary'] ) ) ); elseif( isset( $arrValues['recommended_new_salary'] ) ) $this->setRecommendedNewSalary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['recommended_new_salary'] ) : $arrValues['recommended_new_salary'] );
		if( isset( $arrValues['actual_new_salary'] ) && $boolDirectSet ) $this->set( 'm_strActualNewSalary', trim( stripcslashes( $arrValues['actual_new_salary'] ) ) ); elseif( isset( $arrValues['actual_new_salary'] ) ) $this->setActualNewSalary( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['actual_new_salary'] ) : $arrValues['actual_new_salary'] );
		if( isset( $arrValues['salary_notes'] ) && $boolDirectSet ) $this->set( 'm_strSalaryNotes', trim( stripcslashes( $arrValues['salary_notes'] ) ) ); elseif( isset( $arrValues['salary_notes'] ) ) $this->setSalaryNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['salary_notes'] ) : $arrValues['salary_notes'] );
		if( isset( $arrValues['accepted_on'] ) && $boolDirectSet ) $this->set( 'm_strAcceptedOn', trim( $arrValues['accepted_on'] ) ); elseif( isset( $arrValues['accepted_on'] ) ) $this->setAcceptedOn( $arrValues['accepted_on'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ps_document_id'] ) && $boolDirectSet ) $this->set( 'm_intPsDocumentId', trim( $arrValues['ps_document_id'] ) ); elseif( isset( $arrValues['ps_document_id'] ) ) $this->setPsDocumentId( $arrValues['ps_document_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setManagerEmployeeId( $intManagerEmployeeId ) {
		$this->set( 'm_intManagerEmployeeId', CStrings::strToIntDef( $intManagerEmployeeId, NULL, false ) );
	}

	public function getManagerEmployeeId() {
		return $this->m_intManagerEmployeeId;
	}

	public function sqlManagerEmployeeId() {
		return ( true == isset( $this->m_intManagerEmployeeId ) ) ? ( string ) $this->m_intManagerEmployeeId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setEmployeeAssessmentTypeId( $intEmployeeAssessmentTypeId ) {
		$this->set( 'm_intEmployeeAssessmentTypeId', CStrings::strToIntDef( $intEmployeeAssessmentTypeId, NULL, false ) );
	}

	public function getEmployeeAssessmentTypeId() {
		return $this->m_intEmployeeAssessmentTypeId;
	}

	public function sqlEmployeeAssessmentTypeId() {
		return ( true == isset( $this->m_intEmployeeAssessmentTypeId ) ) ? ( string ) $this->m_intEmployeeAssessmentTypeId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setAssessmentDatetime( $strAssessmentDatetime ) {
		$this->set( 'm_strAssessmentDatetime', CStrings::strTrimDef( $strAssessmentDatetime, -1, NULL, true ) );
	}

	public function getAssessmentDatetime() {
		return $this->m_strAssessmentDatetime;
	}

	public function sqlAssessmentDatetime() {
		return ( true == isset( $this->m_strAssessmentDatetime ) ) ? '\'' . $this->m_strAssessmentDatetime . '\'' : 'NULL';
	}

	public function setCurrentTitle( $strCurrentTitle ) {
		$this->set( 'm_strCurrentTitle', CStrings::strTrimDef( $strCurrentTitle, 50, NULL, true ) );
	}

	public function getCurrentTitle() {
		return $this->m_strCurrentTitle;
	}

	public function sqlCurrentTitle() {
		return ( true == isset( $this->m_strCurrentTitle ) ) ? '\'' . addslashes( $this->m_strCurrentTitle ) . '\'' : 'NULL';
	}

	public function setAssessmentStartDate( $strAssessmentStartDate ) {
		$this->set( 'm_strAssessmentStartDate', CStrings::strTrimDef( $strAssessmentStartDate, -1, NULL, true ) );
	}

	public function getAssessmentStartDate() {
		return $this->m_strAssessmentStartDate;
	}

	public function sqlAssessmentStartDate() {
		return ( true == isset( $this->m_strAssessmentStartDate ) ) ? '\'' . $this->m_strAssessmentStartDate . '\'' : 'NULL';
	}

	public function setAssessmentEndDate( $strAssessmentEndDate ) {
		$this->set( 'm_strAssessmentEndDate', CStrings::strTrimDef( $strAssessmentEndDate, -1, NULL, true ) );
	}

	public function getAssessmentEndDate() {
		return $this->m_strAssessmentEndDate;
	}

	public function sqlAssessmentEndDate() {
		return ( true == isset( $this->m_strAssessmentEndDate ) ) ? '\'' . $this->m_strAssessmentEndDate . '\'' : 'NULL';
	}

	public function setCurrentSalary( $strCurrentSalary ) {
		$this->set( 'm_strCurrentSalary', CStrings::strTrimDef( $strCurrentSalary, 240, NULL, true ) );
	}

	public function getCurrentSalary() {
		return $this->m_strCurrentSalary;
	}

	public function sqlCurrentSalary() {
		return ( true == isset( $this->m_strCurrentSalary ) ) ? '\'' . addslashes( $this->m_strCurrentSalary ) . '\'' : 'NULL';
	}

	public function setRecommendedNewSalary( $strRecommendedNewSalary ) {
		$this->set( 'm_strRecommendedNewSalary', CStrings::strTrimDef( $strRecommendedNewSalary, 240, NULL, true ) );
	}

	public function getRecommendedNewSalary() {
		return $this->m_strRecommendedNewSalary;
	}

	public function sqlRecommendedNewSalary() {
		return ( true == isset( $this->m_strRecommendedNewSalary ) ) ? '\'' . addslashes( $this->m_strRecommendedNewSalary ) . '\'' : 'NULL';
	}

	public function setActualNewSalary( $strActualNewSalary ) {
		$this->set( 'm_strActualNewSalary', CStrings::strTrimDef( $strActualNewSalary, 240, NULL, true ) );
	}

	public function getActualNewSalary() {
		return $this->m_strActualNewSalary;
	}

	public function sqlActualNewSalary() {
		return ( true == isset( $this->m_strActualNewSalary ) ) ? '\'' . addslashes( $this->m_strActualNewSalary ) . '\'' : 'NULL';
	}

	public function setSalaryNotes( $strSalaryNotes ) {
		$this->set( 'm_strSalaryNotes', CStrings::strTrimDef( $strSalaryNotes, 2000, NULL, true ) );
	}

	public function getSalaryNotes() {
		return $this->m_strSalaryNotes;
	}

	public function sqlSalaryNotes() {
		return ( true == isset( $this->m_strSalaryNotes ) ) ? '\'' . addslashes( $this->m_strSalaryNotes ) . '\'' : 'NULL';
	}

	public function setAcceptedOn( $strAcceptedOn ) {
		$this->set( 'm_strAcceptedOn', CStrings::strTrimDef( $strAcceptedOn, -1, NULL, true ) );
	}

	public function getAcceptedOn() {
		return $this->m_strAcceptedOn;
	}

	public function sqlAcceptedOn() {
		return ( true == isset( $this->m_strAcceptedOn ) ) ? '\'' . $this->m_strAcceptedOn . '\'' : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPsDocumentId( $intPsDocumentId ) {
		$this->set( 'm_intPsDocumentId', CStrings::strToIntDef( $intPsDocumentId, NULL, false ) );
	}

	public function getPsDocumentId() {
		return $this->m_intPsDocumentId;
	}

	public function sqlPsDocumentId() {
		return ( true == isset( $this->m_intPsDocumentId ) ) ? ( string ) $this->m_intPsDocumentId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, manager_employee_id, employee_id, employee_assessment_type_id, department_id, assessment_datetime, current_title, assessment_start_date, assessment_end_date, current_salary, recommended_new_salary, actual_new_salary, salary_notes, accepted_on, completed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, ps_document_id )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlManagerEmployeeId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlEmployeeAssessmentTypeId() . ', ' .
 						$this->sqlDepartmentId() . ', ' .
 						$this->sqlAssessmentDatetime() . ', ' .
 						$this->sqlCurrentTitle() . ', ' .
 						$this->sqlAssessmentStartDate() . ', ' .
 						$this->sqlAssessmentEndDate() . ', ' .
 						$this->sqlCurrentSalary() . ', ' .
 						$this->sqlRecommendedNewSalary() . ', ' .
 						$this->sqlActualNewSalary() . ', ' .
 						$this->sqlSalaryNotes() . ', ' .
 						$this->sqlAcceptedOn() . ', ' .
 						$this->sqlCompletedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlPsDocumentId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manager_employee_id = ' . $this->sqlManagerEmployeeId() . ','; } elseif( true == array_key_exists( 'ManagerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' manager_employee_id = ' . $this->sqlManagerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_assessment_type_id = ' . $this->sqlEmployeeAssessmentTypeId() . ','; } elseif( true == array_key_exists( 'EmployeeAssessmentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_assessment_type_id = ' . $this->sqlEmployeeAssessmentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assessment_datetime = ' . $this->sqlAssessmentDatetime() . ','; } elseif( true == array_key_exists( 'AssessmentDatetime', $this->getChangedColumns() ) ) { $strSql .= ' assessment_datetime = ' . $this->sqlAssessmentDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_title = ' . $this->sqlCurrentTitle() . ','; } elseif( true == array_key_exists( 'CurrentTitle', $this->getChangedColumns() ) ) { $strSql .= ' current_title = ' . $this->sqlCurrentTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assessment_start_date = ' . $this->sqlAssessmentStartDate() . ','; } elseif( true == array_key_exists( 'AssessmentStartDate', $this->getChangedColumns() ) ) { $strSql .= ' assessment_start_date = ' . $this->sqlAssessmentStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assessment_end_date = ' . $this->sqlAssessmentEndDate() . ','; } elseif( true == array_key_exists( 'AssessmentEndDate', $this->getChangedColumns() ) ) { $strSql .= ' assessment_end_date = ' . $this->sqlAssessmentEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_salary = ' . $this->sqlCurrentSalary() . ','; } elseif( true == array_key_exists( 'CurrentSalary', $this->getChangedColumns() ) ) { $strSql .= ' current_salary = ' . $this->sqlCurrentSalary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recommended_new_salary = ' . $this->sqlRecommendedNewSalary() . ','; } elseif( true == array_key_exists( 'RecommendedNewSalary', $this->getChangedColumns() ) ) { $strSql .= ' recommended_new_salary = ' . $this->sqlRecommendedNewSalary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_new_salary = ' . $this->sqlActualNewSalary() . ','; } elseif( true == array_key_exists( 'ActualNewSalary', $this->getChangedColumns() ) ) { $strSql .= ' actual_new_salary = ' . $this->sqlActualNewSalary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' salary_notes = ' . $this->sqlSalaryNotes() . ','; } elseif( true == array_key_exists( 'SalaryNotes', $this->getChangedColumns() ) ) { $strSql .= ' salary_notes = ' . $this->sqlSalaryNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn() . ','; } elseif( true == array_key_exists( 'AcceptedOn', $this->getChangedColumns() ) ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; } elseif( true == array_key_exists( 'PsDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'manager_employee_id' => $this->getManagerEmployeeId(),
			'employee_id' => $this->getEmployeeId(),
			'employee_assessment_type_id' => $this->getEmployeeAssessmentTypeId(),
			'department_id' => $this->getDepartmentId(),
			'assessment_datetime' => $this->getAssessmentDatetime(),
			'current_title' => $this->getCurrentTitle(),
			'assessment_start_date' => $this->getAssessmentStartDate(),
			'assessment_end_date' => $this->getAssessmentEndDate(),
			'current_salary' => $this->getCurrentSalary(),
			'recommended_new_salary' => $this->getRecommendedNewSalary(),
			'actual_new_salary' => $this->getActualNewSalary(),
			'salary_notes' => $this->getSalaryNotes(),
			'accepted_on' => $this->getAcceptedOn(),
			'completed_on' => $this->getCompletedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ps_document_id' => $this->getPsDocumentId()
		);
	}

}
?>