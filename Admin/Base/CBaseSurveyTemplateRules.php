<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplateRules
 * Do not add any new functions to this class.
 */

class CBaseSurveyTemplateRules extends CEosPluralBase {

	/**
	 * @return CSurveyTemplateRule[]
	 */
	public static function fetchSurveyTemplateRules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyTemplateRule', $objDatabase );
	}

	/**
	 * @return CSurveyTemplateRule
	 */
	public static function fetchSurveyTemplateRule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyTemplateRule', $objDatabase );
	}

	public static function fetchSurveyTemplateRuleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_template_rules', $objDatabase );
	}

	public static function fetchSurveyTemplateRuleById( $intId, $objDatabase ) {
		return self::fetchSurveyTemplateRule( sprintf( 'SELECT * FROM survey_template_rules WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveyTemplateRulesBySurveyRuleTypeId( $intSurveyRuleTypeId, $objDatabase ) {
		return self::fetchSurveyTemplateRules( sprintf( 'SELECT * FROM survey_template_rules WHERE survey_rule_type_id = %d', ( int ) $intSurveyRuleTypeId ), $objDatabase );
	}

	public static function fetchSurveyTemplateRulesBySurveyTemplateRuleSetId( $intSurveyTemplateRuleSetId, $objDatabase ) {
		return self::fetchSurveyTemplateRules( sprintf( 'SELECT * FROM survey_template_rules WHERE survey_template_rule_set_id = %d', ( int ) $intSurveyTemplateRuleSetId ), $objDatabase );
	}

	public static function fetchSurveyTemplateRulesBySurveyTemplateQuestionGroupId( $intSurveyTemplateQuestionGroupId, $objDatabase ) {
		return self::fetchSurveyTemplateRules( sprintf( 'SELECT * FROM survey_template_rules WHERE survey_template_question_group_id = %d', ( int ) $intSurveyTemplateQuestionGroupId ), $objDatabase );
	}

	public static function fetchSurveyTemplateRulesBySurveyTemplateQuestionId( $intSurveyTemplateQuestionId, $objDatabase ) {
		return self::fetchSurveyTemplateRules( sprintf( 'SELECT * FROM survey_template_rules WHERE survey_template_question_id = %d', ( int ) $intSurveyTemplateQuestionId ), $objDatabase );
	}

	public static function fetchSurveyTemplateRulesByConditionTemplateQuestionId( $intConditionTemplateQuestionId, $objDatabase ) {
		return self::fetchSurveyTemplateRules( sprintf( 'SELECT * FROM survey_template_rules WHERE condition_template_question_id = %d', ( int ) $intConditionTemplateQuestionId ), $objDatabase );
	}

	public static function fetchSurveyTemplateRulesByConditionTemplateQuestionOptionId( $intConditionTemplateQuestionOptionId, $objDatabase ) {
		return self::fetchSurveyTemplateRules( sprintf( 'SELECT * FROM survey_template_rules WHERE condition_template_question_option_id = %d', ( int ) $intConditionTemplateQuestionOptionId ), $objDatabase );
	}

	public static function fetchSurveyTemplateRulesBySurveyConditionTypeId( $intSurveyConditionTypeId, $objDatabase ) {
		return self::fetchSurveyTemplateRules( sprintf( 'SELECT * FROM survey_template_rules WHERE survey_condition_type_id = %d', ( int ) $intSurveyConditionTypeId ), $objDatabase );
	}

	public static function fetchSurveyTemplateRulesByManagerTemplateSurveyId( $intManagerTemplateSurveyId, $objDatabase ) {
		return self::fetchSurveyTemplateRules( sprintf( 'SELECT * FROM survey_template_rules WHERE manager_template_survey_id = %d', ( int ) $intManagerTemplateSurveyId ), $objDatabase );
	}

	public static function fetchSurveyTemplateRulesBySurveyTemplateTaskId( $intSurveyTemplateTaskId, $objDatabase ) {
		return self::fetchSurveyTemplateRules( sprintf( 'SELECT * FROM survey_template_rules WHERE survey_template_task_id = %d', ( int ) $intSurveyTemplateTaskId ), $objDatabase );
	}

	public static function fetchSurveyTemplateRulesBySurveyTemplateEmailId( $intSurveyTemplateEmailId, $objDatabase ) {
		return self::fetchSurveyTemplateRules( sprintf( 'SELECT * FROM survey_template_rules WHERE survey_template_email_id = %d', ( int ) $intSurveyTemplateEmailId ), $objDatabase );
	}

}
?>