<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemTransactions
 * Do not add any new functions to this class.
 */

class CBaseSemTransactions extends CEosPluralBase {

	/**
	 * @return CSemTransaction[]
	 */
	public static function fetchSemTransactions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemTransaction', $objDatabase );
	}

	/**
	 * @return CSemTransaction
	 */
	public static function fetchSemTransaction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemTransaction', $objDatabase );
	}

	public static function fetchSemTransactionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_transactions', $objDatabase );
	}

	public static function fetchSemTransactionById( $intId, $objDatabase ) {
		return self::fetchSemTransaction( sprintf( 'SELECT * FROM sem_transactions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemTransactionsByCid( $intCid, $objDatabase ) {
		return self::fetchSemTransactions( sprintf( 'SELECT * FROM sem_transactions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSemTransactionsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSemTransactions( sprintf( 'SELECT * FROM sem_transactions WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchSemTransactionsBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemTransactions( sprintf( 'SELECT * FROM sem_transactions WHERE sem_ad_group_id = %d', ( int ) $intSemAdGroupId ), $objDatabase );
	}

	public static function fetchSemTransactionsBySemSourceId( $intSemSourceId, $objDatabase ) {
		return self::fetchSemTransactions( sprintf( 'SELECT * FROM sem_transactions WHERE sem_source_id = %d', ( int ) $intSemSourceId ), $objDatabase );
	}

	public static function fetchSemTransactionsBySemKeywordId( $intSemKeywordId, $objDatabase ) {
		return self::fetchSemTransactions( sprintf( 'SELECT * FROM sem_transactions WHERE sem_keyword_id = %d', ( int ) $intSemKeywordId ), $objDatabase );
	}

	public static function fetchSemTransactionsByCompanyPaymentId( $intCompanyPaymentId, $objDatabase ) {
		return self::fetchSemTransactions( sprintf( 'SELECT * FROM sem_transactions WHERE company_payment_id = %d', ( int ) $intCompanyPaymentId ), $objDatabase );
	}

	public static function fetchSemTransactionsBySemBudgetId( $intSemBudgetId, $objDatabase ) {
		return self::fetchSemTransactions( sprintf( 'SELECT * FROM sem_transactions WHERE sem_budget_id = %d', ( int ) $intSemBudgetId ), $objDatabase );
	}

}
?>