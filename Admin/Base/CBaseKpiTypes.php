<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CKpiTypes
 * Do not add any new functions to this class.
 */

class CBaseKpiTypes extends CEosPluralBase {

	/**
	 * @return CKpiType[]
	 */
	public static function fetchKpiTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CKpiType', $objDatabase );
	}

	/**
	 * @return CKpiType
	 */
	public static function fetchKpiType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CKpiType', $objDatabase );
	}

	public static function fetchKpiTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'kpi_types', $objDatabase );
	}

	public static function fetchKpiTypeById( $intId, $objDatabase ) {
		return self::fetchKpiType( sprintf( 'SELECT * FROM kpi_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchKpiTypesByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchKpiTypes( sprintf( 'SELECT * FROM kpi_types WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>