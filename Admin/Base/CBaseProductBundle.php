<?php

class CBaseProductBundle extends CEosSingularBase {

	const TABLE_NAME = 'public.product_bundles';

	protected $m_intId;
	protected $m_intProductBundleId;
	protected $m_intPsLeadId;
	protected $m_intPsProductDisplayTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltUnitMonthlyRecurring;
	protected $m_fltPropertySetup;
	protected $m_fltPropertyTraining;
	protected $m_intMinUnits;
	protected $m_intMaxUnits;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsShowDetails;
	protected $m_boolIsCorporateEntity;

	public function __construct() {
		parent::__construct();

		$this->m_intPsProductDisplayTypeId = '2';
		$this->m_fltUnitMonthlyRecurring = '0';
		$this->m_fltPropertySetup = '0';
		$this->m_fltPropertyTraining = '0';
		$this->m_intUpdatedBy = '1';
		$this->m_intCreatedBy = '1';
		$this->m_boolIsShowDetails = true;
		$this->m_boolIsCorporateEntity = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['product_bundle_id'] ) && $boolDirectSet ) $this->set( 'm_intProductBundleId', trim( $arrValues['product_bundle_id'] ) ); elseif( isset( $arrValues['product_bundle_id'] ) ) $this->setProductBundleId( $arrValues['product_bundle_id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['ps_product_display_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductDisplayTypeId', trim( $arrValues['ps_product_display_type_id'] ) ); elseif( isset( $arrValues['ps_product_display_type_id'] ) ) $this->setPsProductDisplayTypeId( $arrValues['ps_product_display_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['unit_monthly_recurring'] ) && $boolDirectSet ) $this->set( 'm_fltUnitMonthlyRecurring', trim( $arrValues['unit_monthly_recurring'] ) ); elseif( isset( $arrValues['unit_monthly_recurring'] ) ) $this->setUnitMonthlyRecurring( $arrValues['unit_monthly_recurring'] );
		if( isset( $arrValues['property_setup'] ) && $boolDirectSet ) $this->set( 'm_fltPropertySetup', trim( $arrValues['property_setup'] ) ); elseif( isset( $arrValues['property_setup'] ) ) $this->setPropertySetup( $arrValues['property_setup'] );
		if( isset( $arrValues['property_training'] ) && $boolDirectSet ) $this->set( 'm_fltPropertyTraining', trim( $arrValues['property_training'] ) ); elseif( isset( $arrValues['property_training'] ) ) $this->setPropertyTraining( $arrValues['property_training'] );
		if( isset( $arrValues['min_units'] ) && $boolDirectSet ) $this->set( 'm_intMinUnits', trim( $arrValues['min_units'] ) ); elseif( isset( $arrValues['min_units'] ) ) $this->setMinUnits( $arrValues['min_units'] );
		if( isset( $arrValues['max_units'] ) && $boolDirectSet ) $this->set( 'm_intMaxUnits', trim( $arrValues['max_units'] ) ); elseif( isset( $arrValues['max_units'] ) ) $this->setMaxUnits( $arrValues['max_units'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_show_details'] ) && $boolDirectSet ) $this->set( 'm_boolIsShowDetails', trim( stripcslashes( $arrValues['is_show_details'] ) ) ); elseif( isset( $arrValues['is_show_details'] ) ) $this->setIsShowDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_show_details'] ) : $arrValues['is_show_details'] );
		if( isset( $arrValues['is_corporate_entity'] ) && $boolDirectSet ) $this->set( 'm_boolIsCorporateEntity', trim( stripcslashes( $arrValues['is_corporate_entity'] ) ) ); elseif( isset( $arrValues['is_corporate_entity'] ) ) $this->setIsCorporateEntity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_corporate_entity'] ) : $arrValues['is_corporate_entity'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setProductBundleId( $intProductBundleId ) {
		$this->set( 'm_intProductBundleId', CStrings::strToIntDef( $intProductBundleId, NULL, false ) );
	}

	public function getProductBundleId() {
		return $this->m_intProductBundleId;
	}

	public function sqlProductBundleId() {
		return ( true == isset( $this->m_intProductBundleId ) ) ? ( string ) $this->m_intProductBundleId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setPsProductDisplayTypeId( $intPsProductDisplayTypeId ) {
		$this->set( 'm_intPsProductDisplayTypeId', CStrings::strToIntDef( $intPsProductDisplayTypeId, NULL, false ) );
	}

	public function getPsProductDisplayTypeId() {
		return $this->m_intPsProductDisplayTypeId;
	}

	public function sqlPsProductDisplayTypeId() {
		return ( true == isset( $this->m_intPsProductDisplayTypeId ) ) ? ( string ) $this->m_intPsProductDisplayTypeId : '2';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setUnitMonthlyRecurring( $fltUnitMonthlyRecurring ) {
		$this->set( 'm_fltUnitMonthlyRecurring', CStrings::strToFloatDef( $fltUnitMonthlyRecurring, NULL, false, 2 ) );
	}

	public function getUnitMonthlyRecurring() {
		return $this->m_fltUnitMonthlyRecurring;
	}

	public function sqlUnitMonthlyRecurring() {
		return ( true == isset( $this->m_fltUnitMonthlyRecurring ) ) ? ( string ) $this->m_fltUnitMonthlyRecurring : '0';
	}

	public function setPropertySetup( $fltPropertySetup ) {
		$this->set( 'm_fltPropertySetup', CStrings::strToFloatDef( $fltPropertySetup, NULL, false, 2 ) );
	}

	public function getPropertySetup() {
		return $this->m_fltPropertySetup;
	}

	public function sqlPropertySetup() {
		return ( true == isset( $this->m_fltPropertySetup ) ) ? ( string ) $this->m_fltPropertySetup : '0';
	}

	public function setPropertyTraining( $fltPropertyTraining ) {
		$this->set( 'm_fltPropertyTraining', CStrings::strToFloatDef( $fltPropertyTraining, NULL, false, 2 ) );
	}

	public function getPropertyTraining() {
		return $this->m_fltPropertyTraining;
	}

	public function sqlPropertyTraining() {
		return ( true == isset( $this->m_fltPropertyTraining ) ) ? ( string ) $this->m_fltPropertyTraining : '0';
	}

	public function setMinUnits( $intMinUnits ) {
		$this->set( 'm_intMinUnits', CStrings::strToIntDef( $intMinUnits, NULL, false ) );
	}

	public function getMinUnits() {
		return $this->m_intMinUnits;
	}

	public function sqlMinUnits() {
		return ( true == isset( $this->m_intMinUnits ) ) ? ( string ) $this->m_intMinUnits : 'NULL';
	}

	public function setMaxUnits( $intMaxUnits ) {
		$this->set( 'm_intMaxUnits', CStrings::strToIntDef( $intMaxUnits, NULL, false ) );
	}

	public function getMaxUnits() {
		return $this->m_intMaxUnits;
	}

	public function sqlMaxUnits() {
		return ( true == isset( $this->m_intMaxUnits ) ) ? ( string ) $this->m_intMaxUnits : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsShowDetails( $boolIsShowDetails ) {
		$this->set( 'm_boolIsShowDetails', CStrings::strToBool( $boolIsShowDetails ) );
	}

	public function getIsShowDetails() {
		return $this->m_boolIsShowDetails;
	}

	public function sqlIsShowDetails() {
		return ( true == isset( $this->m_boolIsShowDetails ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsShowDetails ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCorporateEntity( $boolIsCorporateEntity ) {
		$this->set( 'm_boolIsCorporateEntity', CStrings::strToBool( $boolIsCorporateEntity ) );
	}

	public function getIsCorporateEntity() {
		return $this->m_boolIsCorporateEntity;
	}

	public function sqlIsCorporateEntity() {
		return ( true == isset( $this->m_boolIsCorporateEntity ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCorporateEntity ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, product_bundle_id, ps_lead_id, ps_product_display_type_id, name, description, unit_monthly_recurring, property_setup, property_training, min_units, max_units, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, is_show_details, is_corporate_entity )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlProductBundleId() . ', ' .
		          $this->sqlPsLeadId() . ', ' .
		          $this->sqlPsProductDisplayTypeId() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlDescription() . ', ' .
		          $this->sqlUnitMonthlyRecurring() . ', ' .
		          $this->sqlPropertySetup() . ', ' .
		          $this->sqlPropertyTraining() . ', ' .
		          $this->sqlMinUnits() . ', ' .
		          $this->sqlMaxUnits() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlIsShowDetails() . ', ' .
		          $this->sqlIsCorporateEntity() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_bundle_id = ' . $this->sqlProductBundleId(). ',' ; } elseif( true == array_key_exists( 'ProductBundleId', $this->getChangedColumns() ) ) { $strSql .= ' product_bundle_id = ' . $this->sqlProductBundleId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_display_type_id = ' . $this->sqlPsProductDisplayTypeId(). ',' ; } elseif( true == array_key_exists( 'PsProductDisplayTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_display_type_id = ' . $this->sqlPsProductDisplayTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_monthly_recurring = ' . $this->sqlUnitMonthlyRecurring(). ',' ; } elseif( true == array_key_exists( 'UnitMonthlyRecurring', $this->getChangedColumns() ) ) { $strSql .= ' unit_monthly_recurring = ' . $this->sqlUnitMonthlyRecurring() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_setup = ' . $this->sqlPropertySetup(). ',' ; } elseif( true == array_key_exists( 'PropertySetup', $this->getChangedColumns() ) ) { $strSql .= ' property_setup = ' . $this->sqlPropertySetup() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_training = ' . $this->sqlPropertyTraining(). ',' ; } elseif( true == array_key_exists( 'PropertyTraining', $this->getChangedColumns() ) ) { $strSql .= ' property_training = ' . $this->sqlPropertyTraining() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_units = ' . $this->sqlMinUnits(). ',' ; } elseif( true == array_key_exists( 'MinUnits', $this->getChangedColumns() ) ) { $strSql .= ' min_units = ' . $this->sqlMinUnits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_units = ' . $this->sqlMaxUnits(). ',' ; } elseif( true == array_key_exists( 'MaxUnits', $this->getChangedColumns() ) ) { $strSql .= ' max_units = ' . $this->sqlMaxUnits() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_show_details = ' . $this->sqlIsShowDetails(). ',' ; } elseif( true == array_key_exists( 'IsShowDetails', $this->getChangedColumns() ) ) { $strSql .= ' is_show_details = ' . $this->sqlIsShowDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_corporate_entity = ' . $this->sqlIsCorporateEntity(). ',' ; } elseif( true == array_key_exists( 'IsCorporateEntity', $this->getChangedColumns() ) ) { $strSql .= ' is_corporate_entity = ' . $this->sqlIsCorporateEntity() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'product_bundle_id' => $this->getProductBundleId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'ps_product_display_type_id' => $this->getPsProductDisplayTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'unit_monthly_recurring' => $this->getUnitMonthlyRecurring(),
			'property_setup' => $this->getPropertySetup(),
			'property_training' => $this->getPropertyTraining(),
			'min_units' => $this->getMinUnits(),
			'max_units' => $this->getMaxUnits(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_show_details' => $this->getIsShowDetails(),
			'is_corporate_entity' => $this->getIsCorporateEntity()
		);
	}

}
?>