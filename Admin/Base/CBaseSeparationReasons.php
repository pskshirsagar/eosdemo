<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSeparationReasons
 * Do not add any new functions to this class.
 */

class CBaseSeparationReasons extends CEosPluralBase {

	/**
	 * @return CSeparationReason[]
	 */
	public static function fetchSeparationReasons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSeparationReason', $objDatabase );
	}

	/**
	 * @return CSeparationReason
	 */
	public static function fetchSeparationReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSeparationReason', $objDatabase );
	}

	public static function fetchSeparationReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'separation_reasons', $objDatabase );
	}

	public static function fetchSeparationReasonById( $intId, $objDatabase ) {
		return self::fetchSeparationReason( sprintf( 'SELECT * FROM separation_reasons WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSeparationReasonsByParentReasonId( $intParentReasonId, $objDatabase ) {
		return self::fetchSeparationReasons( sprintf( 'SELECT * FROM separation_reasons WHERE parent_reason_id = %d', ( int ) $intParentReasonId ), $objDatabase );
	}

	public static function fetchSeparationReasonsByEmployeeApplicationStatusTypeId( $intEmployeeApplicationStatusTypeId, $objDatabase ) {
		return self::fetchSeparationReasons( sprintf( 'SELECT * FROM separation_reasons WHERE employee_application_status_type_id = %d', ( int ) $intEmployeeApplicationStatusTypeId ), $objDatabase );
	}

}
?>