<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplates
 * Do not add any new functions to this class.
 */

class CBaseDocumentTemplates extends CEosPluralBase {

	/**
	 * @return CDocumentTemplate[]
	 */
	public static function fetchDocumentTemplates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDocumentTemplate', $objDatabase );
	}

	/**
	 * @return CDocumentTemplate
	 */
	public static function fetchDocumentTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentTemplate', $objDatabase );
	}

	public static function fetchDocumentTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_templates', $objDatabase );
	}

	public static function fetchDocumentTemplateById( $intId, $objDatabase ) {
		return self::fetchDocumentTemplate( sprintf( 'SELECT * FROM document_templates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDocumentTemplatesByDocumentTemplateTypeId( $intDocumentTemplateTypeId, $objDatabase ) {
		return self::fetchDocumentTemplates( sprintf( 'SELECT * FROM document_templates WHERE document_template_type_id = %d', ( int ) $intDocumentTemplateTypeId ), $objDatabase );
	}

	public static function fetchDocumentTemplatesByDocumentTypeId( $intDocumentTypeId, $objDatabase ) {
		return self::fetchDocumentTemplates( sprintf( 'SELECT * FROM document_templates WHERE document_type_id = %d', ( int ) $intDocumentTypeId ), $objDatabase );
	}

	public static function fetchDocumentTemplatesByDocumentSubTypeId( $intDocumentSubTypeId, $objDatabase ) {
		return self::fetchDocumentTemplates( sprintf( 'SELECT * FROM document_templates WHERE document_sub_type_id = %d', ( int ) $intDocumentSubTypeId ), $objDatabase );
	}

	public static function fetchDocumentTemplatesByParentDocumentTemplateId( $intParentDocumentTemplateId, $objDatabase ) {
		return self::fetchDocumentTemplates( sprintf( 'SELECT * FROM document_templates WHERE parent_document_template_id = %d', ( int ) $intParentDocumentTemplateId ), $objDatabase );
	}

}
?>