<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeHourTypes
 * Do not add any new functions to this class.
 */

class CBaseEmployeeHourTypes extends CEosPluralBase {

	/**
	 * @return CEmployeeHourType[]
	 */
	public static function fetchEmployeeHourTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeHourType', $objDatabase );
	}

	/**
	 * @return CEmployeeHourType
	 */
	public static function fetchEmployeeHourType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeHourType', $objDatabase );
	}

	public static function fetchEmployeeHourTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_hour_types', $objDatabase );
	}

	public static function fetchEmployeeHourTypeById( $intId, $objDatabase ) {
		return self::fetchEmployeeHourType( sprintf( 'SELECT * FROM employee_hour_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>