<?php

class CBasePlannerParticipant extends CEosSingularBase {

	const TABLE_NAME = 'public.planner_participants';

	protected $m_intId;
	protected $m_intPlannerId;
	protected $m_intEmployeeId;
	protected $m_fltDevPoints;
	protected $m_fltQaPoints;
	protected $m_boolLeaveMeeting;
	protected $m_boolIsObserver;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolLeaveMeeting = false;
		$this->m_boolIsObserver = false;
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['planner_id'] ) && $boolDirectSet ) $this->set( 'm_intPlannerId', trim( $arrValues['planner_id'] ) ); elseif( isset( $arrValues['planner_id'] ) ) $this->setPlannerId( $arrValues['planner_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['dev_points'] ) && $boolDirectSet ) $this->set( 'm_fltDevPoints', trim( $arrValues['dev_points'] ) ); elseif( isset( $arrValues['dev_points'] ) ) $this->setDevPoints( $arrValues['dev_points'] );
		if( isset( $arrValues['qa_points'] ) && $boolDirectSet ) $this->set( 'm_fltQaPoints', trim( $arrValues['qa_points'] ) ); elseif( isset( $arrValues['qa_points'] ) ) $this->setQaPoints( $arrValues['qa_points'] );
		if( isset( $arrValues['leave_meeting'] ) && $boolDirectSet ) $this->set( 'm_boolLeaveMeeting', trim( stripcslashes( $arrValues['leave_meeting'] ) ) ); elseif( isset( $arrValues['leave_meeting'] ) ) $this->setLeaveMeeting( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['leave_meeting'] ) : $arrValues['leave_meeting'] );
		if( isset( $arrValues['is_observer'] ) && $boolDirectSet ) $this->set( 'm_boolIsObserver', trim( stripcslashes( $arrValues['is_observer'] ) ) ); elseif( isset( $arrValues['is_observer'] ) ) $this->setIsObserver( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_observer'] ) : $arrValues['is_observer'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPlannerId( $intPlannerId ) {
		$this->set( 'm_intPlannerId', CStrings::strToIntDef( $intPlannerId, NULL, false ) );
	}

	public function getPlannerId() {
		return $this->m_intPlannerId;
	}

	public function sqlPlannerId() {
		return ( true == isset( $this->m_intPlannerId ) ) ? ( string ) $this->m_intPlannerId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setDevPoints( $fltDevPoints ) {
		$this->set( 'm_fltDevPoints', CStrings::strToFloatDef( $fltDevPoints, NULL, false, 2 ) );
	}

	public function getDevPoints() {
		return $this->m_fltDevPoints;
	}

	public function sqlDevPoints() {
		return ( true == isset( $this->m_fltDevPoints ) ) ? ( string ) $this->m_fltDevPoints : 'NULL';
	}

	public function setQaPoints( $fltQaPoints ) {
		$this->set( 'm_fltQaPoints', CStrings::strToFloatDef( $fltQaPoints, NULL, false, 2 ) );
	}

	public function getQaPoints() {
		return $this->m_fltQaPoints;
	}

	public function sqlQaPoints() {
		return ( true == isset( $this->m_fltQaPoints ) ) ? ( string ) $this->m_fltQaPoints : 'NULL';
	}

	public function setLeaveMeeting( $boolLeaveMeeting ) {
		$this->set( 'm_boolLeaveMeeting', CStrings::strToBool( $boolLeaveMeeting ) );
	}

	public function getLeaveMeeting() {
		return $this->m_boolLeaveMeeting;
	}

	public function sqlLeaveMeeting() {
		return ( true == isset( $this->m_boolLeaveMeeting ) ) ? '\'' . ( true == ( bool ) $this->m_boolLeaveMeeting ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsObserver( $boolIsObserver ) {
		$this->set( 'm_boolIsObserver', CStrings::strToBool( $boolIsObserver ) );
	}

	public function getIsObserver() {
		return $this->m_boolIsObserver;
	}

	public function sqlIsObserver() {
		return ( true == isset( $this->m_boolIsObserver ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsObserver ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, planner_id, employee_id, dev_points, qa_points, leave_meeting, is_observer, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPlannerId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlDevPoints() . ', ' .
 						$this->sqlQaPoints() . ', ' .
 						$this->sqlLeaveMeeting() . ', ' .
 						$this->sqlIsObserver() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' planner_id = ' . $this->sqlPlannerId() . ','; } elseif( true == array_key_exists( 'PlannerId', $this->getChangedColumns() ) ) { $strSql .= ' planner_id = ' . $this->sqlPlannerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dev_points = ' . $this->sqlDevPoints() . ','; } elseif( true == array_key_exists( 'DevPoints', $this->getChangedColumns() ) ) { $strSql .= ' dev_points = ' . $this->sqlDevPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_points = ' . $this->sqlQaPoints() . ','; } elseif( true == array_key_exists( 'QaPoints', $this->getChangedColumns() ) ) { $strSql .= ' qa_points = ' . $this->sqlQaPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' leave_meeting = ' . $this->sqlLeaveMeeting() . ','; } elseif( true == array_key_exists( 'LeaveMeeting', $this->getChangedColumns() ) ) { $strSql .= ' leave_meeting = ' . $this->sqlLeaveMeeting() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_observer = ' . $this->sqlIsObserver() . ','; } elseif( true == array_key_exists( 'IsObserver', $this->getChangedColumns() ) ) { $strSql .= ' is_observer = ' . $this->sqlIsObserver() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'planner_id' => $this->getPlannerId(),
			'employee_id' => $this->getEmployeeId(),
			'dev_points' => $this->getDevPoints(),
			'qa_points' => $this->getQaPoints(),
			'leave_meeting' => $this->getLeaveMeeting(),
			'is_observer' => $this->getIsObserver(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>