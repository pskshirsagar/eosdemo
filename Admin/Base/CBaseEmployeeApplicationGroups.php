<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationGroups
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationGroups extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationGroup[]
	 */
	public static function fetchEmployeeApplicationGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationGroup', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationGroup
	 */
	public static function fetchEmployeeApplicationGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationGroup', $objDatabase );
	}

	public static function fetchEmployeeApplicationGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_groups', $objDatabase );
	}

	public static function fetchEmployeeApplicationGroupById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationGroup( sprintf( 'SELECT * FROM employee_application_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationGroupsByPsWebsiteJobPostingId( $intPsWebsiteJobPostingId, $objDatabase ) {
		return self::fetchEmployeeApplicationGroups( sprintf( 'SELECT * FROM employee_application_groups WHERE ps_website_job_posting_id = %d', ( int ) $intPsWebsiteJobPostingId ), $objDatabase );
	}

}
?>