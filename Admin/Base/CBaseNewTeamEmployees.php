<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CNewTeamEmployees
 * Do not add any new functions to this class.
 */

class CBaseNewTeamEmployees extends CEosPluralBase {

	/**
	 * @return CNewTeamEmployee[]
	 */
	public static function fetchNewTeamEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CNewTeamEmployee::class, $objDatabase );
	}

	/**
	 * @return CNewTeamEmployee
	 */
	public static function fetchNewTeamEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CNewTeamEmployee::class, $objDatabase );
	}

	public static function fetchNewTeamEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'new_team_employees', $objDatabase );
	}

	public static function fetchNewTeamEmployeeById( $intId, $objDatabase ) {
		return self::fetchNewTeamEmployee( sprintf( 'SELECT * FROM new_team_employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchNewTeamEmployeesByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchNewTeamEmployees( sprintf( 'SELECT * FROM new_team_employees WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

	public static function fetchNewTeamEmployeesByReportingTeamId( $intReportingTeamId, $objDatabase ) {
		return self::fetchNewTeamEmployees( sprintf( 'SELECT * FROM new_team_employees WHERE reporting_team_id = %d', ( int ) $intReportingTeamId ), $objDatabase );
	}

	public static function fetchNewTeamEmployeesByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		return self::fetchNewTeamEmployees( sprintf( 'SELECT * FROM new_team_employees WHERE manager_employee_id = %d', ( int ) $intManagerEmployeeId ), $objDatabase );
	}

	public static function fetchNewTeamEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchNewTeamEmployees( sprintf( 'SELECT * FROM new_team_employees WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>