<?php

class CBaseCurrencyRate extends CEosSingularBase {

	const TABLE_NAME = 'public.currency_rates';

	protected $m_intId;
	protected $m_strCurrencyCode;
	protected $m_strCurrencyRateDate;
	protected $m_fltExchangeRate;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		if( isset( $arrValues['currency_rate_date'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyRateDate', trim( $arrValues['currency_rate_date'] ) ); elseif( isset( $arrValues['currency_rate_date'] ) ) $this->setCurrencyRateDate( $arrValues['currency_rate_date'] );
		if( isset( $arrValues['exchange_rate'] ) && $boolDirectSet ) $this->set( 'm_fltExchangeRate', trim( $arrValues['exchange_rate'] ) ); elseif( isset( $arrValues['exchange_rate'] ) ) $this->setExchangeRate( $arrValues['exchange_rate'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : '\'USD\'';
	}

	public function setCurrencyRateDate( $strCurrencyRateDate ) {
		$this->set( 'm_strCurrencyRateDate', CStrings::strTrimDef( $strCurrencyRateDate, -1, NULL, true ) );
	}

	public function getCurrencyRateDate() {
		return $this->m_strCurrencyRateDate;
	}

	public function sqlCurrencyRateDate() {
		return ( true == isset( $this->m_strCurrencyRateDate ) ) ? '\'' . $this->m_strCurrencyRateDate . '\'' : 'NOW()';
	}

	public function setExchangeRate( $fltExchangeRate ) {
		$this->set( 'm_fltExchangeRate', CStrings::strToFloatDef( $fltExchangeRate, NULL, false, 4 ) );
	}

	public function getExchangeRate() {
		return $this->m_fltExchangeRate;
	}

	public function sqlExchangeRate() {
		return ( true == isset( $this->m_fltExchangeRate ) ) ? ( string ) $this->m_fltExchangeRate : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, currency_code, currency_rate_date, exchange_rate, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCurrencyCode() . ', ' .
 						$this->sqlCurrencyRateDate() . ', ' .
 						$this->sqlExchangeRate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_rate_date = ' . $this->sqlCurrencyRateDate() . ','; } elseif( true == array_key_exists( 'CurrencyRateDate', $this->getChangedColumns() ) ) { $strSql .= ' currency_rate_date = ' . $this->sqlCurrencyRateDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exchange_rate = ' . $this->sqlExchangeRate() . ','; } elseif( true == array_key_exists( 'ExchangeRate', $this->getChangedColumns() ) ) { $strSql .= ' exchange_rate = ' . $this->sqlExchangeRate() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'currency_code' => $this->getCurrencyCode(),
			'currency_rate_date' => $this->getCurrencyRateDate(),
			'exchange_rate' => $this->getExchangeRate(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>