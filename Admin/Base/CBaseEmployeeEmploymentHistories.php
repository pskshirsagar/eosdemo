<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeEmploymentHistories
 * Do not add any new functions to this class.
 */

class CBaseEmployeeEmploymentHistories extends CEosPluralBase {

	/**
	 * @return CEmployeeEmploymentHistory[]
	 */
	public static function fetchEmployeeEmploymentHistories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeEmploymentHistory', $objDatabase );
	}

	/**
	 * @return CEmployeeEmploymentHistory
	 */
	public static function fetchEmployeeEmploymentHistory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeEmploymentHistory', $objDatabase );
	}

	public static function fetchEmployeeEmploymentHistoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_employment_histories', $objDatabase );
	}

	public static function fetchEmployeeEmploymentHistoryById( $intId, $objDatabase ) {
		return self::fetchEmployeeEmploymentHistory( sprintf( 'SELECT * FROM employee_employment_histories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeEmploymentHistoriesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeEmploymentHistories( sprintf( 'SELECT * FROM employee_employment_histories WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>