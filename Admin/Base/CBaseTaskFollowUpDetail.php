<?php

class CBaseTaskFollowUpDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.task_follow_up_details';

	protected $m_intId;
	protected $m_intTaskTypeId;
	protected $m_intTaskPriorityId;
	protected $m_intTaskStatusId;
	protected $m_strFollowUpTime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['task_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskTypeId', trim( $arrValues['task_type_id'] ) ); elseif( isset( $arrValues['task_type_id'] ) ) $this->setTaskTypeId( $arrValues['task_type_id'] );
		if( isset( $arrValues['task_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskPriorityId', trim( $arrValues['task_priority_id'] ) ); elseif( isset( $arrValues['task_priority_id'] ) ) $this->setTaskPriorityId( $arrValues['task_priority_id'] );
		if( isset( $arrValues['task_status_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskStatusId', trim( $arrValues['task_status_id'] ) ); elseif( isset( $arrValues['task_status_id'] ) ) $this->setTaskStatusId( $arrValues['task_status_id'] );
		if( isset( $arrValues['follow_up_time'] ) && $boolDirectSet ) $this->set( 'm_strFollowUpTime', trim( stripcslashes( $arrValues['follow_up_time'] ) ) ); elseif( isset( $arrValues['follow_up_time'] ) ) $this->setFollowUpTime( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['follow_up_time'] ) : $arrValues['follow_up_time'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaskTypeId( $intTaskTypeId ) {
		$this->set( 'm_intTaskTypeId', CStrings::strToIntDef( $intTaskTypeId, NULL, false ) );
	}

	public function getTaskTypeId() {
		return $this->m_intTaskTypeId;
	}

	public function sqlTaskTypeId() {
		return ( true == isset( $this->m_intTaskTypeId ) ) ? ( string ) $this->m_intTaskTypeId : 'NULL';
	}

	public function setTaskPriorityId( $intTaskPriorityId ) {
		$this->set( 'm_intTaskPriorityId', CStrings::strToIntDef( $intTaskPriorityId, NULL, false ) );
	}

	public function getTaskPriorityId() {
		return $this->m_intTaskPriorityId;
	}

	public function sqlTaskPriorityId() {
		return ( true == isset( $this->m_intTaskPriorityId ) ) ? ( string ) $this->m_intTaskPriorityId : 'NULL';
	}

	public function setTaskStatusId( $intTaskStatusId ) {
		$this->set( 'm_intTaskStatusId', CStrings::strToIntDef( $intTaskStatusId, NULL, false ) );
	}

	public function getTaskStatusId() {
		return $this->m_intTaskStatusId;
	}

	public function sqlTaskStatusId() {
		return ( true == isset( $this->m_intTaskStatusId ) ) ? ( string ) $this->m_intTaskStatusId : 'NULL';
	}

	public function setFollowUpTime( $strFollowUpTime ) {
		$this->set( 'm_strFollowUpTime', CStrings::strTrimDef( $strFollowUpTime, 10, NULL, true ) );
	}

	public function getFollowUpTime() {
		return $this->m_strFollowUpTime;
	}

	public function sqlFollowUpTime() {
		return ( true == isset( $this->m_strFollowUpTime ) ) ? '\'' . addslashes( $this->m_strFollowUpTime ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, task_type_id, task_priority_id, task_status_id, follow_up_time, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTaskTypeId() . ', ' .
 						$this->sqlTaskPriorityId() . ', ' .
 						$this->sqlTaskStatusId() . ', ' .
 						$this->sqlFollowUpTime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_type_id = ' . $this->sqlTaskTypeId() . ','; } elseif( true == array_key_exists( 'TaskTypeId', $this->getChangedColumns() ) ) { $strSql .= ' task_type_id = ' . $this->sqlTaskTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_priority_id = ' . $this->sqlTaskPriorityId() . ','; } elseif( true == array_key_exists( 'TaskPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' task_priority_id = ' . $this->sqlTaskPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_status_id = ' . $this->sqlTaskStatusId() . ','; } elseif( true == array_key_exists( 'TaskStatusId', $this->getChangedColumns() ) ) { $strSql .= ' task_status_id = ' . $this->sqlTaskStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' follow_up_time = ' . $this->sqlFollowUpTime() . ','; } elseif( true == array_key_exists( 'FollowUpTime', $this->getChangedColumns() ) ) { $strSql .= ' follow_up_time = ' . $this->sqlFollowUpTime() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'task_type_id' => $this->getTaskTypeId(),
			'task_priority_id' => $this->getTaskPriorityId(),
			'task_status_id' => $this->getTaskStatusId(),
			'follow_up_time' => $this->getFollowUpTime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>