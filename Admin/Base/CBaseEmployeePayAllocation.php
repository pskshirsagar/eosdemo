<?php

class CBaseEmployeePayAllocation extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.employee_pay_allocations';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intManagerEmployeeId;
	protected $m_intProposedDesignationId;
	protected $m_intPendingIncreaseEncryptionAssociationId;
	protected $m_intPendingHourlyEncryptionAssociationId;
	protected $m_intEmployeeCompensationLogId;
	protected $m_boolIsLocked;
	protected $m_strPayReviewYear;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intRejectedBy;
	protected $m_strRejectedOn;
	protected $m_intSubmittedBy;
	protected $m_strSubmittedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsLocked = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['manager_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intManagerEmployeeId', trim( $arrValues['manager_employee_id'] ) ); elseif( isset( $arrValues['manager_employee_id'] ) ) $this->setManagerEmployeeId( $arrValues['manager_employee_id'] );
		if( isset( $arrValues['proposed_designation_id'] ) && $boolDirectSet ) $this->set( 'm_intProposedDesignationId', trim( $arrValues['proposed_designation_id'] ) ); elseif( isset( $arrValues['proposed_designation_id'] ) ) $this->setProposedDesignationId( $arrValues['proposed_designation_id'] );
		if( isset( $arrValues['pending_increase_encryption_association_id'] ) && $boolDirectSet ) $this->set( 'm_intPendingIncreaseEncryptionAssociationId', trim( $arrValues['pending_increase_encryption_association_id'] ) ); elseif( isset( $arrValues['pending_increase_encryption_association_id'] ) ) $this->setPendingIncreaseEncryptionAssociationId( $arrValues['pending_increase_encryption_association_id'] );
		if( isset( $arrValues['pending_hourly_encryption_association_id'] ) && $boolDirectSet ) $this->set( 'm_intPendingHourlyEncryptionAssociationId', trim( $arrValues['pending_hourly_encryption_association_id'] ) ); elseif( isset( $arrValues['pending_hourly_encryption_association_id'] ) ) $this->setPendingHourlyEncryptionAssociationId( $arrValues['pending_hourly_encryption_association_id'] );
		if( isset( $arrValues['employee_compensation_log_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeCompensationLogId', trim( $arrValues['employee_compensation_log_id'] ) ); elseif( isset( $arrValues['employee_compensation_log_id'] ) ) $this->setEmployeeCompensationLogId( $arrValues['employee_compensation_log_id'] );
		if( isset( $arrValues['is_locked'] ) && $boolDirectSet ) $this->set( 'm_boolIsLocked', trim( stripcslashes( $arrValues['is_locked'] ) ) ); elseif( isset( $arrValues['is_locked'] ) ) $this->setIsLocked( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_locked'] ) : $arrValues['is_locked'] );
		if( isset( $arrValues['pay_review_year'] ) && $boolDirectSet ) $this->set( 'm_strPayReviewYear', trim( $arrValues['pay_review_year'] ) ); elseif( isset( $arrValues['pay_review_year'] ) ) $this->setPayReviewYear( $arrValues['pay_review_year'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['rejected_by'] ) && $boolDirectSet ) $this->set( 'm_intRejectedBy', trim( $arrValues['rejected_by'] ) ); elseif( isset( $arrValues['rejected_by'] ) ) $this->setRejectedBy( $arrValues['rejected_by'] );
		if( isset( $arrValues['rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strRejectedOn', trim( $arrValues['rejected_on'] ) ); elseif( isset( $arrValues['rejected_on'] ) ) $this->setRejectedOn( $arrValues['rejected_on'] );
		if( isset( $arrValues['submitted_by'] ) && $boolDirectSet ) $this->set( 'm_intSubmittedBy', trim( $arrValues['submitted_by'] ) ); elseif( isset( $arrValues['submitted_by'] ) ) $this->setSubmittedBy( $arrValues['submitted_by'] );
		if( isset( $arrValues['submitted_on'] ) && $boolDirectSet ) $this->set( 'm_strSubmittedOn', trim( $arrValues['submitted_on'] ) ); elseif( isset( $arrValues['submitted_on'] ) ) $this->setSubmittedOn( $arrValues['submitted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setManagerEmployeeId( $intManagerEmployeeId ) {
		$this->set( 'm_intManagerEmployeeId', CStrings::strToIntDef( $intManagerEmployeeId, NULL, false ) );
	}

	public function getManagerEmployeeId() {
		return $this->m_intManagerEmployeeId;
	}

	public function sqlManagerEmployeeId() {
		return ( true == isset( $this->m_intManagerEmployeeId ) ) ? ( string ) $this->m_intManagerEmployeeId : 'NULL';
	}

	public function setProposedDesignationId( $intProposedDesignationId ) {
		$this->set( 'm_intProposedDesignationId', CStrings::strToIntDef( $intProposedDesignationId, NULL, false ) );
	}

	public function getProposedDesignationId() {
		return $this->m_intProposedDesignationId;
	}

	public function sqlProposedDesignationId() {
		return ( true == isset( $this->m_intProposedDesignationId ) ) ? ( string ) $this->m_intProposedDesignationId : 'NULL';
	}

	public function setPendingIncreaseEncryptionAssociationId( $intPendingIncreaseEncryptionAssociationId ) {
		$this->set( 'm_intPendingIncreaseEncryptionAssociationId', CStrings::strToIntDef( $intPendingIncreaseEncryptionAssociationId, NULL, false ) );
	}

	public function getPendingIncreaseEncryptionAssociationId() {
		return $this->m_intPendingIncreaseEncryptionAssociationId;
	}

	public function sqlPendingIncreaseEncryptionAssociationId() {
		return ( true == isset( $this->m_intPendingIncreaseEncryptionAssociationId ) ) ? ( string ) $this->m_intPendingIncreaseEncryptionAssociationId : 'NULL';
	}

	public function setPendingHourlyEncryptionAssociationId( $intPendingHourlyEncryptionAssociationId ) {
		$this->set( 'm_intPendingHourlyEncryptionAssociationId', CStrings::strToIntDef( $intPendingHourlyEncryptionAssociationId, NULL, false ) );
	}

	public function getPendingHourlyEncryptionAssociationId() {
		return $this->m_intPendingHourlyEncryptionAssociationId;
	}

	public function sqlPendingHourlyEncryptionAssociationId() {
		return ( true == isset( $this->m_intPendingHourlyEncryptionAssociationId ) ) ? ( string ) $this->m_intPendingHourlyEncryptionAssociationId : 'NULL';
	}

	public function setEmployeeCompensationLogId( $intEmployeeCompensationLogId ) {
		$this->set( 'm_intEmployeeCompensationLogId', CStrings::strToIntDef( $intEmployeeCompensationLogId, NULL, false ) );
	}

	public function getEmployeeCompensationLogId() {
		return $this->m_intEmployeeCompensationLogId;
	}

	public function sqlEmployeeCompensationLogId() {
		return ( true == isset( $this->m_intEmployeeCompensationLogId ) ) ? ( string ) $this->m_intEmployeeCompensationLogId : 'NULL';
	}

	public function setIsLocked( $boolIsLocked ) {
		$this->set( 'm_boolIsLocked', CStrings::strToBool( $boolIsLocked ) );
	}

	public function getIsLocked() {
		return $this->m_boolIsLocked;
	}

	public function sqlIsLocked() {
		return ( true == isset( $this->m_boolIsLocked ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLocked ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPayReviewYear( $strPayReviewYear ) {
		$this->set( 'm_strPayReviewYear', CStrings::strTrimDef( $strPayReviewYear, -1, NULL, true ) );
	}

	public function getPayReviewYear() {
		return $this->m_strPayReviewYear;
	}

	public function sqlPayReviewYear() {
		return ( true == isset( $this->m_strPayReviewYear ) ) ? '\'' . $this->m_strPayReviewYear . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setRejectedBy( $intRejectedBy ) {
		$this->set( 'm_intRejectedBy', CStrings::strToIntDef( $intRejectedBy, NULL, false ) );
	}

	public function getRejectedBy() {
		return $this->m_intRejectedBy;
	}

	public function sqlRejectedBy() {
		return ( true == isset( $this->m_intRejectedBy ) ) ? ( string ) $this->m_intRejectedBy : 'NULL';
	}

	public function setRejectedOn( $strRejectedOn ) {
		$this->set( 'm_strRejectedOn', CStrings::strTrimDef( $strRejectedOn, -1, NULL, true ) );
	}

	public function getRejectedOn() {
		return $this->m_strRejectedOn;
	}

	public function sqlRejectedOn() {
		return ( true == isset( $this->m_strRejectedOn ) ) ? '\'' . $this->m_strRejectedOn . '\'' : 'NULL';
	}

	public function setSubmittedBy( $intSubmittedBy ) {
		$this->set( 'm_intSubmittedBy', CStrings::strToIntDef( $intSubmittedBy, NULL, false ) );
	}

	public function getSubmittedBy() {
		return $this->m_intSubmittedBy;
	}

	public function sqlSubmittedBy() {
		return ( true == isset( $this->m_intSubmittedBy ) ) ? ( string ) $this->m_intSubmittedBy : 'NULL';
	}

	public function setSubmittedOn( $strSubmittedOn ) {
		$this->set( 'm_strSubmittedOn', CStrings::strTrimDef( $strSubmittedOn, -1, NULL, true ) );
	}

	public function getSubmittedOn() {
		return $this->m_strSubmittedOn;
	}

	public function sqlSubmittedOn() {
		return ( true == isset( $this->m_strSubmittedOn ) ) ? '\'' . $this->m_strSubmittedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, manager_employee_id, proposed_designation_id, pending_increase_encryption_association_id, pending_hourly_encryption_association_id, employee_compensation_log_id, is_locked, pay_review_year, approved_by, approved_on, rejected_by, rejected_on, submitted_by, submitted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlManagerEmployeeId() . ', ' .
						$this->sqlProposedDesignationId() . ', ' .
						$this->sqlPendingIncreaseEncryptionAssociationId() . ', ' .
						$this->sqlPendingHourlyEncryptionAssociationId() . ', ' .
						$this->sqlEmployeeCompensationLogId() . ', ' .
						$this->sqlIsLocked() . ', ' .
						$this->sqlPayReviewYear() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlRejectedBy() . ', ' .
						$this->sqlRejectedOn() . ', ' .
						$this->sqlSubmittedBy() . ', ' .
						$this->sqlSubmittedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manager_employee_id = ' . $this->sqlManagerEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ManagerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' manager_employee_id = ' . $this->sqlManagerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposed_designation_id = ' . $this->sqlProposedDesignationId(). ',' ; } elseif( true == array_key_exists( 'ProposedDesignationId', $this->getChangedColumns() ) ) { $strSql .= ' proposed_designation_id = ' . $this->sqlProposedDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pending_increase_encryption_association_id = ' . $this->sqlPendingIncreaseEncryptionAssociationId(). ',' ; } elseif( true == array_key_exists( 'PendingIncreaseEncryptionAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' pending_increase_encryption_association_id = ' . $this->sqlPendingIncreaseEncryptionAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pending_hourly_encryption_association_id = ' . $this->sqlPendingHourlyEncryptionAssociationId(). ',' ; } elseif( true == array_key_exists( 'PendingHourlyEncryptionAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' pending_hourly_encryption_association_id = ' . $this->sqlPendingHourlyEncryptionAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_compensation_log_id = ' . $this->sqlEmployeeCompensationLogId(). ',' ; } elseif( true == array_key_exists( 'EmployeeCompensationLogId', $this->getChangedColumns() ) ) { $strSql .= ' employee_compensation_log_id = ' . $this->sqlEmployeeCompensationLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_locked = ' . $this->sqlIsLocked(). ',' ; } elseif( true == array_key_exists( 'IsLocked', $this->getChangedColumns() ) ) { $strSql .= ' is_locked = ' . $this->sqlIsLocked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pay_review_year = ' . $this->sqlPayReviewYear(). ',' ; } elseif( true == array_key_exists( 'PayReviewYear', $this->getChangedColumns() ) ) { $strSql .= ' pay_review_year = ' . $this->sqlPayReviewYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy(). ',' ; } elseif( true == array_key_exists( 'RejectedBy', $this->getChangedColumns() ) ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn(). ',' ; } elseif( true == array_key_exists( 'RejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' submitted_by = ' . $this->sqlSubmittedBy(). ',' ; } elseif( true == array_key_exists( 'SubmittedBy', $this->getChangedColumns() ) ) { $strSql .= ' submitted_by = ' . $this->sqlSubmittedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' submitted_on = ' . $this->sqlSubmittedOn(). ',' ; } elseif( true == array_key_exists( 'SubmittedOn', $this->getChangedColumns() ) ) { $strSql .= ' submitted_on = ' . $this->sqlSubmittedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'manager_employee_id' => $this->getManagerEmployeeId(),
			'proposed_designation_id' => $this->getProposedDesignationId(),
			'pending_increase_encryption_association_id' => $this->getPendingIncreaseEncryptionAssociationId(),
			'pending_hourly_encryption_association_id' => $this->getPendingHourlyEncryptionAssociationId(),
			'employee_compensation_log_id' => $this->getEmployeeCompensationLogId(),
			'is_locked' => $this->getIsLocked(),
			'pay_review_year' => $this->getPayReviewYear(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'rejected_by' => $this->getRejectedBy(),
			'rejected_on' => $this->getRejectedOn(),
			'submitted_by' => $this->getSubmittedBy(),
			'submitted_on' => $this->getSubmittedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>
