<?php

class CBaseSemAdSource extends CEosSingularBase {

	const TABLE_NAME = 'public.sem_ad_sources';

	protected $m_intId;
	protected $m_intSemSourceId;
	protected $m_intSemAdGroupId;
	protected $m_intSemAdId;
	protected $m_intSemAdStatusTypeId;
	protected $m_intSemStatusTypeId;
	protected $m_strRemotePrimaryKey;
	protected $m_strSyncRequestedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['sem_source_id'] ) && $boolDirectSet ) $this->set( 'm_intSemSourceId', trim( $arrValues['sem_source_id'] ) ); elseif( isset( $arrValues['sem_source_id'] ) ) $this->setSemSourceId( $arrValues['sem_source_id'] );
		if( isset( $arrValues['sem_ad_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdGroupId', trim( $arrValues['sem_ad_group_id'] ) ); elseif( isset( $arrValues['sem_ad_group_id'] ) ) $this->setSemAdGroupId( $arrValues['sem_ad_group_id'] );
		if( isset( $arrValues['sem_ad_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdId', trim( $arrValues['sem_ad_id'] ) ); elseif( isset( $arrValues['sem_ad_id'] ) ) $this->setSemAdId( $arrValues['sem_ad_id'] );
		if( isset( $arrValues['sem_ad_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdStatusTypeId', trim( $arrValues['sem_ad_status_type_id'] ) ); elseif( isset( $arrValues['sem_ad_status_type_id'] ) ) $this->setSemAdStatusTypeId( $arrValues['sem_ad_status_type_id'] );
		if( isset( $arrValues['sem_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSemStatusTypeId', trim( $arrValues['sem_status_type_id'] ) ); elseif( isset( $arrValues['sem_status_type_id'] ) ) $this->setSemStatusTypeId( $arrValues['sem_status_type_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['sync_requested_on'] ) && $boolDirectSet ) $this->set( 'm_strSyncRequestedOn', trim( $arrValues['sync_requested_on'] ) ); elseif( isset( $arrValues['sync_requested_on'] ) ) $this->setSyncRequestedOn( $arrValues['sync_requested_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSemSourceId( $intSemSourceId ) {
		$this->set( 'm_intSemSourceId', CStrings::strToIntDef( $intSemSourceId, NULL, false ) );
	}

	public function getSemSourceId() {
		return $this->m_intSemSourceId;
	}

	public function sqlSemSourceId() {
		return ( true == isset( $this->m_intSemSourceId ) ) ? ( string ) $this->m_intSemSourceId : 'NULL';
	}

	public function setSemAdGroupId( $intSemAdGroupId ) {
		$this->set( 'm_intSemAdGroupId', CStrings::strToIntDef( $intSemAdGroupId, NULL, false ) );
	}

	public function getSemAdGroupId() {
		return $this->m_intSemAdGroupId;
	}

	public function sqlSemAdGroupId() {
		return ( true == isset( $this->m_intSemAdGroupId ) ) ? ( string ) $this->m_intSemAdGroupId : 'NULL';
	}

	public function setSemAdId( $intSemAdId ) {
		$this->set( 'm_intSemAdId', CStrings::strToIntDef( $intSemAdId, NULL, false ) );
	}

	public function getSemAdId() {
		return $this->m_intSemAdId;
	}

	public function sqlSemAdId() {
		return ( true == isset( $this->m_intSemAdId ) ) ? ( string ) $this->m_intSemAdId : 'NULL';
	}

	public function setSemAdStatusTypeId( $intSemAdStatusTypeId ) {
		$this->set( 'm_intSemAdStatusTypeId', CStrings::strToIntDef( $intSemAdStatusTypeId, NULL, false ) );
	}

	public function getSemAdStatusTypeId() {
		return $this->m_intSemAdStatusTypeId;
	}

	public function sqlSemAdStatusTypeId() {
		return ( true == isset( $this->m_intSemAdStatusTypeId ) ) ? ( string ) $this->m_intSemAdStatusTypeId : 'NULL';
	}

	public function setSemStatusTypeId( $intSemStatusTypeId ) {
		$this->set( 'm_intSemStatusTypeId', CStrings::strToIntDef( $intSemStatusTypeId, NULL, false ) );
	}

	public function getSemStatusTypeId() {
		return $this->m_intSemStatusTypeId;
	}

	public function sqlSemStatusTypeId() {
		return ( true == isset( $this->m_intSemStatusTypeId ) ) ? ( string ) $this->m_intSemStatusTypeId : 'NULL';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setSyncRequestedOn( $strSyncRequestedOn ) {
		$this->set( 'm_strSyncRequestedOn', CStrings::strTrimDef( $strSyncRequestedOn, -1, NULL, true ) );
	}

	public function getSyncRequestedOn() {
		return $this->m_strSyncRequestedOn;
	}

	public function sqlSyncRequestedOn() {
		return ( true == isset( $this->m_strSyncRequestedOn ) ) ? '\'' . $this->m_strSyncRequestedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, sem_source_id, sem_ad_group_id, sem_ad_id, sem_ad_status_type_id, sem_status_type_id, remote_primary_key, sync_requested_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSemSourceId() . ', ' .
 						$this->sqlSemAdGroupId() . ', ' .
 						$this->sqlSemAdId() . ', ' .
 						$this->sqlSemAdStatusTypeId() . ', ' .
 						$this->sqlSemStatusTypeId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlSyncRequestedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; } elseif( true == array_key_exists( 'SemSourceId', $this->getChangedColumns() ) ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; } elseif( true == array_key_exists( 'SemAdGroupId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_id = ' . $this->sqlSemAdId() . ','; } elseif( true == array_key_exists( 'SemAdId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_id = ' . $this->sqlSemAdId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_status_type_id = ' . $this->sqlSemAdStatusTypeId() . ','; } elseif( true == array_key_exists( 'SemAdStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_status_type_id = ' . $this->sqlSemAdStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_status_type_id = ' . $this->sqlSemStatusTypeId() . ','; } elseif( true == array_key_exists( 'SemStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' sem_status_type_id = ' . $this->sqlSemStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sync_requested_on = ' . $this->sqlSyncRequestedOn() . ','; } elseif( true == array_key_exists( 'SyncRequestedOn', $this->getChangedColumns() ) ) { $strSql .= ' sync_requested_on = ' . $this->sqlSyncRequestedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'sem_source_id' => $this->getSemSourceId(),
			'sem_ad_group_id' => $this->getSemAdGroupId(),
			'sem_ad_id' => $this->getSemAdId(),
			'sem_ad_status_type_id' => $this->getSemAdStatusTypeId(),
			'sem_status_type_id' => $this->getSemStatusTypeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'sync_requested_on' => $this->getSyncRequestedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>