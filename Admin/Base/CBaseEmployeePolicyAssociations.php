<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePolicyAssociations
 * Do not add any new functions to this class.
 */

class CBaseEmployeePolicyAssociations extends CEosPluralBase {

	/**
	 * @return CEmployeePolicyAssociation[]
	 */
	public static function fetchEmployeePolicyAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeePolicyAssociation', $objDatabase );
	}

	/**
	 * @return CEmployeePolicyAssociation
	 */
	public static function fetchEmployeePolicyAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeePolicyAssociation', $objDatabase );
	}

	public static function fetchEmployeePolicyAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_policy_associations', $objDatabase );
	}

	public static function fetchEmployeePolicyAssociationById( $intId, $objDatabase ) {
		return self::fetchEmployeePolicyAssociation( sprintf( 'SELECT * FROM employee_policy_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeePolicyAssociationsByPolicyId( $intPolicyId, $objDatabase ) {
		return self::fetchEmployeePolicyAssociations( sprintf( 'SELECT * FROM employee_policy_associations WHERE policy_id = %d', ( int ) $intPolicyId ), $objDatabase );
	}

}
?>