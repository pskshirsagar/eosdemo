<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemCallLogs
 * Do not add any new functions to this class.
 */

class CBaseSemCallLogs extends CEosPluralBase {

	/**
	 * @return CSemCallLog[]
	 */
	public static function fetchSemCallLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemCallLog', $objDatabase );
	}

	/**
	 * @return CSemCallLog
	 */
	public static function fetchSemCallLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemCallLog', $objDatabase );
	}

	public static function fetchSemCallLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_call_logs', $objDatabase );
	}

	public static function fetchSemCallLogById( $intId, $objDatabase ) {
		return self::fetchSemCallLog( sprintf( 'SELECT * FROM sem_call_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemCallLogsByCid( $intCid, $objDatabase ) {
		return self::fetchSemCallLogs( sprintf( 'SELECT * FROM sem_call_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSemCallLogsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSemCallLogs( sprintf( 'SELECT * FROM sem_call_logs WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchSemCallLogsByCallId( $intCallId, $objDatabase ) {
		return self::fetchSemCallLogs( sprintf( 'SELECT * FROM sem_call_logs WHERE call_id = %d', ( int ) $intCallId ), $objDatabase );
	}

	public static function fetchSemCallLogsBySemSourceId( $intSemSourceId, $objDatabase ) {
		return self::fetchSemCallLogs( sprintf( 'SELECT * FROM sem_call_logs WHERE sem_source_id = %d', ( int ) $intSemSourceId ), $objDatabase );
	}

	public static function fetchSemCallLogsBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemCallLogs( sprintf( 'SELECT * FROM sem_call_logs WHERE sem_ad_group_id = %d', ( int ) $intSemAdGroupId ), $objDatabase );
	}

	public static function fetchSemCallLogsBySemKeywordId( $intSemKeywordId, $objDatabase ) {
		return self::fetchSemCallLogs( sprintf( 'SELECT * FROM sem_call_logs WHERE sem_keyword_id = %d', ( int ) $intSemKeywordId ), $objDatabase );
	}

	public static function fetchSemCallLogsByCallPhoneNumberId( $intCallPhoneNumberId, $objDatabase ) {
		return self::fetchSemCallLogs( sprintf( 'SELECT * FROM sem_call_logs WHERE call_phone_number_id = %d', ( int ) $intCallPhoneNumberId ), $objDatabase );
	}

}
?>