<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CClientNameTypes
 * Do not add any new functions to this class.
 */

class CBaseClientNameTypes extends CEosPluralBase {

	/**
	 * @return CClientNameType[]
	 */
	public static function fetchClientNameTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CClientNameType', $objDatabase );
	}

	/**
	 * @return CClientNameType
	 */
	public static function fetchClientNameType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CClientNameType', $objDatabase );
	}

	public static function fetchClientNameTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'client_name_types', $objDatabase );
	}

	public static function fetchClientNameTypeById( $intId, $objDatabase ) {
		return self::fetchClientNameType( sprintf( 'SELECT * FROM client_name_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>