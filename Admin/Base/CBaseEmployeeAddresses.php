<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAddresses
 * Do not add any new functions to this class.
 */

class CBaseEmployeeAddresses extends CEosPluralBase {

	/**
	 * @return CEmployeeAddress[]
	 */
	public static function fetchEmployeeAddresses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeAddress::class, $objDatabase );
	}

	/**
	 * @return CEmployeeAddress
	 */
	public static function fetchEmployeeAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeAddress::class, $objDatabase );
	}

	public static function fetchEmployeeAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_addresses', $objDatabase );
	}

	public static function fetchEmployeeAddressById( $intId, $objDatabase ) {
		return self::fetchEmployeeAddress( sprintf( 'SELECT * FROM employee_addresses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeAddressesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeAddresses( sprintf( 'SELECT * FROM employee_addresses WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeAddressesByAddressTypeId( $intAddressTypeId, $objDatabase ) {
		return self::fetchEmployeeAddresses( sprintf( 'SELECT * FROM employee_addresses WHERE address_type_id = %d', ( int ) $intAddressTypeId ), $objDatabase );
	}

	public static function fetchEmployeeAddressesByCountryStateId( $intCountryStateId, $objDatabase ) {
		return self::fetchEmployeeAddresses( sprintf( 'SELECT * FROM employee_addresses WHERE country_state_id = %d', ( int ) $intCountryStateId ), $objDatabase );
	}

}
?>