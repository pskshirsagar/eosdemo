<?php

class CBaseSemAdGroup extends CEosSingularBase {

	const TABLE_NAME = 'public.sem_ad_groups';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intSemCampaignId;
	protected $m_intSemLocationTypeId;
	protected $m_strName;
	protected $m_strMetaTitle;
	protected $m_strMetaKeywords;
	protected $m_strMetaDescription;
	protected $m_strSeoContent;
	protected $m_fltLongitude;
	protected $m_fltLatitude;
	protected $m_intPopulation;
	protected $m_intIsSystem;
	protected $m_intIsPublished;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsSystem = '0';
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['sem_campaign_id'] ) && $boolDirectSet ) $this->set( 'm_intSemCampaignId', trim( $arrValues['sem_campaign_id'] ) ); elseif( isset( $arrValues['sem_campaign_id'] ) ) $this->setSemCampaignId( $arrValues['sem_campaign_id'] );
		if( isset( $arrValues['sem_location_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSemLocationTypeId', trim( $arrValues['sem_location_type_id'] ) ); elseif( isset( $arrValues['sem_location_type_id'] ) ) $this->setSemLocationTypeId( $arrValues['sem_location_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['meta_title'] ) && $boolDirectSet ) $this->set( 'm_strMetaTitle', trim( stripcslashes( $arrValues['meta_title'] ) ) ); elseif( isset( $arrValues['meta_title'] ) ) $this->setMetaTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['meta_title'] ) : $arrValues['meta_title'] );
		if( isset( $arrValues['meta_keywords'] ) && $boolDirectSet ) $this->set( 'm_strMetaKeywords', trim( stripcslashes( $arrValues['meta_keywords'] ) ) ); elseif( isset( $arrValues['meta_keywords'] ) ) $this->setMetaKeywords( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['meta_keywords'] ) : $arrValues['meta_keywords'] );
		if( isset( $arrValues['meta_description'] ) && $boolDirectSet ) $this->set( 'm_strMetaDescription', trim( stripcslashes( $arrValues['meta_description'] ) ) ); elseif( isset( $arrValues['meta_description'] ) ) $this->setMetaDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['meta_description'] ) : $arrValues['meta_description'] );
		if( isset( $arrValues['seo_content'] ) && $boolDirectSet ) $this->set( 'm_strSeoContent', trim( stripcslashes( $arrValues['seo_content'] ) ) ); elseif( isset( $arrValues['seo_content'] ) ) $this->setSeoContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['seo_content'] ) : $arrValues['seo_content'] );
		if( isset( $arrValues['longitude'] ) && $boolDirectSet ) $this->set( 'm_fltLongitude', trim( $arrValues['longitude'] ) ); elseif( isset( $arrValues['longitude'] ) ) $this->setLongitude( $arrValues['longitude'] );
		if( isset( $arrValues['latitude'] ) && $boolDirectSet ) $this->set( 'm_fltLatitude', trim( $arrValues['latitude'] ) ); elseif( isset( $arrValues['latitude'] ) ) $this->setLatitude( $arrValues['latitude'] );
		if( isset( $arrValues['population'] ) && $boolDirectSet ) $this->set( 'm_intPopulation', trim( $arrValues['population'] ) ); elseif( isset( $arrValues['population'] ) ) $this->setPopulation( $arrValues['population'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setSemCampaignId( $intSemCampaignId ) {
		$this->set( 'm_intSemCampaignId', CStrings::strToIntDef( $intSemCampaignId, NULL, false ) );
	}

	public function getSemCampaignId() {
		return $this->m_intSemCampaignId;
	}

	public function sqlSemCampaignId() {
		return ( true == isset( $this->m_intSemCampaignId ) ) ? ( string ) $this->m_intSemCampaignId : 'NULL';
	}

	public function setSemLocationTypeId( $intSemLocationTypeId ) {
		$this->set( 'm_intSemLocationTypeId', CStrings::strToIntDef( $intSemLocationTypeId, NULL, false ) );
	}

	public function getSemLocationTypeId() {
		return $this->m_intSemLocationTypeId;
	}

	public function sqlSemLocationTypeId() {
		return ( true == isset( $this->m_intSemLocationTypeId ) ) ? ( string ) $this->m_intSemLocationTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setMetaTitle( $strMetaTitle ) {
		$this->set( 'm_strMetaTitle', CStrings::strTrimDef( $strMetaTitle, 240, NULL, true ) );
	}

	public function getMetaTitle() {
		return $this->m_strMetaTitle;
	}

	public function sqlMetaTitle() {
		return ( true == isset( $this->m_strMetaTitle ) ) ? '\'' . addslashes( $this->m_strMetaTitle ) . '\'' : 'NULL';
	}

	public function setMetaKeywords( $strMetaKeywords ) {
		$this->set( 'm_strMetaKeywords', CStrings::strTrimDef( $strMetaKeywords, 4096, NULL, true ) );
	}

	public function getMetaKeywords() {
		return $this->m_strMetaKeywords;
	}

	public function sqlMetaKeywords() {
		return ( true == isset( $this->m_strMetaKeywords ) ) ? '\'' . addslashes( $this->m_strMetaKeywords ) . '\'' : 'NULL';
	}

	public function setMetaDescription( $strMetaDescription ) {
		$this->set( 'm_strMetaDescription', CStrings::strTrimDef( $strMetaDescription, 240, NULL, true ) );
	}

	public function getMetaDescription() {
		return $this->m_strMetaDescription;
	}

	public function sqlMetaDescription() {
		return ( true == isset( $this->m_strMetaDescription ) ) ? '\'' . addslashes( $this->m_strMetaDescription ) . '\'' : 'NULL';
	}

	public function setSeoContent( $strSeoContent ) {
		$this->set( 'm_strSeoContent', CStrings::strTrimDef( $strSeoContent, -1, NULL, true ) );
	}

	public function getSeoContent() {
		return $this->m_strSeoContent;
	}

	public function sqlSeoContent() {
		return ( true == isset( $this->m_strSeoContent ) ) ? '\'' . addslashes( $this->m_strSeoContent ) . '\'' : 'NULL';
	}

	public function setLongitude( $fltLongitude ) {
		$this->set( 'm_fltLongitude', CStrings::strToFloatDef( $fltLongitude, NULL, false, 0 ) );
	}

	public function getLongitude() {
		return $this->m_fltLongitude;
	}

	public function sqlLongitude() {
		return ( true == isset( $this->m_fltLongitude ) ) ? ( string ) $this->m_fltLongitude : 'NULL';
	}

	public function setLatitude( $fltLatitude ) {
		$this->set( 'm_fltLatitude', CStrings::strToFloatDef( $fltLatitude, NULL, false, 0 ) );
	}

	public function getLatitude() {
		return $this->m_fltLatitude;
	}

	public function sqlLatitude() {
		return ( true == isset( $this->m_fltLatitude ) ) ? ( string ) $this->m_fltLatitude : 'NULL';
	}

	public function setPopulation( $intPopulation ) {
		$this->set( 'm_intPopulation', CStrings::strToIntDef( $intPopulation, NULL, false ) );
	}

	public function getPopulation() {
		return $this->m_intPopulation;
	}

	public function sqlPopulation() {
		return ( true == isset( $this->m_intPopulation ) ) ? ( string ) $this->m_intPopulation : 'NULL';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, sem_campaign_id, sem_location_type_id, name, meta_title, meta_keywords, meta_description, seo_content, longitude, latitude, population, is_system, is_published, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlSemCampaignId() . ', ' .
 						$this->sqlSemLocationTypeId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlMetaTitle() . ', ' .
 						$this->sqlMetaKeywords() . ', ' .
 						$this->sqlMetaDescription() . ', ' .
 						$this->sqlSeoContent() . ', ' .
 						$this->sqlLongitude() . ', ' .
 						$this->sqlLatitude() . ', ' .
 						$this->sqlPopulation() . ', ' .
 						$this->sqlIsSystem() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_campaign_id = ' . $this->sqlSemCampaignId() . ','; } elseif( true == array_key_exists( 'SemCampaignId', $this->getChangedColumns() ) ) { $strSql .= ' sem_campaign_id = ' . $this->sqlSemCampaignId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_location_type_id = ' . $this->sqlSemLocationTypeId() . ','; } elseif( true == array_key_exists( 'SemLocationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' sem_location_type_id = ' . $this->sqlSemLocationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meta_title = ' . $this->sqlMetaTitle() . ','; } elseif( true == array_key_exists( 'MetaTitle', $this->getChangedColumns() ) ) { $strSql .= ' meta_title = ' . $this->sqlMetaTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meta_keywords = ' . $this->sqlMetaKeywords() . ','; } elseif( true == array_key_exists( 'MetaKeywords', $this->getChangedColumns() ) ) { $strSql .= ' meta_keywords = ' . $this->sqlMetaKeywords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' meta_description = ' . $this->sqlMetaDescription() . ','; } elseif( true == array_key_exists( 'MetaDescription', $this->getChangedColumns() ) ) { $strSql .= ' meta_description = ' . $this->sqlMetaDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' seo_content = ' . $this->sqlSeoContent() . ','; } elseif( true == array_key_exists( 'SeoContent', $this->getChangedColumns() ) ) { $strSql .= ' seo_content = ' . $this->sqlSeoContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; } elseif( true == array_key_exists( 'Longitude', $this->getChangedColumns() ) ) { $strSql .= ' longitude = ' . $this->sqlLongitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; } elseif( true == array_key_exists( 'Latitude', $this->getChangedColumns() ) ) { $strSql .= ' latitude = ' . $this->sqlLatitude() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' population = ' . $this->sqlPopulation() . ','; } elseif( true == array_key_exists( 'Population', $this->getChangedColumns() ) ) { $strSql .= ' population = ' . $this->sqlPopulation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'sem_campaign_id' => $this->getSemCampaignId(),
			'sem_location_type_id' => $this->getSemLocationTypeId(),
			'name' => $this->getName(),
			'meta_title' => $this->getMetaTitle(),
			'meta_keywords' => $this->getMetaKeywords(),
			'meta_description' => $this->getMetaDescription(),
			'seo_content' => $this->getSeoContent(),
			'longitude' => $this->getLongitude(),
			'latitude' => $this->getLatitude(),
			'population' => $this->getPopulation(),
			'is_system' => $this->getIsSystem(),
			'is_published' => $this->getIsPublished(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>