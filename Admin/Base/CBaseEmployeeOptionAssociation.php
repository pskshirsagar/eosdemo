<?php

class CBaseEmployeeOptionAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_option_associations';

	protected $m_intId;
	protected $m_intViewerEmployeeId;
	protected $m_intEmployeeId;
	protected $m_strEncryptedValues;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['viewer_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intViewerEmployeeId', trim( $arrValues['viewer_employee_id'] ) ); elseif( isset( $arrValues['viewer_employee_id'] ) ) $this->setViewerEmployeeId( $arrValues['viewer_employee_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['encrypted_values'] ) && $boolDirectSet ) $this->set( 'm_strEncryptedValues', trim( stripcslashes( $arrValues['encrypted_values'] ) ) ); elseif( isset( $arrValues['encrypted_values'] ) ) $this->setEncryptedValues( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['encrypted_values'] ) : $arrValues['encrypted_values'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setViewerEmployeeId( $intViewerEmployeeId ) {
		$this->set( 'm_intViewerEmployeeId', CStrings::strToIntDef( $intViewerEmployeeId, NULL, false ) );
	}

	public function getViewerEmployeeId() {
		return $this->m_intViewerEmployeeId;
	}

	public function sqlViewerEmployeeId() {
		return ( true == isset( $this->m_intViewerEmployeeId ) ) ? ( string ) $this->m_intViewerEmployeeId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setEncryptedValues( $strEncryptedValues ) {
		$this->set( 'm_strEncryptedValues', CStrings::strTrimDef( $strEncryptedValues, -1, NULL, true ) );
	}

	public function getEncryptedValues() {
		return $this->m_strEncryptedValues;
	}

	public function sqlEncryptedValues() {
		return ( true == isset( $this->m_strEncryptedValues ) ) ? '\'' . addslashes( $this->m_strEncryptedValues ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, viewer_employee_id, employee_id, encrypted_values, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlViewerEmployeeId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlEncryptedValues() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewer_employee_id = ' . $this->sqlViewerEmployeeId() . ','; } elseif( true == array_key_exists( 'ViewerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' viewer_employee_id = ' . $this->sqlViewerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' encrypted_values = ' . $this->sqlEncryptedValues() . ','; } elseif( true == array_key_exists( 'EncryptedValues', $this->getChangedColumns() ) ) { $strSql .= ' encrypted_values = ' . $this->sqlEncryptedValues() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'viewer_employee_id' => $this->getViewerEmployeeId(),
			'employee_id' => $this->getEmployeeId(),
			'encrypted_values' => $this->getEncryptedValues(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>