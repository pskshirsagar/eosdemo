<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplateQuestionGroups
 * Do not add any new functions to this class.
 */

class CBaseSurveyTemplateQuestionGroups extends CEosPluralBase {

	/**
	 * @return CSurveyTemplateQuestionGroup[]
	 */
	public static function fetchSurveyTemplateQuestionGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyTemplateQuestionGroup', $objDatabase );
	}

	/**
	 * @return CSurveyTemplateQuestionGroup
	 */
	public static function fetchSurveyTemplateQuestionGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyTemplateQuestionGroup', $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_template_question_groups', $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionGroupById( $intId, $objDatabase ) {
		return self::fetchSurveyTemplateQuestionGroup( sprintf( 'SELECT * FROM survey_template_question_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionGroupsBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		return self::fetchSurveyTemplateQuestionGroups( sprintf( 'SELECT * FROM survey_template_question_groups WHERE survey_template_id = %d', ( int ) $intSurveyTemplateId ), $objDatabase );
	}

}
?>