<?php

class CBaseDocumentTemplateParameter extends CEosSingularBase {

	const TABLE_NAME = 'public.document_template_parameters';

	protected $m_intId;
	protected $m_intDocumentTemplateId;
	protected $m_intDocumentTemplateComponentId;
	protected $m_strKey;
	protected $m_strValue;
	protected $m_intOrderNum;
	protected $m_intIsModifiable;
	protected $m_intIsRemovable;
	protected $m_intIsRequired;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['document_template_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentTemplateId', trim( $arrValues['document_template_id'] ) ); elseif( isset( $arrValues['document_template_id'] ) ) $this->setDocumentTemplateId( $arrValues['document_template_id'] );
		if( isset( $arrValues['document_template_component_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentTemplateComponentId', trim( $arrValues['document_template_component_id'] ) ); elseif( isset( $arrValues['document_template_component_id'] ) ) $this->setDocumentTemplateComponentId( $arrValues['document_template_component_id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( stripcslashes( $arrValues['key'] ) ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
		if( isset( $arrValues['value'] ) && $boolDirectSet ) $this->set( 'm_strValue', trim( stripcslashes( $arrValues['value'] ) ) ); elseif( isset( $arrValues['value'] ) ) $this->setValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['value'] ) : $arrValues['value'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['is_modifiable'] ) && $boolDirectSet ) $this->set( 'm_intIsModifiable', trim( $arrValues['is_modifiable'] ) ); elseif( isset( $arrValues['is_modifiable'] ) ) $this->setIsModifiable( $arrValues['is_modifiable'] );
		if( isset( $arrValues['is_removable'] ) && $boolDirectSet ) $this->set( 'm_intIsRemovable', trim( $arrValues['is_removable'] ) ); elseif( isset( $arrValues['is_removable'] ) ) $this->setIsRemovable( $arrValues['is_removable'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_intIsRequired', trim( $arrValues['is_required'] ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( $arrValues['is_required'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDocumentTemplateId( $intDocumentTemplateId ) {
		$this->set( 'm_intDocumentTemplateId', CStrings::strToIntDef( $intDocumentTemplateId, NULL, false ) );
	}

	public function getDocumentTemplateId() {
		return $this->m_intDocumentTemplateId;
	}

	public function sqlDocumentTemplateId() {
		return ( true == isset( $this->m_intDocumentTemplateId ) ) ? ( string ) $this->m_intDocumentTemplateId : 'NULL';
	}

	public function setDocumentTemplateComponentId( $intDocumentTemplateComponentId ) {
		$this->set( 'm_intDocumentTemplateComponentId', CStrings::strToIntDef( $intDocumentTemplateComponentId, NULL, false ) );
	}

	public function getDocumentTemplateComponentId() {
		return $this->m_intDocumentTemplateComponentId;
	}

	public function sqlDocumentTemplateComponentId() {
		return ( true == isset( $this->m_intDocumentTemplateComponentId ) ) ? ( string ) $this->m_intDocumentTemplateComponentId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 64, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
	}

	public function setValue( $strValue ) {
		$this->set( 'm_strValue', CStrings::strTrimDef( $strValue, -1, NULL, true ) );
	}

	public function getValue() {
		return $this->m_strValue;
	}

	public function sqlValue() {
		return ( true == isset( $this->m_strValue ) ) ? '\'' . addslashes( $this->m_strValue ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setIsModifiable( $intIsModifiable ) {
		$this->set( 'm_intIsModifiable', CStrings::strToIntDef( $intIsModifiable, NULL, false ) );
	}

	public function getIsModifiable() {
		return $this->m_intIsModifiable;
	}

	public function sqlIsModifiable() {
		return ( true == isset( $this->m_intIsModifiable ) ) ? ( string ) $this->m_intIsModifiable : 'NULL';
	}

	public function setIsRemovable( $intIsRemovable ) {
		$this->set( 'm_intIsRemovable', CStrings::strToIntDef( $intIsRemovable, NULL, false ) );
	}

	public function getIsRemovable() {
		return $this->m_intIsRemovable;
	}

	public function sqlIsRemovable() {
		return ( true == isset( $this->m_intIsRemovable ) ) ? ( string ) $this->m_intIsRemovable : 'NULL';
	}

	public function setIsRequired( $intIsRequired ) {
		$this->set( 'm_intIsRequired', CStrings::strToIntDef( $intIsRequired, NULL, false ) );
	}

	public function getIsRequired() {
		return $this->m_intIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_intIsRequired ) ) ? ( string ) $this->m_intIsRequired : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, document_template_id, document_template_component_id, key, value, order_num, is_modifiable, is_removable, is_required, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDocumentTemplateId() . ', ' .
 						$this->sqlDocumentTemplateComponentId() . ', ' .
 						$this->sqlKey() . ', ' .
 						$this->sqlValue() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlIsModifiable() . ', ' .
 						$this->sqlIsRemovable() . ', ' .
 						$this->sqlIsRequired() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_template_id = ' . $this->sqlDocumentTemplateId() . ','; } elseif( true == array_key_exists( 'DocumentTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' document_template_id = ' . $this->sqlDocumentTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_template_component_id = ' . $this->sqlDocumentTemplateComponentId() . ','; } elseif( true == array_key_exists( 'DocumentTemplateComponentId', $this->getChangedColumns() ) ) { $strSql .= ' document_template_component_id = ' . $this->sqlDocumentTemplateComponentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey() . ','; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value = ' . $this->sqlValue() . ','; } elseif( true == array_key_exists( 'Value', $this->getChangedColumns() ) ) { $strSql .= ' value = ' . $this->sqlValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_modifiable = ' . $this->sqlIsModifiable() . ','; } elseif( true == array_key_exists( 'IsModifiable', $this->getChangedColumns() ) ) { $strSql .= ' is_modifiable = ' . $this->sqlIsModifiable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_removable = ' . $this->sqlIsRemovable() . ','; } elseif( true == array_key_exists( 'IsRemovable', $this->getChangedColumns() ) ) { $strSql .= ' is_removable = ' . $this->sqlIsRemovable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; } elseif( true == array_key_exists( 'IsRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'document_template_id' => $this->getDocumentTemplateId(),
			'document_template_component_id' => $this->getDocumentTemplateComponentId(),
			'key' => $this->getKey(),
			'value' => $this->getValue(),
			'order_num' => $this->getOrderNum(),
			'is_modifiable' => $this->getIsModifiable(),
			'is_removable' => $this->getIsRemovable(),
			'is_required' => $this->getIsRequired(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>