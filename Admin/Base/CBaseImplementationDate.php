<?php

class CBaseImplementationDate extends CEosSingularBase {

	const TABLE_NAME = 'public.implementation_dates';

	protected $m_intId;
	protected $m_intContractPropertyId;
	protected $m_intContractImplementationRequestId;
	protected $m_strImplementationStartDate;
	protected $m_strImplementationEndDate;
	protected $m_strConfirmationStatus;
	protected $m_strReasonChanged;
	protected $m_strReasonConfirmed;
	protected $m_intConfirmedBy;
	protected $m_strConfirmedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intContractPropertyId', trim( $arrValues['contract_property_id'] ) ); elseif( isset( $arrValues['contract_property_id'] ) ) $this->setContractPropertyId( $arrValues['contract_property_id'] );
		if( isset( $arrValues['contract_implementation_request_id'] ) && $boolDirectSet ) $this->set( 'm_intContractImplementationRequestId', trim( $arrValues['contract_implementation_request_id'] ) ); elseif( isset( $arrValues['contract_implementation_request_id'] ) ) $this->setContractImplementationRequestId( $arrValues['contract_implementation_request_id'] );
		if( isset( $arrValues['implementation_start_date'] ) && $boolDirectSet ) $this->set( 'm_strImplementationStartDate', trim( $arrValues['implementation_start_date'] ) ); elseif( isset( $arrValues['implementation_start_date'] ) ) $this->setImplementationStartDate( $arrValues['implementation_start_date'] );
		if( isset( $arrValues['implementation_end_date'] ) && $boolDirectSet ) $this->set( 'm_strImplementationEndDate', trim( $arrValues['implementation_end_date'] ) ); elseif( isset( $arrValues['implementation_end_date'] ) ) $this->setImplementationEndDate( $arrValues['implementation_end_date'] );
		if( isset( $arrValues['confirmation_status'] ) && $boolDirectSet ) $this->set( 'm_strConfirmationStatus', trim( stripcslashes( $arrValues['confirmation_status'] ) ) ); elseif( isset( $arrValues['confirmation_status'] ) ) $this->setConfirmationStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['confirmation_status'] ) : $arrValues['confirmation_status'] );
		if( isset( $arrValues['reason_changed'] ) && $boolDirectSet ) $this->set( 'm_strReasonChanged', trim( stripcslashes( $arrValues['reason_changed'] ) ) ); elseif( isset( $arrValues['reason_changed'] ) ) $this->setReasonChanged( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reason_changed'] ) : $arrValues['reason_changed'] );
		if( isset( $arrValues['reason_confirmed'] ) && $boolDirectSet ) $this->set( 'm_strReasonConfirmed', trim( stripcslashes( $arrValues['reason_confirmed'] ) ) ); elseif( isset( $arrValues['reason_confirmed'] ) ) $this->setReasonConfirmed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reason_confirmed'] ) : $arrValues['reason_confirmed'] );
		if( isset( $arrValues['confirmed_by'] ) && $boolDirectSet ) $this->set( 'm_intConfirmedBy', trim( $arrValues['confirmed_by'] ) ); elseif( isset( $arrValues['confirmed_by'] ) ) $this->setConfirmedBy( $arrValues['confirmed_by'] );
		if( isset( $arrValues['confirmed_on'] ) && $boolDirectSet ) $this->set( 'm_strConfirmedOn', trim( $arrValues['confirmed_on'] ) ); elseif( isset( $arrValues['confirmed_on'] ) ) $this->setConfirmedOn( $arrValues['confirmed_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setContractPropertyId( $intContractPropertyId ) {
		$this->set( 'm_intContractPropertyId', CStrings::strToIntDef( $intContractPropertyId, NULL, false ) );
	}

	public function getContractPropertyId() {
		return $this->m_intContractPropertyId;
	}

	public function sqlContractPropertyId() {
		return ( true == isset( $this->m_intContractPropertyId ) ) ? ( string ) $this->m_intContractPropertyId : 'NULL';
	}

	public function setContractImplementationRequestId( $intContractImplementationRequestId ) {
		$this->set( 'm_intContractImplementationRequestId', CStrings::strToIntDef( $intContractImplementationRequestId, NULL, false ) );
	}

	public function getContractImplementationRequestId() {
		return $this->m_intContractImplementationRequestId;
	}

	public function sqlContractImplementationRequestId() {
		return ( true == isset( $this->m_intContractImplementationRequestId ) ) ? ( string ) $this->m_intContractImplementationRequestId : 'NULL';
	}

	public function setImplementationStartDate( $strImplementationStartDate ) {
		$this->set( 'm_strImplementationStartDate', CStrings::strTrimDef( $strImplementationStartDate, -1, NULL, true ) );
	}

	public function getImplementationStartDate() {
		return $this->m_strImplementationStartDate;
	}

	public function sqlImplementationStartDate() {
		return ( true == isset( $this->m_strImplementationStartDate ) ) ? '\'' . $this->m_strImplementationStartDate . '\'' : 'NOW()';
	}

	public function setImplementationEndDate( $strImplementationEndDate ) {
		$this->set( 'm_strImplementationEndDate', CStrings::strTrimDef( $strImplementationEndDate, -1, NULL, true ) );
	}

	public function getImplementationEndDate() {
		return $this->m_strImplementationEndDate;
	}

	public function sqlImplementationEndDate() {
		return ( true == isset( $this->m_strImplementationEndDate ) ) ? '\'' . $this->m_strImplementationEndDate . '\'' : 'NOW()';
	}

	public function setConfirmationStatus( $strConfirmationStatus ) {
		$this->set( 'm_strConfirmationStatus', CStrings::strTrimDef( $strConfirmationStatus, 30, NULL, true ) );
	}

	public function getConfirmationStatus() {
		return $this->m_strConfirmationStatus;
	}

	public function sqlConfirmationStatus() {
		return ( true == isset( $this->m_strConfirmationStatus ) ) ? '\'' . addslashes( $this->m_strConfirmationStatus ) . '\'' : 'NULL';
	}

	public function setReasonChanged( $strReasonChanged ) {
		$this->set( 'm_strReasonChanged', CStrings::strTrimDef( $strReasonChanged, -1, NULL, true ) );
	}

	public function getReasonChanged() {
		return $this->m_strReasonChanged;
	}

	public function sqlReasonChanged() {
		return ( true == isset( $this->m_strReasonChanged ) ) ? '\'' . addslashes( $this->m_strReasonChanged ) . '\'' : 'NULL';
	}

	public function setReasonConfirmed( $strReasonConfirmed ) {
		$this->set( 'm_strReasonConfirmed', CStrings::strTrimDef( $strReasonConfirmed, -1, NULL, true ) );
	}

	public function getReasonConfirmed() {
		return $this->m_strReasonConfirmed;
	}

	public function sqlReasonConfirmed() {
		return ( true == isset( $this->m_strReasonConfirmed ) ) ? '\'' . addslashes( $this->m_strReasonConfirmed ) . '\'' : 'NULL';
	}

	public function setConfirmedBy( $intConfirmedBy ) {
		$this->set( 'm_intConfirmedBy', CStrings::strToIntDef( $intConfirmedBy, NULL, false ) );
	}

	public function getConfirmedBy() {
		return $this->m_intConfirmedBy;
	}

	public function sqlConfirmedBy() {
		return ( true == isset( $this->m_intConfirmedBy ) ) ? ( string ) $this->m_intConfirmedBy : 'NULL';
	}

	public function setConfirmedOn( $strConfirmedOn ) {
		$this->set( 'm_strConfirmedOn', CStrings::strTrimDef( $strConfirmedOn, -1, NULL, true ) );
	}

	public function getConfirmedOn() {
		return $this->m_strConfirmedOn;
	}

	public function sqlConfirmedOn() {
		return ( true == isset( $this->m_strConfirmedOn ) ) ? '\'' . $this->m_strConfirmedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, contract_property_id, contract_implementation_request_id, implementation_start_date, implementation_end_date, confirmation_status, reason_changed, reason_confirmed, confirmed_by, confirmed_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlContractPropertyId() . ', ' .
 						$this->sqlContractImplementationRequestId() . ', ' .
 						$this->sqlImplementationStartDate() . ', ' .
 						$this->sqlImplementationEndDate() . ', ' .
 						$this->sqlConfirmationStatus() . ', ' .
 						$this->sqlReasonChanged() . ', ' .
 						$this->sqlReasonConfirmed() . ', ' .
 						$this->sqlConfirmedBy() . ', ' .
 						$this->sqlConfirmedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_property_id = ' . $this->sqlContractPropertyId() . ','; } elseif( true == array_key_exists( 'ContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' contract_property_id = ' . $this->sqlContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_implementation_request_id = ' . $this->sqlContractImplementationRequestId() . ','; } elseif( true == array_key_exists( 'ContractImplementationRequestId', $this->getChangedColumns() ) ) { $strSql .= ' contract_implementation_request_id = ' . $this->sqlContractImplementationRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_start_date = ' . $this->sqlImplementationStartDate() . ','; } elseif( true == array_key_exists( 'ImplementationStartDate', $this->getChangedColumns() ) ) { $strSql .= ' implementation_start_date = ' . $this->sqlImplementationStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_end_date = ' . $this->sqlImplementationEndDate() . ','; } elseif( true == array_key_exists( 'ImplementationEndDate', $this->getChangedColumns() ) ) { $strSql .= ' implementation_end_date = ' . $this->sqlImplementationEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmation_status = ' . $this->sqlConfirmationStatus() . ','; } elseif( true == array_key_exists( 'ConfirmationStatus', $this->getChangedColumns() ) ) { $strSql .= ' confirmation_status = ' . $this->sqlConfirmationStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_changed = ' . $this->sqlReasonChanged() . ','; } elseif( true == array_key_exists( 'ReasonChanged', $this->getChangedColumns() ) ) { $strSql .= ' reason_changed = ' . $this->sqlReasonChanged() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_confirmed = ' . $this->sqlReasonConfirmed() . ','; } elseif( true == array_key_exists( 'ReasonConfirmed', $this->getChangedColumns() ) ) { $strSql .= ' reason_confirmed = ' . $this->sqlReasonConfirmed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_by = ' . $this->sqlConfirmedBy() . ','; } elseif( true == array_key_exists( 'ConfirmedBy', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_by = ' . $this->sqlConfirmedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; } elseif( true == array_key_exists( 'ConfirmedOn', $this->getChangedColumns() ) ) { $strSql .= ' confirmed_on = ' . $this->sqlConfirmedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'contract_property_id' => $this->getContractPropertyId(),
			'contract_implementation_request_id' => $this->getContractImplementationRequestId(),
			'implementation_start_date' => $this->getImplementationStartDate(),
			'implementation_end_date' => $this->getImplementationEndDate(),
			'confirmation_status' => $this->getConfirmationStatus(),
			'reason_changed' => $this->getReasonChanged(),
			'reason_confirmed' => $this->getReasonConfirmed(),
			'confirmed_by' => $this->getConfirmedBy(),
			'confirmed_on' => $this->getConfirmedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>