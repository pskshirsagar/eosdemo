<?php

class CBaseSurveyTemplateRule extends CEosSingularBase {

	const TABLE_NAME = 'public.survey_template_rules';

	protected $m_intId;
	protected $m_intSurveyRuleTypeId;
	protected $m_intSurveyTemplateRuleSetId;
	protected $m_intSurveyTemplateQuestionGroupId;
	protected $m_intSurveyTemplateQuestionId;
	protected $m_intConditionTemplateQuestionId;
	protected $m_intConditionTemplateQuestionOptionId;
	protected $m_intSurveyConditionTypeId;
	protected $m_intManagerTemplateSurveyId;
	protected $m_intSurveyTemplateTaskId;
	protected $m_intSurveyTemplateEmailId;
	protected $m_strConditionAnswer;
	protected $m_intIsIgnoreCase;
	protected $m_strValidationMessage;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsIgnoreCase = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['survey_rule_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyRuleTypeId', trim( $arrValues['survey_rule_type_id'] ) ); elseif( isset( $arrValues['survey_rule_type_id'] ) ) $this->setSurveyRuleTypeId( $arrValues['survey_rule_type_id'] );
		if( isset( $arrValues['survey_template_rule_set_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateRuleSetId', trim( $arrValues['survey_template_rule_set_id'] ) ); elseif( isset( $arrValues['survey_template_rule_set_id'] ) ) $this->setSurveyTemplateRuleSetId( $arrValues['survey_template_rule_set_id'] );
		if( isset( $arrValues['survey_template_question_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateQuestionGroupId', trim( $arrValues['survey_template_question_group_id'] ) ); elseif( isset( $arrValues['survey_template_question_group_id'] ) ) $this->setSurveyTemplateQuestionGroupId( $arrValues['survey_template_question_group_id'] );
		if( isset( $arrValues['survey_template_question_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateQuestionId', trim( $arrValues['survey_template_question_id'] ) ); elseif( isset( $arrValues['survey_template_question_id'] ) ) $this->setSurveyTemplateQuestionId( $arrValues['survey_template_question_id'] );
		if( isset( $arrValues['condition_template_question_id'] ) && $boolDirectSet ) $this->set( 'm_intConditionTemplateQuestionId', trim( $arrValues['condition_template_question_id'] ) ); elseif( isset( $arrValues['condition_template_question_id'] ) ) $this->setConditionTemplateQuestionId( $arrValues['condition_template_question_id'] );
		if( isset( $arrValues['condition_template_question_option_id'] ) && $boolDirectSet ) $this->set( 'm_intConditionTemplateQuestionOptionId', trim( $arrValues['condition_template_question_option_id'] ) ); elseif( isset( $arrValues['condition_template_question_option_id'] ) ) $this->setConditionTemplateQuestionOptionId( $arrValues['condition_template_question_option_id'] );
		if( isset( $arrValues['survey_condition_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyConditionTypeId', trim( $arrValues['survey_condition_type_id'] ) ); elseif( isset( $arrValues['survey_condition_type_id'] ) ) $this->setSurveyConditionTypeId( $arrValues['survey_condition_type_id'] );
		if( isset( $arrValues['manager_template_survey_id'] ) && $boolDirectSet ) $this->set( 'm_intManagerTemplateSurveyId', trim( $arrValues['manager_template_survey_id'] ) ); elseif( isset( $arrValues['manager_template_survey_id'] ) ) $this->setManagerTemplateSurveyId( $arrValues['manager_template_survey_id'] );
		if( isset( $arrValues['survey_template_task_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateTaskId', trim( $arrValues['survey_template_task_id'] ) ); elseif( isset( $arrValues['survey_template_task_id'] ) ) $this->setSurveyTemplateTaskId( $arrValues['survey_template_task_id'] );
		if( isset( $arrValues['survey_template_email_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateEmailId', trim( $arrValues['survey_template_email_id'] ) ); elseif( isset( $arrValues['survey_template_email_id'] ) ) $this->setSurveyTemplateEmailId( $arrValues['survey_template_email_id'] );
		if( isset( $arrValues['condition_answer'] ) && $boolDirectSet ) $this->set( 'm_strConditionAnswer', trim( stripcslashes( $arrValues['condition_answer'] ) ) ); elseif( isset( $arrValues['condition_answer'] ) ) $this->setConditionAnswer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['condition_answer'] ) : $arrValues['condition_answer'] );
		if( isset( $arrValues['is_ignore_case'] ) && $boolDirectSet ) $this->set( 'm_intIsIgnoreCase', trim( $arrValues['is_ignore_case'] ) ); elseif( isset( $arrValues['is_ignore_case'] ) ) $this->setIsIgnoreCase( $arrValues['is_ignore_case'] );
		if( isset( $arrValues['validation_message'] ) && $boolDirectSet ) $this->set( 'm_strValidationMessage', trim( stripcslashes( $arrValues['validation_message'] ) ) ); elseif( isset( $arrValues['validation_message'] ) ) $this->setValidationMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['validation_message'] ) : $arrValues['validation_message'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSurveyRuleTypeId( $intSurveyRuleTypeId ) {
		$this->set( 'm_intSurveyRuleTypeId', CStrings::strToIntDef( $intSurveyRuleTypeId, NULL, false ) );
	}

	public function getSurveyRuleTypeId() {
		return $this->m_intSurveyRuleTypeId;
	}

	public function sqlSurveyRuleTypeId() {
		return ( true == isset( $this->m_intSurveyRuleTypeId ) ) ? ( string ) $this->m_intSurveyRuleTypeId : 'NULL';
	}

	public function setSurveyTemplateRuleSetId( $intSurveyTemplateRuleSetId ) {
		$this->set( 'm_intSurveyTemplateRuleSetId', CStrings::strToIntDef( $intSurveyTemplateRuleSetId, NULL, false ) );
	}

	public function getSurveyTemplateRuleSetId() {
		return $this->m_intSurveyTemplateRuleSetId;
	}

	public function sqlSurveyTemplateRuleSetId() {
		return ( true == isset( $this->m_intSurveyTemplateRuleSetId ) ) ? ( string ) $this->m_intSurveyTemplateRuleSetId : 'NULL';
	}

	public function setSurveyTemplateQuestionGroupId( $intSurveyTemplateQuestionGroupId ) {
		$this->set( 'm_intSurveyTemplateQuestionGroupId', CStrings::strToIntDef( $intSurveyTemplateQuestionGroupId, NULL, false ) );
	}

	public function getSurveyTemplateQuestionGroupId() {
		return $this->m_intSurveyTemplateQuestionGroupId;
	}

	public function sqlSurveyTemplateQuestionGroupId() {
		return ( true == isset( $this->m_intSurveyTemplateQuestionGroupId ) ) ? ( string ) $this->m_intSurveyTemplateQuestionGroupId : 'NULL';
	}

	public function setSurveyTemplateQuestionId( $intSurveyTemplateQuestionId ) {
		$this->set( 'm_intSurveyTemplateQuestionId', CStrings::strToIntDef( $intSurveyTemplateQuestionId, NULL, false ) );
	}

	public function getSurveyTemplateQuestionId() {
		return $this->m_intSurveyTemplateQuestionId;
	}

	public function sqlSurveyTemplateQuestionId() {
		return ( true == isset( $this->m_intSurveyTemplateQuestionId ) ) ? ( string ) $this->m_intSurveyTemplateQuestionId : 'NULL';
	}

	public function setConditionTemplateQuestionId( $intConditionTemplateQuestionId ) {
		$this->set( 'm_intConditionTemplateQuestionId', CStrings::strToIntDef( $intConditionTemplateQuestionId, NULL, false ) );
	}

	public function getConditionTemplateQuestionId() {
		return $this->m_intConditionTemplateQuestionId;
	}

	public function sqlConditionTemplateQuestionId() {
		return ( true == isset( $this->m_intConditionTemplateQuestionId ) ) ? ( string ) $this->m_intConditionTemplateQuestionId : 'NULL';
	}

	public function setConditionTemplateQuestionOptionId( $intConditionTemplateQuestionOptionId ) {
		$this->set( 'm_intConditionTemplateQuestionOptionId', CStrings::strToIntDef( $intConditionTemplateQuestionOptionId, NULL, false ) );
	}

	public function getConditionTemplateQuestionOptionId() {
		return $this->m_intConditionTemplateQuestionOptionId;
	}

	public function sqlConditionTemplateQuestionOptionId() {
		return ( true == isset( $this->m_intConditionTemplateQuestionOptionId ) ) ? ( string ) $this->m_intConditionTemplateQuestionOptionId : 'NULL';
	}

	public function setSurveyConditionTypeId( $intSurveyConditionTypeId ) {
		$this->set( 'm_intSurveyConditionTypeId', CStrings::strToIntDef( $intSurveyConditionTypeId, NULL, false ) );
	}

	public function getSurveyConditionTypeId() {
		return $this->m_intSurveyConditionTypeId;
	}

	public function sqlSurveyConditionTypeId() {
		return ( true == isset( $this->m_intSurveyConditionTypeId ) ) ? ( string ) $this->m_intSurveyConditionTypeId : 'NULL';
	}

	public function setManagerTemplateSurveyId( $intManagerTemplateSurveyId ) {
		$this->set( 'm_intManagerTemplateSurveyId', CStrings::strToIntDef( $intManagerTemplateSurveyId, NULL, false ) );
	}

	public function getManagerTemplateSurveyId() {
		return $this->m_intManagerTemplateSurveyId;
	}

	public function sqlManagerTemplateSurveyId() {
		return ( true == isset( $this->m_intManagerTemplateSurveyId ) ) ? ( string ) $this->m_intManagerTemplateSurveyId : 'NULL';
	}

	public function setSurveyTemplateTaskId( $intSurveyTemplateTaskId ) {
		$this->set( 'm_intSurveyTemplateTaskId', CStrings::strToIntDef( $intSurveyTemplateTaskId, NULL, false ) );
	}

	public function getSurveyTemplateTaskId() {
		return $this->m_intSurveyTemplateTaskId;
	}

	public function sqlSurveyTemplateTaskId() {
		return ( true == isset( $this->m_intSurveyTemplateTaskId ) ) ? ( string ) $this->m_intSurveyTemplateTaskId : 'NULL';
	}

	public function setSurveyTemplateEmailId( $intSurveyTemplateEmailId ) {
		$this->set( 'm_intSurveyTemplateEmailId', CStrings::strToIntDef( $intSurveyTemplateEmailId, NULL, false ) );
	}

	public function getSurveyTemplateEmailId() {
		return $this->m_intSurveyTemplateEmailId;
	}

	public function sqlSurveyTemplateEmailId() {
		return ( true == isset( $this->m_intSurveyTemplateEmailId ) ) ? ( string ) $this->m_intSurveyTemplateEmailId : 'NULL';
	}

	public function setConditionAnswer( $strConditionAnswer ) {
		$this->set( 'm_strConditionAnswer', CStrings::strTrimDef( $strConditionAnswer, -1, NULL, true ) );
	}

	public function getConditionAnswer() {
		return $this->m_strConditionAnswer;
	}

	public function sqlConditionAnswer() {
		return ( true == isset( $this->m_strConditionAnswer ) ) ? '\'' . addslashes( $this->m_strConditionAnswer ) . '\'' : 'NULL';
	}

	public function setIsIgnoreCase( $intIsIgnoreCase ) {
		$this->set( 'm_intIsIgnoreCase', CStrings::strToIntDef( $intIsIgnoreCase, NULL, false ) );
	}

	public function getIsIgnoreCase() {
		return $this->m_intIsIgnoreCase;
	}

	public function sqlIsIgnoreCase() {
		return ( true == isset( $this->m_intIsIgnoreCase ) ) ? ( string ) $this->m_intIsIgnoreCase : '0';
	}

	public function setValidationMessage( $strValidationMessage ) {
		$this->set( 'm_strValidationMessage', CStrings::strTrimDef( $strValidationMessage, -1, NULL, true ) );
	}

	public function getValidationMessage() {
		return $this->m_strValidationMessage;
	}

	public function sqlValidationMessage() {
		return ( true == isset( $this->m_strValidationMessage ) ) ? '\'' . addslashes( $this->m_strValidationMessage ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, survey_rule_type_id, survey_template_rule_set_id, survey_template_question_group_id, survey_template_question_id, condition_template_question_id, condition_template_question_option_id, survey_condition_type_id, manager_template_survey_id, survey_template_task_id, survey_template_email_id, condition_answer, is_ignore_case, validation_message, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSurveyRuleTypeId() . ', ' .
 						$this->sqlSurveyTemplateRuleSetId() . ', ' .
 						$this->sqlSurveyTemplateQuestionGroupId() . ', ' .
 						$this->sqlSurveyTemplateQuestionId() . ', ' .
 						$this->sqlConditionTemplateQuestionId() . ', ' .
 						$this->sqlConditionTemplateQuestionOptionId() . ', ' .
 						$this->sqlSurveyConditionTypeId() . ', ' .
 						$this->sqlManagerTemplateSurveyId() . ', ' .
 						$this->sqlSurveyTemplateTaskId() . ', ' .
 						$this->sqlSurveyTemplateEmailId() . ', ' .
 						$this->sqlConditionAnswer() . ', ' .
 						$this->sqlIsIgnoreCase() . ', ' .
 						$this->sqlValidationMessage() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_rule_type_id = ' . $this->sqlSurveyRuleTypeId() . ','; } elseif( true == array_key_exists( 'SurveyRuleTypeId', $this->getChangedColumns() ) ) { $strSql .= ' survey_rule_type_id = ' . $this->sqlSurveyRuleTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_rule_set_id = ' . $this->sqlSurveyTemplateRuleSetId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateRuleSetId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_rule_set_id = ' . $this->sqlSurveyTemplateRuleSetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_question_group_id = ' . $this->sqlSurveyTemplateQuestionGroupId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateQuestionGroupId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_question_group_id = ' . $this->sqlSurveyTemplateQuestionGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_question_id = ' . $this->sqlSurveyTemplateQuestionId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_question_id = ' . $this->sqlSurveyTemplateQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_template_question_id = ' . $this->sqlConditionTemplateQuestionId() . ','; } elseif( true == array_key_exists( 'ConditionTemplateQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' condition_template_question_id = ' . $this->sqlConditionTemplateQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_template_question_option_id = ' . $this->sqlConditionTemplateQuestionOptionId() . ','; } elseif( true == array_key_exists( 'ConditionTemplateQuestionOptionId', $this->getChangedColumns() ) ) { $strSql .= ' condition_template_question_option_id = ' . $this->sqlConditionTemplateQuestionOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_condition_type_id = ' . $this->sqlSurveyConditionTypeId() . ','; } elseif( true == array_key_exists( 'SurveyConditionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' survey_condition_type_id = ' . $this->sqlSurveyConditionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manager_template_survey_id = ' . $this->sqlManagerTemplateSurveyId() . ','; } elseif( true == array_key_exists( 'ManagerTemplateSurveyId', $this->getChangedColumns() ) ) { $strSql .= ' manager_template_survey_id = ' . $this->sqlManagerTemplateSurveyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_task_id = ' . $this->sqlSurveyTemplateTaskId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateTaskId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_task_id = ' . $this->sqlSurveyTemplateTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_email_id = ' . $this->sqlSurveyTemplateEmailId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateEmailId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_email_id = ' . $this->sqlSurveyTemplateEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' condition_answer = ' . $this->sqlConditionAnswer() . ','; } elseif( true == array_key_exists( 'ConditionAnswer', $this->getChangedColumns() ) ) { $strSql .= ' condition_answer = ' . $this->sqlConditionAnswer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ignore_case = ' . $this->sqlIsIgnoreCase() . ','; } elseif( true == array_key_exists( 'IsIgnoreCase', $this->getChangedColumns() ) ) { $strSql .= ' is_ignore_case = ' . $this->sqlIsIgnoreCase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' validation_message = ' . $this->sqlValidationMessage() . ','; } elseif( true == array_key_exists( 'ValidationMessage', $this->getChangedColumns() ) ) { $strSql .= ' validation_message = ' . $this->sqlValidationMessage() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'survey_rule_type_id' => $this->getSurveyRuleTypeId(),
			'survey_template_rule_set_id' => $this->getSurveyTemplateRuleSetId(),
			'survey_template_question_group_id' => $this->getSurveyTemplateQuestionGroupId(),
			'survey_template_question_id' => $this->getSurveyTemplateQuestionId(),
			'condition_template_question_id' => $this->getConditionTemplateQuestionId(),
			'condition_template_question_option_id' => $this->getConditionTemplateQuestionOptionId(),
			'survey_condition_type_id' => $this->getSurveyConditionTypeId(),
			'manager_template_survey_id' => $this->getManagerTemplateSurveyId(),
			'survey_template_task_id' => $this->getSurveyTemplateTaskId(),
			'survey_template_email_id' => $this->getSurveyTemplateEmailId(),
			'condition_answer' => $this->getConditionAnswer(),
			'is_ignore_case' => $this->getIsIgnoreCase(),
			'validation_message' => $this->getValidationMessage(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>