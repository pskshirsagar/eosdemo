<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsProperties
 * Do not add any new functions to this class.
 */

class CBaseStatsProperties extends CEosPluralBase {

	/**
	 * @return CStatsProperty[]
	 */
	public static function fetchStatsProperties( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStatsProperty', $objDatabase );
	}

	/**
	 * @return CStatsProperty
	 */
	public static function fetchStatsProperty( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStatsProperty', $objDatabase );
	}

	public static function fetchStatsPropertyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stats_properties', $objDatabase );
	}

	public static function fetchStatsPropertyById( $intId, $objDatabase ) {
		return self::fetchStatsProperty( sprintf( 'SELECT * FROM stats_properties WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStatsPropertiesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchStatsProperties( sprintf( 'SELECT * FROM stats_properties WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>