<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CVirtualForeignKeys
 * Do not add any new functions to this class.
 */

class CBaseVirtualForeignKeys extends CEosPluralBase {

	/**
	 * @return CVirtualForeignKey[]
	 */
	public static function fetchVirtualForeignKeys( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CVirtualForeignKey', $objDatabase );
	}

	/**
	 * @return CVirtualForeignKey
	 */
	public static function fetchVirtualForeignKey( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CVirtualForeignKey', $objDatabase );
	}

	public static function fetchVirtualForeignKeyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'virtual_foreign_keys', $objDatabase );
	}

	public static function fetchVirtualForeignKeyById( $intId, $objDatabase ) {
		return self::fetchVirtualForeignKey( sprintf( 'SELECT * FROM virtual_foreign_keys WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchVirtualForeignKeysByFrequencyId( $intFrequencyId, $objDatabase ) {
		return self::fetchVirtualForeignKeys( sprintf( 'SELECT * FROM virtual_foreign_keys WHERE frequency_id = %d', ( int ) $intFrequencyId ), $objDatabase );
	}

	public static function fetchVirtualForeignKeysBySourceDatabaseTypeId( $intSourceDatabaseTypeId, $objDatabase ) {
		return self::fetchVirtualForeignKeys( sprintf( 'SELECT * FROM virtual_foreign_keys WHERE source_database_type_id = %d', ( int ) $intSourceDatabaseTypeId ), $objDatabase );
	}

	public static function fetchVirtualForeignKeysByForeignDatabaseTypeId( $intForeignDatabaseTypeId, $objDatabase ) {
		return self::fetchVirtualForeignKeys( sprintf( 'SELECT * FROM virtual_foreign_keys WHERE foreign_database_type_id = %d', ( int ) $intForeignDatabaseTypeId ), $objDatabase );
	}

	public static function fetchVirtualForeignKeysByDeveloperEmployeeId( $intDeveloperEmployeeId, $objDatabase ) {
		return self::fetchVirtualForeignKeys( sprintf( 'SELECT * FROM virtual_foreign_keys WHERE developer_employee_id = %d', ( int ) $intDeveloperEmployeeId ), $objDatabase );
	}

	public static function fetchVirtualForeignKeysByQaEmployeeId( $intQaEmployeeId, $objDatabase ) {
		return self::fetchVirtualForeignKeys( sprintf( 'SELECT * FROM virtual_foreign_keys WHERE qa_employee_id = %d', ( int ) $intQaEmployeeId ), $objDatabase );
	}

}
?>