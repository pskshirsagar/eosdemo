<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGoogleApiAccessTokens
 * Do not add any new functions to this class.
 */

class CBaseGoogleApiAccessTokens extends CEosPluralBase {

	/**
	 * @return CGoogleApiAccessToken[]
	 */
	public static function fetchGoogleApiAccessTokens( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CGoogleApiAccessToken', $objDatabase );
	}

	/**
	 * @return CGoogleApiAccessToken
	 */
	public static function fetchGoogleApiAccessToken( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGoogleApiAccessToken', $objDatabase );
	}

	public static function fetchGoogleApiAccessTokenCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'google_api_access_tokens', $objDatabase );
	}

	public static function fetchGoogleApiAccessTokenById( $intId, $objDatabase ) {
		return self::fetchGoogleApiAccessToken( sprintf( 'SELECT * FROM google_api_access_tokens WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>