<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroups
 * Do not add any new functions to this class.
 */

class CBaseGroups extends CEosPluralBase {

	/**
	 * @return CGroup[]
	 */
	public static function fetchGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CGroup', $objDatabase );
	}

	/**
	 * @return CGroup
	 */
	public static function fetchGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGroup', $objDatabase );
	}

	public static function fetchGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'groups', $objDatabase );
	}

	public static function fetchGroupById( $intId, $objDatabase ) {
		return self::fetchGroup( sprintf( 'SELECT * FROM groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchGroupsByGroupTypeId( $intGroupTypeId, $objDatabase ) {
		return self::fetchGroups( sprintf( 'SELECT * FROM groups WHERE group_type_id = %d', ( int ) $intGroupTypeId ), $objDatabase );
	}

	public static function fetchGroupsByParentGroupId( $intParentGroupId, $objDatabase ) {
		return self::fetchGroups( sprintf( 'SELECT * FROM groups WHERE parent_group_id = %d', ( int ) $intParentGroupId ), $objDatabase );
	}

}
?>