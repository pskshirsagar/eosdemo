<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CInterviewTypes
 * Do not add any new functions to this class.
 */

class CBaseInterviewTypes extends CEosPluralBase {

	/**
	 * @return CInterviewType[]
	 */
	public static function fetchInterviewTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInterviewType', $objDatabase );
	}

	/**
	 * @return CInterviewType
	 */
	public static function fetchInterviewType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInterviewType', $objDatabase );
	}

	public static function fetchInterviewTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'interview_types', $objDatabase );
	}

	public static function fetchInterviewTypeById( $intId, $objDatabase ) {
		return self::fetchInterviewType( sprintf( 'SELECT * FROM interview_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>