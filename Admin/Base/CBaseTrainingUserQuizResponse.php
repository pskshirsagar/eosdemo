<?php

class CBaseTrainingUserQuizResponse extends CEosSingularBase {

	const TABLE_NAME = 'public.training_user_quiz_responses';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTrainingUserQuizId;
	protected $m_intHelpQuizQuestionId;
	protected $m_intHelpQuizAnswerId;
	protected $m_strAnswer;
	protected $m_boolIsCorrect;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = '1';
		$this->m_boolIsCorrect = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['training_user_quiz_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingUserQuizId', trim( $arrValues['training_user_quiz_id'] ) ); elseif( isset( $arrValues['training_user_quiz_id'] ) ) $this->setTrainingUserQuizId( $arrValues['training_user_quiz_id'] );
		if( isset( $arrValues['help_quiz_question_id'] ) && $boolDirectSet ) $this->set( 'm_intHelpQuizQuestionId', trim( $arrValues['help_quiz_question_id'] ) ); elseif( isset( $arrValues['help_quiz_question_id'] ) ) $this->setHelpQuizQuestionId( $arrValues['help_quiz_question_id'] );
		if( isset( $arrValues['help_quiz_answer_id'] ) && $boolDirectSet ) $this->set( 'm_intHelpQuizAnswerId', trim( $arrValues['help_quiz_answer_id'] ) ); elseif( isset( $arrValues['help_quiz_answer_id'] ) ) $this->setHelpQuizAnswerId( $arrValues['help_quiz_answer_id'] );
		if( isset( $arrValues['answer'] ) && $boolDirectSet ) $this->set( 'm_strAnswer', trim( stripcslashes( $arrValues['answer'] ) ) ); elseif( isset( $arrValues['answer'] ) ) $this->setAnswer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['answer'] ) : $arrValues['answer'] );
		if( isset( $arrValues['is_correct'] ) && $boolDirectSet ) $this->set( 'm_boolIsCorrect', trim( stripcslashes( $arrValues['is_correct'] ) ) ); elseif( isset( $arrValues['is_correct'] ) ) $this->setIsCorrect( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_correct'] ) : $arrValues['is_correct'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : '1';
	}

	public function setTrainingUserQuizId( $intTrainingUserQuizId ) {
		$this->set( 'm_intTrainingUserQuizId', CStrings::strToIntDef( $intTrainingUserQuizId, NULL, false ) );
	}

	public function getTrainingUserQuizId() {
		return $this->m_intTrainingUserQuizId;
	}

	public function sqlTrainingUserQuizId() {
		return ( true == isset( $this->m_intTrainingUserQuizId ) ) ? ( string ) $this->m_intTrainingUserQuizId : 'NULL';
	}

	public function setHelpQuizQuestionId( $intHelpQuizQuestionId ) {
		$this->set( 'm_intHelpQuizQuestionId', CStrings::strToIntDef( $intHelpQuizQuestionId, NULL, false ) );
	}

	public function getHelpQuizQuestionId() {
		return $this->m_intHelpQuizQuestionId;
	}

	public function sqlHelpQuizQuestionId() {
		return ( true == isset( $this->m_intHelpQuizQuestionId ) ) ? ( string ) $this->m_intHelpQuizQuestionId : 'NULL';
	}

	public function setHelpQuizAnswerId( $intHelpQuizAnswerId ) {
		$this->set( 'm_intHelpQuizAnswerId', CStrings::strToIntDef( $intHelpQuizAnswerId, NULL, false ) );
	}

	public function getHelpQuizAnswerId() {
		return $this->m_intHelpQuizAnswerId;
	}

	public function sqlHelpQuizAnswerId() {
		return ( true == isset( $this->m_intHelpQuizAnswerId ) ) ? ( string ) $this->m_intHelpQuizAnswerId : 'NULL';
	}

	public function setAnswer( $strAnswer ) {
		$this->set( 'm_strAnswer', CStrings::strTrimDef( $strAnswer, 500, NULL, true ) );
	}

	public function getAnswer() {
		return $this->m_strAnswer;
	}

	public function sqlAnswer() {
		return ( true == isset( $this->m_strAnswer ) ) ? '\'' . addslashes( $this->m_strAnswer ) . '\'' : 'NULL';
	}

	public function setIsCorrect( $boolIsCorrect ) {
		$this->set( 'm_boolIsCorrect', CStrings::strToBool( $boolIsCorrect ) );
	}

	public function getIsCorrect() {
		return $this->m_boolIsCorrect;
	}

	public function sqlIsCorrect() {
		return ( true == isset( $this->m_boolIsCorrect ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCorrect ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, training_user_quiz_id, help_quiz_question_id, help_quiz_answer_id, answer, is_correct, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlTrainingUserQuizId() . ', ' .
						$this->sqlHelpQuizQuestionId() . ', ' .
						$this->sqlHelpQuizAnswerId() . ', ' .
						$this->sqlAnswer() . ', ' .
						$this->sqlIsCorrect() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_user_quiz_id = ' . $this->sqlTrainingUserQuizId(). ',' ; } elseif( true == array_key_exists( 'TrainingUserQuizId', $this->getChangedColumns() ) ) { $strSql .= ' training_user_quiz_id = ' . $this->sqlTrainingUserQuizId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_quiz_question_id = ' . $this->sqlHelpQuizQuestionId(). ',' ; } elseif( true == array_key_exists( 'HelpQuizQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' help_quiz_question_id = ' . $this->sqlHelpQuizQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' help_quiz_answer_id = ' . $this->sqlHelpQuizAnswerId(). ',' ; } elseif( true == array_key_exists( 'HelpQuizAnswerId', $this->getChangedColumns() ) ) { $strSql .= ' help_quiz_answer_id = ' . $this->sqlHelpQuizAnswerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answer = ' . $this->sqlAnswer(). ',' ; } elseif( true == array_key_exists( 'Answer', $this->getChangedColumns() ) ) { $strSql .= ' answer = ' . $this->sqlAnswer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_correct = ' . $this->sqlIsCorrect(). ',' ; } elseif( true == array_key_exists( 'IsCorrect', $this->getChangedColumns() ) ) { $strSql .= ' is_correct = ' . $this->sqlIsCorrect() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'training_user_quiz_id' => $this->getTrainingUserQuizId(),
			'help_quiz_question_id' => $this->getHelpQuizQuestionId(),
			'help_quiz_answer_id' => $this->getHelpQuizAnswerId(),
			'answer' => $this->getAnswer(),
			'is_correct' => $this->getIsCorrect(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>