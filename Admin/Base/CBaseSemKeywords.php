<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemKeywords
 * Do not add any new functions to this class.
 */

class CBaseSemKeywords extends CEosPluralBase {

	/**
	 * @return CSemKeyword[]
	 */
	public static function fetchSemKeywords( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemKeyword', $objDatabase );
	}

	/**
	 * @return CSemKeyword
	 */
	public static function fetchSemKeyword( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemKeyword', $objDatabase );
	}

	public static function fetchSemKeywordCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_keywords', $objDatabase );
	}

	public static function fetchSemKeywordById( $intId, $objDatabase ) {
		return self::fetchSemKeyword( sprintf( 'SELECT * FROM sem_keywords WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemKeywordsBySemCampaignId( $intSemCampaignId, $objDatabase ) {
		return self::fetchSemKeywords( sprintf( 'SELECT * FROM sem_keywords WHERE sem_campaign_id = %d', ( int ) $intSemCampaignId ), $objDatabase );
	}

	public static function fetchSemKeywordsBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemKeywords( sprintf( 'SELECT * FROM sem_keywords WHERE sem_ad_group_id = %d', ( int ) $intSemAdGroupId ), $objDatabase );
	}

}
?>