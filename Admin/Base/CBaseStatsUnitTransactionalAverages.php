<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsUnitTransactionalAverages
 * Do not add any new functions to this class.
 */

class CBaseStatsUnitTransactionalAverages extends CEosPluralBase {

	/**
	 * @return CStatsUnitTransactionalAverage[]
	 */
	public static function fetchStatsUnitTransactionalAverages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CStatsUnitTransactionalAverage::class, $objDatabase );
	}

	/**
	 * @return CStatsUnitTransactionalAverage
	 */
	public static function fetchStatsUnitTransactionalAverage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStatsUnitTransactionalAverage::class, $objDatabase );
	}

	public static function fetchStatsUnitTransactionalAverageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stats_unit_transactional_averages', $objDatabase );
	}

	public static function fetchStatsUnitTransactionalAverageById( $intId, $objDatabase ) {
		return self::fetchStatsUnitTransactionalAverage( sprintf( 'SELECT * FROM stats_unit_transactional_averages WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStatsUnitTransactionalAveragesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchStatsUnitTransactionalAverages( sprintf( 'SELECT * FROM stats_unit_transactional_averages WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>