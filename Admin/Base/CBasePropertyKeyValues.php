<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPropertyKeyValues
 * Do not add any new functions to this class.
 */

class CBasePropertyKeyValues extends CEosPluralBase {

	/**
	 * @return CPropertyKeyValue[]
	 */
	public static function fetchPropertyKeyValues( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPropertyKeyValue', $objDatabase );
	}

	/**
	 * @return CPropertyKeyValue
	 */
	public static function fetchPropertyKeyValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPropertyKeyValue', $objDatabase );
	}

	public static function fetchPropertyKeyValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'property_key_values', $objDatabase );
	}

	public static function fetchPropertyKeyValueById( $intId, $objDatabase ) {
		return self::fetchPropertyKeyValue( sprintf( 'SELECT * FROM property_key_values WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPropertyKeyValuesByCid( $intCid, $objDatabase ) {
		return self::fetchPropertyKeyValues( sprintf( 'SELECT * FROM property_key_values WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPropertyKeyValuesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchPropertyKeyValues( sprintf( 'SELECT * FROM property_key_values WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>