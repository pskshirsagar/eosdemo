<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportSqlDetails
 * Do not add any new functions to this class.
 */

class CBaseReportSqlDetails extends CEosPluralBase {

	/**
	 * @return CReportSqlDetail[]
	 */
	public static function fetchReportSqlDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportSqlDetail::class, $objDatabase );
	}

	/**
	 * @return CReportSqlDetail
	 */
	public static function fetchReportSqlDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportSqlDetail::class, $objDatabase );
	}

	public static function fetchReportSqlDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_sql_details', $objDatabase );
	}

	public static function fetchReportSqlDetailById( $intId, $objDatabase ) {
		return self::fetchReportSqlDetail( sprintf( 'SELECT * FROM report_sql_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportSqlDetailsByCompanyReportId( $intCompanyReportId, $objDatabase ) {
		return self::fetchReportSqlDetails( sprintf( 'SELECT * FROM report_sql_details WHERE company_report_id = %d', ( int ) $intCompanyReportId ), $objDatabase );
	}

}
?>