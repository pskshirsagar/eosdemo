<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseChartDetails
 * Do not add any new functions to this class.
 */

class CBaseReleaseChartDetails extends CEosPluralBase {

	/**
	 * @return CReleaseChartDetail[]
	 */
	public static function fetchReleaseChartDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReleaseChartDetail::class, $objDatabase );
	}

	/**
	 * @return CReleaseChartDetail
	 */
	public static function fetchReleaseChartDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReleaseChartDetail::class, $objDatabase );
	}

	public static function fetchReleaseChartDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'release_chart_details', $objDatabase );
	}

	public static function fetchReleaseChartDetailById( $intId, $objDatabase ) {
		return self::fetchReleaseChartDetail( sprintf( 'SELECT * FROM release_chart_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReleaseChartDetailsByReleaseReportDetailId( $intReleaseReportDetailId, $objDatabase ) {
		return self::fetchReleaseChartDetails( sprintf( 'SELECT * FROM release_chart_details WHERE release_report_detail_id = %d', ( int ) $intReleaseReportDetailId ), $objDatabase );
	}

	public static function fetchReleaseChartDetailsByReleaseChartReportTemplateId( $intReleaseChartReportTemplateId, $objDatabase ) {
		return self::fetchReleaseChartDetails( sprintf( 'SELECT * FROM release_chart_details WHERE release_chart_report_template_id = %d', ( int ) $intReleaseChartReportTemplateId ), $objDatabase );
	}

}
?>