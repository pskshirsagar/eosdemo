<?php

class CBaseExportObjectReference extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.export_object_references';

	protected $m_intId;
	protected $m_intExportPartnerId;
	protected $m_strExportObjectReferenceType;
	protected $m_strExportObjectReferenceTypeKey;
	protected $m_intExportStatusTypeId;
	protected $m_strReferenceId;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intExportedBy;
	protected $m_strExportedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['export_partner_id'] ) && $boolDirectSet ) $this->set( 'm_intExportPartnerId', trim( $arrValues['export_partner_id'] ) ); elseif( isset( $arrValues['export_partner_id'] ) ) $this->setExportPartnerId( $arrValues['export_partner_id'] );
		if( isset( $arrValues['export_object_reference_type'] ) && $boolDirectSet ) $this->set( 'm_strExportObjectReferenceType', trim( stripcslashes( $arrValues['export_object_reference_type'] ) ) ); elseif( isset( $arrValues['export_object_reference_type'] ) ) $this->setExportObjectReferenceType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['export_object_reference_type'] ) : $arrValues['export_object_reference_type'] );
		if( isset( $arrValues['export_object_reference_type_key'] ) && $boolDirectSet ) $this->set( 'm_strExportObjectReferenceTypeKey', trim( $arrValues['export_object_reference_type_key'] ) ); elseif( isset( $arrValues['export_object_reference_type_key'] ) ) $this->setExportObjectReferenceTypeKey( $arrValues['export_object_reference_type_key'] );
		if( isset( $arrValues['export_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intExportStatusTypeId', trim( $arrValues['export_status_type_id'] ) ); elseif( isset( $arrValues['export_status_type_id'] ) ) $this->setExportStatusTypeId( $arrValues['export_status_type_id'] );
		if( isset( $arrValues['reference_id'] ) && $boolDirectSet ) $this->set( 'm_strReferenceId', trim( $arrValues['reference_id'] ) ); elseif( isset( $arrValues['reference_id'] ) ) $this->setReferenceId( $arrValues['reference_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['exported_by'] ) && $boolDirectSet ) $this->set( 'm_intExportedBy', trim( $arrValues['exported_by'] ) ); elseif( isset( $arrValues['exported_by'] ) ) $this->setExportedBy( $arrValues['exported_by'] );
		if( isset( $arrValues['exported_on'] ) && $boolDirectSet ) $this->set( 'm_strExportedOn', trim( $arrValues['exported_on'] ) ); elseif( isset( $arrValues['exported_on'] ) ) $this->setExportedOn( $arrValues['exported_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function setExportPartnerId( $intExportPartnerId ) {
		$this->set( 'm_intExportPartnerId', CStrings::strToIntDef( $intExportPartnerId, NULL, false ) );
	}

	public function setExportObjectReferenceType( $strExportObjectReferenceType ) {
		$this->set( 'm_strExportObjectReferenceType', CStrings::strTrimDef( $strExportObjectReferenceType, -1, NULL, true ) );
	}

	public function setExportObjectReferenceTypeKey( $strExportObjectReferenceTypeKey ) {
		$this->set( 'm_strExportObjectReferenceTypeKey', CStrings::strTrimDef( $strExportObjectReferenceTypeKey, -1, NULL, true ) );
	}

	public function setExportStatusTypeId( $intExportStatusTypeId ) {
		$this->set( 'm_intExportStatusTypeId', CStrings::strToIntDef( $intExportStatusTypeId, NULL, false ) );
	}

	public function setReferenceId( $strReferenceId ) {
		$this->set( 'm_strReferenceId', CStrings::strTrimDef( $strReferenceId, -1, NULL, true ) );
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function setExportedBy( $intExportedBy ) {
		$this->set( 'm_intExportedBy', CStrings::strToIntDef( $intExportedBy, NULL, false ) );
	}

	public function setExportedOn( $strExportedOn ) {
		$this->set( 'm_strExportedOn', CStrings::strTrimDef( $strExportedOn, -1, NULL, true ) );
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, export_partner_id, export_object_reference_type, export_object_reference_type_key, export_status_type_id, reference_id, details, updated_by, updated_on, exported_by, exported_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlExportPartnerId() . ', ' .
						$this->sqlExportObjectReferenceType() . ', ' .
						$this->sqlExportObjectReferenceTypeKey() . ', ' .
						$this->sqlExportStatusTypeId() . ', ' .
						$this->sqlReferenceId() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlExportedBy() . ', ' .
						$this->sqlExportedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function sqlExportPartnerId() {
		return ( true == isset( $this->m_intExportPartnerId ) ) ? ( string ) $this->m_intExportPartnerId : 'NULL';
	}

	public function sqlExportObjectReferenceType() {
		return ( true == isset( $this->m_strExportObjectReferenceType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExportObjectReferenceType ) : '\'' . addslashes( $this->m_strExportObjectReferenceType ) . '\'' ) : 'NULL';
	}

	public function sqlExportObjectReferenceTypeKey() {
		return ( true == isset( $this->m_strExportObjectReferenceTypeKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strExportObjectReferenceTypeKey ) : '\'' . addslashes( $this->m_strExportObjectReferenceTypeKey ) . '\'' ) : 'NULL';
	}

	public function sqlExportStatusTypeId() {
		return ( true == isset( $this->m_intExportStatusTypeId ) ) ? ( string ) $this->m_intExportStatusTypeId : 'NULL';
	}

	public function sqlReferenceId() {
		return ( true == isset( $this->m_strReferenceId ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReferenceId ) : '\'' . addslashes( $this->m_strReferenceId ) . '\'' ) : 'NULL';
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function sqlExportedBy() {
		return ( true == isset( $this->m_intExportedBy ) ) ? ( string ) $this->m_intExportedBy : 'NULL';
	}

	public function sqlExportedOn() {
		return ( true == isset( $this->m_strExportedOn ) ) ? '\'' . $this->m_strExportedOn . '\'' : 'NOW()';
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_partner_id = ' . $this->sqlExportPartnerId(). ',' ; } elseif( true == array_key_exists( 'ExportPartnerId', $this->getChangedColumns() ) ) { $strSql .= ' export_partner_id = ' . $this->sqlExportPartnerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_object_reference_type = ' . $this->sqlExportObjectReferenceType(). ',' ; } elseif( true == array_key_exists( 'ExportObjectReferenceType', $this->getChangedColumns() ) ) { $strSql .= ' export_object_reference_type = ' . $this->sqlExportObjectReferenceType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_object_reference_type_key = ' . $this->sqlExportObjectReferenceTypeKey(). ',' ; } elseif( true == array_key_exists( 'ExportObjectReferenceTypeKey', $this->getChangedColumns() ) ) { $strSql .= ' export_object_reference_type_key = ' . $this->sqlExportObjectReferenceTypeKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_status_type_id = ' . $this->sqlExportStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ExportStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' export_status_type_id = ' . $this->sqlExportStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId(). ',' ; } elseif( true == array_key_exists( 'ReferenceId', $this->getChangedColumns() ) ) { $strSql .= ' reference_id = ' . $this->sqlReferenceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy(). ',' ; } elseif( true == array_key_exists( 'ExportedBy', $this->getChangedColumns() ) ) { $strSql .= ' exported_by = ' . $this->sqlExportedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn(). ',' ; } elseif( true == array_key_exists( 'ExportedOn', $this->getChangedColumns() ) ) { $strSql .= ' exported_on = ' . $this->sqlExportedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'export_partner_id' => $this->getExportPartnerId(),
			'export_object_reference_type' => $this->getExportObjectReferenceType(),
			'export_object_reference_type_key' => $this->getExportObjectReferenceTypeKey(),
			'export_status_type_id' => $this->getExportStatusTypeId(),
			'reference_id' => $this->getReferenceId(),
			'details' => $this->getDetails(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'exported_by' => $this->getExportedBy(),
			'exported_on' => $this->getExportedOn()
		);
	}

	public function getExportPartnerId() {
		return $this->m_intExportPartnerId;
	}

	public function getExportObjectReferenceType() {
		return $this->m_strExportObjectReferenceType;
	}

	public function getExportObjectReferenceTypeKey() {
		return $this->m_strExportObjectReferenceTypeKey;
	}

	public function getExportStatusTypeId() {
		return $this->m_intExportStatusTypeId;
	}

	public function getReferenceId() {
		return $this->m_strReferenceId;
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function getExportedBy() {
		return $this->m_intExportedBy;
	}

	public function getExportedOn() {
		return $this->m_strExportedOn;
	}

}
?>