<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroupTypes
 * Do not add any new functions to this class.
 */

class CBaseGroupTypes extends CEosPluralBase {

	/**
	 * @return CGroupType[]
	 */
	public static function fetchGroupTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CGroupType', $objDatabase );
	}

	/**
	 * @return CGroupType
	 */
	public static function fetchGroupType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGroupType', $objDatabase );
	}

	public static function fetchGroupTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'group_types', $objDatabase );
	}

	public static function fetchGroupTypeById( $intId, $objDatabase ) {
		return self::fetchGroupType( sprintf( 'SELECT * FROM group_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>