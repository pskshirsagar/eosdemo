<?php

class CBaseEmployeeCompensationLog extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_compensation_logs';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intPurchaseRequestId;
	protected $m_intPreviousEmployeeEncryptionAssociationId;
	protected $m_intNewEmployeeEncryptionAssociationId;
	protected $m_intBonusEmployeeEncryptionAssociationId;
	protected $m_intEstimatedHours;
	protected $m_intHourlyRateEmployeeEncryptionAssociationId;
	protected $m_strNote;
	protected $m_intIsVerified;
	protected $m_intRequestedBy;
	protected $m_strRequestedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsVerified = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['purchase_request_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchaseRequestId', trim( $arrValues['purchase_request_id'] ) ); elseif( isset( $arrValues['purchase_request_id'] ) ) $this->setPurchaseRequestId( $arrValues['purchase_request_id'] );
		if( isset( $arrValues['previous_employee_encryption_association_id'] ) && $boolDirectSet ) $this->set( 'm_intPreviousEmployeeEncryptionAssociationId', trim( $arrValues['previous_employee_encryption_association_id'] ) ); elseif( isset( $arrValues['previous_employee_encryption_association_id'] ) ) $this->setPreviousEmployeeEncryptionAssociationId( $arrValues['previous_employee_encryption_association_id'] );
		if( isset( $arrValues['new_employee_encryption_association_id'] ) && $boolDirectSet ) $this->set( 'm_intNewEmployeeEncryptionAssociationId', trim( $arrValues['new_employee_encryption_association_id'] ) ); elseif( isset( $arrValues['new_employee_encryption_association_id'] ) ) $this->setNewEmployeeEncryptionAssociationId( $arrValues['new_employee_encryption_association_id'] );
		if( isset( $arrValues['bonus_employee_encryption_association_id'] ) && $boolDirectSet ) $this->set( 'm_intBonusEmployeeEncryptionAssociationId', trim( $arrValues['bonus_employee_encryption_association_id'] ) ); elseif( isset( $arrValues['bonus_employee_encryption_association_id'] ) ) $this->setBonusEmployeeEncryptionAssociationId( $arrValues['bonus_employee_encryption_association_id'] );
		if( isset( $arrValues['estimated_hours'] ) && $boolDirectSet ) $this->set( 'm_intEstimatedHours', trim( $arrValues['estimated_hours'] ) ); elseif( isset( $arrValues['estimated_hours'] ) ) $this->setEstimatedHours( $arrValues['estimated_hours'] );
		if( isset( $arrValues['hourly_rate_employee_encryption_association_id'] ) && $boolDirectSet ) $this->set( 'm_intHourlyRateEmployeeEncryptionAssociationId', trim( $arrValues['hourly_rate_employee_encryption_association_id'] ) ); elseif( isset( $arrValues['hourly_rate_employee_encryption_association_id'] ) ) $this->setHourlyRateEmployeeEncryptionAssociationId( $arrValues['hourly_rate_employee_encryption_association_id'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['is_verified'] ) && $boolDirectSet ) $this->set( 'm_intIsVerified', trim( $arrValues['is_verified'] ) ); elseif( isset( $arrValues['is_verified'] ) ) $this->setIsVerified( $arrValues['is_verified'] );
		if( isset( $arrValues['requested_by'] ) && $boolDirectSet ) $this->set( 'm_intRequestedBy', trim( $arrValues['requested_by'] ) ); elseif( isset( $arrValues['requested_by'] ) ) $this->setRequestedBy( $arrValues['requested_by'] );
		if( isset( $arrValues['requested_on'] ) && $boolDirectSet ) $this->set( 'm_strRequestedOn', trim( $arrValues['requested_on'] ) ); elseif( isset( $arrValues['requested_on'] ) ) $this->setRequestedOn( $arrValues['requested_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setPurchaseRequestId( $intPurchaseRequestId ) {
		$this->set( 'm_intPurchaseRequestId', CStrings::strToIntDef( $intPurchaseRequestId, NULL, false ) );
	}

	public function getPurchaseRequestId() {
		return $this->m_intPurchaseRequestId;
	}

	public function sqlPurchaseRequestId() {
		return ( true == isset( $this->m_intPurchaseRequestId ) ) ? ( string ) $this->m_intPurchaseRequestId : 'NULL';
	}

	public function setPreviousEmployeeEncryptionAssociationId( $intPreviousEmployeeEncryptionAssociationId ) {
		$this->set( 'm_intPreviousEmployeeEncryptionAssociationId', CStrings::strToIntDef( $intPreviousEmployeeEncryptionAssociationId, NULL, false ) );
	}

	public function getPreviousEmployeeEncryptionAssociationId() {
		return $this->m_intPreviousEmployeeEncryptionAssociationId;
	}

	public function sqlPreviousEmployeeEncryptionAssociationId() {
		return ( true == isset( $this->m_intPreviousEmployeeEncryptionAssociationId ) ) ? ( string ) $this->m_intPreviousEmployeeEncryptionAssociationId : 'NULL';
	}

	public function setNewEmployeeEncryptionAssociationId( $intNewEmployeeEncryptionAssociationId ) {
		$this->set( 'm_intNewEmployeeEncryptionAssociationId', CStrings::strToIntDef( $intNewEmployeeEncryptionAssociationId, NULL, false ) );
	}

	public function getNewEmployeeEncryptionAssociationId() {
		return $this->m_intNewEmployeeEncryptionAssociationId;
	}

	public function sqlNewEmployeeEncryptionAssociationId() {
		return ( true == isset( $this->m_intNewEmployeeEncryptionAssociationId ) ) ? ( string ) $this->m_intNewEmployeeEncryptionAssociationId : 'NULL';
	}

	public function setBonusEmployeeEncryptionAssociationId( $intBonusEmployeeEncryptionAssociationId ) {
		$this->set( 'm_intBonusEmployeeEncryptionAssociationId', CStrings::strToIntDef( $intBonusEmployeeEncryptionAssociationId, NULL, false ) );
	}

	public function getBonusEmployeeEncryptionAssociationId() {
		return $this->m_intBonusEmployeeEncryptionAssociationId;
	}

	public function sqlBonusEmployeeEncryptionAssociationId() {
		return ( true == isset( $this->m_intBonusEmployeeEncryptionAssociationId ) ) ? ( string ) $this->m_intBonusEmployeeEncryptionAssociationId : 'NULL';
	}

	public function setEstimatedHours( $intEstimatedHours ) {
		$this->set( 'm_intEstimatedHours', CStrings::strToIntDef( $intEstimatedHours, NULL, false ) );
	}

	public function getEstimatedHours() {
		return $this->m_intEstimatedHours;
	}

	public function sqlEstimatedHours() {
		return ( true == isset( $this->m_intEstimatedHours ) ) ? ( string ) $this->m_intEstimatedHours : 'NULL';
	}

	public function setHourlyRateEmployeeEncryptionAssociationId( $intHourlyRateEmployeeEncryptionAssociationId ) {
		$this->set( 'm_intHourlyRateEmployeeEncryptionAssociationId', CStrings::strToIntDef( $intHourlyRateEmployeeEncryptionAssociationId, NULL, false ) );
	}

	public function getHourlyRateEmployeeEncryptionAssociationId() {
		return $this->m_intHourlyRateEmployeeEncryptionAssociationId;
	}

	public function sqlHourlyRateEmployeeEncryptionAssociationId() {
		return ( true == isset( $this->m_intHourlyRateEmployeeEncryptionAssociationId ) ) ? ( string ) $this->m_intHourlyRateEmployeeEncryptionAssociationId : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, 2000, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setIsVerified( $intIsVerified ) {
		$this->set( 'm_intIsVerified', CStrings::strToIntDef( $intIsVerified, NULL, false ) );
	}

	public function getIsVerified() {
		return $this->m_intIsVerified;
	}

	public function sqlIsVerified() {
		return ( true == isset( $this->m_intIsVerified ) ) ? ( string ) $this->m_intIsVerified : '1';
	}

	public function setRequestedBy( $intRequestedBy ) {
		$this->set( 'm_intRequestedBy', CStrings::strToIntDef( $intRequestedBy, NULL, false ) );
	}

	public function getRequestedBy() {
		return $this->m_intRequestedBy;
	}

	public function sqlRequestedBy() {
		return ( true == isset( $this->m_intRequestedBy ) ) ? ( string ) $this->m_intRequestedBy : 'NULL';
	}

	public function setRequestedOn( $strRequestedOn ) {
		$this->set( 'm_strRequestedOn', CStrings::strTrimDef( $strRequestedOn, -1, NULL, true ) );
	}

	public function getRequestedOn() {
		return $this->m_strRequestedOn;
	}

	public function sqlRequestedOn() {
		return ( true == isset( $this->m_strRequestedOn ) ) ? '\'' . $this->m_strRequestedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, purchase_request_id, previous_employee_encryption_association_id, new_employee_encryption_association_id, bonus_employee_encryption_association_id, estimated_hours, hourly_rate_employee_encryption_association_id, note, is_verified, requested_by, requested_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlPurchaseRequestId() . ', ' .
 						$this->sqlPreviousEmployeeEncryptionAssociationId() . ', ' .
 						$this->sqlNewEmployeeEncryptionAssociationId() . ', ' .
 						$this->sqlBonusEmployeeEncryptionAssociationId() . ', ' .
 						$this->sqlEstimatedHours() . ', ' .
 						$this->sqlHourlyRateEmployeeEncryptionAssociationId() . ', ' .
 						$this->sqlNote() . ', ' .
 						$this->sqlIsVerified() . ', ' .
 						$this->sqlRequestedBy() . ', ' .
 						$this->sqlRequestedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; } elseif( true == array_key_exists( 'PurchaseRequestId', $this->getChangedColumns() ) ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_employee_encryption_association_id = ' . $this->sqlPreviousEmployeeEncryptionAssociationId() . ','; } elseif( true == array_key_exists( 'PreviousEmployeeEncryptionAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' previous_employee_encryption_association_id = ' . $this->sqlPreviousEmployeeEncryptionAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_employee_encryption_association_id = ' . $this->sqlNewEmployeeEncryptionAssociationId() . ','; } elseif( true == array_key_exists( 'NewEmployeeEncryptionAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' new_employee_encryption_association_id = ' . $this->sqlNewEmployeeEncryptionAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bonus_employee_encryption_association_id = ' . $this->sqlBonusEmployeeEncryptionAssociationId() . ','; } elseif( true == array_key_exists( 'BonusEmployeeEncryptionAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' bonus_employee_encryption_association_id = ' . $this->sqlBonusEmployeeEncryptionAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimated_hours = ' . $this->sqlEstimatedHours() . ','; } elseif( true == array_key_exists( 'EstimatedHours', $this->getChangedColumns() ) ) { $strSql .= ' estimated_hours = ' . $this->sqlEstimatedHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hourly_rate_employee_encryption_association_id = ' . $this->sqlHourlyRateEmployeeEncryptionAssociationId() . ','; } elseif( true == array_key_exists( 'HourlyRateEmployeeEncryptionAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' hourly_rate_employee_encryption_association_id = ' . $this->sqlHourlyRateEmployeeEncryptionAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote() . ','; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified() . ','; } elseif( true == array_key_exists( 'IsVerified', $this->getChangedColumns() ) ) { $strSql .= ' is_verified = ' . $this->sqlIsVerified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy() . ','; } elseif( true == array_key_exists( 'RequestedBy', $this->getChangedColumns() ) ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_on = ' . $this->sqlRequestedOn() . ','; } elseif( true == array_key_exists( 'RequestedOn', $this->getChangedColumns() ) ) { $strSql .= ' requested_on = ' . $this->sqlRequestedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'purchase_request_id' => $this->getPurchaseRequestId(),
			'previous_employee_encryption_association_id' => $this->getPreviousEmployeeEncryptionAssociationId(),
			'new_employee_encryption_association_id' => $this->getNewEmployeeEncryptionAssociationId(),
			'bonus_employee_encryption_association_id' => $this->getBonusEmployeeEncryptionAssociationId(),
			'estimated_hours' => $this->getEstimatedHours(),
			'hourly_rate_employee_encryption_association_id' => $this->getHourlyRateEmployeeEncryptionAssociationId(),
			'note' => $this->getNote(),
			'is_verified' => $this->getIsVerified(),
			'requested_by' => $this->getRequestedBy(),
			'requested_on' => $this->getRequestedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>