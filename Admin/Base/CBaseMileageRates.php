<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CMileageRates
 * Do not add any new functions to this class.
 */

class CBaseMileageRates extends CEosPluralBase {

	/**
	 * @return CMileageRate[]
	 */
	public static function fetchMileageRates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CMileageRate', $objDatabase );
	}

	/**
	 * @return CMileageRate
	 */
	public static function fetchMileageRate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CMileageRate', $objDatabase );
	}

	public static function fetchMileageRateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'mileage_rates', $objDatabase );
	}

	public static function fetchMileageRateById( $intId, $objDatabase ) {
		return self::fetchMileageRate( sprintf( 'SELECT * FROM mileage_rates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>