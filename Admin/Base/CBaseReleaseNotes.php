<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseNotes
 * Do not add any new functions to this class.
 */

class CBaseReleaseNotes extends CEosPluralBase {

	/**
	 * @return CReleaseNote[]
	 */
	public static function fetchReleaseNotes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReleaseNote::class, $objDatabase );
	}

	/**
	 * @return CReleaseNote
	 */
	public static function fetchReleaseNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReleaseNote::class, $objDatabase );
	}

	public static function fetchReleaseNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'release_notes', $objDatabase );
	}

	public static function fetchReleaseNoteById( $intId, $objDatabase ) {
		return self::fetchReleaseNote( sprintf( 'SELECT * FROM release_notes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReleaseNotesByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchReleaseNotes( sprintf( 'SELECT * FROM release_notes WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

	public static function fetchReleaseNotesByReleaseNoteTypeId( $intReleaseNoteTypeId, $objDatabase ) {
		return self::fetchReleaseNotes( sprintf( 'SELECT * FROM release_notes WHERE release_note_type_id = %d', ( int ) $intReleaseNoteTypeId ), $objDatabase );
	}

	public static function fetchReleaseNotesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchReleaseNotes( sprintf( 'SELECT * FROM release_notes WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>