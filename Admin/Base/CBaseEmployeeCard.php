<?php

class CBaseEmployeeCard extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_cards';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_strCardNumber;
	protected $m_intIsActive;
	protected $m_intIsTemp;
	protected $m_intIssuedBy;
	protected $m_strIssuedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsActive = '1';
		$this->m_intIsTemp = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['card_number'] ) && $boolDirectSet ) $this->set( 'm_strCardNumber', trim( stripcslashes( $arrValues['card_number'] ) ) ); elseif( isset( $arrValues['card_number'] ) ) $this->setCardNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['card_number'] ) : $arrValues['card_number'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_intIsActive', trim( $arrValues['is_active'] ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( $arrValues['is_active'] );
		if( isset( $arrValues['is_temp'] ) && $boolDirectSet ) $this->set( 'm_intIsTemp', trim( $arrValues['is_temp'] ) ); elseif( isset( $arrValues['is_temp'] ) ) $this->setIsTemp( $arrValues['is_temp'] );
		if( isset( $arrValues['issued_by'] ) && $boolDirectSet ) $this->set( 'm_intIssuedBy', trim( $arrValues['issued_by'] ) ); elseif( isset( $arrValues['issued_by'] ) ) $this->setIssuedBy( $arrValues['issued_by'] );
		if( isset( $arrValues['issued_on'] ) && $boolDirectSet ) $this->set( 'm_strIssuedOn', trim( $arrValues['issued_on'] ) ); elseif( isset( $arrValues['issued_on'] ) ) $this->setIssuedOn( $arrValues['issued_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setCardNumber( $strCardNumber ) {
		$this->set( 'm_strCardNumber', CStrings::strTrimDef( $strCardNumber, 20, NULL, true ) );
	}

	public function getCardNumber() {
		return $this->m_strCardNumber;
	}

	public function sqlCardNumber() {
		return ( true == isset( $this->m_strCardNumber ) ) ? '\'' . addslashes( $this->m_strCardNumber ) . '\'' : 'NULL';
	}

	public function setIsActive( $intIsActive ) {
		$this->set( 'm_intIsActive', CStrings::strToIntDef( $intIsActive, NULL, false ) );
	}

	public function getIsActive() {
		return $this->m_intIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_intIsActive ) ) ? ( string ) $this->m_intIsActive : '1';
	}

	public function setIsTemp( $intIsTemp ) {
		$this->set( 'm_intIsTemp', CStrings::strToIntDef( $intIsTemp, NULL, false ) );
	}

	public function getIsTemp() {
		return $this->m_intIsTemp;
	}

	public function sqlIsTemp() {
		return ( true == isset( $this->m_intIsTemp ) ) ? ( string ) $this->m_intIsTemp : '0';
	}

	public function setIssuedBy( $intIssuedBy ) {
		$this->set( 'm_intIssuedBy', CStrings::strToIntDef( $intIssuedBy, NULL, false ) );
	}

	public function getIssuedBy() {
		return $this->m_intIssuedBy;
	}

	public function sqlIssuedBy() {
		return ( true == isset( $this->m_intIssuedBy ) ) ? ( string ) $this->m_intIssuedBy : 'NULL';
	}

	public function setIssuedOn( $strIssuedOn ) {
		$this->set( 'm_strIssuedOn', CStrings::strTrimDef( $strIssuedOn, -1, NULL, true ) );
	}

	public function getIssuedOn() {
		return $this->m_strIssuedOn;
	}

	public function sqlIssuedOn() {
		return ( true == isset( $this->m_strIssuedOn ) ) ? '\'' . $this->m_strIssuedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, card_number, is_active, is_temp, issued_by, issued_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlCardNumber() . ', ' .
 						$this->sqlIsActive() . ', ' .
 						$this->sqlIsTemp() . ', ' .
 						$this->sqlIssuedBy() . ', ' .
 						$this->sqlIssuedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' card_number = ' . $this->sqlCardNumber() . ','; } elseif( true == array_key_exists( 'CardNumber', $this->getChangedColumns() ) ) { $strSql .= ' card_number = ' . $this->sqlCardNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_temp = ' . $this->sqlIsTemp() . ','; } elseif( true == array_key_exists( 'IsTemp', $this->getChangedColumns() ) ) { $strSql .= ' is_temp = ' . $this->sqlIsTemp() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' issued_by = ' . $this->sqlIssuedBy() . ','; } elseif( true == array_key_exists( 'IssuedBy', $this->getChangedColumns() ) ) { $strSql .= ' issued_by = ' . $this->sqlIssuedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' issued_on = ' . $this->sqlIssuedOn() . ','; } elseif( true == array_key_exists( 'IssuedOn', $this->getChangedColumns() ) ) { $strSql .= ' issued_on = ' . $this->sqlIssuedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'card_number' => $this->getCardNumber(),
			'is_active' => $this->getIsActive(),
			'is_temp' => $this->getIsTemp(),
			'issued_by' => $this->getIssuedBy(),
			'issued_on' => $this->getIssuedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>