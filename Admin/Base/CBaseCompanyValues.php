<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyValues
 * Do not add any new functions to this class.
 */

class CBaseCompanyValues extends CEosPluralBase {

	/**
	 * @return CCompanyValue[]
	 */
	public static function fetchCompanyValues( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyValue::class, $objDatabase );
	}

	/**
	 * @return CCompanyValue
	 */
	public static function fetchCompanyValue( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyValue::class, $objDatabase );
	}

	public static function fetchCompanyValueCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_values', $objDatabase );
	}

	public static function fetchCompanyValueById( $intId, $objDatabase ) {
		return self::fetchCompanyValue( sprintf( 'SELECT * FROM company_values WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>