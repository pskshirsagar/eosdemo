<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemPropertyDetails
 * Do not add any new functions to this class.
 */

class CBaseSemPropertyDetails extends CEosPluralBase {

	/**
	 * @return CSemPropertyDetail[]
	 */
	public static function fetchSemPropertyDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemPropertyDetail', $objDatabase );
	}

	/**
	 * @return CSemPropertyDetail
	 */
	public static function fetchSemPropertyDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemPropertyDetail', $objDatabase );
	}

	public static function fetchSemPropertyDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_property_details', $objDatabase );
	}

	public static function fetchSemPropertyDetailById( $intId, $objDatabase ) {
		return self::fetchSemPropertyDetail( sprintf( 'SELECT * FROM sem_property_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemPropertyDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchSemPropertyDetails( sprintf( 'SELECT * FROM sem_property_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSemPropertyDetailsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSemPropertyDetails( sprintf( 'SELECT * FROM sem_property_details WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchSemPropertyDetailsBySemPropertyStatusTypeId( $intSemPropertyStatusTypeId, $objDatabase ) {
		return self::fetchSemPropertyDetails( sprintf( 'SELECT * FROM sem_property_details WHERE sem_property_status_type_id = %d', ( int ) $intSemPropertyStatusTypeId ), $objDatabase );
	}

	public static function fetchSemPropertyDetailsByCompanyUserId( $intCompanyUserId, $objDatabase ) {
		return self::fetchSemPropertyDetails( sprintf( 'SELECT * FROM sem_property_details WHERE company_user_id = %d', ( int ) $intCompanyUserId ), $objDatabase );
	}

}
?>