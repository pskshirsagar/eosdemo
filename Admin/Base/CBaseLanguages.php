<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CLanguages
 * Do not add any new functions to this class.
 */

class CBaseLanguages extends CEosPluralBase {

	/**
	 * @return CLanguage[]
	 */
	public static function fetchLanguages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CLanguage', $objDatabase );
	}

	/**
	 * @return CLanguage
	 */
	public static function fetchLanguage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLanguage', $objDatabase );
	}

	public static function fetchLanguageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'languages', $objDatabase );
	}

	public static function fetchLanguageById( $intId, $objDatabase ) {
		return self::fetchLanguage( sprintf( 'SELECT * FROM languages WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>