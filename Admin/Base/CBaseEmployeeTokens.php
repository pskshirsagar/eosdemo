<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeTokens
 * Do not add any new functions to this class.
 */

class CBaseEmployeeTokens extends CEosPluralBase {

    const TABLE_EMPLOYEE_TOKENS = 'public.employee_tokens';

    public static function fetchEmployeeTokens( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CEmployeeToken', $objDatabase );
    }

    public static function fetchEmployeeToken( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CEmployeeToken', $objDatabase );
    }

    public static function fetchEmployeeTokenCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'employee_tokens', $objDatabase );
    }

    public static function fetchEmployeeTokenById( $intId, $objDatabase ) {
        return self::fetchEmployeeToken( sprintf( 'SELECT * FROM employee_tokens WHERE id = %d', (int) $intId ), $objDatabase );
    }

    public static function fetchEmployeeTokensByEmployeeId( $intEmployeeId, $objDatabase ) {
        return self::fetchEmployeeTokens( sprintf( 'SELECT * FROM employee_tokens WHERE employee_id = %d', (int) $intEmployeeId ), $objDatabase );
    }

}
?>