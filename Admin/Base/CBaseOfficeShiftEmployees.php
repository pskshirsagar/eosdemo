<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\COfficeShiftEmployees
 * Do not add any new functions to this class.
 */

class CBaseOfficeShiftEmployees extends CEosPluralBase {

	/**
	 * @return COfficeShiftEmployee[]
	 */
	public static function fetchOfficeShiftEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, COfficeShiftEmployee::class, $objDatabase );
	}

	/**
	 * @return COfficeShiftEmployee
	 */
	public static function fetchOfficeShiftEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, COfficeShiftEmployee::class, $objDatabase );
	}

	public static function fetchOfficeShiftEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'office_shift_employees', $objDatabase );
	}

	public static function fetchOfficeShiftEmployeeById( $intId, $objDatabase ) {
		return self::fetchOfficeShiftEmployee( sprintf( 'SELECT * FROM office_shift_employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchOfficeShiftEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchOfficeShiftEmployees( sprintf( 'SELECT * FROM office_shift_employees WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchOfficeShiftEmployeesByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchOfficeShiftEmployees( sprintf( 'SELECT * FROM office_shift_employees WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchOfficeShiftEmployeesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchOfficeShiftEmployees( sprintf( 'SELECT * FROM office_shift_employees WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchOfficeShiftEmployeesByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchOfficeShiftEmployees( sprintf( 'SELECT * FROM office_shift_employees WHERE ps_product_option_id = %d', ( int ) $intPsProductOptionId ), $objDatabase );
	}

	public static function fetchOfficeShiftEmployeesByOfficeShiftId( $intOfficeShiftId, $objDatabase ) {
		return self::fetchOfficeShiftEmployees( sprintf( 'SELECT * FROM office_shift_employees WHERE office_shift_id = %d', ( int ) $intOfficeShiftId ), $objDatabase );
	}

}
?>