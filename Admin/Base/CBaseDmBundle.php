<?php

class CBaseDmBundle extends CEosSingularBase {

	const TABLE_NAME = 'public.dm_bundles';

	protected $m_intId;
	protected $m_strName;
	protected $m_strType;
	protected $m_intDefaultYearlyCredit;
	protected $m_boolIsLogin;
	protected $m_boolIsLiveUrl;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strType = 'Ongoing';
		$this->m_intDefaultYearlyCredit = '0';
		$this->m_boolIsLogin = true;
		$this->m_boolIsLiveUrl = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['type'] ) && $boolDirectSet ) $this->set( 'm_strType', trim( stripcslashes( $arrValues['type'] ) ) ); elseif( isset( $arrValues['type'] ) ) $this->setType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['type'] ) : $arrValues['type'] );
		if( isset( $arrValues['default_yearly_credit'] ) && $boolDirectSet ) $this->set( 'm_intDefaultYearlyCredit', trim( $arrValues['default_yearly_credit'] ) ); elseif( isset( $arrValues['default_yearly_credit'] ) ) $this->setDefaultYearlyCredit( $arrValues['default_yearly_credit'] );
		if( isset( $arrValues['is_login'] ) && $boolDirectSet ) $this->set( 'm_boolIsLogin', trim( stripcslashes( $arrValues['is_login'] ) ) ); elseif( isset( $arrValues['is_login'] ) ) $this->setIsLogin( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_login'] ) : $arrValues['is_login'] );
		if( isset( $arrValues['is_live_url'] ) && $boolDirectSet ) $this->set( 'm_boolIsLiveUrl', trim( stripcslashes( $arrValues['is_live_url'] ) ) ); elseif( isset( $arrValues['is_live_url'] ) ) $this->setIsLiveUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_live_url'] ) : $arrValues['is_live_url'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setType( $strType ) {
		$this->set( 'm_strType', CStrings::strTrimDef( $strType, -1, NULL, true ) );
	}

	public function getType() {
		return $this->m_strType;
	}

	public function sqlType() {
		return ( true == isset( $this->m_strType ) ) ? '\'' . addslashes( $this->m_strType ) . '\'' : '\'Ongoing\'';
	}

	public function setDefaultYearlyCredit( $intDefaultYearlyCredit ) {
		$this->set( 'm_intDefaultYearlyCredit', CStrings::strToIntDef( $intDefaultYearlyCredit, NULL, false ) );
	}

	public function getDefaultYearlyCredit() {
		return $this->m_intDefaultYearlyCredit;
	}

	public function sqlDefaultYearlyCredit() {
		return ( true == isset( $this->m_intDefaultYearlyCredit ) ) ? ( string ) $this->m_intDefaultYearlyCredit : '0';
	}

	public function setIsLogin( $boolIsLogin ) {
		$this->set( 'm_boolIsLogin', CStrings::strToBool( $boolIsLogin ) );
	}

	public function getIsLogin() {
		return $this->m_boolIsLogin;
	}

	public function sqlIsLogin() {
		return ( true == isset( $this->m_boolIsLogin ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLogin ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsLiveUrl( $boolIsLiveUrl ) {
		$this->set( 'm_boolIsLiveUrl', CStrings::strToBool( $boolIsLiveUrl ) );
	}

	public function getIsLiveUrl() {
		return $this->m_boolIsLiveUrl;
	}

	public function sqlIsLiveUrl() {
		return ( true == isset( $this->m_boolIsLiveUrl ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLiveUrl ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, type, default_yearly_credit, is_login, is_live_url, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlType() . ', ' .
 						$this->sqlDefaultYearlyCredit() . ', ' .
 						$this->sqlIsLogin() . ', ' .
 						$this->sqlIsLiveUrl() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' type = ' . $this->sqlType() . ','; } elseif( true == array_key_exists( 'Type', $this->getChangedColumns() ) ) { $strSql .= ' type = ' . $this->sqlType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_yearly_credit = ' . $this->sqlDefaultYearlyCredit() . ','; } elseif( true == array_key_exists( 'DefaultYearlyCredit', $this->getChangedColumns() ) ) { $strSql .= ' default_yearly_credit = ' . $this->sqlDefaultYearlyCredit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_login = ' . $this->sqlIsLogin() . ','; } elseif( true == array_key_exists( 'IsLogin', $this->getChangedColumns() ) ) { $strSql .= ' is_login = ' . $this->sqlIsLogin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_live_url = ' . $this->sqlIsLiveUrl() . ','; } elseif( true == array_key_exists( 'IsLiveUrl', $this->getChangedColumns() ) ) { $strSql .= ' is_live_url = ' . $this->sqlIsLiveUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'type' => $this->getType(),
			'default_yearly_credit' => $this->getDefaultYearlyCredit(),
			'is_login' => $this->getIsLogin(),
			'is_live_url' => $this->getIsLiveUrl(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>