<?php

class CBaseEmployeeCallStat extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_call_stats';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intDepartmentId;
	protected $m_strDay;
	protected $m_strPhoneFirstPinIn;
	protected $m_strPhoneLastPinOut;
	protected $m_intUnansweredCalls;
	protected $m_intAnsweredCalls;
	protected $m_intVoicemailsReceived;
	protected $m_intPhoneIngoreCount;
	protected $m_intPhoneRejectCount;
	protected $m_intPhoneWrapUpMins;
	protected $m_intPhoneWaitingMins;
	protected $m_intPhoneRingingMins;
	protected $m_intPhoneTalkingMins;
	protected $m_intPhoneTotalActiveMins;
	protected $m_intPhoneRemovedMins;
	protected $m_intPhoneNewMins;
	protected $m_intPhoneBreakMins;
	protected $m_intPhoneMiscMins;
	protected $m_intPhoneLunchMins;
	protected $m_intPhoneProjectMins;
	protected $m_intPhoneMeetingMins;
	protected $m_intPhoneTrainingMins;
	protected $m_intPhoneCoachingMins;
	protected $m_intPhoneEmailQueueMins;
	protected $m_intPhoneChatMins;
	protected $m_intPhoneTotalMins;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intUnansweredCalls = '0';
		$this->m_intAnsweredCalls = '0';
		$this->m_intVoicemailsReceived = '0';
		$this->m_intPhoneIngoreCount = '0';
		$this->m_intPhoneRejectCount = '0';
		$this->m_intPhoneWrapUpMins = '0';
		$this->m_intPhoneWaitingMins = '0';
		$this->m_intPhoneRingingMins = '0';
		$this->m_intPhoneTalkingMins = '0';
		$this->m_intPhoneTotalActiveMins = '0';
		$this->m_intPhoneRemovedMins = '0';
		$this->m_intPhoneNewMins = '0';
		$this->m_intPhoneBreakMins = '0';
		$this->m_intPhoneMiscMins = '0';
		$this->m_intPhoneLunchMins = '0';
		$this->m_intPhoneProjectMins = '0';
		$this->m_intPhoneMeetingMins = '0';
		$this->m_intPhoneTrainingMins = '0';
		$this->m_intPhoneCoachingMins = '0';
		$this->m_intPhoneEmailQueueMins = '0';
		$this->m_intPhoneChatMins = '0';
		$this->m_intPhoneTotalMins = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['day'] ) && $boolDirectSet ) $this->set( 'm_strDay', trim( $arrValues['day'] ) ); elseif( isset( $arrValues['day'] ) ) $this->setDay( $arrValues['day'] );
		if( isset( $arrValues['phone_first_pin_in'] ) && $boolDirectSet ) $this->set( 'm_strPhoneFirstPinIn', trim( $arrValues['phone_first_pin_in'] ) ); elseif( isset( $arrValues['phone_first_pin_in'] ) ) $this->setPhoneFirstPinIn( $arrValues['phone_first_pin_in'] );
		if( isset( $arrValues['phone_last_pin_out'] ) && $boolDirectSet ) $this->set( 'm_strPhoneLastPinOut', trim( $arrValues['phone_last_pin_out'] ) ); elseif( isset( $arrValues['phone_last_pin_out'] ) ) $this->setPhoneLastPinOut( $arrValues['phone_last_pin_out'] );
		if( isset( $arrValues['unanswered_calls'] ) && $boolDirectSet ) $this->set( 'm_intUnansweredCalls', trim( $arrValues['unanswered_calls'] ) ); elseif( isset( $arrValues['unanswered_calls'] ) ) $this->setUnansweredCalls( $arrValues['unanswered_calls'] );
		if( isset( $arrValues['answered_calls'] ) && $boolDirectSet ) $this->set( 'm_intAnsweredCalls', trim( $arrValues['answered_calls'] ) ); elseif( isset( $arrValues['answered_calls'] ) ) $this->setAnsweredCalls( $arrValues['answered_calls'] );
		if( isset( $arrValues['voicemails_received'] ) && $boolDirectSet ) $this->set( 'm_intVoicemailsReceived', trim( $arrValues['voicemails_received'] ) ); elseif( isset( $arrValues['voicemails_received'] ) ) $this->setVoicemailsReceived( $arrValues['voicemails_received'] );
		if( isset( $arrValues['phone_ingore_count'] ) && $boolDirectSet ) $this->set( 'm_intPhoneIngoreCount', trim( $arrValues['phone_ingore_count'] ) ); elseif( isset( $arrValues['phone_ingore_count'] ) ) $this->setPhoneIngoreCount( $arrValues['phone_ingore_count'] );
		if( isset( $arrValues['phone_reject_count'] ) && $boolDirectSet ) $this->set( 'm_intPhoneRejectCount', trim( $arrValues['phone_reject_count'] ) ); elseif( isset( $arrValues['phone_reject_count'] ) ) $this->setPhoneRejectCount( $arrValues['phone_reject_count'] );
		if( isset( $arrValues['phone_wrap_up_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneWrapUpMins', trim( $arrValues['phone_wrap_up_mins'] ) ); elseif( isset( $arrValues['phone_wrap_up_mins'] ) ) $this->setPhoneWrapUpMins( $arrValues['phone_wrap_up_mins'] );
		if( isset( $arrValues['phone_waiting_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneWaitingMins', trim( $arrValues['phone_waiting_mins'] ) ); elseif( isset( $arrValues['phone_waiting_mins'] ) ) $this->setPhoneWaitingMins( $arrValues['phone_waiting_mins'] );
		if( isset( $arrValues['phone_ringing_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneRingingMins', trim( $arrValues['phone_ringing_mins'] ) ); elseif( isset( $arrValues['phone_ringing_mins'] ) ) $this->setPhoneRingingMins( $arrValues['phone_ringing_mins'] );
		if( isset( $arrValues['phone_talking_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneTalkingMins', trim( $arrValues['phone_talking_mins'] ) ); elseif( isset( $arrValues['phone_talking_mins'] ) ) $this->setPhoneTalkingMins( $arrValues['phone_talking_mins'] );
		if( isset( $arrValues['phone_total_active_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneTotalActiveMins', trim( $arrValues['phone_total_active_mins'] ) ); elseif( isset( $arrValues['phone_total_active_mins'] ) ) $this->setPhoneTotalActiveMins( $arrValues['phone_total_active_mins'] );
		if( isset( $arrValues['phone_removed_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneRemovedMins', trim( $arrValues['phone_removed_mins'] ) ); elseif( isset( $arrValues['phone_removed_mins'] ) ) $this->setPhoneRemovedMins( $arrValues['phone_removed_mins'] );
		if( isset( $arrValues['phone_new_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneNewMins', trim( $arrValues['phone_new_mins'] ) ); elseif( isset( $arrValues['phone_new_mins'] ) ) $this->setPhoneNewMins( $arrValues['phone_new_mins'] );
		if( isset( $arrValues['phone_break_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneBreakMins', trim( $arrValues['phone_break_mins'] ) ); elseif( isset( $arrValues['phone_break_mins'] ) ) $this->setPhoneBreakMins( $arrValues['phone_break_mins'] );
		if( isset( $arrValues['phone_misc_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneMiscMins', trim( $arrValues['phone_misc_mins'] ) ); elseif( isset( $arrValues['phone_misc_mins'] ) ) $this->setPhoneMiscMins( $arrValues['phone_misc_mins'] );
		if( isset( $arrValues['phone_lunch_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneLunchMins', trim( $arrValues['phone_lunch_mins'] ) ); elseif( isset( $arrValues['phone_lunch_mins'] ) ) $this->setPhoneLunchMins( $arrValues['phone_lunch_mins'] );
		if( isset( $arrValues['phone_project_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneProjectMins', trim( $arrValues['phone_project_mins'] ) ); elseif( isset( $arrValues['phone_project_mins'] ) ) $this->setPhoneProjectMins( $arrValues['phone_project_mins'] );
		if( isset( $arrValues['phone_meeting_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneMeetingMins', trim( $arrValues['phone_meeting_mins'] ) ); elseif( isset( $arrValues['phone_meeting_mins'] ) ) $this->setPhoneMeetingMins( $arrValues['phone_meeting_mins'] );
		if( isset( $arrValues['phone_training_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneTrainingMins', trim( $arrValues['phone_training_mins'] ) ); elseif( isset( $arrValues['phone_training_mins'] ) ) $this->setPhoneTrainingMins( $arrValues['phone_training_mins'] );
		if( isset( $arrValues['phone_coaching_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneCoachingMins', trim( $arrValues['phone_coaching_mins'] ) ); elseif( isset( $arrValues['phone_coaching_mins'] ) ) $this->setPhoneCoachingMins( $arrValues['phone_coaching_mins'] );
		if( isset( $arrValues['phone_email_queue_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneEmailQueueMins', trim( $arrValues['phone_email_queue_mins'] ) ); elseif( isset( $arrValues['phone_email_queue_mins'] ) ) $this->setPhoneEmailQueueMins( $arrValues['phone_email_queue_mins'] );
		if( isset( $arrValues['phone_chat_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneChatMins', trim( $arrValues['phone_chat_mins'] ) ); elseif( isset( $arrValues['phone_chat_mins'] ) ) $this->setPhoneChatMins( $arrValues['phone_chat_mins'] );
		if( isset( $arrValues['phone_total_mins'] ) && $boolDirectSet ) $this->set( 'm_intPhoneTotalMins', trim( $arrValues['phone_total_mins'] ) ); elseif( isset( $arrValues['phone_total_mins'] ) ) $this->setPhoneTotalMins( $arrValues['phone_total_mins'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setDay( $strDay ) {
		$this->set( 'm_strDay', CStrings::strTrimDef( $strDay, -1, NULL, true ) );
	}

	public function getDay() {
		return $this->m_strDay;
	}

	public function sqlDay() {
		return ( true == isset( $this->m_strDay ) ) ? '\'' . $this->m_strDay . '\'' : 'NULL';
	}

	public function setPhoneFirstPinIn( $strPhoneFirstPinIn ) {
		$this->set( 'm_strPhoneFirstPinIn', CStrings::strTrimDef( $strPhoneFirstPinIn, -1, NULL, true ) );
	}

	public function getPhoneFirstPinIn() {
		return $this->m_strPhoneFirstPinIn;
	}

	public function sqlPhoneFirstPinIn() {
		return ( true == isset( $this->m_strPhoneFirstPinIn ) ) ? '\'' . $this->m_strPhoneFirstPinIn . '\'' : 'NULL';
	}

	public function setPhoneLastPinOut( $strPhoneLastPinOut ) {
		$this->set( 'm_strPhoneLastPinOut', CStrings::strTrimDef( $strPhoneLastPinOut, -1, NULL, true ) );
	}

	public function getPhoneLastPinOut() {
		return $this->m_strPhoneLastPinOut;
	}

	public function sqlPhoneLastPinOut() {
		return ( true == isset( $this->m_strPhoneLastPinOut ) ) ? '\'' . $this->m_strPhoneLastPinOut . '\'' : 'NULL';
	}

	public function setUnansweredCalls( $intUnansweredCalls ) {
		$this->set( 'm_intUnansweredCalls', CStrings::strToIntDef( $intUnansweredCalls, NULL, false ) );
	}

	public function getUnansweredCalls() {
		return $this->m_intUnansweredCalls;
	}

	public function sqlUnansweredCalls() {
		return ( true == isset( $this->m_intUnansweredCalls ) ) ? ( string ) $this->m_intUnansweredCalls : '0';
	}

	public function setAnsweredCalls( $intAnsweredCalls ) {
		$this->set( 'm_intAnsweredCalls', CStrings::strToIntDef( $intAnsweredCalls, NULL, false ) );
	}

	public function getAnsweredCalls() {
		return $this->m_intAnsweredCalls;
	}

	public function sqlAnsweredCalls() {
		return ( true == isset( $this->m_intAnsweredCalls ) ) ? ( string ) $this->m_intAnsweredCalls : '0';
	}

	public function setVoicemailsReceived( $intVoicemailsReceived ) {
		$this->set( 'm_intVoicemailsReceived', CStrings::strToIntDef( $intVoicemailsReceived, NULL, false ) );
	}

	public function getVoicemailsReceived() {
		return $this->m_intVoicemailsReceived;
	}

	public function sqlVoicemailsReceived() {
		return ( true == isset( $this->m_intVoicemailsReceived ) ) ? ( string ) $this->m_intVoicemailsReceived : '0';
	}

	public function setPhoneIngoreCount( $intPhoneIngoreCount ) {
		$this->set( 'm_intPhoneIngoreCount', CStrings::strToIntDef( $intPhoneIngoreCount, NULL, false ) );
	}

	public function getPhoneIngoreCount() {
		return $this->m_intPhoneIngoreCount;
	}

	public function sqlPhoneIngoreCount() {
		return ( true == isset( $this->m_intPhoneIngoreCount ) ) ? ( string ) $this->m_intPhoneIngoreCount : '0';
	}

	public function setPhoneRejectCount( $intPhoneRejectCount ) {
		$this->set( 'm_intPhoneRejectCount', CStrings::strToIntDef( $intPhoneRejectCount, NULL, false ) );
	}

	public function getPhoneRejectCount() {
		return $this->m_intPhoneRejectCount;
	}

	public function sqlPhoneRejectCount() {
		return ( true == isset( $this->m_intPhoneRejectCount ) ) ? ( string ) $this->m_intPhoneRejectCount : '0';
	}

	public function setPhoneWrapUpMins( $intPhoneWrapUpMins ) {
		$this->set( 'm_intPhoneWrapUpMins', CStrings::strToIntDef( $intPhoneWrapUpMins, NULL, false ) );
	}

	public function getPhoneWrapUpMins() {
		return $this->m_intPhoneWrapUpMins;
	}

	public function sqlPhoneWrapUpMins() {
		return ( true == isset( $this->m_intPhoneWrapUpMins ) ) ? ( string ) $this->m_intPhoneWrapUpMins : '0';
	}

	public function setPhoneWaitingMins( $intPhoneWaitingMins ) {
		$this->set( 'm_intPhoneWaitingMins', CStrings::strToIntDef( $intPhoneWaitingMins, NULL, false ) );
	}

	public function getPhoneWaitingMins() {
		return $this->m_intPhoneWaitingMins;
	}

	public function sqlPhoneWaitingMins() {
		return ( true == isset( $this->m_intPhoneWaitingMins ) ) ? ( string ) $this->m_intPhoneWaitingMins : '0';
	}

	public function setPhoneRingingMins( $intPhoneRingingMins ) {
		$this->set( 'm_intPhoneRingingMins', CStrings::strToIntDef( $intPhoneRingingMins, NULL, false ) );
	}

	public function getPhoneRingingMins() {
		return $this->m_intPhoneRingingMins;
	}

	public function sqlPhoneRingingMins() {
		return ( true == isset( $this->m_intPhoneRingingMins ) ) ? ( string ) $this->m_intPhoneRingingMins : '0';
	}

	public function setPhoneTalkingMins( $intPhoneTalkingMins ) {
		$this->set( 'm_intPhoneTalkingMins', CStrings::strToIntDef( $intPhoneTalkingMins, NULL, false ) );
	}

	public function getPhoneTalkingMins() {
		return $this->m_intPhoneTalkingMins;
	}

	public function sqlPhoneTalkingMins() {
		return ( true == isset( $this->m_intPhoneTalkingMins ) ) ? ( string ) $this->m_intPhoneTalkingMins : '0';
	}

	public function setPhoneTotalActiveMins( $intPhoneTotalActiveMins ) {
		$this->set( 'm_intPhoneTotalActiveMins', CStrings::strToIntDef( $intPhoneTotalActiveMins, NULL, false ) );
	}

	public function getPhoneTotalActiveMins() {
		return $this->m_intPhoneTotalActiveMins;
	}

	public function sqlPhoneTotalActiveMins() {
		return ( true == isset( $this->m_intPhoneTotalActiveMins ) ) ? ( string ) $this->m_intPhoneTotalActiveMins : '0';
	}

	public function setPhoneRemovedMins( $intPhoneRemovedMins ) {
		$this->set( 'm_intPhoneRemovedMins', CStrings::strToIntDef( $intPhoneRemovedMins, NULL, false ) );
	}

	public function getPhoneRemovedMins() {
		return $this->m_intPhoneRemovedMins;
	}

	public function sqlPhoneRemovedMins() {
		return ( true == isset( $this->m_intPhoneRemovedMins ) ) ? ( string ) $this->m_intPhoneRemovedMins : '0';
	}

	public function setPhoneNewMins( $intPhoneNewMins ) {
		$this->set( 'm_intPhoneNewMins', CStrings::strToIntDef( $intPhoneNewMins, NULL, false ) );
	}

	public function getPhoneNewMins() {
		return $this->m_intPhoneNewMins;
	}

	public function sqlPhoneNewMins() {
		return ( true == isset( $this->m_intPhoneNewMins ) ) ? ( string ) $this->m_intPhoneNewMins : '0';
	}

	public function setPhoneBreakMins( $intPhoneBreakMins ) {
		$this->set( 'm_intPhoneBreakMins', CStrings::strToIntDef( $intPhoneBreakMins, NULL, false ) );
	}

	public function getPhoneBreakMins() {
		return $this->m_intPhoneBreakMins;
	}

	public function sqlPhoneBreakMins() {
		return ( true == isset( $this->m_intPhoneBreakMins ) ) ? ( string ) $this->m_intPhoneBreakMins : '0';
	}

	public function setPhoneMiscMins( $intPhoneMiscMins ) {
		$this->set( 'm_intPhoneMiscMins', CStrings::strToIntDef( $intPhoneMiscMins, NULL, false ) );
	}

	public function getPhoneMiscMins() {
		return $this->m_intPhoneMiscMins;
	}

	public function sqlPhoneMiscMins() {
		return ( true == isset( $this->m_intPhoneMiscMins ) ) ? ( string ) $this->m_intPhoneMiscMins : '0';
	}

	public function setPhoneLunchMins( $intPhoneLunchMins ) {
		$this->set( 'm_intPhoneLunchMins', CStrings::strToIntDef( $intPhoneLunchMins, NULL, false ) );
	}

	public function getPhoneLunchMins() {
		return $this->m_intPhoneLunchMins;
	}

	public function sqlPhoneLunchMins() {
		return ( true == isset( $this->m_intPhoneLunchMins ) ) ? ( string ) $this->m_intPhoneLunchMins : '0';
	}

	public function setPhoneProjectMins( $intPhoneProjectMins ) {
		$this->set( 'm_intPhoneProjectMins', CStrings::strToIntDef( $intPhoneProjectMins, NULL, false ) );
	}

	public function getPhoneProjectMins() {
		return $this->m_intPhoneProjectMins;
	}

	public function sqlPhoneProjectMins() {
		return ( true == isset( $this->m_intPhoneProjectMins ) ) ? ( string ) $this->m_intPhoneProjectMins : '0';
	}

	public function setPhoneMeetingMins( $intPhoneMeetingMins ) {
		$this->set( 'm_intPhoneMeetingMins', CStrings::strToIntDef( $intPhoneMeetingMins, NULL, false ) );
	}

	public function getPhoneMeetingMins() {
		return $this->m_intPhoneMeetingMins;
	}

	public function sqlPhoneMeetingMins() {
		return ( true == isset( $this->m_intPhoneMeetingMins ) ) ? ( string ) $this->m_intPhoneMeetingMins : '0';
	}

	public function setPhoneTrainingMins( $intPhoneTrainingMins ) {
		$this->set( 'm_intPhoneTrainingMins', CStrings::strToIntDef( $intPhoneTrainingMins, NULL, false ) );
	}

	public function getPhoneTrainingMins() {
		return $this->m_intPhoneTrainingMins;
	}

	public function sqlPhoneTrainingMins() {
		return ( true == isset( $this->m_intPhoneTrainingMins ) ) ? ( string ) $this->m_intPhoneTrainingMins : '0';
	}

	public function setPhoneCoachingMins( $intPhoneCoachingMins ) {
		$this->set( 'm_intPhoneCoachingMins', CStrings::strToIntDef( $intPhoneCoachingMins, NULL, false ) );
	}

	public function getPhoneCoachingMins() {
		return $this->m_intPhoneCoachingMins;
	}

	public function sqlPhoneCoachingMins() {
		return ( true == isset( $this->m_intPhoneCoachingMins ) ) ? ( string ) $this->m_intPhoneCoachingMins : '0';
	}

	public function setPhoneEmailQueueMins( $intPhoneEmailQueueMins ) {
		$this->set( 'm_intPhoneEmailQueueMins', CStrings::strToIntDef( $intPhoneEmailQueueMins, NULL, false ) );
	}

	public function getPhoneEmailQueueMins() {
		return $this->m_intPhoneEmailQueueMins;
	}

	public function sqlPhoneEmailQueueMins() {
		return ( true == isset( $this->m_intPhoneEmailQueueMins ) ) ? ( string ) $this->m_intPhoneEmailQueueMins : '0';
	}

	public function setPhoneChatMins( $intPhoneChatMins ) {
		$this->set( 'm_intPhoneChatMins', CStrings::strToIntDef( $intPhoneChatMins, NULL, false ) );
	}

	public function getPhoneChatMins() {
		return $this->m_intPhoneChatMins;
	}

	public function sqlPhoneChatMins() {
		return ( true == isset( $this->m_intPhoneChatMins ) ) ? ( string ) $this->m_intPhoneChatMins : '0';
	}

	public function setPhoneTotalMins( $intPhoneTotalMins ) {
		$this->set( 'm_intPhoneTotalMins', CStrings::strToIntDef( $intPhoneTotalMins, NULL, false ) );
	}

	public function getPhoneTotalMins() {
		return $this->m_intPhoneTotalMins;
	}

	public function sqlPhoneTotalMins() {
		return ( true == isset( $this->m_intPhoneTotalMins ) ) ? ( string ) $this->m_intPhoneTotalMins : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, department_id, day, phone_first_pin_in, phone_last_pin_out, unanswered_calls, answered_calls, voicemails_received, phone_ingore_count, phone_reject_count, phone_wrap_up_mins, phone_waiting_mins, phone_ringing_mins, phone_talking_mins, phone_total_active_mins, phone_removed_mins, phone_new_mins, phone_break_mins, phone_misc_mins, phone_lunch_mins, phone_project_mins, phone_meeting_mins, phone_training_mins, phone_coaching_mins, phone_email_queue_mins, phone_chat_mins, phone_total_mins, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlDepartmentId() . ', ' .
						$this->sqlDay() . ', ' .
						$this->sqlPhoneFirstPinIn() . ', ' .
						$this->sqlPhoneLastPinOut() . ', ' .
						$this->sqlUnansweredCalls() . ', ' .
						$this->sqlAnsweredCalls() . ', ' .
						$this->sqlVoicemailsReceived() . ', ' .
						$this->sqlPhoneIngoreCount() . ', ' .
						$this->sqlPhoneRejectCount() . ', ' .
						$this->sqlPhoneWrapUpMins() . ', ' .
						$this->sqlPhoneWaitingMins() . ', ' .
						$this->sqlPhoneRingingMins() . ', ' .
						$this->sqlPhoneTalkingMins() . ', ' .
						$this->sqlPhoneTotalActiveMins() . ', ' .
						$this->sqlPhoneRemovedMins() . ', ' .
						$this->sqlPhoneNewMins() . ', ' .
						$this->sqlPhoneBreakMins() . ', ' .
						$this->sqlPhoneMiscMins() . ', ' .
						$this->sqlPhoneLunchMins() . ', ' .
						$this->sqlPhoneProjectMins() . ', ' .
						$this->sqlPhoneMeetingMins() . ', ' .
						$this->sqlPhoneTrainingMins() . ', ' .
						$this->sqlPhoneCoachingMins() . ', ' .
						$this->sqlPhoneEmailQueueMins() . ', ' .
						$this->sqlPhoneChatMins() . ', ' .
						$this->sqlPhoneTotalMins() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId(). ',' ; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' day = ' . $this->sqlDay(). ',' ; } elseif( true == array_key_exists( 'Day', $this->getChangedColumns() ) ) { $strSql .= ' day = ' . $this->sqlDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_first_pin_in = ' . $this->sqlPhoneFirstPinIn(). ',' ; } elseif( true == array_key_exists( 'PhoneFirstPinIn', $this->getChangedColumns() ) ) { $strSql .= ' phone_first_pin_in = ' . $this->sqlPhoneFirstPinIn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_last_pin_out = ' . $this->sqlPhoneLastPinOut(). ',' ; } elseif( true == array_key_exists( 'PhoneLastPinOut', $this->getChangedColumns() ) ) { $strSql .= ' phone_last_pin_out = ' . $this->sqlPhoneLastPinOut() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unanswered_calls = ' . $this->sqlUnansweredCalls(). ',' ; } elseif( true == array_key_exists( 'UnansweredCalls', $this->getChangedColumns() ) ) { $strSql .= ' unanswered_calls = ' . $this->sqlUnansweredCalls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answered_calls = ' . $this->sqlAnsweredCalls(). ',' ; } elseif( true == array_key_exists( 'AnsweredCalls', $this->getChangedColumns() ) ) { $strSql .= ' answered_calls = ' . $this->sqlAnsweredCalls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' voicemails_received = ' . $this->sqlVoicemailsReceived(). ',' ; } elseif( true == array_key_exists( 'VoicemailsReceived', $this->getChangedColumns() ) ) { $strSql .= ' voicemails_received = ' . $this->sqlVoicemailsReceived() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_ingore_count = ' . $this->sqlPhoneIngoreCount(). ',' ; } elseif( true == array_key_exists( 'PhoneIngoreCount', $this->getChangedColumns() ) ) { $strSql .= ' phone_ingore_count = ' . $this->sqlPhoneIngoreCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_reject_count = ' . $this->sqlPhoneRejectCount(). ',' ; } elseif( true == array_key_exists( 'PhoneRejectCount', $this->getChangedColumns() ) ) { $strSql .= ' phone_reject_count = ' . $this->sqlPhoneRejectCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_wrap_up_mins = ' . $this->sqlPhoneWrapUpMins(). ',' ; } elseif( true == array_key_exists( 'PhoneWrapUpMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_wrap_up_mins = ' . $this->sqlPhoneWrapUpMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_waiting_mins = ' . $this->sqlPhoneWaitingMins(). ',' ; } elseif( true == array_key_exists( 'PhoneWaitingMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_waiting_mins = ' . $this->sqlPhoneWaitingMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_ringing_mins = ' . $this->sqlPhoneRingingMins(). ',' ; } elseif( true == array_key_exists( 'PhoneRingingMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_ringing_mins = ' . $this->sqlPhoneRingingMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_talking_mins = ' . $this->sqlPhoneTalkingMins(). ',' ; } elseif( true == array_key_exists( 'PhoneTalkingMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_talking_mins = ' . $this->sqlPhoneTalkingMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_total_active_mins = ' . $this->sqlPhoneTotalActiveMins(). ',' ; } elseif( true == array_key_exists( 'PhoneTotalActiveMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_total_active_mins = ' . $this->sqlPhoneTotalActiveMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_removed_mins = ' . $this->sqlPhoneRemovedMins(). ',' ; } elseif( true == array_key_exists( 'PhoneRemovedMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_removed_mins = ' . $this->sqlPhoneRemovedMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_new_mins = ' . $this->sqlPhoneNewMins(). ',' ; } elseif( true == array_key_exists( 'PhoneNewMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_new_mins = ' . $this->sqlPhoneNewMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_break_mins = ' . $this->sqlPhoneBreakMins(). ',' ; } elseif( true == array_key_exists( 'PhoneBreakMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_break_mins = ' . $this->sqlPhoneBreakMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_misc_mins = ' . $this->sqlPhoneMiscMins(). ',' ; } elseif( true == array_key_exists( 'PhoneMiscMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_misc_mins = ' . $this->sqlPhoneMiscMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_lunch_mins = ' . $this->sqlPhoneLunchMins(). ',' ; } elseif( true == array_key_exists( 'PhoneLunchMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_lunch_mins = ' . $this->sqlPhoneLunchMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_project_mins = ' . $this->sqlPhoneProjectMins(). ',' ; } elseif( true == array_key_exists( 'PhoneProjectMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_project_mins = ' . $this->sqlPhoneProjectMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_meeting_mins = ' . $this->sqlPhoneMeetingMins(). ',' ; } elseif( true == array_key_exists( 'PhoneMeetingMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_meeting_mins = ' . $this->sqlPhoneMeetingMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_training_mins = ' . $this->sqlPhoneTrainingMins(). ',' ; } elseif( true == array_key_exists( 'PhoneTrainingMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_training_mins = ' . $this->sqlPhoneTrainingMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_coaching_mins = ' . $this->sqlPhoneCoachingMins(). ',' ; } elseif( true == array_key_exists( 'PhoneCoachingMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_coaching_mins = ' . $this->sqlPhoneCoachingMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_email_queue_mins = ' . $this->sqlPhoneEmailQueueMins(). ',' ; } elseif( true == array_key_exists( 'PhoneEmailQueueMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_email_queue_mins = ' . $this->sqlPhoneEmailQueueMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_chat_mins = ' . $this->sqlPhoneChatMins(). ',' ; } elseif( true == array_key_exists( 'PhoneChatMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_chat_mins = ' . $this->sqlPhoneChatMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_total_mins = ' . $this->sqlPhoneTotalMins() ; } elseif( true == array_key_exists( 'PhoneTotalMins', $this->getChangedColumns() ) ) { $strSql .= ' phone_total_mins = ' . $this->sqlPhoneTotalMins() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'department_id' => $this->getDepartmentId(),
			'day' => $this->getDay(),
			'phone_first_pin_in' => $this->getPhoneFirstPinIn(),
			'phone_last_pin_out' => $this->getPhoneLastPinOut(),
			'unanswered_calls' => $this->getUnansweredCalls(),
			'answered_calls' => $this->getAnsweredCalls(),
			'voicemails_received' => $this->getVoicemailsReceived(),
			'phone_ingore_count' => $this->getPhoneIngoreCount(),
			'phone_reject_count' => $this->getPhoneRejectCount(),
			'phone_wrap_up_mins' => $this->getPhoneWrapUpMins(),
			'phone_waiting_mins' => $this->getPhoneWaitingMins(),
			'phone_ringing_mins' => $this->getPhoneRingingMins(),
			'phone_talking_mins' => $this->getPhoneTalkingMins(),
			'phone_total_active_mins' => $this->getPhoneTotalActiveMins(),
			'phone_removed_mins' => $this->getPhoneRemovedMins(),
			'phone_new_mins' => $this->getPhoneNewMins(),
			'phone_break_mins' => $this->getPhoneBreakMins(),
			'phone_misc_mins' => $this->getPhoneMiscMins(),
			'phone_lunch_mins' => $this->getPhoneLunchMins(),
			'phone_project_mins' => $this->getPhoneProjectMins(),
			'phone_meeting_mins' => $this->getPhoneMeetingMins(),
			'phone_training_mins' => $this->getPhoneTrainingMins(),
			'phone_coaching_mins' => $this->getPhoneCoachingMins(),
			'phone_email_queue_mins' => $this->getPhoneEmailQueueMins(),
			'phone_chat_mins' => $this->getPhoneChatMins(),
			'phone_total_mins' => $this->getPhoneTotalMins(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>