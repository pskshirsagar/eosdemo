<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyQuestionTypes
 * Do not add any new functions to this class.
 */

class CBaseSurveyQuestionTypes extends CEosPluralBase {

	/**
	 * @return CSurveyQuestionType[]
	 */
	public static function fetchSurveyQuestionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyQuestionType', $objDatabase );
	}

	/**
	 * @return CSurveyQuestionType
	 */
	public static function fetchSurveyQuestionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyQuestionType', $objDatabase );
	}

	public static function fetchSurveyQuestionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_question_types', $objDatabase );
	}

	public static function fetchSurveyQuestionTypeById( $intId, $objDatabase ) {
		return self::fetchSurveyQuestionType( sprintf( 'SELECT * FROM survey_question_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>