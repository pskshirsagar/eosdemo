<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsFilterTypes
 * Do not add any new functions to this class.
 */

class CBasePsFilterTypes extends CEosPluralBase {

	/**
	 * @return CPsFilterType[]
	 */
	public static function fetchPsFilterTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsFilterType', $objDatabase );
	}

	/**
	 * @return CPsFilterType
	 */
	public static function fetchPsFilterType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsFilterType', $objDatabase );
	}

	public static function fetchPsFilterTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_filter_types', $objDatabase );
	}

	public static function fetchPsFilterTypeById( $intId, $objDatabase ) {
		return self::fetchPsFilterType( sprintf( 'SELECT * FROM ps_filter_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>