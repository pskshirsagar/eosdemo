<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsSupportEmployees
 * Do not add any new functions to this class.
 */

class CBaseStatsSupportEmployees extends CEosPluralBase {

	/**
	 * @return CStatsSupportEmployee[]
	 */
	public static function fetchStatsSupportEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStatsSupportEmployee', $objDatabase );
	}

	/**
	 * @return CStatsSupportEmployee
	 */
	public static function fetchStatsSupportEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStatsSupportEmployee', $objDatabase );
	}

	public static function fetchStatsSupportEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stats_support_employees', $objDatabase );
	}

	public static function fetchStatsSupportEmployeeById( $intId, $objDatabase ) {
		return self::fetchStatsSupportEmployee( sprintf( 'SELECT * FROM stats_support_employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStatsSupportEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchStatsSupportEmployees( sprintf( 'SELECT * FROM stats_support_employees WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchStatsSupportEmployeesByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchStatsSupportEmployees( sprintf( 'SELECT * FROM stats_support_employees WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>