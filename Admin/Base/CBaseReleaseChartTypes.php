<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseChartTypes
 * Do not add any new functions to this class.
 */

class CBaseReleaseChartTypes extends CEosPluralBase {

	/**
	 * @return CReleaseChartType[]
	 */
	public static function fetchReleaseChartTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReleaseChartType::class, $objDatabase );
	}

	/**
	 * @return CReleaseChartType
	 */
	public static function fetchReleaseChartType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReleaseChartType::class, $objDatabase );
	}

	public static function fetchReleaseChartTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'release_chart_types', $objDatabase );
	}

	public static function fetchReleaseChartTypeById( $intId, $objDatabase ) {
		return self::fetchReleaseChartType( sprintf( 'SELECT * FROM release_chart_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>