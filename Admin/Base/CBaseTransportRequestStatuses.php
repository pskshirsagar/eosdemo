<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTransportRequestStatuses
 * Do not add any new functions to this class.
 */

class CBaseTransportRequestStatuses extends CEosPluralBase {

	/**
	 * @return CTransportRequestStatus[]
	 */
	public static function fetchTransportRequestStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTransportRequestStatus::class, $objDatabase );
	}

	/**
	 * @return CTransportRequestStatus
	 */
	public static function fetchTransportRequestStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTransportRequestStatus::class, $objDatabase );
	}

	public static function fetchTransportRequestStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transport_request_statuses', $objDatabase );
	}

	public static function fetchTransportRequestStatusById( $intId, $objDatabase ) {
		return self::fetchTransportRequestStatus( sprintf( 'SELECT * FROM transport_request_statuses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>