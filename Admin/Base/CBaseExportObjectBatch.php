<?php

class CBaseExportObjectBatch extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.export_object_batches';

	protected $m_intId;
	protected $m_strMonth;
	protected $m_intExportObjectBatchStatusId;
	protected $m_intExportPartnerId;
	protected $m_intExecutedBy;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['export_object_batch_status_id'] ) && $boolDirectSet ) $this->set( 'm_intExportObjectBatchStatusId', trim( $arrValues['export_object_batch_status_id'] ) ); elseif( isset( $arrValues['export_object_batch_status_id'] ) ) $this->setExportObjectBatchStatusId( $arrValues['export_object_batch_status_id'] );
		if( isset( $arrValues['export_partner_id'] ) && $boolDirectSet ) $this->set( 'm_intExportPartnerId', trim( $arrValues['export_partner_id'] ) ); elseif( isset( $arrValues['export_partner_id'] ) ) $this->setExportPartnerId( $arrValues['export_partner_id'] );
		if( isset( $arrValues['executed_by'] ) && $boolDirectSet ) $this->set( 'm_intExecutedBy', trim( $arrValues['executed_by'] ) ); elseif( isset( $arrValues['executed_by'] ) ) $this->setExecutedBy( $arrValues['executed_by'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NOW()';
	}

	public function setExportObjectBatchStatusId( $intExportObjectBatchStatusId ) {
		$this->set( 'm_intExportObjectBatchStatusId', CStrings::strToIntDef( $intExportObjectBatchStatusId, NULL, false ) );
	}

	public function getExportObjectBatchStatusId() {
		return $this->m_intExportObjectBatchStatusId;
	}

	public function sqlExportObjectBatchStatusId() {
		return ( true == isset( $this->m_intExportObjectBatchStatusId ) ) ? ( string ) $this->m_intExportObjectBatchStatusId : 'NULL';
	}

	public function setExportPartnerId( $intExportPartnerId ) {
		$this->set( 'm_intExportPartnerId', CStrings::strToIntDef( $intExportPartnerId, NULL, false ) );
	}

	public function getExportPartnerId() {
		return $this->m_intExportPartnerId;
	}

	public function sqlExportPartnerId() {
		return ( true == isset( $this->m_intExportPartnerId ) ) ? ( string ) $this->m_intExportPartnerId : 'NULL';
	}

	public function setExecutedBy( $intExecutedBy ) {
		$this->set( 'm_intExecutedBy', CStrings::strToIntDef( $intExecutedBy, NULL, false ) );
	}

	public function getExecutedBy() {
		return $this->m_intExecutedBy;
	}

	public function sqlExecutedBy() {
		return ( true == isset( $this->m_intExecutedBy ) ) ? ( string ) $this->m_intExecutedBy : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, month, export_object_batch_status_id, export_partner_id, executed_by, details, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlMonth() . ', ' .
						$this->sqlExportObjectBatchStatusId() . ', ' .
						$this->sqlExportPartnerId() . ', ' .
						$this->sqlExecutedBy() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth(). ',' ; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_object_batch_status_id = ' . $this->sqlExportObjectBatchStatusId(). ',' ; } elseif( true == array_key_exists( 'ExportObjectBatchStatusId', $this->getChangedColumns() ) ) { $strSql .= ' export_object_batch_status_id = ' . $this->sqlExportObjectBatchStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_partner_id = ' . $this->sqlExportPartnerId(). ',' ; } elseif( true == array_key_exists( 'ExportPartnerId', $this->getChangedColumns() ) ) { $strSql .= ' export_partner_id = ' . $this->sqlExportPartnerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_by = ' . $this->sqlExecutedBy(). ',' ; } elseif( true == array_key_exists( 'ExecutedBy', $this->getChangedColumns() ) ) { $strSql .= ' executed_by = ' . $this->sqlExecutedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'month' => $this->getMonth(),
			'export_object_batch_status_id' => $this->getExportObjectBatchStatusId(),
			'export_partner_id' => $this->getExportPartnerId(),
			'executed_by' => $this->getExecutedBy(),
			'details' => $this->getDetails(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>