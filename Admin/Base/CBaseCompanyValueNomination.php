<?php

class CBaseCompanyValueNomination extends CEosSingularBase {

	const TABLE_NAME = 'public.company_value_nominations';

	protected $m_intId;
	protected $m_arrintCompanyValueId;
	protected $m_intEmployeeId;
	protected $m_intNominatingEmployeeId;
	protected $m_intCompanyValueNominationBatchId;
	protected $m_strSpiel;
	protected $m_boolIsAnonymous;
	protected $m_strNominationDatetime;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsAnonymous = false;
		$this->m_strNominationDatetime = 'now()';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['company_value_id'] ) && $boolDirectSet ) $this->set( 'm_arrintCompanyValueId', trim( $arrValues['company_value_id'] ) ); elseif( isset( $arrValues['company_value_id'] ) ) $this->setCompanyValueId( $arrValues['company_value_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['nominating_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intNominatingEmployeeId', trim( $arrValues['nominating_employee_id'] ) ); elseif( isset( $arrValues['nominating_employee_id'] ) ) $this->setNominatingEmployeeId( $arrValues['nominating_employee_id'] );
		if( isset( $arrValues['company_value_nomination_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyValueNominationBatchId', trim( $arrValues['company_value_nomination_batch_id'] ) ); elseif( isset( $arrValues['company_value_nomination_batch_id'] ) ) $this->setCompanyValueNominationBatchId( $arrValues['company_value_nomination_batch_id'] );
		if( isset( $arrValues['spiel'] ) && $boolDirectSet ) $this->set( 'm_strSpiel', trim( stripcslashes( $arrValues['spiel'] ) ) ); elseif( isset( $arrValues['spiel'] ) ) $this->setSpiel( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['spiel'] ) : $arrValues['spiel'] );
		if( isset( $arrValues['is_anonymous'] ) && $boolDirectSet ) $this->set( 'm_boolIsAnonymous', trim( stripcslashes( $arrValues['is_anonymous'] ) ) ); elseif( isset( $arrValues['is_anonymous'] ) ) $this->setIsAnonymous( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_anonymous'] ) : $arrValues['is_anonymous'] );
		if( isset( $arrValues['nomination_datetime'] ) && $boolDirectSet ) $this->set( 'm_strNominationDatetime', trim( $arrValues['nomination_datetime'] ) ); elseif( isset( $arrValues['nomination_datetime'] ) ) $this->setNominationDatetime( $arrValues['nomination_datetime'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCompanyValueId( $arrintCompanyValueId ) {
		$this->set( 'm_arrintCompanyValueId', CStrings::strToArrIntDef( $arrintCompanyValueId, NULL ) );
	}

	public function getCompanyValueId() {
		return $this->m_arrintCompanyValueId;
	}

	public function sqlCompanyValueId() {
		return ( true == isset( $this->m_arrintCompanyValueId ) && true == valArr( $this->m_arrintCompanyValueId ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintCompanyValueId, NULL ) . '\'' : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setNominatingEmployeeId( $intNominatingEmployeeId ) {
		$this->set( 'm_intNominatingEmployeeId', CStrings::strToIntDef( $intNominatingEmployeeId, NULL, false ) );
	}

	public function getNominatingEmployeeId() {
		return $this->m_intNominatingEmployeeId;
	}

	public function sqlNominatingEmployeeId() {
		return ( true == isset( $this->m_intNominatingEmployeeId ) ) ? ( string ) $this->m_intNominatingEmployeeId : 'NULL';
	}

	public function setCompanyValueNominationBatchId( $intCompanyValueNominationBatchId ) {
		$this->set( 'm_intCompanyValueNominationBatchId', CStrings::strToIntDef( $intCompanyValueNominationBatchId, NULL, false ) );
	}

	public function getCompanyValueNominationBatchId() {
		return $this->m_intCompanyValueNominationBatchId;
	}

	public function sqlCompanyValueNominationBatchId() {
		return ( true == isset( $this->m_intCompanyValueNominationBatchId ) ) ? ( string ) $this->m_intCompanyValueNominationBatchId : 'NULL';
	}

	public function setSpiel( $strSpiel ) {
		$this->set( 'm_strSpiel', CStrings::strTrimDef( $strSpiel, -1, NULL, true ) );
	}

	public function getSpiel() {
		return $this->m_strSpiel;
	}

	public function sqlSpiel() {
		return ( true == isset( $this->m_strSpiel ) ) ? '\'' . addslashes( $this->m_strSpiel ) . '\'' : 'NULL';
	}

	public function setIsAnonymous( $boolIsAnonymous ) {
		$this->set( 'm_boolIsAnonymous', CStrings::strToBool( $boolIsAnonymous ) );
	}

	public function getIsAnonymous() {
		return $this->m_boolIsAnonymous;
	}

	public function sqlIsAnonymous() {
		return ( true == isset( $this->m_boolIsAnonymous ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAnonymous ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setNominationDatetime( $strNominationDatetime ) {
		$this->set( 'm_strNominationDatetime', CStrings::strTrimDef( $strNominationDatetime, -1, NULL, true ) );
	}

	public function getNominationDatetime() {
		return $this->m_strNominationDatetime;
	}

	public function sqlNominationDatetime() {
		return ( true == isset( $this->m_strNominationDatetime ) ) ? '\'' . $this->m_strNominationDatetime . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, company_value_id, employee_id, nominating_employee_id, company_value_nomination_batch_id, spiel, is_anonymous, nomination_datetime, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCompanyValueId() . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlNominatingEmployeeId() . ', ' .
						$this->sqlCompanyValueNominationBatchId() . ', ' .
						$this->sqlSpiel() . ', ' .
						$this->sqlIsAnonymous() . ', ' .
						$this->sqlNominationDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_value_id = ' . $this->sqlCompanyValueId(). ',' ; } elseif( true == array_key_exists( 'CompanyValueId', $this->getChangedColumns() ) ) { $strSql .= ' company_value_id = ' . $this->sqlCompanyValueId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nominating_employee_id = ' . $this->sqlNominatingEmployeeId(). ',' ; } elseif( true == array_key_exists( 'NominatingEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' nominating_employee_id = ' . $this->sqlNominatingEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_value_nomination_batch_id = ' . $this->sqlCompanyValueNominationBatchId(). ',' ; } elseif( true == array_key_exists( 'CompanyValueNominationBatchId', $this->getChangedColumns() ) ) { $strSql .= ' company_value_nomination_batch_id = ' . $this->sqlCompanyValueNominationBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' spiel = ' . $this->sqlSpiel(). ',' ; } elseif( true == array_key_exists( 'Spiel', $this->getChangedColumns() ) ) { $strSql .= ' spiel = ' . $this->sqlSpiel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_anonymous = ' . $this->sqlIsAnonymous(). ',' ; } elseif( true == array_key_exists( 'IsAnonymous', $this->getChangedColumns() ) ) { $strSql .= ' is_anonymous = ' . $this->sqlIsAnonymous() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nomination_datetime = ' . $this->sqlNominationDatetime(). ',' ; } elseif( true == array_key_exists( 'NominationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' nomination_datetime = ' . $this->sqlNominationDatetime() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'company_value_id' => $this->getCompanyValueId(),
			'employee_id' => $this->getEmployeeId(),
			'nominating_employee_id' => $this->getNominatingEmployeeId(),
			'company_value_nomination_batch_id' => $this->getCompanyValueNominationBatchId(),
			'spiel' => $this->getSpiel(),
			'is_anonymous' => $this->getIsAnonymous(),
			'nomination_datetime' => $this->getNominationDatetime(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>