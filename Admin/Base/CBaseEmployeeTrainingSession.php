<?php

class CBaseEmployeeTrainingSession extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_training_sessions';

	protected $m_intId;
	protected $m_intTrainingSessionId;
	protected $m_intUserId;
	protected $m_intGroupId;
	protected $m_intIsAvailable;
	protected $m_intIsPresent;
	protected $m_intIsLate;
	protected $m_strStartTime;
	protected $m_strEndTime;
	protected $m_strTraineeNote;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsAvailable = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['training_session_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingSessionId', trim( $arrValues['training_session_id'] ) ); elseif( isset( $arrValues['training_session_id'] ) ) $this->setTrainingSessionId( $arrValues['training_session_id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['group_id'] ) && $boolDirectSet ) $this->set( 'm_intGroupId', trim( $arrValues['group_id'] ) ); elseif( isset( $arrValues['group_id'] ) ) $this->setGroupId( $arrValues['group_id'] );
		if( isset( $arrValues['is_available'] ) && $boolDirectSet ) $this->set( 'm_intIsAvailable', trim( $arrValues['is_available'] ) ); elseif( isset( $arrValues['is_available'] ) ) $this->setIsAvailable( $arrValues['is_available'] );
		if( isset( $arrValues['is_present'] ) && $boolDirectSet ) $this->set( 'm_intIsPresent', trim( $arrValues['is_present'] ) ); elseif( isset( $arrValues['is_present'] ) ) $this->setIsPresent( $arrValues['is_present'] );
		if( isset( $arrValues['is_late'] ) && $boolDirectSet ) $this->set( 'm_intIsLate', trim( $arrValues['is_late'] ) ); elseif( isset( $arrValues['is_late'] ) ) $this->setIsLate( $arrValues['is_late'] );
		if( isset( $arrValues['start_time'] ) && $boolDirectSet ) $this->set( 'm_strStartTime', trim( $arrValues['start_time'] ) ); elseif( isset( $arrValues['start_time'] ) ) $this->setStartTime( $arrValues['start_time'] );
		if( isset( $arrValues['end_time'] ) && $boolDirectSet ) $this->set( 'm_strEndTime', trim( $arrValues['end_time'] ) ); elseif( isset( $arrValues['end_time'] ) ) $this->setEndTime( $arrValues['end_time'] );
		if( isset( $arrValues['trainee_note'] ) && $boolDirectSet ) $this->set( 'm_strTraineeNote', trim( stripcslashes( $arrValues['trainee_note'] ) ) ); elseif( isset( $arrValues['trainee_note'] ) ) $this->setTraineeNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['trainee_note'] ) : $arrValues['trainee_note'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTrainingSessionId( $intTrainingSessionId ) {
		$this->set( 'm_intTrainingSessionId', CStrings::strToIntDef( $intTrainingSessionId, NULL, false ) );
	}

	public function getTrainingSessionId() {
		return $this->m_intTrainingSessionId;
	}

	public function sqlTrainingSessionId() {
		return ( true == isset( $this->m_intTrainingSessionId ) ) ? ( string ) $this->m_intTrainingSessionId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setGroupId( $intGroupId ) {
		$this->set( 'm_intGroupId', CStrings::strToIntDef( $intGroupId, NULL, false ) );
	}

	public function getGroupId() {
		return $this->m_intGroupId;
	}

	public function sqlGroupId() {
		return ( true == isset( $this->m_intGroupId ) ) ? ( string ) $this->m_intGroupId : 'NULL';
	}

	public function setIsAvailable( $intIsAvailable ) {
		$this->set( 'm_intIsAvailable', CStrings::strToIntDef( $intIsAvailable, NULL, false ) );
	}

	public function getIsAvailable() {
		return $this->m_intIsAvailable;
	}

	public function sqlIsAvailable() {
		return ( true == isset( $this->m_intIsAvailable ) ) ? ( string ) $this->m_intIsAvailable : '1';
	}

	public function setIsPresent( $intIsPresent ) {
		$this->set( 'm_intIsPresent', CStrings::strToIntDef( $intIsPresent, NULL, false ) );
	}

	public function getIsPresent() {
		return $this->m_intIsPresent;
	}

	public function sqlIsPresent() {
		return ( true == isset( $this->m_intIsPresent ) ) ? ( string ) $this->m_intIsPresent : 'NULL';
	}

	public function setIsLate( $intIsLate ) {
		$this->set( 'm_intIsLate', CStrings::strToIntDef( $intIsLate, NULL, false ) );
	}

	public function getIsLate() {
		return $this->m_intIsLate;
	}

	public function sqlIsLate() {
		return ( true == isset( $this->m_intIsLate ) ) ? ( string ) $this->m_intIsLate : 'NULL';
	}

	public function setStartTime( $strStartTime ) {
		$this->set( 'm_strStartTime', CStrings::strTrimDef( $strStartTime, -1, NULL, true ) );
	}

	public function getStartTime() {
		return $this->m_strStartTime;
	}

	public function sqlStartTime() {
		return ( true == isset( $this->m_strStartTime ) ) ? '\'' . $this->m_strStartTime . '\'' : 'NULL';
	}

	public function setEndTime( $strEndTime ) {
		$this->set( 'm_strEndTime', CStrings::strTrimDef( $strEndTime, -1, NULL, true ) );
	}

	public function getEndTime() {
		return $this->m_strEndTime;
	}

	public function sqlEndTime() {
		return ( true == isset( $this->m_strEndTime ) ) ? '\'' . $this->m_strEndTime . '\'' : 'NULL';
	}

	public function setTraineeNote( $strTraineeNote ) {
		$this->set( 'm_strTraineeNote', CStrings::strTrimDef( $strTraineeNote, 240, NULL, true ) );
	}

	public function getTraineeNote() {
		return $this->m_strTraineeNote;
	}

	public function sqlTraineeNote() {
		return ( true == isset( $this->m_strTraineeNote ) ) ? '\'' . addslashes( $this->m_strTraineeNote ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, training_session_id, user_id, group_id, is_available, is_present, is_late, start_time, end_time, trainee_note, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTrainingSessionId() . ', ' .
 						$this->sqlUserId() . ', ' .
 						$this->sqlGroupId() . ', ' .
 						$this->sqlIsAvailable() . ', ' .
 						$this->sqlIsPresent() . ', ' .
 						$this->sqlIsLate() . ', ' .
 						$this->sqlStartTime() . ', ' .
 						$this->sqlEndTime() . ', ' .
 						$this->sqlTraineeNote() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_session_id = ' . $this->sqlTrainingSessionId() . ','; } elseif( true == array_key_exists( 'TrainingSessionId', $this->getChangedColumns() ) ) { $strSql .= ' training_session_id = ' . $this->sqlTrainingSessionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_id = ' . $this->sqlGroupId() . ','; } elseif( true == array_key_exists( 'GroupId', $this->getChangedColumns() ) ) { $strSql .= ' group_id = ' . $this->sqlGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_available = ' . $this->sqlIsAvailable() . ','; } elseif( true == array_key_exists( 'IsAvailable', $this->getChangedColumns() ) ) { $strSql .= ' is_available = ' . $this->sqlIsAvailable() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_present = ' . $this->sqlIsPresent() . ','; } elseif( true == array_key_exists( 'IsPresent', $this->getChangedColumns() ) ) { $strSql .= ' is_present = ' . $this->sqlIsPresent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_late = ' . $this->sqlIsLate() . ','; } elseif( true == array_key_exists( 'IsLate', $this->getChangedColumns() ) ) { $strSql .= ' is_late = ' . $this->sqlIsLate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_time = ' . $this->sqlStartTime() . ','; } elseif( true == array_key_exists( 'StartTime', $this->getChangedColumns() ) ) { $strSql .= ' start_time = ' . $this->sqlStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_time = ' . $this->sqlEndTime() . ','; } elseif( true == array_key_exists( 'EndTime', $this->getChangedColumns() ) ) { $strSql .= ' end_time = ' . $this->sqlEndTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' trainee_note = ' . $this->sqlTraineeNote() . ','; } elseif( true == array_key_exists( 'TraineeNote', $this->getChangedColumns() ) ) { $strSql .= ' trainee_note = ' . $this->sqlTraineeNote() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'training_session_id' => $this->getTrainingSessionId(),
			'user_id' => $this->getUserId(),
			'group_id' => $this->getGroupId(),
			'is_available' => $this->getIsAvailable(),
			'is_present' => $this->getIsPresent(),
			'is_late' => $this->getIsLate(),
			'start_time' => $this->getStartTime(),
			'end_time' => $this->getEndTime(),
			'trainee_note' => $this->getTraineeNote(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>