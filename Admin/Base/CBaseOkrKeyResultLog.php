<?php

class CBaseOkrKeyResultLog extends CEosSingularBase {

	const TABLE_NAME = 'public.okr_key_result_logs';

	protected $m_intId;
	protected $m_intOkrKeyResultId;
	protected $m_intOkrKeyResultStatusTypeId;
	protected $m_intOwnerEmployeeId;
	protected $m_strKeyResultDescription;
	protected $m_fltProgress;
	protected $m_boolIsOnTrack;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDeliveryDate;
	protected $m_boolIsDeleted;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['okr_key_result_id'] ) && $boolDirectSet ) $this->set( 'm_intOkrKeyResultId', trim( $arrValues['okr_key_result_id'] ) ); elseif( isset( $arrValues['okr_key_result_id'] ) ) $this->setOkrKeyResultId( $arrValues['okr_key_result_id'] );
		if( isset( $arrValues['okr_key_result_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOkrKeyResultStatusTypeId', trim( $arrValues['okr_key_result_status_type_id'] ) ); elseif( isset( $arrValues['okr_key_result_status_type_id'] ) ) $this->setOkrKeyResultStatusTypeId( $arrValues['okr_key_result_status_type_id'] );
		if( isset( $arrValues['owner_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intOwnerEmployeeId', trim( $arrValues['owner_employee_id'] ) ); elseif( isset( $arrValues['owner_employee_id'] ) ) $this->setOwnerEmployeeId( $arrValues['owner_employee_id'] );
		if( isset( $arrValues['key_result_description'] ) && $boolDirectSet ) $this->set( 'm_strKeyResultDescription', trim( $arrValues['key_result_description'] ) ); elseif( isset( $arrValues['key_result_description'] ) ) $this->setKeyResultDescription( $arrValues['key_result_description'] );
		if( isset( $arrValues['progress'] ) && $boolDirectSet ) $this->set( 'm_fltProgress', trim( $arrValues['progress'] ) ); elseif( isset( $arrValues['progress'] ) ) $this->setProgress( $arrValues['progress'] );
		if( isset( $arrValues['is_on_track'] ) && $boolDirectSet ) $this->set( 'm_boolIsOnTrack', trim( stripcslashes( $arrValues['is_on_track'] ) ) ); elseif( isset( $arrValues['is_on_track'] ) ) $this->setIsOnTrack( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_on_track'] ) : $arrValues['is_on_track'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['delivery_date'] ) && $boolDirectSet ) $this->set( 'm_strDeliveryDate', trim( $arrValues['delivery_date'] ) ); elseif( isset( $arrValues['delivery_date'] ) ) $this->setDeliveryDate( $arrValues['delivery_date'] );
		if( isset( $arrValues['is_deleted'] ) && $boolDirectSet ) $this->set( 'm_boolIsDeleted', trim( stripcslashes( $arrValues['is_deleted'] ) ) ); elseif( isset( $arrValues['is_deleted'] ) ) $this->setIsDeleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_deleted'] ) : $arrValues['is_deleted'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOkrKeyResultId( $intOkrKeyResultId ) {
		$this->set( 'm_intOkrKeyResultId', CStrings::strToIntDef( $intOkrKeyResultId, NULL, false ) );
	}

	public function getOkrKeyResultId() {
		return $this->m_intOkrKeyResultId;
	}

	public function sqlOkrKeyResultId() {
		return ( true == isset( $this->m_intOkrKeyResultId ) ) ? ( string ) $this->m_intOkrKeyResultId : 'NULL';
	}

	public function setOkrKeyResultStatusTypeId( $intOkrKeyResultStatusTypeId ) {
		$this->set( 'm_intOkrKeyResultStatusTypeId', CStrings::strToIntDef( $intOkrKeyResultStatusTypeId, NULL, false ) );
	}

	public function getOkrKeyResultStatusTypeId() {
		return $this->m_intOkrKeyResultStatusTypeId;
	}

	public function sqlOkrKeyResultStatusTypeId() {
		return ( true == isset( $this->m_intOkrKeyResultStatusTypeId ) ) ? ( string ) $this->m_intOkrKeyResultStatusTypeId : 'NULL';
	}

	public function setOwnerEmployeeId( $intOwnerEmployeeId ) {
		$this->set( 'm_intOwnerEmployeeId', CStrings::strToIntDef( $intOwnerEmployeeId, NULL, false ) );
	}

	public function getOwnerEmployeeId() {
		return $this->m_intOwnerEmployeeId;
	}

	public function sqlOwnerEmployeeId() {
		return ( true == isset( $this->m_intOwnerEmployeeId ) ) ? ( string ) $this->m_intOwnerEmployeeId : 'NULL';
	}

	public function setKeyResultDescription( $strKeyResultDescription ) {
		$this->set( 'm_strKeyResultDescription', CStrings::strTrimDef( $strKeyResultDescription, -1, NULL, true ) );
	}

	public function getKeyResultDescription() {
		return $this->m_strKeyResultDescription;
	}

	public function sqlKeyResultDescription() {
		return ( true == isset( $this->m_strKeyResultDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strKeyResultDescription ) : '\'' . addslashes( $this->m_strKeyResultDescription ) . '\'' ) : 'NULL';
	}

	public function setProgress( $fltProgress ) {
		$this->set( 'm_fltProgress', CStrings::strToFloatDef( $fltProgress, NULL, false, 2 ) );
	}

	public function getProgress() {
		return $this->m_fltProgress;
	}

	public function sqlProgress() {
		return ( true == isset( $this->m_fltProgress ) ) ? ( string ) $this->m_fltProgress : 'NULL';
	}

	public function setIsOnTrack( $boolIsOnTrack ) {
		$this->set( 'm_boolIsOnTrack', CStrings::strToBool( $boolIsOnTrack ) );
	}

	public function getIsOnTrack() {
		return $this->m_boolIsOnTrack;
	}

	public function sqlIsOnTrack() {
		return ( true == isset( $this->m_boolIsOnTrack ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOnTrack ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setDeliveryDate( $strDeliveryDate ) {
		$this->set( 'm_strDeliveryDate', CStrings::strTrimDef( $strDeliveryDate, -1, NULL, true ) );
	}

	public function getDeliveryDate() {
		return $this->m_strDeliveryDate;
	}

	public function sqlDeliveryDate() {
		return ( true == isset( $this->m_strDeliveryDate ) ) ? '\'' . $this->m_strDeliveryDate . '\'' : 'NULL';
	}

	public function setIsDeleted( $boolIsDeleted ) {
		$this->set( 'm_boolIsDeleted', CStrings::strToBool( $boolIsDeleted ) );
	}

	public function getIsDeleted() {
		return $this->m_boolIsDeleted;
	}

	public function sqlIsDeleted() {
		return ( true == isset( $this->m_boolIsDeleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDeleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, okr_key_result_id, okr_key_result_status_type_id, owner_employee_id, key_result_description, progress, is_on_track, created_by, created_on, delivery_date, is_deleted )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlOkrKeyResultId() . ', ' .
						$this->sqlOkrKeyResultStatusTypeId() . ', ' .
						$this->sqlOwnerEmployeeId() . ', ' .
						$this->sqlKeyResultDescription() . ', ' .
						$this->sqlProgress() . ', ' .
						$this->sqlIsOnTrack() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDeliveryDate() . ', ' .
						$this->sqlIsDeleted() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' okr_key_result_id = ' . $this->sqlOkrKeyResultId(). ',' ; } elseif( true == array_key_exists( 'OkrKeyResultId', $this->getChangedColumns() ) ) { $strSql .= ' okr_key_result_id = ' . $this->sqlOkrKeyResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' okr_key_result_status_type_id = ' . $this->sqlOkrKeyResultStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'OkrKeyResultStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' okr_key_result_status_type_id = ' . $this->sqlOkrKeyResultStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_employee_id = ' . $this->sqlOwnerEmployeeId(). ',' ; } elseif( true == array_key_exists( 'OwnerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' owner_employee_id = ' . $this->sqlOwnerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key_result_description = ' . $this->sqlKeyResultDescription(). ',' ; } elseif( true == array_key_exists( 'KeyResultDescription', $this->getChangedColumns() ) ) { $strSql .= ' key_result_description = ' . $this->sqlKeyResultDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' progress = ' . $this->sqlProgress(). ',' ; } elseif( true == array_key_exists( 'Progress', $this->getChangedColumns() ) ) { $strSql .= ' progress = ' . $this->sqlProgress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_on_track = ' . $this->sqlIsOnTrack(). ',' ; } elseif( true == array_key_exists( 'IsOnTrack', $this->getChangedColumns() ) ) { $strSql .= ' is_on_track = ' . $this->sqlIsOnTrack() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delivery_date = ' . $this->sqlDeliveryDate(). ',' ; } elseif( true == array_key_exists( 'DeliveryDate', $this->getChangedColumns() ) ) { $strSql .= ' delivery_date = ' . $this->sqlDeliveryDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() ; } elseif( true == array_key_exists( 'IsDeleted', $this->getChangedColumns() ) ) { $strSql .= ' is_deleted = ' . $this->sqlIsDeleted() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'okr_key_result_id' => $this->getOkrKeyResultId(),
			'okr_key_result_status_type_id' => $this->getOkrKeyResultStatusTypeId(),
			'owner_employee_id' => $this->getOwnerEmployeeId(),
			'key_result_description' => $this->getKeyResultDescription(),
			'progress' => $this->getProgress(),
			'is_on_track' => $this->getIsOnTrack(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'delivery_date' => $this->getDeliveryDate(),
			'is_deleted' => $this->getIsDeleted()
		);
	}

}
?>