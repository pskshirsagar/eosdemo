<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPurchaseRequestActions
 * Do not add any new functions to this class.
 */

class CBasePurchaseRequestActions extends CEosPluralBase {

	/**
	 * @return CPurchaseRequestAction[]
	 */
	public static function fetchPurchaseRequestActions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPurchaseRequestAction::class, $objDatabase );
	}

	/**
	 * @return CPurchaseRequestAction
	 */
	public static function fetchPurchaseRequestAction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPurchaseRequestAction::class, $objDatabase );
	}

	public static function fetchPurchaseRequestActionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'purchase_request_actions', $objDatabase );
	}

	public static function fetchPurchaseRequestActionById( $intId, $objDatabase ) {
		return self::fetchPurchaseRequestAction( sprintf( 'SELECT * FROM purchase_request_actions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPurchaseRequestActionsByPurchaseRequestId( $intPurchaseRequestId, $objDatabase ) {
		return self::fetchPurchaseRequestActions( sprintf( 'SELECT * FROM purchase_request_actions WHERE purchase_request_id = %d', ( int ) $intPurchaseRequestId ), $objDatabase );
	}

}
?>