<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestQuestionAssociations
 * Do not add any new functions to this class.
 */

class CBaseTestQuestionAssociations extends CEosPluralBase {

	/**
	 * @return CTestQuestionAssociation[]
	 */
	public static function fetchTestQuestionAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestQuestionAssociation', $objDatabase );
	}

	/**
	 * @return CTestQuestionAssociation
	 */
	public static function fetchTestQuestionAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestQuestionAssociation', $objDatabase );
	}

	public static function fetchTestQuestionAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_question_associations', $objDatabase );
	}

	public static function fetchTestQuestionAssociationById( $intId, $objDatabase ) {
		return self::fetchTestQuestionAssociation( sprintf( 'SELECT * FROM test_question_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestQuestionAssociationsByTestId( $intTestId, $objDatabase ) {
		return self::fetchTestQuestionAssociations( sprintf( 'SELECT * FROM test_question_associations WHERE test_id = %d', ( int ) $intTestId ), $objDatabase );
	}

	public static function fetchTestQuestionAssociationsByTestQuestionId( $intTestQuestionId, $objDatabase ) {
		return self::fetchTestQuestionAssociations( sprintf( 'SELECT * FROM test_question_associations WHERE test_question_id = %d', ( int ) $intTestQuestionId ), $objDatabase );
	}

}
?>