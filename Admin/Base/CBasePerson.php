<?php

class CBasePerson extends CEosSingularBase {

	const TABLE_NAME = 'public.persons';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPsLeadId;
	protected $m_intPersonRoleId;
	protected $m_intCompanyUserId;
	protected $m_intPersonSalesRoleId;
	protected $m_intPromoterTypeId;
	protected $m_strCompanyName;
	protected $m_strNamePrefix;
	protected $m_strNameFirst;
	protected $m_strNameMiddle;
	protected $m_strNameLast;
	protected $m_strNameSuffix;
	protected $m_strTitle;
	protected $m_strEmailAddress;
	protected $m_strAddressLine1;
	protected $m_strAddressLine2;
	protected $m_strCity;
	protected $m_strStateCode;
	protected $m_strPostalCode;
	protected $m_strCountryCode;
	protected $m_strNotes;
	protected $m_intPermissionedUnits;
	protected $m_intIsPrimary;
	protected $m_intTotalLogins;
	protected $m_intIsDisabled;
	protected $m_boolIsDecisionMaker;
	protected $m_boolIsTaskNotification;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strPersonalEmailAddress;
	protected $m_strHomeAddressLineOne;
	protected $m_strHomeAddressLineTwo;
	protected $m_strHomeCity;
	protected $m_strHomeStateCode;
	protected $m_strHomePostalCode;
	protected $m_strHomeCountryCode;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPrimary = '0';
		$this->m_intIsDisabled = '0';
		$this->m_boolIsTaskNotification = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['person_role_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonRoleId', trim( $arrValues['person_role_id'] ) ); elseif( isset( $arrValues['person_role_id'] ) ) $this->setPersonRoleId( $arrValues['person_role_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['person_sales_role_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonSalesRoleId', trim( $arrValues['person_sales_role_id'] ) ); elseif( isset( $arrValues['person_sales_role_id'] ) ) $this->setPersonSalesRoleId( $arrValues['person_sales_role_id'] );
		if( isset( $arrValues['promoter_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPromoterTypeId', trim( $arrValues['promoter_type_id'] ) ); elseif( isset( $arrValues['promoter_type_id'] ) ) $this->setPromoterTypeId( $arrValues['promoter_type_id'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( $arrValues['company_name'] ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( $arrValues['company_name'] );
		if( isset( $arrValues['name_prefix'] ) && $boolDirectSet ) $this->set( 'm_strNamePrefix', trim( $arrValues['name_prefix'] ) ); elseif( isset( $arrValues['name_prefix'] ) ) $this->setNamePrefix( $arrValues['name_prefix'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_middle'] ) && $boolDirectSet ) $this->set( 'm_strNameMiddle', trim( $arrValues['name_middle'] ) ); elseif( isset( $arrValues['name_middle'] ) ) $this->setNameMiddle( $arrValues['name_middle'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['name_suffix'] ) && $boolDirectSet ) $this->set( 'm_strNameSuffix', trim( $arrValues['name_suffix'] ) ); elseif( isset( $arrValues['name_suffix'] ) ) $this->setNameSuffix( $arrValues['name_suffix'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['address_line1'] ) && $boolDirectSet ) $this->set( 'm_strAddressLine1', trim( $arrValues['address_line1'] ) ); elseif( isset( $arrValues['address_line1'] ) ) $this->setAddressLine1( $arrValues['address_line1'] );
		if( isset( $arrValues['address_line2'] ) && $boolDirectSet ) $this->set( 'm_strAddressLine2', trim( $arrValues['address_line2'] ) ); elseif( isset( $arrValues['address_line2'] ) ) $this->setAddressLine2( $arrValues['address_line2'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( $arrValues['city'] ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( $arrValues['city'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( $arrValues['state_code'] ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( $arrValues['state_code'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( $arrValues['postal_code'] ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( $arrValues['postal_code'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['permissioned_units'] ) && $boolDirectSet ) $this->set( 'm_intPermissionedUnits', trim( $arrValues['permissioned_units'] ) ); elseif( isset( $arrValues['permissioned_units'] ) ) $this->setPermissionedUnits( $arrValues['permissioned_units'] );
		if( isset( $arrValues['is_primary'] ) && $boolDirectSet ) $this->set( 'm_intIsPrimary', trim( $arrValues['is_primary'] ) ); elseif( isset( $arrValues['is_primary'] ) ) $this->setIsPrimary( $arrValues['is_primary'] );
		if( isset( $arrValues['total_logins'] ) && $boolDirectSet ) $this->set( 'm_intTotalLogins', trim( $arrValues['total_logins'] ) ); elseif( isset( $arrValues['total_logins'] ) ) $this->setTotalLogins( $arrValues['total_logins'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['is_decision_maker'] ) && $boolDirectSet ) $this->set( 'm_boolIsDecisionMaker', trim( stripcslashes( $arrValues['is_decision_maker'] ) ) ); elseif( isset( $arrValues['is_decision_maker'] ) ) $this->setIsDecisionMaker( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_decision_maker'] ) : $arrValues['is_decision_maker'] );
		if( isset( $arrValues['is_task_notification'] ) && $boolDirectSet ) $this->set( 'm_boolIsTaskNotification', trim( stripcslashes( $arrValues['is_task_notification'] ) ) ); elseif( isset( $arrValues['is_task_notification'] ) ) $this->setIsTaskNotification( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_task_notification'] ) : $arrValues['is_task_notification'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['personal_email_address'] ) && $boolDirectSet ) $this->set( 'm_strPersonalEmailAddress', trim( $arrValues['personal_email_address'] ) ); elseif( isset( $arrValues['personal_email_address'] ) ) $this->setPersonalEmailAddress( $arrValues['personal_email_address'] );
		if( isset( $arrValues['home_address_line_one'] ) && $boolDirectSet ) $this->set( 'm_strHomeAddressLineOne', trim( $arrValues['home_address_line_one'] ) ); elseif( isset( $arrValues['home_address_line_one'] ) ) $this->setHomeAddressLineOne( $arrValues['home_address_line_one'] );
		if( isset( $arrValues['home_address_line_two'] ) && $boolDirectSet ) $this->set( 'm_strHomeAddressLineTwo', trim( $arrValues['home_address_line_two'] ) ); elseif( isset( $arrValues['home_address_line_two'] ) ) $this->setHomeAddressLineTwo( $arrValues['home_address_line_two'] );
		if( isset( $arrValues['home_city'] ) && $boolDirectSet ) $this->set( 'm_strHomeCity', trim( $arrValues['home_city'] ) ); elseif( isset( $arrValues['home_city'] ) ) $this->setHomeCity( $arrValues['home_city'] );
		if( isset( $arrValues['home_state_code'] ) && $boolDirectSet ) $this->set( 'm_strHomeStateCode', trim( $arrValues['home_state_code'] ) ); elseif( isset( $arrValues['home_state_code'] ) ) $this->setHomeStateCode( $arrValues['home_state_code'] );
		if( isset( $arrValues['home_postal_code'] ) && $boolDirectSet ) $this->set( 'm_strHomePostalCode', trim( $arrValues['home_postal_code'] ) ); elseif( isset( $arrValues['home_postal_code'] ) ) $this->setHomePostalCode( $arrValues['home_postal_code'] );
		if( isset( $arrValues['home_country_code'] ) && $boolDirectSet ) $this->set( 'm_strHomeCountryCode', trim( $arrValues['home_country_code'] ) ); elseif( isset( $arrValues['home_country_code'] ) ) $this->setHomeCountryCode( $arrValues['home_country_code'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setPersonRoleId( $intPersonRoleId ) {
		$this->set( 'm_intPersonRoleId', CStrings::strToIntDef( $intPersonRoleId, NULL, false ) );
	}

	public function getPersonRoleId() {
		return $this->m_intPersonRoleId;
	}

	public function sqlPersonRoleId() {
		return ( true == isset( $this->m_intPersonRoleId ) ) ? ( string ) $this->m_intPersonRoleId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setPersonSalesRoleId( $intPersonSalesRoleId ) {
		$this->set( 'm_intPersonSalesRoleId', CStrings::strToIntDef( $intPersonSalesRoleId, NULL, false ) );
	}

	public function getPersonSalesRoleId() {
		return $this->m_intPersonSalesRoleId;
	}

	public function sqlPersonSalesRoleId() {
		return ( true == isset( $this->m_intPersonSalesRoleId ) ) ? ( string ) $this->m_intPersonSalesRoleId : 'NULL';
	}

	public function setPromoterTypeId( $intPromoterTypeId ) {
		$this->set( 'm_intPromoterTypeId', CStrings::strToIntDef( $intPromoterTypeId, NULL, false ) );
	}

	public function getPromoterTypeId() {
		return $this->m_intPromoterTypeId;
	}

	public function sqlPromoterTypeId() {
		return ( true == isset( $this->m_intPromoterTypeId ) ) ? ( string ) $this->m_intPromoterTypeId : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyName ) : '\'' . addslashes( $this->m_strCompanyName ) . '\'' ) : 'NULL';
	}

	public function setNamePrefix( $strNamePrefix ) {
		$this->set( 'm_strNamePrefix', CStrings::strTrimDef( $strNamePrefix, 20, NULL, true ) );
	}

	public function getNamePrefix() {
		return $this->m_strNamePrefix;
	}

	public function sqlNamePrefix() {
		return ( true == isset( $this->m_strNamePrefix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNamePrefix ) : '\'' . addslashes( $this->m_strNamePrefix ) . '\'' ) : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameMiddle( $strNameMiddle ) {
		$this->set( 'm_strNameMiddle', CStrings::strTrimDef( $strNameMiddle, 50, NULL, true ) );
	}

	public function getNameMiddle() {
		return $this->m_strNameMiddle;
	}

	public function sqlNameMiddle() {
		return ( true == isset( $this->m_strNameMiddle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameMiddle ) : '\'' . addslashes( $this->m_strNameMiddle ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setNameSuffix( $strNameSuffix ) {
		$this->set( 'm_strNameSuffix', CStrings::strTrimDef( $strNameSuffix, 20, NULL, true ) );
	}

	public function getNameSuffix() {
		return $this->m_strNameSuffix;
	}

	public function sqlNameSuffix() {
		return ( true == isset( $this->m_strNameSuffix ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameSuffix ) : '\'' . addslashes( $this->m_strNameSuffix ) . '\'' ) : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setAddressLine1( $strAddressLine1 ) {
		$this->set( 'm_strAddressLine1', CStrings::strTrimDef( $strAddressLine1, 100, NULL, true ) );
	}

	public function getAddressLine1() {
		return $this->m_strAddressLine1;
	}

	public function sqlAddressLine1() {
		return ( true == isset( $this->m_strAddressLine1 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAddressLine1 ) : '\'' . addslashes( $this->m_strAddressLine1 ) . '\'' ) : 'NULL';
	}

	public function setAddressLine2( $strAddressLine2 ) {
		$this->set( 'm_strAddressLine2', CStrings::strTrimDef( $strAddressLine2, 100, NULL, true ) );
	}

	public function getAddressLine2() {
		return $this->m_strAddressLine2;
	}

	public function sqlAddressLine2() {
		return ( true == isset( $this->m_strAddressLine2 ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAddressLine2 ) : '\'' . addslashes( $this->m_strAddressLine2 ) . '\'' ) : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCity ) : '\'' . addslashes( $this->m_strCity ) . '\'' ) : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStateCode ) : '\'' . addslashes( $this->m_strStateCode ) . '\'' ) : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPostalCode ) : '\'' . addslashes( $this->m_strPostalCode ) . '\'' ) : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountryCode ) : '\'' . addslashes( $this->m_strCountryCode ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setPermissionedUnits( $intPermissionedUnits ) {
		$this->set( 'm_intPermissionedUnits', CStrings::strToIntDef( $intPermissionedUnits, NULL, false ) );
	}

	public function getPermissionedUnits() {
		return $this->m_intPermissionedUnits;
	}

	public function sqlPermissionedUnits() {
		return ( true == isset( $this->m_intPermissionedUnits ) ) ? ( string ) $this->m_intPermissionedUnits : 'NULL';
	}

	public function setIsPrimary( $intIsPrimary ) {
		$this->set( 'm_intIsPrimary', CStrings::strToIntDef( $intIsPrimary, NULL, false ) );
	}

	public function getIsPrimary() {
		return $this->m_intIsPrimary;
	}

	public function sqlIsPrimary() {
		return ( true == isset( $this->m_intIsPrimary ) ) ? ( string ) $this->m_intIsPrimary : '0';
	}

	public function setTotalLogins( $intTotalLogins ) {
		$this->set( 'm_intTotalLogins', CStrings::strToIntDef( $intTotalLogins, NULL, false ) );
	}

	public function getTotalLogins() {
		return $this->m_intTotalLogins;
	}

	public function sqlTotalLogins() {
		return ( true == isset( $this->m_intTotalLogins ) ) ? ( string ) $this->m_intTotalLogins : 'NULL';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setIsDecisionMaker( $boolIsDecisionMaker ) {
		$this->set( 'm_boolIsDecisionMaker', CStrings::strToBool( $boolIsDecisionMaker ) );
	}

	public function getIsDecisionMaker() {
		return $this->m_boolIsDecisionMaker;
	}

	public function sqlIsDecisionMaker() {
		return ( true == isset( $this->m_boolIsDecisionMaker ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsDecisionMaker ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTaskNotification( $boolIsTaskNotification ) {
		$this->set( 'm_boolIsTaskNotification', CStrings::strToBool( $boolIsTaskNotification ) );
	}

	public function getIsTaskNotification() {
		return $this->m_boolIsTaskNotification;
	}

	public function sqlIsTaskNotification() {
		return ( true == isset( $this->m_boolIsTaskNotification ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTaskNotification ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPersonalEmailAddress( $strPersonalEmailAddress ) {
		$this->set( 'm_strPersonalEmailAddress', CStrings::strTrimDef( $strPersonalEmailAddress, -1, NULL, true ) );
	}

	public function getPersonalEmailAddress() {
		return $this->m_strPersonalEmailAddress;
	}

	public function sqlPersonalEmailAddress() {
		return ( true == isset( $this->m_strPersonalEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPersonalEmailAddress ) : '\'' . addslashes( $this->m_strPersonalEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setHomeAddressLineOne( $strHomeAddressLineOne ) {
		$this->set( 'm_strHomeAddressLineOne', CStrings::strTrimDef( $strHomeAddressLineOne, -1, NULL, true ) );
	}

	public function getHomeAddressLineOne() {
		return $this->m_strHomeAddressLineOne;
	}

	public function sqlHomeAddressLineOne() {
		return ( true == isset( $this->m_strHomeAddressLineOne ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHomeAddressLineOne ) : '\'' . addslashes( $this->m_strHomeAddressLineOne ) . '\'' ) : 'NULL';
	}

	public function setHomeAddressLineTwo( $strHomeAddressLineTwo ) {
		$this->set( 'm_strHomeAddressLineTwo', CStrings::strTrimDef( $strHomeAddressLineTwo, -1, NULL, true ) );
	}

	public function getHomeAddressLineTwo() {
		return $this->m_strHomeAddressLineTwo;
	}

	public function sqlHomeAddressLineTwo() {
		return ( true == isset( $this->m_strHomeAddressLineTwo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHomeAddressLineTwo ) : '\'' . addslashes( $this->m_strHomeAddressLineTwo ) . '\'' ) : 'NULL';
	}

	public function setHomeCity( $strHomeCity ) {
		$this->set( 'm_strHomeCity', CStrings::strTrimDef( $strHomeCity, -1, NULL, true ) );
	}

	public function getHomeCity() {
		return $this->m_strHomeCity;
	}

	public function sqlHomeCity() {
		return ( true == isset( $this->m_strHomeCity ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHomeCity ) : '\'' . addslashes( $this->m_strHomeCity ) . '\'' ) : 'NULL';
	}

	public function setHomeStateCode( $strHomeStateCode ) {
		$this->set( 'm_strHomeStateCode', CStrings::strTrimDef( $strHomeStateCode, -1, NULL, true ) );
	}

	public function getHomeStateCode() {
		return $this->m_strHomeStateCode;
	}

	public function sqlHomeStateCode() {
		return ( true == isset( $this->m_strHomeStateCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHomeStateCode ) : '\'' . addslashes( $this->m_strHomeStateCode ) . '\'' ) : 'NULL';
	}

	public function setHomePostalCode( $strHomePostalCode ) {
		$this->set( 'm_strHomePostalCode', CStrings::strTrimDef( $strHomePostalCode, -1, NULL, true ) );
	}

	public function getHomePostalCode() {
		return $this->m_strHomePostalCode;
	}

	public function sqlHomePostalCode() {
		return ( true == isset( $this->m_strHomePostalCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHomePostalCode ) : '\'' . addslashes( $this->m_strHomePostalCode ) . '\'' ) : 'NULL';
	}

	public function setHomeCountryCode( $strHomeCountryCode ) {
		$this->set( 'm_strHomeCountryCode', CStrings::strTrimDef( $strHomeCountryCode, -1, NULL, true ) );
	}

	public function getHomeCountryCode() {
		return $this->m_strHomeCountryCode;
	}

	public function sqlHomeCountryCode() {
		return ( true == isset( $this->m_strHomeCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHomeCountryCode ) : '\'' . addslashes( $this->m_strHomeCountryCode ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, ps_lead_id, person_role_id, company_user_id, person_sales_role_id, promoter_type_id, company_name, name_prefix, name_first, name_middle, name_last, name_suffix, title, email_address, address_line1, address_line2, city, state_code, postal_code, country_code, notes, permissioned_units, is_primary, total_logins, is_disabled, is_decision_maker, is_task_notification, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, personal_email_address, home_address_line_one, home_address_line_two, home_city, home_state_code, home_postal_code, home_country_code )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPsLeadId() . ', ' .
						$this->sqlPersonRoleId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlPersonSalesRoleId() . ', ' .
						$this->sqlPromoterTypeId() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlNamePrefix() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameMiddle() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlNameSuffix() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlAddressLine1() . ', ' .
						$this->sqlAddressLine2() . ', ' .
						$this->sqlCity() . ', ' .
						$this->sqlStateCode() . ', ' .
						$this->sqlPostalCode() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlPermissionedUnits() . ', ' .
						$this->sqlIsPrimary() . ', ' .
						$this->sqlTotalLogins() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlIsDecisionMaker() . ', ' .
						$this->sqlIsTaskNotification() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlPersonalEmailAddress() . ', ' .
						$this->sqlHomeAddressLineOne() . ', ' .
						$this->sqlHomeAddressLineTwo() . ', ' .
						$this->sqlHomeCity() . ', ' .
						$this->sqlHomeStateCode() . ', ' .
						$this->sqlHomePostalCode() . ', ' .
						$this->sqlHomeCountryCode() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' person_role_id = ' . $this->sqlPersonRoleId(). ',' ; } elseif( true == array_key_exists( 'PersonRoleId', $this->getChangedColumns() ) ) { $strSql .= ' person_role_id = ' . $this->sqlPersonRoleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' person_sales_role_id = ' . $this->sqlPersonSalesRoleId(). ',' ; } elseif( true == array_key_exists( 'PersonSalesRoleId', $this->getChangedColumns() ) ) { $strSql .= ' person_sales_role_id = ' . $this->sqlPersonSalesRoleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' promoter_type_id = ' . $this->sqlPromoterTypeId(). ',' ; } elseif( true == array_key_exists( 'PromoterTypeId', $this->getChangedColumns() ) ) { $strSql .= ' promoter_type_id = ' . $this->sqlPromoterTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix(). ',' ; } elseif( true == array_key_exists( 'NamePrefix', $this->getChangedColumns() ) ) { $strSql .= ' name_prefix = ' . $this->sqlNamePrefix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle(). ',' ; } elseif( true == array_key_exists( 'NameMiddle', $this->getChangedColumns() ) ) { $strSql .= ' name_middle = ' . $this->sqlNameMiddle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix(). ',' ; } elseif( true == array_key_exists( 'NameSuffix', $this->getChangedColumns() ) ) { $strSql .= ' name_suffix = ' . $this->sqlNameSuffix() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_line1 = ' . $this->sqlAddressLine1(). ',' ; } elseif( true == array_key_exists( 'AddressLine1', $this->getChangedColumns() ) ) { $strSql .= ' address_line1 = ' . $this->sqlAddressLine1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_line2 = ' . $this->sqlAddressLine2(). ',' ; } elseif( true == array_key_exists( 'AddressLine2', $this->getChangedColumns() ) ) { $strSql .= ' address_line2 = ' . $this->sqlAddressLine2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity(). ',' ; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode(). ',' ; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode(). ',' ; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' permissioned_units = ' . $this->sqlPermissionedUnits(). ',' ; } elseif( true == array_key_exists( 'PermissionedUnits', $this->getChangedColumns() ) ) { $strSql .= ' permissioned_units = ' . $this->sqlPermissionedUnits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_primary = ' . $this->sqlIsPrimary(). ',' ; } elseif( true == array_key_exists( 'IsPrimary', $this->getChangedColumns() ) ) { $strSql .= ' is_primary = ' . $this->sqlIsPrimary() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_logins = ' . $this->sqlTotalLogins(). ',' ; } elseif( true == array_key_exists( 'TotalLogins', $this->getChangedColumns() ) ) { $strSql .= ' total_logins = ' . $this->sqlTotalLogins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_decision_maker = ' . $this->sqlIsDecisionMaker(). ',' ; } elseif( true == array_key_exists( 'IsDecisionMaker', $this->getChangedColumns() ) ) { $strSql .= ' is_decision_maker = ' . $this->sqlIsDecisionMaker() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_task_notification = ' . $this->sqlIsTaskNotification(). ',' ; } elseif( true == array_key_exists( 'IsTaskNotification', $this->getChangedColumns() ) ) { $strSql .= ' is_task_notification = ' . $this->sqlIsTaskNotification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' personal_email_address = ' . $this->sqlPersonalEmailAddress(). ',' ; } elseif( true == array_key_exists( 'PersonalEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' personal_email_address = ' . $this->sqlPersonalEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' home_address_line_one = ' . $this->sqlHomeAddressLineOne(). ',' ; } elseif( true == array_key_exists( 'HomeAddressLineOne', $this->getChangedColumns() ) ) { $strSql .= ' home_address_line_one = ' . $this->sqlHomeAddressLineOne() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' home_address_line_two = ' . $this->sqlHomeAddressLineTwo(). ',' ; } elseif( true == array_key_exists( 'HomeAddressLineTwo', $this->getChangedColumns() ) ) { $strSql .= ' home_address_line_two = ' . $this->sqlHomeAddressLineTwo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' home_city = ' . $this->sqlHomeCity(). ',' ; } elseif( true == array_key_exists( 'HomeCity', $this->getChangedColumns() ) ) { $strSql .= ' home_city = ' . $this->sqlHomeCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' home_state_code = ' . $this->sqlHomeStateCode(). ',' ; } elseif( true == array_key_exists( 'HomeStateCode', $this->getChangedColumns() ) ) { $strSql .= ' home_state_code = ' . $this->sqlHomeStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' home_postal_code = ' . $this->sqlHomePostalCode(). ',' ; } elseif( true == array_key_exists( 'HomePostalCode', $this->getChangedColumns() ) ) { $strSql .= ' home_postal_code = ' . $this->sqlHomePostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' home_country_code = ' . $this->sqlHomeCountryCode(). ',' ; } elseif( true == array_key_exists( 'HomeCountryCode', $this->getChangedColumns() ) ) { $strSql .= ' home_country_code = ' . $this->sqlHomeCountryCode() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'ps_lead_id' => $this->getPsLeadId(),
			'person_role_id' => $this->getPersonRoleId(),
			'company_user_id' => $this->getCompanyUserId(),
			'person_sales_role_id' => $this->getPersonSalesRoleId(),
			'promoter_type_id' => $this->getPromoterTypeId(),
			'company_name' => $this->getCompanyName(),
			'name_prefix' => $this->getNamePrefix(),
			'name_first' => $this->getNameFirst(),
			'name_middle' => $this->getNameMiddle(),
			'name_last' => $this->getNameLast(),
			'name_suffix' => $this->getNameSuffix(),
			'title' => $this->getTitle(),
			'email_address' => $this->getEmailAddress(),
			'address_line1' => $this->getAddressLine1(),
			'address_line2' => $this->getAddressLine2(),
			'city' => $this->getCity(),
			'state_code' => $this->getStateCode(),
			'postal_code' => $this->getPostalCode(),
			'country_code' => $this->getCountryCode(),
			'notes' => $this->getNotes(),
			'permissioned_units' => $this->getPermissionedUnits(),
			'is_primary' => $this->getIsPrimary(),
			'total_logins' => $this->getTotalLogins(),
			'is_disabled' => $this->getIsDisabled(),
			'is_decision_maker' => $this->getIsDecisionMaker(),
			'is_task_notification' => $this->getIsTaskNotification(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'personal_email_address' => $this->getPersonalEmailAddress(),
			'home_address_line_one' => $this->getHomeAddressLineOne(),
			'home_address_line_two' => $this->getHomeAddressLineTwo(),
			'home_city' => $this->getHomeCity(),
			'home_state_code' => $this->getHomeStateCode(),
			'home_postal_code' => $this->getHomePostalCode(),
			'home_country_code' => $this->getHomeCountryCode()
		);
	}

}
?>