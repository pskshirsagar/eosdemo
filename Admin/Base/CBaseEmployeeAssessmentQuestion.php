<?php

class CBaseEmployeeAssessmentQuestion extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_assessment_questions';

	protected $m_intId;
	protected $m_intEmployeeAssessmentTypeId;
	protected $m_strQuestion;
	protected $m_strManagerQuestion;
	protected $m_strPeerQuestion;
	protected $m_intIsRating;
	protected $m_intIsMandatory;
	protected $m_intIsMandatoryForManager;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsEntrataRating;
	protected $m_boolIsManagerRatingMandatory;

	public function __construct() {
		parent::__construct();

		$this->m_intIsRating = '0';
		$this->m_intIsMandatory = '1';
		$this->m_intIsMandatoryForManager = '1';
		$this->m_intOrderNum = '0';
		$this->m_boolIsEntrataRating = false;
		$this->m_boolIsManagerRatingMandatory = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_assessment_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeAssessmentTypeId', trim( $arrValues['employee_assessment_type_id'] ) ); elseif( isset( $arrValues['employee_assessment_type_id'] ) ) $this->setEmployeeAssessmentTypeId( $arrValues['employee_assessment_type_id'] );
		if( isset( $arrValues['question'] ) && $boolDirectSet ) $this->set( 'm_strQuestion', trim( $arrValues['question'] ) ); elseif( isset( $arrValues['question'] ) ) $this->setQuestion( $arrValues['question'] );
		if( isset( $arrValues['manager_question'] ) && $boolDirectSet ) $this->set( 'm_strManagerQuestion', trim( $arrValues['manager_question'] ) ); elseif( isset( $arrValues['manager_question'] ) ) $this->setManagerQuestion( $arrValues['manager_question'] );
		if( isset( $arrValues['peer_question'] ) && $boolDirectSet ) $this->set( 'm_strPeerQuestion', trim( $arrValues['peer_question'] ) ); elseif( isset( $arrValues['peer_question'] ) ) $this->setPeerQuestion( $arrValues['peer_question'] );
		if( isset( $arrValues['is_rating'] ) && $boolDirectSet ) $this->set( 'm_intIsRating', trim( $arrValues['is_rating'] ) ); elseif( isset( $arrValues['is_rating'] ) ) $this->setIsRating( $arrValues['is_rating'] );
		if( isset( $arrValues['is_mandatory'] ) && $boolDirectSet ) $this->set( 'm_intIsMandatory', trim( $arrValues['is_mandatory'] ) ); elseif( isset( $arrValues['is_mandatory'] ) ) $this->setIsMandatory( $arrValues['is_mandatory'] );
		if( isset( $arrValues['is_mandatory_for_manager'] ) && $boolDirectSet ) $this->set( 'm_intIsMandatoryForManager', trim( $arrValues['is_mandatory_for_manager'] ) ); elseif( isset( $arrValues['is_mandatory_for_manager'] ) ) $this->setIsMandatoryForManager( $arrValues['is_mandatory_for_manager'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_entrata_rating'] ) && $boolDirectSet ) $this->set( 'm_boolIsEntrataRating', trim( stripcslashes( $arrValues['is_entrata_rating'] ) ) ); elseif( isset( $arrValues['is_entrata_rating'] ) ) $this->setIsEntrataRating( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_entrata_rating'] ) : $arrValues['is_entrata_rating'] );
		if( isset( $arrValues['is_manager_rating_mandatory'] ) && $boolDirectSet ) $this->set( 'm_boolIsManagerRatingMandatory', trim( stripcslashes( $arrValues['is_manager_rating_mandatory'] ) ) ); elseif( isset( $arrValues['is_manager_rating_mandatory'] ) ) $this->setIsManagerRatingMandatory( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_manager_rating_mandatory'] ) : $arrValues['is_manager_rating_mandatory'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeAssessmentTypeId( $intEmployeeAssessmentTypeId ) {
		$this->set( 'm_intEmployeeAssessmentTypeId', CStrings::strToIntDef( $intEmployeeAssessmentTypeId, NULL, false ) );
	}

	public function getEmployeeAssessmentTypeId() {
		return $this->m_intEmployeeAssessmentTypeId;
	}

	public function sqlEmployeeAssessmentTypeId() {
		return ( true == isset( $this->m_intEmployeeAssessmentTypeId ) ) ? ( string ) $this->m_intEmployeeAssessmentTypeId : 'NULL';
	}

	public function setQuestion( $strQuestion ) {
		$this->set( 'm_strQuestion', CStrings::strTrimDef( $strQuestion, 2000, NULL, true ) );
	}

	public function getQuestion() {
		return $this->m_strQuestion;
	}

	public function sqlQuestion() {
		return ( true == isset( $this->m_strQuestion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strQuestion ) : '\'' . addslashes( $this->m_strQuestion ) . '\'' ) : 'NULL';
	}

	public function setManagerQuestion( $strManagerQuestion ) {
		$this->set( 'm_strManagerQuestion', CStrings::strTrimDef( $strManagerQuestion, 2000, NULL, true ) );
	}

	public function getManagerQuestion() {
		return $this->m_strManagerQuestion;
	}

	public function sqlManagerQuestion() {
		return ( true == isset( $this->m_strManagerQuestion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strManagerQuestion ) : '\'' . addslashes( $this->m_strManagerQuestion ) . '\'' ) : 'NULL';
	}

	public function setPeerQuestion( $strPeerQuestion ) {
		$this->set( 'm_strPeerQuestion', CStrings::strTrimDef( $strPeerQuestion, 2000, NULL, true ) );
	}

	public function getPeerQuestion() {
		return $this->m_strPeerQuestion;
	}

	public function sqlPeerQuestion() {
		return ( true == isset( $this->m_strPeerQuestion ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPeerQuestion ) : '\'' . addslashes( $this->m_strPeerQuestion ) . '\'' ) : 'NULL';
	}

	public function setIsRating( $intIsRating ) {
		$this->set( 'm_intIsRating', CStrings::strToIntDef( $intIsRating, NULL, false ) );
	}

	public function getIsRating() {
		return $this->m_intIsRating;
	}

	public function sqlIsRating() {
		return ( true == isset( $this->m_intIsRating ) ) ? ( string ) $this->m_intIsRating : '0';
	}

	public function setIsMandatory( $intIsMandatory ) {
		$this->set( 'm_intIsMandatory', CStrings::strToIntDef( $intIsMandatory, NULL, false ) );
	}

	public function getIsMandatory() {
		return $this->m_intIsMandatory;
	}

	public function sqlIsMandatory() {
		return ( true == isset( $this->m_intIsMandatory ) ) ? ( string ) $this->m_intIsMandatory : '1';
	}

	public function setIsMandatoryForManager( $intIsMandatoryForManager ) {
		$this->set( 'm_intIsMandatoryForManager', CStrings::strToIntDef( $intIsMandatoryForManager, NULL, false ) );
	}

	public function getIsMandatoryForManager() {
		return $this->m_intIsMandatoryForManager;
	}

	public function sqlIsMandatoryForManager() {
		return ( true == isset( $this->m_intIsMandatoryForManager ) ) ? ( string ) $this->m_intIsMandatoryForManager : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsEntrataRating( $boolIsEntrataRating ) {
		$this->set( 'm_boolIsEntrataRating', CStrings::strToBool( $boolIsEntrataRating ) );
	}

	public function getIsEntrataRating() {
		return $this->m_boolIsEntrataRating;
	}

	public function sqlIsEntrataRating() {
		return ( true == isset( $this->m_boolIsEntrataRating ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsEntrataRating ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsManagerRatingMandatory( $boolIsManagerRatingMandatory ) {
		$this->set( 'm_boolIsManagerRatingMandatory', CStrings::strToBool( $boolIsManagerRatingMandatory ) );
	}

	public function getIsManagerRatingMandatory() {
		return $this->m_boolIsManagerRatingMandatory;
	}

	public function sqlIsManagerRatingMandatory() {
		return ( true == isset( $this->m_boolIsManagerRatingMandatory ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsManagerRatingMandatory ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_assessment_type_id, question, manager_question, peer_question, is_rating, is_mandatory, is_mandatory_for_manager, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, is_entrata_rating, is_manager_rating_mandatory )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeAssessmentTypeId() . ', ' .
						$this->sqlQuestion() . ', ' .
						$this->sqlManagerQuestion() . ', ' .
						$this->sqlPeerQuestion() . ', ' .
						$this->sqlIsRating() . ', ' .
						$this->sqlIsMandatory() . ', ' .
						$this->sqlIsMandatoryForManager() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsEntrataRating() . ', ' .
						$this->sqlIsManagerRatingMandatory() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_assessment_type_id = ' . $this->sqlEmployeeAssessmentTypeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeAssessmentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_assessment_type_id = ' . $this->sqlEmployeeAssessmentTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' question = ' . $this->sqlQuestion(). ',' ; } elseif( true == array_key_exists( 'Question', $this->getChangedColumns() ) ) { $strSql .= ' question = ' . $this->sqlQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manager_question = ' . $this->sqlManagerQuestion(). ',' ; } elseif( true == array_key_exists( 'ManagerQuestion', $this->getChangedColumns() ) ) { $strSql .= ' manager_question = ' . $this->sqlManagerQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' peer_question = ' . $this->sqlPeerQuestion(). ',' ; } elseif( true == array_key_exists( 'PeerQuestion', $this->getChangedColumns() ) ) { $strSql .= ' peer_question = ' . $this->sqlPeerQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_rating = ' . $this->sqlIsRating(). ',' ; } elseif( true == array_key_exists( 'IsRating', $this->getChangedColumns() ) ) { $strSql .= ' is_rating = ' . $this->sqlIsRating() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_mandatory = ' . $this->sqlIsMandatory(). ',' ; } elseif( true == array_key_exists( 'IsMandatory', $this->getChangedColumns() ) ) { $strSql .= ' is_mandatory = ' . $this->sqlIsMandatory() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_mandatory_for_manager = ' . $this->sqlIsMandatoryForManager(). ',' ; } elseif( true == array_key_exists( 'IsMandatoryForManager', $this->getChangedColumns() ) ) { $strSql .= ' is_mandatory_for_manager = ' . $this->sqlIsMandatoryForManager() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_entrata_rating = ' . $this->sqlIsEntrataRating(). ',' ; } elseif( true == array_key_exists( 'IsEntrataRating', $this->getChangedColumns() ) ) { $strSql .= ' is_entrata_rating = ' . $this->sqlIsEntrataRating() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manager_rating_mandatory = ' . $this->sqlIsManagerRatingMandatory(). ',' ; } elseif( true == array_key_exists( 'IsManagerRatingMandatory', $this->getChangedColumns() ) ) { $strSql .= ' is_manager_rating_mandatory = ' . $this->sqlIsManagerRatingMandatory() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_assessment_type_id' => $this->getEmployeeAssessmentTypeId(),
			'question' => $this->getQuestion(),
			'manager_question' => $this->getManagerQuestion(),
			'peer_question' => $this->getPeerQuestion(),
			'is_rating' => $this->getIsRating(),
			'is_mandatory' => $this->getIsMandatory(),
			'is_mandatory_for_manager' => $this->getIsMandatoryForManager(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_entrata_rating' => $this->getIsEntrataRating(),
			'is_manager_rating_mandatory' => $this->getIsManagerRatingMandatory()
		);
	}

}
?>