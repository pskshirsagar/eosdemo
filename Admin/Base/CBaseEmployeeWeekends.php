<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeWeekends
 * Do not add any new functions to this class.
 */

class CBaseEmployeeWeekends extends CEosPluralBase {

	/**
	 * @return CEmployeeWeekend[]
	 */
	public static function fetchEmployeeWeekends( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeWeekend', $objDatabase );
	}

	/**
	 * @return CEmployeeWeekend
	 */
	public static function fetchEmployeeWeekend( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeWeekend', $objDatabase );
	}

	public static function fetchEmployeeWeekendCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_weekends', $objDatabase );
	}

	public static function fetchEmployeeWeekendById( $intId, $objDatabase ) {
		return self::fetchEmployeeWeekend( sprintf( 'SELECT * FROM employee_weekends WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeWeekendsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeWeekends( sprintf( 'SELECT * FROM employee_weekends WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>