<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationScoreTypes
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationScoreTypes extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationScoreType[]
	 */
	public static function fetchEmployeeApplicationScoreTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationScoreType', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationScoreType
	 */
	public static function fetchEmployeeApplicationScoreType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationScoreType', $objDatabase );
	}

	public static function fetchEmployeeApplicationScoreTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_score_types', $objDatabase );
	}

	public static function fetchEmployeeApplicationScoreTypeById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationScoreType( sprintf( 'SELECT * FROM employee_application_score_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>