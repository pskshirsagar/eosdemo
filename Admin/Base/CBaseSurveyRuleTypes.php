<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyRuleTypes
 * Do not add any new functions to this class.
 */

class CBaseSurveyRuleTypes extends CEosPluralBase {

	/**
	 * @return CSurveyRuleType[]
	 */
	public static function fetchSurveyRuleTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyRuleType', $objDatabase );
	}

	/**
	 * @return CSurveyRuleType
	 */
	public static function fetchSurveyRuleType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyRuleType', $objDatabase );
	}

	public static function fetchSurveyRuleTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_rule_types', $objDatabase );
	}

	public static function fetchSurveyRuleTypeById( $intId, $objDatabase ) {
		return self::fetchSurveyRuleType( sprintf( 'SELECT * FROM survey_rule_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>