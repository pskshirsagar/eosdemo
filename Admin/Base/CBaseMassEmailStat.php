<?php

class CBaseMassEmailStat extends CEosSingularBase {

	const TABLE_NAME = 'public.mass_email_stats';

	protected $m_intId;
	protected $m_intMassEmailSubscriberId;
	protected $m_intMassEmailId;
	protected $m_boolIsSubscribed;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsSubscribed = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['mass_email_subscriber_id'] ) && $boolDirectSet ) $this->set( 'm_intMassEmailSubscriberId', trim( $arrValues['mass_email_subscriber_id'] ) ); elseif( isset( $arrValues['mass_email_subscriber_id'] ) ) $this->setMassEmailSubscriberId( $arrValues['mass_email_subscriber_id'] );
		if( isset( $arrValues['mass_email_id'] ) && $boolDirectSet ) $this->set( 'm_intMassEmailId', trim( $arrValues['mass_email_id'] ) ); elseif( isset( $arrValues['mass_email_id'] ) ) $this->setMassEmailId( $arrValues['mass_email_id'] );
		if( isset( $arrValues['is_subscribed'] ) && $boolDirectSet ) $this->set( 'm_boolIsSubscribed', trim( stripcslashes( $arrValues['is_subscribed'] ) ) ); elseif( isset( $arrValues['is_subscribed'] ) ) $this->setIsSubscribed( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_subscribed'] ) : $arrValues['is_subscribed'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setMassEmailSubscriberId( $intMassEmailSubscriberId ) {
		$this->set( 'm_intMassEmailSubscriberId', CStrings::strToIntDef( $intMassEmailSubscriberId, NULL, false ) );
	}

	public function getMassEmailSubscriberId() {
		return $this->m_intMassEmailSubscriberId;
	}

	public function sqlMassEmailSubscriberId() {
		return ( true == isset( $this->m_intMassEmailSubscriberId ) ) ? ( string ) $this->m_intMassEmailSubscriberId : 'NULL';
	}

	public function setMassEmailId( $intMassEmailId ) {
		$this->set( 'm_intMassEmailId', CStrings::strToIntDef( $intMassEmailId, NULL, false ) );
	}

	public function getMassEmailId() {
		return $this->m_intMassEmailId;
	}

	public function sqlMassEmailId() {
		return ( true == isset( $this->m_intMassEmailId ) ) ? ( string ) $this->m_intMassEmailId : 'NULL';
	}

	public function setIsSubscribed( $boolIsSubscribed ) {
		$this->set( 'm_boolIsSubscribed', CStrings::strToBool( $boolIsSubscribed ) );
	}

	public function getIsSubscribed() {
		return $this->m_boolIsSubscribed;
	}

	public function sqlIsSubscribed() {
		return ( true == isset( $this->m_boolIsSubscribed ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSubscribed ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, mass_email_subscriber_id, mass_email_id, is_subscribed, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlMassEmailSubscriberId() . ', ' .
 						$this->sqlMassEmailId() . ', ' .
 						$this->sqlIsSubscribed() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mass_email_subscriber_id = ' . $this->sqlMassEmailSubscriberId() . ','; } elseif( true == array_key_exists( 'MassEmailSubscriberId', $this->getChangedColumns() ) ) { $strSql .= ' mass_email_subscriber_id = ' . $this->sqlMassEmailSubscriberId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mass_email_id = ' . $this->sqlMassEmailId() . ','; } elseif( true == array_key_exists( 'MassEmailId', $this->getChangedColumns() ) ) { $strSql .= ' mass_email_id = ' . $this->sqlMassEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_subscribed = ' . $this->sqlIsSubscribed() . ','; } elseif( true == array_key_exists( 'IsSubscribed', $this->getChangedColumns() ) ) { $strSql .= ' is_subscribed = ' . $this->sqlIsSubscribed() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'mass_email_subscriber_id' => $this->getMassEmailSubscriberId(),
			'mass_email_id' => $this->getMassEmailId(),
			'is_subscribed' => $this->getIsSubscribed(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>