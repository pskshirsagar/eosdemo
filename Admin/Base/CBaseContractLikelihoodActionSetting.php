<?php

class CBaseContractLikelihoodActionSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.contract_likelihood_action_settings';

	protected $m_intId;
	protected $m_intActionTypeId;
	protected $m_intActionResultTypeId;
	protected $m_intAdjustmentStatus;
	protected $m_fltPercentageAdjustment;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['action_type_id'] ) && $boolDirectSet ) $this->set( 'm_intActionTypeId', trim( $arrValues['action_type_id'] ) ); elseif( isset( $arrValues['action_type_id'] ) ) $this->setActionTypeId( $arrValues['action_type_id'] );
		if( isset( $arrValues['action_result_type_id'] ) && $boolDirectSet ) $this->set( 'm_intActionResultTypeId', trim( $arrValues['action_result_type_id'] ) ); elseif( isset( $arrValues['action_result_type_id'] ) ) $this->setActionResultTypeId( $arrValues['action_result_type_id'] );
		if( isset( $arrValues['adjustment_status'] ) && $boolDirectSet ) $this->set( 'm_intAdjustmentStatus', trim( $arrValues['adjustment_status'] ) ); elseif( isset( $arrValues['adjustment_status'] ) ) $this->setAdjustmentStatus( $arrValues['adjustment_status'] );
		if( isset( $arrValues['percentage_adjustment'] ) && $boolDirectSet ) $this->set( 'm_fltPercentageAdjustment', trim( $arrValues['percentage_adjustment'] ) ); elseif( isset( $arrValues['percentage_adjustment'] ) ) $this->setPercentageAdjustment( $arrValues['percentage_adjustment'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setActionTypeId( $intActionTypeId ) {
		$this->set( 'm_intActionTypeId', CStrings::strToIntDef( $intActionTypeId, NULL, false ) );
	}

	public function getActionTypeId() {
		return $this->m_intActionTypeId;
	}

	public function sqlActionTypeId() {
		return ( true == isset( $this->m_intActionTypeId ) ) ? ( string ) $this->m_intActionTypeId : 'NULL';
	}

	public function setActionResultTypeId( $intActionResultTypeId ) {
		$this->set( 'm_intActionResultTypeId', CStrings::strToIntDef( $intActionResultTypeId, NULL, false ) );
	}

	public function getActionResultTypeId() {
		return $this->m_intActionResultTypeId;
	}

	public function sqlActionResultTypeId() {
		return ( true == isset( $this->m_intActionResultTypeId ) ) ? ( string ) $this->m_intActionResultTypeId : 'NULL';
	}

	public function setAdjustmentStatus( $intAdjustmentStatus ) {
		$this->set( 'm_intAdjustmentStatus', CStrings::strToIntDef( $intAdjustmentStatus, NULL, false ) );
	}

	public function getAdjustmentStatus() {
		return $this->m_intAdjustmentStatus;
	}

	public function sqlAdjustmentStatus() {
		return ( true == isset( $this->m_intAdjustmentStatus ) ) ? ( string ) $this->m_intAdjustmentStatus : 'NULL';
	}

	public function setPercentageAdjustment( $fltPercentageAdjustment ) {
		$this->set( 'm_fltPercentageAdjustment', CStrings::strToFloatDef( $fltPercentageAdjustment, NULL, false, 2 ) );
	}

	public function getPercentageAdjustment() {
		return $this->m_fltPercentageAdjustment;
	}

	public function sqlPercentageAdjustment() {
		return ( true == isset( $this->m_fltPercentageAdjustment ) ) ? ( string ) $this->m_fltPercentageAdjustment : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, action_type_id, action_result_type_id, adjustment_status, percentage_adjustment, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlActionTypeId() . ', ' .
 						$this->sqlActionResultTypeId() . ', ' .
 						$this->sqlAdjustmentStatus() . ', ' .
 						$this->sqlPercentageAdjustment() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_type_id = ' . $this->sqlActionTypeId() . ','; } elseif( true == array_key_exists( 'ActionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' action_type_id = ' . $this->sqlActionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_result_type_id = ' . $this->sqlActionResultTypeId() . ','; } elseif( true == array_key_exists( 'ActionResultTypeId', $this->getChangedColumns() ) ) { $strSql .= ' action_result_type_id = ' . $this->sqlActionResultTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' adjustment_status = ' . $this->sqlAdjustmentStatus() . ','; } elseif( true == array_key_exists( 'AdjustmentStatus', $this->getChangedColumns() ) ) { $strSql .= ' adjustment_status = ' . $this->sqlAdjustmentStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percentage_adjustment = ' . $this->sqlPercentageAdjustment() . ','; } elseif( true == array_key_exists( 'PercentageAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' percentage_adjustment = ' . $this->sqlPercentageAdjustment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'action_type_id' => $this->getActionTypeId(),
			'action_result_type_id' => $this->getActionResultTypeId(),
			'adjustment_status' => $this->getAdjustmentStatus(),
			'percentage_adjustment' => $this->getPercentageAdjustment(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>