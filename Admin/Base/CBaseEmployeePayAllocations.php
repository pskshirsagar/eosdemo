<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePayAllocations
 * Do not add any new functions to this class.
 */

class CBaseEmployeePayAllocations extends CEosPluralBase {

	/**
	 * @return CEmployeePayAllocation[]
	 */
	public static function fetchEmployeePayAllocations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeePayAllocation', $objDatabase );
	}

	/**
	 * @return CEmployeePayAllocation
	 */
	public static function fetchEmployeePayAllocation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeePayAllocation', $objDatabase );
	}

	public static function fetchEmployeePayAllocationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_pay_allocations', $objDatabase );
	}

	public static function fetchEmployeePayAllocationById( $intId, $objDatabase ) {
		return self::fetchEmployeePayAllocation( sprintf( 'SELECT * FROM employee_pay_allocations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeePayAllocationsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeePayAllocations( sprintf( 'SELECT * FROM employee_pay_allocations WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeePayAllocationsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		return self::fetchEmployeePayAllocations( sprintf( 'SELECT * FROM employee_pay_allocations WHERE manager_employee_id = %d', ( int ) $intManagerEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeePayAllocationsByProposedDesignationId( $intProposedDesignationId, $objDatabase ) {
		return self::fetchEmployeePayAllocations( sprintf( 'SELECT * FROM employee_pay_allocations WHERE proposed_designation_id = %d', ( int ) $intProposedDesignationId ), $objDatabase );
	}

	public static function fetchEmployeePayAllocationsByPendingIncreaseEncryptionAssociationId( $intPendingIncreaseEncryptionAssociationId, $objDatabase ) {
		return self::fetchEmployeePayAllocations( sprintf( 'SELECT * FROM employee_pay_allocations WHERE pending_increase_encryption_association_id = %d', ( int ) $intPendingIncreaseEncryptionAssociationId ), $objDatabase );
	}

	public static function fetchEmployeePayAllocationsByPendingHourlyEncryptionAssociationId( $intPendingHourlyEncryptionAssociationId, $objDatabase ) {
		return self::fetchEmployeePayAllocations( sprintf( 'SELECT * FROM employee_pay_allocations WHERE pending_hourly_encryption_association_id = %d', ( int ) $intPendingHourlyEncryptionAssociationId ), $objDatabase );
	}

	public static function fetchEmployeePayAllocationsByEmployeeCompensationLogId( $intEmployeeCompensationLogId, $objDatabase ) {
		return self::fetchEmployeePayAllocations( sprintf( 'SELECT * FROM employee_pay_allocations WHERE employee_compensation_log_id = %d', ( int ) $intEmployeeCompensationLogId ), $objDatabase );
	}

}
?>