<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CInternalTransfers
 * Do not add any new functions to this class.
 */

class CBaseInternalTransfers extends CEosPluralBase {

	/**
	 * @return CInternalTransfer[]
	 */
	public static function fetchInternalTransfers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CInternalTransfer::class, $objDatabase );
	}

	/**
	 * @return CInternalTransfer
	 */
	public static function fetchInternalTransfer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CInternalTransfer::class, $objDatabase );
	}

	public static function fetchInternalTransferCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'internal_transfers', $objDatabase );
	}

	public static function fetchInternalTransferById( $intId, $objDatabase ) {
		return self::fetchInternalTransfer( sprintf( 'SELECT * FROM internal_transfers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInternalTransfersByDebitProcessingBankAccountId( $intDebitProcessingBankAccountId, $objDatabase ) {
		return self::fetchInternalTransfers( sprintf( 'SELECT * FROM internal_transfers WHERE debit_processing_bank_account_id = %d', ( int ) $intDebitProcessingBankAccountId ), $objDatabase );
	}

	public static function fetchInternalTransfersByCreditProcessingBankAccountId( $intCreditProcessingBankAccountId, $objDatabase ) {
		return self::fetchInternalTransfers( sprintf( 'SELECT * FROM internal_transfers WHERE credit_processing_bank_account_id = %d', ( int ) $intCreditProcessingBankAccountId ), $objDatabase );
	}

	public static function fetchInternalTransfersByExportBatchId( $intExportBatchId, $objDatabase ) {
		return self::fetchInternalTransfers( sprintf( 'SELECT * FROM internal_transfers WHERE export_batch_id = %d', ( int ) $intExportBatchId ), $objDatabase );
	}

	public static function fetchInternalTransfersByMerchantGatewayId( $intMerchantGatewayId, $objDatabase ) {
		return self::fetchInternalTransfers( sprintf( 'SELECT * FROM internal_transfers WHERE merchant_gateway_id = %d', ( int ) $intMerchantGatewayId ), $objDatabase );
	}

	public static function fetchInternalTransfersByInternalTransferTypeId( $intInternalTransferTypeId, $objDatabase ) {
		return self::fetchInternalTransfers( sprintf( 'SELECT * FROM internal_transfers WHERE internal_transfer_type_id = %d', ( int ) $intInternalTransferTypeId ), $objDatabase );
	}

	public static function fetchInternalTransfersByPaymentTypeId( $intPaymentTypeId, $objDatabase ) {
		return self::fetchInternalTransfers( sprintf( 'SELECT * FROM internal_transfers WHERE payment_type_id = %d', ( int ) $intPaymentTypeId ), $objDatabase );
	}

	public static function fetchInternalTransfersByPaymentStatusTypeId( $intPaymentStatusTypeId, $objDatabase ) {
		return self::fetchInternalTransfers( sprintf( 'SELECT * FROM internal_transfers WHERE payment_status_type_id = %d', ( int ) $intPaymentStatusTypeId ), $objDatabase );
	}

	public static function fetchInternalTransfersByReturnTypeId( $intReturnTypeId, $objDatabase ) {
		return self::fetchInternalTransfers( sprintf( 'SELECT * FROM internal_transfers WHERE return_type_id = %d', ( int ) $intReturnTypeId ), $objDatabase );
	}

	public static function fetchInternalTransfersByDebitEftId( $intDebitEftId, $objDatabase ) {
		return self::fetchInternalTransfers( sprintf( 'SELECT * FROM internal_transfers WHERE debit_eft_id = %d', ( int ) $intDebitEftId ), $objDatabase );
	}

	public static function fetchInternalTransfersByCreditEftId( $intCreditEftId, $objDatabase ) {
		return self::fetchInternalTransfers( sprintf( 'SELECT * FROM internal_transfers WHERE credit_eft_id = %d', ( int ) $intCreditEftId ), $objDatabase );
	}

}
?>