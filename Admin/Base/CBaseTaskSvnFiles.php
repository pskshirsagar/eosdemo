<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskSvnFiles
 * Do not add any new functions to this class.
 */

class CBaseTaskSvnFiles extends CEosPluralBase {

	/**
	 * @return CTaskSvnFile[]
	 */
	public static function fetchTaskSvnFiles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskSvnFile', $objDatabase );
	}

	/**
	 * @return CTaskSvnFile
	 */
	public static function fetchTaskSvnFile( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskSvnFile', $objDatabase );
	}

	public static function fetchTaskSvnFileCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_svn_files', $objDatabase );
	}

	public static function fetchTaskSvnFileById( $intId, $objDatabase ) {
		return self::fetchTaskSvnFile( sprintf( 'SELECT * FROM task_svn_files WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskSvnFilesByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchTaskSvnFiles( sprintf( 'SELECT * FROM task_svn_files WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

	public static function fetchTaskSvnFilesBySvnFileId( $intSvnFileId, $objDatabase ) {
		return self::fetchTaskSvnFiles( sprintf( 'SELECT * FROM task_svn_files WHERE svn_file_id = %d', ( int ) $intSvnFileId ), $objDatabase );
	}

	public static function fetchTaskSvnFilesByUserId( $intUserId, $objDatabase ) {
		return self::fetchTaskSvnFiles( sprintf( 'SELECT * FROM task_svn_files WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

}
?>