<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTagsAssociations
 * Do not add any new functions to this class.
 */

class CBaseTagsAssociations extends CEosPluralBase {

	/**
	 * @return CTagsAssociation[]
	 */
	public static function fetchTagsAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTagsAssociation::class, $objDatabase );
	}

	/**
	 * @return CTagsAssociation
	 */
	public static function fetchTagsAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTagsAssociation::class, $objDatabase );
	}

	public static function fetchTagsAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tags_associations', $objDatabase );
	}

	public static function fetchTagsAssociationById( $intId, $objDatabase ) {
		return self::fetchTagsAssociation( sprintf( 'SELECT * FROM tags_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTagsAssociationsByTagId( $intTagId, $objDatabase ) {
		return self::fetchTagsAssociations( sprintf( 'SELECT * FROM tags_associations WHERE tag_id = %d', ( int ) $intTagId ), $objDatabase );
	}

	public static function fetchTagsAssociationsByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchTagsAssociations( sprintf( 'SELECT * FROM tags_associations WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

	public static function fetchTagsAssociationsByPsLeadId( $intPsLeadId, $objDatabase ) {
		return self::fetchTagsAssociations( sprintf( 'SELECT * FROM tags_associations WHERE ps_lead_id = %d', ( int ) $intPsLeadId ), $objDatabase );
	}

	public static function fetchTagsAssociationsByCompanyReportId( $intCompanyReportId, $objDatabase ) {
		return self::fetchTagsAssociations( sprintf( 'SELECT * FROM tags_associations WHERE company_report_id = %d', ( int ) $intCompanyReportId ), $objDatabase );
	}

}
?>