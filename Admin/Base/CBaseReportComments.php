<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportComments
 * Do not add any new functions to this class.
 */

class CBaseReportComments extends CEosPluralBase {

	/**
	 * @return CReportComment[]
	 */
	public static function fetchReportComments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportComment::class, $objDatabase );
	}

	/**
	 * @return CReportComment
	 */
	public static function fetchReportComment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportComment::class, $objDatabase );
	}

	public static function fetchReportCommentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_comments', $objDatabase );
	}

	public static function fetchReportCommentById( $intId, $objDatabase ) {
		return self::fetchReportComment( sprintf( 'SELECT * FROM report_comments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportCommentsByCompanyReportId( $intCompanyReportId, $objDatabase ) {
		return self::fetchReportComments( sprintf( 'SELECT * FROM report_comments WHERE company_report_id = %d', ( int ) $intCompanyReportId ), $objDatabase );
	}

}
?>