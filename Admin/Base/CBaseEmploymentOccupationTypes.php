<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmploymentOccupationTypes
 * Do not add any new functions to this class.
 */

class CBaseEmploymentOccupationTypes extends CEosPluralBase {

	/**
	 * @return CEmploymentOccupationType[]
	 */
	public static function fetchEmploymentOccupationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmploymentOccupationType', $objDatabase );
	}

	/**
	 * @return CEmploymentOccupationType
	 */
	public static function fetchEmploymentOccupationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmploymentOccupationType', $objDatabase );
	}

	public static function fetchEmploymentOccupationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employment_occupation_types', $objDatabase );
	}

	public static function fetchEmploymentOccupationTypeById( $intId, $objDatabase ) {
		return self::fetchEmploymentOccupationType( sprintf( 'SELECT * FROM employment_occupation_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>