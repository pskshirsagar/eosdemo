<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyPricingTypes
 * Do not add any new functions to this class.
 */

class CBaseCompanyPricingTypes extends CEosPluralBase {

	/**
	 * @return CCompanyPricingType[]
	 */
	public static function fetchCompanyPricingTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCompanyPricingType', $objDatabase );
	}

	/**
	 * @return CCompanyPricingType
	 */
	public static function fetchCompanyPricingType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyPricingType', $objDatabase );
	}

	public static function fetchCompanyPricingTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_pricing_types', $objDatabase );
	}

	public static function fetchCompanyPricingTypeById( $intId, $objDatabase ) {
		return self::fetchCompanyPricingType( sprintf( 'SELECT * FROM company_pricing_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCompanyPricingTypesByChargeCodeId( $intChargeCodeId, $objDatabase ) {
		return self::fetchCompanyPricingTypes( sprintf( 'SELECT * FROM company_pricing_types WHERE charge_code_id = %d', ( int ) $intChargeCodeId ), $objDatabase );
	}

}
?>