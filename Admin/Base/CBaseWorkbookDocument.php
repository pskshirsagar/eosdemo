<?php

class CBaseWorkbookDocument extends CEosSingularBase {

	const TABLE_NAME = 'public.workbook_documents';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intWorkbookId;
	protected $m_intReportSectionTypeId;
	protected $m_intMigrationWorkbookStatusTypeId;
	protected $m_arrintTagIds;
	protected $m_strTitle;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_strRejectedOn;
	protected $m_intRejectedBy;
	protected $m_strRejectNote;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['workbook_id'] ) && $boolDirectSet ) $this->set( 'm_intWorkbookId', trim( $arrValues['workbook_id'] ) ); elseif( isset( $arrValues['workbook_id'] ) ) $this->setWorkbookId( $arrValues['workbook_id'] );
		if( isset( $arrValues['report_section_type_id'] ) && $boolDirectSet ) $this->set( 'm_intReportSectionTypeId', trim( $arrValues['report_section_type_id'] ) ); elseif( isset( $arrValues['report_section_type_id'] ) ) $this->setReportSectionTypeId( $arrValues['report_section_type_id'] );
		if( isset( $arrValues['migration_workbook_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMigrationWorkbookStatusTypeId', trim( $arrValues['migration_workbook_status_type_id'] ) ); elseif( isset( $arrValues['migration_workbook_status_type_id'] ) ) $this->setMigrationWorkbookStatusTypeId( $arrValues['migration_workbook_status_type_id'] );
		if( isset( $arrValues['tag_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintTagIds', trim( $arrValues['tag_ids'] ) ); elseif( isset( $arrValues['tag_ids'] ) ) $this->setTagIds( $arrValues['tag_ids'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( $arrValues['file_name'] ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( $arrValues['file_path'] ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( $arrValues['file_path'] );
		if( isset( $arrValues['rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strRejectedOn', trim( $arrValues['rejected_on'] ) ); elseif( isset( $arrValues['rejected_on'] ) ) $this->setRejectedOn( $arrValues['rejected_on'] );
		if( isset( $arrValues['rejected_by'] ) && $boolDirectSet ) $this->set( 'm_intRejectedBy', trim( $arrValues['rejected_by'] ) ); elseif( isset( $arrValues['rejected_by'] ) ) $this->setRejectedBy( $arrValues['rejected_by'] );
		if( isset( $arrValues['reject_note'] ) && $boolDirectSet ) $this->set( 'm_strRejectNote', trim( $arrValues['reject_note'] ) ); elseif( isset( $arrValues['reject_note'] ) ) $this->setRejectNote( $arrValues['reject_note'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setWorkbookId( $intWorkbookId ) {
		$this->set( 'm_intWorkbookId', CStrings::strToIntDef( $intWorkbookId, NULL, false ) );
	}

	public function getWorkbookId() {
		return $this->m_intWorkbookId;
	}

	public function sqlWorkbookId() {
		return ( true == isset( $this->m_intWorkbookId ) ) ? ( string ) $this->m_intWorkbookId : 'NULL';
	}

	public function setReportSectionTypeId( $intReportSectionTypeId ) {
		$this->set( 'm_intReportSectionTypeId', CStrings::strToIntDef( $intReportSectionTypeId, NULL, false ) );
	}

	public function getReportSectionTypeId() {
		return $this->m_intReportSectionTypeId;
	}

	public function sqlReportSectionTypeId() {
		return ( true == isset( $this->m_intReportSectionTypeId ) ) ? ( string ) $this->m_intReportSectionTypeId : 'NULL';
	}

	public function setMigrationWorkbookStatusTypeId( $intMigrationWorkbookStatusTypeId ) {
		$this->set( 'm_intMigrationWorkbookStatusTypeId', CStrings::strToIntDef( $intMigrationWorkbookStatusTypeId, NULL, false ) );
	}

	public function getMigrationWorkbookStatusTypeId() {
		return $this->m_intMigrationWorkbookStatusTypeId;
	}

	public function sqlMigrationWorkbookStatusTypeId() {
		return ( true == isset( $this->m_intMigrationWorkbookStatusTypeId ) ) ? ( string ) $this->m_intMigrationWorkbookStatusTypeId : 'NULL';
	}

	public function setTagIds( $arrintTagIds ) {
		$this->set( 'm_arrintTagIds', CStrings::strToArrIntDef( $arrintTagIds, NULL ) );
	}

	public function getTagIds() {
		return $this->m_arrintTagIds;
	}

	public function sqlTagIds() {
		return ( true == isset( $this->m_arrintTagIds ) && true == valArr( $this->m_arrintTagIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintTagIds, NULL ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 255, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 255, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		if( CONFIG_CLOUD_ID == CCloud::IRELAND_ID ) {
			return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
		}
		return ( true == isset( $this->m_strFileName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileName ) : '\'' . addslashes( $this->m_strFileName ) . '\'' ) : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, -1, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		if( CONFIG_CLOUD_ID == CCloud::IRELAND_ID ) {
			return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
		}
		return ( true == isset( $this->m_strFilePath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFilePath ) : '\'' . addslashes( $this->m_strFilePath ) . '\'' ) : 'NULL';
	}

	public function setRejectedOn( $strRejectedOn ) {
		$this->set( 'm_strRejectedOn', CStrings::strTrimDef( $strRejectedOn, -1, NULL, true ) );
	}

	public function getRejectedOn() {
		return $this->m_strRejectedOn;
	}

	public function sqlRejectedOn() {
		return ( true == isset( $this->m_strRejectedOn ) ) ? '\'' . $this->m_strRejectedOn . '\'' : 'NULL';
	}

	public function setRejectedBy( $intRejectedBy ) {
		$this->set( 'm_intRejectedBy', CStrings::strToIntDef( $intRejectedBy, NULL, false ) );
	}

	public function getRejectedBy() {
		return $this->m_intRejectedBy;
	}

	public function sqlRejectedBy() {
		return ( true == isset( $this->m_intRejectedBy ) ) ? ( string ) $this->m_intRejectedBy : 'NULL';
	}

	public function setRejectNote( $strRejectNote ) {
		$this->set( 'm_strRejectNote', CStrings::strTrimDef( $strRejectNote, -1, NULL, true ) );
	}

	public function getRejectNote() {
		return $this->m_strRejectNote;
	}

	public function sqlRejectNote() {
		return ( true == isset( $this->m_strRejectNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRejectNote ) : '\'' . addslashes( $this->m_strRejectNote ) . '\'' ) : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, workbook_id, report_section_type_id, migration_workbook_status_type_id, tag_ids, title, file_name, file_path, rejected_on, rejected_by, reject_note, approved_by, approved_on, deleted_by, deleted_on, updated_on, updated_by, created_by, created_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlWorkbookId() . ', ' .
		          $this->sqlReportSectionTypeId() . ', ' .
		          $this->sqlMigrationWorkbookStatusTypeId() . ', ' .
		          $this->sqlTagIds() . ', ' .
		          $this->sqlTitle() . ', ' .
		          $this->sqlFileName() . ', ' .
		          $this->sqlFilePath() . ', ' .
		          $this->sqlRejectedOn() . ', ' .
		          $this->sqlRejectedBy() . ', ' .
		          $this->sqlRejectNote() . ', ' .
		          $this->sqlApprovedBy() . ', ' .
		          $this->sqlApprovedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' workbook_id = ' . $this->sqlWorkbookId(). ',' ; } elseif( true == array_key_exists( 'WorkbookId', $this->getChangedColumns() ) ) { $strSql .= ' workbook_id = ' . $this->sqlWorkbookId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_section_type_id = ' . $this->sqlReportSectionTypeId(). ',' ; } elseif( true == array_key_exists( 'ReportSectionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' report_section_type_id = ' . $this->sqlReportSectionTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' migration_workbook_status_type_id = ' . $this->sqlMigrationWorkbookStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'MigrationWorkbookStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' migration_workbook_status_type_id = ' . $this->sqlMigrationWorkbookStatusTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tag_ids = ' . $this->sqlTagIds(). ',' ; } elseif( true == array_key_exists( 'TagIds', $this->getChangedColumns() ) ) { $strSql .= ' tag_ids = ' . $this->sqlTagIds() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath(). ',' ; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn(). ',' ; } elseif( true == array_key_exists( 'RejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy(). ',' ; } elseif( true == array_key_exists( 'RejectedBy', $this->getChangedColumns() ) ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reject_note = ' . $this->sqlRejectNote(). ',' ; } elseif( true == array_key_exists( 'RejectNote', $this->getChangedColumns() ) ) { $strSql .= ' reject_note = ' . $this->sqlRejectNote() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'workbook_id' => $this->getWorkbookId(),
			'report_section_type_id' => $this->getReportSectionTypeId(),
			'migration_workbook_status_type_id' => $this->getMigrationWorkbookStatusTypeId(),
			'tag_ids' => $this->getTagIds(),
			'title' => $this->getTitle(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'rejected_on' => $this->getRejectedOn(),
			'rejected_by' => $this->getRejectedBy(),
			'reject_note' => $this->getRejectNote(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>