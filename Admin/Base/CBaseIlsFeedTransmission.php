<?php

class CBaseIlsFeedTransmission extends CEosSingularBase {

	const TABLE_NAME = 'public.ils_feed_transmissions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intInternetListingServiceId;
	protected $m_intScriptId;
	protected $m_intIlsFeedTypeId;
	protected $m_intClusterId;
	protected $m_strFileName;
	protected $m_strFilePath;
	protected $m_intFeedFileSize;
	protected $m_intFeedFileLineCount;
	protected $m_strFileGeneratorStarttime;
	protected $m_strFileGeneratorEndtime;
	protected $m_strFtpTransmissionStarttime;
	protected $m_strFtpTransmissionEndtime;
	protected $m_strFtpTransmissionResponse;
	protected $m_intIsMannual;
	protected $m_strSentOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsMannual = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['internet_listing_service_id'] ) && $boolDirectSet ) $this->set( 'm_intInternetListingServiceId', trim( $arrValues['internet_listing_service_id'] ) ); elseif( isset( $arrValues['internet_listing_service_id'] ) ) $this->setInternetListingServiceId( $arrValues['internet_listing_service_id'] );
		if( isset( $arrValues['script_id'] ) && $boolDirectSet ) $this->set( 'm_intScriptId', trim( $arrValues['script_id'] ) ); elseif( isset( $arrValues['script_id'] ) ) $this->setScriptId( $arrValues['script_id'] );
		if( isset( $arrValues['ils_feed_type_id'] ) && $boolDirectSet ) $this->set( 'm_intIlsFeedTypeId', trim( $arrValues['ils_feed_type_id'] ) ); elseif( isset( $arrValues['ils_feed_type_id'] ) ) $this->setIlsFeedTypeId( $arrValues['ils_feed_type_id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( stripcslashes( $arrValues['file_name'] ) ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_name'] ) : $arrValues['file_name'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['feed_file_size'] ) && $boolDirectSet ) $this->set( 'm_intFeedFileSize', trim( $arrValues['feed_file_size'] ) ); elseif( isset( $arrValues['feed_file_size'] ) ) $this->setFeedFileSize( $arrValues['feed_file_size'] );
		if( isset( $arrValues['feed_file_line_count'] ) && $boolDirectSet ) $this->set( 'm_intFeedFileLineCount', trim( $arrValues['feed_file_line_count'] ) ); elseif( isset( $arrValues['feed_file_line_count'] ) ) $this->setFeedFileLineCount( $arrValues['feed_file_line_count'] );
		if( isset( $arrValues['file_generator_starttime'] ) && $boolDirectSet ) $this->set( 'm_strFileGeneratorStarttime', trim( $arrValues['file_generator_starttime'] ) ); elseif( isset( $arrValues['file_generator_starttime'] ) ) $this->setFileGeneratorStarttime( $arrValues['file_generator_starttime'] );
		if( isset( $arrValues['file_generator_endtime'] ) && $boolDirectSet ) $this->set( 'm_strFileGeneratorEndtime', trim( $arrValues['file_generator_endtime'] ) ); elseif( isset( $arrValues['file_generator_endtime'] ) ) $this->setFileGeneratorEndtime( $arrValues['file_generator_endtime'] );
		if( isset( $arrValues['ftp_transmission_starttime'] ) && $boolDirectSet ) $this->set( 'm_strFtpTransmissionStarttime', trim( $arrValues['ftp_transmission_starttime'] ) ); elseif( isset( $arrValues['ftp_transmission_starttime'] ) ) $this->setFtpTransmissionStarttime( $arrValues['ftp_transmission_starttime'] );
		if( isset( $arrValues['ftp_transmission_endtime'] ) && $boolDirectSet ) $this->set( 'm_strFtpTransmissionEndtime', trim( $arrValues['ftp_transmission_endtime'] ) ); elseif( isset( $arrValues['ftp_transmission_endtime'] ) ) $this->setFtpTransmissionEndtime( $arrValues['ftp_transmission_endtime'] );
		if( isset( $arrValues['ftp_transmission_response'] ) && $boolDirectSet ) $this->set( 'm_strFtpTransmissionResponse', trim( stripcslashes( $arrValues['ftp_transmission_response'] ) ) ); elseif( isset( $arrValues['ftp_transmission_response'] ) ) $this->setFtpTransmissionResponse( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ftp_transmission_response'] ) : $arrValues['ftp_transmission_response'] );
		if( isset( $arrValues['is_mannual'] ) && $boolDirectSet ) $this->set( 'm_intIsMannual', trim( $arrValues['is_mannual'] ) ); elseif( isset( $arrValues['is_mannual'] ) ) $this->setIsMannual( $arrValues['is_mannual'] );
		if( isset( $arrValues['sent_on'] ) && $boolDirectSet ) $this->set( 'm_strSentOn', trim( $arrValues['sent_on'] ) ); elseif( isset( $arrValues['sent_on'] ) ) $this->setSentOn( $arrValues['sent_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setInternetListingServiceId( $intInternetListingServiceId ) {
		$this->set( 'm_intInternetListingServiceId', CStrings::strToIntDef( $intInternetListingServiceId, NULL, false ) );
	}

	public function getInternetListingServiceId() {
		return $this->m_intInternetListingServiceId;
	}

	public function sqlInternetListingServiceId() {
		return ( true == isset( $this->m_intInternetListingServiceId ) ) ? ( string ) $this->m_intInternetListingServiceId : 'NULL';
	}

	public function setScriptId( $intScriptId ) {
		$this->set( 'm_intScriptId', CStrings::strToIntDef( $intScriptId, NULL, false ) );
	}

	public function getScriptId() {
		return $this->m_intScriptId;
	}

	public function sqlScriptId() {
		return ( true == isset( $this->m_intScriptId ) ) ? ( string ) $this->m_intScriptId : 'NULL';
	}

	public function setIlsFeedTypeId( $intIlsFeedTypeId ) {
		$this->set( 'm_intIlsFeedTypeId', CStrings::strToIntDef( $intIlsFeedTypeId, NULL, false ) );
	}

	public function getIlsFeedTypeId() {
		return $this->m_intIlsFeedTypeId;
	}

	public function sqlIlsFeedTypeId() {
		return ( true == isset( $this->m_intIlsFeedTypeId ) ) ? ( string ) $this->m_intIlsFeedTypeId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? '\'' . addslashes( $this->m_strFileName ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setFeedFileSize( $intFeedFileSize ) {
		$this->set( 'm_intFeedFileSize', CStrings::strToIntDef( $intFeedFileSize, NULL, false ) );
	}

	public function getFeedFileSize() {
		return $this->m_intFeedFileSize;
	}

	public function sqlFeedFileSize() {
		return ( true == isset( $this->m_intFeedFileSize ) ) ? ( string ) $this->m_intFeedFileSize : 'NULL';
	}

	public function setFeedFileLineCount( $intFeedFileLineCount ) {
		$this->set( 'm_intFeedFileLineCount', CStrings::strToIntDef( $intFeedFileLineCount, NULL, false ) );
	}

	public function getFeedFileLineCount() {
		return $this->m_intFeedFileLineCount;
	}

	public function sqlFeedFileLineCount() {
		return ( true == isset( $this->m_intFeedFileLineCount ) ) ? ( string ) $this->m_intFeedFileLineCount : 'NULL';
	}

	public function setFileGeneratorStarttime( $strFileGeneratorStarttime ) {
		$this->set( 'm_strFileGeneratorStarttime', CStrings::strTrimDef( $strFileGeneratorStarttime, -1, NULL, true ) );
	}

	public function getFileGeneratorStarttime() {
		return $this->m_strFileGeneratorStarttime;
	}

	public function sqlFileGeneratorStarttime() {
		return ( true == isset( $this->m_strFileGeneratorStarttime ) ) ? '\'' . $this->m_strFileGeneratorStarttime . '\'' : 'NULL';
	}

	public function setFileGeneratorEndtime( $strFileGeneratorEndtime ) {
		$this->set( 'm_strFileGeneratorEndtime', CStrings::strTrimDef( $strFileGeneratorEndtime, -1, NULL, true ) );
	}

	public function getFileGeneratorEndtime() {
		return $this->m_strFileGeneratorEndtime;
	}

	public function sqlFileGeneratorEndtime() {
		return ( true == isset( $this->m_strFileGeneratorEndtime ) ) ? '\'' . $this->m_strFileGeneratorEndtime . '\'' : 'NULL';
	}

	public function setFtpTransmissionStarttime( $strFtpTransmissionStarttime ) {
		$this->set( 'm_strFtpTransmissionStarttime', CStrings::strTrimDef( $strFtpTransmissionStarttime, -1, NULL, true ) );
	}

	public function getFtpTransmissionStarttime() {
		return $this->m_strFtpTransmissionStarttime;
	}

	public function sqlFtpTransmissionStarttime() {
		return ( true == isset( $this->m_strFtpTransmissionStarttime ) ) ? '\'' . $this->m_strFtpTransmissionStarttime . '\'' : 'NULL';
	}

	public function setFtpTransmissionEndtime( $strFtpTransmissionEndtime ) {
		$this->set( 'm_strFtpTransmissionEndtime', CStrings::strTrimDef( $strFtpTransmissionEndtime, -1, NULL, true ) );
	}

	public function getFtpTransmissionEndtime() {
		return $this->m_strFtpTransmissionEndtime;
	}

	public function sqlFtpTransmissionEndtime() {
		return ( true == isset( $this->m_strFtpTransmissionEndtime ) ) ? '\'' . $this->m_strFtpTransmissionEndtime . '\'' : 'NULL';
	}

	public function setFtpTransmissionResponse( $strFtpTransmissionResponse ) {
		$this->set( 'm_strFtpTransmissionResponse', CStrings::strTrimDef( $strFtpTransmissionResponse, 4096, NULL, true ) );
	}

	public function getFtpTransmissionResponse() {
		return $this->m_strFtpTransmissionResponse;
	}

	public function sqlFtpTransmissionResponse() {
		return ( true == isset( $this->m_strFtpTransmissionResponse ) ) ? '\'' . addslashes( $this->m_strFtpTransmissionResponse ) . '\'' : 'NULL';
	}

	public function setIsMannual( $intIsMannual ) {
		$this->set( 'm_intIsMannual', CStrings::strToIntDef( $intIsMannual, NULL, false ) );
	}

	public function getIsMannual() {
		return $this->m_intIsMannual;
	}

	public function sqlIsMannual() {
		return ( true == isset( $this->m_intIsMannual ) ) ? ( string ) $this->m_intIsMannual : '0';
	}

	public function setSentOn( $strSentOn ) {
		$this->set( 'm_strSentOn', CStrings::strTrimDef( $strSentOn, -1, NULL, true ) );
	}

	public function getSentOn() {
		return $this->m_strSentOn;
	}

	public function sqlSentOn() {
		return ( true == isset( $this->m_strSentOn ) ) ? '\'' . $this->m_strSentOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, internet_listing_service_id, script_id, ils_feed_type_id, cluster_id, file_name, file_path, feed_file_size, feed_file_line_count, file_generator_starttime, file_generator_endtime, ftp_transmission_starttime, ftp_transmission_endtime, ftp_transmission_response, is_mannual, sent_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlInternetListingServiceId() . ', ' .
 						$this->sqlScriptId() . ', ' .
 						$this->sqlIlsFeedTypeId() . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlFileName() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlFeedFileSize() . ', ' .
 						$this->sqlFeedFileLineCount() . ', ' .
 						$this->sqlFileGeneratorStarttime() . ', ' .
 						$this->sqlFileGeneratorEndtime() . ', ' .
 						$this->sqlFtpTransmissionStarttime() . ', ' .
 						$this->sqlFtpTransmissionEndtime() . ', ' .
 						$this->sqlFtpTransmissionResponse() . ', ' .
 						$this->sqlIsMannual() . ', ' .
 						$this->sqlSentOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId() . ','; } elseif( true == array_key_exists( 'InternetListingServiceId', $this->getChangedColumns() ) ) { $strSql .= ' internet_listing_service_id = ' . $this->sqlInternetListingServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' script_id = ' . $this->sqlScriptId() . ','; } elseif( true == array_key_exists( 'ScriptId', $this->getChangedColumns() ) ) { $strSql .= ' script_id = ' . $this->sqlScriptId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ils_feed_type_id = ' . $this->sqlIlsFeedTypeId() . ','; } elseif( true == array_key_exists( 'IlsFeedTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ils_feed_type_id = ' . $this->sqlIlsFeedTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' feed_file_size = ' . $this->sqlFeedFileSize() . ','; } elseif( true == array_key_exists( 'FeedFileSize', $this->getChangedColumns() ) ) { $strSql .= ' feed_file_size = ' . $this->sqlFeedFileSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' feed_file_line_count = ' . $this->sqlFeedFileLineCount() . ','; } elseif( true == array_key_exists( 'FeedFileLineCount', $this->getChangedColumns() ) ) { $strSql .= ' feed_file_line_count = ' . $this->sqlFeedFileLineCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_generator_starttime = ' . $this->sqlFileGeneratorStarttime() . ','; } elseif( true == array_key_exists( 'FileGeneratorStarttime', $this->getChangedColumns() ) ) { $strSql .= ' file_generator_starttime = ' . $this->sqlFileGeneratorStarttime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_generator_endtime = ' . $this->sqlFileGeneratorEndtime() . ','; } elseif( true == array_key_exists( 'FileGeneratorEndtime', $this->getChangedColumns() ) ) { $strSql .= ' file_generator_endtime = ' . $this->sqlFileGeneratorEndtime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ftp_transmission_starttime = ' . $this->sqlFtpTransmissionStarttime() . ','; } elseif( true == array_key_exists( 'FtpTransmissionStarttime', $this->getChangedColumns() ) ) { $strSql .= ' ftp_transmission_starttime = ' . $this->sqlFtpTransmissionStarttime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ftp_transmission_endtime = ' . $this->sqlFtpTransmissionEndtime() . ','; } elseif( true == array_key_exists( 'FtpTransmissionEndtime', $this->getChangedColumns() ) ) { $strSql .= ' ftp_transmission_endtime = ' . $this->sqlFtpTransmissionEndtime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ftp_transmission_response = ' . $this->sqlFtpTransmissionResponse() . ','; } elseif( true == array_key_exists( 'FtpTransmissionResponse', $this->getChangedColumns() ) ) { $strSql .= ' ftp_transmission_response = ' . $this->sqlFtpTransmissionResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_mannual = ' . $this->sqlIsMannual() . ','; } elseif( true == array_key_exists( 'IsMannual', $this->getChangedColumns() ) ) { $strSql .= ' is_mannual = ' . $this->sqlIsMannual() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; } elseif( true == array_key_exists( 'SentOn', $this->getChangedColumns() ) ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'internet_listing_service_id' => $this->getInternetListingServiceId(),
			'script_id' => $this->getScriptId(),
			'ils_feed_type_id' => $this->getIlsFeedTypeId(),
			'cluster_id' => $this->getClusterId(),
			'file_name' => $this->getFileName(),
			'file_path' => $this->getFilePath(),
			'feed_file_size' => $this->getFeedFileSize(),
			'feed_file_line_count' => $this->getFeedFileLineCount(),
			'file_generator_starttime' => $this->getFileGeneratorStarttime(),
			'file_generator_endtime' => $this->getFileGeneratorEndtime(),
			'ftp_transmission_starttime' => $this->getFtpTransmissionStarttime(),
			'ftp_transmission_endtime' => $this->getFtpTransmissionEndtime(),
			'ftp_transmission_response' => $this->getFtpTransmissionResponse(),
			'is_mannual' => $this->getIsMannual(),
			'sent_on' => $this->getSentOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>