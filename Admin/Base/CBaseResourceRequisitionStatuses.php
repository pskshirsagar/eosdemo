<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CResourceRequisitionStatuses
 * Do not add any new functions to this class.
 */

class CBaseResourceRequisitionStatuses extends CEosPluralBase {

	/**
	 * @return CResourceRequisitionStatus[]
	 */
	public static function fetchResourceRequisitionStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CResourceRequisitionStatus', $objDatabase );
	}

	/**
	 * @return CResourceRequisitionStatus
	 */
	public static function fetchResourceRequisitionStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CResourceRequisitionStatus', $objDatabase );
	}

	public static function fetchResourceRequisitionStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'resource_requisition_statuses', $objDatabase );
	}

	public static function fetchResourceRequisitionStatusById( $intId, $objDatabase ) {
		return self::fetchResourceRequisitionStatus( sprintf( 'SELECT * FROM resource_requisition_statuses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>