<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAssessmentQuestions
 * Do not add any new functions to this class.
 */

class CBaseEmployeeAssessmentQuestions extends CEosPluralBase {

	/**
	 * @return CEmployeeAssessmentQuestion[]
	 */
	public static function fetchEmployeeAssessmentQuestions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeAssessmentQuestion', $objDatabase );
	}

	/**
	 * @return CEmployeeAssessmentQuestion
	 */
	public static function fetchEmployeeAssessmentQuestion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeAssessmentQuestion', $objDatabase );
	}

	public static function fetchEmployeeAssessmentQuestionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_assessment_questions', $objDatabase );
	}

	public static function fetchEmployeeAssessmentQuestionById( $intId, $objDatabase ) {
		return self::fetchEmployeeAssessmentQuestion( sprintf( 'SELECT * FROM employee_assessment_questions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentQuestionsByEmployeeAssessmentTypeId( $intEmployeeAssessmentTypeId, $objDatabase ) {
		return self::fetchEmployeeAssessmentQuestions( sprintf( 'SELECT * FROM employee_assessment_questions WHERE employee_assessment_type_id = %d', ( int ) $intEmployeeAssessmentTypeId ), $objDatabase );
	}

}
?>