<?php

class CBaseOfficeShift extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.office_shifts';

	protected $m_intId;
	protected $m_strName;
	protected $m_strOfficeStartTime;
	protected $m_intRequiredHours;
	protected $m_strCountryCode;
	protected $m_intDefaultShift;
	protected $m_boolIsSupport;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_boolIsShowInPayroll;
	protected $m_strEncryptedShiftAmount;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intRequiredHours = '8';
		$this->m_boolIsSupport = false;
		$this->m_intOrderNum = '0';
		$this->m_boolIsShowInPayroll = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['office_start_time'] ) && $boolDirectSet ) $this->set( 'm_strOfficeStartTime', trim( $arrValues['office_start_time'] ) ); elseif( isset( $arrValues['office_start_time'] ) ) $this->setOfficeStartTime( $arrValues['office_start_time'] );
		if( isset( $arrValues['required_hours'] ) && $boolDirectSet ) $this->set( 'm_intRequiredHours', trim( $arrValues['required_hours'] ) ); elseif( isset( $arrValues['required_hours'] ) ) $this->setRequiredHours( $arrValues['required_hours'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['default_shift'] ) && $boolDirectSet ) $this->set( 'm_intDefaultShift', trim( $arrValues['default_shift'] ) ); elseif( isset( $arrValues['default_shift'] ) ) $this->setDefaultShift( $arrValues['default_shift'] );
		if( isset( $arrValues['is_support'] ) && $boolDirectSet ) $this->set( 'm_boolIsSupport', trim( stripcslashes( $arrValues['is_support'] ) ) ); elseif( isset( $arrValues['is_support'] ) ) $this->setIsSupport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_support'] ) : $arrValues['is_support'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['is_show_in_payroll'] ) && $boolDirectSet ) $this->set( 'm_boolIsShowInPayroll', trim( stripcslashes( $arrValues['is_show_in_payroll'] ) ) ); elseif( isset( $arrValues['is_show_in_payroll'] ) ) $this->setIsShowInPayroll( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_show_in_payroll'] ) : $arrValues['is_show_in_payroll'] );
		if( isset( $arrValues['encrypted_shift_amount'] ) && $boolDirectSet ) $this->set( 'm_strEncryptedShiftAmount', trim( stripcslashes( $arrValues['encrypted_shift_amount'] ) ) ); elseif( isset( $arrValues['encrypted_shift_amount'] ) ) $this->setEncryptedShiftAmount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['encrypted_shift_amount'] ) : $arrValues['encrypted_shift_amount'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setOfficeStartTime( $strOfficeStartTime ) {
		$this->set( 'm_strOfficeStartTime', CStrings::strTrimDef( $strOfficeStartTime, -1, NULL, true ) );
	}

	public function getOfficeStartTime() {
		return $this->m_strOfficeStartTime;
	}

	public function sqlOfficeStartTime() {
		return ( true == isset( $this->m_strOfficeStartTime ) ) ? '\'' . $this->m_strOfficeStartTime . '\'' : 'NOW()';
	}

	public function setRequiredHours( $intRequiredHours ) {
		$this->set( 'm_intRequiredHours', CStrings::strToIntDef( $intRequiredHours, NULL, false ) );
	}

	public function getRequiredHours() {
		return $this->m_intRequiredHours;
	}

	public function sqlRequiredHours() {
		return ( true == isset( $this->m_intRequiredHours ) ) ? ( string ) $this->m_intRequiredHours : '8';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setDefaultShift( $intDefaultShift ) {
		$this->set( 'm_intDefaultShift', CStrings::strToIntDef( $intDefaultShift, NULL, false ) );
	}

	public function getDefaultShift() {
		return $this->m_intDefaultShift;
	}

	public function sqlDefaultShift() {
		return ( true == isset( $this->m_intDefaultShift ) ) ? ( string ) $this->m_intDefaultShift : 'NULL';
	}

	public function setIsSupport( $boolIsSupport ) {
		$this->set( 'm_boolIsSupport', CStrings::strToBool( $boolIsSupport ) );
	}

	public function getIsSupport() {
		return $this->m_boolIsSupport;
	}

	public function sqlIsSupport() {
		return ( true == isset( $this->m_boolIsSupport ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSupport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setIsShowInPayroll( $boolIsShowInPayroll ) {
		$this->set( 'm_boolIsShowInPayroll', CStrings::strToBool( $boolIsShowInPayroll ) );
	}

	public function getIsShowInPayroll() {
		return $this->m_boolIsShowInPayroll;
	}

	public function sqlIsShowInPayroll() {
		return ( true == isset( $this->m_boolIsShowInPayroll ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsShowInPayroll ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setEncryptedShiftAmount( $strEncryptedShiftAmount ) {
		$this->set( 'm_strEncryptedShiftAmount', CStrings::strTrimDef( $strEncryptedShiftAmount, -1, NULL, true ) );
	}

	public function getEncryptedShiftAmount() {
		return $this->m_strEncryptedShiftAmount;
	}

	public function sqlEncryptedShiftAmount() {
		return ( true == isset( $this->m_strEncryptedShiftAmount ) ) ? '\'' . addslashes( $this->m_strEncryptedShiftAmount ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, office_start_time, required_hours, country_code, default_shift, is_support, order_num, updated_by, updated_on, created_by, created_on, deleted_by, deleted_on, is_show_in_payroll, encrypted_shift_amount, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlOfficeStartTime() . ', ' .
						$this->sqlRequiredHours() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlDefaultShift() . ', ' .
						$this->sqlIsSupport() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlIsShowInPayroll() . ', ' .
						$this->sqlEncryptedShiftAmount() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_start_time = ' . $this->sqlOfficeStartTime(). ',' ; } elseif( true == array_key_exists( 'OfficeStartTime', $this->getChangedColumns() ) ) { $strSql .= ' office_start_time = ' . $this->sqlOfficeStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' required_hours = ' . $this->sqlRequiredHours(). ',' ; } elseif( true == array_key_exists( 'RequiredHours', $this->getChangedColumns() ) ) { $strSql .= ' required_hours = ' . $this->sqlRequiredHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_shift = ' . $this->sqlDefaultShift(). ',' ; } elseif( true == array_key_exists( 'DefaultShift', $this->getChangedColumns() ) ) { $strSql .= ' default_shift = ' . $this->sqlDefaultShift() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_support = ' . $this->sqlIsSupport(). ',' ; } elseif( true == array_key_exists( 'IsSupport', $this->getChangedColumns() ) ) { $strSql .= ' is_support = ' . $this->sqlIsSupport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_show_in_payroll = ' . $this->sqlIsShowInPayroll(). ',' ; } elseif( true == array_key_exists( 'IsShowInPayroll', $this->getChangedColumns() ) ) { $strSql .= ' is_show_in_payroll = ' . $this->sqlIsShowInPayroll() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' encrypted_shift_amount = ' . $this->sqlEncryptedShiftAmount(). ',' ; } elseif( true == array_key_exists( 'EncryptedShiftAmount', $this->getChangedColumns() ) ) { $strSql .= ' encrypted_shift_amount = ' . $this->sqlEncryptedShiftAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'office_start_time' => $this->getOfficeStartTime(),
			'required_hours' => $this->getRequiredHours(),
			'country_code' => $this->getCountryCode(),
			'default_shift' => $this->getDefaultShift(),
			'is_support' => $this->getIsSupport(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'is_show_in_payroll' => $this->getIsShowInPayroll(),
			'encrypted_shift_amount' => $this->getEncryptedShiftAmount(),
			'details' => $this->getDetails()
		);
	}

}
?>