<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportDetails
 * Do not add any new functions to this class.
 */

class CBaseReportDetails extends CEosPluralBase {

	/**
	 * @return CReportDetail[]
	 */
	public static function fetchReportDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportDetail::class, $objDatabase );
	}

	/**
	 * @return CReportDetail
	 */
	public static function fetchReportDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportDetail::class, $objDatabase );
	}

	public static function fetchReportDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_details', $objDatabase );
	}

	public static function fetchReportDetailById( $intId, $objDatabase ) {
		return self::fetchReportDetail( sprintf( 'SELECT * FROM report_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportDetailsByCompanyReportId( $intCompanyReportId, $objDatabase ) {
		return self::fetchReportDetails( sprintf( 'SELECT * FROM report_details WHERE company_report_id = %d', ( int ) $intCompanyReportId ), $objDatabase );
	}

	public static function fetchReportDetailsByReportDataTypeId( $intReportDataTypeId, $objDatabase ) {
		return self::fetchReportDetails( sprintf( 'SELECT * FROM report_details WHERE report_data_type_id = %d', ( int ) $intReportDataTypeId ), $objDatabase );
	}

	public static function fetchReportDetailsByReportSqlDetailId( $intReportSqlDetailId, $objDatabase ) {
		return self::fetchReportDetails( sprintf( 'SELECT * FROM report_details WHERE report_sql_detail_id = %d', ( int ) $intReportSqlDetailId ), $objDatabase );
	}

	public static function fetchReportDetailsByReportSpreadsheetId( $intReportSpreadsheetId, $objDatabase ) {
		return self::fetchReportDetails( sprintf( 'SELECT * FROM report_details WHERE report_spreadsheet_id = %d', ( int ) $intReportSpreadsheetId ), $objDatabase );
	}

}
?>