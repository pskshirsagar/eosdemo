<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReimbursementExpenseEmployees
 * Do not add any new functions to this class.
 */

class CBaseReimbursementExpenseEmployees extends CEosPluralBase {

	/**
	 * @return CReimbursementExpenseEmployee[]
	 */
	public static function fetchReimbursementExpenseEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CReimbursementExpenseEmployee', $objDatabase );
	}

	/**
	 * @return CReimbursementExpenseEmployee
	 */
	public static function fetchReimbursementExpenseEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReimbursementExpenseEmployee', $objDatabase );
	}

	public static function fetchReimbursementExpenseEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reimbursement_expense_employees', $objDatabase );
	}

	public static function fetchReimbursementExpenseEmployeeById( $intId, $objDatabase ) {
		return self::fetchReimbursementExpenseEmployee( sprintf( 'SELECT * FROM reimbursement_expense_employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReimbursementExpenseEmployeesByReimbursementExpenseId( $intReimbursementExpenseId, $objDatabase ) {
		return self::fetchReimbursementExpenseEmployees( sprintf( 'SELECT * FROM reimbursement_expense_employees WHERE reimbursement_expense_id = %d', ( int ) $intReimbursementExpenseId ), $objDatabase );
	}

	public static function fetchReimbursementExpenseEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchReimbursementExpenseEmployees( sprintf( 'SELECT * FROM reimbursement_expense_employees WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>