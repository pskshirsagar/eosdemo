<?php

class CBaseCompanyPaymentTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.company_payment_transactions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intCompanyMerchantAccountId;
	protected $m_intCompanyPaymentId;
	protected $m_intPaymentTransactionTypeId;
	protected $m_intTransactionNumber;
	protected $m_strTransactionDatetime;
	protected $m_fltTransactionAmount;
	protected $m_strTransactionMemo;
	protected $m_strGatewayResponseCode;
	protected $m_strGatewayResponseText;
	protected $m_strGatewayResponseReasonCode;
	protected $m_strGatewayResponseReasonText;
	protected $m_strGatewayTransactionNumber;
	protected $m_strGatewayApprovalCode;
	protected $m_strGatewayAvsResultCode;
	protected $m_strGatewayAvsResultText;
	protected $m_strGatewayRawData;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['company_merchant_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyMerchantAccountId', trim( $arrValues['company_merchant_account_id'] ) ); elseif( isset( $arrValues['company_merchant_account_id'] ) ) $this->setCompanyMerchantAccountId( $arrValues['company_merchant_account_id'] );
		if( isset( $arrValues['company_payment_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyPaymentId', trim( $arrValues['company_payment_id'] ) ); elseif( isset( $arrValues['company_payment_id'] ) ) $this->setCompanyPaymentId( $arrValues['company_payment_id'] );
		if( isset( $arrValues['payment_transaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPaymentTransactionTypeId', trim( $arrValues['payment_transaction_type_id'] ) ); elseif( isset( $arrValues['payment_transaction_type_id'] ) ) $this->setPaymentTransactionTypeId( $arrValues['payment_transaction_type_id'] );
		if( isset( $arrValues['transaction_number'] ) && $boolDirectSet ) $this->set( 'm_intTransactionNumber', trim( $arrValues['transaction_number'] ) ); elseif( isset( $arrValues['transaction_number'] ) ) $this->setTransactionNumber( $arrValues['transaction_number'] );
		if( isset( $arrValues['transaction_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTransactionDatetime', trim( $arrValues['transaction_datetime'] ) ); elseif( isset( $arrValues['transaction_datetime'] ) ) $this->setTransactionDatetime( $arrValues['transaction_datetime'] );
		if( isset( $arrValues['transaction_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionAmount', trim( $arrValues['transaction_amount'] ) ); elseif( isset( $arrValues['transaction_amount'] ) ) $this->setTransactionAmount( $arrValues['transaction_amount'] );
		if( isset( $arrValues['transaction_memo'] ) && $boolDirectSet ) $this->set( 'm_strTransactionMemo', trim( stripcslashes( $arrValues['transaction_memo'] ) ) ); elseif( isset( $arrValues['transaction_memo'] ) ) $this->setTransactionMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['transaction_memo'] ) : $arrValues['transaction_memo'] );
		if( isset( $arrValues['gateway_response_code'] ) && $boolDirectSet ) $this->set( 'm_strGatewayResponseCode', trim( stripcslashes( $arrValues['gateway_response_code'] ) ) ); elseif( isset( $arrValues['gateway_response_code'] ) ) $this->setGatewayResponseCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gateway_response_code'] ) : $arrValues['gateway_response_code'] );
		if( isset( $arrValues['gateway_response_text'] ) && $boolDirectSet ) $this->set( 'm_strGatewayResponseText', trim( stripcslashes( $arrValues['gateway_response_text'] ) ) ); elseif( isset( $arrValues['gateway_response_text'] ) ) $this->setGatewayResponseText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gateway_response_text'] ) : $arrValues['gateway_response_text'] );
		if( isset( $arrValues['gateway_response_reason_code'] ) && $boolDirectSet ) $this->set( 'm_strGatewayResponseReasonCode', trim( stripcslashes( $arrValues['gateway_response_reason_code'] ) ) ); elseif( isset( $arrValues['gateway_response_reason_code'] ) ) $this->setGatewayResponseReasonCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gateway_response_reason_code'] ) : $arrValues['gateway_response_reason_code'] );
		if( isset( $arrValues['gateway_response_reason_text'] ) && $boolDirectSet ) $this->set( 'm_strGatewayResponseReasonText', trim( stripcslashes( $arrValues['gateway_response_reason_text'] ) ) ); elseif( isset( $arrValues['gateway_response_reason_text'] ) ) $this->setGatewayResponseReasonText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gateway_response_reason_text'] ) : $arrValues['gateway_response_reason_text'] );
		if( isset( $arrValues['gateway_transaction_number'] ) && $boolDirectSet ) $this->set( 'm_strGatewayTransactionNumber', trim( stripcslashes( $arrValues['gateway_transaction_number'] ) ) ); elseif( isset( $arrValues['gateway_transaction_number'] ) ) $this->setGatewayTransactionNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gateway_transaction_number'] ) : $arrValues['gateway_transaction_number'] );
		if( isset( $arrValues['gateway_approval_code'] ) && $boolDirectSet ) $this->set( 'm_strGatewayApprovalCode', trim( stripcslashes( $arrValues['gateway_approval_code'] ) ) ); elseif( isset( $arrValues['gateway_approval_code'] ) ) $this->setGatewayApprovalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gateway_approval_code'] ) : $arrValues['gateway_approval_code'] );
		if( isset( $arrValues['gateway_avs_result_code'] ) && $boolDirectSet ) $this->set( 'm_strGatewayAvsResultCode', trim( stripcslashes( $arrValues['gateway_avs_result_code'] ) ) ); elseif( isset( $arrValues['gateway_avs_result_code'] ) ) $this->setGatewayAvsResultCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gateway_avs_result_code'] ) : $arrValues['gateway_avs_result_code'] );
		if( isset( $arrValues['gateway_avs_result_text'] ) && $boolDirectSet ) $this->set( 'm_strGatewayAvsResultText', trim( stripcslashes( $arrValues['gateway_avs_result_text'] ) ) ); elseif( isset( $arrValues['gateway_avs_result_text'] ) ) $this->setGatewayAvsResultText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gateway_avs_result_text'] ) : $arrValues['gateway_avs_result_text'] );
		if( isset( $arrValues['gateway_raw_data'] ) && $boolDirectSet ) $this->set( 'm_strGatewayRawData', trim( stripcslashes( $arrValues['gateway_raw_data'] ) ) ); elseif( isset( $arrValues['gateway_raw_data'] ) ) $this->setGatewayRawData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gateway_raw_data'] ) : $arrValues['gateway_raw_data'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setCompanyMerchantAccountId( $intCompanyMerchantAccountId ) {
		$this->set( 'm_intCompanyMerchantAccountId', CStrings::strToIntDef( $intCompanyMerchantAccountId, NULL, false ) );
	}

	public function getCompanyMerchantAccountId() {
		return $this->m_intCompanyMerchantAccountId;
	}

	public function sqlCompanyMerchantAccountId() {
		return ( true == isset( $this->m_intCompanyMerchantAccountId ) ) ? ( string ) $this->m_intCompanyMerchantAccountId : 'NULL';
	}

	public function setCompanyPaymentId( $intCompanyPaymentId ) {
		$this->set( 'm_intCompanyPaymentId', CStrings::strToIntDef( $intCompanyPaymentId, NULL, false ) );
	}

	public function getCompanyPaymentId() {
		return $this->m_intCompanyPaymentId;
	}

	public function sqlCompanyPaymentId() {
		return ( true == isset( $this->m_intCompanyPaymentId ) ) ? ( string ) $this->m_intCompanyPaymentId : 'NULL';
	}

	public function setPaymentTransactionTypeId( $intPaymentTransactionTypeId ) {
		$this->set( 'm_intPaymentTransactionTypeId', CStrings::strToIntDef( $intPaymentTransactionTypeId, NULL, false ) );
	}

	public function getPaymentTransactionTypeId() {
		return $this->m_intPaymentTransactionTypeId;
	}

	public function sqlPaymentTransactionTypeId() {
		return ( true == isset( $this->m_intPaymentTransactionTypeId ) ) ? ( string ) $this->m_intPaymentTransactionTypeId : 'NULL';
	}

	public function setTransactionNumber( $intTransactionNumber ) {
		$this->set( 'm_intTransactionNumber', CStrings::strToIntDef( $intTransactionNumber, NULL, false ) );
	}

	public function getTransactionNumber() {
		return $this->m_intTransactionNumber;
	}

	public function sqlTransactionNumber() {
		return ( true == isset( $this->m_intTransactionNumber ) ) ? ( string ) $this->m_intTransactionNumber : 'NULL';
	}

	public function setTransactionDatetime( $strTransactionDatetime ) {
		$this->set( 'm_strTransactionDatetime', CStrings::strTrimDef( $strTransactionDatetime, -1, NULL, true ) );
	}

	public function getTransactionDatetime() {
		return $this->m_strTransactionDatetime;
	}

	public function sqlTransactionDatetime() {
		return ( true == isset( $this->m_strTransactionDatetime ) ) ? '\'' . $this->m_strTransactionDatetime . '\'' : 'NOW()';
	}

	public function setTransactionAmount( $fltTransactionAmount ) {
		$this->set( 'm_fltTransactionAmount', CStrings::strToFloatDef( $fltTransactionAmount, NULL, false, 4 ) );
	}

	public function getTransactionAmount() {
		return $this->m_fltTransactionAmount;
	}

	public function sqlTransactionAmount() {
		return ( true == isset( $this->m_fltTransactionAmount ) ) ? ( string ) $this->m_fltTransactionAmount : 'NULL';
	}

	public function setTransactionMemo( $strTransactionMemo ) {
		$this->set( 'm_strTransactionMemo', CStrings::strTrimDef( $strTransactionMemo, 2000, NULL, true ) );
	}

	public function getTransactionMemo() {
		return $this->m_strTransactionMemo;
	}

	public function sqlTransactionMemo() {
		return ( true == isset( $this->m_strTransactionMemo ) ) ? '\'' . addslashes( $this->m_strTransactionMemo ) . '\'' : 'NULL';
	}

	public function setGatewayResponseCode( $strGatewayResponseCode ) {
		$this->set( 'm_strGatewayResponseCode', CStrings::strTrimDef( $strGatewayResponseCode, 5, NULL, true ) );
	}

	public function getGatewayResponseCode() {
		return $this->m_strGatewayResponseCode;
	}

	public function sqlGatewayResponseCode() {
		return ( true == isset( $this->m_strGatewayResponseCode ) ) ? '\'' . addslashes( $this->m_strGatewayResponseCode ) . '\'' : 'NULL';
	}

	public function setGatewayResponseText( $strGatewayResponseText ) {
		$this->set( 'm_strGatewayResponseText', CStrings::strTrimDef( $strGatewayResponseText, 240, NULL, true ) );
	}

	public function getGatewayResponseText() {
		return $this->m_strGatewayResponseText;
	}

	public function sqlGatewayResponseText() {
		return ( true == isset( $this->m_strGatewayResponseText ) ) ? '\'' . addslashes( $this->m_strGatewayResponseText ) . '\'' : 'NULL';
	}

	public function setGatewayResponseReasonCode( $strGatewayResponseReasonCode ) {
		$this->set( 'm_strGatewayResponseReasonCode', CStrings::strTrimDef( $strGatewayResponseReasonCode, 5, NULL, true ) );
	}

	public function getGatewayResponseReasonCode() {
		return $this->m_strGatewayResponseReasonCode;
	}

	public function sqlGatewayResponseReasonCode() {
		return ( true == isset( $this->m_strGatewayResponseReasonCode ) ) ? '\'' . addslashes( $this->m_strGatewayResponseReasonCode ) . '\'' : 'NULL';
	}

	public function setGatewayResponseReasonText( $strGatewayResponseReasonText ) {
		$this->set( 'm_strGatewayResponseReasonText', CStrings::strTrimDef( $strGatewayResponseReasonText, 240, NULL, true ) );
	}

	public function getGatewayResponseReasonText() {
		return $this->m_strGatewayResponseReasonText;
	}

	public function sqlGatewayResponseReasonText() {
		return ( true == isset( $this->m_strGatewayResponseReasonText ) ) ? '\'' . addslashes( $this->m_strGatewayResponseReasonText ) . '\'' : 'NULL';
	}

	public function setGatewayTransactionNumber( $strGatewayTransactionNumber ) {
		$this->set( 'm_strGatewayTransactionNumber', CStrings::strTrimDef( $strGatewayTransactionNumber, 60, NULL, true ) );
	}

	public function getGatewayTransactionNumber() {
		return $this->m_strGatewayTransactionNumber;
	}

	public function sqlGatewayTransactionNumber() {
		return ( true == isset( $this->m_strGatewayTransactionNumber ) ) ? '\'' . addslashes( $this->m_strGatewayTransactionNumber ) . '\'' : 'NULL';
	}

	public function setGatewayApprovalCode( $strGatewayApprovalCode ) {
		$this->set( 'm_strGatewayApprovalCode', CStrings::strTrimDef( $strGatewayApprovalCode, 60, NULL, true ) );
	}

	public function getGatewayApprovalCode() {
		return $this->m_strGatewayApprovalCode;
	}

	public function sqlGatewayApprovalCode() {
		return ( true == isset( $this->m_strGatewayApprovalCode ) ) ? '\'' . addslashes( $this->m_strGatewayApprovalCode ) . '\'' : 'NULL';
	}

	public function setGatewayAvsResultCode( $strGatewayAvsResultCode ) {
		$this->set( 'm_strGatewayAvsResultCode', CStrings::strTrimDef( $strGatewayAvsResultCode, 5, NULL, true ) );
	}

	public function getGatewayAvsResultCode() {
		return $this->m_strGatewayAvsResultCode;
	}

	public function sqlGatewayAvsResultCode() {
		return ( true == isset( $this->m_strGatewayAvsResultCode ) ) ? '\'' . addslashes( $this->m_strGatewayAvsResultCode ) . '\'' : 'NULL';
	}

	public function setGatewayAvsResultText( $strGatewayAvsResultText ) {
		$this->set( 'm_strGatewayAvsResultText', CStrings::strTrimDef( $strGatewayAvsResultText, 240, NULL, true ) );
	}

	public function getGatewayAvsResultText() {
		return $this->m_strGatewayAvsResultText;
	}

	public function sqlGatewayAvsResultText() {
		return ( true == isset( $this->m_strGatewayAvsResultText ) ) ? '\'' . addslashes( $this->m_strGatewayAvsResultText ) . '\'' : 'NULL';
	}

	public function setGatewayRawData( $strGatewayRawData ) {
		$this->set( 'm_strGatewayRawData', CStrings::strTrimDef( $strGatewayRawData, -1, NULL, true ) );
	}

	public function getGatewayRawData() {
		return $this->m_strGatewayRawData;
	}

	public function sqlGatewayRawData() {
		return ( true == isset( $this->m_strGatewayRawData ) ) ? '\'' . addslashes( $this->m_strGatewayRawData ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, company_merchant_account_id, company_payment_id, payment_transaction_type_id, transaction_number, transaction_datetime, transaction_amount, transaction_memo, gateway_response_code, gateway_response_text, gateway_response_reason_code, gateway_response_reason_text, gateway_transaction_number, gateway_approval_code, gateway_avs_result_code, gateway_avs_result_text, gateway_raw_data, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlAccountId() . ', ' .
 						$this->sqlCompanyMerchantAccountId() . ', ' .
 						$this->sqlCompanyPaymentId() . ', ' .
 						$this->sqlPaymentTransactionTypeId() . ', ' .
 						$this->sqlTransactionNumber() . ', ' .
 						$this->sqlTransactionDatetime() . ', ' .
 						$this->sqlTransactionAmount() . ', ' .
 						$this->sqlTransactionMemo() . ', ' .
 						$this->sqlGatewayResponseCode() . ', ' .
 						$this->sqlGatewayResponseText() . ', ' .
 						$this->sqlGatewayResponseReasonCode() . ', ' .
 						$this->sqlGatewayResponseReasonText() . ', ' .
 						$this->sqlGatewayTransactionNumber() . ', ' .
 						$this->sqlGatewayApprovalCode() . ', ' .
 						$this->sqlGatewayAvsResultCode() . ', ' .
 						$this->sqlGatewayAvsResultText() . ', ' .
 						$this->sqlGatewayRawData() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId() . ','; } elseif( true == array_key_exists( 'CompanyMerchantAccountId', $this->getChangedColumns() ) ) { $strSql .= ' company_merchant_account_id = ' . $this->sqlCompanyMerchantAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; } elseif( true == array_key_exists( 'CompanyPaymentId', $this->getChangedColumns() ) ) { $strSql .= ' company_payment_id = ' . $this->sqlCompanyPaymentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_transaction_type_id = ' . $this->sqlPaymentTransactionTypeId() . ','; } elseif( true == array_key_exists( 'PaymentTransactionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' payment_transaction_type_id = ' . $this->sqlPaymentTransactionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_number = ' . $this->sqlTransactionNumber() . ','; } elseif( true == array_key_exists( 'TransactionNumber', $this->getChangedColumns() ) ) { $strSql .= ' transaction_number = ' . $this->sqlTransactionNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; } elseif( true == array_key_exists( 'TransactionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' transaction_datetime = ' . $this->sqlTransactionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; } elseif( true == array_key_exists( 'TransactionAmount', $this->getChangedColumns() ) ) { $strSql .= ' transaction_amount = ' . $this->sqlTransactionAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_memo = ' . $this->sqlTransactionMemo() . ','; } elseif( true == array_key_exists( 'TransactionMemo', $this->getChangedColumns() ) ) { $strSql .= ' transaction_memo = ' . $this->sqlTransactionMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_response_code = ' . $this->sqlGatewayResponseCode() . ','; } elseif( true == array_key_exists( 'GatewayResponseCode', $this->getChangedColumns() ) ) { $strSql .= ' gateway_response_code = ' . $this->sqlGatewayResponseCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_response_text = ' . $this->sqlGatewayResponseText() . ','; } elseif( true == array_key_exists( 'GatewayResponseText', $this->getChangedColumns() ) ) { $strSql .= ' gateway_response_text = ' . $this->sqlGatewayResponseText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_response_reason_code = ' . $this->sqlGatewayResponseReasonCode() . ','; } elseif( true == array_key_exists( 'GatewayResponseReasonCode', $this->getChangedColumns() ) ) { $strSql .= ' gateway_response_reason_code = ' . $this->sqlGatewayResponseReasonCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_response_reason_text = ' . $this->sqlGatewayResponseReasonText() . ','; } elseif( true == array_key_exists( 'GatewayResponseReasonText', $this->getChangedColumns() ) ) { $strSql .= ' gateway_response_reason_text = ' . $this->sqlGatewayResponseReasonText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_transaction_number = ' . $this->sqlGatewayTransactionNumber() . ','; } elseif( true == array_key_exists( 'GatewayTransactionNumber', $this->getChangedColumns() ) ) { $strSql .= ' gateway_transaction_number = ' . $this->sqlGatewayTransactionNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_approval_code = ' . $this->sqlGatewayApprovalCode() . ','; } elseif( true == array_key_exists( 'GatewayApprovalCode', $this->getChangedColumns() ) ) { $strSql .= ' gateway_approval_code = ' . $this->sqlGatewayApprovalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_avs_result_code = ' . $this->sqlGatewayAvsResultCode() . ','; } elseif( true == array_key_exists( 'GatewayAvsResultCode', $this->getChangedColumns() ) ) { $strSql .= ' gateway_avs_result_code = ' . $this->sqlGatewayAvsResultCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_avs_result_text = ' . $this->sqlGatewayAvsResultText() . ','; } elseif( true == array_key_exists( 'GatewayAvsResultText', $this->getChangedColumns() ) ) { $strSql .= ' gateway_avs_result_text = ' . $this->sqlGatewayAvsResultText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gateway_raw_data = ' . $this->sqlGatewayRawData() . ','; } elseif( true == array_key_exists( 'GatewayRawData', $this->getChangedColumns() ) ) { $strSql .= ' gateway_raw_data = ' . $this->sqlGatewayRawData() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'company_merchant_account_id' => $this->getCompanyMerchantAccountId(),
			'company_payment_id' => $this->getCompanyPaymentId(),
			'payment_transaction_type_id' => $this->getPaymentTransactionTypeId(),
			'transaction_number' => $this->getTransactionNumber(),
			'transaction_datetime' => $this->getTransactionDatetime(),
			'transaction_amount' => $this->getTransactionAmount(),
			'transaction_memo' => $this->getTransactionMemo(),
			'gateway_response_code' => $this->getGatewayResponseCode(),
			'gateway_response_text' => $this->getGatewayResponseText(),
			'gateway_response_reason_code' => $this->getGatewayResponseReasonCode(),
			'gateway_response_reason_text' => $this->getGatewayResponseReasonText(),
			'gateway_transaction_number' => $this->getGatewayTransactionNumber(),
			'gateway_approval_code' => $this->getGatewayApprovalCode(),
			'gateway_avs_result_code' => $this->getGatewayAvsResultCode(),
			'gateway_avs_result_text' => $this->getGatewayAvsResultText(),
			'gateway_raw_data' => $this->getGatewayRawData(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>