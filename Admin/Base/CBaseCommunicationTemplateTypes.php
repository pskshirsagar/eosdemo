<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCommunicationTemplateTypes
 * Do not add any new functions to this class.
 */

class CBaseCommunicationTemplateTypes extends CEosPluralBase {

	/**
	 * @return CCommunicationTemplateType[]
	 */
	public static function fetchCommunicationTemplateTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCommunicationTemplateType', $objDatabase );
	}

	/**
	 * @return CCommunicationTemplateType
	 */
	public static function fetchCommunicationTemplateType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCommunicationTemplateType', $objDatabase );
	}

	public static function fetchCommunicationTemplateTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'communication_template_types', $objDatabase );
	}

	public static function fetchCommunicationTemplateTypeById( $intId, $objDatabase ) {
		return self::fetchCommunicationTemplateType( sprintf( 'SELECT * FROM communication_template_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>