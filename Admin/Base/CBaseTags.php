<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTags
 * Do not add any new functions to this class.
 */

class CBaseTags extends CEosPluralBase {

	/**
	 * @return CTag[]
	 */
	public static function fetchTags( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTag', $objDatabase );
	}

	/**
	 * @return CTag
	 */
	public static function fetchTag( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTag', $objDatabase );
	}

	public static function fetchTagCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tags', $objDatabase );
	}

	public static function fetchTagById( $intId, $objDatabase ) {
		return self::fetchTag( sprintf( 'SELECT * FROM tags WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTagsByActionResultId( $intActionResultId, $objDatabase ) {
		return self::fetchTags( sprintf( 'SELECT * FROM tags WHERE action_result_id = %d', ( int ) $intActionResultId ), $objDatabase );
	}

}
?>