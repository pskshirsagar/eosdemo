<?php

class CBaseHelpCenterSearchEvent extends CEosSingularBase {

	const TABLE_NAME = 'analytics.help_center_search_events';

	protected $m_intId;
	protected $m_strSearchDatetime;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_strHttpReferer;
	protected $m_strHttpUri;
	protected $m_strRequestedModule;
	protected $m_strRequestedAction;
	protected $m_strSearchStringUri;
	protected $m_strSearchStringReferer;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['search_datetime'] ) && $boolDirectSet ) $this->set( 'm_strSearchDatetime', trim( $arrValues['search_datetime'] ) ); elseif( isset( $arrValues['search_datetime'] ) ) $this->setSearchDatetime( $arrValues['search_datetime'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['http_referer'] ) && $boolDirectSet ) $this->set( 'm_strHttpReferer', trim( $arrValues['http_referer'] ) ); elseif( isset( $arrValues['http_referer'] ) ) $this->setHttpReferer( $arrValues['http_referer'] );
		if( isset( $arrValues['http_uri'] ) && $boolDirectSet ) $this->set( 'm_strHttpUri', trim( $arrValues['http_uri'] ) ); elseif( isset( $arrValues['http_uri'] ) ) $this->setHttpUri( $arrValues['http_uri'] );
		if( isset( $arrValues['requested_module'] ) && $boolDirectSet ) $this->set( 'm_strRequestedModule', trim( $arrValues['requested_module'] ) ); elseif( isset( $arrValues['requested_module'] ) ) $this->setRequestedModule( $arrValues['requested_module'] );
		if( isset( $arrValues['requested_action'] ) && $boolDirectSet ) $this->set( 'm_strRequestedAction', trim( $arrValues['requested_action'] ) ); elseif( isset( $arrValues['requested_action'] ) ) $this->setRequestedAction( $arrValues['requested_action'] );
		if( isset( $arrValues['search_string_uri'] ) && $boolDirectSet ) $this->set( 'm_strSearchStringUri', trim( $arrValues['search_string_uri'] ) ); elseif( isset( $arrValues['search_string_uri'] ) ) $this->setSearchStringUri( $arrValues['search_string_uri'] );
		if( isset( $arrValues['search_string_referer'] ) && $boolDirectSet ) $this->set( 'm_strSearchStringReferer', trim( $arrValues['search_string_referer'] ) ); elseif( isset( $arrValues['search_string_referer'] ) ) $this->setSearchStringReferer( $arrValues['search_string_referer'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSearchDatetime( $strSearchDatetime ) {
		$this->set( 'm_strSearchDatetime', CStrings::strTrimDef( $strSearchDatetime, -1, NULL, true ) );
	}

	public function getSearchDatetime() {
		return $this->m_strSearchDatetime;
	}

	public function sqlSearchDatetime() {
		return ( true == isset( $this->m_strSearchDatetime ) ) ? '\'' . $this->m_strSearchDatetime . '\'' : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setHttpReferer( $strHttpReferer ) {
		$this->set( 'm_strHttpReferer', CStrings::strTrimDef( $strHttpReferer, -1, NULL, true ) );
	}

	public function getHttpReferer() {
		return $this->m_strHttpReferer;
	}

	public function sqlHttpReferer() {
		return ( true == isset( $this->m_strHttpReferer ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHttpReferer ) : '\'' . addslashes( $this->m_strHttpReferer ) . '\'' ) : 'NULL';
	}

	public function setHttpUri( $strHttpUri ) {
		$this->set( 'm_strHttpUri', CStrings::strTrimDef( $strHttpUri, -1, NULL, true ) );
	}

	public function getHttpUri() {
		return $this->m_strHttpUri;
	}

	public function sqlHttpUri() {
		return ( true == isset( $this->m_strHttpUri ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHttpUri ) : '\'' . addslashes( $this->m_strHttpUri ) . '\'' ) : 'NULL';
	}

	public function setRequestedModule( $strRequestedModule ) {
		$this->set( 'm_strRequestedModule', CStrings::strTrimDef( $strRequestedModule, -1, NULL, true ) );
	}

	public function getRequestedModule() {
		return $this->m_strRequestedModule;
	}

	public function sqlRequestedModule() {
		return ( true == isset( $this->m_strRequestedModule ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRequestedModule ) : '\'' . addslashes( $this->m_strRequestedModule ) . '\'' ) : 'NULL';
	}

	public function setRequestedAction( $strRequestedAction ) {
		$this->set( 'm_strRequestedAction', CStrings::strTrimDef( $strRequestedAction, -1, NULL, true ) );
	}

	public function getRequestedAction() {
		return $this->m_strRequestedAction;
	}

	public function sqlRequestedAction() {
		return ( true == isset( $this->m_strRequestedAction ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRequestedAction ) : '\'' . addslashes( $this->m_strRequestedAction ) . '\'' ) : 'NULL';
	}

	public function setSearchStringUri( $strSearchStringUri ) {
		$this->set( 'm_strSearchStringUri', CStrings::strTrimDef( $strSearchStringUri, -1, NULL, true ) );
	}

	public function getSearchStringUri() {
		return $this->m_strSearchStringUri;
	}

	public function sqlSearchStringUri() {
		return ( true == isset( $this->m_strSearchStringUri ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSearchStringUri ) : '\'' . addslashes( $this->m_strSearchStringUri ) . '\'' ) : 'NULL';
	}

	public function setSearchStringReferer( $strSearchStringReferer ) {
		$this->set( 'm_strSearchStringReferer', CStrings::strTrimDef( $strSearchStringReferer, -1, NULL, true ) );
	}

	public function getSearchStringReferer() {
		return $this->m_strSearchStringReferer;
	}

	public function sqlSearchStringReferer() {
		return ( true == isset( $this->m_strSearchStringReferer ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSearchStringReferer ) : '\'' . addslashes( $this->m_strSearchStringReferer ) . '\'' ) : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, search_datetime, cid, company_user_id, http_referer, http_uri, requested_module, requested_action, search_string_uri, search_string_referer, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlSearchDatetime() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlHttpReferer() . ', ' .
						$this->sqlHttpUri() . ', ' .
						$this->sqlRequestedModule() . ', ' .
						$this->sqlRequestedAction() . ', ' .
						$this->sqlSearchStringUri() . ', ' .
						$this->sqlSearchStringReferer() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' search_datetime = ' . $this->sqlSearchDatetime(). ',' ; } elseif( true == array_key_exists( 'SearchDatetime', $this->getChangedColumns() ) ) { $strSql .= ' search_datetime = ' . $this->sqlSearchDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' http_referer = ' . $this->sqlHttpReferer(). ',' ; } elseif( true == array_key_exists( 'HttpReferer', $this->getChangedColumns() ) ) { $strSql .= ' http_referer = ' . $this->sqlHttpReferer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' http_uri = ' . $this->sqlHttpUri(). ',' ; } elseif( true == array_key_exists( 'HttpUri', $this->getChangedColumns() ) ) { $strSql .= ' http_uri = ' . $this->sqlHttpUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_module = ' . $this->sqlRequestedModule(). ',' ; } elseif( true == array_key_exists( 'RequestedModule', $this->getChangedColumns() ) ) { $strSql .= ' requested_module = ' . $this->sqlRequestedModule() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_action = ' . $this->sqlRequestedAction(). ',' ; } elseif( true == array_key_exists( 'RequestedAction', $this->getChangedColumns() ) ) { $strSql .= ' requested_action = ' . $this->sqlRequestedAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' search_string_uri = ' . $this->sqlSearchStringUri(). ',' ; } elseif( true == array_key_exists( 'SearchStringUri', $this->getChangedColumns() ) ) { $strSql .= ' search_string_uri = ' . $this->sqlSearchStringUri() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' search_string_referer = ' . $this->sqlSearchStringReferer() ; } elseif( true == array_key_exists( 'SearchStringReferer', $this->getChangedColumns() ) ) { $strSql .= ' search_string_referer = ' . $this->sqlSearchStringReferer() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'search_datetime' => $this->getSearchDatetime(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'http_referer' => $this->getHttpReferer(),
			'http_uri' => $this->getHttpUri(),
			'requested_module' => $this->getRequestedModule(),
			'requested_action' => $this->getRequestedAction(),
			'search_string_uri' => $this->getSearchStringUri(),
			'search_string_referer' => $this->getSearchStringReferer(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>