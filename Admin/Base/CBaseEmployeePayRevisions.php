<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePayRevisions
 * Do not add any new functions to this class.
 */

class CBaseEmployeePayRevisions extends CEosPluralBase {

	/**
	 * @return CEmployeePayRevision[]
	 */
	public static function fetchEmployeePayRevisions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeePayRevision', $objDatabase );
	}

	/**
	 * @return CEmployeePayRevision
	 */
	public static function fetchEmployeePayRevision( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeePayRevision', $objDatabase );
	}

	public static function fetchEmployeePayRevisionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_pay_revisions', $objDatabase );
	}

	public static function fetchEmployeePayRevisionById( $intId, $objDatabase ) {
		return self::fetchEmployeePayRevision( sprintf( 'SELECT * FROM employee_pay_revisions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeePayRevisionsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeePayRevisions( sprintf( 'SELECT * FROM employee_pay_revisions WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>