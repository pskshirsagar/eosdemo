<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CBonusTypes
 * Do not add any new functions to this class.
 */

class CBaseBonusTypes extends CEosPluralBase {

	/**
	 * @return CBonusType[]
	 */
	public static function fetchBonusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CBonusType', $objDatabase );
	}

	/**
	 * @return CBonusType
	 */
	public static function fetchBonusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CBonusType', $objDatabase );
	}

	public static function fetchBonusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'bonus_types', $objDatabase );
	}

	public static function fetchBonusTypeById( $intId, $objDatabase ) {
		return self::fetchBonusType( sprintf( 'SELECT * FROM bonus_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>