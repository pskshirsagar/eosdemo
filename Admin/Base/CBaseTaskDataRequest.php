<?php

class CBaseTaskDataRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.task_data_requests';

	protected $m_intId;
	protected $m_intCompanyReportId;
	protected $m_intTaskId;
	protected $m_strAdditionalInfo;
	protected $m_strRequestedCompletionDate;
	protected $m_strProjectedCompletionDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['company_report_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyReportId', trim( $arrValues['company_report_id'] ) ); elseif( isset( $arrValues['company_report_id'] ) ) $this->setCompanyReportId( $arrValues['company_report_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['additional_info'] ) && $boolDirectSet ) $this->set( 'm_strAdditionalInfo', trim( stripcslashes( $arrValues['additional_info'] ) ) ); elseif( isset( $arrValues['additional_info'] ) ) $this->setAdditionalInfo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['additional_info'] ) : $arrValues['additional_info'] );
		if( isset( $arrValues['requested_completion_date'] ) && $boolDirectSet ) $this->set( 'm_strRequestedCompletionDate', trim( $arrValues['requested_completion_date'] ) ); elseif( isset( $arrValues['requested_completion_date'] ) ) $this->setRequestedCompletionDate( $arrValues['requested_completion_date'] );
		if( isset( $arrValues['projected_completion_date'] ) && $boolDirectSet ) $this->set( 'm_strProjectedCompletionDate', trim( $arrValues['projected_completion_date'] ) ); elseif( isset( $arrValues['projected_completion_date'] ) ) $this->setProjectedCompletionDate( $arrValues['projected_completion_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCompanyReportId( $intCompanyReportId ) {
		$this->set( 'm_intCompanyReportId', CStrings::strToIntDef( $intCompanyReportId, NULL, false ) );
	}

	public function getCompanyReportId() {
		return $this->m_intCompanyReportId;
	}

	public function sqlCompanyReportId() {
		return ( true == isset( $this->m_intCompanyReportId ) ) ? ( string ) $this->m_intCompanyReportId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setAdditionalInfo( $strAdditionalInfo ) {
		$this->set( 'm_strAdditionalInfo', CStrings::strTrimDef( $strAdditionalInfo, -1, NULL, true ) );
	}

	public function getAdditionalInfo() {
		return $this->m_strAdditionalInfo;
	}

	public function sqlAdditionalInfo() {
		return ( true == isset( $this->m_strAdditionalInfo ) ) ? '\'' . addslashes( $this->m_strAdditionalInfo ) . '\'' : 'NULL';
	}

	public function setRequestedCompletionDate( $strRequestedCompletionDate ) {
		$this->set( 'm_strRequestedCompletionDate', CStrings::strTrimDef( $strRequestedCompletionDate, -1, NULL, true ) );
	}

	public function getRequestedCompletionDate() {
		return $this->m_strRequestedCompletionDate;
	}

	public function sqlRequestedCompletionDate() {
		return ( true == isset( $this->m_strRequestedCompletionDate ) ) ? '\'' . $this->m_strRequestedCompletionDate . '\'' : 'NULL';
	}

	public function setProjectedCompletionDate( $strProjectedCompletionDate ) {
		$this->set( 'm_strProjectedCompletionDate', CStrings::strTrimDef( $strProjectedCompletionDate, -1, NULL, true ) );
	}

	public function getProjectedCompletionDate() {
		return $this->m_strProjectedCompletionDate;
	}

	public function sqlProjectedCompletionDate() {
		return ( true == isset( $this->m_strProjectedCompletionDate ) ) ? '\'' . $this->m_strProjectedCompletionDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, company_report_id, task_id, additional_info, requested_completion_date, projected_completion_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCompanyReportId() . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlAdditionalInfo() . ', ' .
 						$this->sqlRequestedCompletionDate() . ', ' .
 						$this->sqlProjectedCompletionDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_report_id = ' . $this->sqlCompanyReportId() . ','; } elseif( true == array_key_exists( 'CompanyReportId', $this->getChangedColumns() ) ) { $strSql .= ' company_report_id = ' . $this->sqlCompanyReportId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_info = ' . $this->sqlAdditionalInfo() . ','; } elseif( true == array_key_exists( 'AdditionalInfo', $this->getChangedColumns() ) ) { $strSql .= ' additional_info = ' . $this->sqlAdditionalInfo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_completion_date = ' . $this->sqlRequestedCompletionDate() . ','; } elseif( true == array_key_exists( 'RequestedCompletionDate', $this->getChangedColumns() ) ) { $strSql .= ' requested_completion_date = ' . $this->sqlRequestedCompletionDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' projected_completion_date = ' . $this->sqlProjectedCompletionDate() . ','; } elseif( true == array_key_exists( 'ProjectedCompletionDate', $this->getChangedColumns() ) ) { $strSql .= ' projected_completion_date = ' . $this->sqlProjectedCompletionDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'company_report_id' => $this->getCompanyReportId(),
			'task_id' => $this->getTaskId(),
			'additional_info' => $this->getAdditionalInfo(),
			'requested_completion_date' => $this->getRequestedCompletionDate(),
			'projected_completion_date' => $this->getProjectedCompletionDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>