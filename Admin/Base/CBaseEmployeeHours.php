<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeHours
 * Do not add any new functions to this class.
 */

class CBaseEmployeeHours extends CEosPluralBase {

	/**
	 * @return CEmployeeHour[]
	 */
	public static function fetchEmployeeHours( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeHour', $objDatabase );
	}

	/**
	 * @return CEmployeeHour
	 */
	public static function fetchEmployeeHour( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeHour', $objDatabase );
	}

	public static function fetchEmployeeHourCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_hours', $objDatabase );
	}

	public static function fetchEmployeeHourById( $intId, $objDatabase ) {
		return self::fetchEmployeeHour( sprintf( 'SELECT * FROM employee_hours WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeHoursByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeHours( sprintf( 'SELECT * FROM employee_hours WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeHoursByPayrollPeriodId( $intPayrollPeriodId, $objDatabase ) {
		return self::fetchEmployeeHours( sprintf( 'SELECT * FROM employee_hours WHERE payroll_period_id = %d', ( int ) $intPayrollPeriodId ), $objDatabase );
	}

	public static function fetchEmployeeHoursByEmployeeVacationRequestId( $intEmployeeVacationRequestId, $objDatabase ) {
		return self::fetchEmployeeHours( sprintf( 'SELECT * FROM employee_hours WHERE employee_vacation_request_id = %d', ( int ) $intEmployeeVacationRequestId ), $objDatabase );
	}

	public static function fetchEmployeeHoursByEmployeeHourTypeId( $intEmployeeHourTypeId, $objDatabase ) {
		return self::fetchEmployeeHours( sprintf( 'SELECT * FROM employee_hours WHERE employee_hour_type_id = %d', ( int ) $intEmployeeHourTypeId ), $objDatabase );
	}

}
?>