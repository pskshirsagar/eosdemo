<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDesignations
 * Do not add any new functions to this class.
 */

class CBaseDesignations extends CEosPluralBase {

	/**
	 * @return CDesignation[]
	 */
	public static function fetchDesignations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CDesignation::class, $objDatabase );
	}

	/**
	 * @return CDesignation
	 */
	public static function fetchDesignation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CDesignation::class, $objDatabase );
	}

	public static function fetchDesignationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'designations', $objDatabase );
	}

	public static function fetchDesignationById( $intId, $objDatabase ) {
		return self::fetchDesignation( sprintf( 'SELECT * FROM designations WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchDesignationsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchDesignations( sprintf( 'SELECT * FROM designations WHERE department_id = %d', $intDepartmentId ), $objDatabase );
	}

	public static function fetchDesignationsByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchDesignations( sprintf( 'SELECT * FROM designations WHERE designation_id = %d', $intDesignationId ), $objDatabase );
	}

	public static function fetchDesignationsByApproverEmployeeId( $intApproverEmployeeId, $objDatabase ) {
		return self::fetchDesignations( sprintf( 'SELECT * FROM designations WHERE approver_employee_id = %d', $intApproverEmployeeId ), $objDatabase );
	}

	public static function fetchDesignationsByCompChangeApproverId( $intCompChangeApproverId, $objDatabase ) {
		return self::fetchDesignations( sprintf( 'SELECT * FROM designations WHERE comp_change_approver_id = %d', $intCompChangeApproverId ), $objDatabase );
	}

	public static function fetchDesignationsByReplacementApproverId( $intReplacementApproverId, $objDatabase ) {
		return self::fetchDesignations( sprintf( 'SELECT * FROM designations WHERE replacement_approver_id = %d', $intReplacementApproverId ), $objDatabase );
	}

	public static function fetchDesignationsByEeocClassificationTypeId( $intEeocClassificationTypeId, $objDatabase ) {
		return self::fetchDesignations( sprintf( 'SELECT * FROM designations WHERE eeoc_classification_type_id = %d', $intEeocClassificationTypeId ), $objDatabase );
	}

	public static function fetchDesignationsByEmployeeSalaryTypeId( $intEmployeeSalaryTypeId, $objDatabase ) {
		return self::fetchDesignations( sprintf( 'SELECT * FROM designations WHERE employee_salary_type_id = %d', $intEmployeeSalaryTypeId ), $objDatabase );
	}

	public static function fetchDesignationsByBonusApproverEmployeeId( $intBonusApproverEmployeeId, $objDatabase ) {
		return self::fetchDesignations( sprintf( 'SELECT * FROM designations WHERE bonus_approver_employee_id = %d', $intBonusApproverEmployeeId ), $objDatabase );
	}

	public static function fetchDesignationsByBonusCriteriaApproverEmployeeId( $intBonusCriteriaApproverEmployeeId, $objDatabase ) {
		return self::fetchDesignations( sprintf( 'SELECT * FROM designations WHERE bonus_criteria_approver_employee_id = %d', $intBonusCriteriaApproverEmployeeId ), $objDatabase );
	}

}
?>