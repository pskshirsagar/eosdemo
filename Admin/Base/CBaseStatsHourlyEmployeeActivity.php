<?php

class CBaseStatsHourlyEmployeeActivity extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_hourly_employee_activities';

	protected $m_dblId;
	protected $m_intEmployeeId;
	protected $m_strHour;
	protected $m_intHasAdminActivity;
	protected $m_intTotalAdminClicks;
	protected $m_intTotalEntrataClicks;
	protected $m_intAdminMinutesActive;
	protected $m_intInternalEmailsSent;
	protected $m_intExternalEmailsSent;
	protected $m_intInternalEmailsReceived;
	protected $m_intInteralChats;
	protected $m_intExternalChats;
	protected $m_intChatCharacters;
	protected $m_intWorkWebPageVisits;
	protected $m_intBreakWebPageVisits;
	protected $m_intPhoneCalls;
	protected $m_intPhoneCallMinutes;
	protected $m_intWebConferenceMinutes;
	protected $m_intMinutesClockedIn;
	protected $m_intNewGoogleDocs;
	protected $m_intGoogleDocEdits;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intHasAdminActivity = '0';
		$this->m_intTotalAdminClicks = '0';
		$this->m_intTotalEntrataClicks = '0';
		$this->m_intAdminMinutesActive = '0';
		$this->m_intInternalEmailsSent = '0';
		$this->m_intExternalEmailsSent = '0';
		$this->m_intInternalEmailsReceived = '0';
		$this->m_intInteralChats = '0';
		$this->m_intExternalChats = '0';
		$this->m_intChatCharacters = '0';
		$this->m_intWorkWebPageVisits = '0';
		$this->m_intBreakWebPageVisits = '0';
		$this->m_intPhoneCalls = '0';
		$this->m_intPhoneCallMinutes = '0';
		$this->m_intWebConferenceMinutes = '0';
		$this->m_intMinutesClockedIn = '0';
		$this->m_intNewGoogleDocs = '0';
		$this->m_intGoogleDocEdits = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
        if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_dblId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->m_intEmployeeId = trim( $arrValues['employee_id'] ); else if( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['hour'] ) && $boolDirectSet ) $this->m_strHour = trim( $arrValues['hour'] ); else if( isset( $arrValues['hour'] ) ) $this->setHour( $arrValues['hour'] );
		if( isset( $arrValues['has_admin_activity'] ) && $boolDirectSet ) $this->m_intHasAdminActivity = trim( $arrValues['has_admin_activity'] ); else if( isset( $arrValues['has_admin_activity'] ) ) $this->setHasAdminActivity( $arrValues['has_admin_activity'] );
		if( isset( $arrValues['total_admin_clicks'] ) && $boolDirectSet ) $this->m_intTotalAdminClicks = trim( $arrValues['total_admin_clicks'] ); else if( isset( $arrValues['total_admin_clicks'] ) ) $this->setTotalAdminClicks( $arrValues['total_admin_clicks'] );
		if( isset( $arrValues['total_entrata_clicks'] ) && $boolDirectSet ) $this->m_intTotalEntrataClicks = trim( $arrValues['total_entrata_clicks'] ); else if( isset( $arrValues['total_entrata_clicks'] ) ) $this->setTotalEntrataClicks( $arrValues['total_entrata_clicks'] );
		if( isset( $arrValues['admin_minutes_active'] ) && $boolDirectSet ) $this->m_intAdminMinutesActive = trim( $arrValues['admin_minutes_active'] ); else if( isset( $arrValues['admin_minutes_active'] ) ) $this->setAdminMinutesActive( $arrValues['admin_minutes_active'] );
		if( isset( $arrValues['internal_emails_sent'] ) && $boolDirectSet ) $this->m_intInternalEmailsSent = trim( $arrValues['internal_emails_sent'] ); else if( isset( $arrValues['internal_emails_sent'] ) ) $this->setInternalEmailsSent( $arrValues['internal_emails_sent'] );
		if( isset( $arrValues['external_emails_sent'] ) && $boolDirectSet ) $this->m_intExternalEmailsSent = trim( $arrValues['external_emails_sent'] ); else if( isset( $arrValues['external_emails_sent'] ) ) $this->setExternalEmailsSent( $arrValues['external_emails_sent'] );
		if( isset( $arrValues['internal_emails_received'] ) && $boolDirectSet ) $this->m_intInternalEmailsReceived = trim( $arrValues['internal_emails_received'] ); else if( isset( $arrValues['internal_emails_received'] ) ) $this->setInternalEmailsReceived( $arrValues['internal_emails_received'] );
		if( isset( $arrValues['interal_chats'] ) && $boolDirectSet ) $this->m_intInteralChats = trim( $arrValues['interal_chats'] ); else if( isset( $arrValues['interal_chats'] ) ) $this->setInteralChats( $arrValues['interal_chats'] );
		if( isset( $arrValues['external_chats'] ) && $boolDirectSet ) $this->m_intExternalChats = trim( $arrValues['external_chats'] ); else if( isset( $arrValues['external_chats'] ) ) $this->setExternalChats( $arrValues['external_chats'] );
		if( isset( $arrValues['chat_characters'] ) && $boolDirectSet ) $this->m_intChatCharacters = trim( $arrValues['chat_characters'] ); else if( isset( $arrValues['chat_characters'] ) ) $this->setChatCharacters( $arrValues['chat_characters'] );
		if( isset( $arrValues['work_web_page_visits'] ) && $boolDirectSet ) $this->m_intWorkWebPageVisits = trim( $arrValues['work_web_page_visits'] ); else if( isset( $arrValues['work_web_page_visits'] ) ) $this->setWorkWebPageVisits( $arrValues['work_web_page_visits'] );
		if( isset( $arrValues['break_web_page_visits'] ) && $boolDirectSet ) $this->m_intBreakWebPageVisits = trim( $arrValues['break_web_page_visits'] ); else if( isset( $arrValues['break_web_page_visits'] ) ) $this->setBreakWebPageVisits( $arrValues['break_web_page_visits'] );
		if( isset( $arrValues['phone_calls'] ) && $boolDirectSet ) $this->m_intPhoneCalls = trim( $arrValues['phone_calls'] ); else if( isset( $arrValues['phone_calls'] ) ) $this->setPhoneCalls( $arrValues['phone_calls'] );
		if( isset( $arrValues['phone_call_minutes'] ) && $boolDirectSet ) $this->m_intPhoneCallMinutes = trim( $arrValues['phone_call_minutes'] ); else if( isset( $arrValues['phone_call_minutes'] ) ) $this->setPhoneCallMinutes( $arrValues['phone_call_minutes'] );
		if( isset( $arrValues['web_conference_minutes'] ) && $boolDirectSet ) $this->m_intWebConferenceMinutes = trim( $arrValues['web_conference_minutes'] ); else if( isset( $arrValues['web_conference_minutes'] ) ) $this->setWebConferenceMinutes( $arrValues['web_conference_minutes'] );
		if( isset( $arrValues['minutes_clocked_in'] ) && $boolDirectSet ) $this->m_intMinutesClockedIn = trim( $arrValues['minutes_clocked_in'] ); else if( isset( $arrValues['minutes_clocked_in'] ) ) $this->setMinutesClockedIn( $arrValues['minutes_clocked_in'] );
		if( isset( $arrValues['new_google_docs'] ) && $boolDirectSet ) $this->m_intNewGoogleDocs = trim( $arrValues['new_google_docs'] ); else if( isset( $arrValues['new_google_docs'] ) ) $this->setNewGoogleDocs( $arrValues['new_google_docs'] );
		if( isset( $arrValues['google_doc_edits'] ) && $boolDirectSet ) $this->m_intGoogleDocEdits = trim( $arrValues['google_doc_edits'] ); else if( isset( $arrValues['google_doc_edits'] ) ) $this->setGoogleDocEdits( $arrValues['google_doc_edits'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
	}

    public function setId( $dblId ) {
        $this->m_dblId = CStrings::strToDoubleDef( $dblId, NULL, false );
	}

	public function getId() {
        return $this->m_dblId;
	}

	public function sqlId() {
        return ( true == isset( $this->m_dblId ) ) ? ( double ) $this->m_dblId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = CStrings::strToIntDef( $intEmployeeId, NULL, false );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setHour( $strHour ) {
		$this->m_strHour = CStrings::strTrimDef( $strHour, -1, NULL, true );
	}

	public function getHour() {
		return $this->m_strHour;
	}

	public function sqlHour() {
		return ( true == isset( $this->m_strHour ) ) ? '\'' . $this->m_strHour . '\'' : 'NOW()';
	}

	public function setHasAdminActivity( $intHasAdminActivity ) {
		$this->m_intHasAdminActivity = CStrings::strToIntDef( $intHasAdminActivity, NULL, false );
	}

	public function getHasAdminActivity() {
		return $this->m_intHasAdminActivity;
	}

	public function sqlHasAdminActivity() {
		return ( true == isset( $this->m_intHasAdminActivity ) ) ? ( string ) $this->m_intHasAdminActivity : '0';
	}

	public function setTotalAdminClicks( $intTotalAdminClicks ) {
		$this->m_intTotalAdminClicks = CStrings::strToIntDef( $intTotalAdminClicks, NULL, false );
	}

	public function getTotalAdminClicks() {
		return $this->m_intTotalAdminClicks;
	}

	public function sqlTotalAdminClicks() {
		return ( true == isset( $this->m_intTotalAdminClicks ) ) ? ( string ) $this->m_intTotalAdminClicks : '0';
	}

	public function setTotalEntrataClicks( $intTotalEntrataClicks ) {
		$this->m_intTotalEntrataClicks = CStrings::strToIntDef( $intTotalEntrataClicks, NULL, false );
	}

	public function getTotalEntrataClicks() {
		return $this->m_intTotalEntrataClicks;
	}

	public function sqlTotalEntrataClicks() {
		return ( true == isset( $this->m_intTotalEntrataClicks ) ) ? ( string ) $this->m_intTotalEntrataClicks : '0';
	}

	public function setAdminMinutesActive( $intAdminMinutesActive ) {
		$this->m_intAdminMinutesActive = CStrings::strToIntDef( $intAdminMinutesActive, NULL, false );
	}

	public function getAdminMinutesActive() {
		return $this->m_intAdminMinutesActive;
	}

	public function sqlAdminMinutesActive() {
		return ( true == isset( $this->m_intAdminMinutesActive ) ) ? ( string ) $this->m_intAdminMinutesActive : '0';
	}

	public function setInternalEmailsSent( $intInternalEmailsSent ) {
		$this->m_intInternalEmailsSent = CStrings::strToIntDef( $intInternalEmailsSent, NULL, false );
	}

	public function getInternalEmailsSent() {
		return $this->m_intInternalEmailsSent;
	}

	public function sqlInternalEmailsSent() {
		return ( true == isset( $this->m_intInternalEmailsSent ) ) ? ( string ) $this->m_intInternalEmailsSent : '0';
	}

	public function setExternalEmailsSent( $intExternalEmailsSent ) {
		$this->m_intExternalEmailsSent = CStrings::strToIntDef( $intExternalEmailsSent, NULL, false );
	}

	public function getExternalEmailsSent() {
		return $this->m_intExternalEmailsSent;
	}

	public function sqlExternalEmailsSent() {
		return ( true == isset( $this->m_intExternalEmailsSent ) ) ? ( string ) $this->m_intExternalEmailsSent : '0';
	}

	public function setInternalEmailsReceived( $intInternalEmailsReceived ) {
		$this->m_intInternalEmailsReceived = CStrings::strToIntDef( $intInternalEmailsReceived, NULL, false );
	}

	public function getInternalEmailsReceived() {
		return $this->m_intInternalEmailsReceived;
	}

	public function sqlInternalEmailsReceived() {
		return ( true == isset( $this->m_intInternalEmailsReceived ) ) ? ( string ) $this->m_intInternalEmailsReceived : '0';
	}

	public function setInteralChats( $intInteralChats ) {
		$this->m_intInteralChats = CStrings::strToIntDef( $intInteralChats, NULL, false );
	}

	public function getInteralChats() {
		return $this->m_intInteralChats;
	}

	public function sqlInteralChats() {
		return ( true == isset( $this->m_intInteralChats ) ) ? ( string ) $this->m_intInteralChats : '0';
	}

	public function setExternalChats( $intExternalChats ) {
		$this->m_intExternalChats = CStrings::strToIntDef( $intExternalChats, NULL, false );
	}

	public function getExternalChats() {
		return $this->m_intExternalChats;
	}

	public function sqlExternalChats() {
		return ( true == isset( $this->m_intExternalChats ) ) ? ( string ) $this->m_intExternalChats : '0';
	}

	public function setChatCharacters( $intChatCharacters ) {
		$this->m_intChatCharacters = CStrings::strToIntDef( $intChatCharacters, NULL, false );
	}

	public function getChatCharacters() {
		return $this->m_intChatCharacters;
	}

	public function sqlChatCharacters() {
		return ( true == isset( $this->m_intChatCharacters ) ) ? ( string ) $this->m_intChatCharacters : '0';
	}

	public function setWorkWebPageVisits( $intWorkWebPageVisits ) {
		$this->m_intWorkWebPageVisits = CStrings::strToIntDef( $intWorkWebPageVisits, NULL, false );
	}

	public function getWorkWebPageVisits() {
		return $this->m_intWorkWebPageVisits;
	}

	public function sqlWorkWebPageVisits() {
		return ( true == isset( $this->m_intWorkWebPageVisits ) ) ? ( string ) $this->m_intWorkWebPageVisits : '0';
	}

	public function setBreakWebPageVisits( $intBreakWebPageVisits ) {
		$this->m_intBreakWebPageVisits = CStrings::strToIntDef( $intBreakWebPageVisits, NULL, false );
	}

	public function getBreakWebPageVisits() {
		return $this->m_intBreakWebPageVisits;
	}

	public function sqlBreakWebPageVisits() {
		return ( true == isset( $this->m_intBreakWebPageVisits ) ) ? ( string ) $this->m_intBreakWebPageVisits : '0';
	}

	public function setPhoneCalls( $intPhoneCalls ) {
		$this->m_intPhoneCalls = CStrings::strToIntDef( $intPhoneCalls, NULL, false );
	}

	public function getPhoneCalls() {
		return $this->m_intPhoneCalls;
	}

	public function sqlPhoneCalls() {
		return ( true == isset( $this->m_intPhoneCalls ) ) ? ( string ) $this->m_intPhoneCalls : '0';
	}

	public function setPhoneCallMinutes( $intPhoneCallMinutes ) {
		$this->m_intPhoneCallMinutes = CStrings::strToIntDef( $intPhoneCallMinutes, NULL, false );
	}

	public function getPhoneCallMinutes() {
		return $this->m_intPhoneCallMinutes;
	}

	public function sqlPhoneCallMinutes() {
		return ( true == isset( $this->m_intPhoneCallMinutes ) ) ? ( string ) $this->m_intPhoneCallMinutes : '0';
	}

	public function setWebConferenceMinutes( $intWebConferenceMinutes ) {
		$this->m_intWebConferenceMinutes = CStrings::strToIntDef( $intWebConferenceMinutes, NULL, false );
	}

	public function getWebConferenceMinutes() {
		return $this->m_intWebConferenceMinutes;
	}

	public function sqlWebConferenceMinutes() {
		return ( true == isset( $this->m_intWebConferenceMinutes ) ) ? ( string ) $this->m_intWebConferenceMinutes : '0';
	}

	public function setMinutesClockedIn( $intMinutesClockedIn ) {
		$this->m_intMinutesClockedIn = CStrings::strToIntDef( $intMinutesClockedIn, NULL, false );
	}

	public function getMinutesClockedIn() {
		return $this->m_intMinutesClockedIn;
	}

	public function sqlMinutesClockedIn() {
		return ( true == isset( $this->m_intMinutesClockedIn ) ) ? ( string ) $this->m_intMinutesClockedIn : '0';
	}

	public function setNewGoogleDocs( $intNewGoogleDocs ) {
		$this->m_intNewGoogleDocs = CStrings::strToIntDef( $intNewGoogleDocs, NULL, false );
	}

	public function getNewGoogleDocs() {
		return $this->m_intNewGoogleDocs;
	}

	public function sqlNewGoogleDocs() {
		return ( true == isset( $this->m_intNewGoogleDocs ) ) ? ( string ) $this->m_intNewGoogleDocs : '0';
	}

	public function setGoogleDocEdits( $intGoogleDocEdits ) {
		$this->m_intGoogleDocEdits = CStrings::strToIntDef( $intGoogleDocEdits, NULL, false );
	}

	public function getGoogleDocEdits() {
		return $this->m_intGoogleDocEdits;
	}

	public function sqlGoogleDocEdits() {
		return ( true == isset( $this->m_intGoogleDocEdits ) ) ? ( string ) $this->m_intGoogleDocEdits : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, hour, has_admin_activity, total_admin_clicks, total_entrata_clicks, admin_minutes_active, internal_emails_sent, external_emails_sent, internal_emails_received, interal_chats, external_chats, chat_characters, work_web_page_visits, break_web_page_visits, phone_calls, phone_call_minutes, web_conference_minutes, minutes_clocked_in, new_google_docs, google_doc_edits, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlHour() . ', ' .
 						$this->sqlHasAdminActivity() . ', ' .
 						$this->sqlTotalAdminClicks() . ', ' .
 						$this->sqlTotalEntrataClicks() . ', ' .
 						$this->sqlAdminMinutesActive() . ', ' .
 						$this->sqlInternalEmailsSent() . ', ' .
 						$this->sqlExternalEmailsSent() . ', ' .
 						$this->sqlInternalEmailsReceived() . ', ' .
 						$this->sqlInteralChats() . ', ' .
 						$this->sqlExternalChats() . ', ' .
 						$this->sqlChatCharacters() . ', ' .
 						$this->sqlWorkWebPageVisits() . ', ' .
 						$this->sqlBreakWebPageVisits() . ', ' .
 						$this->sqlPhoneCalls() . ', ' .
 						$this->sqlPhoneCallMinutes() . ', ' .
 						$this->sqlWebConferenceMinutes() . ', ' .
 						$this->sqlMinutesClockedIn() . ', ' .
 						$this->sqlNewGoogleDocs() . ', ' .
 						$this->sqlGoogleDocEdits() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

			$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlEmployeeId() ) != $this->getOriginalValueByFieldName ( 'employee_id' ) ) { $arrstrOriginalValueChanges['employee_id'] = $this->sqlEmployeeId(); $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hour = ' . $this->sqlHour() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlHour() ) != $this->getOriginalValueByFieldName ( 'hour' ) ) { $arrstrOriginalValueChanges['hour'] = $this->sqlHour(); $strSql .= ' hour = ' . $this->sqlHour() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_admin_activity = ' . $this->sqlHasAdminActivity() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlHasAdminActivity() ) != $this->getOriginalValueByFieldName ( 'has_admin_activity' ) ) { $arrstrOriginalValueChanges['has_admin_activity'] = $this->sqlHasAdminActivity(); $strSql .= ' has_admin_activity = ' . $this->sqlHasAdminActivity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_admin_clicks = ' . $this->sqlTotalAdminClicks() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTotalAdminClicks() ) != $this->getOriginalValueByFieldName ( 'total_admin_clicks' ) ) { $arrstrOriginalValueChanges['total_admin_clicks'] = $this->sqlTotalAdminClicks(); $strSql .= ' total_admin_clicks = ' . $this->sqlTotalAdminClicks() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_entrata_clicks = ' . $this->sqlTotalEntrataClicks() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlTotalEntrataClicks() ) != $this->getOriginalValueByFieldName ( 'total_entrata_clicks' ) ) { $arrstrOriginalValueChanges['total_entrata_clicks'] = $this->sqlTotalEntrataClicks(); $strSql .= ' total_entrata_clicks = ' . $this->sqlTotalEntrataClicks() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' admin_minutes_active = ' . $this->sqlAdminMinutesActive() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlAdminMinutesActive() ) != $this->getOriginalValueByFieldName ( 'admin_minutes_active' ) ) { $arrstrOriginalValueChanges['admin_minutes_active'] = $this->sqlAdminMinutesActive(); $strSql .= ' admin_minutes_active = ' . $this->sqlAdminMinutesActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_emails_sent = ' . $this->sqlInternalEmailsSent() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlInternalEmailsSent() ) != $this->getOriginalValueByFieldName ( 'internal_emails_sent' ) ) { $arrstrOriginalValueChanges['internal_emails_sent'] = $this->sqlInternalEmailsSent(); $strSql .= ' internal_emails_sent = ' . $this->sqlInternalEmailsSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_emails_sent = ' . $this->sqlExternalEmailsSent() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlExternalEmailsSent() ) != $this->getOriginalValueByFieldName ( 'external_emails_sent' ) ) { $arrstrOriginalValueChanges['external_emails_sent'] = $this->sqlExternalEmailsSent(); $strSql .= ' external_emails_sent = ' . $this->sqlExternalEmailsSent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_emails_received = ' . $this->sqlInternalEmailsReceived() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlInternalEmailsReceived() ) != $this->getOriginalValueByFieldName ( 'internal_emails_received' ) ) { $arrstrOriginalValueChanges['internal_emails_received'] = $this->sqlInternalEmailsReceived(); $strSql .= ' internal_emails_received = ' . $this->sqlInternalEmailsReceived() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interal_chats = ' . $this->sqlInteralChats() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlInteralChats() ) != $this->getOriginalValueByFieldName ( 'interal_chats' ) ) { $arrstrOriginalValueChanges['interal_chats'] = $this->sqlInteralChats(); $strSql .= ' interal_chats = ' . $this->sqlInteralChats() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_chats = ' . $this->sqlExternalChats() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlExternalChats() ) != $this->getOriginalValueByFieldName ( 'external_chats' ) ) { $arrstrOriginalValueChanges['external_chats'] = $this->sqlExternalChats(); $strSql .= ' external_chats = ' . $this->sqlExternalChats() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chat_characters = ' . $this->sqlChatCharacters() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlChatCharacters() ) != $this->getOriginalValueByFieldName ( 'chat_characters' ) ) { $arrstrOriginalValueChanges['chat_characters'] = $this->sqlChatCharacters(); $strSql .= ' chat_characters = ' . $this->sqlChatCharacters() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' work_web_page_visits = ' . $this->sqlWorkWebPageVisits() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlWorkWebPageVisits() ) != $this->getOriginalValueByFieldName ( 'work_web_page_visits' ) ) { $arrstrOriginalValueChanges['work_web_page_visits'] = $this->sqlWorkWebPageVisits(); $strSql .= ' work_web_page_visits = ' . $this->sqlWorkWebPageVisits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' break_web_page_visits = ' . $this->sqlBreakWebPageVisits() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlBreakWebPageVisits() ) != $this->getOriginalValueByFieldName ( 'break_web_page_visits' ) ) { $arrstrOriginalValueChanges['break_web_page_visits'] = $this->sqlBreakWebPageVisits(); $strSql .= ' break_web_page_visits = ' . $this->sqlBreakWebPageVisits() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_calls = ' . $this->sqlPhoneCalls() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPhoneCalls() ) != $this->getOriginalValueByFieldName ( 'phone_calls' ) ) { $arrstrOriginalValueChanges['phone_calls'] = $this->sqlPhoneCalls(); $strSql .= ' phone_calls = ' . $this->sqlPhoneCalls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_call_minutes = ' . $this->sqlPhoneCallMinutes() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPhoneCallMinutes() ) != $this->getOriginalValueByFieldName ( 'phone_call_minutes' ) ) { $arrstrOriginalValueChanges['phone_call_minutes'] = $this->sqlPhoneCallMinutes(); $strSql .= ' phone_call_minutes = ' . $this->sqlPhoneCallMinutes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' web_conference_minutes = ' . $this->sqlWebConferenceMinutes() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlWebConferenceMinutes() ) != $this->getOriginalValueByFieldName ( 'web_conference_minutes' ) ) { $arrstrOriginalValueChanges['web_conference_minutes'] = $this->sqlWebConferenceMinutes(); $strSql .= ' web_conference_minutes = ' . $this->sqlWebConferenceMinutes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minutes_clocked_in = ' . $this->sqlMinutesClockedIn() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlMinutesClockedIn() ) != $this->getOriginalValueByFieldName ( 'minutes_clocked_in' ) ) { $arrstrOriginalValueChanges['minutes_clocked_in'] = $this->sqlMinutesClockedIn(); $strSql .= ' minutes_clocked_in = ' . $this->sqlMinutesClockedIn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_google_docs = ' . $this->sqlNewGoogleDocs() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlNewGoogleDocs() ) != $this->getOriginalValueByFieldName ( 'new_google_docs' ) ) { $arrstrOriginalValueChanges['new_google_docs'] = $this->sqlNewGoogleDocs(); $strSql .= ' new_google_docs = ' . $this->sqlNewGoogleDocs() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' google_doc_edits = ' . $this->sqlGoogleDocEdits() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlGoogleDocEdits() ) != $this->getOriginalValueByFieldName ( 'google_doc_edits' ) ) { $arrstrOriginalValueChanges['google_doc_edits'] = $this->sqlGoogleDocEdits(); $strSql .= ' google_doc_edits = ' . $this->sqlGoogleDocEdits() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'hour' => $this->getHour(),
			'has_admin_activity' => $this->getHasAdminActivity(),
			'total_admin_clicks' => $this->getTotalAdminClicks(),
			'total_entrata_clicks' => $this->getTotalEntrataClicks(),
			'admin_minutes_active' => $this->getAdminMinutesActive(),
			'internal_emails_sent' => $this->getInternalEmailsSent(),
			'external_emails_sent' => $this->getExternalEmailsSent(),
			'internal_emails_received' => $this->getInternalEmailsReceived(),
			'interal_chats' => $this->getInteralChats(),
			'external_chats' => $this->getExternalChats(),
			'chat_characters' => $this->getChatCharacters(),
			'work_web_page_visits' => $this->getWorkWebPageVisits(),
			'break_web_page_visits' => $this->getBreakWebPageVisits(),
			'phone_calls' => $this->getPhoneCalls(),
			'phone_call_minutes' => $this->getPhoneCallMinutes(),
			'web_conference_minutes' => $this->getWebConferenceMinutes(),
			'minutes_clocked_in' => $this->getMinutesClockedIn(),
			'new_google_docs' => $this->getNewGoogleDocs(),
			'google_doc_edits' => $this->getGoogleDocEdits(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>