<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTransportRoutePickupAssociations
 * Do not add any new functions to this class.
 */

class CBaseTransportRoutePickupAssociations extends CEosPluralBase {

	/**
	 * @return CTransportRoutePickupAssociation[]
	 */
	public static function fetchTransportRoutePickupAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTransportRoutePickupAssociation', $objDatabase );
	}

	/**
	 * @return CTransportRoutePickupAssociation
	 */
	public static function fetchTransportRoutePickupAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTransportRoutePickupAssociation', $objDatabase );
	}

	public static function fetchTransportRoutePickupAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transport_route_pickup_associations', $objDatabase );
	}

	public static function fetchTransportRoutePickupAssociationById( $intId, $objDatabase ) {
		return self::fetchTransportRoutePickupAssociation( sprintf( 'SELECT * FROM transport_route_pickup_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTransportRoutePickupAssociationsByTransportRouteId( $intTransportRouteId, $objDatabase ) {
		return self::fetchTransportRoutePickupAssociations( sprintf( 'SELECT * FROM transport_route_pickup_associations WHERE transport_route_id = %d', ( int ) $intTransportRouteId ), $objDatabase );
	}

	public static function fetchTransportRoutePickupAssociationsByTransportPickupPointId( $intTransportPickupPointId, $objDatabase ) {
		return self::fetchTransportRoutePickupAssociations( sprintf( 'SELECT * FROM transport_route_pickup_associations WHERE transport_pickup_point_id = %d', ( int ) $intTransportPickupPointId ), $objDatabase );
	}

}
?>