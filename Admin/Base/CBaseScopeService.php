<?php

class CBaseScopeService extends CEosSingularBase {

	const TABLE_NAME = 'public.scope_services';

	protected $m_intId;
	protected $m_intScopeId;
	protected $m_intServiceId;
	protected $m_intMinorApiVersionId;
	protected $m_intOrderNum;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['scope_id'] ) && $boolDirectSet ) $this->set( 'm_intScopeId', trim( $arrValues['scope_id'] ) ); elseif( isset( $arrValues['scope_id'] ) ) $this->setScopeId( $arrValues['scope_id'] );
		if( isset( $arrValues['service_id'] ) && $boolDirectSet ) $this->set( 'm_intServiceId', trim( $arrValues['service_id'] ) ); elseif( isset( $arrValues['service_id'] ) ) $this->setServiceId( $arrValues['service_id'] );
		if( isset( $arrValues['minor_api_version_id'] ) && $boolDirectSet ) $this->set( 'm_intMinorApiVersionId', trim( $arrValues['minor_api_version_id'] ) ); elseif( isset( $arrValues['minor_api_version_id'] ) ) $this->setMinorApiVersionId( $arrValues['minor_api_version_id'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setScopeId( $intScopeId ) {
		$this->set( 'm_intScopeId', CStrings::strToIntDef( $intScopeId, NULL, false ) );
	}

	public function getScopeId() {
		return $this->m_intScopeId;
	}

	public function sqlScopeId() {
		return ( true == isset( $this->m_intScopeId ) ) ? ( string ) $this->m_intScopeId : 'NULL';
	}

	public function setServiceId( $intServiceId ) {
		$this->set( 'm_intServiceId', CStrings::strToIntDef( $intServiceId, NULL, false ) );
	}

	public function getServiceId() {
		return $this->m_intServiceId;
	}

	public function sqlServiceId() {
		return ( true == isset( $this->m_intServiceId ) ) ? ( string ) $this->m_intServiceId : 'NULL';
	}

	public function setMinorApiVersionId( $intMinorApiVersionId ) {
		$this->set( 'm_intMinorApiVersionId', CStrings::strToIntDef( $intMinorApiVersionId, NULL, false ) );
	}

	public function getMinorApiVersionId() {
		return $this->m_intMinorApiVersionId;
	}

	public function sqlMinorApiVersionId() {
		return ( true == isset( $this->m_intMinorApiVersionId ) ) ? ( string ) $this->m_intMinorApiVersionId : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, scope_id, service_id, minor_api_version_id, order_num, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlScopeId() . ', ' .
 						$this->sqlServiceId() . ', ' .
 						$this->sqlMinorApiVersionId() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scope_id = ' . $this->sqlScopeId() . ','; } elseif( true == array_key_exists( 'ScopeId', $this->getChangedColumns() ) ) { $strSql .= ' scope_id = ' . $this->sqlScopeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_id = ' . $this->sqlServiceId() . ','; } elseif( true == array_key_exists( 'ServiceId', $this->getChangedColumns() ) ) { $strSql .= ' service_id = ' . $this->sqlServiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minor_api_version_id = ' . $this->sqlMinorApiVersionId() . ','; } elseif( true == array_key_exists( 'MinorApiVersionId', $this->getChangedColumns() ) ) { $strSql .= ' minor_api_version_id = ' . $this->sqlMinorApiVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'scope_id' => $this->getScopeId(),
			'service_id' => $this->getServiceId(),
			'minor_api_version_id' => $this->getMinorApiVersionId(),
			'order_num' => $this->getOrderNum(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>