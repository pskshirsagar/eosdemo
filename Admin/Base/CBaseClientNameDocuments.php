<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CClientNameDocuments
 * Do not add any new functions to this class.
 */

class CBaseClientNameDocuments extends CEosPluralBase {

	/**
	 * @return CClientNameDocument[]
	 */
	public static function fetchClientNameDocuments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CClientNameDocument', $objDatabase );
	}

	/**
	 * @return CClientNameDocument
	 */
	public static function fetchClientNameDocument( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CClientNameDocument', $objDatabase );
	}

	public static function fetchClientNameDocumentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'client_name_documents', $objDatabase );
	}

	public static function fetchClientNameDocumentById( $intId, $objDatabase ) {
		return self::fetchClientNameDocument( sprintf( 'SELECT * FROM client_name_documents WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchClientNameDocumentsByCid( $intCid, $objDatabase ) {
		return self::fetchClientNameDocuments( sprintf( 'SELECT * FROM client_name_documents WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchClientNameDocumentsByClientNameId( $intClientNameId, $objDatabase ) {
		return self::fetchClientNameDocuments( sprintf( 'SELECT * FROM client_name_documents WHERE client_name_id = %d', ( int ) $intClientNameId ), $objDatabase );
	}

	public static function fetchClientNameDocumentsByPsDocumentId( $intPsDocumentId, $objDatabase ) {
		return self::fetchClientNameDocuments( sprintf( 'SELECT * FROM client_name_documents WHERE ps_document_id = %d', ( int ) $intPsDocumentId ), $objDatabase );
	}

}
?>