<?php

class CBaseSalesTaxCategory extends CEosSingularBase {

	const TABLE_NAME = 'public.sales_tax_categories';

	protected $m_intId;
	protected $m_strSkuCode;
	protected $m_strDescription;
	protected $m_strCchGroup;
	protected $m_strCchItem;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['sku_code'] ) && $boolDirectSet ) $this->set( 'm_strSkuCode', trim( stripcslashes( $arrValues['sku_code'] ) ) ); elseif( isset( $arrValues['sku_code'] ) ) $this->setSkuCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sku_code'] ) : $arrValues['sku_code'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['cch_group'] ) && $boolDirectSet ) $this->set( 'm_strCchGroup', trim( stripcslashes( $arrValues['cch_group'] ) ) ); elseif( isset( $arrValues['cch_group'] ) ) $this->setCchGroup( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cch_group'] ) : $arrValues['cch_group'] );
		if( isset( $arrValues['cch_item'] ) && $boolDirectSet ) $this->set( 'm_strCchItem', trim( stripcslashes( $arrValues['cch_item'] ) ) ); elseif( isset( $arrValues['cch_item'] ) ) $this->setCchItem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cch_item'] ) : $arrValues['cch_item'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSkuCode( $strSkuCode ) {
		$this->set( 'm_strSkuCode', CStrings::strTrimDef( $strSkuCode, 10, NULL, true ) );
	}

	public function getSkuCode() {
		return $this->m_strSkuCode;
	}

	public function sqlSkuCode() {
		return ( true == isset( $this->m_strSkuCode ) ) ? '\'' . addslashes( $this->m_strSkuCode ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 100, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setCchGroup( $strCchGroup ) {
		$this->set( 'm_strCchGroup', CStrings::strTrimDef( $strCchGroup, 100, NULL, true ) );
	}

	public function getCchGroup() {
		return $this->m_strCchGroup;
	}

	public function sqlCchGroup() {
		return ( true == isset( $this->m_strCchGroup ) ) ? '\'' . addslashes( $this->m_strCchGroup ) . '\'' : 'NULL';
	}

	public function setCchItem( $strCchItem ) {
		$this->set( 'm_strCchItem', CStrings::strTrimDef( $strCchItem, 100, NULL, true ) );
	}

	public function getCchItem() {
		return $this->m_strCchItem;
	}

	public function sqlCchItem() {
		return ( true == isset( $this->m_strCchItem ) ) ? '\'' . addslashes( $this->m_strCchItem ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, sku_code, description, cch_group, cch_item, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSkuCode() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlCchGroup() . ', ' .
 						$this->sqlCchItem() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sku_code = ' . $this->sqlSkuCode() . ','; } elseif( true == array_key_exists( 'SkuCode', $this->getChangedColumns() ) ) { $strSql .= ' sku_code = ' . $this->sqlSkuCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cch_group = ' . $this->sqlCchGroup() . ','; } elseif( true == array_key_exists( 'CchGroup', $this->getChangedColumns() ) ) { $strSql .= ' cch_group = ' . $this->sqlCchGroup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cch_item = ' . $this->sqlCchItem() . ','; } elseif( true == array_key_exists( 'CchItem', $this->getChangedColumns() ) ) { $strSql .= ' cch_item = ' . $this->sqlCchItem() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'sku_code' => $this->getSkuCode(),
			'description' => $this->getDescription(),
			'cch_group' => $this->getCchGroup(),
			'cch_item' => $this->getCchItem(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>