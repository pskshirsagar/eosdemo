<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsTotals
 * Do not add any new functions to this class.
 */

class CBaseStatsTotals extends CEosPluralBase {

	/**
	 * @return CStatsTotal[]
	 */
	public static function fetchStatsTotals( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CStatsTotal::class, $objDatabase );
	}

	/**
	 * @return CStatsTotal
	 */
	public static function fetchStatsTotal( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStatsTotal::class, $objDatabase );
	}

	public static function fetchStatsTotalCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stats_totals', $objDatabase );
	}

	public static function fetchStatsTotalById( $intId, $objDatabase ) {
		return self::fetchStatsTotal( sprintf( 'SELECT * FROM stats_totals WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>