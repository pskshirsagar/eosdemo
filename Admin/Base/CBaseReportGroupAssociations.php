<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportGroupAssociations
 * Do not add any new functions to this class.
 */

class CBaseReportGroupAssociations extends CEosPluralBase {

	/**
	 * @return CReportGroupAssociation[]
	 */
	public static function fetchReportGroupAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportGroupAssociation::class, $objDatabase );
	}

	/**
	 * @return CReportGroupAssociation
	 */
	public static function fetchReportGroupAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportGroupAssociation::class, $objDatabase );
	}

	public static function fetchReportGroupAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_group_associations', $objDatabase );
	}

	public static function fetchReportGroupAssociationById( $intId, $objDatabase ) {
		return self::fetchReportGroupAssociation( sprintf( 'SELECT * FROM report_group_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportGroupAssociationsByCompanyReportId( $intCompanyReportId, $objDatabase ) {
		return self::fetchReportGroupAssociations( sprintf( 'SELECT * FROM report_group_associations WHERE company_report_id = %d', ( int ) $intCompanyReportId ), $objDatabase );
	}

	public static function fetchReportGroupAssociationsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchReportGroupAssociations( sprintf( 'SELECT * FROM report_group_associations WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

}
?>