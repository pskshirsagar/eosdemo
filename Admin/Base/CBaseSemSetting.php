<?php

class CBaseSemSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.sem_settings';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intSemAdGroupId;
	protected $m_intPropertyId;
	protected $m_intFrequencyId;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_fltAdBudget;
	protected $m_fltOrganicFeatureFee;
	protected $m_fltInternalAdMarkup;
	protected $m_fltExternalAdMarkup;
	protected $m_intPaymentFailures;
	protected $m_strLastPostedOn;
	protected $m_strLastBilledOn;
	protected $m_strOrganicActivatedOn;
	protected $m_strOrganicDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intFrequencyId = '4';
		$this->m_fltAdBudget = '0';
		$this->m_fltOrganicFeatureFee = '0';
		$this->m_fltInternalAdMarkup = '1.12';
		$this->m_fltExternalAdMarkup = '1.12';
		$this->m_intPaymentFailures = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['sem_ad_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdGroupId', trim( $arrValues['sem_ad_group_id'] ) ); elseif( isset( $arrValues['sem_ad_group_id'] ) ) $this->setSemAdGroupId( $arrValues['sem_ad_group_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['frequency_id'] ) && $boolDirectSet ) $this->set( 'm_intFrequencyId', trim( $arrValues['frequency_id'] ) ); elseif( isset( $arrValues['frequency_id'] ) ) $this->setFrequencyId( $arrValues['frequency_id'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['ad_budget'] ) && $boolDirectSet ) $this->set( 'm_fltAdBudget', trim( $arrValues['ad_budget'] ) ); elseif( isset( $arrValues['ad_budget'] ) ) $this->setAdBudget( $arrValues['ad_budget'] );
		if( isset( $arrValues['organic_feature_fee'] ) && $boolDirectSet ) $this->set( 'm_fltOrganicFeatureFee', trim( $arrValues['organic_feature_fee'] ) ); elseif( isset( $arrValues['organic_feature_fee'] ) ) $this->setOrganicFeatureFee( $arrValues['organic_feature_fee'] );
		if( isset( $arrValues['internal_ad_markup'] ) && $boolDirectSet ) $this->set( 'm_fltInternalAdMarkup', trim( $arrValues['internal_ad_markup'] ) ); elseif( isset( $arrValues['internal_ad_markup'] ) ) $this->setInternalAdMarkup( $arrValues['internal_ad_markup'] );
		if( isset( $arrValues['external_ad_markup'] ) && $boolDirectSet ) $this->set( 'm_fltExternalAdMarkup', trim( $arrValues['external_ad_markup'] ) ); elseif( isset( $arrValues['external_ad_markup'] ) ) $this->setExternalAdMarkup( $arrValues['external_ad_markup'] );
		if( isset( $arrValues['payment_failures'] ) && $boolDirectSet ) $this->set( 'm_intPaymentFailures', trim( $arrValues['payment_failures'] ) ); elseif( isset( $arrValues['payment_failures'] ) ) $this->setPaymentFailures( $arrValues['payment_failures'] );
		if( isset( $arrValues['last_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedOn', trim( $arrValues['last_posted_on'] ) ); elseif( isset( $arrValues['last_posted_on'] ) ) $this->setLastPostedOn( $arrValues['last_posted_on'] );
		if( isset( $arrValues['last_billed_on'] ) && $boolDirectSet ) $this->set( 'm_strLastBilledOn', trim( $arrValues['last_billed_on'] ) ); elseif( isset( $arrValues['last_billed_on'] ) ) $this->setLastBilledOn( $arrValues['last_billed_on'] );
		if( isset( $arrValues['organic_activated_on'] ) && $boolDirectSet ) $this->set( 'm_strOrganicActivatedOn', trim( $arrValues['organic_activated_on'] ) ); elseif( isset( $arrValues['organic_activated_on'] ) ) $this->setOrganicActivatedOn( $arrValues['organic_activated_on'] );
		if( isset( $arrValues['organic_disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strOrganicDisabledOn', trim( $arrValues['organic_disabled_on'] ) ); elseif( isset( $arrValues['organic_disabled_on'] ) ) $this->setOrganicDisabledOn( $arrValues['organic_disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setSemAdGroupId( $intSemAdGroupId ) {
		$this->set( 'm_intSemAdGroupId', CStrings::strToIntDef( $intSemAdGroupId, NULL, false ) );
	}

	public function getSemAdGroupId() {
		return $this->m_intSemAdGroupId;
	}

	public function sqlSemAdGroupId() {
		return ( true == isset( $this->m_intSemAdGroupId ) ) ? ( string ) $this->m_intSemAdGroupId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setFrequencyId( $intFrequencyId ) {
		$this->set( 'm_intFrequencyId', CStrings::strToIntDef( $intFrequencyId, NULL, false ) );
	}

	public function getFrequencyId() {
		return $this->m_intFrequencyId;
	}

	public function sqlFrequencyId() {
		return ( true == isset( $this->m_intFrequencyId ) ) ? ( string ) $this->m_intFrequencyId : '4';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setAdBudget( $fltAdBudget ) {
		$this->set( 'm_fltAdBudget', CStrings::strToFloatDef( $fltAdBudget, NULL, false, 2 ) );
	}

	public function getAdBudget() {
		return $this->m_fltAdBudget;
	}

	public function sqlAdBudget() {
		return ( true == isset( $this->m_fltAdBudget ) ) ? ( string ) $this->m_fltAdBudget : '0';
	}

	public function setOrganicFeatureFee( $fltOrganicFeatureFee ) {
		$this->set( 'm_fltOrganicFeatureFee', CStrings::strToFloatDef( $fltOrganicFeatureFee, NULL, false, 2 ) );
	}

	public function getOrganicFeatureFee() {
		return $this->m_fltOrganicFeatureFee;
	}

	public function sqlOrganicFeatureFee() {
		return ( true == isset( $this->m_fltOrganicFeatureFee ) ) ? ( string ) $this->m_fltOrganicFeatureFee : '0';
	}

	public function setInternalAdMarkup( $fltInternalAdMarkup ) {
		$this->set( 'm_fltInternalAdMarkup', CStrings::strToFloatDef( $fltInternalAdMarkup, NULL, false, 6 ) );
	}

	public function getInternalAdMarkup() {
		return $this->m_fltInternalAdMarkup;
	}

	public function sqlInternalAdMarkup() {
		return ( true == isset( $this->m_fltInternalAdMarkup ) ) ? ( string ) $this->m_fltInternalAdMarkup : '1.12';
	}

	public function setExternalAdMarkup( $fltExternalAdMarkup ) {
		$this->set( 'm_fltExternalAdMarkup', CStrings::strToFloatDef( $fltExternalAdMarkup, NULL, false, 6 ) );
	}

	public function getExternalAdMarkup() {
		return $this->m_fltExternalAdMarkup;
	}

	public function sqlExternalAdMarkup() {
		return ( true == isset( $this->m_fltExternalAdMarkup ) ) ? ( string ) $this->m_fltExternalAdMarkup : '1.12';
	}

	public function setPaymentFailures( $intPaymentFailures ) {
		$this->set( 'm_intPaymentFailures', CStrings::strToIntDef( $intPaymentFailures, NULL, false ) );
	}

	public function getPaymentFailures() {
		return $this->m_intPaymentFailures;
	}

	public function sqlPaymentFailures() {
		return ( true == isset( $this->m_intPaymentFailures ) ) ? ( string ) $this->m_intPaymentFailures : '0';
	}

	public function setLastPostedOn( $strLastPostedOn ) {
		$this->set( 'm_strLastPostedOn', CStrings::strTrimDef( $strLastPostedOn, -1, NULL, true ) );
	}

	public function getLastPostedOn() {
		return $this->m_strLastPostedOn;
	}

	public function sqlLastPostedOn() {
		return ( true == isset( $this->m_strLastPostedOn ) ) ? '\'' . $this->m_strLastPostedOn . '\'' : 'NULL';
	}

	public function setLastBilledOn( $strLastBilledOn ) {
		$this->set( 'm_strLastBilledOn', CStrings::strTrimDef( $strLastBilledOn, -1, NULL, true ) );
	}

	public function getLastBilledOn() {
		return $this->m_strLastBilledOn;
	}

	public function sqlLastBilledOn() {
		return ( true == isset( $this->m_strLastBilledOn ) ) ? '\'' . $this->m_strLastBilledOn . '\'' : 'NULL';
	}

	public function setOrganicActivatedOn( $strOrganicActivatedOn ) {
		$this->set( 'm_strOrganicActivatedOn', CStrings::strTrimDef( $strOrganicActivatedOn, -1, NULL, true ) );
	}

	public function getOrganicActivatedOn() {
		return $this->m_strOrganicActivatedOn;
	}

	public function sqlOrganicActivatedOn() {
		return ( true == isset( $this->m_strOrganicActivatedOn ) ) ? '\'' . $this->m_strOrganicActivatedOn . '\'' : 'NULL';
	}

	public function setOrganicDisabledOn( $strOrganicDisabledOn ) {
		$this->set( 'm_strOrganicDisabledOn', CStrings::strTrimDef( $strOrganicDisabledOn, -1, NULL, true ) );
	}

	public function getOrganicDisabledOn() {
		return $this->m_strOrganicDisabledOn;
	}

	public function sqlOrganicDisabledOn() {
		return ( true == isset( $this->m_strOrganicDisabledOn ) ) ? '\'' . $this->m_strOrganicDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, sem_ad_group_id, property_id, frequency_id, start_date, end_date, ad_budget, organic_feature_fee, internal_ad_markup, external_ad_markup, payment_failures, last_posted_on, last_billed_on, organic_activated_on, organic_disabled_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlAccountId() . ', ' .
 						$this->sqlSemAdGroupId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlFrequencyId() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlAdBudget() . ', ' .
 						$this->sqlOrganicFeatureFee() . ', ' .
 						$this->sqlInternalAdMarkup() . ', ' .
 						$this->sqlExternalAdMarkup() . ', ' .
 						$this->sqlPaymentFailures() . ', ' .
 						$this->sqlLastPostedOn() . ', ' .
 						$this->sqlLastBilledOn() . ', ' .
 						$this->sqlOrganicActivatedOn() . ', ' .
 						$this->sqlOrganicDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; } elseif( true == array_key_exists( 'SemAdGroupId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; } elseif( true == array_key_exists( 'FrequencyId', $this->getChangedColumns() ) ) { $strSql .= ' frequency_id = ' . $this->sqlFrequencyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ad_budget = ' . $this->sqlAdBudget() . ','; } elseif( true == array_key_exists( 'AdBudget', $this->getChangedColumns() ) ) { $strSql .= ' ad_budget = ' . $this->sqlAdBudget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organic_feature_fee = ' . $this->sqlOrganicFeatureFee() . ','; } elseif( true == array_key_exists( 'OrganicFeatureFee', $this->getChangedColumns() ) ) { $strSql .= ' organic_feature_fee = ' . $this->sqlOrganicFeatureFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_ad_markup = ' . $this->sqlInternalAdMarkup() . ','; } elseif( true == array_key_exists( 'InternalAdMarkup', $this->getChangedColumns() ) ) { $strSql .= ' internal_ad_markup = ' . $this->sqlInternalAdMarkup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' external_ad_markup = ' . $this->sqlExternalAdMarkup() . ','; } elseif( true == array_key_exists( 'ExternalAdMarkup', $this->getChangedColumns() ) ) { $strSql .= ' external_ad_markup = ' . $this->sqlExternalAdMarkup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payment_failures = ' . $this->sqlPaymentFailures() . ','; } elseif( true == array_key_exists( 'PaymentFailures', $this->getChangedColumns() ) ) { $strSql .= ' payment_failures = ' . $this->sqlPaymentFailures() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; } elseif( true == array_key_exists( 'LastPostedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_billed_on = ' . $this->sqlLastBilledOn() . ','; } elseif( true == array_key_exists( 'LastBilledOn', $this->getChangedColumns() ) ) { $strSql .= ' last_billed_on = ' . $this->sqlLastBilledOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organic_activated_on = ' . $this->sqlOrganicActivatedOn() . ','; } elseif( true == array_key_exists( 'OrganicActivatedOn', $this->getChangedColumns() ) ) { $strSql .= ' organic_activated_on = ' . $this->sqlOrganicActivatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' organic_disabled_on = ' . $this->sqlOrganicDisabledOn() . ','; } elseif( true == array_key_exists( 'OrganicDisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' organic_disabled_on = ' . $this->sqlOrganicDisabledOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'sem_ad_group_id' => $this->getSemAdGroupId(),
			'property_id' => $this->getPropertyId(),
			'frequency_id' => $this->getFrequencyId(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'ad_budget' => $this->getAdBudget(),
			'organic_feature_fee' => $this->getOrganicFeatureFee(),
			'internal_ad_markup' => $this->getInternalAdMarkup(),
			'external_ad_markup' => $this->getExternalAdMarkup(),
			'payment_failures' => $this->getPaymentFailures(),
			'last_posted_on' => $this->getLastPostedOn(),
			'last_billed_on' => $this->getLastBilledOn(),
			'organic_activated_on' => $this->getOrganicActivatedOn(),
			'organic_disabled_on' => $this->getOrganicDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>