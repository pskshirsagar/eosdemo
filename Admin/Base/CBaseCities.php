<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCities
 * Do not add any new functions to this class.
 */

class CBaseCities extends CEosPluralBase {

	/**
	 * @return CCity[]
	 */
	public static function fetchCities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCity', $objDatabase );
	}

	/**
	 * @return CCity
	 */
	public static function fetchCity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCity', $objDatabase );
	}

	public static function fetchCityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'cities', $objDatabase );
	}

	public static function fetchCityById( $intId, $objDatabase ) {
		return self::fetchCity( sprintf( 'SELECT * FROM cities WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>