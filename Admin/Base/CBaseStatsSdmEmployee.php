<?php

class CBaseStatsSdmEmployee extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_sdm_employees';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intPsProductId;
	protected $m_strWeek;
	protected $m_fltTicketAverageAgingBug;
	protected $m_fltTicketAverageAgingFeature;
	protected $m_fltTicketAverageAgingSupport;
	protected $m_intTicketsOpenBug;
	protected $m_intTicketsOpenFeature;
	protected $m_intTicketsOpenSupport;
	protected $m_intModulesAccessed;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltTicketAverageAgingBug = '0';
		$this->m_fltTicketAverageAgingFeature = '0';
		$this->m_fltTicketAverageAgingSupport = '0';
		$this->m_intTicketsOpenBug = '0';
		$this->m_intTicketsOpenFeature = '0';
		$this->m_intTicketsOpenSupport = '0';
		$this->m_intModulesAccessed = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['week'] ) && $boolDirectSet ) $this->set( 'm_strWeek', trim( $arrValues['week'] ) ); elseif( isset( $arrValues['week'] ) ) $this->setWeek( $arrValues['week'] );
		if( isset( $arrValues['ticket_average_aging_bug'] ) && $boolDirectSet ) $this->set( 'm_fltTicketAverageAgingBug', trim( $arrValues['ticket_average_aging_bug'] ) ); elseif( isset( $arrValues['ticket_average_aging_bug'] ) ) $this->setTicketAverageAgingBug( $arrValues['ticket_average_aging_bug'] );
		if( isset( $arrValues['ticket_average_aging_feature'] ) && $boolDirectSet ) $this->set( 'm_fltTicketAverageAgingFeature', trim( $arrValues['ticket_average_aging_feature'] ) ); elseif( isset( $arrValues['ticket_average_aging_feature'] ) ) $this->setTicketAverageAgingFeature( $arrValues['ticket_average_aging_feature'] );
		if( isset( $arrValues['ticket_average_aging_support'] ) && $boolDirectSet ) $this->set( 'm_fltTicketAverageAgingSupport', trim( $arrValues['ticket_average_aging_support'] ) ); elseif( isset( $arrValues['ticket_average_aging_support'] ) ) $this->setTicketAverageAgingSupport( $arrValues['ticket_average_aging_support'] );
		if( isset( $arrValues['tickets_open_bug'] ) && $boolDirectSet ) $this->set( 'm_intTicketsOpenBug', trim( $arrValues['tickets_open_bug'] ) ); elseif( isset( $arrValues['tickets_open_bug'] ) ) $this->setTicketsOpenBug( $arrValues['tickets_open_bug'] );
		if( isset( $arrValues['tickets_open_feature'] ) && $boolDirectSet ) $this->set( 'm_intTicketsOpenFeature', trim( $arrValues['tickets_open_feature'] ) ); elseif( isset( $arrValues['tickets_open_feature'] ) ) $this->setTicketsOpenFeature( $arrValues['tickets_open_feature'] );
		if( isset( $arrValues['tickets_open_support'] ) && $boolDirectSet ) $this->set( 'm_intTicketsOpenSupport', trim( $arrValues['tickets_open_support'] ) ); elseif( isset( $arrValues['tickets_open_support'] ) ) $this->setTicketsOpenSupport( $arrValues['tickets_open_support'] );
		if( isset( $arrValues['modules_accessed'] ) && $boolDirectSet ) $this->set( 'm_intModulesAccessed', trim( $arrValues['modules_accessed'] ) ); elseif( isset( $arrValues['modules_accessed'] ) ) $this->setModulesAccessed( $arrValues['modules_accessed'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setWeek( $strWeek ) {
		$this->set( 'm_strWeek', CStrings::strTrimDef( $strWeek, -1, NULL, true ) );
	}

	public function getWeek() {
		return $this->m_strWeek;
	}

	public function sqlWeek() {
		return ( true == isset( $this->m_strWeek ) ) ? '\'' . $this->m_strWeek . '\'' : 'NOW()';
	}

	public function setTicketAverageAgingBug( $fltTicketAverageAgingBug ) {
		$this->set( 'm_fltTicketAverageAgingBug', CStrings::strToFloatDef( $fltTicketAverageAgingBug, NULL, false, 2 ) );
	}

	public function getTicketAverageAgingBug() {
		return $this->m_fltTicketAverageAgingBug;
	}

	public function sqlTicketAverageAgingBug() {
		return ( true == isset( $this->m_fltTicketAverageAgingBug ) ) ? ( string ) $this->m_fltTicketAverageAgingBug : '0';
	}

	public function setTicketAverageAgingFeature( $fltTicketAverageAgingFeature ) {
		$this->set( 'm_fltTicketAverageAgingFeature', CStrings::strToFloatDef( $fltTicketAverageAgingFeature, NULL, false, 2 ) );
	}

	public function getTicketAverageAgingFeature() {
		return $this->m_fltTicketAverageAgingFeature;
	}

	public function sqlTicketAverageAgingFeature() {
		return ( true == isset( $this->m_fltTicketAverageAgingFeature ) ) ? ( string ) $this->m_fltTicketAverageAgingFeature : '0';
	}

	public function setTicketAverageAgingSupport( $fltTicketAverageAgingSupport ) {
		$this->set( 'm_fltTicketAverageAgingSupport', CStrings::strToFloatDef( $fltTicketAverageAgingSupport, NULL, false, 2 ) );
	}

	public function getTicketAverageAgingSupport() {
		return $this->m_fltTicketAverageAgingSupport;
	}

	public function sqlTicketAverageAgingSupport() {
		return ( true == isset( $this->m_fltTicketAverageAgingSupport ) ) ? ( string ) $this->m_fltTicketAverageAgingSupport : '0';
	}

	public function setTicketsOpenBug( $intTicketsOpenBug ) {
		$this->set( 'm_intTicketsOpenBug', CStrings::strToIntDef( $intTicketsOpenBug, NULL, false ) );
	}

	public function getTicketsOpenBug() {
		return $this->m_intTicketsOpenBug;
	}

	public function sqlTicketsOpenBug() {
		return ( true == isset( $this->m_intTicketsOpenBug ) ) ? ( string ) $this->m_intTicketsOpenBug : '0';
	}

	public function setTicketsOpenFeature( $intTicketsOpenFeature ) {
		$this->set( 'm_intTicketsOpenFeature', CStrings::strToIntDef( $intTicketsOpenFeature, NULL, false ) );
	}

	public function getTicketsOpenFeature() {
		return $this->m_intTicketsOpenFeature;
	}

	public function sqlTicketsOpenFeature() {
		return ( true == isset( $this->m_intTicketsOpenFeature ) ) ? ( string ) $this->m_intTicketsOpenFeature : '0';
	}

	public function setTicketsOpenSupport( $intTicketsOpenSupport ) {
		$this->set( 'm_intTicketsOpenSupport', CStrings::strToIntDef( $intTicketsOpenSupport, NULL, false ) );
	}

	public function getTicketsOpenSupport() {
		return $this->m_intTicketsOpenSupport;
	}

	public function sqlTicketsOpenSupport() {
		return ( true == isset( $this->m_intTicketsOpenSupport ) ) ? ( string ) $this->m_intTicketsOpenSupport : '0';
	}

	public function setModulesAccessed( $intModulesAccessed ) {
		$this->set( 'm_intModulesAccessed', CStrings::strToIntDef( $intModulesAccessed, NULL, false ) );
	}

	public function getModulesAccessed() {
		return $this->m_intModulesAccessed;
	}

	public function sqlModulesAccessed() {
		return ( true == isset( $this->m_intModulesAccessed ) ) ? ( string ) $this->m_intModulesAccessed : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, ps_product_id, week, ticket_average_aging_bug, ticket_average_aging_feature, ticket_average_aging_support, tickets_open_bug, tickets_open_feature, tickets_open_support, modules_accessed, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlWeek() . ', ' .
 						$this->sqlTicketAverageAgingBug() . ', ' .
 						$this->sqlTicketAverageAgingFeature() . ', ' .
 						$this->sqlTicketAverageAgingSupport() . ', ' .
 						$this->sqlTicketsOpenBug() . ', ' .
 						$this->sqlTicketsOpenFeature() . ', ' .
 						$this->sqlTicketsOpenSupport() . ', ' .
 						$this->sqlModulesAccessed() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' week = ' . $this->sqlWeek() . ','; } elseif( true == array_key_exists( 'Week', $this->getChangedColumns() ) ) { $strSql .= ' week = ' . $this->sqlWeek() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ticket_average_aging_bug = ' . $this->sqlTicketAverageAgingBug() . ','; } elseif( true == array_key_exists( 'TicketAverageAgingBug', $this->getChangedColumns() ) ) { $strSql .= ' ticket_average_aging_bug = ' . $this->sqlTicketAverageAgingBug() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ticket_average_aging_feature = ' . $this->sqlTicketAverageAgingFeature() . ','; } elseif( true == array_key_exists( 'TicketAverageAgingFeature', $this->getChangedColumns() ) ) { $strSql .= ' ticket_average_aging_feature = ' . $this->sqlTicketAverageAgingFeature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ticket_average_aging_support = ' . $this->sqlTicketAverageAgingSupport() . ','; } elseif( true == array_key_exists( 'TicketAverageAgingSupport', $this->getChangedColumns() ) ) { $strSql .= ' ticket_average_aging_support = ' . $this->sqlTicketAverageAgingSupport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tickets_open_bug = ' . $this->sqlTicketsOpenBug() . ','; } elseif( true == array_key_exists( 'TicketsOpenBug', $this->getChangedColumns() ) ) { $strSql .= ' tickets_open_bug = ' . $this->sqlTicketsOpenBug() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tickets_open_feature = ' . $this->sqlTicketsOpenFeature() . ','; } elseif( true == array_key_exists( 'TicketsOpenFeature', $this->getChangedColumns() ) ) { $strSql .= ' tickets_open_feature = ' . $this->sqlTicketsOpenFeature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tickets_open_support = ' . $this->sqlTicketsOpenSupport() . ','; } elseif( true == array_key_exists( 'TicketsOpenSupport', $this->getChangedColumns() ) ) { $strSql .= ' tickets_open_support = ' . $this->sqlTicketsOpenSupport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' modules_accessed = ' . $this->sqlModulesAccessed() . ','; } elseif( true == array_key_exists( 'ModulesAccessed', $this->getChangedColumns() ) ) { $strSql .= ' modules_accessed = ' . $this->sqlModulesAccessed() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'ps_product_id' => $this->getPsProductId(),
			'week' => $this->getWeek(),
			'ticket_average_aging_bug' => $this->getTicketAverageAgingBug(),
			'ticket_average_aging_feature' => $this->getTicketAverageAgingFeature(),
			'ticket_average_aging_support' => $this->getTicketAverageAgingSupport(),
			'tickets_open_bug' => $this->getTicketsOpenBug(),
			'tickets_open_feature' => $this->getTicketsOpenFeature(),
			'tickets_open_support' => $this->getTicketsOpenSupport(),
			'modules_accessed' => $this->getModulesAccessed(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>