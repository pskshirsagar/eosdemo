<?php

class CBaseStatsContract extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_contracts';

	protected $m_intId;
	protected $m_intContractId;
	protected $m_intPsLeadId;
	protected $m_intCid;
	protected $m_intPsProductId;
	protected $m_intPropertyId;
	protected $m_intContractProductId;
	protected $m_intContractPropertyId;
	protected $m_intRenewalContractPropertyId;
	protected $m_intOriginalContractPropertyId;
	protected $m_intContractTypeId;
	protected $m_intContractStatusTypeId;
	protected $m_intCompanyStatusTypeId;
	protected $m_intCommissionBucketId;
	protected $m_intContractTerminationRequestId;
	protected $m_strCurrencyCode;
	protected $m_strCompanyName;
	protected $m_strProductName;
	protected $m_strPropertyName;
	protected $m_strPropertyType;
	protected $m_intPropertyUnitCount;
	protected $m_intContractProductUnitCount;
	protected $m_intContractProductSubscriptionAcv;
	protected $m_intContractProductTransactionAcv;
	protected $m_strClientStartDate;
	protected $m_strClientLastActiveDate;
	protected $m_intClientIsActive;
	protected $m_strClientProductStartDate;
	protected $m_strClientProductLastActiveDate;
	protected $m_intClientProductIsActive;
	protected $m_strClientPropertyStartDate;
	protected $m_strClientPropertyLastActiveDate;
	protected $m_intClientPropertyIsActive;
	protected $m_strClientProductPropertyStartDate;
	protected $m_strClientProductPropertyLastActiveDate;
	protected $m_intClientProductPropertyIsActive;
	protected $m_strContractPropertyStartDate;
	protected $m_strContractPropertyLastActiveDate;
	protected $m_intContractPropertyIsActive;
	protected $m_strContractPropertyImplementationEndDate;
	protected $m_strContractPropertyImplementationDate;
	protected $m_intContractPropertyIsImplemented;
	protected $m_strCloseDate;
	protected $m_strContractStartDate;
	protected $m_strContractEnteredPipelineOn;
	protected $m_strProposalSentOn;
	protected $m_strContractWonOn;
	protected $m_strContractLostOn;
	protected $m_strContractTerminatedOn;
	protected $m_strImplementedOn;
	protected $m_strImplementationEndDate;
	protected $m_strDeactivationDate;
	protected $m_intImplementationEmployeeId;
	protected $m_strImplementationEmployeeName;
	protected $m_fltImplementationWeightScore;
	protected $m_intImplementationDelayTypeId;
	protected $m_strImplementationDelayType;
	protected $m_intImplementedBy;
	protected $m_strBillingStartedOn;
	protected $m_fltMonthlyRecurringAmount;
	protected $m_fltMonthlyChangeAmount;
	protected $m_fltTransactionalAmount;
	protected $m_fltTransactionalChangeAmount;
	protected $m_fltImplementationAmount;
	protected $m_strContractTerminationReason;
	protected $m_intIsFirstClientProductPropertyRecord;
	protected $m_intIsLastClientProductPropertyRecord;
	protected $m_intIsFirstContractPropertyRecord;
	protected $m_intIsLastContractPropertyRecord;
	protected $m_strClientPropertyEntrataCoreStartDate;
	protected $m_strClientPropertyEntrataCoreLastActiveDate;
	protected $m_intIsTransactionalProduct;
	protected $m_intIsPilot;
	protected $m_intIsContractProperty;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCreatedBy = '1';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['contract_product_id'] ) && $boolDirectSet ) $this->set( 'm_intContractProductId', trim( $arrValues['contract_product_id'] ) ); elseif( isset( $arrValues['contract_product_id'] ) ) $this->setContractProductId( $arrValues['contract_product_id'] );
		if( isset( $arrValues['contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intContractPropertyId', trim( $arrValues['contract_property_id'] ) ); elseif( isset( $arrValues['contract_property_id'] ) ) $this->setContractPropertyId( $arrValues['contract_property_id'] );
		if( isset( $arrValues['renewal_contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intRenewalContractPropertyId', trim( $arrValues['renewal_contract_property_id'] ) ); elseif( isset( $arrValues['renewal_contract_property_id'] ) ) $this->setRenewalContractPropertyId( $arrValues['renewal_contract_property_id'] );
		if( isset( $arrValues['original_contract_property_id'] ) && $boolDirectSet ) $this->set( 'm_intOriginalContractPropertyId', trim( $arrValues['original_contract_property_id'] ) ); elseif( isset( $arrValues['original_contract_property_id'] ) ) $this->setOriginalContractPropertyId( $arrValues['original_contract_property_id'] );
		if( isset( $arrValues['contract_type_id'] ) && $boolDirectSet ) $this->set( 'm_intContractTypeId', trim( $arrValues['contract_type_id'] ) ); elseif( isset( $arrValues['contract_type_id'] ) ) $this->setContractTypeId( $arrValues['contract_type_id'] );
		if( isset( $arrValues['contract_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intContractStatusTypeId', trim( $arrValues['contract_status_type_id'] ) ); elseif( isset( $arrValues['contract_status_type_id'] ) ) $this->setContractStatusTypeId( $arrValues['contract_status_type_id'] );
		if( isset( $arrValues['company_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyStatusTypeId', trim( $arrValues['company_status_type_id'] ) ); elseif( isset( $arrValues['company_status_type_id'] ) ) $this->setCompanyStatusTypeId( $arrValues['company_status_type_id'] );
		if( isset( $arrValues['commission_bucket_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionBucketId', trim( $arrValues['commission_bucket_id'] ) ); elseif( isset( $arrValues['commission_bucket_id'] ) ) $this->setCommissionBucketId( $arrValues['commission_bucket_id'] );
		if( isset( $arrValues['contract_termination_request_id'] ) && $boolDirectSet ) $this->set( 'm_intContractTerminationRequestId', trim( $arrValues['contract_termination_request_id'] ) ); elseif( isset( $arrValues['contract_termination_request_id'] ) ) $this->setContractTerminationRequestId( $arrValues['contract_termination_request_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrValues['company_name'] ) ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_name'] ) : $arrValues['company_name'] );
		if( isset( $arrValues['product_name'] ) && $boolDirectSet ) $this->set( 'm_strProductName', trim( stripcslashes( $arrValues['product_name'] ) ) ); elseif( isset( $arrValues['product_name'] ) ) $this->setProductName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['product_name'] ) : $arrValues['product_name'] );
		if( isset( $arrValues['property_name'] ) && $boolDirectSet ) $this->set( 'm_strPropertyName', trim( stripcslashes( $arrValues['property_name'] ) ) ); elseif( isset( $arrValues['property_name'] ) ) $this->setPropertyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_name'] ) : $arrValues['property_name'] );
		if( isset( $arrValues['property_type'] ) && $boolDirectSet ) $this->set( 'm_strPropertyType', trim( stripcslashes( $arrValues['property_type'] ) ) ); elseif( isset( $arrValues['property_type'] ) ) $this->setPropertyType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_type'] ) : $arrValues['property_type'] );
		if( isset( $arrValues['property_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intPropertyUnitCount', trim( $arrValues['property_unit_count'] ) ); elseif( isset( $arrValues['property_unit_count'] ) ) $this->setPropertyUnitCount( $arrValues['property_unit_count'] );
		if( isset( $arrValues['contract_product_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intContractProductUnitCount', trim( $arrValues['contract_product_unit_count'] ) ); elseif( isset( $arrValues['contract_product_unit_count'] ) ) $this->setContractProductUnitCount( $arrValues['contract_product_unit_count'] );
		if( isset( $arrValues['contract_product_subscription_acv'] ) && $boolDirectSet ) $this->set( 'm_intContractProductSubscriptionAcv', trim( $arrValues['contract_product_subscription_acv'] ) ); elseif( isset( $arrValues['contract_product_subscription_acv'] ) ) $this->setContractProductSubscriptionAcv( $arrValues['contract_product_subscription_acv'] );
		if( isset( $arrValues['contract_product_transaction_acv'] ) && $boolDirectSet ) $this->set( 'm_intContractProductTransactionAcv', trim( $arrValues['contract_product_transaction_acv'] ) ); elseif( isset( $arrValues['contract_product_transaction_acv'] ) ) $this->setContractProductTransactionAcv( $arrValues['contract_product_transaction_acv'] );
		if( isset( $arrValues['client_start_date'] ) && $boolDirectSet ) $this->set( 'm_strClientStartDate', trim( $arrValues['client_start_date'] ) ); elseif( isset( $arrValues['client_start_date'] ) ) $this->setClientStartDate( $arrValues['client_start_date'] );
		if( isset( $arrValues['client_last_active_date'] ) && $boolDirectSet ) $this->set( 'm_strClientLastActiveDate', trim( $arrValues['client_last_active_date'] ) ); elseif( isset( $arrValues['client_last_active_date'] ) ) $this->setClientLastActiveDate( $arrValues['client_last_active_date'] );
		if( isset( $arrValues['client_is_active'] ) && $boolDirectSet ) $this->set( 'm_intClientIsActive', trim( $arrValues['client_is_active'] ) ); elseif( isset( $arrValues['client_is_active'] ) ) $this->setClientIsActive( $arrValues['client_is_active'] );
		if( isset( $arrValues['client_product_start_date'] ) && $boolDirectSet ) $this->set( 'm_strClientProductStartDate', trim( $arrValues['client_product_start_date'] ) ); elseif( isset( $arrValues['client_product_start_date'] ) ) $this->setClientProductStartDate( $arrValues['client_product_start_date'] );
		if( isset( $arrValues['client_product_last_active_date'] ) && $boolDirectSet ) $this->set( 'm_strClientProductLastActiveDate', trim( $arrValues['client_product_last_active_date'] ) ); elseif( isset( $arrValues['client_product_last_active_date'] ) ) $this->setClientProductLastActiveDate( $arrValues['client_product_last_active_date'] );
		if( isset( $arrValues['client_product_is_active'] ) && $boolDirectSet ) $this->set( 'm_intClientProductIsActive', trim( $arrValues['client_product_is_active'] ) ); elseif( isset( $arrValues['client_product_is_active'] ) ) $this->setClientProductIsActive( $arrValues['client_product_is_active'] );
		if( isset( $arrValues['client_property_start_date'] ) && $boolDirectSet ) $this->set( 'm_strClientPropertyStartDate', trim( $arrValues['client_property_start_date'] ) ); elseif( isset( $arrValues['client_property_start_date'] ) ) $this->setClientPropertyStartDate( $arrValues['client_property_start_date'] );
		if( isset( $arrValues['client_property_last_active_date'] ) && $boolDirectSet ) $this->set( 'm_strClientPropertyLastActiveDate', trim( $arrValues['client_property_last_active_date'] ) ); elseif( isset( $arrValues['client_property_last_active_date'] ) ) $this->setClientPropertyLastActiveDate( $arrValues['client_property_last_active_date'] );
		if( isset( $arrValues['client_property_is_active'] ) && $boolDirectSet ) $this->set( 'm_intClientPropertyIsActive', trim( $arrValues['client_property_is_active'] ) ); elseif( isset( $arrValues['client_property_is_active'] ) ) $this->setClientPropertyIsActive( $arrValues['client_property_is_active'] );
		if( isset( $arrValues['client_product_property_start_date'] ) && $boolDirectSet ) $this->set( 'm_strClientProductPropertyStartDate', trim( $arrValues['client_product_property_start_date'] ) ); elseif( isset( $arrValues['client_product_property_start_date'] ) ) $this->setClientProductPropertyStartDate( $arrValues['client_product_property_start_date'] );
		if( isset( $arrValues['client_product_property_last_active_date'] ) && $boolDirectSet ) $this->set( 'm_strClientProductPropertyLastActiveDate', trim( $arrValues['client_product_property_last_active_date'] ) ); elseif( isset( $arrValues['client_product_property_last_active_date'] ) ) $this->setClientProductPropertyLastActiveDate( $arrValues['client_product_property_last_active_date'] );
		if( isset( $arrValues['client_product_property_is_active'] ) && $boolDirectSet ) $this->set( 'm_intClientProductPropertyIsActive', trim( $arrValues['client_product_property_is_active'] ) ); elseif( isset( $arrValues['client_product_property_is_active'] ) ) $this->setClientProductPropertyIsActive( $arrValues['client_product_property_is_active'] );
		if( isset( $arrValues['contract_property_start_date'] ) && $boolDirectSet ) $this->set( 'm_strContractPropertyStartDate', trim( $arrValues['contract_property_start_date'] ) ); elseif( isset( $arrValues['contract_property_start_date'] ) ) $this->setContractPropertyStartDate( $arrValues['contract_property_start_date'] );
		if( isset( $arrValues['contract_property_last_active_date'] ) && $boolDirectSet ) $this->set( 'm_strContractPropertyLastActiveDate', trim( $arrValues['contract_property_last_active_date'] ) ); elseif( isset( $arrValues['contract_property_last_active_date'] ) ) $this->setContractPropertyLastActiveDate( $arrValues['contract_property_last_active_date'] );
		if( isset( $arrValues['contract_property_is_active'] ) && $boolDirectSet ) $this->set( 'm_intContractPropertyIsActive', trim( $arrValues['contract_property_is_active'] ) ); elseif( isset( $arrValues['contract_property_is_active'] ) ) $this->setContractPropertyIsActive( $arrValues['contract_property_is_active'] );
		if( isset( $arrValues['contract_property_implementation_end_date'] ) && $boolDirectSet ) $this->set( 'm_strContractPropertyImplementationEndDate', trim( $arrValues['contract_property_implementation_end_date'] ) ); elseif( isset( $arrValues['contract_property_implementation_end_date'] ) ) $this->setContractPropertyImplementationEndDate( $arrValues['contract_property_implementation_end_date'] );
		if( isset( $arrValues['contract_property_implementation_date'] ) && $boolDirectSet ) $this->set( 'm_strContractPropertyImplementationDate', trim( $arrValues['contract_property_implementation_date'] ) ); elseif( isset( $arrValues['contract_property_implementation_date'] ) ) $this->setContractPropertyImplementationDate( $arrValues['contract_property_implementation_date'] );
		if( isset( $arrValues['contract_property_is_implemented'] ) && $boolDirectSet ) $this->set( 'm_intContractPropertyIsImplemented', trim( $arrValues['contract_property_is_implemented'] ) ); elseif( isset( $arrValues['contract_property_is_implemented'] ) ) $this->setContractPropertyIsImplemented( $arrValues['contract_property_is_implemented'] );
		if( isset( $arrValues['close_date'] ) && $boolDirectSet ) $this->set( 'm_strCloseDate', trim( $arrValues['close_date'] ) ); elseif( isset( $arrValues['close_date'] ) ) $this->setCloseDate( $arrValues['close_date'] );
		if( isset( $arrValues['contract_start_date'] ) && $boolDirectSet ) $this->set( 'm_strContractStartDate', trim( $arrValues['contract_start_date'] ) ); elseif( isset( $arrValues['contract_start_date'] ) ) $this->setContractStartDate( $arrValues['contract_start_date'] );
		if( isset( $arrValues['contract_entered_pipeline_on'] ) && $boolDirectSet ) $this->set( 'm_strContractEnteredPipelineOn', trim( $arrValues['contract_entered_pipeline_on'] ) ); elseif( isset( $arrValues['contract_entered_pipeline_on'] ) ) $this->setContractEnteredPipelineOn( $arrValues['contract_entered_pipeline_on'] );
		if( isset( $arrValues['proposal_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strProposalSentOn', trim( $arrValues['proposal_sent_on'] ) ); elseif( isset( $arrValues['proposal_sent_on'] ) ) $this->setProposalSentOn( $arrValues['proposal_sent_on'] );
		if( isset( $arrValues['contract_won_on'] ) && $boolDirectSet ) $this->set( 'm_strContractWonOn', trim( $arrValues['contract_won_on'] ) ); elseif( isset( $arrValues['contract_won_on'] ) ) $this->setContractWonOn( $arrValues['contract_won_on'] );
		if( isset( $arrValues['contract_lost_on'] ) && $boolDirectSet ) $this->set( 'm_strContractLostOn', trim( $arrValues['contract_lost_on'] ) ); elseif( isset( $arrValues['contract_lost_on'] ) ) $this->setContractLostOn( $arrValues['contract_lost_on'] );
		if( isset( $arrValues['contract_terminated_on'] ) && $boolDirectSet ) $this->set( 'm_strContractTerminatedOn', trim( $arrValues['contract_terminated_on'] ) ); elseif( isset( $arrValues['contract_terminated_on'] ) ) $this->setContractTerminatedOn( $arrValues['contract_terminated_on'] );
		if( isset( $arrValues['implemented_on'] ) && $boolDirectSet ) $this->set( 'm_strImplementedOn', trim( $arrValues['implemented_on'] ) ); elseif( isset( $arrValues['implemented_on'] ) ) $this->setImplementedOn( $arrValues['implemented_on'] );
		if( isset( $arrValues['implementation_end_date'] ) && $boolDirectSet ) $this->set( 'm_strImplementationEndDate', trim( $arrValues['implementation_end_date'] ) ); elseif( isset( $arrValues['implementation_end_date'] ) ) $this->setImplementationEndDate( $arrValues['implementation_end_date'] );
		if( isset( $arrValues['deactivation_date'] ) && $boolDirectSet ) $this->set( 'm_strDeactivationDate', trim( $arrValues['deactivation_date'] ) ); elseif( isset( $arrValues['deactivation_date'] ) ) $this->setDeactivationDate( $arrValues['deactivation_date'] );
		if( isset( $arrValues['implementation_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationEmployeeId', trim( $arrValues['implementation_employee_id'] ) ); elseif( isset( $arrValues['implementation_employee_id'] ) ) $this->setImplementationEmployeeId( $arrValues['implementation_employee_id'] );
		if( isset( $arrValues['implementation_employee_name'] ) && $boolDirectSet ) $this->set( 'm_strImplementationEmployeeName', trim( stripcslashes( $arrValues['implementation_employee_name'] ) ) ); elseif( isset( $arrValues['implementation_employee_name'] ) ) $this->setImplementationEmployeeName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['implementation_employee_name'] ) : $arrValues['implementation_employee_name'] );
		if( isset( $arrValues['implementation_weight_score'] ) && $boolDirectSet ) $this->set( 'm_fltImplementationWeightScore', trim( $arrValues['implementation_weight_score'] ) ); elseif( isset( $arrValues['implementation_weight_score'] ) ) $this->setImplementationWeightScore( $arrValues['implementation_weight_score'] );
		if( isset( $arrValues['implementation_delay_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationDelayTypeId', trim( $arrValues['implementation_delay_type_id'] ) ); elseif( isset( $arrValues['implementation_delay_type_id'] ) ) $this->setImplementationDelayTypeId( $arrValues['implementation_delay_type_id'] );
		if( isset( $arrValues['implementation_delay_type'] ) && $boolDirectSet ) $this->set( 'm_strImplementationDelayType', trim( stripcslashes( $arrValues['implementation_delay_type'] ) ) ); elseif( isset( $arrValues['implementation_delay_type'] ) ) $this->setImplementationDelayType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['implementation_delay_type'] ) : $arrValues['implementation_delay_type'] );
		if( isset( $arrValues['implemented_by'] ) && $boolDirectSet ) $this->set( 'm_intImplementedBy', trim( $arrValues['implemented_by'] ) ); elseif( isset( $arrValues['implemented_by'] ) ) $this->setImplementedBy( $arrValues['implemented_by'] );
		if( isset( $arrValues['billing_started_on'] ) && $boolDirectSet ) $this->set( 'm_strBillingStartedOn', trim( $arrValues['billing_started_on'] ) ); elseif( isset( $arrValues['billing_started_on'] ) ) $this->setBillingStartedOn( $arrValues['billing_started_on'] );
		if( isset( $arrValues['monthly_recurring_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyRecurringAmount', trim( $arrValues['monthly_recurring_amount'] ) ); elseif( isset( $arrValues['monthly_recurring_amount'] ) ) $this->setMonthlyRecurringAmount( $arrValues['monthly_recurring_amount'] );
		if( isset( $arrValues['monthly_change_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyChangeAmount', trim( $arrValues['monthly_change_amount'] ) ); elseif( isset( $arrValues['monthly_change_amount'] ) ) $this->setMonthlyChangeAmount( $arrValues['monthly_change_amount'] );
		if( isset( $arrValues['transactional_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionalAmount', trim( $arrValues['transactional_amount'] ) ); elseif( isset( $arrValues['transactional_amount'] ) ) $this->setTransactionalAmount( $arrValues['transactional_amount'] );
		if( isset( $arrValues['transactional_change_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTransactionalChangeAmount', trim( $arrValues['transactional_change_amount'] ) ); elseif( isset( $arrValues['transactional_change_amount'] ) ) $this->setTransactionalChangeAmount( $arrValues['transactional_change_amount'] );
		if( isset( $arrValues['implementation_amount'] ) && $boolDirectSet ) $this->set( 'm_fltImplementationAmount', trim( $arrValues['implementation_amount'] ) ); elseif( isset( $arrValues['implementation_amount'] ) ) $this->setImplementationAmount( $arrValues['implementation_amount'] );
		if( isset( $arrValues['contract_termination_reason'] ) && $boolDirectSet ) $this->set( 'm_strContractTerminationReason', trim( stripcslashes( $arrValues['contract_termination_reason'] ) ) ); elseif( isset( $arrValues['contract_termination_reason'] ) ) $this->setContractTerminationReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contract_termination_reason'] ) : $arrValues['contract_termination_reason'] );
		if( isset( $arrValues['is_first_client_product_property_record'] ) && $boolDirectSet ) $this->set( 'm_intIsFirstClientProductPropertyRecord', trim( $arrValues['is_first_client_product_property_record'] ) ); elseif( isset( $arrValues['is_first_client_product_property_record'] ) ) $this->setIsFirstClientProductPropertyRecord( $arrValues['is_first_client_product_property_record'] );
		if( isset( $arrValues['is_last_client_product_property_record'] ) && $boolDirectSet ) $this->set( 'm_intIsLastClientProductPropertyRecord', trim( $arrValues['is_last_client_product_property_record'] ) ); elseif( isset( $arrValues['is_last_client_product_property_record'] ) ) $this->setIsLastClientProductPropertyRecord( $arrValues['is_last_client_product_property_record'] );
		if( isset( $arrValues['is_first_contract_property_record'] ) && $boolDirectSet ) $this->set( 'm_intIsFirstContractPropertyRecord', trim( $arrValues['is_first_contract_property_record'] ) ); elseif( isset( $arrValues['is_first_contract_property_record'] ) ) $this->setIsFirstContractPropertyRecord( $arrValues['is_first_contract_property_record'] );
		if( isset( $arrValues['is_last_contract_property_record'] ) && $boolDirectSet ) $this->set( 'm_intIsLastContractPropertyRecord', trim( $arrValues['is_last_contract_property_record'] ) ); elseif( isset( $arrValues['is_last_contract_property_record'] ) ) $this->setIsLastContractPropertyRecord( $arrValues['is_last_contract_property_record'] );
		if( isset( $arrValues['client_property_entrata_core_start_date'] ) && $boolDirectSet ) $this->set( 'm_strClientPropertyEntrataCoreStartDate', trim( $arrValues['client_property_entrata_core_start_date'] ) ); elseif( isset( $arrValues['client_property_entrata_core_start_date'] ) ) $this->setClientPropertyEntrataCoreStartDate( $arrValues['client_property_entrata_core_start_date'] );
		if( isset( $arrValues['client_property_entrata_core_last_active_date'] ) && $boolDirectSet ) $this->set( 'm_strClientPropertyEntrataCoreLastActiveDate', trim( $arrValues['client_property_entrata_core_last_active_date'] ) ); elseif( isset( $arrValues['client_property_entrata_core_last_active_date'] ) ) $this->setClientPropertyEntrataCoreLastActiveDate( $arrValues['client_property_entrata_core_last_active_date'] );
		if( isset( $arrValues['is_transactional_product'] ) && $boolDirectSet ) $this->set( 'm_intIsTransactionalProduct', trim( $arrValues['is_transactional_product'] ) ); elseif( isset( $arrValues['is_transactional_product'] ) ) $this->setIsTransactionalProduct( $arrValues['is_transactional_product'] );
		if( isset( $arrValues['is_pilot'] ) && $boolDirectSet ) $this->set( 'm_intIsPilot', trim( $arrValues['is_pilot'] ) ); elseif( isset( $arrValues['is_pilot'] ) ) $this->setIsPilot( $arrValues['is_pilot'] );
		if( isset( $arrValues['is_contract_property'] ) && $boolDirectSet ) $this->set( 'm_intIsContractProperty', trim( $arrValues['is_contract_property'] ) ); elseif( isset( $arrValues['is_contract_property'] ) ) $this->setIsContractProperty( $arrValues['is_contract_property'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setContractProductId( $intContractProductId ) {
		$this->set( 'm_intContractProductId', CStrings::strToIntDef( $intContractProductId, NULL, false ) );
	}

	public function getContractProductId() {
		return $this->m_intContractProductId;
	}

	public function sqlContractProductId() {
		return ( true == isset( $this->m_intContractProductId ) ) ? ( string ) $this->m_intContractProductId : 'NULL';
	}

	public function setContractPropertyId( $intContractPropertyId ) {
		$this->set( 'm_intContractPropertyId', CStrings::strToIntDef( $intContractPropertyId, NULL, false ) );
	}

	public function getContractPropertyId() {
		return $this->m_intContractPropertyId;
	}

	public function sqlContractPropertyId() {
		return ( true == isset( $this->m_intContractPropertyId ) ) ? ( string ) $this->m_intContractPropertyId : 'NULL';
	}

	public function setRenewalContractPropertyId( $intRenewalContractPropertyId ) {
		$this->set( 'm_intRenewalContractPropertyId', CStrings::strToIntDef( $intRenewalContractPropertyId, NULL, false ) );
	}

	public function getRenewalContractPropertyId() {
		return $this->m_intRenewalContractPropertyId;
	}

	public function sqlRenewalContractPropertyId() {
		return ( true == isset( $this->m_intRenewalContractPropertyId ) ) ? ( string ) $this->m_intRenewalContractPropertyId : 'NULL';
	}

	public function setOriginalContractPropertyId( $intOriginalContractPropertyId ) {
		$this->set( 'm_intOriginalContractPropertyId', CStrings::strToIntDef( $intOriginalContractPropertyId, NULL, false ) );
	}

	public function getOriginalContractPropertyId() {
		return $this->m_intOriginalContractPropertyId;
	}

	public function sqlOriginalContractPropertyId() {
		return ( true == isset( $this->m_intOriginalContractPropertyId ) ) ? ( string ) $this->m_intOriginalContractPropertyId : 'NULL';
	}

	public function setContractTypeId( $intContractTypeId ) {
		$this->set( 'm_intContractTypeId', CStrings::strToIntDef( $intContractTypeId, NULL, false ) );
	}

	public function getContractTypeId() {
		return $this->m_intContractTypeId;
	}

	public function sqlContractTypeId() {
		return ( true == isset( $this->m_intContractTypeId ) ) ? ( string ) $this->m_intContractTypeId : 'NULL';
	}

	public function setContractStatusTypeId( $intContractStatusTypeId ) {
		$this->set( 'm_intContractStatusTypeId', CStrings::strToIntDef( $intContractStatusTypeId, NULL, false ) );
	}

	public function getContractStatusTypeId() {
		return $this->m_intContractStatusTypeId;
	}

	public function sqlContractStatusTypeId() {
		return ( true == isset( $this->m_intContractStatusTypeId ) ) ? ( string ) $this->m_intContractStatusTypeId : 'NULL';
	}

	public function setCompanyStatusTypeId( $intCompanyStatusTypeId ) {
		$this->set( 'm_intCompanyStatusTypeId', CStrings::strToIntDef( $intCompanyStatusTypeId, NULL, false ) );
	}

	public function getCompanyStatusTypeId() {
		return $this->m_intCompanyStatusTypeId;
	}

	public function sqlCompanyStatusTypeId() {
		return ( true == isset( $this->m_intCompanyStatusTypeId ) ) ? ( string ) $this->m_intCompanyStatusTypeId : 'NULL';
	}

	public function setCommissionBucketId( $intCommissionBucketId ) {
		$this->set( 'm_intCommissionBucketId', CStrings::strToIntDef( $intCommissionBucketId, NULL, false ) );
	}

	public function getCommissionBucketId() {
		return $this->m_intCommissionBucketId;
	}

	public function sqlCommissionBucketId() {
		return ( true == isset( $this->m_intCommissionBucketId ) ) ? ( string ) $this->m_intCommissionBucketId : 'NULL';
	}

	public function setContractTerminationRequestId( $intContractTerminationRequestId ) {
		$this->set( 'm_intContractTerminationRequestId', CStrings::strToIntDef( $intContractTerminationRequestId, NULL, false ) );
	}

	public function getContractTerminationRequestId() {
		return $this->m_intContractTerminationRequestId;
	}

	public function sqlContractTerminationRequestId() {
		return ( true == isset( $this->m_intContractTerminationRequestId ) ) ? ( string ) $this->m_intContractTerminationRequestId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 100, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

	public function setProductName( $strProductName ) {
		$this->set( 'm_strProductName', CStrings::strTrimDef( $strProductName, 50, NULL, true ) );
	}

	public function getProductName() {
		return $this->m_strProductName;
	}

	public function sqlProductName() {
		return ( true == isset( $this->m_strProductName ) ) ? '\'' . addslashes( $this->m_strProductName ) . '\'' : 'NULL';
	}

	public function setPropertyName( $strPropertyName ) {
		$this->set( 'm_strPropertyName', CStrings::strTrimDef( $strPropertyName, 50, NULL, true ) );
	}

	public function getPropertyName() {
		return $this->m_strPropertyName;
	}

	public function sqlPropertyName() {
		return ( true == isset( $this->m_strPropertyName ) ) ? '\'' . addslashes( $this->m_strPropertyName ) . '\'' : 'NULL';
	}

	public function setPropertyType( $strPropertyType ) {
		$this->set( 'm_strPropertyType', CStrings::strTrimDef( $strPropertyType, 50, NULL, true ) );
	}

	public function getPropertyType() {
		return $this->m_strPropertyType;
	}

	public function sqlPropertyType() {
		return ( true == isset( $this->m_strPropertyType ) ) ? '\'' . addslashes( $this->m_strPropertyType ) . '\'' : 'NULL';
	}

	public function setPropertyUnitCount( $intPropertyUnitCount ) {
		$this->set( 'm_intPropertyUnitCount', CStrings::strToIntDef( $intPropertyUnitCount, NULL, false ) );
	}

	public function getPropertyUnitCount() {
		return $this->m_intPropertyUnitCount;
	}

	public function sqlPropertyUnitCount() {
		return ( true == isset( $this->m_intPropertyUnitCount ) ) ? ( string ) $this->m_intPropertyUnitCount : 'NULL';
	}

	public function setContractProductUnitCount( $intContractProductUnitCount ) {
		$this->set( 'm_intContractProductUnitCount', CStrings::strToIntDef( $intContractProductUnitCount, NULL, false ) );
	}

	public function getContractProductUnitCount() {
		return $this->m_intContractProductUnitCount;
	}

	public function sqlContractProductUnitCount() {
		return ( true == isset( $this->m_intContractProductUnitCount ) ) ? ( string ) $this->m_intContractProductUnitCount : 'NULL';
	}

	public function setContractProductSubscriptionAcv( $intContractProductSubscriptionAcv ) {
		$this->set( 'm_intContractProductSubscriptionAcv', CStrings::strToIntDef( $intContractProductSubscriptionAcv, NULL, false ) );
	}

	public function getContractProductSubscriptionAcv() {
		return $this->m_intContractProductSubscriptionAcv;
	}

	public function sqlContractProductSubscriptionAcv() {
		return ( true == isset( $this->m_intContractProductSubscriptionAcv ) ) ? ( string ) $this->m_intContractProductSubscriptionAcv : 'NULL';
	}

	public function setContractProductTransactionAcv( $intContractProductTransactionAcv ) {
		$this->set( 'm_intContractProductTransactionAcv', CStrings::strToIntDef( $intContractProductTransactionAcv, NULL, false ) );
	}

	public function getContractProductTransactionAcv() {
		return $this->m_intContractProductTransactionAcv;
	}

	public function sqlContractProductTransactionAcv() {
		return ( true == isset( $this->m_intContractProductTransactionAcv ) ) ? ( string ) $this->m_intContractProductTransactionAcv : 'NULL';
	}

	public function setClientStartDate( $strClientStartDate ) {
		$this->set( 'm_strClientStartDate', CStrings::strTrimDef( $strClientStartDate, -1, NULL, true ) );
	}

	public function getClientStartDate() {
		return $this->m_strClientStartDate;
	}

	public function sqlClientStartDate() {
		return ( true == isset( $this->m_strClientStartDate ) ) ? '\'' . $this->m_strClientStartDate . '\'' : 'NULL';
	}

	public function setClientLastActiveDate( $strClientLastActiveDate ) {
		$this->set( 'm_strClientLastActiveDate', CStrings::strTrimDef( $strClientLastActiveDate, -1, NULL, true ) );
	}

	public function getClientLastActiveDate() {
		return $this->m_strClientLastActiveDate;
	}

	public function sqlClientLastActiveDate() {
		return ( true == isset( $this->m_strClientLastActiveDate ) ) ? '\'' . $this->m_strClientLastActiveDate . '\'' : 'NULL';
	}

	public function setClientIsActive( $intClientIsActive ) {
		$this->set( 'm_intClientIsActive', CStrings::strToIntDef( $intClientIsActive, NULL, false ) );
	}

	public function getClientIsActive() {
		return $this->m_intClientIsActive;
	}

	public function sqlClientIsActive() {
		return ( true == isset( $this->m_intClientIsActive ) ) ? ( string ) $this->m_intClientIsActive : 'NULL';
	}

	public function setClientProductStartDate( $strClientProductStartDate ) {
		$this->set( 'm_strClientProductStartDate', CStrings::strTrimDef( $strClientProductStartDate, -1, NULL, true ) );
	}

	public function getClientProductStartDate() {
		return $this->m_strClientProductStartDate;
	}

	public function sqlClientProductStartDate() {
		return ( true == isset( $this->m_strClientProductStartDate ) ) ? '\'' . $this->m_strClientProductStartDate . '\'' : 'NULL';
	}

	public function setClientProductLastActiveDate( $strClientProductLastActiveDate ) {
		$this->set( 'm_strClientProductLastActiveDate', CStrings::strTrimDef( $strClientProductLastActiveDate, -1, NULL, true ) );
	}

	public function getClientProductLastActiveDate() {
		return $this->m_strClientProductLastActiveDate;
	}

	public function sqlClientProductLastActiveDate() {
		return ( true == isset( $this->m_strClientProductLastActiveDate ) ) ? '\'' . $this->m_strClientProductLastActiveDate . '\'' : 'NULL';
	}

	public function setClientProductIsActive( $intClientProductIsActive ) {
		$this->set( 'm_intClientProductIsActive', CStrings::strToIntDef( $intClientProductIsActive, NULL, false ) );
	}

	public function getClientProductIsActive() {
		return $this->m_intClientProductIsActive;
	}

	public function sqlClientProductIsActive() {
		return ( true == isset( $this->m_intClientProductIsActive ) ) ? ( string ) $this->m_intClientProductIsActive : 'NULL';
	}

	public function setClientPropertyStartDate( $strClientPropertyStartDate ) {
		$this->set( 'm_strClientPropertyStartDate', CStrings::strTrimDef( $strClientPropertyStartDate, -1, NULL, true ) );
	}

	public function getClientPropertyStartDate() {
		return $this->m_strClientPropertyStartDate;
	}

	public function sqlClientPropertyStartDate() {
		return ( true == isset( $this->m_strClientPropertyStartDate ) ) ? '\'' . $this->m_strClientPropertyStartDate . '\'' : 'NULL';
	}

	public function setClientPropertyLastActiveDate( $strClientPropertyLastActiveDate ) {
		$this->set( 'm_strClientPropertyLastActiveDate', CStrings::strTrimDef( $strClientPropertyLastActiveDate, -1, NULL, true ) );
	}

	public function getClientPropertyLastActiveDate() {
		return $this->m_strClientPropertyLastActiveDate;
	}

	public function sqlClientPropertyLastActiveDate() {
		return ( true == isset( $this->m_strClientPropertyLastActiveDate ) ) ? '\'' . $this->m_strClientPropertyLastActiveDate . '\'' : 'NULL';
	}

	public function setClientPropertyIsActive( $intClientPropertyIsActive ) {
		$this->set( 'm_intClientPropertyIsActive', CStrings::strToIntDef( $intClientPropertyIsActive, NULL, false ) );
	}

	public function getClientPropertyIsActive() {
		return $this->m_intClientPropertyIsActive;
	}

	public function sqlClientPropertyIsActive() {
		return ( true == isset( $this->m_intClientPropertyIsActive ) ) ? ( string ) $this->m_intClientPropertyIsActive : 'NULL';
	}

	public function setClientProductPropertyStartDate( $strClientProductPropertyStartDate ) {
		$this->set( 'm_strClientProductPropertyStartDate', CStrings::strTrimDef( $strClientProductPropertyStartDate, -1, NULL, true ) );
	}

	public function getClientProductPropertyStartDate() {
		return $this->m_strClientProductPropertyStartDate;
	}

	public function sqlClientProductPropertyStartDate() {
		return ( true == isset( $this->m_strClientProductPropertyStartDate ) ) ? '\'' . $this->m_strClientProductPropertyStartDate . '\'' : 'NULL';
	}

	public function setClientProductPropertyLastActiveDate( $strClientProductPropertyLastActiveDate ) {
		$this->set( 'm_strClientProductPropertyLastActiveDate', CStrings::strTrimDef( $strClientProductPropertyLastActiveDate, -1, NULL, true ) );
	}

	public function getClientProductPropertyLastActiveDate() {
		return $this->m_strClientProductPropertyLastActiveDate;
	}

	public function sqlClientProductPropertyLastActiveDate() {
		return ( true == isset( $this->m_strClientProductPropertyLastActiveDate ) ) ? '\'' . $this->m_strClientProductPropertyLastActiveDate . '\'' : 'NULL';
	}

	public function setClientProductPropertyIsActive( $intClientProductPropertyIsActive ) {
		$this->set( 'm_intClientProductPropertyIsActive', CStrings::strToIntDef( $intClientProductPropertyIsActive, NULL, false ) );
	}

	public function getClientProductPropertyIsActive() {
		return $this->m_intClientProductPropertyIsActive;
	}

	public function sqlClientProductPropertyIsActive() {
		return ( true == isset( $this->m_intClientProductPropertyIsActive ) ) ? ( string ) $this->m_intClientProductPropertyIsActive : 'NULL';
	}

	public function setContractPropertyStartDate( $strContractPropertyStartDate ) {
		$this->set( 'm_strContractPropertyStartDate', CStrings::strTrimDef( $strContractPropertyStartDate, -1, NULL, true ) );
	}

	public function getContractPropertyStartDate() {
		return $this->m_strContractPropertyStartDate;
	}

	public function sqlContractPropertyStartDate() {
		return ( true == isset( $this->m_strContractPropertyStartDate ) ) ? '\'' . $this->m_strContractPropertyStartDate . '\'' : 'NULL';
	}

	public function setContractPropertyLastActiveDate( $strContractPropertyLastActiveDate ) {
		$this->set( 'm_strContractPropertyLastActiveDate', CStrings::strTrimDef( $strContractPropertyLastActiveDate, -1, NULL, true ) );
	}

	public function getContractPropertyLastActiveDate() {
		return $this->m_strContractPropertyLastActiveDate;
	}

	public function sqlContractPropertyLastActiveDate() {
		return ( true == isset( $this->m_strContractPropertyLastActiveDate ) ) ? '\'' . $this->m_strContractPropertyLastActiveDate . '\'' : 'NULL';
	}

	public function setContractPropertyIsActive( $intContractPropertyIsActive ) {
		$this->set( 'm_intContractPropertyIsActive', CStrings::strToIntDef( $intContractPropertyIsActive, NULL, false ) );
	}

	public function getContractPropertyIsActive() {
		return $this->m_intContractPropertyIsActive;
	}

	public function sqlContractPropertyIsActive() {
		return ( true == isset( $this->m_intContractPropertyIsActive ) ) ? ( string ) $this->m_intContractPropertyIsActive : 'NULL';
	}

	public function setContractPropertyImplementationEndDate( $strContractPropertyImplementationEndDate ) {
		$this->set( 'm_strContractPropertyImplementationEndDate', CStrings::strTrimDef( $strContractPropertyImplementationEndDate, -1, NULL, true ) );
	}

	public function getContractPropertyImplementationEndDate() {
		return $this->m_strContractPropertyImplementationEndDate;
	}

	public function sqlContractPropertyImplementationEndDate() {
		return ( true == isset( $this->m_strContractPropertyImplementationEndDate ) ) ? '\'' . $this->m_strContractPropertyImplementationEndDate . '\'' : 'NULL';
	}

	public function setContractPropertyImplementationDate( $strContractPropertyImplementationDate ) {
		$this->set( 'm_strContractPropertyImplementationDate', CStrings::strTrimDef( $strContractPropertyImplementationDate, -1, NULL, true ) );
	}

	public function getContractPropertyImplementationDate() {
		return $this->m_strContractPropertyImplementationDate;
	}

	public function sqlContractPropertyImplementationDate() {
		return ( true == isset( $this->m_strContractPropertyImplementationDate ) ) ? '\'' . $this->m_strContractPropertyImplementationDate . '\'' : 'NULL';
	}

	public function setContractPropertyIsImplemented( $intContractPropertyIsImplemented ) {
		$this->set( 'm_intContractPropertyIsImplemented', CStrings::strToIntDef( $intContractPropertyIsImplemented, NULL, false ) );
	}

	public function getContractPropertyIsImplemented() {
		return $this->m_intContractPropertyIsImplemented;
	}

	public function sqlContractPropertyIsImplemented() {
		return ( true == isset( $this->m_intContractPropertyIsImplemented ) ) ? ( string ) $this->m_intContractPropertyIsImplemented : 'NULL';
	}

	public function setCloseDate( $strCloseDate ) {
		$this->set( 'm_strCloseDate', CStrings::strTrimDef( $strCloseDate, -1, NULL, true ) );
	}

	public function getCloseDate() {
		return $this->m_strCloseDate;
	}

	public function sqlCloseDate() {
		return ( true == isset( $this->m_strCloseDate ) ) ? '\'' . $this->m_strCloseDate . '\'' : 'NULL';
	}

	public function setContractStartDate( $strContractStartDate ) {
		$this->set( 'm_strContractStartDate', CStrings::strTrimDef( $strContractStartDate, -1, NULL, true ) );
	}

	public function getContractStartDate() {
		return $this->m_strContractStartDate;
	}

	public function sqlContractStartDate() {
		return ( true == isset( $this->m_strContractStartDate ) ) ? '\'' . $this->m_strContractStartDate . '\'' : 'NULL';
	}

	public function setContractEnteredPipelineOn( $strContractEnteredPipelineOn ) {
		$this->set( 'm_strContractEnteredPipelineOn', CStrings::strTrimDef( $strContractEnteredPipelineOn, -1, NULL, true ) );
	}

	public function getContractEnteredPipelineOn() {
		return $this->m_strContractEnteredPipelineOn;
	}

	public function sqlContractEnteredPipelineOn() {
		return ( true == isset( $this->m_strContractEnteredPipelineOn ) ) ? '\'' . $this->m_strContractEnteredPipelineOn . '\'' : 'NULL';
	}

	public function setProposalSentOn( $strProposalSentOn ) {
		$this->set( 'm_strProposalSentOn', CStrings::strTrimDef( $strProposalSentOn, -1, NULL, true ) );
	}

	public function getProposalSentOn() {
		return $this->m_strProposalSentOn;
	}

	public function sqlProposalSentOn() {
		return ( true == isset( $this->m_strProposalSentOn ) ) ? '\'' . $this->m_strProposalSentOn . '\'' : 'NULL';
	}

	public function setContractWonOn( $strContractWonOn ) {
		$this->set( 'm_strContractWonOn', CStrings::strTrimDef( $strContractWonOn, -1, NULL, true ) );
	}

	public function getContractWonOn() {
		return $this->m_strContractWonOn;
	}

	public function sqlContractWonOn() {
		return ( true == isset( $this->m_strContractWonOn ) ) ? '\'' . $this->m_strContractWonOn . '\'' : 'NULL';
	}

	public function setContractLostOn( $strContractLostOn ) {
		$this->set( 'm_strContractLostOn', CStrings::strTrimDef( $strContractLostOn, -1, NULL, true ) );
	}

	public function getContractLostOn() {
		return $this->m_strContractLostOn;
	}

	public function sqlContractLostOn() {
		return ( true == isset( $this->m_strContractLostOn ) ) ? '\'' . $this->m_strContractLostOn . '\'' : 'NULL';
	}

	public function setContractTerminatedOn( $strContractTerminatedOn ) {
		$this->set( 'm_strContractTerminatedOn', CStrings::strTrimDef( $strContractTerminatedOn, -1, NULL, true ) );
	}

	public function getContractTerminatedOn() {
		return $this->m_strContractTerminatedOn;
	}

	public function sqlContractTerminatedOn() {
		return ( true == isset( $this->m_strContractTerminatedOn ) ) ? '\'' . $this->m_strContractTerminatedOn . '\'' : 'NULL';
	}

	public function setImplementedOn( $strImplementedOn ) {
		$this->set( 'm_strImplementedOn', CStrings::strTrimDef( $strImplementedOn, -1, NULL, true ) );
	}

	public function getImplementedOn() {
		return $this->m_strImplementedOn;
	}

	public function sqlImplementedOn() {
		return ( true == isset( $this->m_strImplementedOn ) ) ? '\'' . $this->m_strImplementedOn . '\'' : 'NULL';
	}

	public function setImplementationEndDate( $strImplementationEndDate ) {
		$this->set( 'm_strImplementationEndDate', CStrings::strTrimDef( $strImplementationEndDate, -1, NULL, true ) );
	}

	public function getImplementationEndDate() {
		return $this->m_strImplementationEndDate;
	}

	public function sqlImplementationEndDate() {
		return ( true == isset( $this->m_strImplementationEndDate ) ) ? '\'' . $this->m_strImplementationEndDate . '\'' : 'NULL';
	}

	public function setDeactivationDate( $strDeactivationDate ) {
		$this->set( 'm_strDeactivationDate', CStrings::strTrimDef( $strDeactivationDate, -1, NULL, true ) );
	}

	public function getDeactivationDate() {
		return $this->m_strDeactivationDate;
	}

	public function sqlDeactivationDate() {
		return ( true == isset( $this->m_strDeactivationDate ) ) ? '\'' . $this->m_strDeactivationDate . '\'' : 'NULL';
	}

	public function setImplementationEmployeeId( $intImplementationEmployeeId ) {
		$this->set( 'm_intImplementationEmployeeId', CStrings::strToIntDef( $intImplementationEmployeeId, NULL, false ) );
	}

	public function getImplementationEmployeeId() {
		return $this->m_intImplementationEmployeeId;
	}

	public function sqlImplementationEmployeeId() {
		return ( true == isset( $this->m_intImplementationEmployeeId ) ) ? ( string ) $this->m_intImplementationEmployeeId : 'NULL';
	}

	public function setImplementationEmployeeName( $strImplementationEmployeeName ) {
		$this->set( 'm_strImplementationEmployeeName', CStrings::strTrimDef( $strImplementationEmployeeName, 50, NULL, true ) );
	}

	public function getImplementationEmployeeName() {
		return $this->m_strImplementationEmployeeName;
	}

	public function sqlImplementationEmployeeName() {
		return ( true == isset( $this->m_strImplementationEmployeeName ) ) ? '\'' . addslashes( $this->m_strImplementationEmployeeName ) . '\'' : 'NULL';
	}

	public function setImplementationWeightScore( $fltImplementationWeightScore ) {
		$this->set( 'm_fltImplementationWeightScore', CStrings::strToFloatDef( $fltImplementationWeightScore, NULL, false, 0 ) );
	}

	public function getImplementationWeightScore() {
		return $this->m_fltImplementationWeightScore;
	}

	public function sqlImplementationWeightScore() {
		return ( true == isset( $this->m_fltImplementationWeightScore ) ) ? ( string ) $this->m_fltImplementationWeightScore : 'NULL';
	}

	public function setImplementationDelayTypeId( $intImplementationDelayTypeId ) {
		$this->set( 'm_intImplementationDelayTypeId', CStrings::strToIntDef( $intImplementationDelayTypeId, NULL, false ) );
	}

	public function getImplementationDelayTypeId() {
		return $this->m_intImplementationDelayTypeId;
	}

	public function sqlImplementationDelayTypeId() {
		return ( true == isset( $this->m_intImplementationDelayTypeId ) ) ? ( string ) $this->m_intImplementationDelayTypeId : 'NULL';
	}

	public function setImplementationDelayType( $strImplementationDelayType ) {
		$this->set( 'm_strImplementationDelayType', CStrings::strTrimDef( $strImplementationDelayType, 50, NULL, true ) );
	}

	public function getImplementationDelayType() {
		return $this->m_strImplementationDelayType;
	}

	public function sqlImplementationDelayType() {
		return ( true == isset( $this->m_strImplementationDelayType ) ) ? '\'' . addslashes( $this->m_strImplementationDelayType ) . '\'' : 'NULL';
	}

	public function setImplementedBy( $intImplementedBy ) {
		$this->set( 'm_intImplementedBy', CStrings::strToIntDef( $intImplementedBy, NULL, false ) );
	}

	public function getImplementedBy() {
		return $this->m_intImplementedBy;
	}

	public function sqlImplementedBy() {
		return ( true == isset( $this->m_intImplementedBy ) ) ? ( string ) $this->m_intImplementedBy : 'NULL';
	}

	public function setBillingStartedOn( $strBillingStartedOn ) {
		$this->set( 'm_strBillingStartedOn', CStrings::strTrimDef( $strBillingStartedOn, -1, NULL, true ) );
	}

	public function getBillingStartedOn() {
		return $this->m_strBillingStartedOn;
	}

	public function sqlBillingStartedOn() {
		return ( true == isset( $this->m_strBillingStartedOn ) ) ? '\'' . $this->m_strBillingStartedOn . '\'' : 'NULL';
	}

	public function setMonthlyRecurringAmount( $fltMonthlyRecurringAmount ) {
		$this->set( 'm_fltMonthlyRecurringAmount', CStrings::strToFloatDef( $fltMonthlyRecurringAmount, NULL, false, 0 ) );
	}

	public function getMonthlyRecurringAmount() {
		return $this->m_fltMonthlyRecurringAmount;
	}

	public function sqlMonthlyRecurringAmount() {
		return ( true == isset( $this->m_fltMonthlyRecurringAmount ) ) ? ( string ) $this->m_fltMonthlyRecurringAmount : 'NULL';
	}

	public function setMonthlyChangeAmount( $fltMonthlyChangeAmount ) {
		$this->set( 'm_fltMonthlyChangeAmount', CStrings::strToFloatDef( $fltMonthlyChangeAmount, NULL, false, 0 ) );
	}

	public function getMonthlyChangeAmount() {
		return $this->m_fltMonthlyChangeAmount;
	}

	public function sqlMonthlyChangeAmount() {
		return ( true == isset( $this->m_fltMonthlyChangeAmount ) ) ? ( string ) $this->m_fltMonthlyChangeAmount : 'NULL';
	}

	public function setTransactionalAmount( $fltTransactionalAmount ) {
		$this->set( 'm_fltTransactionalAmount', CStrings::strToFloatDef( $fltTransactionalAmount, NULL, false, 0 ) );
	}

	public function getTransactionalAmount() {
		return $this->m_fltTransactionalAmount;
	}

	public function sqlTransactionalAmount() {
		return ( true == isset( $this->m_fltTransactionalAmount ) ) ? ( string ) $this->m_fltTransactionalAmount : 'NULL';
	}

	public function setTransactionalChangeAmount( $fltTransactionalChangeAmount ) {
		$this->set( 'm_fltTransactionalChangeAmount', CStrings::strToFloatDef( $fltTransactionalChangeAmount, NULL, false, 0 ) );
	}

	public function getTransactionalChangeAmount() {
		return $this->m_fltTransactionalChangeAmount;
	}

	public function sqlTransactionalChangeAmount() {
		return ( true == isset( $this->m_fltTransactionalChangeAmount ) ) ? ( string ) $this->m_fltTransactionalChangeAmount : 'NULL';
	}

	public function setImplementationAmount( $fltImplementationAmount ) {
		$this->set( 'm_fltImplementationAmount', CStrings::strToFloatDef( $fltImplementationAmount, NULL, false, 0 ) );
	}

	public function getImplementationAmount() {
		return $this->m_fltImplementationAmount;
	}

	public function sqlImplementationAmount() {
		return ( true == isset( $this->m_fltImplementationAmount ) ) ? ( string ) $this->m_fltImplementationAmount : 'NULL';
	}

	public function setContractTerminationReason( $strContractTerminationReason ) {
		$this->set( 'm_strContractTerminationReason', CStrings::strTrimDef( $strContractTerminationReason, 20, NULL, true ) );
	}

	public function getContractTerminationReason() {
		return $this->m_strContractTerminationReason;
	}

	public function sqlContractTerminationReason() {
		return ( true == isset( $this->m_strContractTerminationReason ) ) ? '\'' . addslashes( $this->m_strContractTerminationReason ) . '\'' : 'NULL';
	}

	public function setIsFirstClientProductPropertyRecord( $intIsFirstClientProductPropertyRecord ) {
		$this->set( 'm_intIsFirstClientProductPropertyRecord', CStrings::strToIntDef( $intIsFirstClientProductPropertyRecord, NULL, false ) );
	}

	public function getIsFirstClientProductPropertyRecord() {
		return $this->m_intIsFirstClientProductPropertyRecord;
	}

	public function sqlIsFirstClientProductPropertyRecord() {
		return ( true == isset( $this->m_intIsFirstClientProductPropertyRecord ) ) ? ( string ) $this->m_intIsFirstClientProductPropertyRecord : 'NULL';
	}

	public function setIsLastClientProductPropertyRecord( $intIsLastClientProductPropertyRecord ) {
		$this->set( 'm_intIsLastClientProductPropertyRecord', CStrings::strToIntDef( $intIsLastClientProductPropertyRecord, NULL, false ) );
	}

	public function getIsLastClientProductPropertyRecord() {
		return $this->m_intIsLastClientProductPropertyRecord;
	}

	public function sqlIsLastClientProductPropertyRecord() {
		return ( true == isset( $this->m_intIsLastClientProductPropertyRecord ) ) ? ( string ) $this->m_intIsLastClientProductPropertyRecord : 'NULL';
	}

	public function setIsFirstContractPropertyRecord( $intIsFirstContractPropertyRecord ) {
		$this->set( 'm_intIsFirstContractPropertyRecord', CStrings::strToIntDef( $intIsFirstContractPropertyRecord, NULL, false ) );
	}

	public function getIsFirstContractPropertyRecord() {
		return $this->m_intIsFirstContractPropertyRecord;
	}

	public function sqlIsFirstContractPropertyRecord() {
		return ( true == isset( $this->m_intIsFirstContractPropertyRecord ) ) ? ( string ) $this->m_intIsFirstContractPropertyRecord : 'NULL';
	}

	public function setIsLastContractPropertyRecord( $intIsLastContractPropertyRecord ) {
		$this->set( 'm_intIsLastContractPropertyRecord', CStrings::strToIntDef( $intIsLastContractPropertyRecord, NULL, false ) );
	}

	public function getIsLastContractPropertyRecord() {
		return $this->m_intIsLastContractPropertyRecord;
	}

	public function sqlIsLastContractPropertyRecord() {
		return ( true == isset( $this->m_intIsLastContractPropertyRecord ) ) ? ( string ) $this->m_intIsLastContractPropertyRecord : 'NULL';
	}

	public function setClientPropertyEntrataCoreStartDate( $strClientPropertyEntrataCoreStartDate ) {
		$this->set( 'm_strClientPropertyEntrataCoreStartDate', CStrings::strTrimDef( $strClientPropertyEntrataCoreStartDate, -1, NULL, true ) );
	}

	public function getClientPropertyEntrataCoreStartDate() {
		return $this->m_strClientPropertyEntrataCoreStartDate;
	}

	public function sqlClientPropertyEntrataCoreStartDate() {
		return ( true == isset( $this->m_strClientPropertyEntrataCoreStartDate ) ) ? '\'' . $this->m_strClientPropertyEntrataCoreStartDate . '\'' : 'NULL';
	}

	public function setClientPropertyEntrataCoreLastActiveDate( $strClientPropertyEntrataCoreLastActiveDate ) {
		$this->set( 'm_strClientPropertyEntrataCoreLastActiveDate', CStrings::strTrimDef( $strClientPropertyEntrataCoreLastActiveDate, -1, NULL, true ) );
	}

	public function getClientPropertyEntrataCoreLastActiveDate() {
		return $this->m_strClientPropertyEntrataCoreLastActiveDate;
	}

	public function sqlClientPropertyEntrataCoreLastActiveDate() {
		return ( true == isset( $this->m_strClientPropertyEntrataCoreLastActiveDate ) ) ? '\'' . $this->m_strClientPropertyEntrataCoreLastActiveDate . '\'' : 'NULL';
	}

	public function setIsTransactionalProduct( $intIsTransactionalProduct ) {
		$this->set( 'm_intIsTransactionalProduct', CStrings::strToIntDef( $intIsTransactionalProduct, NULL, false ) );
	}

	public function getIsTransactionalProduct() {
		return $this->m_intIsTransactionalProduct;
	}

	public function sqlIsTransactionalProduct() {
		return ( true == isset( $this->m_intIsTransactionalProduct ) ) ? ( string ) $this->m_intIsTransactionalProduct : 'NULL';
	}

	public function setIsPilot( $intIsPilot ) {
		$this->set( 'm_intIsPilot', CStrings::strToIntDef( $intIsPilot, NULL, false ) );
	}

	public function getIsPilot() {
		return $this->m_intIsPilot;
	}

	public function sqlIsPilot() {
		return ( true == isset( $this->m_intIsPilot ) ) ? ( string ) $this->m_intIsPilot : 'NULL';
	}

	public function setIsContractProperty( $intIsContractProperty ) {
		$this->set( 'm_intIsContractProperty', CStrings::strToIntDef( $intIsContractProperty, NULL, false ) );
	}

	public function getIsContractProperty() {
		return $this->m_intIsContractProperty;
	}

	public function sqlIsContractProperty() {
		return ( true == isset( $this->m_intIsContractProperty ) ) ? ( string ) $this->m_intIsContractProperty : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, contract_id, ps_lead_id, cid, ps_product_id, property_id, contract_product_id, contract_property_id, renewal_contract_property_id, original_contract_property_id, contract_type_id, contract_status_type_id, company_status_type_id, commission_bucket_id, contract_termination_request_id, currency_code, company_name, product_name, property_name, property_type, property_unit_count, contract_product_unit_count, contract_product_subscription_acv, contract_product_transaction_acv, client_start_date, client_last_active_date, client_is_active, client_product_start_date, client_product_last_active_date, client_product_is_active, client_property_start_date, client_property_last_active_date, client_property_is_active, client_product_property_start_date, client_product_property_last_active_date, client_product_property_is_active, contract_property_start_date, contract_property_last_active_date, contract_property_is_active, contract_property_implementation_end_date, contract_property_implementation_date, contract_property_is_implemented, close_date, contract_start_date, contract_entered_pipeline_on, proposal_sent_on, contract_won_on, contract_lost_on, contract_terminated_on, implemented_on, implementation_end_date, deactivation_date, implementation_employee_id, implementation_employee_name, implementation_weight_score, implementation_delay_type_id, implementation_delay_type, implemented_by, billing_started_on, monthly_recurring_amount, monthly_change_amount, transactional_amount, transactional_change_amount, implementation_amount, contract_termination_reason, is_first_client_product_property_record, is_last_client_product_property_record, is_first_contract_property_record, is_last_contract_property_record, client_property_entrata_core_start_date, client_property_entrata_core_last_active_date, is_transactional_product, is_pilot, is_contract_property, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlContractId() . ', ' .
						$this->sqlPsLeadId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlContractProductId() . ', ' .
						$this->sqlContractPropertyId() . ', ' .
						$this->sqlRenewalContractPropertyId() . ', ' .
						$this->sqlOriginalContractPropertyId() . ', ' .
						$this->sqlContractTypeId() . ', ' .
						$this->sqlContractStatusTypeId() . ', ' .
						$this->sqlCompanyStatusTypeId() . ', ' .
						$this->sqlCommissionBucketId() . ', ' .
						$this->sqlContractTerminationRequestId() . ', ' .
						$this->sqlCurrencyCode() . ', ' .
						$this->sqlCompanyName() . ', ' .
						$this->sqlProductName() . ', ' .
						$this->sqlPropertyName() . ', ' .
						$this->sqlPropertyType() . ', ' .
						$this->sqlPropertyUnitCount() . ', ' .
						$this->sqlContractProductUnitCount() . ', ' .
						$this->sqlContractProductSubscriptionAcv() . ', ' .
						$this->sqlContractProductTransactionAcv() . ', ' .
						$this->sqlClientStartDate() . ', ' .
						$this->sqlClientLastActiveDate() . ', ' .
						$this->sqlClientIsActive() . ', ' .
						$this->sqlClientProductStartDate() . ', ' .
						$this->sqlClientProductLastActiveDate() . ', ' .
						$this->sqlClientProductIsActive() . ', ' .
						$this->sqlClientPropertyStartDate() . ', ' .
						$this->sqlClientPropertyLastActiveDate() . ', ' .
						$this->sqlClientPropertyIsActive() . ', ' .
						$this->sqlClientProductPropertyStartDate() . ', ' .
						$this->sqlClientProductPropertyLastActiveDate() . ', ' .
						$this->sqlClientProductPropertyIsActive() . ', ' .
						$this->sqlContractPropertyStartDate() . ', ' .
						$this->sqlContractPropertyLastActiveDate() . ', ' .
						$this->sqlContractPropertyIsActive() . ', ' .
						$this->sqlContractPropertyImplementationEndDate() . ', ' .
						$this->sqlContractPropertyImplementationDate() . ', ' .
						$this->sqlContractPropertyIsImplemented() . ', ' .
						$this->sqlCloseDate() . ', ' .
						$this->sqlContractStartDate() . ', ' .
						$this->sqlContractEnteredPipelineOn() . ', ' .
						$this->sqlProposalSentOn() . ', ' .
						$this->sqlContractWonOn() . ', ' .
						$this->sqlContractLostOn() . ', ' .
						$this->sqlContractTerminatedOn() . ', ' .
						$this->sqlImplementedOn() . ', ' .
						$this->sqlImplementationEndDate() . ', ' .
						$this->sqlDeactivationDate() . ', ' .
						$this->sqlImplementationEmployeeId() . ', ' .
						$this->sqlImplementationEmployeeName() . ', ' .
						$this->sqlImplementationWeightScore() . ', ' .
						$this->sqlImplementationDelayTypeId() . ', ' .
						$this->sqlImplementationDelayType() . ', ' .
						$this->sqlImplementedBy() . ', ' .
						$this->sqlBillingStartedOn() . ', ' .
						$this->sqlMonthlyRecurringAmount() . ', ' .
						$this->sqlMonthlyChangeAmount() . ', ' .
						$this->sqlTransactionalAmount() . ', ' .
						$this->sqlTransactionalChangeAmount() . ', ' .
						$this->sqlImplementationAmount() . ', ' .
						$this->sqlContractTerminationReason() . ', ' .
						$this->sqlIsFirstClientProductPropertyRecord() . ', ' .
						$this->sqlIsLastClientProductPropertyRecord() . ', ' .
						$this->sqlIsFirstContractPropertyRecord() . ', ' .
						$this->sqlIsLastContractPropertyRecord() . ', ' .
						$this->sqlClientPropertyEntrataCoreStartDate() . ', ' .
						$this->sqlClientPropertyEntrataCoreLastActiveDate() . ', ' .
						$this->sqlIsTransactionalProduct() . ', ' .
						$this->sqlIsPilot() . ', ' .
						$this->sqlIsContractProperty() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId(). ',' ; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_product_id = ' . $this->sqlContractProductId(). ',' ; } elseif( true == array_key_exists( 'ContractProductId', $this->getChangedColumns() ) ) { $strSql .= ' contract_product_id = ' . $this->sqlContractProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_property_id = ' . $this->sqlContractPropertyId(). ',' ; } elseif( true == array_key_exists( 'ContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' contract_property_id = ' . $this->sqlContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' renewal_contract_property_id = ' . $this->sqlRenewalContractPropertyId(). ',' ; } elseif( true == array_key_exists( 'RenewalContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' renewal_contract_property_id = ' . $this->sqlRenewalContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_contract_property_id = ' . $this->sqlOriginalContractPropertyId(). ',' ; } elseif( true == array_key_exists( 'OriginalContractPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' original_contract_property_id = ' . $this->sqlOriginalContractPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_type_id = ' . $this->sqlContractTypeId(). ',' ; } elseif( true == array_key_exists( 'ContractTypeId', $this->getChangedColumns() ) ) { $strSql .= ' contract_type_id = ' . $this->sqlContractTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_status_type_id = ' . $this->sqlContractStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ContractStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' contract_status_type_id = ' . $this->sqlContractStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_status_type_id = ' . $this->sqlCompanyStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'CompanyStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' company_status_type_id = ' . $this->sqlCompanyStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_bucket_id = ' . $this->sqlCommissionBucketId(). ',' ; } elseif( true == array_key_exists( 'CommissionBucketId', $this->getChangedColumns() ) ) { $strSql .= ' commission_bucket_id = ' . $this->sqlCommissionBucketId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_termination_request_id = ' . $this->sqlContractTerminationRequestId(). ',' ; } elseif( true == array_key_exists( 'ContractTerminationRequestId', $this->getChangedColumns() ) ) { $strSql .= ' contract_termination_request_id = ' . $this->sqlContractTerminationRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode(). ',' ; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName(). ',' ; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_name = ' . $this->sqlProductName(). ',' ; } elseif( true == array_key_exists( 'ProductName', $this->getChangedColumns() ) ) { $strSql .= ' product_name = ' . $this->sqlProductName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_name = ' . $this->sqlPropertyName(). ',' ; } elseif( true == array_key_exists( 'PropertyName', $this->getChangedColumns() ) ) { $strSql .= ' property_name = ' . $this->sqlPropertyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_type = ' . $this->sqlPropertyType(). ',' ; } elseif( true == array_key_exists( 'PropertyType', $this->getChangedColumns() ) ) { $strSql .= ' property_type = ' . $this->sqlPropertyType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_unit_count = ' . $this->sqlPropertyUnitCount(). ',' ; } elseif( true == array_key_exists( 'PropertyUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' property_unit_count = ' . $this->sqlPropertyUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_product_unit_count = ' . $this->sqlContractProductUnitCount(). ',' ; } elseif( true == array_key_exists( 'ContractProductUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' contract_product_unit_count = ' . $this->sqlContractProductUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_product_subscription_acv = ' . $this->sqlContractProductSubscriptionAcv(). ',' ; } elseif( true == array_key_exists( 'ContractProductSubscriptionAcv', $this->getChangedColumns() ) ) { $strSql .= ' contract_product_subscription_acv = ' . $this->sqlContractProductSubscriptionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_product_transaction_acv = ' . $this->sqlContractProductTransactionAcv(). ',' ; } elseif( true == array_key_exists( 'ContractProductTransactionAcv', $this->getChangedColumns() ) ) { $strSql .= ' contract_product_transaction_acv = ' . $this->sqlContractProductTransactionAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_start_date = ' . $this->sqlClientStartDate(). ',' ; } elseif( true == array_key_exists( 'ClientStartDate', $this->getChangedColumns() ) ) { $strSql .= ' client_start_date = ' . $this->sqlClientStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_last_active_date = ' . $this->sqlClientLastActiveDate(). ',' ; } elseif( true == array_key_exists( 'ClientLastActiveDate', $this->getChangedColumns() ) ) { $strSql .= ' client_last_active_date = ' . $this->sqlClientLastActiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_is_active = ' . $this->sqlClientIsActive(). ',' ; } elseif( true == array_key_exists( 'ClientIsActive', $this->getChangedColumns() ) ) { $strSql .= ' client_is_active = ' . $this->sqlClientIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_product_start_date = ' . $this->sqlClientProductStartDate(). ',' ; } elseif( true == array_key_exists( 'ClientProductStartDate', $this->getChangedColumns() ) ) { $strSql .= ' client_product_start_date = ' . $this->sqlClientProductStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_product_last_active_date = ' . $this->sqlClientProductLastActiveDate(). ',' ; } elseif( true == array_key_exists( 'ClientProductLastActiveDate', $this->getChangedColumns() ) ) { $strSql .= ' client_product_last_active_date = ' . $this->sqlClientProductLastActiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_product_is_active = ' . $this->sqlClientProductIsActive(). ',' ; } elseif( true == array_key_exists( 'ClientProductIsActive', $this->getChangedColumns() ) ) { $strSql .= ' client_product_is_active = ' . $this->sqlClientProductIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_property_start_date = ' . $this->sqlClientPropertyStartDate(). ',' ; } elseif( true == array_key_exists( 'ClientPropertyStartDate', $this->getChangedColumns() ) ) { $strSql .= ' client_property_start_date = ' . $this->sqlClientPropertyStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_property_last_active_date = ' . $this->sqlClientPropertyLastActiveDate(). ',' ; } elseif( true == array_key_exists( 'ClientPropertyLastActiveDate', $this->getChangedColumns() ) ) { $strSql .= ' client_property_last_active_date = ' . $this->sqlClientPropertyLastActiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_property_is_active = ' . $this->sqlClientPropertyIsActive(). ',' ; } elseif( true == array_key_exists( 'ClientPropertyIsActive', $this->getChangedColumns() ) ) { $strSql .= ' client_property_is_active = ' . $this->sqlClientPropertyIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_product_property_start_date = ' . $this->sqlClientProductPropertyStartDate(). ',' ; } elseif( true == array_key_exists( 'ClientProductPropertyStartDate', $this->getChangedColumns() ) ) { $strSql .= ' client_product_property_start_date = ' . $this->sqlClientProductPropertyStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_product_property_last_active_date = ' . $this->sqlClientProductPropertyLastActiveDate(). ',' ; } elseif( true == array_key_exists( 'ClientProductPropertyLastActiveDate', $this->getChangedColumns() ) ) { $strSql .= ' client_product_property_last_active_date = ' . $this->sqlClientProductPropertyLastActiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_product_property_is_active = ' . $this->sqlClientProductPropertyIsActive(). ',' ; } elseif( true == array_key_exists( 'ClientProductPropertyIsActive', $this->getChangedColumns() ) ) { $strSql .= ' client_product_property_is_active = ' . $this->sqlClientProductPropertyIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_property_start_date = ' . $this->sqlContractPropertyStartDate(). ',' ; } elseif( true == array_key_exists( 'ContractPropertyStartDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_property_start_date = ' . $this->sqlContractPropertyStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_property_last_active_date = ' . $this->sqlContractPropertyLastActiveDate(). ',' ; } elseif( true == array_key_exists( 'ContractPropertyLastActiveDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_property_last_active_date = ' . $this->sqlContractPropertyLastActiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_property_is_active = ' . $this->sqlContractPropertyIsActive(). ',' ; } elseif( true == array_key_exists( 'ContractPropertyIsActive', $this->getChangedColumns() ) ) { $strSql .= ' contract_property_is_active = ' . $this->sqlContractPropertyIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_property_implementation_end_date = ' . $this->sqlContractPropertyImplementationEndDate(). ',' ; } elseif( true == array_key_exists( 'ContractPropertyImplementationEndDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_property_implementation_end_date = ' . $this->sqlContractPropertyImplementationEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_property_implementation_date = ' . $this->sqlContractPropertyImplementationDate(). ',' ; } elseif( true == array_key_exists( 'ContractPropertyImplementationDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_property_implementation_date = ' . $this->sqlContractPropertyImplementationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_property_is_implemented = ' . $this->sqlContractPropertyIsImplemented(). ',' ; } elseif( true == array_key_exists( 'ContractPropertyIsImplemented', $this->getChangedColumns() ) ) { $strSql .= ' contract_property_is_implemented = ' . $this->sqlContractPropertyIsImplemented() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' close_date = ' . $this->sqlCloseDate(). ',' ; } elseif( true == array_key_exists( 'CloseDate', $this->getChangedColumns() ) ) { $strSql .= ' close_date = ' . $this->sqlCloseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_start_date = ' . $this->sqlContractStartDate(). ',' ; } elseif( true == array_key_exists( 'ContractStartDate', $this->getChangedColumns() ) ) { $strSql .= ' contract_start_date = ' . $this->sqlContractStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_entered_pipeline_on = ' . $this->sqlContractEnteredPipelineOn(). ',' ; } elseif( true == array_key_exists( 'ContractEnteredPipelineOn', $this->getChangedColumns() ) ) { $strSql .= ' contract_entered_pipeline_on = ' . $this->sqlContractEnteredPipelineOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposal_sent_on = ' . $this->sqlProposalSentOn(). ',' ; } elseif( true == array_key_exists( 'ProposalSentOn', $this->getChangedColumns() ) ) { $strSql .= ' proposal_sent_on = ' . $this->sqlProposalSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_won_on = ' . $this->sqlContractWonOn(). ',' ; } elseif( true == array_key_exists( 'ContractWonOn', $this->getChangedColumns() ) ) { $strSql .= ' contract_won_on = ' . $this->sqlContractWonOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_lost_on = ' . $this->sqlContractLostOn(). ',' ; } elseif( true == array_key_exists( 'ContractLostOn', $this->getChangedColumns() ) ) { $strSql .= ' contract_lost_on = ' . $this->sqlContractLostOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_terminated_on = ' . $this->sqlContractTerminatedOn(). ',' ; } elseif( true == array_key_exists( 'ContractTerminatedOn', $this->getChangedColumns() ) ) { $strSql .= ' contract_terminated_on = ' . $this->sqlContractTerminatedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implemented_on = ' . $this->sqlImplementedOn(). ',' ; } elseif( true == array_key_exists( 'ImplementedOn', $this->getChangedColumns() ) ) { $strSql .= ' implemented_on = ' . $this->sqlImplementedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_end_date = ' . $this->sqlImplementationEndDate(). ',' ; } elseif( true == array_key_exists( 'ImplementationEndDate', $this->getChangedColumns() ) ) { $strSql .= ' implementation_end_date = ' . $this->sqlImplementationEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deactivation_date = ' . $this->sqlDeactivationDate(). ',' ; } elseif( true == array_key_exists( 'DeactivationDate', $this->getChangedColumns() ) ) { $strSql .= ' deactivation_date = ' . $this->sqlDeactivationDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_employee_id = ' . $this->sqlImplementationEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ImplementationEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_employee_id = ' . $this->sqlImplementationEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_employee_name = ' . $this->sqlImplementationEmployeeName(). ',' ; } elseif( true == array_key_exists( 'ImplementationEmployeeName', $this->getChangedColumns() ) ) { $strSql .= ' implementation_employee_name = ' . $this->sqlImplementationEmployeeName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_weight_score = ' . $this->sqlImplementationWeightScore(). ',' ; } elseif( true == array_key_exists( 'ImplementationWeightScore', $this->getChangedColumns() ) ) { $strSql .= ' implementation_weight_score = ' . $this->sqlImplementationWeightScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_delay_type_id = ' . $this->sqlImplementationDelayTypeId(). ',' ; } elseif( true == array_key_exists( 'ImplementationDelayTypeId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_delay_type_id = ' . $this->sqlImplementationDelayTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_delay_type = ' . $this->sqlImplementationDelayType(). ',' ; } elseif( true == array_key_exists( 'ImplementationDelayType', $this->getChangedColumns() ) ) { $strSql .= ' implementation_delay_type = ' . $this->sqlImplementationDelayType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implemented_by = ' . $this->sqlImplementedBy(). ',' ; } elseif( true == array_key_exists( 'ImplementedBy', $this->getChangedColumns() ) ) { $strSql .= ' implemented_by = ' . $this->sqlImplementedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_started_on = ' . $this->sqlBillingStartedOn(). ',' ; } elseif( true == array_key_exists( 'BillingStartedOn', $this->getChangedColumns() ) ) { $strSql .= ' billing_started_on = ' . $this->sqlBillingStartedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_recurring_amount = ' . $this->sqlMonthlyRecurringAmount(). ',' ; } elseif( true == array_key_exists( 'MonthlyRecurringAmount', $this->getChangedColumns() ) ) { $strSql .= ' monthly_recurring_amount = ' . $this->sqlMonthlyRecurringAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_change_amount = ' . $this->sqlMonthlyChangeAmount(). ',' ; } elseif( true == array_key_exists( 'MonthlyChangeAmount', $this->getChangedColumns() ) ) { $strSql .= ' monthly_change_amount = ' . $this->sqlMonthlyChangeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transactional_amount = ' . $this->sqlTransactionalAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionalAmount', $this->getChangedColumns() ) ) { $strSql .= ' transactional_amount = ' . $this->sqlTransactionalAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transactional_change_amount = ' . $this->sqlTransactionalChangeAmount(). ',' ; } elseif( true == array_key_exists( 'TransactionalChangeAmount', $this->getChangedColumns() ) ) { $strSql .= ' transactional_change_amount = ' . $this->sqlTransactionalChangeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_amount = ' . $this->sqlImplementationAmount(). ',' ; } elseif( true == array_key_exists( 'ImplementationAmount', $this->getChangedColumns() ) ) { $strSql .= ' implementation_amount = ' . $this->sqlImplementationAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_termination_reason = ' . $this->sqlContractTerminationReason(). ',' ; } elseif( true == array_key_exists( 'ContractTerminationReason', $this->getChangedColumns() ) ) { $strSql .= ' contract_termination_reason = ' . $this->sqlContractTerminationReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_first_client_product_property_record = ' . $this->sqlIsFirstClientProductPropertyRecord(). ',' ; } elseif( true == array_key_exists( 'IsFirstClientProductPropertyRecord', $this->getChangedColumns() ) ) { $strSql .= ' is_first_client_product_property_record = ' . $this->sqlIsFirstClientProductPropertyRecord() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_last_client_product_property_record = ' . $this->sqlIsLastClientProductPropertyRecord(). ',' ; } elseif( true == array_key_exists( 'IsLastClientProductPropertyRecord', $this->getChangedColumns() ) ) { $strSql .= ' is_last_client_product_property_record = ' . $this->sqlIsLastClientProductPropertyRecord() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_first_contract_property_record = ' . $this->sqlIsFirstContractPropertyRecord(). ',' ; } elseif( true == array_key_exists( 'IsFirstContractPropertyRecord', $this->getChangedColumns() ) ) { $strSql .= ' is_first_contract_property_record = ' . $this->sqlIsFirstContractPropertyRecord() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_last_contract_property_record = ' . $this->sqlIsLastContractPropertyRecord(). ',' ; } elseif( true == array_key_exists( 'IsLastContractPropertyRecord', $this->getChangedColumns() ) ) { $strSql .= ' is_last_contract_property_record = ' . $this->sqlIsLastContractPropertyRecord() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_property_entrata_core_start_date = ' . $this->sqlClientPropertyEntrataCoreStartDate(). ',' ; } elseif( true == array_key_exists( 'ClientPropertyEntrataCoreStartDate', $this->getChangedColumns() ) ) { $strSql .= ' client_property_entrata_core_start_date = ' . $this->sqlClientPropertyEntrataCoreStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_property_entrata_core_last_active_date = ' . $this->sqlClientPropertyEntrataCoreLastActiveDate(). ',' ; } elseif( true == array_key_exists( 'ClientPropertyEntrataCoreLastActiveDate', $this->getChangedColumns() ) ) { $strSql .= ' client_property_entrata_core_last_active_date = ' . $this->sqlClientPropertyEntrataCoreLastActiveDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_transactional_product = ' . $this->sqlIsTransactionalProduct(). ',' ; } elseif( true == array_key_exists( 'IsTransactionalProduct', $this->getChangedColumns() ) ) { $strSql .= ' is_transactional_product = ' . $this->sqlIsTransactionalProduct() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pilot = ' . $this->sqlIsPilot(). ',' ; } elseif( true == array_key_exists( 'IsPilot', $this->getChangedColumns() ) ) { $strSql .= ' is_pilot = ' . $this->sqlIsPilot() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_contract_property = ' . $this->sqlIsContractProperty() ; } elseif( true == array_key_exists( 'IsContractProperty', $this->getChangedColumns() ) ) { $strSql .= ' is_contract_property = ' . $this->sqlIsContractProperty() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'contract_id' => $this->getContractId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'cid' => $this->getCid(),
			'ps_product_id' => $this->getPsProductId(),
			'property_id' => $this->getPropertyId(),
			'contract_product_id' => $this->getContractProductId(),
			'contract_property_id' => $this->getContractPropertyId(),
			'renewal_contract_property_id' => $this->getRenewalContractPropertyId(),
			'original_contract_property_id' => $this->getOriginalContractPropertyId(),
			'contract_type_id' => $this->getContractTypeId(),
			'contract_status_type_id' => $this->getContractStatusTypeId(),
			'company_status_type_id' => $this->getCompanyStatusTypeId(),
			'commission_bucket_id' => $this->getCommissionBucketId(),
			'contract_termination_request_id' => $this->getContractTerminationRequestId(),
			'currency_code' => $this->getCurrencyCode(),
			'company_name' => $this->getCompanyName(),
			'product_name' => $this->getProductName(),
			'property_name' => $this->getPropertyName(),
			'property_type' => $this->getPropertyType(),
			'property_unit_count' => $this->getPropertyUnitCount(),
			'contract_product_unit_count' => $this->getContractProductUnitCount(),
			'contract_product_subscription_acv' => $this->getContractProductSubscriptionAcv(),
			'contract_product_transaction_acv' => $this->getContractProductTransactionAcv(),
			'client_start_date' => $this->getClientStartDate(),
			'client_last_active_date' => $this->getClientLastActiveDate(),
			'client_is_active' => $this->getClientIsActive(),
			'client_product_start_date' => $this->getClientProductStartDate(),
			'client_product_last_active_date' => $this->getClientProductLastActiveDate(),
			'client_product_is_active' => $this->getClientProductIsActive(),
			'client_property_start_date' => $this->getClientPropertyStartDate(),
			'client_property_last_active_date' => $this->getClientPropertyLastActiveDate(),
			'client_property_is_active' => $this->getClientPropertyIsActive(),
			'client_product_property_start_date' => $this->getClientProductPropertyStartDate(),
			'client_product_property_last_active_date' => $this->getClientProductPropertyLastActiveDate(),
			'client_product_property_is_active' => $this->getClientProductPropertyIsActive(),
			'contract_property_start_date' => $this->getContractPropertyStartDate(),
			'contract_property_last_active_date' => $this->getContractPropertyLastActiveDate(),
			'contract_property_is_active' => $this->getContractPropertyIsActive(),
			'contract_property_implementation_end_date' => $this->getContractPropertyImplementationEndDate(),
			'contract_property_implementation_date' => $this->getContractPropertyImplementationDate(),
			'contract_property_is_implemented' => $this->getContractPropertyIsImplemented(),
			'close_date' => $this->getCloseDate(),
			'contract_start_date' => $this->getContractStartDate(),
			'contract_entered_pipeline_on' => $this->getContractEnteredPipelineOn(),
			'proposal_sent_on' => $this->getProposalSentOn(),
			'contract_won_on' => $this->getContractWonOn(),
			'contract_lost_on' => $this->getContractLostOn(),
			'contract_terminated_on' => $this->getContractTerminatedOn(),
			'implemented_on' => $this->getImplementedOn(),
			'implementation_end_date' => $this->getImplementationEndDate(),
			'deactivation_date' => $this->getDeactivationDate(),
			'implementation_employee_id' => $this->getImplementationEmployeeId(),
			'implementation_employee_name' => $this->getImplementationEmployeeName(),
			'implementation_weight_score' => $this->getImplementationWeightScore(),
			'implementation_delay_type_id' => $this->getImplementationDelayTypeId(),
			'implementation_delay_type' => $this->getImplementationDelayType(),
			'implemented_by' => $this->getImplementedBy(),
			'billing_started_on' => $this->getBillingStartedOn(),
			'monthly_recurring_amount' => $this->getMonthlyRecurringAmount(),
			'monthly_change_amount' => $this->getMonthlyChangeAmount(),
			'transactional_amount' => $this->getTransactionalAmount(),
			'transactional_change_amount' => $this->getTransactionalChangeAmount(),
			'implementation_amount' => $this->getImplementationAmount(),
			'contract_termination_reason' => $this->getContractTerminationReason(),
			'is_first_client_product_property_record' => $this->getIsFirstClientProductPropertyRecord(),
			'is_last_client_product_property_record' => $this->getIsLastClientProductPropertyRecord(),
			'is_first_contract_property_record' => $this->getIsFirstContractPropertyRecord(),
			'is_last_contract_property_record' => $this->getIsLastContractPropertyRecord(),
			'client_property_entrata_core_start_date' => $this->getClientPropertyEntrataCoreStartDate(),
			'client_property_entrata_core_last_active_date' => $this->getClientPropertyEntrataCoreLastActiveDate(),
			'is_transactional_product' => $this->getIsTransactionalProduct(),
			'is_pilot' => $this->getIsPilot(),
			'is_contract_property' => $this->getIsContractProperty(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>