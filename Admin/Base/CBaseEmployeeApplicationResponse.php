<?php

class CBaseEmployeeApplicationResponse extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_application_responses';

	protected $m_intId;
	protected $m_intEmployeeApplicationId;
	protected $m_intEmploymentApplicationQuestionId;
	protected $m_intEmploymentApplicationId;
	protected $m_strQuestion;
	protected $m_strAnswer;
	protected $m_intIsRating;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsRating = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationId', trim( $arrValues['employee_application_id'] ) ); elseif( isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( isset( $arrValues['employment_application_question_id'] ) && $boolDirectSet ) $this->set( 'm_intEmploymentApplicationQuestionId', trim( $arrValues['employment_application_question_id'] ) ); elseif( isset( $arrValues['employment_application_question_id'] ) ) $this->setEmploymentApplicationQuestionId( $arrValues['employment_application_question_id'] );
		if( isset( $arrValues['employment_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmploymentApplicationId', trim( $arrValues['employment_application_id'] ) ); elseif( isset( $arrValues['employment_application_id'] ) ) $this->setEmploymentApplicationId( $arrValues['employment_application_id'] );
		if( isset( $arrValues['question'] ) && $boolDirectSet ) $this->set( 'm_strQuestion', trim( stripcslashes( $arrValues['question'] ) ) ); elseif( isset( $arrValues['question'] ) ) $this->setQuestion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['question'] ) : $arrValues['question'] );
		if( isset( $arrValues['answer'] ) && $boolDirectSet ) $this->set( 'm_strAnswer', trim( stripcslashes( $arrValues['answer'] ) ) ); elseif( isset( $arrValues['answer'] ) ) $this->setAnswer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['answer'] ) : $arrValues['answer'] );
		if( isset( $arrValues['is_rating'] ) && $boolDirectSet ) $this->set( 'm_intIsRating', trim( $arrValues['is_rating'] ) ); elseif( isset( $arrValues['is_rating'] ) ) $this->setIsRating( $arrValues['is_rating'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeApplicationId( $intEmployeeApplicationId ) {
		$this->set( 'm_intEmployeeApplicationId', CStrings::strToIntDef( $intEmployeeApplicationId, NULL, false ) );
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function sqlEmployeeApplicationId() {
		return ( true == isset( $this->m_intEmployeeApplicationId ) ) ? ( string ) $this->m_intEmployeeApplicationId : 'NULL';
	}

	public function setEmploymentApplicationQuestionId( $intEmploymentApplicationQuestionId ) {
		$this->set( 'm_intEmploymentApplicationQuestionId', CStrings::strToIntDef( $intEmploymentApplicationQuestionId, NULL, false ) );
	}

	public function getEmploymentApplicationQuestionId() {
		return $this->m_intEmploymentApplicationQuestionId;
	}

	public function sqlEmploymentApplicationQuestionId() {
		return ( true == isset( $this->m_intEmploymentApplicationQuestionId ) ) ? ( string ) $this->m_intEmploymentApplicationQuestionId : 'NULL';
	}

	public function setEmploymentApplicationId( $intEmploymentApplicationId ) {
		$this->set( 'm_intEmploymentApplicationId', CStrings::strToIntDef( $intEmploymentApplicationId, NULL, false ) );
	}

	public function getEmploymentApplicationId() {
		return $this->m_intEmploymentApplicationId;
	}

	public function sqlEmploymentApplicationId() {
		return ( true == isset( $this->m_intEmploymentApplicationId ) ) ? ( string ) $this->m_intEmploymentApplicationId : 'NULL';
	}

	public function setQuestion( $strQuestion ) {
		$this->set( 'm_strQuestion', CStrings::strTrimDef( $strQuestion, 2000, NULL, true ) );
	}

	public function getQuestion() {
		return $this->m_strQuestion;
	}

	public function sqlQuestion() {
		return ( true == isset( $this->m_strQuestion ) ) ? '\'' . addslashes( $this->m_strQuestion ) . '\'' : 'NULL';
	}

	public function setAnswer( $strAnswer ) {
		$this->set( 'm_strAnswer', CStrings::strTrimDef( $strAnswer, -1, NULL, true ) );
	}

	public function getAnswer() {
		return $this->m_strAnswer;
	}

	public function sqlAnswer() {
		return ( true == isset( $this->m_strAnswer ) ) ? '\'' . addslashes( $this->m_strAnswer ) . '\'' : 'NULL';
	}

	public function setIsRating( $intIsRating ) {
		$this->set( 'm_intIsRating', CStrings::strToIntDef( $intIsRating, NULL, false ) );
	}

	public function getIsRating() {
		return $this->m_intIsRating;
	}

	public function sqlIsRating() {
		return ( true == isset( $this->m_intIsRating ) ) ? ( string ) $this->m_intIsRating : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_application_id, employment_application_question_id, employment_application_id, question, answer, is_rating, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeApplicationId() . ', ' .
 						$this->sqlEmploymentApplicationQuestionId() . ', ' .
 						$this->sqlEmploymentApplicationId() . ', ' .
 						$this->sqlQuestion() . ', ' .
 						$this->sqlAnswer() . ', ' .
 						$this->sqlIsRating() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employment_application_question_id = ' . $this->sqlEmploymentApplicationQuestionId() . ','; } elseif( true == array_key_exists( 'EmploymentApplicationQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' employment_application_question_id = ' . $this->sqlEmploymentApplicationQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employment_application_id = ' . $this->sqlEmploymentApplicationId() . ','; } elseif( true == array_key_exists( 'EmploymentApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employment_application_id = ' . $this->sqlEmploymentApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' question = ' . $this->sqlQuestion() . ','; } elseif( true == array_key_exists( 'Question', $this->getChangedColumns() ) ) { $strSql .= ' question = ' . $this->sqlQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' answer = ' . $this->sqlAnswer() . ','; } elseif( true == array_key_exists( 'Answer', $this->getChangedColumns() ) ) { $strSql .= ' answer = ' . $this->sqlAnswer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_rating = ' . $this->sqlIsRating() . ','; } elseif( true == array_key_exists( 'IsRating', $this->getChangedColumns() ) ) { $strSql .= ' is_rating = ' . $this->sqlIsRating() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_application_id' => $this->getEmployeeApplicationId(),
			'employment_application_question_id' => $this->getEmploymentApplicationQuestionId(),
			'employment_application_id' => $this->getEmploymentApplicationId(),
			'question' => $this->getQuestion(),
			'answer' => $this->getAnswer(),
			'is_rating' => $this->getIsRating(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>