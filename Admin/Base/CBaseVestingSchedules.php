<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CVestingSchedules
 * Do not add any new functions to this class.
 */

class CBaseVestingSchedules extends CEosPluralBase {

	/**
	 * @return CVestingSchedule[]
	 */
	public static function fetchVestingSchedules( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CVestingSchedule', $objDatabase );
	}

	/**
	 * @return CVestingSchedule
	 */
	public static function fetchVestingSchedule( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CVestingSchedule', $objDatabase );
	}

	public static function fetchVestingScheduleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'vesting_schedules', $objDatabase );
	}

	public static function fetchVestingScheduleById( $intId, $objDatabase ) {
		return self::fetchVestingSchedule( sprintf( 'SELECT * FROM vesting_schedules WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>