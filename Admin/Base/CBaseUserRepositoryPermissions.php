<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserRepositoryPermissions
 * Do not add any new functions to this class.
 */

class CBaseUserRepositoryPermissions extends CEosPluralBase {

	/**
	 * @return CUserRepositoryPermission[]
	 */
	public static function fetchUserRepositoryPermissions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserRepositoryPermission', $objDatabase );
	}

	/**
	 * @return CUserRepositoryPermission
	 */
	public static function fetchUserRepositoryPermission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserRepositoryPermission', $objDatabase );
	}

	public static function fetchUserRepositoryPermissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_repository_permissions', $objDatabase );
	}

	public static function fetchUserRepositoryPermissionById( $intId, $objDatabase ) {
		return self::fetchUserRepositoryPermission( sprintf( 'SELECT * FROM user_repository_permissions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserRepositoryPermissionsByUserId( $intUserId, $objDatabase ) {
		return self::fetchUserRepositoryPermissions( sprintf( 'SELECT * FROM user_repository_permissions WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

	public static function fetchUserRepositoryPermissionsByRepositoryId( $intRepositoryId, $objDatabase ) {
		return self::fetchUserRepositoryPermissions( sprintf( 'SELECT * FROM user_repository_permissions WHERE repository_id = %d', ( int ) $intRepositoryId ), $objDatabase );
	}

}
?>