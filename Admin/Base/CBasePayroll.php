<?php

class CBasePayroll extends CEosSingularBase {

	const TABLE_NAME = 'public.payrolls';

	protected $m_intId;
	protected $m_intPayrollPeriodId;
	protected $m_strBeginDate;
	protected $m_strEndDate;
	protected $m_intWorkingDays;
	protected $m_strSupportingData;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['payroll_period_id'] ) && $boolDirectSet ) $this->set( 'm_intPayrollPeriodId', trim( $arrValues['payroll_period_id'] ) ); elseif( isset( $arrValues['payroll_period_id'] ) ) $this->setPayrollPeriodId( $arrValues['payroll_period_id'] );
		if( isset( $arrValues['begin_date'] ) && $boolDirectSet ) $this->set( 'm_strBeginDate', trim( $arrValues['begin_date'] ) ); elseif( isset( $arrValues['begin_date'] ) ) $this->setBeginDate( $arrValues['begin_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['working_days'] ) && $boolDirectSet ) $this->set( 'm_intWorkingDays', trim( $arrValues['working_days'] ) ); elseif( isset( $arrValues['working_days'] ) ) $this->setWorkingDays( $arrValues['working_days'] );
		if( isset( $arrValues['supporting_data'] ) && $boolDirectSet ) $this->set( 'm_strSupportingData', trim( stripcslashes( $arrValues['supporting_data'] ) ) ); elseif( isset( $arrValues['supporting_data'] ) ) $this->setSupportingData( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['supporting_data'] ) : $arrValues['supporting_data'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPayrollPeriodId( $intPayrollPeriodId ) {
		$this->set( 'm_intPayrollPeriodId', CStrings::strToIntDef( $intPayrollPeriodId, NULL, false ) );
	}

	public function getPayrollPeriodId() {
		return $this->m_intPayrollPeriodId;
	}

	public function sqlPayrollPeriodId() {
		return ( true == isset( $this->m_intPayrollPeriodId ) ) ? ( string ) $this->m_intPayrollPeriodId : 'NULL';
	}

	public function setBeginDate( $strBeginDate ) {
		$this->set( 'm_strBeginDate', CStrings::strTrimDef( $strBeginDate, -1, NULL, true ) );
	}

	public function getBeginDate() {
		return $this->m_strBeginDate;
	}

	public function sqlBeginDate() {
		return ( true == isset( $this->m_strBeginDate ) ) ? '\'' . $this->m_strBeginDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NOW()';
	}

	public function setWorkingDays( $intWorkingDays ) {
		$this->set( 'm_intWorkingDays', CStrings::strToIntDef( $intWorkingDays, NULL, false ) );
	}

	public function getWorkingDays() {
		return $this->m_intWorkingDays;
	}

	public function sqlWorkingDays() {
		return ( true == isset( $this->m_intWorkingDays ) ) ? ( string ) $this->m_intWorkingDays : 'NULL';
	}

	public function setSupportingData( $strSupportingData ) {
		$this->set( 'm_strSupportingData', CStrings::strTrimDef( $strSupportingData, -1, NULL, true ) );
	}

	public function getSupportingData() {
		return $this->m_strSupportingData;
	}

	public function sqlSupportingData() {
		return ( true == isset( $this->m_strSupportingData ) ) ? '\'' . addslashes( $this->m_strSupportingData ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, payroll_period_id, begin_date, end_date, working_days, supporting_data, approved_by, approved_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPayrollPeriodId() . ', ' .
 						$this->sqlBeginDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlWorkingDays() . ', ' .
 						$this->sqlSupportingData() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payroll_period_id = ' . $this->sqlPayrollPeriodId() . ','; } elseif( true == array_key_exists( 'PayrollPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' payroll_period_id = ' . $this->sqlPayrollPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_date = ' . $this->sqlBeginDate() . ','; } elseif( true == array_key_exists( 'BeginDate', $this->getChangedColumns() ) ) { $strSql .= ' begin_date = ' . $this->sqlBeginDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' working_days = ' . $this->sqlWorkingDays() . ','; } elseif( true == array_key_exists( 'WorkingDays', $this->getChangedColumns() ) ) { $strSql .= ' working_days = ' . $this->sqlWorkingDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' supporting_data = ' . $this->sqlSupportingData() . ','; } elseif( true == array_key_exists( 'SupportingData', $this->getChangedColumns() ) ) { $strSql .= ' supporting_data = ' . $this->sqlSupportingData() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'payroll_period_id' => $this->getPayrollPeriodId(),
			'begin_date' => $this->getBeginDate(),
			'end_date' => $this->getEndDate(),
			'working_days' => $this->getWorkingDays(),
			'supporting_data' => $this->getSupportingData(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>