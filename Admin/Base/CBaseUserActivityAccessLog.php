<?php

class CBaseUserActivityAccessLog extends CEosSingularBase {

	const TABLE_NAME = 'public.user_activity_access_logs';

	protected $m_intUserId;
	protected $m_strHour;
	protected $m_intFirstMinute;
	protected $m_intLastMinute;

	public function __construct() {
		parent::__construct();

		$this->m_intFirstMinute = '0';
		$this->m_intLastMinute = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['hour'] ) && $boolDirectSet ) $this->set( 'm_strHour', trim( $arrValues['hour'] ) ); elseif( isset( $arrValues['hour'] ) ) $this->setHour( $arrValues['hour'] );
		if( isset( $arrValues['first_minute'] ) && $boolDirectSet ) $this->set( 'm_intFirstMinute', trim( $arrValues['first_minute'] ) ); elseif( isset( $arrValues['first_minute'] ) ) $this->setFirstMinute( $arrValues['first_minute'] );
		if( isset( $arrValues['last_minute'] ) && $boolDirectSet ) $this->set( 'm_intLastMinute', trim( $arrValues['last_minute'] ) ); elseif( isset( $arrValues['last_minute'] ) ) $this->setLastMinute( $arrValues['last_minute'] );
		$this->m_boolInitialized = true;
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setHour( $strHour ) {
		$this->set( 'm_strHour', CStrings::strTrimDef( $strHour, -1, NULL, true ) );
	}

	public function getHour() {
		return $this->m_strHour;
	}

	public function sqlHour() {
		return ( true == isset( $this->m_strHour ) ) ? '\'' . $this->m_strHour . '\'' : 'NOW()';
	}

	public function setFirstMinute( $intFirstMinute ) {
		$this->set( 'm_intFirstMinute', CStrings::strToIntDef( $intFirstMinute, NULL, false ) );
	}

	public function getFirstMinute() {
		return $this->m_intFirstMinute;
	}

	public function sqlFirstMinute() {
		return ( true == isset( $this->m_intFirstMinute ) ) ? ( string ) $this->m_intFirstMinute : '0';
	}

	public function setLastMinute( $intLastMinute ) {
		$this->set( 'm_intLastMinute', CStrings::strToIntDef( $intLastMinute, NULL, false ) );
	}

	public function getLastMinute() {
		return $this->m_intLastMinute;
	}

	public function sqlLastMinute() {
		return ( true == isset( $this->m_intLastMinute ) ) ? ( string ) $this->m_intLastMinute : '0';
	}

	public function toArray() {
		return array(
			'user_id' => $this->getUserId(),
			'hour' => $this->getHour(),
			'first_minute' => $this->getFirstMinute(),
			'last_minute' => $this->getLastMinute()
		);
	}

}
?>