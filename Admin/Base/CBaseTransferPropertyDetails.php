<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTransferPropertyDetails
 * Do not add any new functions to this class.
 */

class CBaseTransferPropertyDetails extends CEosPluralBase {

	/**
	 * @return CTransferPropertyDetail[]
	 */
	public static function fetchTransferPropertyDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTransferPropertyDetail', $objDatabase );
	}

	/**
	 * @return CTransferPropertyDetail
	 */
	public static function fetchTransferPropertyDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTransferPropertyDetail', $objDatabase );
	}

	public static function fetchTransferPropertyDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transfer_property_details', $objDatabase );
	}

	public static function fetchTransferPropertyDetailById( $intId, $objDatabase ) {
		return self::fetchTransferPropertyDetail( sprintf( 'SELECT * FROM transfer_property_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTransferPropertyDetailsByCid( $intCid, $objDatabase ) {
		return self::fetchTransferPropertyDetails( sprintf( 'SELECT * FROM transfer_property_details WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchTransferPropertyDetailsByTransferPropertyId( $intTransferPropertyId, $objDatabase ) {
		return self::fetchTransferPropertyDetails( sprintf( 'SELECT * FROM transfer_property_details WHERE transfer_property_id = %d', ( int ) $intTransferPropertyId ), $objDatabase );
	}

	public static function fetchTransferPropertyDetailsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchTransferPropertyDetails( sprintf( 'SELECT * FROM transfer_property_details WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchTransferPropertyDetailsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchTransferPropertyDetails( sprintf( 'SELECT * FROM transfer_property_details WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchTransferPropertyDetailsByContractPropertyId( $intContractPropertyId, $objDatabase ) {
		return self::fetchTransferPropertyDetails( sprintf( 'SELECT * FROM transfer_property_details WHERE contract_property_id = %d', ( int ) $intContractPropertyId ), $objDatabase );
	}

	public static function fetchTransferPropertyDetailsByPreviousContractPropertyId( $intPreviousContractPropertyId, $objDatabase ) {
		return self::fetchTransferPropertyDetails( sprintf( 'SELECT * FROM transfer_property_details WHERE previous_contract_property_id = %d', ( int ) $intPreviousContractPropertyId ), $objDatabase );
	}

	public static function fetchTransferPropertyDetailsByPreviousCommissionRateAssociationId( $intPreviousCommissionRateAssociationId, $objDatabase ) {
		return self::fetchTransferPropertyDetails( sprintf( 'SELECT * FROM transfer_property_details WHERE previous_commission_rate_association_id = %d', ( int ) $intPreviousCommissionRateAssociationId ), $objDatabase );
	}

}
?>