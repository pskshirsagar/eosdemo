<?php

class CBaseInvoiceBatchProcess extends CEosSingularBase {

	const TABLE_NAME = 'public.invoice_batch_processes';

	protected $m_intId;
	protected $m_strClassName;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_strGroupName;
	protected $m_arrintDependentIds;
	protected $m_arrintAdditionalProcesses;
	protected $m_boolIsAdditionalProcess;
	protected $m_boolIsManualProcess;
	protected $m_intRunDayOfMonth;
	protected $m_intEffectiveDayOfMonth;
	protected $m_boolIsPublished;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['class_name'] ) && $boolDirectSet ) $this->set( 'm_strClassName', trim( stripcslashes( $arrValues['class_name'] ) ) ); elseif( isset( $arrValues['class_name'] ) ) $this->setClassName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['class_name'] ) : $arrValues['class_name'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['group_name'] ) && $boolDirectSet ) $this->set( 'm_strGroupName', trim( stripcslashes( $arrValues['group_name'] ) ) ); elseif( isset( $arrValues['group_name'] ) ) $this->setGroupName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['group_name'] ) : $arrValues['group_name'] );
		if( isset( $arrValues['dependent_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintDependentIds', trim( $arrValues['dependent_ids'] ) ); elseif( isset( $arrValues['dependent_ids'] ) ) $this->setDependentIds( $arrValues['dependent_ids'] );
		if( isset( $arrValues['additional_processes'] ) && $boolDirectSet ) $this->set( 'm_arrintAdditionalProcesses', trim( $arrValues['additional_processes'] ) ); elseif( isset( $arrValues['additional_processes'] ) ) $this->setAdditionalProcesses( $arrValues['additional_processes'] );
		if( isset( $arrValues['is_additional_process'] ) && $boolDirectSet ) $this->set( 'm_boolIsAdditionalProcess', trim( stripcslashes( $arrValues['is_additional_process'] ) ) ); elseif( isset( $arrValues['is_additional_process'] ) ) $this->setIsAdditionalProcess( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_additional_process'] ) : $arrValues['is_additional_process'] );
		if( isset( $arrValues['is_manual_process'] ) && $boolDirectSet ) $this->set( 'm_boolIsManualProcess', trim( stripcslashes( $arrValues['is_manual_process'] ) ) ); elseif( isset( $arrValues['is_manual_process'] ) ) $this->setIsManualProcess( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_manual_process'] ) : $arrValues['is_manual_process'] );
		if( isset( $arrValues['run_day_of_month'] ) && $boolDirectSet ) $this->set( 'm_intRunDayOfMonth', trim( $arrValues['run_day_of_month'] ) ); elseif( isset( $arrValues['run_day_of_month'] ) ) $this->setRunDayOfMonth( $arrValues['run_day_of_month'] );
		if( isset( $arrValues['effective_day_of_month'] ) && $boolDirectSet ) $this->set( 'm_intEffectiveDayOfMonth', trim( $arrValues['effective_day_of_month'] ) ); elseif( isset( $arrValues['effective_day_of_month'] ) ) $this->setEffectiveDayOfMonth( $arrValues['effective_day_of_month'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClassName( $strClassName ) {
		$this->set( 'm_strClassName', CStrings::strTrimDef( $strClassName, 100, NULL, true ) );
	}

	public function getClassName() {
		return $this->m_strClassName;
	}

	public function sqlClassName() {
		return ( true == isset( $this->m_strClassName ) ) ? '\'' . addslashes( $this->m_strClassName ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 100, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setGroupName( $strGroupName ) {
		$this->set( 'm_strGroupName', CStrings::strTrimDef( $strGroupName, 100, NULL, true ) );
	}

	public function getGroupName() {
		return $this->m_strGroupName;
	}

	public function sqlGroupName() {
		return ( true == isset( $this->m_strGroupName ) ) ? '\'' . addslashes( $this->m_strGroupName ) . '\'' : 'NULL';
	}

	public function setDependentIds( $arrintDependentIds ) {
		$this->set( 'm_arrintDependentIds', CStrings::strToArrIntDef( $arrintDependentIds, NULL ) );
	}

	public function getDependentIds() {
		return $this->m_arrintDependentIds;
	}

	public function sqlDependentIds() {
		return ( true == isset( $this->m_arrintDependentIds ) && true == valArr( $this->m_arrintDependentIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintDependentIds, NULL ) . '\'' : 'NULL';
	}

	public function setAdditionalProcesses( $arrintAdditionalProcesses ) {
		$this->set( 'm_arrintAdditionalProcesses', CStrings::strToArrIntDef( $arrintAdditionalProcesses, NULL ) );
	}

	public function getAdditionalProcesses() {
		return $this->m_arrintAdditionalProcesses;
	}

	public function sqlAdditionalProcesses() {
		return ( true == isset( $this->m_arrintAdditionalProcesses ) && true == valArr( $this->m_arrintAdditionalProcesses ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintAdditionalProcesses, NULL ) . '\'' : 'NULL';
	}

	public function setIsAdditionalProcess( $boolIsAdditionalProcess ) {
		$this->set( 'm_boolIsAdditionalProcess', CStrings::strToBool( $boolIsAdditionalProcess ) );
	}

	public function getIsAdditionalProcess() {
		return $this->m_boolIsAdditionalProcess;
	}

	public function sqlIsAdditionalProcess() {
		return ( true == isset( $this->m_boolIsAdditionalProcess ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAdditionalProcess ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsManualProcess( $boolIsManualProcess ) {
		$this->set( 'm_boolIsManualProcess', CStrings::strToBool( $boolIsManualProcess ) );
	}

	public function getIsManualProcess() {
		return $this->m_boolIsManualProcess;
	}

	public function sqlIsManualProcess() {
		return ( true == isset( $this->m_boolIsManualProcess ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsManualProcess ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRunDayOfMonth( $intRunDayOfMonth ) {
		$this->set( 'm_intRunDayOfMonth', CStrings::strToIntDef( $intRunDayOfMonth, NULL, false ) );
	}

	public function getRunDayOfMonth() {
		return $this->m_intRunDayOfMonth;
	}

	public function sqlRunDayOfMonth() {
		return ( true == isset( $this->m_intRunDayOfMonth ) ) ? ( string ) $this->m_intRunDayOfMonth : 'NULL';
	}

	public function setEffectiveDayOfMonth( $intEffectiveDayOfMonth ) {
		$this->set( 'm_intEffectiveDayOfMonth', CStrings::strToIntDef( $intEffectiveDayOfMonth, NULL, false ) );
	}

	public function getEffectiveDayOfMonth() {
		return $this->m_intEffectiveDayOfMonth;
	}

	public function sqlEffectiveDayOfMonth() {
		return ( true == isset( $this->m_intEffectiveDayOfMonth ) ) ? ( string ) $this->m_intEffectiveDayOfMonth : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, class_name, title, description, group_name, dependent_ids, additional_processes, is_additional_process, is_manual_process, run_day_of_month, effective_day_of_month, is_published, created_by, created_on, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlClassName() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlGroupName() . ', ' .
 						$this->sqlDependentIds() . ', ' .
 						$this->sqlAdditionalProcesses() . ', ' .
 						$this->sqlIsAdditionalProcess() . ', ' .
 						$this->sqlIsManualProcess() . ', ' .
 						$this->sqlRunDayOfMonth() . ', ' .
 						$this->sqlEffectiveDayOfMonth() . ', ' .
 						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' class_name = ' . $this->sqlClassName() . ','; } elseif( true == array_key_exists( 'ClassName', $this->getChangedColumns() ) ) { $strSql .= ' class_name = ' . $this->sqlClassName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_name = ' . $this->sqlGroupName() . ','; } elseif( true == array_key_exists( 'GroupName', $this->getChangedColumns() ) ) { $strSql .= ' group_name = ' . $this->sqlGroupName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dependent_ids = ' . $this->sqlDependentIds() . ','; } elseif( true == array_key_exists( 'DependentIds', $this->getChangedColumns() ) ) { $strSql .= ' dependent_ids = ' . $this->sqlDependentIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_processes = ' . $this->sqlAdditionalProcesses() . ','; } elseif( true == array_key_exists( 'AdditionalProcesses', $this->getChangedColumns() ) ) { $strSql .= ' additional_processes = ' . $this->sqlAdditionalProcesses() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_additional_process = ' . $this->sqlIsAdditionalProcess() . ','; } elseif( true == array_key_exists( 'IsAdditionalProcess', $this->getChangedColumns() ) ) { $strSql .= ' is_additional_process = ' . $this->sqlIsAdditionalProcess() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual_process = ' . $this->sqlIsManualProcess() . ','; } elseif( true == array_key_exists( 'IsManualProcess', $this->getChangedColumns() ) ) { $strSql .= ' is_manual_process = ' . $this->sqlIsManualProcess() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' run_day_of_month = ' . $this->sqlRunDayOfMonth() . ','; } elseif( true == array_key_exists( 'RunDayOfMonth', $this->getChangedColumns() ) ) { $strSql .= ' run_day_of_month = ' . $this->sqlRunDayOfMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' effective_day_of_month = ' . $this->sqlEffectiveDayOfMonth() . ','; } elseif( true == array_key_exists( 'EffectiveDayOfMonth', $this->getChangedColumns() ) ) { $strSql .= ' effective_day_of_month = ' . $this->sqlEffectiveDayOfMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'class_name' => $this->getClassName(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'group_name' => $this->getGroupName(),
			'dependent_ids' => $this->getDependentIds(),
			'additional_processes' => $this->getAdditionalProcesses(),
			'is_additional_process' => $this->getIsAdditionalProcess(),
			'is_manual_process' => $this->getIsManualProcess(),
			'run_day_of_month' => $this->getRunDayOfMonth(),
			'effective_day_of_month' => $this->getEffectiveDayOfMonth(),
			'is_published' => $this->getIsPublished(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>