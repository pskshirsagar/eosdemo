<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplateOrders
 * Do not add any new functions to this class.
 */

class CBaseDocumentTemplateOrders extends CEosPluralBase {

	/**
	 * @return CDocumentTemplateOrder[]
	 */
	public static function fetchDocumentTemplateOrders( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDocumentTemplateOrder', $objDatabase );
	}

	/**
	 * @return CDocumentTemplateOrder
	 */
	public static function fetchDocumentTemplateOrder( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentTemplateOrder', $objDatabase );
	}

	public static function fetchDocumentTemplateOrderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_template_orders', $objDatabase );
	}

	public static function fetchDocumentTemplateOrderById( $intId, $objDatabase ) {
		return self::fetchDocumentTemplateOrder( sprintf( 'SELECT * FROM document_template_orders WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDocumentTemplateOrdersByCid( $intCid, $objDatabase ) {
		return self::fetchDocumentTemplateOrders( sprintf( 'SELECT * FROM document_template_orders WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchDocumentTemplateOrdersByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchDocumentTemplateOrders( sprintf( 'SELECT * FROM document_template_orders WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchDocumentTemplateOrdersByCompanyPricingId( $intCompanyPricingId, $objDatabase ) {
		return self::fetchDocumentTemplateOrders( sprintf( 'SELECT * FROM document_template_orders WHERE company_pricing_id = %d', ( int ) $intCompanyPricingId ), $objDatabase );
	}

	public static function fetchDocumentTemplateOrdersByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchDocumentTemplateOrders( sprintf( 'SELECT * FROM document_template_orders WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

	public static function fetchDocumentTemplateOrdersByTransactionId( $intTransactionId, $objDatabase ) {
		return self::fetchDocumentTemplateOrders( sprintf( 'SELECT * FROM document_template_orders WHERE transaction_id = %d', ( int ) $intTransactionId ), $objDatabase );
	}

	public static function fetchDocumentTemplateOrdersByDocumentTemplateId( $intDocumentTemplateId, $objDatabase ) {
		return self::fetchDocumentTemplateOrders( sprintf( 'SELECT * FROM document_template_orders WHERE document_template_id = %d', ( int ) $intDocumentTemplateId ), $objDatabase );
	}

	public static function fetchDocumentTemplateOrdersByDocumentId( $intDocumentId, $objDatabase ) {
		return self::fetchDocumentTemplateOrders( sprintf( 'SELECT * FROM document_template_orders WHERE document_id = %d', ( int ) $intDocumentId ), $objDatabase );
	}

}
?>