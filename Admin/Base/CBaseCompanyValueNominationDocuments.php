<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyValueNominationDocuments
 * Do not add any new functions to this class.
 */

class CBaseCompanyValueNominationDocuments extends CEosPluralBase {

	/**
	 * @return CCompanyValueNominationDocument[]
	 */
	public static function fetchCompanyValueNominationDocuments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyValueNominationDocument::class, $objDatabase );
	}

	/**
	 * @return CCompanyValueNominationDocument
	 */
	public static function fetchCompanyValueNominationDocument( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyValueNominationDocument::class, $objDatabase );
	}

	public static function fetchCompanyValueNominationDocumentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_value_nomination_documents', $objDatabase );
	}

	public static function fetchCompanyValueNominationDocumentById( $intId, $objDatabase ) {
		return self::fetchCompanyValueNominationDocument( sprintf( 'SELECT * FROM company_value_nomination_documents WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCompanyValueNominationDocumentsByCompanyValueNominationId( $intCompanyValueNominationId, $objDatabase ) {
		return self::fetchCompanyValueNominationDocuments( sprintf( 'SELECT * FROM company_value_nomination_documents WHERE company_value_nomination_id = %d', ( int ) $intCompanyValueNominationId ), $objDatabase );
	}

	public static function fetchCompanyValueNominationDocumentsByPsDocumentId( $intPsDocumentId, $objDatabase ) {
		return self::fetchCompanyValueNominationDocuments( sprintf( 'SELECT * FROM company_value_nomination_documents WHERE ps_document_id = %d', ( int ) $intPsDocumentId ), $objDatabase );
	}

}
?>