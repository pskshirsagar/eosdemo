<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePolicies
 * Do not add any new functions to this class.
 */

class CBaseEmployeePolicies extends CEosPluralBase {

	/**
	 * @return CEmployeePolicy[]
	 */
	public static function fetchEmployeePolicies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeePolicy', $objDatabase );
	}

	/**
	 * @return CEmployeePolicy
	 */
	public static function fetchEmployeePolicy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeePolicy', $objDatabase );
	}

	public static function fetchEmployeePolicyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_policies', $objDatabase );
	}

	public static function fetchEmployeePolicyById( $intId, $objDatabase ) {
		return self::fetchEmployeePolicy( sprintf( 'SELECT * FROM employee_policies WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeePoliciesByPolicyId( $intPolicyId, $objDatabase ) {
		return self::fetchEmployeePolicies( sprintf( 'SELECT * FROM employee_policies WHERE policy_id = %d', ( int ) $intPolicyId ), $objDatabase );
	}

	public static function fetchEmployeePoliciesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeePolicies( sprintf( 'SELECT * FROM employee_policies WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>