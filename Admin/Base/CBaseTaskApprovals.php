<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskApprovals
 * Do not add any new functions to this class.
 */

class CBaseTaskApprovals extends CEosPluralBase {

	/**
	 * @return CTaskApproval[]
	 */
	public static function fetchTaskApprovals( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTaskApproval::class, $objDatabase );
	}

	/**
	 * @return CTaskApproval
	 */
	public static function fetchTaskApproval( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaskApproval::class, $objDatabase );
	}

	public static function fetchTaskApprovalCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_approvals', $objDatabase );
	}

	public static function fetchTaskApprovalById( $intId, $objDatabase ) {
		return self::fetchTaskApproval( sprintf( 'SELECT * FROM task_approvals WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchTaskApprovalsByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchTaskApprovals( sprintf( 'SELECT * FROM task_approvals WHERE task_id = %d', $intTaskId ), $objDatabase );
	}

	public static function fetchTaskApprovalsByApprovalTypeId( $intApprovalTypeId, $objDatabase ) {
		return self::fetchTaskApprovals( sprintf( 'SELECT * FROM task_approvals WHERE approval_type_id = %d', $intApprovalTypeId ), $objDatabase );
	}

	public static function fetchTaskApprovalsByApprovedByEmployeeId( $intApprovedByEmployeeId, $objDatabase ) {
		return self::fetchTaskApprovals( sprintf( 'SELECT * FROM task_approvals WHERE approved_by_employee_id = %d', $intApprovedByEmployeeId ), $objDatabase );
	}

}
?>