<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTranslations
 * Do not add any new functions to this class.
 */

class CBaseTranslations extends CEosPluralBase {

	/**
	 * @return CTranslation[]
	 */
	public static function fetchTranslations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTranslation::class, $objDatabase );
	}

	/**
	 * @return CTranslation
	 */
	public static function fetchTranslation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTranslation::class, $objDatabase );
	}

	public static function fetchTranslationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'translations', $objDatabase );
	}

	public static function fetchTranslationById( $intId, $objDatabase ) {
		return self::fetchTranslation( sprintf( 'SELECT * FROM translations WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchTranslationsByTranslationKeyId( $intTranslationKeyId, $objDatabase ) {
		return self::fetchTranslations( sprintf( 'SELECT * FROM translations WHERE translation_key_id = %d', $intTranslationKeyId ), $objDatabase );
	}

}
?>