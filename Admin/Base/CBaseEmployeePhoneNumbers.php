<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePhoneNumbers
 * Do not add any new functions to this class.
 */

class CBaseEmployeePhoneNumbers extends CEosPluralBase {

	/**
	 * @return CEmployeePhoneNumber[]
	 */
	public static function fetchEmployeePhoneNumbers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeePhoneNumber', $objDatabase );
	}

	/**
	 * @return CEmployeePhoneNumber
	 */
	public static function fetchEmployeePhoneNumber( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeePhoneNumber', $objDatabase );
	}

	public static function fetchEmployeePhoneNumberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_phone_numbers', $objDatabase );
	}

	public static function fetchEmployeePhoneNumberById( $intId, $objDatabase ) {
		return self::fetchEmployeePhoneNumber( sprintf( 'SELECT * FROM employee_phone_numbers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeePhoneNumbersByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeePhoneNumbers( sprintf( 'SELECT * FROM employee_phone_numbers WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeePhoneNumbersByPhoneNumberTypeId( $intPhoneNumberTypeId, $objDatabase ) {
		return self::fetchEmployeePhoneNumbers( sprintf( 'SELECT * FROM employee_phone_numbers WHERE phone_number_type_id = %d', ( int ) $intPhoneNumberTypeId ), $objDatabase );
	}

	public static function fetchEmployeePhoneNumbersByMessageOperatorId( $intMessageOperatorId, $objDatabase ) {
		return self::fetchEmployeePhoneNumbers( sprintf( 'SELECT * FROM employee_phone_numbers WHERE message_operator_id = %d', ( int ) $intMessageOperatorId ), $objDatabase );
	}

}
?>