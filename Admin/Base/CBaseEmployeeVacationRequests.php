<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeVacationRequests
 * Do not add any new functions to this class.
 */

class CBaseEmployeeVacationRequests extends CEosPluralBase {

	/**
	 * @return CEmployeeVacationRequest[]
	 */
	public static function fetchEmployeeVacationRequests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeVacationRequest', $objDatabase );
	}

	/**
	 * @return CEmployeeVacationRequest
	 */
	public static function fetchEmployeeVacationRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeVacationRequest', $objDatabase );
	}

	public static function fetchEmployeeVacationRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_vacation_requests', $objDatabase );
	}

	public static function fetchEmployeeVacationRequestById( $intId, $objDatabase ) {
		return self::fetchEmployeeVacationRequest( sprintf( 'SELECT * FROM employee_vacation_requests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeVacationRequestsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeVacationRequests( sprintf( 'SELECT * FROM employee_vacation_requests WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeVacationRequestsByVacationTypeId( $intVacationTypeId, $objDatabase ) {
		return self::fetchEmployeeVacationRequests( sprintf( 'SELECT * FROM employee_vacation_requests WHERE vacation_type_id = %d', ( int ) $intVacationTypeId ), $objDatabase );
	}

}
?>