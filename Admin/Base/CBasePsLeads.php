<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsLeads
 * Do not add any new functions to this class.
 */

class CBasePsLeads extends CEosPluralBase {

	/**
	 * @return CPsLead[]
	 */
	public static function fetchPsLeads( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsLead', $objDatabase );
	}

	/**
	 * @return CPsLead
	 */
	public static function fetchPsLead( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsLead', $objDatabase );
	}

	public static function fetchPsLeadCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_leads', $objDatabase );
	}

	public static function fetchPsLeadById( $intId, $objDatabase ) {
		return self::fetchPsLead( sprintf( 'SELECT * FROM ps_leads WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsLeadsByCompanyStatusTypeId( $intCompanyStatusTypeId, $objDatabase ) {
		return self::fetchPsLeads( sprintf( 'SELECT * FROM ps_leads WHERE company_status_type_id = %d', ( int ) $intCompanyStatusTypeId ), $objDatabase );
	}

	public static function fetchPsLeadsByPsLeadOriginId( $intPsLeadOriginId, $objDatabase ) {
		return self::fetchPsLeads( sprintf( 'SELECT * FROM ps_leads WHERE ps_lead_origin_id = %d', ( int ) $intPsLeadOriginId ), $objDatabase );
	}

	public static function fetchPsLeadsByPsLeadSourceId( $intPsLeadSourceId, $objDatabase ) {
		return self::fetchPsLeads( sprintf( 'SELECT * FROM ps_leads WHERE ps_lead_source_id = %d', ( int ) $intPsLeadSourceId ), $objDatabase );
	}

	public static function fetchPsLeadsByPsLeadId( $intPsLeadId, $objDatabase ) {
		return self::fetchPsLeads( sprintf( 'SELECT * FROM ps_leads WHERE ps_lead_id = %d', ( int ) $intPsLeadId ), $objDatabase );
	}

	public static function fetchPsLeadsByPsLeadEventId( $intPsLeadEventId, $objDatabase ) {
		return self::fetchPsLeads( sprintf( 'SELECT * FROM ps_leads WHERE ps_lead_event_id = %d', ( int ) $intPsLeadEventId ), $objDatabase );
	}

}
?>