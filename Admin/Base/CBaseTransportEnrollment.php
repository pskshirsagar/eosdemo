<?php

class CBaseTransportEnrollment extends CEosSingularBase {

	const TABLE_NAME = 'public.transport_enrollments';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intTransportRouteId;
	protected $m_intTransportPickupPointId;
	protected $m_intTransportRequestStatusId;
	protected $m_intProposedTransportRouteId;
	protected $m_intProposedTransportPickupPointId;
	protected $m_boolIsPolicyAgreedByEmployee;
	protected $m_strRemark;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strProposedStartDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPolicyAgreedByEmployee = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['transport_route_id'] ) && $boolDirectSet ) $this->set( 'm_intTransportRouteId', trim( $arrValues['transport_route_id'] ) ); elseif( isset( $arrValues['transport_route_id'] ) ) $this->setTransportRouteId( $arrValues['transport_route_id'] );
		if( isset( $arrValues['transport_pickup_point_id'] ) && $boolDirectSet ) $this->set( 'm_intTransportPickupPointId', trim( $arrValues['transport_pickup_point_id'] ) ); elseif( isset( $arrValues['transport_pickup_point_id'] ) ) $this->setTransportPickupPointId( $arrValues['transport_pickup_point_id'] );
		if( isset( $arrValues['transport_request_status_id'] ) && $boolDirectSet ) $this->set( 'm_intTransportRequestStatusId', trim( $arrValues['transport_request_status_id'] ) ); elseif( isset( $arrValues['transport_request_status_id'] ) ) $this->setTransportRequestStatusId( $arrValues['transport_request_status_id'] );
		if( isset( $arrValues['proposed_transport_route_id'] ) && $boolDirectSet ) $this->set( 'm_intProposedTransportRouteId', trim( $arrValues['proposed_transport_route_id'] ) ); elseif( isset( $arrValues['proposed_transport_route_id'] ) ) $this->setProposedTransportRouteId( $arrValues['proposed_transport_route_id'] );
		if( isset( $arrValues['proposed_transport_pickup_point_id'] ) && $boolDirectSet ) $this->set( 'm_intProposedTransportPickupPointId', trim( $arrValues['proposed_transport_pickup_point_id'] ) ); elseif( isset( $arrValues['proposed_transport_pickup_point_id'] ) ) $this->setProposedTransportPickupPointId( $arrValues['proposed_transport_pickup_point_id'] );
		if( isset( $arrValues['is_policy_agreed_by_employee'] ) && $boolDirectSet ) $this->set( 'm_boolIsPolicyAgreedByEmployee', trim( stripcslashes( $arrValues['is_policy_agreed_by_employee'] ) ) ); elseif( isset( $arrValues['is_policy_agreed_by_employee'] ) ) $this->setIsPolicyAgreedByEmployee( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_policy_agreed_by_employee'] ) : $arrValues['is_policy_agreed_by_employee'] );
		if( isset( $arrValues['remark'] ) && $boolDirectSet ) $this->set( 'm_strRemark', trim( stripcslashes( $arrValues['remark'] ) ) ); elseif( isset( $arrValues['remark'] ) ) $this->setRemark( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remark'] ) : $arrValues['remark'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['proposed_start_date'] ) && $boolDirectSet ) $this->set( 'm_strProposedStartDate', trim( $arrValues['proposed_start_date'] ) ); elseif( isset( $arrValues['proposed_start_date'] ) ) $this->setProposedStartDate( $arrValues['proposed_start_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setTransportRouteId( $intTransportRouteId ) {
		$this->set( 'm_intTransportRouteId', CStrings::strToIntDef( $intTransportRouteId, NULL, false ) );
	}

	public function getTransportRouteId() {
		return $this->m_intTransportRouteId;
	}

	public function sqlTransportRouteId() {
		return ( true == isset( $this->m_intTransportRouteId ) ) ? ( string ) $this->m_intTransportRouteId : 'NULL';
	}

	public function setTransportPickupPointId( $intTransportPickupPointId ) {
		$this->set( 'm_intTransportPickupPointId', CStrings::strToIntDef( $intTransportPickupPointId, NULL, false ) );
	}

	public function getTransportPickupPointId() {
		return $this->m_intTransportPickupPointId;
	}

	public function sqlTransportPickupPointId() {
		return ( true == isset( $this->m_intTransportPickupPointId ) ) ? ( string ) $this->m_intTransportPickupPointId : 'NULL';
	}

	public function setTransportRequestStatusId( $intTransportRequestStatusId ) {
		$this->set( 'm_intTransportRequestStatusId', CStrings::strToIntDef( $intTransportRequestStatusId, NULL, false ) );
	}

	public function getTransportRequestStatusId() {
		return $this->m_intTransportRequestStatusId;
	}

	public function sqlTransportRequestStatusId() {
		return ( true == isset( $this->m_intTransportRequestStatusId ) ) ? ( string ) $this->m_intTransportRequestStatusId : 'NULL';
	}

	public function setProposedTransportRouteId( $intProposedTransportRouteId ) {
		$this->set( 'm_intProposedTransportRouteId', CStrings::strToIntDef( $intProposedTransportRouteId, NULL, false ) );
	}

	public function getProposedTransportRouteId() {
		return $this->m_intProposedTransportRouteId;
	}

	public function sqlProposedTransportRouteId() {
		return ( true == isset( $this->m_intProposedTransportRouteId ) ) ? ( string ) $this->m_intProposedTransportRouteId : 'NULL';
	}

	public function setProposedTransportPickupPointId( $intProposedTransportPickupPointId ) {
		$this->set( 'm_intProposedTransportPickupPointId', CStrings::strToIntDef( $intProposedTransportPickupPointId, NULL, false ) );
	}

	public function getProposedTransportPickupPointId() {
		return $this->m_intProposedTransportPickupPointId;
	}

	public function sqlProposedTransportPickupPointId() {
		return ( true == isset( $this->m_intProposedTransportPickupPointId ) ) ? ( string ) $this->m_intProposedTransportPickupPointId : 'NULL';
	}

	public function setIsPolicyAgreedByEmployee( $boolIsPolicyAgreedByEmployee ) {
		$this->set( 'm_boolIsPolicyAgreedByEmployee', CStrings::strToBool( $boolIsPolicyAgreedByEmployee ) );
	}

	public function getIsPolicyAgreedByEmployee() {
		return $this->m_boolIsPolicyAgreedByEmployee;
	}

	public function sqlIsPolicyAgreedByEmployee() {
		return ( true == isset( $this->m_boolIsPolicyAgreedByEmployee ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPolicyAgreedByEmployee ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setRemark( $strRemark ) {
		$this->set( 'm_strRemark', CStrings::strTrimDef( $strRemark, -1, NULL, true ) );
	}

	public function getRemark() {
		return $this->m_strRemark;
	}

	public function sqlRemark() {
		return ( true == isset( $this->m_strRemark ) ) ? '\'' . addslashes( $this->m_strRemark ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setProposedStartDate( $strProposedStartDate ) {
		$this->set( 'm_strProposedStartDate', CStrings::strTrimDef( $strProposedStartDate, -1, NULL, true ) );
	}

	public function getProposedStartDate() {
		return $this->m_strProposedStartDate;
	}

	public function sqlProposedStartDate() {
		return ( true == isset( $this->m_strProposedStartDate ) ) ? '\'' . $this->m_strProposedStartDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, transport_route_id, transport_pickup_point_id, transport_request_status_id, proposed_transport_route_id, proposed_transport_pickup_point_id, is_policy_agreed_by_employee, remark, start_date, end_date, proposed_start_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlTransportRouteId() . ', ' .
 						$this->sqlTransportPickupPointId() . ', ' .
 						$this->sqlTransportRequestStatusId() . ', ' .
 						$this->sqlProposedTransportRouteId() . ', ' .
 						$this->sqlProposedTransportPickupPointId() . ', ' .
 						$this->sqlIsPolicyAgreedByEmployee() . ', ' .
 						$this->sqlRemark() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlProposedStartDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transport_route_id = ' . $this->sqlTransportRouteId() . ','; } elseif( true == array_key_exists( 'TransportRouteId', $this->getChangedColumns() ) ) { $strSql .= ' transport_route_id = ' . $this->sqlTransportRouteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transport_pickup_point_id = ' . $this->sqlTransportPickupPointId() . ','; } elseif( true == array_key_exists( 'TransportPickupPointId', $this->getChangedColumns() ) ) { $strSql .= ' transport_pickup_point_id = ' . $this->sqlTransportPickupPointId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transport_request_status_id = ' . $this->sqlTransportRequestStatusId() . ','; } elseif( true == array_key_exists( 'TransportRequestStatusId', $this->getChangedColumns() ) ) { $strSql .= ' transport_request_status_id = ' . $this->sqlTransportRequestStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposed_transport_route_id = ' . $this->sqlProposedTransportRouteId() . ','; } elseif( true == array_key_exists( 'ProposedTransportRouteId', $this->getChangedColumns() ) ) { $strSql .= ' proposed_transport_route_id = ' . $this->sqlProposedTransportRouteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposed_transport_pickup_point_id = ' . $this->sqlProposedTransportPickupPointId() . ','; } elseif( true == array_key_exists( 'ProposedTransportPickupPointId', $this->getChangedColumns() ) ) { $strSql .= ' proposed_transport_pickup_point_id = ' . $this->sqlProposedTransportPickupPointId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_policy_agreed_by_employee = ' . $this->sqlIsPolicyAgreedByEmployee() . ','; } elseif( true == array_key_exists( 'IsPolicyAgreedByEmployee', $this->getChangedColumns() ) ) { $strSql .= ' is_policy_agreed_by_employee = ' . $this->sqlIsPolicyAgreedByEmployee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remark = ' . $this->sqlRemark() . ','; } elseif( true == array_key_exists( 'Remark', $this->getChangedColumns() ) ) { $strSql .= ' remark = ' . $this->sqlRemark() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' proposed_start_date = ' . $this->sqlProposedStartDate() . ','; } elseif( true == array_key_exists( 'ProposedStartDate', $this->getChangedColumns() ) ) { $strSql .= ' proposed_start_date = ' . $this->sqlProposedStartDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'transport_route_id' => $this->getTransportRouteId(),
			'transport_pickup_point_id' => $this->getTransportPickupPointId(),
			'transport_request_status_id' => $this->getTransportRequestStatusId(),
			'proposed_transport_route_id' => $this->getProposedTransportRouteId(),
			'proposed_transport_pickup_point_id' => $this->getProposedTransportPickupPointId(),
			'is_policy_agreed_by_employee' => $this->getIsPolicyAgreedByEmployee(),
			'remark' => $this->getRemark(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'proposed_start_date' => $this->getProposedStartDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>