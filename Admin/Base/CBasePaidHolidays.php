<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPaidHolidays
 * Do not add any new functions to this class.
 */

class CBasePaidHolidays extends CEosPluralBase {

	/**
	 * @return CPaidHoliday[]
	 */
	public static function fetchPaidHolidays( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPaidHoliday', $objDatabase );
	}

	/**
	 * @return CPaidHoliday
	 */
	public static function fetchPaidHoliday( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPaidHoliday', $objDatabase );
	}

	public static function fetchPaidHolidayCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'paid_holidays', $objDatabase );
	}

	public static function fetchPaidHolidayById( $intId, $objDatabase ) {
		return self::fetchPaidHoliday( sprintf( 'SELECT * FROM paid_holidays WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>