<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyPricings
 * Do not add any new functions to this class.
 */

class CBaseCompanyPricings extends CEosPluralBase {

	/**
	 * @return CCompanyPricing[]
	 */
	public static function fetchCompanyPricings( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCompanyPricing', $objDatabase );
	}

	/**
	 * @return CCompanyPricing
	 */
	public static function fetchCompanyPricing( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyPricing', $objDatabase );
	}

	public static function fetchCompanyPricingCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_pricings', $objDatabase );
	}

	public static function fetchCompanyPricingById( $intId, $objDatabase ) {
		return self::fetchCompanyPricing( sprintf( 'SELECT * FROM company_pricings WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCompanyPricingsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyPricings( sprintf( 'SELECT * FROM company_pricings WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyPricingsByCompanyPricingTypeId( $intCompanyPricingTypeId, $objDatabase ) {
		return self::fetchCompanyPricings( sprintf( 'SELECT * FROM company_pricings WHERE company_pricing_type_id = %d', ( int ) $intCompanyPricingTypeId ), $objDatabase );
	}

	public static function fetchCompanyPricingsByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchCompanyPricings( sprintf( 'SELECT * FROM company_pricings WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

	public static function fetchCompanyPricingsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchCompanyPricings( sprintf( 'SELECT * FROM company_pricings WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>