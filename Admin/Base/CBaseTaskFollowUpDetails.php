<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskFollowUpDetails
 * Do not add any new functions to this class.
 */

class CBaseTaskFollowUpDetails extends CEosPluralBase {

	/**
	 * @return CTaskFollowUpDetail[]
	 */
	public static function fetchTaskFollowUpDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskFollowUpDetail', $objDatabase );
	}

	/**
	 * @return CTaskFollowUpDetail
	 */
	public static function fetchTaskFollowUpDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskFollowUpDetail', $objDatabase );
	}

	public static function fetchTaskFollowUpDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_follow_up_details', $objDatabase );
	}

	public static function fetchTaskFollowUpDetailById( $intId, $objDatabase ) {
		return self::fetchTaskFollowUpDetail( sprintf( 'SELECT * FROM task_follow_up_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskFollowUpDetailsByTaskTypeId( $intTaskTypeId, $objDatabase ) {
		return self::fetchTaskFollowUpDetails( sprintf( 'SELECT * FROM task_follow_up_details WHERE task_type_id = %d', ( int ) $intTaskTypeId ), $objDatabase );
	}

	public static function fetchTaskFollowUpDetailsByTaskPriorityId( $intTaskPriorityId, $objDatabase ) {
		return self::fetchTaskFollowUpDetails( sprintf( 'SELECT * FROM task_follow_up_details WHERE task_priority_id = %d', ( int ) $intTaskPriorityId ), $objDatabase );
	}

	public static function fetchTaskFollowUpDetailsByTaskStatusId( $intTaskStatusId, $objDatabase ) {
		return self::fetchTaskFollowUpDetails( sprintf( 'SELECT * FROM task_follow_up_details WHERE task_status_id = %d', ( int ) $intTaskStatusId ), $objDatabase );
	}

}
?>