<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskDataRequests
 * Do not add any new functions to this class.
 */

class CBaseTaskDataRequests extends CEosPluralBase {

	/**
	 * @return CTaskDataRequest[]
	 */
	public static function fetchTaskDataRequests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTaskDataRequest::class, $objDatabase );
	}

	/**
	 * @return CTaskDataRequest
	 */
	public static function fetchTaskDataRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaskDataRequest::class, $objDatabase );
	}

	public static function fetchTaskDataRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_data_requests', $objDatabase );
	}

	public static function fetchTaskDataRequestById( $intId, $objDatabase ) {
		return self::fetchTaskDataRequest( sprintf( 'SELECT * FROM task_data_requests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskDataRequestsByCompanyReportId( $intCompanyReportId, $objDatabase ) {
		return self::fetchTaskDataRequests( sprintf( 'SELECT * FROM task_data_requests WHERE company_report_id = %d', ( int ) $intCompanyReportId ), $objDatabase );
	}

	public static function fetchTaskDataRequestsByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchTaskDataRequests( sprintf( 'SELECT * FROM task_data_requests WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

}
?>