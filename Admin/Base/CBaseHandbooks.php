<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CHandbooks
 * Do not add any new functions to this class.
 */

class CBaseHandbooks extends CEosPluralBase {

	/**
	 * @return CHandbook[]
	 */
	public static function fetchHandbooks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CHandbook', $objDatabase );
	}

	/**
	 * @return CHandbook
	 */
	public static function fetchHandbook( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHandbook', $objDatabase );
	}

	public static function fetchHandbookCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'handbooks', $objDatabase );
	}

	public static function fetchHandbookById( $intId, $objDatabase ) {
		return self::fetchHandbook( sprintf( 'SELECT * FROM handbooks WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchHandbooksByPsDocumentId( $intPsDocumentId, $objDatabase ) {
		return self::fetchHandbooks( sprintf( 'SELECT * FROM handbooks WHERE ps_document_id = %d', ( int ) $intPsDocumentId ), $objDatabase );
	}

}
?>