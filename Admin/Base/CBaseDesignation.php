<?php

class CBaseDesignation extends CEosSingularBase {

	const TABLE_NAME = 'public.designations';

	protected $m_intId;
	protected $m_intDepartmentId;
	protected $m_intDesignationId;
	protected $m_intApproverEmployeeId;
	protected $m_intCompChangeApproverId;
	protected $m_intReplacementApproverId;
	protected $m_intEeocClassificationTypeId;
	protected $m_intEmployeeSalaryTypeId;
	protected $m_strCountryCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strGrade;
	protected $m_strJobResponsibilities;
	protected $m_strSuccessMeasures;
	protected $m_intIsPublished;
	protected $m_intNoticePeriod;
	protected $m_intProbationPeriod;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intBonusApproverEmployeeId;
	protected $m_intBonusCriteriaApproverEmployeeId;
	protected $m_intDesignationLevelId;
	protected $m_intMinExperience;
	protected $m_intMaxExperience;

	public function __construct() {
		parent::__construct();

		$this->m_strCountryCode = 'US';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['approver_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intApproverEmployeeId', trim( $arrValues['approver_employee_id'] ) ); elseif( isset( $arrValues['approver_employee_id'] ) ) $this->setApproverEmployeeId( $arrValues['approver_employee_id'] );
		if( isset( $arrValues['comp_change_approver_id'] ) && $boolDirectSet ) $this->set( 'm_intCompChangeApproverId', trim( $arrValues['comp_change_approver_id'] ) ); elseif( isset( $arrValues['comp_change_approver_id'] ) ) $this->setCompChangeApproverId( $arrValues['comp_change_approver_id'] );
		if( isset( $arrValues['replacement_approver_id'] ) && $boolDirectSet ) $this->set( 'm_intReplacementApproverId', trim( $arrValues['replacement_approver_id'] ) ); elseif( isset( $arrValues['replacement_approver_id'] ) ) $this->setReplacementApproverId( $arrValues['replacement_approver_id'] );
		if( isset( $arrValues['eeoc_classification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEeocClassificationTypeId', trim( $arrValues['eeoc_classification_type_id'] ) ); elseif( isset( $arrValues['eeoc_classification_type_id'] ) ) $this->setEeocClassificationTypeId( $arrValues['eeoc_classification_type_id'] );
		if( isset( $arrValues['employee_salary_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeSalaryTypeId', trim( $arrValues['employee_salary_type_id'] ) ); elseif( isset( $arrValues['employee_salary_type_id'] ) ) $this->setEmployeeSalaryTypeId( $arrValues['employee_salary_type_id'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['grade'] ) && $boolDirectSet ) $this->set( 'm_strGrade', trim( $arrValues['grade'] ) ); elseif( isset( $arrValues['grade'] ) ) $this->setGrade( $arrValues['grade'] );
		if( isset( $arrValues['job_responsibilities'] ) && $boolDirectSet ) $this->set( 'm_strJobResponsibilities', trim( $arrValues['job_responsibilities'] ) ); elseif( isset( $arrValues['job_responsibilities'] ) ) $this->setJobResponsibilities( $arrValues['job_responsibilities'] );
		if( isset( $arrValues['success_measures'] ) && $boolDirectSet ) $this->set( 'm_strSuccessMeasures', trim( $arrValues['success_measures'] ) ); elseif( isset( $arrValues['success_measures'] ) ) $this->setSuccessMeasures( $arrValues['success_measures'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['notice_period'] ) && $boolDirectSet ) $this->set( 'm_intNoticePeriod', trim( $arrValues['notice_period'] ) ); elseif( isset( $arrValues['notice_period'] ) ) $this->setNoticePeriod( $arrValues['notice_period'] );
		if( isset( $arrValues['probation_period'] ) && $boolDirectSet ) $this->set( 'm_intProbationPeriod', trim( $arrValues['probation_period'] ) ); elseif( isset( $arrValues['probation_period'] ) ) $this->setProbationPeriod( $arrValues['probation_period'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['bonus_approver_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intBonusApproverEmployeeId', trim( $arrValues['bonus_approver_employee_id'] ) ); elseif( isset( $arrValues['bonus_approver_employee_id'] ) ) $this->setBonusApproverEmployeeId( $arrValues['bonus_approver_employee_id'] );
		if( isset( $arrValues['bonus_criteria_approver_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intBonusCriteriaApproverEmployeeId', trim( $arrValues['bonus_criteria_approver_employee_id'] ) ); elseif( isset( $arrValues['bonus_criteria_approver_employee_id'] ) ) $this->setBonusCriteriaApproverEmployeeId( $arrValues['bonus_criteria_approver_employee_id'] );
		if( isset( $arrValues['designation_level_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationLevelId', trim( $arrValues['designation_level_id'] ) ); elseif( isset( $arrValues['designation_level_id'] ) ) $this->setDesignationLevelId( $arrValues['designation_level_id'] );
		if( isset( $arrValues['min_experience'] ) && $boolDirectSet ) $this->set( 'm_intMinExperience', trim( $arrValues['min_experience'] ) ); elseif( isset( $arrValues['min_experience'] ) ) $this->setMinExperience( $arrValues['min_experience'] );
		if( isset( $arrValues['max_experience'] ) && $boolDirectSet ) $this->set( 'm_intMaxExperience', trim( $arrValues['max_experience'] ) ); elseif( isset( $arrValues['max_experience'] ) ) $this->setMaxExperience( $arrValues['max_experience'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setApproverEmployeeId( $intApproverEmployeeId ) {
		$this->set( 'm_intApproverEmployeeId', CStrings::strToIntDef( $intApproverEmployeeId, NULL, false ) );
	}

	public function getApproverEmployeeId() {
		return $this->m_intApproverEmployeeId;
	}

	public function sqlApproverEmployeeId() {
		return ( true == isset( $this->m_intApproverEmployeeId ) ) ? ( string ) $this->m_intApproverEmployeeId : 'NULL';
	}

	public function setCompChangeApproverId( $intCompChangeApproverId ) {
		$this->set( 'm_intCompChangeApproverId', CStrings::strToIntDef( $intCompChangeApproverId, NULL, false ) );
	}

	public function getCompChangeApproverId() {
		return $this->m_intCompChangeApproverId;
	}

	public function sqlCompChangeApproverId() {
		return ( true == isset( $this->m_intCompChangeApproverId ) ) ? ( string ) $this->m_intCompChangeApproverId : 'NULL';
	}

	public function setReplacementApproverId( $intReplacementApproverId ) {
		$this->set( 'm_intReplacementApproverId', CStrings::strToIntDef( $intReplacementApproverId, NULL, false ) );
	}

	public function getReplacementApproverId() {
		return $this->m_intReplacementApproverId;
	}

	public function sqlReplacementApproverId() {
		return ( true == isset( $this->m_intReplacementApproverId ) ) ? ( string ) $this->m_intReplacementApproverId : 'NULL';
	}

	public function setEeocClassificationTypeId( $intEeocClassificationTypeId ) {
		$this->set( 'm_intEeocClassificationTypeId', CStrings::strToIntDef( $intEeocClassificationTypeId, NULL, false ) );
	}

	public function getEeocClassificationTypeId() {
		return $this->m_intEeocClassificationTypeId;
	}

	public function sqlEeocClassificationTypeId() {
		return ( true == isset( $this->m_intEeocClassificationTypeId ) ) ? ( string ) $this->m_intEeocClassificationTypeId : 'NULL';
	}

	public function setEmployeeSalaryTypeId( $intEmployeeSalaryTypeId ) {
		$this->set( 'm_intEmployeeSalaryTypeId', CStrings::strToIntDef( $intEmployeeSalaryTypeId, NULL, false ) );
	}

	public function getEmployeeSalaryTypeId() {
		return $this->m_intEmployeeSalaryTypeId;
	}

	public function sqlEmployeeSalaryTypeId() {
		return ( true == isset( $this->m_intEmployeeSalaryTypeId ) ) ? ( string ) $this->m_intEmployeeSalaryTypeId : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountryCode ) : '\'' . addslashes( $this->m_strCountryCode ) . '\'' ) : '\'US\'';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setGrade( $strGrade ) {
		$this->set( 'm_strGrade', CStrings::strTrimDef( $strGrade, 255, NULL, true ) );
	}

	public function getGrade() {
		return $this->m_strGrade;
	}

	public function sqlGrade() {
		return ( true == isset( $this->m_strGrade ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGrade ) : '\'' . addslashes( $this->m_strGrade ) . '\'' ) : 'NULL';
	}

	public function setJobResponsibilities( $strJobResponsibilities ) {
		$this->set( 'm_strJobResponsibilities', CStrings::strTrimDef( $strJobResponsibilities, -1, NULL, true ) );
	}

	public function getJobResponsibilities() {
		return $this->m_strJobResponsibilities;
	}

	public function sqlJobResponsibilities() {
		return ( true == isset( $this->m_strJobResponsibilities ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strJobResponsibilities ) : '\'' . addslashes( $this->m_strJobResponsibilities ) . '\'' ) : 'NULL';
	}

	public function setSuccessMeasures( $strSuccessMeasures ) {
		$this->set( 'm_strSuccessMeasures', CStrings::strTrimDef( $strSuccessMeasures, -1, NULL, true ) );
	}

	public function getSuccessMeasures() {
		return $this->m_strSuccessMeasures;
	}

	public function sqlSuccessMeasures() {
		return ( true == isset( $this->m_strSuccessMeasures ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSuccessMeasures ) : '\'' . addslashes( $this->m_strSuccessMeasures ) . '\'' ) : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setNoticePeriod( $intNoticePeriod ) {
		$this->set( 'm_intNoticePeriod', CStrings::strToIntDef( $intNoticePeriod, NULL, false ) );
	}

	public function getNoticePeriod() {
		return $this->m_intNoticePeriod;
	}

	public function sqlNoticePeriod() {
		return ( true == isset( $this->m_intNoticePeriod ) ) ? ( string ) $this->m_intNoticePeriod : 'NULL';
	}

	public function setProbationPeriod( $intProbationPeriod ) {
		$this->set( 'm_intProbationPeriod', CStrings::strToIntDef( $intProbationPeriod, NULL, false ) );
	}

	public function getProbationPeriod() {
		return $this->m_intProbationPeriod;
	}

	public function sqlProbationPeriod() {
		return ( true == isset( $this->m_intProbationPeriod ) ) ? ( string ) $this->m_intProbationPeriod : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setBonusApproverEmployeeId( $intBonusApproverEmployeeId ) {
		$this->set( 'm_intBonusApproverEmployeeId', CStrings::strToIntDef( $intBonusApproverEmployeeId, NULL, false ) );
	}

	public function getBonusApproverEmployeeId() {
		return $this->m_intBonusApproverEmployeeId;
	}

	public function sqlBonusApproverEmployeeId() {
		return ( true == isset( $this->m_intBonusApproverEmployeeId ) ) ? ( string ) $this->m_intBonusApproverEmployeeId : 'NULL';
	}

	public function setBonusCriteriaApproverEmployeeId( $intBonusCriteriaApproverEmployeeId ) {
		$this->set( 'm_intBonusCriteriaApproverEmployeeId', CStrings::strToIntDef( $intBonusCriteriaApproverEmployeeId, NULL, false ) );
	}

	public function getBonusCriteriaApproverEmployeeId() {
		return $this->m_intBonusCriteriaApproverEmployeeId;
	}

	public function sqlBonusCriteriaApproverEmployeeId() {
		return ( true == isset( $this->m_intBonusCriteriaApproverEmployeeId ) ) ? ( string ) $this->m_intBonusCriteriaApproverEmployeeId : 'NULL';
	}

	public function setDesignationLevelId( $intDesignationLevelId ) {
		$this->set( 'm_intDesignationLevelId', CStrings::strToIntDef( $intDesignationLevelId, NULL, false ) );
	}

	public function getDesignationLevelId() {
		return $this->m_intDesignationLevelId;
	}

	public function sqlDesignationLevelId() {
		return ( true == isset( $this->m_intDesignationLevelId ) ) ? ( string ) $this->m_intDesignationLevelId : 'NULL';
	}

	public function setMinExperience( $intMinExperience ) {
		$this->set( 'm_intMinExperience', CStrings::strToIntDef( $intMinExperience, NULL, false ) );
	}

	public function getMinExperience() {
		return $this->m_intMinExperience;
	}

	public function sqlMinExperience() {
		return ( true == isset( $this->m_intMinExperience ) ) ? ( string ) $this->m_intMinExperience : 'NULL';
	}

	public function setMaxExperience( $intMaxExperience ) {
		$this->set( 'm_intMaxExperience', CStrings::strToIntDef( $intMaxExperience, NULL, false ) );
	}

	public function getMaxExperience() {
		return $this->m_intMaxExperience;
	}

	public function sqlMaxExperience() {
		return ( true == isset( $this->m_intMaxExperience ) ) ? ( string ) $this->m_intMaxExperience : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, department_id, designation_id, approver_employee_id, comp_change_approver_id, replacement_approver_id, eeoc_classification_type_id, employee_salary_type_id, country_code, name, description, grade, job_responsibilities, success_measures, is_published, notice_period, probation_period, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, approved_by, approved_on, bonus_approver_employee_id, bonus_criteria_approver_employee_id, designation_level_id, min_experience, max_experience )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlDepartmentId() . ', ' .
						$this->sqlDesignationId() . ', ' .
						$this->sqlApproverEmployeeId() . ', ' .
						$this->sqlCompChangeApproverId() . ', ' .
						$this->sqlReplacementApproverId() . ', ' .
						$this->sqlEeocClassificationTypeId() . ', ' .
						$this->sqlEmployeeSalaryTypeId() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlGrade() . ', ' .
						$this->sqlJobResponsibilities() . ', ' .
						$this->sqlSuccessMeasures() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlNoticePeriod() . ', ' .
						$this->sqlProbationPeriod() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlBonusApproverEmployeeId() . ', ' .
						$this->sqlBonusCriteriaApproverEmployeeId() . ', ' .
						$this->sqlDesignationLevelId() . ', ' .
						$this->sqlMinExperience() . ', ' .
						$this->sqlMaxExperience() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId(). ',' ; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId(). ',' ; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approver_employee_id = ' . $this->sqlApproverEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ApproverEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' approver_employee_id = ' . $this->sqlApproverEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' comp_change_approver_id = ' . $this->sqlCompChangeApproverId(). ',' ; } elseif( true == array_key_exists( 'CompChangeApproverId', $this->getChangedColumns() ) ) { $strSql .= ' comp_change_approver_id = ' . $this->sqlCompChangeApproverId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' replacement_approver_id = ' . $this->sqlReplacementApproverId(). ',' ; } elseif( true == array_key_exists( 'ReplacementApproverId', $this->getChangedColumns() ) ) { $strSql .= ' replacement_approver_id = ' . $this->sqlReplacementApproverId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eeoc_classification_type_id = ' . $this->sqlEeocClassificationTypeId(). ',' ; } elseif( true == array_key_exists( 'EeocClassificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' eeoc_classification_type_id = ' . $this->sqlEeocClassificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_salary_type_id = ' . $this->sqlEmployeeSalaryTypeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeSalaryTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_salary_type_id = ' . $this->sqlEmployeeSalaryTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' grade = ' . $this->sqlGrade(). ',' ; } elseif( true == array_key_exists( 'Grade', $this->getChangedColumns() ) ) { $strSql .= ' grade = ' . $this->sqlGrade() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' job_responsibilities = ' . $this->sqlJobResponsibilities(). ',' ; } elseif( true == array_key_exists( 'JobResponsibilities', $this->getChangedColumns() ) ) { $strSql .= ' job_responsibilities = ' . $this->sqlJobResponsibilities() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' success_measures = ' . $this->sqlSuccessMeasures(). ',' ; } elseif( true == array_key_exists( 'SuccessMeasures', $this->getChangedColumns() ) ) { $strSql .= ' success_measures = ' . $this->sqlSuccessMeasures() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notice_period = ' . $this->sqlNoticePeriod(). ',' ; } elseif( true == array_key_exists( 'NoticePeriod', $this->getChangedColumns() ) ) { $strSql .= ' notice_period = ' . $this->sqlNoticePeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' probation_period = ' . $this->sqlProbationPeriod(). ',' ; } elseif( true == array_key_exists( 'ProbationPeriod', $this->getChangedColumns() ) ) { $strSql .= ' probation_period = ' . $this->sqlProbationPeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bonus_approver_employee_id = ' . $this->sqlBonusApproverEmployeeId(). ',' ; } elseif( true == array_key_exists( 'BonusApproverEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' bonus_approver_employee_id = ' . $this->sqlBonusApproverEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bonus_criteria_approver_employee_id = ' . $this->sqlBonusCriteriaApproverEmployeeId(). ',' ; } elseif( true == array_key_exists( 'BonusCriteriaApproverEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' bonus_criteria_approver_employee_id = ' . $this->sqlBonusCriteriaApproverEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_level_id = ' . $this->sqlDesignationLevelId(). ',' ; } elseif( true == array_key_exists( 'DesignationLevelId', $this->getChangedColumns() ) ) { $strSql .= ' designation_level_id = ' . $this->sqlDesignationLevelId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_experience = ' . $this->sqlMinExperience(). ',' ; } elseif( true == array_key_exists( 'MinExperience', $this->getChangedColumns() ) ) { $strSql .= ' min_experience = ' . $this->sqlMinExperience() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_experience = ' . $this->sqlMaxExperience(). ',' ; } elseif( true == array_key_exists( 'MaxExperience', $this->getChangedColumns() ) ) { $strSql .= ' max_experience = ' . $this->sqlMaxExperience() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'department_id' => $this->getDepartmentId(),
			'designation_id' => $this->getDesignationId(),
			'approver_employee_id' => $this->getApproverEmployeeId(),
			'comp_change_approver_id' => $this->getCompChangeApproverId(),
			'replacement_approver_id' => $this->getReplacementApproverId(),
			'eeoc_classification_type_id' => $this->getEeocClassificationTypeId(),
			'employee_salary_type_id' => $this->getEmployeeSalaryTypeId(),
			'country_code' => $this->getCountryCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'grade' => $this->getGrade(),
			'job_responsibilities' => $this->getJobResponsibilities(),
			'success_measures' => $this->getSuccessMeasures(),
			'is_published' => $this->getIsPublished(),
			'notice_period' => $this->getNoticePeriod(),
			'probation_period' => $this->getProbationPeriod(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'bonus_approver_employee_id' => $this->getBonusApproverEmployeeId(),
			'bonus_criteria_approver_employee_id' => $this->getBonusCriteriaApproverEmployeeId(),
			'designation_level_id' => $this->getDesignationLevelId(),
			'min_experience' => $this->getMinExperience(),
			'max_experience' => $this->getMaxExperience()
		);
	}

}
?>