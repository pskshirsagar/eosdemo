<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskSvnLogs
 * Do not add any new functions to this class.
 */

class CBaseTaskSvnLogs extends CEosPluralBase {

	/**
	 * @return CTaskSvnLog[]
	 */
	public static function fetchTaskSvnLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskSvnLog', $objDatabase );
	}

	/**
	 * @return CTaskSvnLog
	 */
	public static function fetchTaskSvnLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskSvnLog', $objDatabase );
	}

	public static function fetchTaskSvnLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_svn_logs', $objDatabase );
	}

	public static function fetchTaskSvnLogById( $intId, $objDatabase ) {
		return self::fetchTaskSvnLog( sprintf( 'SELECT * FROM task_svn_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskSvnLogsByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchTaskSvnLogs( sprintf( 'SELECT * FROM task_svn_logs WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

	public static function fetchTaskSvnLogsBySvnLogId( $intSvnLogId, $objDatabase ) {
		return self::fetchTaskSvnLogs( sprintf( 'SELECT * FROM task_svn_logs WHERE svn_log_id = %d', ( int ) $intSvnLogId ), $objDatabase );
	}

	public static function fetchTaskSvnLogsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchTaskSvnLogs( sprintf( 'SELECT * FROM task_svn_logs WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>