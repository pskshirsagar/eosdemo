<?php

class CBasePsProductOption extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_product_options';

	protected $m_intId;
	protected $m_intPsProductId;
	protected $m_intSetUpChargeCodeId;
	protected $m_intRecurringChargeCodeId;
	protected $m_intSdmEmployeeId;
	protected $m_intSqmEmployeeId;
	protected $m_intQaEmployeeId;
	protected $m_intQamEmployeeId;
	protected $m_intTechWriterEmployeeId;
	protected $m_intTpmEmployeeId;
	protected $m_intUxEmployeeId;
	protected $m_intBusinessAnalystEmployeeId;
	protected $m_intTlEmployeeId;
	protected $m_intSupportEmployeeId;
	protected $m_intSalesEmployeeId;
	protected $m_intProductOwnerEmployeeId;
	protected $m_intProductManagerEmployeeId;
	protected $m_intImplementationEmployeeId;
	protected $m_intExecutiveEmployeeId;
	protected $m_intDoeEmployeeId;
	protected $m_intArchitectEmployeeId;
	protected $m_intSmeEmployeeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_fltSetUpAmount;
	protected $m_fltMonthlyRecurringAmount;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltSetUpAmount = '0';
		$this->m_fltMonthlyRecurringAmount = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['set_up_charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intSetUpChargeCodeId', trim( $arrValues['set_up_charge_code_id'] ) ); elseif( isset( $arrValues['set_up_charge_code_id'] ) ) $this->setSetUpChargeCodeId( $arrValues['set_up_charge_code_id'] );
		if( isset( $arrValues['recurring_charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intRecurringChargeCodeId', trim( $arrValues['recurring_charge_code_id'] ) ); elseif( isset( $arrValues['recurring_charge_code_id'] ) ) $this->setRecurringChargeCodeId( $arrValues['recurring_charge_code_id'] );
		if( isset( $arrValues['sdm_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSdmEmployeeId', trim( $arrValues['sdm_employee_id'] ) ); elseif( isset( $arrValues['sdm_employee_id'] ) ) $this->setSdmEmployeeId( $arrValues['sdm_employee_id'] );
		if( isset( $arrValues['sqm_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSqmEmployeeId', trim( $arrValues['sqm_employee_id'] ) ); elseif( isset( $arrValues['sqm_employee_id'] ) ) $this->setSqmEmployeeId( $arrValues['sqm_employee_id'] );
		if( isset( $arrValues['qa_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQaEmployeeId', trim( $arrValues['qa_employee_id'] ) ); elseif( isset( $arrValues['qa_employee_id'] ) ) $this->setQaEmployeeId( $arrValues['qa_employee_id'] );
		if( isset( $arrValues['qam_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQamEmployeeId', trim( $arrValues['qam_employee_id'] ) ); elseif( isset( $arrValues['qam_employee_id'] ) ) $this->setQamEmployeeId( $arrValues['qam_employee_id'] );
		if( isset( $arrValues['tech_writer_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intTechWriterEmployeeId', trim( $arrValues['tech_writer_employee_id'] ) ); elseif( isset( $arrValues['tech_writer_employee_id'] ) ) $this->setTechWriterEmployeeId( $arrValues['tech_writer_employee_id'] );
		if( isset( $arrValues['tpm_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intTpmEmployeeId', trim( $arrValues['tpm_employee_id'] ) ); elseif( isset( $arrValues['tpm_employee_id'] ) ) $this->setTpmEmployeeId( $arrValues['tpm_employee_id'] );
		if( isset( $arrValues['ux_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intUxEmployeeId', trim( $arrValues['ux_employee_id'] ) ); elseif( isset( $arrValues['ux_employee_id'] ) ) $this->setUxEmployeeId( $arrValues['ux_employee_id'] );
		if( isset( $arrValues['business_analyst_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intBusinessAnalystEmployeeId', trim( $arrValues['business_analyst_employee_id'] ) ); elseif( isset( $arrValues['business_analyst_employee_id'] ) ) $this->setBusinessAnalystEmployeeId( $arrValues['business_analyst_employee_id'] );
		if( isset( $arrValues['tl_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intTlEmployeeId', trim( $arrValues['tl_employee_id'] ) ); elseif( isset( $arrValues['tl_employee_id'] ) ) $this->setTlEmployeeId( $arrValues['tl_employee_id'] );
		if( isset( $arrValues['support_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSupportEmployeeId', trim( $arrValues['support_employee_id'] ) ); elseif( isset( $arrValues['support_employee_id'] ) ) $this->setSupportEmployeeId( $arrValues['support_employee_id'] );
		if( isset( $arrValues['sales_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSalesEmployeeId', trim( $arrValues['sales_employee_id'] ) ); elseif( isset( $arrValues['sales_employee_id'] ) ) $this->setSalesEmployeeId( $arrValues['sales_employee_id'] );
		if( isset( $arrValues['product_owner_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intProductOwnerEmployeeId', trim( $arrValues['product_owner_employee_id'] ) ); elseif( isset( $arrValues['product_owner_employee_id'] ) ) $this->setProductOwnerEmployeeId( $arrValues['product_owner_employee_id'] );
		if( isset( $arrValues['product_manager_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intProductManagerEmployeeId', trim( $arrValues['product_manager_employee_id'] ) ); elseif( isset( $arrValues['product_manager_employee_id'] ) ) $this->setProductManagerEmployeeId( $arrValues['product_manager_employee_id'] );
		if( isset( $arrValues['implementation_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationEmployeeId', trim( $arrValues['implementation_employee_id'] ) ); elseif( isset( $arrValues['implementation_employee_id'] ) ) $this->setImplementationEmployeeId( $arrValues['implementation_employee_id'] );
		if( isset( $arrValues['executive_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intExecutiveEmployeeId', trim( $arrValues['executive_employee_id'] ) ); elseif( isset( $arrValues['executive_employee_id'] ) ) $this->setExecutiveEmployeeId( $arrValues['executive_employee_id'] );
		if( isset( $arrValues['doe_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intDoeEmployeeId', trim( $arrValues['doe_employee_id'] ) ); elseif( isset( $arrValues['doe_employee_id'] ) ) $this->setDoeEmployeeId( $arrValues['doe_employee_id'] );
		if( isset( $arrValues['architect_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intArchitectEmployeeId', trim( $arrValues['architect_employee_id'] ) ); elseif( isset( $arrValues['architect_employee_id'] ) ) $this->setArchitectEmployeeId( $arrValues['architect_employee_id'] );
		if( isset( $arrValues['sme_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSmeEmployeeId', trim( $arrValues['sme_employee_id'] ) ); elseif( isset( $arrValues['sme_employee_id'] ) ) $this->setSmeEmployeeId( $arrValues['sme_employee_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['set_up_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSetUpAmount', trim( $arrValues['set_up_amount'] ) ); elseif( isset( $arrValues['set_up_amount'] ) ) $this->setSetUpAmount( $arrValues['set_up_amount'] );
		if( isset( $arrValues['monthly_recurring_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyRecurringAmount', trim( $arrValues['monthly_recurring_amount'] ) ); elseif( isset( $arrValues['monthly_recurring_amount'] ) ) $this->setMonthlyRecurringAmount( $arrValues['monthly_recurring_amount'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setSetUpChargeCodeId( $intSetUpChargeCodeId ) {
		$this->set( 'm_intSetUpChargeCodeId', CStrings::strToIntDef( $intSetUpChargeCodeId, NULL, false ) );
	}

	public function getSetUpChargeCodeId() {
		return $this->m_intSetUpChargeCodeId;
	}

	public function sqlSetUpChargeCodeId() {
		return ( true == isset( $this->m_intSetUpChargeCodeId ) ) ? ( string ) $this->m_intSetUpChargeCodeId : 'NULL';
	}

	public function setRecurringChargeCodeId( $intRecurringChargeCodeId ) {
		$this->set( 'm_intRecurringChargeCodeId', CStrings::strToIntDef( $intRecurringChargeCodeId, NULL, false ) );
	}

	public function getRecurringChargeCodeId() {
		return $this->m_intRecurringChargeCodeId;
	}

	public function sqlRecurringChargeCodeId() {
		return ( true == isset( $this->m_intRecurringChargeCodeId ) ) ? ( string ) $this->m_intRecurringChargeCodeId : 'NULL';
	}

	public function setSdmEmployeeId( $intSdmEmployeeId ) {
		$this->set( 'm_intSdmEmployeeId', CStrings::strToIntDef( $intSdmEmployeeId, NULL, false ) );
	}

	public function getSdmEmployeeId() {
		return $this->m_intSdmEmployeeId;
	}

	public function sqlSdmEmployeeId() {
		return ( true == isset( $this->m_intSdmEmployeeId ) ) ? ( string ) $this->m_intSdmEmployeeId : 'NULL';
	}

	public function setSqmEmployeeId( $intSqmEmployeeId ) {
		$this->set( 'm_intSqmEmployeeId', CStrings::strToIntDef( $intSqmEmployeeId, NULL, false ) );
	}

	public function getSqmEmployeeId() {
		return $this->m_intSqmEmployeeId;
	}

	public function sqlSqmEmployeeId() {
		return ( true == isset( $this->m_intSqmEmployeeId ) ) ? ( string ) $this->m_intSqmEmployeeId : 'NULL';
	}

	public function setQaEmployeeId( $intQaEmployeeId ) {
		$this->set( 'm_intQaEmployeeId', CStrings::strToIntDef( $intQaEmployeeId, NULL, false ) );
	}

	public function getQaEmployeeId() {
		return $this->m_intQaEmployeeId;
	}

	public function sqlQaEmployeeId() {
		return ( true == isset( $this->m_intQaEmployeeId ) ) ? ( string ) $this->m_intQaEmployeeId : 'NULL';
	}

	public function setQamEmployeeId( $intQamEmployeeId ) {
		$this->set( 'm_intQamEmployeeId', CStrings::strToIntDef( $intQamEmployeeId, NULL, false ) );
	}

	public function getQamEmployeeId() {
		return $this->m_intQamEmployeeId;
	}

	public function sqlQamEmployeeId() {
		return ( true == isset( $this->m_intQamEmployeeId ) ) ? ( string ) $this->m_intQamEmployeeId : 'NULL';
	}

	public function setTechWriterEmployeeId( $intTechWriterEmployeeId ) {
		$this->set( 'm_intTechWriterEmployeeId', CStrings::strToIntDef( $intTechWriterEmployeeId, NULL, false ) );
	}

	public function getTechWriterEmployeeId() {
		return $this->m_intTechWriterEmployeeId;
	}

	public function sqlTechWriterEmployeeId() {
		return ( true == isset( $this->m_intTechWriterEmployeeId ) ) ? ( string ) $this->m_intTechWriterEmployeeId : 'NULL';
	}

	public function setTpmEmployeeId( $intTpmEmployeeId ) {
		$this->set( 'm_intTpmEmployeeId', CStrings::strToIntDef( $intTpmEmployeeId, NULL, false ) );
	}

	public function getTpmEmployeeId() {
		return $this->m_intTpmEmployeeId;
	}

	public function sqlTpmEmployeeId() {
		return ( true == isset( $this->m_intTpmEmployeeId ) ) ? ( string ) $this->m_intTpmEmployeeId : 'NULL';
	}

	public function setUxEmployeeId( $intUxEmployeeId ) {
		$this->set( 'm_intUxEmployeeId', CStrings::strToIntDef( $intUxEmployeeId, NULL, false ) );
	}

	public function getUxEmployeeId() {
		return $this->m_intUxEmployeeId;
	}

	public function sqlUxEmployeeId() {
		return ( true == isset( $this->m_intUxEmployeeId ) ) ? ( string ) $this->m_intUxEmployeeId : 'NULL';
	}

	public function setBusinessAnalystEmployeeId( $intBusinessAnalystEmployeeId ) {
		$this->set( 'm_intBusinessAnalystEmployeeId', CStrings::strToIntDef( $intBusinessAnalystEmployeeId, NULL, false ) );
	}

	public function getBusinessAnalystEmployeeId() {
		return $this->m_intBusinessAnalystEmployeeId;
	}

	public function sqlBusinessAnalystEmployeeId() {
		return ( true == isset( $this->m_intBusinessAnalystEmployeeId ) ) ? ( string ) $this->m_intBusinessAnalystEmployeeId : 'NULL';
	}

	public function setTlEmployeeId( $intTlEmployeeId ) {
		$this->set( 'm_intTlEmployeeId', CStrings::strToIntDef( $intTlEmployeeId, NULL, false ) );
	}

	public function getTlEmployeeId() {
		return $this->m_intTlEmployeeId;
	}

	public function sqlTlEmployeeId() {
		return ( true == isset( $this->m_intTlEmployeeId ) ) ? ( string ) $this->m_intTlEmployeeId : 'NULL';
	}

	public function setSupportEmployeeId( $intSupportEmployeeId ) {
		$this->set( 'm_intSupportEmployeeId', CStrings::strToIntDef( $intSupportEmployeeId, NULL, false ) );
	}

	public function getSupportEmployeeId() {
		return $this->m_intSupportEmployeeId;
	}

	public function sqlSupportEmployeeId() {
		return ( true == isset( $this->m_intSupportEmployeeId ) ) ? ( string ) $this->m_intSupportEmployeeId : 'NULL';
	}

	public function setSalesEmployeeId( $intSalesEmployeeId ) {
		$this->set( 'm_intSalesEmployeeId', CStrings::strToIntDef( $intSalesEmployeeId, NULL, false ) );
	}

	public function getSalesEmployeeId() {
		return $this->m_intSalesEmployeeId;
	}

	public function sqlSalesEmployeeId() {
		return ( true == isset( $this->m_intSalesEmployeeId ) ) ? ( string ) $this->m_intSalesEmployeeId : 'NULL';
	}

	public function setProductOwnerEmployeeId( $intProductOwnerEmployeeId ) {
		$this->set( 'm_intProductOwnerEmployeeId', CStrings::strToIntDef( $intProductOwnerEmployeeId, NULL, false ) );
	}

	public function getProductOwnerEmployeeId() {
		return $this->m_intProductOwnerEmployeeId;
	}

	public function sqlProductOwnerEmployeeId() {
		return ( true == isset( $this->m_intProductOwnerEmployeeId ) ) ? ( string ) $this->m_intProductOwnerEmployeeId : 'NULL';
	}

	public function setProductManagerEmployeeId( $intProductManagerEmployeeId ) {
		$this->set( 'm_intProductManagerEmployeeId', CStrings::strToIntDef( $intProductManagerEmployeeId, NULL, false ) );
	}

	public function getProductManagerEmployeeId() {
		return $this->m_intProductManagerEmployeeId;
	}

	public function sqlProductManagerEmployeeId() {
		return ( true == isset( $this->m_intProductManagerEmployeeId ) ) ? ( string ) $this->m_intProductManagerEmployeeId : 'NULL';
	}

	public function setImplementationEmployeeId( $intImplementationEmployeeId ) {
		$this->set( 'm_intImplementationEmployeeId', CStrings::strToIntDef( $intImplementationEmployeeId, NULL, false ) );
	}

	public function getImplementationEmployeeId() {
		return $this->m_intImplementationEmployeeId;
	}

	public function sqlImplementationEmployeeId() {
		return ( true == isset( $this->m_intImplementationEmployeeId ) ) ? ( string ) $this->m_intImplementationEmployeeId : 'NULL';
	}

	public function setExecutiveEmployeeId( $intExecutiveEmployeeId ) {
		$this->set( 'm_intExecutiveEmployeeId', CStrings::strToIntDef( $intExecutiveEmployeeId, NULL, false ) );
	}

	public function getExecutiveEmployeeId() {
		return $this->m_intExecutiveEmployeeId;
	}

	public function sqlExecutiveEmployeeId() {
		return ( true == isset( $this->m_intExecutiveEmployeeId ) ) ? ( string ) $this->m_intExecutiveEmployeeId : 'NULL';
	}

	public function setDoeEmployeeId( $intDoeEmployeeId ) {
		$this->set( 'm_intDoeEmployeeId', CStrings::strToIntDef( $intDoeEmployeeId, NULL, false ) );
	}

	public function getDoeEmployeeId() {
		return $this->m_intDoeEmployeeId;
	}

	public function sqlDoeEmployeeId() {
		return ( true == isset( $this->m_intDoeEmployeeId ) ) ? ( string ) $this->m_intDoeEmployeeId : 'NULL';
	}

	public function setArchitectEmployeeId( $intArchitectEmployeeId ) {
		$this->set( 'm_intArchitectEmployeeId', CStrings::strToIntDef( $intArchitectEmployeeId, NULL, false ) );
	}

	public function getArchitectEmployeeId() {
		return $this->m_intArchitectEmployeeId;
	}

	public function sqlArchitectEmployeeId() {
		return ( true == isset( $this->m_intArchitectEmployeeId ) ) ? ( string ) $this->m_intArchitectEmployeeId : 'NULL';
	}

	public function setSmeEmployeeId( $intSmeEmployeeId ) {
		$this->set( 'm_intSmeEmployeeId', CStrings::strToIntDef( $intSmeEmployeeId, NULL, false ) );
	}

	public function getSmeEmployeeId() {
		return $this->m_intSmeEmployeeId;
	}

	public function sqlSmeEmployeeId() {
		return ( true == isset( $this->m_intSmeEmployeeId ) ) ? ( string ) $this->m_intSmeEmployeeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setSetUpAmount( $fltSetUpAmount ) {
		$this->set( 'm_fltSetUpAmount', CStrings::strToFloatDef( $fltSetUpAmount, NULL, false, 2 ) );
	}

	public function getSetUpAmount() {
		return $this->m_fltSetUpAmount;
	}

	public function sqlSetUpAmount() {
		return ( true == isset( $this->m_fltSetUpAmount ) ) ? ( string ) $this->m_fltSetUpAmount : '0';
	}

	public function setMonthlyRecurringAmount( $fltMonthlyRecurringAmount ) {
		$this->set( 'm_fltMonthlyRecurringAmount', CStrings::strToFloatDef( $fltMonthlyRecurringAmount, NULL, false, 2 ) );
	}

	public function getMonthlyRecurringAmount() {
		return $this->m_fltMonthlyRecurringAmount;
	}

	public function sqlMonthlyRecurringAmount() {
		return ( true == isset( $this->m_fltMonthlyRecurringAmount ) ) ? ( string ) $this->m_fltMonthlyRecurringAmount : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_product_id, set_up_charge_code_id, recurring_charge_code_id, sdm_employee_id, sqm_employee_id, qa_employee_id, qam_employee_id, tech_writer_employee_id, tpm_employee_id, ux_employee_id, business_analyst_employee_id, tl_employee_id, support_employee_id, sales_employee_id, product_owner_employee_id, product_manager_employee_id, implementation_employee_id, executive_employee_id, doe_employee_id, architect_employee_id, sme_employee_id, name, description, set_up_amount, monthly_recurring_amount, is_published, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlSetUpChargeCodeId() . ', ' .
 						$this->sqlRecurringChargeCodeId() . ', ' .
 						$this->sqlSdmEmployeeId() . ', ' .
 						$this->sqlSqmEmployeeId() . ', ' .
 						$this->sqlQaEmployeeId() . ', ' .
 						$this->sqlQamEmployeeId() . ', ' .
 						$this->sqlTechWriterEmployeeId() . ', ' .
 						$this->sqlTpmEmployeeId() . ', ' .
 						$this->sqlUxEmployeeId() . ', ' .
 						$this->sqlBusinessAnalystEmployeeId() . ', ' .
 						$this->sqlTlEmployeeId() . ', ' .
 						$this->sqlSupportEmployeeId() . ', ' .
 						$this->sqlSalesEmployeeId() . ', ' .
 						$this->sqlProductOwnerEmployeeId() . ', ' .
 						$this->sqlProductManagerEmployeeId() . ', ' .
 						$this->sqlImplementationEmployeeId() . ', ' .
 						$this->sqlExecutiveEmployeeId() . ', ' .
 						$this->sqlDoeEmployeeId() . ', ' .
 						$this->sqlArchitectEmployeeId() . ', ' .
 						$this->sqlSmeEmployeeId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlSetUpAmount() . ', ' .
 						$this->sqlMonthlyRecurringAmount() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' set_up_charge_code_id = ' . $this->sqlSetUpChargeCodeId() . ','; } elseif( true == array_key_exists( 'SetUpChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' set_up_charge_code_id = ' . $this->sqlSetUpChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recurring_charge_code_id = ' . $this->sqlRecurringChargeCodeId() . ','; } elseif( true == array_key_exists( 'RecurringChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' recurring_charge_code_id = ' . $this->sqlRecurringChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sdm_employee_id = ' . $this->sqlSdmEmployeeId() . ','; } elseif( true == array_key_exists( 'SdmEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sdm_employee_id = ' . $this->sqlSdmEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sqm_employee_id = ' . $this->sqlSqmEmployeeId() . ','; } elseif( true == array_key_exists( 'SqmEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sqm_employee_id = ' . $this->sqlSqmEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId() . ','; } elseif( true == array_key_exists( 'QaEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qam_employee_id = ' . $this->sqlQamEmployeeId() . ','; } elseif( true == array_key_exists( 'QamEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qam_employee_id = ' . $this->sqlQamEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tech_writer_employee_id = ' . $this->sqlTechWriterEmployeeId() . ','; } elseif( true == array_key_exists( 'TechWriterEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' tech_writer_employee_id = ' . $this->sqlTechWriterEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tpm_employee_id = ' . $this->sqlTpmEmployeeId() . ','; } elseif( true == array_key_exists( 'TpmEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' tpm_employee_id = ' . $this->sqlTpmEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ux_employee_id = ' . $this->sqlUxEmployeeId() . ','; } elseif( true == array_key_exists( 'UxEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' ux_employee_id = ' . $this->sqlUxEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' business_analyst_employee_id = ' . $this->sqlBusinessAnalystEmployeeId() . ','; } elseif( true == array_key_exists( 'BusinessAnalystEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' business_analyst_employee_id = ' . $this->sqlBusinessAnalystEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tl_employee_id = ' . $this->sqlTlEmployeeId() . ','; } elseif( true == array_key_exists( 'TlEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' tl_employee_id = ' . $this->sqlTlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' support_employee_id = ' . $this->sqlSupportEmployeeId() . ','; } elseif( true == array_key_exists( 'SupportEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' support_employee_id = ' . $this->sqlSupportEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_employee_id = ' . $this->sqlSalesEmployeeId() . ','; } elseif( true == array_key_exists( 'SalesEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sales_employee_id = ' . $this->sqlSalesEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_owner_employee_id = ' . $this->sqlProductOwnerEmployeeId() . ','; } elseif( true == array_key_exists( 'ProductOwnerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' product_owner_employee_id = ' . $this->sqlProductOwnerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' product_manager_employee_id = ' . $this->sqlProductManagerEmployeeId() . ','; } elseif( true == array_key_exists( 'ProductManagerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' product_manager_employee_id = ' . $this->sqlProductManagerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_employee_id = ' . $this->sqlImplementationEmployeeId() . ','; } elseif( true == array_key_exists( 'ImplementationEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_employee_id = ' . $this->sqlImplementationEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executive_employee_id = ' . $this->sqlExecutiveEmployeeId() . ','; } elseif( true == array_key_exists( 'ExecutiveEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' executive_employee_id = ' . $this->sqlExecutiveEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' doe_employee_id = ' . $this->sqlDoeEmployeeId() . ','; } elseif( true == array_key_exists( 'DoeEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' doe_employee_id = ' . $this->sqlDoeEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' architect_employee_id = ' . $this->sqlArchitectEmployeeId() . ','; } elseif( true == array_key_exists( 'ArchitectEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' architect_employee_id = ' . $this->sqlArchitectEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sme_employee_id = ' . $this->sqlSmeEmployeeId() . ','; } elseif( true == array_key_exists( 'SmeEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sme_employee_id = ' . $this->sqlSmeEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' set_up_amount = ' . $this->sqlSetUpAmount() . ','; } elseif( true == array_key_exists( 'SetUpAmount', $this->getChangedColumns() ) ) { $strSql .= ' set_up_amount = ' . $this->sqlSetUpAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_recurring_amount = ' . $this->sqlMonthlyRecurringAmount() . ','; } elseif( true == array_key_exists( 'MonthlyRecurringAmount', $this->getChangedColumns() ) ) { $strSql .= ' monthly_recurring_amount = ' . $this->sqlMonthlyRecurringAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_product_id' => $this->getPsProductId(),
			'set_up_charge_code_id' => $this->getSetUpChargeCodeId(),
			'recurring_charge_code_id' => $this->getRecurringChargeCodeId(),
			'sdm_employee_id' => $this->getSdmEmployeeId(),
			'sqm_employee_id' => $this->getSqmEmployeeId(),
			'qa_employee_id' => $this->getQaEmployeeId(),
			'qam_employee_id' => $this->getQamEmployeeId(),
			'tech_writer_employee_id' => $this->getTechWriterEmployeeId(),
			'tpm_employee_id' => $this->getTpmEmployeeId(),
			'ux_employee_id' => $this->getUxEmployeeId(),
			'business_analyst_employee_id' => $this->getBusinessAnalystEmployeeId(),
			'tl_employee_id' => $this->getTlEmployeeId(),
			'support_employee_id' => $this->getSupportEmployeeId(),
			'sales_employee_id' => $this->getSalesEmployeeId(),
			'product_owner_employee_id' => $this->getProductOwnerEmployeeId(),
			'product_manager_employee_id' => $this->getProductManagerEmployeeId(),
			'implementation_employee_id' => $this->getImplementationEmployeeId(),
			'executive_employee_id' => $this->getExecutiveEmployeeId(),
			'doe_employee_id' => $this->getDoeEmployeeId(),
			'architect_employee_id' => $this->getArchitectEmployeeId(),
			'sme_employee_id' => $this->getSmeEmployeeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'set_up_amount' => $this->getSetUpAmount(),
			'monthly_recurring_amount' => $this->getMonthlyRecurringAmount(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>