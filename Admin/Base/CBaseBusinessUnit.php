<?php

class CBaseBusinessUnit extends CEosSingularBase {

	const TABLE_NAME = 'public.business_units';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intBuHrEmployeeId;
	protected $m_intDirectorEmployeeId;
	protected $m_intTrainingRepresentativeEmployeeId;
	protected $m_intRecruiterEmployeeId;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['bu_hr_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intBuHrEmployeeId', trim( $arrValues['bu_hr_employee_id'] ) ); elseif( isset( $arrValues['bu_hr_employee_id'] ) ) $this->setBuHrEmployeeId( $arrValues['bu_hr_employee_id'] );
		if( isset( $arrValues['director_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intDirectorEmployeeId', trim( $arrValues['director_employee_id'] ) ); elseif( isset( $arrValues['director_employee_id'] ) ) $this->setDirectorEmployeeId( $arrValues['director_employee_id'] );
		if( isset( $arrValues['training_representative_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingRepresentativeEmployeeId', trim( $arrValues['training_representative_employee_id'] ) ); elseif( isset( $arrValues['training_representative_employee_id'] ) ) $this->setTrainingRepresentativeEmployeeId( $arrValues['training_representative_employee_id'] );
		if( isset( $arrValues['recruiter_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intRecruiterEmployeeId', trim( $arrValues['recruiter_employee_id'] ) ); elseif( isset( $arrValues['recruiter_employee_id'] ) ) $this->setRecruiterEmployeeId( $arrValues['recruiter_employee_id'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setBuHrEmployeeId( $intBuHrEmployeeId ) {
		$this->set( 'm_intBuHrEmployeeId', CStrings::strToIntDef( $intBuHrEmployeeId, NULL, false ) );
	}

	public function getBuHrEmployeeId() {
		return $this->m_intBuHrEmployeeId;
	}

	public function sqlBuHrEmployeeId() {
		return ( true == isset( $this->m_intBuHrEmployeeId ) ) ? ( string ) $this->m_intBuHrEmployeeId : 'NULL';
	}

	public function setDirectorEmployeeId( $intDirectorEmployeeId ) {
		$this->set( 'm_intDirectorEmployeeId', CStrings::strToIntDef( $intDirectorEmployeeId, NULL, false ) );
	}

	public function getDirectorEmployeeId() {
		return $this->m_intDirectorEmployeeId;
	}

	public function sqlDirectorEmployeeId() {
		return ( true == isset( $this->m_intDirectorEmployeeId ) ) ? ( string ) $this->m_intDirectorEmployeeId : 'NULL';
	}

	public function setTrainingRepresentativeEmployeeId( $intTrainingRepresentativeEmployeeId ) {
		$this->set( 'm_intTrainingRepresentativeEmployeeId', CStrings::strToIntDef( $intTrainingRepresentativeEmployeeId, NULL, false ) );
	}

	public function getTrainingRepresentativeEmployeeId() {
		return $this->m_intTrainingRepresentativeEmployeeId;
	}

	public function sqlTrainingRepresentativeEmployeeId() {
		return ( true == isset( $this->m_intTrainingRepresentativeEmployeeId ) ) ? ( string ) $this->m_intTrainingRepresentativeEmployeeId : 'NULL';
	}

	public function setRecruiterEmployeeId( $intRecruiterEmployeeId ) {
		$this->set( 'm_intRecruiterEmployeeId', CStrings::strToIntDef( $intRecruiterEmployeeId, NULL, false ) );
	}

	public function getRecruiterEmployeeId() {
		return $this->m_intRecruiterEmployeeId;
	}

	public function sqlRecruiterEmployeeId() {
		return ( true == isset( $this->m_intRecruiterEmployeeId ) ) ? ( string ) $this->m_intRecruiterEmployeeId : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, description, bu_hr_employee_id, director_employee_id, training_representative_employee_id, recruiter_employee_id, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlBuHrEmployeeId() . ', ' .
 						$this->sqlDirectorEmployeeId() . ', ' .
 						$this->sqlTrainingRepresentativeEmployeeId() . ', ' .
 						$this->sqlRecruiterEmployeeId() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bu_hr_employee_id = ' . $this->sqlBuHrEmployeeId() . ','; } elseif( true == array_key_exists( 'BuHrEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' bu_hr_employee_id = ' . $this->sqlBuHrEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' director_employee_id = ' . $this->sqlDirectorEmployeeId() . ','; } elseif( true == array_key_exists( 'DirectorEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' director_employee_id = ' . $this->sqlDirectorEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_representative_employee_id = ' . $this->sqlTrainingRepresentativeEmployeeId() . ','; } elseif( true == array_key_exists( 'TrainingRepresentativeEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' training_representative_employee_id = ' . $this->sqlTrainingRepresentativeEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recruiter_employee_id = ' . $this->sqlRecruiterEmployeeId() . ','; } elseif( true == array_key_exists( 'RecruiterEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' recruiter_employee_id = ' . $this->sqlRecruiterEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'bu_hr_employee_id' => $this->getBuHrEmployeeId(),
			'director_employee_id' => $this->getDirectorEmployeeId(),
			'training_representative_employee_id' => $this->getTrainingRepresentativeEmployeeId(),
			'recruiter_employee_id' => $this->getRecruiterEmployeeId(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>