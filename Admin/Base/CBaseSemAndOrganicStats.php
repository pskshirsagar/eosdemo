<?php

class CBaseSemAndOrganicStats extends CEosPluralBase {

    const TABLE_SEM_AND_ORGANIC_STATS = 'public.sem_and_organic_stats';

    public static function fetchSemAndOrganicStats( $strSql, $objDatabase ) {
        return parent::fetchObjects( $strSql, 'CSemAndOrganicStat', $objDatabase );
    }

    public static function fetchSemAndOrganicStat( $strSql, $objDatabase ) {
        return parent::fetchObject( $strSql, 'CSemAndOrganicStat', $objDatabase );
    }

    public static function fetchSemAndOrganicStatCount( $strWhere = NULL, $objDatabase ) {
        return parent::fetchRowCount( $strWhere, 'sem_and_organic_stats', $objDatabase );
    }

    public static function fetchSemAndOrganicStatById( $intId, $objDatabase ) {
        return self::fetchSemAndOrganicStat( sprintf( 'SELECT * FROM sem_and_organic_stats WHERE id = %d', (int) $intId ), $objDatabase );
    }

    public static function fetchSemAndOrganicStatsByCid( $intCid, $objDatabase ) {
        return self::fetchSemAndOrganicStats( sprintf( 'SELECT * FROM sem_and_organic_stats WHERE cid = %d', (int) $intCid ), $objDatabase );
    }

    public static function fetchSemAndOrganicStatsByPropertyId( $intPropertyId, $objDatabase ) {
        return self::fetchSemAndOrganicStats( sprintf( 'SELECT * FROM sem_and_organic_stats WHERE property_id = %d', (int) $intPropertyId ), $objDatabase );
    }

    public static function fetchSemAndOrganicStatsBySemSourceId( $intSemSourceId, $objDatabase ) {
        return self::fetchSemAndOrganicStats( sprintf( 'SELECT * FROM sem_and_organic_stats WHERE sem_source_id = %d', (int) $intSemSourceId ), $objDatabase );
    }

    public static function fetchSemAndOrganicStatsBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
        return self::fetchSemAndOrganicStats( sprintf( 'SELECT * FROM sem_and_organic_stats WHERE sem_ad_group_id = %d', (int) $intSemAdGroupId ), $objDatabase );
    }

    public static function fetchSemAndOrganicStatsBySemKeywordId( $intSemKeywordId, $objDatabase ) {
        return self::fetchSemAndOrganicStats( sprintf( 'SELECT * FROM sem_and_organic_stats WHERE sem_keyword_id = %d', (int) $intSemKeywordId ), $objDatabase );
    }

}
?>