<?php

class CBaseStatsCsmEmployee extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_csm_employees';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_strMonth;
	protected $m_intNumberOfClientSuccessReviewsPerformed;
	protected $m_intNumberOfDemosPerformed;
	protected $m_intUpsellRevenue;
	protected $m_intNumberOfDelts;
	protected $m_intNumberOfTotalTouches;
	protected $m_intNpsCount;
	protected $m_fltNetPromoterScore;
	protected $m_intNumberOfDetractorFollowups;
	protected $m_intNumberOfDetractors;
	protected $m_intNumberOfAdoptionProjects;
	protected $m_intProfileCount;
	protected $m_fltAverageProfilePercentCompletion;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		$this->m_intNumberOfClientSuccessReviewsPerformed = '0';
		$this->m_intNumberOfDemosPerformed = '0';
		$this->m_intUpsellRevenue = '0';
		$this->m_intNumberOfDelts = '0';
		$this->m_intNumberOfTotalTouches = '0';
		$this->m_intNpsCount = '0';
		$this->m_fltNetPromoterScore = '0';
		$this->m_intNumberOfDetractorFollowups = '0';
		$this->m_intNumberOfDetractors = '0';
		$this->m_intNumberOfAdoptionProjects = '0';
		$this->m_intProfileCount = '0';
		$this->m_fltAverageProfilePercentCompletion = '0';
		$this->m_intCreatedBy = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['number_of_client_success_reviews_performed'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfClientSuccessReviewsPerformed', trim( $arrValues['number_of_client_success_reviews_performed'] ) ); elseif( isset( $arrValues['number_of_client_success_reviews_performed'] ) ) $this->setNumberOfClientSuccessReviewsPerformed( $arrValues['number_of_client_success_reviews_performed'] );
		if( isset( $arrValues['number_of_demos_performed'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfDemosPerformed', trim( $arrValues['number_of_demos_performed'] ) ); elseif( isset( $arrValues['number_of_demos_performed'] ) ) $this->setNumberOfDemosPerformed( $arrValues['number_of_demos_performed'] );
		if( isset( $arrValues['upsell_revenue'] ) && $boolDirectSet ) $this->set( 'm_intUpsellRevenue', trim( $arrValues['upsell_revenue'] ) ); elseif( isset( $arrValues['upsell_revenue'] ) ) $this->setUpsellRevenue( $arrValues['upsell_revenue'] );
		if( isset( $arrValues['number_of_delts'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfDelts', trim( $arrValues['number_of_delts'] ) ); elseif( isset( $arrValues['number_of_delts'] ) ) $this->setNumberOfDelts( $arrValues['number_of_delts'] );
		if( isset( $arrValues['number_of_total_touches'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfTotalTouches', trim( $arrValues['number_of_total_touches'] ) ); elseif( isset( $arrValues['number_of_total_touches'] ) ) $this->setNumberOfTotalTouches( $arrValues['number_of_total_touches'] );
		if( isset( $arrValues['nps_count'] ) && $boolDirectSet ) $this->set( 'm_intNpsCount', trim( $arrValues['nps_count'] ) ); elseif( isset( $arrValues['nps_count'] ) ) $this->setNpsCount( $arrValues['nps_count'] );
		if( isset( $arrValues['net_promoter_score'] ) && $boolDirectSet ) $this->set( 'm_fltNetPromoterScore', trim( $arrValues['net_promoter_score'] ) ); elseif( isset( $arrValues['net_promoter_score'] ) ) $this->setNetPromoterScore( $arrValues['net_promoter_score'] );
		if( isset( $arrValues['number_of_detractor_followups'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfDetractorFollowups', trim( $arrValues['number_of_detractor_followups'] ) ); elseif( isset( $arrValues['number_of_detractor_followups'] ) ) $this->setNumberOfDetractorFollowups( $arrValues['number_of_detractor_followups'] );
		if( isset( $arrValues['number_of_detractors'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfDetractors', trim( $arrValues['number_of_detractors'] ) ); elseif( isset( $arrValues['number_of_detractors'] ) ) $this->setNumberOfDetractors( $arrValues['number_of_detractors'] );
		if( isset( $arrValues['number_of_adoption_projects'] ) && $boolDirectSet ) $this->set( 'm_intNumberOfAdoptionProjects', trim( $arrValues['number_of_adoption_projects'] ) ); elseif( isset( $arrValues['number_of_adoption_projects'] ) ) $this->setNumberOfAdoptionProjects( $arrValues['number_of_adoption_projects'] );
		if( isset( $arrValues['profile_count'] ) && $boolDirectSet ) $this->set( 'm_intProfileCount', trim( $arrValues['profile_count'] ) ); elseif( isset( $arrValues['profile_count'] ) ) $this->setProfileCount( $arrValues['profile_count'] );
		if( isset( $arrValues['average_profile_percent_completion'] ) && $boolDirectSet ) $this->set( 'm_fltAverageProfilePercentCompletion', trim( $arrValues['average_profile_percent_completion'] ) ); elseif( isset( $arrValues['average_profile_percent_completion'] ) ) $this->setAverageProfilePercentCompletion( $arrValues['average_profile_percent_completion'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NOW()';
	}

	public function setNumberOfClientSuccessReviewsPerformed( $intNumberOfClientSuccessReviewsPerformed ) {
		$this->set( 'm_intNumberOfClientSuccessReviewsPerformed', CStrings::strToIntDef( $intNumberOfClientSuccessReviewsPerformed, NULL, false ) );
	}

	public function getNumberOfClientSuccessReviewsPerformed() {
		return $this->m_intNumberOfClientSuccessReviewsPerformed;
	}

	public function sqlNumberOfClientSuccessReviewsPerformed() {
		return ( true == isset( $this->m_intNumberOfClientSuccessReviewsPerformed ) ) ? ( string ) $this->m_intNumberOfClientSuccessReviewsPerformed : '0';
	}

	public function setNumberOfDemosPerformed( $intNumberOfDemosPerformed ) {
		$this->set( 'm_intNumberOfDemosPerformed', CStrings::strToIntDef( $intNumberOfDemosPerformed, NULL, false ) );
	}

	public function getNumberOfDemosPerformed() {
		return $this->m_intNumberOfDemosPerformed;
	}

	public function sqlNumberOfDemosPerformed() {
		return ( true == isset( $this->m_intNumberOfDemosPerformed ) ) ? ( string ) $this->m_intNumberOfDemosPerformed : '0';
	}

	public function setUpsellRevenue( $intUpsellRevenue ) {
		$this->set( 'm_intUpsellRevenue', CStrings::strToIntDef( $intUpsellRevenue, NULL, false ) );
	}

	public function getUpsellRevenue() {
		return $this->m_intUpsellRevenue;
	}

	public function sqlUpsellRevenue() {
		return ( true == isset( $this->m_intUpsellRevenue ) ) ? ( string ) $this->m_intUpsellRevenue : '0';
	}

	public function setNumberOfDelts( $intNumberOfDelts ) {
		$this->set( 'm_intNumberOfDelts', CStrings::strToIntDef( $intNumberOfDelts, NULL, false ) );
	}

	public function getNumberOfDelts() {
		return $this->m_intNumberOfDelts;
	}

	public function sqlNumberOfDelts() {
		return ( true == isset( $this->m_intNumberOfDelts ) ) ? ( string ) $this->m_intNumberOfDelts : '0';
	}

	public function setNumberOfTotalTouches( $intNumberOfTotalTouches ) {
		$this->set( 'm_intNumberOfTotalTouches', CStrings::strToIntDef( $intNumberOfTotalTouches, NULL, false ) );
	}

	public function getNumberOfTotalTouches() {
		return $this->m_intNumberOfTotalTouches;
	}

	public function sqlNumberOfTotalTouches() {
		return ( true == isset( $this->m_intNumberOfTotalTouches ) ) ? ( string ) $this->m_intNumberOfTotalTouches : '0';
	}

	public function setNpsCount( $intNpsCount ) {
		$this->set( 'm_intNpsCount', CStrings::strToIntDef( $intNpsCount, NULL, false ) );
	}

	public function getNpsCount() {
		return $this->m_intNpsCount;
	}

	public function sqlNpsCount() {
		return ( true == isset( $this->m_intNpsCount ) ) ? ( string ) $this->m_intNpsCount : '0';
	}

	public function setNetPromoterScore( $fltNetPromoterScore ) {
		$this->set( 'm_fltNetPromoterScore', CStrings::strToFloatDef( $fltNetPromoterScore, NULL, false, 2 ) );
	}

	public function getNetPromoterScore() {
		return $this->m_fltNetPromoterScore;
	}

	public function sqlNetPromoterScore() {
		return ( true == isset( $this->m_fltNetPromoterScore ) ) ? ( string ) $this->m_fltNetPromoterScore : '0';
	}

	public function setNumberOfDetractorFollowups( $intNumberOfDetractorFollowups ) {
		$this->set( 'm_intNumberOfDetractorFollowups', CStrings::strToIntDef( $intNumberOfDetractorFollowups, NULL, false ) );
	}

	public function getNumberOfDetractorFollowups() {
		return $this->m_intNumberOfDetractorFollowups;
	}

	public function sqlNumberOfDetractorFollowups() {
		return ( true == isset( $this->m_intNumberOfDetractorFollowups ) ) ? ( string ) $this->m_intNumberOfDetractorFollowups : '0';
	}

	public function setNumberOfDetractors( $intNumberOfDetractors ) {
		$this->set( 'm_intNumberOfDetractors', CStrings::strToIntDef( $intNumberOfDetractors, NULL, false ) );
	}

	public function getNumberOfDetractors() {
		return $this->m_intNumberOfDetractors;
	}

	public function sqlNumberOfDetractors() {
		return ( true == isset( $this->m_intNumberOfDetractors ) ) ? ( string ) $this->m_intNumberOfDetractors : '0';
	}

	public function setNumberOfAdoptionProjects( $intNumberOfAdoptionProjects ) {
		$this->set( 'm_intNumberOfAdoptionProjects', CStrings::strToIntDef( $intNumberOfAdoptionProjects, NULL, false ) );
	}

	public function getNumberOfAdoptionProjects() {
		return $this->m_intNumberOfAdoptionProjects;
	}

	public function sqlNumberOfAdoptionProjects() {
		return ( true == isset( $this->m_intNumberOfAdoptionProjects ) ) ? ( string ) $this->m_intNumberOfAdoptionProjects : '0';
	}

	public function setProfileCount( $intProfileCount ) {
		$this->set( 'm_intProfileCount', CStrings::strToIntDef( $intProfileCount, NULL, false ) );
	}

	public function getProfileCount() {
		return $this->m_intProfileCount;
	}

	public function sqlProfileCount() {
		return ( true == isset( $this->m_intProfileCount ) ) ? ( string ) $this->m_intProfileCount : '0';
	}

	public function setAverageProfilePercentCompletion( $fltAverageProfilePercentCompletion ) {
		$this->set( 'm_fltAverageProfilePercentCompletion', CStrings::strToFloatDef( $fltAverageProfilePercentCompletion, NULL, false, 2 ) );
	}

	public function getAverageProfilePercentCompletion() {
		return $this->m_fltAverageProfilePercentCompletion;
	}

	public function sqlAverageProfilePercentCompletion() {
		return ( true == isset( $this->m_fltAverageProfilePercentCompletion ) ) ? ( string ) $this->m_fltAverageProfilePercentCompletion : '0';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '0';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, month, number_of_client_success_reviews_performed, number_of_demos_performed, upsell_revenue, number_of_delts, number_of_total_touches, nps_count, net_promoter_score, number_of_detractor_followups, number_of_detractors, number_of_adoption_projects, profile_count, average_profile_percent_completion, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlMonth() . ', ' .
 						$this->sqlNumberOfClientSuccessReviewsPerformed() . ', ' .
 						$this->sqlNumberOfDemosPerformed() . ', ' .
 						$this->sqlUpsellRevenue() . ', ' .
 						$this->sqlNumberOfDelts() . ', ' .
 						$this->sqlNumberOfTotalTouches() . ', ' .
 						$this->sqlNpsCount() . ', ' .
 						$this->sqlNetPromoterScore() . ', ' .
 						$this->sqlNumberOfDetractorFollowups() . ', ' .
 						$this->sqlNumberOfDetractors() . ', ' .
 						$this->sqlNumberOfAdoptionProjects() . ', ' .
 						$this->sqlProfileCount() . ', ' .
 						$this->sqlAverageProfilePercentCompletion() . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_client_success_reviews_performed = ' . $this->sqlNumberOfClientSuccessReviewsPerformed() . ','; } elseif( true == array_key_exists( 'NumberOfClientSuccessReviewsPerformed', $this->getChangedColumns() ) ) { $strSql .= ' number_of_client_success_reviews_performed = ' . $this->sqlNumberOfClientSuccessReviewsPerformed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_demos_performed = ' . $this->sqlNumberOfDemosPerformed() . ','; } elseif( true == array_key_exists( 'NumberOfDemosPerformed', $this->getChangedColumns() ) ) { $strSql .= ' number_of_demos_performed = ' . $this->sqlNumberOfDemosPerformed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' upsell_revenue = ' . $this->sqlUpsellRevenue() . ','; } elseif( true == array_key_exists( 'UpsellRevenue', $this->getChangedColumns() ) ) { $strSql .= ' upsell_revenue = ' . $this->sqlUpsellRevenue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_delts = ' . $this->sqlNumberOfDelts() . ','; } elseif( true == array_key_exists( 'NumberOfDelts', $this->getChangedColumns() ) ) { $strSql .= ' number_of_delts = ' . $this->sqlNumberOfDelts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_total_touches = ' . $this->sqlNumberOfTotalTouches() . ','; } elseif( true == array_key_exists( 'NumberOfTotalTouches', $this->getChangedColumns() ) ) { $strSql .= ' number_of_total_touches = ' . $this->sqlNumberOfTotalTouches() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nps_count = ' . $this->sqlNpsCount() . ','; } elseif( true == array_key_exists( 'NpsCount', $this->getChangedColumns() ) ) { $strSql .= ' nps_count = ' . $this->sqlNpsCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' net_promoter_score = ' . $this->sqlNetPromoterScore() . ','; } elseif( true == array_key_exists( 'NetPromoterScore', $this->getChangedColumns() ) ) { $strSql .= ' net_promoter_score = ' . $this->sqlNetPromoterScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_detractor_followups = ' . $this->sqlNumberOfDetractorFollowups() . ','; } elseif( true == array_key_exists( 'NumberOfDetractorFollowups', $this->getChangedColumns() ) ) { $strSql .= ' number_of_detractor_followups = ' . $this->sqlNumberOfDetractorFollowups() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_detractors = ' . $this->sqlNumberOfDetractors() . ','; } elseif( true == array_key_exists( 'NumberOfDetractors', $this->getChangedColumns() ) ) { $strSql .= ' number_of_detractors = ' . $this->sqlNumberOfDetractors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' number_of_adoption_projects = ' . $this->sqlNumberOfAdoptionProjects() . ','; } elseif( true == array_key_exists( 'NumberOfAdoptionProjects', $this->getChangedColumns() ) ) { $strSql .= ' number_of_adoption_projects = ' . $this->sqlNumberOfAdoptionProjects() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' profile_count = ' . $this->sqlProfileCount() . ','; } elseif( true == array_key_exists( 'ProfileCount', $this->getChangedColumns() ) ) { $strSql .= ' profile_count = ' . $this->sqlProfileCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_profile_percent_completion = ' . $this->sqlAverageProfilePercentCompletion() . ','; } elseif( true == array_key_exists( 'AverageProfilePercentCompletion', $this->getChangedColumns() ) ) { $strSql .= ' average_profile_percent_completion = ' . $this->sqlAverageProfilePercentCompletion() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'month' => $this->getMonth(),
			'number_of_client_success_reviews_performed' => $this->getNumberOfClientSuccessReviewsPerformed(),
			'number_of_demos_performed' => $this->getNumberOfDemosPerformed(),
			'upsell_revenue' => $this->getUpsellRevenue(),
			'number_of_delts' => $this->getNumberOfDelts(),
			'number_of_total_touches' => $this->getNumberOfTotalTouches(),
			'nps_count' => $this->getNpsCount(),
			'net_promoter_score' => $this->getNetPromoterScore(),
			'number_of_detractor_followups' => $this->getNumberOfDetractorFollowups(),
			'number_of_detractors' => $this->getNumberOfDetractors(),
			'number_of_adoption_projects' => $this->getNumberOfAdoptionProjects(),
			'profile_count' => $this->getProfileCount(),
			'average_profile_percent_completion' => $this->getAverageProfilePercentCompletion(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>