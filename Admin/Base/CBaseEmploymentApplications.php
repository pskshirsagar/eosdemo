<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmploymentApplications
 * Do not add any new functions to this class.
 */

class CBaseEmploymentApplications extends CEosPluralBase {

	/**
	 * @return CEmploymentApplication[]
	 */
	public static function fetchEmploymentApplications( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmploymentApplication', $objDatabase );
	}

	/**
	 * @return CEmploymentApplication
	 */
	public static function fetchEmploymentApplication( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmploymentApplication', $objDatabase );
	}

	public static function fetchEmploymentApplicationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employment_applications', $objDatabase );
	}

	public static function fetchEmploymentApplicationById( $intId, $objDatabase ) {
		return self::fetchEmploymentApplication( sprintf( 'SELECT * FROM employment_applications WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmploymentApplicationsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchEmploymentApplications( sprintf( 'SELECT * FROM employment_applications WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>