<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPlannerParticipants
 * Do not add any new functions to this class.
 */

class CBasePlannerParticipants extends CEosPluralBase {

	/**
	 * @return CPlannerParticipant[]
	 */
	public static function fetchPlannerParticipants( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPlannerParticipant', $objDatabase );
	}

	/**
	 * @return CPlannerParticipant
	 */
	public static function fetchPlannerParticipant( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPlannerParticipant', $objDatabase );
	}

	public static function fetchPlannerParticipantCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'planner_participants', $objDatabase );
	}

	public static function fetchPlannerParticipantById( $intId, $objDatabase ) {
		return self::fetchPlannerParticipant( sprintf( 'SELECT * FROM planner_participants WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPlannerParticipantsByPlannerId( $intPlannerId, $objDatabase ) {
		return self::fetchPlannerParticipants( sprintf( 'SELECT * FROM planner_participants WHERE planner_id = %d', ( int ) $intPlannerId ), $objDatabase );
	}

	public static function fetchPlannerParticipantsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchPlannerParticipants( sprintf( 'SELECT * FROM planner_participants WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>