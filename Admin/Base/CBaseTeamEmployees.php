<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTeamEmployees
 * Do not add any new functions to this class.
 */

class CBaseTeamEmployees extends CEosPluralBase {

	/**
	 * @return CTeamEmployee[]
	 */
	public static function fetchTeamEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTeamEmployee', $objDatabase );
	}

	/**
	 * @return CTeamEmployee
	 */
	public static function fetchTeamEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTeamEmployee', $objDatabase );
	}

	public static function fetchTeamEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'team_employees', $objDatabase );
	}

	public static function fetchTeamEmployeeById( $intId, $objDatabase ) {
		return self::fetchTeamEmployee( sprintf( 'SELECT * FROM team_employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTeamEmployeesByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchTeamEmployees( sprintf( 'SELECT * FROM team_employees WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

	public static function fetchTeamEmployeesByReportingTeamId( $intReportingTeamId, $objDatabase ) {
		return self::fetchTeamEmployees( sprintf( 'SELECT * FROM team_employees WHERE reporting_team_id = %d', ( int ) $intReportingTeamId ), $objDatabase );
	}

	public static function fetchTeamEmployeesByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		return self::fetchTeamEmployees( sprintf( 'SELECT * FROM team_employees WHERE manager_employee_id = %d', ( int ) $intManagerEmployeeId ), $objDatabase );
	}

	public static function fetchTeamEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchTeamEmployees( sprintf( 'SELECT * FROM team_employees WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>