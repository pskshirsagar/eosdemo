<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CHonourBadgeReasons
 * Do not add any new functions to this class.
 */

class CBaseHonourBadgeReasons extends CEosPluralBase {

	/**
	 * @return CHonourBadgeReason[]
	 */
	public static function fetchHonourBadgeReasons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CHonourBadgeReason::class, $objDatabase );
	}

	/**
	 * @return CHonourBadgeReason
	 */
	public static function fetchHonourBadgeReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CHonourBadgeReason::class, $objDatabase );
	}

	public static function fetchHonourBadgeReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'honour_badge_reasons', $objDatabase );
	}

	public static function fetchHonourBadgeReasonById( $intId, $objDatabase ) {
		return self::fetchHonourBadgeReason( sprintf( 'SELECT * FROM honour_badge_reasons WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchHonourBadgeReasonsByHonourBadgeTypeId( $intHonourBadgeTypeId, $objDatabase ) {
		return self::fetchHonourBadgeReasons( sprintf( 'SELECT * FROM honour_badge_reasons WHERE honour_badge_type_id = %d', ( int ) $intHonourBadgeTypeId ), $objDatabase );
	}

}
?>