<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserModulePermissions
 * Do not add any new functions to this class.
 */

class CBaseUserModulePermissions extends CEosPluralBase {

	/**
	 * @return CUserModulePermission[]
	 */
	public static function fetchUserModulePermissions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserModulePermission', $objDatabase );
	}

	/**
	 * @return CUserModulePermission
	 */
	public static function fetchUserModulePermission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserModulePermission', $objDatabase );
	}

	public static function fetchUserModulePermissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_module_permissions', $objDatabase );
	}

	public static function fetchUserModulePermissionById( $intId, $objDatabase ) {
		return self::fetchUserModulePermission( sprintf( 'SELECT * FROM user_module_permissions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserModulePermissionsByUserId( $intUserId, $objDatabase ) {
		return self::fetchUserModulePermissions( sprintf( 'SELECT * FROM user_module_permissions WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

	public static function fetchUserModulePermissionsByModuleId( $intModuleId, $objDatabase ) {
		return self::fetchUserModulePermissions( sprintf( 'SELECT * FROM user_module_permissions WHERE module_id = %d', ( int ) $intModuleId ), $objDatabase );
	}

}
?>