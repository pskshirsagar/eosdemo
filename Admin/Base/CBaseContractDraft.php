<?php

class CBaseContractDraft extends CEosSingularBase {

	const TABLE_NAME = 'public.contract_drafts';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intContractId;
	protected $m_intContractDraftStatusId;
	protected $m_intContactPersonId;
	protected $m_intPsDocumentId;
	protected $m_strSubject;
	protected $m_boolIsUrgent;
	protected $m_strSentOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intSalesEmployeeId;
	protected $m_boolIsManualSignature;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsUrgent = false;
		$this->m_boolIsManualSignature = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['contract_draft_status_id'] ) && $boolDirectSet ) $this->set( 'm_intContractDraftStatusId', trim( $arrValues['contract_draft_status_id'] ) ); elseif( isset( $arrValues['contract_draft_status_id'] ) ) $this->setContractDraftStatusId( $arrValues['contract_draft_status_id'] );
		if( isset( $arrValues['contact_person_id'] ) && $boolDirectSet ) $this->set( 'm_intContactPersonId', trim( $arrValues['contact_person_id'] ) ); elseif( isset( $arrValues['contact_person_id'] ) ) $this->setContactPersonId( $arrValues['contact_person_id'] );
		if( isset( $arrValues['ps_document_id'] ) && $boolDirectSet ) $this->set( 'm_intPsDocumentId', trim( $arrValues['ps_document_id'] ) ); elseif( isset( $arrValues['ps_document_id'] ) ) $this->setPsDocumentId( $arrValues['ps_document_id'] );
		if( isset( $arrValues['subject'] ) && $boolDirectSet ) $this->set( 'm_strSubject', trim( $arrValues['subject'] ) ); elseif( isset( $arrValues['subject'] ) ) $this->setSubject( $arrValues['subject'] );
		if( isset( $arrValues['is_urgent'] ) && $boolDirectSet ) $this->set( 'm_boolIsUrgent', trim( stripcslashes( $arrValues['is_urgent'] ) ) ); elseif( isset( $arrValues['is_urgent'] ) ) $this->setIsUrgent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_urgent'] ) : $arrValues['is_urgent'] );
		if( isset( $arrValues['sent_on'] ) && $boolDirectSet ) $this->set( 'm_strSentOn', trim( $arrValues['sent_on'] ) ); elseif( isset( $arrValues['sent_on'] ) ) $this->setSentOn( $arrValues['sent_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['sales_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSalesEmployeeId', trim( $arrValues['sales_employee_id'] ) ); elseif( isset( $arrValues['sales_employee_id'] ) ) $this->setSalesEmployeeId( $arrValues['sales_employee_id'] );
		if( isset( $arrValues['is_manual_signature'] ) && $boolDirectSet ) $this->set( 'm_boolIsManualSignature', trim( stripcslashes( $arrValues['is_manual_signature'] ) ) ); elseif( isset( $arrValues['is_manual_signature'] ) ) $this->setIsManualSignature( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_manual_signature'] ) : $arrValues['is_manual_signature'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setContractDraftStatusId( $intContractDraftStatusId ) {
		$this->set( 'm_intContractDraftStatusId', CStrings::strToIntDef( $intContractDraftStatusId, NULL, false ) );
	}

	public function getContractDraftStatusId() {
		return $this->m_intContractDraftStatusId;
	}

	public function sqlContractDraftStatusId() {
		return ( true == isset( $this->m_intContractDraftStatusId ) ) ? ( string ) $this->m_intContractDraftStatusId : 'NULL';
	}

	public function setContactPersonId( $intContactPersonId ) {
		$this->set( 'm_intContactPersonId', CStrings::strToIntDef( $intContactPersonId, NULL, false ) );
	}

	public function getContactPersonId() {
		return $this->m_intContactPersonId;
	}

	public function sqlContactPersonId() {
		return ( true == isset( $this->m_intContactPersonId ) ) ? ( string ) $this->m_intContactPersonId : 'NULL';
	}

	public function setPsDocumentId( $intPsDocumentId ) {
		$this->set( 'm_intPsDocumentId', CStrings::strToIntDef( $intPsDocumentId, NULL, false ) );
	}

	public function getPsDocumentId() {
		return $this->m_intPsDocumentId;
	}

	public function sqlPsDocumentId() {
		return ( true == isset( $this->m_intPsDocumentId ) ) ? ( string ) $this->m_intPsDocumentId : 'NULL';
	}

	public function setSubject( $strSubject ) {
		$this->set( 'm_strSubject', CStrings::strTrimDef( $strSubject, 240, NULL, true ) );
	}

	public function getSubject() {
		return $this->m_strSubject;
	}

	public function sqlSubject() {
		return ( true == isset( $this->m_strSubject ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSubject ) : '\'' . addslashes( $this->m_strSubject ) . '\'' ) : 'NULL';
	}

	public function setIsUrgent( $boolIsUrgent ) {
		$this->set( 'm_boolIsUrgent', CStrings::strToBool( $boolIsUrgent ) );
	}

	public function getIsUrgent() {
		return $this->m_boolIsUrgent;
	}

	public function sqlIsUrgent() {
		return ( true == isset( $this->m_boolIsUrgent ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUrgent ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSentOn( $strSentOn ) {
		$this->set( 'm_strSentOn', CStrings::strTrimDef( $strSentOn, -1, NULL, true ) );
	}

	public function getSentOn() {
		return $this->m_strSentOn;
	}

	public function sqlSentOn() {
		return ( true == isset( $this->m_strSentOn ) ) ? '\'' . $this->m_strSentOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSalesEmployeeId( $intSalesEmployeeId ) {
		$this->set( 'm_intSalesEmployeeId', CStrings::strToIntDef( $intSalesEmployeeId, NULL, false ) );
	}

	public function getSalesEmployeeId() {
		return $this->m_intSalesEmployeeId;
	}

	public function sqlSalesEmployeeId() {
		return ( true == isset( $this->m_intSalesEmployeeId ) ) ? ( string ) $this->m_intSalesEmployeeId : 'NULL';
	}

	public function setIsManualSignature( $boolIsManualSignature ) {
		$this->set( 'm_boolIsManualSignature', CStrings::strToBool( $boolIsManualSignature ) );
	}

	public function getIsManualSignature() {
		return $this->m_boolIsManualSignature;
	}

	public function sqlIsManualSignature() {
		return ( true == isset( $this->m_boolIsManualSignature ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsManualSignature ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, contract_id, contract_draft_status_id, contact_person_id, ps_document_id, subject, is_urgent, sent_on, approved_by, approved_on, updated_by, updated_on, created_by, created_on, sales_employee_id, is_manual_signature )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlContractId() . ', ' .
						$this->sqlContractDraftStatusId() . ', ' .
						$this->sqlContactPersonId() . ', ' .
						$this->sqlPsDocumentId() . ', ' .
						$this->sqlSubject() . ', ' .
						$this->sqlIsUrgent() . ', ' .
						$this->sqlSentOn() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlSalesEmployeeId() . ', ' .
						$this->sqlIsManualSignature() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId(). ',' ; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_draft_status_id = ' . $this->sqlContractDraftStatusId(). ',' ; } elseif( true == array_key_exists( 'ContractDraftStatusId', $this->getChangedColumns() ) ) { $strSql .= ' contract_draft_status_id = ' . $this->sqlContractDraftStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_person_id = ' . $this->sqlContactPersonId(). ',' ; } elseif( true == array_key_exists( 'ContactPersonId', $this->getChangedColumns() ) ) { $strSql .= ' contact_person_id = ' . $this->sqlContactPersonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId(). ',' ; } elseif( true == array_key_exists( 'PsDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' subject = ' . $this->sqlSubject(). ',' ; } elseif( true == array_key_exists( 'Subject', $this->getChangedColumns() ) ) { $strSql .= ' subject = ' . $this->sqlSubject() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_urgent = ' . $this->sqlIsUrgent(). ',' ; } elseif( true == array_key_exists( 'IsUrgent', $this->getChangedColumns() ) ) { $strSql .= ' is_urgent = ' . $this->sqlIsUrgent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_on = ' . $this->sqlSentOn(). ',' ; } elseif( true == array_key_exists( 'SentOn', $this->getChangedColumns() ) ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_employee_id = ' . $this->sqlSalesEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SalesEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sales_employee_id = ' . $this->sqlSalesEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_manual_signature = ' . $this->sqlIsManualSignature(). ',' ; } elseif( true == array_key_exists( 'IsManualSignature', $this->getChangedColumns() ) ) { $strSql .= ' is_manual_signature = ' . $this->sqlIsManualSignature() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'contract_id' => $this->getContractId(),
			'contract_draft_status_id' => $this->getContractDraftStatusId(),
			'contact_person_id' => $this->getContactPersonId(),
			'ps_document_id' => $this->getPsDocumentId(),
			'subject' => $this->getSubject(),
			'is_urgent' => $this->getIsUrgent(),
			'sent_on' => $this->getSentOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'sales_employee_id' => $this->getSalesEmployeeId(),
			'is_manual_signature' => $this->getIsManualSignature()
		);
	}

}
?>
