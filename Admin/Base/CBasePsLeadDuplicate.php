<?php

class CBasePsLeadDuplicate extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_lead_duplicates';

	protected $m_intId;
	protected $m_intPsLeadDuplicateTypeId;
	protected $m_intPsLeadId1;
	protected $m_intPsLeadId2;
	protected $m_strReason;
	protected $m_intIsDuplicate;
	protected $m_intIgnoredBy;
	protected $m_strIgnoredOn;
	protected $m_intProcessedBy;
	protected $m_strProcessedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_lead_duplicate_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadDuplicateTypeId', trim( $arrValues['ps_lead_duplicate_type_id'] ) ); elseif( isset( $arrValues['ps_lead_duplicate_type_id'] ) ) $this->setPsLeadDuplicateTypeId( $arrValues['ps_lead_duplicate_type_id'] );
		if( isset( $arrValues['ps_lead_id1'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId1', trim( $arrValues['ps_lead_id1'] ) ); elseif( isset( $arrValues['ps_lead_id1'] ) ) $this->setPsLeadId1( $arrValues['ps_lead_id1'] );
		if( isset( $arrValues['ps_lead_id2'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId2', trim( $arrValues['ps_lead_id2'] ) ); elseif( isset( $arrValues['ps_lead_id2'] ) ) $this->setPsLeadId2( $arrValues['ps_lead_id2'] );
		if( isset( $arrValues['reason'] ) && $boolDirectSet ) $this->set( 'm_strReason', trim( stripcslashes( $arrValues['reason'] ) ) ); elseif( isset( $arrValues['reason'] ) ) $this->setReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reason'] ) : $arrValues['reason'] );
		if( isset( $arrValues['is_duplicate'] ) && $boolDirectSet ) $this->set( 'm_intIsDuplicate', trim( $arrValues['is_duplicate'] ) ); elseif( isset( $arrValues['is_duplicate'] ) ) $this->setIsDuplicate( $arrValues['is_duplicate'] );
		if( isset( $arrValues['ignored_by'] ) && $boolDirectSet ) $this->set( 'm_intIgnoredBy', trim( $arrValues['ignored_by'] ) ); elseif( isset( $arrValues['ignored_by'] ) ) $this->setIgnoredBy( $arrValues['ignored_by'] );
		if( isset( $arrValues['ignored_on'] ) && $boolDirectSet ) $this->set( 'm_strIgnoredOn', trim( $arrValues['ignored_on'] ) ); elseif( isset( $arrValues['ignored_on'] ) ) $this->setIgnoredOn( $arrValues['ignored_on'] );
		if( isset( $arrValues['processed_by'] ) && $boolDirectSet ) $this->set( 'm_intProcessedBy', trim( $arrValues['processed_by'] ) ); elseif( isset( $arrValues['processed_by'] ) ) $this->setProcessedBy( $arrValues['processed_by'] );
		if( isset( $arrValues['processed_on'] ) && $boolDirectSet ) $this->set( 'm_strProcessedOn', trim( $arrValues['processed_on'] ) ); elseif( isset( $arrValues['processed_on'] ) ) $this->setProcessedOn( $arrValues['processed_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsLeadDuplicateTypeId( $intPsLeadDuplicateTypeId ) {
		$this->set( 'm_intPsLeadDuplicateTypeId', CStrings::strToIntDef( $intPsLeadDuplicateTypeId, NULL, false ) );
	}

	public function getPsLeadDuplicateTypeId() {
		return $this->m_intPsLeadDuplicateTypeId;
	}

	public function sqlPsLeadDuplicateTypeId() {
		return ( true == isset( $this->m_intPsLeadDuplicateTypeId ) ) ? ( string ) $this->m_intPsLeadDuplicateTypeId : 'NULL';
	}

	public function setPsLeadId1( $intPsLeadId1 ) {
		$this->set( 'm_intPsLeadId1', CStrings::strToIntDef( $intPsLeadId1, NULL, false ) );
	}

	public function getPsLeadId1() {
		return $this->m_intPsLeadId1;
	}

	public function sqlPsLeadId1() {
		return ( true == isset( $this->m_intPsLeadId1 ) ) ? ( string ) $this->m_intPsLeadId1 : 'NULL';
	}

	public function setPsLeadId2( $intPsLeadId2 ) {
		$this->set( 'm_intPsLeadId2', CStrings::strToIntDef( $intPsLeadId2, NULL, false ) );
	}

	public function getPsLeadId2() {
		return $this->m_intPsLeadId2;
	}

	public function sqlPsLeadId2() {
		return ( true == isset( $this->m_intPsLeadId2 ) ) ? ( string ) $this->m_intPsLeadId2 : 'NULL';
	}

	public function setReason( $strReason ) {
		$this->set( 'm_strReason', CStrings::strTrimDef( $strReason, 100, NULL, true ) );
	}

	public function getReason() {
		return $this->m_strReason;
	}

	public function sqlReason() {
		return ( true == isset( $this->m_strReason ) ) ? '\'' . addslashes( $this->m_strReason ) . '\'' : 'NULL';
	}

	public function setIsDuplicate( $intIsDuplicate ) {
		$this->set( 'm_intIsDuplicate', CStrings::strToIntDef( $intIsDuplicate, NULL, false ) );
	}

	public function getIsDuplicate() {
		return $this->m_intIsDuplicate;
	}

	public function sqlIsDuplicate() {
		return ( true == isset( $this->m_intIsDuplicate ) ) ? ( string ) $this->m_intIsDuplicate : 'NULL';
	}

	public function setIgnoredBy( $intIgnoredBy ) {
		$this->set( 'm_intIgnoredBy', CStrings::strToIntDef( $intIgnoredBy, NULL, false ) );
	}

	public function getIgnoredBy() {
		return $this->m_intIgnoredBy;
	}

	public function sqlIgnoredBy() {
		return ( true == isset( $this->m_intIgnoredBy ) ) ? ( string ) $this->m_intIgnoredBy : 'NULL';
	}

	public function setIgnoredOn( $strIgnoredOn ) {
		$this->set( 'm_strIgnoredOn', CStrings::strTrimDef( $strIgnoredOn, -1, NULL, true ) );
	}

	public function getIgnoredOn() {
		return $this->m_strIgnoredOn;
	}

	public function sqlIgnoredOn() {
		return ( true == isset( $this->m_strIgnoredOn ) ) ? '\'' . $this->m_strIgnoredOn . '\'' : 'NULL';
	}

	public function setProcessedBy( $intProcessedBy ) {
		$this->set( 'm_intProcessedBy', CStrings::strToIntDef( $intProcessedBy, NULL, false ) );
	}

	public function getProcessedBy() {
		return $this->m_intProcessedBy;
	}

	public function sqlProcessedBy() {
		return ( true == isset( $this->m_intProcessedBy ) ) ? ( string ) $this->m_intProcessedBy : 'NULL';
	}

	public function setProcessedOn( $strProcessedOn ) {
		$this->set( 'm_strProcessedOn', CStrings::strTrimDef( $strProcessedOn, -1, NULL, true ) );
	}

	public function getProcessedOn() {
		return $this->m_strProcessedOn;
	}

	public function sqlProcessedOn() {
		return ( true == isset( $this->m_strProcessedOn ) ) ? '\'' . $this->m_strProcessedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_lead_duplicate_type_id, ps_lead_id1, ps_lead_id2, reason, is_duplicate, ignored_by, ignored_on, processed_by, processed_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsLeadDuplicateTypeId() . ', ' .
 						$this->sqlPsLeadId1() . ', ' .
 						$this->sqlPsLeadId2() . ', ' .
 						$this->sqlReason() . ', ' .
 						$this->sqlIsDuplicate() . ', ' .
 						$this->sqlIgnoredBy() . ', ' .
 						$this->sqlIgnoredOn() . ', ' .
 						$this->sqlProcessedBy() . ', ' .
 						$this->sqlProcessedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_duplicate_type_id = ' . $this->sqlPsLeadDuplicateTypeId() . ','; } elseif( true == array_key_exists( 'PsLeadDuplicateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_duplicate_type_id = ' . $this->sqlPsLeadDuplicateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id1 = ' . $this->sqlPsLeadId1() . ','; } elseif( true == array_key_exists( 'PsLeadId1', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id1 = ' . $this->sqlPsLeadId1() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id2 = ' . $this->sqlPsLeadId2() . ','; } elseif( true == array_key_exists( 'PsLeadId2', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id2 = ' . $this->sqlPsLeadId2() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason = ' . $this->sqlReason() . ','; } elseif( true == array_key_exists( 'Reason', $this->getChangedColumns() ) ) { $strSql .= ' reason = ' . $this->sqlReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_duplicate = ' . $this->sqlIsDuplicate() . ','; } elseif( true == array_key_exists( 'IsDuplicate', $this->getChangedColumns() ) ) { $strSql .= ' is_duplicate = ' . $this->sqlIsDuplicate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ignored_by = ' . $this->sqlIgnoredBy() . ','; } elseif( true == array_key_exists( 'IgnoredBy', $this->getChangedColumns() ) ) { $strSql .= ' ignored_by = ' . $this->sqlIgnoredBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ignored_on = ' . $this->sqlIgnoredOn() . ','; } elseif( true == array_key_exists( 'IgnoredOn', $this->getChangedColumns() ) ) { $strSql .= ' ignored_on = ' . $this->sqlIgnoredOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_by = ' . $this->sqlProcessedBy() . ','; } elseif( true == array_key_exists( 'ProcessedBy', $this->getChangedColumns() ) ) { $strSql .= ' processed_by = ' . $this->sqlProcessedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; } elseif( true == array_key_exists( 'ProcessedOn', $this->getChangedColumns() ) ) { $strSql .= ' processed_on = ' . $this->sqlProcessedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_lead_duplicate_type_id' => $this->getPsLeadDuplicateTypeId(),
			'ps_lead_id1' => $this->getPsLeadId1(),
			'ps_lead_id2' => $this->getPsLeadId2(),
			'reason' => $this->getReason(),
			'is_duplicate' => $this->getIsDuplicate(),
			'ignored_by' => $this->getIgnoredBy(),
			'ignored_on' => $this->getIgnoredOn(),
			'processed_by' => $this->getProcessedBy(),
			'processed_on' => $this->getProcessedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>