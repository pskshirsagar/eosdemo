<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CJobPostingRecruiters
 * Do not add any new functions to this class.
 */

class CBaseJobPostingRecruiters extends CEosPluralBase {

	/**
	 * @return CJobPostingRecruiter[]
	 */
	public static function fetchJobPostingRecruiters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CJobPostingRecruiter', $objDatabase );
	}

	/**
	 * @return CJobPostingRecruiter
	 */
	public static function fetchJobPostingRecruiter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobPostingRecruiter', $objDatabase );
	}

	public static function fetchJobPostingRecruiterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_posting_recruiters', $objDatabase );
	}

	public static function fetchJobPostingRecruiterById( $intId, $objDatabase ) {
		return self::fetchJobPostingRecruiter( sprintf( 'SELECT * FROM job_posting_recruiters WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchJobPostingRecruitersByJobPostingId( $intJobPostingId, $objDatabase ) {
		return self::fetchJobPostingRecruiters( sprintf( 'SELECT * FROM job_posting_recruiters WHERE job_posting_id = %d', ( int ) $intJobPostingId ), $objDatabase );
	}

	public static function fetchJobPostingRecruitersByRecruiterEmployeeId( $intRecruiterEmployeeId, $objDatabase ) {
		return self::fetchJobPostingRecruiters( sprintf( 'SELECT * FROM job_posting_recruiters WHERE recruiter_employee_id = %d', ( int ) $intRecruiterEmployeeId ), $objDatabase );
	}

}
?>