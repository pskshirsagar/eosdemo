<?php

class CBasePsLeadFilter extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_lead_filters';

	protected $m_intId;
	protected $m_intUserId;
	protected $m_intPsLeadId;
	protected $m_strContractStatusTypes;
	protected $m_strPsLeads;
	protected $m_strPsLeadOrigins;
	protected $m_strPsLeadSources;
	protected $m_strContracts;
	protected $m_strPersons;
	protected $m_strSalesAreas;
	protected $m_strActionTypes;
	protected $m_strActionResults;
	protected $m_strPsProducts;
	protected $m_strContractPsProducts;
	protected $m_strMissingContractPsProducts;
	protected $m_strPsLeadEvents;
	protected $m_strPsLeadTypes;
	protected $m_strPeepEmployees;
	protected $m_strPropertyTypes;
	protected $m_strCompanyStatusTypes;
	protected $m_strSalesEmployees;
	protected $m_strSalesEngineers;
	protected $m_strSupportEmployees;
	protected $m_strResponsibleEmployees;
	protected $m_strStateCodes;
	protected $m_strMassEmails;
	protected $m_strCompetitors;
	protected $m_strFilterName;
	protected $m_strCompanyName;
	protected $m_strTitle;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strEmailAddress;
	protected $m_strCity;
	protected $m_strPostalCode;
	protected $m_strNoteText;
	protected $m_strStartRequestDate;
	protected $m_strEndRequestDate;
	protected $m_strEstimatedCloseStartDate;
	protected $m_strEstimatedCloseEndDate;
	protected $m_strActualCloseStartDate;
	protected $m_strActualCloseEndDate;
	protected $m_strActionStartDate;
	protected $m_strActionEndDate;
	protected $m_intMinUnitCount;
	protected $m_intMaxUnitCount;
	protected $m_strPhoneNumber;
	protected $m_intDeleteStatus;
	protected $m_intShowWatchlistGroupsOnly;
	protected $m_intExcludeEntrataPersons;
	protected $m_intRequireCloseDate;
	protected $m_strSortBy;
	protected $m_strSortDirection;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intDeleteStatus = '0';
		$this->m_intShowWatchlistGroupsOnly = '0';
		$this->m_intRequireCloseDate = '0';
		$this->m_strSortBy = 'pl.request_datetime';
		$this->m_strSortDirection = 'DESC';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['contract_status_types'] ) && $boolDirectSet ) $this->set( 'm_strContractStatusTypes', trim( stripcslashes( $arrValues['contract_status_types'] ) ) ); elseif( isset( $arrValues['contract_status_types'] ) ) $this->setContractStatusTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contract_status_types'] ) : $arrValues['contract_status_types'] );
		if( isset( $arrValues['ps_leads'] ) && $boolDirectSet ) $this->set( 'm_strPsLeads', trim( stripcslashes( $arrValues['ps_leads'] ) ) ); elseif( isset( $arrValues['ps_leads'] ) ) $this->setPsLeads( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_leads'] ) : $arrValues['ps_leads'] );
		if( isset( $arrValues['ps_lead_origins'] ) && $boolDirectSet ) $this->set( 'm_strPsLeadOrigins', trim( stripcslashes( $arrValues['ps_lead_origins'] ) ) ); elseif( isset( $arrValues['ps_lead_origins'] ) ) $this->setPsLeadOrigins( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_lead_origins'] ) : $arrValues['ps_lead_origins'] );
		if( isset( $arrValues['ps_lead_sources'] ) && $boolDirectSet ) $this->set( 'm_strPsLeadSources', trim( stripcslashes( $arrValues['ps_lead_sources'] ) ) ); elseif( isset( $arrValues['ps_lead_sources'] ) ) $this->setPsLeadSources( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_lead_sources'] ) : $arrValues['ps_lead_sources'] );
		if( isset( $arrValues['contracts'] ) && $boolDirectSet ) $this->set( 'm_strContracts', trim( stripcslashes( $arrValues['contracts'] ) ) ); elseif( isset( $arrValues['contracts'] ) ) $this->setContracts( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contracts'] ) : $arrValues['contracts'] );
		if( isset( $arrValues['persons'] ) && $boolDirectSet ) $this->set( 'm_strPersons', trim( stripcslashes( $arrValues['persons'] ) ) ); elseif( isset( $arrValues['persons'] ) ) $this->setPersons( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['persons'] ) : $arrValues['persons'] );
		if( isset( $arrValues['sales_areas'] ) && $boolDirectSet ) $this->set( 'm_strSalesAreas', trim( stripcslashes( $arrValues['sales_areas'] ) ) ); elseif( isset( $arrValues['sales_areas'] ) ) $this->setSalesAreas( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sales_areas'] ) : $arrValues['sales_areas'] );
		if( isset( $arrValues['action_types'] ) && $boolDirectSet ) $this->set( 'm_strActionTypes', trim( stripcslashes( $arrValues['action_types'] ) ) ); elseif( isset( $arrValues['action_types'] ) ) $this->setActionTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_types'] ) : $arrValues['action_types'] );
		if( isset( $arrValues['action_results'] ) && $boolDirectSet ) $this->set( 'm_strActionResults', trim( stripcslashes( $arrValues['action_results'] ) ) ); elseif( isset( $arrValues['action_results'] ) ) $this->setActionResults( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_results'] ) : $arrValues['action_results'] );
		if( isset( $arrValues['ps_products'] ) && $boolDirectSet ) $this->set( 'm_strPsProducts', trim( stripcslashes( $arrValues['ps_products'] ) ) ); elseif( isset( $arrValues['ps_products'] ) ) $this->setPsProducts( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_products'] ) : $arrValues['ps_products'] );
		if( isset( $arrValues['contract_ps_products'] ) && $boolDirectSet ) $this->set( 'm_strContractPsProducts', trim( stripcslashes( $arrValues['contract_ps_products'] ) ) ); elseif( isset( $arrValues['contract_ps_products'] ) ) $this->setContractPsProducts( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['contract_ps_products'] ) : $arrValues['contract_ps_products'] );
		if( isset( $arrValues['missing_contract_ps_products'] ) && $boolDirectSet ) $this->set( 'm_strMissingContractPsProducts', trim( stripcslashes( $arrValues['missing_contract_ps_products'] ) ) ); elseif( isset( $arrValues['missing_contract_ps_products'] ) ) $this->setMissingContractPsProducts( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['missing_contract_ps_products'] ) : $arrValues['missing_contract_ps_products'] );
		if( isset( $arrValues['ps_lead_events'] ) && $boolDirectSet ) $this->set( 'm_strPsLeadEvents', trim( stripcslashes( $arrValues['ps_lead_events'] ) ) ); elseif( isset( $arrValues['ps_lead_events'] ) ) $this->setPsLeadEvents( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_lead_events'] ) : $arrValues['ps_lead_events'] );
		if( isset( $arrValues['ps_lead_types'] ) && $boolDirectSet ) $this->set( 'm_strPsLeadTypes', trim( stripcslashes( $arrValues['ps_lead_types'] ) ) ); elseif( isset( $arrValues['ps_lead_types'] ) ) $this->setPsLeadTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_lead_types'] ) : $arrValues['ps_lead_types'] );
		if( isset( $arrValues['peep_employees'] ) && $boolDirectSet ) $this->set( 'm_strPeepEmployees', trim( stripcslashes( $arrValues['peep_employees'] ) ) ); elseif( isset( $arrValues['peep_employees'] ) ) $this->setPeepEmployees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['peep_employees'] ) : $arrValues['peep_employees'] );
		if( isset( $arrValues['property_types'] ) && $boolDirectSet ) $this->set( 'm_strPropertyTypes', trim( stripcslashes( $arrValues['property_types'] ) ) ); elseif( isset( $arrValues['property_types'] ) ) $this->setPropertyTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['property_types'] ) : $arrValues['property_types'] );
		if( isset( $arrValues['company_status_types'] ) && $boolDirectSet ) $this->set( 'm_strCompanyStatusTypes', trim( stripcslashes( $arrValues['company_status_types'] ) ) ); elseif( isset( $arrValues['company_status_types'] ) ) $this->setCompanyStatusTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_status_types'] ) : $arrValues['company_status_types'] );
		if( isset( $arrValues['sales_employees'] ) && $boolDirectSet ) $this->set( 'm_strSalesEmployees', trim( stripcslashes( $arrValues['sales_employees'] ) ) ); elseif( isset( $arrValues['sales_employees'] ) ) $this->setSalesEmployees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sales_employees'] ) : $arrValues['sales_employees'] );
		if( isset( $arrValues['sales_engineers'] ) && $boolDirectSet ) $this->set( 'm_strSalesEngineers', trim( stripcslashes( $arrValues['sales_engineers'] ) ) ); elseif( isset( $arrValues['sales_engineers'] ) ) $this->setSalesEngineers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sales_engineers'] ) : $arrValues['sales_engineers'] );
		if( isset( $arrValues['support_employees'] ) && $boolDirectSet ) $this->set( 'm_strSupportEmployees', trim( stripcslashes( $arrValues['support_employees'] ) ) ); elseif( isset( $arrValues['support_employees'] ) ) $this->setSupportEmployees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['support_employees'] ) : $arrValues['support_employees'] );
		if( isset( $arrValues['responsible_employees'] ) && $boolDirectSet ) $this->set( 'm_strResponsibleEmployees', trim( stripcslashes( $arrValues['responsible_employees'] ) ) ); elseif( isset( $arrValues['responsible_employees'] ) ) $this->setResponsibleEmployees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['responsible_employees'] ) : $arrValues['responsible_employees'] );
		if( isset( $arrValues['state_codes'] ) && $boolDirectSet ) $this->set( 'm_strStateCodes', trim( stripcslashes( $arrValues['state_codes'] ) ) ); elseif( isset( $arrValues['state_codes'] ) ) $this->setStateCodes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_codes'] ) : $arrValues['state_codes'] );
		if( isset( $arrValues['mass_emails'] ) && $boolDirectSet ) $this->set( 'm_strMassEmails', trim( stripcslashes( $arrValues['mass_emails'] ) ) ); elseif( isset( $arrValues['mass_emails'] ) ) $this->setMassEmails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['mass_emails'] ) : $arrValues['mass_emails'] );
		if( isset( $arrValues['competitors'] ) && $boolDirectSet ) $this->set( 'm_strCompetitors', trim( stripcslashes( $arrValues['competitors'] ) ) ); elseif( isset( $arrValues['competitors'] ) ) $this->setCompetitors( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['competitors'] ) : $arrValues['competitors'] );
		if( isset( $arrValues['filter_name'] ) && $boolDirectSet ) $this->set( 'm_strFilterName', trim( stripcslashes( $arrValues['filter_name'] ) ) ); elseif( isset( $arrValues['filter_name'] ) ) $this->setFilterName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['filter_name'] ) : $arrValues['filter_name'] );
		if( isset( $arrValues['company_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyName', trim( stripcslashes( $arrValues['company_name'] ) ) ); elseif( isset( $arrValues['company_name'] ) ) $this->setCompanyName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['company_name'] ) : $arrValues['company_name'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( stripcslashes( $arrValues['name_first'] ) ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_first'] ) : $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( stripcslashes( $arrValues['name_last'] ) ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name_last'] ) : $arrValues['name_last'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( stripcslashes( $arrValues['email_address'] ) ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_address'] ) : $arrValues['email_address'] );
		if( isset( $arrValues['city'] ) && $boolDirectSet ) $this->set( 'm_strCity', trim( stripcslashes( $arrValues['city'] ) ) ); elseif( isset( $arrValues['city'] ) ) $this->setCity( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['city'] ) : $arrValues['city'] );
		if( isset( $arrValues['postal_code'] ) && $boolDirectSet ) $this->set( 'm_strPostalCode', trim( stripcslashes( $arrValues['postal_code'] ) ) ); elseif( isset( $arrValues['postal_code'] ) ) $this->setPostalCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['postal_code'] ) : $arrValues['postal_code'] );
		if( isset( $arrValues['note_text'] ) && $boolDirectSet ) $this->set( 'm_strNoteText', trim( stripcslashes( $arrValues['note_text'] ) ) ); elseif( isset( $arrValues['note_text'] ) ) $this->setNoteText( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note_text'] ) : $arrValues['note_text'] );
		if( isset( $arrValues['start_request_date'] ) && $boolDirectSet ) $this->set( 'm_strStartRequestDate', trim( $arrValues['start_request_date'] ) ); elseif( isset( $arrValues['start_request_date'] ) ) $this->setStartRequestDate( $arrValues['start_request_date'] );
		if( isset( $arrValues['end_request_date'] ) && $boolDirectSet ) $this->set( 'm_strEndRequestDate', trim( $arrValues['end_request_date'] ) ); elseif( isset( $arrValues['end_request_date'] ) ) $this->setEndRequestDate( $arrValues['end_request_date'] );
		if( isset( $arrValues['estimated_close_start_date'] ) && $boolDirectSet ) $this->set( 'm_strEstimatedCloseStartDate', trim( $arrValues['estimated_close_start_date'] ) ); elseif( isset( $arrValues['estimated_close_start_date'] ) ) $this->setEstimatedCloseStartDate( $arrValues['estimated_close_start_date'] );
		if( isset( $arrValues['estimated_close_end_date'] ) && $boolDirectSet ) $this->set( 'm_strEstimatedCloseEndDate', trim( $arrValues['estimated_close_end_date'] ) ); elseif( isset( $arrValues['estimated_close_end_date'] ) ) $this->setEstimatedCloseEndDate( $arrValues['estimated_close_end_date'] );
		if( isset( $arrValues['actual_close_start_date'] ) && $boolDirectSet ) $this->set( 'm_strActualCloseStartDate', trim( $arrValues['actual_close_start_date'] ) ); elseif( isset( $arrValues['actual_close_start_date'] ) ) $this->setActualCloseStartDate( $arrValues['actual_close_start_date'] );
		if( isset( $arrValues['actual_close_end_date'] ) && $boolDirectSet ) $this->set( 'm_strActualCloseEndDate', trim( $arrValues['actual_close_end_date'] ) ); elseif( isset( $arrValues['actual_close_end_date'] ) ) $this->setActualCloseEndDate( $arrValues['actual_close_end_date'] );
		if( isset( $arrValues['action_start_date'] ) && $boolDirectSet ) $this->set( 'm_strActionStartDate', trim( $arrValues['action_start_date'] ) ); elseif( isset( $arrValues['action_start_date'] ) ) $this->setActionStartDate( $arrValues['action_start_date'] );
		if( isset( $arrValues['action_end_date'] ) && $boolDirectSet ) $this->set( 'm_strActionEndDate', trim( $arrValues['action_end_date'] ) ); elseif( isset( $arrValues['action_end_date'] ) ) $this->setActionEndDate( $arrValues['action_end_date'] );
		if( isset( $arrValues['min_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intMinUnitCount', trim( $arrValues['min_unit_count'] ) ); elseif( isset( $arrValues['min_unit_count'] ) ) $this->setMinUnitCount( $arrValues['min_unit_count'] );
		if( isset( $arrValues['max_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intMaxUnitCount', trim( $arrValues['max_unit_count'] ) ); elseif( isset( $arrValues['max_unit_count'] ) ) $this->setMaxUnitCount( $arrValues['max_unit_count'] );
		if( isset( $arrValues['phone_number'] ) && $boolDirectSet ) $this->set( 'm_strPhoneNumber', trim( stripcslashes( $arrValues['phone_number'] ) ) ); elseif( isset( $arrValues['phone_number'] ) ) $this->setPhoneNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['phone_number'] ) : $arrValues['phone_number'] );
		if( isset( $arrValues['delete_status'] ) && $boolDirectSet ) $this->set( 'm_intDeleteStatus', trim( $arrValues['delete_status'] ) ); elseif( isset( $arrValues['delete_status'] ) ) $this->setDeleteStatus( $arrValues['delete_status'] );
		if( isset( $arrValues['show_watchlist_groups_only'] ) && $boolDirectSet ) $this->set( 'm_intShowWatchlistGroupsOnly', trim( $arrValues['show_watchlist_groups_only'] ) ); elseif( isset( $arrValues['show_watchlist_groups_only'] ) ) $this->setShowWatchlistGroupsOnly( $arrValues['show_watchlist_groups_only'] );
		if( isset( $arrValues['exclude_entrata_persons'] ) && $boolDirectSet ) $this->set( 'm_intExcludeEntrataPersons', trim( $arrValues['exclude_entrata_persons'] ) ); elseif( isset( $arrValues['exclude_entrata_persons'] ) ) $this->setExcludeEntrataPersons( $arrValues['exclude_entrata_persons'] );
		if( isset( $arrValues['require_close_date'] ) && $boolDirectSet ) $this->set( 'm_intRequireCloseDate', trim( $arrValues['require_close_date'] ) ); elseif( isset( $arrValues['require_close_date'] ) ) $this->setRequireCloseDate( $arrValues['require_close_date'] );
		if( isset( $arrValues['sort_by'] ) && $boolDirectSet ) $this->set( 'm_strSortBy', trim( stripcslashes( $arrValues['sort_by'] ) ) ); elseif( isset( $arrValues['sort_by'] ) ) $this->setSortBy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sort_by'] ) : $arrValues['sort_by'] );
		if( isset( $arrValues['sort_direction'] ) && $boolDirectSet ) $this->set( 'm_strSortDirection', trim( stripcslashes( $arrValues['sort_direction'] ) ) ); elseif( isset( $arrValues['sort_direction'] ) ) $this->setSortDirection( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sort_direction'] ) : $arrValues['sort_direction'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setContractStatusTypes( $strContractStatusTypes ) {
		$this->set( 'm_strContractStatusTypes', CStrings::strTrimDef( $strContractStatusTypes, -1, NULL, true ) );
	}

	public function getContractStatusTypes() {
		return $this->m_strContractStatusTypes;
	}

	public function sqlContractStatusTypes() {
		return ( true == isset( $this->m_strContractStatusTypes ) ) ? '\'' . addslashes( $this->m_strContractStatusTypes ) . '\'' : 'NULL';
	}

	public function setPsLeads( $strPsLeads ) {
		$this->set( 'm_strPsLeads', CStrings::strTrimDef( $strPsLeads, -1, NULL, true ) );
	}

	public function getPsLeads() {
		return $this->m_strPsLeads;
	}

	public function sqlPsLeads() {
		return ( true == isset( $this->m_strPsLeads ) ) ? '\'' . addslashes( $this->m_strPsLeads ) . '\'' : 'NULL';
	}

	public function setPsLeadOrigins( $strPsLeadOrigins ) {
		$this->set( 'm_strPsLeadOrigins', CStrings::strTrimDef( $strPsLeadOrigins, -1, NULL, true ) );
	}

	public function getPsLeadOrigins() {
		return $this->m_strPsLeadOrigins;
	}

	public function sqlPsLeadOrigins() {
		return ( true == isset( $this->m_strPsLeadOrigins ) ) ? '\'' . addslashes( $this->m_strPsLeadOrigins ) . '\'' : 'NULL';
	}

	public function setPsLeadSources( $strPsLeadSources ) {
		$this->set( 'm_strPsLeadSources', CStrings::strTrimDef( $strPsLeadSources, -1, NULL, true ) );
	}

	public function getPsLeadSources() {
		return $this->m_strPsLeadSources;
	}

	public function sqlPsLeadSources() {
		return ( true == isset( $this->m_strPsLeadSources ) ) ? '\'' . addslashes( $this->m_strPsLeadSources ) . '\'' : 'NULL';
	}

	public function setContracts( $strContracts ) {
		$this->set( 'm_strContracts', CStrings::strTrimDef( $strContracts, -1, NULL, true ) );
	}

	public function getContracts() {
		return $this->m_strContracts;
	}

	public function sqlContracts() {
		return ( true == isset( $this->m_strContracts ) ) ? '\'' . addslashes( $this->m_strContracts ) . '\'' : 'NULL';
	}

	public function setPersons( $strPersons ) {
		$this->set( 'm_strPersons', CStrings::strTrimDef( $strPersons, -1, NULL, true ) );
	}

	public function getPersons() {
		return $this->m_strPersons;
	}

	public function sqlPersons() {
		return ( true == isset( $this->m_strPersons ) ) ? '\'' . addslashes( $this->m_strPersons ) . '\'' : 'NULL';
	}

	public function setSalesAreas( $strSalesAreas ) {
		$this->set( 'm_strSalesAreas', CStrings::strTrimDef( $strSalesAreas, -1, NULL, true ) );
	}

	public function getSalesAreas() {
		return $this->m_strSalesAreas;
	}

	public function sqlSalesAreas() {
		return ( true == isset( $this->m_strSalesAreas ) ) ? '\'' . addslashes( $this->m_strSalesAreas ) . '\'' : 'NULL';
	}

	public function setActionTypes( $strActionTypes ) {
		$this->set( 'm_strActionTypes', CStrings::strTrimDef( $strActionTypes, -1, NULL, true ) );
	}

	public function getActionTypes() {
		return $this->m_strActionTypes;
	}

	public function sqlActionTypes() {
		return ( true == isset( $this->m_strActionTypes ) ) ? '\'' . addslashes( $this->m_strActionTypes ) . '\'' : 'NULL';
	}

	public function setActionResults( $strActionResults ) {
		$this->set( 'm_strActionResults', CStrings::strTrimDef( $strActionResults, -1, NULL, true ) );
	}

	public function getActionResults() {
		return $this->m_strActionResults;
	}

	public function sqlActionResults() {
		return ( true == isset( $this->m_strActionResults ) ) ? '\'' . addslashes( $this->m_strActionResults ) . '\'' : 'NULL';
	}

	public function setPsProducts( $strPsProducts ) {
		$this->set( 'm_strPsProducts', CStrings::strTrimDef( $strPsProducts, -1, NULL, true ) );
	}

	public function getPsProducts() {
		return $this->m_strPsProducts;
	}

	public function sqlPsProducts() {
		return ( true == isset( $this->m_strPsProducts ) ) ? '\'' . addslashes( $this->m_strPsProducts ) . '\'' : 'NULL';
	}

	public function setContractPsProducts( $strContractPsProducts ) {
		$this->set( 'm_strContractPsProducts', CStrings::strTrimDef( $strContractPsProducts, -1, NULL, true ) );
	}

	public function getContractPsProducts() {
		return $this->m_strContractPsProducts;
	}

	public function sqlContractPsProducts() {
		return ( true == isset( $this->m_strContractPsProducts ) ) ? '\'' . addslashes( $this->m_strContractPsProducts ) . '\'' : 'NULL';
	}

	public function setMissingContractPsProducts( $strMissingContractPsProducts ) {
		$this->set( 'm_strMissingContractPsProducts', CStrings::strTrimDef( $strMissingContractPsProducts, -1, NULL, true ) );
	}

	public function getMissingContractPsProducts() {
		return $this->m_strMissingContractPsProducts;
	}

	public function sqlMissingContractPsProducts() {
		return ( true == isset( $this->m_strMissingContractPsProducts ) ) ? '\'' . addslashes( $this->m_strMissingContractPsProducts ) . '\'' : 'NULL';
	}

	public function setPsLeadEvents( $strPsLeadEvents ) {
		$this->set( 'm_strPsLeadEvents', CStrings::strTrimDef( $strPsLeadEvents, -1, NULL, true ) );
	}

	public function getPsLeadEvents() {
		return $this->m_strPsLeadEvents;
	}

	public function sqlPsLeadEvents() {
		return ( true == isset( $this->m_strPsLeadEvents ) ) ? '\'' . addslashes( $this->m_strPsLeadEvents ) . '\'' : 'NULL';
	}

	public function setPsLeadTypes( $strPsLeadTypes ) {
		$this->set( 'm_strPsLeadTypes', CStrings::strTrimDef( $strPsLeadTypes, -1, NULL, true ) );
	}

	public function getPsLeadTypes() {
		return $this->m_strPsLeadTypes;
	}

	public function sqlPsLeadTypes() {
		return ( true == isset( $this->m_strPsLeadTypes ) ) ? '\'' . addslashes( $this->m_strPsLeadTypes ) . '\'' : 'NULL';
	}

	public function setPeepEmployees( $strPeepEmployees ) {
		$this->set( 'm_strPeepEmployees', CStrings::strTrimDef( $strPeepEmployees, -1, NULL, true ) );
	}

	public function getPeepEmployees() {
		return $this->m_strPeepEmployees;
	}

	public function sqlPeepEmployees() {
		return ( true == isset( $this->m_strPeepEmployees ) ) ? '\'' . addslashes( $this->m_strPeepEmployees ) . '\'' : 'NULL';
	}

	public function setPropertyTypes( $strPropertyTypes ) {
		$this->set( 'm_strPropertyTypes', CStrings::strTrimDef( $strPropertyTypes, -1, NULL, true ) );
	}

	public function getPropertyTypes() {
		return $this->m_strPropertyTypes;
	}

	public function sqlPropertyTypes() {
		return ( true == isset( $this->m_strPropertyTypes ) ) ? '\'' . addslashes( $this->m_strPropertyTypes ) . '\'' : 'NULL';
	}

	public function setCompanyStatusTypes( $strCompanyStatusTypes ) {
		$this->set( 'm_strCompanyStatusTypes', CStrings::strTrimDef( $strCompanyStatusTypes, -1, NULL, true ) );
	}

	public function getCompanyStatusTypes() {
		return $this->m_strCompanyStatusTypes;
	}

	public function sqlCompanyStatusTypes() {
		return ( true == isset( $this->m_strCompanyStatusTypes ) ) ? '\'' . addslashes( $this->m_strCompanyStatusTypes ) . '\'' : 'NULL';
	}

	public function setSalesEmployees( $strSalesEmployees ) {
		$this->set( 'm_strSalesEmployees', CStrings::strTrimDef( $strSalesEmployees, -1, NULL, true ) );
	}

	public function getSalesEmployees() {
		return $this->m_strSalesEmployees;
	}

	public function sqlSalesEmployees() {
		return ( true == isset( $this->m_strSalesEmployees ) ) ? '\'' . addslashes( $this->m_strSalesEmployees ) . '\'' : 'NULL';
	}

	public function setSalesEngineers( $strSalesEngineers ) {
		$this->set( 'm_strSalesEngineers', CStrings::strTrimDef( $strSalesEngineers, -1, NULL, true ) );
	}

	public function getSalesEngineers() {
		return $this->m_strSalesEngineers;
	}

	public function sqlSalesEngineers() {
		return ( true == isset( $this->m_strSalesEngineers ) ) ? '\'' . addslashes( $this->m_strSalesEngineers ) . '\'' : 'NULL';
	}

	public function setSupportEmployees( $strSupportEmployees ) {
		$this->set( 'm_strSupportEmployees', CStrings::strTrimDef( $strSupportEmployees, -1, NULL, true ) );
	}

	public function getSupportEmployees() {
		return $this->m_strSupportEmployees;
	}

	public function sqlSupportEmployees() {
		return ( true == isset( $this->m_strSupportEmployees ) ) ? '\'' . addslashes( $this->m_strSupportEmployees ) . '\'' : 'NULL';
	}

	public function setResponsibleEmployees( $strResponsibleEmployees ) {
		$this->set( 'm_strResponsibleEmployees', CStrings::strTrimDef( $strResponsibleEmployees, -1, NULL, true ) );
	}

	public function getResponsibleEmployees() {
		return $this->m_strResponsibleEmployees;
	}

	public function sqlResponsibleEmployees() {
		return ( true == isset( $this->m_strResponsibleEmployees ) ) ? '\'' . addslashes( $this->m_strResponsibleEmployees ) . '\'' : 'NULL';
	}

	public function setStateCodes( $strStateCodes ) {
		$this->set( 'm_strStateCodes', CStrings::strTrimDef( $strStateCodes, -1, NULL, true ) );
	}

	public function getStateCodes() {
		return $this->m_strStateCodes;
	}

	public function sqlStateCodes() {
		return ( true == isset( $this->m_strStateCodes ) ) ? '\'' . addslashes( $this->m_strStateCodes ) . '\'' : 'NULL';
	}

	public function setMassEmails( $strMassEmails ) {
		$this->set( 'm_strMassEmails', CStrings::strTrimDef( $strMassEmails, -1, NULL, true ) );
	}

	public function getMassEmails() {
		return $this->m_strMassEmails;
	}

	public function sqlMassEmails() {
		return ( true == isset( $this->m_strMassEmails ) ) ? '\'' . addslashes( $this->m_strMassEmails ) . '\'' : 'NULL';
	}

	public function setCompetitors( $strCompetitors ) {
		$this->set( 'm_strCompetitors', CStrings::strTrimDef( $strCompetitors, -1, NULL, true ) );
	}

	public function getCompetitors() {
		return $this->m_strCompetitors;
	}

	public function sqlCompetitors() {
		return ( true == isset( $this->m_strCompetitors ) ) ? '\'' . addslashes( $this->m_strCompetitors ) . '\'' : 'NULL';
	}

	public function setFilterName( $strFilterName ) {
		$this->set( 'm_strFilterName', CStrings::strTrimDef( $strFilterName, 50, NULL, true ) );
	}

	public function getFilterName() {
		return $this->m_strFilterName;
	}

	public function sqlFilterName() {
		return ( true == isset( $this->m_strFilterName ) ) ? '\'' . addslashes( $this->m_strFilterName ) . '\'' : 'NULL';
	}

	public function setCompanyName( $strCompanyName ) {
		$this->set( 'm_strCompanyName', CStrings::strTrimDef( $strCompanyName, 50, NULL, true ) );
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function sqlCompanyName() {
		return ( true == isset( $this->m_strCompanyName ) ) ? '\'' . addslashes( $this->m_strCompanyName ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, 50, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? '\'' . addslashes( $this->m_strNameFirst ) . '\'' : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, 50, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? '\'' . addslashes( $this->m_strNameLast ) . '\'' : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, 240, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? '\'' . addslashes( $this->m_strEmailAddress ) . '\'' : 'NULL';
	}

	public function setCity( $strCity ) {
		$this->set( 'm_strCity', CStrings::strTrimDef( $strCity, 50, NULL, true ) );
	}

	public function getCity() {
		return $this->m_strCity;
	}

	public function sqlCity() {
		return ( true == isset( $this->m_strCity ) ) ? '\'' . addslashes( $this->m_strCity ) . '\'' : 'NULL';
	}

	public function setPostalCode( $strPostalCode ) {
		$this->set( 'm_strPostalCode', CStrings::strTrimDef( $strPostalCode, 20, NULL, true ) );
	}

	public function getPostalCode() {
		return $this->m_strPostalCode;
	}

	public function sqlPostalCode() {
		return ( true == isset( $this->m_strPostalCode ) ) ? '\'' . addslashes( $this->m_strPostalCode ) . '\'' : 'NULL';
	}

	public function setNoteText( $strNoteText ) {
		$this->set( 'm_strNoteText', CStrings::strTrimDef( $strNoteText, -1, NULL, true ) );
	}

	public function getNoteText() {
		return $this->m_strNoteText;
	}

	public function sqlNoteText() {
		return ( true == isset( $this->m_strNoteText ) ) ? '\'' . addslashes( $this->m_strNoteText ) . '\'' : 'NULL';
	}

	public function setStartRequestDate( $strStartRequestDate ) {
		$this->set( 'm_strStartRequestDate', CStrings::strTrimDef( $strStartRequestDate, -1, NULL, true ) );
	}

	public function getStartRequestDate() {
		return $this->m_strStartRequestDate;
	}

	public function sqlStartRequestDate() {
		return ( true == isset( $this->m_strStartRequestDate ) ) ? '\'' . $this->m_strStartRequestDate . '\'' : 'NULL';
	}

	public function setEndRequestDate( $strEndRequestDate ) {
		$this->set( 'm_strEndRequestDate', CStrings::strTrimDef( $strEndRequestDate, -1, NULL, true ) );
	}

	public function getEndRequestDate() {
		return $this->m_strEndRequestDate;
	}

	public function sqlEndRequestDate() {
		return ( true == isset( $this->m_strEndRequestDate ) ) ? '\'' . $this->m_strEndRequestDate . '\'' : 'NULL';
	}

	public function setEstimatedCloseStartDate( $strEstimatedCloseStartDate ) {
		$this->set( 'm_strEstimatedCloseStartDate', CStrings::strTrimDef( $strEstimatedCloseStartDate, -1, NULL, true ) );
	}

	public function getEstimatedCloseStartDate() {
		return $this->m_strEstimatedCloseStartDate;
	}

	public function sqlEstimatedCloseStartDate() {
		return ( true == isset( $this->m_strEstimatedCloseStartDate ) ) ? '\'' . $this->m_strEstimatedCloseStartDate . '\'' : 'NULL';
	}

	public function setEstimatedCloseEndDate( $strEstimatedCloseEndDate ) {
		$this->set( 'm_strEstimatedCloseEndDate', CStrings::strTrimDef( $strEstimatedCloseEndDate, -1, NULL, true ) );
	}

	public function getEstimatedCloseEndDate() {
		return $this->m_strEstimatedCloseEndDate;
	}

	public function sqlEstimatedCloseEndDate() {
		return ( true == isset( $this->m_strEstimatedCloseEndDate ) ) ? '\'' . $this->m_strEstimatedCloseEndDate . '\'' : 'NULL';
	}

	public function setActualCloseStartDate( $strActualCloseStartDate ) {
		$this->set( 'm_strActualCloseStartDate', CStrings::strTrimDef( $strActualCloseStartDate, -1, NULL, true ) );
	}

	public function getActualCloseStartDate() {
		return $this->m_strActualCloseStartDate;
	}

	public function sqlActualCloseStartDate() {
		return ( true == isset( $this->m_strActualCloseStartDate ) ) ? '\'' . $this->m_strActualCloseStartDate . '\'' : 'NULL';
	}

	public function setActualCloseEndDate( $strActualCloseEndDate ) {
		$this->set( 'm_strActualCloseEndDate', CStrings::strTrimDef( $strActualCloseEndDate, -1, NULL, true ) );
	}

	public function getActualCloseEndDate() {
		return $this->m_strActualCloseEndDate;
	}

	public function sqlActualCloseEndDate() {
		return ( true == isset( $this->m_strActualCloseEndDate ) ) ? '\'' . $this->m_strActualCloseEndDate . '\'' : 'NULL';
	}

	public function setActionStartDate( $strActionStartDate ) {
		$this->set( 'm_strActionStartDate', CStrings::strTrimDef( $strActionStartDate, -1, NULL, true ) );
	}

	public function getActionStartDate() {
		return $this->m_strActionStartDate;
	}

	public function sqlActionStartDate() {
		return ( true == isset( $this->m_strActionStartDate ) ) ? '\'' . $this->m_strActionStartDate . '\'' : 'NULL';
	}

	public function setActionEndDate( $strActionEndDate ) {
		$this->set( 'm_strActionEndDate', CStrings::strTrimDef( $strActionEndDate, -1, NULL, true ) );
	}

	public function getActionEndDate() {
		return $this->m_strActionEndDate;
	}

	public function sqlActionEndDate() {
		return ( true == isset( $this->m_strActionEndDate ) ) ? '\'' . $this->m_strActionEndDate . '\'' : 'NULL';
	}

	public function setMinUnitCount( $intMinUnitCount ) {
		$this->set( 'm_intMinUnitCount', CStrings::strToIntDef( $intMinUnitCount, NULL, false ) );
	}

	public function getMinUnitCount() {
		return $this->m_intMinUnitCount;
	}

	public function sqlMinUnitCount() {
		return ( true == isset( $this->m_intMinUnitCount ) ) ? ( string ) $this->m_intMinUnitCount : 'NULL';
	}

	public function setMaxUnitCount( $intMaxUnitCount ) {
		$this->set( 'm_intMaxUnitCount', CStrings::strToIntDef( $intMaxUnitCount, NULL, false ) );
	}

	public function getMaxUnitCount() {
		return $this->m_intMaxUnitCount;
	}

	public function sqlMaxUnitCount() {
		return ( true == isset( $this->m_intMaxUnitCount ) ) ? ( string ) $this->m_intMaxUnitCount : 'NULL';
	}

	public function setPhoneNumber( $strPhoneNumber ) {
		$this->set( 'm_strPhoneNumber', CStrings::strTrimDef( $strPhoneNumber, 30, NULL, true ) );
	}

	public function getPhoneNumber() {
		return $this->m_strPhoneNumber;
	}

	public function sqlPhoneNumber() {
		return ( true == isset( $this->m_strPhoneNumber ) ) ? '\'' . addslashes( $this->m_strPhoneNumber ) . '\'' : 'NULL';
	}

	public function setDeleteStatus( $intDeleteStatus ) {
		$this->set( 'm_intDeleteStatus', CStrings::strToIntDef( $intDeleteStatus, NULL, false ) );
	}

	public function getDeleteStatus() {
		return $this->m_intDeleteStatus;
	}

	public function sqlDeleteStatus() {
		return ( true == isset( $this->m_intDeleteStatus ) ) ? ( string ) $this->m_intDeleteStatus : '0';
	}

	public function setShowWatchlistGroupsOnly( $intShowWatchlistGroupsOnly ) {
		$this->set( 'm_intShowWatchlistGroupsOnly', CStrings::strToIntDef( $intShowWatchlistGroupsOnly, NULL, false ) );
	}

	public function getShowWatchlistGroupsOnly() {
		return $this->m_intShowWatchlistGroupsOnly;
	}

	public function sqlShowWatchlistGroupsOnly() {
		return ( true == isset( $this->m_intShowWatchlistGroupsOnly ) ) ? ( string ) $this->m_intShowWatchlistGroupsOnly : '0';
	}

	public function setExcludeEntrataPersons( $intExcludeEntrataPersons ) {
		$this->set( 'm_intExcludeEntrataPersons', CStrings::strToIntDef( $intExcludeEntrataPersons, NULL, false ) );
	}

	public function getExcludeEntrataPersons() {
		return $this->m_intExcludeEntrataPersons;
	}

	public function sqlExcludeEntrataPersons() {
		return ( true == isset( $this->m_intExcludeEntrataPersons ) ) ? ( string ) $this->m_intExcludeEntrataPersons : 'NULL';
	}

	public function setRequireCloseDate( $intRequireCloseDate ) {
		$this->set( 'm_intRequireCloseDate', CStrings::strToIntDef( $intRequireCloseDate, NULL, false ) );
	}

	public function getRequireCloseDate() {
		return $this->m_intRequireCloseDate;
	}

	public function sqlRequireCloseDate() {
		return ( true == isset( $this->m_intRequireCloseDate ) ) ? ( string ) $this->m_intRequireCloseDate : '0';
	}

	public function setSortBy( $strSortBy ) {
		$this->set( 'm_strSortBy', CStrings::strTrimDef( $strSortBy, 240, NULL, true ) );
	}

	public function getSortBy() {
		return $this->m_strSortBy;
	}

	public function sqlSortBy() {
		return ( true == isset( $this->m_strSortBy ) ) ? '\'' . addslashes( $this->m_strSortBy ) . '\'' : '\'pl.request_datetime\'';
	}

	public function setSortDirection( $strSortDirection ) {
		$this->set( 'm_strSortDirection', CStrings::strTrimDef( $strSortDirection, 240, NULL, true ) );
	}

	public function getSortDirection() {
		return $this->m_strSortDirection;
	}

	public function sqlSortDirection() {
		return ( true == isset( $this->m_strSortDirection ) ) ? '\'' . addslashes( $this->m_strSortDirection ) . '\'' : '\'DESC\'';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, user_id, ps_lead_id, contract_status_types, ps_leads, ps_lead_origins, ps_lead_sources, contracts, persons, sales_areas, action_types, action_results, ps_products, contract_ps_products, missing_contract_ps_products, ps_lead_events, ps_lead_types, peep_employees, property_types, company_status_types, sales_employees, sales_engineers, support_employees, responsible_employees, state_codes, mass_emails, competitors, filter_name, company_name, title, name_first, name_last, email_address, city, postal_code, note_text, start_request_date, end_request_date, estimated_close_start_date, estimated_close_end_date, actual_close_start_date, actual_close_end_date, action_start_date, action_end_date, min_unit_count, max_unit_count, phone_number, delete_status, show_watchlist_groups_only, exclude_entrata_persons, require_close_date, sort_by, sort_direction, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlUserId() . ', ' .
 						$this->sqlPsLeadId() . ', ' .
 						$this->sqlContractStatusTypes() . ', ' .
 						$this->sqlPsLeads() . ', ' .
 						$this->sqlPsLeadOrigins() . ', ' .
 						$this->sqlPsLeadSources() . ', ' .
 						$this->sqlContracts() . ', ' .
 						$this->sqlPersons() . ', ' .
 						$this->sqlSalesAreas() . ', ' .
 						$this->sqlActionTypes() . ', ' .
 						$this->sqlActionResults() . ', ' .
 						$this->sqlPsProducts() . ', ' .
 						$this->sqlContractPsProducts() . ', ' .
 						$this->sqlMissingContractPsProducts() . ', ' .
 						$this->sqlPsLeadEvents() . ', ' .
 						$this->sqlPsLeadTypes() . ', ' .
 						$this->sqlPeepEmployees() . ', ' .
 						$this->sqlPropertyTypes() . ', ' .
 						$this->sqlCompanyStatusTypes() . ', ' .
 						$this->sqlSalesEmployees() . ', ' .
 						$this->sqlSalesEngineers() . ', ' .
 						$this->sqlSupportEmployees() . ', ' .
 						$this->sqlResponsibleEmployees() . ', ' .
 						$this->sqlStateCodes() . ', ' .
 						$this->sqlMassEmails() . ', ' .
 						$this->sqlCompetitors() . ', ' .
 						$this->sqlFilterName() . ', ' .
 						$this->sqlCompanyName() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlNameFirst() . ', ' .
 						$this->sqlNameLast() . ', ' .
 						$this->sqlEmailAddress() . ', ' .
 						$this->sqlCity() . ', ' .
 						$this->sqlPostalCode() . ', ' .
 						$this->sqlNoteText() . ', ' .
 						$this->sqlStartRequestDate() . ', ' .
 						$this->sqlEndRequestDate() . ', ' .
 						$this->sqlEstimatedCloseStartDate() . ', ' .
 						$this->sqlEstimatedCloseEndDate() . ', ' .
 						$this->sqlActualCloseStartDate() . ', ' .
 						$this->sqlActualCloseEndDate() . ', ' .
 						$this->sqlActionStartDate() . ', ' .
 						$this->sqlActionEndDate() . ', ' .
 						$this->sqlMinUnitCount() . ', ' .
 						$this->sqlMaxUnitCount() . ', ' .
 						$this->sqlPhoneNumber() . ', ' .
 						$this->sqlDeleteStatus() . ', ' .
 						$this->sqlShowWatchlistGroupsOnly() . ', ' .
 						$this->sqlExcludeEntrataPersons() . ', ' .
 						$this->sqlRequireCloseDate() . ', ' .
 						$this->sqlSortBy() . ', ' .
 						$this->sqlSortDirection() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_status_types = ' . $this->sqlContractStatusTypes() . ','; } elseif( true == array_key_exists( 'ContractStatusTypes', $this->getChangedColumns() ) ) { $strSql .= ' contract_status_types = ' . $this->sqlContractStatusTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_leads = ' . $this->sqlPsLeads() . ','; } elseif( true == array_key_exists( 'PsLeads', $this->getChangedColumns() ) ) { $strSql .= ' ps_leads = ' . $this->sqlPsLeads() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_origins = ' . $this->sqlPsLeadOrigins() . ','; } elseif( true == array_key_exists( 'PsLeadOrigins', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_origins = ' . $this->sqlPsLeadOrigins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_sources = ' . $this->sqlPsLeadSources() . ','; } elseif( true == array_key_exists( 'PsLeadSources', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_sources = ' . $this->sqlPsLeadSources() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contracts = ' . $this->sqlContracts() . ','; } elseif( true == array_key_exists( 'Contracts', $this->getChangedColumns() ) ) { $strSql .= ' contracts = ' . $this->sqlContracts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' persons = ' . $this->sqlPersons() . ','; } elseif( true == array_key_exists( 'Persons', $this->getChangedColumns() ) ) { $strSql .= ' persons = ' . $this->sqlPersons() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_areas = ' . $this->sqlSalesAreas() . ','; } elseif( true == array_key_exists( 'SalesAreas', $this->getChangedColumns() ) ) { $strSql .= ' sales_areas = ' . $this->sqlSalesAreas() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_types = ' . $this->sqlActionTypes() . ','; } elseif( true == array_key_exists( 'ActionTypes', $this->getChangedColumns() ) ) { $strSql .= ' action_types = ' . $this->sqlActionTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_results = ' . $this->sqlActionResults() . ','; } elseif( true == array_key_exists( 'ActionResults', $this->getChangedColumns() ) ) { $strSql .= ' action_results = ' . $this->sqlActionResults() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_products = ' . $this->sqlPsProducts() . ','; } elseif( true == array_key_exists( 'PsProducts', $this->getChangedColumns() ) ) { $strSql .= ' ps_products = ' . $this->sqlPsProducts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_ps_products = ' . $this->sqlContractPsProducts() . ','; } elseif( true == array_key_exists( 'ContractPsProducts', $this->getChangedColumns() ) ) { $strSql .= ' contract_ps_products = ' . $this->sqlContractPsProducts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' missing_contract_ps_products = ' . $this->sqlMissingContractPsProducts() . ','; } elseif( true == array_key_exists( 'MissingContractPsProducts', $this->getChangedColumns() ) ) { $strSql .= ' missing_contract_ps_products = ' . $this->sqlMissingContractPsProducts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_events = ' . $this->sqlPsLeadEvents() . ','; } elseif( true == array_key_exists( 'PsLeadEvents', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_events = ' . $this->sqlPsLeadEvents() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_types = ' . $this->sqlPsLeadTypes() . ','; } elseif( true == array_key_exists( 'PsLeadTypes', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_types = ' . $this->sqlPsLeadTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' peep_employees = ' . $this->sqlPeepEmployees() . ','; } elseif( true == array_key_exists( 'PeepEmployees', $this->getChangedColumns() ) ) { $strSql .= ' peep_employees = ' . $this->sqlPeepEmployees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_types = ' . $this->sqlPropertyTypes() . ','; } elseif( true == array_key_exists( 'PropertyTypes', $this->getChangedColumns() ) ) { $strSql .= ' property_types = ' . $this->sqlPropertyTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_status_types = ' . $this->sqlCompanyStatusTypes() . ','; } elseif( true == array_key_exists( 'CompanyStatusTypes', $this->getChangedColumns() ) ) { $strSql .= ' company_status_types = ' . $this->sqlCompanyStatusTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_employees = ' . $this->sqlSalesEmployees() . ','; } elseif( true == array_key_exists( 'SalesEmployees', $this->getChangedColumns() ) ) { $strSql .= ' sales_employees = ' . $this->sqlSalesEmployees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sales_engineers = ' . $this->sqlSalesEngineers() . ','; } elseif( true == array_key_exists( 'SalesEngineers', $this->getChangedColumns() ) ) { $strSql .= ' sales_engineers = ' . $this->sqlSalesEngineers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' support_employees = ' . $this->sqlSupportEmployees() . ','; } elseif( true == array_key_exists( 'SupportEmployees', $this->getChangedColumns() ) ) { $strSql .= ' support_employees = ' . $this->sqlSupportEmployees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' responsible_employees = ' . $this->sqlResponsibleEmployees() . ','; } elseif( true == array_key_exists( 'ResponsibleEmployees', $this->getChangedColumns() ) ) { $strSql .= ' responsible_employees = ' . $this->sqlResponsibleEmployees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_codes = ' . $this->sqlStateCodes() . ','; } elseif( true == array_key_exists( 'StateCodes', $this->getChangedColumns() ) ) { $strSql .= ' state_codes = ' . $this->sqlStateCodes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mass_emails = ' . $this->sqlMassEmails() . ','; } elseif( true == array_key_exists( 'MassEmails', $this->getChangedColumns() ) ) { $strSql .= ' mass_emails = ' . $this->sqlMassEmails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitors = ' . $this->sqlCompetitors() . ','; } elseif( true == array_key_exists( 'Competitors', $this->getChangedColumns() ) ) { $strSql .= ' competitors = ' . $this->sqlCompetitors() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' filter_name = ' . $this->sqlFilterName() . ','; } elseif( true == array_key_exists( 'FilterName', $this->getChangedColumns() ) ) { $strSql .= ' filter_name = ' . $this->sqlFilterName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; } elseif( true == array_key_exists( 'CompanyName', $this->getChangedColumns() ) ) { $strSql .= ' company_name = ' . $this->sqlCompanyName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' city = ' . $this->sqlCity() . ','; } elseif( true == array_key_exists( 'City', $this->getChangedColumns() ) ) { $strSql .= ' city = ' . $this->sqlCity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; } elseif( true == array_key_exists( 'PostalCode', $this->getChangedColumns() ) ) { $strSql .= ' postal_code = ' . $this->sqlPostalCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note_text = ' . $this->sqlNoteText() . ','; } elseif( true == array_key_exists( 'NoteText', $this->getChangedColumns() ) ) { $strSql .= ' note_text = ' . $this->sqlNoteText() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_request_date = ' . $this->sqlStartRequestDate() . ','; } elseif( true == array_key_exists( 'StartRequestDate', $this->getChangedColumns() ) ) { $strSql .= ' start_request_date = ' . $this->sqlStartRequestDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_request_date = ' . $this->sqlEndRequestDate() . ','; } elseif( true == array_key_exists( 'EndRequestDate', $this->getChangedColumns() ) ) { $strSql .= ' end_request_date = ' . $this->sqlEndRequestDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimated_close_start_date = ' . $this->sqlEstimatedCloseStartDate() . ','; } elseif( true == array_key_exists( 'EstimatedCloseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' estimated_close_start_date = ' . $this->sqlEstimatedCloseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' estimated_close_end_date = ' . $this->sqlEstimatedCloseEndDate() . ','; } elseif( true == array_key_exists( 'EstimatedCloseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' estimated_close_end_date = ' . $this->sqlEstimatedCloseEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_close_start_date = ' . $this->sqlActualCloseStartDate() . ','; } elseif( true == array_key_exists( 'ActualCloseStartDate', $this->getChangedColumns() ) ) { $strSql .= ' actual_close_start_date = ' . $this->sqlActualCloseStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_close_end_date = ' . $this->sqlActualCloseEndDate() . ','; } elseif( true == array_key_exists( 'ActualCloseEndDate', $this->getChangedColumns() ) ) { $strSql .= ' actual_close_end_date = ' . $this->sqlActualCloseEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_start_date = ' . $this->sqlActionStartDate() . ','; } elseif( true == array_key_exists( 'ActionStartDate', $this->getChangedColumns() ) ) { $strSql .= ' action_start_date = ' . $this->sqlActionStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_end_date = ' . $this->sqlActionEndDate() . ','; } elseif( true == array_key_exists( 'ActionEndDate', $this->getChangedColumns() ) ) { $strSql .= ' action_end_date = ' . $this->sqlActionEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' min_unit_count = ' . $this->sqlMinUnitCount() . ','; } elseif( true == array_key_exists( 'MinUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' min_unit_count = ' . $this->sqlMinUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_unit_count = ' . $this->sqlMaxUnitCount() . ','; } elseif( true == array_key_exists( 'MaxUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' max_unit_count = ' . $this->sqlMaxUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; } elseif( true == array_key_exists( 'PhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' phone_number = ' . $this->sqlPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delete_status = ' . $this->sqlDeleteStatus() . ','; } elseif( true == array_key_exists( 'DeleteStatus', $this->getChangedColumns() ) ) { $strSql .= ' delete_status = ' . $this->sqlDeleteStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_watchlist_groups_only = ' . $this->sqlShowWatchlistGroupsOnly() . ','; } elseif( true == array_key_exists( 'ShowWatchlistGroupsOnly', $this->getChangedColumns() ) ) { $strSql .= ' show_watchlist_groups_only = ' . $this->sqlShowWatchlistGroupsOnly() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' exclude_entrata_persons = ' . $this->sqlExcludeEntrataPersons() . ','; } elseif( true == array_key_exists( 'ExcludeEntrataPersons', $this->getChangedColumns() ) ) { $strSql .= ' exclude_entrata_persons = ' . $this->sqlExcludeEntrataPersons() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_close_date = ' . $this->sqlRequireCloseDate() . ','; } elseif( true == array_key_exists( 'RequireCloseDate', $this->getChangedColumns() ) ) { $strSql .= ' require_close_date = ' . $this->sqlRequireCloseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sort_by = ' . $this->sqlSortBy() . ','; } elseif( true == array_key_exists( 'SortBy', $this->getChangedColumns() ) ) { $strSql .= ' sort_by = ' . $this->sqlSortBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sort_direction = ' . $this->sqlSortDirection() . ','; } elseif( true == array_key_exists( 'SortDirection', $this->getChangedColumns() ) ) { $strSql .= ' sort_direction = ' . $this->sqlSortDirection() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'user_id' => $this->getUserId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'contract_status_types' => $this->getContractStatusTypes(),
			'ps_leads' => $this->getPsLeads(),
			'ps_lead_origins' => $this->getPsLeadOrigins(),
			'ps_lead_sources' => $this->getPsLeadSources(),
			'contracts' => $this->getContracts(),
			'persons' => $this->getPersons(),
			'sales_areas' => $this->getSalesAreas(),
			'action_types' => $this->getActionTypes(),
			'action_results' => $this->getActionResults(),
			'ps_products' => $this->getPsProducts(),
			'contract_ps_products' => $this->getContractPsProducts(),
			'missing_contract_ps_products' => $this->getMissingContractPsProducts(),
			'ps_lead_events' => $this->getPsLeadEvents(),
			'ps_lead_types' => $this->getPsLeadTypes(),
			'peep_employees' => $this->getPeepEmployees(),
			'property_types' => $this->getPropertyTypes(),
			'company_status_types' => $this->getCompanyStatusTypes(),
			'sales_employees' => $this->getSalesEmployees(),
			'sales_engineers' => $this->getSalesEngineers(),
			'support_employees' => $this->getSupportEmployees(),
			'responsible_employees' => $this->getResponsibleEmployees(),
			'state_codes' => $this->getStateCodes(),
			'mass_emails' => $this->getMassEmails(),
			'competitors' => $this->getCompetitors(),
			'filter_name' => $this->getFilterName(),
			'company_name' => $this->getCompanyName(),
			'title' => $this->getTitle(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'email_address' => $this->getEmailAddress(),
			'city' => $this->getCity(),
			'postal_code' => $this->getPostalCode(),
			'note_text' => $this->getNoteText(),
			'start_request_date' => $this->getStartRequestDate(),
			'end_request_date' => $this->getEndRequestDate(),
			'estimated_close_start_date' => $this->getEstimatedCloseStartDate(),
			'estimated_close_end_date' => $this->getEstimatedCloseEndDate(),
			'actual_close_start_date' => $this->getActualCloseStartDate(),
			'actual_close_end_date' => $this->getActualCloseEndDate(),
			'action_start_date' => $this->getActionStartDate(),
			'action_end_date' => $this->getActionEndDate(),
			'min_unit_count' => $this->getMinUnitCount(),
			'max_unit_count' => $this->getMaxUnitCount(),
			'phone_number' => $this->getPhoneNumber(),
			'delete_status' => $this->getDeleteStatus(),
			'show_watchlist_groups_only' => $this->getShowWatchlistGroupsOnly(),
			'exclude_entrata_persons' => $this->getExcludeEntrataPersons(),
			'require_close_date' => $this->getRequireCloseDate(),
			'sort_by' => $this->getSortBy(),
			'sort_direction' => $this->getSortDirection(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>