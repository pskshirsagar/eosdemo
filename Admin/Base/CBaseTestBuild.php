<?php

class CBaseTestBuild extends CEosSingularBase {

	const TABLE_NAME = 'public.test_builds';

	protected $m_intId;
	protected $m_strName;
	protected $m_intTestCaseProductFolderId;
	protected $m_strReleaseDate;
	protected $m_boolIsStandardRelease;
	protected $m_boolIsActive;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intRegressionTaskId;
	protected $m_intQaLeadEmployeeId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsStandardRelease = false;
		$this->m_boolIsActive = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['test_case_product_folder_id'] ) && $boolDirectSet ) $this->set( 'm_intTestCaseProductFolderId', trim( $arrValues['test_case_product_folder_id'] ) ); elseif( isset( $arrValues['test_case_product_folder_id'] ) ) $this->setTestCaseProductFolderId( $arrValues['test_case_product_folder_id'] );
		if( isset( $arrValues['release_date'] ) && $boolDirectSet ) $this->set( 'm_strReleaseDate', trim( $arrValues['release_date'] ) ); elseif( isset( $arrValues['release_date'] ) ) $this->setReleaseDate( $arrValues['release_date'] );
		if( isset( $arrValues['is_standard_release'] ) && $boolDirectSet ) $this->set( 'm_boolIsStandardRelease', trim( stripcslashes( $arrValues['is_standard_release'] ) ) ); elseif( isset( $arrValues['is_standard_release'] ) ) $this->setIsStandardRelease( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_standard_release'] ) : $arrValues['is_standard_release'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['regression_task_id'] ) && $boolDirectSet ) $this->set( 'm_intRegressionTaskId', trim( $arrValues['regression_task_id'] ) ); elseif( isset( $arrValues['regression_task_id'] ) ) $this->setRegressionTaskId( $arrValues['regression_task_id'] );
		if( isset( $arrValues['qa_lead_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQaLeadEmployeeId', trim( $arrValues['qa_lead_employee_id'] ) ); elseif( isset( $arrValues['qa_lead_employee_id'] ) ) $this->setQaLeadEmployeeId( $arrValues['qa_lead_employee_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setTestCaseProductFolderId( $intTestCaseProductFolderId ) {
		$this->set( 'm_intTestCaseProductFolderId', CStrings::strToIntDef( $intTestCaseProductFolderId, NULL, false ) );
	}

	public function getTestCaseProductFolderId() {
		return $this->m_intTestCaseProductFolderId;
	}

	public function sqlTestCaseProductFolderId() {
		return ( true == isset( $this->m_intTestCaseProductFolderId ) ) ? ( string ) $this->m_intTestCaseProductFolderId : 'NULL';
	}

	public function setReleaseDate( $strReleaseDate ) {
		$this->set( 'm_strReleaseDate', CStrings::strTrimDef( $strReleaseDate, -1, NULL, true ) );
	}

	public function getReleaseDate() {
		return $this->m_strReleaseDate;
	}

	public function sqlReleaseDate() {
		return ( true == isset( $this->m_strReleaseDate ) ) ? '\'' . $this->m_strReleaseDate . '\'' : 'NOW()';
	}

	public function setIsStandardRelease( $boolIsStandardRelease ) {
		$this->set( 'm_boolIsStandardRelease', CStrings::strToBool( $boolIsStandardRelease ) );
	}

	public function getIsStandardRelease() {
		return $this->m_boolIsStandardRelease;
	}

	public function sqlIsStandardRelease() {
		return ( true == isset( $this->m_boolIsStandardRelease ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsStandardRelease ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setRegressionTaskId( $intRegressionTaskId ) {
		$this->set( 'm_intRegressionTaskId', CStrings::strToIntDef( $intRegressionTaskId, NULL, false ) );
	}

	public function getRegressionTaskId() {
		return $this->m_intRegressionTaskId;
	}

	public function sqlRegressionTaskId() {
		return ( true == isset( $this->m_intRegressionTaskId ) ) ? ( string ) $this->m_intRegressionTaskId : 'NULL';
	}

	public function setQaLeadEmployeeId( $intQaLeadEmployeeId ) {
		$this->set( 'm_intQaLeadEmployeeId', CStrings::strToIntDef( $intQaLeadEmployeeId, NULL, false ) );
	}

	public function getQaLeadEmployeeId() {
		return $this->m_intQaLeadEmployeeId;
	}

	public function sqlQaLeadEmployeeId() {
		return ( true == isset( $this->m_intQaLeadEmployeeId ) ) ? ( string ) $this->m_intQaLeadEmployeeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, test_case_product_folder_id, release_date, is_standard_release, is_active, updated_by, updated_on, created_by, created_on, regression_task_id, qa_lead_employee_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlTestCaseProductFolderId() . ', ' .
						$this->sqlReleaseDate() . ', ' .
						$this->sqlIsStandardRelease() . ', ' .
						$this->sqlIsActive() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlRegressionTaskId() . ', ' .
						$this->sqlQaLeadEmployeeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_product_folder_id = ' . $this->sqlTestCaseProductFolderId(). ',' ; } elseif( true == array_key_exists( 'TestCaseProductFolderId', $this->getChangedColumns() ) ) { $strSql .= ' test_case_product_folder_id = ' . $this->sqlTestCaseProductFolderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_date = ' . $this->sqlReleaseDate(). ',' ; } elseif( true == array_key_exists( 'ReleaseDate', $this->getChangedColumns() ) ) { $strSql .= ' release_date = ' . $this->sqlReleaseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_standard_release = ' . $this->sqlIsStandardRelease(). ',' ; } elseif( true == array_key_exists( 'IsStandardRelease', $this->getChangedColumns() ) ) { $strSql .= ' is_standard_release = ' . $this->sqlIsStandardRelease() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive(). ',' ; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' regression_task_id = ' . $this->sqlRegressionTaskId(). ',' ; } elseif( true == array_key_exists( 'RegressionTaskId', $this->getChangedColumns() ) ) { $strSql .= ' regression_task_id = ' . $this->sqlRegressionTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_lead_employee_id = ' . $this->sqlQaLeadEmployeeId(). ',' ; } elseif( true == array_key_exists( 'QaLeadEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qa_lead_employee_id = ' . $this->sqlQaLeadEmployeeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'test_case_product_folder_id' => $this->getTestCaseProductFolderId(),
			'release_date' => $this->getReleaseDate(),
			'is_standard_release' => $this->getIsStandardRelease(),
			'is_active' => $this->getIsActive(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'regression_task_id' => $this->getRegressionTaskId(),
			'qa_lead_employee_id' => $this->getQaLeadEmployeeId()
		);
	}

}
?>