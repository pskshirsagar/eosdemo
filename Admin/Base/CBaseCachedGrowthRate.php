<?php

class CBaseCachedGrowthRate extends CEosSingularBase {

	const TABLE_NAME = 'public.cached_growth_rates';

	protected $m_strYear;
	protected $m_fltProjectedSalesRate;
	protected $m_fltProjectedTransactionRate;
	protected $m_fltProjectedTransferInRate;
	protected $m_fltProjectedPropertyRate;
	protected $m_fltInsuranceContingentCommissionPercent;
	protected $m_fltPercentCore;
	protected $m_fltPercentStudent;
	protected $m_fltPercentNonCore;
	protected $m_boolAllowDifferentialUpdate;


	public function __construct() {
		parent::__construct();

		$this->m_fltProjectedSalesRate = '0';
		$this->m_fltProjectedTransactionRate = '0';
		$this->m_fltProjectedTransferInRate = '0';
		$this->m_fltProjectedPropertyRate = '0';
		$this->m_fltInsuranceContingentCommissionPercent = '0';
		$this->m_fltPercentCore = '0';
		$this->m_fltPercentStudent = '0';
		$this->m_fltPercentNonCore = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['year'] ) && $boolDirectSet ) $this->set( 'm_strYear', trim( $arrValues['year'] ) ); elseif( isset( $arrValues['year'] ) ) $this->setYear( $arrValues['year'] );
		if( isset( $arrValues['projected_sales_rate'] ) && $boolDirectSet ) $this->set( 'm_fltProjectedSalesRate', trim( $arrValues['projected_sales_rate'] ) ); elseif( isset( $arrValues['projected_sales_rate'] ) ) $this->setProjectedSalesRate( $arrValues['projected_sales_rate'] );
		if( isset( $arrValues['projected_transaction_rate'] ) && $boolDirectSet ) $this->set( 'm_fltProjectedTransactionRate', trim( $arrValues['projected_transaction_rate'] ) ); elseif( isset( $arrValues['projected_transaction_rate'] ) ) $this->setProjectedTransactionRate( $arrValues['projected_transaction_rate'] );
		if( isset( $arrValues['projected_transfer_in_rate'] ) && $boolDirectSet ) $this->set( 'm_fltProjectedTransferInRate', trim( $arrValues['projected_transfer_in_rate'] ) ); elseif( isset( $arrValues['projected_transfer_in_rate'] ) ) $this->setProjectedTransferInRate( $arrValues['projected_transfer_in_rate'] );
		if( isset( $arrValues['projected_property_rate'] ) && $boolDirectSet ) $this->set( 'm_fltProjectedPropertyRate', trim( $arrValues['projected_property_rate'] ) ); elseif( isset( $arrValues['projected_property_rate'] ) ) $this->setProjectedPropertyRate( $arrValues['projected_property_rate'] );
		if( isset( $arrValues['insurance_contingent_commission_percent'] ) && $boolDirectSet ) $this->set( 'm_fltInsuranceContingentCommissionPercent', trim( $arrValues['insurance_contingent_commission_percent'] ) ); elseif( isset( $arrValues['insurance_contingent_commission_percent'] ) ) $this->setInsuranceContingentCommissionPercent( $arrValues['insurance_contingent_commission_percent'] );
		if( isset( $arrValues['percent_core'] ) && $boolDirectSet ) $this->set( 'm_fltPercentCore', trim( $arrValues['percent_core'] ) ); elseif( isset( $arrValues['percent_core'] ) ) $this->setPercentCore( $arrValues['percent_core'] );
		if( isset( $arrValues['percent_student'] ) && $boolDirectSet ) $this->set( 'm_fltPercentStudent', trim( $arrValues['percent_student'] ) ); elseif( isset( $arrValues['percent_student'] ) ) $this->setPercentStudent( $arrValues['percent_student'] );
		if( isset( $arrValues['percent_non_core'] ) && $boolDirectSet ) $this->set( 'm_fltPercentNonCore', trim( $arrValues['percent_non_core'] ) ); elseif( isset( $arrValues['percent_non_core'] ) ) $this->setPercentNonCore( $arrValues['percent_non_core'] );
		$this->m_boolInitialized = true;
	}

	public function setYear( $strYear ) {
		$this->set( 'm_strYear', CStrings::strTrimDef( $strYear, -1, NULL, true ) );
	}

	public function setProjectedSalesRate( $fltProjectedSalesRate ) {
		$this->set( 'm_fltProjectedSalesRate', CStrings::strToFloatDef( $fltProjectedSalesRate, NULL, false, 2 ) );
	}

	public function setProjectedTransactionRate( $fltProjectedTransactionRate ) {
		$this->set( 'm_fltProjectedTransactionRate', CStrings::strToFloatDef( $fltProjectedTransactionRate, NULL, false, 2 ) );
	}

	public function setProjectedTransferInRate( $fltProjectedTransferInRate ) {
		$this->set( 'm_fltProjectedTransferInRate', CStrings::strToFloatDef( $fltProjectedTransferInRate, NULL, false, 2 ) );
	}

	public function setProjectedPropertyRate( $fltProjectedPropertyRate ) {
		$this->set( 'm_fltProjectedPropertyRate', CStrings::strToFloatDef( $fltProjectedPropertyRate, NULL, false, 2 ) );
	}

	public function setInsuranceContingentCommissionPercent( $fltInsuranceContingentCommissionPercent ) {
		$this->set( 'm_fltInsuranceContingentCommissionPercent', CStrings::strToFloatDef( $fltInsuranceContingentCommissionPercent, NULL, false, 2 ) );
	}

	public function setPercentCore( $fltPercentCore ) {
		$this->set( 'm_fltPercentCore', CStrings::strToFloatDef( $fltPercentCore, NULL, false, 2 ) );
	}

	public function setPercentStudent( $fltPercentStudent ) {
		$this->set( 'm_fltPercentStudent', CStrings::strToFloatDef( $fltPercentStudent, NULL, false, 2 ) );
	}

	public function setPercentNonCore( $fltPercentNonCore ) {
		$this->set( 'm_fltPercentNonCore', CStrings::strToFloatDef( $fltPercentNonCore, NULL, false, 2 ) );
	}

	public function toArray() {
		return array(
			'year' => $this->getYear(),
			'projected_sales_rate' => $this->getProjectedSalesRate(),
			'projected_transaction_rate' => $this->getProjectedTransactionRate(),
			'projected_transfer_in_rate' => $this->getProjectedTransferInRate(),
			'projected_property_rate' => $this->getProjectedPropertyRate(),
			'insurance_contingent_commission_percent' => $this->getInsuranceContingentCommissionPercent(),
			'percent_core' => $this->getPercentCore(),
			'percent_student' => $this->getPercentStudent(),
			'percent_non_core' => $this->getPercentNonCore()
		);
	}

	public function getYear() {
		return $this->m_strYear;
	}

	public function getProjectedSalesRate() {
		return $this->m_fltProjectedSalesRate;
	}

	public function getProjectedTransactionRate() {
		return $this->m_fltProjectedTransactionRate;
	}

	public function getProjectedTransferInRate() {
		return $this->m_fltProjectedTransferInRate;
	}

	public function getProjectedPropertyRate() {
		return $this->m_fltProjectedPropertyRate;
	}

	public function getInsuranceContingentCommissionPercent() {
		return $this->m_fltInsuranceContingentCommissionPercent;
	}

	public function getPercentCore() {
		return $this->m_fltPercentCore;
	}

	public function getPercentStudent() {
		return $this->m_fltPercentStudent;
	}

	public function getPercentNonCore() {
		return $this->m_fltPercentNonCore;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( year, projected_sales_rate, projected_transaction_rate, projected_transfer_in_rate, projected_property_rate, insurance_contingent_commission_percent, percent_core, percent_student, percent_non_core )
					VALUES ( ' .
		          $this->sqlYear() . ', ' .
		          $this->sqlProjectedSalesRate() . ', ' .
		          $this->sqlProjectedTransactionRate() . ', ' .
		          $this->sqlProjectedTransferInRate() . ', ' .
		          $this->sqlProjectedPropertyRate() . ', ' .
		          $this->sqlInsuranceContingentCommissionPercent() . ', ' .
		          $this->sqlPercentCore() . ', ' .
		          $this->sqlPercentStudent() . ', ' .
		          $this->sqlPercentNonCore() . ' );';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function sqlYear() {
		return ( true == isset( $this->m_strYear ) ) ? '\'' . $this->m_strYear . '\'' : 'NOW()';
	}

	public function sqlProjectedSalesRate() {
		return ( true == isset( $this->m_fltProjectedSalesRate ) ) ? ( string ) $this->m_fltProjectedSalesRate : '0';
	}

	public function sqlProjectedTransactionRate() {
		return ( true == isset( $this->m_fltProjectedTransactionRate ) ) ? ( string ) $this->m_fltProjectedTransactionRate : '0';
	}

	public function sqlProjectedTransferInRate() {
		return ( true == isset( $this->m_fltProjectedTransferInRate ) ) ? ( string ) $this->m_fltProjectedTransferInRate : '0';
	}

	public function sqlProjectedPropertyRate() {
		return ( true == isset( $this->m_fltProjectedPropertyRate ) ) ? ( string ) $this->m_fltProjectedPropertyRate : '0';
	}

	public function sqlInsuranceContingentCommissionPercent() {
		return ( true == isset( $this->m_fltInsuranceContingentCommissionPercent ) ) ? ( string ) $this->m_fltInsuranceContingentCommissionPercent : '0';
	}

	public function sqlPercentCore() {
		return ( true == isset( $this->m_fltPercentCore ) ) ? ( string ) $this->m_fltPercentCore : '0';
	}

	public function sqlPercentStudent() {
		return ( true == isset( $this->m_fltPercentStudent ) ) ? ( string ) $this->m_fltPercentStudent : '0';
	}

	public function sqlPercentNonCore() {
		return ( true == isset( $this->m_fltPercentNonCore ) ) ? ( string ) $this->m_fltPercentNonCore : '0';
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' projected_sales_rate = ' . $this->sqlProjectedSalesRate(). ',' ; } elseif( true == array_key_exists( 'ProjectedSalesRate', $this->getChangedColumns() ) ) { $strSql .= ' projected_sales_rate = ' . $this->sqlProjectedSalesRate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' projected_transaction_rate= ' . $this->sqlProjectedTransactionRate(). ',' ; } elseif( true == array_key_exists( 'ProjectedTransactionRate', $this->getChangedColumns() ) ) { $strSql .= ' projected_transaction_rate= ' . $this->sqlProjectedTransactionRate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' projected_transfer_in_rate = ' . $this->sqlProjectedTransferInRate(). ',' ; } elseif( true == array_key_exists( 'ProjectedTransferInRate', $this->getChangedColumns() ) ) { $strSql .= ' projected_transfer_in_rate = ' . $this->sqlProjectedTransferInRate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' projected_property_rate = ' . $this->sqlProjectedPropertyRate(). ',' ; } elseif( true == array_key_exists( 'ProjectedPropertyRate', $this->getChangedColumns() ) ) { $strSql .= ' projected_property_rate = ' . $this->sqlProjectedPropertyRate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' insurance_contingent_commission_percent = ' . $this->sqlInsuranceContingentCommissionPercent(). ',' ; } elseif( true == array_key_exists( 'InsuranceContingentCommissionPercent', $this->getChangedColumns() ) ) { $strSql .= ' insurance_contingent_commission_percent = ' . $this->sqlInsuranceContingentCommissionPercent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percent_core = ' . $this->sqlPercentCore(). ',' ; } elseif( true == array_key_exists( 'PercentCore', $this->getChangedColumns() ) ) { $strSql .= ' percent_core = ' . $this->sqlPercentCore() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percent_student = ' . $this->sqlPercentStudent(). ',' ; } elseif( true == array_key_exists( 'PercentStudent', $this->getChangedColumns() ) ) { $strSql .= ' percent_student = ' . $this->sqlPercentStudent() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' percent_non_core = ' . $this->sqlPercentNonCore() . ','; } elseif( true == array_key_exists( 'PercentNonCore', $this->getChangedColumns() ) ) { $strSql .= ' percent_non_core = ' . $this->sqlPercentNonCore() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' year = ' . $this->sqlYear(); } elseif( true == array_key_exists( 'Year', $this->getChangedColumns() ) ) { $strSql .= ' year = ' . $this->sqlYear(); $boolUpdate = true; }

		$strSql .= ' WHERE
						year = ' . $this->sqlYear() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function getAllowDifferentialUpdate() {
		return $this->m_boolAllowDifferentialUpdate;
	}


}
?>