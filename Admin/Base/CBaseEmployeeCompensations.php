<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeCompensations
 * Do not add any new functions to this class.
 */

class CBaseEmployeeCompensations extends CEosPluralBase {

	/**
	 * @return CEmployeeCompensation[]
	 */
	public static function fetchEmployeeCompensations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeCompensation', $objDatabase );
	}

	/**
	 * @return CEmployeeCompensation
	 */
	public static function fetchEmployeeCompensation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeCompensation', $objDatabase );
	}

	public static function fetchEmployeeCompensationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_compensations', $objDatabase );
	}

	public static function fetchEmployeeCompensationById( $intId, $objDatabase ) {
		return self::fetchEmployeeCompensation( sprintf( 'SELECT * FROM employee_compensations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeCompensationsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeCompensations( sprintf( 'SELECT * FROM employee_compensations WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeCompensationsByEmployeeEncryptionAssociationId( $intEmployeeEncryptionAssociationId, $objDatabase ) {
		return self::fetchEmployeeCompensations( sprintf( 'SELECT * FROM employee_compensations WHERE employee_encryption_association_id = %d', ( int ) $intEmployeeEncryptionAssociationId ), $objDatabase );
	}

}
?>