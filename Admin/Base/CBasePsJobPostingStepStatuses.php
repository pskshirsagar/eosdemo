<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsJobPostingStepStatuses
 * Do not add any new functions to this class.
 */

class CBasePsJobPostingStepStatuses extends CEosPluralBase {

	/**
	 * @return CPsJobPostingStepStatus[]
	 */
	public static function fetchPsJobPostingStepStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsJobPostingStepStatus', $objDatabase );
	}

	/**
	 * @return CPsJobPostingStepStatus
	 */
	public static function fetchPsJobPostingStepStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsJobPostingStepStatus', $objDatabase );
	}

	public static function fetchPsJobPostingStepStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_job_posting_step_statuses', $objDatabase );
	}

	public static function fetchPsJobPostingStepStatusById( $intId, $objDatabase ) {
		return self::fetchPsJobPostingStepStatus( sprintf( 'SELECT * FROM ps_job_posting_step_statuses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsJobPostingStepStatusesByPsJobPostingStepId( $intPsJobPostingStepId, $objDatabase ) {
		return self::fetchPsJobPostingStepStatuses( sprintf( 'SELECT * FROM ps_job_posting_step_statuses WHERE ps_job_posting_step_id = %d', ( int ) $intPsJobPostingStepId ), $objDatabase );
	}

	public static function fetchPsJobPostingStepStatusesByPsJobPostingStatusId( $intPsJobPostingStatusId, $objDatabase ) {
		return self::fetchPsJobPostingStepStatuses( sprintf( 'SELECT * FROM ps_job_posting_step_statuses WHERE ps_job_posting_status_id = %d', ( int ) $intPsJobPostingStatusId ), $objDatabase );
	}

}
?>