<?php

class CBasePsEvent extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_events';

	protected $m_intId;
	protected $m_intPsEventTypeId;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_strInstructions;
	protected $m_strLocation;
	protected $m_strStartDatetime;
	protected $m_strEndDatetime;
	protected $m_intShowOnWebsite;
	protected $m_intShowInResidentWorks;
	protected $m_intShowInNewsletter;
	protected $m_boolShowInClientAdmin;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intShowOnWebsite = '0';
		$this->m_intShowInResidentWorks = '0';
		$this->m_intShowInNewsletter = '0';
		$this->m_boolShowInClientAdmin = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_event_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsEventTypeId', trim( $arrValues['ps_event_type_id'] ) ); elseif( isset( $arrValues['ps_event_type_id'] ) ) $this->setPsEventTypeId( $arrValues['ps_event_type_id'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['instructions'] ) && $boolDirectSet ) $this->set( 'm_strInstructions', trim( stripcslashes( $arrValues['instructions'] ) ) ); elseif( isset( $arrValues['instructions'] ) ) $this->setInstructions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['instructions'] ) : $arrValues['instructions'] );
		if( isset( $arrValues['location'] ) && $boolDirectSet ) $this->set( 'm_strLocation', trim( stripcslashes( $arrValues['location'] ) ) ); elseif( isset( $arrValues['location'] ) ) $this->setLocation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['location'] ) : $arrValues['location'] );
		if( isset( $arrValues['start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strStartDatetime', trim( $arrValues['start_datetime'] ) ); elseif( isset( $arrValues['start_datetime'] ) ) $this->setStartDatetime( $arrValues['start_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['show_on_website'] ) && $boolDirectSet ) $this->set( 'm_intShowOnWebsite', trim( $arrValues['show_on_website'] ) ); elseif( isset( $arrValues['show_on_website'] ) ) $this->setShowOnWebsite( $arrValues['show_on_website'] );
		if( isset( $arrValues['show_in_resident_works'] ) && $boolDirectSet ) $this->set( 'm_intShowInResidentWorks', trim( $arrValues['show_in_resident_works'] ) ); elseif( isset( $arrValues['show_in_resident_works'] ) ) $this->setShowInResidentWorks( $arrValues['show_in_resident_works'] );
		if( isset( $arrValues['show_in_newsletter'] ) && $boolDirectSet ) $this->set( 'm_intShowInNewsletter', trim( $arrValues['show_in_newsletter'] ) ); elseif( isset( $arrValues['show_in_newsletter'] ) ) $this->setShowInNewsletter( $arrValues['show_in_newsletter'] );
		if( isset( $arrValues['show_in_client_admin'] ) && $boolDirectSet ) $this->set( 'm_boolShowInClientAdmin', trim( stripcslashes( $arrValues['show_in_client_admin'] ) ) ); elseif( isset( $arrValues['show_in_client_admin'] ) ) $this->setShowInClientAdmin( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['show_in_client_admin'] ) : $arrValues['show_in_client_admin'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsEventTypeId( $intPsEventTypeId ) {
		$this->set( 'm_intPsEventTypeId', CStrings::strToIntDef( $intPsEventTypeId, NULL, false ) );
	}

	public function getPsEventTypeId() {
		return $this->m_intPsEventTypeId;
	}

	public function sqlPsEventTypeId() {
		return ( true == isset( $this->m_intPsEventTypeId ) ) ? ( string ) $this->m_intPsEventTypeId : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setInstructions( $strInstructions ) {
		$this->set( 'm_strInstructions', CStrings::strTrimDef( $strInstructions, 2000, NULL, true ) );
	}

	public function getInstructions() {
		return $this->m_strInstructions;
	}

	public function sqlInstructions() {
		return ( true == isset( $this->m_strInstructions ) ) ? '\'' . addslashes( $this->m_strInstructions ) . '\'' : 'NULL';
	}

	public function setLocation( $strLocation ) {
		$this->set( 'm_strLocation', CStrings::strTrimDef( $strLocation, 240, NULL, true ) );
	}

	public function getLocation() {
		return $this->m_strLocation;
	}

	public function sqlLocation() {
		return ( true == isset( $this->m_strLocation ) ) ? '\'' . addslashes( $this->m_strLocation ) . '\'' : 'NULL';
	}

	public function setStartDatetime( $strStartDatetime ) {
		$this->set( 'm_strStartDatetime', CStrings::strTrimDef( $strStartDatetime, -1, NULL, true ) );
	}

	public function getStartDatetime() {
		return $this->m_strStartDatetime;
	}

	public function sqlStartDatetime() {
		return ( true == isset( $this->m_strStartDatetime ) ) ? '\'' . $this->m_strStartDatetime . '\'' : 'NULL';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setShowOnWebsite( $intShowOnWebsite ) {
		$this->set( 'm_intShowOnWebsite', CStrings::strToIntDef( $intShowOnWebsite, NULL, false ) );
	}

	public function getShowOnWebsite() {
		return $this->m_intShowOnWebsite;
	}

	public function sqlShowOnWebsite() {
		return ( true == isset( $this->m_intShowOnWebsite ) ) ? ( string ) $this->m_intShowOnWebsite : '0';
	}

	public function setShowInResidentWorks( $intShowInResidentWorks ) {
		$this->set( 'm_intShowInResidentWorks', CStrings::strToIntDef( $intShowInResidentWorks, NULL, false ) );
	}

	public function getShowInResidentWorks() {
		return $this->m_intShowInResidentWorks;
	}

	public function sqlShowInResidentWorks() {
		return ( true == isset( $this->m_intShowInResidentWorks ) ) ? ( string ) $this->m_intShowInResidentWorks : '0';
	}

	public function setShowInNewsletter( $intShowInNewsletter ) {
		$this->set( 'm_intShowInNewsletter', CStrings::strToIntDef( $intShowInNewsletter, NULL, false ) );
	}

	public function getShowInNewsletter() {
		return $this->m_intShowInNewsletter;
	}

	public function sqlShowInNewsletter() {
		return ( true == isset( $this->m_intShowInNewsletter ) ) ? ( string ) $this->m_intShowInNewsletter : '0';
	}

	public function setShowInClientAdmin( $boolShowInClientAdmin ) {
		$this->set( 'm_boolShowInClientAdmin', CStrings::strToBool( $boolShowInClientAdmin ) );
	}

	public function getShowInClientAdmin() {
		return $this->m_boolShowInClientAdmin;
	}

	public function sqlShowInClientAdmin() {
		return ( true == isset( $this->m_boolShowInClientAdmin ) ) ? '\'' . ( true == ( bool ) $this->m_boolShowInClientAdmin ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_event_type_id, title, description, instructions, location, start_datetime, end_datetime, show_on_website, show_in_resident_works, show_in_newsletter, show_in_client_admin, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsEventTypeId() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlInstructions() . ', ' .
 						$this->sqlLocation() . ', ' .
 						$this->sqlStartDatetime() . ', ' .
 						$this->sqlEndDatetime() . ', ' .
 						$this->sqlShowOnWebsite() . ', ' .
 						$this->sqlShowInResidentWorks() . ', ' .
 						$this->sqlShowInNewsletter() . ', ' .
 						$this->sqlShowInClientAdmin() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_event_type_id = ' . $this->sqlPsEventTypeId() . ','; } elseif( true == array_key_exists( 'PsEventTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ps_event_type_id = ' . $this->sqlPsEventTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instructions = ' . $this->sqlInstructions() . ','; } elseif( true == array_key_exists( 'Instructions', $this->getChangedColumns() ) ) { $strSql .= ' instructions = ' . $this->sqlInstructions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' location = ' . $this->sqlLocation() . ','; } elseif( true == array_key_exists( 'Location', $this->getChangedColumns() ) ) { $strSql .= ' location = ' . $this->sqlLocation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; } elseif( true == array_key_exists( 'StartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' start_datetime = ' . $this->sqlStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ','; } elseif( true == array_key_exists( 'ShowOnWebsite', $this->getChangedColumns() ) ) { $strSql .= ' show_on_website = ' . $this->sqlShowOnWebsite() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_resident_works = ' . $this->sqlShowInResidentWorks() . ','; } elseif( true == array_key_exists( 'ShowInResidentWorks', $this->getChangedColumns() ) ) { $strSql .= ' show_in_resident_works = ' . $this->sqlShowInResidentWorks() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_newsletter = ' . $this->sqlShowInNewsletter() . ','; } elseif( true == array_key_exists( 'ShowInNewsletter', $this->getChangedColumns() ) ) { $strSql .= ' show_in_newsletter = ' . $this->sqlShowInNewsletter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' show_in_client_admin = ' . $this->sqlShowInClientAdmin() . ','; } elseif( true == array_key_exists( 'ShowInClientAdmin', $this->getChangedColumns() ) ) { $strSql .= ' show_in_client_admin = ' . $this->sqlShowInClientAdmin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_event_type_id' => $this->getPsEventTypeId(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'instructions' => $this->getInstructions(),
			'location' => $this->getLocation(),
			'start_datetime' => $this->getStartDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'show_on_website' => $this->getShowOnWebsite(),
			'show_in_resident_works' => $this->getShowInResidentWorks(),
			'show_in_newsletter' => $this->getShowInNewsletter(),
			'show_in_client_admin' => $this->getShowInClientAdmin(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>