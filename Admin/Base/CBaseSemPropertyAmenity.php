<?php

class CBaseSemPropertyAmenity extends CEosSingularBase {

	const TABLE_NAME = 'public.sem_property_amenities';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intStandardAmenityId;
	protected $m_intIsCommunityAmenity;
	protected $m_intIsUnitAmenity;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsCommunityAmenity = '0';
		$this->m_intIsUnitAmenity = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['standard_amenity_id'] ) && $boolDirectSet ) $this->set( 'm_intStandardAmenityId', trim( $arrValues['standard_amenity_id'] ) ); elseif( isset( $arrValues['standard_amenity_id'] ) ) $this->setStandardAmenityId( $arrValues['standard_amenity_id'] );
		if( isset( $arrValues['is_community_amenity'] ) && $boolDirectSet ) $this->set( 'm_intIsCommunityAmenity', trim( $arrValues['is_community_amenity'] ) ); elseif( isset( $arrValues['is_community_amenity'] ) ) $this->setIsCommunityAmenity( $arrValues['is_community_amenity'] );
		if( isset( $arrValues['is_unit_amenity'] ) && $boolDirectSet ) $this->set( 'm_intIsUnitAmenity', trim( $arrValues['is_unit_amenity'] ) ); elseif( isset( $arrValues['is_unit_amenity'] ) ) $this->setIsUnitAmenity( $arrValues['is_unit_amenity'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setStandardAmenityId( $intStandardAmenityId ) {
		$this->set( 'm_intStandardAmenityId', CStrings::strToIntDef( $intStandardAmenityId, NULL, false ) );
	}

	public function getStandardAmenityId() {
		return $this->m_intStandardAmenityId;
	}

	public function sqlStandardAmenityId() {
		return ( true == isset( $this->m_intStandardAmenityId ) ) ? ( string ) $this->m_intStandardAmenityId : 'NULL';
	}

	public function setIsCommunityAmenity( $intIsCommunityAmenity ) {
		$this->set( 'm_intIsCommunityAmenity', CStrings::strToIntDef( $intIsCommunityAmenity, NULL, false ) );
	}

	public function getIsCommunityAmenity() {
		return $this->m_intIsCommunityAmenity;
	}

	public function sqlIsCommunityAmenity() {
		return ( true == isset( $this->m_intIsCommunityAmenity ) ) ? ( string ) $this->m_intIsCommunityAmenity : '0';
	}

	public function setIsUnitAmenity( $intIsUnitAmenity ) {
		$this->set( 'm_intIsUnitAmenity', CStrings::strToIntDef( $intIsUnitAmenity, NULL, false ) );
	}

	public function getIsUnitAmenity() {
		return $this->m_intIsUnitAmenity;
	}

	public function sqlIsUnitAmenity() {
		return ( true == isset( $this->m_intIsUnitAmenity ) ) ? ( string ) $this->m_intIsUnitAmenity : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, standard_amenity_id, is_community_amenity, is_unit_amenity, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlStandardAmenityId() . ', ' .
 						$this->sqlIsCommunityAmenity() . ', ' .
 						$this->sqlIsUnitAmenity() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' standard_amenity_id = ' . $this->sqlStandardAmenityId() . ','; } elseif( true == array_key_exists( 'StandardAmenityId', $this->getChangedColumns() ) ) { $strSql .= ' standard_amenity_id = ' . $this->sqlStandardAmenityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_community_amenity = ' . $this->sqlIsCommunityAmenity() . ','; } elseif( true == array_key_exists( 'IsCommunityAmenity', $this->getChangedColumns() ) ) { $strSql .= ' is_community_amenity = ' . $this->sqlIsCommunityAmenity() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_unit_amenity = ' . $this->sqlIsUnitAmenity() . ','; } elseif( true == array_key_exists( 'IsUnitAmenity', $this->getChangedColumns() ) ) { $strSql .= ' is_unit_amenity = ' . $this->sqlIsUnitAmenity() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'standard_amenity_id' => $this->getStandardAmenityId(),
			'is_community_amenity' => $this->getIsCommunityAmenity(),
			'is_unit_amenity' => $this->getIsUnitAmenity(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>