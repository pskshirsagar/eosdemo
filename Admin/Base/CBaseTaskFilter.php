<?php

class CBaseTaskFilter extends CEosSingularBase {

	const TABLE_NAME = 'public.task_filters';

	protected $m_intId;
	protected $m_intUserId;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_strClients;
	protected $m_strProjectManagers;
	protected $m_strUsers;
	protected $m_strCreatedByUsers;
	protected $m_strClientResolutionEmployees;
	protected $m_strDepartments;
	protected $m_strTaskTypes;
	protected $m_strTaskStatuses;
	protected $m_strTaskPriorities;
	protected $m_strTaskReleases;
	protected $m_strPdmTaskReleases;
	protected $m_strTaskServers;
	protected $m_strWebsites;
	protected $m_strProperties;
	protected $m_strPsProducts;
	protected $m_strPsProductOptions;
	protected $m_strPsProductModules;
	protected $m_strPsProductVersions;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_strDueDate;
	protected $m_strStartDueDate;
	protected $m_strEndDueDate;
	protected $m_strKeywordSearch;
	protected $m_strOrderByField;
	protected $m_strOrderByType;
	protected $m_strSerializedFilter;
	protected $m_intSinceAssignedTo;
	protected $m_intSinceNotUpdated;
	protected $m_intSinceLastUpdated;
	protected $m_intIsSystem;
	protected $m_intIsDisabled;
	protected $m_intIsQaApproved;
	protected $m_intAmIStakeholder;
	protected $m_intHasNoParentTask;
	protected $m_strTaskQuarters;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['clients'] ) && $boolDirectSet ) $this->set( 'm_strClients', trim( stripcslashes( $arrValues['clients'] ) ) ); elseif( isset( $arrValues['clients'] ) ) $this->setClients( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['clients'] ) : $arrValues['clients'] );
		if( isset( $arrValues['project_managers'] ) && $boolDirectSet ) $this->set( 'm_strProjectManagers', trim( stripcslashes( $arrValues['project_managers'] ) ) ); elseif( isset( $arrValues['project_managers'] ) ) $this->setProjectManagers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['project_managers'] ) : $arrValues['project_managers'] );
		if( isset( $arrValues['users'] ) && $boolDirectSet ) $this->set( 'm_strUsers', trim( stripcslashes( $arrValues['users'] ) ) ); elseif( isset( $arrValues['users'] ) ) $this->setUsers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['users'] ) : $arrValues['users'] );
		if( isset( $arrValues['created_by_users'] ) && $boolDirectSet ) $this->set( 'm_strCreatedByUsers', trim( stripcslashes( $arrValues['created_by_users'] ) ) ); elseif( isset( $arrValues['created_by_users'] ) ) $this->setCreatedByUsers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['created_by_users'] ) : $arrValues['created_by_users'] );
		if( isset( $arrValues['client_resolution_employees'] ) && $boolDirectSet ) $this->set( 'm_strClientResolutionEmployees', trim( stripcslashes( $arrValues['client_resolution_employees'] ) ) ); elseif( isset( $arrValues['client_resolution_employees'] ) ) $this->setClientResolutionEmployees( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['client_resolution_employees'] ) : $arrValues['client_resolution_employees'] );
		if( isset( $arrValues['departments'] ) && $boolDirectSet ) $this->set( 'm_strDepartments', trim( stripcslashes( $arrValues['departments'] ) ) ); elseif( isset( $arrValues['departments'] ) ) $this->setDepartments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['departments'] ) : $arrValues['departments'] );
		if( isset( $arrValues['task_types'] ) && $boolDirectSet ) $this->set( 'm_strTaskTypes', trim( stripcslashes( $arrValues['task_types'] ) ) ); elseif( isset( $arrValues['task_types'] ) ) $this->setTaskTypes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['task_types'] ) : $arrValues['task_types'] );
		if( isset( $arrValues['task_statuses'] ) && $boolDirectSet ) $this->set( 'm_strTaskStatuses', trim( stripcslashes( $arrValues['task_statuses'] ) ) ); elseif( isset( $arrValues['task_statuses'] ) ) $this->setTaskStatuses( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['task_statuses'] ) : $arrValues['task_statuses'] );
		if( isset( $arrValues['task_priorities'] ) && $boolDirectSet ) $this->set( 'm_strTaskPriorities', trim( stripcslashes( $arrValues['task_priorities'] ) ) ); elseif( isset( $arrValues['task_priorities'] ) ) $this->setTaskPriorities( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['task_priorities'] ) : $arrValues['task_priorities'] );
		if( isset( $arrValues['task_releases'] ) && $boolDirectSet ) $this->set( 'm_strTaskReleases', trim( stripcslashes( $arrValues['task_releases'] ) ) ); elseif( isset( $arrValues['task_releases'] ) ) $this->setTaskReleases( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['task_releases'] ) : $arrValues['task_releases'] );
		if( isset( $arrValues['pdm_task_releases'] ) && $boolDirectSet ) $this->set( 'm_strPdmTaskReleases', trim( stripcslashes( $arrValues['pdm_task_releases'] ) ) ); elseif( isset( $arrValues['pdm_task_releases'] ) ) $this->setPdmTaskReleases( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['pdm_task_releases'] ) : $arrValues['pdm_task_releases'] );
		if( isset( $arrValues['task_servers'] ) && $boolDirectSet ) $this->set( 'm_strTaskServers', trim( stripcslashes( $arrValues['task_servers'] ) ) ); elseif( isset( $arrValues['task_servers'] ) ) $this->setTaskServers( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['task_servers'] ) : $arrValues['task_servers'] );
		if( isset( $arrValues['websites'] ) && $boolDirectSet ) $this->set( 'm_strWebsites', trim( stripcslashes( $arrValues['websites'] ) ) ); elseif( isset( $arrValues['websites'] ) ) $this->setWebsites( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['websites'] ) : $arrValues['websites'] );
		if( isset( $arrValues['properties'] ) && $boolDirectSet ) $this->set( 'm_strProperties', trim( stripcslashes( $arrValues['properties'] ) ) ); elseif( isset( $arrValues['properties'] ) ) $this->setProperties( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['properties'] ) : $arrValues['properties'] );
		if( isset( $arrValues['ps_products'] ) && $boolDirectSet ) $this->set( 'm_strPsProducts', trim( stripcslashes( $arrValues['ps_products'] ) ) ); elseif( isset( $arrValues['ps_products'] ) ) $this->setPsProducts( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_products'] ) : $arrValues['ps_products'] );
		if( isset( $arrValues['ps_product_options'] ) && $boolDirectSet ) $this->set( 'm_strPsProductOptions', trim( stripcslashes( $arrValues['ps_product_options'] ) ) ); elseif( isset( $arrValues['ps_product_options'] ) ) $this->setPsProductOptions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_product_options'] ) : $arrValues['ps_product_options'] );
		if( isset( $arrValues['ps_product_modules'] ) && $boolDirectSet ) $this->set( 'm_strPsProductModules', trim( stripcslashes( $arrValues['ps_product_modules'] ) ) ); elseif( isset( $arrValues['ps_product_modules'] ) ) $this->setPsProductModules( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_product_modules'] ) : $arrValues['ps_product_modules'] );
		if( isset( $arrValues['ps_product_versions'] ) && $boolDirectSet ) $this->set( 'm_strPsProductVersions', trim( stripcslashes( $arrValues['ps_product_versions'] ) ) ); elseif( isset( $arrValues['ps_product_versions'] ) ) $this->setPsProductVersions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_product_versions'] ) : $arrValues['ps_product_versions'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['due_date'] ) && $boolDirectSet ) $this->set( 'm_strDueDate', trim( $arrValues['due_date'] ) ); elseif( isset( $arrValues['due_date'] ) ) $this->setDueDate( $arrValues['due_date'] );
		if( isset( $arrValues['start_due_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDueDate', trim( $arrValues['start_due_date'] ) ); elseif( isset( $arrValues['start_due_date'] ) ) $this->setStartDueDate( $arrValues['start_due_date'] );
		if( isset( $arrValues['end_due_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDueDate', trim( $arrValues['end_due_date'] ) ); elseif( isset( $arrValues['end_due_date'] ) ) $this->setEndDueDate( $arrValues['end_due_date'] );
		if( isset( $arrValues['keyword_search'] ) && $boolDirectSet ) $this->set( 'm_strKeywordSearch', trim( stripcslashes( $arrValues['keyword_search'] ) ) ); elseif( isset( $arrValues['keyword_search'] ) ) $this->setKeywordSearch( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['keyword_search'] ) : $arrValues['keyword_search'] );
		if( isset( $arrValues['order_by_field'] ) && $boolDirectSet ) $this->set( 'm_strOrderByField', trim( stripcslashes( $arrValues['order_by_field'] ) ) ); elseif( isset( $arrValues['order_by_field'] ) ) $this->setOrderByField( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['order_by_field'] ) : $arrValues['order_by_field'] );
		if( isset( $arrValues['order_by_type'] ) && $boolDirectSet ) $this->set( 'm_strOrderByType', trim( stripcslashes( $arrValues['order_by_type'] ) ) ); elseif( isset( $arrValues['order_by_type'] ) ) $this->setOrderByType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['order_by_type'] ) : $arrValues['order_by_type'] );
		if( isset( $arrValues['serialized_filter'] ) && $boolDirectSet ) $this->set( 'm_strSerializedFilter', trim( stripcslashes( $arrValues['serialized_filter'] ) ) ); elseif( isset( $arrValues['serialized_filter'] ) ) $this->setSerializedFilter( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['serialized_filter'] ) : $arrValues['serialized_filter'] );
		if( isset( $arrValues['since_assigned_to'] ) && $boolDirectSet ) $this->set( 'm_intSinceAssignedTo', trim( $arrValues['since_assigned_to'] ) ); elseif( isset( $arrValues['since_assigned_to'] ) ) $this->setSinceAssignedTo( $arrValues['since_assigned_to'] );
		if( isset( $arrValues['since_not_updated'] ) && $boolDirectSet ) $this->set( 'm_intSinceNotUpdated', trim( $arrValues['since_not_updated'] ) ); elseif( isset( $arrValues['since_not_updated'] ) ) $this->setSinceNotUpdated( $arrValues['since_not_updated'] );
		if( isset( $arrValues['since_last_updated'] ) && $boolDirectSet ) $this->set( 'm_intSinceLastUpdated', trim( $arrValues['since_last_updated'] ) ); elseif( isset( $arrValues['since_last_updated'] ) ) $this->setSinceLastUpdated( $arrValues['since_last_updated'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['is_qa_approved'] ) && $boolDirectSet ) $this->set( 'm_intIsQaApproved', trim( $arrValues['is_qa_approved'] ) ); elseif( isset( $arrValues['is_qa_approved'] ) ) $this->setIsQaApproved( $arrValues['is_qa_approved'] );
		if( isset( $arrValues['am_i_stakeholder'] ) && $boolDirectSet ) $this->set( 'm_intAmIStakeholder', trim( $arrValues['am_i_stakeholder'] ) ); elseif( isset( $arrValues['am_i_stakeholder'] ) ) $this->setAmIStakeholder( $arrValues['am_i_stakeholder'] );
		if( isset( $arrValues['has_no_parent_task'] ) && $boolDirectSet ) $this->set( 'm_intHasNoParentTask', trim( $arrValues['has_no_parent_task'] ) ); elseif( isset( $arrValues['has_no_parent_task'] ) ) $this->setHasNoParentTask( $arrValues['has_no_parent_task'] );
		if( isset( $arrValues['task_quarters'] ) && $boolDirectSet ) $this->set( 'm_strTaskQuarters', trim( stripcslashes( $arrValues['task_quarters'] ) ) ); elseif( isset( $arrValues['task_quarters'] ) ) $this->setTaskQuarters( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['task_quarters'] ) : $arrValues['task_quarters'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setClients( $strClients ) {
		$this->set( 'm_strClients', CStrings::strTrimDef( $strClients, -1, NULL, true ) );
	}

	public function getClients() {
		return $this->m_strClients;
	}

	public function sqlClients() {
		return ( true == isset( $this->m_strClients ) ) ? '\'' . addslashes( $this->m_strClients ) . '\'' : 'NULL';
	}

	public function setProjectManagers( $strProjectManagers ) {
		$this->set( 'm_strProjectManagers', CStrings::strTrimDef( $strProjectManagers, -1, NULL, true ) );
	}

	public function getProjectManagers() {
		return $this->m_strProjectManagers;
	}

	public function sqlProjectManagers() {
		return ( true == isset( $this->m_strProjectManagers ) ) ? '\'' . addslashes( $this->m_strProjectManagers ) . '\'' : 'NULL';
	}

	public function setUsers( $strUsers ) {
		$this->set( 'm_strUsers', CStrings::strTrimDef( $strUsers, -1, NULL, true ) );
	}

	public function getUsers() {
		return $this->m_strUsers;
	}

	public function sqlUsers() {
		return ( true == isset( $this->m_strUsers ) ) ? '\'' . addslashes( $this->m_strUsers ) . '\'' : 'NULL';
	}

	public function setCreatedByUsers( $strCreatedByUsers ) {
		$this->set( 'm_strCreatedByUsers', CStrings::strTrimDef( $strCreatedByUsers, -1, NULL, true ) );
	}

	public function getCreatedByUsers() {
		return $this->m_strCreatedByUsers;
	}

	public function sqlCreatedByUsers() {
		return ( true == isset( $this->m_strCreatedByUsers ) ) ? '\'' . addslashes( $this->m_strCreatedByUsers ) . '\'' : 'NULL';
	}

	public function setClientResolutionEmployees( $strClientResolutionEmployees ) {
		$this->set( 'm_strClientResolutionEmployees', CStrings::strTrimDef( $strClientResolutionEmployees, -1, NULL, true ) );
	}

	public function getClientResolutionEmployees() {
		return $this->m_strClientResolutionEmployees;
	}

	public function sqlClientResolutionEmployees() {
		return ( true == isset( $this->m_strClientResolutionEmployees ) ) ? '\'' . addslashes( $this->m_strClientResolutionEmployees ) . '\'' : 'NULL';
	}

	public function setDepartments( $strDepartments ) {
		$this->set( 'm_strDepartments', CStrings::strTrimDef( $strDepartments, -1, NULL, true ) );
	}

	public function getDepartments() {
		return $this->m_strDepartments;
	}

	public function sqlDepartments() {
		return ( true == isset( $this->m_strDepartments ) ) ? '\'' . addslashes( $this->m_strDepartments ) . '\'' : 'NULL';
	}

	public function setTaskTypes( $strTaskTypes ) {
		$this->set( 'm_strTaskTypes', CStrings::strTrimDef( $strTaskTypes, -1, NULL, true ) );
	}

	public function getTaskTypes() {
		return $this->m_strTaskTypes;
	}

	public function sqlTaskTypes() {
		return ( true == isset( $this->m_strTaskTypes ) ) ? '\'' . addslashes( $this->m_strTaskTypes ) . '\'' : 'NULL';
	}

	public function setTaskStatuses( $strTaskStatuses ) {
		$this->set( 'm_strTaskStatuses', CStrings::strTrimDef( $strTaskStatuses, -1, NULL, true ) );
	}

	public function getTaskStatuses() {
		return $this->m_strTaskStatuses;
	}

	public function sqlTaskStatuses() {
		return ( true == isset( $this->m_strTaskStatuses ) ) ? '\'' . addslashes( $this->m_strTaskStatuses ) . '\'' : 'NULL';
	}

	public function setTaskPriorities( $strTaskPriorities ) {
		$this->set( 'm_strTaskPriorities', CStrings::strTrimDef( $strTaskPriorities, -1, NULL, true ) );
	}

	public function getTaskPriorities() {
		return $this->m_strTaskPriorities;
	}

	public function sqlTaskPriorities() {
		return ( true == isset( $this->m_strTaskPriorities ) ) ? '\'' . addslashes( $this->m_strTaskPriorities ) . '\'' : 'NULL';
	}

	public function setTaskReleases( $strTaskReleases ) {
		$this->set( 'm_strTaskReleases', CStrings::strTrimDef( $strTaskReleases, -1, NULL, true ) );
	}

	public function getTaskReleases() {
		return $this->m_strTaskReleases;
	}

	public function sqlTaskReleases() {
		return ( true == isset( $this->m_strTaskReleases ) ) ? '\'' . addslashes( $this->m_strTaskReleases ) . '\'' : 'NULL';
	}

	public function setPdmTaskReleases( $strPdmTaskReleases ) {
		$this->set( 'm_strPdmTaskReleases', CStrings::strTrimDef( $strPdmTaskReleases, -1, NULL, true ) );
	}

	public function getPdmTaskReleases() {
		return $this->m_strPdmTaskReleases;
	}

	public function sqlPdmTaskReleases() {
		return ( true == isset( $this->m_strPdmTaskReleases ) ) ? '\'' . addslashes( $this->m_strPdmTaskReleases ) . '\'' : 'NULL';
	}

	public function setTaskServers( $strTaskServers ) {
		$this->set( 'm_strTaskServers', CStrings::strTrimDef( $strTaskServers, -1, NULL, true ) );
	}

	public function getTaskServers() {
		return $this->m_strTaskServers;
	}

	public function sqlTaskServers() {
		return ( true == isset( $this->m_strTaskServers ) ) ? '\'' . addslashes( $this->m_strTaskServers ) . '\'' : 'NULL';
	}

	public function setWebsites( $strWebsites ) {
		$this->set( 'm_strWebsites', CStrings::strTrimDef( $strWebsites, -1, NULL, true ) );
	}

	public function getWebsites() {
		return $this->m_strWebsites;
	}

	public function sqlWebsites() {
		return ( true == isset( $this->m_strWebsites ) ) ? '\'' . addslashes( $this->m_strWebsites ) . '\'' : 'NULL';
	}

	public function setProperties( $strProperties ) {
		$this->set( 'm_strProperties', CStrings::strTrimDef( $strProperties, -1, NULL, true ) );
	}

	public function getProperties() {
		return $this->m_strProperties;
	}

	public function sqlProperties() {
		return ( true == isset( $this->m_strProperties ) ) ? '\'' . addslashes( $this->m_strProperties ) . '\'' : 'NULL';
	}

	public function setPsProducts( $strPsProducts ) {
		$this->set( 'm_strPsProducts', CStrings::strTrimDef( $strPsProducts, -1, NULL, true ) );
	}

	public function getPsProducts() {
		return $this->m_strPsProducts;
	}

	public function sqlPsProducts() {
		return ( true == isset( $this->m_strPsProducts ) ) ? '\'' . addslashes( $this->m_strPsProducts ) . '\'' : 'NULL';
	}

	public function setPsProductOptions( $strPsProductOptions ) {
		$this->set( 'm_strPsProductOptions', CStrings::strTrimDef( $strPsProductOptions, -1, NULL, true ) );
	}

	public function getPsProductOptions() {
		return $this->m_strPsProductOptions;
	}

	public function sqlPsProductOptions() {
		return ( true == isset( $this->m_strPsProductOptions ) ) ? '\'' . addslashes( $this->m_strPsProductOptions ) . '\'' : 'NULL';
	}

	public function setPsProductModules( $strPsProductModules ) {
		$this->set( 'm_strPsProductModules', CStrings::strTrimDef( $strPsProductModules, -1, NULL, true ) );
	}

	public function getPsProductModules() {
		return $this->m_strPsProductModules;
	}

	public function sqlPsProductModules() {
		return ( true == isset( $this->m_strPsProductModules ) ) ? '\'' . addslashes( $this->m_strPsProductModules ) . '\'' : 'NULL';
	}

	public function setPsProductVersions( $strPsProductVersions ) {
		$this->set( 'm_strPsProductVersions', CStrings::strTrimDef( $strPsProductVersions, -1, NULL, true ) );
	}

	public function getPsProductVersions() {
		return $this->m_strPsProductVersions;
	}

	public function sqlPsProductVersions() {
		return ( true == isset( $this->m_strPsProductVersions ) ) ? '\'' . addslashes( $this->m_strPsProductVersions ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setDueDate( $strDueDate ) {
		$this->set( 'm_strDueDate', CStrings::strTrimDef( $strDueDate, -1, NULL, true ) );
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function sqlDueDate() {
		return ( true == isset( $this->m_strDueDate ) ) ? '\'' . $this->m_strDueDate . '\'' : 'NULL';
	}

	public function setStartDueDate( $strStartDueDate ) {
		$this->set( 'm_strStartDueDate', CStrings::strTrimDef( $strStartDueDate, -1, NULL, true ) );
	}

	public function getStartDueDate() {
		return $this->m_strStartDueDate;
	}

	public function sqlStartDueDate() {
		return ( true == isset( $this->m_strStartDueDate ) ) ? '\'' . $this->m_strStartDueDate . '\'' : 'NULL';
	}

	public function setEndDueDate( $strEndDueDate ) {
		$this->set( 'm_strEndDueDate', CStrings::strTrimDef( $strEndDueDate, -1, NULL, true ) );
	}

	public function getEndDueDate() {
		return $this->m_strEndDueDate;
	}

	public function sqlEndDueDate() {
		return ( true == isset( $this->m_strEndDueDate ) ) ? '\'' . $this->m_strEndDueDate . '\'' : 'NULL';
	}

	public function setKeywordSearch( $strKeywordSearch ) {
		$this->set( 'm_strKeywordSearch', CStrings::strTrimDef( $strKeywordSearch, 2000, NULL, true ) );
	}

	public function getKeywordSearch() {
		return $this->m_strKeywordSearch;
	}

	public function sqlKeywordSearch() {
		return ( true == isset( $this->m_strKeywordSearch ) ) ? '\'' . addslashes( $this->m_strKeywordSearch ) . '\'' : 'NULL';
	}

	public function setOrderByField( $strOrderByField ) {
		$this->set( 'm_strOrderByField', CStrings::strTrimDef( $strOrderByField, 2000, NULL, true ) );
	}

	public function getOrderByField() {
		return $this->m_strOrderByField;
	}

	public function sqlOrderByField() {
		return ( true == isset( $this->m_strOrderByField ) ) ? '\'' . addslashes( $this->m_strOrderByField ) . '\'' : 'NULL';
	}

	public function setOrderByType( $strOrderByType ) {
		$this->set( 'm_strOrderByType', CStrings::strTrimDef( $strOrderByType, 2000, NULL, true ) );
	}

	public function getOrderByType() {
		return $this->m_strOrderByType;
	}

	public function sqlOrderByType() {
		return ( true == isset( $this->m_strOrderByType ) ) ? '\'' . addslashes( $this->m_strOrderByType ) . '\'' : 'NULL';
	}

	public function setSerializedFilter( $strSerializedFilter ) {
		$this->set( 'm_strSerializedFilter', CStrings::strTrimDef( $strSerializedFilter, -1, NULL, true ) );
	}

	public function getSerializedFilter() {
		return $this->m_strSerializedFilter;
	}

	public function sqlSerializedFilter() {
		return ( true == isset( $this->m_strSerializedFilter ) ) ? '\'' . addslashes( $this->m_strSerializedFilter ) . '\'' : 'NULL';
	}

	public function setSinceAssignedTo( $intSinceAssignedTo ) {
		$this->set( 'm_intSinceAssignedTo', CStrings::strToIntDef( $intSinceAssignedTo, NULL, false ) );
	}

	public function getSinceAssignedTo() {
		return $this->m_intSinceAssignedTo;
	}

	public function sqlSinceAssignedTo() {
		return ( true == isset( $this->m_intSinceAssignedTo ) ) ? ( string ) $this->m_intSinceAssignedTo : 'NULL';
	}

	public function setSinceNotUpdated( $intSinceNotUpdated ) {
		$this->set( 'm_intSinceNotUpdated', CStrings::strToIntDef( $intSinceNotUpdated, NULL, false ) );
	}

	public function getSinceNotUpdated() {
		return $this->m_intSinceNotUpdated;
	}

	public function sqlSinceNotUpdated() {
		return ( true == isset( $this->m_intSinceNotUpdated ) ) ? ( string ) $this->m_intSinceNotUpdated : 'NULL';
	}

	public function setSinceLastUpdated( $intSinceLastUpdated ) {
		$this->set( 'm_intSinceLastUpdated', CStrings::strToIntDef( $intSinceLastUpdated, NULL, false ) );
	}

	public function getSinceLastUpdated() {
		return $this->m_intSinceLastUpdated;
	}

	public function sqlSinceLastUpdated() {
		return ( true == isset( $this->m_intSinceLastUpdated ) ) ? ( string ) $this->m_intSinceLastUpdated : 'NULL';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : 'NULL';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : 'NULL';
	}

	public function setIsQaApproved( $intIsQaApproved ) {
		$this->set( 'm_intIsQaApproved', CStrings::strToIntDef( $intIsQaApproved, NULL, false ) );
	}

	public function getIsQaApproved() {
		return $this->m_intIsQaApproved;
	}

	public function sqlIsQaApproved() {
		return ( true == isset( $this->m_intIsQaApproved ) ) ? ( string ) $this->m_intIsQaApproved : 'NULL';
	}

	public function setAmIStakeholder( $intAmIStakeholder ) {
		$this->set( 'm_intAmIStakeholder', CStrings::strToIntDef( $intAmIStakeholder, NULL, false ) );
	}

	public function getAmIStakeholder() {
		return $this->m_intAmIStakeholder;
	}

	public function sqlAmIStakeholder() {
		return ( true == isset( $this->m_intAmIStakeholder ) ) ? ( string ) $this->m_intAmIStakeholder : 'NULL';
	}

	public function setHasNoParentTask( $intHasNoParentTask ) {
		$this->set( 'm_intHasNoParentTask', CStrings::strToIntDef( $intHasNoParentTask, NULL, false ) );
	}

	public function getHasNoParentTask() {
		return $this->m_intHasNoParentTask;
	}

	public function sqlHasNoParentTask() {
		return ( true == isset( $this->m_intHasNoParentTask ) ) ? ( string ) $this->m_intHasNoParentTask : 'NULL';
	}

	public function setTaskQuarters( $strTaskQuarters ) {
		$this->set( 'm_strTaskQuarters', CStrings::strTrimDef( $strTaskQuarters, -1, NULL, true ) );
	}

	public function getTaskQuarters() {
		return $this->m_strTaskQuarters;
	}

	public function sqlTaskQuarters() {
		return ( true == isset( $this->m_strTaskQuarters ) ) ? '\'' . addslashes( $this->m_strTaskQuarters ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, user_id, title, description, clients, project_managers, users, created_by_users, client_resolution_employees, departments, task_types, task_statuses, task_priorities, task_releases, pdm_task_releases, task_servers, websites, properties, ps_products, ps_product_options, ps_product_modules, ps_product_versions, start_date, end_date, due_date, start_due_date, end_due_date, keyword_search, order_by_field, order_by_type, serialized_filter, since_assigned_to, since_not_updated, since_last_updated, is_system, is_disabled, is_qa_approved, am_i_stakeholder, has_no_parent_task, task_quarters, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlUserId() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlClients() . ', ' .
 						$this->sqlProjectManagers() . ', ' .
 						$this->sqlUsers() . ', ' .
 						$this->sqlCreatedByUsers() . ', ' .
 						$this->sqlClientResolutionEmployees() . ', ' .
 						$this->sqlDepartments() . ', ' .
 						$this->sqlTaskTypes() . ', ' .
 						$this->sqlTaskStatuses() . ', ' .
 						$this->sqlTaskPriorities() . ', ' .
 						$this->sqlTaskReleases() . ', ' .
 						$this->sqlPdmTaskReleases() . ', ' .
 						$this->sqlTaskServers() . ', ' .
 						$this->sqlWebsites() . ', ' .
 						$this->sqlProperties() . ', ' .
 						$this->sqlPsProducts() . ', ' .
 						$this->sqlPsProductOptions() . ', ' .
 						$this->sqlPsProductModules() . ', ' .
 						$this->sqlPsProductVersions() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
 						$this->sqlDueDate() . ', ' .
 						$this->sqlStartDueDate() . ', ' .
 						$this->sqlEndDueDate() . ', ' .
 						$this->sqlKeywordSearch() . ', ' .
 						$this->sqlOrderByField() . ', ' .
 						$this->sqlOrderByType() . ', ' .
 						$this->sqlSerializedFilter() . ', ' .
 						$this->sqlSinceAssignedTo() . ', ' .
 						$this->sqlSinceNotUpdated() . ', ' .
 						$this->sqlSinceLastUpdated() . ', ' .
 						$this->sqlIsSystem() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
 						$this->sqlIsQaApproved() . ', ' .
 						$this->sqlAmIStakeholder() . ', ' .
 						$this->sqlHasNoParentTask() . ', ' .
 						$this->sqlTaskQuarters() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' clients = ' . $this->sqlClients() . ','; } elseif( true == array_key_exists( 'Clients', $this->getChangedColumns() ) ) { $strSql .= ' clients = ' . $this->sqlClients() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_managers = ' . $this->sqlProjectManagers() . ','; } elseif( true == array_key_exists( 'ProjectManagers', $this->getChangedColumns() ) ) { $strSql .= ' project_managers = ' . $this->sqlProjectManagers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' users = ' . $this->sqlUsers() . ','; } elseif( true == array_key_exists( 'Users', $this->getChangedColumns() ) ) { $strSql .= ' users = ' . $this->sqlUsers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' created_by_users = ' . $this->sqlCreatedByUsers() . ','; } elseif( true == array_key_exists( 'CreatedByUsers', $this->getChangedColumns() ) ) { $strSql .= ' created_by_users = ' . $this->sqlCreatedByUsers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_resolution_employees = ' . $this->sqlClientResolutionEmployees() . ','; } elseif( true == array_key_exists( 'ClientResolutionEmployees', $this->getChangedColumns() ) ) { $strSql .= ' client_resolution_employees = ' . $this->sqlClientResolutionEmployees() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' departments = ' . $this->sqlDepartments() . ','; } elseif( true == array_key_exists( 'Departments', $this->getChangedColumns() ) ) { $strSql .= ' departments = ' . $this->sqlDepartments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_types = ' . $this->sqlTaskTypes() . ','; } elseif( true == array_key_exists( 'TaskTypes', $this->getChangedColumns() ) ) { $strSql .= ' task_types = ' . $this->sqlTaskTypes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_statuses = ' . $this->sqlTaskStatuses() . ','; } elseif( true == array_key_exists( 'TaskStatuses', $this->getChangedColumns() ) ) { $strSql .= ' task_statuses = ' . $this->sqlTaskStatuses() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_priorities = ' . $this->sqlTaskPriorities() . ','; } elseif( true == array_key_exists( 'TaskPriorities', $this->getChangedColumns() ) ) { $strSql .= ' task_priorities = ' . $this->sqlTaskPriorities() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_releases = ' . $this->sqlTaskReleases() . ','; } elseif( true == array_key_exists( 'TaskReleases', $this->getChangedColumns() ) ) { $strSql .= ' task_releases = ' . $this->sqlTaskReleases() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pdm_task_releases = ' . $this->sqlPdmTaskReleases() . ','; } elseif( true == array_key_exists( 'PdmTaskReleases', $this->getChangedColumns() ) ) { $strSql .= ' pdm_task_releases = ' . $this->sqlPdmTaskReleases() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_servers = ' . $this->sqlTaskServers() . ','; } elseif( true == array_key_exists( 'TaskServers', $this->getChangedColumns() ) ) { $strSql .= ' task_servers = ' . $this->sqlTaskServers() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' websites = ' . $this->sqlWebsites() . ','; } elseif( true == array_key_exists( 'Websites', $this->getChangedColumns() ) ) { $strSql .= ' websites = ' . $this->sqlWebsites() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' properties = ' . $this->sqlProperties() . ','; } elseif( true == array_key_exists( 'Properties', $this->getChangedColumns() ) ) { $strSql .= ' properties = ' . $this->sqlProperties() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_products = ' . $this->sqlPsProducts() . ','; } elseif( true == array_key_exists( 'PsProducts', $this->getChangedColumns() ) ) { $strSql .= ' ps_products = ' . $this->sqlPsProducts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_options = ' . $this->sqlPsProductOptions() . ','; } elseif( true == array_key_exists( 'PsProductOptions', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_options = ' . $this->sqlPsProductOptions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_modules = ' . $this->sqlPsProductModules() . ','; } elseif( true == array_key_exists( 'PsProductModules', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_modules = ' . $this->sqlPsProductModules() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_versions = ' . $this->sqlPsProductVersions() . ','; } elseif( true == array_key_exists( 'PsProductVersions', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_versions = ' . $this->sqlPsProductVersions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date = ' . $this->sqlDueDate() . ','; } elseif( true == array_key_exists( 'DueDate', $this->getChangedColumns() ) ) { $strSql .= ' due_date = ' . $this->sqlDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_due_date = ' . $this->sqlStartDueDate() . ','; } elseif( true == array_key_exists( 'StartDueDate', $this->getChangedColumns() ) ) { $strSql .= ' start_due_date = ' . $this->sqlStartDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_due_date = ' . $this->sqlEndDueDate() . ','; } elseif( true == array_key_exists( 'EndDueDate', $this->getChangedColumns() ) ) { $strSql .= ' end_due_date = ' . $this->sqlEndDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keyword_search = ' . $this->sqlKeywordSearch() . ','; } elseif( true == array_key_exists( 'KeywordSearch', $this->getChangedColumns() ) ) { $strSql .= ' keyword_search = ' . $this->sqlKeywordSearch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_by_field = ' . $this->sqlOrderByField() . ','; } elseif( true == array_key_exists( 'OrderByField', $this->getChangedColumns() ) ) { $strSql .= ' order_by_field = ' . $this->sqlOrderByField() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_by_type = ' . $this->sqlOrderByType() . ','; } elseif( true == array_key_exists( 'OrderByType', $this->getChangedColumns() ) ) { $strSql .= ' order_by_type = ' . $this->sqlOrderByType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' serialized_filter = ' . $this->sqlSerializedFilter() . ','; } elseif( true == array_key_exists( 'SerializedFilter', $this->getChangedColumns() ) ) { $strSql .= ' serialized_filter = ' . $this->sqlSerializedFilter() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' since_assigned_to = ' . $this->sqlSinceAssignedTo() . ','; } elseif( true == array_key_exists( 'SinceAssignedTo', $this->getChangedColumns() ) ) { $strSql .= ' since_assigned_to = ' . $this->sqlSinceAssignedTo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' since_not_updated = ' . $this->sqlSinceNotUpdated() . ','; } elseif( true == array_key_exists( 'SinceNotUpdated', $this->getChangedColumns() ) ) { $strSql .= ' since_not_updated = ' . $this->sqlSinceNotUpdated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' since_last_updated = ' . $this->sqlSinceLastUpdated() . ','; } elseif( true == array_key_exists( 'SinceLastUpdated', $this->getChangedColumns() ) ) { $strSql .= ' since_last_updated = ' . $this->sqlSinceLastUpdated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_qa_approved = ' . $this->sqlIsQaApproved() . ','; } elseif( true == array_key_exists( 'IsQaApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_qa_approved = ' . $this->sqlIsQaApproved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' am_i_stakeholder = ' . $this->sqlAmIStakeholder() . ','; } elseif( true == array_key_exists( 'AmIStakeholder', $this->getChangedColumns() ) ) { $strSql .= ' am_i_stakeholder = ' . $this->sqlAmIStakeholder() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_no_parent_task = ' . $this->sqlHasNoParentTask() . ','; } elseif( true == array_key_exists( 'HasNoParentTask', $this->getChangedColumns() ) ) { $strSql .= ' has_no_parent_task = ' . $this->sqlHasNoParentTask() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_quarters = ' . $this->sqlTaskQuarters() . ','; } elseif( true == array_key_exists( 'TaskQuarters', $this->getChangedColumns() ) ) { $strSql .= ' task_quarters = ' . $this->sqlTaskQuarters() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'user_id' => $this->getUserId(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'clients' => $this->getClients(),
			'project_managers' => $this->getProjectManagers(),
			'users' => $this->getUsers(),
			'created_by_users' => $this->getCreatedByUsers(),
			'client_resolution_employees' => $this->getClientResolutionEmployees(),
			'departments' => $this->getDepartments(),
			'task_types' => $this->getTaskTypes(),
			'task_statuses' => $this->getTaskStatuses(),
			'task_priorities' => $this->getTaskPriorities(),
			'task_releases' => $this->getTaskReleases(),
			'pdm_task_releases' => $this->getPdmTaskReleases(),
			'task_servers' => $this->getTaskServers(),
			'websites' => $this->getWebsites(),
			'properties' => $this->getProperties(),
			'ps_products' => $this->getPsProducts(),
			'ps_product_options' => $this->getPsProductOptions(),
			'ps_product_modules' => $this->getPsProductModules(),
			'ps_product_versions' => $this->getPsProductVersions(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'due_date' => $this->getDueDate(),
			'start_due_date' => $this->getStartDueDate(),
			'end_due_date' => $this->getEndDueDate(),
			'keyword_search' => $this->getKeywordSearch(),
			'order_by_field' => $this->getOrderByField(),
			'order_by_type' => $this->getOrderByType(),
			'serialized_filter' => $this->getSerializedFilter(),
			'since_assigned_to' => $this->getSinceAssignedTo(),
			'since_not_updated' => $this->getSinceNotUpdated(),
			'since_last_updated' => $this->getSinceLastUpdated(),
			'is_system' => $this->getIsSystem(),
			'is_disabled' => $this->getIsDisabled(),
			'is_qa_approved' => $this->getIsQaApproved(),
			'am_i_stakeholder' => $this->getAmIStakeholder(),
			'has_no_parent_task' => $this->getHasNoParentTask(),
			'task_quarters' => $this->getTaskQuarters(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>