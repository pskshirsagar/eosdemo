<?php

class CBaseEmployeeInteraction extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_interactions';

	protected $m_intId;
	protected $m_intEmployeeInteractionTypeId;
	protected $m_intEmployeeId;
	protected $m_intCallAgentId;
	protected $m_intCallId;
	protected $m_strNotes;
	protected $m_strRemarks;
	protected $m_intScheduledBy;
	protected $m_strScheduledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strRemarks = NULL;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_interaction_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeInteractionTypeId', trim( $arrValues['employee_interaction_type_id'] ) ); elseif( isset( $arrValues['employee_interaction_type_id'] ) ) $this->setEmployeeInteractionTypeId( $arrValues['employee_interaction_type_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['call_agent_id'] ) && $boolDirectSet ) $this->set( 'm_intCallAgentId', trim( $arrValues['call_agent_id'] ) ); elseif( isset( $arrValues['call_agent_id'] ) ) $this->setCallAgentId( $arrValues['call_agent_id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['remarks'] ) && $boolDirectSet ) $this->set( 'm_strRemarks', trim( stripcslashes( $arrValues['remarks'] ) ) ); elseif( isset( $arrValues['remarks'] ) ) $this->setRemarks( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remarks'] ) : $arrValues['remarks'] );
		if( isset( $arrValues['scheduled_by'] ) && $boolDirectSet ) $this->set( 'm_intScheduledBy', trim( $arrValues['scheduled_by'] ) ); elseif( isset( $arrValues['scheduled_by'] ) ) $this->setScheduledBy( $arrValues['scheduled_by'] );
		if( isset( $arrValues['scheduled_on'] ) && $boolDirectSet ) $this->set( 'm_strScheduledOn', trim( $arrValues['scheduled_on'] ) ); elseif( isset( $arrValues['scheduled_on'] ) ) $this->setScheduledOn( $arrValues['scheduled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeInteractionTypeId( $intEmployeeInteractionTypeId ) {
		$this->set( 'm_intEmployeeInteractionTypeId', CStrings::strToIntDef( $intEmployeeInteractionTypeId, NULL, false ) );
	}

	public function getEmployeeInteractionTypeId() {
		return $this->m_intEmployeeInteractionTypeId;
	}

	public function sqlEmployeeInteractionTypeId() {
		return ( true == isset( $this->m_intEmployeeInteractionTypeId ) ) ? ( string ) $this->m_intEmployeeInteractionTypeId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setCallAgentId( $intCallAgentId ) {
		$this->set( 'm_intCallAgentId', CStrings::strToIntDef( $intCallAgentId, NULL, false ) );
	}

	public function getCallAgentId() {
		return $this->m_intCallAgentId;
	}

	public function sqlCallAgentId() {
		return ( true == isset( $this->m_intCallAgentId ) ) ? ( string ) $this->m_intCallAgentId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setRemarks( $strRemarks ) {
		$this->set( 'm_strRemarks', CStrings::strTrimDef( $strRemarks, -1, NULL, true ) );
	}

	public function getRemarks() {
		return $this->m_strRemarks;
	}

	public function sqlRemarks() {
		return ( true == isset( $this->m_strRemarks ) ) ? '\'' . addslashes( $this->m_strRemarks ) . '\'' : 'NULL';
	}

	public function setScheduledBy( $intScheduledBy ) {
		$this->set( 'm_intScheduledBy', CStrings::strToIntDef( $intScheduledBy, NULL, false ) );
	}

	public function getScheduledBy() {
		return $this->m_intScheduledBy;
	}

	public function sqlScheduledBy() {
		return ( true == isset( $this->m_intScheduledBy ) ) ? ( string ) $this->m_intScheduledBy : 'NULL';
	}

	public function setScheduledOn( $strScheduledOn ) {
		$this->set( 'm_strScheduledOn', CStrings::strTrimDef( $strScheduledOn, -1, NULL, true ) );
	}

	public function getScheduledOn() {
		return $this->m_strScheduledOn;
	}

	public function sqlScheduledOn() {
		return ( true == isset( $this->m_strScheduledOn ) ) ? '\'' . $this->m_strScheduledOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_interaction_type_id, employee_id, call_agent_id, call_id, notes, remarks, scheduled_by, scheduled_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeInteractionTypeId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlCallAgentId() . ', ' .
 						$this->sqlCallId() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlRemarks() . ', ' .
 						$this->sqlScheduledBy() . ', ' .
 						$this->sqlScheduledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_interaction_type_id = ' . $this->sqlEmployeeInteractionTypeId() . ','; } elseif( true == array_key_exists( 'EmployeeInteractionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_interaction_type_id = ' . $this->sqlEmployeeInteractionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; } elseif( true == array_key_exists( 'CallAgentId', $this->getChangedColumns() ) ) { $strSql .= ' call_agent_id = ' . $this->sqlCallAgentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remarks = ' . $this->sqlRemarks() . ','; } elseif( true == array_key_exists( 'Remarks', $this->getChangedColumns() ) ) { $strSql .= ' remarks = ' . $this->sqlRemarks() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_by = ' . $this->sqlScheduledBy() . ','; } elseif( true == array_key_exists( 'ScheduledBy', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_by = ' . $this->sqlScheduledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn() . ','; } elseif( true == array_key_exists( 'ScheduledOn', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_on = ' . $this->sqlScheduledOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_interaction_type_id' => $this->getEmployeeInteractionTypeId(),
			'employee_id' => $this->getEmployeeId(),
			'call_agent_id' => $this->getCallAgentId(),
			'call_id' => $this->getCallId(),
			'notes' => $this->getNotes(),
			'remarks' => $this->getRemarks(),
			'scheduled_by' => $this->getScheduledBy(),
			'scheduled_on' => $this->getScheduledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>