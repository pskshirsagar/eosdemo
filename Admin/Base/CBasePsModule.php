<?php

class CBasePsModule extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_modules';

	protected $m_intId;
	protected $m_intPsModuleId;
	protected $m_strModuleName;
	protected $m_strActionName;
	protected $m_strUrl;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_intIsPublic;
	protected $m_intIsPublished;
	protected $m_intIsNavigation;
	protected $m_boolIsAllCloud;
	protected $m_intOpenInNewTab;
	protected $m_intBlockExternalAccess;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublic = '0';
		$this->m_intIsPublished = '1';
		$this->m_intIsNavigation = '0';
		$this->m_boolIsAllCloud = false;
		$this->m_intOpenInNewTab = '0';
		$this->m_intBlockExternalAccess = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_module_id'] ) && $boolDirectSet ) $this->set( 'm_intPsModuleId', trim( $arrValues['ps_module_id'] ) ); elseif( isset( $arrValues['ps_module_id'] ) ) $this->setPsModuleId( $arrValues['ps_module_id'] );
		if( isset( $arrValues['module_name'] ) && $boolDirectSet ) $this->set( 'm_strModuleName', trim( stripcslashes( $arrValues['module_name'] ) ) ); elseif( isset( $arrValues['module_name'] ) ) $this->setModuleName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['module_name'] ) : $arrValues['module_name'] );
		if( isset( $arrValues['action_name'] ) && $boolDirectSet ) $this->set( 'm_strActionName', trim( stripcslashes( $arrValues['action_name'] ) ) ); elseif( isset( $arrValues['action_name'] ) ) $this->setActionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_name'] ) : $arrValues['action_name'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( stripcslashes( $arrValues['url'] ) ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['url'] ) : $arrValues['url'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_public'] ) && $boolDirectSet ) $this->set( 'm_intIsPublic', trim( $arrValues['is_public'] ) ); elseif( isset( $arrValues['is_public'] ) ) $this->setIsPublic( $arrValues['is_public'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_navigation'] ) && $boolDirectSet ) $this->set( 'm_intIsNavigation', trim( $arrValues['is_navigation'] ) ); elseif( isset( $arrValues['is_navigation'] ) ) $this->setIsNavigation( $arrValues['is_navigation'] );
		if( isset( $arrValues['is_all_cloud'] ) && $boolDirectSet ) $this->set( 'm_boolIsAllCloud', trim( stripcslashes( $arrValues['is_all_cloud'] ) ) ); elseif( isset( $arrValues['is_all_cloud'] ) ) $this->setIsAllCloud( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_all_cloud'] ) : $arrValues['is_all_cloud'] );
		if( isset( $arrValues['open_in_new_tab'] ) && $boolDirectSet ) $this->set( 'm_intOpenInNewTab', trim( $arrValues['open_in_new_tab'] ) ); elseif( isset( $arrValues['open_in_new_tab'] ) ) $this->setOpenInNewTab( $arrValues['open_in_new_tab'] );
		if( isset( $arrValues['block_external_access'] ) && $boolDirectSet ) $this->set( 'm_intBlockExternalAccess', trim( $arrValues['block_external_access'] ) ); elseif( isset( $arrValues['block_external_access'] ) ) $this->setBlockExternalAccess( $arrValues['block_external_access'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsModuleId( $intPsModuleId ) {
		$this->set( 'm_intPsModuleId', CStrings::strToIntDef( $intPsModuleId, NULL, false ) );
	}

	public function getPsModuleId() {
		return $this->m_intPsModuleId;
	}

	public function sqlPsModuleId() {
		return ( true == isset( $this->m_intPsModuleId ) ) ? ( string ) $this->m_intPsModuleId : 'NULL';
	}

	public function setModuleName( $strModuleName ) {
		$this->set( 'm_strModuleName', CStrings::strTrimDef( $strModuleName, 50, NULL, true ) );
	}

	public function getModuleName() {
		return $this->m_strModuleName;
	}

	public function sqlModuleName() {
		return ( true == isset( $this->m_strModuleName ) ) ? '\'' . addslashes( $this->m_strModuleName ) . '\'' : 'NULL';
	}

	public function setActionName( $strActionName ) {
		$this->set( 'm_strActionName', CStrings::strTrimDef( $strActionName, 150, NULL, true ) );
	}

	public function getActionName() {
		return $this->m_strActionName;
	}

	public function sqlActionName() {
		return ( true == isset( $this->m_strActionName ) ) ? '\'' . addslashes( $this->m_strActionName ) . '\'' : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, 1024, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? '\'' . addslashes( $this->m_strUrl ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 255, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsPublic( $intIsPublic ) {
		$this->set( 'm_intIsPublic', CStrings::strToIntDef( $intIsPublic, NULL, false ) );
	}

	public function getIsPublic() {
		return $this->m_intIsPublic;
	}

	public function sqlIsPublic() {
		return ( true == isset( $this->m_intIsPublic ) ) ? ( string ) $this->m_intIsPublic : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsNavigation( $intIsNavigation ) {
		$this->set( 'm_intIsNavigation', CStrings::strToIntDef( $intIsNavigation, NULL, false ) );
	}

	public function getIsNavigation() {
		return $this->m_intIsNavigation;
	}

	public function sqlIsNavigation() {
		return ( true == isset( $this->m_intIsNavigation ) ) ? ( string ) $this->m_intIsNavigation : '0';
	}

	public function setIsAllCloud( $boolIsAllCloud ) {
		$this->set( 'm_boolIsAllCloud', CStrings::strToBool( $boolIsAllCloud ) );
	}

	public function getIsAllCloud() {
		return $this->m_boolIsAllCloud;
	}

	public function sqlIsAllCloud() {
		return ( true == isset( $this->m_boolIsAllCloud ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsAllCloud ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOpenInNewTab( $intOpenInNewTab ) {
		$this->set( 'm_intOpenInNewTab', CStrings::strToIntDef( $intOpenInNewTab, NULL, false ) );
	}

	public function getOpenInNewTab() {
		return $this->m_intOpenInNewTab;
	}

	public function sqlOpenInNewTab() {
		return ( true == isset( $this->m_intOpenInNewTab ) ) ? ( string ) $this->m_intOpenInNewTab : '0';
	}

	public function setBlockExternalAccess( $intBlockExternalAccess ) {
		$this->set( 'm_intBlockExternalAccess', CStrings::strToIntDef( $intBlockExternalAccess, NULL, false ) );
	}

	public function getBlockExternalAccess() {
		return $this->m_intBlockExternalAccess;
	}

	public function sqlBlockExternalAccess() {
		return ( true == isset( $this->m_intBlockExternalAccess ) ) ? ( string ) $this->m_intBlockExternalAccess : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_module_id, module_name, action_name, url, title, description, is_public, is_published, is_navigation, is_all_cloud, open_in_new_tab, block_external_access, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPsModuleId() . ', ' .
						$this->sqlModuleName() . ', ' .
						$this->sqlActionName() . ', ' .
						$this->sqlUrl() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlIsPublic() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsNavigation() . ', ' .
						$this->sqlIsAllCloud() . ', ' .
						$this->sqlOpenInNewTab() . ', ' .
						$this->sqlBlockExternalAccess() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_module_id = ' . $this->sqlPsModuleId(). ',' ; } elseif( true == array_key_exists( 'PsModuleId', $this->getChangedColumns() ) ) { $strSql .= ' ps_module_id = ' . $this->sqlPsModuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' module_name = ' . $this->sqlModuleName(). ',' ; } elseif( true == array_key_exists( 'ModuleName', $this->getChangedColumns() ) ) { $strSql .= ' module_name = ' . $this->sqlModuleName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_name = ' . $this->sqlActionName(). ',' ; } elseif( true == array_key_exists( 'ActionName', $this->getChangedColumns() ) ) { $strSql .= ' action_name = ' . $this->sqlActionName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl(). ',' ; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_public = ' . $this->sqlIsPublic(). ',' ; } elseif( true == array_key_exists( 'IsPublic', $this->getChangedColumns() ) ) { $strSql .= ' is_public = ' . $this->sqlIsPublic() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_navigation = ' . $this->sqlIsNavigation(). ',' ; } elseif( true == array_key_exists( 'IsNavigation', $this->getChangedColumns() ) ) { $strSql .= ' is_navigation = ' . $this->sqlIsNavigation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_all_cloud = ' . $this->sqlIsAllCloud(). ',' ; } elseif( true == array_key_exists( 'IsAllCloud', $this->getChangedColumns() ) ) { $strSql .= ' is_all_cloud = ' . $this->sqlIsAllCloud() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' open_in_new_tab = ' . $this->sqlOpenInNewTab(). ',' ; } elseif( true == array_key_exists( 'OpenInNewTab', $this->getChangedColumns() ) ) { $strSql .= ' open_in_new_tab = ' . $this->sqlOpenInNewTab() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_external_access = ' . $this->sqlBlockExternalAccess(). ',' ; } elseif( true == array_key_exists( 'BlockExternalAccess', $this->getChangedColumns() ) ) { $strSql .= ' block_external_access = ' . $this->sqlBlockExternalAccess() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_module_id' => $this->getPsModuleId(),
			'module_name' => $this->getModuleName(),
			'action_name' => $this->getActionName(),
			'url' => $this->getUrl(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'is_public' => $this->getIsPublic(),
			'is_published' => $this->getIsPublished(),
			'is_navigation' => $this->getIsNavigation(),
			'is_all_cloud' => $this->getIsAllCloud(),
			'open_in_new_tab' => $this->getOpenInNewTab(),
			'block_external_access' => $this->getBlockExternalAccess(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>