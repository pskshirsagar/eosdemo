<?php

class CBaseInvoiceBatchDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.invoice_batch_details';

	protected $m_intId;
	protected $m_intInvoiceBatchId;
	protected $m_strCurrencyCode;
	protected $m_fltTrack1BilledAmount;
	protected $m_fltTrack2BilledAmount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';
		$this->m_fltTrack1BilledAmount = '0';
		$this->m_fltTrack2BilledAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['invoice_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceBatchId', trim( $arrValues['invoice_batch_id'] ) ); elseif( isset( $arrValues['invoice_batch_id'] ) ) $this->setInvoiceBatchId( $arrValues['invoice_batch_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		if( isset( $arrValues['track1_billed_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTrack1BilledAmount', trim( $arrValues['track1_billed_amount'] ) ); elseif( isset( $arrValues['track1_billed_amount'] ) ) $this->setTrack1BilledAmount( $arrValues['track1_billed_amount'] );
		if( isset( $arrValues['track2_billed_amount'] ) && $boolDirectSet ) $this->set( 'm_fltTrack2BilledAmount', trim( $arrValues['track2_billed_amount'] ) ); elseif( isset( $arrValues['track2_billed_amount'] ) ) $this->setTrack2BilledAmount( $arrValues['track2_billed_amount'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setInvoiceBatchId( $intInvoiceBatchId ) {
		$this->set( 'm_intInvoiceBatchId', CStrings::strToIntDef( $intInvoiceBatchId, NULL, false ) );
	}

	public function getInvoiceBatchId() {
		return $this->m_intInvoiceBatchId;
	}

	public function sqlInvoiceBatchId() {
		return ( true == isset( $this->m_intInvoiceBatchId ) ) ? ( string ) $this->m_intInvoiceBatchId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : '\'USD\'';
	}

	public function setTrack1BilledAmount( $fltTrack1BilledAmount ) {
		$this->set( 'm_fltTrack1BilledAmount', CStrings::strToFloatDef( $fltTrack1BilledAmount, NULL, false, 2 ) );
	}

	public function getTrack1BilledAmount() {
		return $this->m_fltTrack1BilledAmount;
	}

	public function sqlTrack1BilledAmount() {
		return ( true == isset( $this->m_fltTrack1BilledAmount ) ) ? ( string ) $this->m_fltTrack1BilledAmount : '0';
	}

	public function setTrack2BilledAmount( $fltTrack2BilledAmount ) {
		$this->set( 'm_fltTrack2BilledAmount', CStrings::strToFloatDef( $fltTrack2BilledAmount, NULL, false, 2 ) );
	}

	public function getTrack2BilledAmount() {
		return $this->m_fltTrack2BilledAmount;
	}

	public function sqlTrack2BilledAmount() {
		return ( true == isset( $this->m_fltTrack2BilledAmount ) ) ? ( string ) $this->m_fltTrack2BilledAmount : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, invoice_batch_id, currency_code, track1_billed_amount, track2_billed_amount, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlInvoiceBatchId() . ', ' .
 						$this->sqlCurrencyCode() . ', ' .
 						$this->sqlTrack1BilledAmount() . ', ' .
 						$this->sqlTrack2BilledAmount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_batch_id = ' . $this->sqlInvoiceBatchId() . ','; } elseif( true == array_key_exists( 'InvoiceBatchId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_batch_id = ' . $this->sqlInvoiceBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track1_billed_amount = ' . $this->sqlTrack1BilledAmount() . ','; } elseif( true == array_key_exists( 'Track1BilledAmount', $this->getChangedColumns() ) ) { $strSql .= ' track1_billed_amount = ' . $this->sqlTrack1BilledAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' track2_billed_amount = ' . $this->sqlTrack2BilledAmount() . ','; } elseif( true == array_key_exists( 'Track2BilledAmount', $this->getChangedColumns() ) ) { $strSql .= ' track2_billed_amount = ' . $this->sqlTrack2BilledAmount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'invoice_batch_id' => $this->getInvoiceBatchId(),
			'currency_code' => $this->getCurrencyCode(),
			'track1_billed_amount' => $this->getTrack1BilledAmount(),
			'track2_billed_amount' => $this->getTrack2BilledAmount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>