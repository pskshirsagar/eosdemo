<?php

class CBaseTaskCompany extends CEosSingularBase {

	const TABLE_NAME = 'public.task_companies';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTaskId;
	protected $m_intUserId;
	protected $m_intTaskNoteId;
	protected $m_strPostDatetime;
	protected $m_strCompletionNotifiedOn;
	protected $m_intIsPublished;
	protected $m_strLatestUpdate;
	protected $m_intResolvedBy;
	protected $m_strResolvedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['task_note_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskNoteId', trim( $arrValues['task_note_id'] ) ); elseif( isset( $arrValues['task_note_id'] ) ) $this->setTaskNoteId( $arrValues['task_note_id'] );
		if( isset( $arrValues['post_datetime'] ) && $boolDirectSet ) $this->set( 'm_strPostDatetime', trim( $arrValues['post_datetime'] ) ); elseif( isset( $arrValues['post_datetime'] ) ) $this->setPostDatetime( $arrValues['post_datetime'] );
		if( isset( $arrValues['completion_notified_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletionNotifiedOn', trim( $arrValues['completion_notified_on'] ) ); elseif( isset( $arrValues['completion_notified_on'] ) ) $this->setCompletionNotifiedOn( $arrValues['completion_notified_on'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['latest_update'] ) && $boolDirectSet ) $this->set( 'm_strLatestUpdate', trim( stripcslashes( $arrValues['latest_update'] ) ) ); elseif( isset( $arrValues['latest_update'] ) ) $this->setLatestUpdate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['latest_update'] ) : $arrValues['latest_update'] );
		if( isset( $arrValues['resolved_by'] ) && $boolDirectSet ) $this->set( 'm_intResolvedBy', trim( $arrValues['resolved_by'] ) ); elseif( isset( $arrValues['resolved_by'] ) ) $this->setResolvedBy( $arrValues['resolved_by'] );
		if( isset( $arrValues['resolved_on'] ) && $boolDirectSet ) $this->set( 'm_strResolvedOn', trim( $arrValues['resolved_on'] ) ); elseif( isset( $arrValues['resolved_on'] ) ) $this->setResolvedOn( $arrValues['resolved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setTaskNoteId( $intTaskNoteId ) {
		$this->set( 'm_intTaskNoteId', CStrings::strToIntDef( $intTaskNoteId, NULL, false ) );
	}

	public function getTaskNoteId() {
		return $this->m_intTaskNoteId;
	}

	public function sqlTaskNoteId() {
		return ( true == isset( $this->m_intTaskNoteId ) ) ? ( string ) $this->m_intTaskNoteId : 'NULL';
	}

	public function setPostDatetime( $strPostDatetime ) {
		$this->set( 'm_strPostDatetime', CStrings::strTrimDef( $strPostDatetime, -1, NULL, true ) );
	}

	public function getPostDatetime() {
		return $this->m_strPostDatetime;
	}

	public function sqlPostDatetime() {
		return ( true == isset( $this->m_strPostDatetime ) ) ? '\'' . $this->m_strPostDatetime . '\'' : 'NOW()';
	}

	public function setCompletionNotifiedOn( $strCompletionNotifiedOn ) {
		$this->set( 'm_strCompletionNotifiedOn', CStrings::strTrimDef( $strCompletionNotifiedOn, -1, NULL, true ) );
	}

	public function getCompletionNotifiedOn() {
		return $this->m_strCompletionNotifiedOn;
	}

	public function sqlCompletionNotifiedOn() {
		return ( true == isset( $this->m_strCompletionNotifiedOn ) ) ? '\'' . $this->m_strCompletionNotifiedOn . '\'' : 'NOW()';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setLatestUpdate( $strLatestUpdate ) {
		$this->set( 'm_strLatestUpdate', CStrings::strTrimDef( $strLatestUpdate, 256, NULL, true ) );
	}

	public function getLatestUpdate() {
		return $this->m_strLatestUpdate;
	}

	public function sqlLatestUpdate() {
		return ( true == isset( $this->m_strLatestUpdate ) ) ? '\'' . addslashes( $this->m_strLatestUpdate ) . '\'' : 'NULL';
	}

	public function setResolvedBy( $intResolvedBy ) {
		$this->set( 'm_intResolvedBy', CStrings::strToIntDef( $intResolvedBy, NULL, false ) );
	}

	public function getResolvedBy() {
		return $this->m_intResolvedBy;
	}

	public function sqlResolvedBy() {
		return ( true == isset( $this->m_intResolvedBy ) ) ? ( string ) $this->m_intResolvedBy : 'NULL';
	}

	public function setResolvedOn( $strResolvedOn ) {
		$this->set( 'm_strResolvedOn', CStrings::strTrimDef( $strResolvedOn, -1, NULL, true ) );
	}

	public function getResolvedOn() {
		return $this->m_strResolvedOn;
	}

	public function sqlResolvedOn() {
		return ( true == isset( $this->m_strResolvedOn ) ) ? '\'' . $this->m_strResolvedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, task_id, user_id, task_note_id, post_datetime, completion_notified_on, is_published, latest_update, resolved_by, resolved_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlUserId() . ', ' .
 						$this->sqlTaskNoteId() . ', ' .
 						$this->sqlPostDatetime() . ', ' .
 						$this->sqlCompletionNotifiedOn() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlLatestUpdate() . ', ' .
 						$this->sqlResolvedBy() . ', ' .
 						$this->sqlResolvedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_note_id = ' . $this->sqlTaskNoteId() . ','; } elseif( true == array_key_exists( 'TaskNoteId', $this->getChangedColumns() ) ) { $strSql .= ' task_note_id = ' . $this->sqlTaskNoteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_datetime = ' . $this->sqlPostDatetime() . ','; } elseif( true == array_key_exists( 'PostDatetime', $this->getChangedColumns() ) ) { $strSql .= ' post_datetime = ' . $this->sqlPostDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completion_notified_on = ' . $this->sqlCompletionNotifiedOn() . ','; } elseif( true == array_key_exists( 'CompletionNotifiedOn', $this->getChangedColumns() ) ) { $strSql .= ' completion_notified_on = ' . $this->sqlCompletionNotifiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' latest_update = ' . $this->sqlLatestUpdate() . ','; } elseif( true == array_key_exists( 'LatestUpdate', $this->getChangedColumns() ) ) { $strSql .= ' latest_update = ' . $this->sqlLatestUpdate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_by = ' . $this->sqlResolvedBy() . ','; } elseif( true == array_key_exists( 'ResolvedBy', $this->getChangedColumns() ) ) { $strSql .= ' resolved_by = ' . $this->sqlResolvedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn() . ','; } elseif( true == array_key_exists( 'ResolvedOn', $this->getChangedColumns() ) ) { $strSql .= ' resolved_on = ' . $this->sqlResolvedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'task_id' => $this->getTaskId(),
			'user_id' => $this->getUserId(),
			'task_note_id' => $this->getTaskNoteId(),
			'post_datetime' => $this->getPostDatetime(),
			'completion_notified_on' => $this->getCompletionNotifiedOn(),
			'is_published' => $this->getIsPublished(),
			'latest_update' => $this->getLatestUpdate(),
			'resolved_by' => $this->getResolvedBy(),
			'resolved_on' => $this->getResolvedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>