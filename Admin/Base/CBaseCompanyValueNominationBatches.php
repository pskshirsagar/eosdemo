<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyValueNominationBatches
 * Do not add any new functions to this class.
 */

class CBaseCompanyValueNominationBatches extends CEosPluralBase {

	/**
	 * @return CCompanyValueNominationBatch[]
	 */
	public static function fetchCompanyValueNominationBatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyValueNominationBatch::class, $objDatabase );
	}

	/**
	 * @return CCompanyValueNominationBatch
	 */
	public static function fetchCompanyValueNominationBatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyValueNominationBatch::class, $objDatabase );
	}

	public static function fetchCompanyValueNominationBatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_value_nomination_batches', $objDatabase );
	}

	public static function fetchCompanyValueNominationBatchById( $intId, $objDatabase ) {
		return self::fetchCompanyValueNominationBatch( sprintf( 'SELECT * FROM company_value_nomination_batches WHERE id = %d', $intId ), $objDatabase );
	}

}
?>