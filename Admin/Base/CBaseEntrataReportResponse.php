<?php

class CBaseEntrataReportResponse extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.entrata_report_responses';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intCompanyUserId;
	protected $m_intDefaultReportId;
	protected $m_intDefaultReportVersionId;
	protected $m_intReportInstanceId;
	protected $m_intReportHistoryId;
	protected $m_intEntrataReportResponseTypeId;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strEmailAddress;
	protected $m_strReportTitle;
	protected $m_intMajor;
	protected $m_intMinor;
	protected $m_strReportInstanceName;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_strRespondedOn;
	protected $m_strViewedOn;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;

	public function __construct() {
		parent::__construct();

		$this->m_jsonDetails = '{}';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['default_report_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultReportId', trim( $arrValues['default_report_id'] ) ); elseif( isset( $arrValues['default_report_id'] ) ) $this->setDefaultReportId( $arrValues['default_report_id'] );
		if( isset( $arrValues['default_report_version_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultReportVersionId', trim( $arrValues['default_report_version_id'] ) ); elseif( isset( $arrValues['default_report_version_id'] ) ) $this->setDefaultReportVersionId( $arrValues['default_report_version_id'] );
		if( isset( $arrValues['report_instance_id'] ) && $boolDirectSet ) $this->set( 'm_intReportInstanceId', trim( $arrValues['report_instance_id'] ) ); elseif( isset( $arrValues['report_instance_id'] ) ) $this->setReportInstanceId( $arrValues['report_instance_id'] );
		if( isset( $arrValues['report_history_id'] ) && $boolDirectSet ) $this->set( 'm_intReportHistoryId', trim( $arrValues['report_history_id'] ) ); elseif( isset( $arrValues['report_history_id'] ) ) $this->setReportHistoryId( $arrValues['report_history_id'] );
		if( isset( $arrValues['entrata_report_response_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEntrataReportResponseTypeId', trim( $arrValues['entrata_report_response_type_id'] ) ); elseif( isset( $arrValues['entrata_report_response_type_id'] ) ) $this->setEntrataReportResponseTypeId( $arrValues['entrata_report_response_type_id'] );
		if( isset( $arrValues['name_first'] ) && $boolDirectSet ) $this->set( 'm_strNameFirst', trim( $arrValues['name_first'] ) ); elseif( isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( isset( $arrValues['name_last'] ) && $boolDirectSet ) $this->set( 'm_strNameLast', trim( $arrValues['name_last'] ) ); elseif( isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( isset( $arrValues['email_address'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddress', trim( $arrValues['email_address'] ) ); elseif( isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( isset( $arrValues['report_title'] ) && $boolDirectSet ) $this->set( 'm_strReportTitle', trim( $arrValues['report_title'] ) ); elseif( isset( $arrValues['report_title'] ) ) $this->setReportTitle( $arrValues['report_title'] );
		if( isset( $arrValues['major'] ) && $boolDirectSet ) $this->set( 'm_intMajor', trim( $arrValues['major'] ) ); elseif( isset( $arrValues['major'] ) ) $this->setMajor( $arrValues['major'] );
		if( isset( $arrValues['minor'] ) && $boolDirectSet ) $this->set( 'm_intMinor', trim( $arrValues['minor'] ) ); elseif( isset( $arrValues['minor'] ) ) $this->setMinor( $arrValues['minor'] );
		if( isset( $arrValues['report_instance_name'] ) && $boolDirectSet ) $this->set( 'm_strReportInstanceName', trim( $arrValues['report_instance_name'] ) ); elseif( isset( $arrValues['report_instance_name'] ) ) $this->setReportInstanceName( $arrValues['report_instance_name'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['responded_on'] ) && $boolDirectSet ) $this->set( 'm_strRespondedOn', trim( $arrValues['responded_on'] ) ); elseif( isset( $arrValues['responded_on'] ) ) $this->setRespondedOn( $arrValues['responded_on'] );
		if( isset( $arrValues['viewed_on'] ) && $boolDirectSet ) $this->set( 'm_strViewedOn', trim( $arrValues['viewed_on'] ) ); elseif( isset( $arrValues['viewed_on'] ) ) $this->setViewedOn( $arrValues['viewed_on'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setDefaultReportId( $intDefaultReportId ) {
		$this->set( 'm_intDefaultReportId', CStrings::strToIntDef( $intDefaultReportId, NULL, false ) );
	}

	public function getDefaultReportId() {
		return $this->m_intDefaultReportId;
	}

	public function sqlDefaultReportId() {
		return ( true == isset( $this->m_intDefaultReportId ) ) ? ( string ) $this->m_intDefaultReportId : 'NULL';
	}

	public function setDefaultReportVersionId( $intDefaultReportVersionId ) {
		$this->set( 'm_intDefaultReportVersionId', CStrings::strToIntDef( $intDefaultReportVersionId, NULL, false ) );
	}

	public function getDefaultReportVersionId() {
		return $this->m_intDefaultReportVersionId;
	}

	public function sqlDefaultReportVersionId() {
		return ( true == isset( $this->m_intDefaultReportVersionId ) ) ? ( string ) $this->m_intDefaultReportVersionId : 'NULL';
	}

	public function setReportInstanceId( $intReportInstanceId ) {
		$this->set( 'm_intReportInstanceId', CStrings::strToIntDef( $intReportInstanceId, NULL, false ) );
	}

	public function getReportInstanceId() {
		return $this->m_intReportInstanceId;
	}

	public function sqlReportInstanceId() {
		return ( true == isset( $this->m_intReportInstanceId ) ) ? ( string ) $this->m_intReportInstanceId : 'NULL';
	}

	public function setReportHistoryId( $intReportHistoryId ) {
		$this->set( 'm_intReportHistoryId', CStrings::strToIntDef( $intReportHistoryId, NULL, false ) );
	}

	public function getReportHistoryId() {
		return $this->m_intReportHistoryId;
	}

	public function sqlReportHistoryId() {
		return ( true == isset( $this->m_intReportHistoryId ) ) ? ( string ) $this->m_intReportHistoryId : 'NULL';
	}

	public function setEntrataReportResponseTypeId( $intEntrataReportResponseTypeId ) {
		$this->set( 'm_intEntrataReportResponseTypeId', CStrings::strToIntDef( $intEntrataReportResponseTypeId, NULL, false ) );
	}

	public function getEntrataReportResponseTypeId() {
		return $this->m_intEntrataReportResponseTypeId;
	}

	public function sqlEntrataReportResponseTypeId() {
		return ( true == isset( $this->m_intEntrataReportResponseTypeId ) ) ? ( string ) $this->m_intEntrataReportResponseTypeId : 'NULL';
	}

	public function setNameFirst( $strNameFirst ) {
		$this->set( 'm_strNameFirst', CStrings::strTrimDef( $strNameFirst, -1, NULL, true ) );
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function sqlNameFirst() {
		return ( true == isset( $this->m_strNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameFirst ) : '\'' . addslashes( $this->m_strNameFirst ) . '\'' ) : 'NULL';
	}

	public function setNameLast( $strNameLast ) {
		$this->set( 'm_strNameLast', CStrings::strTrimDef( $strNameLast, -1, NULL, true ) );
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function sqlNameLast() {
		return ( true == isset( $this->m_strNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNameLast ) : '\'' . addslashes( $this->m_strNameLast ) . '\'' ) : 'NULL';
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->set( 'm_strEmailAddress', CStrings::strTrimDef( $strEmailAddress, -1, NULL, true ) );
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function sqlEmailAddress() {
		return ( true == isset( $this->m_strEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEmailAddress ) : '\'' . addslashes( $this->m_strEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setReportTitle( $strReportTitle ) {
		$this->set( 'm_strReportTitle', CStrings::strTrimDef( $strReportTitle, -1, NULL, true ) );
	}

	public function getReportTitle() {
		return $this->m_strReportTitle;
	}

	public function sqlReportTitle() {
		return ( true == isset( $this->m_strReportTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReportTitle ) : '\'' . addslashes( $this->m_strReportTitle ) . '\'' ) : 'NULL';
	}

	public function setMajor( $intMajor ) {
		$this->set( 'm_intMajor', CStrings::strToIntDef( $intMajor, NULL, false ) );
	}

	public function getMajor() {
		return $this->m_intMajor;
	}

	public function sqlMajor() {
		return ( true == isset( $this->m_intMajor ) ) ? ( string ) $this->m_intMajor : 'NULL';
	}

	public function setMinor( $intMinor ) {
		$this->set( 'm_intMinor', CStrings::strToIntDef( $intMinor, NULL, false ) );
	}

	public function getMinor() {
		return $this->m_intMinor;
	}

	public function sqlMinor() {
		return ( true == isset( $this->m_intMinor ) ) ? ( string ) $this->m_intMinor : 'NULL';
	}

	public function setReportInstanceName( $strReportInstanceName ) {
		$this->set( 'm_strReportInstanceName', CStrings::strTrimDef( $strReportInstanceName, -1, NULL, true ) );
	}

	public function getReportInstanceName() {
		return $this->m_strReportInstanceName;
	}

	public function sqlReportInstanceName() {
		return ( true == isset( $this->m_strReportInstanceName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strReportInstanceName ) : '\'' . addslashes( $this->m_strReportInstanceName ) . '\'' ) : 'NULL';
	}

	public function setRespondedOn( $strRespondedOn ) {
		$this->set( 'm_strRespondedOn', CStrings::strTrimDef( $strRespondedOn, -1, NULL, true ) );
	}

	public function getRespondedOn() {
		return $this->m_strRespondedOn;
	}

	public function sqlRespondedOn() {
		return ( true == isset( $this->m_strRespondedOn ) ) ? '\'' . $this->m_strRespondedOn . '\'' : 'NOW()';
	}

	public function setViewedOn( $strViewedOn ) {
		$this->set( 'm_strViewedOn', CStrings::strTrimDef( $strViewedOn, -1, NULL, true ) );
	}

	public function getViewedOn() {
		return $this->m_strViewedOn;
	}

	public function sqlViewedOn() {
		return ( true == isset( $this->m_strViewedOn ) ) ? '\'' . $this->m_strViewedOn . '\'' : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, company_user_id, default_report_id, default_report_version_id, report_instance_id, report_history_id, entrata_report_response_type_id, name_first, name_last, email_address, report_title, major, minor, report_instance_name, details, responded_on, viewed_on, created_on, created_by )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlDefaultReportId() . ', ' .
						$this->sqlDefaultReportVersionId() . ', ' .
						$this->sqlReportInstanceId() . ', ' .
						$this->sqlReportHistoryId() . ', ' .
						$this->sqlEntrataReportResponseTypeId() . ', ' .
						$this->sqlNameFirst() . ', ' .
						$this->sqlNameLast() . ', ' .
						$this->sqlEmailAddress() . ', ' .
						$this->sqlReportTitle() . ', ' .
						$this->sqlMajor() . ', ' .
						$this->sqlMinor() . ', ' .
						$this->sqlReportInstanceName() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlRespondedOn() . ', ' .
						$this->sqlViewedOn() . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_report_id = ' . $this->sqlDefaultReportId(). ',' ; } elseif( true == array_key_exists( 'DefaultReportId', $this->getChangedColumns() ) ) { $strSql .= ' default_report_id = ' . $this->sqlDefaultReportId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_report_version_id = ' . $this->sqlDefaultReportVersionId(). ',' ; } elseif( true == array_key_exists( 'DefaultReportVersionId', $this->getChangedColumns() ) ) { $strSql .= ' default_report_version_id = ' . $this->sqlDefaultReportVersionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_instance_id = ' . $this->sqlReportInstanceId(). ',' ; } elseif( true == array_key_exists( 'ReportInstanceId', $this->getChangedColumns() ) ) { $strSql .= ' report_instance_id = ' . $this->sqlReportInstanceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_history_id = ' . $this->sqlReportHistoryId(). ',' ; } elseif( true == array_key_exists( 'ReportHistoryId', $this->getChangedColumns() ) ) { $strSql .= ' report_history_id = ' . $this->sqlReportHistoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_report_response_type_id = ' . $this->sqlEntrataReportResponseTypeId(). ',' ; } elseif( true == array_key_exists( 'EntrataReportResponseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' entrata_report_response_type_id = ' . $this->sqlEntrataReportResponseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_first = ' . $this->sqlNameFirst(). ',' ; } elseif( true == array_key_exists( 'NameFirst', $this->getChangedColumns() ) ) { $strSql .= ' name_first = ' . $this->sqlNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name_last = ' . $this->sqlNameLast(). ',' ; } elseif( true == array_key_exists( 'NameLast', $this->getChangedColumns() ) ) { $strSql .= ' name_last = ' . $this->sqlNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress(). ',' ; } elseif( true == array_key_exists( 'EmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' email_address = ' . $this->sqlEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_title = ' . $this->sqlReportTitle(). ',' ; } elseif( true == array_key_exists( 'ReportTitle', $this->getChangedColumns() ) ) { $strSql .= ' report_title = ' . $this->sqlReportTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' major = ' . $this->sqlMajor(). ',' ; } elseif( true == array_key_exists( 'Major', $this->getChangedColumns() ) ) { $strSql .= ' major = ' . $this->sqlMajor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minor = ' . $this->sqlMinor(). ',' ; } elseif( true == array_key_exists( 'Minor', $this->getChangedColumns() ) ) { $strSql .= ' minor = ' . $this->sqlMinor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' report_instance_name = ' . $this->sqlReportInstanceName(). ',' ; } elseif( true == array_key_exists( 'ReportInstanceName', $this->getChangedColumns() ) ) { $strSql .= ' report_instance_name = ' . $this->sqlReportInstanceName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' responded_on = ' . $this->sqlRespondedOn(). ',' ; } elseif( true == array_key_exists( 'RespondedOn', $this->getChangedColumns() ) ) { $strSql .= ' responded_on = ' . $this->sqlRespondedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn() ; } elseif( true == array_key_exists( 'ViewedOn', $this->getChangedColumns() ) ) { $strSql .= ' viewed_on = ' . $this->sqlViewedOn() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'company_user_id' => $this->getCompanyUserId(),
			'default_report_id' => $this->getDefaultReportId(),
			'default_report_version_id' => $this->getDefaultReportVersionId(),
			'report_instance_id' => $this->getReportInstanceId(),
			'report_history_id' => $this->getReportHistoryId(),
			'entrata_report_response_type_id' => $this->getEntrataReportResponseTypeId(),
			'name_first' => $this->getNameFirst(),
			'name_last' => $this->getNameLast(),
			'email_address' => $this->getEmailAddress(),
			'report_title' => $this->getReportTitle(),
			'major' => $this->getMajor(),
			'minor' => $this->getMinor(),
			'report_instance_name' => $this->getReportInstanceName(),
			'details' => $this->getDetails(),
			'responded_on' => $this->getRespondedOn(),
			'viewed_on' => $this->getViewedOn(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy()
		);
	}

}
?>