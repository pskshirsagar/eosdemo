<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAssessmentTypes
 * Do not add any new functions to this class.
 */

class CBaseEmployeeAssessmentTypes extends CEosPluralBase {

	/**
	 * @return CEmployeeAssessmentType[]
	 */
	public static function fetchEmployeeAssessmentTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeAssessmentType', $objDatabase );
	}

	/**
	 * @return CEmployeeAssessmentType
	 */
	public static function fetchEmployeeAssessmentType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeAssessmentType', $objDatabase );
	}

	public static function fetchEmployeeAssessmentTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_assessment_types', $objDatabase );
	}

	public static function fetchEmployeeAssessmentTypeById( $intId, $objDatabase ) {
		return self::fetchEmployeeAssessmentType( sprintf( 'SELECT * FROM employee_assessment_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentTypesByEmployeeAssessmentTypeLevelId( $intEmployeeAssessmentTypeLevelId, $objDatabase ) {
		return self::fetchEmployeeAssessmentTypes( sprintf( 'SELECT * FROM employee_assessment_types WHERE employee_assessment_type_level_id = %d', ( int ) $intEmployeeAssessmentTypeLevelId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentTypesByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchEmployeeAssessmentTypes( sprintf( 'SELECT * FROM employee_assessment_types WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>