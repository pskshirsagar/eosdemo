<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDepartments
 * Do not add any new functions to this class.
 */

class CBaseDepartments extends CEosPluralBase {

	/**
	 * @return CDepartment[]
	 */
	public static function fetchDepartments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDepartment', $objDatabase );
	}

	/**
	 * @return CDepartment
	 */
	public static function fetchDepartment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDepartment', $objDatabase );
	}

	public static function fetchDepartmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'departments', $objDatabase );
	}

	public static function fetchDepartmentById( $intId, $objDatabase ) {
		return self::fetchDepartment( sprintf( 'SELECT * FROM departments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>