<?php

class CBaseSurveyTemplateTask extends CEosSingularBase {

	const TABLE_NAME = 'public.survey_template_tasks';

	protected $m_intId;
	protected $m_intResponseTaskTypeId;
	protected $m_intResponseTaskPriorityId;
	protected $m_intResponseTaskDepartmentId;
	protected $m_intResponseTaskEmployeeId;
	protected $m_strResponseTaskTitle;
	protected $m_strResponseTaskDescription;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intResponseTaskTypeId = '0';
		$this->m_intResponseTaskPriorityId = '0';
		$this->m_intResponseTaskDepartmentId = '0';
		$this->m_intResponseTaskEmployeeId = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['response_task_type_id'] ) && $boolDirectSet ) $this->set( 'm_intResponseTaskTypeId', trim( $arrValues['response_task_type_id'] ) ); elseif( isset( $arrValues['response_task_type_id'] ) ) $this->setResponseTaskTypeId( $arrValues['response_task_type_id'] );
		if( isset( $arrValues['response_task_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intResponseTaskPriorityId', trim( $arrValues['response_task_priority_id'] ) ); elseif( isset( $arrValues['response_task_priority_id'] ) ) $this->setResponseTaskPriorityId( $arrValues['response_task_priority_id'] );
		if( isset( $arrValues['response_task_department_id'] ) && $boolDirectSet ) $this->set( 'm_intResponseTaskDepartmentId', trim( $arrValues['response_task_department_id'] ) ); elseif( isset( $arrValues['response_task_department_id'] ) ) $this->setResponseTaskDepartmentId( $arrValues['response_task_department_id'] );
		if( isset( $arrValues['response_task_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intResponseTaskEmployeeId', trim( $arrValues['response_task_employee_id'] ) ); elseif( isset( $arrValues['response_task_employee_id'] ) ) $this->setResponseTaskEmployeeId( $arrValues['response_task_employee_id'] );
		if( isset( $arrValues['response_task_title'] ) && $boolDirectSet ) $this->set( 'm_strResponseTaskTitle', trim( stripcslashes( $arrValues['response_task_title'] ) ) ); elseif( isset( $arrValues['response_task_title'] ) ) $this->setResponseTaskTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response_task_title'] ) : $arrValues['response_task_title'] );
		if( isset( $arrValues['response_task_description'] ) && $boolDirectSet ) $this->set( 'm_strResponseTaskDescription', trim( stripcslashes( $arrValues['response_task_description'] ) ) ); elseif( isset( $arrValues['response_task_description'] ) ) $this->setResponseTaskDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['response_task_description'] ) : $arrValues['response_task_description'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setResponseTaskTypeId( $intResponseTaskTypeId ) {
		$this->set( 'm_intResponseTaskTypeId', CStrings::strToIntDef( $intResponseTaskTypeId, NULL, false ) );
	}

	public function getResponseTaskTypeId() {
		return $this->m_intResponseTaskTypeId;
	}

	public function sqlResponseTaskTypeId() {
		return ( true == isset( $this->m_intResponseTaskTypeId ) ) ? ( string ) $this->m_intResponseTaskTypeId : '0';
	}

	public function setResponseTaskPriorityId( $intResponseTaskPriorityId ) {
		$this->set( 'm_intResponseTaskPriorityId', CStrings::strToIntDef( $intResponseTaskPriorityId, NULL, false ) );
	}

	public function getResponseTaskPriorityId() {
		return $this->m_intResponseTaskPriorityId;
	}

	public function sqlResponseTaskPriorityId() {
		return ( true == isset( $this->m_intResponseTaskPriorityId ) ) ? ( string ) $this->m_intResponseTaskPriorityId : '0';
	}

	public function setResponseTaskDepartmentId( $intResponseTaskDepartmentId ) {
		$this->set( 'm_intResponseTaskDepartmentId', CStrings::strToIntDef( $intResponseTaskDepartmentId, NULL, false ) );
	}

	public function getResponseTaskDepartmentId() {
		return $this->m_intResponseTaskDepartmentId;
	}

	public function sqlResponseTaskDepartmentId() {
		return ( true == isset( $this->m_intResponseTaskDepartmentId ) ) ? ( string ) $this->m_intResponseTaskDepartmentId : '0';
	}

	public function setResponseTaskEmployeeId( $intResponseTaskEmployeeId ) {
		$this->set( 'm_intResponseTaskEmployeeId', CStrings::strToIntDef( $intResponseTaskEmployeeId, NULL, false ) );
	}

	public function getResponseTaskEmployeeId() {
		return $this->m_intResponseTaskEmployeeId;
	}

	public function sqlResponseTaskEmployeeId() {
		return ( true == isset( $this->m_intResponseTaskEmployeeId ) ) ? ( string ) $this->m_intResponseTaskEmployeeId : '0';
	}

	public function setResponseTaskTitle( $strResponseTaskTitle ) {
		$this->set( 'm_strResponseTaskTitle', CStrings::strTrimDef( $strResponseTaskTitle, 50, NULL, true ) );
	}

	public function getResponseTaskTitle() {
		return $this->m_strResponseTaskTitle;
	}

	public function sqlResponseTaskTitle() {
		return ( true == isset( $this->m_strResponseTaskTitle ) ) ? '\'' . addslashes( $this->m_strResponseTaskTitle ) . '\'' : 'NULL';
	}

	public function setResponseTaskDescription( $strResponseTaskDescription ) {
		$this->set( 'm_strResponseTaskDescription', CStrings::strTrimDef( $strResponseTaskDescription, 250, NULL, true ) );
	}

	public function getResponseTaskDescription() {
		return $this->m_strResponseTaskDescription;
	}

	public function sqlResponseTaskDescription() {
		return ( true == isset( $this->m_strResponseTaskDescription ) ) ? '\'' . addslashes( $this->m_strResponseTaskDescription ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, response_task_type_id, response_task_priority_id, response_task_department_id, response_task_employee_id, response_task_title, response_task_description, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlResponseTaskTypeId() . ', ' .
 						$this->sqlResponseTaskPriorityId() . ', ' .
 						$this->sqlResponseTaskDepartmentId() . ', ' .
 						$this->sqlResponseTaskEmployeeId() . ', ' .
 						$this->sqlResponseTaskTitle() . ', ' .
 						$this->sqlResponseTaskDescription() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_task_type_id = ' . $this->sqlResponseTaskTypeId() . ','; } elseif( true == array_key_exists( 'ResponseTaskTypeId', $this->getChangedColumns() ) ) { $strSql .= ' response_task_type_id = ' . $this->sqlResponseTaskTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_task_priority_id = ' . $this->sqlResponseTaskPriorityId() . ','; } elseif( true == array_key_exists( 'ResponseTaskPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' response_task_priority_id = ' . $this->sqlResponseTaskPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_task_department_id = ' . $this->sqlResponseTaskDepartmentId() . ','; } elseif( true == array_key_exists( 'ResponseTaskDepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' response_task_department_id = ' . $this->sqlResponseTaskDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_task_employee_id = ' . $this->sqlResponseTaskEmployeeId() . ','; } elseif( true == array_key_exists( 'ResponseTaskEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' response_task_employee_id = ' . $this->sqlResponseTaskEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_task_title = ' . $this->sqlResponseTaskTitle() . ','; } elseif( true == array_key_exists( 'ResponseTaskTitle', $this->getChangedColumns() ) ) { $strSql .= ' response_task_title = ' . $this->sqlResponseTaskTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response_task_description = ' . $this->sqlResponseTaskDescription() . ','; } elseif( true == array_key_exists( 'ResponseTaskDescription', $this->getChangedColumns() ) ) { $strSql .= ' response_task_description = ' . $this->sqlResponseTaskDescription() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'response_task_type_id' => $this->getResponseTaskTypeId(),
			'response_task_priority_id' => $this->getResponseTaskPriorityId(),
			'response_task_department_id' => $this->getResponseTaskDepartmentId(),
			'response_task_employee_id' => $this->getResponseTaskEmployeeId(),
			'response_task_title' => $this->getResponseTaskTitle(),
			'response_task_description' => $this->getResponseTaskDescription(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>