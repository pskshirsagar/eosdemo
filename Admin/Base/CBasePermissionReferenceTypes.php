<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPermissionReferenceTypes
 * Do not add any new functions to this class.
 */

class CBasePermissionReferenceTypes extends CEosPluralBase {

	/**
	 * @return CPermissionReferenceType[]
	 */
	public static function fetchPermissionReferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPermissionReferenceType', $objDatabase );
	}

	/**
	 * @return CPermissionReferenceType
	 */
	public static function fetchPermissionReferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPermissionReferenceType', $objDatabase );
	}

	public static function fetchPermissionReferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'permission_reference_types', $objDatabase );
	}

	public static function fetchPermissionReferenceTypeById( $intId, $objDatabase ) {
		return self::fetchPermissionReferenceType( sprintf( 'SELECT * FROM permission_reference_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>