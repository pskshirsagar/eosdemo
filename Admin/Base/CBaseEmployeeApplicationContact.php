<?php

class CBaseEmployeeApplicationContact extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_application_contacts';

	protected $m_intId;
	protected $m_intEmployeeApplicationId;
	protected $m_intEmployeeApplicationContactTypeId;
	protected $m_intMassEmailId;
	protected $m_intSystemEmailId;
	protected $m_intContactMinutes;
	protected $m_strContactDatetime;
	protected $m_strNote;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationId', trim( $arrValues['employee_application_id'] ) ); elseif( isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( isset( $arrValues['employee_application_contact_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationContactTypeId', trim( $arrValues['employee_application_contact_type_id'] ) ); elseif( isset( $arrValues['employee_application_contact_type_id'] ) ) $this->setEmployeeApplicationContactTypeId( $arrValues['employee_application_contact_type_id'] );
		if( isset( $arrValues['mass_email_id'] ) && $boolDirectSet ) $this->set( 'm_intMassEmailId', trim( $arrValues['mass_email_id'] ) ); elseif( isset( $arrValues['mass_email_id'] ) ) $this->setMassEmailId( $arrValues['mass_email_id'] );
		if( isset( $arrValues['system_email_id'] ) && $boolDirectSet ) $this->set( 'm_intSystemEmailId', trim( $arrValues['system_email_id'] ) ); elseif( isset( $arrValues['system_email_id'] ) ) $this->setSystemEmailId( $arrValues['system_email_id'] );
		if( isset( $arrValues['contact_minutes'] ) && $boolDirectSet ) $this->set( 'm_intContactMinutes', trim( $arrValues['contact_minutes'] ) ); elseif( isset( $arrValues['contact_minutes'] ) ) $this->setContactMinutes( $arrValues['contact_minutes'] );
		if( isset( $arrValues['contact_datetime'] ) && $boolDirectSet ) $this->set( 'm_strContactDatetime', trim( $arrValues['contact_datetime'] ) ); elseif( isset( $arrValues['contact_datetime'] ) ) $this->setContactDatetime( $arrValues['contact_datetime'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeApplicationId( $intEmployeeApplicationId ) {
		$this->set( 'm_intEmployeeApplicationId', CStrings::strToIntDef( $intEmployeeApplicationId, NULL, false ) );
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function sqlEmployeeApplicationId() {
		return ( true == isset( $this->m_intEmployeeApplicationId ) ) ? ( string ) $this->m_intEmployeeApplicationId : 'NULL';
	}

	public function setEmployeeApplicationContactTypeId( $intEmployeeApplicationContactTypeId ) {
		$this->set( 'm_intEmployeeApplicationContactTypeId', CStrings::strToIntDef( $intEmployeeApplicationContactTypeId, NULL, false ) );
	}

	public function getEmployeeApplicationContactTypeId() {
		return $this->m_intEmployeeApplicationContactTypeId;
	}

	public function sqlEmployeeApplicationContactTypeId() {
		return ( true == isset( $this->m_intEmployeeApplicationContactTypeId ) ) ? ( string ) $this->m_intEmployeeApplicationContactTypeId : 'NULL';
	}

	public function setMassEmailId( $intMassEmailId ) {
		$this->set( 'm_intMassEmailId', CStrings::strToIntDef( $intMassEmailId, NULL, false ) );
	}

	public function getMassEmailId() {
		return $this->m_intMassEmailId;
	}

	public function sqlMassEmailId() {
		return ( true == isset( $this->m_intMassEmailId ) ) ? ( string ) $this->m_intMassEmailId : 'NULL';
	}

	public function setSystemEmailId( $intSystemEmailId ) {
		$this->set( 'm_intSystemEmailId', CStrings::strToIntDef( $intSystemEmailId, NULL, false ) );
	}

	public function getSystemEmailId() {
		return $this->m_intSystemEmailId;
	}

	public function sqlSystemEmailId() {
		return ( true == isset( $this->m_intSystemEmailId ) ) ? ( string ) $this->m_intSystemEmailId : 'NULL';
	}

	public function setContactMinutes( $intContactMinutes ) {
		$this->set( 'm_intContactMinutes', CStrings::strToIntDef( $intContactMinutes, NULL, false ) );
	}

	public function getContactMinutes() {
		return $this->m_intContactMinutes;
	}

	public function sqlContactMinutes() {
		return ( true == isset( $this->m_intContactMinutes ) ) ? ( string ) $this->m_intContactMinutes : 'NULL';
	}

	public function setContactDatetime( $strContactDatetime ) {
		$this->set( 'm_strContactDatetime', CStrings::strTrimDef( $strContactDatetime, -1, NULL, true ) );
	}

	public function getContactDatetime() {
		return $this->m_strContactDatetime;
	}

	public function sqlContactDatetime() {
		return ( true == isset( $this->m_strContactDatetime ) ) ? '\'' . $this->m_strContactDatetime . '\'' : 'NOW()';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, 2000, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_application_id, employee_application_contact_type_id, mass_email_id, system_email_id, contact_minutes, contact_datetime, note, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeApplicationId() . ', ' .
 						$this->sqlEmployeeApplicationContactTypeId() . ', ' .
 						$this->sqlMassEmailId() . ', ' .
 						$this->sqlSystemEmailId() . ', ' .
 						$this->sqlContactMinutes() . ', ' .
 						$this->sqlContactDatetime() . ', ' .
 						$this->sqlNote() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_contact_type_id = ' . $this->sqlEmployeeApplicationContactTypeId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationContactTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_contact_type_id = ' . $this->sqlEmployeeApplicationContactTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mass_email_id = ' . $this->sqlMassEmailId() . ','; } elseif( true == array_key_exists( 'MassEmailId', $this->getChangedColumns() ) ) { $strSql .= ' mass_email_id = ' . $this->sqlMassEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; } elseif( true == array_key_exists( 'SystemEmailId', $this->getChangedColumns() ) ) { $strSql .= ' system_email_id = ' . $this->sqlSystemEmailId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_minutes = ' . $this->sqlContactMinutes() . ','; } elseif( true == array_key_exists( 'ContactMinutes', $this->getChangedColumns() ) ) { $strSql .= ' contact_minutes = ' . $this->sqlContactMinutes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_datetime = ' . $this->sqlContactDatetime() . ','; } elseif( true == array_key_exists( 'ContactDatetime', $this->getChangedColumns() ) ) { $strSql .= ' contact_datetime = ' . $this->sqlContactDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote() . ','; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_application_id' => $this->getEmployeeApplicationId(),
			'employee_application_contact_type_id' => $this->getEmployeeApplicationContactTypeId(),
			'mass_email_id' => $this->getMassEmailId(),
			'system_email_id' => $this->getSystemEmailId(),
			'contact_minutes' => $this->getContactMinutes(),
			'contact_datetime' => $this->getContactDatetime(),
			'note' => $this->getNote(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>