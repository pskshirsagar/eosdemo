<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseChartReportTemplates
 * Do not add any new functions to this class.
 */

class CBaseReleaseChartReportTemplates extends CEosPluralBase {

	/**
	 * @return CReleaseChartReportTemplate[]
	 */
	public static function fetchReleaseChartReportTemplates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReleaseChartReportTemplate::class, $objDatabase );
	}

	/**
	 * @return CReleaseChartReportTemplate
	 */
	public static function fetchReleaseChartReportTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReleaseChartReportTemplate::class, $objDatabase );
	}

	public static function fetchReleaseChartReportTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'release_chart_report_templates', $objDatabase );
	}

	public static function fetchReleaseChartReportTemplateById( $intId, $objDatabase ) {
		return self::fetchReleaseChartReportTemplate( sprintf( 'SELECT * FROM release_chart_report_templates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReleaseChartReportTemplatesByReleaseChartTypeId( $intReleaseChartTypeId, $objDatabase ) {
		return self::fetchReleaseChartReportTemplates( sprintf( 'SELECT * FROM release_chart_report_templates WHERE release_chart_type_id = %d', ( int ) $intReleaseChartTypeId ), $objDatabase );
	}

	public static function fetchReleaseChartReportTemplatesByReleaseChartCategoriesId( $intReleaseChartCategoriesId, $objDatabase ) {
		return self::fetchReleaseChartReportTemplates( sprintf( 'SELECT * FROM release_chart_report_templates WHERE release_chart_categories_id = %d', ( int ) $intReleaseChartCategoriesId ), $objDatabase );
	}

}
?>