<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEcsMessages
 * Do not add any new functions to this class.
 */

class CBaseEcsMessages extends CEosPluralBase {

	/**
	 * @return CEcsMessage[]
	 */
	public static function fetchEcsMessages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEcsMessage', $objDatabase );
	}

	/**
	 * @return CEcsMessage
	 */
	public static function fetchEcsMessage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEcsMessage', $objDatabase );
	}

	public static function fetchEcsMessageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ecs_messages', $objDatabase );
	}

	public static function fetchEcsMessageById( $intId, $objDatabase ) {
		return self::fetchEcsMessage( sprintf( 'SELECT * FROM ecs_messages WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>