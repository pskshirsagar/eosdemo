<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTransportPickupPoints
 * Do not add any new functions to this class.
 */

class CBaseTransportPickupPoints extends CEosPluralBase {

	/**
	 * @return CTransportPickupPoint[]
	 */
	public static function fetchTransportPickupPoints( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTransportPickupPoint', $objDatabase );
	}

	/**
	 * @return CTransportPickupPoint
	 */
	public static function fetchTransportPickupPoint( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTransportPickupPoint', $objDatabase );
	}

	public static function fetchTransportPickupPointById( $intId, $objDatabase ) {
		return self::fetchTransportPickupPoint( sprintf( 'SELECT * FROM transport_pickup_points WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTransportPickupPointCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transport_pickup_points', $objDatabase );
	}

}
?>