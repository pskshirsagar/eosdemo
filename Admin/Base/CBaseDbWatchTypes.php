<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDbWatchTypes
 * Do not add any new functions to this class.
 */

class CBaseDbWatchTypes extends CEosPluralBase {

	/**
	 * @return CDbWatchType[]
	 */
	public static function fetchDbWatchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDbWatchType', $objDatabase );
	}

	/**
	 * @return CDbWatchType
	 */
	public static function fetchDbWatchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDbWatchType', $objDatabase );
	}

	public static function fetchDbWatchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'db_watch_types', $objDatabase );
	}

	public static function fetchDbWatchTypeById( $intId, $objDatabase ) {
		return self::fetchDbWatchType( sprintf( 'SELECT * FROM db_watch_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>