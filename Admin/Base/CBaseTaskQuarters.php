<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskQuarters
 * Do not add any new functions to this class.
 */

class CBaseTaskQuarters extends CEosPluralBase {

	/**
	 * @return CTaskQuarter[]
	 */
	public static function fetchTaskQuarters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskQuarter', $objDatabase );
	}

	/**
	 * @return CTaskQuarter
	 */
	public static function fetchTaskQuarter( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskQuarter', $objDatabase );
	}

	public static function fetchTaskQuarterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_quarters', $objDatabase );
	}

	public static function fetchTaskQuarterById( $intId, $objDatabase ) {
		return self::fetchTaskQuarter( sprintf( 'SELECT * FROM task_quarters WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>