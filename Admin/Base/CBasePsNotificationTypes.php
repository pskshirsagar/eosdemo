<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsNotificationTypes
 * Do not add any new functions to this class.
 */

class CBasePsNotificationTypes extends CEosPluralBase {

	/**
	 * @return CPsNotificationType[]
	 */
	public static function fetchPsNotificationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsNotificationType', $objDatabase );
	}

	/**
	 * @return CPsNotificationType
	 */
	public static function fetchPsNotificationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsNotificationType', $objDatabase );
	}

	public static function fetchPsNotificationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_notification_types', $objDatabase );
	}

	public static function fetchPsNotificationTypeById( $intId, $objDatabase ) {
		return self::fetchPsNotificationType( sprintf( 'SELECT * FROM ps_notification_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>