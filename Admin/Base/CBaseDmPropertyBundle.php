<?php

class CBaseDmPropertyBundle extends CEosSingularBase {

	const TABLE_NAME = 'public.dm_property_bundles';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intDmTemplateId;
	protected $m_intDmBundleId;
	protected $m_intYearlyCredit;
	protected $m_boolStatus;
	protected $m_strNotes;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strEncryptedUsername;
	protected $m_strEncryptedPassword;

	public function __construct() {
		parent::__construct();

		$this->m_intYearlyCredit = '0';
		$this->m_boolStatus = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['dm_template_id'] ) && $boolDirectSet ) $this->set( 'm_intDmTemplateId', trim( $arrValues['dm_template_id'] ) ); elseif( isset( $arrValues['dm_template_id'] ) ) $this->setDmTemplateId( $arrValues['dm_template_id'] );
		if( isset( $arrValues['dm_bundle_id'] ) && $boolDirectSet ) $this->set( 'm_intDmBundleId', trim( $arrValues['dm_bundle_id'] ) ); elseif( isset( $arrValues['dm_bundle_id'] ) ) $this->setDmBundleId( $arrValues['dm_bundle_id'] );
		if( isset( $arrValues['yearly_credit'] ) && $boolDirectSet ) $this->set( 'm_intYearlyCredit', trim( $arrValues['yearly_credit'] ) ); elseif( isset( $arrValues['yearly_credit'] ) ) $this->setYearlyCredit( $arrValues['yearly_credit'] );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_boolStatus', trim( stripcslashes( $arrValues['status'] ) ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status'] ) : $arrValues['status'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['encrypted_username'] ) && $boolDirectSet ) $this->set( 'm_strEncryptedUsername', trim( stripcslashes( $arrValues['encrypted_username'] ) ) ); elseif( isset( $arrValues['encrypted_username'] ) ) $this->setEncryptedUsername( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['encrypted_username'] ) : $arrValues['encrypted_username'] );
		if( isset( $arrValues['encrypted_password'] ) && $boolDirectSet ) $this->set( 'm_strEncryptedPassword', trim( stripcslashes( $arrValues['encrypted_password'] ) ) ); elseif( isset( $arrValues['encrypted_password'] ) ) $this->setEncryptedPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['encrypted_password'] ) : $arrValues['encrypted_password'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setDmTemplateId( $intDmTemplateId ) {
		$this->set( 'm_intDmTemplateId', CStrings::strToIntDef( $intDmTemplateId, NULL, false ) );
	}

	public function getDmTemplateId() {
		return $this->m_intDmTemplateId;
	}

	public function sqlDmTemplateId() {
		return ( true == isset( $this->m_intDmTemplateId ) ) ? ( string ) $this->m_intDmTemplateId : 'NULL';
	}

	public function setDmBundleId( $intDmBundleId ) {
		$this->set( 'm_intDmBundleId', CStrings::strToIntDef( $intDmBundleId, NULL, false ) );
	}

	public function getDmBundleId() {
		return $this->m_intDmBundleId;
	}

	public function sqlDmBundleId() {
		return ( true == isset( $this->m_intDmBundleId ) ) ? ( string ) $this->m_intDmBundleId : 'NULL';
	}

	public function setYearlyCredit( $intYearlyCredit ) {
		$this->set( 'm_intYearlyCredit', CStrings::strToIntDef( $intYearlyCredit, NULL, false ) );
	}

	public function getYearlyCredit() {
		return $this->m_intYearlyCredit;
	}

	public function sqlYearlyCredit() {
		return ( true == isset( $this->m_intYearlyCredit ) ) ? ( string ) $this->m_intYearlyCredit : '0';
	}

	public function setStatus( $boolStatus ) {
		$this->set( 'm_boolStatus', CStrings::strToBool( $boolStatus ) );
	}

	public function getStatus() {
		return $this->m_boolStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_boolStatus ) ) ? '\'' . ( true == ( bool ) $this->m_boolStatus ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setEncryptedUsername( $strEncryptedUsername ) {
		$this->set( 'm_strEncryptedUsername', CStrings::strTrimDef( $strEncryptedUsername, 240, NULL, true ) );
	}

	public function getEncryptedUsername() {
		return $this->m_strEncryptedUsername;
	}

	public function sqlEncryptedUsername() {
		return ( true == isset( $this->m_strEncryptedUsername ) ) ? '\'' . addslashes( $this->m_strEncryptedUsername ) . '\'' : 'NULL';
	}

	public function setEncryptedPassword( $strEncryptedPassword ) {
		$this->set( 'm_strEncryptedPassword', CStrings::strTrimDef( $strEncryptedPassword, 240, NULL, true ) );
	}

	public function getEncryptedPassword() {
		return $this->m_strEncryptedPassword;
	}

	public function sqlEncryptedPassword() {
		return ( true == isset( $this->m_strEncryptedPassword ) ) ? '\'' . addslashes( $this->m_strEncryptedPassword ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, dm_template_id, dm_bundle_id, yearly_credit, status, notes, updated_by, updated_on, created_by, created_on, encrypted_username, encrypted_password )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlDmTemplateId() . ', ' .
						$this->sqlDmBundleId() . ', ' .
						$this->sqlYearlyCredit() . ', ' .
						$this->sqlStatus() . ', ' .
						$this->sqlNotes() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlEncryptedUsername() . ', ' .
						$this->sqlEncryptedPassword() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dm_template_id = ' . $this->sqlDmTemplateId(). ',' ; } elseif( true == array_key_exists( 'DmTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' dm_template_id = ' . $this->sqlDmTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dm_bundle_id = ' . $this->sqlDmBundleId(). ',' ; } elseif( true == array_key_exists( 'DmBundleId', $this->getChangedColumns() ) ) { $strSql .= ' dm_bundle_id = ' . $this->sqlDmBundleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' yearly_credit = ' . $this->sqlYearlyCredit(). ',' ; } elseif( true == array_key_exists( 'YearlyCredit', $this->getChangedColumns() ) ) { $strSql .= ' yearly_credit = ' . $this->sqlYearlyCredit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus(). ',' ; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' encrypted_username = ' . $this->sqlEncryptedUsername(). ',' ; } elseif( true == array_key_exists( 'EncryptedUsername', $this->getChangedColumns() ) ) { $strSql .= ' encrypted_username = ' . $this->sqlEncryptedUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' encrypted_password = ' . $this->sqlEncryptedPassword(). ',' ; } elseif( true == array_key_exists( 'EncryptedPassword', $this->getChangedColumns() ) ) { $strSql .= ' encrypted_password = ' . $this->sqlEncryptedPassword() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'dm_template_id' => $this->getDmTemplateId(),
			'dm_bundle_id' => $this->getDmBundleId(),
			'yearly_credit' => $this->getYearlyCredit(),
			'status' => $this->getStatus(),
			'notes' => $this->getNotes(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'encrypted_username' => $this->getEncryptedUsername(),
			'encrypted_password' => $this->getEncryptedPassword()
		);
	}

}
?>