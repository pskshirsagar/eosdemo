<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyBlacklists
 * Do not add any new functions to this class.
 */

class CBaseSurveyBlacklists extends CEosPluralBase {

	/**
	 * @return CSurveyBlacklist[]
	 */
	public static function fetchSurveyBlacklists( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyBlacklist', $objDatabase );
	}

	/**
	 * @return CSurveyBlacklist
	 */
	public static function fetchSurveyBlacklist( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyBlacklist', $objDatabase );
	}

	public static function fetchSurveyBlacklistCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_blacklists', $objDatabase );
	}

	public static function fetchSurveyBlacklistById( $intId, $objDatabase ) {
		return self::fetchSurveyBlacklist( sprintf( 'SELECT * FROM survey_blacklists WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveyBlacklistsByCid( $intCid, $objDatabase ) {
		return self::fetchSurveyBlacklists( sprintf( 'SELECT * FROM survey_blacklists WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSurveyBlacklistsBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		return self::fetchSurveyBlacklists( sprintf( 'SELECT * FROM survey_blacklists WHERE survey_template_id = %d', ( int ) $intSurveyTemplateId ), $objDatabase );
	}

	public static function fetchSurveyBlacklistsByPersonRoleId( $intPersonRoleId, $objDatabase ) {
		return self::fetchSurveyBlacklists( sprintf( 'SELECT * FROM survey_blacklists WHERE person_role_id = %d', ( int ) $intPersonRoleId ), $objDatabase );
	}

	public static function fetchSurveyBlacklistsByPersonId( $intPersonId, $objDatabase ) {
		return self::fetchSurveyBlacklists( sprintf( 'SELECT * FROM survey_blacklists WHERE person_id = %d', ( int ) $intPersonId ), $objDatabase );
	}

	public static function fetchSurveyBlacklistsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchSurveyBlacklists( sprintf( 'SELECT * FROM survey_blacklists WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchSurveyBlacklistsByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchSurveyBlacklists( sprintf( 'SELECT * FROM survey_blacklists WHERE designation_id = %d', ( int ) $intDesignationId ), $objDatabase );
	}

}
?>