<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeInteractionTypes
 * Do not add any new functions to this class.
 */

class CBaseEmployeeInteractionTypes extends CEosPluralBase {

	/**
	 * @return CEmployeeInteractionType[]
	 */
	public static function fetchEmployeeInteractionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeInteractionType::class, $objDatabase );
	}

	/**
	 * @return CEmployeeInteractionType
	 */
	public static function fetchEmployeeInteractionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeInteractionType::class, $objDatabase );
	}

	public static function fetchEmployeeInteractionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_interaction_types', $objDatabase );
	}

	public static function fetchEmployeeInteractionTypeById( $intId, $objDatabase ) {
		return self::fetchEmployeeInteractionType( sprintf( 'SELECT * FROM employee_interaction_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>