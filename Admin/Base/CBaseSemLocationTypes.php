<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemLocationTypes
 * Do not add any new functions to this class.
 */

class CBaseSemLocationTypes extends CEosPluralBase {

	/**
	 * @return CSemLocationType[]
	 */
	public static function fetchSemLocationTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemLocationType', $objDatabase );
	}

	/**
	 * @return CSemLocationType
	 */
	public static function fetchSemLocationType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemLocationType', $objDatabase );
	}

	public static function fetchSemLocationTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_location_types', $objDatabase );
	}

	public static function fetchSemLocationTypeById( $intId, $objDatabase ) {
		return self::fetchSemLocationType( sprintf( 'SELECT * FROM sem_location_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>