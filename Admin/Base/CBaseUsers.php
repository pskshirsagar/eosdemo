<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUsers
 * Do not add any new functions to this class.
 */

class CBaseUsers extends CEosPluralBase {

	/**
	 * @return CUser[]
	 */
	public static function fetchUsers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUser', $objDatabase );
	}

	/**
	 * @return CUser
	 */
	public static function fetchUser( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUser', $objDatabase );
	}

	public static function fetchUserCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'users', $objDatabase );
	}

	public static function fetchUserById( $intId, $objDatabase ) {
		return self::fetchUser( sprintf( 'SELECT * FROM users WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUsersByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchUsers( sprintf( 'SELECT * FROM users WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>