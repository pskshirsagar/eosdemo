<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsDocumentCategories
 * Do not add any new functions to this class.
 */

class CBasePsDocumentCategories extends CEosPluralBase {

	/**
	 * @return CPsDocumentCategory[]
	 */
	public static function fetchPsDocumentCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsDocumentCategory', $objDatabase );
	}

	/**
	 * @return CPsDocumentCategory
	 */
	public static function fetchPsDocumentCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsDocumentCategory', $objDatabase );
	}

	public static function fetchPsDocumentCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_document_categories', $objDatabase );
	}

	public static function fetchPsDocumentCategoryById( $intId, $objDatabase ) {
		return self::fetchPsDocumentCategory( sprintf( 'SELECT * FROM ps_document_categories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsDocumentCategoriesByPsDocumentTypeId( $intPsDocumentTypeId, $objDatabase ) {
		return self::fetchPsDocumentCategories( sprintf( 'SELECT * FROM ps_document_categories WHERE ps_document_type_id = %d', ( int ) $intPsDocumentTypeId ), $objDatabase );
	}

}
?>