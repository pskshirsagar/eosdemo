<?php

class CBaseIlsEmailReparseLog extends CEosSingularBase {

	const TABLE_NAME = 'public.ils_email_reparse_logs';

	protected $m_intId;
	protected $m_intIlsErrorFixLogId;
	protected $m_intSubmittedBy;
	protected $m_strReparseDate;
	protected $m_strReparseScriptStartTime;
	protected $m_strReparseScriptEndTime;
	protected $m_strReparseFromDate;
	protected $m_strReparseToDate;
	protected $m_intTotalGuestCards;
	protected $m_intReparseSuccess;
	protected $m_intReparseFailed;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ils_error_fix_log_id'] ) && $boolDirectSet ) $this->set( 'm_intIlsErrorFixLogId', trim( $arrValues['ils_error_fix_log_id'] ) ); elseif( isset( $arrValues['ils_error_fix_log_id'] ) ) $this->setIlsErrorFixLogId( $arrValues['ils_error_fix_log_id'] );
		if( isset( $arrValues['submitted_by'] ) && $boolDirectSet ) $this->set( 'm_intSubmittedBy', trim( $arrValues['submitted_by'] ) ); elseif( isset( $arrValues['submitted_by'] ) ) $this->setSubmittedBy( $arrValues['submitted_by'] );
		if( isset( $arrValues['reparse_date'] ) && $boolDirectSet ) $this->set( 'm_strReparseDate', trim( $arrValues['reparse_date'] ) ); elseif( isset( $arrValues['reparse_date'] ) ) $this->setReparseDate( $arrValues['reparse_date'] );
		if( isset( $arrValues['reparse_script_start_time'] ) && $boolDirectSet ) $this->set( 'm_strReparseScriptStartTime', trim( $arrValues['reparse_script_start_time'] ) ); elseif( isset( $arrValues['reparse_script_start_time'] ) ) $this->setReparseScriptStartTime( $arrValues['reparse_script_start_time'] );
		if( isset( $arrValues['reparse_script_end_time'] ) && $boolDirectSet ) $this->set( 'm_strReparseScriptEndTime', trim( $arrValues['reparse_script_end_time'] ) ); elseif( isset( $arrValues['reparse_script_end_time'] ) ) $this->setReparseScriptEndTime( $arrValues['reparse_script_end_time'] );
		if( isset( $arrValues['reparse_from_date'] ) && $boolDirectSet ) $this->set( 'm_strReparseFromDate', trim( $arrValues['reparse_from_date'] ) ); elseif( isset( $arrValues['reparse_from_date'] ) ) $this->setReparseFromDate( $arrValues['reparse_from_date'] );
		if( isset( $arrValues['reparse_to_date'] ) && $boolDirectSet ) $this->set( 'm_strReparseToDate', trim( $arrValues['reparse_to_date'] ) ); elseif( isset( $arrValues['reparse_to_date'] ) ) $this->setReparseToDate( $arrValues['reparse_to_date'] );
		if( isset( $arrValues['total_guest_cards'] ) && $boolDirectSet ) $this->set( 'm_intTotalGuestCards', trim( $arrValues['total_guest_cards'] ) ); elseif( isset( $arrValues['total_guest_cards'] ) ) $this->setTotalGuestCards( $arrValues['total_guest_cards'] );
		if( isset( $arrValues['reparse_success'] ) && $boolDirectSet ) $this->set( 'm_intReparseSuccess', trim( $arrValues['reparse_success'] ) ); elseif( isset( $arrValues['reparse_success'] ) ) $this->setReparseSuccess( $arrValues['reparse_success'] );
		if( isset( $arrValues['reparse_failed'] ) && $boolDirectSet ) $this->set( 'm_intReparseFailed', trim( $arrValues['reparse_failed'] ) ); elseif( isset( $arrValues['reparse_failed'] ) ) $this->setReparseFailed( $arrValues['reparse_failed'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setIlsErrorFixLogId( $intIlsErrorFixLogId ) {
		$this->set( 'm_intIlsErrorFixLogId', CStrings::strToIntDef( $intIlsErrorFixLogId, NULL, false ) );
	}

	public function getIlsErrorFixLogId() {
		return $this->m_intIlsErrorFixLogId;
	}

	public function sqlIlsErrorFixLogId() {
		return ( true == isset( $this->m_intIlsErrorFixLogId ) ) ? ( string ) $this->m_intIlsErrorFixLogId : 'NULL';
	}

	public function setSubmittedBy( $intSubmittedBy ) {
		$this->set( 'm_intSubmittedBy', CStrings::strToIntDef( $intSubmittedBy, NULL, false ) );
	}

	public function getSubmittedBy() {
		return $this->m_intSubmittedBy;
	}

	public function sqlSubmittedBy() {
		return ( true == isset( $this->m_intSubmittedBy ) ) ? ( string ) $this->m_intSubmittedBy : 'NULL';
	}

	public function setReparseDate( $strReparseDate ) {
		$this->set( 'm_strReparseDate', CStrings::strTrimDef( $strReparseDate, -1, NULL, true ) );
	}

	public function getReparseDate() {
		return $this->m_strReparseDate;
	}

	public function sqlReparseDate() {
		return ( true == isset( $this->m_strReparseDate ) ) ? '\'' . $this->m_strReparseDate . '\'' : 'NULL';
	}

	public function setReparseScriptStartTime( $strReparseScriptStartTime ) {
		$this->set( 'm_strReparseScriptStartTime', CStrings::strTrimDef( $strReparseScriptStartTime, -1, NULL, true ) );
	}

	public function getReparseScriptStartTime() {
		return $this->m_strReparseScriptStartTime;
	}

	public function sqlReparseScriptStartTime() {
		return ( true == isset( $this->m_strReparseScriptStartTime ) ) ? '\'' . $this->m_strReparseScriptStartTime . '\'' : 'NULL';
	}

	public function setReparseScriptEndTime( $strReparseScriptEndTime ) {
		$this->set( 'm_strReparseScriptEndTime', CStrings::strTrimDef( $strReparseScriptEndTime, -1, NULL, true ) );
	}

	public function getReparseScriptEndTime() {
		return $this->m_strReparseScriptEndTime;
	}

	public function sqlReparseScriptEndTime() {
		return ( true == isset( $this->m_strReparseScriptEndTime ) ) ? '\'' . $this->m_strReparseScriptEndTime . '\'' : 'NULL';
	}

	public function setReparseFromDate( $strReparseFromDate ) {
		$this->set( 'm_strReparseFromDate', CStrings::strTrimDef( $strReparseFromDate, -1, NULL, true ) );
	}

	public function getReparseFromDate() {
		return $this->m_strReparseFromDate;
	}

	public function sqlReparseFromDate() {
		return ( true == isset( $this->m_strReparseFromDate ) ) ? '\'' . $this->m_strReparseFromDate . '\'' : 'NULL';
	}

	public function setReparseToDate( $strReparseToDate ) {
		$this->set( 'm_strReparseToDate', CStrings::strTrimDef( $strReparseToDate, -1, NULL, true ) );
	}

	public function getReparseToDate() {
		return $this->m_strReparseToDate;
	}

	public function sqlReparseToDate() {
		return ( true == isset( $this->m_strReparseToDate ) ) ? '\'' . $this->m_strReparseToDate . '\'' : 'NULL';
	}

	public function setTotalGuestCards( $intTotalGuestCards ) {
		$this->set( 'm_intTotalGuestCards', CStrings::strToIntDef( $intTotalGuestCards, NULL, false ) );
	}

	public function getTotalGuestCards() {
		return $this->m_intTotalGuestCards;
	}

	public function sqlTotalGuestCards() {
		return ( true == isset( $this->m_intTotalGuestCards ) ) ? ( string ) $this->m_intTotalGuestCards : 'NULL';
	}

	public function setReparseSuccess( $intReparseSuccess ) {
		$this->set( 'm_intReparseSuccess', CStrings::strToIntDef( $intReparseSuccess, NULL, false ) );
	}

	public function getReparseSuccess() {
		return $this->m_intReparseSuccess;
	}

	public function sqlReparseSuccess() {
		return ( true == isset( $this->m_intReparseSuccess ) ) ? ( string ) $this->m_intReparseSuccess : 'NULL';
	}

	public function setReparseFailed( $intReparseFailed ) {
		$this->set( 'm_intReparseFailed', CStrings::strToIntDef( $intReparseFailed, NULL, false ) );
	}

	public function getReparseFailed() {
		return $this->m_intReparseFailed;
	}

	public function sqlReparseFailed() {
		return ( true == isset( $this->m_intReparseFailed ) ) ? ( string ) $this->m_intReparseFailed : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ils_error_fix_log_id, submitted_by, reparse_date, reparse_script_start_time, reparse_script_end_time, reparse_from_date, reparse_to_date, total_guest_cards, reparse_success, reparse_failed, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlIlsErrorFixLogId() . ', ' .
 						$this->sqlSubmittedBy() . ', ' .
 						$this->sqlReparseDate() . ', ' .
 						$this->sqlReparseScriptStartTime() . ', ' .
 						$this->sqlReparseScriptEndTime() . ', ' .
 						$this->sqlReparseFromDate() . ', ' .
 						$this->sqlReparseToDate() . ', ' .
 						$this->sqlTotalGuestCards() . ', ' .
 						$this->sqlReparseSuccess() . ', ' .
 						$this->sqlReparseFailed() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ils_error_fix_log_id = ' . $this->sqlIlsErrorFixLogId() . ','; } elseif( true == array_key_exists( 'IlsErrorFixLogId', $this->getChangedColumns() ) ) { $strSql .= ' ils_error_fix_log_id = ' . $this->sqlIlsErrorFixLogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' submitted_by = ' . $this->sqlSubmittedBy() . ','; } elseif( true == array_key_exists( 'SubmittedBy', $this->getChangedColumns() ) ) { $strSql .= ' submitted_by = ' . $this->sqlSubmittedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reparse_date = ' . $this->sqlReparseDate() . ','; } elseif( true == array_key_exists( 'ReparseDate', $this->getChangedColumns() ) ) { $strSql .= ' reparse_date = ' . $this->sqlReparseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reparse_script_start_time = ' . $this->sqlReparseScriptStartTime() . ','; } elseif( true == array_key_exists( 'ReparseScriptStartTime', $this->getChangedColumns() ) ) { $strSql .= ' reparse_script_start_time = ' . $this->sqlReparseScriptStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reparse_script_end_time = ' . $this->sqlReparseScriptEndTime() . ','; } elseif( true == array_key_exists( 'ReparseScriptEndTime', $this->getChangedColumns() ) ) { $strSql .= ' reparse_script_end_time = ' . $this->sqlReparseScriptEndTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reparse_from_date = ' . $this->sqlReparseFromDate() . ','; } elseif( true == array_key_exists( 'ReparseFromDate', $this->getChangedColumns() ) ) { $strSql .= ' reparse_from_date = ' . $this->sqlReparseFromDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reparse_to_date = ' . $this->sqlReparseToDate() . ','; } elseif( true == array_key_exists( 'ReparseToDate', $this->getChangedColumns() ) ) { $strSql .= ' reparse_to_date = ' . $this->sqlReparseToDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_guest_cards = ' . $this->sqlTotalGuestCards() . ','; } elseif( true == array_key_exists( 'TotalGuestCards', $this->getChangedColumns() ) ) { $strSql .= ' total_guest_cards = ' . $this->sqlTotalGuestCards() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reparse_success = ' . $this->sqlReparseSuccess() . ','; } elseif( true == array_key_exists( 'ReparseSuccess', $this->getChangedColumns() ) ) { $strSql .= ' reparse_success = ' . $this->sqlReparseSuccess() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reparse_failed = ' . $this->sqlReparseFailed() . ','; } elseif( true == array_key_exists( 'ReparseFailed', $this->getChangedColumns() ) ) { $strSql .= ' reparse_failed = ' . $this->sqlReparseFailed() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ils_error_fix_log_id' => $this->getIlsErrorFixLogId(),
			'submitted_by' => $this->getSubmittedBy(),
			'reparse_date' => $this->getReparseDate(),
			'reparse_script_start_time' => $this->getReparseScriptStartTime(),
			'reparse_script_end_time' => $this->getReparseScriptEndTime(),
			'reparse_from_date' => $this->getReparseFromDate(),
			'reparse_to_date' => $this->getReparseToDate(),
			'total_guest_cards' => $this->getTotalGuestCards(),
			'reparse_success' => $this->getReparseSuccess(),
			'reparse_failed' => $this->getReparseFailed(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>