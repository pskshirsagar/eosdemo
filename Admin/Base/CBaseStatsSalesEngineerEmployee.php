<?php

class CBaseStatsSalesEngineerEmployee extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_sales_engineer_employees';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_strWeek;
	protected $m_intDemos;
	protected $m_intUnitsDemoed;
	protected $m_intTotalOpportunityDollars;
	protected $m_intDealsClosed;
	protected $m_intDealsValue;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intDemos = '0';
		$this->m_intUnitsDemoed = '0';
		$this->m_intTotalOpportunityDollars = '0';
		$this->m_intDealsClosed = '0';
		$this->m_intDealsValue = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['week'] ) && $boolDirectSet ) $this->set( 'm_strWeek', trim( $arrValues['week'] ) ); elseif( isset( $arrValues['week'] ) ) $this->setWeek( $arrValues['week'] );
		if( isset( $arrValues['demos'] ) && $boolDirectSet ) $this->set( 'm_intDemos', trim( $arrValues['demos'] ) ); elseif( isset( $arrValues['demos'] ) ) $this->setDemos( $arrValues['demos'] );
		if( isset( $arrValues['units_demoed'] ) && $boolDirectSet ) $this->set( 'm_intUnitsDemoed', trim( $arrValues['units_demoed'] ) ); elseif( isset( $arrValues['units_demoed'] ) ) $this->setUnitsDemoed( $arrValues['units_demoed'] );
		if( isset( $arrValues['total_opportunity_dollars'] ) && $boolDirectSet ) $this->set( 'm_intTotalOpportunityDollars', trim( $arrValues['total_opportunity_dollars'] ) ); elseif( isset( $arrValues['total_opportunity_dollars'] ) ) $this->setTotalOpportunityDollars( $arrValues['total_opportunity_dollars'] );
		if( isset( $arrValues['deals_closed'] ) && $boolDirectSet ) $this->set( 'm_intDealsClosed', trim( $arrValues['deals_closed'] ) ); elseif( isset( $arrValues['deals_closed'] ) ) $this->setDealsClosed( $arrValues['deals_closed'] );
		if( isset( $arrValues['deals_value'] ) && $boolDirectSet ) $this->set( 'm_intDealsValue', trim( $arrValues['deals_value'] ) ); elseif( isset( $arrValues['deals_value'] ) ) $this->setDealsValue( $arrValues['deals_value'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setWeek( $strWeek ) {
		$this->set( 'm_strWeek', CStrings::strTrimDef( $strWeek, -1, NULL, true ) );
	}

	public function getWeek() {
		return $this->m_strWeek;
	}

	public function sqlWeek() {
		return ( true == isset( $this->m_strWeek ) ) ? '\'' . $this->m_strWeek . '\'' : 'NOW()';
	}

	public function setDemos( $intDemos ) {
		$this->set( 'm_intDemos', CStrings::strToIntDef( $intDemos, NULL, false ) );
	}

	public function getDemos() {
		return $this->m_intDemos;
	}

	public function sqlDemos() {
		return ( true == isset( $this->m_intDemos ) ) ? ( string ) $this->m_intDemos : '0';
	}

	public function setUnitsDemoed( $intUnitsDemoed ) {
		$this->set( 'm_intUnitsDemoed', CStrings::strToIntDef( $intUnitsDemoed, NULL, false ) );
	}

	public function getUnitsDemoed() {
		return $this->m_intUnitsDemoed;
	}

	public function sqlUnitsDemoed() {
		return ( true == isset( $this->m_intUnitsDemoed ) ) ? ( string ) $this->m_intUnitsDemoed : '0';
	}

	public function setTotalOpportunityDollars( $intTotalOpportunityDollars ) {
		$this->set( 'm_intTotalOpportunityDollars', CStrings::strToIntDef( $intTotalOpportunityDollars, NULL, false ) );
	}

	public function getTotalOpportunityDollars() {
		return $this->m_intTotalOpportunityDollars;
	}

	public function sqlTotalOpportunityDollars() {
		return ( true == isset( $this->m_intTotalOpportunityDollars ) ) ? ( string ) $this->m_intTotalOpportunityDollars : '0';
	}

	public function setDealsClosed( $intDealsClosed ) {
		$this->set( 'm_intDealsClosed', CStrings::strToIntDef( $intDealsClosed, NULL, false ) );
	}

	public function getDealsClosed() {
		return $this->m_intDealsClosed;
	}

	public function sqlDealsClosed() {
		return ( true == isset( $this->m_intDealsClosed ) ) ? ( string ) $this->m_intDealsClosed : '0';
	}

	public function setDealsValue( $intDealsValue ) {
		$this->set( 'm_intDealsValue', CStrings::strToIntDef( $intDealsValue, NULL, false ) );
	}

	public function getDealsValue() {
		return $this->m_intDealsValue;
	}

	public function sqlDealsValue() {
		return ( true == isset( $this->m_intDealsValue ) ) ? ( string ) $this->m_intDealsValue : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, week, demos, units_demoed, total_opportunity_dollars, deals_closed, deals_value, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlWeek() . ', ' .
 						$this->sqlDemos() . ', ' .
 						$this->sqlUnitsDemoed() . ', ' .
 						$this->sqlTotalOpportunityDollars() . ', ' .
 						$this->sqlDealsClosed() . ', ' .
 						$this->sqlDealsValue() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' week = ' . $this->sqlWeek() . ','; } elseif( true == array_key_exists( 'Week', $this->getChangedColumns() ) ) { $strSql .= ' week = ' . $this->sqlWeek() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' demos = ' . $this->sqlDemos() . ','; } elseif( true == array_key_exists( 'Demos', $this->getChangedColumns() ) ) { $strSql .= ' demos = ' . $this->sqlDemos() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' units_demoed = ' . $this->sqlUnitsDemoed() . ','; } elseif( true == array_key_exists( 'UnitsDemoed', $this->getChangedColumns() ) ) { $strSql .= ' units_demoed = ' . $this->sqlUnitsDemoed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_opportunity_dollars = ' . $this->sqlTotalOpportunityDollars() . ','; } elseif( true == array_key_exists( 'TotalOpportunityDollars', $this->getChangedColumns() ) ) { $strSql .= ' total_opportunity_dollars = ' . $this->sqlTotalOpportunityDollars() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deals_closed = ' . $this->sqlDealsClosed() . ','; } elseif( true == array_key_exists( 'DealsClosed', $this->getChangedColumns() ) ) { $strSql .= ' deals_closed = ' . $this->sqlDealsClosed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deals_value = ' . $this->sqlDealsValue() . ','; } elseif( true == array_key_exists( 'DealsValue', $this->getChangedColumns() ) ) { $strSql .= ' deals_value = ' . $this->sqlDealsValue() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'week' => $this->getWeek(),
			'demos' => $this->getDemos(),
			'units_demoed' => $this->getUnitsDemoed(),
			'total_opportunity_dollars' => $this->getTotalOpportunityDollars(),
			'deals_closed' => $this->getDealsClosed(),
			'deals_value' => $this->getDealsValue(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>