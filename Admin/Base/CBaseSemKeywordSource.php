<?php

class CBaseSemKeywordSource extends CEosSingularBase {

	const TABLE_NAME = 'public.sem_keyword_sources';

	protected $m_intId;
	protected $m_intSemSourceId;
	protected $m_intSemAdGroupId;
	protected $m_intSemKeywordId;
	protected $m_intSemStatusTypeId;
	protected $m_fltAvgCostPerClick;
	protected $m_fltMaxCostPerClick;
	protected $m_strRemotePrimaryKey;
	protected $m_intEnforceExactMatch;
	protected $m_strSyncRequestedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltAvgCostPerClick = '0';
		$this->m_fltMaxCostPerClick = '0';
		$this->m_intEnforceExactMatch = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['sem_source_id'] ) && $boolDirectSet ) $this->set( 'm_intSemSourceId', trim( $arrValues['sem_source_id'] ) ); elseif( isset( $arrValues['sem_source_id'] ) ) $this->setSemSourceId( $arrValues['sem_source_id'] );
		if( isset( $arrValues['sem_ad_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdGroupId', trim( $arrValues['sem_ad_group_id'] ) ); elseif( isset( $arrValues['sem_ad_group_id'] ) ) $this->setSemAdGroupId( $arrValues['sem_ad_group_id'] );
		if( isset( $arrValues['sem_keyword_id'] ) && $boolDirectSet ) $this->set( 'm_intSemKeywordId', trim( $arrValues['sem_keyword_id'] ) ); elseif( isset( $arrValues['sem_keyword_id'] ) ) $this->setSemKeywordId( $arrValues['sem_keyword_id'] );
		if( isset( $arrValues['sem_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSemStatusTypeId', trim( $arrValues['sem_status_type_id'] ) ); elseif( isset( $arrValues['sem_status_type_id'] ) ) $this->setSemStatusTypeId( $arrValues['sem_status_type_id'] );
		if( isset( $arrValues['avg_cost_per_click'] ) && $boolDirectSet ) $this->set( 'm_fltAvgCostPerClick', trim( $arrValues['avg_cost_per_click'] ) ); elseif( isset( $arrValues['avg_cost_per_click'] ) ) $this->setAvgCostPerClick( $arrValues['avg_cost_per_click'] );
		if( isset( $arrValues['max_cost_per_click'] ) && $boolDirectSet ) $this->set( 'm_fltMaxCostPerClick', trim( $arrValues['max_cost_per_click'] ) ); elseif( isset( $arrValues['max_cost_per_click'] ) ) $this->setMaxCostPerClick( $arrValues['max_cost_per_click'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['enforce_exact_match'] ) && $boolDirectSet ) $this->set( 'm_intEnforceExactMatch', trim( $arrValues['enforce_exact_match'] ) ); elseif( isset( $arrValues['enforce_exact_match'] ) ) $this->setEnforceExactMatch( $arrValues['enforce_exact_match'] );
		if( isset( $arrValues['sync_requested_on'] ) && $boolDirectSet ) $this->set( 'm_strSyncRequestedOn', trim( $arrValues['sync_requested_on'] ) ); elseif( isset( $arrValues['sync_requested_on'] ) ) $this->setSyncRequestedOn( $arrValues['sync_requested_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSemSourceId( $intSemSourceId ) {
		$this->set( 'm_intSemSourceId', CStrings::strToIntDef( $intSemSourceId, NULL, false ) );
	}

	public function getSemSourceId() {
		return $this->m_intSemSourceId;
	}

	public function sqlSemSourceId() {
		return ( true == isset( $this->m_intSemSourceId ) ) ? ( string ) $this->m_intSemSourceId : 'NULL';
	}

	public function setSemAdGroupId( $intSemAdGroupId ) {
		$this->set( 'm_intSemAdGroupId', CStrings::strToIntDef( $intSemAdGroupId, NULL, false ) );
	}

	public function getSemAdGroupId() {
		return $this->m_intSemAdGroupId;
	}

	public function sqlSemAdGroupId() {
		return ( true == isset( $this->m_intSemAdGroupId ) ) ? ( string ) $this->m_intSemAdGroupId : 'NULL';
	}

	public function setSemKeywordId( $intSemKeywordId ) {
		$this->set( 'm_intSemKeywordId', CStrings::strToIntDef( $intSemKeywordId, NULL, false ) );
	}

	public function getSemKeywordId() {
		return $this->m_intSemKeywordId;
	}

	public function sqlSemKeywordId() {
		return ( true == isset( $this->m_intSemKeywordId ) ) ? ( string ) $this->m_intSemKeywordId : 'NULL';
	}

	public function setSemStatusTypeId( $intSemStatusTypeId ) {
		$this->set( 'm_intSemStatusTypeId', CStrings::strToIntDef( $intSemStatusTypeId, NULL, false ) );
	}

	public function getSemStatusTypeId() {
		return $this->m_intSemStatusTypeId;
	}

	public function sqlSemStatusTypeId() {
		return ( true == isset( $this->m_intSemStatusTypeId ) ) ? ( string ) $this->m_intSemStatusTypeId : 'NULL';
	}

	public function setAvgCostPerClick( $fltAvgCostPerClick ) {
		$this->set( 'm_fltAvgCostPerClick', CStrings::strToFloatDef( $fltAvgCostPerClick, NULL, false, 2 ) );
	}

	public function getAvgCostPerClick() {
		return $this->m_fltAvgCostPerClick;
	}

	public function sqlAvgCostPerClick() {
		return ( true == isset( $this->m_fltAvgCostPerClick ) ) ? ( string ) $this->m_fltAvgCostPerClick : '0';
	}

	public function setMaxCostPerClick( $fltMaxCostPerClick ) {
		$this->set( 'm_fltMaxCostPerClick', CStrings::strToFloatDef( $fltMaxCostPerClick, NULL, false, 2 ) );
	}

	public function getMaxCostPerClick() {
		return $this->m_fltMaxCostPerClick;
	}

	public function sqlMaxCostPerClick() {
		return ( true == isset( $this->m_fltMaxCostPerClick ) ) ? ( string ) $this->m_fltMaxCostPerClick : '0';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setEnforceExactMatch( $intEnforceExactMatch ) {
		$this->set( 'm_intEnforceExactMatch', CStrings::strToIntDef( $intEnforceExactMatch, NULL, false ) );
	}

	public function getEnforceExactMatch() {
		return $this->m_intEnforceExactMatch;
	}

	public function sqlEnforceExactMatch() {
		return ( true == isset( $this->m_intEnforceExactMatch ) ) ? ( string ) $this->m_intEnforceExactMatch : '1';
	}

	public function setSyncRequestedOn( $strSyncRequestedOn ) {
		$this->set( 'm_strSyncRequestedOn', CStrings::strTrimDef( $strSyncRequestedOn, -1, NULL, true ) );
	}

	public function getSyncRequestedOn() {
		return $this->m_strSyncRequestedOn;
	}

	public function sqlSyncRequestedOn() {
		return ( true == isset( $this->m_strSyncRequestedOn ) ) ? '\'' . $this->m_strSyncRequestedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, sem_source_id, sem_ad_group_id, sem_keyword_id, sem_status_type_id, avg_cost_per_click, max_cost_per_click, remote_primary_key, enforce_exact_match, sync_requested_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSemSourceId() . ', ' .
 						$this->sqlSemAdGroupId() . ', ' .
 						$this->sqlSemKeywordId() . ', ' .
 						$this->sqlSemStatusTypeId() . ', ' .
 						$this->sqlAvgCostPerClick() . ', ' .
 						$this->sqlMaxCostPerClick() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlEnforceExactMatch() . ', ' .
 						$this->sqlSyncRequestedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; } elseif( true == array_key_exists( 'SemSourceId', $this->getChangedColumns() ) ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; } elseif( true == array_key_exists( 'SemAdGroupId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId() . ','; } elseif( true == array_key_exists( 'SemKeywordId', $this->getChangedColumns() ) ) { $strSql .= ' sem_keyword_id = ' . $this->sqlSemKeywordId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_status_type_id = ' . $this->sqlSemStatusTypeId() . ','; } elseif( true == array_key_exists( 'SemStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' sem_status_type_id = ' . $this->sqlSemStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_cost_per_click = ' . $this->sqlAvgCostPerClick() . ','; } elseif( true == array_key_exists( 'AvgCostPerClick', $this->getChangedColumns() ) ) { $strSql .= ' avg_cost_per_click = ' . $this->sqlAvgCostPerClick() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_cost_per_click = ' . $this->sqlMaxCostPerClick() . ','; } elseif( true == array_key_exists( 'MaxCostPerClick', $this->getChangedColumns() ) ) { $strSql .= ' max_cost_per_click = ' . $this->sqlMaxCostPerClick() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enforce_exact_match = ' . $this->sqlEnforceExactMatch() . ','; } elseif( true == array_key_exists( 'EnforceExactMatch', $this->getChangedColumns() ) ) { $strSql .= ' enforce_exact_match = ' . $this->sqlEnforceExactMatch() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sync_requested_on = ' . $this->sqlSyncRequestedOn() . ','; } elseif( true == array_key_exists( 'SyncRequestedOn', $this->getChangedColumns() ) ) { $strSql .= ' sync_requested_on = ' . $this->sqlSyncRequestedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'sem_source_id' => $this->getSemSourceId(),
			'sem_ad_group_id' => $this->getSemAdGroupId(),
			'sem_keyword_id' => $this->getSemKeywordId(),
			'sem_status_type_id' => $this->getSemStatusTypeId(),
			'avg_cost_per_click' => $this->getAvgCostPerClick(),
			'max_cost_per_click' => $this->getMaxCostPerClick(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'enforce_exact_match' => $this->getEnforceExactMatch(),
			'sync_requested_on' => $this->getSyncRequestedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>