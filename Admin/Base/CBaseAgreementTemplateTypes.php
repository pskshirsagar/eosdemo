<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CAgreementTemplateTypes
 * Do not add any new functions to this class.
 */

class CBaseAgreementTemplateTypes extends CEosPluralBase {

	/**
	 * @return CAgreementTemplateType[]
	 */
	public static function fetchAgreementTemplateTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CAgreementTemplateType', $objDatabase );
	}

	/**
	 * @return CAgreementTemplateType
	 */
	public static function fetchAgreementTemplateType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CAgreementTemplateType', $objDatabase );
	}

	public static function fetchAgreementTemplateTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'agreement_template_types', $objDatabase );
	}

	public static function fetchAgreementTemplateTypeById( $intId, $objDatabase ) {
		return self::fetchAgreementTemplateType( sprintf( 'SELECT * FROM agreement_template_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>