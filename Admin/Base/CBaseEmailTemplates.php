<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmailTemplates
 * Do not add any new functions to this class.
 */

class CBaseEmailTemplates extends CEosPluralBase {

	/**
	 * @return CEmailTemplate[]
	 */
	public static function fetchEmailTemplates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmailTemplate', $objDatabase );
	}

	/**
	 * @return CEmailTemplate
	 */
	public static function fetchEmailTemplate( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailTemplate', $objDatabase );
	}

	public static function fetchEmailTemplateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_templates', $objDatabase );
	}

	public static function fetchEmailTemplateById( $intId, $objDatabase ) {
		return self::fetchEmailTemplate( sprintf( 'SELECT * FROM email_templates WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>