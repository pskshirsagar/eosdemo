<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeProducts
 * Do not add any new functions to this class.
 */

class CBaseEmployeeProducts extends CEosPluralBase {

	/**
	 * @return CEmployeeProduct[]
	 */
	public static function fetchEmployeeProducts( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeProduct', $objDatabase );
	}

	/**
	 * @return CEmployeeProduct
	 */
	public static function fetchEmployeeProduct( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeProduct', $objDatabase );
	}

	public static function fetchEmployeeProductCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_products', $objDatabase );
	}

	public static function fetchEmployeeProductById( $intId, $objDatabase ) {
		return self::fetchEmployeeProduct( sprintf( 'SELECT * FROM employee_products WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeProductsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeProducts( sprintf( 'SELECT * FROM employee_products WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeProductsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchEmployeeProducts( sprintf( 'SELECT * FROM employee_products WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>