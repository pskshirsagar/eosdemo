<?php

class CBaseDataPrivacyRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.data_privacy_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_arrintPropertyIds;
	protected $m_intCustomerDataRequestId;
	protected $m_intCustomerId;
	protected $m_strCustomerName;
	protected $m_intCompanyEmployeeId;
	protected $m_strCompanyEmployeeName;
	protected $m_strRequestDatetime;
	protected $m_intCompletedBy;
	protected $m_strCompletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDataPrivacyRequestType;
	protected $m_strStatus;
	protected $m_strReminderEmailSentOn;

	public function __construct() {
		parent::__construct();

		$this->m_strRequestDatetime = 'now()';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';
		$this->m_strDataPrivacyRequestType = 'Info';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintPropertyIds', trim( $arrValues['property_ids'] ) ); elseif( isset( $arrValues['property_ids'] ) ) $this->setPropertyIds( $arrValues['property_ids'] );
		if( isset( $arrValues['customer_data_request_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerDataRequestId', trim( $arrValues['customer_data_request_id'] ) ); elseif( isset( $arrValues['customer_data_request_id'] ) ) $this->setCustomerDataRequestId( $arrValues['customer_data_request_id'] );
		if( isset( $arrValues['customer_id'] ) && $boolDirectSet ) $this->set( 'm_intCustomerId', trim( $arrValues['customer_id'] ) ); elseif( isset( $arrValues['customer_id'] ) ) $this->setCustomerId( $arrValues['customer_id'] );
		if( isset( $arrValues['customer_name'] ) && $boolDirectSet ) $this->set( 'm_strCustomerName', trim( $arrValues['customer_name'] ) ); elseif( isset( $arrValues['customer_name'] ) ) $this->setCustomerName( $arrValues['customer_name'] );
		if( isset( $arrValues['company_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyEmployeeId', trim( $arrValues['company_employee_id'] ) ); elseif( isset( $arrValues['company_employee_id'] ) ) $this->setCompanyEmployeeId( $arrValues['company_employee_id'] );
		if( isset( $arrValues['company_employee_name'] ) && $boolDirectSet ) $this->set( 'm_strCompanyEmployeeName', trim( $arrValues['company_employee_name'] ) ); elseif( isset( $arrValues['company_employee_name'] ) ) $this->setCompanyEmployeeName( $arrValues['company_employee_name'] );
		if( isset( $arrValues['request_datetime'] ) && $boolDirectSet ) $this->set( 'm_strRequestDatetime', trim( $arrValues['request_datetime'] ) ); elseif( isset( $arrValues['request_datetime'] ) ) $this->setRequestDatetime( $arrValues['request_datetime'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['data_privacy_request_type'] ) && $boolDirectSet ) $this->set( 'm_strDataPrivacyRequestType', trim( stripcslashes( $arrValues['data_privacy_request_type'] ) ) ); elseif( isset( $arrValues['data_privacy_request_type'] ) ) $this->setDataPrivacyRequestType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['data_privacy_request_type'] ) : $arrValues['data_privacy_request_type'] );
		if( isset( $arrValues['status'] ) && $boolDirectSet ) $this->set( 'm_strStatus', trim( stripcslashes( $arrValues['status'] ) ) ); elseif( isset( $arrValues['status'] ) ) $this->setStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status'] ) : $arrValues['status'] );
		if( isset( $arrValues['reminder_email_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strReminderEmailSentOn', trim( $arrValues['reminder_email_sent_on'] ) ); elseif( isset( $arrValues['reminder_email_sent_on'] ) ) $this->setReminderEmailSentOn( $arrValues['reminder_email_sent_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyIds( $arrintPropertyIds ) {
		$this->set( 'm_arrintPropertyIds', CStrings::strToArrIntDef( $arrintPropertyIds, NULL ) );
	}

	public function getPropertyIds() {
		return $this->m_arrintPropertyIds;
	}

	public function sqlPropertyIds() {
		return ( true == isset( $this->m_arrintPropertyIds ) && true == valArr( $this->m_arrintPropertyIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintPropertyIds, NULL ) . '\'' : 'NULL';
	}

	public function setCustomerDataRequestId( $intCustomerDataRequestId ) {
		$this->set( 'm_intCustomerDataRequestId', CStrings::strToIntDef( $intCustomerDataRequestId, NULL, false ) );
	}

	public function getCustomerDataRequestId() {
		return $this->m_intCustomerDataRequestId;
	}

	public function sqlCustomerDataRequestId() {
		return ( true == isset( $this->m_intCustomerDataRequestId ) ) ? ( string ) $this->m_intCustomerDataRequestId : 'NULL';
	}

	public function setCustomerId( $intCustomerId ) {
		$this->set( 'm_intCustomerId', CStrings::strToIntDef( $intCustomerId, NULL, false ) );
	}

	public function getCustomerId() {
		return $this->m_intCustomerId;
	}

	public function sqlCustomerId() {
		return ( true == isset( $this->m_intCustomerId ) ) ? ( string ) $this->m_intCustomerId : 'NULL';
	}

	public function setCustomerName( $strCustomerName ) {
		$this->set( 'm_strCustomerName', CStrings::strTrimDef( $strCustomerName, 50, NULL, true ) );
	}

	public function getCustomerName() {
		return $this->m_strCustomerName;
	}

	public function sqlCustomerName() {
		return ( true == isset( $this->m_strCustomerName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCustomerName ) : '\'' . addslashes( $this->m_strCustomerName ) . '\'' ) : 'NULL';
	}

	public function setCompanyEmployeeId( $intCompanyEmployeeId ) {
		$this->set( 'm_intCompanyEmployeeId', CStrings::strToIntDef( $intCompanyEmployeeId, NULL, false ) );
	}

	public function getCompanyEmployeeId() {
		return $this->m_intCompanyEmployeeId;
	}

	public function sqlCompanyEmployeeId() {
		return ( true == isset( $this->m_intCompanyEmployeeId ) ) ? ( string ) $this->m_intCompanyEmployeeId : 'NULL';
	}

	public function setCompanyEmployeeName( $strCompanyEmployeeName ) {
		$this->set( 'm_strCompanyEmployeeName', CStrings::strTrimDef( $strCompanyEmployeeName, 75, NULL, true ) );
	}

	public function getCompanyEmployeeName() {
		return $this->m_strCompanyEmployeeName;
	}

	public function sqlCompanyEmployeeName() {
		return ( true == isset( $this->m_strCompanyEmployeeName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCompanyEmployeeName ) : '\'' . addslashes( $this->m_strCompanyEmployeeName ) . '\'' ) : 'NULL';
	}

	public function setRequestDatetime( $strRequestDatetime ) {
		$this->set( 'm_strRequestDatetime', CStrings::strTrimDef( $strRequestDatetime, -1, NULL, true ) );
	}

	public function getRequestDatetime() {
		return $this->m_strRequestDatetime;
	}

	public function sqlRequestDatetime() {
		return ( true == isset( $this->m_strRequestDatetime ) ) ? '\'' . $this->m_strRequestDatetime . '\'' : 'NOW()';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDataPrivacyRequestType( $strDataPrivacyRequestType ) {
		$this->set( 'm_strDataPrivacyRequestType', CStrings::strTrimDef( $strDataPrivacyRequestType, -1, NULL, true ) );
	}

	public function getDataPrivacyRequestType() {
		return $this->m_strDataPrivacyRequestType;
	}

	public function sqlDataPrivacyRequestType() {
		return ( true == isset( $this->m_strDataPrivacyRequestType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDataPrivacyRequestType ) : '\'' . addslashes( $this->m_strDataPrivacyRequestType ) . '\'' ) : '\'Info\'';
	}

	public function setStatus( $strStatus ) {
		$this->set( 'm_strStatus', CStrings::strTrimDef( $strStatus, -1, NULL, true ) );
	}

	public function getStatus() {
		return $this->m_strStatus;
	}

	public function sqlStatus() {
		return ( true == isset( $this->m_strStatus ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStatus ) : '\'' . addslashes( $this->m_strStatus ) . '\'' ) : 'NULL';
	}

	public function setReminderEmailSentOn( $strReminderEmailSentOn ) {
		$this->set( 'm_strReminderEmailSentOn', CStrings::strTrimDef( $strReminderEmailSentOn, -1, NULL, true ) );
	}

	public function getReminderEmailSentOn() {
		return $this->m_strReminderEmailSentOn;
	}

	public function sqlReminderEmailSentOn() {
		return ( true == isset( $this->m_strReminderEmailSentOn ) ) ? '\'' . $this->m_strReminderEmailSentOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_ids, customer_data_request_id, customer_id, customer_name, company_employee_id, company_employee_name, request_datetime, completed_by, completed_on, updated_by, updated_on, created_by, created_on, data_privacy_request_type, status, reminder_email_sent_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyIds() . ', ' .
						$this->sqlCustomerDataRequestId() . ', ' .
						$this->sqlCustomerId() . ', ' .
						$this->sqlCustomerName() . ', ' .
						$this->sqlCompanyEmployeeId() . ', ' .
						$this->sqlCompanyEmployeeName() . ', ' .
						$this->sqlRequestDatetime() . ', ' .
						$this->sqlCompletedBy() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDataPrivacyRequestType() . ', ' .
						$this->sqlStatus() . ', ' .
						$this->sqlReminderEmailSentOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_ids = ' . $this->sqlPropertyIds(). ',' ; } elseif( true == array_key_exists( 'PropertyIds', $this->getChangedColumns() ) ) { $strSql .= ' property_ids = ' . $this->sqlPropertyIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_data_request_id = ' . $this->sqlCustomerDataRequestId(). ',' ; } elseif( true == array_key_exists( 'CustomerDataRequestId', $this->getChangedColumns() ) ) { $strSql .= ' customer_data_request_id = ' . $this->sqlCustomerDataRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId(). ',' ; } elseif( true == array_key_exists( 'CustomerId', $this->getChangedColumns() ) ) { $strSql .= ' customer_id = ' . $this->sqlCustomerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' customer_name = ' . $this->sqlCustomerName(). ',' ; } elseif( true == array_key_exists( 'CustomerName', $this->getChangedColumns() ) ) { $strSql .= ' customer_name = ' . $this->sqlCustomerName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_id = ' . $this->sqlCompanyEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_employee_name = ' . $this->sqlCompanyEmployeeName(). ',' ; } elseif( true == array_key_exists( 'CompanyEmployeeName', $this->getChangedColumns() ) ) { $strSql .= ' company_employee_name = ' . $this->sqlCompanyEmployeeName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime(). ',' ; } elseif( true == array_key_exists( 'RequestDatetime', $this->getChangedColumns() ) ) { $strSql .= ' request_datetime = ' . $this->sqlRequestDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_privacy_request_type = ' . $this->sqlDataPrivacyRequestType(). ',' ; } elseif( true == array_key_exists( 'DataPrivacyRequestType', $this->getChangedColumns() ) ) { $strSql .= ' data_privacy_request_type = ' . $this->sqlDataPrivacyRequestType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status = ' . $this->sqlStatus(). ',' ; } elseif( true == array_key_exists( 'Status', $this->getChangedColumns() ) ) { $strSql .= ' status = ' . $this->sqlStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reminder_email_sent_on = ' . $this->sqlReminderEmailSentOn(). ',' ; } elseif( true == array_key_exists( 'ReminderEmailSentOn', $this->getChangedColumns() ) ) { $strSql .= ' reminder_email_sent_on = ' . $this->sqlReminderEmailSentOn() . ','; $boolUpdate = true; }

		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_ids' => $this->getPropertyIds(),
			'customer_data_request_id' => $this->getCustomerDataRequestId(),
			'customer_id' => $this->getCustomerId(),
			'customer_name' => $this->getCustomerName(),
			'company_employee_id' => $this->getCompanyEmployeeId(),
			'company_employee_name' => $this->getCompanyEmployeeName(),
			'request_datetime' => $this->getRequestDatetime(),
			'completed_by' => $this->getCompletedBy(),
			'completed_on' => $this->getCompletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'data_privacy_request_type' => $this->getDataPrivacyRequestType(),
			'status' => $this->getStatus(),
			'reminder_email_sent_on' => $this->getReminderEmailSentOn()
		);
	}

}
?>