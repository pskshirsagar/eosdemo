<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplateTasks
 * Do not add any new functions to this class.
 */

class CBaseSurveyTemplateTasks extends CEosPluralBase {

	/**
	 * @return CSurveyTemplateTask[]
	 */
	public static function fetchSurveyTemplateTasks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyTemplateTask', $objDatabase );
	}

	/**
	 * @return CSurveyTemplateTask
	 */
	public static function fetchSurveyTemplateTask( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyTemplateTask', $objDatabase );
	}

	public static function fetchSurveyTemplateTaskCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_template_tasks', $objDatabase );
	}

	public static function fetchSurveyTemplateTaskById( $intId, $objDatabase ) {
		return self::fetchSurveyTemplateTask( sprintf( 'SELECT * FROM survey_template_tasks WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveyTemplateTasksByResponseTaskTypeId( $intResponseTaskTypeId, $objDatabase ) {
		return self::fetchSurveyTemplateTasks( sprintf( 'SELECT * FROM survey_template_tasks WHERE response_task_type_id = %d', ( int ) $intResponseTaskTypeId ), $objDatabase );
	}

	public static function fetchSurveyTemplateTasksByResponseTaskPriorityId( $intResponseTaskPriorityId, $objDatabase ) {
		return self::fetchSurveyTemplateTasks( sprintf( 'SELECT * FROM survey_template_tasks WHERE response_task_priority_id = %d', ( int ) $intResponseTaskPriorityId ), $objDatabase );
	}

	public static function fetchSurveyTemplateTasksByResponseTaskDepartmentId( $intResponseTaskDepartmentId, $objDatabase ) {
		return self::fetchSurveyTemplateTasks( sprintf( 'SELECT * FROM survey_template_tasks WHERE response_task_department_id = %d', ( int ) $intResponseTaskDepartmentId ), $objDatabase );
	}

	public static function fetchSurveyTemplateTasksByResponseTaskEmployeeId( $intResponseTaskEmployeeId, $objDatabase ) {
		return self::fetchSurveyTemplateTasks( sprintf( 'SELECT * FROM survey_template_tasks WHERE response_task_employee_id = %d', ( int ) $intResponseTaskEmployeeId ), $objDatabase );
	}

}
?>