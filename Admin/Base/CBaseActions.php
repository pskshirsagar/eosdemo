<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CActions
 * Do not add any new functions to this class.
 */

class CBaseActions extends CEosPluralBase {

	/**
	 * @return CAction[]
	 */
	public static function fetchActions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CAction::class, $objDatabase );
	}

	/**
	 * @return CAction
	 */
	public static function fetchAction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CAction::class, $objDatabase );
	}

	public static function fetchActionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'actions', $objDatabase );
	}

	public static function fetchActionById( $intId, $objDatabase ) {
		return self::fetchAction( sprintf( 'SELECT * FROM actions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchActionsByActionTypeId( $intActionTypeId, $objDatabase ) {
		return self::fetchActions( sprintf( 'SELECT * FROM actions WHERE action_type_id = %d', ( int ) $intActionTypeId ), $objDatabase );
	}

	public static function fetchActionsByActionResultId( $intActionResultId, $objDatabase ) {
		return self::fetchActions( sprintf( 'SELECT * FROM actions WHERE action_result_id = %d', ( int ) $intActionResultId ), $objDatabase );
	}

	public static function fetchActionsByOldStatusTypeId( $intOldStatusTypeId, $objDatabase ) {
		return self::fetchActions( sprintf( 'SELECT * FROM actions WHERE old_status_type_id = %d', ( int ) $intOldStatusTypeId ), $objDatabase );
	}

	public static function fetchActionsByNewStatusTypeId( $intNewStatusTypeId, $objDatabase ) {
		return self::fetchActions( sprintf( 'SELECT * FROM actions WHERE new_status_type_id = %d', ( int ) $intNewStatusTypeId ), $objDatabase );
	}

	public static function fetchActionsByImplementationDelayTypeId( $intImplementationDelayTypeId, $objDatabase ) {
		return self::fetchActions( sprintf( 'SELECT * FROM actions WHERE implementation_delay_type_id = %d', ( int ) $intImplementationDelayTypeId ), $objDatabase );
	}

	public static function fetchActionsByPsLeadId( $intPsLeadId, $objDatabase ) {
		return self::fetchActions( sprintf( 'SELECT * FROM actions WHERE ps_lead_id = %d', ( int ) $intPsLeadId ), $objDatabase );
	}

	public static function fetchActionsByContractId( $intContractId, $objDatabase ) {
		return self::fetchActions( sprintf( 'SELECT * FROM actions WHERE contract_id = %d', ( int ) $intContractId ), $objDatabase );
	}

	public static function fetchActionsByPsLeadEventId( $intPsLeadEventId, $objDatabase ) {
		return self::fetchActions( sprintf( 'SELECT * FROM actions WHERE ps_lead_event_id = %d', ( int ) $intPsLeadEventId ), $objDatabase );
	}

	public static function fetchActionsByPersonId( $intPersonId, $objDatabase ) {
		return self::fetchActions( sprintf( 'SELECT * FROM actions WHERE person_id = %d', ( int ) $intPersonId ), $objDatabase );
	}

	public static function fetchActionsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchActions( sprintf( 'SELECT * FROM actions WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchActionsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchActions( sprintf( 'SELECT * FROM actions WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchActionsByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchActions( sprintf( 'SELECT * FROM actions WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

}
?>