<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsJobTypes
 * Do not add any new functions to this class.
 */

class CBasePsJobTypes extends CEosPluralBase {

	/**
	 * @return CPsJobType[]
	 */
	public static function fetchPsJobTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsJobType', $objDatabase );
	}

	/**
	 * @return CPsJobType
	 */
	public static function fetchPsJobType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsJobType', $objDatabase );
	}

	public static function fetchPsJobTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_job_types', $objDatabase );
	}

	public static function fetchPsJobTypeById( $intId, $objDatabase ) {
		return self::fetchPsJobType( sprintf( 'SELECT * FROM ps_job_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsJobTypesByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchPsJobTypes( sprintf( 'SELECT * FROM ps_job_types WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>