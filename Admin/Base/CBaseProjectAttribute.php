<?php

class CBaseProjectAttribute extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.project_attributes';

	protected $m_intId;
	protected $m_intProjectAttributeId;
	protected $m_intProjectAttributeTypeId;
	protected $m_intProjectId;
	protected $m_intClientId;
	protected $m_strDescription;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['project_attribute_id'] ) && $boolDirectSet ) $this->set( 'm_intProjectAttributeId', trim( $arrValues['project_attribute_id'] ) ); elseif( isset( $arrValues['project_attribute_id'] ) ) $this->setProjectAttributeId( $arrValues['project_attribute_id'] );
		if( isset( $arrValues['project_attribute_type_id'] ) && $boolDirectSet ) $this->set( 'm_intProjectAttributeTypeId', trim( $arrValues['project_attribute_type_id'] ) ); elseif( isset( $arrValues['project_attribute_type_id'] ) ) $this->setProjectAttributeTypeId( $arrValues['project_attribute_type_id'] );
		if( isset( $arrValues['project_id'] ) && $boolDirectSet ) $this->set( 'm_intProjectId', trim( $arrValues['project_id'] ) ); elseif( isset( $arrValues['project_id'] ) ) $this->setProjectId( $arrValues['project_id'] );
		if( isset( $arrValues['client_id'] ) && $boolDirectSet ) $this->set( 'm_intClientId', trim( $arrValues['client_id'] ) ); elseif( isset( $arrValues['client_id'] ) ) $this->setClientId( $arrValues['client_id'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setProjectAttributeId( $intProjectAttributeId ) {
		$this->set( 'm_intProjectAttributeId', CStrings::strToIntDef( $intProjectAttributeId, NULL, false ) );
	}

	public function getProjectAttributeId() {
		return $this->m_intProjectAttributeId;
	}

	public function sqlProjectAttributeId() {
		return ( true == isset( $this->m_intProjectAttributeId ) ) ? ( string ) $this->m_intProjectAttributeId : 'NULL';
	}

	public function setProjectAttributeTypeId( $intProjectAttributeTypeId ) {
		$this->set( 'm_intProjectAttributeTypeId', CStrings::strToIntDef( $intProjectAttributeTypeId, NULL, false ) );
	}

	public function getProjectAttributeTypeId() {
		return $this->m_intProjectAttributeTypeId;
	}

	public function sqlProjectAttributeTypeId() {
		return ( true == isset( $this->m_intProjectAttributeTypeId ) ) ? ( string ) $this->m_intProjectAttributeTypeId : 'NULL';
	}

	public function setProjectId( $intProjectId ) {
		$this->set( 'm_intProjectId', CStrings::strToIntDef( $intProjectId, NULL, false ) );
	}

	public function getProjectId() {
		return $this->m_intProjectId;
	}

	public function sqlProjectId() {
		return ( true == isset( $this->m_intProjectId ) ) ? ( string ) $this->m_intProjectId : 'NULL';
	}

	public function setClientId( $intClientId ) {
		$this->set( 'm_intClientId', CStrings::strToIntDef( $intClientId, NULL, false ) );
	}

	public function getClientId() {
		return $this->m_intClientId;
	}

	public function sqlClientId() {
		return ( true == isset( $this->m_intClientId ) ) ? ( string ) $this->m_intClientId : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, project_attribute_id, project_attribute_type_id, project_id, client_id, description, details, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlProjectAttributeId() . ', ' .
						$this->sqlProjectAttributeTypeId() . ', ' .
						$this->sqlProjectId() . ', ' .
						$this->sqlClientId() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDetails() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_attribute_id = ' . $this->sqlProjectAttributeId(). ',' ; } elseif( true == array_key_exists( 'ProjectAttributeId', $this->getChangedColumns() ) ) { $strSql .= ' project_attribute_id = ' . $this->sqlProjectAttributeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_attribute_type_id = ' . $this->sqlProjectAttributeTypeId(). ',' ; } elseif( true == array_key_exists( 'ProjectAttributeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' project_attribute_type_id = ' . $this->sqlProjectAttributeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_id = ' . $this->sqlProjectId(). ',' ; } elseif( true == array_key_exists( 'ProjectId', $this->getChangedColumns() ) ) { $strSql .= ' project_id = ' . $this->sqlProjectId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_id = ' . $this->sqlClientId(). ',' ; } elseif( true == array_key_exists( 'ClientId', $this->getChangedColumns() ) ) { $strSql .= ' client_id = ' . $this->sqlClientId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails() ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() ; $boolUpdate = true; }
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'project_attribute_id' => $this->getProjectAttributeId(),
			'project_attribute_type_id' => $this->getProjectAttributeTypeId(),
			'project_id' => $this->getProjectId(),
			'client_id' => $this->getClientId(),
			'description' => $this->getDescription(),
			'details' => $this->getDetails(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>