<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportChangeHistories
 * Do not add any new functions to this class.
 */

class CBaseReportChangeHistories extends CEosPluralBase {

	/**
	 * @return CReportChangeHistory[]
	 */
	public static function fetchReportChangeHistories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportChangeHistory::class, $objDatabase );
	}

	/**
	 * @return CReportChangeHistory
	 */
	public static function fetchReportChangeHistory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportChangeHistory::class, $objDatabase );
	}

	public static function fetchReportChangeHistoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_change_histories', $objDatabase );
	}

	public static function fetchReportChangeHistoryById( $intId, $objDatabase ) {
		return self::fetchReportChangeHistory( sprintf( 'SELECT * FROM report_change_histories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportChangeHistoriesByCompanyReportId( $intCompanyReportId, $objDatabase ) {
		return self::fetchReportChangeHistories( sprintf( 'SELECT * FROM report_change_histories WHERE company_report_id = %d', ( int ) $intCompanyReportId ), $objDatabase );
	}

}
?>