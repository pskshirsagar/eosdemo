<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTests
 * Do not add any new functions to this class.
 */

class CBaseTests extends CEosPluralBase {

	/**
	 * @return CTest[]
	 */
	public static function fetchTests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTest', $objDatabase );
	}

	/**
	 * @return CTest
	 */
	public static function fetchTest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTest', $objDatabase );
	}

	public static function fetchTestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'tests', $objDatabase );
	}

	public static function fetchTestById( $intId, $objDatabase ) {
		return self::fetchTest( sprintf( 'SELECT * FROM tests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestsByTestTypeId( $intTestTypeId, $objDatabase ) {
		return self::fetchTests( sprintf( 'SELECT * FROM tests WHERE test_type_id = %d', ( int ) $intTestTypeId ), $objDatabase );
	}

	public static function fetchTestsByTrainingSessionId( $intTrainingSessionId, $objDatabase ) {
		return self::fetchTests( sprintf( 'SELECT * FROM tests WHERE training_session_id = %d', ( int ) $intTrainingSessionId ), $objDatabase );
	}

	public static function fetchTestsByTestLevelTypeId( $intTestLevelTypeId, $objDatabase ) {
		return self::fetchTests( sprintf( 'SELECT * FROM tests WHERE test_level_type_id = %d', ( int ) $intTestLevelTypeId ), $objDatabase );
	}

}
?>