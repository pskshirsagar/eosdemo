<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPurchaseRequests
 * Do not add any new functions to this class.
 */

class CBasePurchaseRequests extends CEosPluralBase {

	/**
	 * @return CPurchaseRequest[]
	 */
	public static function fetchPurchaseRequests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPurchaseRequest', $objDatabase );
	}

	/**
	 * @return CPurchaseRequest
	 */
	public static function fetchPurchaseRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPurchaseRequest', $objDatabase );
	}

	public static function fetchPurchaseRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'purchase_requests', $objDatabase );
	}

	public static function fetchPurchaseRequestById( $intId, $objDatabase ) {
		return self::fetchPurchaseRequest( sprintf( 'SELECT * FROM purchase_requests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPurchaseRequestsByPurchaseRequestTypeId( $intPurchaseRequestTypeId, $objDatabase ) {
		return self::fetchPurchaseRequests( sprintf( 'SELECT * FROM purchase_requests WHERE purchase_request_type_id = %d', ( int ) $intPurchaseRequestTypeId ), $objDatabase );
	}

	public static function fetchPurchaseRequestsByPurchaseRequestStatusTypeId( $intPurchaseRequestStatusTypeId, $objDatabase ) {
		return self::fetchPurchaseRequests( sprintf( 'SELECT * FROM purchase_requests WHERE purchase_request_status_type_id = %d', ( int ) $intPurchaseRequestStatusTypeId ), $objDatabase );
	}

	public static function fetchPurchaseRequestsByApproverEmployeeId( $intApproverEmployeeId, $objDatabase ) {
		return self::fetchPurchaseRequests( sprintf( 'SELECT * FROM purchase_requests WHERE approver_employee_id = %d', ( int ) $intApproverEmployeeId ), $objDatabase );
	}

	public static function fetchPurchaseRequestsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchPurchaseRequests( sprintf( 'SELECT * FROM purchase_requests WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchPurchaseRequestsByPsAssetTypeId( $intPsAssetTypeId, $objDatabase ) {
		return self::fetchPurchaseRequests( sprintf( 'SELECT * FROM purchase_requests WHERE ps_asset_type_id = %d', ( int ) $intPsAssetTypeId ), $objDatabase );
	}

	public static function fetchPurchaseRequestsByCurrentSalaryTypeId( $intCurrentSalaryTypeId, $objDatabase ) {
		return self::fetchPurchaseRequests( sprintf( 'SELECT * FROM purchase_requests WHERE current_salary_type_id = %d', ( int ) $intCurrentSalaryTypeId ), $objDatabase );
	}

	public static function fetchPurchaseRequestsByProposedSalaryTypeId( $intProposedSalaryTypeId, $objDatabase ) {
		return self::fetchPurchaseRequests( sprintf( 'SELECT * FROM purchase_requests WHERE proposed_salary_type_id = %d', ( int ) $intProposedSalaryTypeId ), $objDatabase );
	}

	public static function fetchPurchaseRequestsByPurchaseRequestId( $intPurchaseRequestId, $objDatabase ) {
		return self::fetchPurchaseRequests( sprintf( 'SELECT * FROM purchase_requests WHERE purchase_request_id = %d', ( int ) $intPurchaseRequestId ), $objDatabase );
	}

	public static function fetchPurchaseRequestsByBonusTypeId( $intBonusTypeId, $objDatabase ) {
		return self::fetchPurchaseRequests( sprintf( 'SELECT * FROM purchase_requests WHERE bonus_type_id = %d', ( int ) $intBonusTypeId ), $objDatabase );
	}

	public static function fetchPurchaseRequestsByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchPurchaseRequests( sprintf( 'SELECT * FROM purchase_requests WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

	public static function fetchPurchaseRequestsByAnnualBonusTypeId( $intAnnualBonusTypeId, $objDatabase ) {
		return self::fetchPurchaseRequests( sprintf( 'SELECT * FROM purchase_requests WHERE annual_bonus_type_id = %d', ( int ) $intAnnualBonusTypeId ), $objDatabase );
	}

}
?>