<?php

class CBaseEmployeeApplicationDemographic extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_application_demographics';

	protected $m_intId;
	protected $m_intEmployeeApplicationId;
	protected $m_intEmployeeApplicationEthnicityId;
	protected $m_strMaritalStatus;
	protected $m_strGender;
	protected $m_strMonth;
	protected $m_intHasDisability;
	protected $m_intIsVietnamVeteren;
	protected $m_intIsOtherProtectedVeteran;
	protected $m_intIsDisabilityVeteren;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationId', trim( $arrValues['employee_application_id'] ) ); elseif( isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( isset( $arrValues['employee_application_ethnicity_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationEthnicityId', trim( $arrValues['employee_application_ethnicity_id'] ) ); elseif( isset( $arrValues['employee_application_ethnicity_id'] ) ) $this->setEmployeeApplicationEthnicityId( $arrValues['employee_application_ethnicity_id'] );
		if( isset( $arrValues['marital_status'] ) && $boolDirectSet ) $this->set( 'm_strMaritalStatus', trim( stripcslashes( $arrValues['marital_status'] ) ) ); elseif( isset( $arrValues['marital_status'] ) ) $this->setMaritalStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['marital_status'] ) : $arrValues['marital_status'] );
		if( isset( $arrValues['gender'] ) && $boolDirectSet ) $this->set( 'm_strGender', trim( stripcslashes( $arrValues['gender'] ) ) ); elseif( isset( $arrValues['gender'] ) ) $this->setGender( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['gender'] ) : $arrValues['gender'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['has_disability'] ) && $boolDirectSet ) $this->set( 'm_intHasDisability', trim( $arrValues['has_disability'] ) ); elseif( isset( $arrValues['has_disability'] ) ) $this->setHasDisability( $arrValues['has_disability'] );
		if( isset( $arrValues['is_vietnam_veteren'] ) && $boolDirectSet ) $this->set( 'm_intIsVietnamVeteren', trim( $arrValues['is_vietnam_veteren'] ) ); elseif( isset( $arrValues['is_vietnam_veteren'] ) ) $this->setIsVietnamVeteren( $arrValues['is_vietnam_veteren'] );
		if( isset( $arrValues['is_other_protected_veteran'] ) && $boolDirectSet ) $this->set( 'm_intIsOtherProtectedVeteran', trim( $arrValues['is_other_protected_veteran'] ) ); elseif( isset( $arrValues['is_other_protected_veteran'] ) ) $this->setIsOtherProtectedVeteran( $arrValues['is_other_protected_veteran'] );
		if( isset( $arrValues['is_disability_veteren'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabilityVeteren', trim( $arrValues['is_disability_veteren'] ) ); elseif( isset( $arrValues['is_disability_veteren'] ) ) $this->setIsDisabilityVeteren( $arrValues['is_disability_veteren'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeApplicationId( $intEmployeeApplicationId ) {
		$this->set( 'm_intEmployeeApplicationId', CStrings::strToIntDef( $intEmployeeApplicationId, NULL, false ) );
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function sqlEmployeeApplicationId() {
		return ( true == isset( $this->m_intEmployeeApplicationId ) ) ? ( string ) $this->m_intEmployeeApplicationId : 'NULL';
	}

	public function setEmployeeApplicationEthnicityId( $intEmployeeApplicationEthnicityId ) {
		$this->set( 'm_intEmployeeApplicationEthnicityId', CStrings::strToIntDef( $intEmployeeApplicationEthnicityId, NULL, false ) );
	}

	public function getEmployeeApplicationEthnicityId() {
		return $this->m_intEmployeeApplicationEthnicityId;
	}

	public function sqlEmployeeApplicationEthnicityId() {
		return ( true == isset( $this->m_intEmployeeApplicationEthnicityId ) ) ? ( string ) $this->m_intEmployeeApplicationEthnicityId : 'NULL';
	}

	public function setMaritalStatus( $strMaritalStatus ) {
		$this->set( 'm_strMaritalStatus', CStrings::strTrimDef( $strMaritalStatus, 10, NULL, true ) );
	}

	public function getMaritalStatus() {
		return $this->m_strMaritalStatus;
	}

	public function sqlMaritalStatus() {
		return ( true == isset( $this->m_strMaritalStatus ) ) ? '\'' . addslashes( $this->m_strMaritalStatus ) . '\'' : 'NULL';
	}

	public function setGender( $strGender ) {
		$this->set( 'm_strGender', CStrings::strTrimDef( $strGender, 1, NULL, true ) );
	}

	public function getGender() {
		return $this->m_strGender;
	}

	public function sqlGender() {
		return ( true == isset( $this->m_strGender ) ) ? '\'' . addslashes( $this->m_strGender ) . '\'' : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NULL';
	}

	public function setHasDisability( $intHasDisability ) {
		$this->set( 'm_intHasDisability', CStrings::strToIntDef( $intHasDisability, NULL, false ) );
	}

	public function getHasDisability() {
		return $this->m_intHasDisability;
	}

	public function sqlHasDisability() {
		return ( true == isset( $this->m_intHasDisability ) ) ? ( string ) $this->m_intHasDisability : 'NULL';
	}

	public function setIsVietnamVeteren( $intIsVietnamVeteren ) {
		$this->set( 'm_intIsVietnamVeteren', CStrings::strToIntDef( $intIsVietnamVeteren, NULL, false ) );
	}

	public function getIsVietnamVeteren() {
		return $this->m_intIsVietnamVeteren;
	}

	public function sqlIsVietnamVeteren() {
		return ( true == isset( $this->m_intIsVietnamVeteren ) ) ? ( string ) $this->m_intIsVietnamVeteren : 'NULL';
	}

	public function setIsOtherProtectedVeteran( $intIsOtherProtectedVeteran ) {
		$this->set( 'm_intIsOtherProtectedVeteran', CStrings::strToIntDef( $intIsOtherProtectedVeteran, NULL, false ) );
	}

	public function getIsOtherProtectedVeteran() {
		return $this->m_intIsOtherProtectedVeteran;
	}

	public function sqlIsOtherProtectedVeteran() {
		return ( true == isset( $this->m_intIsOtherProtectedVeteran ) ) ? ( string ) $this->m_intIsOtherProtectedVeteran : 'NULL';
	}

	public function setIsDisabilityVeteren( $intIsDisabilityVeteren ) {
		$this->set( 'm_intIsDisabilityVeteren', CStrings::strToIntDef( $intIsDisabilityVeteren, NULL, false ) );
	}

	public function getIsDisabilityVeteren() {
		return $this->m_intIsDisabilityVeteren;
	}

	public function sqlIsDisabilityVeteren() {
		return ( true == isset( $this->m_intIsDisabilityVeteren ) ) ? ( string ) $this->m_intIsDisabilityVeteren : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_application_id, employee_application_ethnicity_id, marital_status, gender, month, has_disability, is_vietnam_veteren, is_other_protected_veteran, is_disability_veteren, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeApplicationId() . ', ' .
 						$this->sqlEmployeeApplicationEthnicityId() . ', ' .
 						$this->sqlMaritalStatus() . ', ' .
 						$this->sqlGender() . ', ' .
 						$this->sqlMonth() . ', ' .
 						$this->sqlHasDisability() . ', ' .
 						$this->sqlIsVietnamVeteren() . ', ' .
 						$this->sqlIsOtherProtectedVeteran() . ', ' .
 						$this->sqlIsDisabilityVeteren() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_ethnicity_id = ' . $this->sqlEmployeeApplicationEthnicityId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationEthnicityId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_ethnicity_id = ' . $this->sqlEmployeeApplicationEthnicityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marital_status = ' . $this->sqlMaritalStatus() . ','; } elseif( true == array_key_exists( 'MaritalStatus', $this->getChangedColumns() ) ) { $strSql .= ' marital_status = ' . $this->sqlMaritalStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; } elseif( true == array_key_exists( 'Gender', $this->getChangedColumns() ) ) { $strSql .= ' gender = ' . $this->sqlGender() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; } elseif( true == array_key_exists( 'Month', $this->getChangedColumns() ) ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_disability = ' . $this->sqlHasDisability() . ','; } elseif( true == array_key_exists( 'HasDisability', $this->getChangedColumns() ) ) { $strSql .= ' has_disability = ' . $this->sqlHasDisability() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_vietnam_veteren = ' . $this->sqlIsVietnamVeteren() . ','; } elseif( true == array_key_exists( 'IsVietnamVeteren', $this->getChangedColumns() ) ) { $strSql .= ' is_vietnam_veteren = ' . $this->sqlIsVietnamVeteren() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_other_protected_veteran = ' . $this->sqlIsOtherProtectedVeteran() . ','; } elseif( true == array_key_exists( 'IsOtherProtectedVeteran', $this->getChangedColumns() ) ) { $strSql .= ' is_other_protected_veteran = ' . $this->sqlIsOtherProtectedVeteran() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disability_veteren = ' . $this->sqlIsDisabilityVeteren() . ','; } elseif( true == array_key_exists( 'IsDisabilityVeteren', $this->getChangedColumns() ) ) { $strSql .= ' is_disability_veteren = ' . $this->sqlIsDisabilityVeteren() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_application_id' => $this->getEmployeeApplicationId(),
			'employee_application_ethnicity_id' => $this->getEmployeeApplicationEthnicityId(),
			'marital_status' => $this->getMaritalStatus(),
			'gender' => $this->getGender(),
			'month' => $this->getMonth(),
			'has_disability' => $this->getHasDisability(),
			'is_vietnam_veteren' => $this->getIsVietnamVeteren(),
			'is_other_protected_veteran' => $this->getIsOtherProtectedVeteran(),
			'is_disability_veteren' => $this->getIsDisabilityVeteren(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>