<?php

class CBaseUserModuleLog extends CEosSingularBase {

	const TABLE_NAME = 'public.user_module_logs';

	protected $m_intId;
	protected $m_intUserId;
	protected $m_strHour;
	protected $m_strModuleName;
	protected $m_strActionName;
	protected $m_intClickCount;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intClickCount = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['hour'] ) && $boolDirectSet ) $this->set( 'm_strHour', trim( $arrValues['hour'] ) ); elseif( isset( $arrValues['hour'] ) ) $this->setHour( $arrValues['hour'] );
		if( isset( $arrValues['module_name'] ) && $boolDirectSet ) $this->set( 'm_strModuleName', trim( stripcslashes( $arrValues['module_name'] ) ) ); elseif( isset( $arrValues['module_name'] ) ) $this->setModuleName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['module_name'] ) : $arrValues['module_name'] );
		if( isset( $arrValues['action_name'] ) && $boolDirectSet ) $this->set( 'm_strActionName', trim( stripcslashes( $arrValues['action_name'] ) ) ); elseif( isset( $arrValues['action_name'] ) ) $this->setActionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action_name'] ) : $arrValues['action_name'] );
		if( isset( $arrValues['click_count'] ) && $boolDirectSet ) $this->set( 'm_intClickCount', trim( $arrValues['click_count'] ) ); elseif( isset( $arrValues['click_count'] ) ) $this->setClickCount( $arrValues['click_count'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setHour( $strHour ) {
		$this->set( 'm_strHour', CStrings::strTrimDef( $strHour, -1, NULL, true ) );
	}

	public function getHour() {
		return $this->m_strHour;
	}

	public function sqlHour() {
		return ( true == isset( $this->m_strHour ) ) ? '\'' . $this->m_strHour . '\'' : 'NOW()';
	}

	public function setModuleName( $strModuleName ) {
		$this->set( 'm_strModuleName', CStrings::strTrimDef( $strModuleName, -1, NULL, true ) );
	}

	public function getModuleName() {
		return $this->m_strModuleName;
	}

	public function sqlModuleName() {
		return ( true == isset( $this->m_strModuleName ) ) ? '\'' . addslashes( $this->m_strModuleName ) . '\'' : 'NULL';
	}

	public function setActionName( $strActionName ) {
		$this->set( 'm_strActionName', CStrings::strTrimDef( $strActionName, -1, NULL, true ) );
	}

	public function getActionName() {
		return $this->m_strActionName;
	}

	public function sqlActionName() {
		return ( true == isset( $this->m_strActionName ) ) ? '\'' . addslashes( $this->m_strActionName ) . '\'' : 'NULL';
	}

	public function setClickCount( $intClickCount ) {
		$this->set( 'm_intClickCount', CStrings::strToIntDef( $intClickCount, NULL, false ) );
	}

	public function getClickCount() {
		return $this->m_intClickCount;
	}

	public function sqlClickCount() {
		return ( true == isset( $this->m_intClickCount ) ) ? ( string ) $this->m_intClickCount : '1';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, user_id, hour, module_name, action_name, click_count, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlUserId() . ', ' .
 						$this->sqlHour() . ', ' .
 						$this->sqlModuleName() . ', ' .
 						$this->sqlActionName() . ', ' .
 						$this->sqlClickCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hour = ' . $this->sqlHour() . ','; } elseif( true == array_key_exists( 'Hour', $this->getChangedColumns() ) ) { $strSql .= ' hour = ' . $this->sqlHour() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' module_name = ' . $this->sqlModuleName() . ','; } elseif( true == array_key_exists( 'ModuleName', $this->getChangedColumns() ) ) { $strSql .= ' module_name = ' . $this->sqlModuleName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_name = ' . $this->sqlActionName() . ','; } elseif( true == array_key_exists( 'ActionName', $this->getChangedColumns() ) ) { $strSql .= ' action_name = ' . $this->sqlActionName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' click_count = ' . $this->sqlClickCount() . ','; } elseif( true == array_key_exists( 'ClickCount', $this->getChangedColumns() ) ) { $strSql .= ' click_count = ' . $this->sqlClickCount() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'user_id' => $this->getUserId(),
			'hour' => $this->getHour(),
			'module_name' => $this->getModuleName(),
			'action_name' => $this->getActionName(),
			'click_count' => $this->getClickCount(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>