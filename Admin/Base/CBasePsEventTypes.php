<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsEventTypes
 * Do not add any new functions to this class.
 */

class CBasePsEventTypes extends CEosPluralBase {

	/**
	 * @return CPsEventType[]
	 */
	public static function fetchPsEventTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsEventType', $objDatabase );
	}

	/**
	 * @return CPsEventType
	 */
	public static function fetchPsEventType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsEventType', $objDatabase );
	}

	public static function fetchPsEventTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_event_types', $objDatabase );
	}

	public static function fetchPsEventTypeById( $intId, $objDatabase ) {
		return self::fetchPsEventType( sprintf( 'SELECT * FROM ps_event_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>