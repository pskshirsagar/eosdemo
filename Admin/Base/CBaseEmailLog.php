<?php

class CBaseEmailLog extends CEosSingularBase {

	const TABLE_NAME = 'public.email_logs';

	protected $m_intId;
	protected $m_intFromEmployeeId;
	protected $m_intToEmployeeId;
	protected $m_intRemotePrimaryKey;
	protected $m_strToEmailAddress;
	protected $m_strLogDatetime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['from_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intFromEmployeeId', trim( $arrValues['from_employee_id'] ) ); elseif( isset( $arrValues['from_employee_id'] ) ) $this->setFromEmployeeId( $arrValues['from_employee_id'] );
		if( isset( $arrValues['to_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intToEmployeeId', trim( $arrValues['to_employee_id'] ) ); elseif( isset( $arrValues['to_employee_id'] ) ) $this->setToEmployeeId( $arrValues['to_employee_id'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_intRemotePrimaryKey', trim( $arrValues['remote_primary_key'] ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( $arrValues['remote_primary_key'] );
		if( isset( $arrValues['to_email_address'] ) && $boolDirectSet ) $this->set( 'm_strToEmailAddress', trim( stripcslashes( $arrValues['to_email_address'] ) ) ); elseif( isset( $arrValues['to_email_address'] ) ) $this->setToEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['to_email_address'] ) : $arrValues['to_email_address'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setFromEmployeeId( $intFromEmployeeId ) {
		$this->set( 'm_intFromEmployeeId', CStrings::strToIntDef( $intFromEmployeeId, NULL, false ) );
	}

	public function getFromEmployeeId() {
		return $this->m_intFromEmployeeId;
	}

	public function sqlFromEmployeeId() {
		return ( true == isset( $this->m_intFromEmployeeId ) ) ? ( string ) $this->m_intFromEmployeeId : 'NULL';
	}

	public function setToEmployeeId( $intToEmployeeId ) {
		$this->set( 'm_intToEmployeeId', CStrings::strToIntDef( $intToEmployeeId, NULL, false ) );
	}

	public function getToEmployeeId() {
		return $this->m_intToEmployeeId;
	}

	public function sqlToEmployeeId() {
		return ( true == isset( $this->m_intToEmployeeId ) ) ? ( string ) $this->m_intToEmployeeId : 'NULL';
	}

	public function setRemotePrimaryKey( $intRemotePrimaryKey ) {
		$this->set( 'm_intRemotePrimaryKey', CStrings::strToIntDef( $intRemotePrimaryKey, NULL, false ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_intRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_intRemotePrimaryKey ) ) ? ( string ) $this->m_intRemotePrimaryKey : 'NULL';
	}

	public function setToEmailAddress( $strToEmailAddress ) {
		$this->set( 'm_strToEmailAddress', CStrings::strTrimDef( $strToEmailAddress, 150, NULL, true ) );
	}

	public function getToEmailAddress() {
		return $this->m_strToEmailAddress;
	}

	public function sqlToEmailAddress() {
		return ( true == isset( $this->m_strToEmailAddress ) ) ? '\'' . addslashes( $this->m_strToEmailAddress ) . '\'' : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, from_employee_id, to_employee_id, remote_primary_key, to_email_address, log_datetime, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlFromEmployeeId() . ', ' .
 						$this->sqlToEmployeeId() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlToEmailAddress() . ', ' .
 						$this->sqlLogDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' from_employee_id = ' . $this->sqlFromEmployeeId() . ','; } elseif( true == array_key_exists( 'FromEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' from_employee_id = ' . $this->sqlFromEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' to_employee_id = ' . $this->sqlToEmployeeId() . ','; } elseif( true == array_key_exists( 'ToEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' to_employee_id = ' . $this->sqlToEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' to_email_address = ' . $this->sqlToEmailAddress() . ','; } elseif( true == array_key_exists( 'ToEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' to_email_address = ' . $this->sqlToEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'from_employee_id' => $this->getFromEmployeeId(),
			'to_employee_id' => $this->getToEmployeeId(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'to_email_address' => $this->getToEmailAddress(),
			'log_datetime' => $this->getLogDatetime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>