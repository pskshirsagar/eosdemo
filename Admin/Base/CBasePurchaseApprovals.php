<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPurchaseApprovals
 * Do not add any new functions to this class.
 */

class CBasePurchaseApprovals extends CEosPluralBase {

	/**
	 * @return CPurchaseApproval[]
	 */
	public static function fetchPurchaseApprovals( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPurchaseApproval', $objDatabase );
	}

	/**
	 * @return CPurchaseApproval
	 */
	public static function fetchPurchaseApproval( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPurchaseApproval', $objDatabase );
	}

	public static function fetchPurchaseApprovalCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'purchase_approvals', $objDatabase );
	}

	public static function fetchPurchaseApprovalById( $intId, $objDatabase ) {
		return self::fetchPurchaseApproval( sprintf( 'SELECT * FROM purchase_approvals WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPurchaseApprovalsByPurchaseRequestId( $intPurchaseRequestId, $objDatabase ) {
		return self::fetchPurchaseApprovals( sprintf( 'SELECT * FROM purchase_approvals WHERE purchase_request_id = %d', ( int ) $intPurchaseRequestId ), $objDatabase );
	}

	public static function fetchPurchaseApprovalsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchPurchaseApprovals( sprintf( 'SELECT * FROM purchase_approvals WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>