<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAssets
 * Do not add any new functions to this class.
 */

class CBaseEmployeeAssets extends CEosPluralBase {

	/**
	 * @return CEmployeeAsset[]
	 */
	public static function fetchEmployeeAssets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeAsset::class, $objDatabase );
	}

	/**
	 * @return CEmployeeAsset
	 */
	public static function fetchEmployeeAsset( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeAsset::class, $objDatabase );
	}

	public static function fetchEmployeeAssetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_assets', $objDatabase );
	}

	public static function fetchEmployeeAssetById( $intId, $objDatabase ) {
		return self::fetchEmployeeAsset( sprintf( 'SELECT * FROM employee_assets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeAssetsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeAssets( sprintf( 'SELECT * FROM employee_assets WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeAssetsByOfficeDeskId( $intOfficeDeskId, $objDatabase ) {
		return self::fetchEmployeeAssets( sprintf( 'SELECT * FROM employee_assets WHERE office_desk_id = %d', ( int ) $intOfficeDeskId ), $objDatabase );
	}

	public static function fetchEmployeeAssetsByPsAssetId( $intPsAssetId, $objDatabase ) {
		return self::fetchEmployeeAssets( sprintf( 'SELECT * FROM employee_assets WHERE ps_asset_id = %d', ( int ) $intPsAssetId ), $objDatabase );
	}

	public static function fetchEmployeeAssetsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchEmployeeAssets( sprintf( 'SELECT * FROM employee_assets WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>