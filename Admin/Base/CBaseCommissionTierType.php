<?php

class CBaseCommissionTierType extends CEosSingularBase {

	const TABLE_NAME = 'public.commission_tier_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intQuotaStartRange;
	protected $m_intQuotaEndRange;
	protected $m_intIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_intQuotaStartRange = '0';
		$this->m_intQuotaEndRange = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['quota_start_range'] ) && $boolDirectSet ) $this->set( 'm_intQuotaStartRange', trim( $arrValues['quota_start_range'] ) ); elseif( isset( $arrValues['quota_start_range'] ) ) $this->setQuotaStartRange( $arrValues['quota_start_range'] );
		if( isset( $arrValues['quota_end_range'] ) && $boolDirectSet ) $this->set( 'm_intQuotaEndRange', trim( $arrValues['quota_end_range'] ) ); elseif( isset( $arrValues['quota_end_range'] ) ) $this->setQuotaEndRange( $arrValues['quota_end_range'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, -1, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setQuotaStartRange( $intQuotaStartRange ) {
		$this->set( 'm_intQuotaStartRange', CStrings::strToIntDef( $intQuotaStartRange, NULL, false ) );
	}

	public function getQuotaStartRange() {
		return $this->m_intQuotaStartRange;
	}

	public function sqlQuotaStartRange() {
		return ( true == isset( $this->m_intQuotaStartRange ) ) ? ( string ) $this->m_intQuotaStartRange : '0';
	}

	public function setQuotaEndRange( $intQuotaEndRange ) {
		$this->set( 'm_intQuotaEndRange', CStrings::strToIntDef( $intQuotaEndRange, NULL, false ) );
	}

	public function getQuotaEndRange() {
		return $this->m_intQuotaEndRange;
	}

	public function sqlQuotaEndRange() {
		return ( true == isset( $this->m_intQuotaEndRange ) ) ? ( string ) $this->m_intQuotaEndRange : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'quota_start_range' => $this->getQuotaStartRange(),
			'quota_end_range' => $this->getQuotaEndRange(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>