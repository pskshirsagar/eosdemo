<?php

class CBaseSalesTaxTransaction extends CEosSingularBase {

	const TABLE_NAME = 'public.sales_tax_transactions';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intInvoiceId;
	protected $m_intTransactionId;
	protected $m_intPostedTransactionId;
	protected $m_strCchTransactionId;
	protected $m_intExportBatchId;
	protected $m_strCountryCode;
	protected $m_strStateOrProvince;
	protected $m_fltTotalTaxApplied;
	protected $m_intIsAdjustment;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltTotalTaxApplied = '0';
		$this->m_intIsAdjustment = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceId', trim( $arrValues['invoice_id'] ) ); elseif( isset( $arrValues['invoice_id'] ) ) $this->setInvoiceId( $arrValues['invoice_id'] );
		if( isset( $arrValues['transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intTransactionId', trim( $arrValues['transaction_id'] ) ); elseif( isset( $arrValues['transaction_id'] ) ) $this->setTransactionId( $arrValues['transaction_id'] );
		if( isset( $arrValues['posted_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_intPostedTransactionId', trim( $arrValues['posted_transaction_id'] ) ); elseif( isset( $arrValues['posted_transaction_id'] ) ) $this->setPostedTransactionId( $arrValues['posted_transaction_id'] );
		if( isset( $arrValues['cch_transaction_id'] ) && $boolDirectSet ) $this->set( 'm_strCchTransactionId', trim( stripcslashes( $arrValues['cch_transaction_id'] ) ) ); elseif( isset( $arrValues['cch_transaction_id'] ) ) $this->setCchTransactionId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['cch_transaction_id'] ) : $arrValues['cch_transaction_id'] );
		if( isset( $arrValues['export_batch_id'] ) && $boolDirectSet ) $this->set( 'm_intExportBatchId', trim( $arrValues['export_batch_id'] ) ); elseif( isset( $arrValues['export_batch_id'] ) ) $this->setExportBatchId( $arrValues['export_batch_id'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['state_or_province'] ) && $boolDirectSet ) $this->set( 'm_strStateOrProvince', trim( stripcslashes( $arrValues['state_or_province'] ) ) ); elseif( isset( $arrValues['state_or_province'] ) ) $this->setStateOrProvince( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_or_province'] ) : $arrValues['state_or_province'] );
		if( isset( $arrValues['total_tax_applied'] ) && $boolDirectSet ) $this->set( 'm_fltTotalTaxApplied', trim( $arrValues['total_tax_applied'] ) ); elseif( isset( $arrValues['total_tax_applied'] ) ) $this->setTotalTaxApplied( $arrValues['total_tax_applied'] );
		if( isset( $arrValues['is_adjustment'] ) && $boolDirectSet ) $this->set( 'm_intIsAdjustment', trim( $arrValues['is_adjustment'] ) ); elseif( isset( $arrValues['is_adjustment'] ) ) $this->setIsAdjustment( $arrValues['is_adjustment'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setInvoiceId( $intInvoiceId ) {
		$this->set( 'm_intInvoiceId', CStrings::strToIntDef( $intInvoiceId, NULL, false ) );
	}

	public function getInvoiceId() {
		return $this->m_intInvoiceId;
	}

	public function sqlInvoiceId() {
		return ( true == isset( $this->m_intInvoiceId ) ) ? ( string ) $this->m_intInvoiceId : 'NULL';
	}

	public function setTransactionId( $intTransactionId ) {
		$this->set( 'm_intTransactionId', CStrings::strToIntDef( $intTransactionId, NULL, false ) );
	}

	public function getTransactionId() {
		return $this->m_intTransactionId;
	}

	public function sqlTransactionId() {
		return ( true == isset( $this->m_intTransactionId ) ) ? ( string ) $this->m_intTransactionId : 'NULL';
	}

	public function setPostedTransactionId( $intPostedTransactionId ) {
		$this->set( 'm_intPostedTransactionId', CStrings::strToIntDef( $intPostedTransactionId, NULL, false ) );
	}

	public function getPostedTransactionId() {
		return $this->m_intPostedTransactionId;
	}

	public function sqlPostedTransactionId() {
		return ( true == isset( $this->m_intPostedTransactionId ) ) ? ( string ) $this->m_intPostedTransactionId : 'NULL';
	}

	public function setCchTransactionId( $strCchTransactionId ) {
		$this->set( 'm_strCchTransactionId', CStrings::strTrimDef( $strCchTransactionId, 100, NULL, true ) );
	}

	public function getCchTransactionId() {
		return $this->m_strCchTransactionId;
	}

	public function sqlCchTransactionId() {
		return ( true == isset( $this->m_strCchTransactionId ) ) ? '\'' . addslashes( $this->m_strCchTransactionId ) . '\'' : 'NULL';
	}

	public function setExportBatchId( $intExportBatchId ) {
		$this->set( 'm_intExportBatchId', CStrings::strToIntDef( $intExportBatchId, NULL, false ) );
	}

	public function getExportBatchId() {
		return $this->m_intExportBatchId;
	}

	public function sqlExportBatchId() {
		return ( true == isset( $this->m_intExportBatchId ) ) ? ( string ) $this->m_intExportBatchId : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 10, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setStateOrProvince( $strStateOrProvince ) {
		$this->set( 'm_strStateOrProvince', CStrings::strTrimDef( $strStateOrProvince, 100, NULL, true ) );
	}

	public function getStateOrProvince() {
		return $this->m_strStateOrProvince;
	}

	public function sqlStateOrProvince() {
		return ( true == isset( $this->m_strStateOrProvince ) ) ? '\'' . addslashes( $this->m_strStateOrProvince ) . '\'' : 'NULL';
	}

	public function setTotalTaxApplied( $fltTotalTaxApplied ) {
		$this->set( 'm_fltTotalTaxApplied', CStrings::strToFloatDef( $fltTotalTaxApplied, NULL, false, 4 ) );
	}

	public function getTotalTaxApplied() {
		return $this->m_fltTotalTaxApplied;
	}

	public function sqlTotalTaxApplied() {
		return ( true == isset( $this->m_fltTotalTaxApplied ) ) ? ( string ) $this->m_fltTotalTaxApplied : '0';
	}

	public function setIsAdjustment( $intIsAdjustment ) {
		$this->set( 'm_intIsAdjustment', CStrings::strToIntDef( $intIsAdjustment, NULL, false ) );
	}

	public function getIsAdjustment() {
		return $this->m_intIsAdjustment;
	}

	public function sqlIsAdjustment() {
		return ( true == isset( $this->m_intIsAdjustment ) ) ? ( string ) $this->m_intIsAdjustment : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, invoice_id, transaction_id, posted_transaction_id, cch_transaction_id, export_batch_id, country_code, state_or_province, total_tax_applied, is_adjustment, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlInvoiceId() . ', ' .
 						$this->sqlTransactionId() . ', ' .
 						$this->sqlPostedTransactionId() . ', ' .
 						$this->sqlCchTransactionId() . ', ' .
 						$this->sqlExportBatchId() . ', ' .
 						$this->sqlCountryCode() . ', ' .
 						$this->sqlStateOrProvince() . ', ' .
 						$this->sqlTotalTaxApplied() . ', ' .
 						$this->sqlIsAdjustment() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_id = ' . $this->sqlInvoiceId() . ','; } elseif( true == array_key_exists( 'InvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_id = ' . $this->sqlInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; } elseif( true == array_key_exists( 'TransactionId', $this->getChangedColumns() ) ) { $strSql .= ' transaction_id = ' . $this->sqlTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' posted_transaction_id = ' . $this->sqlPostedTransactionId() . ','; } elseif( true == array_key_exists( 'PostedTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' posted_transaction_id = ' . $this->sqlPostedTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cch_transaction_id = ' . $this->sqlCchTransactionId() . ','; } elseif( true == array_key_exists( 'CchTransactionId', $this->getChangedColumns() ) ) { $strSql .= ' cch_transaction_id = ' . $this->sqlCchTransactionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_batch_id = ' . $this->sqlExportBatchId() . ','; } elseif( true == array_key_exists( 'ExportBatchId', $this->getChangedColumns() ) ) { $strSql .= ' export_batch_id = ' . $this->sqlExportBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_or_province = ' . $this->sqlStateOrProvince() . ','; } elseif( true == array_key_exists( 'StateOrProvince', $this->getChangedColumns() ) ) { $strSql .= ' state_or_province = ' . $this->sqlStateOrProvince() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_tax_applied = ' . $this->sqlTotalTaxApplied() . ','; } elseif( true == array_key_exists( 'TotalTaxApplied', $this->getChangedColumns() ) ) { $strSql .= ' total_tax_applied = ' . $this->sqlTotalTaxApplied() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_adjustment = ' . $this->sqlIsAdjustment() . ','; } elseif( true == array_key_exists( 'IsAdjustment', $this->getChangedColumns() ) ) { $strSql .= ' is_adjustment = ' . $this->sqlIsAdjustment() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'invoice_id' => $this->getInvoiceId(),
			'transaction_id' => $this->getTransactionId(),
			'posted_transaction_id' => $this->getPostedTransactionId(),
			'cch_transaction_id' => $this->getCchTransactionId(),
			'export_batch_id' => $this->getExportBatchId(),
			'country_code' => $this->getCountryCode(),
			'state_or_province' => $this->getStateOrProvince(),
			'total_tax_applied' => $this->getTotalTaxApplied(),
			'is_adjustment' => $this->getIsAdjustment(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>