<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationAnswers
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationAnswers extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationAnswer[]
	 */
	public static function fetchEmployeeApplicationAnswers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationAnswer', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationAnswer
	 */
	public static function fetchEmployeeApplicationAnswer( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationAnswer', $objDatabase );
	}

	public static function fetchEmployeeApplicationAnswerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_answers', $objDatabase );
	}

	public static function fetchEmployeeApplicationAnswerById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationAnswer( sprintf( 'SELECT * FROM employee_application_answers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationAnswersByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchEmployeeApplicationAnswers( sprintf( 'SELECT * FROM employee_application_answers WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

}
?>