<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskStatuses
 * Do not add any new functions to this class.
 */

class CBaseTaskStatuses extends CEosPluralBase {

	/**
	 * @return CTaskStatus[]
	 */
	public static function fetchTaskStatuses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTaskStatus::class, $objDatabase );
	}

	/**
	 * @return CTaskStatus
	 */
	public static function fetchTaskStatus( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaskStatus::class, $objDatabase );
	}

	public static function fetchTaskStatusCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_statuses', $objDatabase );
	}

	public static function fetchTaskStatusById( $intId, $objDatabase ) {
		return self::fetchTaskStatus( sprintf( 'SELECT * FROM task_statuses WHERE id = %d', $intId ), $objDatabase );
	}

}
?>