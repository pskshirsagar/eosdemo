<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmailSummaries
 * Do not add any new functions to this class.
 */

class CBaseEmailSummaries extends CEosPluralBase {

	/**
	 * @return CEmailSummary[]
	 */
	public static function fetchEmailSummaries( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmailSummary', $objDatabase );
	}

	/**
	 * @return CEmailSummary
	 */
	public static function fetchEmailSummary( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailSummary', $objDatabase );
	}

	public static function fetchEmailSummaryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_summaries', $objDatabase );
	}

	public static function fetchEmailSummaryById( $intId, $objDatabase ) {
		return self::fetchEmailSummary( sprintf( 'SELECT * FROM email_summaries WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmailSummariesByCid( $intCid, $objDatabase ) {
		return self::fetchEmailSummaries( sprintf( 'SELECT * FROM email_summaries WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchEmailSummariesByEmailSummaryTypeId( $intEmailSummaryTypeId, $objDatabase ) {
		return self::fetchEmailSummaries( sprintf( 'SELECT * FROM email_summaries WHERE email_summary_type_id = %d', ( int ) $intEmailSummaryTypeId ), $objDatabase );
	}

}
?>