<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplateQuestions
 * Do not add any new functions to this class.
 */

class CBaseSurveyTemplateQuestions extends CEosPluralBase {

	/**
	 * @return CSurveyTemplateQuestion[]
	 */
	public static function fetchSurveyTemplateQuestions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyTemplateQuestion', $objDatabase );
	}

	/**
	 * @return CSurveyTemplateQuestion
	 */
	public static function fetchSurveyTemplateQuestion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyTemplateQuestion', $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_template_questions', $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionById( $intId, $objDatabase ) {
		return self::fetchSurveyTemplateQuestion( sprintf( 'SELECT * FROM survey_template_questions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionsBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		return self::fetchSurveyTemplateQuestions( sprintf( 'SELECT * FROM survey_template_questions WHERE survey_template_id = %d', ( int ) $intSurveyTemplateId ), $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionsBySurveyTemplateQuestionGroupId( $intSurveyTemplateQuestionGroupId, $objDatabase ) {
		return self::fetchSurveyTemplateQuestions( sprintf( 'SELECT * FROM survey_template_questions WHERE survey_template_question_group_id = %d', ( int ) $intSurveyTemplateQuestionGroupId ), $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionsBySurveyQuestionTypeId( $intSurveyQuestionTypeId, $objDatabase ) {
		return self::fetchSurveyTemplateQuestions( sprintf( 'SELECT * FROM survey_template_questions WHERE survey_question_type_id = %d', ( int ) $intSurveyQuestionTypeId ), $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionsBySurveyTemplateQuestionId( $intSurveyTemplateQuestionId, $objDatabase ) {
		return self::fetchSurveyTemplateQuestions( sprintf( 'SELECT * FROM survey_template_questions WHERE survey_template_question_id = %d', ( int ) $intSurveyTemplateQuestionId ), $objDatabase );
	}

}
?>