<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStorageObjects
 * Do not add any new functions to this class.
 */

class CBaseStorageObjects extends CEosPluralBase {

	/**
	 * @return CStorageObject[]
	 */
	public static function fetchStorageObjects( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CStorageObject::class, $objDatabase );
	}

	/**
	 * @return CStorageObject
	 */
	public static function fetchStorageObject( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStorageObject::class, $objDatabase );
	}

	public static function fetchStorageObjectCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'storage_objects', $objDatabase );
	}

	public static function fetchStorageObjectById( $intId, $objDatabase ) {
		return self::fetchStorageObject( sprintf( 'SELECT * FROM storage_objects WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStorageObjectsByStorageTypeId( $intStorageTypeId, $objDatabase ) {
		return self::fetchStorageObjects( sprintf( 'SELECT * FROM storage_objects WHERE storage_type_id = %d', ( int ) $intStorageTypeId ), $objDatabase );
	}

	public static function fetchStorageObjectsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchStorageObjects( sprintf( 'SELECT * FROM storage_objects WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchStorageObjectsByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchStorageObjects( sprintf( 'SELECT * FROM storage_objects WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

	public static function fetchStorageObjectsByParentStorageObjectId( $intParentStorageObjectId, $objDatabase ) {
		return self::fetchStorageObjects( sprintf( 'SELECT * FROM storage_objects WHERE parent_storage_object_id = %d', ( int ) $intParentStorageObjectId ), $objDatabase );
	}

	public static function fetchStorageObjectsByScriptId( $intScriptId, $objDatabase ) {
		return self::fetchStorageObjects( sprintf( 'SELECT * FROM storage_objects WHERE script_id = %d', ( int ) $intScriptId ), $objDatabase );
	}

}
?>