<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsAssetLogs
 * Do not add any new functions to this class.
 */

class CBasePsAssetLogs extends CEosPluralBase {

	/**
	 * @return CPsAssetLog[]
	 */
	public static function fetchPsAssetLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsAssetLog', $objDatabase );
	}

	/**
	 * @return CPsAssetLog
	 */
	public static function fetchPsAssetLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsAssetLog', $objDatabase );
	}

	public static function fetchPsAssetLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_asset_logs', $objDatabase );
	}

	public static function fetchPsAssetLogById( $intId, $objDatabase ) {
		return self::fetchPsAssetLog( sprintf( 'SELECT * FROM ps_asset_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsAssetLogsByPsAssetId( $intPsAssetId, $objDatabase ) {
		return self::fetchPsAssetLogs( sprintf( 'SELECT * FROM ps_asset_logs WHERE ps_asset_id = %d', ( int ) $intPsAssetId ), $objDatabase );
	}

	public static function fetchPsAssetLogsByPsAssetStatusTypeId( $intPsAssetStatusTypeId, $objDatabase ) {
		return self::fetchPsAssetLogs( sprintf( 'SELECT * FROM ps_asset_logs WHERE ps_asset_status_type_id = %d', ( int ) $intPsAssetStatusTypeId ), $objDatabase );
	}

}
?>