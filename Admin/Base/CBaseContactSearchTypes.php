<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CContactSearchTypes
 * Do not add any new functions to this class.
 */

class CBaseContactSearchTypes extends CEosPluralBase {

	/**
	 * @return CContactSearchType[]
	 */
	public static function fetchContactSearchTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CContactSearchType::class, $objDatabase );
	}

	/**
	 * @return CContactSearchType
	 */
	public static function fetchContactSearchType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CContactSearchType::class, $objDatabase );
	}

	public static function fetchContactSearchTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'contact_search_types', $objDatabase );
	}

	public static function fetchContactSearchTypeById( $intId, $objDatabase ) {
		return self::fetchContactSearchType( sprintf( 'SELECT * FROM contact_search_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>