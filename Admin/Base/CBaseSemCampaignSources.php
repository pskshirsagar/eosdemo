<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemCampaignSources
 * Do not add any new functions to this class.
 */

class CBaseSemCampaignSources extends CEosPluralBase {

	/**
	 * @return CSemCampaignSource[]
	 */
	public static function fetchSemCampaignSources( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemCampaignSource', $objDatabase );
	}

	/**
	 * @return CSemCampaignSource
	 */
	public static function fetchSemCampaignSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemCampaignSource', $objDatabase );
	}

	public static function fetchSemCampaignSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_campaign_sources', $objDatabase );
	}

	public static function fetchSemCampaignSourceById( $intId, $objDatabase ) {
		return self::fetchSemCampaignSource( sprintf( 'SELECT * FROM sem_campaign_sources WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemCampaignSourcesBySemSourceId( $intSemSourceId, $objDatabase ) {
		return self::fetchSemCampaignSources( sprintf( 'SELECT * FROM sem_campaign_sources WHERE sem_source_id = %d', ( int ) $intSemSourceId ), $objDatabase );
	}

	public static function fetchSemCampaignSourcesBySemAccountId( $intSemAccountId, $objDatabase ) {
		return self::fetchSemCampaignSources( sprintf( 'SELECT * FROM sem_campaign_sources WHERE sem_account_id = %d', ( int ) $intSemAccountId ), $objDatabase );
	}

	public static function fetchSemCampaignSourcesBySemCampaignId( $intSemCampaignId, $objDatabase ) {
		return self::fetchSemCampaignSources( sprintf( 'SELECT * FROM sem_campaign_sources WHERE sem_campaign_id = %d', ( int ) $intSemCampaignId ), $objDatabase );
	}

}
?>