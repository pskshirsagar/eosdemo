<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsDocuments
 * Do not add any new functions to this class.
 */

class CBasePsDocuments extends CEosPluralBase {

	/**
	 * @return CPsDocument[]
	 */
	public static function fetchPsDocuments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPsDocument::class, $objDatabase );
	}

	/**
	 * @return CPsDocument
	 */
	public static function fetchPsDocument( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPsDocument::class, $objDatabase );
	}

	public static function fetchPsDocumentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_documents', $objDatabase );
	}

	public static function fetchPsDocumentById( $intId, $objDatabase ) {
		return self::fetchPsDocument( sprintf( 'SELECT * FROM ps_documents WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsDocumentsByCid( $intCid, $objDatabase ) {
		return self::fetchPsDocuments( sprintf( 'SELECT * FROM ps_documents WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchPsDocumentsByPsLeadId( $intPsLeadId, $objDatabase ) {
		return self::fetchPsDocuments( sprintf( 'SELECT * FROM ps_documents WHERE ps_lead_id = %d', ( int ) $intPsLeadId ), $objDatabase );
	}

	public static function fetchPsDocumentsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchPsDocuments( sprintf( 'SELECT * FROM ps_documents WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchPsDocumentsByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchPsDocuments( sprintf( 'SELECT * FROM ps_documents WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchPsDocumentsByPsDocumentTypeId( $intPsDocumentTypeId, $objDatabase ) {
		return self::fetchPsDocuments( sprintf( 'SELECT * FROM ps_documents WHERE ps_document_type_id = %d', ( int ) $intPsDocumentTypeId ), $objDatabase );
	}

	public static function fetchPsDocumentsByPsDocumentCategoryId( $intPsDocumentCategoryId, $objDatabase ) {
		return self::fetchPsDocuments( sprintf( 'SELECT * FROM ps_documents WHERE ps_document_category_id = %d', ( int ) $intPsDocumentCategoryId ), $objDatabase );
	}

	public static function fetchPsDocumentsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchPsDocuments( sprintf( 'SELECT * FROM ps_documents WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchPsDocumentsByFileExtensionId( $intFileExtensionId, $objDatabase ) {
		return self::fetchPsDocuments( sprintf( 'SELECT * FROM ps_documents WHERE file_extension_id = %d', ( int ) $intFileExtensionId ), $objDatabase );
	}

	public static function fetchPsDocumentsByTrainingSessionId( $intTrainingSessionId, $objDatabase ) {
		return self::fetchPsDocuments( sprintf( 'SELECT * FROM ps_documents WHERE training_session_id = %d', ( int ) $intTrainingSessionId ), $objDatabase );
	}

	public static function fetchPsDocumentsByAssociatedPsDocumentId( $intAssociatedPsDocumentId, $objDatabase ) {
		return self::fetchPsDocuments( sprintf( 'SELECT * FROM ps_documents WHERE associated_ps_document_id = %d', ( int ) $intAssociatedPsDocumentId ), $objDatabase );
	}

}
?>