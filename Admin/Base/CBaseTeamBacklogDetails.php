<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTeamBacklogDetails
 * Do not add any new functions to this class.
 */

class CBaseTeamBacklogDetails extends CEosPluralBase {

	/**
	 * @return CTeamBacklogDetail[]
	 */
	public static function fetchTeamBacklogDetails( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTeamBacklogDetail::class, $objDatabase );
	}

	/**
	 * @return CTeamBacklogDetail
	 */
	public static function fetchTeamBacklogDetail( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTeamBacklogDetail::class, $objDatabase );
	}

	public static function fetchTeamBacklogDetailCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'team_backlog_details', $objDatabase );
	}

	public static function fetchTeamBacklogDetailById( $intId, $objDatabase ) {
		return self::fetchTeamBacklogDetail( sprintf( 'SELECT * FROM team_backlog_details WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTeamBacklogDetailsByTeamBacklogId( $intTeamBacklogId, $objDatabase ) {
		return self::fetchTeamBacklogDetails( sprintf( 'SELECT * FROM team_backlog_details WHERE team_backlog_id = %d', ( int ) $intTeamBacklogId ), $objDatabase );
	}

	public static function fetchTeamBacklogDetailsByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchTeamBacklogDetails( sprintf( 'SELECT * FROM team_backlog_details WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

	public static function fetchTeamBacklogDetailsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchTeamBacklogDetails( sprintf( 'SELECT * FROM team_backlog_details WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchTeamBacklogDetailsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchTeamBacklogDetails( sprintf( 'SELECT * FROM team_backlog_details WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchTeamBacklogDetailsByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchTeamBacklogDetails( sprintf( 'SELECT * FROM team_backlog_details WHERE ps_product_option_id = %d', ( int ) $intPsProductOptionId ), $objDatabase );
	}

}
?>