<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CHonourBadgeSemesters
 * Do not add any new functions to this class.
 */

class CBaseHonourBadgeSemesters extends CEosPluralBase {

	/**
	 * @return CHonourBadgeSemester[]
	 */
	public static function fetchHonourBadgeSemesters( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CHonourBadgeSemester', $objDatabase );
	}

	/**
	 * @return CHonourBadgeSemester
	 */
	public static function fetchHonourBadgeSemester( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHonourBadgeSemester', $objDatabase );
	}

	public static function fetchHonourBadgeSemesterCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'honour_badge_semesters', $objDatabase );
	}

	public static function fetchHonourBadgeSemesterById( $intId, $objDatabase ) {
		return self::fetchHonourBadgeSemester( sprintf( 'SELECT * FROM honour_badge_semesters WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>