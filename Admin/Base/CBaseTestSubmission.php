<?php

class CBaseTestSubmission extends CEosSingularBase {

	const TABLE_NAME = 'public.test_submissions';

	protected $m_intId;
	protected $m_intTestId;
	protected $m_intEmployeeTestId;
	protected $m_intEmployeeId;
	protected $m_intEmployeeApplicationId;
	protected $m_intTrainingSessionId;
	protected $m_strScore;
	protected $m_strInstructorComments;
	protected $m_intIsApproved;
	protected $m_intGradedBy;
	protected $m_strGradedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsApproved = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['test_id'] ) && $boolDirectSet ) $this->set( 'm_intTestId', trim( $arrValues['test_id'] ) ); elseif( isset( $arrValues['test_id'] ) ) $this->setTestId( $arrValues['test_id'] );
		if( isset( $arrValues['employee_test_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeTestId', trim( $arrValues['employee_test_id'] ) ); elseif( isset( $arrValues['employee_test_id'] ) ) $this->setEmployeeTestId( $arrValues['employee_test_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['employee_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationId', trim( $arrValues['employee_application_id'] ) ); elseif( isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( isset( $arrValues['training_session_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingSessionId', trim( $arrValues['training_session_id'] ) ); elseif( isset( $arrValues['training_session_id'] ) ) $this->setTrainingSessionId( $arrValues['training_session_id'] );
		if( isset( $arrValues['score'] ) && $boolDirectSet ) $this->set( 'm_strScore', trim( stripcslashes( $arrValues['score'] ) ) ); elseif( isset( $arrValues['score'] ) ) $this->setScore( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['score'] ) : $arrValues['score'] );
		if( isset( $arrValues['instructor_comments'] ) && $boolDirectSet ) $this->set( 'm_strInstructorComments', trim( stripcslashes( $arrValues['instructor_comments'] ) ) ); elseif( isset( $arrValues['instructor_comments'] ) ) $this->setInstructorComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['instructor_comments'] ) : $arrValues['instructor_comments'] );
		if( isset( $arrValues['is_approved'] ) && $boolDirectSet ) $this->set( 'm_intIsApproved', trim( $arrValues['is_approved'] ) ); elseif( isset( $arrValues['is_approved'] ) ) $this->setIsApproved( $arrValues['is_approved'] );
		if( isset( $arrValues['graded_by'] ) && $boolDirectSet ) $this->set( 'm_intGradedBy', trim( $arrValues['graded_by'] ) ); elseif( isset( $arrValues['graded_by'] ) ) $this->setGradedBy( $arrValues['graded_by'] );
		if( isset( $arrValues['graded_on'] ) && $boolDirectSet ) $this->set( 'm_strGradedOn', trim( $arrValues['graded_on'] ) ); elseif( isset( $arrValues['graded_on'] ) ) $this->setGradedOn( $arrValues['graded_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTestId( $intTestId ) {
		$this->set( 'm_intTestId', CStrings::strToIntDef( $intTestId, NULL, false ) );
	}

	public function getTestId() {
		return $this->m_intTestId;
	}

	public function sqlTestId() {
		return ( true == isset( $this->m_intTestId ) ) ? ( string ) $this->m_intTestId : 'NULL';
	}

	public function setEmployeeTestId( $intEmployeeTestId ) {
		$this->set( 'm_intEmployeeTestId', CStrings::strToIntDef( $intEmployeeTestId, NULL, false ) );
	}

	public function getEmployeeTestId() {
		return $this->m_intEmployeeTestId;
	}

	public function sqlEmployeeTestId() {
		return ( true == isset( $this->m_intEmployeeTestId ) ) ? ( string ) $this->m_intEmployeeTestId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setEmployeeApplicationId( $intEmployeeApplicationId ) {
		$this->set( 'm_intEmployeeApplicationId', CStrings::strToIntDef( $intEmployeeApplicationId, NULL, false ) );
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function sqlEmployeeApplicationId() {
		return ( true == isset( $this->m_intEmployeeApplicationId ) ) ? ( string ) $this->m_intEmployeeApplicationId : 'NULL';
	}

	public function setTrainingSessionId( $intTrainingSessionId ) {
		$this->set( 'm_intTrainingSessionId', CStrings::strToIntDef( $intTrainingSessionId, NULL, false ) );
	}

	public function getTrainingSessionId() {
		return $this->m_intTrainingSessionId;
	}

	public function sqlTrainingSessionId() {
		return ( true == isset( $this->m_intTrainingSessionId ) ) ? ( string ) $this->m_intTrainingSessionId : 'NULL';
	}

	public function setScore( $strScore ) {
		$this->set( 'm_strScore', CStrings::strTrimDef( $strScore, 240, NULL, true ) );
	}

	public function getScore() {
		return $this->m_strScore;
	}

	public function sqlScore() {
		return ( true == isset( $this->m_strScore ) ) ? '\'' . addslashes( $this->m_strScore ) . '\'' : 'NULL';
	}

	public function setInstructorComments( $strInstructorComments ) {
		$this->set( 'm_strInstructorComments', CStrings::strTrimDef( $strInstructorComments, -1, NULL, true ) );
	}

	public function getInstructorComments() {
		return $this->m_strInstructorComments;
	}

	public function sqlInstructorComments() {
		return ( true == isset( $this->m_strInstructorComments ) ) ? '\'' . addslashes( $this->m_strInstructorComments ) . '\'' : 'NULL';
	}

	public function setIsApproved( $intIsApproved ) {
		$this->set( 'm_intIsApproved', CStrings::strToIntDef( $intIsApproved, NULL, false ) );
	}

	public function getIsApproved() {
		return $this->m_intIsApproved;
	}

	public function sqlIsApproved() {
		return ( true == isset( $this->m_intIsApproved ) ) ? ( string ) $this->m_intIsApproved : '0';
	}

	public function setGradedBy( $intGradedBy ) {
		$this->set( 'm_intGradedBy', CStrings::strToIntDef( $intGradedBy, NULL, false ) );
	}

	public function getGradedBy() {
		return $this->m_intGradedBy;
	}

	public function sqlGradedBy() {
		return ( true == isset( $this->m_intGradedBy ) ) ? ( string ) $this->m_intGradedBy : 'NULL';
	}

	public function setGradedOn( $strGradedOn ) {
		$this->set( 'm_strGradedOn', CStrings::strTrimDef( $strGradedOn, -1, NULL, true ) );
	}

	public function getGradedOn() {
		return $this->m_strGradedOn;
	}

	public function sqlGradedOn() {
		return ( true == isset( $this->m_strGradedOn ) ) ? '\'' . $this->m_strGradedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, test_id, employee_test_id, employee_id, employee_application_id, training_session_id, score, instructor_comments, is_approved, graded_by, graded_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTestId() . ', ' .
 						$this->sqlEmployeeTestId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlEmployeeApplicationId() . ', ' .
 						$this->sqlTrainingSessionId() . ', ' .
 						$this->sqlScore() . ', ' .
 						$this->sqlInstructorComments() . ', ' .
 						$this->sqlIsApproved() . ', ' .
 						$this->sqlGradedBy() . ', ' .
 						$this->sqlGradedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_id = ' . $this->sqlTestId() . ','; } elseif( true == array_key_exists( 'TestId', $this->getChangedColumns() ) ) { $strSql .= ' test_id = ' . $this->sqlTestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_test_id = ' . $this->sqlEmployeeTestId() . ','; } elseif( true == array_key_exists( 'EmployeeTestId', $this->getChangedColumns() ) ) { $strSql .= ' employee_test_id = ' . $this->sqlEmployeeTestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_session_id = ' . $this->sqlTrainingSessionId() . ','; } elseif( true == array_key_exists( 'TrainingSessionId', $this->getChangedColumns() ) ) { $strSql .= ' training_session_id = ' . $this->sqlTrainingSessionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' score = ' . $this->sqlScore() . ','; } elseif( true == array_key_exists( 'Score', $this->getChangedColumns() ) ) { $strSql .= ' score = ' . $this->sqlScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instructor_comments = ' . $this->sqlInstructorComments() . ','; } elseif( true == array_key_exists( 'InstructorComments', $this->getChangedColumns() ) ) { $strSql .= ' instructor_comments = ' . $this->sqlInstructorComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; } elseif( true == array_key_exists( 'IsApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' graded_by = ' . $this->sqlGradedBy() . ','; } elseif( true == array_key_exists( 'GradedBy', $this->getChangedColumns() ) ) { $strSql .= ' graded_by = ' . $this->sqlGradedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' graded_on = ' . $this->sqlGradedOn() . ','; } elseif( true == array_key_exists( 'GradedOn', $this->getChangedColumns() ) ) { $strSql .= ' graded_on = ' . $this->sqlGradedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'test_id' => $this->getTestId(),
			'employee_test_id' => $this->getEmployeeTestId(),
			'employee_id' => $this->getEmployeeId(),
			'employee_application_id' => $this->getEmployeeApplicationId(),
			'training_session_id' => $this->getTrainingSessionId(),
			'score' => $this->getScore(),
			'instructor_comments' => $this->getInstructorComments(),
			'is_approved' => $this->getIsApproved(),
			'graded_by' => $this->getGradedBy(),
			'graded_on' => $this->getGradedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>