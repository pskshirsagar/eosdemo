<?php

class CBaseTaskSvnFile extends CEosSingularBase {

	const TABLE_NAME = 'public.task_svn_files';

	protected $m_intId;
	protected $m_intTaskId;
	protected $m_intSvnFileId;
	protected $m_intUserId;
	protected $m_intSchemaApprovedBy;
	protected $m_intOrderNum;
	protected $m_strAssociationDatetime;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['svn_file_id'] ) && $boolDirectSet ) $this->set( 'm_intSvnFileId', trim( $arrValues['svn_file_id'] ) ); elseif( isset( $arrValues['svn_file_id'] ) ) $this->setSvnFileId( $arrValues['svn_file_id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['schema_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intSchemaApprovedBy', trim( $arrValues['schema_approved_by'] ) ); elseif( isset( $arrValues['schema_approved_by'] ) ) $this->setSchemaApprovedBy( $arrValues['schema_approved_by'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['association_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAssociationDatetime', trim( $arrValues['association_datetime'] ) ); elseif( isset( $arrValues['association_datetime'] ) ) $this->setAssociationDatetime( $arrValues['association_datetime'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setSvnFileId( $intSvnFileId ) {
		$this->set( 'm_intSvnFileId', CStrings::strToIntDef( $intSvnFileId, NULL, false ) );
	}

	public function getSvnFileId() {
		return $this->m_intSvnFileId;
	}

	public function sqlSvnFileId() {
		return ( true == isset( $this->m_intSvnFileId ) ) ? ( string ) $this->m_intSvnFileId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setSchemaApprovedBy( $intSchemaApprovedBy ) {
		$this->set( 'm_intSchemaApprovedBy', CStrings::strToIntDef( $intSchemaApprovedBy, NULL, false ) );
	}

	public function getSchemaApprovedBy() {
		return $this->m_intSchemaApprovedBy;
	}

	public function sqlSchemaApprovedBy() {
		return ( true == isset( $this->m_intSchemaApprovedBy ) ) ? ( string ) $this->m_intSchemaApprovedBy : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setAssociationDatetime( $strAssociationDatetime ) {
		$this->set( 'm_strAssociationDatetime', CStrings::strTrimDef( $strAssociationDatetime, -1, NULL, true ) );
	}

	public function getAssociationDatetime() {
		return $this->m_strAssociationDatetime;
	}

	public function sqlAssociationDatetime() {
		return ( true == isset( $this->m_strAssociationDatetime ) ) ? '\'' . $this->m_strAssociationDatetime . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, task_id, svn_file_id, user_id, schema_approved_by, order_num, association_datetime, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlSvnFileId() . ', ' .
 						$this->sqlUserId() . ', ' .
 						$this->sqlSchemaApprovedBy() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlAssociationDatetime() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' svn_file_id = ' . $this->sqlSvnFileId() . ','; } elseif( true == array_key_exists( 'SvnFileId', $this->getChangedColumns() ) ) { $strSql .= ' svn_file_id = ' . $this->sqlSvnFileId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' schema_approved_by = ' . $this->sqlSchemaApprovedBy() . ','; } elseif( true == array_key_exists( 'SchemaApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' schema_approved_by = ' . $this->sqlSchemaApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' association_datetime = ' . $this->sqlAssociationDatetime() . ','; } elseif( true == array_key_exists( 'AssociationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' association_datetime = ' . $this->sqlAssociationDatetime() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'task_id' => $this->getTaskId(),
			'svn_file_id' => $this->getSvnFileId(),
			'user_id' => $this->getUserId(),
			'schema_approved_by' => $this->getSchemaApprovedBy(),
			'order_num' => $this->getOrderNum(),
			'association_datetime' => $this->getAssociationDatetime(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>