<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTranslationNotes
 * Do not add any new functions to this class.
 */

class CBaseTranslationNotes extends CEosPluralBase {

	/**
	 * @return CTranslationNote[]
	 */
	public static function fetchTranslationNotes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTranslationNote::class, $objDatabase );
	}

	/**
	 * @return CTranslationNote
	 */
	public static function fetchTranslationNote( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTranslationNote::class, $objDatabase );
	}

	public static function fetchTranslationNoteCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'translation_notes', $objDatabase );
	}

	public static function fetchTranslationNoteById( $intId, $objDatabase ) {
		return self::fetchTranslationNote( sprintf( 'SELECT * FROM translation_notes WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchTranslationNotesByTranslationKeyId( $intTranslationKeyId, $objDatabase ) {
		return self::fetchTranslationNotes( sprintf( 'SELECT * FROM translation_notes WHERE translation_key_id = %d', $intTranslationKeyId ), $objDatabase );
	}

}
?>