<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskStakeholders
 * Do not add any new functions to this class.
 */

class CBaseTaskStakeholders extends CEosPluralBase {

	/**
	 * @return CTaskStakeholder[]
	 */
	public static function fetchTaskStakeholders( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskStakeholder', $objDatabase );
	}

	/**
	 * @return CTaskStakeholder
	 */
	public static function fetchTaskStakeholder( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskStakeholder', $objDatabase );
	}

	public static function fetchTaskStakeholderCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_stakeholders', $objDatabase );
	}

	public static function fetchTaskStakeholderById( $intId, $objDatabase ) {
		return self::fetchTaskStakeholder( sprintf( 'SELECT * FROM task_stakeholders WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskStakeholdersByTaskTypeId( $intTaskTypeId, $objDatabase ) {
		return self::fetchTaskStakeholders( sprintf( 'SELECT * FROM task_stakeholders WHERE task_type_id = %d', ( int ) $intTaskTypeId ), $objDatabase );
	}

	public static function fetchTaskStakeholdersByTaskPriorityId( $intTaskPriorityId, $objDatabase ) {
		return self::fetchTaskStakeholders( sprintf( 'SELECT * FROM task_stakeholders WHERE task_priority_id = %d', ( int ) $intTaskPriorityId ), $objDatabase );
	}

	public static function fetchTaskStakeholdersByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchTaskStakeholders( sprintf( 'SELECT * FROM task_stakeholders WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchTaskStakeholdersByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchTaskStakeholders( sprintf( 'SELECT * FROM task_stakeholders WHERE ps_product_option_id = %d', ( int ) $intPsProductOptionId ), $objDatabase );
	}

	public static function fetchTaskStakeholdersByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchTaskStakeholders( sprintf( 'SELECT * FROM task_stakeholders WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchTaskStakeholdersByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchTaskStakeholders( sprintf( 'SELECT * FROM task_stakeholders WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

}
?>