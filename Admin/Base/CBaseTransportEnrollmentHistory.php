<?php

class CBaseTransportEnrollmentHistory extends CEosSingularBase {

	const TABLE_NAME = 'public.transport_enrollment_histories';

	protected $m_intId;
	protected $m_intTransportEnrollmentId;
	protected $m_intTransportRouteId;
	protected $m_intTransportPickupPointId;
	protected $m_intTransportRequestStatusId;
	protected $m_strTransportRequestStatusReason;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['transport_enrollment_id'] ) && $boolDirectSet ) $this->set( 'm_intTransportEnrollmentId', trim( $arrValues['transport_enrollment_id'] ) ); elseif( isset( $arrValues['transport_enrollment_id'] ) ) $this->setTransportEnrollmentId( $arrValues['transport_enrollment_id'] );
		if( isset( $arrValues['transport_route_id'] ) && $boolDirectSet ) $this->set( 'm_intTransportRouteId', trim( $arrValues['transport_route_id'] ) ); elseif( isset( $arrValues['transport_route_id'] ) ) $this->setTransportRouteId( $arrValues['transport_route_id'] );
		if( isset( $arrValues['transport_pickup_point_id'] ) && $boolDirectSet ) $this->set( 'm_intTransportPickupPointId', trim( $arrValues['transport_pickup_point_id'] ) ); elseif( isset( $arrValues['transport_pickup_point_id'] ) ) $this->setTransportPickupPointId( $arrValues['transport_pickup_point_id'] );
		if( isset( $arrValues['transport_request_status_id'] ) && $boolDirectSet ) $this->set( 'm_intTransportRequestStatusId', trim( $arrValues['transport_request_status_id'] ) ); elseif( isset( $arrValues['transport_request_status_id'] ) ) $this->setTransportRequestStatusId( $arrValues['transport_request_status_id'] );
		if( isset( $arrValues['transport_request_status_reason'] ) && $boolDirectSet ) $this->set( 'm_strTransportRequestStatusReason', trim( stripcslashes( $arrValues['transport_request_status_reason'] ) ) ); elseif( isset( $arrValues['transport_request_status_reason'] ) ) $this->setTransportRequestStatusReason( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['transport_request_status_reason'] ) : $arrValues['transport_request_status_reason'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTransportEnrollmentId( $intTransportEnrollmentId ) {
		$this->set( 'm_intTransportEnrollmentId', CStrings::strToIntDef( $intTransportEnrollmentId, NULL, false ) );
	}

	public function getTransportEnrollmentId() {
		return $this->m_intTransportEnrollmentId;
	}

	public function sqlTransportEnrollmentId() {
		return ( true == isset( $this->m_intTransportEnrollmentId ) ) ? ( string ) $this->m_intTransportEnrollmentId : 'NULL';
	}

	public function setTransportRouteId( $intTransportRouteId ) {
		$this->set( 'm_intTransportRouteId', CStrings::strToIntDef( $intTransportRouteId, NULL, false ) );
	}

	public function getTransportRouteId() {
		return $this->m_intTransportRouteId;
	}

	public function sqlTransportRouteId() {
		return ( true == isset( $this->m_intTransportRouteId ) ) ? ( string ) $this->m_intTransportRouteId : 'NULL';
	}

	public function setTransportPickupPointId( $intTransportPickupPointId ) {
		$this->set( 'm_intTransportPickupPointId', CStrings::strToIntDef( $intTransportPickupPointId, NULL, false ) );
	}

	public function getTransportPickupPointId() {
		return $this->m_intTransportPickupPointId;
	}

	public function sqlTransportPickupPointId() {
		return ( true == isset( $this->m_intTransportPickupPointId ) ) ? ( string ) $this->m_intTransportPickupPointId : 'NULL';
	}

	public function setTransportRequestStatusId( $intTransportRequestStatusId ) {
		$this->set( 'm_intTransportRequestStatusId', CStrings::strToIntDef( $intTransportRequestStatusId, NULL, false ) );
	}

	public function getTransportRequestStatusId() {
		return $this->m_intTransportRequestStatusId;
	}

	public function sqlTransportRequestStatusId() {
		return ( true == isset( $this->m_intTransportRequestStatusId ) ) ? ( string ) $this->m_intTransportRequestStatusId : 'NULL';
	}

	public function setTransportRequestStatusReason( $strTransportRequestStatusReason ) {
		$this->set( 'm_strTransportRequestStatusReason', CStrings::strTrimDef( $strTransportRequestStatusReason, 255, NULL, true ) );
	}

	public function getTransportRequestStatusReason() {
		return $this->m_strTransportRequestStatusReason;
	}

	public function sqlTransportRequestStatusReason() {
		return ( true == isset( $this->m_strTransportRequestStatusReason ) ) ? '\'' . addslashes( $this->m_strTransportRequestStatusReason ) . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, transport_enrollment_id, transport_route_id, transport_pickup_point_id, transport_request_status_id, transport_request_status_reason, start_date, end_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTransportEnrollmentId() . ', ' .
 						$this->sqlTransportRouteId() . ', ' .
 						$this->sqlTransportPickupPointId() . ', ' .
 						$this->sqlTransportRequestStatusId() . ', ' .
 						$this->sqlTransportRequestStatusReason() . ', ' .
 						$this->sqlStartDate() . ', ' .
 						$this->sqlEndDate() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transport_enrollment_id = ' . $this->sqlTransportEnrollmentId() . ','; } elseif( true == array_key_exists( 'TransportEnrollmentId', $this->getChangedColumns() ) ) { $strSql .= ' transport_enrollment_id = ' . $this->sqlTransportEnrollmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transport_route_id = ' . $this->sqlTransportRouteId() . ','; } elseif( true == array_key_exists( 'TransportRouteId', $this->getChangedColumns() ) ) { $strSql .= ' transport_route_id = ' . $this->sqlTransportRouteId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transport_pickup_point_id = ' . $this->sqlTransportPickupPointId() . ','; } elseif( true == array_key_exists( 'TransportPickupPointId', $this->getChangedColumns() ) ) { $strSql .= ' transport_pickup_point_id = ' . $this->sqlTransportPickupPointId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transport_request_status_id = ' . $this->sqlTransportRequestStatusId() . ','; } elseif( true == array_key_exists( 'TransportRequestStatusId', $this->getChangedColumns() ) ) { $strSql .= ' transport_request_status_id = ' . $this->sqlTransportRequestStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' transport_request_status_reason = ' . $this->sqlTransportRequestStatusReason() . ','; } elseif( true == array_key_exists( 'TransportRequestStatusReason', $this->getChangedColumns() ) ) { $strSql .= ' transport_request_status_reason = ' . $this->sqlTransportRequestStatusReason() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'transport_enrollment_id' => $this->getTransportEnrollmentId(),
			'transport_route_id' => $this->getTransportRouteId(),
			'transport_pickup_point_id' => $this->getTransportPickupPointId(),
			'transport_request_status_id' => $this->getTransportRequestStatusId(),
			'transport_request_status_reason' => $this->getTransportRequestStatusReason(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>