<?php

class CBasePsProductType extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_product_types';

	protected $m_intId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strHexColor;
	protected $m_intIsBundle;
	protected $m_intIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_intIsBundle = '0';
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['hex_color'] ) && $boolDirectSet ) $this->set( 'm_strHexColor', trim( stripcslashes( $arrValues['hex_color'] ) ) ); elseif( isset( $arrValues['hex_color'] ) ) $this->setHexColor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['hex_color'] ) : $arrValues['hex_color'] );
		if( isset( $arrValues['is_bundle'] ) && $boolDirectSet ) $this->set( 'm_intIsBundle', trim( $arrValues['is_bundle'] ) ); elseif( isset( $arrValues['is_bundle'] ) ) $this->setIsBundle( $arrValues['is_bundle'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 250, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setHexColor( $strHexColor ) {
		$this->set( 'm_strHexColor', CStrings::strTrimDef( $strHexColor, 6, NULL, true ) );
	}

	public function getHexColor() {
		return $this->m_strHexColor;
	}

	public function sqlHexColor() {
		return ( true == isset( $this->m_strHexColor ) ) ? '\'' . addslashes( $this->m_strHexColor ) . '\'' : 'NULL';
	}

	public function setIsBundle( $intIsBundle ) {
		$this->set( 'm_intIsBundle', CStrings::strToIntDef( $intIsBundle, NULL, false ) );
	}

	public function getIsBundle() {
		return $this->m_intIsBundle;
	}

	public function sqlIsBundle() {
		return ( true == isset( $this->m_intIsBundle ) ) ? ( string ) $this->m_intIsBundle : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'hex_color' => $this->getHexColor(),
			'is_bundle' => $this->getIsBundle(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>