<?php

class CBaseSurveyTemplateQuestion extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.survey_template_questions';

	protected $m_intId;
	protected $m_intSurveyTemplateId;
	protected $m_intSurveyTemplateQuestionGroupId;
	protected $m_intSurveyQuestionTypeId;
	protected $m_intSurveyTemplateQuestionId;
	protected $m_strQuestion;
	protected $m_fltNumericWeight;
	protected $m_intOrderNum;
	protected $m_strWidgetAction;
	protected $m_intIsPercentage;
	protected $m_intIsRequired;
	protected $m_intIsPublished;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	private $m_boolIsSystemTranslated = true;

	public function __construct() {
		parent::__construct();

		$this->m_intOrderNum = '0';
		$this->m_intIsPercentage = '0';
		$this->m_intIsRequired = '0';
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['survey_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateId', trim( $arrValues['survey_template_id'] ) ); elseif( isset( $arrValues['survey_template_id'] ) ) $this->setSurveyTemplateId( $arrValues['survey_template_id'] );
		if( isset( $arrValues['survey_template_question_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateQuestionGroupId', trim( $arrValues['survey_template_question_group_id'] ) ); elseif( isset( $arrValues['survey_template_question_group_id'] ) ) $this->setSurveyTemplateQuestionGroupId( $arrValues['survey_template_question_group_id'] );
		if( isset( $arrValues['survey_question_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyQuestionTypeId', trim( $arrValues['survey_question_type_id'] ) ); elseif( isset( $arrValues['survey_question_type_id'] ) ) $this->setSurveyQuestionTypeId( $arrValues['survey_question_type_id'] );
		if( isset( $arrValues['survey_template_question_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateQuestionId', trim( $arrValues['survey_template_question_id'] ) ); elseif( isset( $arrValues['survey_template_question_id'] ) ) $this->setSurveyTemplateQuestionId( $arrValues['survey_template_question_id'] );
		if( isset( $arrValues['question'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strQuestion', trim( stripcslashes( $arrValues['question'] ) ) ); elseif( isset( $arrValues['question'] ) ) $this->setQuestion( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['question'] ) : $arrValues['question'] );
		if( isset( $arrValues['numeric_weight'] ) && $boolDirectSet ) $this->set( 'm_fltNumericWeight', trim( $arrValues['numeric_weight'] ) ); elseif( isset( $arrValues['numeric_weight'] ) ) $this->setNumericWeight( $arrValues['numeric_weight'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['widget_action'] ) && $boolDirectSet ) $this->set( 'm_strWidgetAction', trim( stripcslashes( $arrValues['widget_action'] ) ) ); elseif( isset( $arrValues['widget_action'] ) ) $this->setWidgetAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['widget_action'] ) : $arrValues['widget_action'] );
		if( isset( $arrValues['is_percentage'] ) && $boolDirectSet ) $this->set( 'm_intIsPercentage', trim( $arrValues['is_percentage'] ) ); elseif( isset( $arrValues['is_percentage'] ) ) $this->setIsPercentage( $arrValues['is_percentage'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_intIsRequired', trim( $arrValues['is_required'] ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( $arrValues['is_required'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSurveyTemplateId( $intSurveyTemplateId ) {
		$this->set( 'm_intSurveyTemplateId', CStrings::strToIntDef( $intSurveyTemplateId, NULL, false ) );
	}

	public function getSurveyTemplateId() {
		return $this->m_intSurveyTemplateId;
	}

	public function sqlSurveyTemplateId() {
		return ( true == isset( $this->m_intSurveyTemplateId ) ) ? ( string ) $this->m_intSurveyTemplateId : 'NULL';
	}

	public function setSurveyTemplateQuestionGroupId( $intSurveyTemplateQuestionGroupId ) {
		$this->set( 'm_intSurveyTemplateQuestionGroupId', CStrings::strToIntDef( $intSurveyTemplateQuestionGroupId, NULL, false ) );
	}

	public function getSurveyTemplateQuestionGroupId() {
		return $this->m_intSurveyTemplateQuestionGroupId;
	}

	public function sqlSurveyTemplateQuestionGroupId() {
		return ( true == isset( $this->m_intSurveyTemplateQuestionGroupId ) ) ? ( string ) $this->m_intSurveyTemplateQuestionGroupId : 'NULL';
	}

	public function setSurveyQuestionTypeId( $intSurveyQuestionTypeId ) {
		$this->set( 'm_intSurveyQuestionTypeId', CStrings::strToIntDef( $intSurveyQuestionTypeId, NULL, false ) );
	}

	public function getSurveyQuestionTypeId() {
		return $this->m_intSurveyQuestionTypeId;
	}

	public function sqlSurveyQuestionTypeId() {
		return ( true == isset( $this->m_intSurveyQuestionTypeId ) ) ? ( string ) $this->m_intSurveyQuestionTypeId : 'NULL';
	}

	public function setSurveyTemplateQuestionId( $intSurveyTemplateQuestionId ) {
		$this->set( 'm_intSurveyTemplateQuestionId', CStrings::strToIntDef( $intSurveyTemplateQuestionId, NULL, false ) );
	}

	public function getSurveyTemplateQuestionId() {
		return $this->m_intSurveyTemplateQuestionId;
	}

	public function sqlSurveyTemplateQuestionId() {
		return ( true == isset( $this->m_intSurveyTemplateQuestionId ) ) ? ( string ) $this->m_intSurveyTemplateQuestionId : 'NULL';
	}

	public function setQuestion( $strQuestion, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strQuestion', CStrings::strTrimDef( $strQuestion, -1, NULL, true ), $strLocaleCode );
	}

	public function getQuestion( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strQuestion', $strLocaleCode );
	}

	public function sqlQuestion() {
		return ( true == isset( $this->m_strQuestion ) ) ? '\'' . addslashes( $this->m_strQuestion ) . '\'' : 'NULL';
	}

	public function setNumericWeight( $fltNumericWeight ) {
		$this->set( 'm_fltNumericWeight', CStrings::strToFloatDef( $fltNumericWeight, NULL, false, 2 ) );
	}

	public function getNumericWeight() {
		return $this->m_fltNumericWeight;
	}

	public function sqlNumericWeight() {
		return ( true == isset( $this->m_fltNumericWeight ) ) ? ( string ) $this->m_fltNumericWeight : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setWidgetAction( $strWidgetAction ) {
		$this->set( 'm_strWidgetAction', CStrings::strTrimDef( $strWidgetAction, -1, NULL, true ) );
	}

	public function getWidgetAction() {
		return $this->m_strWidgetAction;
	}

	public function sqlWidgetAction() {
		return ( true == isset( $this->m_strWidgetAction ) ) ? '\'' . addslashes( $this->m_strWidgetAction ) . '\'' : 'NULL';
	}

	public function setIsPercentage( $intIsPercentage ) {
		$this->set( 'm_intIsPercentage', CStrings::strToIntDef( $intIsPercentage, NULL, false ) );
	}

	public function getIsPercentage() {
		return $this->m_intIsPercentage;
	}

	public function sqlIsPercentage() {
		return ( true == isset( $this->m_intIsPercentage ) ) ? ( string ) $this->m_intIsPercentage : '0';
	}

	public function setIsRequired( $intIsRequired ) {
		$this->set( 'm_intIsRequired', CStrings::strToIntDef( $intIsRequired, NULL, false ) );
	}

	public function getIsRequired() {
		return $this->m_intIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_intIsRequired ) ) ? ( string ) $this->m_intIsRequired : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function getIsSystemTranslated() {
		return $this->m_boolIsSystemTranslated;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, survey_template_id, survey_template_question_group_id, survey_question_type_id, survey_template_question_id, question, numeric_weight, order_num, widget_action, is_percentage, is_required, is_published, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlSurveyTemplateId() . ', ' .
						$this->sqlSurveyTemplateQuestionGroupId() . ', ' .
						$this->sqlSurveyQuestionTypeId() . ', ' .
						$this->sqlSurveyTemplateQuestionId() . ', ' .
						$this->sqlQuestion() . ', ' .
						$this->sqlNumericWeight() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlWidgetAction() . ', ' .
						$this->sqlIsPercentage() . ', ' .
						$this->sqlIsRequired() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId(). ',' ; } elseif( true == array_key_exists( 'SurveyTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_question_group_id = ' . $this->sqlSurveyTemplateQuestionGroupId(). ',' ; } elseif( true == array_key_exists( 'SurveyTemplateQuestionGroupId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_question_group_id = ' . $this->sqlSurveyTemplateQuestionGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_question_type_id = ' . $this->sqlSurveyQuestionTypeId(). ',' ; } elseif( true == array_key_exists( 'SurveyQuestionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' survey_question_type_id = ' . $this->sqlSurveyQuestionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_question_id = ' . $this->sqlSurveyTemplateQuestionId(). ',' ; } elseif( true == array_key_exists( 'SurveyTemplateQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_question_id = ' . $this->sqlSurveyTemplateQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' question = ' . $this->sqlQuestion(). ',' ; } elseif( true == array_key_exists( 'Question', $this->getChangedColumns() ) ) { $strSql .= ' question = ' . $this->sqlQuestion() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' numeric_weight = ' . $this->sqlNumericWeight(). ',' ; } elseif( true == array_key_exists( 'NumericWeight', $this->getChangedColumns() ) ) { $strSql .= ' numeric_weight = ' . $this->sqlNumericWeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' widget_action = ' . $this->sqlWidgetAction(). ',' ; } elseif( true == array_key_exists( 'WidgetAction', $this->getChangedColumns() ) ) { $strSql .= ' widget_action = ' . $this->sqlWidgetAction() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_percentage = ' . $this->sqlIsPercentage(). ',' ; } elseif( true == array_key_exists( 'IsPercentage', $this->getChangedColumns() ) ) { $strSql .= ' is_percentage = ' . $this->sqlIsPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required = ' . $this->sqlIsRequired(). ',' ; } elseif( true == array_key_exists( 'IsRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'survey_template_id' => $this->getSurveyTemplateId(),
			'survey_template_question_group_id' => $this->getSurveyTemplateQuestionGroupId(),
			'survey_question_type_id' => $this->getSurveyQuestionTypeId(),
			'survey_template_question_id' => $this->getSurveyTemplateQuestionId(),
			'question' => $this->getQuestion(),
			'numeric_weight' => $this->getNumericWeight(),
			'order_num' => $this->getOrderNum(),
			'widget_action' => $this->getWidgetAction(),
			'is_percentage' => $this->getIsPercentage(),
			'is_required' => $this->getIsRequired(),
			'is_published' => $this->getIsPublished(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>