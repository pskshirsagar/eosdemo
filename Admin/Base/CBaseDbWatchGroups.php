<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDbWatchGroups
 * Do not add any new functions to this class.
 */

class CBaseDbWatchGroups extends CEosPluralBase {

	/**
	 * @return CDbWatchGroup[]
	 */
	public static function fetchDbWatchGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDbWatchGroup', $objDatabase );
	}

	/**
	 * @return CDbWatchGroup
	 */
	public static function fetchDbWatchGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDbWatchGroup', $objDatabase );
	}

	public static function fetchDbWatchGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'db_watch_groups', $objDatabase );
	}

	public static function fetchDbWatchGroupById( $intId, $objDatabase ) {
		return self::fetchDbWatchGroup( sprintf( 'SELECT * FROM db_watch_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDbWatchGroupsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchDbWatchGroups( sprintf( 'SELECT * FROM db_watch_groups WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

	public static function fetchDbWatchGroupsByDbWatchId( $intDbWatchId, $objDatabase ) {
		return self::fetchDbWatchGroups( sprintf( 'SELECT * FROM db_watch_groups WHERE db_watch_id = %d', ( int ) $intDbWatchId ), $objDatabase );
	}

}
?>