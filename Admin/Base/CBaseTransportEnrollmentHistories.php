<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTransportEnrollmentHistories
 * Do not add any new functions to this class.
 */

class CBaseTransportEnrollmentHistories extends CEosPluralBase {

	/**
	 * @return CTransportEnrollmentHistory[]
	 */
	public static function fetchTransportEnrollmentHistories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTransportEnrollmentHistory', $objDatabase );
	}

	/**
	 * @return CTransportEnrollmentHistory
	 */
	public static function fetchTransportEnrollmentHistory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTransportEnrollmentHistory', $objDatabase );
	}

	public static function fetchTransportEnrollmentHistoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transport_enrollment_histories', $objDatabase );
	}

	public static function fetchTransportEnrollmentHistoryById( $intId, $objDatabase ) {
		return self::fetchTransportEnrollmentHistory( sprintf( 'SELECT * FROM transport_enrollment_histories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTransportEnrollmentHistoriesByTransportEnrollmentId( $intTransportEnrollmentId, $objDatabase ) {
		return self::fetchTransportEnrollmentHistories( sprintf( 'SELECT * FROM transport_enrollment_histories WHERE transport_enrollment_id = %d', ( int ) $intTransportEnrollmentId ), $objDatabase );
	}

	public static function fetchTransportEnrollmentHistoriesByTransportRouteId( $intTransportRouteId, $objDatabase ) {
		return self::fetchTransportEnrollmentHistories( sprintf( 'SELECT * FROM transport_enrollment_histories WHERE transport_route_id = %d', ( int ) $intTransportRouteId ), $objDatabase );
	}

	public static function fetchTransportEnrollmentHistoriesByTransportPickupPointId( $intTransportPickupPointId, $objDatabase ) {
		return self::fetchTransportEnrollmentHistories( sprintf( 'SELECT * FROM transport_enrollment_histories WHERE transport_pickup_point_id = %d', ( int ) $intTransportPickupPointId ), $objDatabase );
	}

	public static function fetchTransportEnrollmentHistoriesByTransportRequestStatusId( $intTransportRequestStatusId, $objDatabase ) {
		return self::fetchTransportEnrollmentHistories( sprintf( 'SELECT * FROM transport_enrollment_histories WHERE transport_request_status_id = %d', ( int ) $intTransportRequestStatusId ), $objDatabase );
	}

}
?>