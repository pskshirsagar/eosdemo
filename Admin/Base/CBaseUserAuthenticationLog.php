<?php

class CBaseUserAuthenticationLog extends CEosSingularBase {

	const TABLE_NAME = 'public.user_authentication_logs';

	protected $m_intId;
	protected $m_intUserId;
	protected $m_strPreviousPassword;
	protected $m_strLoginDatetime;
	protected $m_strLogoutDatetime;
	protected $m_strIpAddress;
	protected $m_strSessionName;
	protected $m_intClickCount;
	protected $m_strLastClick;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intClickCount = '0';
		$this->m_strLastClick = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['previous_password'] ) && $boolDirectSet ) $this->set( 'm_strPreviousPassword', trim( stripcslashes( $arrValues['previous_password'] ) ) ); elseif( isset( $arrValues['previous_password'] ) ) $this->setPreviousPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['previous_password'] ) : $arrValues['previous_password'] );
		if( isset( $arrValues['login_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLoginDatetime', trim( $arrValues['login_datetime'] ) ); elseif( isset( $arrValues['login_datetime'] ) ) $this->setLoginDatetime( $arrValues['login_datetime'] );
		if( isset( $arrValues['logout_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogoutDatetime', trim( $arrValues['logout_datetime'] ) ); elseif( isset( $arrValues['logout_datetime'] ) ) $this->setLogoutDatetime( $arrValues['logout_datetime'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( stripcslashes( $arrValues['ip_address'] ) ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ip_address'] ) : $arrValues['ip_address'] );
		if( isset( $arrValues['session_name'] ) && $boolDirectSet ) $this->set( 'm_strSessionName', trim( stripcslashes( $arrValues['session_name'] ) ) ); elseif( isset( $arrValues['session_name'] ) ) $this->setSessionName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['session_name'] ) : $arrValues['session_name'] );
		if( isset( $arrValues['click_count'] ) && $boolDirectSet ) $this->set( 'm_intClickCount', trim( $arrValues['click_count'] ) ); elseif( isset( $arrValues['click_count'] ) ) $this->setClickCount( $arrValues['click_count'] );
		if( isset( $arrValues['last_click'] ) && $boolDirectSet ) $this->set( 'm_strLastClick', trim( $arrValues['last_click'] ) ); elseif( isset( $arrValues['last_click'] ) ) $this->setLastClick( $arrValues['last_click'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setPreviousPassword( $strPreviousPassword ) {
		$this->set( 'm_strPreviousPassword', CStrings::strTrimDef( $strPreviousPassword, 240, NULL, true ) );
	}

	public function getPreviousPassword() {
		return $this->m_strPreviousPassword;
	}

	public function sqlPreviousPassword() {
		return ( true == isset( $this->m_strPreviousPassword ) ) ? '\'' . addslashes( $this->m_strPreviousPassword ) . '\'' : 'NULL';
	}

	public function setLoginDatetime( $strLoginDatetime ) {
		$this->set( 'm_strLoginDatetime', CStrings::strTrimDef( $strLoginDatetime, -1, NULL, true ) );
	}

	public function getLoginDatetime() {
		return $this->m_strLoginDatetime;
	}

	public function sqlLoginDatetime() {
		return ( true == isset( $this->m_strLoginDatetime ) ) ? '\'' . $this->m_strLoginDatetime . '\'' : 'NULL';
	}

	public function setLogoutDatetime( $strLogoutDatetime ) {
		$this->set( 'm_strLogoutDatetime', CStrings::strTrimDef( $strLogoutDatetime, -1, NULL, true ) );
	}

	public function getLogoutDatetime() {
		return $this->m_strLogoutDatetime;
	}

	public function sqlLogoutDatetime() {
		return ( true == isset( $this->m_strLogoutDatetime ) ) ? '\'' . $this->m_strLogoutDatetime . '\'' : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 23, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? '\'' . addslashes( $this->m_strIpAddress ) . '\'' : 'NULL';
	}

	public function setSessionName( $strSessionName ) {
		$this->set( 'm_strSessionName', CStrings::strTrimDef( $strSessionName, 240, NULL, true ) );
	}

	public function getSessionName() {
		return $this->m_strSessionName;
	}

	public function sqlSessionName() {
		return ( true == isset( $this->m_strSessionName ) ) ? '\'' . addslashes( $this->m_strSessionName ) . '\'' : 'NULL';
	}

	public function setClickCount( $intClickCount ) {
		$this->set( 'm_intClickCount', CStrings::strToIntDef( $intClickCount, NULL, false ) );
	}

	public function getClickCount() {
		return $this->m_intClickCount;
	}

	public function sqlClickCount() {
		return ( true == isset( $this->m_intClickCount ) ) ? ( string ) $this->m_intClickCount : '0';
	}

	public function setLastClick( $strLastClick ) {
		$this->set( 'm_strLastClick', CStrings::strTrimDef( $strLastClick, -1, NULL, true ) );
	}

	public function getLastClick() {
		return $this->m_strLastClick;
	}

	public function sqlLastClick() {
		return ( true == isset( $this->m_strLastClick ) ) ? '\'' . $this->m_strLastClick . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, user_id, previous_password, login_datetime, logout_datetime, ip_address, session_name, click_count, last_click, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlUserId() . ', ' .
 						$this->sqlPreviousPassword() . ', ' .
 						$this->sqlLoginDatetime() . ', ' .
 						$this->sqlLogoutDatetime() . ', ' .
 						$this->sqlIpAddress() . ', ' .
 						$this->sqlSessionName() . ', ' .
 						$this->sqlClickCount() . ', ' .
 						$this->sqlLastClick() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_password = ' . $this->sqlPreviousPassword() . ','; } elseif( true == array_key_exists( 'PreviousPassword', $this->getChangedColumns() ) ) { $strSql .= ' previous_password = ' . $this->sqlPreviousPassword() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_datetime = ' . $this->sqlLoginDatetime() . ','; } elseif( true == array_key_exists( 'LoginDatetime', $this->getChangedColumns() ) ) { $strSql .= ' login_datetime = ' . $this->sqlLoginDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' logout_datetime = ' . $this->sqlLogoutDatetime() . ','; } elseif( true == array_key_exists( 'LogoutDatetime', $this->getChangedColumns() ) ) { $strSql .= ' logout_datetime = ' . $this->sqlLogoutDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' session_name = ' . $this->sqlSessionName() . ','; } elseif( true == array_key_exists( 'SessionName', $this->getChangedColumns() ) ) { $strSql .= ' session_name = ' . $this->sqlSessionName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' click_count = ' . $this->sqlClickCount() . ','; } elseif( true == array_key_exists( 'ClickCount', $this->getChangedColumns() ) ) { $strSql .= ' click_count = ' . $this->sqlClickCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_click = ' . $this->sqlLastClick() . ','; } elseif( true == array_key_exists( 'LastClick', $this->getChangedColumns() ) ) { $strSql .= ' last_click = ' . $this->sqlLastClick() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'user_id' => $this->getUserId(),
			'previous_password' => $this->getPreviousPassword(),
			'login_datetime' => $this->getLoginDatetime(),
			'logout_datetime' => $this->getLogoutDatetime(),
			'ip_address' => $this->getIpAddress(),
			'session_name' => $this->getSessionName(),
			'click_count' => $this->getClickCount(),
			'last_click' => $this->getLastClick(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>