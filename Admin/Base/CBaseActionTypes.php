<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CActionTypes
 * Do not add any new functions to this class.
 */

class CBaseActionTypes extends CEosPluralBase {

	/**
	 * @return CActionType[]
	 */
	public static function fetchActionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CActionType', $objDatabase );
	}

	/**
	 * @return CActionType
	 */
	public static function fetchActionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CActionType', $objDatabase );
	}

	public static function fetchActionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'action_types', $objDatabase );
	}

	public static function fetchActionTypeById( $intId, $objDatabase ) {
		return self::fetchActionType( sprintf( 'SELECT * FROM action_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchActionTypesByStatusTypeId( $intStatusTypeId, $objDatabase ) {
		return self::fetchActionTypes( sprintf( 'SELECT * FROM action_types WHERE status_type_id = %d', ( int ) $intStatusTypeId ), $objDatabase );
	}

}
?>