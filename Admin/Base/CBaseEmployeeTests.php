<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeTests
 * Do not add any new functions to this class.
 */

class CBaseEmployeeTests extends CEosPluralBase {

	/**
	 * @return CEmployeeTest[]
	 */
	public static function fetchEmployeeTests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeTest', $objDatabase );
	}

	/**
	 * @return CEmployeeTest
	 */
	public static function fetchEmployeeTest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeTest', $objDatabase );
	}

	public static function fetchEmployeeTestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_tests', $objDatabase );
	}

	public static function fetchEmployeeTestById( $intId, $objDatabase ) {
		return self::fetchEmployeeTest( sprintf( 'SELECT * FROM employee_tests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeTestsByTestId( $intTestId, $objDatabase ) {
		return self::fetchEmployeeTests( sprintf( 'SELECT * FROM employee_tests WHERE test_id = %d', ( int ) $intTestId ), $objDatabase );
	}

	public static function fetchEmployeeTestsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeTests( sprintf( 'SELECT * FROM employee_tests WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeTestsByTestAssignmentReferenceTypeId( $intTestAssignmentReferenceTypeId, $objDatabase ) {
		return self::fetchEmployeeTests( sprintf( 'SELECT * FROM employee_tests WHERE test_assignment_reference_type_id = %d', ( int ) $intTestAssignmentReferenceTypeId ), $objDatabase );
	}

}
?>