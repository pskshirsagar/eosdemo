<?php

class CBaseReleaseReportDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.release_report_details';

	protected $m_intId;
	protected $m_intTaskReleaseId;
	protected $m_strDownTimeDetails;
	protected $m_strReleasedTaskDetails;
	protected $m_strDescription;
	protected $m_strEmailedOn;
	protected $m_strPublishedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['task_release_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskReleaseId', trim( $arrValues['task_release_id'] ) ); elseif( isset( $arrValues['task_release_id'] ) ) $this->setTaskReleaseId( $arrValues['task_release_id'] );
		if( isset( $arrValues['down_time_details'] ) && $boolDirectSet ) $this->set( 'm_strDownTimeDetails', trim( stripcslashes( $arrValues['down_time_details'] ) ) ); elseif( isset( $arrValues['down_time_details'] ) ) $this->setDownTimeDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['down_time_details'] ) : $arrValues['down_time_details'] );
		if( isset( $arrValues['released_task_details'] ) && $boolDirectSet ) $this->set( 'm_strReleasedTaskDetails', trim( stripcslashes( $arrValues['released_task_details'] ) ) ); elseif( isset( $arrValues['released_task_details'] ) ) $this->setReleasedTaskDetails( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['released_task_details'] ) : $arrValues['released_task_details'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['emailed_on'] ) && $boolDirectSet ) $this->set( 'm_strEmailedOn', trim( $arrValues['emailed_on'] ) ); elseif( isset( $arrValues['emailed_on'] ) ) $this->setEmailedOn( $arrValues['emailed_on'] );
		if( isset( $arrValues['published_on'] ) && $boolDirectSet ) $this->set( 'm_strPublishedOn', trim( $arrValues['published_on'] ) ); elseif( isset( $arrValues['published_on'] ) ) $this->setPublishedOn( $arrValues['published_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaskReleaseId( $intTaskReleaseId ) {
		$this->set( 'm_intTaskReleaseId', CStrings::strToIntDef( $intTaskReleaseId, NULL, false ) );
	}

	public function getTaskReleaseId() {
		return $this->m_intTaskReleaseId;
	}

	public function sqlTaskReleaseId() {
		return ( true == isset( $this->m_intTaskReleaseId ) ) ? ( string ) $this->m_intTaskReleaseId : 'NULL';
	}

	public function setDownTimeDetails( $strDownTimeDetails ) {
		$this->set( 'm_strDownTimeDetails', CStrings::strTrimDef( $strDownTimeDetails, -1, NULL, true ) );
	}

	public function getDownTimeDetails() {
		return $this->m_strDownTimeDetails;
	}

	public function sqlDownTimeDetails() {
		return ( true == isset( $this->m_strDownTimeDetails ) ) ? '\'' . addslashes( $this->m_strDownTimeDetails ) . '\'' : 'NULL';
	}

	public function setReleasedTaskDetails( $strReleasedTaskDetails ) {
		$this->set( 'm_strReleasedTaskDetails', CStrings::strTrimDef( $strReleasedTaskDetails, -1, NULL, true ) );
	}

	public function getReleasedTaskDetails() {
		return $this->m_strReleasedTaskDetails;
	}

	public function sqlReleasedTaskDetails() {
		return ( true == isset( $this->m_strReleasedTaskDetails ) ) ? '\'' . addslashes( $this->m_strReleasedTaskDetails ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setEmailedOn( $strEmailedOn ) {
		$this->set( 'm_strEmailedOn', CStrings::strTrimDef( $strEmailedOn, -1, NULL, true ) );
	}

	public function getEmailedOn() {
		return $this->m_strEmailedOn;
	}

	public function sqlEmailedOn() {
		return ( true == isset( $this->m_strEmailedOn ) ) ? '\'' . $this->m_strEmailedOn . '\'' : 'NULL';
	}

	public function setPublishedOn( $strPublishedOn ) {
		$this->set( 'm_strPublishedOn', CStrings::strTrimDef( $strPublishedOn, -1, NULL, true ) );
	}

	public function getPublishedOn() {
		return $this->m_strPublishedOn;
	}

	public function sqlPublishedOn() {
		return ( true == isset( $this->m_strPublishedOn ) ) ? '\'' . $this->m_strPublishedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, task_release_id, down_time_details, released_task_details, description, emailed_on, published_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTaskReleaseId() . ', ' .
 						$this->sqlDownTimeDetails() . ', ' .
 						$this->sqlReleasedTaskDetails() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlEmailedOn() . ', ' .
 						$this->sqlPublishedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_release_id = ' . $this->sqlTaskReleaseId() . ','; } elseif( true == array_key_exists( 'TaskReleaseId', $this->getChangedColumns() ) ) { $strSql .= ' task_release_id = ' . $this->sqlTaskReleaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' down_time_details = ' . $this->sqlDownTimeDetails() . ','; } elseif( true == array_key_exists( 'DownTimeDetails', $this->getChangedColumns() ) ) { $strSql .= ' down_time_details = ' . $this->sqlDownTimeDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' released_task_details = ' . $this->sqlReleasedTaskDetails() . ','; } elseif( true == array_key_exists( 'ReleasedTaskDetails', $this->getChangedColumns() ) ) { $strSql .= ' released_task_details = ' . $this->sqlReleasedTaskDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; } elseif( true == array_key_exists( 'EmailedOn', $this->getChangedColumns() ) ) { $strSql .= ' emailed_on = ' . $this->sqlEmailedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn() . ','; } elseif( true == array_key_exists( 'PublishedOn', $this->getChangedColumns() ) ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'task_release_id' => $this->getTaskReleaseId(),
			'down_time_details' => $this->getDownTimeDetails(),
			'released_task_details' => $this->getReleasedTaskDetails(),
			'description' => $this->getDescription(),
			'emailed_on' => $this->getEmailedOn(),
			'published_on' => $this->getPublishedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>