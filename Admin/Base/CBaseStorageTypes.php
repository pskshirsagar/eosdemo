<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStorageTypes
 * Do not add any new functions to this class.
 */

class CBaseStorageTypes extends CEosPluralBase {

	/**
	 * @return CStorageType[]
	 */
	public static function fetchStorageTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CStorageType::class, $objDatabase );
	}

	/**
	 * @return CStorageType
	 */
	public static function fetchStorageType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CStorageType::class, $objDatabase );
	}

	public static function fetchStorageTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'storage_types', $objDatabase );
	}

	public static function fetchStorageTypeById( $intId, $objDatabase ) {
		return self::fetchStorageType( sprintf( 'SELECT * FROM storage_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>