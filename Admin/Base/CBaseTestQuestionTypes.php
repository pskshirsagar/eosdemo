<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestQuestionTypes
 * Do not add any new functions to this class.
 */

class CBaseTestQuestionTypes extends CEosPluralBase {

	/**
	 * @return CTestQuestionType[]
	 */
	public static function fetchTestQuestionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestQuestionType', $objDatabase );
	}

	/**
	 * @return CTestQuestionType
	 */
	public static function fetchTestQuestionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestQuestionType', $objDatabase );
	}

	public static function fetchTestQuestionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_question_types', $objDatabase );
	}

	public static function fetchTestQuestionTypeById( $intId, $objDatabase ) {
		return self::fetchTestQuestionType( sprintf( 'SELECT * FROM test_question_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>