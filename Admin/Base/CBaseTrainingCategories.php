<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingCategories
 * Do not add any new functions to this class.
 */

class CBaseTrainingCategories extends CEosPluralBase {

	/**
	 * @return CTrainingCategory[]
	 */
	public static function fetchTrainingCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTrainingCategory', $objDatabase );
	}

	/**
	 * @return CTrainingCategory
	 */
	public static function fetchTrainingCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTrainingCategory', $objDatabase );
	}

	public static function fetchTrainingCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'training_categories', $objDatabase );
	}

	public static function fetchTrainingCategoryById( $intId, $objDatabase ) {
		return self::fetchTrainingCategory( sprintf( 'SELECT * FROM training_categories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTrainingCategoriesByTrainingCategoryId( $intTrainingCategoryId, $objDatabase ) {
		return self::fetchTrainingCategories( sprintf( 'SELECT * FROM training_categories WHERE training_category_id = %d', ( int ) $intTrainingCategoryId ), $objDatabase );
	}

}
?>