<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskEstimationSizeTypes
 * Do not add any new functions to this class.
 */

class CBaseTaskEstimationSizeTypes extends CEosPluralBase {

	/**
	 * @return CTaskEstimationSizeType[]
	 */
	public static function fetchTaskEstimationSizeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTaskEstimationSizeType::class, $objDatabase );
	}

	/**
	 * @return CTaskEstimationSizeType
	 */
	public static function fetchTaskEstimationSizeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaskEstimationSizeType::class, $objDatabase );
	}

	public static function fetchTaskEstimationSizeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_estimation_size_types', $objDatabase );
	}

	public static function fetchTaskEstimationSizeTypeById( $intId, $objDatabase ) {
		return self::fetchTaskEstimationSizeType( sprintf( 'SELECT * FROM task_estimation_size_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>