<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePayrolls
 * Do not add any new functions to this class.
 */

class CBaseEmployeePayrolls extends CEosPluralBase {

	/**
	 * @return CEmployeePayroll[]
	 */
	public static function fetchEmployeePayrolls( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeePayroll', $objDatabase );
	}

	/**
	 * @return CEmployeePayroll
	 */
	public static function fetchEmployeePayroll( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeePayroll', $objDatabase );
	}

	public static function fetchEmployeePayrollCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_payrolls', $objDatabase );
	}

	public static function fetchEmployeePayrollById( $intId, $objDatabase ) {
		return self::fetchEmployeePayroll( sprintf( 'SELECT * FROM employee_payrolls WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeePayrollsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeePayrolls( sprintf( 'SELECT * FROM employee_payrolls WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeePayrollsByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchEmployeePayrolls( sprintf( 'SELECT * FROM employee_payrolls WHERE designation_id = %d', ( int ) $intDesignationId ), $objDatabase );
	}

	public static function fetchEmployeePayrollsByPayrollPeriodId( $intPayrollPeriodId, $objDatabase ) {
		return self::fetchEmployeePayrolls( sprintf( 'SELECT * FROM employee_payrolls WHERE payroll_period_id = %d', ( int ) $intPayrollPeriodId ), $objDatabase );
	}

	public static function fetchEmployeePayrollsByPayrollId( $intPayrollId, $objDatabase ) {
		return self::fetchEmployeePayrolls( sprintf( 'SELECT * FROM employee_payrolls WHERE payroll_id = %d', ( int ) $intPayrollId ), $objDatabase );
	}

	public static function fetchEmployeePayrollsByGrossPayEmployeeEncryptionAssociationId( $intGrossPayEmployeeEncryptionAssociationId, $objDatabase ) {
		return self::fetchEmployeePayrolls( sprintf( 'SELECT * FROM employee_payrolls WHERE gross_pay_employee_encryption_association_id = %d', ( int ) $intGrossPayEmployeeEncryptionAssociationId ), $objDatabase );
	}

	public static function fetchEmployeePayrollsByHourlyPayEmployeeEncryptionAssociationId( $intHourlyPayEmployeeEncryptionAssociationId, $objDatabase ) {
		return self::fetchEmployeePayrolls( sprintf( 'SELECT * FROM employee_payrolls WHERE hourly_pay_employee_encryption_association_id = %d', ( int ) $intHourlyPayEmployeeEncryptionAssociationId ), $objDatabase );
	}

}
?>