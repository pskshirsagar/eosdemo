<?php

class CBaseTask extends CEosSingularBase {

	use TEosDetails;

	use TEosTranslated;

	const TABLE_NAME = 'public.tasks';

	protected $m_intId;
	protected $m_intPsLeadId;
	protected $m_intCid;
	protected $m_intUserId;
	protected $m_intGroupId;
	protected $m_intCompanyUserId;
	protected $m_intProjectManagerId;
	protected $m_intClientResolutionEmployeeId;
	protected $m_intDepartmentId;
	protected $m_intClusterId;
	protected $m_intTaskReleaseId;
	protected $m_intTaskStandardReleaseId;
	protected $m_intTaskTypeId;
	protected $m_intSubTaskTypeId;
	protected $m_intTaskStatusId;
	protected $m_intSeleniumScriptTypeId;
	protected $m_intTaskPriorityId;
	protected $m_intTaskResultId;
	protected $m_intTaskPurchaseId;
	protected $m_intTaskMediumId;
	protected $m_intPropertyId;
	protected $m_intMerchantAccountApplicationId;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_intPsProductModuleId;
	protected $m_intContactPhoneNumberTypeId;
	protected $m_intMessageOperatorId;
	protected $m_intTeamId;
	protected $m_intPdmTaskReleaseId;
	protected $m_intCallId;
	protected $m_intCallType;
	protected $m_intRevisionNumber;
	protected $m_intDurationMinutes;
	protected $m_strCalendarPrimaryKey;
	protected $m_strTitle;
	protected $m_strDescription;
	protected $m_strContactNameFirst;
	protected $m_strContactNameLast;
	protected $m_strContactEmailAddress;
	protected $m_strContactPhoneNumber;
	protected $m_strPhoneExtension;
	protected $m_strTaskDatetime;
	protected $m_strDueDate;
	protected $m_strFollowUpOn;
	protected $m_strFollowUpDatetime;
	protected $m_strOriginalDueDate;
	protected $m_strPlannedCompletionDate;
	protected $m_strTasksLastVerfiedOn;
	protected $m_strUrl;
	protected $m_strMarketingTitle;
	protected $m_strMarketingDescription;
	protected $m_strKeywords;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intIsUnderConsideration;
	protected $m_intIsClientTask;
	protected $m_intIsSupportTicket;
	protected $m_intIsResidentContact;
	protected $m_intIsPublished;
	protected $m_intIsHighlightFeature;
	protected $m_strCalendarRemotePrimaryKey;
	protected $m_intChargeSupportFee;
	protected $m_intIsInternalRelease;
	protected $m_intIsDataFixes;
	protected $m_strReleasedInternallyOn;
	protected $m_strReleasedExternallyOn;
	protected $m_intOrderNum;
	protected $m_intCodeReviewedBy;
	protected $m_intCompletedBy;
	protected $m_intReleaseNoteBy;
	protected $m_strCompletedOn;
	protected $m_strFirstResponseOn;
	protected $m_strFeesPostedOn;
	protected $m_intLastViewedBy;
	protected $m_strLastViewedOn;
	protected $m_intQaApprovedBy;
	protected $m_strQaApprovedOn;
	protected $m_intClosedBy;
	protected $m_strClosedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsUnderConsideration = '0';
		$this->m_intIsClientTask = '0';
		$this->m_intIsHighlightFeature = '0';
		$this->m_intChargeSupportFee = '0';
		$this->m_intIsDataFixes = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['group_id'] ) && $boolDirectSet ) $this->set( 'm_intGroupId', trim( $arrValues['group_id'] ) ); elseif( isset( $arrValues['group_id'] ) ) $this->setGroupId( $arrValues['group_id'] );
		if( isset( $arrValues['company_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCompanyUserId', trim( $arrValues['company_user_id'] ) ); elseif( isset( $arrValues['company_user_id'] ) ) $this->setCompanyUserId( $arrValues['company_user_id'] );
		if( isset( $arrValues['project_manager_id'] ) && $boolDirectSet ) $this->set( 'm_intProjectManagerId', trim( $arrValues['project_manager_id'] ) ); elseif( isset( $arrValues['project_manager_id'] ) ) $this->setProjectManagerId( $arrValues['project_manager_id'] );
		if( isset( $arrValues['client_resolution_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intClientResolutionEmployeeId', trim( $arrValues['client_resolution_employee_id'] ) ); elseif( isset( $arrValues['client_resolution_employee_id'] ) ) $this->setClientResolutionEmployeeId( $arrValues['client_resolution_employee_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['task_release_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskReleaseId', trim( $arrValues['task_release_id'] ) ); elseif( isset( $arrValues['task_release_id'] ) ) $this->setTaskReleaseId( $arrValues['task_release_id'] );
		if( isset( $arrValues['task_standard_release_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskStandardReleaseId', trim( $arrValues['task_standard_release_id'] ) ); elseif( isset( $arrValues['task_standard_release_id'] ) ) $this->setTaskStandardReleaseId( $arrValues['task_standard_release_id'] );
		if( isset( $arrValues['task_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskTypeId', trim( $arrValues['task_type_id'] ) ); elseif( isset( $arrValues['task_type_id'] ) ) $this->setTaskTypeId( $arrValues['task_type_id'] );
		if( isset( $arrValues['sub_task_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSubTaskTypeId', trim( $arrValues['sub_task_type_id'] ) ); elseif( isset( $arrValues['sub_task_type_id'] ) ) $this->setSubTaskTypeId( $arrValues['sub_task_type_id'] );
		if( isset( $arrValues['task_status_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskStatusId', trim( $arrValues['task_status_id'] ) ); elseif( isset( $arrValues['task_status_id'] ) ) $this->setTaskStatusId( $arrValues['task_status_id'] );
		if( isset( $arrValues['selenium_script_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSeleniumScriptTypeId', trim( $arrValues['selenium_script_type_id'] ) ); elseif( isset( $arrValues['selenium_script_type_id'] ) ) $this->setSeleniumScriptTypeId( $arrValues['selenium_script_type_id'] );
		if( isset( $arrValues['task_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskPriorityId', trim( $arrValues['task_priority_id'] ) ); elseif( isset( $arrValues['task_priority_id'] ) ) $this->setTaskPriorityId( $arrValues['task_priority_id'] );
		if( isset( $arrValues['task_result_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskResultId', trim( $arrValues['task_result_id'] ) ); elseif( isset( $arrValues['task_result_id'] ) ) $this->setTaskResultId( $arrValues['task_result_id'] );
		if( isset( $arrValues['task_purchase_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskPurchaseId', trim( $arrValues['task_purchase_id'] ) ); elseif( isset( $arrValues['task_purchase_id'] ) ) $this->setTaskPurchaseId( $arrValues['task_purchase_id'] );
		if( isset( $arrValues['task_medium_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskMediumId', trim( $arrValues['task_medium_id'] ) ); elseif( isset( $arrValues['task_medium_id'] ) ) $this->setTaskMediumId( $arrValues['task_medium_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['merchant_account_application_id'] ) && $boolDirectSet ) $this->set( 'm_intMerchantAccountApplicationId', trim( $arrValues['merchant_account_application_id'] ) ); elseif( isset( $arrValues['merchant_account_application_id'] ) ) $this->setMerchantAccountApplicationId( $arrValues['merchant_account_application_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['ps_product_module_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductModuleId', trim( $arrValues['ps_product_module_id'] ) ); elseif( isset( $arrValues['ps_product_module_id'] ) ) $this->setPsProductModuleId( $arrValues['ps_product_module_id'] );
		if( isset( $arrValues['contact_phone_number_type_id'] ) && $boolDirectSet ) $this->set( 'm_intContactPhoneNumberTypeId', trim( $arrValues['contact_phone_number_type_id'] ) ); elseif( isset( $arrValues['contact_phone_number_type_id'] ) ) $this->setContactPhoneNumberTypeId( $arrValues['contact_phone_number_type_id'] );
		if( isset( $arrValues['message_operator_id'] ) && $boolDirectSet ) $this->set( 'm_intMessageOperatorId', trim( $arrValues['message_operator_id'] ) ); elseif( isset( $arrValues['message_operator_id'] ) ) $this->setMessageOperatorId( $arrValues['message_operator_id'] );
		if( isset( $arrValues['team_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamId', trim( $arrValues['team_id'] ) ); elseif( isset( $arrValues['team_id'] ) ) $this->setTeamId( $arrValues['team_id'] );
		if( isset( $arrValues['pdm_task_release_id'] ) && $boolDirectSet ) $this->set( 'm_intPdmTaskReleaseId', trim( $arrValues['pdm_task_release_id'] ) ); elseif( isset( $arrValues['pdm_task_release_id'] ) ) $this->setPdmTaskReleaseId( $arrValues['pdm_task_release_id'] );
		if( isset( $arrValues['call_id'] ) && $boolDirectSet ) $this->set( 'm_intCallId', trim( $arrValues['call_id'] ) ); elseif( isset( $arrValues['call_id'] ) ) $this->setCallId( $arrValues['call_id'] );
		if( isset( $arrValues['call_type'] ) && $boolDirectSet ) $this->set( 'm_intCallType', trim( $arrValues['call_type'] ) ); elseif( isset( $arrValues['call_type'] ) ) $this->setCallType( $arrValues['call_type'] );
		if( isset( $arrValues['revision_number'] ) && $boolDirectSet ) $this->set( 'm_intRevisionNumber', trim( $arrValues['revision_number'] ) ); elseif( isset( $arrValues['revision_number'] ) ) $this->setRevisionNumber( $arrValues['revision_number'] );
		if( isset( $arrValues['duration_minutes'] ) && $boolDirectSet ) $this->set( 'm_intDurationMinutes', trim( $arrValues['duration_minutes'] ) ); elseif( isset( $arrValues['duration_minutes'] ) ) $this->setDurationMinutes( $arrValues['duration_minutes'] );
		if( isset( $arrValues['calendar_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strCalendarPrimaryKey', trim( $arrValues['calendar_primary_key'] ) ); elseif( isset( $arrValues['calendar_primary_key'] ) ) $this->setCalendarPrimaryKey( $arrValues['calendar_primary_key'] );
		if( isset( $arrValues['title'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strTitle', trim( $arrValues['title'] ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( $arrValues['title'] );
		if( isset( $arrValues['description'] ) && false == $this->m_boolInitialized ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['contact_name_first'] ) && $boolDirectSet ) $this->set( 'm_strContactNameFirst', trim( $arrValues['contact_name_first'] ) ); elseif( isset( $arrValues['contact_name_first'] ) ) $this->setContactNameFirst( $arrValues['contact_name_first'] );
		if( isset( $arrValues['contact_name_last'] ) && $boolDirectSet ) $this->set( 'm_strContactNameLast', trim( $arrValues['contact_name_last'] ) ); elseif( isset( $arrValues['contact_name_last'] ) ) $this->setContactNameLast( $arrValues['contact_name_last'] );
		if( isset( $arrValues['contact_email_address'] ) && $boolDirectSet ) $this->set( 'm_strContactEmailAddress', trim( $arrValues['contact_email_address'] ) ); elseif( isset( $arrValues['contact_email_address'] ) ) $this->setContactEmailAddress( $arrValues['contact_email_address'] );
		if( isset( $arrValues['contact_phone_number'] ) && $boolDirectSet ) $this->set( 'm_strContactPhoneNumber', trim( $arrValues['contact_phone_number'] ) ); elseif( isset( $arrValues['contact_phone_number'] ) ) $this->setContactPhoneNumber( $arrValues['contact_phone_number'] );
		if( isset( $arrValues['phone_extension'] ) && $boolDirectSet ) $this->set( 'm_strPhoneExtension', trim( $arrValues['phone_extension'] ) ); elseif( isset( $arrValues['phone_extension'] ) ) $this->setPhoneExtension( $arrValues['phone_extension'] );
		if( isset( $arrValues['task_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTaskDatetime', trim( $arrValues['task_datetime'] ) ); elseif( isset( $arrValues['task_datetime'] ) ) $this->setTaskDatetime( $arrValues['task_datetime'] );
		if( isset( $arrValues['due_date'] ) && $boolDirectSet ) $this->set( 'm_strDueDate', trim( $arrValues['due_date'] ) ); elseif( isset( $arrValues['due_date'] ) ) $this->setDueDate( $arrValues['due_date'] );
		if( isset( $arrValues['follow_up_on'] ) && $boolDirectSet ) $this->set( 'm_strFollowUpOn', trim( $arrValues['follow_up_on'] ) ); elseif( isset( $arrValues['follow_up_on'] ) ) $this->setFollowUpOn( $arrValues['follow_up_on'] );
		if( isset( $arrValues['follow_up_datetime'] ) && $boolDirectSet ) $this->set( 'm_strFollowUpDatetime', trim( $arrValues['follow_up_datetime'] ) ); elseif( isset( $arrValues['follow_up_datetime'] ) ) $this->setFollowUpDatetime( $arrValues['follow_up_datetime'] );
		if( isset( $arrValues['original_due_date'] ) && $boolDirectSet ) $this->set( 'm_strOriginalDueDate', trim( $arrValues['original_due_date'] ) ); elseif( isset( $arrValues['original_due_date'] ) ) $this->setOriginalDueDate( $arrValues['original_due_date'] );
		if( isset( $arrValues['planned_completion_date'] ) && $boolDirectSet ) $this->set( 'm_strPlannedCompletionDate', trim( $arrValues['planned_completion_date'] ) ); elseif( isset( $arrValues['planned_completion_date'] ) ) $this->setPlannedCompletionDate( $arrValues['planned_completion_date'] );
		if( isset( $arrValues['tasks_last_verfied_on'] ) && $boolDirectSet ) $this->set( 'm_strTasksLastVerfiedOn', trim( $arrValues['tasks_last_verfied_on'] ) ); elseif( isset( $arrValues['tasks_last_verfied_on'] ) ) $this->setTasksLastVerfiedOn( $arrValues['tasks_last_verfied_on'] );
		if( isset( $arrValues['url'] ) && $boolDirectSet ) $this->set( 'm_strUrl', trim( $arrValues['url'] ) ); elseif( isset( $arrValues['url'] ) ) $this->setUrl( $arrValues['url'] );
		if( isset( $arrValues['marketing_title'] ) && $boolDirectSet ) $this->set( 'm_strMarketingTitle', trim( $arrValues['marketing_title'] ) ); elseif( isset( $arrValues['marketing_title'] ) ) $this->setMarketingTitle( $arrValues['marketing_title'] );
		if( isset( $arrValues['marketing_description'] ) && $boolDirectSet ) $this->set( 'm_strMarketingDescription', trim( $arrValues['marketing_description'] ) ); elseif( isset( $arrValues['marketing_description'] ) ) $this->setMarketingDescription( $arrValues['marketing_description'] );
		if( isset( $arrValues['keywords'] ) && $boolDirectSet ) $this->set( 'm_strKeywords', trim( $arrValues['keywords'] ) ); elseif( isset( $arrValues['keywords'] ) ) $this->setKeywords( $arrValues['keywords'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['is_under_consideration'] ) && $boolDirectSet ) $this->set( 'm_intIsUnderConsideration', trim( $arrValues['is_under_consideration'] ) ); elseif( isset( $arrValues['is_under_consideration'] ) ) $this->setIsUnderConsideration( $arrValues['is_under_consideration'] );
		if( isset( $arrValues['is_client_task'] ) && $boolDirectSet ) $this->set( 'm_intIsClientTask', trim( $arrValues['is_client_task'] ) ); elseif( isset( $arrValues['is_client_task'] ) ) $this->setIsClientTask( $arrValues['is_client_task'] );
		if( isset( $arrValues['is_support_ticket'] ) && $boolDirectSet ) $this->set( 'm_intIsSupportTicket', trim( $arrValues['is_support_ticket'] ) ); elseif( isset( $arrValues['is_support_ticket'] ) ) $this->setIsSupportTicket( $arrValues['is_support_ticket'] );
		if( isset( $arrValues['is_resident_contact'] ) && $boolDirectSet ) $this->set( 'm_intIsResidentContact', trim( $arrValues['is_resident_contact'] ) ); elseif( isset( $arrValues['is_resident_contact'] ) ) $this->setIsResidentContact( $arrValues['is_resident_contact'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_highlight_feature'] ) && $boolDirectSet ) $this->set( 'm_intIsHighlightFeature', trim( $arrValues['is_highlight_feature'] ) ); elseif( isset( $arrValues['is_highlight_feature'] ) ) $this->setIsHighlightFeature( $arrValues['is_highlight_feature'] );
		if( isset( $arrValues['calendar_remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strCalendarRemotePrimaryKey', trim( $arrValues['calendar_remote_primary_key'] ) ); elseif( isset( $arrValues['calendar_remote_primary_key'] ) ) $this->setCalendarRemotePrimaryKey( $arrValues['calendar_remote_primary_key'] );
		if( isset( $arrValues['charge_support_fee'] ) && $boolDirectSet ) $this->set( 'm_intChargeSupportFee', trim( $arrValues['charge_support_fee'] ) ); elseif( isset( $arrValues['charge_support_fee'] ) ) $this->setChargeSupportFee( $arrValues['charge_support_fee'] );
		if( isset( $arrValues['is_internal_release'] ) && $boolDirectSet ) $this->set( 'm_intIsInternalRelease', trim( $arrValues['is_internal_release'] ) ); elseif( isset( $arrValues['is_internal_release'] ) ) $this->setIsInternalRelease( $arrValues['is_internal_release'] );
		if( isset( $arrValues['is_data_fixes'] ) && $boolDirectSet ) $this->set( 'm_intIsDataFixes', trim( $arrValues['is_data_fixes'] ) ); elseif( isset( $arrValues['is_data_fixes'] ) ) $this->setIsDataFixes( $arrValues['is_data_fixes'] );
		if( isset( $arrValues['released_internally_on'] ) && $boolDirectSet ) $this->set( 'm_strReleasedInternallyOn', trim( $arrValues['released_internally_on'] ) ); elseif( isset( $arrValues['released_internally_on'] ) ) $this->setReleasedInternallyOn( $arrValues['released_internally_on'] );
		if( isset( $arrValues['released_externally_on'] ) && $boolDirectSet ) $this->set( 'm_strReleasedExternallyOn', trim( $arrValues['released_externally_on'] ) ); elseif( isset( $arrValues['released_externally_on'] ) ) $this->setReleasedExternallyOn( $arrValues['released_externally_on'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['code_reviewed_by'] ) && $boolDirectSet ) $this->set( 'm_intCodeReviewedBy', trim( $arrValues['code_reviewed_by'] ) ); elseif( isset( $arrValues['code_reviewed_by'] ) ) $this->setCodeReviewedBy( $arrValues['code_reviewed_by'] );
		if( isset( $arrValues['completed_by'] ) && $boolDirectSet ) $this->set( 'm_intCompletedBy', trim( $arrValues['completed_by'] ) ); elseif( isset( $arrValues['completed_by'] ) ) $this->setCompletedBy( $arrValues['completed_by'] );
		if( isset( $arrValues['release_note_by'] ) && $boolDirectSet ) $this->set( 'm_intReleaseNoteBy', trim( $arrValues['release_note_by'] ) ); elseif( isset( $arrValues['release_note_by'] ) ) $this->setReleaseNoteBy( $arrValues['release_note_by'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['first_response_on'] ) && $boolDirectSet ) $this->set( 'm_strFirstResponseOn', trim( $arrValues['first_response_on'] ) ); elseif( isset( $arrValues['first_response_on'] ) ) $this->setFirstResponseOn( $arrValues['first_response_on'] );
		if( isset( $arrValues['fees_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strFeesPostedOn', trim( $arrValues['fees_posted_on'] ) ); elseif( isset( $arrValues['fees_posted_on'] ) ) $this->setFeesPostedOn( $arrValues['fees_posted_on'] );
		if( isset( $arrValues['last_viewed_by'] ) && $boolDirectSet ) $this->set( 'm_intLastViewedBy', trim( $arrValues['last_viewed_by'] ) ); elseif( isset( $arrValues['last_viewed_by'] ) ) $this->setLastViewedBy( $arrValues['last_viewed_by'] );
		if( isset( $arrValues['last_viewed_on'] ) && $boolDirectSet ) $this->set( 'm_strLastViewedOn', trim( $arrValues['last_viewed_on'] ) ); elseif( isset( $arrValues['last_viewed_on'] ) ) $this->setLastViewedOn( $arrValues['last_viewed_on'] );
		if( isset( $arrValues['qa_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intQaApprovedBy', trim( $arrValues['qa_approved_by'] ) ); elseif( isset( $arrValues['qa_approved_by'] ) ) $this->setQaApprovedBy( $arrValues['qa_approved_by'] );
		if( isset( $arrValues['qa_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strQaApprovedOn', trim( $arrValues['qa_approved_on'] ) ); elseif( isset( $arrValues['qa_approved_on'] ) ) $this->setQaApprovedOn( $arrValues['qa_approved_on'] );
		if( isset( $arrValues['closed_by'] ) && $boolDirectSet ) $this->set( 'm_intClosedBy', trim( $arrValues['closed_by'] ) ); elseif( isset( $arrValues['closed_by'] ) ) $this->setClosedBy( $arrValues['closed_by'] );
		if( isset( $arrValues['closed_on'] ) && $boolDirectSet ) $this->set( 'm_strClosedOn', trim( $arrValues['closed_on'] ) ); elseif( isset( $arrValues['closed_on'] ) ) $this->setClosedOn( $arrValues['closed_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setGroupId( $intGroupId ) {
		$this->set( 'm_intGroupId', CStrings::strToIntDef( $intGroupId, NULL, false ) );
	}

	public function getGroupId() {
		return $this->m_intGroupId;
	}

	public function sqlGroupId() {
		return ( true == isset( $this->m_intGroupId ) ) ? ( string ) $this->m_intGroupId : 'NULL';
	}

	public function setCompanyUserId( $intCompanyUserId ) {
		$this->set( 'm_intCompanyUserId', CStrings::strToIntDef( $intCompanyUserId, NULL, false ) );
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function sqlCompanyUserId() {
		return ( true == isset( $this->m_intCompanyUserId ) ) ? ( string ) $this->m_intCompanyUserId : 'NULL';
	}

	public function setProjectManagerId( $intProjectManagerId ) {
		$this->set( 'm_intProjectManagerId', CStrings::strToIntDef( $intProjectManagerId, NULL, false ) );
	}

	public function getProjectManagerId() {
		return $this->m_intProjectManagerId;
	}

	public function sqlProjectManagerId() {
		return ( true == isset( $this->m_intProjectManagerId ) ) ? ( string ) $this->m_intProjectManagerId : 'NULL';
	}

	public function setClientResolutionEmployeeId( $intClientResolutionEmployeeId ) {
		$this->set( 'm_intClientResolutionEmployeeId', CStrings::strToIntDef( $intClientResolutionEmployeeId, NULL, false ) );
	}

	public function getClientResolutionEmployeeId() {
		return $this->m_intClientResolutionEmployeeId;
	}

	public function sqlClientResolutionEmployeeId() {
		return ( true == isset( $this->m_intClientResolutionEmployeeId ) ) ? ( string ) $this->m_intClientResolutionEmployeeId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : 'NULL';
	}

	public function setTaskReleaseId( $intTaskReleaseId ) {
		$this->set( 'm_intTaskReleaseId', CStrings::strToIntDef( $intTaskReleaseId, NULL, false ) );
	}

	public function getTaskReleaseId() {
		return $this->m_intTaskReleaseId;
	}

	public function sqlTaskReleaseId() {
		return ( true == isset( $this->m_intTaskReleaseId ) ) ? ( string ) $this->m_intTaskReleaseId : 'NULL';
	}

	public function setTaskStandardReleaseId( $intTaskStandardReleaseId ) {
		$this->set( 'm_intTaskStandardReleaseId', CStrings::strToIntDef( $intTaskStandardReleaseId, NULL, false ) );
	}

	public function getTaskStandardReleaseId() {
		return $this->m_intTaskStandardReleaseId;
	}

	public function sqlTaskStandardReleaseId() {
		return ( true == isset( $this->m_intTaskStandardReleaseId ) ) ? ( string ) $this->m_intTaskStandardReleaseId : 'NULL';
	}

	public function setTaskTypeId( $intTaskTypeId ) {
		$this->set( 'm_intTaskTypeId', CStrings::strToIntDef( $intTaskTypeId, NULL, false ) );
	}

	public function getTaskTypeId() {
		return $this->m_intTaskTypeId;
	}

	public function sqlTaskTypeId() {
		return ( true == isset( $this->m_intTaskTypeId ) ) ? ( string ) $this->m_intTaskTypeId : 'NULL';
	}

	public function setSubTaskTypeId( $intSubTaskTypeId ) {
		$this->set( 'm_intSubTaskTypeId', CStrings::strToIntDef( $intSubTaskTypeId, NULL, false ) );
	}

	public function getSubTaskTypeId() {
		return $this->m_intSubTaskTypeId;
	}

	public function sqlSubTaskTypeId() {
		return ( true == isset( $this->m_intSubTaskTypeId ) ) ? ( string ) $this->m_intSubTaskTypeId : 'NULL';
	}

	public function setTaskStatusId( $intTaskStatusId ) {
		$this->set( 'm_intTaskStatusId', CStrings::strToIntDef( $intTaskStatusId, NULL, false ) );
	}

	public function getTaskStatusId() {
		return $this->m_intTaskStatusId;
	}

	public function sqlTaskStatusId() {
		return ( true == isset( $this->m_intTaskStatusId ) ) ? ( string ) $this->m_intTaskStatusId : 'NULL';
	}

	public function setSeleniumScriptTypeId( $intSeleniumScriptTypeId ) {
		$this->set( 'm_intSeleniumScriptTypeId', CStrings::strToIntDef( $intSeleniumScriptTypeId, NULL, false ) );
	}

	public function getSeleniumScriptTypeId() {
		return $this->m_intSeleniumScriptTypeId;
	}

	public function sqlSeleniumScriptTypeId() {
		return ( true == isset( $this->m_intSeleniumScriptTypeId ) ) ? ( string ) $this->m_intSeleniumScriptTypeId : 'NULL';
	}

	public function setTaskPriorityId( $intTaskPriorityId ) {
		$this->set( 'm_intTaskPriorityId', CStrings::strToIntDef( $intTaskPriorityId, NULL, false ) );
	}

	public function getTaskPriorityId() {
		return $this->m_intTaskPriorityId;
	}

	public function sqlTaskPriorityId() {
		return ( true == isset( $this->m_intTaskPriorityId ) ) ? ( string ) $this->m_intTaskPriorityId : 'NULL';
	}

	public function setTaskResultId( $intTaskResultId ) {
		$this->set( 'm_intTaskResultId', CStrings::strToIntDef( $intTaskResultId, NULL, false ) );
	}

	public function getTaskResultId() {
		return $this->m_intTaskResultId;
	}

	public function sqlTaskResultId() {
		return ( true == isset( $this->m_intTaskResultId ) ) ? ( string ) $this->m_intTaskResultId : 'NULL';
	}

	public function setTaskPurchaseId( $intTaskPurchaseId ) {
		$this->set( 'm_intTaskPurchaseId', CStrings::strToIntDef( $intTaskPurchaseId, NULL, false ) );
	}

	public function getTaskPurchaseId() {
		return $this->m_intTaskPurchaseId;
	}

	public function sqlTaskPurchaseId() {
		return ( true == isset( $this->m_intTaskPurchaseId ) ) ? ( string ) $this->m_intTaskPurchaseId : 'NULL';
	}

	public function setTaskMediumId( $intTaskMediumId ) {
		$this->set( 'm_intTaskMediumId', CStrings::strToIntDef( $intTaskMediumId, NULL, false ) );
	}

	public function getTaskMediumId() {
		return $this->m_intTaskMediumId;
	}

	public function sqlTaskMediumId() {
		return ( true == isset( $this->m_intTaskMediumId ) ) ? ( string ) $this->m_intTaskMediumId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setMerchantAccountApplicationId( $intMerchantAccountApplicationId ) {
		$this->set( 'm_intMerchantAccountApplicationId', CStrings::strToIntDef( $intMerchantAccountApplicationId, NULL, false ) );
	}

	public function getMerchantAccountApplicationId() {
		return $this->m_intMerchantAccountApplicationId;
	}

	public function sqlMerchantAccountApplicationId() {
		return ( true == isset( $this->m_intMerchantAccountApplicationId ) ) ? ( string ) $this->m_intMerchantAccountApplicationId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setPsProductModuleId( $intPsProductModuleId ) {
		$this->set( 'm_intPsProductModuleId', CStrings::strToIntDef( $intPsProductModuleId, NULL, false ) );
	}

	public function getPsProductModuleId() {
		return $this->m_intPsProductModuleId;
	}

	public function sqlPsProductModuleId() {
		return ( true == isset( $this->m_intPsProductModuleId ) ) ? ( string ) $this->m_intPsProductModuleId : 'NULL';
	}

	public function setContactPhoneNumberTypeId( $intContactPhoneNumberTypeId ) {
		$this->set( 'm_intContactPhoneNumberTypeId', CStrings::strToIntDef( $intContactPhoneNumberTypeId, NULL, false ) );
	}

	public function getContactPhoneNumberTypeId() {
		return $this->m_intContactPhoneNumberTypeId;
	}

	public function sqlContactPhoneNumberTypeId() {
		return ( true == isset( $this->m_intContactPhoneNumberTypeId ) ) ? ( string ) $this->m_intContactPhoneNumberTypeId : 'NULL';
	}

	public function setMessageOperatorId( $intMessageOperatorId ) {
		$this->set( 'm_intMessageOperatorId', CStrings::strToIntDef( $intMessageOperatorId, NULL, false ) );
	}

	public function getMessageOperatorId() {
		return $this->m_intMessageOperatorId;
	}

	public function sqlMessageOperatorId() {
		return ( true == isset( $this->m_intMessageOperatorId ) ) ? ( string ) $this->m_intMessageOperatorId : 'NULL';
	}

	public function setTeamId( $intTeamId ) {
		$this->set( 'm_intTeamId', CStrings::strToIntDef( $intTeamId, NULL, false ) );
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function sqlTeamId() {
		return ( true == isset( $this->m_intTeamId ) ) ? ( string ) $this->m_intTeamId : 'NULL';
	}

	public function setPdmTaskReleaseId( $intPdmTaskReleaseId ) {
		$this->set( 'm_intPdmTaskReleaseId', CStrings::strToIntDef( $intPdmTaskReleaseId, NULL, false ) );
	}

	public function getPdmTaskReleaseId() {
		return $this->m_intPdmTaskReleaseId;
	}

	public function sqlPdmTaskReleaseId() {
		return ( true == isset( $this->m_intPdmTaskReleaseId ) ) ? ( string ) $this->m_intPdmTaskReleaseId : 'NULL';
	}

	public function setCallId( $intCallId ) {
		$this->set( 'm_intCallId', CStrings::strToIntDef( $intCallId, NULL, false ) );
	}

	public function getCallId() {
		return $this->m_intCallId;
	}

	public function sqlCallId() {
		return ( true == isset( $this->m_intCallId ) ) ? ( string ) $this->m_intCallId : 'NULL';
	}

	public function setCallType( $intCallType ) {
		$this->set( 'm_intCallType', CStrings::strToIntDef( $intCallType, NULL, false ) );
	}

	public function getCallType() {
		return $this->m_intCallType;
	}

	public function sqlCallType() {
		return ( true == isset( $this->m_intCallType ) ) ? ( string ) $this->m_intCallType : 'NULL';
	}

	public function setRevisionNumber( $intRevisionNumber ) {
		$this->set( 'm_intRevisionNumber', CStrings::strToIntDef( $intRevisionNumber, NULL, false ) );
	}

	public function getRevisionNumber() {
		return $this->m_intRevisionNumber;
	}

	public function sqlRevisionNumber() {
		return ( true == isset( $this->m_intRevisionNumber ) ) ? ( string ) $this->m_intRevisionNumber : 'NULL';
	}

	public function setDurationMinutes( $intDurationMinutes ) {
		$this->set( 'm_intDurationMinutes', CStrings::strToIntDef( $intDurationMinutes, NULL, false ) );
	}

	public function getDurationMinutes() {
		return $this->m_intDurationMinutes;
	}

	public function sqlDurationMinutes() {
		return ( true == isset( $this->m_intDurationMinutes ) ) ? ( string ) $this->m_intDurationMinutes : 'NULL';
	}

	public function setCalendarPrimaryKey( $strCalendarPrimaryKey ) {
		$this->set( 'm_strCalendarPrimaryKey', CStrings::strTrimDef( $strCalendarPrimaryKey, 4096, NULL, true ) );
	}

	public function getCalendarPrimaryKey() {
		return $this->m_strCalendarPrimaryKey;
	}

	public function sqlCalendarPrimaryKey() {
		return ( true == isset( $this->m_strCalendarPrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCalendarPrimaryKey ) : '\'' . addslashes( $this->m_strCalendarPrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setTitle( $strTitle, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strTitle', CStrings::strTrimDef( $strTitle, 240, NULL, true ), $strLocaleCode );
	}

	public function getTitle( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strTitle', $strLocaleCode );
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTitle ) : '\'' . addslashes( $this->m_strTitle ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription, $strLocaleCode = NULL ) {
		$this->setTranslated( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ), $strLocaleCode );
	}

	public function getDescription( $strLocaleCode = NULL ) {
		return $this->getTranslated( 'm_strDescription', $strLocaleCode );
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setContactNameFirst( $strContactNameFirst ) {
		$this->set( 'm_strContactNameFirst', CStrings::strTrimDef( $strContactNameFirst, 50, NULL, true ) );
	}

	public function getContactNameFirst() {
		return $this->m_strContactNameFirst;
	}

	public function sqlContactNameFirst() {
		return ( true == isset( $this->m_strContactNameFirst ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContactNameFirst ) : '\'' . addslashes( $this->m_strContactNameFirst ) . '\'' ) : 'NULL';
	}

	public function setContactNameLast( $strContactNameLast ) {
		$this->set( 'm_strContactNameLast', CStrings::strTrimDef( $strContactNameLast, 50, NULL, true ) );
	}

	public function getContactNameLast() {
		return $this->m_strContactNameLast;
	}

	public function sqlContactNameLast() {
		return ( true == isset( $this->m_strContactNameLast ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContactNameLast ) : '\'' . addslashes( $this->m_strContactNameLast ) . '\'' ) : 'NULL';
	}

	public function setContactEmailAddress( $strContactEmailAddress ) {
		$this->set( 'm_strContactEmailAddress', CStrings::strTrimDef( $strContactEmailAddress, 240, NULL, true ) );
	}

	public function getContactEmailAddress() {
		return $this->m_strContactEmailAddress;
	}

	public function sqlContactEmailAddress() {
		return ( true == isset( $this->m_strContactEmailAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContactEmailAddress ) : '\'' . addslashes( $this->m_strContactEmailAddress ) . '\'' ) : 'NULL';
	}

	public function setContactPhoneNumber( $strContactPhoneNumber ) {
		$this->set( 'm_strContactPhoneNumber', CStrings::strTrimDef( $strContactPhoneNumber, 30, NULL, true ) );
	}

	public function getContactPhoneNumber() {
		return $this->m_strContactPhoneNumber;
	}

	public function sqlContactPhoneNumber() {
		return ( true == isset( $this->m_strContactPhoneNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strContactPhoneNumber ) : '\'' . addslashes( $this->m_strContactPhoneNumber ) . '\'' ) : 'NULL';
	}

	public function setPhoneExtension( $strPhoneExtension ) {
		$this->set( 'm_strPhoneExtension', CStrings::strTrimDef( $strPhoneExtension, 20, NULL, true ) );
	}

	public function getPhoneExtension() {
		return $this->m_strPhoneExtension;
	}

	public function sqlPhoneExtension() {
		return ( true == isset( $this->m_strPhoneExtension ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPhoneExtension ) : '\'' . addslashes( $this->m_strPhoneExtension ) . '\'' ) : 'NULL';
	}

	public function setTaskDatetime( $strTaskDatetime ) {
		$this->set( 'm_strTaskDatetime', CStrings::strTrimDef( $strTaskDatetime, -1, NULL, true ) );
	}

	public function getTaskDatetime() {
		return $this->m_strTaskDatetime;
	}

	public function sqlTaskDatetime() {
		return ( true == isset( $this->m_strTaskDatetime ) ) ? '\'' . $this->m_strTaskDatetime . '\'' : 'NOW()';
	}

	public function setDueDate( $strDueDate ) {
		$this->set( 'm_strDueDate', CStrings::strTrimDef( $strDueDate, -1, NULL, true ) );
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function sqlDueDate() {
		return ( true == isset( $this->m_strDueDate ) ) ? '\'' . $this->m_strDueDate . '\'' : 'NULL';
	}

	public function setFollowUpOn( $strFollowUpOn ) {
		$this->set( 'm_strFollowUpOn', CStrings::strTrimDef( $strFollowUpOn, -1, NULL, true ) );
	}

	public function getFollowUpOn() {
		return $this->m_strFollowUpOn;
	}

	public function sqlFollowUpOn() {
		return ( true == isset( $this->m_strFollowUpOn ) ) ? '\'' . $this->m_strFollowUpOn . '\'' : 'NULL';
	}

	public function setFollowUpDatetime( $strFollowUpDatetime ) {
		$this->set( 'm_strFollowUpDatetime', CStrings::strTrimDef( $strFollowUpDatetime, -1, NULL, true ) );
	}

	public function getFollowUpDatetime() {
		return $this->m_strFollowUpDatetime;
	}

	public function sqlFollowUpDatetime() {
		return ( true == isset( $this->m_strFollowUpDatetime ) ) ? '\'' . $this->m_strFollowUpDatetime . '\'' : 'NULL';
	}

	public function setOriginalDueDate( $strOriginalDueDate ) {
		$this->set( 'm_strOriginalDueDate', CStrings::strTrimDef( $strOriginalDueDate, -1, NULL, true ) );
	}

	public function getOriginalDueDate() {
		return $this->m_strOriginalDueDate;
	}

	public function sqlOriginalDueDate() {
		return ( true == isset( $this->m_strOriginalDueDate ) ) ? '\'' . $this->m_strOriginalDueDate . '\'' : 'NULL';
	}

	public function setPlannedCompletionDate( $strPlannedCompletionDate ) {
		$this->set( 'm_strPlannedCompletionDate', CStrings::strTrimDef( $strPlannedCompletionDate, -1, NULL, true ) );
	}

	public function getPlannedCompletionDate() {
		return $this->m_strPlannedCompletionDate;
	}

	public function sqlPlannedCompletionDate() {
		return ( true == isset( $this->m_strPlannedCompletionDate ) ) ? '\'' . $this->m_strPlannedCompletionDate . '\'' : 'NULL';
	}

	public function setTasksLastVerfiedOn( $strTasksLastVerfiedOn ) {
		$this->set( 'm_strTasksLastVerfiedOn', CStrings::strTrimDef( $strTasksLastVerfiedOn, -1, NULL, true ) );
	}

	public function getTasksLastVerfiedOn() {
		return $this->m_strTasksLastVerfiedOn;
	}

	public function sqlTasksLastVerfiedOn() {
		return ( true == isset( $this->m_strTasksLastVerfiedOn ) ) ? '\'' . $this->m_strTasksLastVerfiedOn . '\'' : 'NULL';
	}

	public function setUrl( $strUrl ) {
		$this->set( 'm_strUrl', CStrings::strTrimDef( $strUrl, -1, NULL, true ) );
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function sqlUrl() {
		return ( true == isset( $this->m_strUrl ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUrl ) : '\'' . addslashes( $this->m_strUrl ) . '\'' ) : 'NULL';
	}

	public function setMarketingTitle( $strMarketingTitle ) {
		$this->set( 'm_strMarketingTitle', CStrings::strTrimDef( $strMarketingTitle, 240, NULL, true ) );
	}

	public function getMarketingTitle() {
		return $this->m_strMarketingTitle;
	}

	public function sqlMarketingTitle() {
		return ( true == isset( $this->m_strMarketingTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMarketingTitle ) : '\'' . addslashes( $this->m_strMarketingTitle ) . '\'' ) : 'NULL';
	}

	public function setMarketingDescription( $strMarketingDescription ) {
		$this->set( 'm_strMarketingDescription', CStrings::strTrimDef( $strMarketingDescription, -1, NULL, true ) );
	}

	public function getMarketingDescription() {
		return $this->m_strMarketingDescription;
	}

	public function sqlMarketingDescription() {
		return ( true == isset( $this->m_strMarketingDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMarketingDescription ) : '\'' . addslashes( $this->m_strMarketingDescription ) . '\'' ) : 'NULL';
	}

	public function setKeywords( $strKeywords ) {
		$this->set( 'm_strKeywords', CStrings::strTrimDef( $strKeywords, NULL, NULL, true ) );
	}

	public function getKeywords() {
		return $this->m_strKeywords;
	}

	public function sqlKeywords() {
		return ( true == isset( $this->m_strKeywords ) ) ? '\'' . addslashes( $this->m_strKeywords ) . '\'' : 'NULL';
	}

	public function setIsUnderConsideration( $intIsUnderConsideration ) {
		$this->set( 'm_intIsUnderConsideration', CStrings::strToIntDef( $intIsUnderConsideration, NULL, false ) );
	}

	public function getIsUnderConsideration() {
		return $this->m_intIsUnderConsideration;
	}

	public function sqlIsUnderConsideration() {
		return ( true == isset( $this->m_intIsUnderConsideration ) ) ? ( string ) $this->m_intIsUnderConsideration : '0';
	}

	public function setIsClientTask( $intIsClientTask ) {
		$this->set( 'm_intIsClientTask', CStrings::strToIntDef( $intIsClientTask, NULL, false ) );
	}

	public function getIsClientTask() {
		return $this->m_intIsClientTask;
	}

	public function sqlIsClientTask() {
		return ( true == isset( $this->m_intIsClientTask ) ) ? ( string ) $this->m_intIsClientTask : '0';
	}

	public function setIsSupportTicket( $intIsSupportTicket ) {
		$this->set( 'm_intIsSupportTicket', CStrings::strToIntDef( $intIsSupportTicket, NULL, false ) );
	}

	public function getIsSupportTicket() {
		return $this->m_intIsSupportTicket;
	}

	public function sqlIsSupportTicket() {
		return ( true == isset( $this->m_intIsSupportTicket ) ) ? ( string ) $this->m_intIsSupportTicket : 'NULL';
	}

	public function setIsResidentContact( $intIsResidentContact ) {
		$this->set( 'm_intIsResidentContact', CStrings::strToIntDef( $intIsResidentContact, NULL, false ) );
	}

	public function getIsResidentContact() {
		return $this->m_intIsResidentContact;
	}

	public function sqlIsResidentContact() {
		return ( true == isset( $this->m_intIsResidentContact ) ) ? ( string ) $this->m_intIsResidentContact : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : 'NULL';
	}

	public function setIsHighlightFeature( $intIsHighlightFeature ) {
		$this->set( 'm_intIsHighlightFeature', CStrings::strToIntDef( $intIsHighlightFeature, NULL, false ) );
	}

	public function getIsHighlightFeature() {
		return $this->m_intIsHighlightFeature;
	}

	public function sqlIsHighlightFeature() {
		return ( true == isset( $this->m_intIsHighlightFeature ) ) ? ( string ) $this->m_intIsHighlightFeature : '0';
	}

	public function setCalendarRemotePrimaryKey( $strCalendarRemotePrimaryKey ) {
		$this->set( 'm_strCalendarRemotePrimaryKey', CStrings::strTrimDef( $strCalendarRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getCalendarRemotePrimaryKey() {
		return $this->m_strCalendarRemotePrimaryKey;
	}

	public function sqlCalendarRemotePrimaryKey() {
		return ( true == isset( $this->m_strCalendarRemotePrimaryKey ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCalendarRemotePrimaryKey ) : '\'' . addslashes( $this->m_strCalendarRemotePrimaryKey ) . '\'' ) : 'NULL';
	}

	public function setChargeSupportFee( $intChargeSupportFee ) {
		$this->set( 'm_intChargeSupportFee', CStrings::strToIntDef( $intChargeSupportFee, NULL, false ) );
	}

	public function getChargeSupportFee() {
		return $this->m_intChargeSupportFee;
	}

	public function sqlChargeSupportFee() {
		return ( true == isset( $this->m_intChargeSupportFee ) ) ? ( string ) $this->m_intChargeSupportFee : '0';
	}

	public function setIsInternalRelease( $intIsInternalRelease ) {
		$this->set( 'm_intIsInternalRelease', CStrings::strToIntDef( $intIsInternalRelease, NULL, false ) );
	}

	public function getIsInternalRelease() {
		return $this->m_intIsInternalRelease;
	}

	public function sqlIsInternalRelease() {
		return ( true == isset( $this->m_intIsInternalRelease ) ) ? ( string ) $this->m_intIsInternalRelease : 'NULL';
	}

	public function setIsDataFixes( $intIsDataFixes ) {
		$this->set( 'm_intIsDataFixes', CStrings::strToIntDef( $intIsDataFixes, NULL, false ) );
	}

	public function getIsDataFixes() {
		return $this->m_intIsDataFixes;
	}

	public function sqlIsDataFixes() {
		return ( true == isset( $this->m_intIsDataFixes ) ) ? ( string ) $this->m_intIsDataFixes : '0';
	}

	public function setReleasedInternallyOn( $strReleasedInternallyOn ) {
		$this->set( 'm_strReleasedInternallyOn', CStrings::strTrimDef( $strReleasedInternallyOn, -1, NULL, true ) );
	}

	public function getReleasedInternallyOn() {
		return $this->m_strReleasedInternallyOn;
	}

	public function sqlReleasedInternallyOn() {
		return ( true == isset( $this->m_strReleasedInternallyOn ) ) ? '\'' . $this->m_strReleasedInternallyOn . '\'' : 'NULL';
	}

	public function setReleasedExternallyOn( $strReleasedExternallyOn ) {
		$this->set( 'm_strReleasedExternallyOn', CStrings::strTrimDef( $strReleasedExternallyOn, -1, NULL, true ) );
	}

	public function getReleasedExternallyOn() {
		return $this->m_strReleasedExternallyOn;
	}

	public function sqlReleasedExternallyOn() {
		return ( true == isset( $this->m_strReleasedExternallyOn ) ) ? '\'' . $this->m_strReleasedExternallyOn . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setCodeReviewedBy( $intCodeReviewedBy ) {
		$this->set( 'm_intCodeReviewedBy', CStrings::strToIntDef( $intCodeReviewedBy, NULL, false ) );
	}

	public function getCodeReviewedBy() {
		return $this->m_intCodeReviewedBy;
	}

	public function sqlCodeReviewedBy() {
		return ( true == isset( $this->m_intCodeReviewedBy ) ) ? ( string ) $this->m_intCodeReviewedBy : 'NULL';
	}

	public function setCompletedBy( $intCompletedBy ) {
		$this->set( 'm_intCompletedBy', CStrings::strToIntDef( $intCompletedBy, NULL, false ) );
	}

	public function getCompletedBy() {
		return $this->m_intCompletedBy;
	}

	public function sqlCompletedBy() {
		return ( true == isset( $this->m_intCompletedBy ) ) ? ( string ) $this->m_intCompletedBy : 'NULL';
	}

	public function setReleaseNoteBy( $intReleaseNoteBy ) {
		$this->set( 'm_intReleaseNoteBy', CStrings::strToIntDef( $intReleaseNoteBy, NULL, false ) );
	}

	public function getReleaseNoteBy() {
		return $this->m_intReleaseNoteBy;
	}

	public function sqlReleaseNoteBy() {
		return ( true == isset( $this->m_intReleaseNoteBy ) ) ? ( string ) $this->m_intReleaseNoteBy : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setFirstResponseOn( $strFirstResponseOn ) {
		$this->set( 'm_strFirstResponseOn', CStrings::strTrimDef( $strFirstResponseOn, -1, NULL, true ) );
	}

	public function getFirstResponseOn() {
		return $this->m_strFirstResponseOn;
	}

	public function sqlFirstResponseOn() {
		return ( true == isset( $this->m_strFirstResponseOn ) ) ? '\'' . $this->m_strFirstResponseOn . '\'' : 'NULL';
	}

	public function setFeesPostedOn( $strFeesPostedOn ) {
		$this->set( 'm_strFeesPostedOn', CStrings::strTrimDef( $strFeesPostedOn, -1, NULL, true ) );
	}

	public function getFeesPostedOn() {
		return $this->m_strFeesPostedOn;
	}

	public function sqlFeesPostedOn() {
		return ( true == isset( $this->m_strFeesPostedOn ) ) ? '\'' . $this->m_strFeesPostedOn . '\'' : 'NULL';
	}

	public function setLastViewedBy( $intLastViewedBy ) {
		$this->set( 'm_intLastViewedBy', CStrings::strToIntDef( $intLastViewedBy, NULL, false ) );
	}

	public function getLastViewedBy() {
		return $this->m_intLastViewedBy;
	}

	public function sqlLastViewedBy() {
		return ( true == isset( $this->m_intLastViewedBy ) ) ? ( string ) $this->m_intLastViewedBy : 'NULL';
	}

	public function setLastViewedOn( $strLastViewedOn ) {
		$this->set( 'm_strLastViewedOn', CStrings::strTrimDef( $strLastViewedOn, -1, NULL, true ) );
	}

	public function getLastViewedOn() {
		return $this->m_strLastViewedOn;
	}

	public function sqlLastViewedOn() {
		return ( true == isset( $this->m_strLastViewedOn ) ) ? '\'' . $this->m_strLastViewedOn . '\'' : 'NULL';
	}

	public function setQaApprovedBy( $intQaApprovedBy ) {
		$this->set( 'm_intQaApprovedBy', CStrings::strToIntDef( $intQaApprovedBy, NULL, false ) );
	}

	public function getQaApprovedBy() {
		return $this->m_intQaApprovedBy;
	}

	public function sqlQaApprovedBy() {
		return ( true == isset( $this->m_intQaApprovedBy ) ) ? ( string ) $this->m_intQaApprovedBy : 'NULL';
	}

	public function setQaApprovedOn( $strQaApprovedOn ) {
		$this->set( 'm_strQaApprovedOn', CStrings::strTrimDef( $strQaApprovedOn, -1, NULL, true ) );
	}

	public function getQaApprovedOn() {
		return $this->m_strQaApprovedOn;
	}

	public function sqlQaApprovedOn() {
		return ( true == isset( $this->m_strQaApprovedOn ) ) ? '\'' . $this->m_strQaApprovedOn . '\'' : 'NULL';
	}

	public function setClosedBy( $intClosedBy ) {
		$this->set( 'm_intClosedBy', CStrings::strToIntDef( $intClosedBy, NULL, false ) );
	}

	public function getClosedBy() {
		return $this->m_intClosedBy;
	}

	public function sqlClosedBy() {
		return ( true == isset( $this->m_intClosedBy ) ) ? ( string ) $this->m_intClosedBy : 'NULL';
	}

	public function setClosedOn( $strClosedOn ) {
		$this->set( 'm_strClosedOn', CStrings::strTrimDef( $strClosedOn, -1, NULL, true ) );
	}

	public function getClosedOn() {
		return $this->m_strClosedOn;
	}

	public function sqlClosedOn() {
		return ( true == isset( $this->m_strClosedOn ) ) ? '\'' . $this->m_strClosedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_lead_id, cid, user_id, group_id, company_user_id, project_manager_id, client_resolution_employee_id, department_id, cluster_id, task_release_id, task_standard_release_id, task_type_id, sub_task_type_id, task_status_id, selenium_script_type_id, task_priority_id, task_result_id, task_purchase_id, task_medium_id, property_id, merchant_account_application_id, ps_product_id, ps_product_option_id, ps_product_module_id, contact_phone_number_type_id, message_operator_id, team_id, pdm_task_release_id, call_id, call_type, revision_number, duration_minutes, calendar_primary_key, title, description, contact_name_first, contact_name_last, contact_email_address, contact_phone_number, phone_extension, task_datetime, due_date, follow_up_on, follow_up_datetime, original_due_date, planned_completion_date, tasks_last_verfied_on, url, marketing_title, marketing_description, keywords, details, is_under_consideration, is_client_task, is_support_ticket, is_resident_contact, is_published, is_highlight_feature, calendar_remote_primary_key, charge_support_fee, is_internal_release, is_data_fixes, released_internally_on, released_externally_on, order_num, code_reviewed_by, completed_by, release_note_by, completed_on, first_response_on, fees_posted_on, last_viewed_by, last_viewed_on, qa_approved_by, qa_approved_on, closed_by, closed_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPsLeadId() . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlUserId() . ', ' .
						$this->sqlGroupId() . ', ' .
						$this->sqlCompanyUserId() . ', ' .
						$this->sqlProjectManagerId() . ', ' .
						$this->sqlClientResolutionEmployeeId() . ', ' .
						$this->sqlDepartmentId() . ', ' .
						$this->sqlClusterId() . ', ' .
						$this->sqlTaskReleaseId() . ', ' .
						$this->sqlTaskStandardReleaseId() . ', ' .
						$this->sqlTaskTypeId() . ', ' .
						$this->sqlSubTaskTypeId() . ', ' .
						$this->sqlTaskStatusId() . ', ' .
						$this->sqlSeleniumScriptTypeId() . ', ' .
						$this->sqlTaskPriorityId() . ', ' .
						$this->sqlTaskResultId() . ', ' .
						$this->sqlTaskPurchaseId() . ', ' .
						$this->sqlTaskMediumId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlMerchantAccountApplicationId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlPsProductOptionId() . ', ' .
						$this->sqlPsProductModuleId() . ', ' .
						$this->sqlContactPhoneNumberTypeId() . ', ' .
						$this->sqlMessageOperatorId() . ', ' .
						$this->sqlTeamId() . ', ' .
						$this->sqlPdmTaskReleaseId() . ', ' .
						$this->sqlCallId() . ', ' .
						$this->sqlCallType() . ', ' .
						$this->sqlRevisionNumber() . ', ' .
						$this->sqlDurationMinutes() . ', ' .
						$this->sqlCalendarPrimaryKey() . ', ' .
						$this->sqlTitle() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlContactNameFirst() . ', ' .
						$this->sqlContactNameLast() . ', ' .
						$this->sqlContactEmailAddress() . ', ' .
						$this->sqlContactPhoneNumber() . ', ' .
						$this->sqlPhoneExtension() . ', ' .
						$this->sqlTaskDatetime() . ', ' .
						$this->sqlDueDate() . ', ' .
						$this->sqlFollowUpOn() . ', ' .
						$this->sqlFollowUpDatetime() . ', ' .
						$this->sqlOriginalDueDate() . ', ' .
						$this->sqlPlannedCompletionDate() . ', ' .
						$this->sqlTasksLastVerfiedOn() . ', ' .
						$this->sqlUrl() . ', ' .
						$this->sqlMarketingTitle() . ', ' .
						$this->sqlMarketingDescription() . ', ' .
						$this->sqlKeywords() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlIsUnderConsideration() . ', ' .
						$this->sqlIsClientTask() . ', ' .
						$this->sqlIsSupportTicket() . ', ' .
						$this->sqlIsResidentContact() . ', ' .
						$this->sqlIsPublished() . ', ' .
						$this->sqlIsHighlightFeature() . ', ' .
						$this->sqlCalendarRemotePrimaryKey() . ', ' .
						$this->sqlChargeSupportFee() . ', ' .
						$this->sqlIsInternalRelease() . ', ' .
						$this->sqlIsDataFixes() . ', ' .
						$this->sqlReleasedInternallyOn() . ', ' .
						$this->sqlReleasedExternallyOn() . ', ' .
						$this->sqlOrderNum() . ', ' .
						$this->sqlCodeReviewedBy() . ', ' .
						$this->sqlCompletedBy() . ', ' .
						$this->sqlReleaseNoteBy() . ', ' .
						$this->sqlCompletedOn() . ', ' .
						$this->sqlFirstResponseOn() . ', ' .
						$this->sqlFeesPostedOn() . ', ' .
						$this->sqlLastViewedBy() . ', ' .
						$this->sqlLastViewedOn() . ', ' .
						$this->sqlQaApprovedBy() . ', ' .
						$this->sqlQaApprovedOn() . ', ' .
						$this->sqlClosedBy() . ', ' .
						$this->sqlClosedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId(). ',' ; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_id = ' . $this->sqlGroupId(). ',' ; } elseif( true == array_key_exists( 'GroupId', $this->getChangedColumns() ) ) { $strSql .= ' group_id = ' . $this->sqlGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId(). ',' ; } elseif( true == array_key_exists( 'CompanyUserId', $this->getChangedColumns() ) ) { $strSql .= ' company_user_id = ' . $this->sqlCompanyUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_manager_id = ' . $this->sqlProjectManagerId(). ',' ; } elseif( true == array_key_exists( 'ProjectManagerId', $this->getChangedColumns() ) ) { $strSql .= ' project_manager_id = ' . $this->sqlProjectManagerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' client_resolution_employee_id = ' . $this->sqlClientResolutionEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ClientResolutionEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' client_resolution_employee_id = ' . $this->sqlClientResolutionEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId(). ',' ; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId(). ',' ; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_release_id = ' . $this->sqlTaskReleaseId(). ',' ; } elseif( true == array_key_exists( 'TaskReleaseId', $this->getChangedColumns() ) ) { $strSql .= ' task_release_id = ' . $this->sqlTaskReleaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_standard_release_id = ' . $this->sqlTaskStandardReleaseId(). ',' ; } elseif( true == array_key_exists( 'TaskStandardReleaseId', $this->getChangedColumns() ) ) { $strSql .= ' task_standard_release_id = ' . $this->sqlTaskStandardReleaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_type_id = ' . $this->sqlTaskTypeId(). ',' ; } elseif( true == array_key_exists( 'TaskTypeId', $this->getChangedColumns() ) ) { $strSql .= ' task_type_id = ' . $this->sqlTaskTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sub_task_type_id = ' . $this->sqlSubTaskTypeId(). ',' ; } elseif( true == array_key_exists( 'SubTaskTypeId', $this->getChangedColumns() ) ) { $strSql .= ' sub_task_type_id = ' . $this->sqlSubTaskTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_status_id = ' . $this->sqlTaskStatusId(). ',' ; } elseif( true == array_key_exists( 'TaskStatusId', $this->getChangedColumns() ) ) { $strSql .= ' task_status_id = ' . $this->sqlTaskStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' selenium_script_type_id = ' . $this->sqlSeleniumScriptTypeId(). ',' ; } elseif( true == array_key_exists( 'SeleniumScriptTypeId', $this->getChangedColumns() ) ) { $strSql .= ' selenium_script_type_id = ' . $this->sqlSeleniumScriptTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_priority_id = ' . $this->sqlTaskPriorityId(). ',' ; } elseif( true == array_key_exists( 'TaskPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' task_priority_id = ' . $this->sqlTaskPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_result_id = ' . $this->sqlTaskResultId(). ',' ; } elseif( true == array_key_exists( 'TaskResultId', $this->getChangedColumns() ) ) { $strSql .= ' task_result_id = ' . $this->sqlTaskResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_purchase_id = ' . $this->sqlTaskPurchaseId(). ',' ; } elseif( true == array_key_exists( 'TaskPurchaseId', $this->getChangedColumns() ) ) { $strSql .= ' task_purchase_id = ' . $this->sqlTaskPurchaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_medium_id = ' . $this->sqlTaskMediumId(). ',' ; } elseif( true == array_key_exists( 'TaskMediumId', $this->getChangedColumns() ) ) { $strSql .= ' task_medium_id = ' . $this->sqlTaskMediumId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' merchant_account_application_id = ' . $this->sqlMerchantAccountApplicationId(). ',' ; } elseif( true == array_key_exists( 'MerchantAccountApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' merchant_account_application_id = ' . $this->sqlMerchantAccountApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId(). ',' ; } elseif( true == array_key_exists( 'PsProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_module_id = ' . $this->sqlPsProductModuleId(). ',' ; } elseif( true == array_key_exists( 'PsProductModuleId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_module_id = ' . $this->sqlPsProductModuleId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_phone_number_type_id = ' . $this->sqlContactPhoneNumberTypeId(). ',' ; } elseif( true == array_key_exists( 'ContactPhoneNumberTypeId', $this->getChangedColumns() ) ) { $strSql .= ' contact_phone_number_type_id = ' . $this->sqlContactPhoneNumberTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message_operator_id = ' . $this->sqlMessageOperatorId(). ',' ; } elseif( true == array_key_exists( 'MessageOperatorId', $this->getChangedColumns() ) ) { $strSql .= ' message_operator_id = ' . $this->sqlMessageOperatorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_id = ' . $this->sqlTeamId(). ',' ; } elseif( true == array_key_exists( 'TeamId', $this->getChangedColumns() ) ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pdm_task_release_id = ' . $this->sqlPdmTaskReleaseId(). ',' ; } elseif( true == array_key_exists( 'PdmTaskReleaseId', $this->getChangedColumns() ) ) { $strSql .= ' pdm_task_release_id = ' . $this->sqlPdmTaskReleaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_id = ' . $this->sqlCallId(). ',' ; } elseif( true == array_key_exists( 'CallId', $this->getChangedColumns() ) ) { $strSql .= ' call_id = ' . $this->sqlCallId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' call_type = ' . $this->sqlCallType(). ',' ; } elseif( true == array_key_exists( 'CallType', $this->getChangedColumns() ) ) { $strSql .= ' call_type = ' . $this->sqlCallType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revision_number = ' . $this->sqlRevisionNumber(). ',' ; } elseif( true == array_key_exists( 'RevisionNumber', $this->getChangedColumns() ) ) { $strSql .= ' revision_number = ' . $this->sqlRevisionNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' duration_minutes = ' . $this->sqlDurationMinutes(). ',' ; } elseif( true == array_key_exists( 'DurationMinutes', $this->getChangedColumns() ) ) { $strSql .= ' duration_minutes = ' . $this->sqlDurationMinutes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calendar_primary_key = ' . $this->sqlCalendarPrimaryKey(). ',' ; } elseif( true == array_key_exists( 'CalendarPrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' calendar_primary_key = ' . $this->sqlCalendarPrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle(). ',' ; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_name_first = ' . $this->sqlContactNameFirst(). ',' ; } elseif( true == array_key_exists( 'ContactNameFirst', $this->getChangedColumns() ) ) { $strSql .= ' contact_name_first = ' . $this->sqlContactNameFirst() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_name_last = ' . $this->sqlContactNameLast(). ',' ; } elseif( true == array_key_exists( 'ContactNameLast', $this->getChangedColumns() ) ) { $strSql .= ' contact_name_last = ' . $this->sqlContactNameLast() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_email_address = ' . $this->sqlContactEmailAddress(). ',' ; } elseif( true == array_key_exists( 'ContactEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' contact_email_address = ' . $this->sqlContactEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber(). ',' ; } elseif( true == array_key_exists( 'ContactPhoneNumber', $this->getChangedColumns() ) ) { $strSql .= ' contact_phone_number = ' . $this->sqlContactPhoneNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' phone_extension = ' . $this->sqlPhoneExtension(). ',' ; } elseif( true == array_key_exists( 'PhoneExtension', $this->getChangedColumns() ) ) { $strSql .= ' phone_extension = ' . $this->sqlPhoneExtension() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_datetime = ' . $this->sqlTaskDatetime(). ',' ; } elseif( true == array_key_exists( 'TaskDatetime', $this->getChangedColumns() ) ) { $strSql .= ' task_datetime = ' . $this->sqlTaskDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' due_date = ' . $this->sqlDueDate(). ',' ; } elseif( true == array_key_exists( 'DueDate', $this->getChangedColumns() ) ) { $strSql .= ' due_date = ' . $this->sqlDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' follow_up_on = ' . $this->sqlFollowUpOn(). ',' ; } elseif( true == array_key_exists( 'FollowUpOn', $this->getChangedColumns() ) ) { $strSql .= ' follow_up_on = ' . $this->sqlFollowUpOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' follow_up_datetime = ' . $this->sqlFollowUpDatetime(). ',' ; } elseif( true == array_key_exists( 'FollowUpDatetime', $this->getChangedColumns() ) ) { $strSql .= ' follow_up_datetime = ' . $this->sqlFollowUpDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' original_due_date = ' . $this->sqlOriginalDueDate(). ',' ; } elseif( true == array_key_exists( 'OriginalDueDate', $this->getChangedColumns() ) ) { $strSql .= ' original_due_date = ' . $this->sqlOriginalDueDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' planned_completion_date = ' . $this->sqlPlannedCompletionDate(). ',' ; } elseif( true == array_key_exists( 'PlannedCompletionDate', $this->getChangedColumns() ) ) { $strSql .= ' planned_completion_date = ' . $this->sqlPlannedCompletionDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tasks_last_verfied_on = ' . $this->sqlTasksLastVerfiedOn(). ',' ; } elseif( true == array_key_exists( 'TasksLastVerfiedOn', $this->getChangedColumns() ) ) { $strSql .= ' tasks_last_verfied_on = ' . $this->sqlTasksLastVerfiedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' url = ' . $this->sqlUrl(). ',' ; } elseif( true == array_key_exists( 'Url', $this->getChangedColumns() ) ) { $strSql .= ' url = ' . $this->sqlUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_title = ' . $this->sqlMarketingTitle(). ',' ; } elseif( true == array_key_exists( 'MarketingTitle', $this->getChangedColumns() ) ) { $strSql .= ' marketing_title = ' . $this->sqlMarketingTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' marketing_description = ' . $this->sqlMarketingDescription(). ',' ; } elseif( true == array_key_exists( 'MarketingDescription', $this->getChangedColumns() ) ) { $strSql .= ' marketing_description = ' . $this->sqlMarketingDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' keywords = ' . $this->sqlKeywords(). ',' ; } elseif( true == array_key_exists( 'Keywords', $this->getChangedColumns() ) ) { $strSql .= ' keywords = ' . $this->sqlKeywords() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_under_consideration = ' . $this->sqlIsUnderConsideration(). ',' ; } elseif( true == array_key_exists( 'IsUnderConsideration', $this->getChangedColumns() ) ) { $strSql .= ' is_under_consideration = ' . $this->sqlIsUnderConsideration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_task = ' . $this->sqlIsClientTask(). ',' ; } elseif( true == array_key_exists( 'IsClientTask', $this->getChangedColumns() ) ) { $strSql .= ' is_client_task = ' . $this->sqlIsClientTask() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_support_ticket = ' . $this->sqlIsSupportTicket(). ',' ; } elseif( true == array_key_exists( 'IsSupportTicket', $this->getChangedColumns() ) ) { $strSql .= ' is_support_ticket = ' . $this->sqlIsSupportTicket() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_resident_contact = ' . $this->sqlIsResidentContact(). ',' ; } elseif( true == array_key_exists( 'IsResidentContact', $this->getChangedColumns() ) ) { $strSql .= ' is_resident_contact = ' . $this->sqlIsResidentContact() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_highlight_feature = ' . $this->sqlIsHighlightFeature(). ',' ; } elseif( true == array_key_exists( 'IsHighlightFeature', $this->getChangedColumns() ) ) { $strSql .= ' is_highlight_feature = ' . $this->sqlIsHighlightFeature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' calendar_remote_primary_key = ' . $this->sqlCalendarRemotePrimaryKey(). ',' ; } elseif( true == array_key_exists( 'CalendarRemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' calendar_remote_primary_key = ' . $this->sqlCalendarRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_support_fee = ' . $this->sqlChargeSupportFee(). ',' ; } elseif( true == array_key_exists( 'ChargeSupportFee', $this->getChangedColumns() ) ) { $strSql .= ' charge_support_fee = ' . $this->sqlChargeSupportFee() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_internal_release = ' . $this->sqlIsInternalRelease(). ',' ; } elseif( true == array_key_exists( 'IsInternalRelease', $this->getChangedColumns() ) ) { $strSql .= ' is_internal_release = ' . $this->sqlIsInternalRelease() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_data_fixes = ' . $this->sqlIsDataFixes(). ',' ; } elseif( true == array_key_exists( 'IsDataFixes', $this->getChangedColumns() ) ) { $strSql .= ' is_data_fixes = ' . $this->sqlIsDataFixes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' released_internally_on = ' . $this->sqlReleasedInternallyOn(). ',' ; } elseif( true == array_key_exists( 'ReleasedInternallyOn', $this->getChangedColumns() ) ) { $strSql .= ' released_internally_on = ' . $this->sqlReleasedInternallyOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' released_externally_on = ' . $this->sqlReleasedExternallyOn(). ',' ; } elseif( true == array_key_exists( 'ReleasedExternallyOn', $this->getChangedColumns() ) ) { $strSql .= ' released_externally_on = ' . $this->sqlReleasedExternallyOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' code_reviewed_by = ' . $this->sqlCodeReviewedBy(). ',' ; } elseif( true == array_key_exists( 'CodeReviewedBy', $this->getChangedColumns() ) ) { $strSql .= ' code_reviewed_by = ' . $this->sqlCodeReviewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy(). ',' ; } elseif( true == array_key_exists( 'CompletedBy', $this->getChangedColumns() ) ) { $strSql .= ' completed_by = ' . $this->sqlCompletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_note_by = ' . $this->sqlReleaseNoteBy(). ',' ; } elseif( true == array_key_exists( 'ReleaseNoteBy', $this->getChangedColumns() ) ) { $strSql .= ' release_note_by = ' . $this->sqlReleaseNoteBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn(). ',' ; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_response_on = ' . $this->sqlFirstResponseOn(). ',' ; } elseif( true == array_key_exists( 'FirstResponseOn', $this->getChangedColumns() ) ) { $strSql .= ' first_response_on = ' . $this->sqlFirstResponseOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' fees_posted_on = ' . $this->sqlFeesPostedOn(). ',' ; } elseif( true == array_key_exists( 'FeesPostedOn', $this->getChangedColumns() ) ) { $strSql .= ' fees_posted_on = ' . $this->sqlFeesPostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_viewed_by = ' . $this->sqlLastViewedBy(). ',' ; } elseif( true == array_key_exists( 'LastViewedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_viewed_by = ' . $this->sqlLastViewedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_viewed_on = ' . $this->sqlLastViewedOn(). ',' ; } elseif( true == array_key_exists( 'LastViewedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_viewed_on = ' . $this->sqlLastViewedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_approved_by = ' . $this->sqlQaApprovedBy(). ',' ; } elseif( true == array_key_exists( 'QaApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' qa_approved_by = ' . $this->sqlQaApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_approved_on = ' . $this->sqlQaApprovedOn(). ',' ; } elseif( true == array_key_exists( 'QaApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' qa_approved_on = ' . $this->sqlQaApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' closed_by = ' . $this->sqlClosedBy(). ',' ; } elseif( true == array_key_exists( 'ClosedBy', $this->getChangedColumns() ) ) { $strSql .= ' closed_by = ' . $this->sqlClosedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' closed_on = ' . $this->sqlClosedOn(). ',' ; } elseif( true == array_key_exists( 'ClosedOn', $this->getChangedColumns() ) ) { $strSql .= ' closed_on = ' . $this->sqlClosedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'cid' => $this->getCid(),
			'user_id' => $this->getUserId(),
			'group_id' => $this->getGroupId(),
			'company_user_id' => $this->getCompanyUserId(),
			'project_manager_id' => $this->getProjectManagerId(),
			'client_resolution_employee_id' => $this->getClientResolutionEmployeeId(),
			'department_id' => $this->getDepartmentId(),
			'cluster_id' => $this->getClusterId(),
			'task_release_id' => $this->getTaskReleaseId(),
			'task_standard_release_id' => $this->getTaskStandardReleaseId(),
			'task_type_id' => $this->getTaskTypeId(),
			'sub_task_type_id' => $this->getSubTaskTypeId(),
			'task_status_id' => $this->getTaskStatusId(),
			'selenium_script_type_id' => $this->getSeleniumScriptTypeId(),
			'task_priority_id' => $this->getTaskPriorityId(),
			'task_result_id' => $this->getTaskResultId(),
			'task_purchase_id' => $this->getTaskPurchaseId(),
			'task_medium_id' => $this->getTaskMediumId(),
			'property_id' => $this->getPropertyId(),
			'merchant_account_application_id' => $this->getMerchantAccountApplicationId(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'ps_product_module_id' => $this->getPsProductModuleId(),
			'contact_phone_number_type_id' => $this->getContactPhoneNumberTypeId(),
			'message_operator_id' => $this->getMessageOperatorId(),
			'team_id' => $this->getTeamId(),
			'pdm_task_release_id' => $this->getPdmTaskReleaseId(),
			'call_id' => $this->getCallId(),
			'call_type' => $this->getCallType(),
			'revision_number' => $this->getRevisionNumber(),
			'duration_minutes' => $this->getDurationMinutes(),
			'calendar_primary_key' => $this->getCalendarPrimaryKey(),
			'title' => $this->getTitle(),
			'description' => $this->getDescription(),
			'contact_name_first' => $this->getContactNameFirst(),
			'contact_name_last' => $this->getContactNameLast(),
			'contact_email_address' => $this->getContactEmailAddress(),
			'contact_phone_number' => $this->getContactPhoneNumber(),
			'phone_extension' => $this->getPhoneExtension(),
			'task_datetime' => $this->getTaskDatetime(),
			'due_date' => $this->getDueDate(),
			'follow_up_on' => $this->getFollowUpOn(),
			'follow_up_datetime' => $this->getFollowUpDatetime(),
			'original_due_date' => $this->getOriginalDueDate(),
			'planned_completion_date' => $this->getPlannedCompletionDate(),
			'tasks_last_verfied_on' => $this->getTasksLastVerfiedOn(),
			'url' => $this->getUrl(),
			'marketing_title' => $this->getMarketingTitle(),
			'marketing_description' => $this->getMarketingDescription(),
			'keywords' => $this->getKeywords(),
			'details' => $this->getDetails(),
			'is_under_consideration' => $this->getIsUnderConsideration(),
			'is_client_task' => $this->getIsClientTask(),
			'is_support_ticket' => $this->getIsSupportTicket(),
			'is_resident_contact' => $this->getIsResidentContact(),
			'is_published' => $this->getIsPublished(),
			'is_highlight_feature' => $this->getIsHighlightFeature(),
			'calendar_remote_primary_key' => $this->getCalendarRemotePrimaryKey(),
			'charge_support_fee' => $this->getChargeSupportFee(),
			'is_internal_release' => $this->getIsInternalRelease(),
			'is_data_fixes' => $this->getIsDataFixes(),
			'released_internally_on' => $this->getReleasedInternallyOn(),
			'released_externally_on' => $this->getReleasedExternallyOn(),
			'order_num' => $this->getOrderNum(),
			'code_reviewed_by' => $this->getCodeReviewedBy(),
			'completed_by' => $this->getCompletedBy(),
			'release_note_by' => $this->getReleaseNoteBy(),
			'completed_on' => $this->getCompletedOn(),
			'first_response_on' => $this->getFirstResponseOn(),
			'fees_posted_on' => $this->getFeesPostedOn(),
			'last_viewed_by' => $this->getLastViewedBy(),
			'last_viewed_on' => $this->getLastViewedOn(),
			'qa_approved_by' => $this->getQaApprovedBy(),
			'qa_approved_on' => $this->getQaApprovedOn(),
			'closed_by' => $this->getClosedBy(),
			'closed_on' => $this->getClosedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>