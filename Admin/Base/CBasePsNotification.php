<?php

class CBasePsNotification extends CEosSingularBase {

	const TABLE_NAME = 'public.ps_notifications';

	protected $m_intId;
	protected $m_intPsNotificationTypeId;
	protected $m_intEmployeeId;
	protected $m_intGroupId;
	protected $m_intPsDocumentId;
	protected $m_strMessage;
	protected $m_strReferenceUrl;
	protected $m_strAccessedOn;
	protected $m_intIsPublic;
	protected $m_boolIsSync;
	protected $m_boolIsShowPopup;
	protected $m_boolIsFromDepartment;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublic = '0';
		$this->m_boolIsSync = false;
		$this->m_boolIsShowPopup = false;
		$this->m_boolIsFromDepartment = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_notification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsNotificationTypeId', trim( $arrValues['ps_notification_type_id'] ) ); elseif( isset( $arrValues['ps_notification_type_id'] ) ) $this->setPsNotificationTypeId( $arrValues['ps_notification_type_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['group_id'] ) && $boolDirectSet ) $this->set( 'm_intGroupId', trim( $arrValues['group_id'] ) ); elseif( isset( $arrValues['group_id'] ) ) $this->setGroupId( $arrValues['group_id'] );
		if( isset( $arrValues['ps_document_id'] ) && $boolDirectSet ) $this->set( 'm_intPsDocumentId', trim( $arrValues['ps_document_id'] ) ); elseif( isset( $arrValues['ps_document_id'] ) ) $this->setPsDocumentId( $arrValues['ps_document_id'] );
		if( isset( $arrValues['message'] ) && $boolDirectSet ) $this->set( 'm_strMessage', trim( stripcslashes( $arrValues['message'] ) ) ); elseif( isset( $arrValues['message'] ) ) $this->setMessage( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['message'] ) : $arrValues['message'] );
		if( isset( $arrValues['reference_url'] ) && $boolDirectSet ) $this->set( 'm_strReferenceUrl', trim( stripcslashes( $arrValues['reference_url'] ) ) ); elseif( isset( $arrValues['reference_url'] ) ) $this->setReferenceUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reference_url'] ) : $arrValues['reference_url'] );
		if( isset( $arrValues['accessed_on'] ) && $boolDirectSet ) $this->set( 'm_strAccessedOn', trim( $arrValues['accessed_on'] ) ); elseif( isset( $arrValues['accessed_on'] ) ) $this->setAccessedOn( $arrValues['accessed_on'] );
		if( isset( $arrValues['is_public'] ) && $boolDirectSet ) $this->set( 'm_intIsPublic', trim( $arrValues['is_public'] ) ); elseif( isset( $arrValues['is_public'] ) ) $this->setIsPublic( $arrValues['is_public'] );
		if( isset( $arrValues['is_sync'] ) && $boolDirectSet ) $this->set( 'm_boolIsSync', trim( stripcslashes( $arrValues['is_sync'] ) ) ); elseif( isset( $arrValues['is_sync'] ) ) $this->setIsSync( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_sync'] ) : $arrValues['is_sync'] );
		if( isset( $arrValues['is_show_popup'] ) && $boolDirectSet ) $this->set( 'm_boolIsShowPopup', trim( stripcslashes( $arrValues['is_show_popup'] ) ) ); elseif( isset( $arrValues['is_show_popup'] ) ) $this->setIsShowPopup( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_show_popup'] ) : $arrValues['is_show_popup'] );
		if( isset( $arrValues['is_from_department'] ) && $boolDirectSet ) $this->set( 'm_boolIsFromDepartment', trim( stripcslashes( $arrValues['is_from_department'] ) ) ); elseif( isset( $arrValues['is_from_department'] ) ) $this->setIsFromDepartment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_from_department'] ) : $arrValues['is_from_department'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsNotificationTypeId( $intPsNotificationTypeId ) {
		$this->set( 'm_intPsNotificationTypeId', CStrings::strToIntDef( $intPsNotificationTypeId, NULL, false ) );
	}

	public function getPsNotificationTypeId() {
		return $this->m_intPsNotificationTypeId;
	}

	public function sqlPsNotificationTypeId() {
		return ( true == isset( $this->m_intPsNotificationTypeId ) ) ? ( string ) $this->m_intPsNotificationTypeId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setGroupId( $intGroupId ) {
		$this->set( 'm_intGroupId', CStrings::strToIntDef( $intGroupId, NULL, false ) );
	}

	public function getGroupId() {
		return $this->m_intGroupId;
	}

	public function sqlGroupId() {
		return ( true == isset( $this->m_intGroupId ) ) ? ( string ) $this->m_intGroupId : 'NULL';
	}

	public function setPsDocumentId( $intPsDocumentId ) {
		$this->set( 'm_intPsDocumentId', CStrings::strToIntDef( $intPsDocumentId, NULL, false ) );
	}

	public function getPsDocumentId() {
		return $this->m_intPsDocumentId;
	}

	public function sqlPsDocumentId() {
		return ( true == isset( $this->m_intPsDocumentId ) ) ? ( string ) $this->m_intPsDocumentId : 'NULL';
	}

	public function setMessage( $strMessage ) {
		$this->set( 'm_strMessage', CStrings::strTrimDef( $strMessage, -1, NULL, true ) );
	}

	public function getMessage() {
		return $this->m_strMessage;
	}

	public function sqlMessage() {
		return ( true == isset( $this->m_strMessage ) ) ? '\'' . addslashes( $this->m_strMessage ) . '\'' : 'NULL';
	}

	public function setReferenceUrl( $strReferenceUrl ) {
		$this->set( 'm_strReferenceUrl', CStrings::strTrimDef( $strReferenceUrl, 250, NULL, true ) );
	}

	public function getReferenceUrl() {
		return $this->m_strReferenceUrl;
	}

	public function sqlReferenceUrl() {
		return ( true == isset( $this->m_strReferenceUrl ) ) ? '\'' . addslashes( $this->m_strReferenceUrl ) . '\'' : 'NULL';
	}

	public function setAccessedOn( $strAccessedOn ) {
		$this->set( 'm_strAccessedOn', CStrings::strTrimDef( $strAccessedOn, -1, NULL, true ) );
	}

	public function getAccessedOn() {
		return $this->m_strAccessedOn;
	}

	public function sqlAccessedOn() {
		return ( true == isset( $this->m_strAccessedOn ) ) ? '\'' . $this->m_strAccessedOn . '\'' : 'NULL';
	}

	public function setIsPublic( $intIsPublic ) {
		$this->set( 'm_intIsPublic', CStrings::strToIntDef( $intIsPublic, NULL, false ) );
	}

	public function getIsPublic() {
		return $this->m_intIsPublic;
	}

	public function sqlIsPublic() {
		return ( true == isset( $this->m_intIsPublic ) ) ? ( string ) $this->m_intIsPublic : '0';
	}

	public function setIsSync( $boolIsSync ) {
		$this->set( 'm_boolIsSync', CStrings::strToBool( $boolIsSync ) );
	}

	public function getIsSync() {
		return $this->m_boolIsSync;
	}

	public function sqlIsSync() {
		return ( true == isset( $this->m_boolIsSync ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsSync ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsShowPopup( $boolIsShowPopup ) {
		$this->set( 'm_boolIsShowPopup', CStrings::strToBool( $boolIsShowPopup ) );
	}

	public function getIsShowPopup() {
		return $this->m_boolIsShowPopup;
	}

	public function sqlIsShowPopup() {
		return ( true == isset( $this->m_boolIsShowPopup ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsShowPopup ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsFromDepartment( $boolIsFromDepartment ) {
		$this->set( 'm_boolIsFromDepartment', CStrings::strToBool( $boolIsFromDepartment ) );
	}

	public function getIsFromDepartment() {
		return $this->m_boolIsFromDepartment;
	}

	public function sqlIsFromDepartment() {
		return ( true == isset( $this->m_boolIsFromDepartment ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFromDepartment ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_notification_type_id, employee_id, group_id, ps_document_id, message, reference_url, accessed_on, is_public, is_sync, is_show_popup, is_from_department, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsNotificationTypeId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlGroupId() . ', ' .
 						$this->sqlPsDocumentId() . ', ' .
 						$this->sqlMessage() . ', ' .
 						$this->sqlReferenceUrl() . ', ' .
 						$this->sqlAccessedOn() . ', ' .
 						$this->sqlIsPublic() . ', ' .
 						$this->sqlIsSync() . ', ' .
 						$this->sqlIsShowPopup() . ', ' .
 						$this->sqlIsFromDepartment() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_notification_type_id = ' . $this->sqlPsNotificationTypeId() . ','; } elseif( true == array_key_exists( 'PsNotificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ps_notification_type_id = ' . $this->sqlPsNotificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_id = ' . $this->sqlGroupId() . ','; } elseif( true == array_key_exists( 'GroupId', $this->getChangedColumns() ) ) { $strSql .= ' group_id = ' . $this->sqlGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; } elseif( true == array_key_exists( 'PsDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; } elseif( true == array_key_exists( 'Message', $this->getChangedColumns() ) ) { $strSql .= ' message = ' . $this->sqlMessage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reference_url = ' . $this->sqlReferenceUrl() . ','; } elseif( true == array_key_exists( 'ReferenceUrl', $this->getChangedColumns() ) ) { $strSql .= ' reference_url = ' . $this->sqlReferenceUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accessed_on = ' . $this->sqlAccessedOn() . ','; } elseif( true == array_key_exists( 'AccessedOn', $this->getChangedColumns() ) ) { $strSql .= ' accessed_on = ' . $this->sqlAccessedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_public = ' . $this->sqlIsPublic() . ','; } elseif( true == array_key_exists( 'IsPublic', $this->getChangedColumns() ) ) { $strSql .= ' is_public = ' . $this->sqlIsPublic() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_sync = ' . $this->sqlIsSync() . ','; } elseif( true == array_key_exists( 'IsSync', $this->getChangedColumns() ) ) { $strSql .= ' is_sync = ' . $this->sqlIsSync() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_show_popup = ' . $this->sqlIsShowPopup() . ','; } elseif( true == array_key_exists( 'IsShowPopup', $this->getChangedColumns() ) ) { $strSql .= ' is_show_popup = ' . $this->sqlIsShowPopup() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_from_department = ' . $this->sqlIsFromDepartment() . ','; } elseif( true == array_key_exists( 'IsFromDepartment', $this->getChangedColumns() ) ) { $strSql .= ' is_from_department = ' . $this->sqlIsFromDepartment() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_notification_type_id' => $this->getPsNotificationTypeId(),
			'employee_id' => $this->getEmployeeId(),
			'group_id' => $this->getGroupId(),
			'ps_document_id' => $this->getPsDocumentId(),
			'message' => $this->getMessage(),
			'reference_url' => $this->getReferenceUrl(),
			'accessed_on' => $this->getAccessedOn(),
			'is_public' => $this->getIsPublic(),
			'is_sync' => $this->getIsSync(),
			'is_show_popup' => $this->getIsShowPopup(),
			'is_from_department' => $this->getIsFromDepartment(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>