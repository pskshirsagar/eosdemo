<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CBacklogTaskRanks
 * Do not add any new functions to this class.
 */

class CBaseBacklogTaskRanks extends CEosPluralBase {

	/**
	 * @return CBacklogTaskRank[]
	 */
	public static function fetchBacklogTaskRanks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CBacklogTaskRank::class, $objDatabase );
	}

	/**
	 * @return CBacklogTaskRank
	 */
	public static function fetchBacklogTaskRank( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CBacklogTaskRank::class, $objDatabase );
	}

	public static function fetchBacklogTaskRankCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'backlog_task_ranks', $objDatabase );
	}

	public static function fetchBacklogTaskRankById( $intId, $objDatabase ) {
		return self::fetchBacklogTaskRank( sprintf( 'SELECT * FROM backlog_task_ranks WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchBacklogTaskRanksByTeamBacklogId( $intTeamBacklogId, $objDatabase ) {
		return self::fetchBacklogTaskRanks( sprintf( 'SELECT * FROM backlog_task_ranks WHERE team_backlog_id = %d', $intTeamBacklogId ), $objDatabase );
	}

	public static function fetchBacklogTaskRanksByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchBacklogTaskRanks( sprintf( 'SELECT * FROM backlog_task_ranks WHERE task_id = %d', $intTaskId ), $objDatabase );
	}

}
?>