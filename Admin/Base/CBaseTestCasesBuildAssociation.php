<?php

class CBaseTestCasesBuildAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.test_cases_build_associations';

	protected $m_intId;
	protected $m_intTaskId;
	protected $m_intTestBuildId;
	protected $m_intTaskStatusId;
	protected $m_intExecutorEmployeeId;
	protected $m_strExecutedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intTestCaseProductFolderId;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['test_build_id'] ) && $boolDirectSet ) $this->set( 'm_intTestBuildId', trim( $arrValues['test_build_id'] ) ); elseif( isset( $arrValues['test_build_id'] ) ) $this->setTestBuildId( $arrValues['test_build_id'] );
		if( isset( $arrValues['task_status_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskStatusId', trim( $arrValues['task_status_id'] ) ); elseif( isset( $arrValues['task_status_id'] ) ) $this->setTaskStatusId( $arrValues['task_status_id'] );
		if( isset( $arrValues['executor_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intExecutorEmployeeId', trim( $arrValues['executor_employee_id'] ) ); elseif( isset( $arrValues['executor_employee_id'] ) ) $this->setExecutorEmployeeId( $arrValues['executor_employee_id'] );
		if( isset( $arrValues['executed_on'] ) && $boolDirectSet ) $this->set( 'm_strExecutedOn', trim( $arrValues['executed_on'] ) ); elseif( isset( $arrValues['executed_on'] ) ) $this->setExecutedOn( $arrValues['executed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['test_case_product_folder_id'] ) && $boolDirectSet ) $this->set( 'm_intTestCaseProductFolderId', trim( $arrValues['test_case_product_folder_id'] ) ); elseif( isset( $arrValues['test_case_product_folder_id'] ) ) $this->setTestCaseProductFolderId( $arrValues['test_case_product_folder_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setTestBuildId( $intTestBuildId ) {
		$this->set( 'm_intTestBuildId', CStrings::strToIntDef( $intTestBuildId, NULL, false ) );
	}

	public function getTestBuildId() {
		return $this->m_intTestBuildId;
	}

	public function sqlTestBuildId() {
		return ( true == isset( $this->m_intTestBuildId ) ) ? ( string ) $this->m_intTestBuildId : 'NULL';
	}

	public function setTaskStatusId( $intTaskStatusId ) {
		$this->set( 'm_intTaskStatusId', CStrings::strToIntDef( $intTaskStatusId, NULL, false ) );
	}

	public function getTaskStatusId() {
		return $this->m_intTaskStatusId;
	}

	public function sqlTaskStatusId() {
		return ( true == isset( $this->m_intTaskStatusId ) ) ? ( string ) $this->m_intTaskStatusId : 'NULL';
	}

	public function setExecutorEmployeeId( $intExecutorEmployeeId ) {
		$this->set( 'm_intExecutorEmployeeId', CStrings::strToIntDef( $intExecutorEmployeeId, NULL, false ) );
	}

	public function getExecutorEmployeeId() {
		return $this->m_intExecutorEmployeeId;
	}

	public function sqlExecutorEmployeeId() {
		return ( true == isset( $this->m_intExecutorEmployeeId ) ) ? ( string ) $this->m_intExecutorEmployeeId : 'NULL';
	}

	public function setExecutedOn( $strExecutedOn ) {
		$this->set( 'm_strExecutedOn', CStrings::strTrimDef( $strExecutedOn, -1, NULL, true ) );
	}

	public function getExecutedOn() {
		return $this->m_strExecutedOn;
	}

	public function sqlExecutedOn() {
		return ( true == isset( $this->m_strExecutedOn ) ) ? '\'' . $this->m_strExecutedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setTestCaseProductFolderId( $intTestCaseProductFolderId ) {
		$this->set( 'm_intTestCaseProductFolderId', CStrings::strToIntDef( $intTestCaseProductFolderId, NULL, false ) );
	}

	public function getTestCaseProductFolderId() {
		return $this->m_intTestCaseProductFolderId;
	}

	public function sqlTestCaseProductFolderId() {
		return ( true == isset( $this->m_intTestCaseProductFolderId ) ) ? ( string ) $this->m_intTestCaseProductFolderId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, task_id, test_build_id, task_status_id, executor_employee_id, executed_on, updated_by, updated_on, created_by, created_on, test_case_product_folder_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlTaskId() . ', ' .
						$this->sqlTestBuildId() . ', ' .
						$this->sqlTaskStatusId() . ', ' .
						$this->sqlExecutorEmployeeId() . ', ' .
						$this->sqlExecutedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlTestCaseProductFolderId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId(). ',' ; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_build_id = ' . $this->sqlTestBuildId(). ',' ; } elseif( true == array_key_exists( 'TestBuildId', $this->getChangedColumns() ) ) { $strSql .= ' test_build_id = ' . $this->sqlTestBuildId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_status_id = ' . $this->sqlTaskStatusId(). ',' ; } elseif( true == array_key_exists( 'TaskStatusId', $this->getChangedColumns() ) ) { $strSql .= ' task_status_id = ' . $this->sqlTaskStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executor_employee_id = ' . $this->sqlExecutorEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ExecutorEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' executor_employee_id = ' . $this->sqlExecutorEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' executed_on = ' . $this->sqlExecutedOn(). ',' ; } elseif( true == array_key_exists( 'ExecutedOn', $this->getChangedColumns() ) ) { $strSql .= ' executed_on = ' . $this->sqlExecutedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_product_folder_id = ' . $this->sqlTestCaseProductFolderId(). ',' ; } elseif( true == array_key_exists( 'TestCaseProductFolderId', $this->getChangedColumns() ) ) { $strSql .= ' test_case_product_folder_id = ' . $this->sqlTestCaseProductFolderId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'task_id' => $this->getTaskId(),
			'test_build_id' => $this->getTestBuildId(),
			'task_status_id' => $this->getTaskStatusId(),
			'executor_employee_id' => $this->getExecutorEmployeeId(),
			'executed_on' => $this->getExecutedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'test_case_product_folder_id' => $this->getTestCaseProductFolderId()
		);
	}

}
?>