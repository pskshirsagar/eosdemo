<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeManagers
 * Do not add any new functions to this class.
 */

class CBaseEmployeeManagers extends CEosPluralBase {

	/**
	 * @return CEmployeeManager[]
	 */
	public static function fetchEmployeeManagers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeManager', $objDatabase );
	}

	/**
	 * @return CEmployeeManager
	 */
	public static function fetchEmployeeManager( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeManager', $objDatabase );
	}

	public static function fetchEmployeeManagerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_managers', $objDatabase );
	}

	public static function fetchEmployeeManagerById( $intId, $objDatabase ) {
		return self::fetchEmployeeManager( sprintf( 'SELECT * FROM employee_managers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeManagersByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		return self::fetchEmployeeManagers( sprintf( 'SELECT * FROM employee_managers WHERE manager_employee_id = %d', ( int ) $intManagerEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeManagersByMemberEmployeeId( $intMemberEmployeeId, $objDatabase ) {
		return self::fetchEmployeeManagers( sprintf( 'SELECT * FROM employee_managers WHERE member_employee_id = %d', ( int ) $intMemberEmployeeId ), $objDatabase );
	}

}
?>