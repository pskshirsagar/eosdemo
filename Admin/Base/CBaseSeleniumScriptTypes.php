<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSeleniumScriptTypes
 * Do not add any new functions to this class.
 */

class CBaseSeleniumScriptTypes extends CEosPluralBase {

	/**
	 * @return CSeleniumScriptType[]
	 */
	public static function fetchSeleniumScriptTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSeleniumScriptType', $objDatabase );
	}

	/**
	 * @return CSeleniumScriptType
	 */
	public static function fetchSeleniumScriptType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSeleniumScriptType', $objDatabase );
	}

	public static function fetchSeleniumScriptTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'selenium_script_types', $objDatabase );
	}

	public static function fetchSeleniumScriptTypeById( $intId, $objDatabase ) {
		return self::fetchSeleniumScriptType( sprintf( 'SELECT * FROM selenium_script_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>