<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdGroupAssociations
 * Do not add any new functions to this class.
 */

class CBaseSemAdGroupAssociations extends CEosPluralBase {

	/**
	 * @return CSemAdGroupAssociation[]
	 */
	public static function fetchSemAdGroupAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemAdGroupAssociation', $objDatabase );
	}

	/**
	 * @return CSemAdGroupAssociation
	 */
	public static function fetchSemAdGroupAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemAdGroupAssociation', $objDatabase );
	}

	public static function fetchSemAdGroupAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_ad_group_associations', $objDatabase );
	}

	public static function fetchSemAdGroupAssociationById( $intId, $objDatabase ) {
		return self::fetchSemAdGroupAssociation( sprintf( 'SELECT * FROM sem_ad_group_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemAdGroupAssociationsByCid( $intCid, $objDatabase ) {
		return self::fetchSemAdGroupAssociations( sprintf( 'SELECT * FROM sem_ad_group_associations WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSemAdGroupAssociationsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSemAdGroupAssociations( sprintf( 'SELECT * FROM sem_ad_group_associations WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchSemAdGroupAssociationsBySemCampaignId( $intSemCampaignId, $objDatabase ) {
		return self::fetchSemAdGroupAssociations( sprintf( 'SELECT * FROM sem_ad_group_associations WHERE sem_campaign_id = %d', ( int ) $intSemCampaignId ), $objDatabase );
	}

	public static function fetchSemAdGroupAssociationsBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemAdGroupAssociations( sprintf( 'SELECT * FROM sem_ad_group_associations WHERE sem_ad_group_id = %d', ( int ) $intSemAdGroupId ), $objDatabase );
	}

}
?>