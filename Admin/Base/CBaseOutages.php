<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\COutages
 * Do not add any new functions to this class.
 */

class CBaseOutages extends CEosPluralBase {

	/**
	 * @return COutage[]
	 */
	public static function fetchOutages( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'COutage', $objDatabase );
	}

	/**
	 * @return COutage
	 */
	public static function fetchOutage( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COutage', $objDatabase );
	}

	public static function fetchOutageCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'outages', $objDatabase );
	}

	public static function fetchOutageById( $intId, $objDatabase ) {
		return self::fetchOutage( sprintf( 'SELECT * FROM outages WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchOutagesByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchOutages( sprintf( 'SELECT * FROM outages WHERE deleted_by IS NULL AND task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

}
?>