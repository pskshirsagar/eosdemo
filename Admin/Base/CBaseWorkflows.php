<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CWorkflows
 * Do not add any new functions to this class.
 */

class CBaseWorkflows extends CEosPluralBase {

	/**
	 * @return CWorkflow[]
	 */
	public static function fetchWorkflows( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CWorkflow', $objDatabase );
	}

	/**
	 * @return CWorkflow
	 */
	public static function fetchWorkflow( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CWorkflow', $objDatabase );
	}

	public static function fetchWorkflowCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'workflows', $objDatabase );
	}

	public static function fetchWorkflowById( $intId, $objDatabase ) {
		return self::fetchWorkflow( sprintf( 'SELECT * FROM workflows WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchWorkflowsByCid( $intCid, $objDatabase ) {
		return self::fetchWorkflows( sprintf( 'SELECT * FROM workflows WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

}
?>