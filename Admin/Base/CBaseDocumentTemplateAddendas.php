<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplateAddendas
 * Do not add any new functions to this class.
 */

class CBaseDocumentTemplateAddendas extends CEosPluralBase {

	/**
	 * @return CDocumentTemplateAddenda[]
	 */
	public static function fetchDocumentTemplateAddendas( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDocumentTemplateAddenda', $objDatabase );
	}

	/**
	 * @return CDocumentTemplateAddenda
	 */
	public static function fetchDocumentTemplateAddenda( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentTemplateAddenda', $objDatabase );
	}

	public static function fetchDocumentTemplateAddendaCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_template_addendas', $objDatabase );
	}

	public static function fetchDocumentTemplateAddendaById( $intId, $objDatabase ) {
		return self::fetchDocumentTemplateAddenda( sprintf( 'SELECT * FROM document_template_addendas WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDocumentTemplateAddendasByLeaseDocumentTemplateId( $intLeaseDocumentTemplateId, $objDatabase ) {
		return self::fetchDocumentTemplateAddendas( sprintf( 'SELECT * FROM document_template_addendas WHERE lease_document_template_id = %d', ( int ) $intLeaseDocumentTemplateId ), $objDatabase );
	}

	public static function fetchDocumentTemplateAddendasByAddendaDocumentTemplateId( $intAddendaDocumentTemplateId, $objDatabase ) {
		return self::fetchDocumentTemplateAddendas( sprintf( 'SELECT * FROM document_template_addendas WHERE addenda_document_template_id = %d', ( int ) $intAddendaDocumentTemplateId ), $objDatabase );
	}

	public static function fetchDocumentTemplateAddendasByDocumentTemplateAddendaId( $intDocumentTemplateAddendaId, $objDatabase ) {
		return self::fetchDocumentTemplateAddendas( sprintf( 'SELECT * FROM document_template_addendas WHERE document_template_addenda_id = %d', ( int ) $intDocumentTemplateAddendaId ), $objDatabase );
	}

}
?>