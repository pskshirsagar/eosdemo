<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsAssetTypes
 * Do not add any new functions to this class.
 */

class CBasePsAssetTypes extends CEosPluralBase {

	/**
	 * @return CPsAssetType[]
	 */
	public static function fetchPsAssetTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsAssetType', $objDatabase );
	}

	/**
	 * @return CPsAssetType
	 */
	public static function fetchPsAssetType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsAssetType', $objDatabase );
	}

	public static function fetchPsAssetTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_asset_types', $objDatabase );
	}

	public static function fetchPsAssetTypeById( $intId, $objDatabase ) {
		return self::fetchPsAssetType( sprintf( 'SELECT * FROM ps_asset_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsAssetTypesByPsAssetTypeId( $intPsAssetTypeId, $objDatabase ) {
		return self::fetchPsAssetTypes( sprintf( 'SELECT * FROM ps_asset_types WHERE ps_asset_type_id = %d', ( int ) $intPsAssetTypeId ), $objDatabase );
	}

	public static function fetchPsAssetTypesByApproverEmployeeId( $intApproverEmployeeId, $objDatabase ) {
		return self::fetchPsAssetTypes( sprintf( 'SELECT * FROM ps_asset_types WHERE approver_employee_id = %d', ( int ) $intApproverEmployeeId ), $objDatabase );
	}

}
?>