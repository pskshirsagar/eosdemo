<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseReferenceTypes
 * Do not add any new functions to this class.
 */

class CBaseReleaseReferenceTypes extends CEosPluralBase {

	/**
	 * @return CReleaseReferenceType[]
	 */
	public static function fetchReleaseReferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CReleaseReferenceType', $objDatabase );
	}

	/**
	 * @return CReleaseReferenceType
	 */
	public static function fetchReleaseReferenceType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReleaseReferenceType', $objDatabase );
	}

	public static function fetchReleaseReferenceTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'release_reference_types', $objDatabase );
	}

	public static function fetchReleaseReferenceTypeById( $intId, $objDatabase ) {
		return self::fetchReleaseReferenceType( sprintf( 'SELECT * FROM release_reference_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>