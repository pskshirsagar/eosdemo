<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsSdmEmployees
 * Do not add any new functions to this class.
 */

class CBaseStatsSdmEmployees extends CEosPluralBase {

	/**
	 * @return CStatsSdmEmployee[]
	 */
	public static function fetchStatsSdmEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStatsSdmEmployee', $objDatabase );
	}

	/**
	 * @return CStatsSdmEmployee
	 */
	public static function fetchStatsSdmEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStatsSdmEmployee', $objDatabase );
	}

	public static function fetchStatsSdmEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stats_sdm_employees', $objDatabase );
	}

	public static function fetchStatsSdmEmployeeById( $intId, $objDatabase ) {
		return self::fetchStatsSdmEmployee( sprintf( 'SELECT * FROM stats_sdm_employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStatsSdmEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchStatsSdmEmployees( sprintf( 'SELECT * FROM stats_sdm_employees WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchStatsSdmEmployeesByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchStatsSdmEmployees( sprintf( 'SELECT * FROM stats_sdm_employees WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

}
?>