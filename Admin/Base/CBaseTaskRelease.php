<?php

class CBaseTaskRelease extends CEosSingularBase {

	const TABLE_NAME = 'public.task_releases';

	protected $m_intId;
	protected $m_intClusterId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strReleaseDatetime;
	protected $m_strGoogleCalendarEventId;
	protected $m_strNotes;
	protected $m_strReleaseVideoUrl;
	protected $m_strEmailAddresses;
	protected $m_strSystemDownOn;
	protected $m_strSystemUpOn;
	protected $m_intIsPublished;
	protected $m_intIsClientNotified;
	protected $m_intIsPsNotified;
	protected $m_intIsReleased;
	protected $m_boolIsStageCreated;
	protected $m_boolIsCommitFreeze;
	protected $m_strFreezedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intClusterId = '1';
		$this->m_intIsPublished = '0';
		$this->m_intIsClientNotified = '0';
		$this->m_intIsPsNotified = '0';
		$this->m_intIsReleased = '0';
		$this->m_boolIsStageCreated = false;
		$this->m_boolIsCommitFreeze = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cluster_id'] ) && $boolDirectSet ) $this->set( 'm_intClusterId', trim( $arrValues['cluster_id'] ) ); elseif( isset( $arrValues['cluster_id'] ) ) $this->setClusterId( $arrValues['cluster_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['release_datetime'] ) && $boolDirectSet ) $this->set( 'm_strReleaseDatetime', trim( $arrValues['release_datetime'] ) ); elseif( isset( $arrValues['release_datetime'] ) ) $this->setReleaseDatetime( $arrValues['release_datetime'] );
		if( isset( $arrValues['google_calendar_event_id'] ) && $boolDirectSet ) $this->set( 'm_strGoogleCalendarEventId', trim( stripcslashes( $arrValues['google_calendar_event_id'] ) ) ); elseif( isset( $arrValues['google_calendar_event_id'] ) ) $this->setGoogleCalendarEventId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['google_calendar_event_id'] ) : $arrValues['google_calendar_event_id'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['release_video_url'] ) && $boolDirectSet ) $this->set( 'm_strReleaseVideoUrl', trim( stripcslashes( $arrValues['release_video_url'] ) ) ); elseif( isset( $arrValues['release_video_url'] ) ) $this->setReleaseVideoUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['release_video_url'] ) : $arrValues['release_video_url'] );
		if( isset( $arrValues['email_addresses'] ) && $boolDirectSet ) $this->set( 'm_strEmailAddresses', trim( stripcslashes( $arrValues['email_addresses'] ) ) ); elseif( isset( $arrValues['email_addresses'] ) ) $this->setEmailAddresses( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['email_addresses'] ) : $arrValues['email_addresses'] );
		if( isset( $arrValues['system_down_on'] ) && $boolDirectSet ) $this->set( 'm_strSystemDownOn', trim( $arrValues['system_down_on'] ) ); elseif( isset( $arrValues['system_down_on'] ) ) $this->setSystemDownOn( $arrValues['system_down_on'] );
		if( isset( $arrValues['system_up_on'] ) && $boolDirectSet ) $this->set( 'm_strSystemUpOn', trim( $arrValues['system_up_on'] ) ); elseif( isset( $arrValues['system_up_on'] ) ) $this->setSystemUpOn( $arrValues['system_up_on'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_client_notified'] ) && $boolDirectSet ) $this->set( 'm_intIsClientNotified', trim( $arrValues['is_client_notified'] ) ); elseif( isset( $arrValues['is_client_notified'] ) ) $this->setIsClientNotified( $arrValues['is_client_notified'] );
		if( isset( $arrValues['is_ps_notified'] ) && $boolDirectSet ) $this->set( 'm_intIsPsNotified', trim( $arrValues['is_ps_notified'] ) ); elseif( isset( $arrValues['is_ps_notified'] ) ) $this->setIsPsNotified( $arrValues['is_ps_notified'] );
		if( isset( $arrValues['is_released'] ) && $boolDirectSet ) $this->set( 'm_intIsReleased', trim( $arrValues['is_released'] ) ); elseif( isset( $arrValues['is_released'] ) ) $this->setIsReleased( $arrValues['is_released'] );
		if( isset( $arrValues['is_stage_created'] ) && $boolDirectSet ) $this->set( 'm_boolIsStageCreated', trim( stripcslashes( $arrValues['is_stage_created'] ) ) ); elseif( isset( $arrValues['is_stage_created'] ) ) $this->setIsStageCreated( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_stage_created'] ) : $arrValues['is_stage_created'] );
		if( isset( $arrValues['is_commit_freeze'] ) && $boolDirectSet ) $this->set( 'm_boolIsCommitFreeze', trim( stripcslashes( $arrValues['is_commit_freeze'] ) ) ); elseif( isset( $arrValues['is_commit_freeze'] ) ) $this->setIsCommitFreeze( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_commit_freeze'] ) : $arrValues['is_commit_freeze'] );
		if( isset( $arrValues['freezed_on'] ) && $boolDirectSet ) $this->set( 'm_strFreezedOn', trim( $arrValues['freezed_on'] ) ); elseif( isset( $arrValues['freezed_on'] ) ) $this->setFreezedOn( $arrValues['freezed_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setClusterId( $intClusterId ) {
		$this->set( 'm_intClusterId', CStrings::strToIntDef( $intClusterId, NULL, false ) );
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function sqlClusterId() {
		return ( true == isset( $this->m_intClusterId ) ) ? ( string ) $this->m_intClusterId : '1';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setReleaseDatetime( $strReleaseDatetime ) {
		$this->set( 'm_strReleaseDatetime', CStrings::strTrimDef( $strReleaseDatetime, -1, NULL, true ) );
	}

	public function getReleaseDatetime() {
		return $this->m_strReleaseDatetime;
	}

	public function sqlReleaseDatetime() {
		return ( true == isset( $this->m_strReleaseDatetime ) ) ? '\'' . $this->m_strReleaseDatetime . '\'' : 'NOW()';
	}

	public function setGoogleCalendarEventId( $strGoogleCalendarEventId ) {
		$this->set( 'm_strGoogleCalendarEventId', CStrings::strTrimDef( $strGoogleCalendarEventId, -1, NULL, true ) );
	}

	public function getGoogleCalendarEventId() {
		return $this->m_strGoogleCalendarEventId;
	}

	public function sqlGoogleCalendarEventId() {
		return ( true == isset( $this->m_strGoogleCalendarEventId ) ) ? '\'' . addslashes( $this->m_strGoogleCalendarEventId ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, 2000, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setReleaseVideoUrl( $strReleaseVideoUrl ) {
		$this->set( 'm_strReleaseVideoUrl', CStrings::strTrimDef( $strReleaseVideoUrl, 500, NULL, true ) );
	}

	public function getReleaseVideoUrl() {
		return $this->m_strReleaseVideoUrl;
	}

	public function sqlReleaseVideoUrl() {
		return ( true == isset( $this->m_strReleaseVideoUrl ) ) ? '\'' . addslashes( $this->m_strReleaseVideoUrl ) . '\'' : 'NULL';
	}

	public function setEmailAddresses( $strEmailAddresses ) {
		$this->set( 'm_strEmailAddresses', CStrings::strTrimDef( $strEmailAddresses, -1, NULL, true ) );
	}

	public function getEmailAddresses() {
		return $this->m_strEmailAddresses;
	}

	public function sqlEmailAddresses() {
		return ( true == isset( $this->m_strEmailAddresses ) ) ? '\'' . addslashes( $this->m_strEmailAddresses ) . '\'' : 'NULL';
	}

	public function setSystemDownOn( $strSystemDownOn ) {
		$this->set( 'm_strSystemDownOn', CStrings::strTrimDef( $strSystemDownOn, -1, NULL, true ) );
	}

	public function getSystemDownOn() {
		return $this->m_strSystemDownOn;
	}

	public function sqlSystemDownOn() {
		return ( true == isset( $this->m_strSystemDownOn ) ) ? '\'' . $this->m_strSystemDownOn . '\'' : 'NULL';
	}

	public function setSystemUpOn( $strSystemUpOn ) {
		$this->set( 'm_strSystemUpOn', CStrings::strTrimDef( $strSystemUpOn, -1, NULL, true ) );
	}

	public function getSystemUpOn() {
		return $this->m_strSystemUpOn;
	}

	public function sqlSystemUpOn() {
		return ( true == isset( $this->m_strSystemUpOn ) ) ? '\'' . $this->m_strSystemUpOn . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setIsClientNotified( $intIsClientNotified ) {
		$this->set( 'm_intIsClientNotified', CStrings::strToIntDef( $intIsClientNotified, NULL, false ) );
	}

	public function getIsClientNotified() {
		return $this->m_intIsClientNotified;
	}

	public function sqlIsClientNotified() {
		return ( true == isset( $this->m_intIsClientNotified ) ) ? ( string ) $this->m_intIsClientNotified : '0';
	}

	public function setIsPsNotified( $intIsPsNotified ) {
		$this->set( 'm_intIsPsNotified', CStrings::strToIntDef( $intIsPsNotified, NULL, false ) );
	}

	public function getIsPsNotified() {
		return $this->m_intIsPsNotified;
	}

	public function sqlIsPsNotified() {
		return ( true == isset( $this->m_intIsPsNotified ) ) ? ( string ) $this->m_intIsPsNotified : '0';
	}

	public function setIsReleased( $intIsReleased ) {
		$this->set( 'm_intIsReleased', CStrings::strToIntDef( $intIsReleased, NULL, false ) );
	}

	public function getIsReleased() {
		return $this->m_intIsReleased;
	}

	public function sqlIsReleased() {
		return ( true == isset( $this->m_intIsReleased ) ) ? ( string ) $this->m_intIsReleased : '0';
	}

	public function setIsStageCreated( $boolIsStageCreated ) {
		$this->set( 'm_boolIsStageCreated', CStrings::strToBool( $boolIsStageCreated ) );
	}

	public function getIsStageCreated() {
		return $this->m_boolIsStageCreated;
	}

	public function sqlIsStageCreated() {
		return ( true == isset( $this->m_boolIsStageCreated ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsStageCreated ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsCommitFreeze( $boolIsCommitFreeze ) {
		$this->set( 'm_boolIsCommitFreeze', CStrings::strToBool( $boolIsCommitFreeze ) );
	}

	public function getIsCommitFreeze() {
		return $this->m_boolIsCommitFreeze;
	}

	public function sqlIsCommitFreeze() {
		return ( true == isset( $this->m_boolIsCommitFreeze ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCommitFreeze ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setFreezedOn( $strFreezedOn ) {
		$this->set( 'm_strFreezedOn', CStrings::strTrimDef( $strFreezedOn, -1, NULL, true ) );
	}

	public function getFreezedOn() {
		return $this->m_strFreezedOn;
	}

	public function sqlFreezedOn() {
		return ( true == isset( $this->m_strFreezedOn ) ) ? '\'' . $this->m_strFreezedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cluster_id, name, description, release_datetime, google_calendar_event_id, notes, release_video_url, email_addresses, system_down_on, system_up_on, is_published, is_client_notified, is_ps_notified, is_released, is_stage_created, is_commit_freeze, freezed_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlClusterId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlReleaseDatetime() . ', ' .
 						$this->sqlGoogleCalendarEventId() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlReleaseVideoUrl() . ', ' .
 						$this->sqlEmailAddresses() . ', ' .
 						$this->sqlSystemDownOn() . ', ' .
 						$this->sqlSystemUpOn() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsClientNotified() . ', ' .
 						$this->sqlIsPsNotified() . ', ' .
 						$this->sqlIsReleased() . ', ' .
 						$this->sqlIsStageCreated() . ', ' .
 						$this->sqlIsCommitFreeze() . ', ' .
 						$this->sqlFreezedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; } elseif( true == array_key_exists( 'ClusterId', $this->getChangedColumns() ) ) { $strSql .= ' cluster_id = ' . $this->sqlClusterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_datetime = ' . $this->sqlReleaseDatetime() . ','; } elseif( true == array_key_exists( 'ReleaseDatetime', $this->getChangedColumns() ) ) { $strSql .= ' release_datetime = ' . $this->sqlReleaseDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' google_calendar_event_id = ' . $this->sqlGoogleCalendarEventId() . ','; } elseif( true == array_key_exists( 'GoogleCalendarEventId', $this->getChangedColumns() ) ) { $strSql .= ' google_calendar_event_id = ' . $this->sqlGoogleCalendarEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_video_url = ' . $this->sqlReleaseVideoUrl() . ','; } elseif( true == array_key_exists( 'ReleaseVideoUrl', $this->getChangedColumns() ) ) { $strSql .= ' release_video_url = ' . $this->sqlReleaseVideoUrl() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_addresses = ' . $this->sqlEmailAddresses() . ','; } elseif( true == array_key_exists( 'EmailAddresses', $this->getChangedColumns() ) ) { $strSql .= ' email_addresses = ' . $this->sqlEmailAddresses() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_down_on = ' . $this->sqlSystemDownOn() . ','; } elseif( true == array_key_exists( 'SystemDownOn', $this->getChangedColumns() ) ) { $strSql .= ' system_down_on = ' . $this->sqlSystemDownOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' system_up_on = ' . $this->sqlSystemUpOn() . ','; } elseif( true == array_key_exists( 'SystemUpOn', $this->getChangedColumns() ) ) { $strSql .= ' system_up_on = ' . $this->sqlSystemUpOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_notified = ' . $this->sqlIsClientNotified() . ','; } elseif( true == array_key_exists( 'IsClientNotified', $this->getChangedColumns() ) ) { $strSql .= ' is_client_notified = ' . $this->sqlIsClientNotified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ps_notified = ' . $this->sqlIsPsNotified() . ','; } elseif( true == array_key_exists( 'IsPsNotified', $this->getChangedColumns() ) ) { $strSql .= ' is_ps_notified = ' . $this->sqlIsPsNotified() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_released = ' . $this->sqlIsReleased() . ','; } elseif( true == array_key_exists( 'IsReleased', $this->getChangedColumns() ) ) { $strSql .= ' is_released = ' . $this->sqlIsReleased() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_stage_created = ' . $this->sqlIsStageCreated() . ','; } elseif( true == array_key_exists( 'IsStageCreated', $this->getChangedColumns() ) ) { $strSql .= ' is_stage_created = ' . $this->sqlIsStageCreated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_commit_freeze = ' . $this->sqlIsCommitFreeze() . ','; } elseif( true == array_key_exists( 'IsCommitFreeze', $this->getChangedColumns() ) ) { $strSql .= ' is_commit_freeze = ' . $this->sqlIsCommitFreeze() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' freezed_on = ' . $this->sqlFreezedOn() . ','; } elseif( true == array_key_exists( 'FreezedOn', $this->getChangedColumns() ) ) { $strSql .= ' freezed_on = ' . $this->sqlFreezedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cluster_id' => $this->getClusterId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'release_datetime' => $this->getReleaseDatetime(),
			'google_calendar_event_id' => $this->getGoogleCalendarEventId(),
			'notes' => $this->getNotes(),
			'release_video_url' => $this->getReleaseVideoUrl(),
			'email_addresses' => $this->getEmailAddresses(),
			'system_down_on' => $this->getSystemDownOn(),
			'system_up_on' => $this->getSystemUpOn(),
			'is_published' => $this->getIsPublished(),
			'is_client_notified' => $this->getIsClientNotified(),
			'is_ps_notified' => $this->getIsPsNotified(),
			'is_released' => $this->getIsReleased(),
			'is_stage_created' => $this->getIsStageCreated(),
			'is_commit_freeze' => $this->getIsCommitFreeze(),
			'freezed_on' => $this->getFreezedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>