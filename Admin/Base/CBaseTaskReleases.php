<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskReleases
 * Do not add any new functions to this class.
 */

class CBaseTaskReleases extends CEosPluralBase {

	/**
	 * @return CTaskRelease[]
	 */
	public static function fetchTaskReleases( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskRelease', $objDatabase );
	}

	/**
	 * @return CTaskRelease
	 */
	public static function fetchTaskRelease( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskRelease', $objDatabase );
	}

	public static function fetchTaskReleaseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_releases', $objDatabase );
	}

	public static function fetchTaskReleaseById( $intId, $objDatabase ) {
		return self::fetchTaskRelease( sprintf( 'SELECT * FROM task_releases WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskReleasesByClusterId( $intClusterId, $objDatabase ) {
		return self::fetchTaskReleases( sprintf( 'SELECT * FROM task_releases WHERE cluster_id = %d', ( int ) $intClusterId ), $objDatabase );
	}

	public static function fetchTaskReleasesByGoogleCalendarEventId( $strGoogleCalendarEventId, $objDatabase ) {
		return self::fetchTaskReleases( sprintf( 'SELECT * FROM task_releases WHERE google_calendar_event_id = \'%s\'', $strGoogleCalendarEventId ), $objDatabase );
	}

}
?>