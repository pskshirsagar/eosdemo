<?php

class CBasePackageProductDiscount extends CEosSingularBase {

	const TABLE_NAME = 'public.package_product_discounts';

	protected $m_intId;
	protected $m_intPackageProductRelationshipId;
	protected $m_intDependentProductId;
	protected $m_fltSetupDiscountPercentage;
	protected $m_fltRecurringDiscountPercentage;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDeletedOn;
	protected $m_intDeletedBy;

	public function __construct() {
		parent::__construct();

		$this->m_fltSetupDiscountPercentage = '0';
		$this->m_fltRecurringDiscountPercentage = '0';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['package_product_relationship_id'] ) && $boolDirectSet ) $this->set( 'm_intPackageProductRelationshipId', trim( $arrValues['package_product_relationship_id'] ) ); elseif( isset( $arrValues['package_product_relationship_id'] ) ) $this->setPackageProductRelationshipId( $arrValues['package_product_relationship_id'] );
		if( isset( $arrValues['dependent_product_id'] ) && $boolDirectSet ) $this->set( 'm_intDependentProductId', trim( $arrValues['dependent_product_id'] ) ); elseif( isset( $arrValues['dependent_product_id'] ) ) $this->setDependentProductId( $arrValues['dependent_product_id'] );
		if( isset( $arrValues['setup_discount_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltSetupDiscountPercentage', trim( $arrValues['setup_discount_percentage'] ) ); elseif( isset( $arrValues['setup_discount_percentage'] ) ) $this->setSetupDiscountPercentage( $arrValues['setup_discount_percentage'] );
		if( isset( $arrValues['recurring_discount_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltRecurringDiscountPercentage', trim( $arrValues['recurring_discount_percentage'] ) ); elseif( isset( $arrValues['recurring_discount_percentage'] ) ) $this->setRecurringDiscountPercentage( $arrValues['recurring_discount_percentage'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPackageProductRelationshipId( $intPackageProductRelationshipId ) {
		$this->set( 'm_intPackageProductRelationshipId', CStrings::strToIntDef( $intPackageProductRelationshipId, NULL, false ) );
	}

	public function getPackageProductRelationshipId() {
		return $this->m_intPackageProductRelationshipId;
	}

	public function sqlPackageProductRelationshipId() {
		return ( true == isset( $this->m_intPackageProductRelationshipId ) ) ? ( string ) $this->m_intPackageProductRelationshipId : 'NULL';
	}

	public function setDependentProductId( $intDependentProductId ) {
		$this->set( 'm_intDependentProductId', CStrings::strToIntDef( $intDependentProductId, NULL, false ) );
	}

	public function getDependentProductId() {
		return $this->m_intDependentProductId;
	}

	public function sqlDependentProductId() {
		return ( true == isset( $this->m_intDependentProductId ) ) ? ( string ) $this->m_intDependentProductId : 'NULL';
	}

	public function setSetupDiscountPercentage( $fltSetupDiscountPercentage ) {
		$this->set( 'm_fltSetupDiscountPercentage', CStrings::strToFloatDef( $fltSetupDiscountPercentage, NULL, false, 0 ) );
	}

	public function getSetupDiscountPercentage() {
		return $this->m_fltSetupDiscountPercentage;
	}

	public function sqlSetupDiscountPercentage() {
		return ( true == isset( $this->m_fltSetupDiscountPercentage ) ) ? ( string ) $this->m_fltSetupDiscountPercentage : '0';
	}

	public function setRecurringDiscountPercentage( $fltRecurringDiscountPercentage ) {
		$this->set( 'm_fltRecurringDiscountPercentage', CStrings::strToFloatDef( $fltRecurringDiscountPercentage, NULL, false, 0 ) );
	}

	public function getRecurringDiscountPercentage() {
		return $this->m_fltRecurringDiscountPercentage;
	}

	public function sqlRecurringDiscountPercentage() {
		return ( true == isset( $this->m_fltRecurringDiscountPercentage ) ) ? ( string ) $this->m_fltRecurringDiscountPercentage : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, package_product_relationship_id, dependent_product_id, setup_discount_percentage, recurring_discount_percentage, updated_by, updated_on, created_by, created_on, deleted_on, deleted_by )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPackageProductRelationshipId() . ', ' .
						$this->sqlDependentProductId() . ', ' .
						$this->sqlSetupDiscountPercentage() . ', ' .
						$this->sqlRecurringDiscountPercentage() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						$this->sqlDeletedBy() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' package_product_relationship_id = ' . $this->sqlPackageProductRelationshipId(). ',' ; } elseif( true == array_key_exists( 'PackageProductRelationshipId', $this->getChangedColumns() ) ) { $strSql .= ' package_product_relationship_id = ' . $this->sqlPackageProductRelationshipId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' dependent_product_id = ' . $this->sqlDependentProductId(). ',' ; } elseif( true == array_key_exists( 'DependentProductId', $this->getChangedColumns() ) ) { $strSql .= ' dependent_product_id = ' . $this->sqlDependentProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' setup_discount_percentage = ' . $this->sqlSetupDiscountPercentage(). ',' ; } elseif( true == array_key_exists( 'SetupDiscountPercentage', $this->getChangedColumns() ) ) { $strSql .= ' setup_discount_percentage = ' . $this->sqlSetupDiscountPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recurring_discount_percentage = ' . $this->sqlRecurringDiscountPercentage(). ',' ; } elseif( true == array_key_exists( 'RecurringDiscountPercentage', $this->getChangedColumns() ) ) { $strSql .= ' recurring_discount_percentage = ' . $this->sqlRecurringDiscountPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'package_product_relationship_id' => $this->getPackageProductRelationshipId(),
			'dependent_product_id' => $this->getDependentProductId(),
			'setup_discount_percentage' => $this->getSetupDiscountPercentage(),
			'recurring_discount_percentage' => $this->getRecurringDiscountPercentage(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'deleted_on' => $this->getDeletedOn(),
			'deleted_by' => $this->getDeletedBy()
		);
	}

}
?>