<?php

class CBaseContractProductOption extends CEosSingularBase {

	const TABLE_NAME = 'public.contract_product_options';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intContractId;
	protected $m_strCurrencyCode;
	protected $m_intPropertyId;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_intBundlePsProductId;
	protected $m_intSetUpChargeCodeId;
	protected $m_intRecurringChargeCodeId;
	protected $m_fltSetUpAmount;
	protected $m_fltMonthlyRecurringAmount;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';
		$this->m_fltSetUpAmount = '0';
		$this->m_fltMonthlyRecurringAmount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['bundle_ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intBundlePsProductId', trim( $arrValues['bundle_ps_product_id'] ) ); elseif( isset( $arrValues['bundle_ps_product_id'] ) ) $this->setBundlePsProductId( $arrValues['bundle_ps_product_id'] );
		if( isset( $arrValues['set_up_charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intSetUpChargeCodeId', trim( $arrValues['set_up_charge_code_id'] ) ); elseif( isset( $arrValues['set_up_charge_code_id'] ) ) $this->setSetUpChargeCodeId( $arrValues['set_up_charge_code_id'] );
		if( isset( $arrValues['recurring_charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intRecurringChargeCodeId', trim( $arrValues['recurring_charge_code_id'] ) ); elseif( isset( $arrValues['recurring_charge_code_id'] ) ) $this->setRecurringChargeCodeId( $arrValues['recurring_charge_code_id'] );
		if( isset( $arrValues['set_up_amount'] ) && $boolDirectSet ) $this->set( 'm_fltSetUpAmount', trim( $arrValues['set_up_amount'] ) ); elseif( isset( $arrValues['set_up_amount'] ) ) $this->setSetUpAmount( $arrValues['set_up_amount'] );
		if( isset( $arrValues['monthly_recurring_amount'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyRecurringAmount', trim( $arrValues['monthly_recurring_amount'] ) ); elseif( isset( $arrValues['monthly_recurring_amount'] ) ) $this->setMonthlyRecurringAmount( $arrValues['monthly_recurring_amount'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : '\'USD\'';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setBundlePsProductId( $intBundlePsProductId ) {
		$this->set( 'm_intBundlePsProductId', CStrings::strToIntDef( $intBundlePsProductId, NULL, false ) );
	}

	public function getBundlePsProductId() {
		return $this->m_intBundlePsProductId;
	}

	public function sqlBundlePsProductId() {
		return ( true == isset( $this->m_intBundlePsProductId ) ) ? ( string ) $this->m_intBundlePsProductId : 'NULL';
	}

	public function setSetUpChargeCodeId( $intSetUpChargeCodeId ) {
		$this->set( 'm_intSetUpChargeCodeId', CStrings::strToIntDef( $intSetUpChargeCodeId, NULL, false ) );
	}

	public function getSetUpChargeCodeId() {
		return $this->m_intSetUpChargeCodeId;
	}

	public function sqlSetUpChargeCodeId() {
		return ( true == isset( $this->m_intSetUpChargeCodeId ) ) ? ( string ) $this->m_intSetUpChargeCodeId : 'NULL';
	}

	public function setRecurringChargeCodeId( $intRecurringChargeCodeId ) {
		$this->set( 'm_intRecurringChargeCodeId', CStrings::strToIntDef( $intRecurringChargeCodeId, NULL, false ) );
	}

	public function getRecurringChargeCodeId() {
		return $this->m_intRecurringChargeCodeId;
	}

	public function sqlRecurringChargeCodeId() {
		return ( true == isset( $this->m_intRecurringChargeCodeId ) ) ? ( string ) $this->m_intRecurringChargeCodeId : 'NULL';
	}

	public function setSetUpAmount( $fltSetUpAmount ) {
		$this->set( 'm_fltSetUpAmount', CStrings::strToFloatDef( $fltSetUpAmount, NULL, false, 2 ) );
	}

	public function getSetUpAmount() {
		return $this->m_fltSetUpAmount;
	}

	public function sqlSetUpAmount() {
		return ( true == isset( $this->m_fltSetUpAmount ) ) ? ( string ) $this->m_fltSetUpAmount : '0';
	}

	public function setMonthlyRecurringAmount( $fltMonthlyRecurringAmount ) {
		$this->set( 'm_fltMonthlyRecurringAmount', CStrings::strToFloatDef( $fltMonthlyRecurringAmount, NULL, false, 2 ) );
	}

	public function getMonthlyRecurringAmount() {
		return $this->m_fltMonthlyRecurringAmount;
	}

	public function sqlMonthlyRecurringAmount() {
		return ( true == isset( $this->m_fltMonthlyRecurringAmount ) ) ? ( string ) $this->m_fltMonthlyRecurringAmount : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, contract_id, currency_code, property_id, ps_product_id, ps_product_option_id, bundle_ps_product_id, set_up_charge_code_id, recurring_charge_code_id, set_up_amount, monthly_recurring_amount, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlContractId() . ', ' .
 						$this->sqlCurrencyCode() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPsProductOptionId() . ', ' .
 						$this->sqlBundlePsProductId() . ', ' .
 						$this->sqlSetUpChargeCodeId() . ', ' .
 						$this->sqlRecurringChargeCodeId() . ', ' .
 						$this->sqlSetUpAmount() . ', ' .
 						$this->sqlMonthlyRecurringAmount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; } elseif( true == array_key_exists( 'PsProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bundle_ps_product_id = ' . $this->sqlBundlePsProductId() . ','; } elseif( true == array_key_exists( 'BundlePsProductId', $this->getChangedColumns() ) ) { $strSql .= ' bundle_ps_product_id = ' . $this->sqlBundlePsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' set_up_charge_code_id = ' . $this->sqlSetUpChargeCodeId() . ','; } elseif( true == array_key_exists( 'SetUpChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' set_up_charge_code_id = ' . $this->sqlSetUpChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' recurring_charge_code_id = ' . $this->sqlRecurringChargeCodeId() . ','; } elseif( true == array_key_exists( 'RecurringChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' recurring_charge_code_id = ' . $this->sqlRecurringChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' set_up_amount = ' . $this->sqlSetUpAmount() . ','; } elseif( true == array_key_exists( 'SetUpAmount', $this->getChangedColumns() ) ) { $strSql .= ' set_up_amount = ' . $this->sqlSetUpAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_recurring_amount = ' . $this->sqlMonthlyRecurringAmount() . ','; } elseif( true == array_key_exists( 'MonthlyRecurringAmount', $this->getChangedColumns() ) ) { $strSql .= ' monthly_recurring_amount = ' . $this->sqlMonthlyRecurringAmount() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'contract_id' => $this->getContractId(),
			'currency_code' => $this->getCurrencyCode(),
			'property_id' => $this->getPropertyId(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'bundle_ps_product_id' => $this->getBundlePsProductId(),
			'set_up_charge_code_id' => $this->getSetUpChargeCodeId(),
			'recurring_charge_code_id' => $this->getRecurringChargeCodeId(),
			'set_up_amount' => $this->getSetUpAmount(),
			'monthly_recurring_amount' => $this->getMonthlyRecurringAmount(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>