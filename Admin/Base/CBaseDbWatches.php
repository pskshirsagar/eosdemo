<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDbWatches
 * Do not add any new functions to this class.
 */

class CBaseDbWatches extends CEosPluralBase {

	/**
	 * @return CDbWatch[]
	 */
	public static function fetchDbWatches( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDbWatch', $objDatabase );
	}

	/**
	 * @return CDbWatch
	 */
	public static function fetchDbWatch( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDbWatch', $objDatabase );
	}

	public static function fetchDbWatchCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'db_watches', $objDatabase );
	}

	public static function fetchDbWatchById( $intId, $objDatabase ) {
		return self::fetchDbWatch( sprintf( 'SELECT * FROM db_watches WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDbWatchesByDbWatchTypeId( $intDbWatchTypeId, $objDatabase ) {
		return self::fetchDbWatches( sprintf( 'SELECT * FROM db_watches WHERE db_watch_type_id = %d', ( int ) $intDbWatchTypeId ), $objDatabase );
	}

	public static function fetchDbWatchesByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchDbWatches( sprintf( 'SELECT * FROM db_watches WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

	public static function fetchDbWatchesByDbWatchPriorityId( $intDbWatchPriorityId, $objDatabase ) {
		return self::fetchDbWatches( sprintf( 'SELECT * FROM db_watches WHERE db_watch_priority_id = %d', ( int ) $intDbWatchPriorityId ), $objDatabase );
	}

}
?>