<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserModuleLogs
 * Do not add any new functions to this class.
 */

class CBaseUserModuleLogs extends CEosPluralBase {

	/**
	 * @return CUserModuleLog[]
	 */
	public static function fetchUserModuleLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserModuleLog', $objDatabase );
	}

	/**
	 * @return CUserModuleLog
	 */
	public static function fetchUserModuleLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserModuleLog', $objDatabase );
	}

	public static function fetchUserModuleLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_module_logs', $objDatabase );
	}

	public static function fetchUserModuleLogById( $intId, $objDatabase ) {
		return self::fetchUserModuleLog( sprintf( 'SELECT * FROM user_module_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserModuleLogsByUserId( $intUserId, $objDatabase ) {
		return self::fetchUserModuleLogs( sprintf( 'SELECT * FROM user_module_logs WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

}
?>