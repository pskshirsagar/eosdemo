<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskTypes
 * Do not add any new functions to this class.
 */

class CBaseTaskTypes extends CEosPluralBase {

	/**
	 * @return CTaskType[]
	 */
	public static function fetchTaskTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskType', $objDatabase );
	}

	/**
	 * @return CTaskType
	 */
	public static function fetchTaskType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskType', $objDatabase );
	}

	public static function fetchTaskTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_types', $objDatabase );
	}

	public static function fetchTaskTypeById( $intId, $objDatabase ) {
		return self::fetchTaskType( sprintf( 'SELECT * FROM task_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskTypesByParentTaskTypeId( $intParentTaskTypeId, $objDatabase ) {
		return self::fetchTaskTypes( sprintf( 'SELECT * FROM task_types WHERE parent_task_type_id = %d', ( int ) $intParentTaskTypeId ), $objDatabase );
	}

	public static function fetchTaskTypesByDefaultTaskPriorityId( $intDefaultTaskPriorityId, $objDatabase ) {
		return self::fetchTaskTypes( sprintf( 'SELECT * FROM task_types WHERE default_task_priority_id = %d', ( int ) $intDefaultTaskPriorityId ), $objDatabase );
	}

	public static function fetchTaskTypesByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchTaskTypes( sprintf( 'SELECT * FROM task_types WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchTaskTypesByUserId( $intUserId, $objDatabase ) {
		return self::fetchTaskTypes( sprintf( 'SELECT * FROM task_types WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

}
?>