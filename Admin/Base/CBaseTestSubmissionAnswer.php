<?php

class CBaseTestSubmissionAnswer extends CEosSingularBase {

	const TABLE_NAME = 'public.test_submission_answers';

	protected $m_intId;
	protected $m_intTestSubmissionId;
	protected $m_intTestQuestionId;
	protected $m_intTestAnswerOptionId;
	protected $m_strSubmissionAnswer;
	protected $m_intPointsGranted;
	protected $m_strInstructorComments;
	protected $m_strFilePath;
	protected $m_strAdditionalFields;
	protected $m_intIsCorrect;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsCorrect = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['test_submission_id'] ) && $boolDirectSet ) $this->set( 'm_intTestSubmissionId', trim( $arrValues['test_submission_id'] ) ); elseif( isset( $arrValues['test_submission_id'] ) ) $this->setTestSubmissionId( $arrValues['test_submission_id'] );
		if( isset( $arrValues['test_question_id'] ) && $boolDirectSet ) $this->set( 'm_intTestQuestionId', trim( $arrValues['test_question_id'] ) ); elseif( isset( $arrValues['test_question_id'] ) ) $this->setTestQuestionId( $arrValues['test_question_id'] );
		if( isset( $arrValues['test_answer_option_id'] ) && $boolDirectSet ) $this->set( 'm_intTestAnswerOptionId', trim( $arrValues['test_answer_option_id'] ) ); elseif( isset( $arrValues['test_answer_option_id'] ) ) $this->setTestAnswerOptionId( $arrValues['test_answer_option_id'] );
		if( isset( $arrValues['submission_answer'] ) && $boolDirectSet ) $this->set( 'm_strSubmissionAnswer', trim( stripcslashes( $arrValues['submission_answer'] ) ) ); elseif( isset( $arrValues['submission_answer'] ) ) $this->setSubmissionAnswer( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['submission_answer'] ) : $arrValues['submission_answer'] );
		if( isset( $arrValues['points_granted'] ) && $boolDirectSet ) $this->set( 'm_intPointsGranted', trim( $arrValues['points_granted'] ) ); elseif( isset( $arrValues['points_granted'] ) ) $this->setPointsGranted( $arrValues['points_granted'] );
		if( isset( $arrValues['instructor_comments'] ) && $boolDirectSet ) $this->set( 'm_strInstructorComments', trim( stripcslashes( $arrValues['instructor_comments'] ) ) ); elseif( isset( $arrValues['instructor_comments'] ) ) $this->setInstructorComments( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['instructor_comments'] ) : $arrValues['instructor_comments'] );
		if( isset( $arrValues['file_path'] ) && $boolDirectSet ) $this->set( 'm_strFilePath', trim( stripcslashes( $arrValues['file_path'] ) ) ); elseif( isset( $arrValues['file_path'] ) ) $this->setFilePath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['file_path'] ) : $arrValues['file_path'] );
		if( isset( $arrValues['additional_fields'] ) && $boolDirectSet ) $this->set( 'm_strAdditionalFields', trim( stripcslashes( $arrValues['additional_fields'] ) ) ); elseif( isset( $arrValues['additional_fields'] ) ) $this->setAdditionalFields( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['additional_fields'] ) : $arrValues['additional_fields'] );
		if( isset( $arrValues['is_correct'] ) && $boolDirectSet ) $this->set( 'm_intIsCorrect', trim( $arrValues['is_correct'] ) ); elseif( isset( $arrValues['is_correct'] ) ) $this->setIsCorrect( $arrValues['is_correct'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTestSubmissionId( $intTestSubmissionId ) {
		$this->set( 'm_intTestSubmissionId', CStrings::strToIntDef( $intTestSubmissionId, NULL, false ) );
	}

	public function getTestSubmissionId() {
		return $this->m_intTestSubmissionId;
	}

	public function sqlTestSubmissionId() {
		return ( true == isset( $this->m_intTestSubmissionId ) ) ? ( string ) $this->m_intTestSubmissionId : 'NULL';
	}

	public function setTestQuestionId( $intTestQuestionId ) {
		$this->set( 'm_intTestQuestionId', CStrings::strToIntDef( $intTestQuestionId, NULL, false ) );
	}

	public function getTestQuestionId() {
		return $this->m_intTestQuestionId;
	}

	public function sqlTestQuestionId() {
		return ( true == isset( $this->m_intTestQuestionId ) ) ? ( string ) $this->m_intTestQuestionId : 'NULL';
	}

	public function setTestAnswerOptionId( $intTestAnswerOptionId ) {
		$this->set( 'm_intTestAnswerOptionId', CStrings::strToIntDef( $intTestAnswerOptionId, NULL, false ) );
	}

	public function getTestAnswerOptionId() {
		return $this->m_intTestAnswerOptionId;
	}

	public function sqlTestAnswerOptionId() {
		return ( true == isset( $this->m_intTestAnswerOptionId ) ) ? ( string ) $this->m_intTestAnswerOptionId : 'NULL';
	}

	public function setSubmissionAnswer( $strSubmissionAnswer ) {
		$this->set( 'm_strSubmissionAnswer', CStrings::strTrimDef( $strSubmissionAnswer, -1, NULL, true ) );
	}

	public function getSubmissionAnswer() {
		return $this->m_strSubmissionAnswer;
	}

	public function sqlSubmissionAnswer() {
		return ( true == isset( $this->m_strSubmissionAnswer ) ) ? '\'' . addslashes( $this->m_strSubmissionAnswer ) . '\'' : 'NULL';
	}

	public function setPointsGranted( $intPointsGranted ) {
		$this->set( 'm_intPointsGranted', CStrings::strToIntDef( $intPointsGranted, NULL, false ) );
	}

	public function getPointsGranted() {
		return $this->m_intPointsGranted;
	}

	public function sqlPointsGranted() {
		return ( true == isset( $this->m_intPointsGranted ) ) ? ( string ) $this->m_intPointsGranted : 'NULL';
	}

	public function setInstructorComments( $strInstructorComments ) {
		$this->set( 'm_strInstructorComments', CStrings::strTrimDef( $strInstructorComments, -1, NULL, true ) );
	}

	public function getInstructorComments() {
		return $this->m_strInstructorComments;
	}

	public function sqlInstructorComments() {
		return ( true == isset( $this->m_strInstructorComments ) ) ? '\'' . addslashes( $this->m_strInstructorComments ) . '\'' : 'NULL';
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, -1, NULL, true ) );
	}

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function sqlFilePath() {
		return ( true == isset( $this->m_strFilePath ) ) ? '\'' . addslashes( $this->m_strFilePath ) . '\'' : 'NULL';
	}

	public function setAdditionalFields( $strAdditionalFields ) {
		$this->set( 'm_strAdditionalFields', CStrings::strTrimDef( $strAdditionalFields, -1, NULL, true ) );
	}

	public function getAdditionalFields() {
		return $this->m_strAdditionalFields;
	}

	public function sqlAdditionalFields() {
		return ( true == isset( $this->m_strAdditionalFields ) ) ? '\'' . addslashes( $this->m_strAdditionalFields ) . '\'' : 'NULL';
	}

	public function setIsCorrect( $intIsCorrect ) {
		$this->set( 'm_intIsCorrect', CStrings::strToIntDef( $intIsCorrect, NULL, false ) );
	}

	public function getIsCorrect() {
		return $this->m_intIsCorrect;
	}

	public function sqlIsCorrect() {
		return ( true == isset( $this->m_intIsCorrect ) ) ? ( string ) $this->m_intIsCorrect : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, test_submission_id, test_question_id, test_answer_option_id, submission_answer, points_granted, instructor_comments, file_path, additional_fields, is_correct, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTestSubmissionId() . ', ' .
 						$this->sqlTestQuestionId() . ', ' .
 						$this->sqlTestAnswerOptionId() . ', ' .
 						$this->sqlSubmissionAnswer() . ', ' .
 						$this->sqlPointsGranted() . ', ' .
 						$this->sqlInstructorComments() . ', ' .
 						$this->sqlFilePath() . ', ' .
 						$this->sqlAdditionalFields() . ', ' .
 						$this->sqlIsCorrect() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_submission_id = ' . $this->sqlTestSubmissionId() . ','; } elseif( true == array_key_exists( 'TestSubmissionId', $this->getChangedColumns() ) ) { $strSql .= ' test_submission_id = ' . $this->sqlTestSubmissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_question_id = ' . $this->sqlTestQuestionId() . ','; } elseif( true == array_key_exists( 'TestQuestionId', $this->getChangedColumns() ) ) { $strSql .= ' test_question_id = ' . $this->sqlTestQuestionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_answer_option_id = ' . $this->sqlTestAnswerOptionId() . ','; } elseif( true == array_key_exists( 'TestAnswerOptionId', $this->getChangedColumns() ) ) { $strSql .= ' test_answer_option_id = ' . $this->sqlTestAnswerOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' submission_answer = ' . $this->sqlSubmissionAnswer() . ','; } elseif( true == array_key_exists( 'SubmissionAnswer', $this->getChangedColumns() ) ) { $strSql .= ' submission_answer = ' . $this->sqlSubmissionAnswer() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' points_granted = ' . $this->sqlPointsGranted() . ','; } elseif( true == array_key_exists( 'PointsGranted', $this->getChangedColumns() ) ) { $strSql .= ' points_granted = ' . $this->sqlPointsGranted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' instructor_comments = ' . $this->sqlInstructorComments() . ','; } elseif( true == array_key_exists( 'InstructorComments', $this->getChangedColumns() ) ) { $strSql .= ' instructor_comments = ' . $this->sqlInstructorComments() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; } elseif( true == array_key_exists( 'FilePath', $this->getChangedColumns() ) ) { $strSql .= ' file_path = ' . $this->sqlFilePath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_fields = ' . $this->sqlAdditionalFields() . ','; } elseif( true == array_key_exists( 'AdditionalFields', $this->getChangedColumns() ) ) { $strSql .= ' additional_fields = ' . $this->sqlAdditionalFields() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_correct = ' . $this->sqlIsCorrect() . ','; } elseif( true == array_key_exists( 'IsCorrect', $this->getChangedColumns() ) ) { $strSql .= ' is_correct = ' . $this->sqlIsCorrect() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'test_submission_id' => $this->getTestSubmissionId(),
			'test_question_id' => $this->getTestQuestionId(),
			'test_answer_option_id' => $this->getTestAnswerOptionId(),
			'submission_answer' => $this->getSubmissionAnswer(),
			'points_granted' => $this->getPointsGranted(),
			'instructor_comments' => $this->getInstructorComments(),
			'file_path' => $this->getFilePath(),
			'additional_fields' => $this->getAdditionalFields(),
			'is_correct' => $this->getIsCorrect(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>