<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskReasons
 * Do not add any new functions to this class.
 */

class CBaseTaskReasons extends CEosPluralBase {

	/**
	 * @return CTaskReason[]
	 */
	public static function fetchTaskReasons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskReason', $objDatabase );
	}

	/**
	 * @return CTaskReason
	 */
	public static function fetchTaskReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskReason', $objDatabase );
	}

	public static function fetchTaskReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_reasons', $objDatabase );
	}

	public static function fetchTaskReasonById( $intId, $objDatabase ) {
		return self::fetchTaskReason( sprintf( 'SELECT * FROM task_reasons WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTaskReasonsByTaskReasonTypeId( $intTaskReasonTypeId, $objDatabase ) {
		return self::fetchTaskReasons( sprintf( 'SELECT * FROM task_reasons WHERE task_reason_type_id = %d', ( int ) $intTaskReasonTypeId ), $objDatabase );
	}

}
?>