<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployees
 * Do not add any new functions to this class.
 */

class CBaseEmployees extends CEosPluralBase {

	/**
	 * @return CEmployee[]
	 */
	public static function fetchEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployee', $objDatabase );
	}

	/**
	 * @return CEmployee
	 */
	public static function fetchEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployee', $objDatabase );
	}

	public static function fetchEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employees', $objDatabase );
	}

	public static function fetchEmployeeById( $intId, $objDatabase ) {
		return self::fetchEmployee( sprintf( 'SELECT * FROM employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeesByEmployeeStatusTypeId( $intEmployeeStatusTypeId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM employees WHERE employee_status_type_id = %d', ( int ) $intEmployeeStatusTypeId ), $objDatabase );
	}

	public static function fetchEmployeesByEmployeeSalaryTypeId( $intEmployeeSalaryTypeId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM employees WHERE employee_salary_type_id = %d', ( int ) $intEmployeeSalaryTypeId ), $objDatabase );
	}

	public static function fetchEmployeesByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM employees WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchEmployeesByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM employees WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchEmployeesByTechnologyId( $intTechnologyId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM employees WHERE technology_id = %d', ( int ) $intTechnologyId ), $objDatabase );
	}

	public static function fetchEmployeesByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM employees WHERE designation_id = %d', ( int ) $intDesignationId ), $objDatabase );
	}

	public static function fetchEmployeesByOfficeId( $intOfficeId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM employees WHERE office_id = %d', ( int ) $intOfficeId ), $objDatabase );
	}

	public static function fetchEmployeesByOfficeDeskId( $intOfficeDeskId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM employees WHERE office_desk_id = %d', ( int ) $intOfficeDeskId ), $objDatabase );
	}

	public static function fetchEmployeesByDeskAllocationOfficeDeskId( $intDeskAllocationOfficeDeskId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM employees WHERE desk_allocation_office_desk_id = %d', ( int ) $intDeskAllocationOfficeDeskId ), $objDatabase );
	}

	public static function fetchEmployeesByPhysicalPhoneExtensionId( $intPhysicalPhoneExtensionId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM employees WHERE physical_phone_extension_id = %d', ( int ) $intPhysicalPhoneExtensionId ), $objDatabase );
	}

	public static function fetchEmployeesByAgentPhoneExtensionId( $intAgentPhoneExtensionId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM employees WHERE agent_phone_extension_id = %d', ( int ) $intAgentPhoneExtensionId ), $objDatabase );
	}

	public static function fetchEmployeesByAnnualBonusTypeId( $intAnnualBonusTypeId, $objDatabase ) {
		return self::fetchEmployees( sprintf( 'SELECT * FROM employees WHERE annual_bonus_type_id = %d', ( int ) $intAnnualBonusTypeId ), $objDatabase );
	}

}
?>