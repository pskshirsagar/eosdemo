<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestTypes
 * Do not add any new functions to this class.
 */

class CBaseTestTypes extends CEosPluralBase {

	/**
	 * @return CTestType[]
	 */
	public static function fetchTestTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestType', $objDatabase );
	}

	/**
	 * @return CTestType
	 */
	public static function fetchTestType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestType', $objDatabase );
	}

	public static function fetchTestTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_types', $objDatabase );
	}

	public static function fetchTestTypeById( $intId, $objDatabase ) {
		return self::fetchTestType( sprintf( 'SELECT * FROM test_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestTypesByCertificationTypeId( $intCertificationTypeId, $objDatabase ) {
		return self::fetchTestTypes( sprintf( 'SELECT * FROM test_types WHERE certification_type_id = %d', ( int ) $intCertificationTypeId ), $objDatabase );
	}

}
?>