<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestSubmissions
 * Do not add any new functions to this class.
 */

class CBaseTestSubmissions extends CEosPluralBase {

	/**
	 * @return CTestSubmission[]
	 */
	public static function fetchTestSubmissions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestSubmission', $objDatabase );
	}

	/**
	 * @return CTestSubmission
	 */
	public static function fetchTestSubmission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestSubmission', $objDatabase );
	}

	public static function fetchTestSubmissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_submissions', $objDatabase );
	}

	public static function fetchTestSubmissionById( $intId, $objDatabase ) {
		return self::fetchTestSubmission( sprintf( 'SELECT * FROM test_submissions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestSubmissionsByTestId( $intTestId, $objDatabase ) {
		return self::fetchTestSubmissions( sprintf( 'SELECT * FROM test_submissions WHERE test_id = %d', ( int ) $intTestId ), $objDatabase );
	}

	public static function fetchTestSubmissionsByEmployeeTestId( $intEmployeeTestId, $objDatabase ) {
		return self::fetchTestSubmissions( sprintf( 'SELECT * FROM test_submissions WHERE employee_test_id = %d', ( int ) $intEmployeeTestId ), $objDatabase );
	}

	public static function fetchTestSubmissionsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchTestSubmissions( sprintf( 'SELECT * FROM test_submissions WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchTestSubmissionsByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchTestSubmissions( sprintf( 'SELECT * FROM test_submissions WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchTestSubmissionsByTrainingSessionId( $intTrainingSessionId, $objDatabase ) {
		return self::fetchTestSubmissions( sprintf( 'SELECT * FROM test_submissions WHERE training_session_id = %d', ( int ) $intTrainingSessionId ), $objDatabase );
	}

}
?>