<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsImplementationEmployees
 * Do not add any new functions to this class.
 */

class CBaseStatsImplementationEmployees extends CEosPluralBase {

	/**
	 * @return CStatsImplementationEmployee[]
	 */
	public static function fetchStatsImplementationEmployees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStatsImplementationEmployee', $objDatabase );
	}

	/**
	 * @return CStatsImplementationEmployee
	 */
	public static function fetchStatsImplementationEmployee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStatsImplementationEmployee', $objDatabase );
	}

	public static function fetchStatsImplementationEmployeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stats_implementation_employees', $objDatabase );
	}

	public static function fetchStatsImplementationEmployeeById( $intId, $objDatabase ) {
		return self::fetchStatsImplementationEmployee( sprintf( 'SELECT * FROM stats_implementation_employees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchStatsImplementationEmployeesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchStatsImplementationEmployees( sprintf( 'SELECT * FROM stats_implementation_employees WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>