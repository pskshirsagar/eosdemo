<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CClientNames
 * Do not add any new functions to this class.
 */

class CBaseClientNames extends CEosPluralBase {

	/**
	 * @return CClientName[]
	 */
	public static function fetchClientNames( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CClientName', $objDatabase );
	}

	/**
	 * @return CClientName
	 */
	public static function fetchClientName( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CClientName', $objDatabase );
	}

	public static function fetchClientNameCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'client_names', $objDatabase );
	}

	public static function fetchClientNameById( $intId, $objDatabase ) {
		return self::fetchClientName( sprintf( 'SELECT * FROM client_names WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchClientNamesByCid( $intCid, $objDatabase ) {
		return self::fetchClientNames( sprintf( 'SELECT * FROM client_names WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchClientNamesByClientNameTypeId( $intClientNameTypeId, $objDatabase ) {
		return self::fetchClientNames( sprintf( 'SELECT * FROM client_names WHERE client_name_type_id = %d', ( int ) $intClientNameTypeId ), $objDatabase );
	}

}
?>