<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportSqlDatabases
 * Do not add any new functions to this class.
 */

class CBaseReportSqlDatabases extends CEosPluralBase {

	/**
	 * @return CReportSqlDatabase[]
	 */
	public static function fetchReportSqlDatabases( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportSqlDatabase::class, $objDatabase );
	}

	/**
	 * @return CReportSqlDatabase
	 */
	public static function fetchReportSqlDatabase( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportSqlDatabase::class, $objDatabase );
	}

	public static function fetchReportSqlDatabaseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_sql_databases', $objDatabase );
	}

	public static function fetchReportSqlDatabaseById( $intId, $objDatabase ) {
		return self::fetchReportSqlDatabase( sprintf( 'SELECT * FROM report_sql_databases WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportSqlDatabasesByReportSqlDetailId( $intReportSqlDetailId, $objDatabase ) {
		return self::fetchReportSqlDatabases( sprintf( 'SELECT * FROM report_sql_databases WHERE report_sql_detail_id = %d', ( int ) $intReportSqlDetailId ), $objDatabase );
	}

	public static function fetchReportSqlDatabasesByDatabaseId( $intDatabaseId, $objDatabase ) {
		return self::fetchReportSqlDatabases( sprintf( 'SELECT * FROM report_sql_databases WHERE database_id = %d', ( int ) $intDatabaseId ), $objDatabase );
	}

}
?>