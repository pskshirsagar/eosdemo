<?php

class CBaseCompensationGrade extends CEosSingularBase {

	const TABLE_NAME = 'public.compensation_grades';

	protected $m_intId;
	protected $m_strPayReviewYear;
	protected $m_intDesignationId;
	protected $m_intTechnologyId;
	protected $m_strGradeValue;
	protected $m_strStartAmountEncrypted;
	protected $m_strEndAmountEncrypted;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['pay_review_year'] ) && $boolDirectSet ) $this->set( 'm_strPayReviewYear', trim( $arrValues['pay_review_year'] ) ); elseif( isset( $arrValues['pay_review_year'] ) ) $this->setPayReviewYear( $arrValues['pay_review_year'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['technology_id'] ) && $boolDirectSet ) $this->set( 'm_intTechnologyId', trim( $arrValues['technology_id'] ) ); elseif( isset( $arrValues['technology_id'] ) ) $this->setTechnologyId( $arrValues['technology_id'] );
		if( isset( $arrValues['grade_value'] ) && $boolDirectSet ) $this->set( 'm_strGradeValue', trim( stripcslashes( $arrValues['grade_value'] ) ) ); elseif( isset( $arrValues['grade_value'] ) ) $this->setGradeValue( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['grade_value'] ) : $arrValues['grade_value'] );
		if( isset( $arrValues['start_amount_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strStartAmountEncrypted', trim( $arrValues['start_amount_encrypted'] ) ); elseif( isset( $arrValues['start_amount_encrypted'] ) ) $this->setStartAmountEncrypted( $arrValues['start_amount_encrypted'] );
		if( isset( $arrValues['end_amount_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strEndAmountEncrypted', trim( $arrValues['end_amount_encrypted'] ) ); elseif( isset( $arrValues['end_amount_encrypted'] ) ) $this->setEndAmountEncrypted( $arrValues['end_amount_encrypted'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPayReviewYear( $strPayReviewYear ) {
		$this->set( 'm_strPayReviewYear', CStrings::strTrimDef( $strPayReviewYear, -1, NULL, true ) );
	}

	public function getPayReviewYear() {
		return $this->m_strPayReviewYear;
	}

	public function sqlPayReviewYear() {
		return ( true == isset( $this->m_strPayReviewYear ) ) ? '\'' . $this->m_strPayReviewYear . '\'' : 'NOW()';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setTechnologyId( $intTechnologyId ) {
		$this->set( 'm_intTechnologyId', CStrings::strToIntDef( $intTechnologyId, NULL, false ) );
	}

	public function getTechnologyId() {
		return $this->m_intTechnologyId;
	}

	public function sqlTechnologyId() {
		return ( true == isset( $this->m_intTechnologyId ) ) ? ( string ) $this->m_intTechnologyId : 'NULL';
	}

	public function setGradeValue( $strGradeValue ) {
		$this->set( 'm_strGradeValue', CStrings::strTrimDef( $strGradeValue, -1, NULL, true ) );
	}

	public function getGradeValue() {
		return $this->m_strGradeValue;
	}

	public function sqlGradeValue() {
		return ( true == isset( $this->m_strGradeValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strGradeValue ) : '\'' . addslashes( $this->m_strGradeValue ) . '\'' ) : 'NULL';
	}

	public function setStartAmountEncrypted( $strStartAmountEncrypted ) {
		$this->set( 'm_strStartAmountEncrypted', CStrings::strTrimDef( $strStartAmountEncrypted, -1, NULL, true ) );
	}

	public function getStartAmountEncrypted() {
		return $this->m_strStartAmountEncrypted;
	}

	public function sqlStartAmountEncrypted() {
		return ( true == isset( $this->m_strStartAmountEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strStartAmountEncrypted ) : '\'' . addslashes( $this->m_strStartAmountEncrypted ) . '\'' ) : 'NULL';
	}

	public function setEndAmountEncrypted( $strEndAmountEncrypted ) {
		$this->set( 'm_strEndAmountEncrypted', CStrings::strTrimDef( $strEndAmountEncrypted, -1, NULL, true ) );
	}

	public function getEndAmountEncrypted() {
		return $this->m_strEndAmountEncrypted;
	}

	public function sqlEndAmountEncrypted() {
		return ( true == isset( $this->m_strEndAmountEncrypted ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strEndAmountEncrypted ) : '\'' . addslashes( $this->m_strEndAmountEncrypted ) . '\'' ) : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, pay_review_year, designation_id, technology_id, grade_value, start_amount_encrypted, end_amount_encrypted, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPayReviewYear() . ', ' .
						$this->sqlDesignationId() . ', ' .
						$this->sqlTechnologyId() . ', ' .
						$this->sqlGradeValue() . ', ' .
						$this->sqlStartAmountEncrypted() . ', ' .
						$this->sqlEndAmountEncrypted() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pay_review_year = ' . $this->sqlPayReviewYear(). ',' ; } elseif( true == array_key_exists( 'PayReviewYear', $this->getChangedColumns() ) ) { $strSql .= ' pay_review_year = ' . $this->sqlPayReviewYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId(). ',' ; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' technology_id = ' . $this->sqlTechnologyId(). ',' ; } elseif( true == array_key_exists( 'TechnologyId', $this->getChangedColumns() ) ) { $strSql .= ' technology_id = ' . $this->sqlTechnologyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' grade_value = ' . $this->sqlGradeValue(). ',' ; } elseif( true == array_key_exists( 'GradeValue', $this->getChangedColumns() ) ) { $strSql .= ' grade_value = ' . $this->sqlGradeValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_amount_encrypted = ' . $this->sqlStartAmountEncrypted(). ',' ; } elseif( true == array_key_exists( 'StartAmountEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' start_amount_encrypted = ' . $this->sqlStartAmountEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_amount_encrypted = ' . $this->sqlEndAmountEncrypted(). ',' ; } elseif( true == array_key_exists( 'EndAmountEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' end_amount_encrypted = ' . $this->sqlEndAmountEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'pay_review_year' => $this->getPayReviewYear(),
			'designation_id' => $this->getDesignationId(),
			'technology_id' => $this->getTechnologyId(),
			'grade_value' => $this->getGradeValue(),
			'start_amount_encrypted' => $this->getStartAmountEncrypted(),
			'end_amount_encrypted' => $this->getEndAmountEncrypted(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>