<?php

class CBaseEmployeeApplicationDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_application_details';

	protected $m_intId;
	protected $m_intEmployeeApplicationId;
	protected $m_intInterviewer1EmployeeId;
	protected $m_intInterviewer2EmployeeId;
	protected $m_intOfficeDeskId;
	protected $m_intPsDocumentId;
	protected $m_strReviewCycle;
	protected $m_strBaseWageEncrypted;
	protected $m_strBonusAmountEncrypted;
	protected $m_strBonusDescriptionEncrypted;
	protected $m_strRelocationAllowanceEncrypted;
	protected $m_intPtoDays;
	protected $m_strEquipmentSoftware;
	protected $m_strOfferNote;
	protected $m_strCaseForHiring;
	protected $m_strPsFamiliarityDescription;
	protected $m_strPsConversationDescription;
	protected $m_strPsApplicationHistory;
	protected $m_strLegalAgreementDescription;
	protected $m_strReasonForLeaving;
	protected $m_intExperienceYear;
	protected $m_intExperienceMonth;
	protected $m_strWorkExperience;
	protected $m_strPositionAppliedFor;
	protected $m_strCurrentCompany;
	protected $m_strCurrentLocation;
	protected $m_boolIsReadyToRelocate;
	protected $m_strCurrentAnnualCompensationEncrypted;
	protected $m_strExpectedAnnualCompensationEncrypted;
	protected $m_strNegotiatedAnnualCompensationEncrypted;
	protected $m_intExpectedEmploymentDays;
	protected $m_strSkillSets;
	protected $m_strInterestDescription;
	protected $m_strCriminalBackgroundEncrypted;
	protected $m_intHasLegalStatus;
	protected $m_strOnlineTestDate;
	protected $m_strJoiningDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsReadyToRelocate = false;
		$this->m_intHasLegalStatus = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationId', trim( $arrValues['employee_application_id'] ) ); elseif( isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( isset( $arrValues['interviewer1_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intInterviewer1EmployeeId', trim( $arrValues['interviewer1_employee_id'] ) ); elseif( isset( $arrValues['interviewer1_employee_id'] ) ) $this->setInterviewer1EmployeeId( $arrValues['interviewer1_employee_id'] );
		if( isset( $arrValues['interviewer2_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intInterviewer2EmployeeId', trim( $arrValues['interviewer2_employee_id'] ) ); elseif( isset( $arrValues['interviewer2_employee_id'] ) ) $this->setInterviewer2EmployeeId( $arrValues['interviewer2_employee_id'] );
		if( isset( $arrValues['office_desk_id'] ) && $boolDirectSet ) $this->set( 'm_intOfficeDeskId', trim( $arrValues['office_desk_id'] ) ); elseif( isset( $arrValues['office_desk_id'] ) ) $this->setOfficeDeskId( $arrValues['office_desk_id'] );
		if( isset( $arrValues['ps_document_id'] ) && $boolDirectSet ) $this->set( 'm_intPsDocumentId', trim( $arrValues['ps_document_id'] ) ); elseif( isset( $arrValues['ps_document_id'] ) ) $this->setPsDocumentId( $arrValues['ps_document_id'] );
		if( isset( $arrValues['review_cycle'] ) && $boolDirectSet ) $this->set( 'm_strReviewCycle', trim( stripcslashes( $arrValues['review_cycle'] ) ) ); elseif( isset( $arrValues['review_cycle'] ) ) $this->setReviewCycle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['review_cycle'] ) : $arrValues['review_cycle'] );
		if( isset( $arrValues['base_wage_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBaseWageEncrypted', trim( stripcslashes( $arrValues['base_wage_encrypted'] ) ) ); elseif( isset( $arrValues['base_wage_encrypted'] ) ) $this->setBaseWageEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['base_wage_encrypted'] ) : $arrValues['base_wage_encrypted'] );
		if( isset( $arrValues['bonus_amount_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBonusAmountEncrypted', trim( stripcslashes( $arrValues['bonus_amount_encrypted'] ) ) ); elseif( isset( $arrValues['bonus_amount_encrypted'] ) ) $this->setBonusAmountEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bonus_amount_encrypted'] ) : $arrValues['bonus_amount_encrypted'] );
		if( isset( $arrValues['bonus_description_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBonusDescriptionEncrypted', trim( stripcslashes( $arrValues['bonus_description_encrypted'] ) ) ); elseif( isset( $arrValues['bonus_description_encrypted'] ) ) $this->setBonusDescriptionEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bonus_description_encrypted'] ) : $arrValues['bonus_description_encrypted'] );
		if( isset( $arrValues['relocation_allowance_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strRelocationAllowanceEncrypted', trim( stripcslashes( $arrValues['relocation_allowance_encrypted'] ) ) ); elseif( isset( $arrValues['relocation_allowance_encrypted'] ) ) $this->setRelocationAllowanceEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['relocation_allowance_encrypted'] ) : $arrValues['relocation_allowance_encrypted'] );
		if( isset( $arrValues['pto_days'] ) && $boolDirectSet ) $this->set( 'm_intPtoDays', trim( $arrValues['pto_days'] ) ); elseif( isset( $arrValues['pto_days'] ) ) $this->setPtoDays( $arrValues['pto_days'] );
		if( isset( $arrValues['equipment_software'] ) && $boolDirectSet ) $this->set( 'm_strEquipmentSoftware', trim( stripcslashes( $arrValues['equipment_software'] ) ) ); elseif( isset( $arrValues['equipment_software'] ) ) $this->setEquipmentSoftware( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['equipment_software'] ) : $arrValues['equipment_software'] );
		if( isset( $arrValues['offer_note'] ) && $boolDirectSet ) $this->set( 'm_strOfferNote', trim( stripcslashes( $arrValues['offer_note'] ) ) ); elseif( isset( $arrValues['offer_note'] ) ) $this->setOfferNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['offer_note'] ) : $arrValues['offer_note'] );
		if( isset( $arrValues['case_for_hiring'] ) && $boolDirectSet ) $this->set( 'm_strCaseForHiring', trim( stripcslashes( $arrValues['case_for_hiring'] ) ) ); elseif( isset( $arrValues['case_for_hiring'] ) ) $this->setCaseForHiring( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['case_for_hiring'] ) : $arrValues['case_for_hiring'] );
		if( isset( $arrValues['ps_familiarity_description'] ) && $boolDirectSet ) $this->set( 'm_strPsFamiliarityDescription', trim( stripcslashes( $arrValues['ps_familiarity_description'] ) ) ); elseif( isset( $arrValues['ps_familiarity_description'] ) ) $this->setPsFamiliarityDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_familiarity_description'] ) : $arrValues['ps_familiarity_description'] );
		if( isset( $arrValues['ps_conversation_description'] ) && $boolDirectSet ) $this->set( 'm_strPsConversationDescription', trim( stripcslashes( $arrValues['ps_conversation_description'] ) ) ); elseif( isset( $arrValues['ps_conversation_description'] ) ) $this->setPsConversationDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_conversation_description'] ) : $arrValues['ps_conversation_description'] );
		if( isset( $arrValues['ps_application_history'] ) && $boolDirectSet ) $this->set( 'm_strPsApplicationHistory', trim( stripcslashes( $arrValues['ps_application_history'] ) ) ); elseif( isset( $arrValues['ps_application_history'] ) ) $this->setPsApplicationHistory( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ps_application_history'] ) : $arrValues['ps_application_history'] );
		if( isset( $arrValues['legal_agreement_description'] ) && $boolDirectSet ) $this->set( 'm_strLegalAgreementDescription', trim( stripcslashes( $arrValues['legal_agreement_description'] ) ) ); elseif( isset( $arrValues['legal_agreement_description'] ) ) $this->setLegalAgreementDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['legal_agreement_description'] ) : $arrValues['legal_agreement_description'] );
		if( isset( $arrValues['reason_for_leaving'] ) && $boolDirectSet ) $this->set( 'm_strReasonForLeaving', trim( stripcslashes( $arrValues['reason_for_leaving'] ) ) ); elseif( isset( $arrValues['reason_for_leaving'] ) ) $this->setReasonForLeaving( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['reason_for_leaving'] ) : $arrValues['reason_for_leaving'] );
		if( isset( $arrValues['experience_year'] ) && $boolDirectSet ) $this->set( 'm_intExperienceYear', trim( $arrValues['experience_year'] ) ); elseif( isset( $arrValues['experience_year'] ) ) $this->setExperienceYear( $arrValues['experience_year'] );
		if( isset( $arrValues['experience_month'] ) && $boolDirectSet ) $this->set( 'm_intExperienceMonth', trim( $arrValues['experience_month'] ) ); elseif( isset( $arrValues['experience_month'] ) ) $this->setExperienceMonth( $arrValues['experience_month'] );
		if( isset( $arrValues['work_experience'] ) && $boolDirectSet ) $this->set( 'm_strWorkExperience', trim( stripcslashes( $arrValues['work_experience'] ) ) ); elseif( isset( $arrValues['work_experience'] ) ) $this->setWorkExperience( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['work_experience'] ) : $arrValues['work_experience'] );
		if( isset( $arrValues['position_applied_for'] ) && $boolDirectSet ) $this->set( 'm_strPositionAppliedFor', trim( stripcslashes( $arrValues['position_applied_for'] ) ) ); elseif( isset( $arrValues['position_applied_for'] ) ) $this->setPositionAppliedFor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['position_applied_for'] ) : $arrValues['position_applied_for'] );
		if( isset( $arrValues['current_company'] ) && $boolDirectSet ) $this->set( 'm_strCurrentCompany', trim( stripcslashes( $arrValues['current_company'] ) ) ); elseif( isset( $arrValues['current_company'] ) ) $this->setCurrentCompany( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['current_company'] ) : $arrValues['current_company'] );
		if( isset( $arrValues['current_location'] ) && $boolDirectSet ) $this->set( 'm_strCurrentLocation', trim( stripcslashes( $arrValues['current_location'] ) ) ); elseif( isset( $arrValues['current_location'] ) ) $this->setCurrentLocation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['current_location'] ) : $arrValues['current_location'] );
		if( isset( $arrValues['is_ready_to_relocate'] ) && $boolDirectSet ) $this->set( 'm_boolIsReadyToRelocate', trim( stripcslashes( $arrValues['is_ready_to_relocate'] ) ) ); elseif( isset( $arrValues['is_ready_to_relocate'] ) ) $this->setIsReadyToRelocate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_ready_to_relocate'] ) : $arrValues['is_ready_to_relocate'] );
		if( isset( $arrValues['current_annual_compensation_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCurrentAnnualCompensationEncrypted', trim( stripcslashes( $arrValues['current_annual_compensation_encrypted'] ) ) ); elseif( isset( $arrValues['current_annual_compensation_encrypted'] ) ) $this->setCurrentAnnualCompensationEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['current_annual_compensation_encrypted'] ) : $arrValues['current_annual_compensation_encrypted'] );
		if( isset( $arrValues['expected_annual_compensation_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strExpectedAnnualCompensationEncrypted', trim( stripcslashes( $arrValues['expected_annual_compensation_encrypted'] ) ) ); elseif( isset( $arrValues['expected_annual_compensation_encrypted'] ) ) $this->setExpectedAnnualCompensationEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['expected_annual_compensation_encrypted'] ) : $arrValues['expected_annual_compensation_encrypted'] );
		if( isset( $arrValues['negotiated_annual_compensation_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strNegotiatedAnnualCompensationEncrypted', trim( stripcslashes( $arrValues['negotiated_annual_compensation_encrypted'] ) ) ); elseif( isset( $arrValues['negotiated_annual_compensation_encrypted'] ) ) $this->setNegotiatedAnnualCompensationEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['negotiated_annual_compensation_encrypted'] ) : $arrValues['negotiated_annual_compensation_encrypted'] );
		if( isset( $arrValues['expected_employment_days'] ) && $boolDirectSet ) $this->set( 'm_intExpectedEmploymentDays', trim( $arrValues['expected_employment_days'] ) ); elseif( isset( $arrValues['expected_employment_days'] ) ) $this->setExpectedEmploymentDays( $arrValues['expected_employment_days'] );
		if( isset( $arrValues['skill_sets'] ) && $boolDirectSet ) $this->set( 'm_strSkillSets', trim( stripcslashes( $arrValues['skill_sets'] ) ) ); elseif( isset( $arrValues['skill_sets'] ) ) $this->setSkillSets( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['skill_sets'] ) : $arrValues['skill_sets'] );
		if( isset( $arrValues['interest_description'] ) && $boolDirectSet ) $this->set( 'm_strInterestDescription', trim( stripcslashes( $arrValues['interest_description'] ) ) ); elseif( isset( $arrValues['interest_description'] ) ) $this->setInterestDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['interest_description'] ) : $arrValues['interest_description'] );
		if( isset( $arrValues['criminal_background_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strCriminalBackgroundEncrypted', trim( stripcslashes( $arrValues['criminal_background_encrypted'] ) ) ); elseif( isset( $arrValues['criminal_background_encrypted'] ) ) $this->setCriminalBackgroundEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['criminal_background_encrypted'] ) : $arrValues['criminal_background_encrypted'] );
		if( isset( $arrValues['has_legal_status'] ) && $boolDirectSet ) $this->set( 'm_intHasLegalStatus', trim( $arrValues['has_legal_status'] ) ); elseif( isset( $arrValues['has_legal_status'] ) ) $this->setHasLegalStatus( $arrValues['has_legal_status'] );
		if( isset( $arrValues['online_test_date'] ) && $boolDirectSet ) $this->set( 'm_strOnlineTestDate', trim( $arrValues['online_test_date'] ) ); elseif( isset( $arrValues['online_test_date'] ) ) $this->setOnlineTestDate( $arrValues['online_test_date'] );
		if( isset( $arrValues['joining_date'] ) && $boolDirectSet ) $this->set( 'm_strJoiningDate', trim( $arrValues['joining_date'] ) ); elseif( isset( $arrValues['joining_date'] ) ) $this->setJoiningDate( $arrValues['joining_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeApplicationId( $intEmployeeApplicationId ) {
		$this->set( 'm_intEmployeeApplicationId', CStrings::strToIntDef( $intEmployeeApplicationId, NULL, false ) );
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function sqlEmployeeApplicationId() {
		return ( true == isset( $this->m_intEmployeeApplicationId ) ) ? ( string ) $this->m_intEmployeeApplicationId : 'NULL';
	}

	public function setInterviewer1EmployeeId( $intInterviewer1EmployeeId ) {
		$this->set( 'm_intInterviewer1EmployeeId', CStrings::strToIntDef( $intInterviewer1EmployeeId, NULL, false ) );
	}

	public function getInterviewer1EmployeeId() {
		return $this->m_intInterviewer1EmployeeId;
	}

	public function sqlInterviewer1EmployeeId() {
		return ( true == isset( $this->m_intInterviewer1EmployeeId ) ) ? ( string ) $this->m_intInterviewer1EmployeeId : 'NULL';
	}

	public function setInterviewer2EmployeeId( $intInterviewer2EmployeeId ) {
		$this->set( 'm_intInterviewer2EmployeeId', CStrings::strToIntDef( $intInterviewer2EmployeeId, NULL, false ) );
	}

	public function getInterviewer2EmployeeId() {
		return $this->m_intInterviewer2EmployeeId;
	}

	public function sqlInterviewer2EmployeeId() {
		return ( true == isset( $this->m_intInterviewer2EmployeeId ) ) ? ( string ) $this->m_intInterviewer2EmployeeId : 'NULL';
	}

	public function setOfficeDeskId( $intOfficeDeskId ) {
		$this->set( 'm_intOfficeDeskId', CStrings::strToIntDef( $intOfficeDeskId, NULL, false ) );
	}

	public function getOfficeDeskId() {
		return $this->m_intOfficeDeskId;
	}

	public function sqlOfficeDeskId() {
		return ( true == isset( $this->m_intOfficeDeskId ) ) ? ( string ) $this->m_intOfficeDeskId : 'NULL';
	}

	public function setPsDocumentId( $intPsDocumentId ) {
		$this->set( 'm_intPsDocumentId', CStrings::strToIntDef( $intPsDocumentId, NULL, false ) );
	}

	public function getPsDocumentId() {
		return $this->m_intPsDocumentId;
	}

	public function sqlPsDocumentId() {
		return ( true == isset( $this->m_intPsDocumentId ) ) ? ( string ) $this->m_intPsDocumentId : 'NULL';
	}

	public function setReviewCycle( $strReviewCycle ) {
		$this->set( 'm_strReviewCycle', CStrings::strTrimDef( $strReviewCycle, 8, NULL, true ) );
	}

	public function getReviewCycle() {
		return $this->m_strReviewCycle;
	}

	public function sqlReviewCycle() {
		return ( true == isset( $this->m_strReviewCycle ) ) ? '\'' . addslashes( $this->m_strReviewCycle ) . '\'' : 'NULL';
	}

	public function setBaseWageEncrypted( $strBaseWageEncrypted ) {
		$this->set( 'm_strBaseWageEncrypted', CStrings::strTrimDef( $strBaseWageEncrypted, -1, NULL, true ) );
	}

	public function getBaseWageEncrypted() {
		return $this->m_strBaseWageEncrypted;
	}

	public function sqlBaseWageEncrypted() {
		return ( true == isset( $this->m_strBaseWageEncrypted ) ) ? '\'' . addslashes( $this->m_strBaseWageEncrypted ) . '\'' : 'NULL';
	}

	public function setBonusAmountEncrypted( $strBonusAmountEncrypted ) {
		$this->set( 'm_strBonusAmountEncrypted', CStrings::strTrimDef( $strBonusAmountEncrypted, -1, NULL, true ) );
	}

	public function getBonusAmountEncrypted() {
		return $this->m_strBonusAmountEncrypted;
	}

	public function sqlBonusAmountEncrypted() {
		return ( true == isset( $this->m_strBonusAmountEncrypted ) ) ? '\'' . addslashes( $this->m_strBonusAmountEncrypted ) . '\'' : 'NULL';
	}

	public function setBonusDescriptionEncrypted( $strBonusDescriptionEncrypted ) {
		$this->set( 'm_strBonusDescriptionEncrypted', CStrings::strTrimDef( $strBonusDescriptionEncrypted, -1, NULL, true ) );
	}

	public function getBonusDescriptionEncrypted() {
		return $this->m_strBonusDescriptionEncrypted;
	}

	public function sqlBonusDescriptionEncrypted() {
		return ( true == isset( $this->m_strBonusDescriptionEncrypted ) ) ? '\'' . addslashes( $this->m_strBonusDescriptionEncrypted ) . '\'' : 'NULL';
	}

	public function setRelocationAllowanceEncrypted( $strRelocationAllowanceEncrypted ) {
		$this->set( 'm_strRelocationAllowanceEncrypted', CStrings::strTrimDef( $strRelocationAllowanceEncrypted, -1, NULL, true ) );
	}

	public function getRelocationAllowanceEncrypted() {
		return $this->m_strRelocationAllowanceEncrypted;
	}

	public function sqlRelocationAllowanceEncrypted() {
		return ( true == isset( $this->m_strRelocationAllowanceEncrypted ) ) ? '\'' . addslashes( $this->m_strRelocationAllowanceEncrypted ) . '\'' : 'NULL';
	}

	public function setPtoDays( $intPtoDays ) {
		$this->set( 'm_intPtoDays', CStrings::strToIntDef( $intPtoDays, NULL, false ) );
	}

	public function getPtoDays() {
		return $this->m_intPtoDays;
	}

	public function sqlPtoDays() {
		return ( true == isset( $this->m_intPtoDays ) ) ? ( string ) $this->m_intPtoDays : 'NULL';
	}

	public function setEquipmentSoftware( $strEquipmentSoftware ) {
		$this->set( 'm_strEquipmentSoftware', CStrings::strTrimDef( $strEquipmentSoftware, -1, NULL, true ) );
	}

	public function getEquipmentSoftware() {
		return $this->m_strEquipmentSoftware;
	}

	public function sqlEquipmentSoftware() {
		return ( true == isset( $this->m_strEquipmentSoftware ) ) ? '\'' . addslashes( $this->m_strEquipmentSoftware ) . '\'' : 'NULL';
	}

	public function setOfferNote( $strOfferNote ) {
		$this->set( 'm_strOfferNote', CStrings::strTrimDef( $strOfferNote, -1, NULL, true ) );
	}

	public function getOfferNote() {
		return $this->m_strOfferNote;
	}

	public function sqlOfferNote() {
		return ( true == isset( $this->m_strOfferNote ) ) ? '\'' . addslashes( $this->m_strOfferNote ) . '\'' : 'NULL';
	}

	public function setCaseForHiring( $strCaseForHiring ) {
		$this->set( 'm_strCaseForHiring', CStrings::strTrimDef( $strCaseForHiring, -1, NULL, true ) );
	}

	public function getCaseForHiring() {
		return $this->m_strCaseForHiring;
	}

	public function sqlCaseForHiring() {
		return ( true == isset( $this->m_strCaseForHiring ) ) ? '\'' . addslashes( $this->m_strCaseForHiring ) . '\'' : 'NULL';
	}

	public function setPsFamiliarityDescription( $strPsFamiliarityDescription ) {
		$this->set( 'm_strPsFamiliarityDescription', CStrings::strTrimDef( $strPsFamiliarityDescription, -1, NULL, true ) );
	}

	public function getPsFamiliarityDescription() {
		return $this->m_strPsFamiliarityDescription;
	}

	public function sqlPsFamiliarityDescription() {
		return ( true == isset( $this->m_strPsFamiliarityDescription ) ) ? '\'' . addslashes( $this->m_strPsFamiliarityDescription ) . '\'' : 'NULL';
	}

	public function setPsConversationDescription( $strPsConversationDescription ) {
		$this->set( 'm_strPsConversationDescription', CStrings::strTrimDef( $strPsConversationDescription, -1, NULL, true ) );
	}

	public function getPsConversationDescription() {
		return $this->m_strPsConversationDescription;
	}

	public function sqlPsConversationDescription() {
		return ( true == isset( $this->m_strPsConversationDescription ) ) ? '\'' . addslashes( $this->m_strPsConversationDescription ) . '\'' : 'NULL';
	}

	public function setPsApplicationHistory( $strPsApplicationHistory ) {
		$this->set( 'm_strPsApplicationHistory', CStrings::strTrimDef( $strPsApplicationHistory, -1, NULL, true ) );
	}

	public function getPsApplicationHistory() {
		return $this->m_strPsApplicationHistory;
	}

	public function sqlPsApplicationHistory() {
		return ( true == isset( $this->m_strPsApplicationHistory ) ) ? '\'' . addslashes( $this->m_strPsApplicationHistory ) . '\'' : 'NULL';
	}

	public function setLegalAgreementDescription( $strLegalAgreementDescription ) {
		$this->set( 'm_strLegalAgreementDescription', CStrings::strTrimDef( $strLegalAgreementDescription, -1, NULL, true ) );
	}

	public function getLegalAgreementDescription() {
		return $this->m_strLegalAgreementDescription;
	}

	public function sqlLegalAgreementDescription() {
		return ( true == isset( $this->m_strLegalAgreementDescription ) ) ? '\'' . addslashes( $this->m_strLegalAgreementDescription ) . '\'' : 'NULL';
	}

	public function setReasonForLeaving( $strReasonForLeaving ) {
		$this->set( 'm_strReasonForLeaving', CStrings::strTrimDef( $strReasonForLeaving, -1, NULL, true ) );
	}

	public function getReasonForLeaving() {
		return $this->m_strReasonForLeaving;
	}

	public function sqlReasonForLeaving() {
		return ( true == isset( $this->m_strReasonForLeaving ) ) ? '\'' . addslashes( $this->m_strReasonForLeaving ) . '\'' : 'NULL';
	}

	public function setExperienceYear( $intExperienceYear ) {
		$this->set( 'm_intExperienceYear', CStrings::strToIntDef( $intExperienceYear, NULL, false ) );
	}

	public function getExperienceYear() {
		return $this->m_intExperienceYear;
	}

	public function sqlExperienceYear() {
		return ( true == isset( $this->m_intExperienceYear ) ) ? ( string ) $this->m_intExperienceYear : 'NULL';
	}

	public function setExperienceMonth( $intExperienceMonth ) {
		$this->set( 'm_intExperienceMonth', CStrings::strToIntDef( $intExperienceMonth, NULL, false ) );
	}

	public function getExperienceMonth() {
		return $this->m_intExperienceMonth;
	}

	public function sqlExperienceMonth() {
		return ( true == isset( $this->m_intExperienceMonth ) ) ? ( string ) $this->m_intExperienceMonth : 'NULL';
	}

	public function setWorkExperience( $strWorkExperience ) {
		$this->set( 'm_strWorkExperience', CStrings::strTrimDef( $strWorkExperience, -1, NULL, true ) );
	}

	public function getWorkExperience() {
		return $this->m_strWorkExperience;
	}

	public function sqlWorkExperience() {
		return ( true == isset( $this->m_strWorkExperience ) ) ? '\'' . addslashes( $this->m_strWorkExperience ) . '\'' : 'NULL';
	}

	public function setPositionAppliedFor( $strPositionAppliedFor ) {
		$this->set( 'm_strPositionAppliedFor', CStrings::strTrimDef( $strPositionAppliedFor, 50, NULL, true ) );
	}

	public function getPositionAppliedFor() {
		return $this->m_strPositionAppliedFor;
	}

	public function sqlPositionAppliedFor() {
		return ( true == isset( $this->m_strPositionAppliedFor ) ) ? '\'' . addslashes( $this->m_strPositionAppliedFor ) . '\'' : 'NULL';
	}

	public function setCurrentCompany( $strCurrentCompany ) {
		$this->set( 'm_strCurrentCompany', CStrings::strTrimDef( $strCurrentCompany, 50, NULL, true ) );
	}

	public function getCurrentCompany() {
		return $this->m_strCurrentCompany;
	}

	public function sqlCurrentCompany() {
		return ( true == isset( $this->m_strCurrentCompany ) ) ? '\'' . addslashes( $this->m_strCurrentCompany ) . '\'' : 'NULL';
	}

	public function setCurrentLocation( $strCurrentLocation ) {
		$this->set( 'm_strCurrentLocation', CStrings::strTrimDef( $strCurrentLocation, 50, NULL, true ) );
	}

	public function getCurrentLocation() {
		return $this->m_strCurrentLocation;
	}

	public function sqlCurrentLocation() {
		return ( true == isset( $this->m_strCurrentLocation ) ) ? '\'' . addslashes( $this->m_strCurrentLocation ) . '\'' : 'NULL';
	}

	public function setIsReadyToRelocate( $boolIsReadyToRelocate ) {
		$this->set( 'm_boolIsReadyToRelocate', CStrings::strToBool( $boolIsReadyToRelocate ) );
	}

	public function getIsReadyToRelocate() {
		return $this->m_boolIsReadyToRelocate;
	}

	public function sqlIsReadyToRelocate() {
		return ( true == isset( $this->m_boolIsReadyToRelocate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsReadyToRelocate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setCurrentAnnualCompensationEncrypted( $strCurrentAnnualCompensationEncrypted ) {
		$this->set( 'm_strCurrentAnnualCompensationEncrypted', CStrings::strTrimDef( $strCurrentAnnualCompensationEncrypted, -1, NULL, true ) );
	}

	public function getCurrentAnnualCompensationEncrypted() {
		return $this->m_strCurrentAnnualCompensationEncrypted;
	}

	public function sqlCurrentAnnualCompensationEncrypted() {
		return ( true == isset( $this->m_strCurrentAnnualCompensationEncrypted ) ) ? '\'' . addslashes( $this->m_strCurrentAnnualCompensationEncrypted ) . '\'' : 'NULL';
	}

	public function setExpectedAnnualCompensationEncrypted( $strExpectedAnnualCompensationEncrypted ) {
		$this->set( 'm_strExpectedAnnualCompensationEncrypted', CStrings::strTrimDef( $strExpectedAnnualCompensationEncrypted, -1, NULL, true ) );
	}

	public function getExpectedAnnualCompensationEncrypted() {
		return $this->m_strExpectedAnnualCompensationEncrypted;
	}

	public function sqlExpectedAnnualCompensationEncrypted() {
		return ( true == isset( $this->m_strExpectedAnnualCompensationEncrypted ) ) ? '\'' . addslashes( $this->m_strExpectedAnnualCompensationEncrypted ) . '\'' : 'NULL';
	}

	public function setNegotiatedAnnualCompensationEncrypted( $strNegotiatedAnnualCompensationEncrypted ) {
		$this->set( 'm_strNegotiatedAnnualCompensationEncrypted', CStrings::strTrimDef( $strNegotiatedAnnualCompensationEncrypted, -1, NULL, true ) );
	}

	public function getNegotiatedAnnualCompensationEncrypted() {
		return $this->m_strNegotiatedAnnualCompensationEncrypted;
	}

	public function sqlNegotiatedAnnualCompensationEncrypted() {
		return ( true == isset( $this->m_strNegotiatedAnnualCompensationEncrypted ) ) ? '\'' . addslashes( $this->m_strNegotiatedAnnualCompensationEncrypted ) . '\'' : 'NULL';
	}

	public function setExpectedEmploymentDays( $intExpectedEmploymentDays ) {
		$this->set( 'm_intExpectedEmploymentDays', CStrings::strToIntDef( $intExpectedEmploymentDays, NULL, false ) );
	}

	public function getExpectedEmploymentDays() {
		return $this->m_intExpectedEmploymentDays;
	}

	public function sqlExpectedEmploymentDays() {
		return ( true == isset( $this->m_intExpectedEmploymentDays ) ) ? ( string ) $this->m_intExpectedEmploymentDays : 'NULL';
	}

	public function setSkillSets( $strSkillSets ) {
		$this->set( 'm_strSkillSets', CStrings::strTrimDef( $strSkillSets, -1, NULL, true ) );
	}

	public function getSkillSets() {
		return $this->m_strSkillSets;
	}

	public function sqlSkillSets() {
		return ( true == isset( $this->m_strSkillSets ) ) ? '\'' . addslashes( $this->m_strSkillSets ) . '\'' : 'NULL';
	}

	public function setInterestDescription( $strInterestDescription ) {
		$this->set( 'm_strInterestDescription', CStrings::strTrimDef( $strInterestDescription, -1, NULL, true ) );
	}

	public function getInterestDescription() {
		return $this->m_strInterestDescription;
	}

	public function sqlInterestDescription() {
		return ( true == isset( $this->m_strInterestDescription ) ) ? '\'' . addslashes( $this->m_strInterestDescription ) . '\'' : 'NULL';
	}

	public function setCriminalBackgroundEncrypted( $strCriminalBackgroundEncrypted ) {
		$this->set( 'm_strCriminalBackgroundEncrypted', CStrings::strTrimDef( $strCriminalBackgroundEncrypted, -1, NULL, true ) );
	}

	public function getCriminalBackgroundEncrypted() {
		return $this->m_strCriminalBackgroundEncrypted;
	}

	public function sqlCriminalBackgroundEncrypted() {
		return ( true == isset( $this->m_strCriminalBackgroundEncrypted ) ) ? '\'' . addslashes( $this->m_strCriminalBackgroundEncrypted ) . '\'' : 'NULL';
	}

	public function setHasLegalStatus( $intHasLegalStatus ) {
		$this->set( 'm_intHasLegalStatus', CStrings::strToIntDef( $intHasLegalStatus, NULL, false ) );
	}

	public function getHasLegalStatus() {
		return $this->m_intHasLegalStatus;
	}

	public function sqlHasLegalStatus() {
		return ( true == isset( $this->m_intHasLegalStatus ) ) ? ( string ) $this->m_intHasLegalStatus : '0';
	}

	public function setOnlineTestDate( $strOnlineTestDate ) {
		$this->set( 'm_strOnlineTestDate', CStrings::strTrimDef( $strOnlineTestDate, -1, NULL, true ) );
	}

	public function getOnlineTestDate() {
		return $this->m_strOnlineTestDate;
	}

	public function sqlOnlineTestDate() {
		return ( true == isset( $this->m_strOnlineTestDate ) ) ? '\'' . $this->m_strOnlineTestDate . '\'' : 'NULL';
	}

	public function setJoiningDate( $strJoiningDate ) {
		$this->set( 'm_strJoiningDate', CStrings::strTrimDef( $strJoiningDate, -1, NULL, true ) );
	}

	public function getJoiningDate() {
		return $this->m_strJoiningDate;
	}

	public function sqlJoiningDate() {
		return ( true == isset( $this->m_strJoiningDate ) ) ? '\'' . $this->m_strJoiningDate . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_application_id, interviewer1_employee_id, interviewer2_employee_id, office_desk_id, ps_document_id, review_cycle, base_wage_encrypted, bonus_amount_encrypted, bonus_description_encrypted, relocation_allowance_encrypted, pto_days, equipment_software, offer_note, case_for_hiring, ps_familiarity_description, ps_conversation_description, ps_application_history, legal_agreement_description, reason_for_leaving, experience_year, experience_month, work_experience, position_applied_for, current_company, current_location, is_ready_to_relocate, current_annual_compensation_encrypted, expected_annual_compensation_encrypted, negotiated_annual_compensation_encrypted, expected_employment_days, skill_sets, interest_description, criminal_background_encrypted, has_legal_status, online_test_date, joining_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
				  $strId . ', ' .
				  $this->sqlEmployeeApplicationId() . ', ' .
				  $this->sqlInterviewer1EmployeeId() . ', ' .
				  $this->sqlInterviewer2EmployeeId() . ', ' .
				  $this->sqlOfficeDeskId() . ', ' .
				  $this->sqlPsDocumentId() . ', ' .
				  $this->sqlReviewCycle() . ', ' .
				  $this->sqlBaseWageEncrypted() . ', ' .
				  $this->sqlBonusAmountEncrypted() . ', ' .
				  $this->sqlBonusDescriptionEncrypted() . ', ' .
				  $this->sqlRelocationAllowanceEncrypted() . ', ' .
				  $this->sqlPtoDays() . ', ' .
				  $this->sqlEquipmentSoftware() . ', ' .
				  $this->sqlOfferNote() . ', ' .
				  $this->sqlCaseForHiring() . ', ' .
				  $this->sqlPsFamiliarityDescription() . ', ' .
				  $this->sqlPsConversationDescription() . ', ' .
				  $this->sqlPsApplicationHistory() . ', ' .
				  $this->sqlLegalAgreementDescription() . ', ' .
				  $this->sqlReasonForLeaving() . ', ' .
				  $this->sqlExperienceYear() . ', ' .
				  $this->sqlExperienceMonth() . ', ' .
				  $this->sqlWorkExperience() . ', ' .
				  $this->sqlPositionAppliedFor() . ', ' .
				  $this->sqlCurrentCompany() . ', ' .
				  $this->sqlCurrentLocation() . ', ' .
				  $this->sqlIsReadyToRelocate() . ', ' .
				  $this->sqlCurrentAnnualCompensationEncrypted() . ', ' .
				  $this->sqlExpectedAnnualCompensationEncrypted() . ', ' .
				  $this->sqlNegotiatedAnnualCompensationEncrypted() . ', ' .
				  $this->sqlExpectedEmploymentDays() . ', ' .
				  $this->sqlSkillSets() . ', ' .
				  $this->sqlInterestDescription() . ', ' .
				  $this->sqlCriminalBackgroundEncrypted() . ', ' .
				  $this->sqlHasLegalStatus() . ', ' .
				  $this->sqlOnlineTestDate() . ', ' .
				  $this->sqlJoiningDate() . ', ' .
				  ( int ) $intCurrentUserId . ', ' .
				  $this->sqlUpdatedOn() . ', ' .
				  ( int ) $intCurrentUserId . ', ' .
				  $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interviewer1_employee_id = ' . $this->sqlInterviewer1EmployeeId() . ','; } elseif( true == array_key_exists( 'Interviewer1EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' interviewer1_employee_id = ' . $this->sqlInterviewer1EmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interviewer2_employee_id = ' . $this->sqlInterviewer2EmployeeId() . ','; } elseif( true == array_key_exists( 'Interviewer2EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' interviewer2_employee_id = ' . $this->sqlInterviewer2EmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_desk_id = ' . $this->sqlOfficeDeskId() . ','; } elseif( true == array_key_exists( 'OfficeDeskId', $this->getChangedColumns() ) ) { $strSql .= ' office_desk_id = ' . $this->sqlOfficeDeskId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; } elseif( true == array_key_exists( 'PsDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' review_cycle = ' . $this->sqlReviewCycle() . ','; } elseif( true == array_key_exists( 'ReviewCycle', $this->getChangedColumns() ) ) { $strSql .= ' review_cycle = ' . $this->sqlReviewCycle() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_wage_encrypted = ' . $this->sqlBaseWageEncrypted() . ','; } elseif( true == array_key_exists( 'BaseWageEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' base_wage_encrypted = ' . $this->sqlBaseWageEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bonus_amount_encrypted = ' . $this->sqlBonusAmountEncrypted() . ','; } elseif( true == array_key_exists( 'BonusAmountEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bonus_amount_encrypted = ' . $this->sqlBonusAmountEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bonus_description_encrypted = ' . $this->sqlBonusDescriptionEncrypted() . ','; } elseif( true == array_key_exists( 'BonusDescriptionEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bonus_description_encrypted = ' . $this->sqlBonusDescriptionEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' relocation_allowance_encrypted = ' . $this->sqlRelocationAllowanceEncrypted() . ','; } elseif( true == array_key_exists( 'RelocationAllowanceEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' relocation_allowance_encrypted = ' . $this->sqlRelocationAllowanceEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' pto_days = ' . $this->sqlPtoDays() . ','; } elseif( true == array_key_exists( 'PtoDays', $this->getChangedColumns() ) ) { $strSql .= ' pto_days = ' . $this->sqlPtoDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' equipment_software = ' . $this->sqlEquipmentSoftware() . ','; } elseif( true == array_key_exists( 'EquipmentSoftware', $this->getChangedColumns() ) ) { $strSql .= ' equipment_software = ' . $this->sqlEquipmentSoftware() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' offer_note = ' . $this->sqlOfferNote() . ','; } elseif( true == array_key_exists( 'OfferNote', $this->getChangedColumns() ) ) { $strSql .= ' offer_note = ' . $this->sqlOfferNote() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' case_for_hiring = ' . $this->sqlCaseForHiring() . ','; } elseif( true == array_key_exists( 'CaseForHiring', $this->getChangedColumns() ) ) { $strSql .= ' case_for_hiring = ' . $this->sqlCaseForHiring() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_familiarity_description = ' . $this->sqlPsFamiliarityDescription() . ','; } elseif( true == array_key_exists( 'PsFamiliarityDescription', $this->getChangedColumns() ) ) { $strSql .= ' ps_familiarity_description = ' . $this->sqlPsFamiliarityDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_conversation_description = ' . $this->sqlPsConversationDescription() . ','; } elseif( true == array_key_exists( 'PsConversationDescription', $this->getChangedColumns() ) ) { $strSql .= ' ps_conversation_description = ' . $this->sqlPsConversationDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_application_history = ' . $this->sqlPsApplicationHistory() . ','; } elseif( true == array_key_exists( 'PsApplicationHistory', $this->getChangedColumns() ) ) { $strSql .= ' ps_application_history = ' . $this->sqlPsApplicationHistory() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' legal_agreement_description = ' . $this->sqlLegalAgreementDescription() . ','; } elseif( true == array_key_exists( 'LegalAgreementDescription', $this->getChangedColumns() ) ) { $strSql .= ' legal_agreement_description = ' . $this->sqlLegalAgreementDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reason_for_leaving = ' . $this->sqlReasonForLeaving() . ','; } elseif( true == array_key_exists( 'ReasonForLeaving', $this->getChangedColumns() ) ) { $strSql .= ' reason_for_leaving = ' . $this->sqlReasonForLeaving() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' experience_year = ' . $this->sqlExperienceYear() . ','; } elseif( true == array_key_exists( 'ExperienceYear', $this->getChangedColumns() ) ) { $strSql .= ' experience_year = ' . $this->sqlExperienceYear() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' experience_month = ' . $this->sqlExperienceMonth() . ','; } elseif( true == array_key_exists( 'ExperienceMonth', $this->getChangedColumns() ) ) { $strSql .= ' experience_month = ' . $this->sqlExperienceMonth() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' work_experience = ' . $this->sqlWorkExperience() . ','; } elseif( true == array_key_exists( 'WorkExperience', $this->getChangedColumns() ) ) { $strSql .= ' work_experience = ' . $this->sqlWorkExperience() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' position_applied_for = ' . $this->sqlPositionAppliedFor() . ','; } elseif( true == array_key_exists( 'PositionAppliedFor', $this->getChangedColumns() ) ) { $strSql .= ' position_applied_for = ' . $this->sqlPositionAppliedFor() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_company = ' . $this->sqlCurrentCompany() . ','; } elseif( true == array_key_exists( 'CurrentCompany', $this->getChangedColumns() ) ) { $strSql .= ' current_company = ' . $this->sqlCurrentCompany() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_location = ' . $this->sqlCurrentLocation() . ','; } elseif( true == array_key_exists( 'CurrentLocation', $this->getChangedColumns() ) ) { $strSql .= ' current_location = ' . $this->sqlCurrentLocation() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_ready_to_relocate = ' . $this->sqlIsReadyToRelocate() . ','; } elseif( true == array_key_exists( 'IsReadyToRelocate', $this->getChangedColumns() ) ) { $strSql .= ' is_ready_to_relocate = ' . $this->sqlIsReadyToRelocate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_annual_compensation_encrypted = ' . $this->sqlCurrentAnnualCompensationEncrypted() . ','; } elseif( true == array_key_exists( 'CurrentAnnualCompensationEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' current_annual_compensation_encrypted = ' . $this->sqlCurrentAnnualCompensationEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_annual_compensation_encrypted = ' . $this->sqlExpectedAnnualCompensationEncrypted() . ','; } elseif( true == array_key_exists( 'ExpectedAnnualCompensationEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' expected_annual_compensation_encrypted = ' . $this->sqlExpectedAnnualCompensationEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' negotiated_annual_compensation_encrypted = ' . $this->sqlNegotiatedAnnualCompensationEncrypted() . ','; } elseif( true == array_key_exists( 'NegotiatedAnnualCompensationEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' negotiated_annual_compensation_encrypted = ' . $this->sqlNegotiatedAnnualCompensationEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_employment_days = ' . $this->sqlExpectedEmploymentDays() . ','; } elseif( true == array_key_exists( 'ExpectedEmploymentDays', $this->getChangedColumns() ) ) { $strSql .= ' expected_employment_days = ' . $this->sqlExpectedEmploymentDays() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' skill_sets = ' . $this->sqlSkillSets() . ','; } elseif( true == array_key_exists( 'SkillSets', $this->getChangedColumns() ) ) { $strSql .= ' skill_sets = ' . $this->sqlSkillSets() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' interest_description = ' . $this->sqlInterestDescription() . ','; } elseif( true == array_key_exists( 'InterestDescription', $this->getChangedColumns() ) ) { $strSql .= ' interest_description = ' . $this->sqlInterestDescription() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' criminal_background_encrypted = ' . $this->sqlCriminalBackgroundEncrypted() . ','; } elseif( true == array_key_exists( 'CriminalBackgroundEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' criminal_background_encrypted = ' . $this->sqlCriminalBackgroundEncrypted() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' has_legal_status = ' . $this->sqlHasLegalStatus() . ','; } elseif( true == array_key_exists( 'HasLegalStatus', $this->getChangedColumns() ) ) { $strSql .= ' has_legal_status = ' . $this->sqlHasLegalStatus() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' online_test_date = ' . $this->sqlOnlineTestDate() . ','; } elseif( true == array_key_exists( 'OnlineTestDate', $this->getChangedColumns() ) ) { $strSql .= ' online_test_date = ' . $this->sqlOnlineTestDate() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' joining_date = ' . $this->sqlJoiningDate() . ','; } elseif( true == array_key_exists( 'JoiningDate', $this->getChangedColumns() ) ) { $strSql .= ' joining_date = ' . $this->sqlJoiningDate() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_application_id' => $this->getEmployeeApplicationId(),
			'interviewer1_employee_id' => $this->getInterviewer1EmployeeId(),
			'interviewer2_employee_id' => $this->getInterviewer2EmployeeId(),
			'office_desk_id' => $this->getOfficeDeskId(),
			'ps_document_id' => $this->getPsDocumentId(),
			'review_cycle' => $this->getReviewCycle(),
			'base_wage_encrypted' => $this->getBaseWageEncrypted(),
			'bonus_amount_encrypted' => $this->getBonusAmountEncrypted(),
			'bonus_description_encrypted' => $this->getBonusDescriptionEncrypted(),
			'relocation_allowance_encrypted' => $this->getRelocationAllowanceEncrypted(),
			'pto_days' => $this->getPtoDays(),
			'equipment_software' => $this->getEquipmentSoftware(),
			'offer_note' => $this->getOfferNote(),
			'case_for_hiring' => $this->getCaseForHiring(),
			'ps_familiarity_description' => $this->getPsFamiliarityDescription(),
			'ps_conversation_description' => $this->getPsConversationDescription(),
			'ps_application_history' => $this->getPsApplicationHistory(),
			'legal_agreement_description' => $this->getLegalAgreementDescription(),
			'reason_for_leaving' => $this->getReasonForLeaving(),
			'experience_year' => $this->getExperienceYear(),
			'experience_month' => $this->getExperienceMonth(),
			'work_experience' => $this->getWorkExperience(),
			'position_applied_for' => $this->getPositionAppliedFor(),
			'current_company' => $this->getCurrentCompany(),
			'current_location' => $this->getCurrentLocation(),
			'is_ready_to_relocate' => $this->getIsReadyToRelocate(),
			'current_annual_compensation_encrypted' => $this->getCurrentAnnualCompensationEncrypted(),
			'expected_annual_compensation_encrypted' => $this->getExpectedAnnualCompensationEncrypted(),
			'negotiated_annual_compensation_encrypted' => $this->getNegotiatedAnnualCompensationEncrypted(),
			'expected_employment_days' => $this->getExpectedEmploymentDays(),
			'skill_sets' => $this->getSkillSets(),
			'interest_description' => $this->getInterestDescription(),
			'criminal_background_encrypted' => $this->getCriminalBackgroundEncrypted(),
			'has_legal_status' => $this->getHasLegalStatus(),
			'online_test_date' => $this->getOnlineTestDate(),
			'joining_date' => $this->getJoiningDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>