<?php

class CBaseTaskAssignment extends CEosSingularBase {

	const TABLE_NAME = 'public.task_assignments';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intTaskId;
	protected $m_intTaskStatusId;
	protected $m_intTaskPriorityId;
	protected $m_intTaskReleaseId;
	protected $m_intTaskTypeId;
	protected $m_intUserId;
	protected $m_intGroupId;
	protected $m_intSdmEmployeeId;
	protected $m_intMinutesAssigned;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_fltDeveloperStoryPoints;
	protected $m_fltQaStoryPoints;
	protected $m_intIsFlagged;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsFlagged = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['task_status_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskStatusId', trim( $arrValues['task_status_id'] ) ); elseif( isset( $arrValues['task_status_id'] ) ) $this->setTaskStatusId( $arrValues['task_status_id'] );
		if( isset( $arrValues['task_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskPriorityId', trim( $arrValues['task_priority_id'] ) ); elseif( isset( $arrValues['task_priority_id'] ) ) $this->setTaskPriorityId( $arrValues['task_priority_id'] );
		if( isset( $arrValues['task_release_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskReleaseId', trim( $arrValues['task_release_id'] ) ); elseif( isset( $arrValues['task_release_id'] ) ) $this->setTaskReleaseId( $arrValues['task_release_id'] );
		if( isset( $arrValues['task_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskTypeId', trim( $arrValues['task_type_id'] ) ); elseif( isset( $arrValues['task_type_id'] ) ) $this->setTaskTypeId( $arrValues['task_type_id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['group_id'] ) && $boolDirectSet ) $this->set( 'm_intGroupId', trim( $arrValues['group_id'] ) ); elseif( isset( $arrValues['group_id'] ) ) $this->setGroupId( $arrValues['group_id'] );
		if( isset( $arrValues['sdm_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSdmEmployeeId', trim( $arrValues['sdm_employee_id'] ) ); elseif( isset( $arrValues['sdm_employee_id'] ) ) $this->setSdmEmployeeId( $arrValues['sdm_employee_id'] );
		if( isset( $arrValues['minutes_assigned'] ) && $boolDirectSet ) $this->set( 'm_intMinutesAssigned', trim( $arrValues['minutes_assigned'] ) ); elseif( isset( $arrValues['minutes_assigned'] ) ) $this->setMinutesAssigned( $arrValues['minutes_assigned'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['developer_story_points'] ) && $boolDirectSet ) $this->set( 'm_fltDeveloperStoryPoints', trim( $arrValues['developer_story_points'] ) ); elseif( isset( $arrValues['developer_story_points'] ) ) $this->setDeveloperStoryPoints( $arrValues['developer_story_points'] );
		if( isset( $arrValues['qa_story_points'] ) && $boolDirectSet ) $this->set( 'm_fltQaStoryPoints', trim( $arrValues['qa_story_points'] ) ); elseif( isset( $arrValues['qa_story_points'] ) ) $this->setQaStoryPoints( $arrValues['qa_story_points'] );
		if( isset( $arrValues['is_flagged'] ) && $boolDirectSet ) $this->set( 'm_intIsFlagged', trim( $arrValues['is_flagged'] ) ); elseif( isset( $arrValues['is_flagged'] ) ) $this->setIsFlagged( $arrValues['is_flagged'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setTaskStatusId( $intTaskStatusId ) {
		$this->set( 'm_intTaskStatusId', CStrings::strToIntDef( $intTaskStatusId, NULL, false ) );
	}

	public function getTaskStatusId() {
		return $this->m_intTaskStatusId;
	}

	public function sqlTaskStatusId() {
		return ( true == isset( $this->m_intTaskStatusId ) ) ? ( string ) $this->m_intTaskStatusId : 'NULL';
	}

	public function setTaskPriorityId( $intTaskPriorityId ) {
		$this->set( 'm_intTaskPriorityId', CStrings::strToIntDef( $intTaskPriorityId, NULL, false ) );
	}

	public function getTaskPriorityId() {
		return $this->m_intTaskPriorityId;
	}

	public function sqlTaskPriorityId() {
		return ( true == isset( $this->m_intTaskPriorityId ) ) ? ( string ) $this->m_intTaskPriorityId : 'NULL';
	}

	public function setTaskReleaseId( $intTaskReleaseId ) {
		$this->set( 'm_intTaskReleaseId', CStrings::strToIntDef( $intTaskReleaseId, NULL, false ) );
	}

	public function getTaskReleaseId() {
		return $this->m_intTaskReleaseId;
	}

	public function sqlTaskReleaseId() {
		return ( true == isset( $this->m_intTaskReleaseId ) ) ? ( string ) $this->m_intTaskReleaseId : 'NULL';
	}

	public function setTaskTypeId( $intTaskTypeId ) {
		$this->set( 'm_intTaskTypeId', CStrings::strToIntDef( $intTaskTypeId, NULL, false ) );
	}

	public function getTaskTypeId() {
		return $this->m_intTaskTypeId;
	}

	public function sqlTaskTypeId() {
		return ( true == isset( $this->m_intTaskTypeId ) ) ? ( string ) $this->m_intTaskTypeId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setGroupId( $intGroupId ) {
		$this->set( 'm_intGroupId', CStrings::strToIntDef( $intGroupId, NULL, false ) );
	}

	public function getGroupId() {
		return $this->m_intGroupId;
	}

	public function sqlGroupId() {
		return ( true == isset( $this->m_intGroupId ) ) ? ( string ) $this->m_intGroupId : 'NULL';
	}

	public function setSdmEmployeeId( $intSdmEmployeeId ) {
		$this->set( 'm_intSdmEmployeeId', CStrings::strToIntDef( $intSdmEmployeeId, NULL, false ) );
	}

	public function getSdmEmployeeId() {
		return $this->m_intSdmEmployeeId;
	}

	public function sqlSdmEmployeeId() {
		return ( true == isset( $this->m_intSdmEmployeeId ) ) ? ( string ) $this->m_intSdmEmployeeId : 'NULL';
	}

	public function setMinutesAssigned( $intMinutesAssigned ) {
		$this->set( 'm_intMinutesAssigned', CStrings::strToIntDef( $intMinutesAssigned, NULL, false ) );
	}

	public function getMinutesAssigned() {
		return $this->m_intMinutesAssigned;
	}

	public function sqlMinutesAssigned() {
		return ( true == isset( $this->m_intMinutesAssigned ) ) ? ( string ) $this->m_intMinutesAssigned : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setDeveloperStoryPoints( $fltDeveloperStoryPoints ) {
		$this->set( 'm_fltDeveloperStoryPoints', CStrings::strToFloatDef( $fltDeveloperStoryPoints, NULL, false, 2 ) );
	}

	public function getDeveloperStoryPoints() {
		return $this->m_fltDeveloperStoryPoints;
	}

	public function sqlDeveloperStoryPoints() {
		return ( true == isset( $this->m_fltDeveloperStoryPoints ) ) ? ( string ) $this->m_fltDeveloperStoryPoints : 'NULL';
	}

	public function setQaStoryPoints( $fltQaStoryPoints ) {
		$this->set( 'm_fltQaStoryPoints', CStrings::strToFloatDef( $fltQaStoryPoints, NULL, false, 2 ) );
	}

	public function getQaStoryPoints() {
		return $this->m_fltQaStoryPoints;
	}

	public function sqlQaStoryPoints() {
		return ( true == isset( $this->m_fltQaStoryPoints ) ) ? ( string ) $this->m_fltQaStoryPoints : 'NULL';
	}

	public function setIsFlagged( $intIsFlagged ) {
		$this->set( 'm_intIsFlagged', CStrings::strToIntDef( $intIsFlagged, NULL, false ) );
	}

	public function getIsFlagged() {
		return $this->m_intIsFlagged;
	}

	public function sqlIsFlagged() {
		return ( true == isset( $this->m_intIsFlagged ) ) ? ( string ) $this->m_intIsFlagged : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, task_id, task_status_id, task_priority_id, task_release_id, task_type_id, user_id, group_id, sdm_employee_id, minutes_assigned, ps_product_id, ps_product_option_id, developer_story_points, qa_story_points, is_flagged, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlTaskStatusId() . ', ' .
 						$this->sqlTaskPriorityId() . ', ' .
 						$this->sqlTaskReleaseId() . ', ' .
 						$this->sqlTaskTypeId() . ', ' .
 						$this->sqlUserId() . ', ' .
 						$this->sqlGroupId() . ', ' .
 						$this->sqlSdmEmployeeId() . ', ' .
 						$this->sqlMinutesAssigned() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlPsProductOptionId() . ', ' .
 						$this->sqlDeveloperStoryPoints() . ', ' .
 						$this->sqlQaStoryPoints() . ', ' .
 						$this->sqlIsFlagged() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_status_id = ' . $this->sqlTaskStatusId() . ','; } elseif( true == array_key_exists( 'TaskStatusId', $this->getChangedColumns() ) ) { $strSql .= ' task_status_id = ' . $this->sqlTaskStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_priority_id = ' . $this->sqlTaskPriorityId() . ','; } elseif( true == array_key_exists( 'TaskPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' task_priority_id = ' . $this->sqlTaskPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_release_id = ' . $this->sqlTaskReleaseId() . ','; } elseif( true == array_key_exists( 'TaskReleaseId', $this->getChangedColumns() ) ) { $strSql .= ' task_release_id = ' . $this->sqlTaskReleaseId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_type_id = ' . $this->sqlTaskTypeId() . ','; } elseif( true == array_key_exists( 'TaskTypeId', $this->getChangedColumns() ) ) { $strSql .= ' task_type_id = ' . $this->sqlTaskTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' group_id = ' . $this->sqlGroupId() . ','; } elseif( true == array_key_exists( 'GroupId', $this->getChangedColumns() ) ) { $strSql .= ' group_id = ' . $this->sqlGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sdm_employee_id = ' . $this->sqlSdmEmployeeId() . ','; } elseif( true == array_key_exists( 'SdmEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sdm_employee_id = ' . $this->sqlSdmEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minutes_assigned = ' . $this->sqlMinutesAssigned() . ','; } elseif( true == array_key_exists( 'MinutesAssigned', $this->getChangedColumns() ) ) { $strSql .= ' minutes_assigned = ' . $this->sqlMinutesAssigned() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; } elseif( true == array_key_exists( 'PsProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' developer_story_points = ' . $this->sqlDeveloperStoryPoints() . ','; } elseif( true == array_key_exists( 'DeveloperStoryPoints', $this->getChangedColumns() ) ) { $strSql .= ' developer_story_points = ' . $this->sqlDeveloperStoryPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_story_points = ' . $this->sqlQaStoryPoints() . ','; } elseif( true == array_key_exists( 'QaStoryPoints', $this->getChangedColumns() ) ) { $strSql .= ' qa_story_points = ' . $this->sqlQaStoryPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_flagged = ' . $this->sqlIsFlagged() . ','; } elseif( true == array_key_exists( 'IsFlagged', $this->getChangedColumns() ) ) { $strSql .= ' is_flagged = ' . $this->sqlIsFlagged() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'task_id' => $this->getTaskId(),
			'task_status_id' => $this->getTaskStatusId(),
			'task_priority_id' => $this->getTaskPriorityId(),
			'task_release_id' => $this->getTaskReleaseId(),
			'task_type_id' => $this->getTaskTypeId(),
			'user_id' => $this->getUserId(),
			'group_id' => $this->getGroupId(),
			'sdm_employee_id' => $this->getSdmEmployeeId(),
			'minutes_assigned' => $this->getMinutesAssigned(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'developer_story_points' => $this->getDeveloperStoryPoints(),
			'qa_story_points' => $this->getQaStoryPoints(),
			'is_flagged' => $this->getIsFlagged(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>