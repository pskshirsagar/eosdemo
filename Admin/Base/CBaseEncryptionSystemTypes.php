<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEncryptionSystemTypes
 * Do not add any new functions to this class.
 */

class CBaseEncryptionSystemTypes extends CEosPluralBase {

	/**
	 * @return CEncryptionSystemType[]
	 */
	public static function fetchEncryptionSystemTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEncryptionSystemType', $objDatabase );
	}

	/**
	 * @return CEncryptionSystemType
	 */
	public static function fetchEncryptionSystemType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEncryptionSystemType', $objDatabase );
	}

	public static function fetchEncryptionSystemTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'encryption_system_types', $objDatabase );
	}

	public static function fetchEncryptionSystemTypeById( $intId, $objDatabase ) {
		return self::fetchEncryptionSystemType( sprintf( 'SELECT * FROM encryption_system_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>