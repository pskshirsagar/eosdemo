<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsTypes
 * Do not add any new functions to this class.
 */

class CBaseStatsTypes extends CEosPluralBase {

	/**
	 * @return CStatsType[]
	 */
	public static function fetchStatsTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStatsType', $objDatabase );
	}

	/**
	 * @return CStatsType
	 */
	public static function fetchStatsType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStatsType', $objDatabase );
	}

	public static function fetchStatsTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'stats_types', $objDatabase );
	}

	public static function fetchStatsTypeById( $intId, $objDatabase ) {
		return self::fetchStatsType( sprintf( 'SELECT * FROM stats_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>