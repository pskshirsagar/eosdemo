<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportSqlSnippets
 * Do not add any new functions to this class.
 */

class CBaseReportSqlSnippets extends CEosPluralBase {

	/**
	 * @return CReportSqlSnippet[]
	 */
	public static function fetchReportSqlSnippets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportSqlSnippet::class, $objDatabase );
	}

	/**
	 * @return CReportSqlSnippet
	 */
	public static function fetchReportSqlSnippet( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportSqlSnippet::class, $objDatabase );
	}

	public static function fetchReportSqlSnippetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_sql_snippets', $objDatabase );
	}

	public static function fetchReportSqlSnippetById( $intId, $objDatabase ) {
		return self::fetchReportSqlSnippet( sprintf( 'SELECT * FROM report_sql_snippets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>