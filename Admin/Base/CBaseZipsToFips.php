<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CZipsToFips
 * Do not add any new functions to this class.
 */

class CBaseZipsToFips extends CEosPluralBase {

	/**
	 * @return CZipsToFip[]
	 */
	public static function fetchZipsToFips( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CZipsToFip::class, $objDatabase );
	}

	/**
	 * @return CZipsToFip
	 */
	public static function fetchZipsToFip( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CZipsToFip::class, $objDatabase );
	}

	public static function fetchZipsToFipCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'zips_to_fips', $objDatabase );
	}

	public static function fetchZipsToFipById( $intId, $objDatabase ) {
		return self::fetchZipsToFip( sprintf( 'SELECT * FROM zips_to_fips WHERE id = %d', $intId ), $objDatabase );
	}

}
?>