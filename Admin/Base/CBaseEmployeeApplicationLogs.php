<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationLogs
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationLogs extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationLog[]
	 */
	public static function fetchEmployeeApplicationLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationLog', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationLog
	 */
	public static function fetchEmployeeApplicationLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationLog', $objDatabase );
	}

	public static function fetchEmployeeApplicationLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_logs', $objDatabase );
	}

	public static function fetchEmployeeApplicationLogById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationLog( sprintf( 'SELECT * FROM employee_application_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>