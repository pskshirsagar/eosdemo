<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestLevelTypes
 * Do not add any new functions to this class.
 */

class CBaseTestLevelTypes extends CEosPluralBase {

	/**
	 * @return CTestLevelType[]
	 */
	public static function fetchTestLevelTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestLevelType', $objDatabase );
	}

	/**
	 * @return CTestLevelType
	 */
	public static function fetchTestLevelType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestLevelType', $objDatabase );
	}

	public static function fetchTestLevelTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_level_types', $objDatabase );
	}

	public static function fetchTestLevelTypeById( $intId, $objDatabase ) {
		return self::fetchTestLevelType( sprintf( 'SELECT * FROM test_level_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>