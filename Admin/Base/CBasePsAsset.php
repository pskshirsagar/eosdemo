<?php

class CBasePsAsset extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.ps_assets';

	protected $m_intId;
	protected $m_intPsAssetStatusTypeId;
	protected $m_intOfficeId;
	protected $m_intPsAssetTypeId;
	protected $m_intPsAssetBrandId;
	protected $m_intPsAssetModelId;
	protected $m_intPurchaseRequestId;
	protected $m_intHardwareTypeId;
	protected $m_intMachineTypeId;
	protected $m_intSwitchTypeId;
	protected $m_intLocationId;
	protected $m_intPsDocumentId;
	protected $m_strInvoiceNumber;
	protected $m_intCpuCoreNum;
	protected $m_strRamDescription;
	protected $m_strHardDiskDescription;
	protected $m_strPowerRequirementDescription;
	protected $m_strDeskNumberDescription;
	protected $m_strIpAddress;
	protected $m_strIpSubnet;
	protected $m_strAssetTag;
	protected $m_strServiceTag;
	protected $m_strMacTag;
	protected $m_strProcessorDescription;
	protected $m_strServerRoleDescription;
	protected $m_strSpecification;
	protected $m_strHostName;
	protected $m_strCountryCode;
	protected $m_strPurchaseDate;
	protected $m_strExpiryDate;
	protected $m_fltPurchasedCost;
	protected $m_intDepriciationRate;
	protected $m_strLicenceNo;
	protected $m_intUserLimit;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intPsAssetPurchaseSourcesId;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intUpdatedBy = '1';
		$this->m_intCreatedBy = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_asset_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsAssetStatusTypeId', trim( $arrValues['ps_asset_status_type_id'] ) ); elseif( isset( $arrValues['ps_asset_status_type_id'] ) ) $this->setPsAssetStatusTypeId( $arrValues['ps_asset_status_type_id'] );
		if( isset( $arrValues['office_id'] ) && $boolDirectSet ) $this->set( 'm_intOfficeId', trim( $arrValues['office_id'] ) ); elseif( isset( $arrValues['office_id'] ) ) $this->setOfficeId( $arrValues['office_id'] );
		if( isset( $arrValues['ps_asset_type_id'] ) && $boolDirectSet ) $this->set( 'm_intPsAssetTypeId', trim( $arrValues['ps_asset_type_id'] ) ); elseif( isset( $arrValues['ps_asset_type_id'] ) ) $this->setPsAssetTypeId( $arrValues['ps_asset_type_id'] );
		if( isset( $arrValues['ps_asset_brand_id'] ) && $boolDirectSet ) $this->set( 'm_intPsAssetBrandId', trim( $arrValues['ps_asset_brand_id'] ) ); elseif( isset( $arrValues['ps_asset_brand_id'] ) ) $this->setPsAssetBrandId( $arrValues['ps_asset_brand_id'] );
		if( isset( $arrValues['ps_asset_model_id'] ) && $boolDirectSet ) $this->set( 'm_intPsAssetModelId', trim( $arrValues['ps_asset_model_id'] ) ); elseif( isset( $arrValues['ps_asset_model_id'] ) ) $this->setPsAssetModelId( $arrValues['ps_asset_model_id'] );
		if( isset( $arrValues['purchase_request_id'] ) && $boolDirectSet ) $this->set( 'm_intPurchaseRequestId', trim( $arrValues['purchase_request_id'] ) ); elseif( isset( $arrValues['purchase_request_id'] ) ) $this->setPurchaseRequestId( $arrValues['purchase_request_id'] );
		if( isset( $arrValues['hardware_type_id'] ) && $boolDirectSet ) $this->set( 'm_intHardwareTypeId', trim( $arrValues['hardware_type_id'] ) ); elseif( isset( $arrValues['hardware_type_id'] ) ) $this->setHardwareTypeId( $arrValues['hardware_type_id'] );
		if( isset( $arrValues['machine_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMachineTypeId', trim( $arrValues['machine_type_id'] ) ); elseif( isset( $arrValues['machine_type_id'] ) ) $this->setMachineTypeId( $arrValues['machine_type_id'] );
		if( isset( $arrValues['switch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intSwitchTypeId', trim( $arrValues['switch_type_id'] ) ); elseif( isset( $arrValues['switch_type_id'] ) ) $this->setSwitchTypeId( $arrValues['switch_type_id'] );
		if( isset( $arrValues['location_id'] ) && $boolDirectSet ) $this->set( 'm_intLocationId', trim( $arrValues['location_id'] ) ); elseif( isset( $arrValues['location_id'] ) ) $this->setLocationId( $arrValues['location_id'] );
		if( isset( $arrValues['ps_document_id'] ) && $boolDirectSet ) $this->set( 'm_intPsDocumentId', trim( $arrValues['ps_document_id'] ) ); elseif( isset( $arrValues['ps_document_id'] ) ) $this->setPsDocumentId( $arrValues['ps_document_id'] );
		if( isset( $arrValues['invoice_number'] ) && $boolDirectSet ) $this->set( 'm_strInvoiceNumber', trim( $arrValues['invoice_number'] ) ); elseif( isset( $arrValues['invoice_number'] ) ) $this->setInvoiceNumber( $arrValues['invoice_number'] );
		if( isset( $arrValues['cpu_core_num'] ) && $boolDirectSet ) $this->set( 'm_intCpuCoreNum', trim( $arrValues['cpu_core_num'] ) ); elseif( isset( $arrValues['cpu_core_num'] ) ) $this->setCpuCoreNum( $arrValues['cpu_core_num'] );
		if( isset( $arrValues['ram_description'] ) && $boolDirectSet ) $this->set( 'm_strRamDescription', trim( $arrValues['ram_description'] ) ); elseif( isset( $arrValues['ram_description'] ) ) $this->setRamDescription( $arrValues['ram_description'] );
		if( isset( $arrValues['hard_disk_description'] ) && $boolDirectSet ) $this->set( 'm_strHardDiskDescription', trim( $arrValues['hard_disk_description'] ) ); elseif( isset( $arrValues['hard_disk_description'] ) ) $this->setHardDiskDescription( $arrValues['hard_disk_description'] );
		if( isset( $arrValues['power_requirement_description'] ) && $boolDirectSet ) $this->set( 'm_strPowerRequirementDescription', trim( $arrValues['power_requirement_description'] ) ); elseif( isset( $arrValues['power_requirement_description'] ) ) $this->setPowerRequirementDescription( $arrValues['power_requirement_description'] );
		if( isset( $arrValues['desk_number_description'] ) && $boolDirectSet ) $this->set( 'm_strDeskNumberDescription', trim( $arrValues['desk_number_description'] ) ); elseif( isset( $arrValues['desk_number_description'] ) ) $this->setDeskNumberDescription( $arrValues['desk_number_description'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['ip_subnet'] ) && $boolDirectSet ) $this->set( 'm_strIpSubnet', trim( $arrValues['ip_subnet'] ) ); elseif( isset( $arrValues['ip_subnet'] ) ) $this->setIpSubnet( $arrValues['ip_subnet'] );
		if( isset( $arrValues['asset_tag'] ) && $boolDirectSet ) $this->set( 'm_strAssetTag', trim( $arrValues['asset_tag'] ) ); elseif( isset( $arrValues['asset_tag'] ) ) $this->setAssetTag( $arrValues['asset_tag'] );
		if( isset( $arrValues['service_tag'] ) && $boolDirectSet ) $this->set( 'm_strServiceTag', trim( $arrValues['service_tag'] ) ); elseif( isset( $arrValues['service_tag'] ) ) $this->setServiceTag( $arrValues['service_tag'] );
		if( isset( $arrValues['mac_tag'] ) && $boolDirectSet ) $this->set( 'm_strMacTag', trim( $arrValues['mac_tag'] ) ); elseif( isset( $arrValues['mac_tag'] ) ) $this->setMacTag( $arrValues['mac_tag'] );
		if( isset( $arrValues['processor_description'] ) && $boolDirectSet ) $this->set( 'm_strProcessorDescription', trim( $arrValues['processor_description'] ) ); elseif( isset( $arrValues['processor_description'] ) ) $this->setProcessorDescription( $arrValues['processor_description'] );
		if( isset( $arrValues['server_role_description'] ) && $boolDirectSet ) $this->set( 'm_strServerRoleDescription', trim( $arrValues['server_role_description'] ) ); elseif( isset( $arrValues['server_role_description'] ) ) $this->setServerRoleDescription( $arrValues['server_role_description'] );
		if( isset( $arrValues['specification'] ) && $boolDirectSet ) $this->set( 'm_strSpecification', trim( $arrValues['specification'] ) ); elseif( isset( $arrValues['specification'] ) ) $this->setSpecification( $arrValues['specification'] );
		if( isset( $arrValues['host_name'] ) && $boolDirectSet ) $this->set( 'm_strHostName', trim( $arrValues['host_name'] ) ); elseif( isset( $arrValues['host_name'] ) ) $this->setHostName( $arrValues['host_name'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( $arrValues['country_code'] ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( $arrValues['country_code'] );
		if( isset( $arrValues['purchase_date'] ) && $boolDirectSet ) $this->set( 'm_strPurchaseDate', trim( $arrValues['purchase_date'] ) ); elseif( isset( $arrValues['purchase_date'] ) ) $this->setPurchaseDate( $arrValues['purchase_date'] );
		if( isset( $arrValues['expiry_date'] ) && $boolDirectSet ) $this->set( 'm_strExpiryDate', trim( $arrValues['expiry_date'] ) ); elseif( isset( $arrValues['expiry_date'] ) ) $this->setExpiryDate( $arrValues['expiry_date'] );
		if( isset( $arrValues['purchased_cost'] ) && $boolDirectSet ) $this->set( 'm_fltPurchasedCost', trim( $arrValues['purchased_cost'] ) ); elseif( isset( $arrValues['purchased_cost'] ) ) $this->setPurchasedCost( $arrValues['purchased_cost'] );
		if( isset( $arrValues['depriciation_rate'] ) && $boolDirectSet ) $this->set( 'm_intDepriciationRate', trim( $arrValues['depriciation_rate'] ) ); elseif( isset( $arrValues['depriciation_rate'] ) ) $this->setDepriciationRate( $arrValues['depriciation_rate'] );
		if( isset( $arrValues['licence_no'] ) && $boolDirectSet ) $this->set( 'm_strLicenceNo', trim( $arrValues['licence_no'] ) ); elseif( isset( $arrValues['licence_no'] ) ) $this->setLicenceNo( $arrValues['licence_no'] );
		if( isset( $arrValues['user_limit'] ) && $boolDirectSet ) $this->set( 'm_intUserLimit', trim( $arrValues['user_limit'] ) ); elseif( isset( $arrValues['user_limit'] ) ) $this->setUserLimit( $arrValues['user_limit'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['ps_asset_purchase_sources_id'] ) && $boolDirectSet ) $this->set( 'm_intPsAssetPurchaseSourcesId', trim( $arrValues['ps_asset_purchase_sources_id'] ) ); elseif( isset( $arrValues['ps_asset_purchase_sources_id'] ) ) $this->setPsAssetPurchaseSourcesId( $arrValues['ps_asset_purchase_sources_id'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsAssetStatusTypeId( $intPsAssetStatusTypeId ) {
		$this->set( 'm_intPsAssetStatusTypeId', CStrings::strToIntDef( $intPsAssetStatusTypeId, NULL, false ) );
	}

	public function getPsAssetStatusTypeId() {
		return $this->m_intPsAssetStatusTypeId;
	}

	public function sqlPsAssetStatusTypeId() {
		return ( true == isset( $this->m_intPsAssetStatusTypeId ) ) ? ( string ) $this->m_intPsAssetStatusTypeId : 'NULL';
	}

	public function setOfficeId( $intOfficeId ) {
		$this->set( 'm_intOfficeId', CStrings::strToIntDef( $intOfficeId, NULL, false ) );
	}

	public function getOfficeId() {
		return $this->m_intOfficeId;
	}

	public function sqlOfficeId() {
		return ( true == isset( $this->m_intOfficeId ) ) ? ( string ) $this->m_intOfficeId : 'NULL';
	}

	public function setPsAssetTypeId( $intPsAssetTypeId ) {
		$this->set( 'm_intPsAssetTypeId', CStrings::strToIntDef( $intPsAssetTypeId, NULL, false ) );
	}

	public function getPsAssetTypeId() {
		return $this->m_intPsAssetTypeId;
	}

	public function sqlPsAssetTypeId() {
		return ( true == isset( $this->m_intPsAssetTypeId ) ) ? ( string ) $this->m_intPsAssetTypeId : 'NULL';
	}

	public function setPsAssetBrandId( $intPsAssetBrandId ) {
		$this->set( 'm_intPsAssetBrandId', CStrings::strToIntDef( $intPsAssetBrandId, NULL, false ) );
	}

	public function getPsAssetBrandId() {
		return $this->m_intPsAssetBrandId;
	}

	public function sqlPsAssetBrandId() {
		return ( true == isset( $this->m_intPsAssetBrandId ) ) ? ( string ) $this->m_intPsAssetBrandId : 'NULL';
	}

	public function setPsAssetModelId( $intPsAssetModelId ) {
		$this->set( 'm_intPsAssetModelId', CStrings::strToIntDef( $intPsAssetModelId, NULL, false ) );
	}

	public function getPsAssetModelId() {
		return $this->m_intPsAssetModelId;
	}

	public function sqlPsAssetModelId() {
		return ( true == isset( $this->m_intPsAssetModelId ) ) ? ( string ) $this->m_intPsAssetModelId : 'NULL';
	}

	public function setPurchaseRequestId( $intPurchaseRequestId ) {
		$this->set( 'm_intPurchaseRequestId', CStrings::strToIntDef( $intPurchaseRequestId, NULL, false ) );
	}

	public function getPurchaseRequestId() {
		return $this->m_intPurchaseRequestId;
	}

	public function sqlPurchaseRequestId() {
		return ( true == isset( $this->m_intPurchaseRequestId ) ) ? ( string ) $this->m_intPurchaseRequestId : 'NULL';
	}

	public function setHardwareTypeId( $intHardwareTypeId ) {
		$this->set( 'm_intHardwareTypeId', CStrings::strToIntDef( $intHardwareTypeId, NULL, false ) );
	}

	public function getHardwareTypeId() {
		return $this->m_intHardwareTypeId;
	}

	public function sqlHardwareTypeId() {
		return ( true == isset( $this->m_intHardwareTypeId ) ) ? ( string ) $this->m_intHardwareTypeId : 'NULL';
	}

	public function setMachineTypeId( $intMachineTypeId ) {
		$this->set( 'm_intMachineTypeId', CStrings::strToIntDef( $intMachineTypeId, NULL, false ) );
	}

	public function getMachineTypeId() {
		return $this->m_intMachineTypeId;
	}

	public function sqlMachineTypeId() {
		return ( true == isset( $this->m_intMachineTypeId ) ) ? ( string ) $this->m_intMachineTypeId : 'NULL';
	}

	public function setSwitchTypeId( $intSwitchTypeId ) {
		$this->set( 'm_intSwitchTypeId', CStrings::strToIntDef( $intSwitchTypeId, NULL, false ) );
	}

	public function getSwitchTypeId() {
		return $this->m_intSwitchTypeId;
	}

	public function sqlSwitchTypeId() {
		return ( true == isset( $this->m_intSwitchTypeId ) ) ? ( string ) $this->m_intSwitchTypeId : 'NULL';
	}

	public function setLocationId( $intLocationId ) {
		$this->set( 'm_intLocationId', CStrings::strToIntDef( $intLocationId, NULL, false ) );
	}

	public function getLocationId() {
		return $this->m_intLocationId;
	}

	public function sqlLocationId() {
		return ( true == isset( $this->m_intLocationId ) ) ? ( string ) $this->m_intLocationId : 'NULL';
	}

	public function setPsDocumentId( $intPsDocumentId ) {
		$this->set( 'm_intPsDocumentId', CStrings::strToIntDef( $intPsDocumentId, NULL, false ) );
	}

	public function getPsDocumentId() {
		return $this->m_intPsDocumentId;
	}

	public function sqlPsDocumentId() {
		return ( true == isset( $this->m_intPsDocumentId ) ) ? ( string ) $this->m_intPsDocumentId : 'NULL';
	}

	public function setInvoiceNumber( $strInvoiceNumber ) {
		$this->set( 'm_strInvoiceNumber', CStrings::strTrimDef( $strInvoiceNumber, 25, NULL, true ) );
	}

	public function getInvoiceNumber() {
		return $this->m_strInvoiceNumber;
	}

	public function sqlInvoiceNumber() {
		return ( true == isset( $this->m_strInvoiceNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInvoiceNumber ) : '\'' . addslashes( $this->m_strInvoiceNumber ) . '\'' ) : 'NULL';
	}

	public function setCpuCoreNum( $intCpuCoreNum ) {
		$this->set( 'm_intCpuCoreNum', CStrings::strToIntDef( $intCpuCoreNum, NULL, false ) );
	}

	public function getCpuCoreNum() {
		return $this->m_intCpuCoreNum;
	}

	public function sqlCpuCoreNum() {
		return ( true == isset( $this->m_intCpuCoreNum ) ) ? ( string ) $this->m_intCpuCoreNum : 'NULL';
	}

	public function setRamDescription( $strRamDescription ) {
		$this->set( 'm_strRamDescription', CStrings::strTrimDef( $strRamDescription, 50, NULL, true ) );
	}

	public function getRamDescription() {
		return $this->m_strRamDescription;
	}

	public function sqlRamDescription() {
		return ( true == isset( $this->m_strRamDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRamDescription ) : '\'' . addslashes( $this->m_strRamDescription ) . '\'' ) : 'NULL';
	}

	public function setHardDiskDescription( $strHardDiskDescription ) {
		$this->set( 'm_strHardDiskDescription', CStrings::strTrimDef( $strHardDiskDescription, 50, NULL, true ) );
	}

	public function getHardDiskDescription() {
		return $this->m_strHardDiskDescription;
	}

	public function sqlHardDiskDescription() {
		return ( true == isset( $this->m_strHardDiskDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHardDiskDescription ) : '\'' . addslashes( $this->m_strHardDiskDescription ) . '\'' ) : 'NULL';
	}

	public function setPowerRequirementDescription( $strPowerRequirementDescription ) {
		$this->set( 'm_strPowerRequirementDescription', CStrings::strTrimDef( $strPowerRequirementDescription, 75, NULL, true ) );
	}

	public function getPowerRequirementDescription() {
		return $this->m_strPowerRequirementDescription;
	}

	public function sqlPowerRequirementDescription() {
		return ( true == isset( $this->m_strPowerRequirementDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPowerRequirementDescription ) : '\'' . addslashes( $this->m_strPowerRequirementDescription ) . '\'' ) : 'NULL';
	}

	public function setDeskNumberDescription( $strDeskNumberDescription ) {
		$this->set( 'm_strDeskNumberDescription', CStrings::strTrimDef( $strDeskNumberDescription, 150, NULL, true ) );
	}

	public function getDeskNumberDescription() {
		return $this->m_strDeskNumberDescription;
	}

	public function sqlDeskNumberDescription() {
		return ( true == isset( $this->m_strDeskNumberDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDeskNumberDescription ) : '\'' . addslashes( $this->m_strDeskNumberDescription ) . '\'' ) : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 50, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
	}

	public function setIpSubnet( $strIpSubnet ) {
		$this->set( 'm_strIpSubnet', CStrings::strTrimDef( $strIpSubnet, 50, NULL, true ) );
	}

	public function getIpSubnet() {
		return $this->m_strIpSubnet;
	}

	public function sqlIpSubnet() {
		return ( true == isset( $this->m_strIpSubnet ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpSubnet ) : '\'' . addslashes( $this->m_strIpSubnet ) . '\'' ) : 'NULL';
	}

	public function setAssetTag( $strAssetTag ) {
		$this->set( 'm_strAssetTag', CStrings::strTrimDef( $strAssetTag, 75, NULL, true ) );
	}

	public function getAssetTag() {
		return $this->m_strAssetTag;
	}

	public function sqlAssetTag() {
		return ( true == isset( $this->m_strAssetTag ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAssetTag ) : '\'' . addslashes( $this->m_strAssetTag ) . '\'' ) : 'NULL';
	}

	public function setServiceTag( $strServiceTag ) {
		$this->set( 'm_strServiceTag', CStrings::strTrimDef( $strServiceTag, 75, NULL, true ) );
	}

	public function getServiceTag() {
		return $this->m_strServiceTag;
	}

	public function sqlServiceTag() {
		return ( true == isset( $this->m_strServiceTag ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServiceTag ) : '\'' . addslashes( $this->m_strServiceTag ) . '\'' ) : 'NULL';
	}

	public function setMacTag( $strMacTag ) {
		$this->set( 'm_strMacTag', CStrings::strTrimDef( $strMacTag, 75, NULL, true ) );
	}

	public function getMacTag() {
		return $this->m_strMacTag;
	}

	public function sqlMacTag() {
		return ( true == isset( $this->m_strMacTag ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strMacTag ) : '\'' . addslashes( $this->m_strMacTag ) . '\'' ) : 'NULL';
	}

	public function setProcessorDescription( $strProcessorDescription ) {
		$this->set( 'm_strProcessorDescription', CStrings::strTrimDef( $strProcessorDescription, 120, NULL, true ) );
	}

	public function getProcessorDescription() {
		return $this->m_strProcessorDescription;
	}

	public function sqlProcessorDescription() {
		return ( true == isset( $this->m_strProcessorDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProcessorDescription ) : '\'' . addslashes( $this->m_strProcessorDescription ) . '\'' ) : 'NULL';
	}

	public function setServerRoleDescription( $strServerRoleDescription ) {
		$this->set( 'm_strServerRoleDescription', CStrings::strTrimDef( $strServerRoleDescription, 120, NULL, true ) );
	}

	public function getServerRoleDescription() {
		return $this->m_strServerRoleDescription;
	}

	public function sqlServerRoleDescription() {
		return ( true == isset( $this->m_strServerRoleDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strServerRoleDescription ) : '\'' . addslashes( $this->m_strServerRoleDescription ) . '\'' ) : 'NULL';
	}

	public function setSpecification( $strSpecification ) {
		$this->set( 'm_strSpecification', CStrings::strTrimDef( $strSpecification, 250, NULL, true ) );
	}

	public function getSpecification() {
		return $this->m_strSpecification;
	}

	public function sqlSpecification() {
		return ( true == isset( $this->m_strSpecification ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSpecification ) : '\'' . addslashes( $this->m_strSpecification ) . '\'' ) : 'NULL';
	}

	public function setHostName( $strHostName ) {
		$this->set( 'm_strHostName', CStrings::strTrimDef( $strHostName, 50, NULL, true ) );
	}

	public function getHostName() {
		return $this->m_strHostName;
	}

	public function sqlHostName() {
		return ( true == isset( $this->m_strHostName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strHostName ) : '\'' . addslashes( $this->m_strHostName ) . '\'' ) : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 5, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCountryCode ) : '\'' . addslashes( $this->m_strCountryCode ) . '\'' ) : 'NULL';
	}

	public function setPurchaseDate( $strPurchaseDate ) {
		$this->set( 'm_strPurchaseDate', CStrings::strTrimDef( $strPurchaseDate, -1, NULL, true ) );
	}

	public function getPurchaseDate() {
		return $this->m_strPurchaseDate;
	}

	public function sqlPurchaseDate() {
		return ( true == isset( $this->m_strPurchaseDate ) ) ? '\'' . $this->m_strPurchaseDate . '\'' : 'NOW()';
	}

	public function setExpiryDate( $strExpiryDate ) {
		$this->set( 'm_strExpiryDate', CStrings::strTrimDef( $strExpiryDate, -1, NULL, true ) );
	}

	public function getExpiryDate() {
		return $this->m_strExpiryDate;
	}

	public function sqlExpiryDate() {
		return ( true == isset( $this->m_strExpiryDate ) ) ? '\'' . $this->m_strExpiryDate . '\'' : 'NULL';
	}

	public function setPurchasedCost( $fltPurchasedCost ) {
		$this->set( 'm_fltPurchasedCost', CStrings::strToFloatDef( $fltPurchasedCost, NULL, false, 2 ) );
	}

	public function getPurchasedCost() {
		return $this->m_fltPurchasedCost;
	}

	public function sqlPurchasedCost() {
		return ( true == isset( $this->m_fltPurchasedCost ) ) ? ( string ) $this->m_fltPurchasedCost : 'NULL';
	}

	public function setDepriciationRate( $intDepriciationRate ) {
		$this->set( 'm_intDepriciationRate', CStrings::strToIntDef( $intDepriciationRate, NULL, false ) );
	}

	public function getDepriciationRate() {
		return $this->m_intDepriciationRate;
	}

	public function sqlDepriciationRate() {
		return ( true == isset( $this->m_intDepriciationRate ) ) ? ( string ) $this->m_intDepriciationRate : 'NULL';
	}

	public function setLicenceNo( $strLicenceNo ) {
		$this->set( 'm_strLicenceNo', CStrings::strTrimDef( $strLicenceNo, 100, NULL, true ) );
	}

	public function getLicenceNo() {
		return $this->m_strLicenceNo;
	}

	public function sqlLicenceNo() {
		return ( true == isset( $this->m_strLicenceNo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLicenceNo ) : '\'' . addslashes( $this->m_strLicenceNo ) . '\'' ) : 'NULL';
	}

	public function setUserLimit( $intUserLimit ) {
		$this->set( 'm_intUserLimit', CStrings::strToIntDef( $intUserLimit, NULL, false ) );
	}

	public function getUserLimit() {
		return $this->m_intUserLimit;
	}

	public function sqlUserLimit() {
		return ( true == isset( $this->m_intUserLimit ) ) ? ( string ) $this->m_intUserLimit : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : '1';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '1';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPsAssetPurchaseSourcesId( $intPsAssetPurchaseSourcesId ) {
		$this->set( 'm_intPsAssetPurchaseSourcesId', CStrings::strToIntDef( $intPsAssetPurchaseSourcesId, NULL, false ) );
	}

	public function getPsAssetPurchaseSourcesId() {
		return $this->m_intPsAssetPurchaseSourcesId;
	}

	public function sqlPsAssetPurchaseSourcesId() {
		return ( true == isset( $this->m_intPsAssetPurchaseSourcesId ) ) ? ( string ) $this->m_intPsAssetPurchaseSourcesId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_asset_status_type_id, office_id, ps_asset_type_id, ps_asset_brand_id, ps_asset_model_id, purchase_request_id, hardware_type_id, machine_type_id, switch_type_id, location_id, ps_document_id, invoice_number, cpu_core_num, ram_description, hard_disk_description, power_requirement_description, desk_number_description, ip_address, ip_subnet, asset_tag, service_tag, mac_tag, processor_description, server_role_description, specification, host_name, country_code, purchase_date, expiry_date, purchased_cost, depriciation_rate, licence_no, user_limit, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, ps_asset_purchase_sources_id, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPsAssetStatusTypeId() . ', ' .
						$this->sqlOfficeId() . ', ' .
						$this->sqlPsAssetTypeId() . ', ' .
						$this->sqlPsAssetBrandId() . ', ' .
						$this->sqlPsAssetModelId() . ', ' .
						$this->sqlPurchaseRequestId() . ', ' .
						$this->sqlHardwareTypeId() . ', ' .
						$this->sqlMachineTypeId() . ', ' .
						$this->sqlSwitchTypeId() . ', ' .
						$this->sqlLocationId() . ', ' .
						$this->sqlPsDocumentId() . ', ' .
						$this->sqlInvoiceNumber() . ', ' .
						$this->sqlCpuCoreNum() . ', ' .
						$this->sqlRamDescription() . ', ' .
						$this->sqlHardDiskDescription() . ', ' .
						$this->sqlPowerRequirementDescription() . ', ' .
						$this->sqlDeskNumberDescription() . ', ' .
						$this->sqlIpAddress() . ', ' .
						$this->sqlIpSubnet() . ', ' .
						$this->sqlAssetTag() . ', ' .
						$this->sqlServiceTag() . ', ' .
						$this->sqlMacTag() . ', ' .
						$this->sqlProcessorDescription() . ', ' .
						$this->sqlServerRoleDescription() . ', ' .
						$this->sqlSpecification() . ', ' .
						$this->sqlHostName() . ', ' .
						$this->sqlCountryCode() . ', ' .
						$this->sqlPurchaseDate() . ', ' .
						$this->sqlExpiryDate() . ', ' .
						$this->sqlPurchasedCost() . ', ' .
						$this->sqlDepriciationRate() . ', ' .
						$this->sqlLicenceNo() . ', ' .
						$this->sqlUserLimit() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlPsAssetPurchaseSourcesId() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_asset_status_type_id = ' . $this->sqlPsAssetStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'PsAssetStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ps_asset_status_type_id = ' . $this->sqlPsAssetStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_id = ' . $this->sqlOfficeId(). ',' ; } elseif( true == array_key_exists( 'OfficeId', $this->getChangedColumns() ) ) { $strSql .= ' office_id = ' . $this->sqlOfficeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_asset_type_id = ' . $this->sqlPsAssetTypeId(). ',' ; } elseif( true == array_key_exists( 'PsAssetTypeId', $this->getChangedColumns() ) ) { $strSql .= ' ps_asset_type_id = ' . $this->sqlPsAssetTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_asset_brand_id = ' . $this->sqlPsAssetBrandId(). ',' ; } elseif( true == array_key_exists( 'PsAssetBrandId', $this->getChangedColumns() ) ) { $strSql .= ' ps_asset_brand_id = ' . $this->sqlPsAssetBrandId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_asset_model_id = ' . $this->sqlPsAssetModelId(). ',' ; } elseif( true == array_key_exists( 'PsAssetModelId', $this->getChangedColumns() ) ) { $strSql .= ' ps_asset_model_id = ' . $this->sqlPsAssetModelId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId(). ',' ; } elseif( true == array_key_exists( 'PurchaseRequestId', $this->getChangedColumns() ) ) { $strSql .= ' purchase_request_id = ' . $this->sqlPurchaseRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hardware_type_id = ' . $this->sqlHardwareTypeId(). ',' ; } elseif( true == array_key_exists( 'HardwareTypeId', $this->getChangedColumns() ) ) { $strSql .= ' hardware_type_id = ' . $this->sqlHardwareTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' machine_type_id = ' . $this->sqlMachineTypeId(). ',' ; } elseif( true == array_key_exists( 'MachineTypeId', $this->getChangedColumns() ) ) { $strSql .= ' machine_type_id = ' . $this->sqlMachineTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' switch_type_id = ' . $this->sqlSwitchTypeId(). ',' ; } elseif( true == array_key_exists( 'SwitchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' switch_type_id = ' . $this->sqlSwitchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' location_id = ' . $this->sqlLocationId(). ',' ; } elseif( true == array_key_exists( 'LocationId', $this->getChangedColumns() ) ) { $strSql .= ' location_id = ' . $this->sqlLocationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId(). ',' ; } elseif( true == array_key_exists( 'PsDocumentId', $this->getChangedColumns() ) ) { $strSql .= ' ps_document_id = ' . $this->sqlPsDocumentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_number = ' . $this->sqlInvoiceNumber(). ',' ; } elseif( true == array_key_exists( 'InvoiceNumber', $this->getChangedColumns() ) ) { $strSql .= ' invoice_number = ' . $this->sqlInvoiceNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cpu_core_num = ' . $this->sqlCpuCoreNum(). ',' ; } elseif( true == array_key_exists( 'CpuCoreNum', $this->getChangedColumns() ) ) { $strSql .= ' cpu_core_num = ' . $this->sqlCpuCoreNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ram_description = ' . $this->sqlRamDescription(). ',' ; } elseif( true == array_key_exists( 'RamDescription', $this->getChangedColumns() ) ) { $strSql .= ' ram_description = ' . $this->sqlRamDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hard_disk_description = ' . $this->sqlHardDiskDescription(). ',' ; } elseif( true == array_key_exists( 'HardDiskDescription', $this->getChangedColumns() ) ) { $strSql .= ' hard_disk_description = ' . $this->sqlHardDiskDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' power_requirement_description = ' . $this->sqlPowerRequirementDescription(). ',' ; } elseif( true == array_key_exists( 'PowerRequirementDescription', $this->getChangedColumns() ) ) { $strSql .= ' power_requirement_description = ' . $this->sqlPowerRequirementDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desk_number_description = ' . $this->sqlDeskNumberDescription(). ',' ; } elseif( true == array_key_exists( 'DeskNumberDescription', $this->getChangedColumns() ) ) { $strSql .= ' desk_number_description = ' . $this->sqlDeskNumberDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_subnet = ' . $this->sqlIpSubnet(). ',' ; } elseif( true == array_key_exists( 'IpSubnet', $this->getChangedColumns() ) ) { $strSql .= ' ip_subnet = ' . $this->sqlIpSubnet() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' asset_tag = ' . $this->sqlAssetTag(). ',' ; } elseif( true == array_key_exists( 'AssetTag', $this->getChangedColumns() ) ) { $strSql .= ' asset_tag = ' . $this->sqlAssetTag() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' service_tag = ' . $this->sqlServiceTag(). ',' ; } elseif( true == array_key_exists( 'ServiceTag', $this->getChangedColumns() ) ) { $strSql .= ' service_tag = ' . $this->sqlServiceTag() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' mac_tag = ' . $this->sqlMacTag(). ',' ; } elseif( true == array_key_exists( 'MacTag', $this->getChangedColumns() ) ) { $strSql .= ' mac_tag = ' . $this->sqlMacTag() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processor_description = ' . $this->sqlProcessorDescription(). ',' ; } elseif( true == array_key_exists( 'ProcessorDescription', $this->getChangedColumns() ) ) { $strSql .= ' processor_description = ' . $this->sqlProcessorDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' server_role_description = ' . $this->sqlServerRoleDescription(). ',' ; } elseif( true == array_key_exists( 'ServerRoleDescription', $this->getChangedColumns() ) ) { $strSql .= ' server_role_description = ' . $this->sqlServerRoleDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' specification = ' . $this->sqlSpecification(). ',' ; } elseif( true == array_key_exists( 'Specification', $this->getChangedColumns() ) ) { $strSql .= ' specification = ' . $this->sqlSpecification() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' host_name = ' . $this->sqlHostName(). ',' ; } elseif( true == array_key_exists( 'HostName', $this->getChangedColumns() ) ) { $strSql .= ' host_name = ' . $this->sqlHostName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode(). ',' ; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchase_date = ' . $this->sqlPurchaseDate(). ',' ; } elseif( true == array_key_exists( 'PurchaseDate', $this->getChangedColumns() ) ) { $strSql .= ' purchase_date = ' . $this->sqlPurchaseDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expiry_date = ' . $this->sqlExpiryDate(). ',' ; } elseif( true == array_key_exists( 'ExpiryDate', $this->getChangedColumns() ) ) { $strSql .= ' expiry_date = ' . $this->sqlExpiryDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchased_cost = ' . $this->sqlPurchasedCost(). ',' ; } elseif( true == array_key_exists( 'PurchasedCost', $this->getChangedColumns() ) ) { $strSql .= ' purchased_cost = ' . $this->sqlPurchasedCost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' depriciation_rate = ' . $this->sqlDepriciationRate(). ',' ; } elseif( true == array_key_exists( 'DepriciationRate', $this->getChangedColumns() ) ) { $strSql .= ' depriciation_rate = ' . $this->sqlDepriciationRate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' licence_no = ' . $this->sqlLicenceNo(). ',' ; } elseif( true == array_key_exists( 'LicenceNo', $this->getChangedColumns() ) ) { $strSql .= ' licence_no = ' . $this->sqlLicenceNo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_limit = ' . $this->sqlUserLimit(). ',' ; } elseif( true == array_key_exists( 'UserLimit', $this->getChangedColumns() ) ) { $strSql .= ' user_limit = ' . $this->sqlUserLimit() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_asset_purchase_sources_id = ' . $this->sqlPsAssetPurchaseSourcesId(). ',' ; } elseif( true == array_key_exists( 'PsAssetPurchaseSourcesId', $this->getChangedColumns() ) ) { $strSql .= ' ps_asset_purchase_sources_id = ' . $this->sqlPsAssetPurchaseSourcesId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_asset_status_type_id' => $this->getPsAssetStatusTypeId(),
			'office_id' => $this->getOfficeId(),
			'ps_asset_type_id' => $this->getPsAssetTypeId(),
			'ps_asset_brand_id' => $this->getPsAssetBrandId(),
			'ps_asset_model_id' => $this->getPsAssetModelId(),
			'purchase_request_id' => $this->getPurchaseRequestId(),
			'hardware_type_id' => $this->getHardwareTypeId(),
			'machine_type_id' => $this->getMachineTypeId(),
			'switch_type_id' => $this->getSwitchTypeId(),
			'location_id' => $this->getLocationId(),
			'ps_document_id' => $this->getPsDocumentId(),
			'invoice_number' => $this->getInvoiceNumber(),
			'cpu_core_num' => $this->getCpuCoreNum(),
			'ram_description' => $this->getRamDescription(),
			'hard_disk_description' => $this->getHardDiskDescription(),
			'power_requirement_description' => $this->getPowerRequirementDescription(),
			'desk_number_description' => $this->getDeskNumberDescription(),
			'ip_address' => $this->getIpAddress(),
			'ip_subnet' => $this->getIpSubnet(),
			'asset_tag' => $this->getAssetTag(),
			'service_tag' => $this->getServiceTag(),
			'mac_tag' => $this->getMacTag(),
			'processor_description' => $this->getProcessorDescription(),
			'server_role_description' => $this->getServerRoleDescription(),
			'specification' => $this->getSpecification(),
			'host_name' => $this->getHostName(),
			'country_code' => $this->getCountryCode(),
			'purchase_date' => $this->getPurchaseDate(),
			'expiry_date' => $this->getExpiryDate(),
			'purchased_cost' => $this->getPurchasedCost(),
			'depriciation_rate' => $this->getDepriciationRate(),
			'licence_no' => $this->getLicenceNo(),
			'user_limit' => $this->getUserLimit(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'ps_asset_purchase_sources_id' => $this->getPsAssetPurchaseSourcesId(),
			'details' => $this->getDetails()
		);
	}

}
?>