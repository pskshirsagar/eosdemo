<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemPropertyAmenities
 * Do not add any new functions to this class.
 */

class CBaseSemPropertyAmenities extends CEosPluralBase {

	/**
	 * @return CSemPropertyAmenity[]
	 */
	public static function fetchSemPropertyAmenities( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemPropertyAmenity', $objDatabase );
	}

	/**
	 * @return CSemPropertyAmenity
	 */
	public static function fetchSemPropertyAmenity( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemPropertyAmenity', $objDatabase );
	}

	public static function fetchSemPropertyAmenityCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_property_amenities', $objDatabase );
	}

	public static function fetchSemPropertyAmenityById( $intId, $objDatabase ) {
		return self::fetchSemPropertyAmenity( sprintf( 'SELECT * FROM sem_property_amenities WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemPropertyAmenitiesByCid( $intCid, $objDatabase ) {
		return self::fetchSemPropertyAmenities( sprintf( 'SELECT * FROM sem_property_amenities WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSemPropertyAmenitiesByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSemPropertyAmenities( sprintf( 'SELECT * FROM sem_property_amenities WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchSemPropertyAmenitiesByStandardAmenityId( $intStandardAmenityId, $objDatabase ) {
		return self::fetchSemPropertyAmenities( sprintf( 'SELECT * FROM sem_property_amenities WHERE standard_amenity_id = %d', ( int ) $intStandardAmenityId ), $objDatabase );
	}

}
?>