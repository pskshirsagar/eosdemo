<?php

class CBaseSalesTaxState extends CEosSingularBase {

	const TABLE_NAME = 'public.sales_tax_states';

	protected $m_intId;
	protected $m_strStateCode;
	protected $m_strStoStateId;
	protected $m_boolIsActive;
	protected $m_strAddressRequirementType;
	protected $m_intDebitChartOfAccountId;
	protected $m_intCreditChartOfAccountId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsActive = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['state_code'] ) && $boolDirectSet ) $this->set( 'm_strStateCode', trim( stripcslashes( $arrValues['state_code'] ) ) ); elseif( isset( $arrValues['state_code'] ) ) $this->setStateCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['state_code'] ) : $arrValues['state_code'] );
		if( isset( $arrValues['sto_state_id'] ) && $boolDirectSet ) $this->set( 'm_strStoStateId', trim( stripcslashes( $arrValues['sto_state_id'] ) ) ); elseif( isset( $arrValues['sto_state_id'] ) ) $this->setStoStateId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sto_state_id'] ) : $arrValues['sto_state_id'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_boolIsActive', trim( stripcslashes( $arrValues['is_active'] ) ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_active'] ) : $arrValues['is_active'] );
		if( isset( $arrValues['address_requirement_type'] ) && $boolDirectSet ) $this->set( 'm_strAddressRequirementType', trim( stripcslashes( $arrValues['address_requirement_type'] ) ) ); elseif( isset( $arrValues['address_requirement_type'] ) ) $this->setAddressRequirementType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['address_requirement_type'] ) : $arrValues['address_requirement_type'] );
		if( isset( $arrValues['debit_chart_of_account_id'] ) && $boolDirectSet ) $this->set( 'm_intDebitChartOfAccountId', trim( $arrValues['debit_chart_of_account_id'] ) ); elseif( isset( $arrValues['debit_chart_of_account_id'] ) ) $this->setDebitChartOfAccountId( $arrValues['debit_chart_of_account_id'] );
		if( isset( $arrValues['credit_chart_of_account_id'] ) && $boolDirectSet ) $this->set( 'm_intCreditChartOfAccountId', trim( $arrValues['credit_chart_of_account_id'] ) ); elseif( isset( $arrValues['credit_chart_of_account_id'] ) ) $this->setCreditChartOfAccountId( $arrValues['credit_chart_of_account_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setStateCode( $strStateCode ) {
		$this->set( 'm_strStateCode', CStrings::strTrimDef( $strStateCode, 2, NULL, true ) );
	}

	public function getStateCode() {
		return $this->m_strStateCode;
	}

	public function sqlStateCode() {
		return ( true == isset( $this->m_strStateCode ) ) ? '\'' . addslashes( $this->m_strStateCode ) . '\'' : 'NULL';
	}

	public function setStoStateId( $strStoStateId ) {
		$this->set( 'm_strStoStateId', CStrings::strTrimDef( $strStoStateId, 100, NULL, true ) );
	}

	public function getStoStateId() {
		return $this->m_strStoStateId;
	}

	public function sqlStoStateId() {
		return ( true == isset( $this->m_strStoStateId ) ) ? '\'' . addslashes( $this->m_strStoStateId ) . '\'' : 'NULL';
	}

	public function setIsActive( $boolIsActive ) {
		$this->set( 'm_boolIsActive', CStrings::strToBool( $boolIsActive ) );
	}

	public function getIsActive() {
		return $this->m_boolIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_boolIsActive ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsActive ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setAddressRequirementType( $strAddressRequirementType ) {
		$this->set( 'm_strAddressRequirementType', CStrings::strTrimDef( $strAddressRequirementType, 100, NULL, true ) );
	}

	public function getAddressRequirementType() {
		return $this->m_strAddressRequirementType;
	}

	public function sqlAddressRequirementType() {
		return ( true == isset( $this->m_strAddressRequirementType ) ) ? '\'' . addslashes( $this->m_strAddressRequirementType ) . '\'' : 'NULL';
	}

	public function setDebitChartOfAccountId( $intDebitChartOfAccountId ) {
		$this->set( 'm_intDebitChartOfAccountId', CStrings::strToIntDef( $intDebitChartOfAccountId, NULL, false ) );
	}

	public function getDebitChartOfAccountId() {
		return $this->m_intDebitChartOfAccountId;
	}

	public function sqlDebitChartOfAccountId() {
		return ( true == isset( $this->m_intDebitChartOfAccountId ) ) ? ( string ) $this->m_intDebitChartOfAccountId : 'NULL';
	}

	public function setCreditChartOfAccountId( $intCreditChartOfAccountId ) {
		$this->set( 'm_intCreditChartOfAccountId', CStrings::strToIntDef( $intCreditChartOfAccountId, NULL, false ) );
	}

	public function getCreditChartOfAccountId() {
		return $this->m_intCreditChartOfAccountId;
	}

	public function sqlCreditChartOfAccountId() {
		return ( true == isset( $this->m_intCreditChartOfAccountId ) ) ? ( string ) $this->m_intCreditChartOfAccountId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, state_code, sto_state_id, is_active, address_requirement_type, debit_chart_of_account_id, credit_chart_of_account_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlStateCode() . ', ' .
 						$this->sqlStoStateId() . ', ' .
 						$this->sqlIsActive() . ', ' .
 						$this->sqlAddressRequirementType() . ', ' .
 						$this->sqlDebitChartOfAccountId() . ', ' .
 						$this->sqlCreditChartOfAccountId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; } elseif( true == array_key_exists( 'StateCode', $this->getChangedColumns() ) ) { $strSql .= ' state_code = ' . $this->sqlStateCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sto_state_id = ' . $this->sqlStoStateId() . ','; } elseif( true == array_key_exists( 'StoStateId', $this->getChangedColumns() ) ) { $strSql .= ' sto_state_id = ' . $this->sqlStoStateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' address_requirement_type = ' . $this->sqlAddressRequirementType() . ','; } elseif( true == array_key_exists( 'AddressRequirementType', $this->getChangedColumns() ) ) { $strSql .= ' address_requirement_type = ' . $this->sqlAddressRequirementType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' debit_chart_of_account_id = ' . $this->sqlDebitChartOfAccountId() . ','; } elseif( true == array_key_exists( 'DebitChartOfAccountId', $this->getChangedColumns() ) ) { $strSql .= ' debit_chart_of_account_id = ' . $this->sqlDebitChartOfAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' credit_chart_of_account_id = ' . $this->sqlCreditChartOfAccountId() . ','; } elseif( true == array_key_exists( 'CreditChartOfAccountId', $this->getChangedColumns() ) ) { $strSql .= ' credit_chart_of_account_id = ' . $this->sqlCreditChartOfAccountId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'state_code' => $this->getStateCode(),
			'sto_state_id' => $this->getStoStateId(),
			'is_active' => $this->getIsActive(),
			'address_requirement_type' => $this->getAddressRequirementType(),
			'debit_chart_of_account_id' => $this->getDebitChartOfAccountId(),
			'credit_chart_of_account_id' => $this->getCreditChartOfAccountId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>