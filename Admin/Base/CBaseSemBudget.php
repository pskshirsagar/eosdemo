<?php

class CBaseSemBudget extends CEosSingularBase {

	const TABLE_NAME = 'public.sem_budgets';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intSemAdGroupId;
	protected $m_intPropertyId;
	protected $m_strBudgetDate;
	protected $m_fltMonthlyBudget;
	protected $m_fltRemainingBudget;
	protected $m_fltDailyRemainingBudget;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltMonthlyBudget = '0';
		$this->m_fltRemainingBudget = '0';
		$this->m_fltDailyRemainingBudget = '0.00';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['sem_ad_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdGroupId', trim( $arrValues['sem_ad_group_id'] ) ); elseif( isset( $arrValues['sem_ad_group_id'] ) ) $this->setSemAdGroupId( $arrValues['sem_ad_group_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['budget_date'] ) && $boolDirectSet ) $this->set( 'm_strBudgetDate', trim( $arrValues['budget_date'] ) ); elseif( isset( $arrValues['budget_date'] ) ) $this->setBudgetDate( $arrValues['budget_date'] );
		if( isset( $arrValues['monthly_budget'] ) && $boolDirectSet ) $this->set( 'm_fltMonthlyBudget', trim( $arrValues['monthly_budget'] ) ); elseif( isset( $arrValues['monthly_budget'] ) ) $this->setMonthlyBudget( $arrValues['monthly_budget'] );
		if( isset( $arrValues['remaining_budget'] ) && $boolDirectSet ) $this->set( 'm_fltRemainingBudget', trim( $arrValues['remaining_budget'] ) ); elseif( isset( $arrValues['remaining_budget'] ) ) $this->setRemainingBudget( $arrValues['remaining_budget'] );
		if( isset( $arrValues['daily_remaining_budget'] ) && $boolDirectSet ) $this->set( 'm_fltDailyRemainingBudget', trim( $arrValues['daily_remaining_budget'] ) ); elseif( isset( $arrValues['daily_remaining_budget'] ) ) $this->setDailyRemainingBudget( $arrValues['daily_remaining_budget'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setSemAdGroupId( $intSemAdGroupId ) {
		$this->set( 'm_intSemAdGroupId', CStrings::strToIntDef( $intSemAdGroupId, NULL, false ) );
	}

	public function getSemAdGroupId() {
		return $this->m_intSemAdGroupId;
	}

	public function sqlSemAdGroupId() {
		return ( true == isset( $this->m_intSemAdGroupId ) ) ? ( string ) $this->m_intSemAdGroupId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setBudgetDate( $strBudgetDate ) {
		$this->set( 'm_strBudgetDate', CStrings::strTrimDef( $strBudgetDate, -1, NULL, true ) );
	}

	public function getBudgetDate() {
		return $this->m_strBudgetDate;
	}

	public function sqlBudgetDate() {
		return ( true == isset( $this->m_strBudgetDate ) ) ? '\'' . $this->m_strBudgetDate . '\'' : 'NOW()';
	}

	public function setMonthlyBudget( $fltMonthlyBudget ) {
		$this->set( 'm_fltMonthlyBudget', CStrings::strToFloatDef( $fltMonthlyBudget, NULL, false, 2 ) );
	}

	public function getMonthlyBudget() {
		return $this->m_fltMonthlyBudget;
	}

	public function sqlMonthlyBudget() {
		return ( true == isset( $this->m_fltMonthlyBudget ) ) ? ( string ) $this->m_fltMonthlyBudget : '0';
	}

	public function setRemainingBudget( $fltRemainingBudget ) {
		$this->set( 'm_fltRemainingBudget', CStrings::strToFloatDef( $fltRemainingBudget, NULL, false, 2 ) );
	}

	public function getRemainingBudget() {
		return $this->m_fltRemainingBudget;
	}

	public function sqlRemainingBudget() {
		return ( true == isset( $this->m_fltRemainingBudget ) ) ? ( string ) $this->m_fltRemainingBudget : '0';
	}

	public function setDailyRemainingBudget( $fltDailyRemainingBudget ) {
		$this->set( 'm_fltDailyRemainingBudget', CStrings::strToFloatDef( $fltDailyRemainingBudget, NULL, false, 2 ) );
	}

	public function getDailyRemainingBudget() {
		return $this->m_fltDailyRemainingBudget;
	}

	public function sqlDailyRemainingBudget() {
		return ( true == isset( $this->m_fltDailyRemainingBudget ) ) ? ( string ) $this->m_fltDailyRemainingBudget : '0.00';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, sem_ad_group_id, property_id, budget_date, monthly_budget, remaining_budget, daily_remaining_budget, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlAccountId() . ', ' .
 						$this->sqlSemAdGroupId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlBudgetDate() . ', ' .
 						$this->sqlMonthlyBudget() . ', ' .
 						$this->sqlRemainingBudget() . ', ' .
 						$this->sqlDailyRemainingBudget() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; } elseif( true == array_key_exists( 'SemAdGroupId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' budget_date = ' . $this->sqlBudgetDate() . ','; } elseif( true == array_key_exists( 'BudgetDate', $this->getChangedColumns() ) ) { $strSql .= ' budget_date = ' . $this->sqlBudgetDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' monthly_budget = ' . $this->sqlMonthlyBudget() . ','; } elseif( true == array_key_exists( 'MonthlyBudget', $this->getChangedColumns() ) ) { $strSql .= ' monthly_budget = ' . $this->sqlMonthlyBudget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remaining_budget = ' . $this->sqlRemainingBudget() . ','; } elseif( true == array_key_exists( 'RemainingBudget', $this->getChangedColumns() ) ) { $strSql .= ' remaining_budget = ' . $this->sqlRemainingBudget() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' daily_remaining_budget = ' . $this->sqlDailyRemainingBudget() . ','; } elseif( true == array_key_exists( 'DailyRemainingBudget', $this->getChangedColumns() ) ) { $strSql .= ' daily_remaining_budget = ' . $this->sqlDailyRemainingBudget() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'sem_ad_group_id' => $this->getSemAdGroupId(),
			'property_id' => $this->getPropertyId(),
			'budget_date' => $this->getBudgetDate(),
			'monthly_budget' => $this->getMonthlyBudget(),
			'remaining_budget' => $this->getRemainingBudget(),
			'daily_remaining_budget' => $this->getDailyRemainingBudget(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>