<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CRequestTypes
 * Do not add any new functions to this class.
 */

class CBaseRequestTypes extends CEosPluralBase {

	/**
	 * @return CRequestType[]
	 */
	public static function fetchRequestTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CRequestType', $objDatabase );
	}

	/**
	 * @return CRequestType
	 */
	public static function fetchRequestType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRequestType', $objDatabase );
	}

	public static function fetchRequestTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'request_types', $objDatabase );
	}

	public static function fetchRequestTypeById( $intId, $objDatabase ) {
		return self::fetchRequestType( sprintf( 'SELECT * FROM request_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>