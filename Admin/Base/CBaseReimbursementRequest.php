<?php

class CBaseReimbursementRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.reimbursement_requests';

	protected $m_intId;
	protected $m_intRequestTypeId;
	protected $m_intEmployeePayStatusTypeId;
	protected $m_intBatchId;
	protected $m_intPayrollPeriodId;
	protected $m_strTitle;
	protected $m_strTotalAmountEncrypted;
	protected $m_boolIsLocked;
	protected $m_intApprovedBy;
	protected $m_intRequestedBy;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intRequestTypeId = '1';
		$this->m_intEmployeePayStatusTypeId = '1';
		$this->m_boolIsLocked = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['request_type_id'] ) && $boolDirectSet ) $this->set( 'm_intRequestTypeId', trim( $arrValues['request_type_id'] ) ); elseif( isset( $arrValues['request_type_id'] ) ) $this->setRequestTypeId( $arrValues['request_type_id'] );
		if( isset( $arrValues['employee_pay_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeePayStatusTypeId', trim( $arrValues['employee_pay_status_type_id'] ) ); elseif( isset( $arrValues['employee_pay_status_type_id'] ) ) $this->setEmployeePayStatusTypeId( $arrValues['employee_pay_status_type_id'] );
		if( isset( $arrValues['batch_id'] ) && $boolDirectSet ) $this->set( 'm_intBatchId', trim( $arrValues['batch_id'] ) ); elseif( isset( $arrValues['batch_id'] ) ) $this->setBatchId( $arrValues['batch_id'] );
		if( isset( $arrValues['payroll_period_id'] ) && $boolDirectSet ) $this->set( 'm_intPayrollPeriodId', trim( $arrValues['payroll_period_id'] ) ); elseif( isset( $arrValues['payroll_period_id'] ) ) $this->setPayrollPeriodId( $arrValues['payroll_period_id'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['total_amount_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strTotalAmountEncrypted', trim( stripcslashes( $arrValues['total_amount_encrypted'] ) ) ); elseif( isset( $arrValues['total_amount_encrypted'] ) ) $this->setTotalAmountEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['total_amount_encrypted'] ) : $arrValues['total_amount_encrypted'] );
		if( isset( $arrValues['is_locked'] ) && $boolDirectSet ) $this->set( 'm_boolIsLocked', trim( stripcslashes( $arrValues['is_locked'] ) ) ); elseif( isset( $arrValues['is_locked'] ) ) $this->setIsLocked( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_locked'] ) : $arrValues['is_locked'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['requested_by'] ) && $boolDirectSet ) $this->set( 'm_intRequestedBy', trim( $arrValues['requested_by'] ) ); elseif( isset( $arrValues['requested_by'] ) ) $this->setRequestedBy( $arrValues['requested_by'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setRequestTypeId( $intRequestTypeId ) {
		$this->set( 'm_intRequestTypeId', CStrings::strToIntDef( $intRequestTypeId, NULL, false ) );
	}

	public function getRequestTypeId() {
		return $this->m_intRequestTypeId;
	}

	public function sqlRequestTypeId() {
		return ( true == isset( $this->m_intRequestTypeId ) ) ? ( string ) $this->m_intRequestTypeId : '1';
	}

	public function setEmployeePayStatusTypeId( $intEmployeePayStatusTypeId ) {
		$this->set( 'm_intEmployeePayStatusTypeId', CStrings::strToIntDef( $intEmployeePayStatusTypeId, NULL, false ) );
	}

	public function getEmployeePayStatusTypeId() {
		return $this->m_intEmployeePayStatusTypeId;
	}

	public function sqlEmployeePayStatusTypeId() {
		return ( true == isset( $this->m_intEmployeePayStatusTypeId ) ) ? ( string ) $this->m_intEmployeePayStatusTypeId : '1';
	}

	public function setBatchId( $intBatchId ) {
		$this->set( 'm_intBatchId', CStrings::strToIntDef( $intBatchId, NULL, false ) );
	}

	public function getBatchId() {
		return $this->m_intBatchId;
	}

	public function sqlBatchId() {
		return ( true == isset( $this->m_intBatchId ) ) ? ( string ) $this->m_intBatchId : 'NULL';
	}

	public function setPayrollPeriodId( $intPayrollPeriodId ) {
		$this->set( 'm_intPayrollPeriodId', CStrings::strToIntDef( $intPayrollPeriodId, NULL, false ) );
	}

	public function getPayrollPeriodId() {
		return $this->m_intPayrollPeriodId;
	}

	public function sqlPayrollPeriodId() {
		return ( true == isset( $this->m_intPayrollPeriodId ) ) ? ( string ) $this->m_intPayrollPeriodId : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 150, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setTotalAmountEncrypted( $strTotalAmountEncrypted ) {
		$this->set( 'm_strTotalAmountEncrypted', CStrings::strTrimDef( $strTotalAmountEncrypted, -1, NULL, true ) );
	}

	public function getTotalAmountEncrypted() {
		return $this->m_strTotalAmountEncrypted;
	}

	public function sqlTotalAmountEncrypted() {
		return ( true == isset( $this->m_strTotalAmountEncrypted ) ) ? '\'' . addslashes( $this->m_strTotalAmountEncrypted ) . '\'' : 'NULL';
	}

	public function setIsLocked( $boolIsLocked ) {
		$this->set( 'm_boolIsLocked', CStrings::strToBool( $boolIsLocked ) );
	}

	public function getIsLocked() {
		return $this->m_boolIsLocked;
	}

	public function sqlIsLocked() {
		return ( true == isset( $this->m_boolIsLocked ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsLocked ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setRequestedBy( $intRequestedBy ) {
		$this->set( 'm_intRequestedBy', CStrings::strToIntDef( $intRequestedBy, NULL, false ) );
	}

	public function getRequestedBy() {
		return $this->m_intRequestedBy;
	}

	public function sqlRequestedBy() {
		return ( true == isset( $this->m_intRequestedBy ) ) ? ( string ) $this->m_intRequestedBy : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, request_type_id, employee_pay_status_type_id, batch_id, payroll_period_id, title, total_amount_encrypted, is_locked, approved_by, requested_by, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlRequestTypeId() . ', ' .
 						$this->sqlEmployeePayStatusTypeId() . ', ' .
 						$this->sqlBatchId() . ', ' .
 						$this->sqlPayrollPeriodId() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlTotalAmountEncrypted() . ', ' .
 						$this->sqlIsLocked() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlRequestedBy() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request_type_id = ' . $this->sqlRequestTypeId() . ','; } elseif( true == array_key_exists( 'RequestTypeId', $this->getChangedColumns() ) ) { $strSql .= ' request_type_id = ' . $this->sqlRequestTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_pay_status_type_id = ' . $this->sqlEmployeePayStatusTypeId() . ','; } elseif( true == array_key_exists( 'EmployeePayStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_pay_status_type_id = ' . $this->sqlEmployeePayStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' batch_id = ' . $this->sqlBatchId() . ','; } elseif( true == array_key_exists( 'BatchId', $this->getChangedColumns() ) ) { $strSql .= ' batch_id = ' . $this->sqlBatchId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' payroll_period_id = ' . $this->sqlPayrollPeriodId() . ','; } elseif( true == array_key_exists( 'PayrollPeriodId', $this->getChangedColumns() ) ) { $strSql .= ' payroll_period_id = ' . $this->sqlPayrollPeriodId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' total_amount_encrypted = ' . $this->sqlTotalAmountEncrypted() . ','; } elseif( true == array_key_exists( 'TotalAmountEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' total_amount_encrypted = ' . $this->sqlTotalAmountEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_locked = ' . $this->sqlIsLocked() . ','; } elseif( true == array_key_exists( 'IsLocked', $this->getChangedColumns() ) ) { $strSql .= ' is_locked = ' . $this->sqlIsLocked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy() . ','; } elseif( true == array_key_exists( 'RequestedBy', $this->getChangedColumns() ) ) { $strSql .= ' requested_by = ' . $this->sqlRequestedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'request_type_id' => $this->getRequestTypeId(),
			'employee_pay_status_type_id' => $this->getEmployeePayStatusTypeId(),
			'batch_id' => $this->getBatchId(),
			'payroll_period_id' => $this->getPayrollPeriodId(),
			'title' => $this->getTitle(),
			'total_amount_encrypted' => $this->getTotalAmountEncrypted(),
			'is_locked' => $this->getIsLocked(),
			'approved_by' => $this->getApprovedBy(),
			'requested_by' => $this->getRequestedBy(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>