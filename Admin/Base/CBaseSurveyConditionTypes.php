<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyConditionTypes
 * Do not add any new functions to this class.
 */

class CBaseSurveyConditionTypes extends CEosPluralBase {

	/**
	 * @return CSurveyConditionType[]
	 */
	public static function fetchSurveyConditionTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyConditionType', $objDatabase );
	}

	/**
	 * @return CSurveyConditionType
	 */
	public static function fetchSurveyConditionType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyConditionType', $objDatabase );
	}

	public static function fetchSurveyConditionTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_condition_types', $objDatabase );
	}

	public static function fetchSurveyConditionTypeById( $intId, $objDatabase ) {
		return self::fetchSurveyConditionType( sprintf( 'SELECT * FROM survey_condition_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>