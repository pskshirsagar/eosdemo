<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplateOptions
 * Do not add any new functions to this class.
 */

class CBaseSurveyTemplateOptions extends CEosPluralBase {

	/**
	 * @return CSurveyTemplateOption[]
	 */
	public static function fetchSurveyTemplateOptions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyTemplateOption', $objDatabase );
	}

	/**
	 * @return CSurveyTemplateOption
	 */
	public static function fetchSurveyTemplateOption( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyTemplateOption', $objDatabase );
	}

	public static function fetchSurveyTemplateOptionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_template_options', $objDatabase );
	}

	public static function fetchSurveyTemplateOptionById( $intId, $objDatabase ) {
		return self::fetchSurveyTemplateOption( sprintf( 'SELECT * FROM survey_template_options WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveyTemplateOptionsBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		return self::fetchSurveyTemplateOptions( sprintf( 'SELECT * FROM survey_template_options WHERE survey_template_id = %d', ( int ) $intSurveyTemplateId ), $objDatabase );
	}

	public static function fetchSurveyTemplateOptionsBySurveyTemplateQuestionId( $intSurveyTemplateQuestionId, $objDatabase ) {
		return self::fetchSurveyTemplateOptions( sprintf( 'SELECT * FROM survey_template_options WHERE survey_template_question_id = %d', ( int ) $intSurveyTemplateQuestionId ), $objDatabase );
	}

}
?>