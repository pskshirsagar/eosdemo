<?php

class CBaseCompanyCharge extends CEosSingularBase {

	const TABLE_NAME = 'public.company_charges';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intChargeCodeId;
	protected $m_intPropertyId;
	protected $m_strChargeStartDate;
	protected $m_strChargeEndDate;
	protected $m_strCommissionStartDate;
	protected $m_intChargePeriod;
	protected $m_fltChargeAmount;
	protected $m_strChargeMemo;
	protected $m_strInternalNotes;
	protected $m_intLastPostedBy;
	protected $m_strLastPostedOn;
	protected $m_strLastPostedMemo;
	protected $m_intBlockCommissions;
	protected $m_intIsDisabled;
	protected $m_intAlwaysPost;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsFromContract;

	public function __construct() {
		parent::__construct();

		$this->m_strCommissionStartDate = 'now()';
		$this->m_intBlockCommissions = '0';
		$this->m_intIsDisabled = '0';
		$this->m_intAlwaysPost = '0';
		$this->m_boolIsFromContract = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['charge_code_id'] ) && $boolDirectSet ) $this->set( 'm_intChargeCodeId', trim( $arrValues['charge_code_id'] ) ); elseif( isset( $arrValues['charge_code_id'] ) ) $this->setChargeCodeId( $arrValues['charge_code_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['charge_start_date'] ) && $boolDirectSet ) $this->set( 'm_strChargeStartDate', trim( $arrValues['charge_start_date'] ) ); elseif( isset( $arrValues['charge_start_date'] ) ) $this->setChargeStartDate( $arrValues['charge_start_date'] );
		if( isset( $arrValues['charge_end_date'] ) && $boolDirectSet ) $this->set( 'm_strChargeEndDate', trim( $arrValues['charge_end_date'] ) ); elseif( isset( $arrValues['charge_end_date'] ) ) $this->setChargeEndDate( $arrValues['charge_end_date'] );
		if( isset( $arrValues['commission_start_date'] ) && $boolDirectSet ) $this->set( 'm_strCommissionStartDate', trim( $arrValues['commission_start_date'] ) ); elseif( isset( $arrValues['commission_start_date'] ) ) $this->setCommissionStartDate( $arrValues['commission_start_date'] );
		if( isset( $arrValues['charge_period'] ) && $boolDirectSet ) $this->set( 'm_intChargePeriod', trim( $arrValues['charge_period'] ) ); elseif( isset( $arrValues['charge_period'] ) ) $this->setChargePeriod( $arrValues['charge_period'] );
		if( isset( $arrValues['charge_amount'] ) && $boolDirectSet ) $this->set( 'm_fltChargeAmount', trim( $arrValues['charge_amount'] ) ); elseif( isset( $arrValues['charge_amount'] ) ) $this->setChargeAmount( $arrValues['charge_amount'] );
		if( isset( $arrValues['charge_memo'] ) && $boolDirectSet ) $this->set( 'm_strChargeMemo', trim( $arrValues['charge_memo'] ) ); elseif( isset( $arrValues['charge_memo'] ) ) $this->setChargeMemo( $arrValues['charge_memo'] );
		if( isset( $arrValues['internal_notes'] ) && $boolDirectSet ) $this->set( 'm_strInternalNotes', trim( $arrValues['internal_notes'] ) ); elseif( isset( $arrValues['internal_notes'] ) ) $this->setInternalNotes( $arrValues['internal_notes'] );
		if( isset( $arrValues['last_posted_by'] ) && $boolDirectSet ) $this->set( 'm_intLastPostedBy', trim( $arrValues['last_posted_by'] ) ); elseif( isset( $arrValues['last_posted_by'] ) ) $this->setLastPostedBy( $arrValues['last_posted_by'] );
		if( isset( $arrValues['last_posted_on'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedOn', trim( $arrValues['last_posted_on'] ) ); elseif( isset( $arrValues['last_posted_on'] ) ) $this->setLastPostedOn( $arrValues['last_posted_on'] );
		if( isset( $arrValues['last_posted_memo'] ) && $boolDirectSet ) $this->set( 'm_strLastPostedMemo', trim( $arrValues['last_posted_memo'] ) ); elseif( isset( $arrValues['last_posted_memo'] ) ) $this->setLastPostedMemo( $arrValues['last_posted_memo'] );
		if( isset( $arrValues['block_commissions'] ) && $boolDirectSet ) $this->set( 'm_intBlockCommissions', trim( $arrValues['block_commissions'] ) ); elseif( isset( $arrValues['block_commissions'] ) ) $this->setBlockCommissions( $arrValues['block_commissions'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['always_post'] ) && $boolDirectSet ) $this->set( 'm_intAlwaysPost', trim( $arrValues['always_post'] ) ); elseif( isset( $arrValues['always_post'] ) ) $this->setAlwaysPost( $arrValues['always_post'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_from_contract'] ) && $boolDirectSet ) $this->set( 'm_boolIsFromContract', trim( stripcslashes( $arrValues['is_from_contract'] ) ) ); elseif( isset( $arrValues['is_from_contract'] ) ) $this->setIsFromContract( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_from_contract'] ) : $arrValues['is_from_contract'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setChargeCodeId( $intChargeCodeId ) {
		$this->set( 'm_intChargeCodeId', CStrings::strToIntDef( $intChargeCodeId, NULL, false ) );
	}

	public function getChargeCodeId() {
		return $this->m_intChargeCodeId;
	}

	public function sqlChargeCodeId() {
		return ( true == isset( $this->m_intChargeCodeId ) ) ? ( string ) $this->m_intChargeCodeId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setChargeStartDate( $strChargeStartDate ) {
		$this->set( 'm_strChargeStartDate', CStrings::strTrimDef( $strChargeStartDate, -1, NULL, true ) );
	}

	public function getChargeStartDate() {
		return $this->m_strChargeStartDate;
	}

	public function sqlChargeStartDate() {
		return ( true == isset( $this->m_strChargeStartDate ) ) ? '\'' . $this->m_strChargeStartDate . '\'' : 'NULL';
	}

	public function setChargeEndDate( $strChargeEndDate ) {
		$this->set( 'm_strChargeEndDate', CStrings::strTrimDef( $strChargeEndDate, -1, NULL, true ) );
	}

	public function getChargeEndDate() {
		return $this->m_strChargeEndDate;
	}

	public function sqlChargeEndDate() {
		return ( true == isset( $this->m_strChargeEndDate ) ) ? '\'' . $this->m_strChargeEndDate . '\'' : 'NULL';
	}

	public function setCommissionStartDate( $strCommissionStartDate ) {
		$this->set( 'm_strCommissionStartDate', CStrings::strTrimDef( $strCommissionStartDate, -1, NULL, true ) );
	}

	public function getCommissionStartDate() {
		return $this->m_strCommissionStartDate;
	}

	public function sqlCommissionStartDate() {
		return ( true == isset( $this->m_strCommissionStartDate ) ) ? '\'' . $this->m_strCommissionStartDate . '\'' : 'NOW()';
	}

	public function setChargePeriod( $intChargePeriod ) {
		$this->set( 'm_intChargePeriod', CStrings::strToIntDef( $intChargePeriod, NULL, false ) );
	}

	public function getChargePeriod() {
		return $this->m_intChargePeriod;
	}

	public function sqlChargePeriod() {
		return ( true == isset( $this->m_intChargePeriod ) ) ? ( string ) $this->m_intChargePeriod : 'NULL';
	}

	public function setChargeAmount( $fltChargeAmount ) {
		$this->set( 'm_fltChargeAmount', CStrings::strToFloatDef( $fltChargeAmount, NULL, false, 2 ) );
	}

	public function getChargeAmount() {
		return $this->m_fltChargeAmount;
	}

	public function sqlChargeAmount() {
		return ( true == isset( $this->m_fltChargeAmount ) ) ? ( string ) $this->m_fltChargeAmount : 'NULL';
	}

	public function setChargeMemo( $strChargeMemo ) {
		$this->set( 'm_strChargeMemo', CStrings::strTrimDef( $strChargeMemo, 2000, NULL, true ) );
	}

	public function getChargeMemo() {
		return $this->m_strChargeMemo;
	}

	public function sqlChargeMemo() {
		return ( true == isset( $this->m_strChargeMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strChargeMemo ) : '\'' . addslashes( $this->m_strChargeMemo ) . '\'' ) : 'NULL';
	}

	public function setInternalNotes( $strInternalNotes ) {
		$this->set( 'm_strInternalNotes', CStrings::strTrimDef( $strInternalNotes, 2000, NULL, true ) );
	}

	public function getInternalNotes() {
		return $this->m_strInternalNotes;
	}

	public function sqlInternalNotes() {
		return ( true == isset( $this->m_strInternalNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strInternalNotes ) : '\'' . addslashes( $this->m_strInternalNotes ) . '\'' ) : 'NULL';
	}

	public function setLastPostedBy( $intLastPostedBy ) {
		$this->set( 'm_intLastPostedBy', CStrings::strToIntDef( $intLastPostedBy, NULL, false ) );
	}

	public function getLastPostedBy() {
		return $this->m_intLastPostedBy;
	}

	public function sqlLastPostedBy() {
		return ( true == isset( $this->m_intLastPostedBy ) ) ? ( string ) $this->m_intLastPostedBy : 'NULL';
	}

	public function setLastPostedOn( $strLastPostedOn ) {
		$this->set( 'm_strLastPostedOn', CStrings::strTrimDef( $strLastPostedOn, -1, NULL, true ) );
	}

	public function getLastPostedOn() {
		return $this->m_strLastPostedOn;
	}

	public function sqlLastPostedOn() {
		return ( true == isset( $this->m_strLastPostedOn ) ) ? '\'' . $this->m_strLastPostedOn . '\'' : 'NULL';
	}

	public function setLastPostedMemo( $strLastPostedMemo ) {
		$this->set( 'm_strLastPostedMemo', CStrings::strTrimDef( $strLastPostedMemo, 2000, NULL, true ) );
	}

	public function getLastPostedMemo() {
		return $this->m_strLastPostedMemo;
	}

	public function sqlLastPostedMemo() {
		return ( true == isset( $this->m_strLastPostedMemo ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLastPostedMemo ) : '\'' . addslashes( $this->m_strLastPostedMemo ) . '\'' ) : 'NULL';
	}

	public function setBlockCommissions( $intBlockCommissions ) {
		$this->set( 'm_intBlockCommissions', CStrings::strToIntDef( $intBlockCommissions, NULL, false ) );
	}

	public function getBlockCommissions() {
		return $this->m_intBlockCommissions;
	}

	public function sqlBlockCommissions() {
		return ( true == isset( $this->m_intBlockCommissions ) ) ? ( string ) $this->m_intBlockCommissions : '0';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setAlwaysPost( $intAlwaysPost ) {
		$this->set( 'm_intAlwaysPost', CStrings::strToIntDef( $intAlwaysPost, NULL, false ) );
	}

	public function getAlwaysPost() {
		return $this->m_intAlwaysPost;
	}

	public function sqlAlwaysPost() {
		return ( true == isset( $this->m_intAlwaysPost ) ) ? ( string ) $this->m_intAlwaysPost : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsFromContract( $boolIsFromContract ) {
		$this->set( 'm_boolIsFromContract', CStrings::strToBool( $boolIsFromContract ) );
	}

	public function getIsFromContract() {
		return $this->m_boolIsFromContract;
	}

	public function sqlIsFromContract() {
		return ( true == isset( $this->m_boolIsFromContract ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsFromContract ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, charge_code_id, property_id, charge_start_date, charge_end_date, commission_start_date, charge_period, charge_amount, charge_memo, internal_notes, last_posted_by, last_posted_on, last_posted_memo, block_commissions, is_disabled, always_post, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, is_from_contract )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlChargeCodeId() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlChargeStartDate() . ', ' .
						$this->sqlChargeEndDate() . ', ' .
						$this->sqlCommissionStartDate() . ', ' .
						$this->sqlChargePeriod() . ', ' .
						$this->sqlChargeAmount() . ', ' .
						$this->sqlChargeMemo() . ', ' .
						$this->sqlInternalNotes() . ', ' .
						$this->sqlLastPostedBy() . ', ' .
						$this->sqlLastPostedOn() . ', ' .
						$this->sqlLastPostedMemo() . ', ' .
						$this->sqlBlockCommissions() . ', ' .
						$this->sqlIsDisabled() . ', ' .
						$this->sqlAlwaysPost() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlIsFromContract() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId(). ',' ; } elseif( true == array_key_exists( 'ChargeCodeId', $this->getChangedColumns() ) ) { $strSql .= ' charge_code_id = ' . $this->sqlChargeCodeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_start_date = ' . $this->sqlChargeStartDate(). ',' ; } elseif( true == array_key_exists( 'ChargeStartDate', $this->getChangedColumns() ) ) { $strSql .= ' charge_start_date = ' . $this->sqlChargeStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_end_date = ' . $this->sqlChargeEndDate(). ',' ; } elseif( true == array_key_exists( 'ChargeEndDate', $this->getChangedColumns() ) ) { $strSql .= ' charge_end_date = ' . $this->sqlChargeEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_start_date = ' . $this->sqlCommissionStartDate(). ',' ; } elseif( true == array_key_exists( 'CommissionStartDate', $this->getChangedColumns() ) ) { $strSql .= ' commission_start_date = ' . $this->sqlCommissionStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_period = ' . $this->sqlChargePeriod(). ',' ; } elseif( true == array_key_exists( 'ChargePeriod', $this->getChangedColumns() ) ) { $strSql .= ' charge_period = ' . $this->sqlChargePeriod() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_amount = ' . $this->sqlChargeAmount(). ',' ; } elseif( true == array_key_exists( 'ChargeAmount', $this->getChangedColumns() ) ) { $strSql .= ' charge_amount = ' . $this->sqlChargeAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' charge_memo = ' . $this->sqlChargeMemo(). ',' ; } elseif( true == array_key_exists( 'ChargeMemo', $this->getChangedColumns() ) ) { $strSql .= ' charge_memo = ' . $this->sqlChargeMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' internal_notes = ' . $this->sqlInternalNotes(). ',' ; } elseif( true == array_key_exists( 'InternalNotes', $this->getChangedColumns() ) ) { $strSql .= ' internal_notes = ' . $this->sqlInternalNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_by = ' . $this->sqlLastPostedBy(). ',' ; } elseif( true == array_key_exists( 'LastPostedBy', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_by = ' . $this->sqlLastPostedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn(). ',' ; } elseif( true == array_key_exists( 'LastPostedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_on = ' . $this->sqlLastPostedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_posted_memo = ' . $this->sqlLastPostedMemo(). ',' ; } elseif( true == array_key_exists( 'LastPostedMemo', $this->getChangedColumns() ) ) { $strSql .= ' last_posted_memo = ' . $this->sqlLastPostedMemo() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' block_commissions = ' . $this->sqlBlockCommissions(). ',' ; } elseif( true == array_key_exists( 'BlockCommissions', $this->getChangedColumns() ) ) { $strSql .= ' block_commissions = ' . $this->sqlBlockCommissions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled(). ',' ; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' always_post = ' . $this->sqlAlwaysPost(). ',' ; } elseif( true == array_key_exists( 'AlwaysPost', $this->getChangedColumns() ) ) { $strSql .= ' always_post = ' . $this->sqlAlwaysPost() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_from_contract = ' . $this->sqlIsFromContract(). ',' ; } elseif( true == array_key_exists( 'IsFromContract', $this->getChangedColumns() ) ) { $strSql .= ' is_from_contract = ' . $this->sqlIsFromContract() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'charge_code_id' => $this->getChargeCodeId(),
			'property_id' => $this->getPropertyId(),
			'charge_start_date' => $this->getChargeStartDate(),
			'charge_end_date' => $this->getChargeEndDate(),
			'commission_start_date' => $this->getCommissionStartDate(),
			'charge_period' => $this->getChargePeriod(),
			'charge_amount' => $this->getChargeAmount(),
			'charge_memo' => $this->getChargeMemo(),
			'internal_notes' => $this->getInternalNotes(),
			'last_posted_by' => $this->getLastPostedBy(),
			'last_posted_on' => $this->getLastPostedOn(),
			'last_posted_memo' => $this->getLastPostedMemo(),
			'block_commissions' => $this->getBlockCommissions(),
			'is_disabled' => $this->getIsDisabled(),
			'always_post' => $this->getAlwaysPost(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_from_contract' => $this->getIsFromContract()
		);
	}

}
?>