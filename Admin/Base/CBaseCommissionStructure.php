<?php

class CBaseCommissionStructure extends CEosSingularBase {

	const TABLE_NAME = 'public.commission_structures';

	protected $m_intId;
	protected $m_intCommissionStructureTypeId;
	protected $m_intCommissionTypeId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intDefaultPeriodMonths;
	protected $m_intAutoApply;
	protected $m_intIsContractBased;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intCommissionStructureTypeId = '1';
		$this->m_intDefaultPeriodMonths = '48';
		$this->m_intAutoApply = '0';
		$this->m_intIsContractBased = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['commission_structure_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionStructureTypeId', trim( $arrValues['commission_structure_type_id'] ) ); elseif( isset( $arrValues['commission_structure_type_id'] ) ) $this->setCommissionStructureTypeId( $arrValues['commission_structure_type_id'] );
		if( isset( $arrValues['commission_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCommissionTypeId', trim( $arrValues['commission_type_id'] ) ); elseif( isset( $arrValues['commission_type_id'] ) ) $this->setCommissionTypeId( $arrValues['commission_type_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['default_period_months'] ) && $boolDirectSet ) $this->set( 'm_intDefaultPeriodMonths', trim( $arrValues['default_period_months'] ) ); elseif( isset( $arrValues['default_period_months'] ) ) $this->setDefaultPeriodMonths( $arrValues['default_period_months'] );
		if( isset( $arrValues['auto_apply'] ) && $boolDirectSet ) $this->set( 'm_intAutoApply', trim( $arrValues['auto_apply'] ) ); elseif( isset( $arrValues['auto_apply'] ) ) $this->setAutoApply( $arrValues['auto_apply'] );
		if( isset( $arrValues['is_contract_based'] ) && $boolDirectSet ) $this->set( 'm_intIsContractBased', trim( $arrValues['is_contract_based'] ) ); elseif( isset( $arrValues['is_contract_based'] ) ) $this->setIsContractBased( $arrValues['is_contract_based'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCommissionStructureTypeId( $intCommissionStructureTypeId ) {
		$this->set( 'm_intCommissionStructureTypeId', CStrings::strToIntDef( $intCommissionStructureTypeId, NULL, false ) );
	}

	public function getCommissionStructureTypeId() {
		return $this->m_intCommissionStructureTypeId;
	}

	public function sqlCommissionStructureTypeId() {
		return ( true == isset( $this->m_intCommissionStructureTypeId ) ) ? ( string ) $this->m_intCommissionStructureTypeId : '1';
	}

	public function setCommissionTypeId( $intCommissionTypeId ) {
		$this->set( 'm_intCommissionTypeId', CStrings::strToIntDef( $intCommissionTypeId, NULL, false ) );
	}

	public function getCommissionTypeId() {
		return $this->m_intCommissionTypeId;
	}

	public function sqlCommissionTypeId() {
		return ( true == isset( $this->m_intCommissionTypeId ) ) ? ( string ) $this->m_intCommissionTypeId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 240, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDefaultPeriodMonths( $intDefaultPeriodMonths ) {
		$this->set( 'm_intDefaultPeriodMonths', CStrings::strToIntDef( $intDefaultPeriodMonths, NULL, false ) );
	}

	public function getDefaultPeriodMonths() {
		return $this->m_intDefaultPeriodMonths;
	}

	public function sqlDefaultPeriodMonths() {
		return ( true == isset( $this->m_intDefaultPeriodMonths ) ) ? ( string ) $this->m_intDefaultPeriodMonths : '48';
	}

	public function setAutoApply( $intAutoApply ) {
		$this->set( 'm_intAutoApply', CStrings::strToIntDef( $intAutoApply, NULL, false ) );
	}

	public function getAutoApply() {
		return $this->m_intAutoApply;
	}

	public function sqlAutoApply() {
		return ( true == isset( $this->m_intAutoApply ) ) ? ( string ) $this->m_intAutoApply : '0';
	}

	public function setIsContractBased( $intIsContractBased ) {
		$this->set( 'm_intIsContractBased', CStrings::strToIntDef( $intIsContractBased, NULL, false ) );
	}

	public function getIsContractBased() {
		return $this->m_intIsContractBased;
	}

	public function sqlIsContractBased() {
		return ( true == isset( $this->m_intIsContractBased ) ) ? ( string ) $this->m_intIsContractBased : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, commission_structure_type_id, commission_type_id, name, description, default_period_months, auto_apply, is_contract_based, is_published, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCommissionStructureTypeId() . ', ' .
 						$this->sqlCommissionTypeId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlDefaultPeriodMonths() . ', ' .
 						$this->sqlAutoApply() . ', ' .
 						$this->sqlIsContractBased() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_structure_type_id = ' . $this->sqlCommissionStructureTypeId() . ','; } elseif( true == array_key_exists( 'CommissionStructureTypeId', $this->getChangedColumns() ) ) { $strSql .= ' commission_structure_type_id = ' . $this->sqlCommissionStructureTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commission_type_id = ' . $this->sqlCommissionTypeId() . ','; } elseif( true == array_key_exists( 'CommissionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' commission_type_id = ' . $this->sqlCommissionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_period_months = ' . $this->sqlDefaultPeriodMonths() . ','; } elseif( true == array_key_exists( 'DefaultPeriodMonths', $this->getChangedColumns() ) ) { $strSql .= ' default_period_months = ' . $this->sqlDefaultPeriodMonths() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' auto_apply = ' . $this->sqlAutoApply() . ','; } elseif( true == array_key_exists( 'AutoApply', $this->getChangedColumns() ) ) { $strSql .= ' auto_apply = ' . $this->sqlAutoApply() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_contract_based = ' . $this->sqlIsContractBased() . ','; } elseif( true == array_key_exists( 'IsContractBased', $this->getChangedColumns() ) ) { $strSql .= ' is_contract_based = ' . $this->sqlIsContractBased() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'commission_structure_type_id' => $this->getCommissionStructureTypeId(),
			'commission_type_id' => $this->getCommissionTypeId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'default_period_months' => $this->getDefaultPeriodMonths(),
			'auto_apply' => $this->getAutoApply(),
			'is_contract_based' => $this->getIsContractBased(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>