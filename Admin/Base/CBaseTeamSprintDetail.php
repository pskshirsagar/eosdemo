<?php

class CBaseTeamSprintDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.team_sprint_details';

	protected $m_intId;
	protected $m_intPlannerId;
	protected $m_intTaskId;
	protected $m_intEmployeeId;
	protected $m_fltRemainingHours;
	protected $m_strSprintDate;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['planner_id'] ) && $boolDirectSet ) $this->set( 'm_intPlannerId', trim( $arrValues['planner_id'] ) ); elseif( isset( $arrValues['planner_id'] ) ) $this->setPlannerId( $arrValues['planner_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['remaining_hours'] ) && $boolDirectSet ) $this->set( 'm_fltRemainingHours', trim( $arrValues['remaining_hours'] ) ); elseif( isset( $arrValues['remaining_hours'] ) ) $this->setRemainingHours( $arrValues['remaining_hours'] );
		if( isset( $arrValues['sprint_date'] ) && $boolDirectSet ) $this->set( 'm_strSprintDate', trim( $arrValues['sprint_date'] ) ); elseif( isset( $arrValues['sprint_date'] ) ) $this->setSprintDate( $arrValues['sprint_date'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPlannerId( $intPlannerId ) {
		$this->set( 'm_intPlannerId', CStrings::strToIntDef( $intPlannerId, NULL, false ) );
	}

	public function getPlannerId() {
		return $this->m_intPlannerId;
	}

	public function sqlPlannerId() {
		return ( true == isset( $this->m_intPlannerId ) ) ? ( string ) $this->m_intPlannerId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setRemainingHours( $fltRemainingHours ) {
		$this->set( 'm_fltRemainingHours', CStrings::strToFloatDef( $fltRemainingHours, NULL, false, 2 ) );
	}

	public function getRemainingHours() {
		return $this->m_fltRemainingHours;
	}

	public function sqlRemainingHours() {
		return ( true == isset( $this->m_fltRemainingHours ) ) ? ( string ) $this->m_fltRemainingHours : 'NULL';
	}

	public function setSprintDate( $strSprintDate ) {
		$this->set( 'm_strSprintDate', CStrings::strTrimDef( $strSprintDate, -1, NULL, true ) );
	}

	public function getSprintDate() {
		return $this->m_strSprintDate;
	}

	public function sqlSprintDate() {
		return ( true == isset( $this->m_strSprintDate ) ) ? '\'' . $this->m_strSprintDate . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, planner_id, task_id, employee_id, remaining_hours, sprint_date, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
				  $strId . ', ' .
				  $this->sqlPlannerId() . ', ' .
				  $this->sqlTaskId() . ', ' .
				  $this->sqlEmployeeId() . ', ' .
				  $this->sqlRemainingHours() . ', ' .
				  $this->sqlSprintDate() . ', ' .
				  ( int ) $intCurrentUserId . ', ' .
				  $this->sqlUpdatedOn() . ', ' .
				  ( int ) $intCurrentUserId . ', ' .
				  $this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' planner_id = ' . $this->sqlPlannerId() . ','; } elseif( true == array_key_exists( 'PlannerId', $this->getChangedColumns() ) ) { $strSql .= ' planner_id = ' . $this->sqlPlannerId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remaining_hours = ' . $this->sqlRemainingHours() . ','; } elseif( true == array_key_exists( 'RemainingHours', $this->getChangedColumns() ) ) { $strSql .= ' remaining_hours = ' . $this->sqlRemainingHours() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sprint_date = ' . $this->sqlSprintDate() . ','; } elseif( true == array_key_exists( 'SprintDate', $this->getChangedColumns() ) ) { $strSql .= ' sprint_date = ' . $this->sqlSprintDate() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'planner_id' => $this->getPlannerId(),
			'task_id' => $this->getTaskId(),
			'employee_id' => $this->getEmployeeId(),
			'remaining_hours' => $this->getRemainingHours(),
			'sprint_date' => $this->getSprintDate(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>