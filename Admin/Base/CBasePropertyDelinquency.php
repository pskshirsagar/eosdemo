<?php

class CBasePropertyDelinquency extends CEosSingularBase {

	const TABLE_NAME = 'public.property_delinquencies';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intPropertyId;
	protected $m_intDelinquencyLevelTypeId;
	protected $m_intEligibleDelinquencyLevelTypeId;
	protected $m_intDelinquencyThresholdAmount;
	protected $m_fltPropertyAmountDue;
	protected $m_strDelinquencyChangeDate;
	protected $m_strEligibleDelinquencyChangeDate;
	protected $m_intDeliquencyDelayDays;
	protected $m_intDelayCount;
	protected $m_intDelayBy;
	protected $m_strLastNotificationSendOn;
	protected $m_strLastPrintedOn;
	protected $m_strCompletedOn;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intDelinquencyLevelTypeId = '0';
		$this->m_fltPropertyAmountDue = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['delinquency_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyLevelTypeId', trim( $arrValues['delinquency_level_type_id'] ) ); elseif( isset( $arrValues['delinquency_level_type_id'] ) ) $this->setDelinquencyLevelTypeId( $arrValues['delinquency_level_type_id'] );
		if( isset( $arrValues['eligible_delinquency_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEligibleDelinquencyLevelTypeId', trim( $arrValues['eligible_delinquency_level_type_id'] ) ); elseif( isset( $arrValues['eligible_delinquency_level_type_id'] ) ) $this->setEligibleDelinquencyLevelTypeId( $arrValues['eligible_delinquency_level_type_id'] );
		if( isset( $arrValues['delinquency_threshold_amount'] ) && $boolDirectSet ) $this->set( 'm_intDelinquencyThresholdAmount', trim( $arrValues['delinquency_threshold_amount'] ) ); elseif( isset( $arrValues['delinquency_threshold_amount'] ) ) $this->setDelinquencyThresholdAmount( $arrValues['delinquency_threshold_amount'] );
		if( isset( $arrValues['property_amount_due'] ) && $boolDirectSet ) $this->set( 'm_fltPropertyAmountDue', trim( $arrValues['property_amount_due'] ) ); elseif( isset( $arrValues['property_amount_due'] ) ) $this->setPropertyAmountDue( $arrValues['property_amount_due'] );
		if( isset( $arrValues['delinquency_change_date'] ) && $boolDirectSet ) $this->set( 'm_strDelinquencyChangeDate', trim( $arrValues['delinquency_change_date'] ) ); elseif( isset( $arrValues['delinquency_change_date'] ) ) $this->setDelinquencyChangeDate( $arrValues['delinquency_change_date'] );
		if( isset( $arrValues['eligible_delinquency_change_date'] ) && $boolDirectSet ) $this->set( 'm_strEligibleDelinquencyChangeDate', trim( $arrValues['eligible_delinquency_change_date'] ) ); elseif( isset( $arrValues['eligible_delinquency_change_date'] ) ) $this->setEligibleDelinquencyChangeDate( $arrValues['eligible_delinquency_change_date'] );
		if( isset( $arrValues['deliquency_delay_days'] ) && $boolDirectSet ) $this->set( 'm_intDeliquencyDelayDays', trim( $arrValues['deliquency_delay_days'] ) ); elseif( isset( $arrValues['deliquency_delay_days'] ) ) $this->setDeliquencyDelayDays( $arrValues['deliquency_delay_days'] );
		if( isset( $arrValues['delay_count'] ) && $boolDirectSet ) $this->set( 'm_intDelayCount', trim( $arrValues['delay_count'] ) ); elseif( isset( $arrValues['delay_count'] ) ) $this->setDelayCount( $arrValues['delay_count'] );
		if( isset( $arrValues['delay_by'] ) && $boolDirectSet ) $this->set( 'm_intDelayBy', trim( $arrValues['delay_by'] ) ); elseif( isset( $arrValues['delay_by'] ) ) $this->setDelayBy( $arrValues['delay_by'] );
		if( isset( $arrValues['last_notification_send_on'] ) && $boolDirectSet ) $this->set( 'm_strLastNotificationSendOn', trim( $arrValues['last_notification_send_on'] ) ); elseif( isset( $arrValues['last_notification_send_on'] ) ) $this->setLastNotificationSendOn( $arrValues['last_notification_send_on'] );
		if( isset( $arrValues['last_printed_on'] ) && $boolDirectSet ) $this->set( 'm_strLastPrintedOn', trim( $arrValues['last_printed_on'] ) ); elseif( isset( $arrValues['last_printed_on'] ) ) $this->setLastPrintedOn( $arrValues['last_printed_on'] );
		if( isset( $arrValues['completed_on'] ) && $boolDirectSet ) $this->set( 'm_strCompletedOn', trim( $arrValues['completed_on'] ) ); elseif( isset( $arrValues['completed_on'] ) ) $this->setCompletedOn( $arrValues['completed_on'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setDelinquencyLevelTypeId( $intDelinquencyLevelTypeId ) {
		$this->set( 'm_intDelinquencyLevelTypeId', CStrings::strToIntDef( $intDelinquencyLevelTypeId, NULL, false ) );
	}

	public function getDelinquencyLevelTypeId() {
		return $this->m_intDelinquencyLevelTypeId;
	}

	public function sqlDelinquencyLevelTypeId() {
		return ( true == isset( $this->m_intDelinquencyLevelTypeId ) ) ? ( string ) $this->m_intDelinquencyLevelTypeId : '0';
	}

	public function setEligibleDelinquencyLevelTypeId( $intEligibleDelinquencyLevelTypeId ) {
		$this->set( 'm_intEligibleDelinquencyLevelTypeId', CStrings::strToIntDef( $intEligibleDelinquencyLevelTypeId, NULL, false ) );
	}

	public function getEligibleDelinquencyLevelTypeId() {
		return $this->m_intEligibleDelinquencyLevelTypeId;
	}

	public function sqlEligibleDelinquencyLevelTypeId() {
		return ( true == isset( $this->m_intEligibleDelinquencyLevelTypeId ) ) ? ( string ) $this->m_intEligibleDelinquencyLevelTypeId : 'NULL';
	}

	public function setDelinquencyThresholdAmount( $intDelinquencyThresholdAmount ) {
		$this->set( 'm_intDelinquencyThresholdAmount', CStrings::strToIntDef( $intDelinquencyThresholdAmount, NULL, false ) );
	}

	public function getDelinquencyThresholdAmount() {
		return $this->m_intDelinquencyThresholdAmount;
	}

	public function sqlDelinquencyThresholdAmount() {
		return ( true == isset( $this->m_intDelinquencyThresholdAmount ) ) ? ( string ) $this->m_intDelinquencyThresholdAmount : 'NULL';
	}

	public function setPropertyAmountDue( $fltPropertyAmountDue ) {
		$this->set( 'm_fltPropertyAmountDue', CStrings::strToFloatDef( $fltPropertyAmountDue, NULL, false, 4 ) );
	}

	public function getPropertyAmountDue() {
		return $this->m_fltPropertyAmountDue;
	}

	public function sqlPropertyAmountDue() {
		return ( true == isset( $this->m_fltPropertyAmountDue ) ) ? ( string ) $this->m_fltPropertyAmountDue : '0';
	}

	public function setDelinquencyChangeDate( $strDelinquencyChangeDate ) {
		$this->set( 'm_strDelinquencyChangeDate', CStrings::strTrimDef( $strDelinquencyChangeDate, -1, NULL, true ) );
	}

	public function getDelinquencyChangeDate() {
		return $this->m_strDelinquencyChangeDate;
	}

	public function sqlDelinquencyChangeDate() {
		return ( true == isset( $this->m_strDelinquencyChangeDate ) ) ? '\'' . $this->m_strDelinquencyChangeDate . '\'' : 'NULL';
	}

	public function setEligibleDelinquencyChangeDate( $strEligibleDelinquencyChangeDate ) {
		$this->set( 'm_strEligibleDelinquencyChangeDate', CStrings::strTrimDef( $strEligibleDelinquencyChangeDate, -1, NULL, true ) );
	}

	public function getEligibleDelinquencyChangeDate() {
		return $this->m_strEligibleDelinquencyChangeDate;
	}

	public function sqlEligibleDelinquencyChangeDate() {
		return ( true == isset( $this->m_strEligibleDelinquencyChangeDate ) ) ? '\'' . $this->m_strEligibleDelinquencyChangeDate . '\'' : 'NULL';
	}

	public function setDeliquencyDelayDays( $intDeliquencyDelayDays ) {
		$this->set( 'm_intDeliquencyDelayDays', CStrings::strToIntDef( $intDeliquencyDelayDays, NULL, false ) );
	}

	public function getDeliquencyDelayDays() {
		return $this->m_intDeliquencyDelayDays;
	}

	public function sqlDeliquencyDelayDays() {
		return ( true == isset( $this->m_intDeliquencyDelayDays ) ) ? ( string ) $this->m_intDeliquencyDelayDays : 'NULL';
	}

	public function setDelayCount( $intDelayCount ) {
		$this->set( 'm_intDelayCount', CStrings::strToIntDef( $intDelayCount, NULL, false ) );
	}

	public function getDelayCount() {
		return $this->m_intDelayCount;
	}

	public function sqlDelayCount() {
		return ( true == isset( $this->m_intDelayCount ) ) ? ( string ) $this->m_intDelayCount : 'NULL';
	}

	public function setDelayBy( $intDelayBy ) {
		$this->set( 'm_intDelayBy', CStrings::strToIntDef( $intDelayBy, NULL, false ) );
	}

	public function getDelayBy() {
		return $this->m_intDelayBy;
	}

	public function sqlDelayBy() {
		return ( true == isset( $this->m_intDelayBy ) ) ? ( string ) $this->m_intDelayBy : 'NULL';
	}

	public function setLastNotificationSendOn( $strLastNotificationSendOn ) {
		$this->set( 'm_strLastNotificationSendOn', CStrings::strTrimDef( $strLastNotificationSendOn, -1, NULL, true ) );
	}

	public function getLastNotificationSendOn() {
		return $this->m_strLastNotificationSendOn;
	}

	public function sqlLastNotificationSendOn() {
		return ( true == isset( $this->m_strLastNotificationSendOn ) ) ? '\'' . $this->m_strLastNotificationSendOn . '\'' : 'NULL';
	}

	public function setLastPrintedOn( $strLastPrintedOn ) {
		$this->set( 'm_strLastPrintedOn', CStrings::strTrimDef( $strLastPrintedOn, -1, NULL, true ) );
	}

	public function getLastPrintedOn() {
		return $this->m_strLastPrintedOn;
	}

	public function sqlLastPrintedOn() {
		return ( true == isset( $this->m_strLastPrintedOn ) ) ? '\'' . $this->m_strLastPrintedOn . '\'' : 'NULL';
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->set( 'm_strCompletedOn', CStrings::strTrimDef( $strCompletedOn, -1, NULL, true ) );
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function sqlCompletedOn() {
		return ( true == isset( $this->m_strCompletedOn ) ) ? '\'' . $this->m_strCompletedOn . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, account_id, property_id, delinquency_level_type_id, eligible_delinquency_level_type_id, delinquency_threshold_amount, property_amount_due, delinquency_change_date, eligible_delinquency_change_date, deliquency_delay_days, delay_count, delay_by, last_notification_send_on, last_printed_on, completed_on, approved_by, approved_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlAccountId() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlDelinquencyLevelTypeId() . ', ' .
 						$this->sqlEligibleDelinquencyLevelTypeId() . ', ' .
 						$this->sqlDelinquencyThresholdAmount() . ', ' .
 						$this->sqlPropertyAmountDue() . ', ' .
 						$this->sqlDelinquencyChangeDate() . ', ' .
 						$this->sqlEligibleDelinquencyChangeDate() . ', ' .
 						$this->sqlDeliquencyDelayDays() . ', ' .
 						$this->sqlDelayCount() . ', ' .
 						$this->sqlDelayBy() . ', ' .
 						$this->sqlLastNotificationSendOn() . ', ' .
 						$this->sqlLastPrintedOn() . ', ' .
 						$this->sqlCompletedOn() . ', ' .
 						$this->sqlApprovedBy() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_level_type_id = ' . $this->sqlDelinquencyLevelTypeId() . ','; } elseif( true == array_key_exists( 'DelinquencyLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_level_type_id = ' . $this->sqlDelinquencyLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eligible_delinquency_level_type_id = ' . $this->sqlEligibleDelinquencyLevelTypeId() . ','; } elseif( true == array_key_exists( 'EligibleDelinquencyLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' eligible_delinquency_level_type_id = ' . $this->sqlEligibleDelinquencyLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_threshold_amount = ' . $this->sqlDelinquencyThresholdAmount() . ','; } elseif( true == array_key_exists( 'DelinquencyThresholdAmount', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_threshold_amount = ' . $this->sqlDelinquencyThresholdAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_amount_due = ' . $this->sqlPropertyAmountDue() . ','; } elseif( true == array_key_exists( 'PropertyAmountDue', $this->getChangedColumns() ) ) { $strSql .= ' property_amount_due = ' . $this->sqlPropertyAmountDue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delinquency_change_date = ' . $this->sqlDelinquencyChangeDate() . ','; } elseif( true == array_key_exists( 'DelinquencyChangeDate', $this->getChangedColumns() ) ) { $strSql .= ' delinquency_change_date = ' . $this->sqlDelinquencyChangeDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' eligible_delinquency_change_date = ' . $this->sqlEligibleDelinquencyChangeDate() . ','; } elseif( true == array_key_exists( 'EligibleDelinquencyChangeDate', $this->getChangedColumns() ) ) { $strSql .= ' eligible_delinquency_change_date = ' . $this->sqlEligibleDelinquencyChangeDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deliquency_delay_days = ' . $this->sqlDeliquencyDelayDays() . ','; } elseif( true == array_key_exists( 'DeliquencyDelayDays', $this->getChangedColumns() ) ) { $strSql .= ' deliquency_delay_days = ' . $this->sqlDeliquencyDelayDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delay_count = ' . $this->sqlDelayCount() . ','; } elseif( true == array_key_exists( 'DelayCount', $this->getChangedColumns() ) ) { $strSql .= ' delay_count = ' . $this->sqlDelayCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delay_by = ' . $this->sqlDelayBy() . ','; } elseif( true == array_key_exists( 'DelayBy', $this->getChangedColumns() ) ) { $strSql .= ' delay_by = ' . $this->sqlDelayBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_notification_send_on = ' . $this->sqlLastNotificationSendOn() . ','; } elseif( true == array_key_exists( 'LastNotificationSendOn', $this->getChangedColumns() ) ) { $strSql .= ' last_notification_send_on = ' . $this->sqlLastNotificationSendOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' last_printed_on = ' . $this->sqlLastPrintedOn() . ','; } elseif( true == array_key_exists( 'LastPrintedOn', $this->getChangedColumns() ) ) { $strSql .= ' last_printed_on = ' . $this->sqlLastPrintedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; } elseif( true == array_key_exists( 'CompletedOn', $this->getChangedColumns() ) ) { $strSql .= ' completed_on = ' . $this->sqlCompletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'account_id' => $this->getAccountId(),
			'property_id' => $this->getPropertyId(),
			'delinquency_level_type_id' => $this->getDelinquencyLevelTypeId(),
			'eligible_delinquency_level_type_id' => $this->getEligibleDelinquencyLevelTypeId(),
			'delinquency_threshold_amount' => $this->getDelinquencyThresholdAmount(),
			'property_amount_due' => $this->getPropertyAmountDue(),
			'delinquency_change_date' => $this->getDelinquencyChangeDate(),
			'eligible_delinquency_change_date' => $this->getEligibleDelinquencyChangeDate(),
			'deliquency_delay_days' => $this->getDeliquencyDelayDays(),
			'delay_count' => $this->getDelayCount(),
			'delay_by' => $this->getDelayBy(),
			'last_notification_send_on' => $this->getLastNotificationSendOn(),
			'last_printed_on' => $this->getLastPrintedOn(),
			'completed_on' => $this->getCompletedOn(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>