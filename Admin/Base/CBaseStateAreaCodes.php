<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStateAreaCodes
 * Do not add any new functions to this class.
 */

class CBaseStateAreaCodes extends CEosPluralBase {

	/**
	 * @return CStateAreaCode[]
	 */
	public static function fetchStateAreaCodes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CStateAreaCode', $objDatabase );
	}

	/**
	 * @return CStateAreaCode
	 */
	public static function fetchStateAreaCode( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CStateAreaCode', $objDatabase );
	}

	public static function fetchStateAreaCodeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'state_area_codes', $objDatabase );
	}

	public static function fetchStateAreaCodeById( $intId, $objDatabase ) {
		return self::fetchStateAreaCode( sprintf( 'SELECT * FROM state_area_codes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>