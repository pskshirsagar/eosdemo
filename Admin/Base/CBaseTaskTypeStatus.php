<?php

class CBaseTaskTypeStatus extends CEosSingularBase {

	const TABLE_NAME = 'public.task_type_statuses';

	protected $m_intId;
	protected $m_intTaskTypeId;
	protected $m_intTaskStatusId;
	protected $m_intIsEdit;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsEdit = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['task_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskTypeId', trim( $arrValues['task_type_id'] ) ); elseif( isset( $arrValues['task_type_id'] ) ) $this->setTaskTypeId( $arrValues['task_type_id'] );
		if( isset( $arrValues['task_status_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskStatusId', trim( $arrValues['task_status_id'] ) ); elseif( isset( $arrValues['task_status_id'] ) ) $this->setTaskStatusId( $arrValues['task_status_id'] );
		if( isset( $arrValues['is_edit'] ) && $boolDirectSet ) $this->set( 'm_intIsEdit', trim( $arrValues['is_edit'] ) ); elseif( isset( $arrValues['is_edit'] ) ) $this->setIsEdit( $arrValues['is_edit'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaskTypeId( $intTaskTypeId ) {
		$this->set( 'm_intTaskTypeId', CStrings::strToIntDef( $intTaskTypeId, NULL, false ) );
	}

	public function getTaskTypeId() {
		return $this->m_intTaskTypeId;
	}

	public function sqlTaskTypeId() {
		return ( true == isset( $this->m_intTaskTypeId ) ) ? ( string ) $this->m_intTaskTypeId : 'NULL';
	}

	public function setTaskStatusId( $intTaskStatusId ) {
		$this->set( 'm_intTaskStatusId', CStrings::strToIntDef( $intTaskStatusId, NULL, false ) );
	}

	public function getTaskStatusId() {
		return $this->m_intTaskStatusId;
	}

	public function sqlTaskStatusId() {
		return ( true == isset( $this->m_intTaskStatusId ) ) ? ( string ) $this->m_intTaskStatusId : 'NULL';
	}

	public function setIsEdit( $intIsEdit ) {
		$this->set( 'm_intIsEdit', CStrings::strToIntDef( $intIsEdit, NULL, false ) );
	}

	public function getIsEdit() {
		return $this->m_intIsEdit;
	}

	public function sqlIsEdit() {
		return ( true == isset( $this->m_intIsEdit ) ) ? ( string ) $this->m_intIsEdit : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, task_type_id, task_status_id, is_edit, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTaskTypeId() . ', ' .
 						$this->sqlTaskStatusId() . ', ' .
 						$this->sqlIsEdit() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_type_id = ' . $this->sqlTaskTypeId() . ','; } elseif( true == array_key_exists( 'TaskTypeId', $this->getChangedColumns() ) ) { $strSql .= ' task_type_id = ' . $this->sqlTaskTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_status_id = ' . $this->sqlTaskStatusId() . ','; } elseif( true == array_key_exists( 'TaskStatusId', $this->getChangedColumns() ) ) { $strSql .= ' task_status_id = ' . $this->sqlTaskStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_edit = ' . $this->sqlIsEdit() . ','; } elseif( true == array_key_exists( 'IsEdit', $this->getChangedColumns() ) ) { $strSql .= ' is_edit = ' . $this->sqlIsEdit() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'task_type_id' => $this->getTaskTypeId(),
			'task_status_id' => $this->getTaskStatusId(),
			'is_edit' => $this->getIsEdit(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>