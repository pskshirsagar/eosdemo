<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplateManagerQuestionAssociations
 * Do not add any new functions to this class.
 */

class CBaseSurveyTemplateManagerQuestionAssociations extends CEosPluralBase {

	/**
	 * @return CSurveyTemplateManagerQuestionAssociation[]
	 */
	public static function fetchSurveyTemplateManagerQuestionAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyTemplateManagerQuestionAssociation', $objDatabase );
	}

	/**
	 * @return CSurveyTemplateManagerQuestionAssociation
	 */
	public static function fetchSurveyTemplateManagerQuestionAssociation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyTemplateManagerQuestionAssociation', $objDatabase );
	}

	public static function fetchSurveyTemplateManagerQuestionAssociationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_template_manager_question_associations', $objDatabase );
	}

	public static function fetchSurveyTemplateManagerQuestionAssociationById( $intId, $objDatabase ) {
		return self::fetchSurveyTemplateManagerQuestionAssociation( sprintf( 'SELECT * FROM survey_template_manager_question_associations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveyTemplateManagerQuestionAssociationsBySurveyTemplateManagerQuestionId( $intSurveyTemplateManagerQuestionId, $objDatabase ) {
		return self::fetchSurveyTemplateManagerQuestionAssociations( sprintf( 'SELECT * FROM survey_template_manager_question_associations WHERE survey_template_manager_question_id = %d', ( int ) $intSurveyTemplateManagerQuestionId ), $objDatabase );
	}

	public static function fetchSurveyTemplateManagerQuestionAssociationsBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		return self::fetchSurveyTemplateManagerQuestionAssociations( sprintf( 'SELECT * FROM survey_template_manager_question_associations WHERE survey_template_id = %d', ( int ) $intSurveyTemplateId ), $objDatabase );
	}

	public static function fetchSurveyTemplateManagerQuestionAssociationsBySurveyTemplateQuestionId( $intSurveyTemplateQuestionId, $objDatabase ) {
		return self::fetchSurveyTemplateManagerQuestionAssociations( sprintf( 'SELECT * FROM survey_template_manager_question_associations WHERE survey_template_question_id = %d', ( int ) $intSurveyTemplateQuestionId ), $objDatabase );
	}

}
?>