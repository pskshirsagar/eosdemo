<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSharedStoredObjects
 * Do not add any new functions to this class.
 */

class CBaseSharedStoredObjects extends CEosPluralBase {

	/**
	 * @return CSharedStoredObject[]
	 */
	public static function fetchSharedStoredObjects( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CSharedStoredObject::class, $objDatabase );
	}

	/**
	 * @return CSharedStoredObject
	 */
	public static function fetchSharedStoredObject( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CSharedStoredObject::class, $objDatabase );
	}

	public static function fetchSharedStoredObjectCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'shared_stored_objects', $objDatabase );
	}

	public static function fetchSharedStoredObjectById( $intId, $objDatabase ) {
		return self::fetchSharedStoredObject( sprintf( 'SELECT * FROM shared_stored_objects WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchSharedStoredObjectsByReferenceId( $intReferenceId, $objDatabase ) {
		return self::fetchSharedStoredObjects( sprintf( 'SELECT * FROM shared_stored_objects WHERE reference_id = %d', $intReferenceId ), $objDatabase );
	}

}
?>