<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyPhoneNumbers
 * Do not add any new functions to this class.
 */

class CBaseCompanyPhoneNumbers extends CEosPluralBase {

	/**
	 * @return CCompanyPhoneNumber[]
	 */
	public static function fetchCompanyPhoneNumbers( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCompanyPhoneNumber', $objDatabase );
	}

	/**
	 * @return CCompanyPhoneNumber
	 */
	public static function fetchCompanyPhoneNumber( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyPhoneNumber', $objDatabase );
	}

	public static function fetchCompanyPhoneNumberCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_phone_numbers', $objDatabase );
	}

	public static function fetchCompanyPhoneNumberById( $intId, $objDatabase ) {
		return self::fetchCompanyPhoneNumber( sprintf( 'SELECT * FROM company_phone_numbers WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCompanyPhoneNumbersByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyPhoneNumbers( sprintf( 'SELECT * FROM company_phone_numbers WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyPhoneNumbersByPhoneNumberTypeId( $intPhoneNumberTypeId, $objDatabase ) {
		return self::fetchCompanyPhoneNumbers( sprintf( 'SELECT * FROM company_phone_numbers WHERE phone_number_type_id = %d', ( int ) $intPhoneNumberTypeId ), $objDatabase );
	}

}
?>