<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyPaymentTransactions
 * Do not add any new functions to this class.
 */

class CBaseCompanyPaymentTransactions extends CEosPluralBase {

	/**
	 * @return CCompanyPaymentTransaction[]
	 */
	public static function fetchCompanyPaymentTransactions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCompanyPaymentTransaction', $objDatabase );
	}

	/**
	 * @return CCompanyPaymentTransaction
	 */
	public static function fetchCompanyPaymentTransaction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyPaymentTransaction', $objDatabase );
	}

	public static function fetchCompanyPaymentTransactionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_payment_transactions', $objDatabase );
	}

	public static function fetchCompanyPaymentTransactionById( $intId, $objDatabase ) {
		return self::fetchCompanyPaymentTransaction( sprintf( 'SELECT * FROM company_payment_transactions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCompanyPaymentTransactionsByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyPaymentTransactions( sprintf( 'SELECT * FROM company_payment_transactions WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyPaymentTransactionsByAccountId( $intAccountId, $objDatabase ) {
		return self::fetchCompanyPaymentTransactions( sprintf( 'SELECT * FROM company_payment_transactions WHERE account_id = %d', ( int ) $intAccountId ), $objDatabase );
	}

	public static function fetchCompanyPaymentTransactionsByCompanyMerchantAccountId( $intCompanyMerchantAccountId, $objDatabase ) {
		return self::fetchCompanyPaymentTransactions( sprintf( 'SELECT * FROM company_payment_transactions WHERE company_merchant_account_id = %d', ( int ) $intCompanyMerchantAccountId ), $objDatabase );
	}

	public static function fetchCompanyPaymentTransactionsByCompanyPaymentId( $intCompanyPaymentId, $objDatabase ) {
		return self::fetchCompanyPaymentTransactions( sprintf( 'SELECT * FROM company_payment_transactions WHERE company_payment_id = %d', ( int ) $intCompanyPaymentId ), $objDatabase );
	}

	public static function fetchCompanyPaymentTransactionsByPaymentTransactionTypeId( $intPaymentTransactionTypeId, $objDatabase ) {
		return self::fetchCompanyPaymentTransactions( sprintf( 'SELECT * FROM company_payment_transactions WHERE payment_transaction_type_id = %d', ( int ) $intPaymentTransactionTypeId ), $objDatabase );
	}

}
?>