<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeCallcenterStates
 * Do not add any new functions to this class.
 */

class CBaseEmployeeCallcenterStates extends CEosPluralBase {

	/**
	 * @return CEmployeeCallcenterState[]
	 */
	public static function fetchEmployeeCallcenterStates( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeCallcenterState', $objDatabase );
	}

	/**
	 * @return CEmployeeCallcenterState
	 */
	public static function fetchEmployeeCallcenterState( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeCallcenterState', $objDatabase );
	}

	public static function fetchEmployeeCallcenterStateCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_callcenter_states', $objDatabase );
	}

	public static function fetchEmployeeCallcenterStateById( $intId, $objDatabase ) {
		return self::fetchEmployeeCallcenterState( sprintf( 'SELECT * FROM employee_callcenter_states WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeCallcenterStatesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeCallcenterStates( sprintf( 'SELECT * FROM employee_callcenter_states WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>