<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskApprovalTypes
 * Do not add any new functions to this class.
 */

class CBaseTaskApprovalTypes extends CEosPluralBase {

	/**
	 * @return CTaskApprovalType[]
	 */
	public static function fetchTaskApprovalTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTaskApprovalType::class, $objDatabase );
	}

	/**
	 * @return CTaskApprovalType
	 */
	public static function fetchTaskApprovalType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaskApprovalType::class, $objDatabase );
	}

	public static function fetchTaskApprovalTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_approval_types', $objDatabase );
	}

	public static function fetchTaskApprovalTypeById( $intId, $objDatabase ) {
		return self::fetchTaskApprovalType( sprintf( 'SELECT * FROM task_approval_types WHERE id = %d', $intId ), $objDatabase );
	}

}
?>