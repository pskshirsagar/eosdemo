<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyValueNominationWinners
 * Do not add any new functions to this class.
 */

class CBaseCompanyValueNominationWinners extends CEosPluralBase {

	/**
	 * @return CCompanyValueNominationWinner[]
	 */
	public static function fetchCompanyValueNominationWinners( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CCompanyValueNominationWinner::class, $objDatabase );
	}

	/**
	 * @return CCompanyValueNominationWinner
	 */
	public static function fetchCompanyValueNominationWinner( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CCompanyValueNominationWinner::class, $objDatabase );
	}

	public static function fetchCompanyValueNominationWinnerCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_value_nomination_winners', $objDatabase );
	}

	public static function fetchCompanyValueNominationWinnerById( $intId, $objDatabase ) {
		return self::fetchCompanyValueNominationWinner( sprintf( 'SELECT * FROM company_value_nomination_winners WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCompanyValueNominationWinnersByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchCompanyValueNominationWinners( sprintf( 'SELECT * FROM company_value_nomination_winners WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchCompanyValueNominationWinnersByCompanyValueId( $intCompanyValueId, $objDatabase ) {
		return self::fetchCompanyValueNominationWinners( sprintf( 'SELECT * FROM company_value_nomination_winners WHERE company_value_id = %d', ( int ) $intCompanyValueId ), $objDatabase );
	}

	public static function fetchCompanyValueNominationWinnersByCompanyValueNominationBatchId( $intCompanyValueNominationBatchId, $objDatabase ) {
		return self::fetchCompanyValueNominationWinners( sprintf( 'SELECT * FROM company_value_nomination_winners WHERE company_value_nomination_batch_id = %d', ( int ) $intCompanyValueNominationBatchId ), $objDatabase );
	}

}
?>