<?php

class CBaseEmployeeApplicationLog extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_application_logs';

	protected $m_intId;
	protected $m_intEmployeeApplicationEmailCount;
	protected $m_intEmployeeApplicationUpdateCount;
	protected $m_intEmployeeApplicationDeleteCount;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_application_email_count'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationEmailCount', trim( $arrValues['employee_application_email_count'] ) ); elseif( isset( $arrValues['employee_application_email_count'] ) ) $this->setEmployeeApplicationEmailCount( $arrValues['employee_application_email_count'] );
		if( isset( $arrValues['employee_application_update_count'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationUpdateCount', trim( $arrValues['employee_application_update_count'] ) ); elseif( isset( $arrValues['employee_application_update_count'] ) ) $this->setEmployeeApplicationUpdateCount( $arrValues['employee_application_update_count'] );
		if( isset( $arrValues['employee_application_delete_count'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationDeleteCount', trim( $arrValues['employee_application_delete_count'] ) ); elseif( isset( $arrValues['employee_application_delete_count'] ) ) $this->setEmployeeApplicationDeleteCount( $arrValues['employee_application_delete_count'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeApplicationEmailCount( $intEmployeeApplicationEmailCount ) {
		$this->set( 'm_intEmployeeApplicationEmailCount', CStrings::strToIntDef( $intEmployeeApplicationEmailCount, NULL, false ) );
	}

	public function getEmployeeApplicationEmailCount() {
		return $this->m_intEmployeeApplicationEmailCount;
	}

	public function sqlEmployeeApplicationEmailCount() {
		return ( true == isset( $this->m_intEmployeeApplicationEmailCount ) ) ? ( string ) $this->m_intEmployeeApplicationEmailCount : 'NULL';
	}

	public function setEmployeeApplicationUpdateCount( $intEmployeeApplicationUpdateCount ) {
		$this->set( 'm_intEmployeeApplicationUpdateCount', CStrings::strToIntDef( $intEmployeeApplicationUpdateCount, NULL, false ) );
	}

	public function getEmployeeApplicationUpdateCount() {
		return $this->m_intEmployeeApplicationUpdateCount;
	}

	public function sqlEmployeeApplicationUpdateCount() {
		return ( true == isset( $this->m_intEmployeeApplicationUpdateCount ) ) ? ( string ) $this->m_intEmployeeApplicationUpdateCount : 'NULL';
	}

	public function setEmployeeApplicationDeleteCount( $intEmployeeApplicationDeleteCount ) {
		$this->set( 'm_intEmployeeApplicationDeleteCount', CStrings::strToIntDef( $intEmployeeApplicationDeleteCount, NULL, false ) );
	}

	public function getEmployeeApplicationDeleteCount() {
		return $this->m_intEmployeeApplicationDeleteCount;
	}

	public function sqlEmployeeApplicationDeleteCount() {
		return ( true == isset( $this->m_intEmployeeApplicationDeleteCount ) ) ? ( string ) $this->m_intEmployeeApplicationDeleteCount : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_application_email_count, employee_application_update_count, employee_application_delete_count, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeApplicationEmailCount() . ', ' .
 						$this->sqlEmployeeApplicationUpdateCount() . ', ' .
 						$this->sqlEmployeeApplicationDeleteCount() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_email_count = ' . $this->sqlEmployeeApplicationEmailCount() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationEmailCount', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_email_count = ' . $this->sqlEmployeeApplicationEmailCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_update_count = ' . $this->sqlEmployeeApplicationUpdateCount() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationUpdateCount', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_update_count = ' . $this->sqlEmployeeApplicationUpdateCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_delete_count = ' . $this->sqlEmployeeApplicationDeleteCount() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationDeleteCount', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_delete_count = ' . $this->sqlEmployeeApplicationDeleteCount() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_application_email_count' => $this->getEmployeeApplicationEmailCount(),
			'employee_application_update_count' => $this->getEmployeeApplicationUpdateCount(),
			'employee_application_delete_count' => $this->getEmployeeApplicationDeleteCount(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>