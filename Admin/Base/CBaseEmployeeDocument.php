<?php

class CBaseEmployeeDocument extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.employee_documents';

	protected $m_intId;
	protected $m_strName;
	protected $m_strNote;
	protected $m_intEmployeeDocumentTypeId;
	protected $m_intEmployeeDocumentCategoryId;
	protected $m_intEmployeeId;
	protected $m_intFileExtensionId;
	protected $m_boolIsPublished;
	protected $m_intOrderNum;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeniedBy;
	protected $m_strDeniedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_boolPartiallyApproved;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;
		$this->m_intOrderNum = '0';
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';
		$this->m_boolPartiallyApproved = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( $arrValues['note'] ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( $arrValues['note'] );
		if( isset( $arrValues['employee_document_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeDocumentTypeId', trim( $arrValues['employee_document_type_id'] ) ); elseif( isset( $arrValues['employee_document_type_id'] ) ) $this->setEmployeeDocumentTypeId( $arrValues['employee_document_type_id'] );
		if( isset( $arrValues['employee_document_category_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeDocumentCategoryId', trim( $arrValues['employee_document_category_id'] ) ); elseif( isset( $arrValues['employee_document_category_id'] ) ) $this->setEmployeeDocumentCategoryId( $arrValues['employee_document_category_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['file_extension_id'] ) && $boolDirectSet ) $this->set( 'm_intFileExtensionId', trim( $arrValues['file_extension_id'] ) ); elseif( isset( $arrValues['file_extension_id'] ) ) $this->setFileExtensionId( $arrValues['file_extension_id'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['denied_by'] ) && $boolDirectSet ) $this->set( 'm_intDeniedBy', trim( $arrValues['denied_by'] ) ); elseif( isset( $arrValues['denied_by'] ) ) $this->setDeniedBy( $arrValues['denied_by'] );
		if( isset( $arrValues['denied_on'] ) && $boolDirectSet ) $this->set( 'm_strDeniedOn', trim( $arrValues['denied_on'] ) ); elseif( isset( $arrValues['denied_on'] ) ) $this->setDeniedOn( $arrValues['denied_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['partially_approved'] ) && $boolDirectSet ) $this->set( 'm_boolPartiallyApproved', trim( stripcslashes( $arrValues['partially_approved'] ) ) ); elseif( isset( $arrValues['partially_approved'] ) ) $this->setPartiallyApproved( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['partially_approved'] ) : $arrValues['partially_approved'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 250, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, 250, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNote ) : '\'' . addslashes( $this->m_strNote ) . '\'' ) : 'NULL';
	}

	public function setEmployeeDocumentTypeId( $intEmployeeDocumentTypeId ) {
		$this->set( 'm_intEmployeeDocumentTypeId', CStrings::strToIntDef( $intEmployeeDocumentTypeId, NULL, false ) );
	}

	public function getEmployeeDocumentTypeId() {
		return $this->m_intEmployeeDocumentTypeId;
	}

	public function sqlEmployeeDocumentTypeId() {
		return ( true == isset( $this->m_intEmployeeDocumentTypeId ) ) ? ( string ) $this->m_intEmployeeDocumentTypeId : 'NULL';
	}

	public function setEmployeeDocumentCategoryId( $intEmployeeDocumentCategoryId ) {
		$this->set( 'm_intEmployeeDocumentCategoryId', CStrings::strToIntDef( $intEmployeeDocumentCategoryId, NULL, false ) );
	}

	public function getEmployeeDocumentCategoryId() {
		return $this->m_intEmployeeDocumentCategoryId;
	}

	public function sqlEmployeeDocumentCategoryId() {
		return ( true == isset( $this->m_intEmployeeDocumentCategoryId ) ) ? ( string ) $this->m_intEmployeeDocumentCategoryId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setFileExtensionId( $intFileExtensionId ) {
		$this->set( 'm_intFileExtensionId', CStrings::strToIntDef( $intFileExtensionId, NULL, false ) );
	}

	public function getFileExtensionId() {
		return $this->m_intFileExtensionId;
	}

	public function sqlFileExtensionId() {
		return ( true == isset( $this->m_intFileExtensionId ) ) ? ( string ) $this->m_intFileExtensionId : 'NULL';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeniedBy( $intDeniedBy ) {
		$this->set( 'm_intDeniedBy', CStrings::strToIntDef( $intDeniedBy, NULL, false ) );
	}

	public function getDeniedBy() {
		return $this->m_intDeniedBy;
	}

	public function sqlDeniedBy() {
		return ( true == isset( $this->m_intDeniedBy ) ) ? ( string ) $this->m_intDeniedBy : 'NULL';
	}

	public function setDeniedOn( $strDeniedOn ) {
		$this->set( 'm_strDeniedOn', CStrings::strTrimDef( $strDeniedOn, -1, NULL, true ) );
	}

	public function getDeniedOn() {
		return $this->m_strDeniedOn;
	}

	public function sqlDeniedOn() {
		return ( true == isset( $this->m_strDeniedOn ) ) ? '\'' . $this->m_strDeniedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPartiallyApproved( $boolPartiallyApproved ) {
		$this->set( 'm_boolPartiallyApproved', CStrings::strToBool( $boolPartiallyApproved ) );
	}

	public function getPartiallyApproved() {
		return $this->m_boolPartiallyApproved;
	}

	public function sqlPartiallyApproved() {
		return ( true == isset( $this->m_boolPartiallyApproved ) ) ? '\'' . ( true == ( bool ) $this->m_boolPartiallyApproved ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, name, note, employee_document_type_id, employee_document_category_id, employee_id, file_extension_id, is_published, order_num, approved_by, approved_on, denied_by, denied_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, details, partially_approved )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlNote() . ', ' .
		          $this->sqlEmployeeDocumentTypeId() . ', ' .
		          $this->sqlEmployeeDocumentCategoryId() . ', ' .
		          $this->sqlEmployeeId() . ', ' .
		          $this->sqlFileExtensionId() . ', ' .
		          $this->sqlIsPublished() . ', ' .
		          $this->sqlOrderNum() . ', ' .
		          $this->sqlApprovedBy() . ', ' .
		          $this->sqlApprovedOn() . ', ' .
		          $this->sqlDeniedBy() . ', ' .
		          $this->sqlDeniedOn() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlPartiallyApproved() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote(). ',' ; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_document_type_id = ' . $this->sqlEmployeeDocumentTypeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeDocumentTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_document_type_id = ' . $this->sqlEmployeeDocumentTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_document_category_id = ' . $this->sqlEmployeeDocumentCategoryId(). ',' ; } elseif( true == array_key_exists( 'EmployeeDocumentCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' employee_document_category_id = ' . $this->sqlEmployeeDocumentCategoryId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_extension_id = ' . $this->sqlFileExtensionId(). ',' ; } elseif( true == array_key_exists( 'FileExtensionId', $this->getChangedColumns() ) ) { $strSql .= ' file_extension_id = ' . $this->sqlFileExtensionId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy(). ',' ; } elseif( true == array_key_exists( 'DeniedBy', $this->getChangedColumns() ) ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn(). ',' ; } elseif( true == array_key_exists( 'DeniedOn', $this->getChangedColumns() ) ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' partially_approved = ' . $this->sqlPartiallyApproved(). ',' ; } elseif( true == array_key_exists( 'PartiallyApproved', $this->getChangedColumns() ) ) { $strSql .= ' partially_approved = ' . $this->sqlPartiallyApproved() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'name' => $this->getName(),
			'note' => $this->getNote(),
			'employee_document_type_id' => $this->getEmployeeDocumentTypeId(),
			'employee_document_category_id' => $this->getEmployeeDocumentCategoryId(),
			'employee_id' => $this->getEmployeeId(),
			'file_extension_id' => $this->getFileExtensionId(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'denied_by' => $this->getDeniedBy(),
			'denied_on' => $this->getDeniedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'partially_approved' => $this->getPartiallyApproved()
		);
	}

}
?>