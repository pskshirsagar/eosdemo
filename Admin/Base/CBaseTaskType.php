<?php

class CBaseTaskType extends CEosSingularBase {

	const TABLE_NAME = 'public.task_types';

	protected $m_intId;
	protected $m_intParentTaskTypeId;
	protected $m_intDefaultTaskPriorityId;
	protected $m_intDepartmentId;
	protected $m_intUserId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intDaysAllotted;
	protected $m_intIsClientTask;
	protected $m_intIsSystem;
	protected $m_intIsSupport;
	protected $m_intIsSales;
	protected $m_intIsExternal;
	protected $m_intEnforcePermissions;
	protected $m_intOrderNum;
	protected $m_intDisabledBy;
	protected $m_strDisabledOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsClientTask = '0';
		$this->m_intIsSystem = '0';
		$this->m_intIsSupport = '0';
		$this->m_intIsSales = '0';
		$this->m_intIsExternal = '0';
		$this->m_intEnforcePermissions = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['parent_task_type_id'] ) && $boolDirectSet ) $this->set( 'm_intParentTaskTypeId', trim( $arrValues['parent_task_type_id'] ) ); elseif( isset( $arrValues['parent_task_type_id'] ) ) $this->setParentTaskTypeId( $arrValues['parent_task_type_id'] );
		if( isset( $arrValues['default_task_priority_id'] ) && $boolDirectSet ) $this->set( 'm_intDefaultTaskPriorityId', trim( $arrValues['default_task_priority_id'] ) ); elseif( isset( $arrValues['default_task_priority_id'] ) ) $this->setDefaultTaskPriorityId( $arrValues['default_task_priority_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['days_allotted'] ) && $boolDirectSet ) $this->set( 'm_intDaysAllotted', trim( $arrValues['days_allotted'] ) ); elseif( isset( $arrValues['days_allotted'] ) ) $this->setDaysAllotted( $arrValues['days_allotted'] );
		if( isset( $arrValues['is_client_task'] ) && $boolDirectSet ) $this->set( 'm_intIsClientTask', trim( $arrValues['is_client_task'] ) ); elseif( isset( $arrValues['is_client_task'] ) ) $this->setIsClientTask( $arrValues['is_client_task'] );
		if( isset( $arrValues['is_system'] ) && $boolDirectSet ) $this->set( 'm_intIsSystem', trim( $arrValues['is_system'] ) ); elseif( isset( $arrValues['is_system'] ) ) $this->setIsSystem( $arrValues['is_system'] );
		if( isset( $arrValues['is_support'] ) && $boolDirectSet ) $this->set( 'm_intIsSupport', trim( $arrValues['is_support'] ) ); elseif( isset( $arrValues['is_support'] ) ) $this->setIsSupport( $arrValues['is_support'] );
		if( isset( $arrValues['is_sales'] ) && $boolDirectSet ) $this->set( 'm_intIsSales', trim( $arrValues['is_sales'] ) ); elseif( isset( $arrValues['is_sales'] ) ) $this->setIsSales( $arrValues['is_sales'] );
		if( isset( $arrValues['is_external'] ) && $boolDirectSet ) $this->set( 'm_intIsExternal', trim( $arrValues['is_external'] ) ); elseif( isset( $arrValues['is_external'] ) ) $this->setIsExternal( $arrValues['is_external'] );
		if( isset( $arrValues['enforce_permissions'] ) && $boolDirectSet ) $this->set( 'm_intEnforcePermissions', trim( $arrValues['enforce_permissions'] ) ); elseif( isset( $arrValues['enforce_permissions'] ) ) $this->setEnforcePermissions( $arrValues['enforce_permissions'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['disabled_by'] ) && $boolDirectSet ) $this->set( 'm_intDisabledBy', trim( $arrValues['disabled_by'] ) ); elseif( isset( $arrValues['disabled_by'] ) ) $this->setDisabledBy( $arrValues['disabled_by'] );
		if( isset( $arrValues['disabled_on'] ) && $boolDirectSet ) $this->set( 'm_strDisabledOn', trim( $arrValues['disabled_on'] ) ); elseif( isset( $arrValues['disabled_on'] ) ) $this->setDisabledOn( $arrValues['disabled_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setParentTaskTypeId( $intParentTaskTypeId ) {
		$this->set( 'm_intParentTaskTypeId', CStrings::strToIntDef( $intParentTaskTypeId, NULL, false ) );
	}

	public function getParentTaskTypeId() {
		return $this->m_intParentTaskTypeId;
	}

	public function sqlParentTaskTypeId() {
		return ( true == isset( $this->m_intParentTaskTypeId ) ) ? ( string ) $this->m_intParentTaskTypeId : 'NULL';
	}

	public function setDefaultTaskPriorityId( $intDefaultTaskPriorityId ) {
		$this->set( 'm_intDefaultTaskPriorityId', CStrings::strToIntDef( $intDefaultTaskPriorityId, NULL, false ) );
	}

	public function getDefaultTaskPriorityId() {
		return $this->m_intDefaultTaskPriorityId;
	}

	public function sqlDefaultTaskPriorityId() {
		return ( true == isset( $this->m_intDefaultTaskPriorityId ) ) ? ( string ) $this->m_intDefaultTaskPriorityId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setDaysAllotted( $intDaysAllotted ) {
		$this->set( 'm_intDaysAllotted', CStrings::strToIntDef( $intDaysAllotted, NULL, false ) );
	}

	public function getDaysAllotted() {
		return $this->m_intDaysAllotted;
	}

	public function sqlDaysAllotted() {
		return ( true == isset( $this->m_intDaysAllotted ) ) ? ( string ) $this->m_intDaysAllotted : 'NULL';
	}

	public function setIsClientTask( $intIsClientTask ) {
		$this->set( 'm_intIsClientTask', CStrings::strToIntDef( $intIsClientTask, NULL, false ) );
	}

	public function getIsClientTask() {
		return $this->m_intIsClientTask;
	}

	public function sqlIsClientTask() {
		return ( true == isset( $this->m_intIsClientTask ) ) ? ( string ) $this->m_intIsClientTask : '0';
	}

	public function setIsSystem( $intIsSystem ) {
		$this->set( 'm_intIsSystem', CStrings::strToIntDef( $intIsSystem, NULL, false ) );
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function sqlIsSystem() {
		return ( true == isset( $this->m_intIsSystem ) ) ? ( string ) $this->m_intIsSystem : '0';
	}

	public function setIsSupport( $intIsSupport ) {
		$this->set( 'm_intIsSupport', CStrings::strToIntDef( $intIsSupport, NULL, false ) );
	}

	public function getIsSupport() {
		return $this->m_intIsSupport;
	}

	public function sqlIsSupport() {
		return ( true == isset( $this->m_intIsSupport ) ) ? ( string ) $this->m_intIsSupport : '0';
	}

	public function setIsSales( $intIsSales ) {
		$this->set( 'm_intIsSales', CStrings::strToIntDef( $intIsSales, NULL, false ) );
	}

	public function getIsSales() {
		return $this->m_intIsSales;
	}

	public function sqlIsSales() {
		return ( true == isset( $this->m_intIsSales ) ) ? ( string ) $this->m_intIsSales : '0';
	}

	public function setIsExternal( $intIsExternal ) {
		$this->set( 'm_intIsExternal', CStrings::strToIntDef( $intIsExternal, NULL, false ) );
	}

	public function getIsExternal() {
		return $this->m_intIsExternal;
	}

	public function sqlIsExternal() {
		return ( true == isset( $this->m_intIsExternal ) ) ? ( string ) $this->m_intIsExternal : '0';
	}

	public function setEnforcePermissions( $intEnforcePermissions ) {
		$this->set( 'm_intEnforcePermissions', CStrings::strToIntDef( $intEnforcePermissions, NULL, false ) );
	}

	public function getEnforcePermissions() {
		return $this->m_intEnforcePermissions;
	}

	public function sqlEnforcePermissions() {
		return ( true == isset( $this->m_intEnforcePermissions ) ) ? ( string ) $this->m_intEnforcePermissions : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDisabledBy( $intDisabledBy ) {
		$this->set( 'm_intDisabledBy', CStrings::strToIntDef( $intDisabledBy, NULL, false ) );
	}

	public function getDisabledBy() {
		return $this->m_intDisabledBy;
	}

	public function sqlDisabledBy() {
		return ( true == isset( $this->m_intDisabledBy ) ) ? ( string ) $this->m_intDisabledBy : 'NULL';
	}

	public function setDisabledOn( $strDisabledOn ) {
		$this->set( 'm_strDisabledOn', CStrings::strTrimDef( $strDisabledOn, -1, NULL, true ) );
	}

	public function getDisabledOn() {
		return $this->m_strDisabledOn;
	}

	public function sqlDisabledOn() {
		return ( true == isset( $this->m_strDisabledOn ) ) ? '\'' . $this->m_strDisabledOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, parent_task_type_id, default_task_priority_id, department_id, user_id, name, description, days_allotted, is_client_task, is_system, is_support, is_sales, is_external, enforce_permissions, order_num, disabled_by, disabled_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlParentTaskTypeId() . ', ' .
 						$this->sqlDefaultTaskPriorityId() . ', ' .
 						$this->sqlDepartmentId() . ', ' .
 						$this->sqlUserId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlDaysAllotted() . ', ' .
 						$this->sqlIsClientTask() . ', ' .
 						$this->sqlIsSystem() . ', ' .
 						$this->sqlIsSupport() . ', ' .
 						$this->sqlIsSales() . ', ' .
 						$this->sqlIsExternal() . ', ' .
 						$this->sqlEnforcePermissions() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlDisabledBy() . ', ' .
 						$this->sqlDisabledOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_task_type_id = ' . $this->sqlParentTaskTypeId() . ','; } elseif( true == array_key_exists( 'ParentTaskTypeId', $this->getChangedColumns() ) ) { $strSql .= ' parent_task_type_id = ' . $this->sqlParentTaskTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_task_priority_id = ' . $this->sqlDefaultTaskPriorityId() . ','; } elseif( true == array_key_exists( 'DefaultTaskPriorityId', $this->getChangedColumns() ) ) { $strSql .= ' default_task_priority_id = ' . $this->sqlDefaultTaskPriorityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; } elseif( true == array_key_exists( 'UserId', $this->getChangedColumns() ) ) { $strSql .= ' user_id = ' . $this->sqlUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' days_allotted = ' . $this->sqlDaysAllotted() . ','; } elseif( true == array_key_exists( 'DaysAllotted', $this->getChangedColumns() ) ) { $strSql .= ' days_allotted = ' . $this->sqlDaysAllotted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_client_task = ' . $this->sqlIsClientTask() . ','; } elseif( true == array_key_exists( 'IsClientTask', $this->getChangedColumns() ) ) { $strSql .= ' is_client_task = ' . $this->sqlIsClientTask() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; } elseif( true == array_key_exists( 'IsSystem', $this->getChangedColumns() ) ) { $strSql .= ' is_system = ' . $this->sqlIsSystem() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_support = ' . $this->sqlIsSupport() . ','; } elseif( true == array_key_exists( 'IsSupport', $this->getChangedColumns() ) ) { $strSql .= ' is_support = ' . $this->sqlIsSupport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_sales = ' . $this->sqlIsSales() . ','; } elseif( true == array_key_exists( 'IsSales', $this->getChangedColumns() ) ) { $strSql .= ' is_sales = ' . $this->sqlIsSales() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_external = ' . $this->sqlIsExternal() . ','; } elseif( true == array_key_exists( 'IsExternal', $this->getChangedColumns() ) ) { $strSql .= ' is_external = ' . $this->sqlIsExternal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' enforce_permissions = ' . $this->sqlEnforcePermissions() . ','; } elseif( true == array_key_exists( 'EnforcePermissions', $this->getChangedColumns() ) ) { $strSql .= ' enforce_permissions = ' . $this->sqlEnforcePermissions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; } elseif( true == array_key_exists( 'DisabledBy', $this->getChangedColumns() ) ) { $strSql .= ' disabled_by = ' . $this->sqlDisabledBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; } elseif( true == array_key_exists( 'DisabledOn', $this->getChangedColumns() ) ) { $strSql .= ' disabled_on = ' . $this->sqlDisabledOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'parent_task_type_id' => $this->getParentTaskTypeId(),
			'default_task_priority_id' => $this->getDefaultTaskPriorityId(),
			'department_id' => $this->getDepartmentId(),
			'user_id' => $this->getUserId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'days_allotted' => $this->getDaysAllotted(),
			'is_client_task' => $this->getIsClientTask(),
			'is_system' => $this->getIsSystem(),
			'is_support' => $this->getIsSupport(),
			'is_sales' => $this->getIsSales(),
			'is_external' => $this->getIsExternal(),
			'enforce_permissions' => $this->getEnforcePermissions(),
			'order_num' => $this->getOrderNum(),
			'disabled_by' => $this->getDisabledBy(),
			'disabled_on' => $this->getDisabledOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>