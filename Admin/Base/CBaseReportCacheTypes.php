<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportCacheTypes
 * Do not add any new functions to this class.
 */

class CBaseReportCacheTypes extends CEosPluralBase {

	/**
	 * @return CReportCacheType[]
	 */
	public static function fetchReportCacheTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportCacheType::class, $objDatabase );
	}

	/**
	 * @return CReportCacheType
	 */
	public static function fetchReportCacheType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportCacheType::class, $objDatabase );
	}

	public static function fetchReportCacheTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_cache_types', $objDatabase );
	}

	public static function fetchReportCacheTypeById( $intId, $objDatabase ) {
		return self::fetchReportCacheType( sprintf( 'SELECT * FROM report_cache_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>