<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CRevenueTypes
 * Do not add any new functions to this class.
 */

class CBaseRevenueTypes extends CEosPluralBase {

	/**
	 * @return CRevenueType[]
	 */
	public static function fetchRevenueTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CRevenueType', $objDatabase );
	}

	/**
	 * @return CRevenueType
	 */
	public static function fetchRevenueType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRevenueType', $objDatabase );
	}

	public static function fetchRevenueTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'revenue_types', $objDatabase );
	}

	public static function fetchRevenueTypeById( $intId, $objDatabase ) {
		return self::fetchRevenueType( sprintf( 'SELECT * FROM revenue_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>