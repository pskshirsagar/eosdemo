<?php

class CBaseContractLikelihoodSetting extends CEosSingularBase {

	const TABLE_NAME = 'public.contract_likelihood_settings';

	protected $m_intId;
	protected $m_intContractStatusTypeId;
	protected $m_intBasePercentage;
	protected $m_intDaysAfterStart;
	protected $m_intAdditionalDaysAfterStart;
	protected $m_fltAcvLimitPerAdditionalDays;
	protected $m_fltLossPercentage;
	protected $m_intDaysPerLossPercentage;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['contract_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intContractStatusTypeId', trim( $arrValues['contract_status_type_id'] ) ); elseif( isset( $arrValues['contract_status_type_id'] ) ) $this->setContractStatusTypeId( $arrValues['contract_status_type_id'] );
		if( isset( $arrValues['base_percentage'] ) && $boolDirectSet ) $this->set( 'm_intBasePercentage', trim( $arrValues['base_percentage'] ) ); elseif( isset( $arrValues['base_percentage'] ) ) $this->setBasePercentage( $arrValues['base_percentage'] );
		if( isset( $arrValues['days_after_start'] ) && $boolDirectSet ) $this->set( 'm_intDaysAfterStart', trim( $arrValues['days_after_start'] ) ); elseif( isset( $arrValues['days_after_start'] ) ) $this->setDaysAfterStart( $arrValues['days_after_start'] );
		if( isset( $arrValues['additional_days_after_start'] ) && $boolDirectSet ) $this->set( 'm_intAdditionalDaysAfterStart', trim( $arrValues['additional_days_after_start'] ) ); elseif( isset( $arrValues['additional_days_after_start'] ) ) $this->setAdditionalDaysAfterStart( $arrValues['additional_days_after_start'] );
		if( isset( $arrValues['acv_limit_per_additional_days'] ) && $boolDirectSet ) $this->set( 'm_fltAcvLimitPerAdditionalDays', trim( $arrValues['acv_limit_per_additional_days'] ) ); elseif( isset( $arrValues['acv_limit_per_additional_days'] ) ) $this->setAcvLimitPerAdditionalDays( $arrValues['acv_limit_per_additional_days'] );
		if( isset( $arrValues['loss_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltLossPercentage', trim( $arrValues['loss_percentage'] ) ); elseif( isset( $arrValues['loss_percentage'] ) ) $this->setLossPercentage( $arrValues['loss_percentage'] );
		if( isset( $arrValues['days_per_loss_percentage'] ) && $boolDirectSet ) $this->set( 'm_intDaysPerLossPercentage', trim( $arrValues['days_per_loss_percentage'] ) ); elseif( isset( $arrValues['days_per_loss_percentage'] ) ) $this->setDaysPerLossPercentage( $arrValues['days_per_loss_percentage'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setContractStatusTypeId( $intContractStatusTypeId ) {
		$this->set( 'm_intContractStatusTypeId', CStrings::strToIntDef( $intContractStatusTypeId, NULL, false ) );
	}

	public function getContractStatusTypeId() {
		return $this->m_intContractStatusTypeId;
	}

	public function sqlContractStatusTypeId() {
		return ( true == isset( $this->m_intContractStatusTypeId ) ) ? ( string ) $this->m_intContractStatusTypeId : 'NULL';
	}

	public function setBasePercentage( $intBasePercentage ) {
		$this->set( 'm_intBasePercentage', CStrings::strToIntDef( $intBasePercentage, NULL, false ) );
	}

	public function getBasePercentage() {
		return $this->m_intBasePercentage;
	}

	public function sqlBasePercentage() {
		return ( true == isset( $this->m_intBasePercentage ) ) ? ( string ) $this->m_intBasePercentage : 'NULL';
	}

	public function setDaysAfterStart( $intDaysAfterStart ) {
		$this->set( 'm_intDaysAfterStart', CStrings::strToIntDef( $intDaysAfterStart, NULL, false ) );
	}

	public function getDaysAfterStart() {
		return $this->m_intDaysAfterStart;
	}

	public function sqlDaysAfterStart() {
		return ( true == isset( $this->m_intDaysAfterStart ) ) ? ( string ) $this->m_intDaysAfterStart : 'NULL';
	}

	public function setAdditionalDaysAfterStart( $intAdditionalDaysAfterStart ) {
		$this->set( 'm_intAdditionalDaysAfterStart', CStrings::strToIntDef( $intAdditionalDaysAfterStart, NULL, false ) );
	}

	public function getAdditionalDaysAfterStart() {
		return $this->m_intAdditionalDaysAfterStart;
	}

	public function sqlAdditionalDaysAfterStart() {
		return ( true == isset( $this->m_intAdditionalDaysAfterStart ) ) ? ( string ) $this->m_intAdditionalDaysAfterStart : 'NULL';
	}

	public function setAcvLimitPerAdditionalDays( $fltAcvLimitPerAdditionalDays ) {
		$this->set( 'm_fltAcvLimitPerAdditionalDays', CStrings::strToFloatDef( $fltAcvLimitPerAdditionalDays, NULL, false, 2 ) );
	}

	public function getAcvLimitPerAdditionalDays() {
		return $this->m_fltAcvLimitPerAdditionalDays;
	}

	public function sqlAcvLimitPerAdditionalDays() {
		return ( true == isset( $this->m_fltAcvLimitPerAdditionalDays ) ) ? ( string ) $this->m_fltAcvLimitPerAdditionalDays : 'NULL';
	}

	public function setLossPercentage( $fltLossPercentage ) {
		$this->set( 'm_fltLossPercentage', CStrings::strToFloatDef( $fltLossPercentage, NULL, false, 2 ) );
	}

	public function getLossPercentage() {
		return $this->m_fltLossPercentage;
	}

	public function sqlLossPercentage() {
		return ( true == isset( $this->m_fltLossPercentage ) ) ? ( string ) $this->m_fltLossPercentage : 'NULL';
	}

	public function setDaysPerLossPercentage( $intDaysPerLossPercentage ) {
		$this->set( 'm_intDaysPerLossPercentage', CStrings::strToIntDef( $intDaysPerLossPercentage, NULL, false ) );
	}

	public function getDaysPerLossPercentage() {
		return $this->m_intDaysPerLossPercentage;
	}

	public function sqlDaysPerLossPercentage() {
		return ( true == isset( $this->m_intDaysPerLossPercentage ) ) ? ( string ) $this->m_intDaysPerLossPercentage : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, contract_status_type_id, base_percentage, days_after_start, additional_days_after_start, acv_limit_per_additional_days, loss_percentage, days_per_loss_percentage, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlContractStatusTypeId() . ', ' .
 						$this->sqlBasePercentage() . ', ' .
 						$this->sqlDaysAfterStart() . ', ' .
 						$this->sqlAdditionalDaysAfterStart() . ', ' .
 						$this->sqlAcvLimitPerAdditionalDays() . ', ' .
 						$this->sqlLossPercentage() . ', ' .
 						$this->sqlDaysPerLossPercentage() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_status_type_id = ' . $this->sqlContractStatusTypeId() . ','; } elseif( true == array_key_exists( 'ContractStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' contract_status_type_id = ' . $this->sqlContractStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' base_percentage = ' . $this->sqlBasePercentage() . ','; } elseif( true == array_key_exists( 'BasePercentage', $this->getChangedColumns() ) ) { $strSql .= ' base_percentage = ' . $this->sqlBasePercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' days_after_start = ' . $this->sqlDaysAfterStart() . ','; } elseif( true == array_key_exists( 'DaysAfterStart', $this->getChangedColumns() ) ) { $strSql .= ' days_after_start = ' . $this->sqlDaysAfterStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' additional_days_after_start = ' . $this->sqlAdditionalDaysAfterStart() . ','; } elseif( true == array_key_exists( 'AdditionalDaysAfterStart', $this->getChangedColumns() ) ) { $strSql .= ' additional_days_after_start = ' . $this->sqlAdditionalDaysAfterStart() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' acv_limit_per_additional_days = ' . $this->sqlAcvLimitPerAdditionalDays() . ','; } elseif( true == array_key_exists( 'AcvLimitPerAdditionalDays', $this->getChangedColumns() ) ) { $strSql .= ' acv_limit_per_additional_days = ' . $this->sqlAcvLimitPerAdditionalDays() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' loss_percentage = ' . $this->sqlLossPercentage() . ','; } elseif( true == array_key_exists( 'LossPercentage', $this->getChangedColumns() ) ) { $strSql .= ' loss_percentage = ' . $this->sqlLossPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' days_per_loss_percentage = ' . $this->sqlDaysPerLossPercentage() . ','; } elseif( true == array_key_exists( 'DaysPerLossPercentage', $this->getChangedColumns() ) ) { $strSql .= ' days_per_loss_percentage = ' . $this->sqlDaysPerLossPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'contract_status_type_id' => $this->getContractStatusTypeId(),
			'base_percentage' => $this->getBasePercentage(),
			'days_after_start' => $this->getDaysAfterStart(),
			'additional_days_after_start' => $this->getAdditionalDaysAfterStart(),
			'acv_limit_per_additional_days' => $this->getAcvLimitPerAdditionalDays(),
			'loss_percentage' => $this->getLossPercentage(),
			'days_per_loss_percentage' => $this->getDaysPerLossPercentage(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>