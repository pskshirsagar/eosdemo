<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeExpenseTypes
 * Do not add any new functions to this class.
 */

class CBaseEmployeeExpenseTypes extends CEosPluralBase {

	/**
	 * @return CEmployeeExpenseType[]
	 */
	public static function fetchEmployeeExpenseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeExpenseType', $objDatabase );
	}

	/**
	 * @return CEmployeeExpenseType
	 */
	public static function fetchEmployeeExpenseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeExpenseType', $objDatabase );
	}

	public static function fetchEmployeeExpenseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_expense_types', $objDatabase );
	}

	public static function fetchEmployeeExpenseTypeById( $intId, $objDatabase ) {
		return self::fetchEmployeeExpenseType( sprintf( 'SELECT * FROM employee_expense_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>