<?php

class CBaseEmployeeVacationRequest extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_vacation_requests';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intVacationTypeId;
	protected $m_strBeginDatetime;
	protected $m_strEndDatetime;
	protected $m_strPaidOffDate;
	protected $m_fltScheduledVacationHours;
	protected $m_intActualVacationHours;
	protected $m_strRequest;
	protected $m_strResponse;
	protected $m_strAdminNotes;
	protected $m_boolIsPaid;
	protected $m_intApprovedBy;
	protected $m_strApprovedOn;
	protected $m_intDeniedBy;
	protected $m_strDeniedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strVacationCategoryType;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPaid = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['vacation_type_id'] ) && $boolDirectSet ) $this->set( 'm_intVacationTypeId', trim( $arrValues['vacation_type_id'] ) ); elseif( isset( $arrValues['vacation_type_id'] ) ) $this->setVacationTypeId( $arrValues['vacation_type_id'] );
		if( isset( $arrValues['begin_datetime'] ) && $boolDirectSet ) $this->set( 'm_strBeginDatetime', trim( $arrValues['begin_datetime'] ) ); elseif( isset( $arrValues['begin_datetime'] ) ) $this->setBeginDatetime( $arrValues['begin_datetime'] );
		if( isset( $arrValues['end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strEndDatetime', trim( $arrValues['end_datetime'] ) ); elseif( isset( $arrValues['end_datetime'] ) ) $this->setEndDatetime( $arrValues['end_datetime'] );
		if( isset( $arrValues['paid_off_date'] ) && $boolDirectSet ) $this->set( 'm_strPaidOffDate', trim( $arrValues['paid_off_date'] ) ); elseif( isset( $arrValues['paid_off_date'] ) ) $this->setPaidOffDate( $arrValues['paid_off_date'] );
		if( isset( $arrValues['scheduled_vacation_hours'] ) && $boolDirectSet ) $this->set( 'm_fltScheduledVacationHours', trim( $arrValues['scheduled_vacation_hours'] ) ); elseif( isset( $arrValues['scheduled_vacation_hours'] ) ) $this->setScheduledVacationHours( $arrValues['scheduled_vacation_hours'] );
		if( isset( $arrValues['actual_vacation_hours'] ) && $boolDirectSet ) $this->set( 'm_intActualVacationHours', trim( $arrValues['actual_vacation_hours'] ) ); elseif( isset( $arrValues['actual_vacation_hours'] ) ) $this->setActualVacationHours( $arrValues['actual_vacation_hours'] );
		if( isset( $arrValues['request'] ) && $boolDirectSet ) $this->set( 'm_strRequest', trim( $arrValues['request'] ) ); elseif( isset( $arrValues['request'] ) ) $this->setRequest( $arrValues['request'] );
		if( isset( $arrValues['response'] ) && $boolDirectSet ) $this->set( 'm_strResponse', trim( $arrValues['response'] ) ); elseif( isset( $arrValues['response'] ) ) $this->setResponse( $arrValues['response'] );
		if( isset( $arrValues['admin_notes'] ) && $boolDirectSet ) $this->set( 'm_strAdminNotes', trim( $arrValues['admin_notes'] ) ); elseif( isset( $arrValues['admin_notes'] ) ) $this->setAdminNotes( $arrValues['admin_notes'] );
		if( isset( $arrValues['is_paid'] ) && $boolDirectSet ) $this->set( 'm_boolIsPaid', trim( stripcslashes( $arrValues['is_paid'] ) ) ); elseif( isset( $arrValues['is_paid'] ) ) $this->setIsPaid( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_paid'] ) : $arrValues['is_paid'] );
		if( isset( $arrValues['approved_by'] ) && $boolDirectSet ) $this->set( 'm_intApprovedBy', trim( $arrValues['approved_by'] ) ); elseif( isset( $arrValues['approved_by'] ) ) $this->setApprovedBy( $arrValues['approved_by'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['denied_by'] ) && $boolDirectSet ) $this->set( 'm_intDeniedBy', trim( $arrValues['denied_by'] ) ); elseif( isset( $arrValues['denied_by'] ) ) $this->setDeniedBy( $arrValues['denied_by'] );
		if( isset( $arrValues['denied_on'] ) && $boolDirectSet ) $this->set( 'm_strDeniedOn', trim( $arrValues['denied_on'] ) ); elseif( isset( $arrValues['denied_on'] ) ) $this->setDeniedOn( $arrValues['denied_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['vacation_category_type'] ) && $boolDirectSet ) $this->set( 'm_strVacationCategoryType', trim( stripcslashes( $arrValues['vacation_category_type'] ) ) ); elseif( isset( $arrValues['vacation_category_type'] ) ) $this->setVacationCategoryType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vacation_category_type'] ) : $arrValues['vacation_category_type'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setVacationTypeId( $intVacationTypeId ) {
		$this->set( 'm_intVacationTypeId', CStrings::strToIntDef( $intVacationTypeId, NULL, false ) );
	}

	public function getVacationTypeId() {
		return $this->m_intVacationTypeId;
	}

	public function sqlVacationTypeId() {
		return ( true == isset( $this->m_intVacationTypeId ) ) ? ( string ) $this->m_intVacationTypeId : 'NULL';
	}

	public function setBeginDatetime( $strBeginDatetime ) {
		$this->set( 'm_strBeginDatetime', CStrings::strTrimDef( $strBeginDatetime, -1, NULL, true ) );
	}

	public function getBeginDatetime() {
		return $this->m_strBeginDatetime;
	}

	public function sqlBeginDatetime() {
		return ( true == isset( $this->m_strBeginDatetime ) ) ? '\'' . $this->m_strBeginDatetime . '\'' : 'NOW()';
	}

	public function setEndDatetime( $strEndDatetime ) {
		$this->set( 'm_strEndDatetime', CStrings::strTrimDef( $strEndDatetime, -1, NULL, true ) );
	}

	public function getEndDatetime() {
		return $this->m_strEndDatetime;
	}

	public function sqlEndDatetime() {
		return ( true == isset( $this->m_strEndDatetime ) ) ? '\'' . $this->m_strEndDatetime . '\'' : 'NULL';
	}

	public function setPaidOffDate( $strPaidOffDate ) {
		$this->set( 'm_strPaidOffDate', CStrings::strTrimDef( $strPaidOffDate, -1, NULL, true ) );
	}

	public function getPaidOffDate() {
		return $this->m_strPaidOffDate;
	}

	public function sqlPaidOffDate() {
		return ( true == isset( $this->m_strPaidOffDate ) ) ? '\'' . $this->m_strPaidOffDate . '\'' : 'NULL';
	}

	public function setScheduledVacationHours( $fltScheduledVacationHours ) {
		$this->set( 'm_fltScheduledVacationHours', CStrings::strToFloatDef( $fltScheduledVacationHours, NULL, false, 1 ) );
	}

	public function getScheduledVacationHours() {
		return $this->m_fltScheduledVacationHours;
	}

	public function sqlScheduledVacationHours() {
		return ( true == isset( $this->m_fltScheduledVacationHours ) ) ? ( string ) $this->m_fltScheduledVacationHours : 'NULL';
	}

	public function setActualVacationHours( $intActualVacationHours ) {
		$this->set( 'm_intActualVacationHours', CStrings::strToIntDef( $intActualVacationHours, NULL, false ) );
	}

	public function getActualVacationHours() {
		return $this->m_intActualVacationHours;
	}

	public function sqlActualVacationHours() {
		return ( true == isset( $this->m_intActualVacationHours ) ) ? ( string ) $this->m_intActualVacationHours : 'NULL';
	}

	public function setRequest( $strRequest ) {
		$this->set( 'm_strRequest', CStrings::strTrimDef( $strRequest, -1, NULL, true ) );
	}

	public function getRequest() {
		return $this->m_strRequest;
	}

	public function sqlRequest() {
		return ( true == isset( $this->m_strRequest ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRequest ) : '\'' . addslashes( $this->m_strRequest ) . '\'' ) : 'NULL';
	}

	public function setResponse( $strResponse ) {
		$this->set( 'm_strResponse', CStrings::strTrimDef( $strResponse, -1, NULL, true ) );
	}

	public function getResponse() {
		return $this->m_strResponse;
	}

	public function sqlResponse() {
		return ( true == isset( $this->m_strResponse ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strResponse ) : '\'' . addslashes( $this->m_strResponse ) . '\'' ) : 'NULL';
	}

	public function setAdminNotes( $strAdminNotes ) {
		$this->set( 'm_strAdminNotes', CStrings::strTrimDef( $strAdminNotes, -1, NULL, true ) );
	}

	public function getAdminNotes() {
		return $this->m_strAdminNotes;
	}

	public function sqlAdminNotes() {
		return ( true == isset( $this->m_strAdminNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strAdminNotes ) : '\'' . addslashes( $this->m_strAdminNotes ) . '\'' ) : 'NULL';
	}

	public function setIsPaid( $boolIsPaid ) {
		$this->set( 'm_boolIsPaid', CStrings::strToBool( $boolIsPaid ) );
	}

	public function getIsPaid() {
		return $this->m_boolIsPaid;
	}

	public function sqlIsPaid() {
		return ( true == isset( $this->m_boolIsPaid ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPaid ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setApprovedBy( $intApprovedBy ) {
		$this->set( 'm_intApprovedBy', CStrings::strToIntDef( $intApprovedBy, NULL, false ) );
	}

	public function getApprovedBy() {
		return $this->m_intApprovedBy;
	}

	public function sqlApprovedBy() {
		return ( true == isset( $this->m_intApprovedBy ) ) ? ( string ) $this->m_intApprovedBy : 'NULL';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setDeniedBy( $intDeniedBy ) {
		$this->set( 'm_intDeniedBy', CStrings::strToIntDef( $intDeniedBy, NULL, false ) );
	}

	public function getDeniedBy() {
		return $this->m_intDeniedBy;
	}

	public function sqlDeniedBy() {
		return ( true == isset( $this->m_intDeniedBy ) ) ? ( string ) $this->m_intDeniedBy : 'NULL';
	}

	public function setDeniedOn( $strDeniedOn ) {
		$this->set( 'm_strDeniedOn', CStrings::strTrimDef( $strDeniedOn, -1, NULL, true ) );
	}

	public function getDeniedOn() {
		return $this->m_strDeniedOn;
	}

	public function sqlDeniedOn() {
		return ( true == isset( $this->m_strDeniedOn ) ) ? '\'' . $this->m_strDeniedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setVacationCategoryType( $strVacationCategoryType ) {
		$this->set( 'm_strVacationCategoryType', CStrings::strTrimDef( $strVacationCategoryType, -1, NULL, true ) );
	}

	public function getVacationCategoryType() {
		return $this->m_strVacationCategoryType;
	}

	public function sqlVacationCategoryType() {
		return ( true == isset( $this->m_strVacationCategoryType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strVacationCategoryType ) : '\'' . addslashes( $this->m_strVacationCategoryType ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, vacation_type_id, begin_datetime, end_datetime, paid_off_date, scheduled_vacation_hours, actual_vacation_hours, request, response, admin_notes, is_paid, approved_by, approved_on, denied_by, denied_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, vacation_category_type )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlVacationTypeId() . ', ' .
						$this->sqlBeginDatetime() . ', ' .
						$this->sqlEndDatetime() . ', ' .
						$this->sqlPaidOffDate() . ', ' .
						$this->sqlScheduledVacationHours() . ', ' .
						$this->sqlActualVacationHours() . ', ' .
						$this->sqlRequest() . ', ' .
						$this->sqlResponse() . ', ' .
						$this->sqlAdminNotes() . ', ' .
						$this->sqlIsPaid() . ', ' .
						$this->sqlApprovedBy() . ', ' .
						$this->sqlApprovedOn() . ', ' .
						$this->sqlDeniedBy() . ', ' .
						$this->sqlDeniedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlVacationCategoryType() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacation_type_id = ' . $this->sqlVacationTypeId(). ',' ; } elseif( true == array_key_exists( 'VacationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' vacation_type_id = ' . $this->sqlVacationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' begin_datetime = ' . $this->sqlBeginDatetime(). ',' ; } elseif( true == array_key_exists( 'BeginDatetime', $this->getChangedColumns() ) ) { $strSql .= ' begin_datetime = ' . $this->sqlBeginDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime(). ',' ; } elseif( true == array_key_exists( 'EndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' end_datetime = ' . $this->sqlEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' paid_off_date = ' . $this->sqlPaidOffDate(). ',' ; } elseif( true == array_key_exists( 'PaidOffDate', $this->getChangedColumns() ) ) { $strSql .= ' paid_off_date = ' . $this->sqlPaidOffDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_vacation_hours = ' . $this->sqlScheduledVacationHours(). ',' ; } elseif( true == array_key_exists( 'ScheduledVacationHours', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_vacation_hours = ' . $this->sqlScheduledVacationHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_vacation_hours = ' . $this->sqlActualVacationHours(). ',' ; } elseif( true == array_key_exists( 'ActualVacationHours', $this->getChangedColumns() ) ) { $strSql .= ' actual_vacation_hours = ' . $this->sqlActualVacationHours() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' request = ' . $this->sqlRequest(). ',' ; } elseif( true == array_key_exists( 'Request', $this->getChangedColumns() ) ) { $strSql .= ' request = ' . $this->sqlRequest() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' response = ' . $this->sqlResponse(). ',' ; } elseif( true == array_key_exists( 'Response', $this->getChangedColumns() ) ) { $strSql .= ' response = ' . $this->sqlResponse() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' admin_notes = ' . $this->sqlAdminNotes(). ',' ; } elseif( true == array_key_exists( 'AdminNotes', $this->getChangedColumns() ) ) { $strSql .= ' admin_notes = ' . $this->sqlAdminNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_paid = ' . $this->sqlIsPaid(). ',' ; } elseif( true == array_key_exists( 'IsPaid', $this->getChangedColumns() ) ) { $strSql .= ' is_paid = ' . $this->sqlIsPaid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy(). ',' ; } elseif( true == array_key_exists( 'DeniedBy', $this->getChangedColumns() ) ) { $strSql .= ' denied_by = ' . $this->sqlDeniedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn(). ',' ; } elseif( true == array_key_exists( 'DeniedOn', $this->getChangedColumns() ) ) { $strSql .= ' denied_on = ' . $this->sqlDeniedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vacation_category_type = ' . $this->sqlVacationCategoryType(). ',' ; } elseif( true == array_key_exists( 'VacationCategoryType', $this->getChangedColumns() ) ) { $strSql .= ' vacation_category_type = ' . $this->sqlVacationCategoryType() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'vacation_type_id' => $this->getVacationTypeId(),
			'begin_datetime' => $this->getBeginDatetime(),
			'end_datetime' => $this->getEndDatetime(),
			'paid_off_date' => $this->getPaidOffDate(),
			'scheduled_vacation_hours' => $this->getScheduledVacationHours(),
			'actual_vacation_hours' => $this->getActualVacationHours(),
			'request' => $this->getRequest(),
			'response' => $this->getResponse(),
			'admin_notes' => $this->getAdminNotes(),
			'is_paid' => $this->getIsPaid(),
			'approved_by' => $this->getApprovedBy(),
			'approved_on' => $this->getApprovedOn(),
			'denied_by' => $this->getDeniedBy(),
			'denied_on' => $this->getDeniedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'vacation_category_type' => $this->getVacationCategoryType()
		);
	}

}
?>