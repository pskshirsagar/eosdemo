<?php

class CBaseTag extends CEosSingularBase {

	const TABLE_NAME = 'public.tags';

	protected $m_intId;
	protected $m_intActionResultId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_boolIsPrivate;
	protected $m_intIsPublished;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intParentTagId;
	protected $m_intOwnerId;
	protected $m_intTeamBacklogId;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['action_result_id'] ) && $boolDirectSet ) $this->set( 'm_intActionResultId', trim( $arrValues['action_result_id'] ) ); elseif( isset( $arrValues['action_result_id'] ) ) $this->setActionResultId( $arrValues['action_result_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['is_private'] ) && $boolDirectSet ) $this->set( 'm_boolIsPrivate', trim( stripcslashes( $arrValues['is_private'] ) ) ); elseif( isset( $arrValues['is_private'] ) ) $this->setIsPrivate( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_private'] ) : $arrValues['is_private'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['parent_tag_id'] ) && $boolDirectSet ) $this->set( 'm_intParentTagId', trim( $arrValues['parent_tag_id'] ) ); elseif( isset( $arrValues['parent_tag_id'] ) ) $this->setParentTagId( $arrValues['parent_tag_id'] );
		if( isset( $arrValues['owner_id'] ) && $boolDirectSet ) $this->set( 'm_intOwnerId', trim( $arrValues['owner_id'] ) ); elseif( isset( $arrValues['owner_id'] ) ) $this->setOwnerId( $arrValues['owner_id'] );
		if( isset( $arrValues['team_backlog_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamBacklogId', trim( $arrValues['team_backlog_id'] ) ); elseif( isset( $arrValues['team_backlog_id'] ) ) $this->setTeamBacklogId( $arrValues['team_backlog_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setActionResultId( $intActionResultId ) {
		$this->set( 'm_intActionResultId', CStrings::strToIntDef( $intActionResultId, NULL, false ) );
	}

	public function getActionResultId() {
		return $this->m_intActionResultId;
	}

	public function sqlActionResultId() {
		return ( true == isset( $this->m_intActionResultId ) ) ? ( string ) $this->m_intActionResultId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setIsPrivate( $boolIsPrivate ) {
		$this->set( 'm_boolIsPrivate', CStrings::strToBool( $boolIsPrivate ) );
	}

	public function getIsPrivate() {
		return $this->m_boolIsPrivate;
	}

	public function sqlIsPrivate() {
		return ( true == isset( $this->m_boolIsPrivate ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPrivate ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setParentTagId( $intParentTagId ) {
		$this->set( 'm_intParentTagId', CStrings::strToIntDef( $intParentTagId, NULL, false ) );
	}

	public function getParentTagId() {
		return $this->m_intParentTagId;
	}

	public function sqlParentTagId() {
		return ( true == isset( $this->m_intParentTagId ) ) ? ( string ) $this->m_intParentTagId : 'NULL';
	}

	public function setOwnerId( $intOwnerId ) {
		$this->set( 'm_intOwnerId', CStrings::strToIntDef( $intOwnerId, NULL, false ) );
	}

	public function getOwnerId() {
		return $this->m_intOwnerId;
	}

	public function sqlOwnerId() {
		return ( true == isset( $this->m_intOwnerId ) ) ? ( string ) $this->m_intOwnerId : 'NULL';
	}

	public function setTeamBacklogId( $intTeamBacklogId ) {
		$this->set( 'm_intTeamBacklogId', CStrings::strToIntDef( $intTeamBacklogId, NULL, false ) );
	}

	public function getTeamBacklogId() {
		return $this->m_intTeamBacklogId;
	}

	public function sqlTeamBacklogId() {
		return ( true == isset( $this->m_intTeamBacklogId ) ) ? ( string ) $this->m_intTeamBacklogId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, action_result_id, name, description, is_private, is_published, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, parent_tag_id, owner_id, team_backlog_id )
					VALUES ( ' .
					  $strId . ', ' .
					  $this->sqlActionResultId() . ', ' .
					  $this->sqlName() . ', ' .
					  $this->sqlDescription() . ', ' .
					  $this->sqlIsPrivate() . ', ' .
					  $this->sqlIsPublished() . ', ' .
					  $this->sqlDeletedBy() . ', ' .
					  $this->sqlDeletedOn() . ', ' .
					  ( int ) $intCurrentUserId . ', ' .
					  $this->sqlUpdatedOn() . ', ' .
					  ( int ) $intCurrentUserId . ', ' .
					  $this->sqlCreatedOn() . ', ' .
					  $this->sqlParentTagId() . ', ' .
					  $this->sqlOwnerId() . ', ' .
					  $this->sqlTeamBacklogId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_result_id = ' . $this->sqlActionResultId(). ',' ; } elseif( true == array_key_exists( 'ActionResultId', $this->getChangedColumns() ) ) { $strSql .= ' action_result_id = ' . $this->sqlActionResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_private = ' . $this->sqlIsPrivate(). ',' ; } elseif( true == array_key_exists( 'IsPrivate', $this->getChangedColumns() ) ) { $strSql .= ' is_private = ' . $this->sqlIsPrivate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_tag_id = ' . $this->sqlParentTagId(). ',' ; } elseif( true == array_key_exists( 'ParentTagId', $this->getChangedColumns() ) ) { $strSql .= ' parent_tag_id = ' . $this->sqlParentTagId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_id = ' . $this->sqlOwnerId(). ',' ; } elseif( true == array_key_exists( 'OwnerId', $this->getChangedColumns() ) ) { $strSql .= ' owner_id = ' . $this->sqlOwnerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_backlog_id = ' . $this->sqlTeamBacklogId(). ',' ; } elseif( true == array_key_exists( 'TeamBacklogId', $this->getChangedColumns() ) ) { $strSql .= ' team_backlog_id = ' . $this->sqlTeamBacklogId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'action_result_id' => $this->getActionResultId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_private' => $this->getIsPrivate(),
			'is_published' => $this->getIsPublished(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'parent_tag_id' => $this->getParentTagId(),
			'owner_id' => $this->getOwnerId(),
			'team_backlog_id' => $this->getTeamBacklogId()
		);
	}

}
?>