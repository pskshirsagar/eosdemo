<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplateLibraries
 * Do not add any new functions to this class.
 */

class CBaseDocumentTemplateLibraries extends CEosPluralBase {

	/**
	 * @return CDocumentTemplateLibrary[]
	 */
	public static function fetchDocumentTemplateLibraries( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDocumentTemplateLibrary', $objDatabase );
	}

	/**
	 * @return CDocumentTemplateLibrary
	 */
	public static function fetchDocumentTemplateLibrary( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentTemplateLibrary', $objDatabase );
	}

	public static function fetchDocumentTemplateLibraryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_template_libraries', $objDatabase );
	}

	public static function fetchDocumentTemplateLibraryById( $intId, $objDatabase ) {
		return self::fetchDocumentTemplateLibrary( sprintf( 'SELECT * FROM document_template_libraries WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>