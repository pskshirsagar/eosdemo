<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTeams
 * Do not add any new functions to this class.
 */

class CBaseTeams extends CEosPluralBase {

	/**
	 * @return CTeam[]
	 */
	public static function fetchTeams( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTeam::class, $objDatabase );
	}

	/**
	 * @return CTeam
	 */
	public static function fetchTeam( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTeam::class, $objDatabase );
	}

	public static function fetchTeamCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'teams', $objDatabase );
	}

	public static function fetchTeamById( $intId, $objDatabase ) {
		return self::fetchTeam( sprintf( 'SELECT * FROM teams WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchTeamsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE manager_employee_id = %d', $intManagerEmployeeId ), $objDatabase );
	}

	public static function fetchTeamsBySdmEmployeeId( $intSdmEmployeeId, $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE sdm_employee_id = %d', $intSdmEmployeeId ), $objDatabase );
	}

	public static function fetchTeamsByAddEmployeeId( $intAddEmployeeId, $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE add_employee_id = %d', $intAddEmployeeId ), $objDatabase );
	}

	public static function fetchTeamsByQamEmployeeId( $intQamEmployeeId, $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE qam_employee_id = %d', $intQamEmployeeId ), $objDatabase );
	}

	public static function fetchTeamsByQaEmployeeId( $intQaEmployeeId, $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE qa_employee_id = %d', $intQaEmployeeId ), $objDatabase );
	}

	public static function fetchTeamsByTpmEmployeeId( $intTpmEmployeeId, $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE tpm_employee_id = %d', $intTpmEmployeeId ), $objDatabase );
	}

	public static function fetchTeamsByVdbaEmployeeId( $intVdbaEmployeeId, $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE vdba_employee_id = %d', $intVdbaEmployeeId ), $objDatabase );
	}

	public static function fetchTeamsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE ps_product_id = %d', $intPsProductId ), $objDatabase );
	}

	public static function fetchTeamsByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE ps_product_option_id = %d', $intPsProductOptionId ), $objDatabase );
	}

	public static function fetchTeamsByHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId, $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE hr_representative_employee_id = %d', $intHrRepresentativeEmployeeId ), $objDatabase );
	}

	public static function fetchTeamsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE department_id = %d', $intDepartmentId ), $objDatabase );
	}

	public static function fetchTeamsByTeamTypeId( $intTeamTypeId, $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE team_type_id = %d', $intTeamTypeId ), $objDatabase );
	}

}
?>