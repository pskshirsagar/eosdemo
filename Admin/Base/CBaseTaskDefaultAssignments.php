<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskDefaultAssignments
 * Do not add any new functions to this class.
 */

class CBaseTaskDefaultAssignments extends CEosPluralBase {

	/**
	 * @return CTaskDefaultAssignment[]
	 */
	public static function fetchTaskDefaultAssignments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTaskDefaultAssignment::class, $objDatabase );
	}

	/**
	 * @return CTaskDefaultAssignment
	 */
	public static function fetchTaskDefaultAssignment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTaskDefaultAssignment::class, $objDatabase );
	}

	public static function fetchTaskDefaultAssignmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_default_assignments', $objDatabase );
	}

	public static function fetchTaskDefaultAssignmentById( $intId, $objDatabase ) {
		return self::fetchTaskDefaultAssignment( sprintf( 'SELECT * FROM task_default_assignments WHERE id = %d', $intId ), $objDatabase );
	}

	public static function fetchTaskDefaultAssignmentsByTaskTypeId( $intTaskTypeId, $objDatabase ) {
		return self::fetchTaskDefaultAssignments( sprintf( 'SELECT * FROM task_default_assignments WHERE task_type_id = %d', $intTaskTypeId ), $objDatabase );
	}

	public static function fetchTaskDefaultAssignmentsByTaskPriorityId( $intTaskPriorityId, $objDatabase ) {
		return self::fetchTaskDefaultAssignments( sprintf( 'SELECT * FROM task_default_assignments WHERE task_priority_id = %d', $intTaskPriorityId ), $objDatabase );
	}

	public static function fetchTaskDefaultAssignmentsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchTaskDefaultAssignments( sprintf( 'SELECT * FROM task_default_assignments WHERE employee_id = %d', $intEmployeeId ), $objDatabase );
	}

	public static function fetchTaskDefaultAssignmentsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchTaskDefaultAssignments( sprintf( 'SELECT * FROM task_default_assignments WHERE ps_product_id = %d', $intPsProductId ), $objDatabase );
	}

	public static function fetchTaskDefaultAssignmentsByPsProductOptionId( $intPsProductOptionId, $objDatabase ) {
		return self::fetchTaskDefaultAssignments( sprintf( 'SELECT * FROM task_default_assignments WHERE ps_product_option_id = %d', $intPsProductOptionId ), $objDatabase );
	}

}
?>