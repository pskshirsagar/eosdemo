<?php

class CBaseEmployeeApplicationScore extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_application_scores';

	protected $m_intId;
	protected $m_intEmployeeApplicationId;
	protected $m_intEmployeeApplicationScoreTypeId;
	protected $m_intEmployeeApplicationInterviewId;
	protected $m_intScore;
	protected $m_intRawScore;
	protected $m_strSubmittedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationId', trim( $arrValues['employee_application_id'] ) ); elseif( isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( isset( $arrValues['employee_application_score_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationScoreTypeId', trim( $arrValues['employee_application_score_type_id'] ) ); elseif( isset( $arrValues['employee_application_score_type_id'] ) ) $this->setEmployeeApplicationScoreTypeId( $arrValues['employee_application_score_type_id'] );
		if( isset( $arrValues['employee_application_interview_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationInterviewId', trim( $arrValues['employee_application_interview_id'] ) ); elseif( isset( $arrValues['employee_application_interview_id'] ) ) $this->setEmployeeApplicationInterviewId( $arrValues['employee_application_interview_id'] );
		if( isset( $arrValues['score'] ) && $boolDirectSet ) $this->set( 'm_intScore', trim( $arrValues['score'] ) ); elseif( isset( $arrValues['score'] ) ) $this->setScore( $arrValues['score'] );
		if( isset( $arrValues['raw_score'] ) && $boolDirectSet ) $this->set( 'm_intRawScore', trim( $arrValues['raw_score'] ) ); elseif( isset( $arrValues['raw_score'] ) ) $this->setRawScore( $arrValues['raw_score'] );
		if( isset( $arrValues['submitted_on'] ) && $boolDirectSet ) $this->set( 'm_strSubmittedOn', trim( $arrValues['submitted_on'] ) ); elseif( isset( $arrValues['submitted_on'] ) ) $this->setSubmittedOn( $arrValues['submitted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeApplicationId( $intEmployeeApplicationId ) {
		$this->set( 'm_intEmployeeApplicationId', CStrings::strToIntDef( $intEmployeeApplicationId, NULL, false ) );
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function sqlEmployeeApplicationId() {
		return ( true == isset( $this->m_intEmployeeApplicationId ) ) ? ( string ) $this->m_intEmployeeApplicationId : 'NULL';
	}

	public function setEmployeeApplicationScoreTypeId( $intEmployeeApplicationScoreTypeId ) {
		$this->set( 'm_intEmployeeApplicationScoreTypeId', CStrings::strToIntDef( $intEmployeeApplicationScoreTypeId, NULL, false ) );
	}

	public function getEmployeeApplicationScoreTypeId() {
		return $this->m_intEmployeeApplicationScoreTypeId;
	}

	public function sqlEmployeeApplicationScoreTypeId() {
		return ( true == isset( $this->m_intEmployeeApplicationScoreTypeId ) ) ? ( string ) $this->m_intEmployeeApplicationScoreTypeId : 'NULL';
	}

	public function setEmployeeApplicationInterviewId( $intEmployeeApplicationInterviewId ) {
		$this->set( 'm_intEmployeeApplicationInterviewId', CStrings::strToIntDef( $intEmployeeApplicationInterviewId, NULL, false ) );
	}

	public function getEmployeeApplicationInterviewId() {
		return $this->m_intEmployeeApplicationInterviewId;
	}

	public function sqlEmployeeApplicationInterviewId() {
		return ( true == isset( $this->m_intEmployeeApplicationInterviewId ) ) ? ( string ) $this->m_intEmployeeApplicationInterviewId : 'NULL';
	}

	public function setScore( $intScore ) {
		$this->set( 'm_intScore', CStrings::strToIntDef( $intScore, NULL, false ) );
	}

	public function getScore() {
		return $this->m_intScore;
	}

	public function sqlScore() {
		return ( true == isset( $this->m_intScore ) ) ? ( string ) $this->m_intScore : 'NULL';
	}

	public function setRawScore( $intRawScore ) {
		$this->set( 'm_intRawScore', CStrings::strToIntDef( $intRawScore, NULL, false ) );
	}

	public function getRawScore() {
		return $this->m_intRawScore;
	}

	public function sqlRawScore() {
		return ( true == isset( $this->m_intRawScore ) ) ? ( string ) $this->m_intRawScore : 'NULL';
	}

	public function setSubmittedOn( $strSubmittedOn ) {
		$this->set( 'm_strSubmittedOn', CStrings::strTrimDef( $strSubmittedOn, -1, NULL, true ) );
	}

	public function getSubmittedOn() {
		return $this->m_strSubmittedOn;
	}

	public function sqlSubmittedOn() {
		return ( true == isset( $this->m_strSubmittedOn ) ) ? '\'' . $this->m_strSubmittedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_application_id, employee_application_score_type_id, employee_application_interview_id, score, raw_score, submitted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeApplicationId() . ', ' .
 						$this->sqlEmployeeApplicationScoreTypeId() . ', ' .
 						$this->sqlEmployeeApplicationInterviewId() . ', ' .
 						$this->sqlScore() . ', ' .
 						$this->sqlRawScore() . ', ' .
 						$this->sqlSubmittedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_score_type_id = ' . $this->sqlEmployeeApplicationScoreTypeId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationScoreTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_score_type_id = ' . $this->sqlEmployeeApplicationScoreTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_interview_id = ' . $this->sqlEmployeeApplicationInterviewId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationInterviewId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_interview_id = ' . $this->sqlEmployeeApplicationInterviewId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' score = ' . $this->sqlScore() . ','; } elseif( true == array_key_exists( 'Score', $this->getChangedColumns() ) ) { $strSql .= ' score = ' . $this->sqlScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' raw_score = ' . $this->sqlRawScore() . ','; } elseif( true == array_key_exists( 'RawScore', $this->getChangedColumns() ) ) { $strSql .= ' raw_score = ' . $this->sqlRawScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' submitted_on = ' . $this->sqlSubmittedOn() . ','; } elseif( true == array_key_exists( 'SubmittedOn', $this->getChangedColumns() ) ) { $strSql .= ' submitted_on = ' . $this->sqlSubmittedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_application_id' => $this->getEmployeeApplicationId(),
			'employee_application_score_type_id' => $this->getEmployeeApplicationScoreTypeId(),
			'employee_application_interview_id' => $this->getEmployeeApplicationInterviewId(),
			'score' => $this->getScore(),
			'raw_score' => $this->getRawScore(),
			'submitted_on' => $this->getSubmittedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>