<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeInteractions
 * Do not add any new functions to this class.
 */

class CBaseEmployeeInteractions extends CEosPluralBase {

	/**
	 * @return CEmployeeInteraction[]
	 */
	public static function fetchEmployeeInteractions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeInteraction::class, $objDatabase );
	}

	/**
	 * @return CEmployeeInteraction
	 */
	public static function fetchEmployeeInteraction( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeInteraction::class, $objDatabase );
	}

	public static function fetchEmployeeInteractionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_interactions', $objDatabase );
	}

	public static function fetchEmployeeInteractionById( $intId, $objDatabase ) {
		return self::fetchEmployeeInteraction( sprintf( 'SELECT * FROM employee_interactions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeInteractionsByEmployeeInteractionTypeId( $intEmployeeInteractionTypeId, $objDatabase ) {
		return self::fetchEmployeeInteractions( sprintf( 'SELECT * FROM employee_interactions WHERE employee_interaction_type_id = %d', ( int ) $intEmployeeInteractionTypeId ), $objDatabase );
	}

	public static function fetchEmployeeInteractionsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeInteractions( sprintf( 'SELECT * FROM employee_interactions WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeInteractionsByCallAgentId( $intCallAgentId, $objDatabase ) {
		return self::fetchEmployeeInteractions( sprintf( 'SELECT * FROM employee_interactions WHERE call_agent_id = %d', ( int ) $intCallAgentId ), $objDatabase );
	}

	public static function fetchEmployeeInteractionsByCallId( $intCallId, $objDatabase ) {
		return self::fetchEmployeeInteractions( sprintf( 'SELECT * FROM employee_interactions WHERE call_id = %d', ( int ) $intCallId ), $objDatabase );
	}

}
?>