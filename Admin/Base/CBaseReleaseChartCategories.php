<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseChartCategories
 * Do not add any new functions to this class.
 */

class CBaseReleaseChartCategories extends CEosPluralBase {

	/**
	 * @return CReleaseChartCategory[]
	 */
	public static function fetchReleaseChartCategories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReleaseChartCategory::class, $objDatabase );
	}

	/**
	 * @return CReleaseChartCategory
	 */
	public static function fetchReleaseChartCategory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReleaseChartCategory::class, $objDatabase );
	}

	public static function fetchReleaseChartCategoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'release_chart_categories', $objDatabase );
	}

	public static function fetchReleaseChartCategoryById( $intId, $objDatabase ) {
		return self::fetchReleaseChartCategory( sprintf( 'SELECT * FROM release_chart_categories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>