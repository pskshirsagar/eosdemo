<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdGroupSources
 * Do not add any new functions to this class.
 */

class CBaseSemAdGroupSources extends CEosPluralBase {

	/**
	 * @return CSemAdGroupSource[]
	 */
	public static function fetchSemAdGroupSources( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemAdGroupSource', $objDatabase );
	}

	/**
	 * @return CSemAdGroupSource
	 */
	public static function fetchSemAdGroupSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemAdGroupSource', $objDatabase );
	}

	public static function fetchSemAdGroupSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_ad_group_sources', $objDatabase );
	}

	public static function fetchSemAdGroupSourceById( $intId, $objDatabase ) {
		return self::fetchSemAdGroupSource( sprintf( 'SELECT * FROM sem_ad_group_sources WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemAdGroupSourcesBySemSourceId( $intSemSourceId, $objDatabase ) {
		return self::fetchSemAdGroupSources( sprintf( 'SELECT * FROM sem_ad_group_sources WHERE sem_source_id = %d', ( int ) $intSemSourceId ), $objDatabase );
	}

	public static function fetchSemAdGroupSourcesBySemAccountId( $intSemAccountId, $objDatabase ) {
		return self::fetchSemAdGroupSources( sprintf( 'SELECT * FROM sem_ad_group_sources WHERE sem_account_id = %d', ( int ) $intSemAccountId ), $objDatabase );
	}

	public static function fetchSemAdGroupSourcesBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemAdGroupSources( sprintf( 'SELECT * FROM sem_ad_group_sources WHERE sem_ad_group_id = %d', ( int ) $intSemAdGroupId ), $objDatabase );
	}

	public static function fetchSemAdGroupSourcesBySemStatusTypeId( $intSemStatusTypeId, $objDatabase ) {
		return self::fetchSemAdGroupSources( sprintf( 'SELECT * FROM sem_ad_group_sources WHERE sem_status_type_id = %d', ( int ) $intSemStatusTypeId ), $objDatabase );
	}

}
?>