<?php

class CBaseTeam extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.teams';

	protected $m_intId;
	protected $m_intManagerEmployeeId;
	protected $m_intSdmEmployeeId;
	protected $m_intAddEmployeeId;
	protected $m_intQamEmployeeId;
	protected $m_intQaEmployeeId;
	protected $m_intTpmEmployeeId;
	protected $m_intVdbaEmployeeId;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_intHrRepresentativeEmployeeId;
	protected $m_intDepartmentId;
	protected $m_intTeamTypeId;
	protected $m_strLogoFilePath;
	protected $m_strName;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strPreferredName;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['manager_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intManagerEmployeeId', trim( $arrValues['manager_employee_id'] ) ); elseif( isset( $arrValues['manager_employee_id'] ) ) $this->setManagerEmployeeId( $arrValues['manager_employee_id'] );
		if( isset( $arrValues['sdm_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intSdmEmployeeId', trim( $arrValues['sdm_employee_id'] ) ); elseif( isset( $arrValues['sdm_employee_id'] ) ) $this->setSdmEmployeeId( $arrValues['sdm_employee_id'] );
		if( isset( $arrValues['add_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intAddEmployeeId', trim( $arrValues['add_employee_id'] ) ); elseif( isset( $arrValues['add_employee_id'] ) ) $this->setAddEmployeeId( $arrValues['add_employee_id'] );
		if( isset( $arrValues['qam_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQamEmployeeId', trim( $arrValues['qam_employee_id'] ) ); elseif( isset( $arrValues['qam_employee_id'] ) ) $this->setQamEmployeeId( $arrValues['qam_employee_id'] );
		if( isset( $arrValues['qa_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQaEmployeeId', trim( $arrValues['qa_employee_id'] ) ); elseif( isset( $arrValues['qa_employee_id'] ) ) $this->setQaEmployeeId( $arrValues['qa_employee_id'] );
		if( isset( $arrValues['tpm_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intTpmEmployeeId', trim( $arrValues['tpm_employee_id'] ) ); elseif( isset( $arrValues['tpm_employee_id'] ) ) $this->setTpmEmployeeId( $arrValues['tpm_employee_id'] );
		if( isset( $arrValues['vdba_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intVdbaEmployeeId', trim( $arrValues['vdba_employee_id'] ) ); elseif( isset( $arrValues['vdba_employee_id'] ) ) $this->setVdbaEmployeeId( $arrValues['vdba_employee_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['hr_representative_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intHrRepresentativeEmployeeId', trim( $arrValues['hr_representative_employee_id'] ) ); elseif( isset( $arrValues['hr_representative_employee_id'] ) ) $this->setHrRepresentativeEmployeeId( $arrValues['hr_representative_employee_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['team_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamTypeId', trim( $arrValues['team_type_id'] ) ); elseif( isset( $arrValues['team_type_id'] ) ) $this->setTeamTypeId( $arrValues['team_type_id'] );
		if( isset( $arrValues['logo_file_path'] ) && $boolDirectSet ) $this->set( 'm_strLogoFilePath', trim( $arrValues['logo_file_path'] ) ); elseif( isset( $arrValues['logo_file_path'] ) ) $this->setLogoFilePath( $arrValues['logo_file_path'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['preferred_name'] ) && $boolDirectSet ) $this->set( 'm_strPreferredName', trim( $arrValues['preferred_name'] ) ); elseif( isset( $arrValues['preferred_name'] ) ) $this->setPreferredName( $arrValues['preferred_name'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setManagerEmployeeId( $intManagerEmployeeId ) {
		$this->set( 'm_intManagerEmployeeId', CStrings::strToIntDef( $intManagerEmployeeId, NULL, false ) );
	}

	public function getManagerEmployeeId() {
		return $this->m_intManagerEmployeeId;
	}

	public function sqlManagerEmployeeId() {
		return ( true == isset( $this->m_intManagerEmployeeId ) ) ? ( string ) $this->m_intManagerEmployeeId : 'NULL';
	}

	public function setSdmEmployeeId( $intSdmEmployeeId ) {
		$this->set( 'm_intSdmEmployeeId', CStrings::strToIntDef( $intSdmEmployeeId, NULL, false ) );
	}

	public function getSdmEmployeeId() {
		return $this->m_intSdmEmployeeId;
	}

	public function sqlSdmEmployeeId() {
		return ( true == isset( $this->m_intSdmEmployeeId ) ) ? ( string ) $this->m_intSdmEmployeeId : 'NULL';
	}

	public function setAddEmployeeId( $intAddEmployeeId ) {
		$this->set( 'm_intAddEmployeeId', CStrings::strToIntDef( $intAddEmployeeId, NULL, false ) );
	}

	public function getAddEmployeeId() {
		return $this->m_intAddEmployeeId;
	}

	public function sqlAddEmployeeId() {
		return ( true == isset( $this->m_intAddEmployeeId ) ) ? ( string ) $this->m_intAddEmployeeId : 'NULL';
	}

	public function setQamEmployeeId( $intQamEmployeeId ) {
		$this->set( 'm_intQamEmployeeId', CStrings::strToIntDef( $intQamEmployeeId, NULL, false ) );
	}

	public function getQamEmployeeId() {
		return $this->m_intQamEmployeeId;
	}

	public function sqlQamEmployeeId() {
		return ( true == isset( $this->m_intQamEmployeeId ) ) ? ( string ) $this->m_intQamEmployeeId : 'NULL';
	}

	public function setQaEmployeeId( $intQaEmployeeId ) {
		$this->set( 'm_intQaEmployeeId', CStrings::strToIntDef( $intQaEmployeeId, NULL, false ) );
	}

	public function getQaEmployeeId() {
		return $this->m_intQaEmployeeId;
	}

	public function sqlQaEmployeeId() {
		return ( true == isset( $this->m_intQaEmployeeId ) ) ? ( string ) $this->m_intQaEmployeeId : 'NULL';
	}

	public function setTpmEmployeeId( $intTpmEmployeeId ) {
		$this->set( 'm_intTpmEmployeeId', CStrings::strToIntDef( $intTpmEmployeeId, NULL, false ) );
	}

	public function getTpmEmployeeId() {
		return $this->m_intTpmEmployeeId;
	}

	public function sqlTpmEmployeeId() {
		return ( true == isset( $this->m_intTpmEmployeeId ) ) ? ( string ) $this->m_intTpmEmployeeId : 'NULL';
	}

	public function setVdbaEmployeeId( $intVdbaEmployeeId ) {
		$this->set( 'm_intVdbaEmployeeId', CStrings::strToIntDef( $intVdbaEmployeeId, NULL, false ) );
	}

	public function getVdbaEmployeeId() {
		return $this->m_intVdbaEmployeeId;
	}

	public function sqlVdbaEmployeeId() {
		return ( true == isset( $this->m_intVdbaEmployeeId ) ) ? ( string ) $this->m_intVdbaEmployeeId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId ) {
		$this->set( 'm_intHrRepresentativeEmployeeId', CStrings::strToIntDef( $intHrRepresentativeEmployeeId, NULL, false ) );
	}

	public function getHrRepresentativeEmployeeId() {
		return $this->m_intHrRepresentativeEmployeeId;
	}

	public function sqlHrRepresentativeEmployeeId() {
		return ( true == isset( $this->m_intHrRepresentativeEmployeeId ) ) ? ( string ) $this->m_intHrRepresentativeEmployeeId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setTeamTypeId( $intTeamTypeId ) {
		$this->set( 'm_intTeamTypeId', CStrings::strToIntDef( $intTeamTypeId, NULL, false ) );
	}

	public function getTeamTypeId() {
		return $this->m_intTeamTypeId;
	}

	public function sqlTeamTypeId() {
		return ( true == isset( $this->m_intTeamTypeId ) ) ? ( string ) $this->m_intTeamTypeId : 'NULL';
	}

	public function setLogoFilePath( $strLogoFilePath ) {
		$this->set( 'm_strLogoFilePath', CStrings::strTrimDef( $strLogoFilePath, 500, NULL, true ) );
	}

	public function getLogoFilePath() {
		return $this->m_strLogoFilePath;
	}

	public function sqlLogoFilePath() {
		return ( true == isset( $this->m_strLogoFilePath ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strLogoFilePath ) : '\'' . addslashes( $this->m_strLogoFilePath ) . '\'' ) : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setPreferredName( $strPreferredName ) {
		$this->set( 'm_strPreferredName', CStrings::strTrimDef( $strPreferredName, 100, NULL, true ) );
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function sqlPreferredName() {
		return ( true == isset( $this->m_strPreferredName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strPreferredName ) : '\'' . addslashes( $this->m_strPreferredName ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, manager_employee_id, sdm_employee_id, add_employee_id, qam_employee_id, qa_employee_id, tpm_employee_id, vdba_employee_id, ps_product_id, ps_product_option_id, hr_representative_employee_id, department_id, team_type_id, logo_file_path, name, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, preferred_name, details )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlManagerEmployeeId() . ', ' .
		          $this->sqlSdmEmployeeId() . ', ' .
		          $this->sqlAddEmployeeId() . ', ' .
		          $this->sqlQamEmployeeId() . ', ' .
		          $this->sqlQaEmployeeId() . ', ' .
		          $this->sqlTpmEmployeeId() . ', ' .
		          $this->sqlVdbaEmployeeId() . ', ' .
		          $this->sqlPsProductId() . ', ' .
		          $this->sqlPsProductOptionId() . ', ' .
		          $this->sqlHrRepresentativeEmployeeId() . ', ' .
		          $this->sqlDepartmentId() . ', ' .
		          $this->sqlTeamTypeId() . ', ' .
		          $this->sqlLogoFilePath() . ', ' .
		          $this->sqlName() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlPreferredName() . ', ' .
		          $this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' manager_employee_id = ' . $this->sqlManagerEmployeeId(). ',' ; } elseif( true == array_key_exists( 'ManagerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' manager_employee_id = ' . $this->sqlManagerEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sdm_employee_id = ' . $this->sqlSdmEmployeeId(). ',' ; } elseif( true == array_key_exists( 'SdmEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' sdm_employee_id = ' . $this->sqlSdmEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' add_employee_id = ' . $this->sqlAddEmployeeId(). ',' ; } elseif( true == array_key_exists( 'AddEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' add_employee_id = ' . $this->sqlAddEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qam_employee_id = ' . $this->sqlQamEmployeeId(). ',' ; } elseif( true == array_key_exists( 'QamEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qam_employee_id = ' . $this->sqlQamEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId(). ',' ; } elseif( true == array_key_exists( 'QaEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' tpm_employee_id = ' . $this->sqlTpmEmployeeId(). ',' ; } elseif( true == array_key_exists( 'TpmEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' tpm_employee_id = ' . $this->sqlTpmEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vdba_employee_id = ' . $this->sqlVdbaEmployeeId(). ',' ; } elseif( true == array_key_exists( 'VdbaEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' vdba_employee_id = ' . $this->sqlVdbaEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId(). ',' ; } elseif( true == array_key_exists( 'PsProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' hr_representative_employee_id = ' . $this->sqlHrRepresentativeEmployeeId(). ',' ; } elseif( true == array_key_exists( 'HrRepresentativeEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' hr_representative_employee_id = ' . $this->sqlHrRepresentativeEmployeeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId(). ',' ; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_type_id = ' . $this->sqlTeamTypeId(). ',' ; } elseif( true == array_key_exists( 'TeamTypeId', $this->getChangedColumns() ) ) { $strSql .= ' team_type_id = ' . $this->sqlTeamTypeId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' logo_file_path = ' . $this->sqlLogoFilePath(). ',' ; } elseif( true == array_key_exists( 'LogoFilePath', $this->getChangedColumns() ) ) { $strSql .= ' logo_file_path = ' . $this->sqlLogoFilePath() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' preferred_name = ' . $this->sqlPreferredName(). ',' ; } elseif( true == array_key_exists( 'PreferredName', $this->getChangedColumns() ) ) { $strSql .= ' preferred_name = ' . $this->sqlPreferredName() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'manager_employee_id' => $this->getManagerEmployeeId(),
			'sdm_employee_id' => $this->getSdmEmployeeId(),
			'add_employee_id' => $this->getAddEmployeeId(),
			'qam_employee_id' => $this->getQamEmployeeId(),
			'qa_employee_id' => $this->getQaEmployeeId(),
			'tpm_employee_id' => $this->getTpmEmployeeId(),
			'vdba_employee_id' => $this->getVdbaEmployeeId(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'hr_representative_employee_id' => $this->getHrRepresentativeEmployeeId(),
			'department_id' => $this->getDepartmentId(),
			'team_type_id' => $this->getTeamTypeId(),
			'logo_file_path' => $this->getLogoFilePath(),
			'name' => $this->getName(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'preferred_name' => $this->getPreferredName(),
			'details' => $this->getDetails()
		);
	}

}
?>