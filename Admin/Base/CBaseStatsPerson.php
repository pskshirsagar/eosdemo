<?php

class CBaseStatsPerson extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_persons';

	protected $m_intId;
	protected $m_intPersonId;
	protected $m_intPsLeadId;
	protected $m_intLoginCount;
	protected $m_dblPermissionedUnitCount;
	protected $m_strMonth;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->m_intId = trim( $arrValues['id'] ); else if( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['person_id'] ) && $boolDirectSet ) $this->m_intPersonId = trim( $arrValues['person_id'] ); else if( isset( $arrValues['person_id'] ) ) $this->setPersonId( $arrValues['person_id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->m_intPsLeadId = trim( $arrValues['ps_lead_id'] ); else if( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['login_count'] ) && $boolDirectSet ) $this->m_intLoginCount = trim( $arrValues['login_count'] ); else if( isset( $arrValues['login_count'] ) ) $this->setLoginCount( $arrValues['login_count'] );
		if( isset( $arrValues['permissioned_unit_count'] ) && $boolDirectSet ) $this->m_dblPermissionedUnitCount = trim( $arrValues['permissioned_unit_count'] ); else if( isset( $arrValues['permissioned_unit_count'] ) ) $this->setPermissionedUnitCount( $arrValues['permissioned_unit_count'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->m_strMonth = trim( $arrValues['month'] ); else if( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->m_intCreatedBy = trim( $arrValues['created_by'] ); else if( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->m_strCreatedOn = trim( $arrValues['created_on'] ); else if( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
	}

	public function setId( $intId ) {
		$this->m_intId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPersonId( $intPersonId ) {
		$this->m_intPersonId = CStrings::strToIntDef( $intPersonId, NULL, false );
	}

	public function getPersonId() {
		return $this->m_intPersonId;
	}

	public function sqlPersonId() {
		return ( true == isset( $this->m_intPersonId ) ) ? ( string ) $this->m_intPersonId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->m_intPsLeadId = CStrings::strToIntDef( $intPsLeadId, NULL, false );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setLoginCount( $intLoginCount ) {
		$this->m_intLoginCount = CStrings::strToIntDef( $intLoginCount, NULL, false );
	}

	public function getLoginCount() {
		return $this->m_intLoginCount;
	}

	public function sqlLoginCount() {
		return ( true == isset( $this->m_intLoginCount ) ) ? ( string ) $this->m_intLoginCount : 'NULL';
	}

	public function setPermissionedUnitCount( $dblPermissionedUnitCount ) {
		$this->m_dblPermissionedUnitCount = CStrings::strToDoubleDef( $dblPermissionedUnitCount, NULL, false );
	}

	public function getPermissionedUnitCount() {
		return $this->m_dblPermissionedUnitCount;
	}

	public function sqlPermissionedUnitCount() {
		return ( true == isset( $this->m_dblPermissionedUnitCount ) ) ? ( double ) $this->m_dblPermissionedUnitCount : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->m_strMonth = CStrings::strTrimDef( $strMonth, -1, NULL, true );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->m_intCreatedBy = CStrings::strToIntDef( $intCreatedBy, NULL, false );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->m_strCreatedOn = CStrings::strTrimDef( $strCreatedOn, -1, NULL, true );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, person_id, ps_lead_id, login_count, permissioned_unit_count, month, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPersonId() . ', ' .
 						$this->sqlPsLeadId() . ', ' .
 						$this->sqlLoginCount() . ', ' .
 						$this->sqlPermissionedUnitCount() . ', ' .
 						$this->sqlMonth() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

			$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' person_id = ' . $this->sqlPersonId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPersonId() ) != $this->getOriginalValueByFieldName ( 'person_id' ) ) { $arrstrOriginalValueChanges['person_id'] = $this->sqlPersonId(); $strSql .= ' person_id = ' . $this->sqlPersonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlPsLeadId() ) != $this->getOriginalValueByFieldName ( 'ps_lead_id' ) ) { $arrstrOriginalValueChanges['ps_lead_id'] = $this->sqlPsLeadId(); $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' login_count = ' . $this->sqlLoginCount() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlLoginCount() ) != $this->getOriginalValueByFieldName ( 'login_count' ) ) { $arrstrOriginalValueChanges['login_count'] = $this->sqlLoginCount(); $strSql .= ' login_count = ' . $this->sqlLoginCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' permissioned_unit_count = ' . $this->sqlPermissionedUnitCount() . ','; } elseif( ( 0 != bccomp ( CStrings::reverseSqlFormat( $this->sqlPermissionedUnitCount() ), $this->getOriginalValueByFieldName ( 'permissioned_unit_count' ), 0 ) ) ) { $arrstrOriginalValueChanges['permissioned_unit_count'] = $this->sqlPermissionedUnitCount(); $strSql .= ' permissioned_unit_count = ' . $this->sqlPermissionedUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' month = ' . $this->sqlMonth() . ','; } elseif( CStrings::reverseSqlFormat( $this->sqlMonth() ) != $this->getOriginalValueByFieldName ( 'month' ) ) { $arrstrOriginalValueChanges['month'] = $this->sqlMonth(); $strSql .= ' month = ' . $this->sqlMonth() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'person_id' => $this->getPersonId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'login_count' => $this->getLoginCount(),
			'permissioned_unit_count' => $this->getPermissionedUnitCount(),
			'month' => $this->getMonth(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>