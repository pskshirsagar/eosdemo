<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemBudgetLogs
 * Do not add any new functions to this class.
 */

class CBaseSemBudgetLogs extends CEosPluralBase {

	/**
	 * @return CSemBudgetLog[]
	 */
	public static function fetchSemBudgetLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemBudgetLog', $objDatabase );
	}

	/**
	 * @return CSemBudgetLog
	 */
	public static function fetchSemBudgetLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemBudgetLog', $objDatabase );
	}

	public static function fetchSemBudgetLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_budget_logs', $objDatabase );
	}

	public static function fetchSemBudgetLogById( $intId, $objDatabase ) {
		return self::fetchSemBudgetLog( sprintf( 'SELECT * FROM sem_budget_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemBudgetLogsByCid( $intCid, $objDatabase ) {
		return self::fetchSemBudgetLogs( sprintf( 'SELECT * FROM sem_budget_logs WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSemBudgetLogsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSemBudgetLogs( sprintf( 'SELECT * FROM sem_budget_logs WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

}
?>