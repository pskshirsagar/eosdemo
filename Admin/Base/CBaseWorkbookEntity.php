<?php

class CBaseWorkbookEntity extends CEosSingularBase {

	const TABLE_NAME = 'public.workbook_entities';

	protected $m_intId;
	protected $m_strEntityDetails;
	protected $m_jsonEntityDetails;
	protected $m_intOrderNumber;
	protected $m_boolIsRequired;
	protected $m_boolIsComment;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsRequired = false;
		$this->m_boolIsComment = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['entity_details'] ) ) $this->set( 'm_strEntityDetails', trim( $arrValues['entity_details'] ) );
		if( isset( $arrValues['order_number'] ) && $boolDirectSet ) $this->set( 'm_intOrderNumber', trim( $arrValues['order_number'] ) ); elseif( isset( $arrValues['order_number'] ) ) $this->setOrderNumber( $arrValues['order_number'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_boolIsRequired', trim( stripcslashes( $arrValues['is_required'] ) ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_required'] ) : $arrValues['is_required'] );
		if( isset( $arrValues['is_comment'] ) && $boolDirectSet ) $this->set( 'm_boolIsComment', trim( stripcslashes( $arrValues['is_comment'] ) ) ); elseif( isset( $arrValues['is_comment'] ) ) $this->setIsComment( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_comment'] ) : $arrValues['is_comment'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEntityDetails( $jsonEntityDetails ) {
		if( true == valObj( $jsonEntityDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonEntityDetails', $jsonEntityDetails );
		} elseif( true == valJsonString( $jsonEntityDetails ) ) {
			$this->set( 'm_jsonEntityDetails', CStrings::strToJson( $jsonEntityDetails ) );
		} else {
			$this->set( 'm_jsonEntityDetails', NULL ); 
		}
		unset( $this->m_strEntityDetails );
	}

	public function getEntityDetails() {
		if( true == isset( $this->m_strEntityDetails ) ) {
			$this->m_jsonEntityDetails = CStrings::strToJson( $this->m_strEntityDetails );
			unset( $this->m_strEntityDetails );
		}
		return $this->m_jsonEntityDetails;
	}

	public function sqlEntityDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getEntityDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getEntityDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setOrderNumber( $intOrderNumber ) {
		$this->set( 'm_intOrderNumber', CStrings::strToIntDef( $intOrderNumber, NULL, false ) );
	}

	public function getOrderNumber() {
		return $this->m_intOrderNumber;
	}

	public function sqlOrderNumber() {
		return ( true == isset( $this->m_intOrderNumber ) ) ? ( string ) $this->m_intOrderNumber : 'NULL';
	}

	public function setIsRequired( $boolIsRequired ) {
		$this->set( 'm_boolIsRequired', CStrings::strToBool( $boolIsRequired ) );
	}

	public function getIsRequired() {
		return $this->m_boolIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_boolIsRequired ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsRequired ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsComment( $boolIsComment ) {
		$this->set( 'm_boolIsComment', CStrings::strToBool( $boolIsComment ) );
	}

	public function getIsComment() {
		return $this->m_boolIsComment;
	}

	public function sqlIsComment() {
		return ( true == isset( $this->m_boolIsComment ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsComment ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, entity_details, order_number, is_required, is_comment, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlEntityDetails() . ', ' .
						$this->sqlOrderNumber() . ', ' .
						$this->sqlIsRequired() . ', ' .
						$this->sqlIsComment() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entity_details = ' . $this->sqlEntityDetails(). ',' ; } elseif( true == array_key_exists( 'EntityDetails', $this->getChangedColumns() ) ) { $strSql .= ' entity_details = ' . $this->sqlEntityDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_number = ' . $this->sqlOrderNumber(). ',' ; } elseif( true == array_key_exists( 'OrderNumber', $this->getChangedColumns() ) ) { $strSql .= ' order_number = ' . $this->sqlOrderNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required = ' . $this->sqlIsRequired(). ',' ; } elseif( true == array_key_exists( 'IsRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_comment = ' . $this->sqlIsComment(). ',' ; } elseif( true == array_key_exists( 'IsComment', $this->getChangedColumns() ) ) { $strSql .= ' is_comment = ' . $this->sqlIsComment() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'entity_details' => $this->getEntityDetails(),
			'order_number' => $this->getOrderNumber(),
			'is_required' => $this->getIsRequired(),
			'is_comment' => $this->getIsComment(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>