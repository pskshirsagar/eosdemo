<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationDemographics
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationDemographics extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationDemographic[]
	 */
	public static function fetchEmployeeApplicationDemographics( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationDemographic', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationDemographic
	 */
	public static function fetchEmployeeApplicationDemographic( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationDemographic', $objDatabase );
	}

	public static function fetchEmployeeApplicationDemographicCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_demographics', $objDatabase );
	}

	public static function fetchEmployeeApplicationDemographicById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationDemographic( sprintf( 'SELECT * FROM employee_application_demographics WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationDemographicsByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchEmployeeApplicationDemographics( sprintf( 'SELECT * FROM employee_application_demographics WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationDemographicsByEmployeeApplicationEthnicityId( $intEmployeeApplicationEthnicityId, $objDatabase ) {
		return self::fetchEmployeeApplicationDemographics( sprintf( 'SELECT * FROM employee_application_demographics WHERE employee_application_ethnicity_id = %d', ( int ) $intEmployeeApplicationEthnicityId ), $objDatabase );
	}

}
?>