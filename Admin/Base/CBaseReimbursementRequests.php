<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReimbursementRequests
 * Do not add any new functions to this class.
 */

class CBaseReimbursementRequests extends CEosPluralBase {

	/**
	 * @return CReimbursementRequest[]
	 */
	public static function fetchReimbursementRequests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CReimbursementRequest', $objDatabase );
	}

	/**
	 * @return CReimbursementRequest
	 */
	public static function fetchReimbursementRequest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReimbursementRequest', $objDatabase );
	}

	public static function fetchReimbursementRequestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reimbursement_requests', $objDatabase );
	}

	public static function fetchReimbursementRequestById( $intId, $objDatabase ) {
		return self::fetchReimbursementRequest( sprintf( 'SELECT * FROM reimbursement_requests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReimbursementRequestsByRequestTypeId( $intRequestTypeId, $objDatabase ) {
		return self::fetchReimbursementRequests( sprintf( 'SELECT * FROM reimbursement_requests WHERE request_type_id = %d', ( int ) $intRequestTypeId ), $objDatabase );
	}

	public static function fetchReimbursementRequestsByEmployeePayStatusTypeId( $intEmployeePayStatusTypeId, $objDatabase ) {
		return self::fetchReimbursementRequests( sprintf( 'SELECT * FROM reimbursement_requests WHERE employee_pay_status_type_id = %d', ( int ) $intEmployeePayStatusTypeId ), $objDatabase );
	}

	public static function fetchReimbursementRequestsByBatchId( $intBatchId, $objDatabase ) {
		return self::fetchReimbursementRequests( sprintf( 'SELECT * FROM reimbursement_requests WHERE batch_id = %d', ( int ) $intBatchId ), $objDatabase );
	}

	public static function fetchReimbursementRequestsByPayrollPeriodId( $intPayrollPeriodId, $objDatabase ) {
		return self::fetchReimbursementRequests( sprintf( 'SELECT * FROM reimbursement_requests WHERE payroll_period_id = %d', ( int ) $intPayrollPeriodId ), $objDatabase );
	}

}
?>