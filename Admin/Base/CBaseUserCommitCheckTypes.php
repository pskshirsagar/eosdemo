<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserCommitCheckTypes
 * Do not add any new functions to this class.
 */

class CBaseUserCommitCheckTypes extends CEosPluralBase {

	/**
	 * @return CUserCommitCheckType[]
	 */
	public static function fetchUserCommitCheckTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserCommitCheckType', $objDatabase );
	}

	/**
	 * @return CUserCommitCheckType
	 */
	public static function fetchUserCommitCheckType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserCommitCheckType', $objDatabase );
	}

	public static function fetchUserCommitCheckTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_commit_check_types', $objDatabase );
	}

	public static function fetchUserCommitCheckTypeById( $intId, $objDatabase ) {
		return self::fetchUserCommitCheckType( sprintf( 'SELECT * FROM user_commit_check_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>