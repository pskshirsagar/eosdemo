<?php

class CBaseOfficeDesk extends CEosSingularBase {

	const TABLE_NAME = 'public.office_desks';

	protected $m_intId;
	protected $m_intOfficeId;
	protected $m_intTeamId;
	protected $m_intIsReserved;
	protected $m_intIsActive;
	protected $m_intDeskShiftingRequestStatus;
	protected $m_intDeskShiftingRequestBy;
	protected $m_strDeskNumber;
	protected $m_strDeskDimensions;
	protected $m_strDeskShiftingRequestOn;
	protected $m_boolIsCabin;
	protected $m_boolIsMac;
	protected $m_strUpdatedOn;
	protected $m_intUpdatedBy;
	protected $m_strCreatedOn;
	protected $m_intCreatedBy;
	protected $m_strDeskDimensionDetails;
	protected $m_jsonDeskDimensionDetails;

	public function __construct() {
		parent::__construct();

		$this->m_intIsReserved = '0';
		$this->m_intIsActive = '1';
		$this->m_boolIsCabin = false;
		$this->m_boolIsMac = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['office_id'] ) && $boolDirectSet ) $this->set( 'm_intOfficeId', trim( $arrValues['office_id'] ) ); elseif( isset( $arrValues['office_id'] ) ) $this->setOfficeId( $arrValues['office_id'] );
		if( isset( $arrValues['team_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamId', trim( $arrValues['team_id'] ) ); elseif( isset( $arrValues['team_id'] ) ) $this->setTeamId( $arrValues['team_id'] );
		if( isset( $arrValues['is_reserved'] ) && $boolDirectSet ) $this->set( 'm_intIsReserved', trim( $arrValues['is_reserved'] ) ); elseif( isset( $arrValues['is_reserved'] ) ) $this->setIsReserved( $arrValues['is_reserved'] );
		if( isset( $arrValues['is_active'] ) && $boolDirectSet ) $this->set( 'm_intIsActive', trim( $arrValues['is_active'] ) ); elseif( isset( $arrValues['is_active'] ) ) $this->setIsActive( $arrValues['is_active'] );
		if( isset( $arrValues['desk_shifting_request_status'] ) && $boolDirectSet ) $this->set( 'm_intDeskShiftingRequestStatus', trim( $arrValues['desk_shifting_request_status'] ) ); elseif( isset( $arrValues['desk_shifting_request_status'] ) ) $this->setDeskShiftingRequestStatus( $arrValues['desk_shifting_request_status'] );
		if( isset( $arrValues['desk_shifting_request_by'] ) && $boolDirectSet ) $this->set( 'm_intDeskShiftingRequestBy', trim( $arrValues['desk_shifting_request_by'] ) ); elseif( isset( $arrValues['desk_shifting_request_by'] ) ) $this->setDeskShiftingRequestBy( $arrValues['desk_shifting_request_by'] );
		if( isset( $arrValues['desk_number'] ) && $boolDirectSet ) $this->set( 'm_strDeskNumber', trim( stripcslashes( $arrValues['desk_number'] ) ) ); elseif( isset( $arrValues['desk_number'] ) ) $this->setDeskNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['desk_number'] ) : $arrValues['desk_number'] );
		if( isset( $arrValues['desk_dimensions'] ) && $boolDirectSet ) $this->set( 'm_strDeskDimensions', trim( stripcslashes( $arrValues['desk_dimensions'] ) ) ); elseif( isset( $arrValues['desk_dimensions'] ) ) $this->setDeskDimensions( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['desk_dimensions'] ) : $arrValues['desk_dimensions'] );
		if( isset( $arrValues['desk_shifting_request_on'] ) && $boolDirectSet ) $this->set( 'm_strDeskShiftingRequestOn', trim( $arrValues['desk_shifting_request_on'] ) ); elseif( isset( $arrValues['desk_shifting_request_on'] ) ) $this->setDeskShiftingRequestOn( $arrValues['desk_shifting_request_on'] );
		if( isset( $arrValues['is_cabin'] ) && $boolDirectSet ) $this->set( 'm_boolIsCabin', trim( stripcslashes( $arrValues['is_cabin'] ) ) ); elseif( isset( $arrValues['is_cabin'] ) ) $this->setIsCabin( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_cabin'] ) : $arrValues['is_cabin'] );
		if( isset( $arrValues['is_mac'] ) && $boolDirectSet ) $this->set( 'm_boolIsMac', trim( stripcslashes( $arrValues['is_mac'] ) ) ); elseif( isset( $arrValues['is_mac'] ) ) $this->setIsMac( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_mac'] ) : $arrValues['is_mac'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['desk_dimension_details'] ) ) $this->set( 'm_strDeskDimensionDetails', trim( $arrValues['desk_dimension_details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOfficeId( $intOfficeId ) {
		$this->set( 'm_intOfficeId', CStrings::strToIntDef( $intOfficeId, NULL, false ) );
	}

	public function getOfficeId() {
		return $this->m_intOfficeId;
	}

	public function sqlOfficeId() {
		return ( true == isset( $this->m_intOfficeId ) ) ? ( string ) $this->m_intOfficeId : 'NULL';
	}

	public function setTeamId( $intTeamId ) {
		$this->set( 'm_intTeamId', CStrings::strToIntDef( $intTeamId, NULL, false ) );
	}

	public function getTeamId() {
		return $this->m_intTeamId;
	}

	public function sqlTeamId() {
		return ( true == isset( $this->m_intTeamId ) ) ? ( string ) $this->m_intTeamId : 'NULL';
	}

	public function setIsReserved( $intIsReserved ) {
		$this->set( 'm_intIsReserved', CStrings::strToIntDef( $intIsReserved, NULL, false ) );
	}

	public function getIsReserved() {
		return $this->m_intIsReserved;
	}

	public function sqlIsReserved() {
		return ( true == isset( $this->m_intIsReserved ) ) ? ( string ) $this->m_intIsReserved : '0';
	}

	public function setIsActive( $intIsActive ) {
		$this->set( 'm_intIsActive', CStrings::strToIntDef( $intIsActive, NULL, false ) );
	}

	public function getIsActive() {
		return $this->m_intIsActive;
	}

	public function sqlIsActive() {
		return ( true == isset( $this->m_intIsActive ) ) ? ( string ) $this->m_intIsActive : '1';
	}

	public function setDeskShiftingRequestStatus( $intDeskShiftingRequestStatus ) {
		$this->set( 'm_intDeskShiftingRequestStatus', CStrings::strToIntDef( $intDeskShiftingRequestStatus, NULL, false ) );
	}

	public function getDeskShiftingRequestStatus() {
		return $this->m_intDeskShiftingRequestStatus;
	}

	public function sqlDeskShiftingRequestStatus() {
		return ( true == isset( $this->m_intDeskShiftingRequestStatus ) ) ? ( string ) $this->m_intDeskShiftingRequestStatus : 'NULL';
	}

	public function setDeskShiftingRequestBy( $intDeskShiftingRequestBy ) {
		$this->set( 'm_intDeskShiftingRequestBy', CStrings::strToIntDef( $intDeskShiftingRequestBy, NULL, false ) );
	}

	public function getDeskShiftingRequestBy() {
		return $this->m_intDeskShiftingRequestBy;
	}

	public function sqlDeskShiftingRequestBy() {
		return ( true == isset( $this->m_intDeskShiftingRequestBy ) ) ? ( string ) $this->m_intDeskShiftingRequestBy : 'NULL';
	}

	public function setDeskNumber( $strDeskNumber ) {
		$this->set( 'm_strDeskNumber', CStrings::strTrimDef( $strDeskNumber, 100, NULL, true ) );
	}

	public function getDeskNumber() {
		return $this->m_strDeskNumber;
	}

	public function sqlDeskNumber() {
		return ( true == isset( $this->m_strDeskNumber ) ) ? '\'' . addslashes( $this->m_strDeskNumber ) . '\'' : 'NULL';
	}

	public function setDeskDimensions( $strDeskDimensions ) {
		$this->set( 'm_strDeskDimensions', CStrings::strTrimDef( $strDeskDimensions, -1, NULL, true ) );
	}

	public function getDeskDimensions() {
		return $this->m_strDeskDimensions;
	}

	public function sqlDeskDimensions() {
		return ( true == isset( $this->m_strDeskDimensions ) ) ? '\'' . addslashes( $this->m_strDeskDimensions ) . '\'' : 'NULL';
	}

	public function setDeskShiftingRequestOn( $strDeskShiftingRequestOn ) {
		$this->set( 'm_strDeskShiftingRequestOn', CStrings::strTrimDef( $strDeskShiftingRequestOn, -1, NULL, true ) );
	}

	public function getDeskShiftingRequestOn() {
		return $this->m_strDeskShiftingRequestOn;
	}

	public function sqlDeskShiftingRequestOn() {
		return ( true == isset( $this->m_strDeskShiftingRequestOn ) ) ? '\'' . $this->m_strDeskShiftingRequestOn . '\'' : 'NULL';
	}

	public function setIsCabin( $boolIsCabin ) {
		$this->set( 'm_boolIsCabin', CStrings::strToBool( $boolIsCabin ) );
	}

	public function getIsCabin() {
		return $this->m_boolIsCabin;
	}

	public function sqlIsCabin() {
		return ( true == isset( $this->m_boolIsCabin ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsCabin ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsMac( $boolIsMac ) {
		$this->set( 'm_boolIsMac', CStrings::strToBool( $boolIsMac ) );
	}

	public function getIsMac() {
		return $this->m_boolIsMac;
	}

	public function sqlIsMac() {
		return ( true == isset( $this->m_boolIsMac ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsMac ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setDeskDimensionDetails( $jsonDeskDimensionDetails ) {
		if( true == valObj( $jsonDeskDimensionDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonDeskDimensionDetails', $jsonDeskDimensionDetails );
		} elseif( true == valJsonString( $jsonDeskDimensionDetails ) ) {
			$this->set( 'm_jsonDeskDimensionDetails', CStrings::strToJson( $jsonDeskDimensionDetails ) );
		} else {
			$this->set( 'm_jsonDeskDimensionDetails', NULL ); 
		}
		unset( $this->m_strDeskDimensionDetails );
	}

	public function getDeskDimensionDetails() {
		if( true == isset( $this->m_strDeskDimensionDetails ) ) {
			$this->m_jsonDeskDimensionDetails = CStrings::strToJson( $this->m_strDeskDimensionDetails );
			unset( $this->m_strDeskDimensionDetails );
		}
		return $this->m_jsonDeskDimensionDetails;
	}

	public function sqlDeskDimensionDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getDeskDimensionDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getDeskDimensionDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, office_id, team_id, is_reserved, is_active, desk_shifting_request_status, desk_shifting_request_by, desk_number, desk_dimensions, desk_shifting_request_on, is_cabin, is_mac, updated_on, updated_by, created_on, created_by, desk_dimension_details )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlOfficeId() . ', ' .
 						$this->sqlTeamId() . ', ' .
 						$this->sqlIsReserved() . ', ' .
 						$this->sqlIsActive() . ', ' .
 						$this->sqlDeskShiftingRequestStatus() . ', ' .
 						$this->sqlDeskShiftingRequestBy() . ', ' .
 						$this->sqlDeskNumber() . ', ' .
 						$this->sqlDeskDimensions() . ', ' .
 						$this->sqlDeskShiftingRequestOn() . ', ' .
 						$this->sqlIsCabin() . ', ' .
 						$this->sqlIsMac() . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlDeskDimensionDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_id = ' . $this->sqlOfficeId() . ','; } elseif( true == array_key_exists( 'OfficeId', $this->getChangedColumns() ) ) { $strSql .= ' office_id = ' . $this->sqlOfficeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; } elseif( true == array_key_exists( 'TeamId', $this->getChangedColumns() ) ) { $strSql .= ' team_id = ' . $this->sqlTeamId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_reserved = ' . $this->sqlIsReserved() . ','; } elseif( true == array_key_exists( 'IsReserved', $this->getChangedColumns() ) ) { $strSql .= ' is_reserved = ' . $this->sqlIsReserved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; } elseif( true == array_key_exists( 'IsActive', $this->getChangedColumns() ) ) { $strSql .= ' is_active = ' . $this->sqlIsActive() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desk_shifting_request_status = ' . $this->sqlDeskShiftingRequestStatus() . ','; } elseif( true == array_key_exists( 'DeskShiftingRequestStatus', $this->getChangedColumns() ) ) { $strSql .= ' desk_shifting_request_status = ' . $this->sqlDeskShiftingRequestStatus() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desk_shifting_request_by = ' . $this->sqlDeskShiftingRequestBy() . ','; } elseif( true == array_key_exists( 'DeskShiftingRequestBy', $this->getChangedColumns() ) ) { $strSql .= ' desk_shifting_request_by = ' . $this->sqlDeskShiftingRequestBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desk_number = ' . $this->sqlDeskNumber() . ','; } elseif( true == array_key_exists( 'DeskNumber', $this->getChangedColumns() ) ) { $strSql .= ' desk_number = ' . $this->sqlDeskNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desk_dimensions = ' . $this->sqlDeskDimensions() . ','; } elseif( true == array_key_exists( 'DeskDimensions', $this->getChangedColumns() ) ) { $strSql .= ' desk_dimensions = ' . $this->sqlDeskDimensions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desk_shifting_request_on = ' . $this->sqlDeskShiftingRequestOn() . ','; } elseif( true == array_key_exists( 'DeskShiftingRequestOn', $this->getChangedColumns() ) ) { $strSql .= ' desk_shifting_request_on = ' . $this->sqlDeskShiftingRequestOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_cabin = ' . $this->sqlIsCabin() . ','; } elseif( true == array_key_exists( 'IsCabin', $this->getChangedColumns() ) ) { $strSql .= ' is_cabin = ' . $this->sqlIsCabin() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_mac = ' . $this->sqlIsMac() . ','; } elseif( true == array_key_exists( 'IsMac', $this->getChangedColumns() ) ) { $strSql .= ' is_mac = ' . $this->sqlIsMac() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' desk_dimension_details = ' . $this->sqlDeskDimensionDetails() . ','; } elseif( true == array_key_exists( 'DeskDimensionDetails', $this->getChangedColumns() ) ) { $strSql .= ' desk_dimension_details = ' . $this->sqlDeskDimensionDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'office_id' => $this->getOfficeId(),
			'team_id' => $this->getTeamId(),
			'is_reserved' => $this->getIsReserved(),
			'is_active' => $this->getIsActive(),
			'desk_shifting_request_status' => $this->getDeskShiftingRequestStatus(),
			'desk_shifting_request_by' => $this->getDeskShiftingRequestBy(),
			'desk_number' => $this->getDeskNumber(),
			'desk_dimensions' => $this->getDeskDimensions(),
			'desk_shifting_request_on' => $this->getDeskShiftingRequestOn(),
			'is_cabin' => $this->getIsCabin(),
			'is_mac' => $this->getIsMac(),
			'updated_on' => $this->getUpdatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'created_on' => $this->getCreatedOn(),
			'created_by' => $this->getCreatedBy(),
			'desk_dimension_details' => $this->getDeskDimensionDetails()
		);
	}

}
?>