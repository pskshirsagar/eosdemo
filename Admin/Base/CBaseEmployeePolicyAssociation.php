<?php

class CBaseEmployeePolicyAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_policy_associations';

	protected $m_intId;
	protected $m_intPolicyId;
	protected $m_arrintDesignationIds;
	protected $m_arrstrStateCodes;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_arrintEmployeeIds;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['policy_id'] ) && $boolDirectSet ) $this->set( 'm_intPolicyId', trim( $arrValues['policy_id'] ) ); elseif( isset( $arrValues['policy_id'] ) ) $this->setPolicyId( $arrValues['policy_id'] );
		if( isset( $arrValues['designation_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintDesignationIds', trim( $arrValues['designation_ids'] ) ); elseif( isset( $arrValues['designation_ids'] ) ) $this->setDesignationIds( $arrValues['designation_ids'] );
		if( isset( $arrValues['state_codes'] ) && $boolDirectSet ) $this->set( 'm_arrstrStateCodes', trim( $arrValues['state_codes'] ) ); elseif( isset( $arrValues['state_codes'] ) ) $this->setStateCodes( $arrValues['state_codes'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['employee_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintEmployeeIds', trim( $arrValues['employee_ids'] ) ); elseif( isset( $arrValues['employee_ids'] ) ) $this->setEmployeeIds( $arrValues['employee_ids'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPolicyId( $intPolicyId ) {
		$this->set( 'm_intPolicyId', CStrings::strToIntDef( $intPolicyId, NULL, false ) );
	}

	public function getPolicyId() {
		return $this->m_intPolicyId;
	}

	public function sqlPolicyId() {
		return ( true == isset( $this->m_intPolicyId ) ) ? ( string ) $this->m_intPolicyId : 'NULL';
	}

	public function setDesignationIds( $arrintDesignationIds ) {
		$this->set( 'm_arrintDesignationIds', CStrings::strToArrIntDef( $arrintDesignationIds, NULL ) );
	}

	public function getDesignationIds() {
		return $this->m_arrintDesignationIds;
	}

	public function sqlDesignationIds() {
		return ( true == isset( $this->m_arrintDesignationIds ) && true == valArr( $this->m_arrintDesignationIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintDesignationIds, NULL ) . '\'' : 'NULL';
	}

	public function setStateCodes( $arrstrStateCodes ) {
		$this->set( 'm_arrstrStateCodes', CStrings::strToArrIntDef( $arrstrStateCodes, NULL ) );
	}

	public function getStateCodes() {
		return $this->m_arrstrStateCodes;
	}

	public function sqlStateCodes() {
		return ( true == isset( $this->m_arrstrStateCodes ) && true == valArr( $this->m_arrstrStateCodes ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrstrStateCodes, NULL ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setEmployeeIds( $arrintEmployeeIds ) {
		$this->set( 'm_arrintEmployeeIds', CStrings::strToArrIntDef( $arrintEmployeeIds, NULL ) );
	}

	public function getEmployeeIds() {
		return $this->m_arrintEmployeeIds;
	}

	public function sqlEmployeeIds() {
		return ( true == isset( $this->m_arrintEmployeeIds ) && true == valArr( $this->m_arrintEmployeeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintEmployeeIds, NULL ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, policy_id, designation_ids, state_codes, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, employee_ids )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlPolicyId() . ', ' .
		          $this->sqlDesignationIds() . ', ' .
		          $this->sqlStateCodes() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlEmployeeIds() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' policy_id = ' . $this->sqlPolicyId(). ',' ; } elseif( true == array_key_exists( 'PolicyId', $this->getChangedColumns() ) ) { $strSql .= ' policy_id = ' . $this->sqlPolicyId() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_ids = ' . $this->sqlDesignationIds(). ',' ; } elseif( true == array_key_exists( 'DesignationIds', $this->getChangedColumns() ) ) { $strSql .= ' designation_ids = ' . $this->sqlDesignationIds() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' state_codes = ' . $this->sqlStateCodes(). ',' ; } elseif( true == array_key_exists( 'StateCodes', $this->getChangedColumns() ) ) { $strSql .= ' state_codes = ' . $this->sqlStateCodes() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
		if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_ids = ' . $this->sqlEmployeeIds(). ',' ; } elseif( true == array_key_exists( 'EmployeeIds', $this->getChangedColumns() ) ) { $strSql .= ' employee_ids = ' . $this->sqlEmployeeIds() . ','; $boolUpdate = true; }
		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
		$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'policy_id' => $this->getPolicyId(),
			'designation_ids' => $this->getDesignationIds(),
			'state_codes' => $this->getStateCodes(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'employee_ids' => $this->getEmployeeIds()
		);
	}

}
?>