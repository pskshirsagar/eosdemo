<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTasks
 * Do not add any new functions to this class.
 */

class CBaseSurveyTasks extends CEosPluralBase {

	/**
	 * @return CSurveyTask[]
	 */
	public static function fetchSurveyTasks( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyTask', $objDatabase );
	}

	/**
	 * @return CSurveyTask
	 */
	public static function fetchSurveyTask( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyTask', $objDatabase );
	}

	public static function fetchSurveyTaskCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_tasks', $objDatabase );
	}

	public static function fetchSurveyTaskById( $intId, $objDatabase ) {
		return self::fetchSurveyTask( sprintf( 'SELECT * FROM survey_tasks WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSurveyTasksBySurveyId( $intSurveyId, $objDatabase ) {
		return self::fetchSurveyTasks( sprintf( 'SELECT * FROM survey_tasks WHERE survey_id = %d', ( int ) $intSurveyId ), $objDatabase );
	}

	public static function fetchSurveyTasksByTaskId( $intTaskId, $objDatabase ) {
		return self::fetchSurveyTasks( sprintf( 'SELECT * FROM survey_tasks WHERE task_id = %d', ( int ) $intTaskId ), $objDatabase );
	}

}
?>