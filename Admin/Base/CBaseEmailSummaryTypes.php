<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmailSummaryTypes
 * Do not add any new functions to this class.
 */

class CBaseEmailSummaryTypes extends CEosPluralBase {

	/**
	 * @return CEmailSummaryType[]
	 */
	public static function fetchEmailSummaryTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmailSummaryType', $objDatabase );
	}

	/**
	 * @return CEmailSummaryType
	 */
	public static function fetchEmailSummaryType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmailSummaryType', $objDatabase );
	}

	public static function fetchEmailSummaryTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'email_summary_types', $objDatabase );
	}

	public static function fetchEmailSummaryTypeById( $intId, $objDatabase ) {
		return self::fetchEmailSummaryType( sprintf( 'SELECT * FROM email_summary_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>