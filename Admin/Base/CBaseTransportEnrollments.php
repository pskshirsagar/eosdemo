<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTransportEnrollments
 * Do not add any new functions to this class.
 */

class CBaseTransportEnrollments extends CEosPluralBase {

	/**
	 * @return CTransportEnrollment[]
	 */
	public static function fetchTransportEnrollments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTransportEnrollment::class, $objDatabase );
	}

	/**
	 * @return CTransportEnrollment
	 */
	public static function fetchTransportEnrollment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTransportEnrollment::class, $objDatabase );
	}

	public static function fetchTransportEnrollmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transport_enrollments', $objDatabase );
	}

	public static function fetchTransportEnrollmentById( $intId, $objDatabase ) {
		return self::fetchTransportEnrollment( sprintf( 'SELECT * FROM transport_enrollments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTransportEnrollmentsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchTransportEnrollments( sprintf( 'SELECT * FROM transport_enrollments WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchTransportEnrollmentsByTransportRouteId( $intTransportRouteId, $objDatabase ) {
		return self::fetchTransportEnrollments( sprintf( 'SELECT * FROM transport_enrollments WHERE transport_route_id = %d', ( int ) $intTransportRouteId ), $objDatabase );
	}

	public static function fetchTransportEnrollmentsByTransportPickupPointId( $intTransportPickupPointId, $objDatabase ) {
		return self::fetchTransportEnrollments( sprintf( 'SELECT * FROM transport_enrollments WHERE transport_pickup_point_id = %d', ( int ) $intTransportPickupPointId ), $objDatabase );
	}

	public static function fetchTransportEnrollmentsByTransportRequestStatusId( $intTransportRequestStatusId, $objDatabase ) {
		return self::fetchTransportEnrollments( sprintf( 'SELECT * FROM transport_enrollments WHERE transport_request_status_id = %d', ( int ) $intTransportRequestStatusId ), $objDatabase );
	}

	public static function fetchTransportEnrollmentsByProposedTransportRouteId( $intProposedTransportRouteId, $objDatabase ) {
		return self::fetchTransportEnrollments( sprintf( 'SELECT * FROM transport_enrollments WHERE proposed_transport_route_id = %d', ( int ) $intProposedTransportRouteId ), $objDatabase );
	}

	public static function fetchTransportEnrollmentsByProposedTransportPickupPointId( $intProposedTransportPickupPointId, $objDatabase ) {
		return self::fetchTransportEnrollments( sprintf( 'SELECT * FROM transport_enrollments WHERE proposed_transport_pickup_point_id = %d', ( int ) $intProposedTransportPickupPointId ), $objDatabase );
	}

}
?>