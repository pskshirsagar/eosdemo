<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportPermissions
 * Do not add any new functions to this class.
 */

class CBaseReportPermissions extends CEosPluralBase {

	/**
	 * @return CReportPermission[]
	 */
	public static function fetchReportPermissions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CReportPermission::class, $objDatabase );
	}

	/**
	 * @return CReportPermission
	 */
	public static function fetchReportPermission( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CReportPermission::class, $objDatabase );
	}

	public static function fetchReportPermissionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'report_permissions', $objDatabase );
	}

	public static function fetchReportPermissionById( $intId, $objDatabase ) {
		return self::fetchReportPermission( sprintf( 'SELECT * FROM report_permissions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReportPermissionsByCompanyReportId( $intCompanyReportId, $objDatabase ) {
		return self::fetchReportPermissions( sprintf( 'SELECT * FROM report_permissions WHERE company_report_id = %d', ( int ) $intCompanyReportId ), $objDatabase );
	}

	public static function fetchReportPermissionsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchReportPermissions( sprintf( 'SELECT * FROM report_permissions WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchReportPermissionsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchReportPermissions( sprintf( 'SELECT * FROM report_permissions WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchReportPermissionsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchReportPermissions( sprintf( 'SELECT * FROM report_permissions WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

}
?>