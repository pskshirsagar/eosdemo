<?php

class CBaseDocumentTemplateAddenda extends CEosSingularBase {

	const TABLE_NAME = 'public.document_template_addendas';

	protected $m_intId;
	protected $m_intLeaseDocumentTemplateId;
	protected $m_intAddendaDocumentTemplateId;
	protected $m_intDocumentTemplateAddendaId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intRequireConfirmation;
	protected $m_intAttachToEmail;
	protected $m_intIsForPrimaryApplicant;
	protected $m_intIsForCoApplicant;
	protected $m_intIsForCoSigner;
	protected $m_intIsRequired;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intArchivedBy;
	protected $m_strArchivedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intRequireConfirmation = '0';
		$this->m_intAttachToEmail = '0';
		$this->m_intIsForPrimaryApplicant = '1';
		$this->m_intIsForCoApplicant = '1';
		$this->m_intIsForCoSigner = '1';
		$this->m_intIsRequired = '0';
		$this->m_intIsPublished = '1';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['lease_document_template_id'] ) && $boolDirectSet ) $this->set( 'm_intLeaseDocumentTemplateId', trim( $arrValues['lease_document_template_id'] ) ); elseif( isset( $arrValues['lease_document_template_id'] ) ) $this->setLeaseDocumentTemplateId( $arrValues['lease_document_template_id'] );
		if( isset( $arrValues['addenda_document_template_id'] ) && $boolDirectSet ) $this->set( 'm_intAddendaDocumentTemplateId', trim( $arrValues['addenda_document_template_id'] ) ); elseif( isset( $arrValues['addenda_document_template_id'] ) ) $this->setAddendaDocumentTemplateId( $arrValues['addenda_document_template_id'] );
		if( isset( $arrValues['document_template_addenda_id'] ) && $boolDirectSet ) $this->set( 'm_intDocumentTemplateAddendaId', trim( $arrValues['document_template_addenda_id'] ) ); elseif( isset( $arrValues['document_template_addenda_id'] ) ) $this->setDocumentTemplateAddendaId( $arrValues['document_template_addenda_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['require_confirmation'] ) && $boolDirectSet ) $this->set( 'm_intRequireConfirmation', trim( $arrValues['require_confirmation'] ) ); elseif( isset( $arrValues['require_confirmation'] ) ) $this->setRequireConfirmation( $arrValues['require_confirmation'] );
		if( isset( $arrValues['attach_to_email'] ) && $boolDirectSet ) $this->set( 'm_intAttachToEmail', trim( $arrValues['attach_to_email'] ) ); elseif( isset( $arrValues['attach_to_email'] ) ) $this->setAttachToEmail( $arrValues['attach_to_email'] );
		if( isset( $arrValues['is_for_primary_applicant'] ) && $boolDirectSet ) $this->set( 'm_intIsForPrimaryApplicant', trim( $arrValues['is_for_primary_applicant'] ) ); elseif( isset( $arrValues['is_for_primary_applicant'] ) ) $this->setIsForPrimaryApplicant( $arrValues['is_for_primary_applicant'] );
		if( isset( $arrValues['is_for_co_applicant'] ) && $boolDirectSet ) $this->set( 'm_intIsForCoApplicant', trim( $arrValues['is_for_co_applicant'] ) ); elseif( isset( $arrValues['is_for_co_applicant'] ) ) $this->setIsForCoApplicant( $arrValues['is_for_co_applicant'] );
		if( isset( $arrValues['is_for_co_signer'] ) && $boolDirectSet ) $this->set( 'm_intIsForCoSigner', trim( $arrValues['is_for_co_signer'] ) ); elseif( isset( $arrValues['is_for_co_signer'] ) ) $this->setIsForCoSigner( $arrValues['is_for_co_signer'] );
		if( isset( $arrValues['is_required'] ) && $boolDirectSet ) $this->set( 'm_intIsRequired', trim( $arrValues['is_required'] ) ); elseif( isset( $arrValues['is_required'] ) ) $this->setIsRequired( $arrValues['is_required'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['archived_by'] ) && $boolDirectSet ) $this->set( 'm_intArchivedBy', trim( $arrValues['archived_by'] ) ); elseif( isset( $arrValues['archived_by'] ) ) $this->setArchivedBy( $arrValues['archived_by'] );
		if( isset( $arrValues['archived_on'] ) && $boolDirectSet ) $this->set( 'm_strArchivedOn', trim( $arrValues['archived_on'] ) ); elseif( isset( $arrValues['archived_on'] ) ) $this->setArchivedOn( $arrValues['archived_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setLeaseDocumentTemplateId( $intLeaseDocumentTemplateId ) {
		$this->set( 'm_intLeaseDocumentTemplateId', CStrings::strToIntDef( $intLeaseDocumentTemplateId, NULL, false ) );
	}

	public function getLeaseDocumentTemplateId() {
		return $this->m_intLeaseDocumentTemplateId;
	}

	public function sqlLeaseDocumentTemplateId() {
		return ( true == isset( $this->m_intLeaseDocumentTemplateId ) ) ? ( string ) $this->m_intLeaseDocumentTemplateId : 'NULL';
	}

	public function setAddendaDocumentTemplateId( $intAddendaDocumentTemplateId ) {
		$this->set( 'm_intAddendaDocumentTemplateId', CStrings::strToIntDef( $intAddendaDocumentTemplateId, NULL, false ) );
	}

	public function getAddendaDocumentTemplateId() {
		return $this->m_intAddendaDocumentTemplateId;
	}

	public function sqlAddendaDocumentTemplateId() {
		return ( true == isset( $this->m_intAddendaDocumentTemplateId ) ) ? ( string ) $this->m_intAddendaDocumentTemplateId : 'NULL';
	}

	public function setDocumentTemplateAddendaId( $intDocumentTemplateAddendaId ) {
		$this->set( 'm_intDocumentTemplateAddendaId', CStrings::strToIntDef( $intDocumentTemplateAddendaId, NULL, false ) );
	}

	public function getDocumentTemplateAddendaId() {
		return $this->m_intDocumentTemplateAddendaId;
	}

	public function sqlDocumentTemplateAddendaId() {
		return ( true == isset( $this->m_intDocumentTemplateAddendaId ) ) ? ( string ) $this->m_intDocumentTemplateAddendaId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setRequireConfirmation( $intRequireConfirmation ) {
		$this->set( 'm_intRequireConfirmation', CStrings::strToIntDef( $intRequireConfirmation, NULL, false ) );
	}

	public function getRequireConfirmation() {
		return $this->m_intRequireConfirmation;
	}

	public function sqlRequireConfirmation() {
		return ( true == isset( $this->m_intRequireConfirmation ) ) ? ( string ) $this->m_intRequireConfirmation : '0';
	}

	public function setAttachToEmail( $intAttachToEmail ) {
		$this->set( 'm_intAttachToEmail', CStrings::strToIntDef( $intAttachToEmail, NULL, false ) );
	}

	public function getAttachToEmail() {
		return $this->m_intAttachToEmail;
	}

	public function sqlAttachToEmail() {
		return ( true == isset( $this->m_intAttachToEmail ) ) ? ( string ) $this->m_intAttachToEmail : '0';
	}

	public function setIsForPrimaryApplicant( $intIsForPrimaryApplicant ) {
		$this->set( 'm_intIsForPrimaryApplicant', CStrings::strToIntDef( $intIsForPrimaryApplicant, NULL, false ) );
	}

	public function getIsForPrimaryApplicant() {
		return $this->m_intIsForPrimaryApplicant;
	}

	public function sqlIsForPrimaryApplicant() {
		return ( true == isset( $this->m_intIsForPrimaryApplicant ) ) ? ( string ) $this->m_intIsForPrimaryApplicant : '1';
	}

	public function setIsForCoApplicant( $intIsForCoApplicant ) {
		$this->set( 'm_intIsForCoApplicant', CStrings::strToIntDef( $intIsForCoApplicant, NULL, false ) );
	}

	public function getIsForCoApplicant() {
		return $this->m_intIsForCoApplicant;
	}

	public function sqlIsForCoApplicant() {
		return ( true == isset( $this->m_intIsForCoApplicant ) ) ? ( string ) $this->m_intIsForCoApplicant : '1';
	}

	public function setIsForCoSigner( $intIsForCoSigner ) {
		$this->set( 'm_intIsForCoSigner', CStrings::strToIntDef( $intIsForCoSigner, NULL, false ) );
	}

	public function getIsForCoSigner() {
		return $this->m_intIsForCoSigner;
	}

	public function sqlIsForCoSigner() {
		return ( true == isset( $this->m_intIsForCoSigner ) ) ? ( string ) $this->m_intIsForCoSigner : '1';
	}

	public function setIsRequired( $intIsRequired ) {
		$this->set( 'm_intIsRequired', CStrings::strToIntDef( $intIsRequired, NULL, false ) );
	}

	public function getIsRequired() {
		return $this->m_intIsRequired;
	}

	public function sqlIsRequired() {
		return ( true == isset( $this->m_intIsRequired ) ) ? ( string ) $this->m_intIsRequired : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : 'NULL';
	}

	public function setArchivedBy( $intArchivedBy ) {
		$this->set( 'm_intArchivedBy', CStrings::strToIntDef( $intArchivedBy, NULL, false ) );
	}

	public function getArchivedBy() {
		return $this->m_intArchivedBy;
	}

	public function sqlArchivedBy() {
		return ( true == isset( $this->m_intArchivedBy ) ) ? ( string ) $this->m_intArchivedBy : 'NULL';
	}

	public function setArchivedOn( $strArchivedOn ) {
		$this->set( 'm_strArchivedOn', CStrings::strTrimDef( $strArchivedOn, -1, NULL, true ) );
	}

	public function getArchivedOn() {
		return $this->m_strArchivedOn;
	}

	public function sqlArchivedOn() {
		return ( true == isset( $this->m_strArchivedOn ) ) ? '\'' . $this->m_strArchivedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, lease_document_template_id, addenda_document_template_id, document_template_addenda_id, name, description, require_confirmation, attach_to_email, is_for_primary_applicant, is_for_co_applicant, is_for_co_signer, is_required, is_published, order_num, archived_by, archived_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlLeaseDocumentTemplateId() . ', ' .
 						$this->sqlAddendaDocumentTemplateId() . ', ' .
 						$this->sqlDocumentTemplateAddendaId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlRequireConfirmation() . ', ' .
 						$this->sqlAttachToEmail() . ', ' .
 						$this->sqlIsForPrimaryApplicant() . ', ' .
 						$this->sqlIsForCoApplicant() . ', ' .
 						$this->sqlIsForCoSigner() . ', ' .
 						$this->sqlIsRequired() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlArchivedBy() . ', ' .
 						$this->sqlArchivedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lease_document_template_id = ' . $this->sqlLeaseDocumentTemplateId() . ','; } elseif( true == array_key_exists( 'LeaseDocumentTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' lease_document_template_id = ' . $this->sqlLeaseDocumentTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' addenda_document_template_id = ' . $this->sqlAddendaDocumentTemplateId() . ','; } elseif( true == array_key_exists( 'AddendaDocumentTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' addenda_document_template_id = ' . $this->sqlAddendaDocumentTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' document_template_addenda_id = ' . $this->sqlDocumentTemplateAddendaId() . ','; } elseif( true == array_key_exists( 'DocumentTemplateAddendaId', $this->getChangedColumns() ) ) { $strSql .= ' document_template_addenda_id = ' . $this->sqlDocumentTemplateAddendaId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' require_confirmation = ' . $this->sqlRequireConfirmation() . ','; } elseif( true == array_key_exists( 'RequireConfirmation', $this->getChangedColumns() ) ) { $strSql .= ' require_confirmation = ' . $this->sqlRequireConfirmation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' attach_to_email = ' . $this->sqlAttachToEmail() . ','; } elseif( true == array_key_exists( 'AttachToEmail', $this->getChangedColumns() ) ) { $strSql .= ' attach_to_email = ' . $this->sqlAttachToEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_for_primary_applicant = ' . $this->sqlIsForPrimaryApplicant() . ','; } elseif( true == array_key_exists( 'IsForPrimaryApplicant', $this->getChangedColumns() ) ) { $strSql .= ' is_for_primary_applicant = ' . $this->sqlIsForPrimaryApplicant() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_for_co_applicant = ' . $this->sqlIsForCoApplicant() . ','; } elseif( true == array_key_exists( 'IsForCoApplicant', $this->getChangedColumns() ) ) { $strSql .= ' is_for_co_applicant = ' . $this->sqlIsForCoApplicant() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_for_co_signer = ' . $this->sqlIsForCoSigner() . ','; } elseif( true == array_key_exists( 'IsForCoSigner', $this->getChangedColumns() ) ) { $strSql .= ' is_for_co_signer = ' . $this->sqlIsForCoSigner() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; } elseif( true == array_key_exists( 'IsRequired', $this->getChangedColumns() ) ) { $strSql .= ' is_required = ' . $this->sqlIsRequired() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archived_by = ' . $this->sqlArchivedBy() . ','; } elseif( true == array_key_exists( 'ArchivedBy', $this->getChangedColumns() ) ) { $strSql .= ' archived_by = ' . $this->sqlArchivedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' archived_on = ' . $this->sqlArchivedOn() . ','; } elseif( true == array_key_exists( 'ArchivedOn', $this->getChangedColumns() ) ) { $strSql .= ' archived_on = ' . $this->sqlArchivedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'lease_document_template_id' => $this->getLeaseDocumentTemplateId(),
			'addenda_document_template_id' => $this->getAddendaDocumentTemplateId(),
			'document_template_addenda_id' => $this->getDocumentTemplateAddendaId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'require_confirmation' => $this->getRequireConfirmation(),
			'attach_to_email' => $this->getAttachToEmail(),
			'is_for_primary_applicant' => $this->getIsForPrimaryApplicant(),
			'is_for_co_applicant' => $this->getIsForCoApplicant(),
			'is_for_co_signer' => $this->getIsForCoSigner(),
			'is_required' => $this->getIsRequired(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'archived_by' => $this->getArchivedBy(),
			'archived_on' => $this->getArchivedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>