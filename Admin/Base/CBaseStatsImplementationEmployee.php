<?php

class CBaseStatsImplementationEmployee extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_implementation_employees';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_strWeek;
	protected $m_fltAverageDaysToImplement;
	protected $m_intUnimplementedAcv;
	protected $m_intImplementedAcv;
	protected $m_intImplementationWeightScoreTotal;
	protected $m_intPropertyProductsImplemented;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_fltAverageDaysToImplement = '0';
		$this->m_intUnimplementedAcv = '0';
		$this->m_intImplementedAcv = '0';
		$this->m_intImplementationWeightScoreTotal = '0';
		$this->m_intPropertyProductsImplemented = '0';
		$this->m_intCreatedBy = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['week'] ) && $boolDirectSet ) $this->set( 'm_strWeek', trim( $arrValues['week'] ) ); elseif( isset( $arrValues['week'] ) ) $this->setWeek( $arrValues['week'] );
		if( isset( $arrValues['average_days_to_implement'] ) && $boolDirectSet ) $this->set( 'm_fltAverageDaysToImplement', trim( $arrValues['average_days_to_implement'] ) ); elseif( isset( $arrValues['average_days_to_implement'] ) ) $this->setAverageDaysToImplement( $arrValues['average_days_to_implement'] );
		if( isset( $arrValues['unimplemented_acv'] ) && $boolDirectSet ) $this->set( 'm_intUnimplementedAcv', trim( $arrValues['unimplemented_acv'] ) ); elseif( isset( $arrValues['unimplemented_acv'] ) ) $this->setUnimplementedAcv( $arrValues['unimplemented_acv'] );
		if( isset( $arrValues['implemented_acv'] ) && $boolDirectSet ) $this->set( 'm_intImplementedAcv', trim( $arrValues['implemented_acv'] ) ); elseif( isset( $arrValues['implemented_acv'] ) ) $this->setImplementedAcv( $arrValues['implemented_acv'] );
		if( isset( $arrValues['implementation_weight_score_total'] ) && $boolDirectSet ) $this->set( 'm_intImplementationWeightScoreTotal', trim( $arrValues['implementation_weight_score_total'] ) ); elseif( isset( $arrValues['implementation_weight_score_total'] ) ) $this->setImplementationWeightScoreTotal( $arrValues['implementation_weight_score_total'] );
		if( isset( $arrValues['property_products_implemented'] ) && $boolDirectSet ) $this->set( 'm_intPropertyProductsImplemented', trim( $arrValues['property_products_implemented'] ) ); elseif( isset( $arrValues['property_products_implemented'] ) ) $this->setPropertyProductsImplemented( $arrValues['property_products_implemented'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setWeek( $strWeek ) {
		$this->set( 'm_strWeek', CStrings::strTrimDef( $strWeek, -1, NULL, true ) );
	}

	public function getWeek() {
		return $this->m_strWeek;
	}

	public function sqlWeek() {
		return ( true == isset( $this->m_strWeek ) ) ? '\'' . $this->m_strWeek . '\'' : 'NOW()';
	}

	public function setAverageDaysToImplement( $fltAverageDaysToImplement ) {
		$this->set( 'm_fltAverageDaysToImplement', CStrings::strToFloatDef( $fltAverageDaysToImplement, NULL, false, 2 ) );
	}

	public function getAverageDaysToImplement() {
		return $this->m_fltAverageDaysToImplement;
	}

	public function sqlAverageDaysToImplement() {
		return ( true == isset( $this->m_fltAverageDaysToImplement ) ) ? ( string ) $this->m_fltAverageDaysToImplement : '0';
	}

	public function setUnimplementedAcv( $intUnimplementedAcv ) {
		$this->set( 'm_intUnimplementedAcv', CStrings::strToIntDef( $intUnimplementedAcv, NULL, false ) );
	}

	public function getUnimplementedAcv() {
		return $this->m_intUnimplementedAcv;
	}

	public function sqlUnimplementedAcv() {
		return ( true == isset( $this->m_intUnimplementedAcv ) ) ? ( string ) $this->m_intUnimplementedAcv : '0';
	}

	public function setImplementedAcv( $intImplementedAcv ) {
		$this->set( 'm_intImplementedAcv', CStrings::strToIntDef( $intImplementedAcv, NULL, false ) );
	}

	public function getImplementedAcv() {
		return $this->m_intImplementedAcv;
	}

	public function sqlImplementedAcv() {
		return ( true == isset( $this->m_intImplementedAcv ) ) ? ( string ) $this->m_intImplementedAcv : '0';
	}

	public function setImplementationWeightScoreTotal( $intImplementationWeightScoreTotal ) {
		$this->set( 'm_intImplementationWeightScoreTotal', CStrings::strToIntDef( $intImplementationWeightScoreTotal, NULL, false ) );
	}

	public function getImplementationWeightScoreTotal() {
		return $this->m_intImplementationWeightScoreTotal;
	}

	public function sqlImplementationWeightScoreTotal() {
		return ( true == isset( $this->m_intImplementationWeightScoreTotal ) ) ? ( string ) $this->m_intImplementationWeightScoreTotal : '0';
	}

	public function setPropertyProductsImplemented( $intPropertyProductsImplemented ) {
		$this->set( 'm_intPropertyProductsImplemented', CStrings::strToIntDef( $intPropertyProductsImplemented, NULL, false ) );
	}

	public function getPropertyProductsImplemented() {
		return $this->m_intPropertyProductsImplemented;
	}

	public function sqlPropertyProductsImplemented() {
		return ( true == isset( $this->m_intPropertyProductsImplemented ) ) ? ( string ) $this->m_intPropertyProductsImplemented : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : '0';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, week, average_days_to_implement, unimplemented_acv, implemented_acv, implementation_weight_score_total, property_products_implemented, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlWeek() . ', ' .
 						$this->sqlAverageDaysToImplement() . ', ' .
 						$this->sqlUnimplementedAcv() . ', ' .
 						$this->sqlImplementedAcv() . ', ' .
 						$this->sqlImplementationWeightScoreTotal() . ', ' .
 						$this->sqlPropertyProductsImplemented() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' week = ' . $this->sqlWeek() . ','; } elseif( true == array_key_exists( 'Week', $this->getChangedColumns() ) ) { $strSql .= ' week = ' . $this->sqlWeek() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_days_to_implement = ' . $this->sqlAverageDaysToImplement() . ','; } elseif( true == array_key_exists( 'AverageDaysToImplement', $this->getChangedColumns() ) ) { $strSql .= ' average_days_to_implement = ' . $this->sqlAverageDaysToImplement() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unimplemented_acv = ' . $this->sqlUnimplementedAcv() . ','; } elseif( true == array_key_exists( 'UnimplementedAcv', $this->getChangedColumns() ) ) { $strSql .= ' unimplemented_acv = ' . $this->sqlUnimplementedAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implemented_acv = ' . $this->sqlImplementedAcv() . ','; } elseif( true == array_key_exists( 'ImplementedAcv', $this->getChangedColumns() ) ) { $strSql .= ' implemented_acv = ' . $this->sqlImplementedAcv() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_weight_score_total = ' . $this->sqlImplementationWeightScoreTotal() . ','; } elseif( true == array_key_exists( 'ImplementationWeightScoreTotal', $this->getChangedColumns() ) ) { $strSql .= ' implementation_weight_score_total = ' . $this->sqlImplementationWeightScoreTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_products_implemented = ' . $this->sqlPropertyProductsImplemented() . ','; } elseif( true == array_key_exists( 'PropertyProductsImplemented', $this->getChangedColumns() ) ) { $strSql .= ' property_products_implemented = ' . $this->sqlPropertyProductsImplemented() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'week' => $this->getWeek(),
			'average_days_to_implement' => $this->getAverageDaysToImplement(),
			'unimplemented_acv' => $this->getUnimplementedAcv(),
			'implemented_acv' => $this->getImplementedAcv(),
			'implementation_weight_score_total' => $this->getImplementationWeightScoreTotal(),
			'property_products_implemented' => $this->getPropertyProductsImplemented(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>