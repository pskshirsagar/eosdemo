<?php

class CBaseSemAdGroupSourceLog extends CEosSingularBase {

	const TABLE_NAME = 'public.sem_ad_group_source_logs';

	protected $m_intId;
	protected $m_intSemSourceId;
	protected $m_intSemAccountId;
	protected $m_intSemCampaignId;
	protected $m_intSemAdGroupId;
	protected $m_strLogDatetime;
	protected $m_intOldSemStatusTypeId;
	protected $m_intNewSemStatusTypeId;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['sem_source_id'] ) && $boolDirectSet ) $this->set( 'm_intSemSourceId', trim( $arrValues['sem_source_id'] ) ); elseif( isset( $arrValues['sem_source_id'] ) ) $this->setSemSourceId( $arrValues['sem_source_id'] );
		if( isset( $arrValues['sem_account_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAccountId', trim( $arrValues['sem_account_id'] ) ); elseif( isset( $arrValues['sem_account_id'] ) ) $this->setSemAccountId( $arrValues['sem_account_id'] );
		if( isset( $arrValues['sem_campaign_id'] ) && $boolDirectSet ) $this->set( 'm_intSemCampaignId', trim( $arrValues['sem_campaign_id'] ) ); elseif( isset( $arrValues['sem_campaign_id'] ) ) $this->setSemCampaignId( $arrValues['sem_campaign_id'] );
		if( isset( $arrValues['sem_ad_group_id'] ) && $boolDirectSet ) $this->set( 'm_intSemAdGroupId', trim( $arrValues['sem_ad_group_id'] ) ); elseif( isset( $arrValues['sem_ad_group_id'] ) ) $this->setSemAdGroupId( $arrValues['sem_ad_group_id'] );
		if( isset( $arrValues['log_datetime'] ) && $boolDirectSet ) $this->set( 'm_strLogDatetime', trim( $arrValues['log_datetime'] ) ); elseif( isset( $arrValues['log_datetime'] ) ) $this->setLogDatetime( $arrValues['log_datetime'] );
		if( isset( $arrValues['old_sem_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOldSemStatusTypeId', trim( $arrValues['old_sem_status_type_id'] ) ); elseif( isset( $arrValues['old_sem_status_type_id'] ) ) $this->setOldSemStatusTypeId( $arrValues['old_sem_status_type_id'] );
		if( isset( $arrValues['new_sem_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intNewSemStatusTypeId', trim( $arrValues['new_sem_status_type_id'] ) ); elseif( isset( $arrValues['new_sem_status_type_id'] ) ) $this->setNewSemStatusTypeId( $arrValues['new_sem_status_type_id'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setSemSourceId( $intSemSourceId ) {
		$this->set( 'm_intSemSourceId', CStrings::strToIntDef( $intSemSourceId, NULL, false ) );
	}

	public function getSemSourceId() {
		return $this->m_intSemSourceId;
	}

	public function sqlSemSourceId() {
		return ( true == isset( $this->m_intSemSourceId ) ) ? ( string ) $this->m_intSemSourceId : 'NULL';
	}

	public function setSemAccountId( $intSemAccountId ) {
		$this->set( 'm_intSemAccountId', CStrings::strToIntDef( $intSemAccountId, NULL, false ) );
	}

	public function getSemAccountId() {
		return $this->m_intSemAccountId;
	}

	public function sqlSemAccountId() {
		return ( true == isset( $this->m_intSemAccountId ) ) ? ( string ) $this->m_intSemAccountId : 'NULL';
	}

	public function setSemCampaignId( $intSemCampaignId ) {
		$this->set( 'm_intSemCampaignId', CStrings::strToIntDef( $intSemCampaignId, NULL, false ) );
	}

	public function getSemCampaignId() {
		return $this->m_intSemCampaignId;
	}

	public function sqlSemCampaignId() {
		return ( true == isset( $this->m_intSemCampaignId ) ) ? ( string ) $this->m_intSemCampaignId : 'NULL';
	}

	public function setSemAdGroupId( $intSemAdGroupId ) {
		$this->set( 'm_intSemAdGroupId', CStrings::strToIntDef( $intSemAdGroupId, NULL, false ) );
	}

	public function getSemAdGroupId() {
		return $this->m_intSemAdGroupId;
	}

	public function sqlSemAdGroupId() {
		return ( true == isset( $this->m_intSemAdGroupId ) ) ? ( string ) $this->m_intSemAdGroupId : 'NULL';
	}

	public function setLogDatetime( $strLogDatetime ) {
		$this->set( 'm_strLogDatetime', CStrings::strTrimDef( $strLogDatetime, -1, NULL, true ) );
	}

	public function getLogDatetime() {
		return $this->m_strLogDatetime;
	}

	public function sqlLogDatetime() {
		return ( true == isset( $this->m_strLogDatetime ) ) ? '\'' . $this->m_strLogDatetime . '\'' : 'NOW()';
	}

	public function setOldSemStatusTypeId( $intOldSemStatusTypeId ) {
		$this->set( 'm_intOldSemStatusTypeId', CStrings::strToIntDef( $intOldSemStatusTypeId, NULL, false ) );
	}

	public function getOldSemStatusTypeId() {
		return $this->m_intOldSemStatusTypeId;
	}

	public function sqlOldSemStatusTypeId() {
		return ( true == isset( $this->m_intOldSemStatusTypeId ) ) ? ( string ) $this->m_intOldSemStatusTypeId : 'NULL';
	}

	public function setNewSemStatusTypeId( $intNewSemStatusTypeId ) {
		$this->set( 'm_intNewSemStatusTypeId', CStrings::strToIntDef( $intNewSemStatusTypeId, NULL, false ) );
	}

	public function getNewSemStatusTypeId() {
		return $this->m_intNewSemStatusTypeId;
	}

	public function sqlNewSemStatusTypeId() {
		return ( true == isset( $this->m_intNewSemStatusTypeId ) ) ? ( string ) $this->m_intNewSemStatusTypeId : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, sem_source_id, sem_account_id, sem_campaign_id, sem_ad_group_id, log_datetime, old_sem_status_type_id, new_sem_status_type_id, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlSemSourceId() . ', ' .
 						$this->sqlSemAccountId() . ', ' .
 						$this->sqlSemCampaignId() . ', ' .
 						$this->sqlSemAdGroupId() . ', ' .
 						$this->sqlLogDatetime() . ', ' .
 						$this->sqlOldSemStatusTypeId() . ', ' .
 						$this->sqlNewSemStatusTypeId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; } elseif( true == array_key_exists( 'SemSourceId', $this->getChangedColumns() ) ) { $strSql .= ' sem_source_id = ' . $this->sqlSemSourceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_account_id = ' . $this->sqlSemAccountId() . ','; } elseif( true == array_key_exists( 'SemAccountId', $this->getChangedColumns() ) ) { $strSql .= ' sem_account_id = ' . $this->sqlSemAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_campaign_id = ' . $this->sqlSemCampaignId() . ','; } elseif( true == array_key_exists( 'SemCampaignId', $this->getChangedColumns() ) ) { $strSql .= ' sem_campaign_id = ' . $this->sqlSemCampaignId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; } elseif( true == array_key_exists( 'SemAdGroupId', $this->getChangedColumns() ) ) { $strSql .= ' sem_ad_group_id = ' . $this->sqlSemAdGroupId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; } elseif( true == array_key_exists( 'LogDatetime', $this->getChangedColumns() ) ) { $strSql .= ' log_datetime = ' . $this->sqlLogDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_sem_status_type_id = ' . $this->sqlOldSemStatusTypeId() . ','; } elseif( true == array_key_exists( 'OldSemStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' old_sem_status_type_id = ' . $this->sqlOldSemStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_sem_status_type_id = ' . $this->sqlNewSemStatusTypeId() . ','; } elseif( true == array_key_exists( 'NewSemStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' new_sem_status_type_id = ' . $this->sqlNewSemStatusTypeId() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'sem_source_id' => $this->getSemSourceId(),
			'sem_account_id' => $this->getSemAccountId(),
			'sem_campaign_id' => $this->getSemCampaignId(),
			'sem_ad_group_id' => $this->getSemAdGroupId(),
			'log_datetime' => $this->getLogDatetime(),
			'old_sem_status_type_id' => $this->getOldSemStatusTypeId(),
			'new_sem_status_type_id' => $this->getNewSemStatusTypeId(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>