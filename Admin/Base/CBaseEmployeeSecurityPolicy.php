<?php

class CBaseEmployeeSecurityPolicy extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_security_policies';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_strYear;
	protected $m_intSecurityPolicySignedBy;
	protected $m_strSecurityPolicySignedOn;
	protected $m_strSecurityPolicyDownloadedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['year'] ) && $boolDirectSet ) $this->set( 'm_strYear', trim( stripcslashes( $arrValues['year'] ) ) ); elseif( isset( $arrValues['year'] ) ) $this->setYear( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['year'] ) : $arrValues['year'] );
		if( isset( $arrValues['security_policy_signed_by'] ) && $boolDirectSet ) $this->set( 'm_intSecurityPolicySignedBy', trim( $arrValues['security_policy_signed_by'] ) ); elseif( isset( $arrValues['security_policy_signed_by'] ) ) $this->setSecurityPolicySignedBy( $arrValues['security_policy_signed_by'] );
		if( isset( $arrValues['security_policy_signed_on'] ) && $boolDirectSet ) $this->set( 'm_strSecurityPolicySignedOn', trim( $arrValues['security_policy_signed_on'] ) ); elseif( isset( $arrValues['security_policy_signed_on'] ) ) $this->setSecurityPolicySignedOn( $arrValues['security_policy_signed_on'] );
		if( isset( $arrValues['security_policy_downloaded_on'] ) && $boolDirectSet ) $this->set( 'm_strSecurityPolicyDownloadedOn', trim( $arrValues['security_policy_downloaded_on'] ) ); elseif( isset( $arrValues['security_policy_downloaded_on'] ) ) $this->setSecurityPolicyDownloadedOn( $arrValues['security_policy_downloaded_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setYear( $strYear ) {
		$this->set( 'm_strYear', CStrings::strTrimDef( $strYear, 10, NULL, true ) );
	}

	public function getYear() {
		return $this->m_strYear;
	}

	public function sqlYear() {
		return ( true == isset( $this->m_strYear ) ) ? '\'' . addslashes( $this->m_strYear ) . '\'' : 'NULL';
	}

	public function setSecurityPolicySignedBy( $intSecurityPolicySignedBy ) {
		$this->set( 'm_intSecurityPolicySignedBy', CStrings::strToIntDef( $intSecurityPolicySignedBy, NULL, false ) );
	}

	public function getSecurityPolicySignedBy() {
		return $this->m_intSecurityPolicySignedBy;
	}

	public function sqlSecurityPolicySignedBy() {
		return ( true == isset( $this->m_intSecurityPolicySignedBy ) ) ? ( string ) $this->m_intSecurityPolicySignedBy : 'NULL';
	}

	public function setSecurityPolicySignedOn( $strSecurityPolicySignedOn ) {
		$this->set( 'm_strSecurityPolicySignedOn', CStrings::strTrimDef( $strSecurityPolicySignedOn, -1, NULL, true ) );
	}

	public function getSecurityPolicySignedOn() {
		return $this->m_strSecurityPolicySignedOn;
	}

	public function sqlSecurityPolicySignedOn() {
		return ( true == isset( $this->m_strSecurityPolicySignedOn ) ) ? '\'' . $this->m_strSecurityPolicySignedOn . '\'' : 'NULL';
	}

	public function setSecurityPolicyDownloadedOn( $strSecurityPolicyDownloadedOn ) {
		$this->set( 'm_strSecurityPolicyDownloadedOn', CStrings::strTrimDef( $strSecurityPolicyDownloadedOn, -1, NULL, true ) );
	}

	public function getSecurityPolicyDownloadedOn() {
		return $this->m_strSecurityPolicyDownloadedOn;
	}

	public function sqlSecurityPolicyDownloadedOn() {
		return ( true == isset( $this->m_strSecurityPolicyDownloadedOn ) ) ? '\'' . $this->m_strSecurityPolicyDownloadedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, year, security_policy_signed_by, security_policy_signed_on, security_policy_downloaded_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlYear() . ', ' .
 						$this->sqlSecurityPolicySignedBy() . ', ' .
 						$this->sqlSecurityPolicySignedOn() . ', ' .
 						$this->sqlSecurityPolicyDownloadedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' year = ' . $this->sqlYear() . ','; } elseif( true == array_key_exists( 'Year', $this->getChangedColumns() ) ) { $strSql .= ' year = ' . $this->sqlYear() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' security_policy_signed_by = ' . $this->sqlSecurityPolicySignedBy() . ','; } elseif( true == array_key_exists( 'SecurityPolicySignedBy', $this->getChangedColumns() ) ) { $strSql .= ' security_policy_signed_by = ' . $this->sqlSecurityPolicySignedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' security_policy_signed_on = ' . $this->sqlSecurityPolicySignedOn() . ','; } elseif( true == array_key_exists( 'SecurityPolicySignedOn', $this->getChangedColumns() ) ) { $strSql .= ' security_policy_signed_on = ' . $this->sqlSecurityPolicySignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' security_policy_downloaded_on = ' . $this->sqlSecurityPolicyDownloadedOn() . ','; } elseif( true == array_key_exists( 'SecurityPolicyDownloadedOn', $this->getChangedColumns() ) ) { $strSql .= ' security_policy_downloaded_on = ' . $this->sqlSecurityPolicyDownloadedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'year' => $this->getYear(),
			'security_policy_signed_by' => $this->getSecurityPolicySignedBy(),
			'security_policy_signed_on' => $this->getSecurityPolicySignedOn(),
			'security_policy_downloaded_on' => $this->getSecurityPolicyDownloadedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>