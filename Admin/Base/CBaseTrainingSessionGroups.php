<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingSessionGroups
 * Do not add any new functions to this class.
 */

class CBaseTrainingSessionGroups extends CEosPluralBase {

	/**
	 * @return CTrainingSessionGroup[]
	 */
	public static function fetchTrainingSessionGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTrainingSessionGroup', $objDatabase );
	}

	/**
	 * @return CTrainingSessionGroup
	 */
	public static function fetchTrainingSessionGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTrainingSessionGroup', $objDatabase );
	}

	public static function fetchTrainingSessionGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'training_session_groups', $objDatabase );
	}

	public static function fetchTrainingSessionGroupById( $intId, $objDatabase ) {
		return self::fetchTrainingSessionGroup( sprintf( 'SELECT * FROM training_session_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTrainingSessionGroupsByTrainingSessionId( $intTrainingSessionId, $objDatabase ) {
		return self::fetchTrainingSessionGroups( sprintf( 'SELECT * FROM training_session_groups WHERE training_session_id = %d', ( int ) $intTrainingSessionId ), $objDatabase );
	}

	public static function fetchTrainingSessionGroupsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchTrainingSessionGroups( sprintf( 'SELECT * FROM training_session_groups WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

}
?>