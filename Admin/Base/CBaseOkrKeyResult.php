<?php

class CBaseOkrKeyResult extends CEosSingularBase {

	const TABLE_NAME = 'public.okr_key_results';

	protected $m_intId;
	protected $m_intOkrObjectiveId;
	protected $m_intOkrKeyResultStatusTypeId;
	protected $m_intOwnerEmployeeId;
	protected $m_strDescription;
	protected $m_strDeliveryDate;
	protected $m_boolIsOnTrack;
	protected $m_fltProgress;
	protected $m_intOrderNum;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsOnTrack = true;
		$this->m_fltProgress = '0';
		$this->m_intOrderNum = '0';
		$this->m_strCreatedOn = 'now()';
		$this->m_strUpdatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['okr_objective_id'] ) && $boolDirectSet ) $this->set( 'm_intOkrObjectiveId', trim( $arrValues['okr_objective_id'] ) ); elseif( isset( $arrValues['okr_objective_id'] ) ) $this->setOkrObjectiveId( $arrValues['okr_objective_id'] );
		if( isset( $arrValues['okr_key_result_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOkrKeyResultStatusTypeId', trim( $arrValues['okr_key_result_status_type_id'] ) ); elseif( isset( $arrValues['okr_key_result_status_type_id'] ) ) $this->setOkrKeyResultStatusTypeId( $arrValues['okr_key_result_status_type_id'] );
		if( isset( $arrValues['owner_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intOwnerEmployeeId', trim( $arrValues['owner_employee_id'] ) ); elseif( isset( $arrValues['owner_employee_id'] ) ) $this->setOwnerEmployeeId( $arrValues['owner_employee_id'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( $arrValues['description'] ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( $arrValues['description'] );
		if( isset( $arrValues['delivery_date'] ) && $boolDirectSet ) $this->set( 'm_strDeliveryDate', trim( $arrValues['delivery_date'] ) ); elseif( isset( $arrValues['delivery_date'] ) ) $this->setDeliveryDate( $arrValues['delivery_date'] );
		if( isset( $arrValues['is_on_track'] ) && $boolDirectSet ) $this->set( 'm_boolIsOnTrack', trim( stripcslashes( $arrValues['is_on_track'] ) ) ); elseif( isset( $arrValues['is_on_track'] ) ) $this->setIsOnTrack( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_on_track'] ) : $arrValues['is_on_track'] );
		if( isset( $arrValues['progress'] ) && $boolDirectSet ) $this->set( 'm_fltProgress', trim( $arrValues['progress'] ) ); elseif( isset( $arrValues['progress'] ) ) $this->setProgress( $arrValues['progress'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setOkrObjectiveId( $intOkrObjectiveId ) {
		$this->set( 'm_intOkrObjectiveId', CStrings::strToIntDef( $intOkrObjectiveId, NULL, false ) );
	}

	public function getOkrObjectiveId() {
		return $this->m_intOkrObjectiveId;
	}

	public function sqlOkrObjectiveId() {
		return ( true == isset( $this->m_intOkrObjectiveId ) ) ? ( string ) $this->m_intOkrObjectiveId : 'NULL';
	}

	public function setOkrKeyResultStatusTypeId( $intOkrKeyResultStatusTypeId ) {
		$this->set( 'm_intOkrKeyResultStatusTypeId', CStrings::strToIntDef( $intOkrKeyResultStatusTypeId, NULL, false ) );
	}

	public function getOkrKeyResultStatusTypeId() {
		return $this->m_intOkrKeyResultStatusTypeId;
	}

	public function sqlOkrKeyResultStatusTypeId() {
		return ( true == isset( $this->m_intOkrKeyResultStatusTypeId ) ) ? ( string ) $this->m_intOkrKeyResultStatusTypeId : 'NULL';
	}

	public function setOwnerEmployeeId( $intOwnerEmployeeId ) {
		$this->set( 'm_intOwnerEmployeeId', CStrings::strToIntDef( $intOwnerEmployeeId, NULL, false ) );
	}

	public function getOwnerEmployeeId() {
		return $this->m_intOwnerEmployeeId;
	}

	public function sqlOwnerEmployeeId() {
		return ( true == isset( $this->m_intOwnerEmployeeId ) ) ? ( string ) $this->m_intOwnerEmployeeId : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strDescription ) : '\'' . addslashes( $this->m_strDescription ) . '\'' ) : 'NULL';
	}

	public function setDeliveryDate( $strDeliveryDate ) {
		$this->set( 'm_strDeliveryDate', CStrings::strTrimDef( $strDeliveryDate, -1, NULL, true ) );
	}

	public function getDeliveryDate() {
		return $this->m_strDeliveryDate;
	}

	public function sqlDeliveryDate() {
		return ( true == isset( $this->m_strDeliveryDate ) ) ? '\'' . $this->m_strDeliveryDate . '\'' : 'NULL';
	}

	public function setIsOnTrack( $boolIsOnTrack ) {
		$this->set( 'm_boolIsOnTrack', CStrings::strToBool( $boolIsOnTrack ) );
	}

	public function getIsOnTrack() {
		return $this->m_boolIsOnTrack;
	}

	public function sqlIsOnTrack() {
		return ( true == isset( $this->m_boolIsOnTrack ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsOnTrack ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setProgress( $fltProgress ) {
		$this->set( 'm_fltProgress', CStrings::strToFloatDef( $fltProgress, NULL, false, 2 ) );
	}

	public function getProgress() {
		return $this->m_fltProgress;
	}

	public function sqlProgress() {
		return ( true == isset( $this->m_fltProgress ) ) ? ( string ) $this->m_fltProgress : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, okr_objective_id, okr_key_result_status_type_id, owner_employee_id, description, delivery_date, is_on_track, progress, order_num, created_by, created_on, updated_by, updated_on, deleted_by, deleted_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlOkrObjectiveId() . ', ' .
						$this->sqlOkrKeyResultStatusTypeId() . ', ' .
						$this->sqlOwnerEmployeeId() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlDeliveryDate() . ', ' .
						$this->sqlIsOnTrack() . ', ' .
						$this->sqlProgress() . ', ' .
						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' okr_objective_id = ' . $this->sqlOkrObjectiveId(). ',' ; } elseif( true == array_key_exists( 'OkrObjectiveId', $this->getChangedColumns() ) ) { $strSql .= ' okr_objective_id = ' . $this->sqlOkrObjectiveId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' okr_key_result_status_type_id = ' . $this->sqlOkrKeyResultStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'OkrKeyResultStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' okr_key_result_status_type_id = ' . $this->sqlOkrKeyResultStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' owner_employee_id = ' . $this->sqlOwnerEmployeeId(). ',' ; } elseif( true == array_key_exists( 'OwnerEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' owner_employee_id = ' . $this->sqlOwnerEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' delivery_date = ' . $this->sqlDeliveryDate(). ',' ; } elseif( true == array_key_exists( 'DeliveryDate', $this->getChangedColumns() ) ) { $strSql .= ' delivery_date = ' . $this->sqlDeliveryDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_on_track = ' . $this->sqlIsOnTrack(). ',' ; } elseif( true == array_key_exists( 'IsOnTrack', $this->getChangedColumns() ) ) { $strSql .= ' is_on_track = ' . $this->sqlIsOnTrack() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' progress = ' . $this->sqlProgress(). ',' ; } elseif( true == array_key_exists( 'Progress', $this->getChangedColumns() ) ) { $strSql .= ' progress = ' . $this->sqlProgress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum(). ',' ; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'okr_objective_id' => $this->getOkrObjectiveId(),
			'okr_key_result_status_type_id' => $this->getOkrKeyResultStatusTypeId(),
			'owner_employee_id' => $this->getOwnerEmployeeId(),
			'description' => $this->getDescription(),
			'delivery_date' => $this->getDeliveryDate(),
			'is_on_track' => $this->getIsOnTrack(),
			'progress' => $this->getProgress(),
			'order_num' => $this->getOrderNum(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>
