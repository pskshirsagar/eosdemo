<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestQuestionGroups
 * Do not add any new functions to this class.
 */

class CBaseTestQuestionGroups extends CEosPluralBase {

	/**
	 * @return CTestQuestionGroup[]
	 */
	public static function fetchTestQuestionGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTestQuestionGroup', $objDatabase );
	}

	/**
	 * @return CTestQuestionGroup
	 */
	public static function fetchTestQuestionGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTestQuestionGroup', $objDatabase );
	}

	public static function fetchTestQuestionGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'test_question_groups', $objDatabase );
	}

	public static function fetchTestQuestionGroupById( $intId, $objDatabase ) {
		return self::fetchTestQuestionGroup( sprintf( 'SELECT * FROM test_question_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTestQuestionGroupsByTestTypeId( $intTestTypeId, $objDatabase ) {
		return self::fetchTestQuestionGroups( sprintf( 'SELECT * FROM test_question_groups WHERE test_type_id = %d', ( int ) $intTestTypeId ), $objDatabase );
	}

}
?>