<?php

class CBaseResourceRequisitionApproval extends CEosSingularBase {

	const TABLE_NAME = 'public.resource_requisition_approvals';

	protected $m_intId;
	protected $m_intResourceRequisitionId;
	protected $m_intResourceRequisitionStatusId;
	protected $m_intEmployeeId;
	protected $m_intIsApproved;
	protected $m_intLevel;
	protected $m_strApprovedOn;
	protected $m_strNote;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intResourceRequisitionStatusId = '1';
		$this->m_intIsApproved = '0';
		$this->m_intLevel = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['resource_requisition_id'] ) && $boolDirectSet ) $this->set( 'm_intResourceRequisitionId', trim( $arrValues['resource_requisition_id'] ) ); elseif( isset( $arrValues['resource_requisition_id'] ) ) $this->setResourceRequisitionId( $arrValues['resource_requisition_id'] );
		if( isset( $arrValues['resource_requisition_status_id'] ) && $boolDirectSet ) $this->set( 'm_intResourceRequisitionStatusId', trim( $arrValues['resource_requisition_status_id'] ) ); elseif( isset( $arrValues['resource_requisition_status_id'] ) ) $this->setResourceRequisitionStatusId( $arrValues['resource_requisition_status_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['is_approved'] ) && $boolDirectSet ) $this->set( 'm_intIsApproved', trim( $arrValues['is_approved'] ) ); elseif( isset( $arrValues['is_approved'] ) ) $this->setIsApproved( $arrValues['is_approved'] );
		if( isset( $arrValues['level'] ) && $boolDirectSet ) $this->set( 'm_intLevel', trim( $arrValues['level'] ) ); elseif( isset( $arrValues['level'] ) ) $this->setLevel( $arrValues['level'] );
		if( isset( $arrValues['approved_on'] ) && $boolDirectSet ) $this->set( 'm_strApprovedOn', trim( $arrValues['approved_on'] ) ); elseif( isset( $arrValues['approved_on'] ) ) $this->setApprovedOn( $arrValues['approved_on'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setResourceRequisitionId( $intResourceRequisitionId ) {
		$this->set( 'm_intResourceRequisitionId', CStrings::strToIntDef( $intResourceRequisitionId, NULL, false ) );
	}

	public function getResourceRequisitionId() {
		return $this->m_intResourceRequisitionId;
	}

	public function sqlResourceRequisitionId() {
		return ( true == isset( $this->m_intResourceRequisitionId ) ) ? ( string ) $this->m_intResourceRequisitionId : 'NULL';
	}

	public function setResourceRequisitionStatusId( $intResourceRequisitionStatusId ) {
		$this->set( 'm_intResourceRequisitionStatusId', CStrings::strToIntDef( $intResourceRequisitionStatusId, NULL, false ) );
	}

	public function getResourceRequisitionStatusId() {
		return $this->m_intResourceRequisitionStatusId;
	}

	public function sqlResourceRequisitionStatusId() {
		return ( true == isset( $this->m_intResourceRequisitionStatusId ) ) ? ( string ) $this->m_intResourceRequisitionStatusId : '1';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setIsApproved( $intIsApproved ) {
		$this->set( 'm_intIsApproved', CStrings::strToIntDef( $intIsApproved, NULL, false ) );
	}

	public function getIsApproved() {
		return $this->m_intIsApproved;
	}

	public function sqlIsApproved() {
		return ( true == isset( $this->m_intIsApproved ) ) ? ( string ) $this->m_intIsApproved : '0';
	}

	public function setLevel( $intLevel ) {
		$this->set( 'm_intLevel', CStrings::strToIntDef( $intLevel, NULL, false ) );
	}

	public function getLevel() {
		return $this->m_intLevel;
	}

	public function sqlLevel() {
		return ( true == isset( $this->m_intLevel ) ) ? ( string ) $this->m_intLevel : '0';
	}

	public function setApprovedOn( $strApprovedOn ) {
		$this->set( 'm_strApprovedOn', CStrings::strTrimDef( $strApprovedOn, -1, NULL, true ) );
	}

	public function getApprovedOn() {
		return $this->m_strApprovedOn;
	}

	public function sqlApprovedOn() {
		return ( true == isset( $this->m_strApprovedOn ) ) ? '\'' . $this->m_strApprovedOn . '\'' : 'NULL';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, 300, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, resource_requisition_id, resource_requisition_status_id, employee_id, is_approved, level, approved_on, note, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlResourceRequisitionId() . ', ' .
 						$this->sqlResourceRequisitionStatusId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlIsApproved() . ', ' .
 						$this->sqlLevel() . ', ' .
 						$this->sqlApprovedOn() . ', ' .
 						$this->sqlNote() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resource_requisition_id = ' . $this->sqlResourceRequisitionId() . ','; } elseif( true == array_key_exists( 'ResourceRequisitionId', $this->getChangedColumns() ) ) { $strSql .= ' resource_requisition_id = ' . $this->sqlResourceRequisitionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resource_requisition_status_id = ' . $this->sqlResourceRequisitionStatusId() . ','; } elseif( true == array_key_exists( 'ResourceRequisitionStatusId', $this->getChangedColumns() ) ) { $strSql .= ' resource_requisition_status_id = ' . $this->sqlResourceRequisitionStatusId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; } elseif( true == array_key_exists( 'IsApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_approved = ' . $this->sqlIsApproved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' level = ' . $this->sqlLevel() . ','; } elseif( true == array_key_exists( 'Level', $this->getChangedColumns() ) ) { $strSql .= ' level = ' . $this->sqlLevel() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; } elseif( true == array_key_exists( 'ApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote() . ','; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'resource_requisition_id' => $this->getResourceRequisitionId(),
			'resource_requisition_status_id' => $this->getResourceRequisitionStatusId(),
			'employee_id' => $this->getEmployeeId(),
			'is_approved' => $this->getIsApproved(),
			'level' => $this->getLevel(),
			'approved_on' => $this->getApprovedOn(),
			'note' => $this->getNote(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>