<?php

class CBaseOrderFormRequest extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.order_form_requests';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intOrderFormTypeId;
	protected $m_intOrderFormStatusTypeId;
	protected $m_intInvoiceId;
	protected $m_intRevisionCount;
	protected $m_strSignedName;
	protected $m_strElectronicSignIpAddress;
	protected $m_strFeedback;
	protected $m_jsonFeedback;
	protected $m_strNotes;
	protected $m_strSignedDate;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_intAcceptedBy;
	protected $m_intSubmittedBy;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intAssignedEmployeeId;
	protected $m_intReopenedBy;
	protected $m_strReopenedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['order_form_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOrderFormTypeId', trim( $arrValues['order_form_type_id'] ) ); elseif( isset( $arrValues['order_form_type_id'] ) ) $this->setOrderFormTypeId( $arrValues['order_form_type_id'] );
		if( isset( $arrValues['order_form_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOrderFormStatusTypeId', trim( $arrValues['order_form_status_type_id'] ) ); elseif( isset( $arrValues['order_form_status_type_id'] ) ) $this->setOrderFormStatusTypeId( $arrValues['order_form_status_type_id'] );
		if( isset( $arrValues['invoice_id'] ) && $boolDirectSet ) $this->set( 'm_intInvoiceId', trim( $arrValues['invoice_id'] ) ); elseif( isset( $arrValues['invoice_id'] ) ) $this->setInvoiceId( $arrValues['invoice_id'] );
		if( isset( $arrValues['revision_count'] ) && $boolDirectSet ) $this->set( 'm_intRevisionCount', trim( $arrValues['revision_count'] ) ); elseif( isset( $arrValues['revision_count'] ) ) $this->setRevisionCount( $arrValues['revision_count'] );
		if( isset( $arrValues['signed_name'] ) && $boolDirectSet ) $this->set( 'm_strSignedName', trim( $arrValues['signed_name'] ) ); elseif( isset( $arrValues['signed_name'] ) ) $this->setSignedName( $arrValues['signed_name'] );
		if( isset( $arrValues['electronic_sign_ip_address'] ) && $boolDirectSet ) $this->set( 'm_strElectronicSignIpAddress', trim( $arrValues['electronic_sign_ip_address'] ) ); elseif( isset( $arrValues['electronic_sign_ip_address'] ) ) $this->setElectronicSignIpAddress( $arrValues['electronic_sign_ip_address'] );
		if( isset( $arrValues['feedback'] ) ) $this->set( 'm_strFeedback', trim( $arrValues['feedback'] ) );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['signed_date'] ) && $boolDirectSet ) $this->set( 'm_strSignedDate', trim( $arrValues['signed_date'] ) ); elseif( isset( $arrValues['signed_date'] ) ) $this->setSignedDate( $arrValues['signed_date'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['accepted_by'] ) && $boolDirectSet ) $this->set( 'm_intAcceptedBy', trim( $arrValues['accepted_by'] ) ); elseif( isset( $arrValues['accepted_by'] ) ) $this->setAcceptedBy( $arrValues['accepted_by'] );
		if( isset( $arrValues['submitted_by'] ) && $boolDirectSet ) $this->set( 'm_intSubmittedBy', trim( $arrValues['submitted_by'] ) ); elseif( isset( $arrValues['submitted_by'] ) ) $this->setSubmittedBy( $arrValues['submitted_by'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['assigned_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intAssignedEmployeeId', trim( $arrValues['assigned_employee_id'] ) ); elseif( isset( $arrValues['assigned_employee_id'] ) ) $this->setAssignedEmployeeId( $arrValues['assigned_employee_id'] );
		if( isset( $arrValues['reopened_by'] ) && $boolDirectSet ) $this->set( 'm_intReopenedBy', trim( $arrValues['reopened_by'] ) ); elseif( isset( $arrValues['reopened_by'] ) ) $this->setReopenedBy( $arrValues['reopened_by'] );
		if( isset( $arrValues['reopened_on'] ) && $boolDirectSet ) $this->set( 'm_strReopenedOn', trim( $arrValues['reopened_on'] ) ); elseif( isset( $arrValues['reopened_on'] ) ) $this->setReopenedOn( $arrValues['reopened_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setOrderFormTypeId( $intOrderFormTypeId ) {
		$this->set( 'm_intOrderFormTypeId', CStrings::strToIntDef( $intOrderFormTypeId, NULL, false ) );
	}

	public function getOrderFormTypeId() {
		return $this->m_intOrderFormTypeId;
	}

	public function sqlOrderFormTypeId() {
		return ( true == isset( $this->m_intOrderFormTypeId ) ) ? ( string ) $this->m_intOrderFormTypeId : 'NULL';
	}

	public function setOrderFormStatusTypeId( $intOrderFormStatusTypeId ) {
		$this->set( 'm_intOrderFormStatusTypeId', CStrings::strToIntDef( $intOrderFormStatusTypeId, NULL, false ) );
	}

	public function getOrderFormStatusTypeId() {
		return $this->m_intOrderFormStatusTypeId;
	}

	public function sqlOrderFormStatusTypeId() {
		return ( true == isset( $this->m_intOrderFormStatusTypeId ) ) ? ( string ) $this->m_intOrderFormStatusTypeId : 'NULL';
	}

	public function setInvoiceId( $intInvoiceId ) {
		$this->set( 'm_intInvoiceId', CStrings::strToIntDef( $intInvoiceId, NULL, false ) );
	}

	public function getInvoiceId() {
		return $this->m_intInvoiceId;
	}

	public function sqlInvoiceId() {
		return ( true == isset( $this->m_intInvoiceId ) ) ? ( string ) $this->m_intInvoiceId : 'NULL';
	}

	public function setRevisionCount( $intRevisionCount ) {
		$this->set( 'm_intRevisionCount', CStrings::strToIntDef( $intRevisionCount, NULL, false ) );
	}

	public function getRevisionCount() {
		return $this->m_intRevisionCount;
	}

	public function sqlRevisionCount() {
		return ( true == isset( $this->m_intRevisionCount ) ) ? ( string ) $this->m_intRevisionCount : 'NULL';
	}

	public function setSignedName( $strSignedName ) {
		$this->set( 'm_strSignedName', CStrings::strTrimDef( $strSignedName, 50, NULL, true ) );
	}

	public function getSignedName() {
		return $this->m_strSignedName;
	}

	public function sqlSignedName() {
		return ( true == isset( $this->m_strSignedName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSignedName ) : '\'' . addslashes( $this->m_strSignedName ) . '\'' ) : 'NULL';
	}

	public function setElectronicSignIpAddress( $strElectronicSignIpAddress ) {
		$this->set( 'm_strElectronicSignIpAddress', CStrings::strTrimDef( $strElectronicSignIpAddress, 25, NULL, true ) );
	}

	public function getElectronicSignIpAddress() {
		return $this->m_strElectronicSignIpAddress;
	}

	public function sqlElectronicSignIpAddress() {
		return ( true == isset( $this->m_strElectronicSignIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strElectronicSignIpAddress ) : '\'' . addslashes( $this->m_strElectronicSignIpAddress ) . '\'' ) : 'NULL';
	}

	public function setFeedback( $jsonFeedback ) {
		if( true == valObj( $jsonFeedback, 'stdClass' ) ) {
			$this->set( 'm_jsonFeedback', $jsonFeedback );
		} elseif( true == valJsonString( $jsonFeedback ) ) {
			$this->set( 'm_jsonFeedback', CStrings::strToJson( $jsonFeedback ) );
		} else {
			$this->set( 'm_jsonFeedback', NULL ); 
		}
		unset( $this->m_strFeedback );
	}

	public function getFeedback() {
		if( true == isset( $this->m_strFeedback ) ) {
			$this->m_jsonFeedback = CStrings::strToJson( $this->m_strFeedback );
			unset( $this->m_strFeedback );
		}
		return $this->m_jsonFeedback;
	}

	public function sqlFeedback() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getFeedback() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getFeedback() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getFeedback() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setSignedDate( $strSignedDate ) {
		$this->set( 'm_strSignedDate', CStrings::strTrimDef( $strSignedDate, -1, NULL, true ) );
	}

	public function getSignedDate() {
		return $this->m_strSignedDate;
	}

	public function sqlSignedDate() {
		return ( true == isset( $this->m_strSignedDate ) ) ? '\'' . $this->m_strSignedDate . '\'' : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NULL';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NULL';
	}

	public function setAcceptedBy( $intAcceptedBy ) {
		$this->set( 'm_intAcceptedBy', CStrings::strToIntDef( $intAcceptedBy, NULL, false ) );
	}

	public function getAcceptedBy() {
		return $this->m_intAcceptedBy;
	}

	public function sqlAcceptedBy() {
		return ( true == isset( $this->m_intAcceptedBy ) ) ? ( string ) $this->m_intAcceptedBy : 'NULL';
	}

	public function setSubmittedBy( $intSubmittedBy ) {
		$this->set( 'm_intSubmittedBy', CStrings::strToIntDef( $intSubmittedBy, NULL, false ) );
	}

	public function getSubmittedBy() {
		return $this->m_intSubmittedBy;
	}

	public function sqlSubmittedBy() {
		return ( true == isset( $this->m_intSubmittedBy ) ) ? ( string ) $this->m_intSubmittedBy : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setAssignedEmployeeId( $intAssignedEmployeeId ) {
		$this->set( 'm_intAssignedEmployeeId', CStrings::strToIntDef( $intAssignedEmployeeId, NULL, false ) );
	}

	public function getAssignedEmployeeId() {
		return $this->m_intAssignedEmployeeId;
	}

	public function sqlAssignedEmployeeId() {
		return ( true == isset( $this->m_intAssignedEmployeeId ) ) ? ( string ) $this->m_intAssignedEmployeeId : 'NULL';
	}

	public function setReopenedBy( $intReopenedBy ) {
		$this->set( 'm_intReopenedBy', CStrings::strToIntDef( $intReopenedBy, NULL, false ) );
	}

	public function getReopenedBy() {
		return $this->m_intReopenedBy;
	}

	public function sqlReopenedBy() {
		return ( true == isset( $this->m_intReopenedBy ) ) ? ( string ) $this->m_intReopenedBy : 'NULL';
	}

	public function setReopenedOn( $strReopenedOn ) {
		$this->set( 'm_strReopenedOn', CStrings::strTrimDef( $strReopenedOn, -1, NULL, true ) );
	}

	public function getReopenedOn() {
		return $this->m_strReopenedOn;
	}

	public function sqlReopenedOn() {
		return ( true == isset( $this->m_strReopenedOn ) ) ? '\'' . $this->m_strReopenedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, order_form_type_id, order_form_status_type_id, invoice_id, revision_count, signed_name, electronic_sign_ip_address, feedback, notes, signed_date, start_date, end_date, accepted_by, submitted_by, updated_by, updated_on, created_by, created_on, assigned_employee_id, reopened_by, reopened_on, details )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlOrderFormTypeId() . ', ' .
						$this->sqlOrderFormStatusTypeId() . ', ' .
						$this->sqlInvoiceId() . ', ' .
						$this->sqlRevisionCount() . ', ' .
						$this->sqlSignedName() . ', ' .
						$this->sqlElectronicSignIpAddress() . ', ' .
						$this->sqlFeedback() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlSignedDate() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlAcceptedBy() . ', ' .
						$this->sqlSubmittedBy() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlAssignedEmployeeId() . ', ' .
						$this->sqlReopenedBy() . ', ' .
						$this->sqlReopenedOn() . ', ' .
						$this->sqlDetails() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_form_type_id = ' . $this->sqlOrderFormTypeId(). ',' ; } elseif( true == array_key_exists( 'OrderFormTypeId', $this->getChangedColumns() ) ) { $strSql .= ' order_form_type_id = ' . $this->sqlOrderFormTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_form_status_type_id = ' . $this->sqlOrderFormStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'OrderFormStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' order_form_status_type_id = ' . $this->sqlOrderFormStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' invoice_id = ' . $this->sqlInvoiceId(). ',' ; } elseif( true == array_key_exists( 'InvoiceId', $this->getChangedColumns() ) ) { $strSql .= ' invoice_id = ' . $this->sqlInvoiceId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' revision_count = ' . $this->sqlRevisionCount(). ',' ; } elseif( true == array_key_exists( 'RevisionCount', $this->getChangedColumns() ) ) { $strSql .= ' revision_count = ' . $this->sqlRevisionCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signed_name = ' . $this->sqlSignedName(). ',' ; } elseif( true == array_key_exists( 'SignedName', $this->getChangedColumns() ) ) { $strSql .= ' signed_name = ' . $this->sqlSignedName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' electronic_sign_ip_address = ' . $this->sqlElectronicSignIpAddress(). ',' ; } elseif( true == array_key_exists( 'ElectronicSignIpAddress', $this->getChangedColumns() ) ) { $strSql .= ' electronic_sign_ip_address = ' . $this->sqlElectronicSignIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' feedback = ' . $this->sqlFeedback(). ',' ; } elseif( true == array_key_exists( 'Feedback', $this->getChangedColumns() ) ) { $strSql .= ' feedback = ' . $this->sqlFeedback() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signed_date = ' . $this->sqlSignedDate(). ',' ; } elseif( true == array_key_exists( 'SignedDate', $this->getChangedColumns() ) ) { $strSql .= ' signed_date = ' . $this->sqlSignedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy(). ',' ; } elseif( true == array_key_exists( 'AcceptedBy', $this->getChangedColumns() ) ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' submitted_by = ' . $this->sqlSubmittedBy(). ',' ; } elseif( true == array_key_exists( 'SubmittedBy', $this->getChangedColumns() ) ) { $strSql .= ' submitted_by = ' . $this->sqlSubmittedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' assigned_employee_id = ' . $this->sqlAssignedEmployeeId(). ',' ; } elseif( true == array_key_exists( 'AssignedEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' assigned_employee_id = ' . $this->sqlAssignedEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reopened_by = ' . $this->sqlReopenedBy(). ',' ; } elseif( true == array_key_exists( 'ReopenedBy', $this->getChangedColumns() ) ) { $strSql .= ' reopened_by = ' . $this->sqlReopenedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reopened_on = ' . $this->sqlReopenedOn(). ',' ; } elseif( true == array_key_exists( 'ReopenedOn', $this->getChangedColumns() ) ) { $strSql .= ' reopened_on = ' . $this->sqlReopenedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'order_form_type_id' => $this->getOrderFormTypeId(),
			'order_form_status_type_id' => $this->getOrderFormStatusTypeId(),
			'invoice_id' => $this->getInvoiceId(),
			'revision_count' => $this->getRevisionCount(),
			'signed_name' => $this->getSignedName(),
			'electronic_sign_ip_address' => $this->getElectronicSignIpAddress(),
			'feedback' => $this->getFeedback(),
			'notes' => $this->getNotes(),
			'signed_date' => $this->getSignedDate(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'accepted_by' => $this->getAcceptedBy(),
			'submitted_by' => $this->getSubmittedBy(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'assigned_employee_id' => $this->getAssignedEmployeeId(),
			'reopened_by' => $this->getReopenedBy(),
			'reopened_on' => $this->getReopenedOn(),
			'details' => $this->getDetails()
		);
	}

}
?>
