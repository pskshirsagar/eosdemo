<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsProductCompetitors
 * Do not add any new functions to this class.
 */

class CBasePsProductCompetitors extends CEosPluralBase {

	/**
	 * @return CPsProductCompetitor[]
	 */
	public static function fetchPsProductCompetitors( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPsProductCompetitor', $objDatabase );
	}

	/**
	 * @return CPsProductCompetitor
	 */
	public static function fetchPsProductCompetitor( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPsProductCompetitor', $objDatabase );
	}

	public static function fetchPsProductCompetitorCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'ps_product_competitors', $objDatabase );
	}

	public static function fetchPsProductCompetitorById( $intId, $objDatabase ) {
		return self::fetchPsProductCompetitor( sprintf( 'SELECT * FROM ps_product_competitors WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPsProductCompetitorsByPsProductId( $intPsProductId, $objDatabase ) {
		return self::fetchPsProductCompetitors( sprintf( 'SELECT * FROM ps_product_competitors WHERE ps_product_id = %d', ( int ) $intPsProductId ), $objDatabase );
	}

	public static function fetchPsProductCompetitorsByCompetitorId( $intCompetitorId, $objDatabase ) {
		return self::fetchPsProductCompetitors( sprintf( 'SELECT * FROM ps_product_competitors WHERE competitor_id = %d', ( int ) $intCompetitorId ), $objDatabase );
	}

}
?>