<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplateRuleSets
 * Do not add any new functions to this class.
 */

class CBaseSurveyTemplateRuleSets extends CEosPluralBase {

	/**
	 * @return CSurveyTemplateRuleSet[]
	 */
	public static function fetchSurveyTemplateRuleSets( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSurveyTemplateRuleSet', $objDatabase );
	}

	/**
	 * @return CSurveyTemplateRuleSet
	 */
	public static function fetchSurveyTemplateRuleSet( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSurveyTemplateRuleSet', $objDatabase );
	}

	public static function fetchSurveyTemplateRuleSetCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'survey_template_rule_sets', $objDatabase );
	}

	public static function fetchSurveyTemplateRuleSetById( $intId, $objDatabase ) {
		return self::fetchSurveyTemplateRuleSet( sprintf( 'SELECT * FROM survey_template_rule_sets WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>