<?php

class CBasePlannerTask extends CEosSingularBase {

	const TABLE_NAME = 'public.planner_tasks';

	protected $m_intId;
	protected $m_intPlannerId;
	protected $m_intTaskId;
	protected $m_boolCurrentTask;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strSprintTaskStatus;

	public function __construct() {
		parent::__construct();

		$this->m_boolCurrentTask = false;
		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['planner_id'] ) && $boolDirectSet ) $this->set( 'm_intPlannerId', trim( $arrValues['planner_id'] ) ); elseif( isset( $arrValues['planner_id'] ) ) $this->setPlannerId( $arrValues['planner_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['current_task'] ) && $boolDirectSet ) $this->set( 'm_boolCurrentTask', trim( stripcslashes( $arrValues['current_task'] ) ) ); elseif( isset( $arrValues['current_task'] ) ) $this->setCurrentTask( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['current_task'] ) : $arrValues['current_task'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['sprint_task_status'] ) && $boolDirectSet ) $this->set( 'm_strSprintTaskStatus', trim( stripcslashes( $arrValues['sprint_task_status'] ) ) ); elseif( isset( $arrValues['sprint_task_status'] ) ) $this->setSprintTaskStatus( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sprint_task_status'] ) : $arrValues['sprint_task_status'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPlannerId( $intPlannerId ) {
		$this->set( 'm_intPlannerId', CStrings::strToIntDef( $intPlannerId, NULL, false ) );
	}

	public function getPlannerId() {
		return $this->m_intPlannerId;
	}

	public function sqlPlannerId() {
		return ( true == isset( $this->m_intPlannerId ) ) ? ( string ) $this->m_intPlannerId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setCurrentTask( $boolCurrentTask ) {
		$this->set( 'm_boolCurrentTask', CStrings::strToBool( $boolCurrentTask ) );
	}

	public function getCurrentTask() {
		return $this->m_boolCurrentTask;
	}

	public function sqlCurrentTask() {
		return ( true == isset( $this->m_boolCurrentTask ) ) ? '\'' . ( true == ( bool ) $this->m_boolCurrentTask ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setSprintTaskStatus( $strSprintTaskStatus ) {
		$this->set( 'm_strSprintTaskStatus', CStrings::strTrimDef( $strSprintTaskStatus, -1, NULL, true ) );
	}

	public function getSprintTaskStatus() {
		return $this->m_strSprintTaskStatus;
	}

	public function sqlSprintTaskStatus() {
		return ( true == isset( $this->m_strSprintTaskStatus ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSprintTaskStatus ) : '\'' . addslashes( $this->m_strSprintTaskStatus ) . '\'' ) : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, planner_id, task_id, current_task, updated_by, updated_on, created_by, created_on, sprint_task_status )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlPlannerId() . ', ' .
						$this->sqlTaskId() . ', ' .
						$this->sqlCurrentTask() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlSprintTaskStatus() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' planner_id = ' . $this->sqlPlannerId(). ',' ; } elseif( true == array_key_exists( 'PlannerId', $this->getChangedColumns() ) ) { $strSql .= ' planner_id = ' . $this->sqlPlannerId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId(). ',' ; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_task = ' . $this->sqlCurrentTask(). ',' ; } elseif( true == array_key_exists( 'CurrentTask', $this->getChangedColumns() ) ) { $strSql .= ' current_task = ' . $this->sqlCurrentTask() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sprint_task_status = ' . $this->sqlSprintTaskStatus(). ',' ; } elseif( true == array_key_exists( 'SprintTaskStatus', $this->getChangedColumns() ) ) { $strSql .= ' sprint_task_status = ' . $this->sqlSprintTaskStatus() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'planner_id' => $this->getPlannerId(),
			'task_id' => $this->getTaskId(),
			'current_task' => $this->getCurrentTask(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'sprint_task_status' => $this->getSprintTaskStatus()
		);
	}

}
?>