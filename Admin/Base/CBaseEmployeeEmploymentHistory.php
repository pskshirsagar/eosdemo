<?php

class CBaseEmployeeEmploymentHistory extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_employment_histories';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_strDateStarted;
	protected $m_strDateConfirmed;
	protected $m_strResignedOn;
	protected $m_strDateTerminated;
	protected $m_strCountryCode;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['date_started'] ) && $boolDirectSet ) $this->set( 'm_strDateStarted', trim( $arrValues['date_started'] ) ); elseif( isset( $arrValues['date_started'] ) ) $this->setDateStarted( $arrValues['date_started'] );
		if( isset( $arrValues['date_confirmed'] ) && $boolDirectSet ) $this->set( 'm_strDateConfirmed', trim( $arrValues['date_confirmed'] ) ); elseif( isset( $arrValues['date_confirmed'] ) ) $this->setDateConfirmed( $arrValues['date_confirmed'] );
		if( isset( $arrValues['resigned_on'] ) && $boolDirectSet ) $this->set( 'm_strResignedOn', trim( $arrValues['resigned_on'] ) ); elseif( isset( $arrValues['resigned_on'] ) ) $this->setResignedOn( $arrValues['resigned_on'] );
		if( isset( $arrValues['date_terminated'] ) && $boolDirectSet ) $this->set( 'm_strDateTerminated', trim( $arrValues['date_terminated'] ) ); elseif( isset( $arrValues['date_terminated'] ) ) $this->setDateTerminated( $arrValues['date_terminated'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setDateStarted( $strDateStarted ) {
		$this->set( 'm_strDateStarted', CStrings::strTrimDef( $strDateStarted, -1, NULL, true ) );
	}

	public function getDateStarted() {
		return $this->m_strDateStarted;
	}

	public function sqlDateStarted() {
		return ( true == isset( $this->m_strDateStarted ) ) ? '\'' . $this->m_strDateStarted . '\'' : 'NOW()';
	}

	public function setDateConfirmed( $strDateConfirmed ) {
		$this->set( 'm_strDateConfirmed', CStrings::strTrimDef( $strDateConfirmed, -1, NULL, true ) );
	}

	public function getDateConfirmed() {
		return $this->m_strDateConfirmed;
	}

	public function sqlDateConfirmed() {
		return ( true == isset( $this->m_strDateConfirmed ) ) ? '\'' . $this->m_strDateConfirmed . '\'' : 'NULL';
	}

	public function setResignedOn( $strResignedOn ) {
		$this->set( 'm_strResignedOn', CStrings::strTrimDef( $strResignedOn, -1, NULL, true ) );
	}

	public function getResignedOn() {
		return $this->m_strResignedOn;
	}

	public function sqlResignedOn() {
		return ( true == isset( $this->m_strResignedOn ) ) ? '\'' . $this->m_strResignedOn . '\'' : 'NULL';
	}

	public function setDateTerminated( $strDateTerminated ) {
		$this->set( 'm_strDateTerminated', CStrings::strTrimDef( $strDateTerminated, -1, NULL, true ) );
	}

	public function getDateTerminated() {
		return $this->m_strDateTerminated;
	}

	public function sqlDateTerminated() {
		return ( true == isset( $this->m_strDateTerminated ) ) ? '\'' . $this->m_strDateTerminated . '\'' : 'NOW()';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, date_started, date_confirmed, resigned_on, date_terminated, country_code, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlDateStarted() . ', ' .
 						$this->sqlDateConfirmed() . ', ' .
 						$this->sqlResignedOn() . ', ' .
 						$this->sqlDateTerminated() . ', ' .
 						$this->sqlCountryCode() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; } elseif( true == array_key_exists( 'DateStarted', $this->getChangedColumns() ) ) { $strSql .= ' date_started = ' . $this->sqlDateStarted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_confirmed = ' . $this->sqlDateConfirmed() . ','; } elseif( true == array_key_exists( 'DateConfirmed', $this->getChangedColumns() ) ) { $strSql .= ' date_confirmed = ' . $this->sqlDateConfirmed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' resigned_on = ' . $this->sqlResignedOn() . ','; } elseif( true == array_key_exists( 'ResignedOn', $this->getChangedColumns() ) ) { $strSql .= ' resigned_on = ' . $this->sqlResignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; } elseif( true == array_key_exists( 'DateTerminated', $this->getChangedColumns() ) ) { $strSql .= ' date_terminated = ' . $this->sqlDateTerminated() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'date_started' => $this->getDateStarted(),
			'date_confirmed' => $this->getDateConfirmed(),
			'resigned_on' => $this->getResignedOn(),
			'date_terminated' => $this->getDateTerminated(),
			'country_code' => $this->getCountryCode(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>