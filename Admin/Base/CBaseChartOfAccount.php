<?php

class CBaseChartOfAccount extends CEosSingularBase {

	const TABLE_NAME = 'public.chart_of_accounts';

	protected $m_intId;
	protected $m_intChartOfAccountTypeId;
	protected $m_intChartOfAccountExtraTypeId;
	protected $m_intAccountingSystemTypeId;
	protected $m_intProcessingBankAccountId;
	protected $m_strAccountName;
	protected $m_strAccountNumber;
	protected $m_strSecondaryNumber;
	protected $m_strSecondaryProductName;
	protected $m_strDescription;
	protected $m_strClass;
	protected $m_strOpeningBalanceDate;
	protected $m_fltOpeningBalanceAmount;
	protected $m_strBankAccountNumberEncrypted;
	protected $m_strBankAccountNumberMasked;
	protected $m_strBankRoutingNumberEncrypted;
	protected $m_strBankRoutingNumberMasked;
	protected $m_intIsDisabled;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intAccountingSystemTypeId = '1';
		$this->m_intIsDisabled = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['chart_of_account_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChartOfAccountTypeId', trim( $arrValues['chart_of_account_type_id'] ) ); elseif( isset( $arrValues['chart_of_account_type_id'] ) ) $this->setChartOfAccountTypeId( $arrValues['chart_of_account_type_id'] );
		if( isset( $arrValues['chart_of_account_extra_type_id'] ) && $boolDirectSet ) $this->set( 'm_intChartOfAccountExtraTypeId', trim( $arrValues['chart_of_account_extra_type_id'] ) ); elseif( isset( $arrValues['chart_of_account_extra_type_id'] ) ) $this->setChartOfAccountExtraTypeId( $arrValues['chart_of_account_extra_type_id'] );
		if( isset( $arrValues['accounting_system_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountingSystemTypeId', trim( $arrValues['accounting_system_type_id'] ) ); elseif( isset( $arrValues['accounting_system_type_id'] ) ) $this->setAccountingSystemTypeId( $arrValues['accounting_system_type_id'] );
		if( isset( $arrValues['processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intProcessingBankAccountId', trim( $arrValues['processing_bank_account_id'] ) ); elseif( isset( $arrValues['processing_bank_account_id'] ) ) $this->setProcessingBankAccountId( $arrValues['processing_bank_account_id'] );
		if( isset( $arrValues['account_name'] ) && $boolDirectSet ) $this->set( 'm_strAccountName', trim( stripcslashes( $arrValues['account_name'] ) ) ); elseif( isset( $arrValues['account_name'] ) ) $this->setAccountName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_name'] ) : $arrValues['account_name'] );
		if( isset( $arrValues['account_number'] ) && $boolDirectSet ) $this->set( 'm_strAccountNumber', trim( stripcslashes( $arrValues['account_number'] ) ) ); elseif( isset( $arrValues['account_number'] ) ) $this->setAccountNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['account_number'] ) : $arrValues['account_number'] );
		if( isset( $arrValues['secondary_number'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryNumber', trim( stripcslashes( $arrValues['secondary_number'] ) ) ); elseif( isset( $arrValues['secondary_number'] ) ) $this->setSecondaryNumber( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['secondary_number'] ) : $arrValues['secondary_number'] );
		if( isset( $arrValues['secondary_product_name'] ) && $boolDirectSet ) $this->set( 'm_strSecondaryProductName', trim( stripcslashes( $arrValues['secondary_product_name'] ) ) ); elseif( isset( $arrValues['secondary_product_name'] ) ) $this->setSecondaryProductName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['secondary_product_name'] ) : $arrValues['secondary_product_name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['class'] ) && $boolDirectSet ) $this->set( 'm_strClass', trim( stripcslashes( $arrValues['class'] ) ) ); elseif( isset( $arrValues['class'] ) ) $this->setClass( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['class'] ) : $arrValues['class'] );
		if( isset( $arrValues['opening_balance_date'] ) && $boolDirectSet ) $this->set( 'm_strOpeningBalanceDate', trim( $arrValues['opening_balance_date'] ) ); elseif( isset( $arrValues['opening_balance_date'] ) ) $this->setOpeningBalanceDate( $arrValues['opening_balance_date'] );
		if( isset( $arrValues['opening_balance_amount'] ) && $boolDirectSet ) $this->set( 'm_fltOpeningBalanceAmount', trim( $arrValues['opening_balance_amount'] ) ); elseif( isset( $arrValues['opening_balance_amount'] ) ) $this->setOpeningBalanceAmount( $arrValues['opening_balance_amount'] );
		if( isset( $arrValues['bank_account_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBankAccountNumberEncrypted', trim( stripcslashes( $arrValues['bank_account_number_encrypted'] ) ) ); elseif( isset( $arrValues['bank_account_number_encrypted'] ) ) $this->setBankAccountNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_account_number_encrypted'] ) : $arrValues['bank_account_number_encrypted'] );
		if( isset( $arrValues['bank_account_number_masked'] ) && $boolDirectSet ) $this->set( 'm_strBankAccountNumberMasked', trim( stripcslashes( $arrValues['bank_account_number_masked'] ) ) ); elseif( isset( $arrValues['bank_account_number_masked'] ) ) $this->setBankAccountNumberMasked( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_account_number_masked'] ) : $arrValues['bank_account_number_masked'] );
		if( isset( $arrValues['bank_routing_number_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strBankRoutingNumberEncrypted', trim( stripcslashes( $arrValues['bank_routing_number_encrypted'] ) ) ); elseif( isset( $arrValues['bank_routing_number_encrypted'] ) ) $this->setBankRoutingNumberEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_routing_number_encrypted'] ) : $arrValues['bank_routing_number_encrypted'] );
		if( isset( $arrValues['bank_routing_number_masked'] ) && $boolDirectSet ) $this->set( 'm_strBankRoutingNumberMasked', trim( stripcslashes( $arrValues['bank_routing_number_masked'] ) ) ); elseif( isset( $arrValues['bank_routing_number_masked'] ) ) $this->setBankRoutingNumberMasked( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bank_routing_number_masked'] ) : $arrValues['bank_routing_number_masked'] );
		if( isset( $arrValues['is_disabled'] ) && $boolDirectSet ) $this->set( 'm_intIsDisabled', trim( $arrValues['is_disabled'] ) ); elseif( isset( $arrValues['is_disabled'] ) ) $this->setIsDisabled( $arrValues['is_disabled'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setChartOfAccountTypeId( $intChartOfAccountTypeId ) {
		$this->set( 'm_intChartOfAccountTypeId', CStrings::strToIntDef( $intChartOfAccountTypeId, NULL, false ) );
	}

	public function getChartOfAccountTypeId() {
		return $this->m_intChartOfAccountTypeId;
	}

	public function sqlChartOfAccountTypeId() {
		return ( true == isset( $this->m_intChartOfAccountTypeId ) ) ? ( string ) $this->m_intChartOfAccountTypeId : 'NULL';
	}

	public function setChartOfAccountExtraTypeId( $intChartOfAccountExtraTypeId ) {
		$this->set( 'm_intChartOfAccountExtraTypeId', CStrings::strToIntDef( $intChartOfAccountExtraTypeId, NULL, false ) );
	}

	public function getChartOfAccountExtraTypeId() {
		return $this->m_intChartOfAccountExtraTypeId;
	}

	public function sqlChartOfAccountExtraTypeId() {
		return ( true == isset( $this->m_intChartOfAccountExtraTypeId ) ) ? ( string ) $this->m_intChartOfAccountExtraTypeId : 'NULL';
	}

	public function setAccountingSystemTypeId( $intAccountingSystemTypeId ) {
		$this->set( 'm_intAccountingSystemTypeId', CStrings::strToIntDef( $intAccountingSystemTypeId, NULL, false ) );
	}

	public function getAccountingSystemTypeId() {
		return $this->m_intAccountingSystemTypeId;
	}

	public function sqlAccountingSystemTypeId() {
		return ( true == isset( $this->m_intAccountingSystemTypeId ) ) ? ( string ) $this->m_intAccountingSystemTypeId : '1';
	}

	public function setProcessingBankAccountId( $intProcessingBankAccountId ) {
		$this->set( 'm_intProcessingBankAccountId', CStrings::strToIntDef( $intProcessingBankAccountId, NULL, false ) );
	}

	public function getProcessingBankAccountId() {
		return $this->m_intProcessingBankAccountId;
	}

	public function sqlProcessingBankAccountId() {
		return ( true == isset( $this->m_intProcessingBankAccountId ) ) ? ( string ) $this->m_intProcessingBankAccountId : 'NULL';
	}

	public function setAccountName( $strAccountName ) {
		$this->set( 'm_strAccountName', CStrings::strTrimDef( $strAccountName, 240, NULL, true ) );
	}

	public function getAccountName() {
		return $this->m_strAccountName;
	}

	public function sqlAccountName() {
		return ( true == isset( $this->m_strAccountName ) ) ? '\'' . addslashes( $this->m_strAccountName ) . '\'' : 'NULL';
	}

	public function setAccountNumber( $strAccountNumber ) {
		$this->set( 'm_strAccountNumber', CStrings::strTrimDef( $strAccountNumber, 50, NULL, true ) );
	}

	public function getAccountNumber() {
		return $this->m_strAccountNumber;
	}

	public function sqlAccountNumber() {
		return ( true == isset( $this->m_strAccountNumber ) ) ? '\'' . addslashes( $this->m_strAccountNumber ) . '\'' : 'NULL';
	}

	public function setSecondaryNumber( $strSecondaryNumber ) {
		$this->set( 'm_strSecondaryNumber', CStrings::strTrimDef( $strSecondaryNumber, 50, NULL, true ) );
	}

	public function getSecondaryNumber() {
		return $this->m_strSecondaryNumber;
	}

	public function sqlSecondaryNumber() {
		return ( true == isset( $this->m_strSecondaryNumber ) ) ? '\'' . addslashes( $this->m_strSecondaryNumber ) . '\'' : 'NULL';
	}

	public function setSecondaryProductName( $strSecondaryProductName ) {
		$this->set( 'm_strSecondaryProductName', CStrings::strTrimDef( $strSecondaryProductName, 100, NULL, true ) );
	}

	public function getSecondaryProductName() {
		return $this->m_strSecondaryProductName;
	}

	public function sqlSecondaryProductName() {
		return ( true == isset( $this->m_strSecondaryProductName ) ) ? '\'' . addslashes( $this->m_strSecondaryProductName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setClass( $strClass ) {
		$this->set( 'm_strClass', CStrings::strTrimDef( $strClass, 240, NULL, true ) );
	}

	public function getClass() {
		return $this->m_strClass;
	}

	public function sqlClass() {
		return ( true == isset( $this->m_strClass ) ) ? '\'' . addslashes( $this->m_strClass ) . '\'' : 'NULL';
	}

	public function setOpeningBalanceDate( $strOpeningBalanceDate ) {
		$this->set( 'm_strOpeningBalanceDate', CStrings::strTrimDef( $strOpeningBalanceDate, -1, NULL, true ) );
	}

	public function getOpeningBalanceDate() {
		return $this->m_strOpeningBalanceDate;
	}

	public function sqlOpeningBalanceDate() {
		return ( true == isset( $this->m_strOpeningBalanceDate ) ) ? '\'' . $this->m_strOpeningBalanceDate . '\'' : 'NULL';
	}

	public function setOpeningBalanceAmount( $fltOpeningBalanceAmount ) {
		$this->set( 'm_fltOpeningBalanceAmount', CStrings::strToFloatDef( $fltOpeningBalanceAmount, NULL, false, 4 ) );
	}

	public function getOpeningBalanceAmount() {
		return $this->m_fltOpeningBalanceAmount;
	}

	public function sqlOpeningBalanceAmount() {
		return ( true == isset( $this->m_fltOpeningBalanceAmount ) ) ? ( string ) $this->m_fltOpeningBalanceAmount : 'NULL';
	}

	public function setBankAccountNumberEncrypted( $strBankAccountNumberEncrypted ) {
		$this->set( 'm_strBankAccountNumberEncrypted', CStrings::strTrimDef( $strBankAccountNumberEncrypted, 240, NULL, true ) );
	}

	public function getBankAccountNumberEncrypted() {
		return $this->m_strBankAccountNumberEncrypted;
	}

	public function sqlBankAccountNumberEncrypted() {
		return ( true == isset( $this->m_strBankAccountNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strBankAccountNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setBankAccountNumberMasked( $strBankAccountNumberMasked ) {
		$this->set( 'm_strBankAccountNumberMasked', CStrings::strTrimDef( $strBankAccountNumberMasked, 240, NULL, true ) );
	}

	public function getBankAccountNumberMasked() {
		return $this->m_strBankAccountNumberMasked;
	}

	public function sqlBankAccountNumberMasked() {
		return ( true == isset( $this->m_strBankAccountNumberMasked ) ) ? '\'' . addslashes( $this->m_strBankAccountNumberMasked ) . '\'' : 'NULL';
	}

	public function setBankRoutingNumberEncrypted( $strBankRoutingNumberEncrypted ) {
		$this->set( 'm_strBankRoutingNumberEncrypted', CStrings::strTrimDef( $strBankRoutingNumberEncrypted, 240, NULL, true ) );
	}

	public function getBankRoutingNumberEncrypted() {
		return $this->m_strBankRoutingNumberEncrypted;
	}

	public function sqlBankRoutingNumberEncrypted() {
		return ( true == isset( $this->m_strBankRoutingNumberEncrypted ) ) ? '\'' . addslashes( $this->m_strBankRoutingNumberEncrypted ) . '\'' : 'NULL';
	}

	public function setBankRoutingNumberMasked( $strBankRoutingNumberMasked ) {
		$this->set( 'm_strBankRoutingNumberMasked', CStrings::strTrimDef( $strBankRoutingNumberMasked, 240, NULL, true ) );
	}

	public function getBankRoutingNumberMasked() {
		return $this->m_strBankRoutingNumberMasked;
	}

	public function sqlBankRoutingNumberMasked() {
		return ( true == isset( $this->m_strBankRoutingNumberMasked ) ) ? '\'' . addslashes( $this->m_strBankRoutingNumberMasked ) . '\'' : 'NULL';
	}

	public function setIsDisabled( $intIsDisabled ) {
		$this->set( 'm_intIsDisabled', CStrings::strToIntDef( $intIsDisabled, NULL, false ) );
	}

	public function getIsDisabled() {
		return $this->m_intIsDisabled;
	}

	public function sqlIsDisabled() {
		return ( true == isset( $this->m_intIsDisabled ) ) ? ( string ) $this->m_intIsDisabled : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, chart_of_account_type_id, chart_of_account_extra_type_id, accounting_system_type_id, processing_bank_account_id, account_name, account_number, secondary_number, secondary_product_name, description, class, opening_balance_date, opening_balance_amount, bank_account_number_encrypted, bank_account_number_masked, bank_routing_number_encrypted, bank_routing_number_masked, is_disabled, is_published, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlChartOfAccountTypeId() . ', ' .
 						$this->sqlChartOfAccountExtraTypeId() . ', ' .
 						$this->sqlAccountingSystemTypeId() . ', ' .
 						$this->sqlProcessingBankAccountId() . ', ' .
 						$this->sqlAccountName() . ', ' .
 						$this->sqlAccountNumber() . ', ' .
 						$this->sqlSecondaryNumber() . ', ' .
 						$this->sqlSecondaryProductName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlClass() . ', ' .
 						$this->sqlOpeningBalanceDate() . ', ' .
 						$this->sqlOpeningBalanceAmount() . ', ' .
 						$this->sqlBankAccountNumberEncrypted() . ', ' .
 						$this->sqlBankAccountNumberMasked() . ', ' .
 						$this->sqlBankRoutingNumberEncrypted() . ', ' .
 						$this->sqlBankRoutingNumberMasked() . ', ' .
 						$this->sqlIsDisabled() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chart_of_account_type_id = ' . $this->sqlChartOfAccountTypeId() . ','; } elseif( true == array_key_exists( 'ChartOfAccountTypeId', $this->getChangedColumns() ) ) { $strSql .= ' chart_of_account_type_id = ' . $this->sqlChartOfAccountTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' chart_of_account_extra_type_id = ' . $this->sqlChartOfAccountExtraTypeId() . ','; } elseif( true == array_key_exists( 'ChartOfAccountExtraTypeId', $this->getChangedColumns() ) ) { $strSql .= ' chart_of_account_extra_type_id = ' . $this->sqlChartOfAccountExtraTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accounting_system_type_id = ' . $this->sqlAccountingSystemTypeId() . ','; } elseif( true == array_key_exists( 'AccountingSystemTypeId', $this->getChangedColumns() ) ) { $strSql .= ' accounting_system_type_id = ' . $this->sqlAccountingSystemTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; } elseif( true == array_key_exists( 'ProcessingBankAccountId', $this->getChangedColumns() ) ) { $strSql .= ' processing_bank_account_id = ' . $this->sqlProcessingBankAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_name = ' . $this->sqlAccountName() . ','; } elseif( true == array_key_exists( 'AccountName', $this->getChangedColumns() ) ) { $strSql .= ' account_name = ' . $this->sqlAccountName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber() . ','; } elseif( true == array_key_exists( 'AccountNumber', $this->getChangedColumns() ) ) { $strSql .= ' account_number = ' . $this->sqlAccountNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secondary_number = ' . $this->sqlSecondaryNumber() . ','; } elseif( true == array_key_exists( 'SecondaryNumber', $this->getChangedColumns() ) ) { $strSql .= ' secondary_number = ' . $this->sqlSecondaryNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secondary_product_name = ' . $this->sqlSecondaryProductName() . ','; } elseif( true == array_key_exists( 'SecondaryProductName', $this->getChangedColumns() ) ) { $strSql .= ' secondary_product_name = ' . $this->sqlSecondaryProductName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' class = ' . $this->sqlClass() . ','; } elseif( true == array_key_exists( 'Class', $this->getChangedColumns() ) ) { $strSql .= ' class = ' . $this->sqlClass() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' opening_balance_date = ' . $this->sqlOpeningBalanceDate() . ','; } elseif( true == array_key_exists( 'OpeningBalanceDate', $this->getChangedColumns() ) ) { $strSql .= ' opening_balance_date = ' . $this->sqlOpeningBalanceDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' opening_balance_amount = ' . $this->sqlOpeningBalanceAmount() . ','; } elseif( true == array_key_exists( 'OpeningBalanceAmount', $this->getChangedColumns() ) ) { $strSql .= ' opening_balance_amount = ' . $this->sqlOpeningBalanceAmount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_number_encrypted = ' . $this->sqlBankAccountNumberEncrypted() . ','; } elseif( true == array_key_exists( 'BankAccountNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_number_encrypted = ' . $this->sqlBankAccountNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_account_number_masked = ' . $this->sqlBankAccountNumberMasked() . ','; } elseif( true == array_key_exists( 'BankAccountNumberMasked', $this->getChangedColumns() ) ) { $strSql .= ' bank_account_number_masked = ' . $this->sqlBankAccountNumberMasked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_routing_number_encrypted = ' . $this->sqlBankRoutingNumberEncrypted() . ','; } elseif( true == array_key_exists( 'BankRoutingNumberEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' bank_routing_number_encrypted = ' . $this->sqlBankRoutingNumberEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bank_routing_number_masked = ' . $this->sqlBankRoutingNumberMasked() . ','; } elseif( true == array_key_exists( 'BankRoutingNumberMasked', $this->getChangedColumns() ) ) { $strSql .= ' bank_routing_number_masked = ' . $this->sqlBankRoutingNumberMasked() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; } elseif( true == array_key_exists( 'IsDisabled', $this->getChangedColumns() ) ) { $strSql .= ' is_disabled = ' . $this->sqlIsDisabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'chart_of_account_type_id' => $this->getChartOfAccountTypeId(),
			'chart_of_account_extra_type_id' => $this->getChartOfAccountExtraTypeId(),
			'accounting_system_type_id' => $this->getAccountingSystemTypeId(),
			'processing_bank_account_id' => $this->getProcessingBankAccountId(),
			'account_name' => $this->getAccountName(),
			'account_number' => $this->getAccountNumber(),
			'secondary_number' => $this->getSecondaryNumber(),
			'secondary_product_name' => $this->getSecondaryProductName(),
			'description' => $this->getDescription(),
			'class' => $this->getClass(),
			'opening_balance_date' => $this->getOpeningBalanceDate(),
			'opening_balance_amount' => $this->getOpeningBalanceAmount(),
			'bank_account_number_encrypted' => $this->getBankAccountNumberEncrypted(),
			'bank_account_number_masked' => $this->getBankAccountNumberMasked(),
			'bank_routing_number_encrypted' => $this->getBankRoutingNumberEncrypted(),
			'bank_routing_number_masked' => $this->getBankRoutingNumberMasked(),
			'is_disabled' => $this->getIsDisabled(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>