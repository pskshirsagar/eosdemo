<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CScriptGroups
 * Do not add any new functions to this class.
 */

class CBaseScriptGroups extends CEosPluralBase {

	/**
	 * @return CScriptGroup[]
	 */
	public static function fetchScriptGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CScriptGroup', $objDatabase );
	}

	/**
	 * @return CScriptGroup
	 */
	public static function fetchScriptGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CScriptGroup', $objDatabase );
	}

	public static function fetchScriptGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'script_groups', $objDatabase );
	}

	public static function fetchScriptGroupById( $intId, $objDatabase ) {
		return self::fetchScriptGroup( sprintf( 'SELECT * FROM script_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchScriptGroupsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchScriptGroups( sprintf( 'SELECT * FROM script_groups WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

	public static function fetchScriptGroupsByScriptId( $intScriptId, $objDatabase ) {
		return self::fetchScriptGroups( sprintf( 'SELECT * FROM script_groups WHERE script_id = %d', ( int ) $intScriptId ), $objDatabase );
	}

}
?>