<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAssessmentTrainingResponses
 * Do not add any new functions to this class.
 */

class CBaseEmployeeAssessmentTrainingResponses extends CEosPluralBase {

	/**
	 * @return CEmployeeAssessmentTrainingResponse[]
	 */
	public static function fetchEmployeeAssessmentTrainingResponses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeAssessmentTrainingResponse', $objDatabase );
	}

	/**
	 * @return CEmployeeAssessmentTrainingResponse
	 */
	public static function fetchEmployeeAssessmentTrainingResponse( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeAssessmentTrainingResponse', $objDatabase );
	}

	public static function fetchEmployeeAssessmentTrainingResponseCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_assessment_training_responses', $objDatabase );
	}

	public static function fetchEmployeeAssessmentTrainingResponseById( $intId, $objDatabase ) {
		return self::fetchEmployeeAssessmentTrainingResponse( sprintf( 'SELECT * FROM employee_assessment_training_responses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentTrainingResponsesByTrainingCategoryId( $intTrainingCategoryId, $objDatabase ) {
		return self::fetchEmployeeAssessmentTrainingResponses( sprintf( 'SELECT * FROM employee_assessment_training_responses WHERE training_category_id = %d', ( int ) $intTrainingCategoryId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentTrainingResponsesByEmployeeAssessmentId( $intEmployeeAssessmentId, $objDatabase ) {
		return self::fetchEmployeeAssessmentTrainingResponses( sprintf( 'SELECT * FROM employee_assessment_training_responses WHERE employee_assessment_id = %d', ( int ) $intEmployeeAssessmentId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentTrainingResponsesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeAssessmentTrainingResponses( sprintf( 'SELECT * FROM employee_assessment_training_responses WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeAssessmentTrainingResponsesByEmployeeAssessmentQuestionId( $intEmployeeAssessmentQuestionId, $objDatabase ) {
		return self::fetchEmployeeAssessmentTrainingResponses( sprintf( 'SELECT * FROM employee_assessment_training_responses WHERE employee_assessment_question_id = %d', ( int ) $intEmployeeAssessmentQuestionId ), $objDatabase );
	}

}
?>