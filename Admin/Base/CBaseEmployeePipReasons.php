<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePipReasons
 * Do not add any new functions to this class.
 */

class CBaseEmployeePipReasons extends CEosPluralBase {

	/**
	 * @return CEmployeePipReason[]
	 */
	public static function fetchEmployeePipReasons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeePipReason', $objDatabase );
	}

	/**
	 * @return CEmployeePipReason
	 */
	public static function fetchEmployeePipReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeePipReason', $objDatabase );
	}

	public static function fetchEmployeePipReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_pip_reasons', $objDatabase );
	}

	public static function fetchEmployeePipReasonById( $intId, $objDatabase ) {
		return self::fetchEmployeePipReason( sprintf( 'SELECT * FROM employee_pip_reasons WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeePipReasonsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeePipReasons( sprintf( 'SELECT * FROM employee_pip_reasons WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeePipReasonsByPipReasonId( $intPipReasonId, $objDatabase ) {
		return self::fetchEmployeePipReasons( sprintf( 'SELECT * FROM employee_pip_reasons WHERE pip_reason_id = %d', ( int ) $intPipReasonId ), $objDatabase );
	}

	public static function fetchEmployeePipReasonsByEmployeeHistoryId( $intEmployeeHistoryId, $objDatabase ) {
		return self::fetchEmployeePipReasons( sprintf( 'SELECT * FROM employee_pip_reasons WHERE employee_history_id = %d', ( int ) $intEmployeeHistoryId ), $objDatabase );
	}

}
?>