<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroupScheduleTypes
 * Do not add any new functions to this class.
 */

class CBaseGroupScheduleTypes extends CEosPluralBase {

	/**
	 * @return CGroupScheduleType[]
	 */
	public static function fetchGroupScheduleTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CGroupScheduleType::class, $objDatabase );
	}

	/**
	 * @return CGroupScheduleType
	 */
	public static function fetchGroupScheduleType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CGroupScheduleType::class, $objDatabase );
	}

	public static function fetchGroupScheduleTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'group_schedule_types', $objDatabase );
	}

	public static function fetchGroupScheduleTypeById( $intId, $objDatabase ) {
		return self::fetchGroupScheduleType( sprintf( 'SELECT * FROM group_schedule_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>