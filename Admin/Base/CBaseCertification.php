<?php

class CBaseCertification extends CEosSingularBase {

	const TABLE_NAME = 'public.certifications';

	protected $m_intId;
    protected $m_intCid;
	protected $m_intCertificationId;
	protected $m_intCertificationTypeId;
	protected $m_intCertificationLevelTypeId;
	protected $m_intDepartmentId;
	protected $m_intDesignationId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strLogoPath;
	protected $m_intMinimumScore;
	protected $m_intIsExternal;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsExternal = '0';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['certification_id'] ) && $boolDirectSet ) $this->set( 'm_intCertificationId', trim( $arrValues['certification_id'] ) ); elseif( isset( $arrValues['certification_id'] ) ) $this->setCertificationId( $arrValues['certification_id'] );
		if( isset( $arrValues['certification_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCertificationTypeId', trim( $arrValues['certification_type_id'] ) ); elseif( isset( $arrValues['certification_type_id'] ) ) $this->setCertificationTypeId( $arrValues['certification_type_id'] );
		if( isset( $arrValues['certification_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intCertificationLevelTypeId', trim( $arrValues['certification_level_type_id'] ) ); elseif( isset( $arrValues['certification_level_type_id'] ) ) $this->setCertificationLevelTypeId( $arrValues['certification_level_type_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['designation_id'] ) && $boolDirectSet ) $this->set( 'm_intDesignationId', trim( $arrValues['designation_id'] ) ); elseif( isset( $arrValues['designation_id'] ) ) $this->setDesignationId( $arrValues['designation_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['logo_path'] ) && $boolDirectSet ) $this->set( 'm_strLogoPath', trim( stripcslashes( $arrValues['logo_path'] ) ) ); elseif( isset( $arrValues['logo_path'] ) ) $this->setLogoPath( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['logo_path'] ) : $arrValues['logo_path'] );
		if( isset( $arrValues['minimum_score'] ) && $boolDirectSet ) $this->set( 'm_intMinimumScore', trim( $arrValues['minimum_score'] ) ); elseif( isset( $arrValues['minimum_score'] ) ) $this->setMinimumScore( $arrValues['minimum_score'] );
		if( isset( $arrValues['is_external'] ) && $boolDirectSet ) $this->set( 'm_intIsExternal', trim( $arrValues['is_external'] ) ); elseif( isset( $arrValues['is_external'] ) ) $this->setIsExternal( $arrValues['is_external'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

    public function setCid( $intCid ) {
        $this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
    }

    public function getCid() {
        return $this->m_intCid;
    }

	public function setCertificationId( $intCertificationId ) {
		$this->set( 'm_intCertificationId', CStrings::strToIntDef( $intCertificationId, NULL, false ) );
	}

	public function getCertificationId() {
		return $this->m_intCertificationId;
	}

	public function sqlCertificationId() {
		return ( true == isset( $this->m_intCertificationId ) ) ? ( string ) $this->m_intCertificationId : 'NULL';
	}

	public function setCertificationTypeId( $intCertificationTypeId ) {
		$this->set( 'm_intCertificationTypeId', CStrings::strToIntDef( $intCertificationTypeId, NULL, false ) );
	}

	public function getCertificationTypeId() {
		return $this->m_intCertificationTypeId;
	}

	public function sqlCertificationTypeId() {
		return ( true == isset( $this->m_intCertificationTypeId ) ) ? ( string ) $this->m_intCertificationTypeId : 'NULL';
	}

	public function setCertificationLevelTypeId( $intCertificationLevelTypeId ) {
		$this->set( 'm_intCertificationLevelTypeId', CStrings::strToIntDef( $intCertificationLevelTypeId, NULL, false ) );
	}

	public function getCertificationLevelTypeId() {
		return $this->m_intCertificationLevelTypeId;
	}

	public function sqlCertificationLevelTypeId() {
		return ( true == isset( $this->m_intCertificationLevelTypeId ) ) ? ( string ) $this->m_intCertificationLevelTypeId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setDesignationId( $intDesignationId ) {
		$this->set( 'm_intDesignationId', CStrings::strToIntDef( $intDesignationId, NULL, false ) );
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function sqlDesignationId() {
		return ( true == isset( $this->m_intDesignationId ) ) ? ( string ) $this->m_intDesignationId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setLogoPath( $strLogoPath ) {
		$this->set( 'm_strLogoPath', CStrings::strTrimDef( $strLogoPath, 100, NULL, true ) );
	}

	public function getLogoPath() {
		return $this->m_strLogoPath;
	}

	public function sqlLogoPath() {
		return ( true == isset( $this->m_strLogoPath ) ) ? '\'' . addslashes( $this->m_strLogoPath ) . '\'' : 'NULL';
	}

	public function setMinimumScore( $intMinimumScore ) {
		$this->set( 'm_intMinimumScore', CStrings::strToIntDef( $intMinimumScore, NULL, false ) );
	}

	public function getMinimumScore() {
		return $this->m_intMinimumScore;
	}

	public function sqlMinimumScore() {
		return ( true == isset( $this->m_intMinimumScore ) ) ? ( string ) $this->m_intMinimumScore : 'NULL';
	}

	public function setIsExternal( $intIsExternal ) {
		$this->set( 'm_intIsExternal', CStrings::strToIntDef( $intIsExternal, NULL, false ) );
	}

	public function getIsExternal() {
		return $this->m_intIsExternal;
	}

	public function sqlIsExternal() {
		return ( true == isset( $this->m_intIsExternal ) ) ? ( string ) $this->m_intIsExternal : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, certification_id, certification_type_id, certification_level_type_id, department_id, designation_id, name, description, logo_path, minimum_score, is_external, is_published, order_num, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCertificationId() . ', ' .
 						$this->sqlCertificationTypeId() . ', ' .
 						$this->sqlCertificationLevelTypeId() . ', ' .
 						$this->sqlDepartmentId() . ', ' .
 						$this->sqlDesignationId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlLogoPath() . ', ' .
 						$this->sqlMinimumScore() . ', ' .
 						$this->sqlIsExternal() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' certification_id = ' . $this->sqlCertificationId() . ','; } elseif( true == array_key_exists( 'CertificationId', $this->getChangedColumns() ) ) { $strSql .= ' certification_id = ' . $this->sqlCertificationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' certification_type_id = ' . $this->sqlCertificationTypeId() . ','; } elseif( true == array_key_exists( 'CertificationTypeId', $this->getChangedColumns() ) ) { $strSql .= ' certification_type_id = ' . $this->sqlCertificationTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' certification_level_type_id = ' . $this->sqlCertificationLevelTypeId() . ','; } elseif( true == array_key_exists( 'CertificationLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' certification_level_type_id = ' . $this->sqlCertificationLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; } elseif( true == array_key_exists( 'DesignationId', $this->getChangedColumns() ) ) { $strSql .= ' designation_id = ' . $this->sqlDesignationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' logo_path = ' . $this->sqlLogoPath() . ','; } elseif( true == array_key_exists( 'LogoPath', $this->getChangedColumns() ) ) { $strSql .= ' logo_path = ' . $this->sqlLogoPath() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' minimum_score = ' . $this->sqlMinimumScore() . ','; } elseif( true == array_key_exists( 'MinimumScore', $this->getChangedColumns() ) ) { $strSql .= ' minimum_score = ' . $this->sqlMinimumScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_external = ' . $this->sqlIsExternal() . ','; } elseif( true == array_key_exists( 'IsExternal', $this->getChangedColumns() ) ) { $strSql .= ' is_external = ' . $this->sqlIsExternal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'certification_id' => $this->getCertificationId(),
			'certification_type_id' => $this->getCertificationTypeId(),
			'certification_level_type_id' => $this->getCertificationLevelTypeId(),
			'department_id' => $this->getDepartmentId(),
			'designation_id' => $this->getDesignationId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'logo_path' => $this->getLogoPath(),
			'minimum_score' => $this->getMinimumScore(),
			'is_external' => $this->getIsExternal(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>