<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemKeywordSources
 * Do not add any new functions to this class.
 */

class CBaseSemKeywordSources extends CEosPluralBase {

	/**
	 * @return CSemKeywordSource[]
	 */
	public static function fetchSemKeywordSources( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemKeywordSource', $objDatabase );
	}

	/**
	 * @return CSemKeywordSource
	 */
	public static function fetchSemKeywordSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemKeywordSource', $objDatabase );
	}

	public static function fetchSemKeywordSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_keyword_sources', $objDatabase );
	}

	public static function fetchSemKeywordSourceById( $intId, $objDatabase ) {
		return self::fetchSemKeywordSource( sprintf( 'SELECT * FROM sem_keyword_sources WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemKeywordSourcesBySemSourceId( $intSemSourceId, $objDatabase ) {
		return self::fetchSemKeywordSources( sprintf( 'SELECT * FROM sem_keyword_sources WHERE sem_source_id = %d', ( int ) $intSemSourceId ), $objDatabase );
	}

	public static function fetchSemKeywordSourcesBySemAdGroupId( $intSemAdGroupId, $objDatabase ) {
		return self::fetchSemKeywordSources( sprintf( 'SELECT * FROM sem_keyword_sources WHERE sem_ad_group_id = %d', ( int ) $intSemAdGroupId ), $objDatabase );
	}

	public static function fetchSemKeywordSourcesBySemKeywordId( $intSemKeywordId, $objDatabase ) {
		return self::fetchSemKeywordSources( sprintf( 'SELECT * FROM sem_keyword_sources WHERE sem_keyword_id = %d', ( int ) $intSemKeywordId ), $objDatabase );
	}

	public static function fetchSemKeywordSourcesBySemStatusTypeId( $intSemStatusTypeId, $objDatabase ) {
		return self::fetchSemKeywordSources( sprintf( 'SELECT * FROM sem_keyword_sources WHERE sem_status_type_id = %d', ( int ) $intSemStatusTypeId ), $objDatabase );
	}

}
?>