<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CGroupDesignations
 * Do not add any new functions to this class.
 */

class CBaseGroupDesignations extends CEosPluralBase {

	public static function fetchGroupDesignations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CGroupDesignation', $objDatabase );
	}

	public static function fetchGroupDesignation( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CGroupDesignation', $objDatabase );
	}

	public static function fetchGroupDesignationCount( $strWhere = NULL, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'group_designations', $objDatabase );
	}

	public static function fetchGroupDesignationById( $intId, $objDatabase ) {
		return self::fetchGroupDesignation( sprintf( 'SELECT * FROM group_designations WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchGroupDesignationsByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchGroupDesignations( sprintf( 'SELECT * FROM group_designations WHERE designation_id = %d', ( int ) $intDesignationId ), $objDatabase );
	}

	public static function fetchGroupDesignationsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchGroupDesignations( sprintf( 'SELECT * FROM group_designations WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

}
?>