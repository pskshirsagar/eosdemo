<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeHistories
 * Do not add any new functions to this class.
 */

class CBaseEmployeeHistories extends CEosPluralBase {

	/**
	 * @return CEmployeeHistory[]
	 */
	public static function fetchEmployeeHistories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeHistory::class, $objDatabase );
	}

	/**
	 * @return CEmployeeHistory
	 */
	public static function fetchEmployeeHistory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeHistory::class, $objDatabase );
	}

	public static function fetchEmployeeHistoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_histories', $objDatabase );
	}

	public static function fetchEmployeeHistoryById( $intId, $objDatabase ) {
		return self::fetchEmployeeHistory( sprintf( 'SELECT * FROM employee_histories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeHistoriesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeHistories( sprintf( 'SELECT * FROM employee_histories WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeHistoriesByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchEmployeeHistories( sprintf( 'SELECT * FROM employee_histories WHERE designation_id = %d', ( int ) $intDesignationId ), $objDatabase );
	}

	public static function fetchEmployeeHistoriesByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchEmployeeHistories( sprintf( 'SELECT * FROM employee_histories WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchEmployeeHistoriesByTeamId( $intTeamId, $objDatabase ) {
		return self::fetchEmployeeHistories( sprintf( 'SELECT * FROM employee_histories WHERE team_id = %d', ( int ) $intTeamId ), $objDatabase );
	}

	public static function fetchEmployeeHistoriesByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		return self::fetchEmployeeHistories( sprintf( 'SELECT * FROM employee_histories WHERE manager_employee_id = %d', ( int ) $intManagerEmployeeId ), $objDatabase );
	}

}
?>