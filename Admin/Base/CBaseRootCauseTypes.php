<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CRootCauseTypes
 * Do not add any new functions to this class.
 */

class CBaseRootCauseTypes extends CEosPluralBase {

	/**
	 * @return CRootCauseType[]
	 */
	public static function fetchRootCauseTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CRootCauseType', $objDatabase );
	}

	/**
	 * @return CRootCauseType
	 */
	public static function fetchRootCauseType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CRootCauseType', $objDatabase );
	}

	public static function fetchRootCauseTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'root_cause_types', $objDatabase );
	}

	public static function fetchRootCauseTypeById( $intId, $objDatabase ) {
		return self::fetchRootCauseType( sprintf( 'SELECT * FROM root_cause_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>