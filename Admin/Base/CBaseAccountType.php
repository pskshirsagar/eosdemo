<?php

class CBaseAccountType extends CEosSingularBase {

	const TABLE_NAME = 'public.account_types';

	protected $m_intId;
	protected $m_intPsProcessingBankAccountId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intIsTaxable;
	protected $m_intIsPublished;
	protected $m_intOrderNum;

	public function __construct() {
		parent::__construct();

		$this->m_intIsTaxable = '1';
		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_processing_bank_account_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProcessingBankAccountId', trim( $arrValues['ps_processing_bank_account_id'] ) ); elseif( isset( $arrValues['ps_processing_bank_account_id'] ) ) $this->setPsProcessingBankAccountId( $arrValues['ps_processing_bank_account_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['is_taxable'] ) && $boolDirectSet ) $this->set( 'm_intIsTaxable', trim( $arrValues['is_taxable'] ) ); elseif( isset( $arrValues['is_taxable'] ) ) $this->setIsTaxable( $arrValues['is_taxable'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsProcessingBankAccountId( $intPsProcessingBankAccountId ) {
		$this->set( 'm_intPsProcessingBankAccountId', CStrings::strToIntDef( $intPsProcessingBankAccountId, NULL, false ) );
	}

	public function getPsProcessingBankAccountId() {
		return $this->m_intPsProcessingBankAccountId;
	}

	public function sqlPsProcessingBankAccountId() {
		return ( true == isset( $this->m_intPsProcessingBankAccountId ) ) ? ( string ) $this->m_intPsProcessingBankAccountId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 240, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setIsTaxable( $intIsTaxable ) {
		$this->set( 'm_intIsTaxable', CStrings::strToIntDef( $intIsTaxable, NULL, false ) );
	}

	public function getIsTaxable() {
		return $this->m_intIsTaxable;
	}

	public function sqlIsTaxable() {
		return ( true == isset( $this->m_intIsTaxable ) ) ? ( string ) $this->m_intIsTaxable : '1';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_processing_bank_account_id' => $this->getPsProcessingBankAccountId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'is_taxable' => $this->getIsTaxable(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum()
		);
	}

}
?>