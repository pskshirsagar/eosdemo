<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskResults
 * Do not add any new functions to this class.
 */

class CBaseTaskResults extends CEosPluralBase {

	/**
	 * @return CTaskResult[]
	 */
	public static function fetchTaskResults( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskResult', $objDatabase );
	}

	/**
	 * @return CTaskResult
	 */
	public static function fetchTaskResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskResult', $objDatabase );
	}

	public static function fetchTaskResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_results', $objDatabase );
	}

	public static function fetchTaskResultById( $intId, $objDatabase ) {
		return self::fetchTaskResult( sprintf( 'SELECT * FROM task_results WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>