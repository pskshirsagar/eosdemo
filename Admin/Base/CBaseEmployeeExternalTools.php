<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeExternalTools
 * Do not add any new functions to this class.
 */

class CBaseEmployeeExternalTools extends CEosPluralBase {

	/**
	 * @return CEmployeeExternalTool[]
	 */
	public static function fetchEmployeeExternalTools( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CEmployeeExternalTool::class, $objDatabase );
	}

	/**
	 * @return CEmployeeExternalTool
	 */
	public static function fetchEmployeeExternalTool( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CEmployeeExternalTool::class, $objDatabase );
	}

	public static function fetchEmployeeExternalToolCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_external_tools', $objDatabase );
	}

	public static function fetchEmployeeExternalToolById( $intId, $objDatabase ) {
		return self::fetchEmployeeExternalTool( sprintf( 'SELECT * FROM employee_external_tools WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeExternalToolsByExternalToolId( $intExternalToolId, $objDatabase ) {
		return self::fetchEmployeeExternalTools( sprintf( 'SELECT * FROM employee_external_tools WHERE external_tool_id = %d', ( int ) $intExternalToolId ), $objDatabase );
	}

	public static function fetchEmployeeExternalToolsByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeExternalTools( sprintf( 'SELECT * FROM employee_external_tools WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>