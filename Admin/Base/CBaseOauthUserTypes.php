<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\COauthUserTypes
 * Do not add any new functions to this class.
 */

class CBaseOauthUserTypes extends CEosPluralBase {

	/**
	 * @return COauthUserType[]
	 */
	public static function fetchOauthUserTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'COauthUserType', $objDatabase );
	}

	/**
	 * @return COauthUserType
	 */
	public static function fetchOauthUserType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COauthUserType', $objDatabase );
	}

	public static function fetchOauthUserTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'oauth_user_types', $objDatabase );
	}

	public static function fetchOauthUserTypeById( $intId, $objDatabase ) {
		return self::fetchOauthUserType( sprintf( 'SELECT * FROM oauth_user_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>