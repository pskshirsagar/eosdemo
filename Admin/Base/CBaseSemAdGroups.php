<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdGroups
 * Do not add any new functions to this class.
 */

class CBaseSemAdGroups extends CEosPluralBase {

	/**
	 * @return CSemAdGroup[]
	 */
	public static function fetchSemAdGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemAdGroup', $objDatabase );
	}

	/**
	 * @return CSemAdGroup
	 */
	public static function fetchSemAdGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemAdGroup', $objDatabase );
	}

	public static function fetchSemAdGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_ad_groups', $objDatabase );
	}

	public static function fetchSemAdGroupById( $intId, $objDatabase ) {
		return self::fetchSemAdGroup( sprintf( 'SELECT * FROM sem_ad_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemAdGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchSemAdGroups( sprintf( 'SELECT * FROM sem_ad_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchSemAdGroupsByPropertyId( $intPropertyId, $objDatabase ) {
		return self::fetchSemAdGroups( sprintf( 'SELECT * FROM sem_ad_groups WHERE property_id = %d', ( int ) $intPropertyId ), $objDatabase );
	}

	public static function fetchSemAdGroupsBySemCampaignId( $intSemCampaignId, $objDatabase ) {
		return self::fetchSemAdGroups( sprintf( 'SELECT * FROM sem_ad_groups WHERE sem_campaign_id = %d', ( int ) $intSemCampaignId ), $objDatabase );
	}

	public static function fetchSemAdGroupsBySemLocationTypeId( $intSemLocationTypeId, $objDatabase ) {
		return self::fetchSemAdGroups( sprintf( 'SELECT * FROM sem_ad_groups WHERE sem_location_type_id = %d', ( int ) $intSemLocationTypeId ), $objDatabase );
	}

}
?>