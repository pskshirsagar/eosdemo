<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationHistories
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationHistories extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationHistory[]
	 */
	public static function fetchEmployeeApplicationHistories( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationHistory', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationHistory
	 */
	public static function fetchEmployeeApplicationHistory( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationHistory', $objDatabase );
	}

	public static function fetchEmployeeApplicationHistoryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_histories', $objDatabase );
	}

	public static function fetchEmployeeApplicationHistoryById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationHistory( sprintf( 'SELECT * FROM employee_application_histories WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationHistoriesByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchEmployeeApplicationHistories( sprintf( 'SELECT * FROM employee_application_histories WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationHistoriesByEmployeeApplicationCompanyId( $intEmployeeApplicationCompanyId, $objDatabase ) {
		return self::fetchEmployeeApplicationHistories( sprintf( 'SELECT * FROM employee_application_histories WHERE employee_application_company_id = %d', ( int ) $intEmployeeApplicationCompanyId ), $objDatabase );
	}

}
?>