<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPostalCodes
 * Do not add any new functions to this class.
 */

class CBasePostalCodes extends CEosPluralBase {

	/**
	 * @return CPostalCode[]
	 */
	public static function fetchPostalCodes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPostalCode', $objDatabase );
	}

	/**
	 * @return CPostalCode
	 */
	public static function fetchPostalCode( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPostalCode', $objDatabase );
	}

	public static function fetchPostalCodeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'postal_codes', $objDatabase );
	}

	public static function fetchPostalCodeById( $intId, $objDatabase ) {
		return self::fetchPostalCode( sprintf( 'SELECT * FROM postal_codes WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>