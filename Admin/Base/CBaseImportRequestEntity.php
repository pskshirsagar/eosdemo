<?php

class CBaseImportRequestEntity extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.import_request_entities';

	protected $m_intId;
	protected $m_intImportRequestId;
	protected $m_intPropertyId;
	protected $m_intDestinationPropertyId;
	protected $m_intImportEntityId;
	protected $m_intImportTypeId;
	protected $m_intImportStatusTypeId;
	protected $m_strErrorMessages;
	protected $m_strPostDate;
	protected $m_intAcceptedBy;
	protected $m_strAcceptedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intImportDataTypeId;
	protected $m_strValue;
	protected $m_strFileName;
	protected $m_strFileType;
	protected $m_intFileSize;
	protected $m_strValidationStartDatetime;
	protected $m_strValidationEndDatetime;
	protected $m_strSyncStartDatetime;
	protected $m_strSyncEndDatetime;
	protected $m_strValidationErrorDescription;
	protected $m_jsonValidationErrorDescription;
	protected $m_strSyncErrorDesciption;
	protected $m_jsonSyncErrorDesciption;
	protected $m_intCid;
	protected $m_strDetails;
	protected $m_jsonDetails;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['import_request_id'] ) && $boolDirectSet ) $this->set( 'm_intImportRequestId', trim( $arrValues['import_request_id'] ) ); elseif( isset( $arrValues['import_request_id'] ) ) $this->setImportRequestId( $arrValues['import_request_id'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['destination_property_id'] ) && $boolDirectSet ) $this->set( 'm_intDestinationPropertyId', trim( $arrValues['destination_property_id'] ) ); elseif( isset( $arrValues['destination_property_id'] ) ) $this->setDestinationPropertyId( $arrValues['destination_property_id'] );
		if( isset( $arrValues['import_entity_id'] ) && $boolDirectSet ) $this->set( 'm_intImportEntityId', trim( $arrValues['import_entity_id'] ) ); elseif( isset( $arrValues['import_entity_id'] ) ) $this->setImportEntityId( $arrValues['import_entity_id'] );
		if( isset( $arrValues['import_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportTypeId', trim( $arrValues['import_type_id'] ) ); elseif( isset( $arrValues['import_type_id'] ) ) $this->setImportTypeId( $arrValues['import_type_id'] );
		if( isset( $arrValues['import_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportStatusTypeId', trim( $arrValues['import_status_type_id'] ) ); elseif( isset( $arrValues['import_status_type_id'] ) ) $this->setImportStatusTypeId( $arrValues['import_status_type_id'] );
		if( isset( $arrValues['error_messages'] ) && $boolDirectSet ) $this->set( 'm_strErrorMessages', trim( $arrValues['error_messages'] ) ); elseif( isset( $arrValues['error_messages'] ) ) $this->setErrorMessages( $arrValues['error_messages'] );
		if( isset( $arrValues['post_date'] ) && $boolDirectSet ) $this->set( 'm_strPostDate', trim( $arrValues['post_date'] ) ); elseif( isset( $arrValues['post_date'] ) ) $this->setPostDate( $arrValues['post_date'] );
		if( isset( $arrValues['accepted_by'] ) && $boolDirectSet ) $this->set( 'm_intAcceptedBy', trim( $arrValues['accepted_by'] ) ); elseif( isset( $arrValues['accepted_by'] ) ) $this->setAcceptedBy( $arrValues['accepted_by'] );
		if( isset( $arrValues['accepted_on'] ) && $boolDirectSet ) $this->set( 'm_strAcceptedOn', trim( $arrValues['accepted_on'] ) ); elseif( isset( $arrValues['accepted_on'] ) ) $this->setAcceptedOn( $arrValues['accepted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['import_data_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportDataTypeId', trim( $arrValues['import_data_type_id'] ) ); elseif( isset( $arrValues['import_data_type_id'] ) ) $this->setImportDataTypeId( $arrValues['import_data_type_id'] );
		if( isset( $arrValues['value'] ) && $boolDirectSet ) $this->set( 'm_strValue', trim( $arrValues['value'] ) ); elseif( isset( $arrValues['value'] ) ) $this->setValue( $arrValues['value'] );
		if( isset( $arrValues['file_name'] ) && $boolDirectSet ) $this->set( 'm_strFileName', trim( $arrValues['file_name'] ) ); elseif( isset( $arrValues['file_name'] ) ) $this->setFileName( $arrValues['file_name'] );
		if( isset( $arrValues['file_type'] ) && $boolDirectSet ) $this->set( 'm_strFileType', trim( $arrValues['file_type'] ) ); elseif( isset( $arrValues['file_type'] ) ) $this->setFileType( $arrValues['file_type'] );
		if( isset( $arrValues['file_size'] ) && $boolDirectSet ) $this->set( 'm_intFileSize', trim( $arrValues['file_size'] ) ); elseif( isset( $arrValues['file_size'] ) ) $this->setFileSize( $arrValues['file_size'] );
		if( isset( $arrValues['validation_start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strValidationStartDatetime', trim( $arrValues['validation_start_datetime'] ) ); elseif( isset( $arrValues['validation_start_datetime'] ) ) $this->setValidationStartDatetime( $arrValues['validation_start_datetime'] );
		if( isset( $arrValues['validation_end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strValidationEndDatetime', trim( $arrValues['validation_end_datetime'] ) ); elseif( isset( $arrValues['validation_end_datetime'] ) ) $this->setValidationEndDatetime( $arrValues['validation_end_datetime'] );
		if( isset( $arrValues['sync_start_datetime'] ) && $boolDirectSet ) $this->set( 'm_strSyncStartDatetime', trim( $arrValues['sync_start_datetime'] ) ); elseif( isset( $arrValues['sync_start_datetime'] ) ) $this->setSyncStartDatetime( $arrValues['sync_start_datetime'] );
		if( isset( $arrValues['sync_end_datetime'] ) && $boolDirectSet ) $this->set( 'm_strSyncEndDatetime', trim( $arrValues['sync_end_datetime'] ) ); elseif( isset( $arrValues['sync_end_datetime'] ) ) $this->setSyncEndDatetime( $arrValues['sync_end_datetime'] );
		if( isset( $arrValues['validation_error_description'] ) ) $this->set( 'm_strValidationErrorDescription', trim( $arrValues['validation_error_description'] ) );
		if( isset( $arrValues['sync_error_desciption'] ) ) $this->set( 'm_strSyncErrorDesciption', trim( $arrValues['sync_error_desciption'] ) );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setImportRequestId( $intImportRequestId ) {
		$this->set( 'm_intImportRequestId', CStrings::strToIntDef( $intImportRequestId, NULL, false ) );
	}

	public function getImportRequestId() {
		return $this->m_intImportRequestId;
	}

	public function sqlImportRequestId() {
		return ( true == isset( $this->m_intImportRequestId ) ) ? ( string ) $this->m_intImportRequestId : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setDestinationPropertyId( $intDestinationPropertyId ) {
		$this->set( 'm_intDestinationPropertyId', CStrings::strToIntDef( $intDestinationPropertyId, NULL, false ) );
	}

	public function getDestinationPropertyId() {
		return $this->m_intDestinationPropertyId;
	}

	public function sqlDestinationPropertyId() {
		return ( true == isset( $this->m_intDestinationPropertyId ) ) ? ( string ) $this->m_intDestinationPropertyId : 'NULL';
	}

	public function setImportEntityId( $intImportEntityId ) {
		$this->set( 'm_intImportEntityId', CStrings::strToIntDef( $intImportEntityId, NULL, false ) );
	}

	public function getImportEntityId() {
		return $this->m_intImportEntityId;
	}

	public function sqlImportEntityId() {
		return ( true == isset( $this->m_intImportEntityId ) ) ? ( string ) $this->m_intImportEntityId : 'NULL';
	}

	public function setImportTypeId( $intImportTypeId ) {
		$this->set( 'm_intImportTypeId', CStrings::strToIntDef( $intImportTypeId, NULL, false ) );
	}

	public function getImportTypeId() {
		return $this->m_intImportTypeId;
	}

	public function sqlImportTypeId() {
		return ( true == isset( $this->m_intImportTypeId ) ) ? ( string ) $this->m_intImportTypeId : 'NULL';
	}

	public function setImportStatusTypeId( $intImportStatusTypeId ) {
		$this->set( 'm_intImportStatusTypeId', CStrings::strToIntDef( $intImportStatusTypeId, NULL, false ) );
	}

	public function getImportStatusTypeId() {
		return $this->m_intImportStatusTypeId;
	}

	public function sqlImportStatusTypeId() {
		return ( true == isset( $this->m_intImportStatusTypeId ) ) ? ( string ) $this->m_intImportStatusTypeId : 'NULL';
	}

	public function setErrorMessages( $strErrorMessages ) {
		$this->set( 'm_strErrorMessages', CStrings::strTrimDef( $strErrorMessages, -1, NULL, true ) );
	}

	public function getErrorMessages() {
		return $this->m_strErrorMessages;
	}

	public function sqlErrorMessages() {
		return ( true == isset( $this->m_strErrorMessages ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strErrorMessages ) : '\'' . addslashes( $this->m_strErrorMessages ) . '\'' ) : 'NULL';
	}

	public function setPostDate( $strPostDate ) {
		$this->set( 'm_strPostDate', CStrings::strTrimDef( $strPostDate, -1, NULL, true ) );
	}

	public function getPostDate() {
		return $this->m_strPostDate;
	}

	public function sqlPostDate() {
		return ( true == isset( $this->m_strPostDate ) ) ? '\'' . $this->m_strPostDate . '\'' : 'NULL';
	}

	public function setAcceptedBy( $intAcceptedBy ) {
		$this->set( 'm_intAcceptedBy', CStrings::strToIntDef( $intAcceptedBy, NULL, false ) );
	}

	public function getAcceptedBy() {
		return $this->m_intAcceptedBy;
	}

	public function sqlAcceptedBy() {
		return ( true == isset( $this->m_intAcceptedBy ) ) ? ( string ) $this->m_intAcceptedBy : 'NULL';
	}

	public function setAcceptedOn( $strAcceptedOn ) {
		$this->set( 'm_strAcceptedOn', CStrings::strTrimDef( $strAcceptedOn, -1, NULL, true ) );
	}

	public function getAcceptedOn() {
		return $this->m_strAcceptedOn;
	}

	public function sqlAcceptedOn() {
		return ( true == isset( $this->m_strAcceptedOn ) ) ? '\'' . $this->m_strAcceptedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setImportDataTypeId( $intImportDataTypeId ) {
		$this->set( 'm_intImportDataTypeId', CStrings::strToIntDef( $intImportDataTypeId, NULL, false ) );
	}

	public function getImportDataTypeId() {
		return $this->m_intImportDataTypeId;
	}

	public function sqlImportDataTypeId() {
		return ( true == isset( $this->m_intImportDataTypeId ) ) ? ( string ) $this->m_intImportDataTypeId : 'NULL';
	}

	public function setValue( $strValue ) {
		$this->set( 'm_strValue', CStrings::strTrimDef( $strValue, 50, NULL, true ) );
	}

	public function getValue() {
		return $this->m_strValue;
	}

	public function sqlValue() {
		return ( true == isset( $this->m_strValue ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strValue ) : '\'' . addslashes( $this->m_strValue ) . '\'' ) : 'NULL';
	}

	public function setFileName( $strFileName ) {
		$this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 255, NULL, true ) );
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function sqlFileName() {
		return ( true == isset( $this->m_strFileName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileName ) : '\'' . addslashes( $this->m_strFileName ) . '\'' ) : 'NULL';
	}

	public function setFileType( $strFileType ) {
		$this->set( 'm_strFileType', CStrings::strTrimDef( $strFileType, 255, NULL, true ) );
	}

	public function getFileType() {
		return $this->m_strFileType;
	}

	public function sqlFileType() {
		return ( true == isset( $this->m_strFileType ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFileType ) : '\'' . addslashes( $this->m_strFileType ) . '\'' ) : 'NULL';
	}

	public function setFileSize( $intFileSize ) {
		$this->set( 'm_intFileSize', CStrings::strToIntDef( $intFileSize, NULL, false ) );
	}

	public function getFileSize() {
		return $this->m_intFileSize;
	}

	public function sqlFileSize() {
		return ( true == isset( $this->m_intFileSize ) ) ? ( string ) $this->m_intFileSize : 'NULL';
	}

	public function setValidationStartDatetime( $strValidationStartDatetime ) {
		$this->set( 'm_strValidationStartDatetime', CStrings::strTrimDef( $strValidationStartDatetime, -1, NULL, true ) );
	}

	public function getValidationStartDatetime() {
		return $this->m_strValidationStartDatetime;
	}

	public function sqlValidationStartDatetime() {
		return ( true == isset( $this->m_strValidationStartDatetime ) ) ? '\'' . $this->m_strValidationStartDatetime . '\'' : 'NULL';
	}

	public function setValidationEndDatetime( $strValidationEndDatetime ) {
		$this->set( 'm_strValidationEndDatetime', CStrings::strTrimDef( $strValidationEndDatetime, -1, NULL, true ) );
	}

	public function getValidationEndDatetime() {
		return $this->m_strValidationEndDatetime;
	}

	public function sqlValidationEndDatetime() {
		return ( true == isset( $this->m_strValidationEndDatetime ) ) ? '\'' . $this->m_strValidationEndDatetime . '\'' : 'NULL';
	}

	public function setSyncStartDatetime( $strSyncStartDatetime ) {
		$this->set( 'm_strSyncStartDatetime', CStrings::strTrimDef( $strSyncStartDatetime, -1, NULL, true ) );
	}

	public function getSyncStartDatetime() {
		return $this->m_strSyncStartDatetime;
	}

	public function sqlSyncStartDatetime() {
		return ( true == isset( $this->m_strSyncStartDatetime ) ) ? '\'' . $this->m_strSyncStartDatetime . '\'' : 'NULL';
	}

	public function setSyncEndDatetime( $strSyncEndDatetime ) {
		$this->set( 'm_strSyncEndDatetime', CStrings::strTrimDef( $strSyncEndDatetime, -1, NULL, true ) );
	}

	public function getSyncEndDatetime() {
		return $this->m_strSyncEndDatetime;
	}

	public function sqlSyncEndDatetime() {
		return ( true == isset( $this->m_strSyncEndDatetime ) ) ? '\'' . $this->m_strSyncEndDatetime . '\'' : 'NULL';
	}

	public function setValidationErrorDescription( $jsonValidationErrorDescription ) {
		if( true == valObj( $jsonValidationErrorDescription, 'stdClass' ) ) {
			$this->set( 'm_jsonValidationErrorDescription', $jsonValidationErrorDescription );
		} elseif( true == valJsonString( $jsonValidationErrorDescription ) ) {
			$this->set( 'm_jsonValidationErrorDescription', CStrings::strToJson( $jsonValidationErrorDescription ) );
		} else {
			$this->set( 'm_jsonValidationErrorDescription', NULL );
		}
		unset( $this->m_strValidationErrorDescription );
	}

	public function getValidationErrorDescription() {
		if( true == isset( $this->m_strValidationErrorDescription ) ) {
			$this->m_jsonValidationErrorDescription = CStrings::strToJson( $this->m_strValidationErrorDescription );
			unset( $this->m_strValidationErrorDescription );
		}
		return $this->m_jsonValidationErrorDescription;
	}

	public function sqlValidationErrorDescription() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getValidationErrorDescription() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getValidationErrorDescription() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getValidationErrorDescription() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setSyncErrorDesciption( $jsonSyncErrorDesciption ) {
		if( true == valObj( $jsonSyncErrorDesciption, 'stdClass' ) ) {
			$this->set( 'm_jsonSyncErrorDesciption', $jsonSyncErrorDesciption );
		} elseif( true == valJsonString( $jsonSyncErrorDesciption ) ) {
			$this->set( 'm_jsonSyncErrorDesciption', CStrings::strToJson( $jsonSyncErrorDesciption ) );
		} else {
			$this->set( 'm_jsonSyncErrorDesciption', NULL );
		}
		unset( $this->m_strSyncErrorDesciption );
	}

	public function getSyncErrorDesciption() {
		if( true == isset( $this->m_strSyncErrorDesciption ) ) {
			$this->m_jsonSyncErrorDesciption = CStrings::strToJson( $this->m_strSyncErrorDesciption );
			unset( $this->m_strSyncErrorDesciption );
		}
		return $this->m_jsonSyncErrorDesciption;
	}

	public function sqlSyncErrorDesciption() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getSyncErrorDesciption() ) ) ) {
			return ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), CStrings::jsonToStrDef( $this->getSyncErrorDesciption() ) ) : '\'' . addslashes( CStrings::jsonToStrDef( $this->getSyncErrorDesciption() ) ) . '\'' );
		}
		return 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, import_request_id, property_id, destination_property_id, import_entity_id, import_type_id, import_status_type_id, error_messages, post_date, accepted_by, accepted_on, updated_by, updated_on, created_by, created_on, import_data_type_id, value, file_name, file_type, file_size, validation_start_datetime, validation_end_datetime, sync_start_datetime, sync_end_datetime, validation_error_description, sync_error_desciption, cid, details, deleted_by, deleted_on )
					VALUES ( ' .
		          $strId . ', ' .
		          $this->sqlImportRequestId() . ', ' .
		          $this->sqlPropertyId() . ', ' .
		          $this->sqlDestinationPropertyId() . ', ' .
		          $this->sqlImportEntityId() . ', ' .
		          $this->sqlImportTypeId() . ', ' .
		          $this->sqlImportStatusTypeId() . ', ' .
		          $this->sqlErrorMessages() . ', ' .
		          $this->sqlPostDate() . ', ' .
		          $this->sqlAcceptedBy() . ', ' .
		          $this->sqlAcceptedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlUpdatedOn() . ', ' .
		          ( int ) $intCurrentUserId . ', ' .
		          $this->sqlCreatedOn() . ', ' .
		          $this->sqlImportDataTypeId() . ', ' .
		          $this->sqlValue() . ', ' .
		          $this->sqlFileName() . ', ' .
		          $this->sqlFileType() . ', ' .
		          $this->sqlFileSize() . ', ' .
		          $this->sqlValidationStartDatetime() . ', ' .
		          $this->sqlValidationEndDatetime() . ', ' .
		          $this->sqlSyncStartDatetime() . ', ' .
		          $this->sqlSyncEndDatetime() . ', ' .
		          $this->sqlValidationErrorDescription() . ', ' .
		          $this->sqlSyncErrorDesciption() . ', ' .
		          $this->sqlCid() . ', ' .
		          $this->sqlDetails() . ', ' .
		          $this->sqlDeletedBy() . ', ' .
		          $this->sqlDeletedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_request_id = ' . $this->sqlImportRequestId(). ',' ; } elseif( true == array_key_exists( 'ImportRequestId', $this->getChangedColumns() ) ) { $strSql .= ' import_request_id = ' . $this->sqlImportRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' destination_property_id = ' . $this->sqlDestinationPropertyId(). ',' ; } elseif( true == array_key_exists( 'DestinationPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' destination_property_id = ' . $this->sqlDestinationPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_entity_id = ' . $this->sqlImportEntityId(). ',' ; } elseif( true == array_key_exists( 'ImportEntityId', $this->getChangedColumns() ) ) { $strSql .= ' import_entity_id = ' . $this->sqlImportEntityId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_type_id = ' . $this->sqlImportTypeId(). ',' ; } elseif( true == array_key_exists( 'ImportTypeId', $this->getChangedColumns() ) ) { $strSql .= ' import_type_id = ' . $this->sqlImportTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_status_type_id = ' . $this->sqlImportStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'ImportStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' import_status_type_id = ' . $this->sqlImportStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' error_messages = ' . $this->sqlErrorMessages(). ',' ; } elseif( true == array_key_exists( 'ErrorMessages', $this->getChangedColumns() ) ) { $strSql .= ' error_messages = ' . $this->sqlErrorMessages() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' post_date = ' . $this->sqlPostDate(). ',' ; } elseif( true == array_key_exists( 'PostDate', $this->getChangedColumns() ) ) { $strSql .= ' post_date = ' . $this->sqlPostDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy(). ',' ; } elseif( true == array_key_exists( 'AcceptedBy', $this->getChangedColumns() ) ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn(). ',' ; } elseif( true == array_key_exists( 'AcceptedOn', $this->getChangedColumns() ) ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_data_type_id = ' . $this->sqlImportDataTypeId(). ',' ; } elseif( true == array_key_exists( 'ImportDataTypeId', $this->getChangedColumns() ) ) { $strSql .= ' import_data_type_id = ' . $this->sqlImportDataTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value = ' . $this->sqlValue(). ',' ; } elseif( true == array_key_exists( 'Value', $this->getChangedColumns() ) ) { $strSql .= ' value = ' . $this->sqlValue() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_name = ' . $this->sqlFileName(). ',' ; } elseif( true == array_key_exists( 'FileName', $this->getChangedColumns() ) ) { $strSql .= ' file_name = ' . $this->sqlFileName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_type = ' . $this->sqlFileType(). ',' ; } elseif( true == array_key_exists( 'FileType', $this->getChangedColumns() ) ) { $strSql .= ' file_type = ' . $this->sqlFileType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' file_size = ' . $this->sqlFileSize(). ',' ; } elseif( true == array_key_exists( 'FileSize', $this->getChangedColumns() ) ) { $strSql .= ' file_size = ' . $this->sqlFileSize() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' validation_start_datetime = ' . $this->sqlValidationStartDatetime(). ',' ; } elseif( true == array_key_exists( 'ValidationStartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' validation_start_datetime = ' . $this->sqlValidationStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' validation_end_datetime = ' . $this->sqlValidationEndDatetime(). ',' ; } elseif( true == array_key_exists( 'ValidationEndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' validation_end_datetime = ' . $this->sqlValidationEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sync_start_datetime = ' . $this->sqlSyncStartDatetime(). ',' ; } elseif( true == array_key_exists( 'SyncStartDatetime', $this->getChangedColumns() ) ) { $strSql .= ' sync_start_datetime = ' . $this->sqlSyncStartDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sync_end_datetime = ' . $this->sqlSyncEndDatetime(). ',' ; } elseif( true == array_key_exists( 'SyncEndDatetime', $this->getChangedColumns() ) ) { $strSql .= ' sync_end_datetime = ' . $this->sqlSyncEndDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' validation_error_description = ' . $this->sqlValidationErrorDescription(). ',' ; } elseif( true == array_key_exists( 'ValidationErrorDescription', $this->getChangedColumns() ) ) { $strSql .= ' validation_error_description = ' . $this->sqlValidationErrorDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sync_error_desciption = ' . $this->sqlSyncErrorDesciption(). ',' ; } elseif( true == array_key_exists( 'SyncErrorDesciption', $this->getChangedColumns() ) ) { $strSql .= ' sync_error_desciption = ' . $this->sqlSyncErrorDesciption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }

		$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'import_request_id' => $this->getImportRequestId(),
			'property_id' => $this->getPropertyId(),
			'destination_property_id' => $this->getDestinationPropertyId(),
			'import_entity_id' => $this->getImportEntityId(),
			'import_type_id' => $this->getImportTypeId(),
			'import_status_type_id' => $this->getImportStatusTypeId(),
			'error_messages' => $this->getErrorMessages(),
			'post_date' => $this->getPostDate(),
			'accepted_by' => $this->getAcceptedBy(),
			'accepted_on' => $this->getAcceptedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'import_data_type_id' => $this->getImportDataTypeId(),
			'value' => $this->getValue(),
			'file_name' => $this->getFileName(),
			'file_type' => $this->getFileType(),
			'file_size' => $this->getFileSize(),
			'validation_start_datetime' => $this->getValidationStartDatetime(),
			'validation_end_datetime' => $this->getValidationEndDatetime(),
			'sync_start_datetime' => $this->getSyncStartDatetime(),
			'sync_end_datetime' => $this->getSyncEndDatetime(),
			'validation_error_description' => $this->getValidationErrorDescription(),
			'sync_error_desciption' => $this->getSyncErrorDesciption(),
			'cid' => $this->getCid(),
			'details' => $this->getDetails(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn()
		);
	}

}
?>