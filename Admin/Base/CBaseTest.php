<?php

class CBaseTest extends CEosSingularBase {

	const TABLE_NAME = 'public.tests';

	protected $m_intId;
	protected $m_intTestTypeId;
	protected $m_intTrainingSessionId;
	protected $m_intTestLevelTypeId;
	protected $m_strCountryCode;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strNotifyEmailAddress;
	protected $m_intMaxQuestions;
	protected $m_intMaxAttempts;
	protected $m_fltPassingPercentage;
	protected $m_intIsPublished;
	protected $m_intIsSendEmail;
	protected $m_intIsTrainerSurvey;
	protected $m_intIsMandatory;
	protected $m_intOrderNum;
	protected $m_strPublishedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_boolIsHideScore;

	public function __construct() {
		parent::__construct();

		$this->m_intTestTypeId = '1';
		$this->m_strCountryCode = 'US';
		$this->m_intMaxAttempts = '1';
		$this->m_intIsPublished = '1';
		$this->m_intIsSendEmail = '0';
		$this->m_intIsTrainerSurvey = '0';
		$this->m_intOrderNum = '0';
		$this->m_boolIsHideScore = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['test_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTestTypeId', trim( $arrValues['test_type_id'] ) ); elseif( isset( $arrValues['test_type_id'] ) ) $this->setTestTypeId( $arrValues['test_type_id'] );
		if( isset( $arrValues['training_session_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingSessionId', trim( $arrValues['training_session_id'] ) ); elseif( isset( $arrValues['training_session_id'] ) ) $this->setTrainingSessionId( $arrValues['training_session_id'] );
		if( isset( $arrValues['test_level_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTestLevelTypeId', trim( $arrValues['test_level_type_id'] ) ); elseif( isset( $arrValues['test_level_type_id'] ) ) $this->setTestLevelTypeId( $arrValues['test_level_type_id'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['notify_email_address'] ) && $boolDirectSet ) $this->set( 'm_strNotifyEmailAddress', trim( stripcslashes( $arrValues['notify_email_address'] ) ) ); elseif( isset( $arrValues['notify_email_address'] ) ) $this->setNotifyEmailAddress( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notify_email_address'] ) : $arrValues['notify_email_address'] );
		if( isset( $arrValues['max_questions'] ) && $boolDirectSet ) $this->set( 'm_intMaxQuestions', trim( $arrValues['max_questions'] ) ); elseif( isset( $arrValues['max_questions'] ) ) $this->setMaxQuestions( $arrValues['max_questions'] );
		if( isset( $arrValues['max_attempts'] ) && $boolDirectSet ) $this->set( 'm_intMaxAttempts', trim( $arrValues['max_attempts'] ) ); elseif( isset( $arrValues['max_attempts'] ) ) $this->setMaxAttempts( $arrValues['max_attempts'] );
		if( isset( $arrValues['passing_percentage'] ) && $boolDirectSet ) $this->set( 'm_fltPassingPercentage', trim( $arrValues['passing_percentage'] ) ); elseif( isset( $arrValues['passing_percentage'] ) ) $this->setPassingPercentage( $arrValues['passing_percentage'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_send_email'] ) && $boolDirectSet ) $this->set( 'm_intIsSendEmail', trim( $arrValues['is_send_email'] ) ); elseif( isset( $arrValues['is_send_email'] ) ) $this->setIsSendEmail( $arrValues['is_send_email'] );
		if( isset( $arrValues['is_trainer_survey'] ) && $boolDirectSet ) $this->set( 'm_intIsTrainerSurvey', trim( $arrValues['is_trainer_survey'] ) ); elseif( isset( $arrValues['is_trainer_survey'] ) ) $this->setIsTrainerSurvey( $arrValues['is_trainer_survey'] );
		if( isset( $arrValues['is_mandatory'] ) && $boolDirectSet ) $this->set( 'm_intIsMandatory', trim( $arrValues['is_mandatory'] ) ); elseif( isset( $arrValues['is_mandatory'] ) ) $this->setIsMandatory( $arrValues['is_mandatory'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['published_on'] ) && $boolDirectSet ) $this->set( 'm_strPublishedOn', trim( $arrValues['published_on'] ) ); elseif( isset( $arrValues['published_on'] ) ) $this->setPublishedOn( $arrValues['published_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['is_hide_score'] ) && $boolDirectSet ) $this->set( 'm_boolIsHideScore', trim( stripcslashes( $arrValues['is_hide_score'] ) ) ); elseif( isset( $arrValues['is_hide_score'] ) ) $this->setIsHideScore( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_hide_score'] ) : $arrValues['is_hide_score'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTestTypeId( $intTestTypeId ) {
		$this->set( 'm_intTestTypeId', CStrings::strToIntDef( $intTestTypeId, NULL, false ) );
	}

	public function getTestTypeId() {
		return $this->m_intTestTypeId;
	}

	public function sqlTestTypeId() {
		return ( true == isset( $this->m_intTestTypeId ) ) ? ( string ) $this->m_intTestTypeId : '1';
	}

	public function setTrainingSessionId( $intTrainingSessionId ) {
		$this->set( 'm_intTrainingSessionId', CStrings::strToIntDef( $intTrainingSessionId, NULL, false ) );
	}

	public function getTrainingSessionId() {
		return $this->m_intTrainingSessionId;
	}

	public function sqlTrainingSessionId() {
		return ( true == isset( $this->m_intTrainingSessionId ) ) ? ( string ) $this->m_intTrainingSessionId : 'NULL';
	}

	public function setTestLevelTypeId( $intTestLevelTypeId ) {
		$this->set( 'm_intTestLevelTypeId', CStrings::strToIntDef( $intTestLevelTypeId, NULL, false ) );
	}

	public function getTestLevelTypeId() {
		return $this->m_intTestLevelTypeId;
	}

	public function sqlTestLevelTypeId() {
		return ( true == isset( $this->m_intTestLevelTypeId ) ) ? ( string ) $this->m_intTestLevelTypeId : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : '\'US\'';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, -1, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setNotifyEmailAddress( $strNotifyEmailAddress ) {
		$this->set( 'm_strNotifyEmailAddress', CStrings::strTrimDef( $strNotifyEmailAddress, -1, NULL, true ) );
	}

	public function getNotifyEmailAddress() {
		return $this->m_strNotifyEmailAddress;
	}

	public function sqlNotifyEmailAddress() {
		return ( true == isset( $this->m_strNotifyEmailAddress ) ) ? '\'' . addslashes( $this->m_strNotifyEmailAddress ) . '\'' : 'NULL';
	}

	public function setMaxQuestions( $intMaxQuestions ) {
		$this->set( 'm_intMaxQuestions', CStrings::strToIntDef( $intMaxQuestions, NULL, false ) );
	}

	public function getMaxQuestions() {
		return $this->m_intMaxQuestions;
	}

	public function sqlMaxQuestions() {
		return ( true == isset( $this->m_intMaxQuestions ) ) ? ( string ) $this->m_intMaxQuestions : 'NULL';
	}

	public function setMaxAttempts( $intMaxAttempts ) {
		$this->set( 'm_intMaxAttempts', CStrings::strToIntDef( $intMaxAttempts, NULL, false ) );
	}

	public function getMaxAttempts() {
		return $this->m_intMaxAttempts;
	}

	public function sqlMaxAttempts() {
		return ( true == isset( $this->m_intMaxAttempts ) ) ? ( string ) $this->m_intMaxAttempts : '1';
	}

	public function setPassingPercentage( $fltPassingPercentage ) {
		$this->set( 'm_fltPassingPercentage', CStrings::strToFloatDef( $fltPassingPercentage, NULL, false, 2 ) );
	}

	public function getPassingPercentage() {
		return $this->m_fltPassingPercentage;
	}

	public function sqlPassingPercentage() {
		return ( true == isset( $this->m_fltPassingPercentage ) ) ? ( string ) $this->m_fltPassingPercentage : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setIsSendEmail( $intIsSendEmail ) {
		$this->set( 'm_intIsSendEmail', CStrings::strToIntDef( $intIsSendEmail, NULL, false ) );
	}

	public function getIsSendEmail() {
		return $this->m_intIsSendEmail;
	}

	public function sqlIsSendEmail() {
		return ( true == isset( $this->m_intIsSendEmail ) ) ? ( string ) $this->m_intIsSendEmail : '0';
	}

	public function setIsTrainerSurvey( $intIsTrainerSurvey ) {
		$this->set( 'm_intIsTrainerSurvey', CStrings::strToIntDef( $intIsTrainerSurvey, NULL, false ) );
	}

	public function getIsTrainerSurvey() {
		return $this->m_intIsTrainerSurvey;
	}

	public function sqlIsTrainerSurvey() {
		return ( true == isset( $this->m_intIsTrainerSurvey ) ) ? ( string ) $this->m_intIsTrainerSurvey : '0';
	}

	public function setIsMandatory( $intIsMandatory ) {
		$this->set( 'm_intIsMandatory', CStrings::strToIntDef( $intIsMandatory, NULL, false ) );
	}

	public function getIsMandatory() {
		return $this->m_intIsMandatory;
	}

	public function sqlIsMandatory() {
		return ( true == isset( $this->m_intIsMandatory ) ) ? ( string ) $this->m_intIsMandatory : 'NULL';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setPublishedOn( $strPublishedOn ) {
		$this->set( 'm_strPublishedOn', CStrings::strTrimDef( $strPublishedOn, -1, NULL, true ) );
	}

	public function getPublishedOn() {
		return $this->m_strPublishedOn;
	}

	public function sqlPublishedOn() {
		return ( true == isset( $this->m_strPublishedOn ) ) ? '\'' . $this->m_strPublishedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setIsHideScore( $boolIsHideScore ) {
		$this->set( 'm_boolIsHideScore', CStrings::strToBool( $boolIsHideScore ) );
	}

	public function getIsHideScore() {
		return $this->m_boolIsHideScore;
	}

	public function sqlIsHideScore() {
		return ( true == isset( $this->m_boolIsHideScore ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsHideScore ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, test_type_id, training_session_id, test_level_type_id, country_code, name, description, notify_email_address, max_questions, max_attempts, passing_percentage, is_published, is_send_email, is_trainer_survey, is_mandatory, order_num, published_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, is_hide_score )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlTestTypeId() . ', ' .
 						$this->sqlTrainingSessionId() . ', ' .
 						$this->sqlTestLevelTypeId() . ', ' .
 						$this->sqlCountryCode() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlNotifyEmailAddress() . ', ' .
 						$this->sqlMaxQuestions() . ', ' .
 						$this->sqlMaxAttempts() . ', ' .
 						$this->sqlPassingPercentage() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsSendEmail() . ', ' .
 						$this->sqlIsTrainerSurvey() . ', ' .
 						$this->sqlIsMandatory() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlPublishedOn() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ', ' .
 						$this->sqlIsHideScore() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_type_id = ' . $this->sqlTestTypeId() . ','; } elseif( true == array_key_exists( 'TestTypeId', $this->getChangedColumns() ) ) { $strSql .= ' test_type_id = ' . $this->sqlTestTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_session_id = ' . $this->sqlTrainingSessionId() . ','; } elseif( true == array_key_exists( 'TrainingSessionId', $this->getChangedColumns() ) ) { $strSql .= ' training_session_id = ' . $this->sqlTrainingSessionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_level_type_id = ' . $this->sqlTestLevelTypeId() . ','; } elseif( true == array_key_exists( 'TestLevelTypeId', $this->getChangedColumns() ) ) { $strSql .= ' test_level_type_id = ' . $this->sqlTestLevelTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notify_email_address = ' . $this->sqlNotifyEmailAddress() . ','; } elseif( true == array_key_exists( 'NotifyEmailAddress', $this->getChangedColumns() ) ) { $strSql .= ' notify_email_address = ' . $this->sqlNotifyEmailAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_questions = ' . $this->sqlMaxQuestions() . ','; } elseif( true == array_key_exists( 'MaxQuestions', $this->getChangedColumns() ) ) { $strSql .= ' max_questions = ' . $this->sqlMaxQuestions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' max_attempts = ' . $this->sqlMaxAttempts() . ','; } elseif( true == array_key_exists( 'MaxAttempts', $this->getChangedColumns() ) ) { $strSql .= ' max_attempts = ' . $this->sqlMaxAttempts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' passing_percentage = ' . $this->sqlPassingPercentage() . ','; } elseif( true == array_key_exists( 'PassingPercentage', $this->getChangedColumns() ) ) { $strSql .= ' passing_percentage = ' . $this->sqlPassingPercentage() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_send_email = ' . $this->sqlIsSendEmail() . ','; } elseif( true == array_key_exists( 'IsSendEmail', $this->getChangedColumns() ) ) { $strSql .= ' is_send_email = ' . $this->sqlIsSendEmail() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_trainer_survey = ' . $this->sqlIsTrainerSurvey() . ','; } elseif( true == array_key_exists( 'IsTrainerSurvey', $this->getChangedColumns() ) ) { $strSql .= ' is_trainer_survey = ' . $this->sqlIsTrainerSurvey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_mandatory = ' . $this->sqlIsMandatory() . ','; } elseif( true == array_key_exists( 'IsMandatory', $this->getChangedColumns() ) ) { $strSql .= ' is_mandatory = ' . $this->sqlIsMandatory() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn() . ','; } elseif( true == array_key_exists( 'PublishedOn', $this->getChangedColumns() ) ) { $strSql .= ' published_on = ' . $this->sqlPublishedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_hide_score = ' . $this->sqlIsHideScore() . ','; } elseif( true == array_key_exists( 'IsHideScore', $this->getChangedColumns() ) ) { $strSql .= ' is_hide_score = ' . $this->sqlIsHideScore() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'test_type_id' => $this->getTestTypeId(),
			'training_session_id' => $this->getTrainingSessionId(),
			'test_level_type_id' => $this->getTestLevelTypeId(),
			'country_code' => $this->getCountryCode(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'notify_email_address' => $this->getNotifyEmailAddress(),
			'max_questions' => $this->getMaxQuestions(),
			'max_attempts' => $this->getMaxAttempts(),
			'passing_percentage' => $this->getPassingPercentage(),
			'is_published' => $this->getIsPublished(),
			'is_send_email' => $this->getIsSendEmail(),
			'is_trainer_survey' => $this->getIsTrainerSurvey(),
			'is_mandatory' => $this->getIsMandatory(),
			'order_num' => $this->getOrderNum(),
			'published_on' => $this->getPublishedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'is_hide_score' => $this->getIsHideScore()
		);
	}

}
?>