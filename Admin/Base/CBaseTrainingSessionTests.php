<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingSessionTests
 * Do not add any new functions to this class.
 */

class CBaseTrainingSessionTests extends CEosPluralBase {

	/**
	 * @return CTrainingSessionTest[]
	 */
	public static function fetchTrainingSessionTests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTrainingSessionTest', $objDatabase );
	}

	/**
	 * @return CTrainingSessionTest
	 */
	public static function fetchTrainingSessionTest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTrainingSessionTest', $objDatabase );
	}

	public static function fetchTrainingSessionTestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'training_session_tests', $objDatabase );
	}

	public static function fetchTrainingSessionTestById( $intId, $objDatabase ) {
		return self::fetchTrainingSessionTest( sprintf( 'SELECT * FROM training_session_tests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTrainingSessionTestsByTrainingSessionId( $intTrainingSessionId, $objDatabase ) {
		return self::fetchTrainingSessionTests( sprintf( 'SELECT * FROM training_session_tests WHERE training_session_id = %d', ( int ) $intTrainingSessionId ), $objDatabase );
	}

	public static function fetchTrainingSessionTestsByTestId( $intTestId, $objDatabase ) {
		return self::fetchTrainingSessionTests( sprintf( 'SELECT * FROM training_session_tests WHERE test_id = %d', ( int ) $intTestId ), $objDatabase );
	}

}
?>