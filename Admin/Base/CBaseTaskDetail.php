<?php

class CBaseTaskDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.task_details';

	protected $m_intId;
	protected $m_intTaskId;
	protected $m_intDeveloperStoryPointBudgetId;
	protected $m_intQaStoryPointBudgetId;
	protected $m_intPenaltyStoryPointCommissionId;
	protected $m_strRootCauseTypeIds;
	protected $m_intDeveloperEmployeeId;
	protected $m_intQaEmployeeId;
	protected $m_intTaskCategoryId;
	protected $m_intTaskImpactTypeId;
	protected $m_intTeamBacklogId;
	protected $m_intTaskEstimationSizeTypeId;
	protected $m_fltDeveloperStoryPoints;
	protected $m_fltQaStoryPoints;
	protected $m_fltAuditStoryPoints;
	protected $m_fltSeverityCount;
	protected $m_intPenaltyStoryPoints;
	protected $m_intCausedBySvnRevision;
	protected $m_strCausedBySvnUsername;
	protected $m_strUseCase;
	protected $m_strTermDefinition;
	protected $m_strRelevantFact;
	protected $m_intIsDismissed;
	protected $m_intIsReleaseNoteApproved;
	protected $m_boolIsPciTask;
	protected $m_intTaskQuarterId;
	protected $m_intUnitCount;
	protected $m_intLaggingDaysCount;
	protected $m_intOccupantCount;
	protected $m_intReleaseNoteApprovedBy;
	protected $m_strReleaseNoteApprovedOn;
	protected $m_intCommitApprovedBy;
	protected $m_intStageCommitApprovedBy;
	protected $m_intSmsEnabled;
	protected $m_strSmsNumber;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intProjectId;
	protected $m_intStoryOrderNum;
	protected $m_boolIsUatCompleted;
	protected $m_intTaskUatStatusTypeId;

	public function __construct() {
		parent::__construct();

		$this->m_intTaskCategoryId = '0';
		$this->m_intTaskImpactTypeId = '1';
		$this->m_fltDeveloperStoryPoints = '0';
		$this->m_fltQaStoryPoints = '0';
		$this->m_fltSeverityCount = '0';
		$this->m_intPenaltyStoryPoints = '0';
		$this->m_intIsDismissed = '0';
		$this->m_intLaggingDaysCount = '0';
		$this->m_intSmsEnabled = ( 0 );
		$this->m_boolIsUatCompleted = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['developer_story_point_budget_id'] ) && $boolDirectSet ) $this->set( 'm_intDeveloperStoryPointBudgetId', trim( $arrValues['developer_story_point_budget_id'] ) ); elseif( isset( $arrValues['developer_story_point_budget_id'] ) ) $this->setDeveloperStoryPointBudgetId( $arrValues['developer_story_point_budget_id'] );
		if( isset( $arrValues['qa_story_point_budget_id'] ) && $boolDirectSet ) $this->set( 'm_intQaStoryPointBudgetId', trim( $arrValues['qa_story_point_budget_id'] ) ); elseif( isset( $arrValues['qa_story_point_budget_id'] ) ) $this->setQaStoryPointBudgetId( $arrValues['qa_story_point_budget_id'] );
		if( isset( $arrValues['penalty_story_point_commission_id'] ) && $boolDirectSet ) $this->set( 'm_intPenaltyStoryPointCommissionId', trim( $arrValues['penalty_story_point_commission_id'] ) ); elseif( isset( $arrValues['penalty_story_point_commission_id'] ) ) $this->setPenaltyStoryPointCommissionId( $arrValues['penalty_story_point_commission_id'] );
		if( isset( $arrValues['root_cause_type_ids'] ) && $boolDirectSet ) $this->set( 'm_strRootCauseTypeIds', trim( $arrValues['root_cause_type_ids'] ) ); elseif( isset( $arrValues['root_cause_type_ids'] ) ) $this->setRootCauseTypeIds( $arrValues['root_cause_type_ids'] );
		if( isset( $arrValues['developer_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intDeveloperEmployeeId', trim( $arrValues['developer_employee_id'] ) ); elseif( isset( $arrValues['developer_employee_id'] ) ) $this->setDeveloperEmployeeId( $arrValues['developer_employee_id'] );
		if( isset( $arrValues['qa_employee_id'] ) && $boolDirectSet ) $this->set( 'm_intQaEmployeeId', trim( $arrValues['qa_employee_id'] ) ); elseif( isset( $arrValues['qa_employee_id'] ) ) $this->setQaEmployeeId( $arrValues['qa_employee_id'] );
		if( isset( $arrValues['task_category_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskCategoryId', trim( $arrValues['task_category_id'] ) ); elseif( isset( $arrValues['task_category_id'] ) ) $this->setTaskCategoryId( $arrValues['task_category_id'] );
		if( isset( $arrValues['task_impact_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskImpactTypeId', trim( $arrValues['task_impact_type_id'] ) ); elseif( isset( $arrValues['task_impact_type_id'] ) ) $this->setTaskImpactTypeId( $arrValues['task_impact_type_id'] );
		if( isset( $arrValues['team_backlog_id'] ) && $boolDirectSet ) $this->set( 'm_intTeamBacklogId', trim( $arrValues['team_backlog_id'] ) ); elseif( isset( $arrValues['team_backlog_id'] ) ) $this->setTeamBacklogId( $arrValues['team_backlog_id'] );
		if( isset( $arrValues['task_estimation_size_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskEstimationSizeTypeId', trim( $arrValues['task_estimation_size_type_id'] ) ); elseif( isset( $arrValues['task_estimation_size_type_id'] ) ) $this->setTaskEstimationSizeTypeId( $arrValues['task_estimation_size_type_id'] );
		if( isset( $arrValues['developer_story_points'] ) && $boolDirectSet ) $this->set( 'm_fltDeveloperStoryPoints', trim( $arrValues['developer_story_points'] ) ); elseif( isset( $arrValues['developer_story_points'] ) ) $this->setDeveloperStoryPoints( $arrValues['developer_story_points'] );
		if( isset( $arrValues['qa_story_points'] ) && $boolDirectSet ) $this->set( 'm_fltQaStoryPoints', trim( $arrValues['qa_story_points'] ) ); elseif( isset( $arrValues['qa_story_points'] ) ) $this->setQaStoryPoints( $arrValues['qa_story_points'] );
		if( isset( $arrValues['audit_story_points'] ) && $boolDirectSet ) $this->set( 'm_fltAuditStoryPoints', trim( $arrValues['audit_story_points'] ) ); elseif( isset( $arrValues['audit_story_points'] ) ) $this->setAuditStoryPoints( $arrValues['audit_story_points'] );
		if( isset( $arrValues['severity_count'] ) && $boolDirectSet ) $this->set( 'm_fltSeverityCount', trim( $arrValues['severity_count'] ) ); elseif( isset( $arrValues['severity_count'] ) ) $this->setSeverityCount( $arrValues['severity_count'] );
		if( isset( $arrValues['penalty_story_points'] ) && $boolDirectSet ) $this->set( 'm_intPenaltyStoryPoints', trim( $arrValues['penalty_story_points'] ) ); elseif( isset( $arrValues['penalty_story_points'] ) ) $this->setPenaltyStoryPoints( $arrValues['penalty_story_points'] );
		if( isset( $arrValues['caused_by_svn_revision'] ) && $boolDirectSet ) $this->set( 'm_intCausedBySvnRevision', trim( $arrValues['caused_by_svn_revision'] ) ); elseif( isset( $arrValues['caused_by_svn_revision'] ) ) $this->setCausedBySvnRevision( $arrValues['caused_by_svn_revision'] );
		if( isset( $arrValues['caused_by_svn_username'] ) && $boolDirectSet ) $this->set( 'm_strCausedBySvnUsername', trim( $arrValues['caused_by_svn_username'] ) ); elseif( isset( $arrValues['caused_by_svn_username'] ) ) $this->setCausedBySvnUsername( $arrValues['caused_by_svn_username'] );
		if( isset( $arrValues['use_case'] ) && $boolDirectSet ) $this->set( 'm_strUseCase', trim( $arrValues['use_case'] ) ); elseif( isset( $arrValues['use_case'] ) ) $this->setUseCase( $arrValues['use_case'] );
		if( isset( $arrValues['term_definition'] ) && $boolDirectSet ) $this->set( 'm_strTermDefinition', trim( $arrValues['term_definition'] ) ); elseif( isset( $arrValues['term_definition'] ) ) $this->setTermDefinition( $arrValues['term_definition'] );
		if( isset( $arrValues['relevant_fact'] ) && $boolDirectSet ) $this->set( 'm_strRelevantFact', trim( $arrValues['relevant_fact'] ) ); elseif( isset( $arrValues['relevant_fact'] ) ) $this->setRelevantFact( $arrValues['relevant_fact'] );
		if( isset( $arrValues['is_dismissed'] ) && $boolDirectSet ) $this->set( 'm_intIsDismissed', trim( $arrValues['is_dismissed'] ) ); elseif( isset( $arrValues['is_dismissed'] ) ) $this->setIsDismissed( $arrValues['is_dismissed'] );
		if( isset( $arrValues['is_release_note_approved'] ) && $boolDirectSet ) $this->set( 'm_intIsReleaseNoteApproved', trim( $arrValues['is_release_note_approved'] ) ); elseif( isset( $arrValues['is_release_note_approved'] ) ) $this->setIsReleaseNoteApproved( $arrValues['is_release_note_approved'] );
		if( isset( $arrValues['is_pci_task'] ) && $boolDirectSet ) $this->set( 'm_boolIsPciTask', trim( stripcslashes( $arrValues['is_pci_task'] ) ) ); elseif( isset( $arrValues['is_pci_task'] ) ) $this->setIsPciTask( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_pci_task'] ) : $arrValues['is_pci_task'] );
		if( isset( $arrValues['task_quarter_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskQuarterId', trim( $arrValues['task_quarter_id'] ) ); elseif( isset( $arrValues['task_quarter_id'] ) ) $this->setTaskQuarterId( $arrValues['task_quarter_id'] );
		if( isset( $arrValues['unit_count'] ) && $boolDirectSet ) $this->set( 'm_intUnitCount', trim( $arrValues['unit_count'] ) ); elseif( isset( $arrValues['unit_count'] ) ) $this->setUnitCount( $arrValues['unit_count'] );
		if( isset( $arrValues['lagging_days_count'] ) && $boolDirectSet ) $this->set( 'm_intLaggingDaysCount', trim( $arrValues['lagging_days_count'] ) ); elseif( isset( $arrValues['lagging_days_count'] ) ) $this->setLaggingDaysCount( $arrValues['lagging_days_count'] );
		if( isset( $arrValues['occupant_count'] ) && $boolDirectSet ) $this->set( 'm_intOccupantCount', trim( $arrValues['occupant_count'] ) ); elseif( isset( $arrValues['occupant_count'] ) ) $this->setOccupantCount( $arrValues['occupant_count'] );
		if( isset( $arrValues['release_note_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intReleaseNoteApprovedBy', trim( $arrValues['release_note_approved_by'] ) ); elseif( isset( $arrValues['release_note_approved_by'] ) ) $this->setReleaseNoteApprovedBy( $arrValues['release_note_approved_by'] );
		if( isset( $arrValues['release_note_approved_on'] ) && $boolDirectSet ) $this->set( 'm_strReleaseNoteApprovedOn', trim( $arrValues['release_note_approved_on'] ) ); elseif( isset( $arrValues['release_note_approved_on'] ) ) $this->setReleaseNoteApprovedOn( $arrValues['release_note_approved_on'] );
		if( isset( $arrValues['commit_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intCommitApprovedBy', trim( $arrValues['commit_approved_by'] ) ); elseif( isset( $arrValues['commit_approved_by'] ) ) $this->setCommitApprovedBy( $arrValues['commit_approved_by'] );
		if( isset( $arrValues['stage_commit_approved_by'] ) && $boolDirectSet ) $this->set( 'm_intStageCommitApprovedBy', trim( $arrValues['stage_commit_approved_by'] ) ); elseif( isset( $arrValues['stage_commit_approved_by'] ) ) $this->setStageCommitApprovedBy( $arrValues['stage_commit_approved_by'] );
		if( isset( $arrValues['sms_enabled'] ) && $boolDirectSet ) $this->set( 'm_intSmsEnabled', trim( $arrValues['sms_enabled'] ) ); elseif( isset( $arrValues['sms_enabled'] ) ) $this->setSmsEnabled( $arrValues['sms_enabled'] );
		if( isset( $arrValues['sms_number'] ) && $boolDirectSet ) $this->set( 'm_strSmsNumber', trim( $arrValues['sms_number'] ) ); elseif( isset( $arrValues['sms_number'] ) ) $this->setSmsNumber( $arrValues['sms_number'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['project_id'] ) && $boolDirectSet ) $this->set( 'm_intProjectId', trim( $arrValues['project_id'] ) ); elseif( isset( $arrValues['project_id'] ) ) $this->setProjectId( $arrValues['project_id'] );
		if( isset( $arrValues['story_order_num'] ) && $boolDirectSet ) $this->set( 'm_intStoryOrderNum', trim( $arrValues['story_order_num'] ) ); elseif( isset( $arrValues['story_order_num'] ) ) $this->setStoryOrderNum( $arrValues['story_order_num'] );
		if( isset( $arrValues['is_uat_completed'] ) && $boolDirectSet ) $this->set( 'm_boolIsUatCompleted', trim( stripcslashes( $arrValues['is_uat_completed'] ) ) ); elseif( isset( $arrValues['is_uat_completed'] ) ) $this->setIsUatCompleted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_uat_completed'] ) : $arrValues['is_uat_completed'] );
		if( isset( $arrValues['task_uat_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskUatStatusTypeId', trim( $arrValues['task_uat_status_type_id'] ) ); elseif( isset( $arrValues['task_uat_status_type_id'] ) ) $this->setTaskUatStatusTypeId( $arrValues['task_uat_status_type_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setDeveloperStoryPointBudgetId( $intDeveloperStoryPointBudgetId ) {
		$this->set( 'm_intDeveloperStoryPointBudgetId', CStrings::strToIntDef( $intDeveloperStoryPointBudgetId, NULL, false ) );
	}

	public function getDeveloperStoryPointBudgetId() {
		return $this->m_intDeveloperStoryPointBudgetId;
	}

	public function sqlDeveloperStoryPointBudgetId() {
		return ( true == isset( $this->m_intDeveloperStoryPointBudgetId ) ) ? ( string ) $this->m_intDeveloperStoryPointBudgetId : 'NULL';
	}

	public function setQaStoryPointBudgetId( $intQaStoryPointBudgetId ) {
		$this->set( 'm_intQaStoryPointBudgetId', CStrings::strToIntDef( $intQaStoryPointBudgetId, NULL, false ) );
	}

	public function getQaStoryPointBudgetId() {
		return $this->m_intQaStoryPointBudgetId;
	}

	public function sqlQaStoryPointBudgetId() {
		return ( true == isset( $this->m_intQaStoryPointBudgetId ) ) ? ( string ) $this->m_intQaStoryPointBudgetId : 'NULL';
	}

	public function setPenaltyStoryPointCommissionId( $intPenaltyStoryPointCommissionId ) {
		$this->set( 'm_intPenaltyStoryPointCommissionId', CStrings::strToIntDef( $intPenaltyStoryPointCommissionId, NULL, false ) );
	}

	public function getPenaltyStoryPointCommissionId() {
		return $this->m_intPenaltyStoryPointCommissionId;
	}

	public function sqlPenaltyStoryPointCommissionId() {
		return ( true == isset( $this->m_intPenaltyStoryPointCommissionId ) ) ? ( string ) $this->m_intPenaltyStoryPointCommissionId : 'NULL';
	}

	public function setRootCauseTypeIds( $strRootCauseTypeIds ) {
		$this->set( 'm_strRootCauseTypeIds', CStrings::strTrimDef( $strRootCauseTypeIds, 100, NULL, true ) );
	}

	public function getRootCauseTypeIds() {
		return $this->m_strRootCauseTypeIds;
	}

	public function sqlRootCauseTypeIds() {
		return ( true == isset( $this->m_strRootCauseTypeIds ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRootCauseTypeIds ) : '\'' . addslashes( $this->m_strRootCauseTypeIds ) . '\'' ) : 'NULL';
	}

	public function setDeveloperEmployeeId( $intDeveloperEmployeeId ) {
		$this->set( 'm_intDeveloperEmployeeId', CStrings::strToIntDef( $intDeveloperEmployeeId, NULL, false ) );
	}

	public function getDeveloperEmployeeId() {
		return $this->m_intDeveloperEmployeeId;
	}

	public function sqlDeveloperEmployeeId() {
		return ( true == isset( $this->m_intDeveloperEmployeeId ) ) ? ( string ) $this->m_intDeveloperEmployeeId : 'NULL';
	}

	public function setQaEmployeeId( $intQaEmployeeId ) {
		$this->set( 'm_intQaEmployeeId', CStrings::strToIntDef( $intQaEmployeeId, NULL, false ) );
	}

	public function getQaEmployeeId() {
		return $this->m_intQaEmployeeId;
	}

	public function sqlQaEmployeeId() {
		return ( true == isset( $this->m_intQaEmployeeId ) ) ? ( string ) $this->m_intQaEmployeeId : 'NULL';
	}

	public function setTaskCategoryId( $intTaskCategoryId ) {
		$this->set( 'm_intTaskCategoryId', CStrings::strToIntDef( $intTaskCategoryId, NULL, false ) );
	}

	public function getTaskCategoryId() {
		return $this->m_intTaskCategoryId;
	}

	public function sqlTaskCategoryId() {
		return ( true == isset( $this->m_intTaskCategoryId ) ) ? ( string ) $this->m_intTaskCategoryId : '0';
	}

	public function setTaskImpactTypeId( $intTaskImpactTypeId ) {
		$this->set( 'm_intTaskImpactTypeId', CStrings::strToIntDef( $intTaskImpactTypeId, NULL, false ) );
	}

	public function getTaskImpactTypeId() {
		return $this->m_intTaskImpactTypeId;
	}

	public function sqlTaskImpactTypeId() {
		return ( true == isset( $this->m_intTaskImpactTypeId ) ) ? ( string ) $this->m_intTaskImpactTypeId : '1';
	}

	public function setTeamBacklogId( $intTeamBacklogId ) {
		$this->set( 'm_intTeamBacklogId', CStrings::strToIntDef( $intTeamBacklogId, NULL, false ) );
	}

	public function getTeamBacklogId() {
		return $this->m_intTeamBacklogId;
	}

	public function sqlTeamBacklogId() {
		return ( true == isset( $this->m_intTeamBacklogId ) ) ? ( string ) $this->m_intTeamBacklogId : 'NULL';
	}

	public function setTaskEstimationSizeTypeId( $intTaskEstimationSizeTypeId ) {
		$this->set( 'm_intTaskEstimationSizeTypeId', CStrings::strToIntDef( $intTaskEstimationSizeTypeId, NULL, false ) );
	}

	public function getTaskEstimationSizeTypeId() {
		return $this->m_intTaskEstimationSizeTypeId;
	}

	public function sqlTaskEstimationSizeTypeId() {
		return ( true == isset( $this->m_intTaskEstimationSizeTypeId ) ) ? ( string ) $this->m_intTaskEstimationSizeTypeId : 'NULL';
	}

	public function setDeveloperStoryPoints( $fltDeveloperStoryPoints ) {
		$this->set( 'm_fltDeveloperStoryPoints', CStrings::strToFloatDef( $fltDeveloperStoryPoints, NULL, false, 2 ) );
	}

	public function getDeveloperStoryPoints() {
		return $this->m_fltDeveloperStoryPoints;
	}

	public function sqlDeveloperStoryPoints() {
		return ( true == isset( $this->m_fltDeveloperStoryPoints ) ) ? ( string ) $this->m_fltDeveloperStoryPoints : '0';
	}

	public function setQaStoryPoints( $fltQaStoryPoints ) {
		$this->set( 'm_fltQaStoryPoints', CStrings::strToFloatDef( $fltQaStoryPoints, NULL, false, 2 ) );
	}

	public function getQaStoryPoints() {
		return $this->m_fltQaStoryPoints;
	}

	public function sqlQaStoryPoints() {
		return ( true == isset( $this->m_fltQaStoryPoints ) ) ? ( string ) $this->m_fltQaStoryPoints : '0';
	}

	public function setAuditStoryPoints( $fltAuditStoryPoints ) {
		$this->set( 'm_fltAuditStoryPoints', CStrings::strToFloatDef( $fltAuditStoryPoints, NULL, false, 2 ) );
	}

	public function getAuditStoryPoints() {
		return $this->m_fltAuditStoryPoints;
	}

	public function sqlAuditStoryPoints() {
		return ( true == isset( $this->m_fltAuditStoryPoints ) ) ? ( string ) $this->m_fltAuditStoryPoints : 'NULL';
	}

	public function setSeverityCount( $fltSeverityCount ) {
		$this->set( 'm_fltSeverityCount', CStrings::strToFloatDef( $fltSeverityCount, NULL, false, 2 ) );
	}

	public function getSeverityCount() {
		return $this->m_fltSeverityCount;
	}

	public function sqlSeverityCount() {
		return ( true == isset( $this->m_fltSeverityCount ) ) ? ( string ) $this->m_fltSeverityCount : '0';
	}

	public function setPenaltyStoryPoints( $intPenaltyStoryPoints ) {
		$this->set( 'm_intPenaltyStoryPoints', CStrings::strToIntDef( $intPenaltyStoryPoints, NULL, false ) );
	}

	public function getPenaltyStoryPoints() {
		return $this->m_intPenaltyStoryPoints;
	}

	public function sqlPenaltyStoryPoints() {
		return ( true == isset( $this->m_intPenaltyStoryPoints ) ) ? ( string ) $this->m_intPenaltyStoryPoints : '0';
	}

	public function setCausedBySvnRevision( $intCausedBySvnRevision ) {
		$this->set( 'm_intCausedBySvnRevision', CStrings::strToIntDef( $intCausedBySvnRevision, NULL, false ) );
	}

	public function getCausedBySvnRevision() {
		return $this->m_intCausedBySvnRevision;
	}

	public function sqlCausedBySvnRevision() {
		return ( true == isset( $this->m_intCausedBySvnRevision ) ) ? ( string ) $this->m_intCausedBySvnRevision : 'NULL';
	}

	public function setCausedBySvnUsername( $strCausedBySvnUsername ) {
		$this->set( 'm_strCausedBySvnUsername', CStrings::strTrimDef( $strCausedBySvnUsername, 240, NULL, true ) );
	}

	public function getCausedBySvnUsername() {
		return $this->m_strCausedBySvnUsername;
	}

	public function sqlCausedBySvnUsername() {
		return ( true == isset( $this->m_strCausedBySvnUsername ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strCausedBySvnUsername ) : '\'' . addslashes( $this->m_strCausedBySvnUsername ) . '\'' ) : 'NULL';
	}

	public function setUseCase( $strUseCase ) {
		$this->set( 'm_strUseCase', CStrings::strTrimDef( $strUseCase, -1, NULL, true ) );
	}

	public function getUseCase() {
		return $this->m_strUseCase;
	}

	public function sqlUseCase() {
		return ( true == isset( $this->m_strUseCase ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strUseCase ) : '\'' . addslashes( $this->m_strUseCase ) . '\'' ) : 'NULL';
	}

	public function setTermDefinition( $strTermDefinition ) {
		$this->set( 'm_strTermDefinition', CStrings::strTrimDef( $strTermDefinition, -1, NULL, true ) );
	}

	public function getTermDefinition() {
		return $this->m_strTermDefinition;
	}

	public function sqlTermDefinition() {
		return ( true == isset( $this->m_strTermDefinition ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strTermDefinition ) : '\'' . addslashes( $this->m_strTermDefinition ) . '\'' ) : 'NULL';
	}

	public function setRelevantFact( $strRelevantFact ) {
		$this->set( 'm_strRelevantFact', CStrings::strTrimDef( $strRelevantFact, -1, NULL, true ) );
	}

	public function getRelevantFact() {
		return $this->m_strRelevantFact;
	}

	public function sqlRelevantFact() {
		return ( true == isset( $this->m_strRelevantFact ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strRelevantFact ) : '\'' . addslashes( $this->m_strRelevantFact ) . '\'' ) : 'NULL';
	}

	public function setIsDismissed( $intIsDismissed ) {
		$this->set( 'm_intIsDismissed', CStrings::strToIntDef( $intIsDismissed, NULL, false ) );
	}

	public function getIsDismissed() {
		return $this->m_intIsDismissed;
	}

	public function sqlIsDismissed() {
		return ( true == isset( $this->m_intIsDismissed ) ) ? ( string ) $this->m_intIsDismissed : '0';
	}

	public function setIsReleaseNoteApproved( $intIsReleaseNoteApproved ) {
		$this->set( 'm_intIsReleaseNoteApproved', CStrings::strToIntDef( $intIsReleaseNoteApproved, NULL, false ) );
	}

	public function getIsReleaseNoteApproved() {
		return $this->m_intIsReleaseNoteApproved;
	}

	public function sqlIsReleaseNoteApproved() {
		return ( true == isset( $this->m_intIsReleaseNoteApproved ) ) ? ( string ) $this->m_intIsReleaseNoteApproved : 'NULL';
	}

	public function setIsPciTask( $boolIsPciTask ) {
		$this->set( 'm_boolIsPciTask', CStrings::strToBool( $boolIsPciTask ) );
	}

	public function getIsPciTask() {
		return $this->m_boolIsPciTask;
	}

	public function sqlIsPciTask() {
		return ( true == isset( $this->m_boolIsPciTask ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPciTask ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setTaskQuarterId( $intTaskQuarterId ) {
		$this->set( 'm_intTaskQuarterId', CStrings::strToIntDef( $intTaskQuarterId, NULL, false ) );
	}

	public function getTaskQuarterId() {
		return $this->m_intTaskQuarterId;
	}

	public function sqlTaskQuarterId() {
		return ( true == isset( $this->m_intTaskQuarterId ) ) ? ( string ) $this->m_intTaskQuarterId : 'NULL';
	}

	public function setUnitCount( $intUnitCount ) {
		$this->set( 'm_intUnitCount', CStrings::strToIntDef( $intUnitCount, NULL, false ) );
	}

	public function getUnitCount() {
		return $this->m_intUnitCount;
	}

	public function sqlUnitCount() {
		return ( true == isset( $this->m_intUnitCount ) ) ? ( string ) $this->m_intUnitCount : 'NULL';
	}

	public function setLaggingDaysCount( $intLaggingDaysCount ) {
		$this->set( 'm_intLaggingDaysCount', CStrings::strToIntDef( $intLaggingDaysCount, NULL, false ) );
	}

	public function getLaggingDaysCount() {
		return $this->m_intLaggingDaysCount;
	}

	public function sqlLaggingDaysCount() {
		return ( true == isset( $this->m_intLaggingDaysCount ) ) ? ( string ) $this->m_intLaggingDaysCount : '0';
	}

	public function setOccupantCount( $intOccupantCount ) {
		$this->set( 'm_intOccupantCount', CStrings::strToIntDef( $intOccupantCount, NULL, false ) );
	}

	public function getOccupantCount() {
		return $this->m_intOccupantCount;
	}

	public function sqlOccupantCount() {
		return ( true == isset( $this->m_intOccupantCount ) ) ? ( string ) $this->m_intOccupantCount : 'NULL';
	}

	public function setReleaseNoteApprovedBy( $intReleaseNoteApprovedBy ) {
		$this->set( 'm_intReleaseNoteApprovedBy', CStrings::strToIntDef( $intReleaseNoteApprovedBy, NULL, false ) );
	}

	public function getReleaseNoteApprovedBy() {
		return $this->m_intReleaseNoteApprovedBy;
	}

	public function sqlReleaseNoteApprovedBy() {
		return ( true == isset( $this->m_intReleaseNoteApprovedBy ) ) ? ( string ) $this->m_intReleaseNoteApprovedBy : 'NULL';
	}

	public function setReleaseNoteApprovedOn( $strReleaseNoteApprovedOn ) {
		$this->set( 'm_strReleaseNoteApprovedOn', CStrings::strTrimDef( $strReleaseNoteApprovedOn, -1, NULL, true ) );
	}

	public function getReleaseNoteApprovedOn() {
		return $this->m_strReleaseNoteApprovedOn;
	}

	public function sqlReleaseNoteApprovedOn() {
		return ( true == isset( $this->m_strReleaseNoteApprovedOn ) ) ? '\'' . $this->m_strReleaseNoteApprovedOn . '\'' : 'NULL';
	}

	public function setCommitApprovedBy( $intCommitApprovedBy ) {
		$this->set( 'm_intCommitApprovedBy', CStrings::strToIntDef( $intCommitApprovedBy, NULL, false ) );
	}

	public function getCommitApprovedBy() {
		return $this->m_intCommitApprovedBy;
	}

	public function sqlCommitApprovedBy() {
		return ( true == isset( $this->m_intCommitApprovedBy ) ) ? ( string ) $this->m_intCommitApprovedBy : 'NULL';
	}

	public function setStageCommitApprovedBy( $intStageCommitApprovedBy ) {
		$this->set( 'm_intStageCommitApprovedBy', CStrings::strToIntDef( $intStageCommitApprovedBy, NULL, false ) );
	}

	public function getStageCommitApprovedBy() {
		return $this->m_intStageCommitApprovedBy;
	}

	public function sqlStageCommitApprovedBy() {
		return ( true == isset( $this->m_intStageCommitApprovedBy ) ) ? ( string ) $this->m_intStageCommitApprovedBy : 'NULL';
	}

	public function setSmsEnabled( $intSmsEnabled ) {
		$this->set( 'm_intSmsEnabled', CStrings::strToIntDef( $intSmsEnabled, NULL, false ) );
	}

	public function getSmsEnabled() {
		return $this->m_intSmsEnabled;
	}

	public function sqlSmsEnabled() {
		return ( true == isset( $this->m_intSmsEnabled ) ) ? ( string ) $this->m_intSmsEnabled : 'NULL';
	}

	public function setSmsNumber( $strSmsNumber ) {
		$this->set( 'm_strSmsNumber', CStrings::strTrimDef( $strSmsNumber, 30, NULL, true ) );
	}

	public function getSmsNumber() {
		return $this->m_strSmsNumber;
	}

	public function sqlSmsNumber() {
		return ( true == isset( $this->m_strSmsNumber ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strSmsNumber ) : '\'' . addslashes( $this->m_strSmsNumber ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setProjectId( $intProjectId ) {
		$this->set( 'm_intProjectId', CStrings::strToIntDef( $intProjectId, NULL, false ) );
	}

	public function getProjectId() {
		return $this->m_intProjectId;
	}

	public function sqlProjectId() {
		return ( true == isset( $this->m_intProjectId ) ) ? ( string ) $this->m_intProjectId : 'NULL';
	}

	public function setStoryOrderNum( $intStoryOrderNum ) {
		$this->set( 'm_intStoryOrderNum', CStrings::strToIntDef( $intStoryOrderNum, NULL, false ) );
	}

	public function getStoryOrderNum() {
		return $this->m_intStoryOrderNum;
	}

	public function sqlStoryOrderNum() {
		return ( true == isset( $this->m_intStoryOrderNum ) ) ? ( string ) $this->m_intStoryOrderNum : 'NULL';
	}

	public function setIsUatCompleted( $boolIsUatCompleted ) {
		$this->set( 'm_boolIsUatCompleted', CStrings::strToBool( $boolIsUatCompleted ) );
	}

	public function getIsUatCompleted() {
		return $this->m_boolIsUatCompleted;
	}

	public function sqlIsUatCompleted() {
		return ( true == isset( $this->m_boolIsUatCompleted ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsUatCompleted ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setTaskUatStatusTypeId( $intTaskUatStatusTypeId ) {
		$this->set( 'm_intTaskUatStatusTypeId', CStrings::strToIntDef( $intTaskUatStatusTypeId, NULL, false ) );
	}

	public function getTaskUatStatusTypeId() {
		return $this->m_intTaskUatStatusTypeId;
	}

	public function sqlTaskUatStatusTypeId() {
		return ( true == isset( $this->m_intTaskUatStatusTypeId ) ) ? ( string ) $this->m_intTaskUatStatusTypeId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, task_id, developer_story_point_budget_id, qa_story_point_budget_id, penalty_story_point_commission_id, root_cause_type_ids, developer_employee_id, qa_employee_id, task_category_id, task_impact_type_id, team_backlog_id, task_estimation_size_type_id, developer_story_points, qa_story_points, audit_story_points, severity_count, penalty_story_points, caused_by_svn_revision, caused_by_svn_username, use_case, term_definition, relevant_fact, is_dismissed, is_release_note_approved, is_pci_task, task_quarter_id, unit_count, lagging_days_count, occupant_count, release_note_approved_by, release_note_approved_on, commit_approved_by, stage_commit_approved_by, sms_enabled, sms_number, updated_by, updated_on, created_by, created_on, project_id, story_order_num, is_uat_completed, task_uat_status_type_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlTaskId() . ', ' .
						$this->sqlDeveloperStoryPointBudgetId() . ', ' .
						$this->sqlQaStoryPointBudgetId() . ', ' .
						$this->sqlPenaltyStoryPointCommissionId() . ', ' .
						$this->sqlRootCauseTypeIds() . ', ' .
						$this->sqlDeveloperEmployeeId() . ', ' .
						$this->sqlQaEmployeeId() . ', ' .
						$this->sqlTaskCategoryId() . ', ' .
						$this->sqlTaskImpactTypeId() . ', ' .
						$this->sqlTeamBacklogId() . ', ' .
						$this->sqlTaskEstimationSizeTypeId() . ', ' .
						$this->sqlDeveloperStoryPoints() . ', ' .
						$this->sqlQaStoryPoints() . ', ' .
						$this->sqlAuditStoryPoints() . ', ' .
						$this->sqlSeverityCount() . ', ' .
						$this->sqlPenaltyStoryPoints() . ', ' .
						$this->sqlCausedBySvnRevision() . ', ' .
						$this->sqlCausedBySvnUsername() . ', ' .
						$this->sqlUseCase() . ', ' .
						$this->sqlTermDefinition() . ', ' .
						$this->sqlRelevantFact() . ', ' .
						$this->sqlIsDismissed() . ', ' .
						$this->sqlIsReleaseNoteApproved() . ', ' .
						$this->sqlIsPciTask() . ', ' .
						$this->sqlTaskQuarterId() . ', ' .
						$this->sqlUnitCount() . ', ' .
						$this->sqlLaggingDaysCount() . ', ' .
						$this->sqlOccupantCount() . ', ' .
						$this->sqlReleaseNoteApprovedBy() . ', ' .
						$this->sqlReleaseNoteApprovedOn() . ', ' .
						$this->sqlCommitApprovedBy() . ', ' .
						$this->sqlStageCommitApprovedBy() . ', ' .
						$this->sqlSmsEnabled() . ', ' .
						$this->sqlSmsNumber() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlProjectId() . ', ' .
						$this->sqlStoryOrderNum() . ', ' .
						$this->sqlIsUatCompleted() . ', ' .
						$this->sqlTaskUatStatusTypeId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId(). ',' ; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' developer_story_point_budget_id = ' . $this->sqlDeveloperStoryPointBudgetId(). ',' ; } elseif( true == array_key_exists( 'DeveloperStoryPointBudgetId', $this->getChangedColumns() ) ) { $strSql .= ' developer_story_point_budget_id = ' . $this->sqlDeveloperStoryPointBudgetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_story_point_budget_id = ' . $this->sqlQaStoryPointBudgetId(). ',' ; } elseif( true == array_key_exists( 'QaStoryPointBudgetId', $this->getChangedColumns() ) ) { $strSql .= ' qa_story_point_budget_id = ' . $this->sqlQaStoryPointBudgetId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' penalty_story_point_commission_id = ' . $this->sqlPenaltyStoryPointCommissionId(). ',' ; } elseif( true == array_key_exists( 'PenaltyStoryPointCommissionId', $this->getChangedColumns() ) ) { $strSql .= ' penalty_story_point_commission_id = ' . $this->sqlPenaltyStoryPointCommissionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' root_cause_type_ids = ' . $this->sqlRootCauseTypeIds(). ',' ; } elseif( true == array_key_exists( 'RootCauseTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' root_cause_type_ids = ' . $this->sqlRootCauseTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' developer_employee_id = ' . $this->sqlDeveloperEmployeeId(). ',' ; } elseif( true == array_key_exists( 'DeveloperEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' developer_employee_id = ' . $this->sqlDeveloperEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId(). ',' ; } elseif( true == array_key_exists( 'QaEmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' qa_employee_id = ' . $this->sqlQaEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_category_id = ' . $this->sqlTaskCategoryId(). ',' ; } elseif( true == array_key_exists( 'TaskCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' task_category_id = ' . $this->sqlTaskCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_impact_type_id = ' . $this->sqlTaskImpactTypeId(). ',' ; } elseif( true == array_key_exists( 'TaskImpactTypeId', $this->getChangedColumns() ) ) { $strSql .= ' task_impact_type_id = ' . $this->sqlTaskImpactTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' team_backlog_id = ' . $this->sqlTeamBacklogId(). ',' ; } elseif( true == array_key_exists( 'TeamBacklogId', $this->getChangedColumns() ) ) { $strSql .= ' team_backlog_id = ' . $this->sqlTeamBacklogId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_estimation_size_type_id = ' . $this->sqlTaskEstimationSizeTypeId(). ',' ; } elseif( true == array_key_exists( 'TaskEstimationSizeTypeId', $this->getChangedColumns() ) ) { $strSql .= ' task_estimation_size_type_id = ' . $this->sqlTaskEstimationSizeTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' developer_story_points = ' . $this->sqlDeveloperStoryPoints(). ',' ; } elseif( true == array_key_exists( 'DeveloperStoryPoints', $this->getChangedColumns() ) ) { $strSql .= ' developer_story_points = ' . $this->sqlDeveloperStoryPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_story_points = ' . $this->sqlQaStoryPoints(). ',' ; } elseif( true == array_key_exists( 'QaStoryPoints', $this->getChangedColumns() ) ) { $strSql .= ' qa_story_points = ' . $this->sqlQaStoryPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' audit_story_points = ' . $this->sqlAuditStoryPoints(). ',' ; } elseif( true == array_key_exists( 'AuditStoryPoints', $this->getChangedColumns() ) ) { $strSql .= ' audit_story_points = ' . $this->sqlAuditStoryPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' severity_count = ' . $this->sqlSeverityCount(). ',' ; } elseif( true == array_key_exists( 'SeverityCount', $this->getChangedColumns() ) ) { $strSql .= ' severity_count = ' . $this->sqlSeverityCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' penalty_story_points = ' . $this->sqlPenaltyStoryPoints(). ',' ; } elseif( true == array_key_exists( 'PenaltyStoryPoints', $this->getChangedColumns() ) ) { $strSql .= ' penalty_story_points = ' . $this->sqlPenaltyStoryPoints() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' caused_by_svn_revision = ' . $this->sqlCausedBySvnRevision(). ',' ; } elseif( true == array_key_exists( 'CausedBySvnRevision', $this->getChangedColumns() ) ) { $strSql .= ' caused_by_svn_revision = ' . $this->sqlCausedBySvnRevision() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' caused_by_svn_username = ' . $this->sqlCausedBySvnUsername(). ',' ; } elseif( true == array_key_exists( 'CausedBySvnUsername', $this->getChangedColumns() ) ) { $strSql .= ' caused_by_svn_username = ' . $this->sqlCausedBySvnUsername() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' use_case = ' . $this->sqlUseCase(). ',' ; } elseif( true == array_key_exists( 'UseCase', $this->getChangedColumns() ) ) { $strSql .= ' use_case = ' . $this->sqlUseCase() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' term_definition = ' . $this->sqlTermDefinition(). ',' ; } elseif( true == array_key_exists( 'TermDefinition', $this->getChangedColumns() ) ) { $strSql .= ' term_definition = ' . $this->sqlTermDefinition() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' relevant_fact = ' . $this->sqlRelevantFact(). ',' ; } elseif( true == array_key_exists( 'RelevantFact', $this->getChangedColumns() ) ) { $strSql .= ' relevant_fact = ' . $this->sqlRelevantFact() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_dismissed = ' . $this->sqlIsDismissed(). ',' ; } elseif( true == array_key_exists( 'IsDismissed', $this->getChangedColumns() ) ) { $strSql .= ' is_dismissed = ' . $this->sqlIsDismissed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_release_note_approved = ' . $this->sqlIsReleaseNoteApproved(). ',' ; } elseif( true == array_key_exists( 'IsReleaseNoteApproved', $this->getChangedColumns() ) ) { $strSql .= ' is_release_note_approved = ' . $this->sqlIsReleaseNoteApproved() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_pci_task = ' . $this->sqlIsPciTask(). ',' ; } elseif( true == array_key_exists( 'IsPciTask', $this->getChangedColumns() ) ) { $strSql .= ' is_pci_task = ' . $this->sqlIsPciTask() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_quarter_id = ' . $this->sqlTaskQuarterId(). ',' ; } elseif( true == array_key_exists( 'TaskQuarterId', $this->getChangedColumns() ) ) { $strSql .= ' task_quarter_id = ' . $this->sqlTaskQuarterId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount(). ',' ; } elseif( true == array_key_exists( 'UnitCount', $this->getChangedColumns() ) ) { $strSql .= ' unit_count = ' . $this->sqlUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' lagging_days_count = ' . $this->sqlLaggingDaysCount(). ',' ; } elseif( true == array_key_exists( 'LaggingDaysCount', $this->getChangedColumns() ) ) { $strSql .= ' lagging_days_count = ' . $this->sqlLaggingDaysCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' occupant_count = ' . $this->sqlOccupantCount(). ',' ; } elseif( true == array_key_exists( 'OccupantCount', $this->getChangedColumns() ) ) { $strSql .= ' occupant_count = ' . $this->sqlOccupantCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_note_approved_by = ' . $this->sqlReleaseNoteApprovedBy(). ',' ; } elseif( true == array_key_exists( 'ReleaseNoteApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' release_note_approved_by = ' . $this->sqlReleaseNoteApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' release_note_approved_on = ' . $this->sqlReleaseNoteApprovedOn(). ',' ; } elseif( true == array_key_exists( 'ReleaseNoteApprovedOn', $this->getChangedColumns() ) ) { $strSql .= ' release_note_approved_on = ' . $this->sqlReleaseNoteApprovedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' commit_approved_by = ' . $this->sqlCommitApprovedBy(). ',' ; } elseif( true == array_key_exists( 'CommitApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' commit_approved_by = ' . $this->sqlCommitApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' stage_commit_approved_by = ' . $this->sqlStageCommitApprovedBy(). ',' ; } elseif( true == array_key_exists( 'StageCommitApprovedBy', $this->getChangedColumns() ) ) { $strSql .= ' stage_commit_approved_by = ' . $this->sqlStageCommitApprovedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sms_enabled = ' . $this->sqlSmsEnabled(). ',' ; } elseif( true == array_key_exists( 'SmsEnabled', $this->getChangedColumns() ) ) { $strSql .= ' sms_enabled = ' . $this->sqlSmsEnabled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sms_number = ' . $this->sqlSmsNumber(). ',' ; } elseif( true == array_key_exists( 'SmsNumber', $this->getChangedColumns() ) ) { $strSql .= ' sms_number = ' . $this->sqlSmsNumber() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_id = ' . $this->sqlProjectId(). ',' ; } elseif( true == array_key_exists( 'ProjectId', $this->getChangedColumns() ) ) { $strSql .= ' project_id = ' . $this->sqlProjectId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' story_order_num = ' . $this->sqlStoryOrderNum(). ',' ; } elseif( true == array_key_exists( 'StoryOrderNum', $this->getChangedColumns() ) ) { $strSql .= ' story_order_num = ' . $this->sqlStoryOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_uat_completed = ' . $this->sqlIsUatCompleted(). ',' ; } elseif( true == array_key_exists( 'IsUatCompleted', $this->getChangedColumns() ) ) { $strSql .= ' is_uat_completed = ' . $this->sqlIsUatCompleted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_uat_status_type_id = ' . $this->sqlTaskUatStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'TaskUatStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' task_uat_status_type_id = ' . $this->sqlTaskUatStatusTypeId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'task_id' => $this->getTaskId(),
			'developer_story_point_budget_id' => $this->getDeveloperStoryPointBudgetId(),
			'qa_story_point_budget_id' => $this->getQaStoryPointBudgetId(),
			'penalty_story_point_commission_id' => $this->getPenaltyStoryPointCommissionId(),
			'root_cause_type_ids' => $this->getRootCauseTypeIds(),
			'developer_employee_id' => $this->getDeveloperEmployeeId(),
			'qa_employee_id' => $this->getQaEmployeeId(),
			'task_category_id' => $this->getTaskCategoryId(),
			'task_impact_type_id' => $this->getTaskImpactTypeId(),
			'team_backlog_id' => $this->getTeamBacklogId(),
			'task_estimation_size_type_id' => $this->getTaskEstimationSizeTypeId(),
			'developer_story_points' => $this->getDeveloperStoryPoints(),
			'qa_story_points' => $this->getQaStoryPoints(),
			'audit_story_points' => $this->getAuditStoryPoints(),
			'severity_count' => $this->getSeverityCount(),
			'penalty_story_points' => $this->getPenaltyStoryPoints(),
			'caused_by_svn_revision' => $this->getCausedBySvnRevision(),
			'caused_by_svn_username' => $this->getCausedBySvnUsername(),
			'use_case' => $this->getUseCase(),
			'term_definition' => $this->getTermDefinition(),
			'relevant_fact' => $this->getRelevantFact(),
			'is_dismissed' => $this->getIsDismissed(),
			'is_release_note_approved' => $this->getIsReleaseNoteApproved(),
			'is_pci_task' => $this->getIsPciTask(),
			'task_quarter_id' => $this->getTaskQuarterId(),
			'unit_count' => $this->getUnitCount(),
			'lagging_days_count' => $this->getLaggingDaysCount(),
			'occupant_count' => $this->getOccupantCount(),
			'release_note_approved_by' => $this->getReleaseNoteApprovedBy(),
			'release_note_approved_on' => $this->getReleaseNoteApprovedOn(),
			'commit_approved_by' => $this->getCommitApprovedBy(),
			'stage_commit_approved_by' => $this->getStageCommitApprovedBy(),
			'sms_enabled' => $this->getSmsEnabled(),
			'sms_number' => $this->getSmsNumber(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'project_id' => $this->getProjectId(),
			'story_order_num' => $this->getStoryOrderNum(),
			'is_uat_completed' => $this->getIsUatCompleted(),
			'task_uat_status_type_id' => $this->getTaskUatStatusTypeId()
		);
	}

}
?>