<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\COfficeRooms
 * Do not add any new functions to this class.
 */

class CBaseOfficeRooms extends CEosPluralBase {

	/**
	 * @return COfficeRoom[]
	 */
	public static function fetchOfficeRooms( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'COfficeRoom', $objDatabase );
	}

	/**
	 * @return COfficeRoom
	 */
	public static function fetchOfficeRoom( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'COfficeRoom', $objDatabase );
	}

	public static function fetchOfficeRoomCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'office_rooms', $objDatabase );
	}

	public static function fetchOfficeRoomById( $intId, $objDatabase ) {
		return self::fetchOfficeRoom( sprintf( 'SELECT * FROM office_rooms WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchOfficeRoomsByOfficeId( $intOfficeId, $objDatabase ) {
		return self::fetchOfficeRooms( sprintf( 'SELECT * FROM office_rooms WHERE office_id = %d', ( int ) $intOfficeId ), $objDatabase );
	}

}
?>