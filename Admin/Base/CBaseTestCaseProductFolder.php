<?php

class CBaseTestCaseProductFolder extends CEosSingularBase {

	const TABLE_NAME = 'public.test_case_product_folders';

	protected $m_intId;
	protected $m_intTestCaseProductFolderId;
	protected $m_intPsProductId;
	protected $m_intPsProductOptionId;
	protected $m_strName;
	protected $m_intIsCustom;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intOldFolderId;

	public function __construct() {
		parent::__construct();

		$this->m_intIsCustom = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['test_case_product_folder_id'] ) && $boolDirectSet ) $this->set( 'm_intTestCaseProductFolderId', trim( $arrValues['test_case_product_folder_id'] ) ); elseif( isset( $arrValues['test_case_product_folder_id'] ) ) $this->setTestCaseProductFolderId( $arrValues['test_case_product_folder_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['ps_product_option_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductOptionId', trim( $arrValues['ps_product_option_id'] ) ); elseif( isset( $arrValues['ps_product_option_id'] ) ) $this->setPsProductOptionId( $arrValues['ps_product_option_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( $arrValues['name'] ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( $arrValues['name'] );
		if( isset( $arrValues['is_custom'] ) && $boolDirectSet ) $this->set( 'm_intIsCustom', trim( $arrValues['is_custom'] ) ); elseif( isset( $arrValues['is_custom'] ) ) $this->setIsCustom( $arrValues['is_custom'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['old_folder_id'] ) && $boolDirectSet ) $this->set( 'm_intOldFolderId', trim( $arrValues['old_folder_id'] ) ); elseif( isset( $arrValues['old_folder_id'] ) ) $this->setOldFolderId( $arrValues['old_folder_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setTestCaseProductFolderId( $intTestCaseProductFolderId ) {
		$this->set( 'm_intTestCaseProductFolderId', CStrings::strToIntDef( $intTestCaseProductFolderId, NULL, false ) );
	}

	public function getTestCaseProductFolderId() {
		return $this->m_intTestCaseProductFolderId;
	}

	public function sqlTestCaseProductFolderId() {
		return ( true == isset( $this->m_intTestCaseProductFolderId ) ) ? ( string ) $this->m_intTestCaseProductFolderId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setPsProductOptionId( $intPsProductOptionId ) {
		$this->set( 'm_intPsProductOptionId', CStrings::strToIntDef( $intPsProductOptionId, NULL, false ) );
	}

	public function getPsProductOptionId() {
		return $this->m_intPsProductOptionId;
	}

	public function sqlPsProductOptionId() {
		return ( true == isset( $this->m_intPsProductOptionId ) ) ? ( string ) $this->m_intPsProductOptionId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 100, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strName ) : '\'' . addslashes( $this->m_strName ) . '\'' ) : 'NULL';
	}

	public function setIsCustom( $intIsCustom ) {
		$this->set( 'm_intIsCustom', CStrings::strToIntDef( $intIsCustom, NULL, false ) );
	}

	public function getIsCustom() {
		return $this->m_intIsCustom;
	}

	public function sqlIsCustom() {
		return ( true == isset( $this->m_intIsCustom ) ) ? ( string ) $this->m_intIsCustom : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setOldFolderId( $intOldFolderId ) {
		$this->set( 'm_intOldFolderId', CStrings::strToIntDef( $intOldFolderId, NULL, false ) );
	}

	public function getOldFolderId() {
		return $this->m_intOldFolderId;
	}

	public function sqlOldFolderId() {
		return ( true == isset( $this->m_intOldFolderId ) ) ? ( string ) $this->m_intOldFolderId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, test_case_product_folder_id, ps_product_id, ps_product_option_id, name, is_custom, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, old_folder_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlTestCaseProductFolderId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlPsProductOptionId() . ', ' .
						$this->sqlName() . ', ' .
						$this->sqlIsCustom() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlOldFolderId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' test_case_product_folder_id = ' . $this->sqlTestCaseProductFolderId(). ',' ; } elseif( true == array_key_exists( 'TestCaseProductFolderId', $this->getChangedColumns() ) ) { $strSql .= ' test_case_product_folder_id = ' . $this->sqlTestCaseProductFolderId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId(). ',' ; } elseif( true == array_key_exists( 'PsProductOptionId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_option_id = ' . $this->sqlPsProductOptionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName(). ',' ; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_custom = ' . $this->sqlIsCustom(). ',' ; } elseif( true == array_key_exists( 'IsCustom', $this->getChangedColumns() ) ) { $strSql .= ' is_custom = ' . $this->sqlIsCustom() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_folder_id = ' . $this->sqlOldFolderId(). ',' ; } elseif( true == array_key_exists( 'OldFolderId', $this->getChangedColumns() ) ) { $strSql .= ' old_folder_id = ' . $this->sqlOldFolderId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'test_case_product_folder_id' => $this->getTestCaseProductFolderId(),
			'ps_product_id' => $this->getPsProductId(),
			'ps_product_option_id' => $this->getPsProductOptionId(),
			'name' => $this->getName(),
			'is_custom' => $this->getIsCustom(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'old_folder_id' => $this->getOldFolderId()
		);
	}

}
?>