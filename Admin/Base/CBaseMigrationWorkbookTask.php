<?php

class CBaseMigrationWorkbookTask extends CEosSingularBase {

	const TABLE_NAME = 'public.migration_workbook_tasks';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intMigrationWorkbookId;
	protected $m_intTaskId;
	protected $m_strCategory;
	protected $m_SubCategory;
	protected $m_strStatusType;
	protected $m_intCount;
	protected $m_strDescription;
	protected $m_strExamples;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['migration_workbook_id'] ) && $boolDirectSet ) $this->set( 'm_intMigrationWorkbookId', trim( $arrValues['migration_workbook_id'] ) ); elseif( isset( $arrValues['migration_workbook_id'] ) ) $this->setMigrationWorkbookId( $arrValues['migration_workbook_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['category'] ) && $boolDirectSet ) $this->set( 'm_strCategory', trim( stripcslashes( $arrValues['category'] ) ) ); elseif( isset( $arrValues['category'] ) ) $this->setCategory( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['category'] ) : $arrValues['category'] );
		if( isset( $arrValues['sub_category'] ) && $boolDirectSet ) $this->set( 'SubCategory', trim( $arrValues['sub_category'] ) ); elseif( isset( $arrValues['sub_category'] ) ) $this->setSubCategory( $arrValues['sub_category'] );
		if( isset( $arrValues['status_type'] ) && $boolDirectSet ) $this->set( 'm_strStatusType', trim( stripcslashes( $arrValues['status_type'] ) ) ); elseif( isset( $arrValues['status_type'] ) ) $this->setStatusType( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['status_type'] ) : $arrValues['status_type'] );
		if( isset( $arrValues['count'] ) && $boolDirectSet ) $this->set( 'm_intCount', trim( $arrValues['count'] ) ); elseif( isset( $arrValues['count'] ) ) $this->setCount( $arrValues['count'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['examples'] ) && $boolDirectSet ) $this->set( 'm_strExamples', trim( stripcslashes( $arrValues['examples'] ) ) ); elseif( isset( $arrValues['examples'] ) ) $this->setExamples( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['examples'] ) : $arrValues['examples'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setMigrationWorkbookId( $intMigrationWorkbookId ) {
		$this->set( 'm_intMigrationWorkbookId', CStrings::strToIntDef( $intMigrationWorkbookId, NULL, false ) );
	}

	public function getMigrationWorkbookId() {
		return $this->m_intMigrationWorkbookId;
	}

	public function sqlMigrationWorkbookId() {
		return ( true == isset( $this->m_intMigrationWorkbookId ) ) ? ( string ) $this->m_intMigrationWorkbookId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setCategory( $strCategory ) {
		$this->set( 'm_strCategory', CStrings::strTrimDef( $strCategory, -1, NULL, true ) );
	}

	public function getCategory() {
		return $this->m_strCategory;
	}

	public function sqlCategory() {
		return ( true == isset( $this->m_strCategory ) ) ? '\'' . addslashes( $this->m_strCategory ) . '\'' : 'NULL';
	}

	public function setSubCategory( $SubCategory ) {
		$this->set( 'm_SubCategory', CStrings::strToArrIntDef( $SubCategory, NULL ) );
	}

	public function getSubCategory() {
		return $this->m_SubCategory;
	}

	public function sqlSubCategory() {
		return ( null !== $this->m_SubCategory && true == valArr( $this->m_SubCategory ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_SubCategory, NULL ) . '\'' : 'NULL';
	}

	public function setStatusType( $strStatusType ) {
		$this->set( 'm_strStatusType', CStrings::strTrimDef( $strStatusType, -1, NULL, true ) );
	}

	public function getStatusType() {
		return $this->m_strStatusType;
	}

	public function sqlStatusType() {
		return ( true == isset( $this->m_strStatusType ) ) ? '\'' . addslashes( $this->m_strStatusType ) . '\'' : 'NULL';
	}

	public function setCount( $intCount ) {
		$this->set( 'm_intCount', CStrings::strToIntDef( $intCount, NULL, false ) );
	}

	public function getCount() {
		return $this->m_intCount;
	}

	public function sqlCount() {
		return ( true == isset( $this->m_intCount ) ) ? ( string ) $this->m_intCount : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 500, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setExamples( $strExamples ) {
		$this->set( 'm_strExamples', CStrings::strTrimDef( $strExamples, -1, NULL, true ) );
	}

	public function getExamples() {
		return $this->m_strExamples;
	}

	public function sqlExamples() {
		return ( true == isset( $this->m_strExamples ) ) ? '\'' . addslashes( $this->m_strExamples ) . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, migration_workbook_id, task_id, category, sub_category, status_type, count, description, examples, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlMigrationWorkbookId() . ', ' .
						$this->sqlTaskId() . ', ' .
						$this->sqlCategory() . ', ' .
						$this->sqlSubCategory() . ', ' .
						$this->sqlStatusType() . ', ' .
						$this->sqlCount() . ', ' .
						$this->sqlDescription() . ', ' .
						$this->sqlExamples() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' migration_workbook_id = ' . $this->sqlMigrationWorkbookId(). ',' ; } elseif( true == array_key_exists( 'MigrationWorkbookId', $this->getChangedColumns() ) ) { $strSql .= ' migration_workbook_id = ' . $this->sqlMigrationWorkbookId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId(). ',' ; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' category = ' . $this->sqlCategory(). ',' ; } elseif( true == array_key_exists( 'Category', $this->getChangedColumns() ) ) { $strSql .= ' category = ' . $this->sqlCategory() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sub_category = ' . $this->sqlSubCategory(). ',' ; } elseif( true == array_key_exists( 'SubCategory', $this->getChangedColumns() ) ) { $strSql .= ' sub_category = ' . $this->sqlSubCategory() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status_type = ' . $this->sqlStatusType(). ',' ; } elseif( true == array_key_exists( 'StatusType', $this->getChangedColumns() ) ) { $strSql .= ' status_type = ' . $this->sqlStatusType() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' count = ' . $this->sqlCount(). ',' ; } elseif( true == array_key_exists( 'Count', $this->getChangedColumns() ) ) { $strSql .= ' count = ' . $this->sqlCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription(). ',' ; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' examples = ' . $this->sqlExamples(). ',' ; } elseif( true == array_key_exists( 'Examples', $this->getChangedColumns() ) ) { $strSql .= ' examples = ' . $this->sqlExamples() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'migration_workbook_id' => $this->getMigrationWorkbookId(),
			'task_id' => $this->getTaskId(),
			'category' => $this->getCategory(),
			'sub_category' => $this->getSubCategory(),
			'status_type' => $this->getStatusType(),
			'count' => $this->getCount(),
			'description' => $this->getDescription(),
			'examples' => $this->getExamples(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>