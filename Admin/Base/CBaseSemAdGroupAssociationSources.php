<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdGroupAssociationSources
 * Do not add any new functions to this class.
 */

class CBaseSemAdGroupAssociationSources extends CEosPluralBase {

	/**
	 * @return CSemAdGroupAssociationSource[]
	 */
	public static function fetchSemAdGroupAssociationSources( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemAdGroupAssociationSource', $objDatabase );
	}

	/**
	 * @return CSemAdGroupAssociationSource
	 */
	public static function fetchSemAdGroupAssociationSource( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemAdGroupAssociationSource', $objDatabase );
	}

	public static function fetchSemAdGroupAssociationSourceCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_ad_group_association_sources', $objDatabase );
	}

	public static function fetchSemAdGroupAssociationSourceById( $intId, $objDatabase ) {
		return self::fetchSemAdGroupAssociationSource( sprintf( 'SELECT * FROM sem_ad_group_association_sources WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchSemAdGroupAssociationSourcesBySemAdGroupAssociationId( $intSemAdGroupAssociationId, $objDatabase ) {
		return self::fetchSemAdGroupAssociationSources( sprintf( 'SELECT * FROM sem_ad_group_association_sources WHERE sem_ad_group_association_id = %d', ( int ) $intSemAdGroupAssociationId ), $objDatabase );
	}

	public static function fetchSemAdGroupAssociationSourcesBySemSourceId( $intSemSourceId, $objDatabase ) {
		return self::fetchSemAdGroupAssociationSources( sprintf( 'SELECT * FROM sem_ad_group_association_sources WHERE sem_source_id = %d', ( int ) $intSemSourceId ), $objDatabase );
	}

}
?>