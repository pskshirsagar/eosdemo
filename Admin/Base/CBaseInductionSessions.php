<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CInductionSessions
 * Do not add any new functions to this class.
 */

class CBaseInductionSessions extends CEosPluralBase {

	/**
	 * @return CInductionSession[]
	 */
	public static function fetchInductionSessions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CInductionSession', $objDatabase );
	}

	/**
	 * @return CInductionSession
	 */
	public static function fetchInductionSession( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CInductionSession', $objDatabase );
	}

	public static function fetchInductionSessionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'induction_sessions', $objDatabase );
	}

	public static function fetchInductionSessionById( $intId, $objDatabase ) {
		return self::fetchInductionSession( sprintf( 'SELECT * FROM induction_sessions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchInductionSessionsByDependantSessionId( $intDependantSessionId, $objDatabase ) {
		return self::fetchInductionSessions( sprintf( 'SELECT * FROM induction_sessions WHERE dependant_session_id = %d', ( int ) $intDependantSessionId ), $objDatabase );
	}

	public static function fetchInductionSessionsByInductionTypeId( $intInductionTypeId, $objDatabase ) {
		return self::fetchInductionSessions( sprintf( 'SELECT * FROM induction_sessions WHERE induction_type_id = %d', ( int ) $intInductionTypeId ), $objDatabase );
	}

	public static function fetchInductionSessionsByTrainingId( $intTrainingId, $objDatabase ) {
		return self::fetchInductionSessions( sprintf( 'SELECT * FROM induction_sessions WHERE training_id = %d', ( int ) $intTrainingId ), $objDatabase );
	}

}
?>