<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CJobPostingQuestions
 * Do not add any new functions to this class.
 */

class CBaseJobPostingQuestions extends CEosPluralBase {

	/**
	 * @return CJobPostingQuestion[]
	 */
	public static function fetchJobPostingQuestions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CJobPostingQuestion', $objDatabase );
	}

	/**
	 * @return CJobPostingQuestion
	 */
	public static function fetchJobPostingQuestion( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobPostingQuestion', $objDatabase );
	}

	public static function fetchJobPostingQuestionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_posting_questions', $objDatabase );
	}

	public static function fetchJobPostingQuestionById( $intId, $objDatabase ) {
		return self::fetchJobPostingQuestion( sprintf( 'SELECT * FROM job_posting_questions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchJobPostingQuestionsByJobPostingId( $intJobPostingId, $objDatabase ) {
		return self::fetchJobPostingQuestions( sprintf( 'SELECT * FROM job_posting_questions WHERE job_posting_id = %d', ( int ) $intJobPostingId ), $objDatabase );
	}

	public static function fetchJobPostingQuestionsByQuestionTypeId( $intQuestionTypeId, $objDatabase ) {
		return self::fetchJobPostingQuestions( sprintf( 'SELECT * FROM job_posting_questions WHERE question_type_id = %d', ( int ) $intQuestionTypeId ), $objDatabase );
	}

}
?>