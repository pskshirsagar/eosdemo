<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyAddresses
 * Do not add any new functions to this class.
 */

class CBaseCompanyAddresses extends CEosPluralBase {

	/**
	 * @return CCompanyAddress[]
	 */
	public static function fetchCompanyAddresses( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCompanyAddress', $objDatabase );
	}

	/**
	 * @return CCompanyAddress
	 */
	public static function fetchCompanyAddress( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCompanyAddress', $objDatabase );
	}

	public static function fetchCompanyAddressCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'company_addresses', $objDatabase );
	}

	public static function fetchCompanyAddressById( $intId, $objDatabase ) {
		return self::fetchCompanyAddress( sprintf( 'SELECT * FROM company_addresses WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCompanyAddressesByCid( $intCid, $objDatabase ) {
		return self::fetchCompanyAddresses( sprintf( 'SELECT * FROM company_addresses WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchCompanyAddressesByAddressTypeId( $intAddressTypeId, $objDatabase ) {
		return self::fetchCompanyAddresses( sprintf( 'SELECT * FROM company_addresses WHERE address_type_id = %d', ( int ) $intAddressTypeId ), $objDatabase );
	}

	public static function fetchCompanyAddressesByTimeZoneId( $intTimeZoneId, $objDatabase ) {
		return self::fetchCompanyAddresses( sprintf( 'SELECT * FROM company_addresses WHERE time_zone_id = %d', ( int ) $intTimeZoneId ), $objDatabase );
	}

}
?>