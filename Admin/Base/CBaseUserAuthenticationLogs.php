<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserAuthenticationLogs
 * Do not add any new functions to this class.
 */

class CBaseUserAuthenticationLogs extends CEosPluralBase {

	/**
	 * @return CUserAuthenticationLog[]
	 */
	public static function fetchUserAuthenticationLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserAuthenticationLog', $objDatabase );
	}

	/**
	 * @return CUserAuthenticationLog
	 */
	public static function fetchUserAuthenticationLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserAuthenticationLog', $objDatabase );
	}

	public static function fetchUserAuthenticationLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_authentication_logs', $objDatabase );
	}

	public static function fetchUserAuthenticationLogById( $intId, $objDatabase ) {
		return self::fetchUserAuthenticationLog( sprintf( 'SELECT * FROM user_authentication_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserAuthenticationLogsByUserId( $intUserId, $objDatabase ) {
		return self::fetchUserAuthenticationLogs( sprintf( 'SELECT * FROM user_authentication_logs WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

}
?>