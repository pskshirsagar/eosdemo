<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeTrainingSessions
 * Do not add any new functions to this class.
 */

class CBaseEmployeeTrainingSessions extends CEosPluralBase {

	/**
	 * @return CEmployeeTrainingSession[]
	 */
	public static function fetchEmployeeTrainingSessions( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeTrainingSession', $objDatabase );
	}

	/**
	 * @return CEmployeeTrainingSession
	 */
	public static function fetchEmployeeTrainingSession( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeTrainingSession', $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_training_sessions', $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionById( $intId, $objDatabase ) {
		return self::fetchEmployeeTrainingSession( sprintf( 'SELECT * FROM employee_training_sessions WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionsByTrainingSessionId( $intTrainingSessionId, $objDatabase ) {
		return self::fetchEmployeeTrainingSessions( sprintf( 'SELECT * FROM employee_training_sessions WHERE training_session_id = %d', ( int ) $intTrainingSessionId ), $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionsByUserId( $intUserId, $objDatabase ) {
		return self::fetchEmployeeTrainingSessions( sprintf( 'SELECT * FROM employee_training_sessions WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

	public static function fetchEmployeeTrainingSessionsByGroupId( $intGroupId, $objDatabase ) {
		return self::fetchEmployeeTrainingSessions( sprintf( 'SELECT * FROM employee_training_sessions WHERE group_id = %d', ( int ) $intGroupId ), $objDatabase );
	}

}
?>