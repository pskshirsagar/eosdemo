<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CJobPostingCategoryDepartments
 * Do not add any new functions to this class.
 */

class CBaseJobPostingCategoryDepartments extends CEosPluralBase {

	/**
	 * @return CJobPostingCategoryDepartment[]
	 */
	public static function fetchJobPostingCategoryDepartments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CJobPostingCategoryDepartment', $objDatabase );
	}

	/**
	 * @return CJobPostingCategoryDepartment
	 */
	public static function fetchJobPostingCategoryDepartment( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CJobPostingCategoryDepartment', $objDatabase );
	}

	public static function fetchJobPostingCategoryDepartmentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'job_posting_category_departments', $objDatabase );
	}

	public static function fetchJobPostingCategoryDepartmentById( $intId, $objDatabase ) {
		return self::fetchJobPostingCategoryDepartment( sprintf( 'SELECT * FROM job_posting_category_departments WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchJobPostingCategoryDepartmentsByJobPostingCategoryId( $intJobPostingCategoryId, $objDatabase ) {
		return self::fetchJobPostingCategoryDepartments( sprintf( 'SELECT * FROM job_posting_category_departments WHERE job_posting_category_id = %d', ( int ) $intJobPostingCategoryId ), $objDatabase );
	}

	public static function fetchJobPostingCategoryDepartmentsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchJobPostingCategoryDepartments( sprintf( 'SELECT * FROM job_posting_category_departments WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>