<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPurchaseRequestReasons
 * Do not add any new functions to this class.
 */

class CBasePurchaseRequestReasons extends CEosPluralBase {

	/**
	 * @return CPurchaseRequestReason[]
	 */
	public static function fetchPurchaseRequestReasons( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CPurchaseRequestReason::class, $objDatabase );
	}

	/**
	 * @return CPurchaseRequestReason
	 */
	public static function fetchPurchaseRequestReason( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CPurchaseRequestReason::class, $objDatabase );
	}

	public static function fetchPurchaseRequestReasonCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'purchase_request_reasons', $objDatabase );
	}

	public static function fetchPurchaseRequestReasonById( $intId, $objDatabase ) {
		return self::fetchPurchaseRequestReason( sprintf( 'SELECT * FROM purchase_request_reasons WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPurchaseRequestReasonsByPurchaseRequestTypeId( $intPurchaseRequestTypeId, $objDatabase ) {
		return self::fetchPurchaseRequestReasons( sprintf( 'SELECT * FROM purchase_request_reasons WHERE purchase_request_type_id = %d', ( int ) $intPurchaseRequestTypeId ), $objDatabase );
	}

}
?>