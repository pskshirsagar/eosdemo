<?php

class CBaseEmployeeTaxCycle extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_tax_cycles';

	protected $m_intId;
	protected $m_strFinancialYearName;
	protected $m_strStartDate;
	protected $m_strEndDate;
	protected $m_intCycle;
	protected $m_strCycleStartDate;
	protected $m_strCycleEndDate;
	protected $m_boolIsPublished;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_intFinancialYearId;

	public function __construct() {
		parent::__construct();

		$this->m_boolIsPublished = true;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['financial_year_name'] ) && $boolDirectSet ) $this->set( 'm_strFinancialYearName', trim( $arrValues['financial_year_name'] ) ); elseif( isset( $arrValues['financial_year_name'] ) ) $this->setFinancialYearName( $arrValues['financial_year_name'] );
		if( isset( $arrValues['start_date'] ) && $boolDirectSet ) $this->set( 'm_strStartDate', trim( $arrValues['start_date'] ) ); elseif( isset( $arrValues['start_date'] ) ) $this->setStartDate( $arrValues['start_date'] );
		if( isset( $arrValues['end_date'] ) && $boolDirectSet ) $this->set( 'm_strEndDate', trim( $arrValues['end_date'] ) ); elseif( isset( $arrValues['end_date'] ) ) $this->setEndDate( $arrValues['end_date'] );
		if( isset( $arrValues['cycle'] ) && $boolDirectSet ) $this->set( 'm_intCycle', trim( $arrValues['cycle'] ) ); elseif( isset( $arrValues['cycle'] ) ) $this->setCycle( $arrValues['cycle'] );
		if( isset( $arrValues['cycle_start_date'] ) && $boolDirectSet ) $this->set( 'm_strCycleStartDate', trim( $arrValues['cycle_start_date'] ) ); elseif( isset( $arrValues['cycle_start_date'] ) ) $this->setCycleStartDate( $arrValues['cycle_start_date'] );
		if( isset( $arrValues['cycle_end_date'] ) && $boolDirectSet ) $this->set( 'm_strCycleEndDate', trim( $arrValues['cycle_end_date'] ) ); elseif( isset( $arrValues['cycle_end_date'] ) ) $this->setCycleEndDate( $arrValues['cycle_end_date'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_boolIsPublished', trim( stripcslashes( $arrValues['is_published'] ) ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_published'] ) : $arrValues['is_published'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['financial_year_id'] ) && $boolDirectSet ) $this->set( 'm_intFinancialYearId', trim( $arrValues['financial_year_id'] ) ); elseif( isset( $arrValues['financial_year_id'] ) ) $this->setFinancialYearId( $arrValues['financial_year_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setFinancialYearName( $strFinancialYearName ) {
		$this->set( 'm_strFinancialYearName', CStrings::strTrimDef( $strFinancialYearName, 20, NULL, true ) );
	}

	public function getFinancialYearName() {
		return $this->m_strFinancialYearName;
	}

	public function sqlFinancialYearName() {
		return ( true == isset( $this->m_strFinancialYearName ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strFinancialYearName ) : '\'' . addslashes( $this->m_strFinancialYearName ) . '\'' ) : 'NULL';
	}

	public function setStartDate( $strStartDate ) {
		$this->set( 'm_strStartDate', CStrings::strTrimDef( $strStartDate, -1, NULL, true ) );
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function sqlStartDate() {
		return ( true == isset( $this->m_strStartDate ) ) ? '\'' . $this->m_strStartDate . '\'' : 'NOW()';
	}

	public function setEndDate( $strEndDate ) {
		$this->set( 'm_strEndDate', CStrings::strTrimDef( $strEndDate, -1, NULL, true ) );
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function sqlEndDate() {
		return ( true == isset( $this->m_strEndDate ) ) ? '\'' . $this->m_strEndDate . '\'' : 'NOW()';
	}

	public function setCycle( $intCycle ) {
		$this->set( 'm_intCycle', CStrings::strToIntDef( $intCycle, NULL, false ) );
	}

	public function getCycle() {
		return $this->m_intCycle;
	}

	public function sqlCycle() {
		return ( true == isset( $this->m_intCycle ) ) ? ( string ) $this->m_intCycle : 'NULL';
	}

	public function setCycleStartDate( $strCycleStartDate ) {
		$this->set( 'm_strCycleStartDate', CStrings::strTrimDef( $strCycleStartDate, -1, NULL, true ) );
	}

	public function getCycleStartDate() {
		return $this->m_strCycleStartDate;
	}

	public function sqlCycleStartDate() {
		return ( true == isset( $this->m_strCycleStartDate ) ) ? '\'' . $this->m_strCycleStartDate . '\'' : 'NOW()';
	}

	public function setCycleEndDate( $strCycleEndDate ) {
		$this->set( 'm_strCycleEndDate', CStrings::strTrimDef( $strCycleEndDate, -1, NULL, true ) );
	}

	public function getCycleEndDate() {
		return $this->m_strCycleEndDate;
	}

	public function sqlCycleEndDate() {
		return ( true == isset( $this->m_strCycleEndDate ) ) ? '\'' . $this->m_strCycleEndDate . '\'' : 'NOW()';
	}

	public function setIsPublished( $boolIsPublished ) {
		$this->set( 'm_boolIsPublished', CStrings::strToBool( $boolIsPublished ) );
	}

	public function getIsPublished() {
		return $this->m_boolIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_boolIsPublished ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsPublished ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setFinancialYearId( $intFinancialYearId ) {
		$this->set( 'm_intFinancialYearId', CStrings::strToIntDef( $intFinancialYearId, NULL, false ) );
	}

	public function getFinancialYearId() {
		return $this->m_intFinancialYearId;
	}

	public function sqlFinancialYearId() {
		return ( true == isset( $this->m_intFinancialYearId ) ) ? ( string ) $this->m_intFinancialYearId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, financial_year_name, start_date, end_date, cycle, cycle_start_date, cycle_end_date, is_published, updated_by, updated_on, created_by, created_on, financial_year_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlFinancialYearName() . ', ' .
						$this->sqlStartDate() . ', ' .
						$this->sqlEndDate() . ', ' .
						$this->sqlCycle() . ', ' .
						$this->sqlCycleStartDate() . ', ' .
						$this->sqlCycleEndDate() . ', ' .
						$this->sqlIsPublished() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlFinancialYearId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_year_name = ' . $this->sqlFinancialYearName(). ',' ; } elseif( true == array_key_exists( 'FinancialYearName', $this->getChangedColumns() ) ) { $strSql .= ' financial_year_name = ' . $this->sqlFinancialYearName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' start_date = ' . $this->sqlStartDate(). ',' ; } elseif( true == array_key_exists( 'StartDate', $this->getChangedColumns() ) ) { $strSql .= ' start_date = ' . $this->sqlStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' end_date = ' . $this->sqlEndDate(). ',' ; } elseif( true == array_key_exists( 'EndDate', $this->getChangedColumns() ) ) { $strSql .= ' end_date = ' . $this->sqlEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cycle = ' . $this->sqlCycle(). ',' ; } elseif( true == array_key_exists( 'Cycle', $this->getChangedColumns() ) ) { $strSql .= ' cycle = ' . $this->sqlCycle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cycle_start_date = ' . $this->sqlCycleStartDate(). ',' ; } elseif( true == array_key_exists( 'CycleStartDate', $this->getChangedColumns() ) ) { $strSql .= ' cycle_start_date = ' . $this->sqlCycleStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cycle_end_date = ' . $this->sqlCycleEndDate(). ',' ; } elseif( true == array_key_exists( 'CycleEndDate', $this->getChangedColumns() ) ) { $strSql .= ' cycle_end_date = ' . $this->sqlCycleEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished(). ',' ; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' financial_year_id = ' . $this->sqlFinancialYearId(). ',' ; } elseif( true == array_key_exists( 'FinancialYearId', $this->getChangedColumns() ) ) { $strSql .= ' financial_year_id = ' . $this->sqlFinancialYearId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'financial_year_name' => $this->getFinancialYearName(),
			'start_date' => $this->getStartDate(),
			'end_date' => $this->getEndDate(),
			'cycle' => $this->getCycle(),
			'cycle_start_date' => $this->getCycleStartDate(),
			'cycle_end_date' => $this->getCycleEndDate(),
			'is_published' => $this->getIsPublished(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'financial_year_id' => $this->getFinancialYearId()
		);
	}

}
?>