<?php

class CBaseEmailTemplateSlot extends CEosSingularBase {

	const TABLE_NAME = 'public.email_template_slots';

	protected $m_intId;
	protected $m_intEmailTemplateId;
	protected $m_strKey;
	protected $m_strTitle;
	protected $m_strDefaultImageName;
	protected $m_strBgcolor;
	protected $m_intMediaWidth;
	protected $m_intMediaHeight;
	protected $m_intIsPublished;
	protected $m_intOrderNum;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPublished = '1';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['email_template_id'] ) && $boolDirectSet ) $this->set( 'm_intEmailTemplateId', trim( $arrValues['email_template_id'] ) ); elseif( isset( $arrValues['email_template_id'] ) ) $this->setEmailTemplateId( $arrValues['email_template_id'] );
		if( isset( $arrValues['key'] ) && $boolDirectSet ) $this->set( 'm_strKey', trim( stripcslashes( $arrValues['key'] ) ) ); elseif( isset( $arrValues['key'] ) ) $this->setKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['key'] ) : $arrValues['key'] );
		if( isset( $arrValues['title'] ) && $boolDirectSet ) $this->set( 'm_strTitle', trim( stripcslashes( $arrValues['title'] ) ) ); elseif( isset( $arrValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['title'] ) : $arrValues['title'] );
		if( isset( $arrValues['default_image_name'] ) && $boolDirectSet ) $this->set( 'm_strDefaultImageName', trim( stripcslashes( $arrValues['default_image_name'] ) ) ); elseif( isset( $arrValues['default_image_name'] ) ) $this->setDefaultImageName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['default_image_name'] ) : $arrValues['default_image_name'] );
		if( isset( $arrValues['bgcolor'] ) && $boolDirectSet ) $this->set( 'm_strBgcolor', trim( stripcslashes( $arrValues['bgcolor'] ) ) ); elseif( isset( $arrValues['bgcolor'] ) ) $this->setBgcolor( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['bgcolor'] ) : $arrValues['bgcolor'] );
		if( isset( $arrValues['media_width'] ) && $boolDirectSet ) $this->set( 'm_intMediaWidth', trim( $arrValues['media_width'] ) ); elseif( isset( $arrValues['media_width'] ) ) $this->setMediaWidth( $arrValues['media_width'] );
		if( isset( $arrValues['media_height'] ) && $boolDirectSet ) $this->set( 'm_intMediaHeight', trim( $arrValues['media_height'] ) ); elseif( isset( $arrValues['media_height'] ) ) $this->setMediaHeight( $arrValues['media_height'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmailTemplateId( $intEmailTemplateId ) {
		$this->set( 'm_intEmailTemplateId', CStrings::strToIntDef( $intEmailTemplateId, NULL, false ) );
	}

	public function getEmailTemplateId() {
		return $this->m_intEmailTemplateId;
	}

	public function sqlEmailTemplateId() {
		return ( true == isset( $this->m_intEmailTemplateId ) ) ? ( string ) $this->m_intEmailTemplateId : 'NULL';
	}

	public function setKey( $strKey ) {
		$this->set( 'm_strKey', CStrings::strTrimDef( $strKey, 64, NULL, true ) );
	}

	public function getKey() {
		return $this->m_strKey;
	}

	public function sqlKey() {
		return ( true == isset( $this->m_strKey ) ) ? '\'' . addslashes( $this->m_strKey ) . '\'' : 'NULL';
	}

	public function setTitle( $strTitle ) {
		$this->set( 'm_strTitle', CStrings::strTrimDef( $strTitle, 50, NULL, true ) );
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function sqlTitle() {
		return ( true == isset( $this->m_strTitle ) ) ? '\'' . addslashes( $this->m_strTitle ) . '\'' : 'NULL';
	}

	public function setDefaultImageName( $strDefaultImageName ) {
		$this->set( 'm_strDefaultImageName', CStrings::strTrimDef( $strDefaultImageName, 4096, NULL, true ) );
	}

	public function getDefaultImageName() {
		return $this->m_strDefaultImageName;
	}

	public function sqlDefaultImageName() {
		return ( true == isset( $this->m_strDefaultImageName ) ) ? '\'' . addslashes( $this->m_strDefaultImageName ) . '\'' : 'NULL';
	}

	public function setBgcolor( $strBgcolor ) {
		$this->set( 'm_strBgcolor', CStrings::strTrimDef( $strBgcolor, 20, NULL, true ) );
	}

	public function getBgcolor() {
		return $this->m_strBgcolor;
	}

	public function sqlBgcolor() {
		return ( true == isset( $this->m_strBgcolor ) ) ? '\'' . addslashes( $this->m_strBgcolor ) . '\'' : 'NULL';
	}

	public function setMediaWidth( $intMediaWidth ) {
		$this->set( 'm_intMediaWidth', CStrings::strToIntDef( $intMediaWidth, NULL, false ) );
	}

	public function getMediaWidth() {
		return $this->m_intMediaWidth;
	}

	public function sqlMediaWidth() {
		return ( true == isset( $this->m_intMediaWidth ) ) ? ( string ) $this->m_intMediaWidth : 'NULL';
	}

	public function setMediaHeight( $intMediaHeight ) {
		$this->set( 'm_intMediaHeight', CStrings::strToIntDef( $intMediaHeight, NULL, false ) );
	}

	public function getMediaHeight() {
		return $this->m_intMediaHeight;
	}

	public function sqlMediaHeight() {
		return ( true == isset( $this->m_intMediaHeight ) ) ? ( string ) $this->m_intMediaHeight : 'NULL';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '1';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, email_template_id, key, title, default_image_name, bgcolor, media_width, media_height, is_published, order_num, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmailTemplateId() . ', ' .
 						$this->sqlKey() . ', ' .
 						$this->sqlTitle() . ', ' .
 						$this->sqlDefaultImageName() . ', ' .
 						$this->sqlBgcolor() . ', ' .
 						$this->sqlMediaWidth() . ', ' .
 						$this->sqlMediaHeight() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlOrderNum() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' email_template_id = ' . $this->sqlEmailTemplateId() . ','; } elseif( true == array_key_exists( 'EmailTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' email_template_id = ' . $this->sqlEmailTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' key = ' . $this->sqlKey() . ','; } elseif( true == array_key_exists( 'Key', $this->getChangedColumns() ) ) { $strSql .= ' key = ' . $this->sqlKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; } elseif( true == array_key_exists( 'Title', $this->getChangedColumns() ) ) { $strSql .= ' title = ' . $this->sqlTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' default_image_name = ' . $this->sqlDefaultImageName() . ','; } elseif( true == array_key_exists( 'DefaultImageName', $this->getChangedColumns() ) ) { $strSql .= ' default_image_name = ' . $this->sqlDefaultImageName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' bgcolor = ' . $this->sqlBgcolor() . ','; } elseif( true == array_key_exists( 'Bgcolor', $this->getChangedColumns() ) ) { $strSql .= ' bgcolor = ' . $this->sqlBgcolor() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_width = ' . $this->sqlMediaWidth() . ','; } elseif( true == array_key_exists( 'MediaWidth', $this->getChangedColumns() ) ) { $strSql .= ' media_width = ' . $this->sqlMediaWidth() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' media_height = ' . $this->sqlMediaHeight() . ','; } elseif( true == array_key_exists( 'MediaHeight', $this->getChangedColumns() ) ) { $strSql .= ' media_height = ' . $this->sqlMediaHeight() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'email_template_id' => $this->getEmailTemplateId(),
			'key' => $this->getKey(),
			'title' => $this->getTitle(),
			'default_image_name' => $this->getDefaultImageName(),
			'bgcolor' => $this->getBgcolor(),
			'media_width' => $this->getMediaWidth(),
			'media_height' => $this->getMediaHeight(),
			'is_published' => $this->getIsPublished(),
			'order_num' => $this->getOrderNum(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>