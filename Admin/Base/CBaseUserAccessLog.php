<?php

class CBaseUserAccessLog extends CEosSingularBase {

	const TABLE_NAME = 'public.user_access_logs';

	protected $m_intUserId;
	protected $m_intPsProductId;
	protected $m_strModule;
	protected $m_strAction;
	protected $m_strMonth;
	protected $m_intHitCount;

	public function __construct() {
		parent::__construct();

		$this->m_intHitCount = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['user_id'] ) && $boolDirectSet ) $this->set( 'm_intUserId', trim( $arrValues['user_id'] ) ); elseif( isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['module'] ) && $boolDirectSet ) $this->set( 'm_strModule', trim( stripcslashes( $arrValues['module'] ) ) ); elseif( isset( $arrValues['module'] ) ) $this->setModule( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['module'] ) : $arrValues['module'] );
		if( isset( $arrValues['action'] ) && $boolDirectSet ) $this->set( 'm_strAction', trim( stripcslashes( $arrValues['action'] ) ) ); elseif( isset( $arrValues['action'] ) ) $this->setAction( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['action'] ) : $arrValues['action'] );
		if( isset( $arrValues['month'] ) && $boolDirectSet ) $this->set( 'm_strMonth', trim( $arrValues['month'] ) ); elseif( isset( $arrValues['month'] ) ) $this->setMonth( $arrValues['month'] );
		if( isset( $arrValues['hit_count'] ) && $boolDirectSet ) $this->set( 'm_intHitCount', trim( $arrValues['hit_count'] ) ); elseif( isset( $arrValues['hit_count'] ) ) $this->setHitCount( $arrValues['hit_count'] );
		$this->m_boolInitialized = true;
	}

	public function setUserId( $intUserId ) {
		$this->set( 'm_intUserId', CStrings::strToIntDef( $intUserId, NULL, false ) );
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function sqlUserId() {
		return ( true == isset( $this->m_intUserId ) ) ? ( string ) $this->m_intUserId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setModule( $strModule ) {
		$this->set( 'm_strModule', CStrings::strTrimDef( $strModule, 100, NULL, true ) );
	}

	public function getModule() {
		return $this->m_strModule;
	}

	public function sqlModule() {
		return ( true == isset( $this->m_strModule ) ) ? '\'' . addslashes( $this->m_strModule ) . '\'' : 'NULL';
	}

	public function setAction( $strAction ) {
		$this->set( 'm_strAction', CStrings::strTrimDef( $strAction, 100, NULL, true ) );
	}

	public function getAction() {
		return $this->m_strAction;
	}

	public function sqlAction() {
		return ( true == isset( $this->m_strAction ) ) ? '\'' . addslashes( $this->m_strAction ) . '\'' : 'NULL';
	}

	public function setMonth( $strMonth ) {
		$this->set( 'm_strMonth', CStrings::strTrimDef( $strMonth, -1, NULL, true ) );
	}

	public function getMonth() {
		return $this->m_strMonth;
	}

	public function sqlMonth() {
		return ( true == isset( $this->m_strMonth ) ) ? '\'' . $this->m_strMonth . '\'' : 'NOW()';
	}

	public function setHitCount( $intHitCount ) {
		$this->set( 'm_intHitCount', CStrings::strToIntDef( $intHitCount, NULL, false ) );
	}

	public function getHitCount() {
		return $this->m_intHitCount;
	}

	public function sqlHitCount() {
		return ( true == isset( $this->m_intHitCount ) ) ? ( string ) $this->m_intHitCount : '0';
	}

	public function toArray() {
		return array(
			'user_id' => $this->getUserId(),
			'ps_product_id' => $this->getPsProductId(),
			'module' => $this->getModule(),
			'action' => $this->getAction(),
			'month' => $this->getMonth(),
			'hit_count' => $this->getHitCount()
		);
	}

}
?>