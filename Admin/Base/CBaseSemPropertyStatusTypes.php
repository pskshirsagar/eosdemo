<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemPropertyStatusTypes
 * Do not add any new functions to this class.
 */

class CBaseSemPropertyStatusTypes extends CEosPluralBase {

	/**
	 * @return CSemPropertyStatusType[]
	 */
	public static function fetchSemPropertyStatusTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CSemPropertyStatusType', $objDatabase );
	}

	/**
	 * @return CSemPropertyStatusType
	 */
	public static function fetchSemPropertyStatusType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CSemPropertyStatusType', $objDatabase );
	}

	public static function fetchSemPropertyStatusTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'sem_property_status_types', $objDatabase );
	}

	public static function fetchSemPropertyStatusTypeById( $intId, $objDatabase ) {
		return self::fetchSemPropertyStatusType( sprintf( 'SELECT * FROM sem_property_status_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>