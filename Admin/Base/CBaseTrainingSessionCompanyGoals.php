<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingSessionCompanyGoals
 * Do not add any new functions to this class.
 */

class CBaseTrainingSessionCompanyGoals extends CEosPluralBase {

	/**
	 * @return CTrainingSessionCompanyGoal[]
	 */
	public static function fetchTrainingSessionCompanyGoals( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTrainingSessionCompanyGoal', $objDatabase );
	}

	/**
	 * @return CTrainingSessionCompanyGoal
	 */
	public static function fetchTrainingSessionCompanyGoal( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTrainingSessionCompanyGoal', $objDatabase );
	}

	public static function fetchTrainingSessionCompanyGoalCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'training_session_company_goals', $objDatabase );
	}

	public static function fetchTrainingSessionCompanyGoalById( $intId, $objDatabase ) {
		return self::fetchTrainingSessionCompanyGoal( sprintf( 'SELECT * FROM training_session_company_goals WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchTrainingSessionCompanyGoalsByTrainingSessionId( $intTrainingSessionId, $objDatabase ) {
		return self::fetchTrainingSessionCompanyGoals( sprintf( 'SELECT * FROM training_session_company_goals WHERE training_session_id = %d', ( int ) $intTrainingSessionId ), $objDatabase );
	}

	public static function fetchTrainingSessionCompanyGoalsByTrainingId( $intTrainingId, $objDatabase ) {
		return self::fetchTrainingSessionCompanyGoals( sprintf( 'SELECT * FROM training_session_company_goals WHERE training_id = %d', ( int ) $intTrainingId ), $objDatabase );
	}

	public static function fetchTrainingSessionCompanyGoalsByCompanyGoalId( $intCompanyGoalId, $objDatabase ) {
		return self::fetchTrainingSessionCompanyGoals( sprintf( 'SELECT * FROM training_session_company_goals WHERE company_goal_id = %d', ( int ) $intCompanyGoalId ), $objDatabase );
	}

}
?>