<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCertifications
 * Do not add any new functions to this class.
 */

class CBaseCertifications extends CEosPluralBase {

	/**
	 * @return CCertification[]
	 */
	public static function fetchCertifications( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CCertification', $objDatabase );
	}

	/**
	 * @return CCertification
	 */
	public static function fetchCertification( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CCertification', $objDatabase );
	}

	public static function fetchCertificationCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'certifications', $objDatabase );
	}

	public static function fetchCertificationById( $intId, $objDatabase ) {
		return self::fetchCertification( sprintf( 'SELECT * FROM certifications WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchCertificationsByCertificationId( $intCertificationId, $objDatabase ) {
		return self::fetchCertifications( sprintf( 'SELECT * FROM certifications WHERE certification_id = %d', ( int ) $intCertificationId ), $objDatabase );
	}

	public static function fetchCertificationsByCertificationTypeId( $intCertificationTypeId, $objDatabase ) {
		return self::fetchCertifications( sprintf( 'SELECT * FROM certifications WHERE certification_type_id = %d', ( int ) $intCertificationTypeId ), $objDatabase );
	}

	public static function fetchCertificationsByCertificationLevelTypeId( $intCertificationLevelTypeId, $objDatabase ) {
		return self::fetchCertifications( sprintf( 'SELECT * FROM certifications WHERE certification_level_type_id = %d', ( int ) $intCertificationLevelTypeId ), $objDatabase );
	}

	public static function fetchCertificationsByDepartmentId( $intDepartmentId, $objDatabase ) {
		return self::fetchCertifications( sprintf( 'SELECT * FROM certifications WHERE department_id = %d', ( int ) $intDepartmentId ), $objDatabase );
	}

	public static function fetchCertificationsByDesignationId( $intDesignationId, $objDatabase ) {
		return self::fetchCertifications( sprintf( 'SELECT * FROM certifications WHERE designation_id = %d', ( int ) $intDesignationId ), $objDatabase );
	}

}
?>