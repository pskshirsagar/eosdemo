<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPaymentProcessingFees
 * Do not add any new functions to this class.
 */

class CBasePaymentProcessingFees extends CEosPluralBase {

	/**
	 * @return CPaymentProcessingFee[]
	 */
	public static function fetchPaymentProcessingFees( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CPaymentProcessingFee', $objDatabase );
	}

	/**
	 * @return CPaymentProcessingFee
	 */
	public static function fetchPaymentProcessingFee( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CPaymentProcessingFee', $objDatabase );
	}

	public static function fetchPaymentProcessingFeeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'payment_processing_fees', $objDatabase );
	}

	public static function fetchPaymentProcessingFeeById( $intId, $objDatabase ) {
		return self::fetchPaymentProcessingFee( sprintf( 'SELECT * FROM payment_processing_fees WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchPaymentProcessingFeesByClientId( $intClientId, $objDatabase ) {
		return self::fetchPaymentProcessingFees( sprintf( 'SELECT * FROM payment_processing_fees WHERE client_id = %d', ( int ) $intClientId ), $objDatabase );
	}

	public static function fetchPaymentProcessingFeesByPaymentTypeId( $intPaymentTypeId, $objDatabase ) {
		return self::fetchPaymentProcessingFees( sprintf( 'SELECT * FROM payment_processing_fees WHERE payment_type_id = %d', ( int ) $intPaymentTypeId ), $objDatabase );
	}

}
?>