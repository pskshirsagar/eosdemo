<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CChargeGroups
 * Do not add any new functions to this class.
 */

class CBaseChargeGroups extends CEosPluralBase {

	/**
	 * @return CChargeGroup[]
	 */
	public static function fetchChargeGroups( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CChargeGroup', $objDatabase );
	}

	/**
	 * @return CChargeGroup
	 */
	public static function fetchChargeGroup( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CChargeGroup', $objDatabase );
	}

	public static function fetchChargeGroupCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'charge_groups', $objDatabase );
	}

	public static function fetchChargeGroupById( $intId, $objDatabase ) {
		return self::fetchChargeGroup( sprintf( 'SELECT * FROM charge_groups WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchChargeGroupsByCid( $intCid, $objDatabase ) {
		return self::fetchChargeGroups( sprintf( 'SELECT * FROM charge_groups WHERE cid = %d', ( int ) $intCid ), $objDatabase );
	}

	public static function fetchChargeGroupsByLabelChargeCodeId( $intLabelChargeCodeId, $objDatabase ) {
		return self::fetchChargeGroups( sprintf( 'SELECT * FROM charge_groups WHERE label_charge_code_id = %d', ( int ) $intLabelChargeCodeId ), $objDatabase );
	}

}
?>