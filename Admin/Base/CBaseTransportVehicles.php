<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTransportVehicles
 * Do not add any new functions to this class.
 */

class CBaseTransportVehicles extends CEosPluralBase {

	/**
	 * @return CTransportVehicle[]
	 */
	public static function fetchTransportVehicles( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, CTransportVehicle::class, $objDatabase );
	}

	/**
	 * @return CTransportVehicle
	 */
	public static function fetchTransportVehicle( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, CTransportVehicle::class, $objDatabase );
	}

	public static function fetchTransportVehicleCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'transport_vehicles', $objDatabase );
	}

	public static function fetchTransportVehicleById( $intId, $objDatabase ) {
		return self::fetchTransportVehicle( sprintf( 'SELECT * FROM transport_vehicles WHERE id = %d', $intId ), $objDatabase );
	}

}
?>