<?php

class CBaseExportBatch extends CEosSingularBase {

	const TABLE_NAME = 'public.export_batches';

	protected $m_intId;
	protected $m_intExportBatchTypeId;
	protected $m_strCurrencyCode;
	protected $m_strRemotePrimaryKey;
	protected $m_strExportBatchDate;
	protected $m_fltCachedArBalance;
	protected $m_fltCachedPaymentsTotal;
	protected $m_fltCachedChargesTotal;
	protected $m_strExportBatchMemo;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strCurrencyCode = 'USD';
		$this->m_fltCachedArBalance = '0';
		$this->m_fltCachedPaymentsTotal = '0';
		$this->m_fltCachedChargesTotal = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['export_batch_type_id'] ) && $boolDirectSet ) $this->set( 'm_intExportBatchTypeId', trim( $arrValues['export_batch_type_id'] ) ); elseif( isset( $arrValues['export_batch_type_id'] ) ) $this->setExportBatchTypeId( $arrValues['export_batch_type_id'] );
		if( isset( $arrValues['currency_code'] ) && $boolDirectSet ) $this->set( 'm_strCurrencyCode', trim( stripcslashes( $arrValues['currency_code'] ) ) ); elseif( isset( $arrValues['currency_code'] ) ) $this->setCurrencyCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['currency_code'] ) : $arrValues['currency_code'] );
		if( isset( $arrValues['remote_primary_key'] ) && $boolDirectSet ) $this->set( 'm_strRemotePrimaryKey', trim( stripcslashes( $arrValues['remote_primary_key'] ) ) ); elseif( isset( $arrValues['remote_primary_key'] ) ) $this->setRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['remote_primary_key'] ) : $arrValues['remote_primary_key'] );
		if( isset( $arrValues['export_batch_date'] ) && $boolDirectSet ) $this->set( 'm_strExportBatchDate', trim( $arrValues['export_batch_date'] ) ); elseif( isset( $arrValues['export_batch_date'] ) ) $this->setExportBatchDate( $arrValues['export_batch_date'] );
		if( isset( $arrValues['cached_ar_balance'] ) && $boolDirectSet ) $this->set( 'm_fltCachedArBalance', trim( $arrValues['cached_ar_balance'] ) ); elseif( isset( $arrValues['cached_ar_balance'] ) ) $this->setCachedArBalance( $arrValues['cached_ar_balance'] );
		if( isset( $arrValues['cached_payments_total'] ) && $boolDirectSet ) $this->set( 'm_fltCachedPaymentsTotal', trim( $arrValues['cached_payments_total'] ) ); elseif( isset( $arrValues['cached_payments_total'] ) ) $this->setCachedPaymentsTotal( $arrValues['cached_payments_total'] );
		if( isset( $arrValues['cached_charges_total'] ) && $boolDirectSet ) $this->set( 'm_fltCachedChargesTotal', trim( $arrValues['cached_charges_total'] ) ); elseif( isset( $arrValues['cached_charges_total'] ) ) $this->setCachedChargesTotal( $arrValues['cached_charges_total'] );
		if( isset( $arrValues['export_batch_memo'] ) && $boolDirectSet ) $this->set( 'm_strExportBatchMemo', trim( stripcslashes( $arrValues['export_batch_memo'] ) ) ); elseif( isset( $arrValues['export_batch_memo'] ) ) $this->setExportBatchMemo( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['export_batch_memo'] ) : $arrValues['export_batch_memo'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setExportBatchTypeId( $intExportBatchTypeId ) {
		$this->set( 'm_intExportBatchTypeId', CStrings::strToIntDef( $intExportBatchTypeId, NULL, false ) );
	}

	public function getExportBatchTypeId() {
		return $this->m_intExportBatchTypeId;
	}

	public function sqlExportBatchTypeId() {
		return ( true == isset( $this->m_intExportBatchTypeId ) ) ? ( string ) $this->m_intExportBatchTypeId : 'NULL';
	}

	public function setCurrencyCode( $strCurrencyCode ) {
		$this->set( 'm_strCurrencyCode', CStrings::strTrimDef( $strCurrencyCode, 3, NULL, true ) );
	}

	public function getCurrencyCode() {
		return $this->m_strCurrencyCode;
	}

	public function sqlCurrencyCode() {
		return ( true == isset( $this->m_strCurrencyCode ) ) ? '\'' . addslashes( $this->m_strCurrencyCode ) . '\'' : '\'USD\'';
	}

	public function setRemotePrimaryKey( $strRemotePrimaryKey ) {
		$this->set( 'm_strRemotePrimaryKey', CStrings::strTrimDef( $strRemotePrimaryKey, 64, NULL, true ) );
	}

	public function getRemotePrimaryKey() {
		return $this->m_strRemotePrimaryKey;
	}

	public function sqlRemotePrimaryKey() {
		return ( true == isset( $this->m_strRemotePrimaryKey ) ) ? '\'' . addslashes( $this->m_strRemotePrimaryKey ) . '\'' : 'NULL';
	}

	public function setExportBatchDate( $strExportBatchDate ) {
		$this->set( 'm_strExportBatchDate', CStrings::strTrimDef( $strExportBatchDate, -1, NULL, true ) );
	}

	public function getExportBatchDate() {
		return $this->m_strExportBatchDate;
	}

	public function sqlExportBatchDate() {
		return ( true == isset( $this->m_strExportBatchDate ) ) ? '\'' . $this->m_strExportBatchDate . '\'' : 'NULL';
	}

	public function setCachedArBalance( $fltCachedArBalance ) {
		$this->set( 'm_fltCachedArBalance', CStrings::strToFloatDef( $fltCachedArBalance, NULL, false, 2 ) );
	}

	public function getCachedArBalance() {
		return $this->m_fltCachedArBalance;
	}

	public function sqlCachedArBalance() {
		return ( true == isset( $this->m_fltCachedArBalance ) ) ? ( string ) $this->m_fltCachedArBalance : '0';
	}

	public function setCachedPaymentsTotal( $fltCachedPaymentsTotal ) {
		$this->set( 'm_fltCachedPaymentsTotal', CStrings::strToFloatDef( $fltCachedPaymentsTotal, NULL, false, 2 ) );
	}

	public function getCachedPaymentsTotal() {
		return $this->m_fltCachedPaymentsTotal;
	}

	public function sqlCachedPaymentsTotal() {
		return ( true == isset( $this->m_fltCachedPaymentsTotal ) ) ? ( string ) $this->m_fltCachedPaymentsTotal : '0';
	}

	public function setCachedChargesTotal( $fltCachedChargesTotal ) {
		$this->set( 'm_fltCachedChargesTotal', CStrings::strToFloatDef( $fltCachedChargesTotal, NULL, false, 2 ) );
	}

	public function getCachedChargesTotal() {
		return $this->m_fltCachedChargesTotal;
	}

	public function sqlCachedChargesTotal() {
		return ( true == isset( $this->m_fltCachedChargesTotal ) ) ? ( string ) $this->m_fltCachedChargesTotal : '0';
	}

	public function setExportBatchMemo( $strExportBatchMemo ) {
		$this->set( 'm_strExportBatchMemo', CStrings::strTrimDef( $strExportBatchMemo, 2000, NULL, true ) );
	}

	public function getExportBatchMemo() {
		return $this->m_strExportBatchMemo;
	}

	public function sqlExportBatchMemo() {
		return ( true == isset( $this->m_strExportBatchMemo ) ) ? '\'' . addslashes( $this->m_strExportBatchMemo ) . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, export_batch_type_id, currency_code, remote_primary_key, export_batch_date, cached_ar_balance, cached_payments_total, cached_charges_total, export_batch_memo, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlExportBatchTypeId() . ', ' .
 						$this->sqlCurrencyCode() . ', ' .
 						$this->sqlRemotePrimaryKey() . ', ' .
 						$this->sqlExportBatchDate() . ', ' .
 						$this->sqlCachedArBalance() . ', ' .
 						$this->sqlCachedPaymentsTotal() . ', ' .
 						$this->sqlCachedChargesTotal() . ', ' .
 						$this->sqlExportBatchMemo() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_batch_type_id = ' . $this->sqlExportBatchTypeId() . ','; } elseif( true == array_key_exists( 'ExportBatchTypeId', $this->getChangedColumns() ) ) { $strSql .= ' export_batch_type_id = ' . $this->sqlExportBatchTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; } elseif( true == array_key_exists( 'CurrencyCode', $this->getChangedColumns() ) ) { $strSql .= ' currency_code = ' . $this->sqlCurrencyCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; } elseif( true == array_key_exists( 'RemotePrimaryKey', $this->getChangedColumns() ) ) { $strSql .= ' remote_primary_key = ' . $this->sqlRemotePrimaryKey() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_batch_date = ' . $this->sqlExportBatchDate() . ','; } elseif( true == array_key_exists( 'ExportBatchDate', $this->getChangedColumns() ) ) { $strSql .= ' export_batch_date = ' . $this->sqlExportBatchDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cached_ar_balance = ' . $this->sqlCachedArBalance() . ','; } elseif( true == array_key_exists( 'CachedArBalance', $this->getChangedColumns() ) ) { $strSql .= ' cached_ar_balance = ' . $this->sqlCachedArBalance() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cached_payments_total = ' . $this->sqlCachedPaymentsTotal() . ','; } elseif( true == array_key_exists( 'CachedPaymentsTotal', $this->getChangedColumns() ) ) { $strSql .= ' cached_payments_total = ' . $this->sqlCachedPaymentsTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cached_charges_total = ' . $this->sqlCachedChargesTotal() . ','; } elseif( true == array_key_exists( 'CachedChargesTotal', $this->getChangedColumns() ) ) { $strSql .= ' cached_charges_total = ' . $this->sqlCachedChargesTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' export_batch_memo = ' . $this->sqlExportBatchMemo() . ','; } elseif( true == array_key_exists( 'ExportBatchMemo', $this->getChangedColumns() ) ) { $strSql .= ' export_batch_memo = ' . $this->sqlExportBatchMemo() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'export_batch_type_id' => $this->getExportBatchTypeId(),
			'currency_code' => $this->getCurrencyCode(),
			'remote_primary_key' => $this->getRemotePrimaryKey(),
			'export_batch_date' => $this->getExportBatchDate(),
			'cached_ar_balance' => $this->getCachedArBalance(),
			'cached_payments_total' => $this->getCachedPaymentsTotal(),
			'cached_charges_total' => $this->getCachedChargesTotal(),
			'export_batch_memo' => $this->getExportBatchMemo(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>