<?php

class CBaseReimbursementExpense extends CEosSingularBase {

	const TABLE_NAME = 'public.reimbursement_expenses';

	protected $m_intId;
	protected $m_intReimbursementRequestId;
	protected $m_intEmployeeExpenseTypeId;
	protected $m_intDepartmentId;
	protected $m_intEmployeeId;
	protected $m_strVendorName;
	protected $m_strPurposeEncrypted;
	protected $m_strAmountEncrypted;
	protected $m_strNotes;
	protected $m_strPurchasedDate;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['reimbursement_request_id'] ) && $boolDirectSet ) $this->set( 'm_intReimbursementRequestId', trim( $arrValues['reimbursement_request_id'] ) ); elseif( isset( $arrValues['reimbursement_request_id'] ) ) $this->setReimbursementRequestId( $arrValues['reimbursement_request_id'] );
		if( isset( $arrValues['employee_expense_type_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeExpenseTypeId', trim( $arrValues['employee_expense_type_id'] ) ); elseif( isset( $arrValues['employee_expense_type_id'] ) ) $this->setEmployeeExpenseTypeId( $arrValues['employee_expense_type_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['vendor_name'] ) && $boolDirectSet ) $this->set( 'm_strVendorName', trim( stripcslashes( $arrValues['vendor_name'] ) ) ); elseif( isset( $arrValues['vendor_name'] ) ) $this->setVendorName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['vendor_name'] ) : $arrValues['vendor_name'] );
		if( isset( $arrValues['purpose_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strPurposeEncrypted', trim( stripcslashes( $arrValues['purpose_encrypted'] ) ) ); elseif( isset( $arrValues['purpose_encrypted'] ) ) $this->setPurposeEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['purpose_encrypted'] ) : $arrValues['purpose_encrypted'] );
		if( isset( $arrValues['amount_encrypted'] ) && $boolDirectSet ) $this->set( 'm_strAmountEncrypted', trim( stripcslashes( $arrValues['amount_encrypted'] ) ) ); elseif( isset( $arrValues['amount_encrypted'] ) ) $this->setAmountEncrypted( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['amount_encrypted'] ) : $arrValues['amount_encrypted'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( stripcslashes( $arrValues['notes'] ) ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['notes'] ) : $arrValues['notes'] );
		if( isset( $arrValues['purchased_date'] ) && $boolDirectSet ) $this->set( 'm_strPurchasedDate', trim( $arrValues['purchased_date'] ) ); elseif( isset( $arrValues['purchased_date'] ) ) $this->setPurchasedDate( $arrValues['purchased_date'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setReimbursementRequestId( $intReimbursementRequestId ) {
		$this->set( 'm_intReimbursementRequestId', CStrings::strToIntDef( $intReimbursementRequestId, NULL, false ) );
	}

	public function getReimbursementRequestId() {
		return $this->m_intReimbursementRequestId;
	}

	public function sqlReimbursementRequestId() {
		return ( true == isset( $this->m_intReimbursementRequestId ) ) ? ( string ) $this->m_intReimbursementRequestId : 'NULL';
	}

	public function setEmployeeExpenseTypeId( $intEmployeeExpenseTypeId ) {
		$this->set( 'm_intEmployeeExpenseTypeId', CStrings::strToIntDef( $intEmployeeExpenseTypeId, NULL, false ) );
	}

	public function getEmployeeExpenseTypeId() {
		return $this->m_intEmployeeExpenseTypeId;
	}

	public function sqlEmployeeExpenseTypeId() {
		return ( true == isset( $this->m_intEmployeeExpenseTypeId ) ) ? ( string ) $this->m_intEmployeeExpenseTypeId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setVendorName( $strVendorName ) {
		$this->set( 'm_strVendorName', CStrings::strTrimDef( $strVendorName, 150, NULL, true ) );
	}

	public function getVendorName() {
		return $this->m_strVendorName;
	}

	public function sqlVendorName() {
		return ( true == isset( $this->m_strVendorName ) ) ? '\'' . addslashes( $this->m_strVendorName ) . '\'' : 'NULL';
	}

	public function setPurposeEncrypted( $strPurposeEncrypted ) {
		$this->set( 'm_strPurposeEncrypted', CStrings::strTrimDef( $strPurposeEncrypted, -1, NULL, true ) );
	}

	public function getPurposeEncrypted() {
		return $this->m_strPurposeEncrypted;
	}

	public function sqlPurposeEncrypted() {
		return ( true == isset( $this->m_strPurposeEncrypted ) ) ? '\'' . addslashes( $this->m_strPurposeEncrypted ) . '\'' : 'NULL';
	}

	public function setAmountEncrypted( $strAmountEncrypted ) {
		$this->set( 'm_strAmountEncrypted', CStrings::strTrimDef( $strAmountEncrypted, -1, NULL, true ) );
	}

	public function getAmountEncrypted() {
		return $this->m_strAmountEncrypted;
	}

	public function sqlAmountEncrypted() {
		return ( true == isset( $this->m_strAmountEncrypted ) ) ? '\'' . addslashes( $this->m_strAmountEncrypted ) . '\'' : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? '\'' . addslashes( $this->m_strNotes ) . '\'' : 'NULL';
	}

	public function setPurchasedDate( $strPurchasedDate ) {
		$this->set( 'm_strPurchasedDate', CStrings::strTrimDef( $strPurchasedDate, -1, NULL, true ) );
	}

	public function getPurchasedDate() {
		return $this->m_strPurchasedDate;
	}

	public function sqlPurchasedDate() {
		return ( true == isset( $this->m_strPurchasedDate ) ) ? '\'' . $this->m_strPurchasedDate . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, reimbursement_request_id, employee_expense_type_id, department_id, employee_id, vendor_name, purpose_encrypted, amount_encrypted, notes, purchased_date, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlReimbursementRequestId() . ', ' .
 						$this->sqlEmployeeExpenseTypeId() . ', ' .
 						$this->sqlDepartmentId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlVendorName() . ', ' .
 						$this->sqlPurposeEncrypted() . ', ' .
 						$this->sqlAmountEncrypted() . ', ' .
 						$this->sqlNotes() . ', ' .
 						$this->sqlPurchasedDate() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reimbursement_request_id = ' . $this->sqlReimbursementRequestId() . ','; } elseif( true == array_key_exists( 'ReimbursementRequestId', $this->getChangedColumns() ) ) { $strSql .= ' reimbursement_request_id = ' . $this->sqlReimbursementRequestId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_expense_type_id = ' . $this->sqlEmployeeExpenseTypeId() . ','; } elseif( true == array_key_exists( 'EmployeeExpenseTypeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_expense_type_id = ' . $this->sqlEmployeeExpenseTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' vendor_name = ' . $this->sqlVendorName() . ','; } elseif( true == array_key_exists( 'VendorName', $this->getChangedColumns() ) ) { $strSql .= ' vendor_name = ' . $this->sqlVendorName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purpose_encrypted = ' . $this->sqlPurposeEncrypted() . ','; } elseif( true == array_key_exists( 'PurposeEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' purpose_encrypted = ' . $this->sqlPurposeEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' amount_encrypted = ' . $this->sqlAmountEncrypted() . ','; } elseif( true == array_key_exists( 'AmountEncrypted', $this->getChangedColumns() ) ) { $strSql .= ' amount_encrypted = ' . $this->sqlAmountEncrypted() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' purchased_date = ' . $this->sqlPurchasedDate() . ','; } elseif( true == array_key_exists( 'PurchasedDate', $this->getChangedColumns() ) ) { $strSql .= ' purchased_date = ' . $this->sqlPurchasedDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'reimbursement_request_id' => $this->getReimbursementRequestId(),
			'employee_expense_type_id' => $this->getEmployeeExpenseTypeId(),
			'department_id' => $this->getDepartmentId(),
			'employee_id' => $this->getEmployeeId(),
			'vendor_name' => $this->getVendorName(),
			'purpose_encrypted' => $this->getPurposeEncrypted(),
			'amount_encrypted' => $this->getAmountEncrypted(),
			'notes' => $this->getNotes(),
			'purchased_date' => $this->getPurchasedDate(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>