<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeSecurityPolicies
 * Do not add any new functions to this class.
 */

class CBaseEmployeeSecurityPolicies extends CEosPluralBase {

	/**
	 * @return CEmployeeSecurityPolicy[]
	 */
	public static function fetchEmployeeSecurityPolicies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeSecurityPolicy', $objDatabase );
	}

	/**
	 * @return CEmployeeSecurityPolicy
	 */
	public static function fetchEmployeeSecurityPolicy( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeSecurityPolicy', $objDatabase );
	}

	public static function fetchEmployeeSecurityPolicyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_security_policies', $objDatabase );
	}

	public static function fetchEmployeeSecurityPolicyById( $intId, $objDatabase ) {
		return self::fetchEmployeeSecurityPolicy( sprintf( 'SELECT * FROM employee_security_policies WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeSecurityPoliciesByEmployeeId( $intEmployeeId, $objDatabase ) {
		return self::fetchEmployeeSecurityPolicies( sprintf( 'SELECT * FROM employee_security_policies WHERE employee_id = %d', ( int ) $intEmployeeId ), $objDatabase );
	}

}
?>