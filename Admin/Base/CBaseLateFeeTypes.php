<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CLateFeeTypes
 * Do not add any new functions to this class.
 */

class CBaseLateFeeTypes extends CEosPluralBase {

	/**
	 * @return CLateFeeType[]
	 */
	public static function fetchLateFeeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CLateFeeType', $objDatabase );
	}

	/**
	 * @return CLateFeeType
	 */
	public static function fetchLateFeeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CLateFeeType', $objDatabase );
	}

	public static function fetchLateFeeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'late_fee_types', $objDatabase );
	}

	public static function fetchLateFeeTypeById( $intId, $objDatabase ) {
		return self::fetchLateFeeType( sprintf( 'SELECT * FROM late_fee_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>