<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserActivityAccessLogs
 * Do not add any new functions to this class.
 */

class CBaseUserActivityAccessLogs extends CEosPluralBase {

	/**
	 * @return CUserActivityAccessLog[]
	 */
	public static function fetchUserActivityAccessLogs( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CUserActivityAccessLog', $objDatabase );
	}

	/**
	 * @return CUserActivityAccessLog
	 */
	public static function fetchUserActivityAccessLog( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CUserActivityAccessLog', $objDatabase );
	}

	public static function fetchUserActivityAccessLogCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'user_activity_access_logs', $objDatabase );
	}

	public static function fetchUserActivityAccessLogById( $intId, $objDatabase ) {
		return self::fetchUserActivityAccessLog( sprintf( 'SELECT * FROM user_activity_access_logs WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchUserActivityAccessLogsByUserId( $intUserId, $objDatabase ) {
		return self::fetchUserActivityAccessLogs( sprintf( 'SELECT * FROM user_activity_access_logs WHERE user_id = %d', ( int ) $intUserId ), $objDatabase );
	}

}
?>