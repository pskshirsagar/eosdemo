<?php

class CBaseEmployeeApplicationNote extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_application_notes';

	protected $m_intId;
	protected $m_intEmployeeApplicationId;
	protected $m_intNoteTypeId;
	protected $m_strNoteDatetime;
	protected $m_strNote;
	protected $m_intAddedBy;
	protected $m_strAddedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_application_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeApplicationId', trim( $arrValues['employee_application_id'] ) ); elseif( isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( isset( $arrValues['note_type_id'] ) && $boolDirectSet ) $this->set( 'm_intNoteTypeId', trim( $arrValues['note_type_id'] ) ); elseif( isset( $arrValues['note_type_id'] ) ) $this->setNoteTypeId( $arrValues['note_type_id'] );
		if( isset( $arrValues['note_datetime'] ) && $boolDirectSet ) $this->set( 'm_strNoteDatetime', trim( $arrValues['note_datetime'] ) ); elseif( isset( $arrValues['note_datetime'] ) ) $this->setNoteDatetime( $arrValues['note_datetime'] );
		if( isset( $arrValues['note'] ) && $boolDirectSet ) $this->set( 'm_strNote', trim( stripcslashes( $arrValues['note'] ) ) ); elseif( isset( $arrValues['note'] ) ) $this->setNote( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['note'] ) : $arrValues['note'] );
		if( isset( $arrValues['added_by'] ) && $boolDirectSet ) $this->set( 'm_intAddedBy', trim( $arrValues['added_by'] ) ); elseif( isset( $arrValues['added_by'] ) ) $this->setAddedBy( $arrValues['added_by'] );
		if( isset( $arrValues['added_on'] ) && $boolDirectSet ) $this->set( 'm_strAddedOn', trim( $arrValues['added_on'] ) ); elseif( isset( $arrValues['added_on'] ) ) $this->setAddedOn( $arrValues['added_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeApplicationId( $intEmployeeApplicationId ) {
		$this->set( 'm_intEmployeeApplicationId', CStrings::strToIntDef( $intEmployeeApplicationId, NULL, false ) );
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function sqlEmployeeApplicationId() {
		return ( true == isset( $this->m_intEmployeeApplicationId ) ) ? ( string ) $this->m_intEmployeeApplicationId : 'NULL';
	}

	public function setNoteTypeId( $intNoteTypeId ) {
		$this->set( 'm_intNoteTypeId', CStrings::strToIntDef( $intNoteTypeId, NULL, false ) );
	}

	public function getNoteTypeId() {
		return $this->m_intNoteTypeId;
	}

	public function sqlNoteTypeId() {
		return ( true == isset( $this->m_intNoteTypeId ) ) ? ( string ) $this->m_intNoteTypeId : 'NULL';
	}

	public function setNoteDatetime( $strNoteDatetime ) {
		$this->set( 'm_strNoteDatetime', CStrings::strTrimDef( $strNoteDatetime, -1, NULL, true ) );
	}

	public function getNoteDatetime() {
		return $this->m_strNoteDatetime;
	}

	public function sqlNoteDatetime() {
		return ( true == isset( $this->m_strNoteDatetime ) ) ? '\'' . $this->m_strNoteDatetime . '\'' : 'NOW()';
	}

	public function setNote( $strNote ) {
		$this->set( 'm_strNote', CStrings::strTrimDef( $strNote, -1, NULL, true ) );
	}

	public function getNote() {
		return $this->m_strNote;
	}

	public function sqlNote() {
		return ( true == isset( $this->m_strNote ) ) ? '\'' . addslashes( $this->m_strNote ) . '\'' : 'NULL';
	}

	public function setAddedBy( $intAddedBy ) {
		$this->set( 'm_intAddedBy', CStrings::strToIntDef( $intAddedBy, NULL, false ) );
	}

	public function getAddedBy() {
		return $this->m_intAddedBy;
	}

	public function sqlAddedBy() {
		return ( true == isset( $this->m_intAddedBy ) ) ? ( string ) $this->m_intAddedBy : 'NULL';
	}

	public function setAddedOn( $strAddedOn ) {
		$this->set( 'm_strAddedOn', CStrings::strTrimDef( $strAddedOn, -1, NULL, true ) );
	}

	public function getAddedOn() {
		return $this->m_strAddedOn;
	}

	public function sqlAddedOn() {
		return ( true == isset( $this->m_strAddedOn ) ) ? '\'' . $this->m_strAddedOn . '\'' : 'NOW()';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_application_id, note_type_id, note_datetime, note, added_by, added_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeApplicationId() . ', ' .
 						$this->sqlNoteTypeId() . ', ' .
 						$this->sqlNoteDatetime() . ', ' .
 						$this->sqlNote() . ', ' .
 						$this->sqlAddedBy() . ', ' .
 						$this->sqlAddedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; } elseif( true == array_key_exists( 'EmployeeApplicationId', $this->getChangedColumns() ) ) { $strSql .= ' employee_application_id = ' . $this->sqlEmployeeApplicationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note_type_id = ' . $this->sqlNoteTypeId() . ','; } elseif( true == array_key_exists( 'NoteTypeId', $this->getChangedColumns() ) ) { $strSql .= ' note_type_id = ' . $this->sqlNoteTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note_datetime = ' . $this->sqlNoteDatetime() . ','; } elseif( true == array_key_exists( 'NoteDatetime', $this->getChangedColumns() ) ) { $strSql .= ' note_datetime = ' . $this->sqlNoteDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' note = ' . $this->sqlNote() . ','; } elseif( true == array_key_exists( 'Note', $this->getChangedColumns() ) ) { $strSql .= ' note = ' . $this->sqlNote() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' added_by = ' . $this->sqlAddedBy() . ','; } elseif( true == array_key_exists( 'AddedBy', $this->getChangedColumns() ) ) { $strSql .= ' added_by = ' . $this->sqlAddedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' added_on = ' . $this->sqlAddedOn() . ','; } elseif( true == array_key_exists( 'AddedOn', $this->getChangedColumns() ) ) { $strSql .= ' added_on = ' . $this->sqlAddedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_application_id' => $this->getEmployeeApplicationId(),
			'note_type_id' => $this->getNoteTypeId(),
			'note_datetime' => $this->getNoteDatetime(),
			'note' => $this->getNote(),
			'added_by' => $this->getAddedBy(),
			'added_on' => $this->getAddedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>