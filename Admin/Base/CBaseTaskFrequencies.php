<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskFrequencies
 * Do not add any new functions to this class.
 */

class CBaseTaskFrequencies extends CEosPluralBase {

	/**
	 * @return CTaskFrequency[]
	 */
	public static function fetchTaskFrequencies( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CTaskFrequency', $objDatabase );
	}

	/**
	 * @return CTaskFrequency
	 */
	public static function fetchTaskFrequency( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CTaskFrequency', $objDatabase );
	}

	public static function fetchTaskFrequencyCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'task_frequencies', $objDatabase );
	}

	public static function fetchTaskFrequencyById( $intId, $objDatabase ) {
		return self::fetchTaskFrequency( sprintf( 'SELECT * FROM task_frequencies WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>