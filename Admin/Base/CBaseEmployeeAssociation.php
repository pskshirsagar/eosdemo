<?php

class CBaseEmployeeAssociation extends CEosSingularBase {

	const TABLE_NAME = 'public.employee_associations';

	protected $m_intId;
	protected $m_intPsLeadId;
	protected $m_intEmployeeId;
	protected $m_intDepartmentId;
	protected $m_strAssociationDatetime;
	protected $m_intAcceptedBy;
	protected $m_strAcceptedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['association_datetime'] ) && $boolDirectSet ) $this->set( 'm_strAssociationDatetime', trim( $arrValues['association_datetime'] ) ); elseif( isset( $arrValues['association_datetime'] ) ) $this->setAssociationDatetime( $arrValues['association_datetime'] );
		if( isset( $arrValues['accepted_by'] ) && $boolDirectSet ) $this->set( 'm_intAcceptedBy', trim( $arrValues['accepted_by'] ) ); elseif( isset( $arrValues['accepted_by'] ) ) $this->setAcceptedBy( $arrValues['accepted_by'] );
		if( isset( $arrValues['accepted_on'] ) && $boolDirectSet ) $this->set( 'm_strAcceptedOn', trim( $arrValues['accepted_on'] ) ); elseif( isset( $arrValues['accepted_on'] ) ) $this->setAcceptedOn( $arrValues['accepted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setAssociationDatetime( $strAssociationDatetime ) {
		$this->set( 'm_strAssociationDatetime', CStrings::strTrimDef( $strAssociationDatetime, -1, NULL, true ) );
	}

	public function getAssociationDatetime() {
		return $this->m_strAssociationDatetime;
	}

	public function sqlAssociationDatetime() {
		return ( true == isset( $this->m_strAssociationDatetime ) ) ? '\'' . $this->m_strAssociationDatetime . '\'' : 'NOW()';
	}

	public function setAcceptedBy( $intAcceptedBy ) {
		$this->set( 'm_intAcceptedBy', CStrings::strToIntDef( $intAcceptedBy, NULL, false ) );
	}

	public function getAcceptedBy() {
		return $this->m_intAcceptedBy;
	}

	public function sqlAcceptedBy() {
		return ( true == isset( $this->m_intAcceptedBy ) ) ? ( string ) $this->m_intAcceptedBy : 'NULL';
	}

	public function setAcceptedOn( $strAcceptedOn ) {
		$this->set( 'm_strAcceptedOn', CStrings::strTrimDef( $strAcceptedOn, -1, NULL, true ) );
	}

	public function getAcceptedOn() {
		return $this->m_strAcceptedOn;
	}

	public function sqlAcceptedOn() {
		return ( true == isset( $this->m_strAcceptedOn ) ) ? '\'' . $this->m_strAcceptedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, ps_lead_id, employee_id, department_id, association_datetime, accepted_by, accepted_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlPsLeadId() . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlDepartmentId() . ', ' .
 						$this->sqlAssociationDatetime() . ', ' .
 						$this->sqlAcceptedBy() . ', ' .
 						$this->sqlAcceptedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' association_datetime = ' . $this->sqlAssociationDatetime() . ','; } elseif( true == array_key_exists( 'AssociationDatetime', $this->getChangedColumns() ) ) { $strSql .= ' association_datetime = ' . $this->sqlAssociationDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy() . ','; } elseif( true == array_key_exists( 'AcceptedBy', $this->getChangedColumns() ) ) { $strSql .= ' accepted_by = ' . $this->sqlAcceptedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn() . ','; } elseif( true == array_key_exists( 'AcceptedOn', $this->getChangedColumns() ) ) { $strSql .= ' accepted_on = ' . $this->sqlAcceptedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'employee_id' => $this->getEmployeeId(),
			'department_id' => $this->getDepartmentId(),
			'association_datetime' => $this->getAssociationDatetime(),
			'accepted_by' => $this->getAcceptedBy(),
			'accepted_on' => $this->getAcceptedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>