<?php

class CBaseAgreementTemplate extends CEosSingularBase {

	const TABLE_NAME = 'public.agreement_templates';

	protected $m_intId;
	protected $m_intAgreementTemplateTypeId;
	protected $m_intParentAgreementTemplateId;
	protected $m_intPsProductId;
	protected $m_strName;
	protected $m_strContent;
	protected $m_strChangeDescription;
	protected $m_strTemplateDatetime;
	protected $m_intIsCurrent;
	protected $m_intIsIncluded;
	protected $m_intOrderNum;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsCurrent = '1';
		$this->m_intIsIncluded = '0';
		$this->m_intOrderNum = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['agreement_template_type_id'] ) && $boolDirectSet ) $this->set( 'm_intAgreementTemplateTypeId', trim( $arrValues['agreement_template_type_id'] ) ); elseif( isset( $arrValues['agreement_template_type_id'] ) ) $this->setAgreementTemplateTypeId( $arrValues['agreement_template_type_id'] );
		if( isset( $arrValues['parent_agreement_template_id'] ) && $boolDirectSet ) $this->set( 'm_intParentAgreementTemplateId', trim( $arrValues['parent_agreement_template_id'] ) ); elseif( isset( $arrValues['parent_agreement_template_id'] ) ) $this->setParentAgreementTemplateId( $arrValues['parent_agreement_template_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['content'] ) && $boolDirectSet ) $this->set( 'm_strContent', trim( stripcslashes( $arrValues['content'] ) ) ); elseif( isset( $arrValues['content'] ) ) $this->setContent( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['content'] ) : $arrValues['content'] );
		if( isset( $arrValues['change_description'] ) && $boolDirectSet ) $this->set( 'm_strChangeDescription', trim( stripcslashes( $arrValues['change_description'] ) ) ); elseif( isset( $arrValues['change_description'] ) ) $this->setChangeDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['change_description'] ) : $arrValues['change_description'] );
		if( isset( $arrValues['template_datetime'] ) && $boolDirectSet ) $this->set( 'm_strTemplateDatetime', trim( $arrValues['template_datetime'] ) ); elseif( isset( $arrValues['template_datetime'] ) ) $this->setTemplateDatetime( $arrValues['template_datetime'] );
		if( isset( $arrValues['is_current'] ) && $boolDirectSet ) $this->set( 'm_intIsCurrent', trim( $arrValues['is_current'] ) ); elseif( isset( $arrValues['is_current'] ) ) $this->setIsCurrent( $arrValues['is_current'] );
		if( isset( $arrValues['is_included'] ) && $boolDirectSet ) $this->set( 'm_intIsIncluded', trim( $arrValues['is_included'] ) ); elseif( isset( $arrValues['is_included'] ) ) $this->setIsIncluded( $arrValues['is_included'] );
		if( isset( $arrValues['order_num'] ) && $boolDirectSet ) $this->set( 'm_intOrderNum', trim( $arrValues['order_num'] ) ); elseif( isset( $arrValues['order_num'] ) ) $this->setOrderNum( $arrValues['order_num'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setAgreementTemplateTypeId( $intAgreementTemplateTypeId ) {
		$this->set( 'm_intAgreementTemplateTypeId', CStrings::strToIntDef( $intAgreementTemplateTypeId, NULL, false ) );
	}

	public function getAgreementTemplateTypeId() {
		return $this->m_intAgreementTemplateTypeId;
	}

	public function sqlAgreementTemplateTypeId() {
		return ( true == isset( $this->m_intAgreementTemplateTypeId ) ) ? ( string ) $this->m_intAgreementTemplateTypeId : 'NULL';
	}

	public function setParentAgreementTemplateId( $intParentAgreementTemplateId ) {
		$this->set( 'm_intParentAgreementTemplateId', CStrings::strToIntDef( $intParentAgreementTemplateId, NULL, false ) );
	}

	public function getParentAgreementTemplateId() {
		return $this->m_intParentAgreementTemplateId;
	}

	public function sqlParentAgreementTemplateId() {
		return ( true == isset( $this->m_intParentAgreementTemplateId ) ) ? ( string ) $this->m_intParentAgreementTemplateId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 50, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setContent( $strContent ) {
		$this->set( 'm_strContent', CStrings::strTrimDef( $strContent, -1, NULL, true ) );
	}

	public function getContent() {
		return $this->m_strContent;
	}

	public function sqlContent() {
		return ( true == isset( $this->m_strContent ) ) ? '\'' . addslashes( $this->m_strContent ) . '\'' : 'NULL';
	}

	public function setChangeDescription( $strChangeDescription ) {
		$this->set( 'm_strChangeDescription', CStrings::strTrimDef( $strChangeDescription, 240, NULL, true ) );
	}

	public function getChangeDescription() {
		return $this->m_strChangeDescription;
	}

	public function sqlChangeDescription() {
		return ( true == isset( $this->m_strChangeDescription ) ) ? '\'' . addslashes( $this->m_strChangeDescription ) . '\'' : 'NULL';
	}

	public function setTemplateDatetime( $strTemplateDatetime ) {
		$this->set( 'm_strTemplateDatetime', CStrings::strTrimDef( $strTemplateDatetime, -1, NULL, true ) );
	}

	public function getTemplateDatetime() {
		return $this->m_strTemplateDatetime;
	}

	public function sqlTemplateDatetime() {
		return ( true == isset( $this->m_strTemplateDatetime ) ) ? '\'' . $this->m_strTemplateDatetime . '\'' : 'NOW()';
	}

	public function setIsCurrent( $intIsCurrent ) {
		$this->set( 'm_intIsCurrent', CStrings::strToIntDef( $intIsCurrent, NULL, false ) );
	}

	public function getIsCurrent() {
		return $this->m_intIsCurrent;
	}

	public function sqlIsCurrent() {
		return ( true == isset( $this->m_intIsCurrent ) ) ? ( string ) $this->m_intIsCurrent : '1';
	}

	public function setIsIncluded( $intIsIncluded ) {
		$this->set( 'm_intIsIncluded', CStrings::strToIntDef( $intIsIncluded, NULL, false ) );
	}

	public function getIsIncluded() {
		return $this->m_intIsIncluded;
	}

	public function sqlIsIncluded() {
		return ( true == isset( $this->m_intIsIncluded ) ) ? ( string ) $this->m_intIsIncluded : '0';
	}

	public function setOrderNum( $intOrderNum ) {
		$this->set( 'm_intOrderNum', CStrings::strToIntDef( $intOrderNum, NULL, false ) );
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	public function sqlOrderNum() {
		return ( true == isset( $this->m_intOrderNum ) ) ? ( string ) $this->m_intOrderNum : '0';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, agreement_template_type_id, parent_agreement_template_id, ps_product_id, name, content, change_description, template_datetime, is_current, is_included, order_num, deleted_by, deleted_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlAgreementTemplateTypeId() . ', ' .
 						$this->sqlParentAgreementTemplateId() . ', ' .
 						$this->sqlPsProductId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlContent() . ', ' .
 						$this->sqlChangeDescription() . ', ' .
 						$this->sqlTemplateDatetime() . ', ' .
 						$this->sqlIsCurrent() . ', ' .
 						$this->sqlIsIncluded() . ', ' .
 						$this->sqlOrderNum() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' agreement_template_type_id = ' . $this->sqlAgreementTemplateTypeId() . ','; } elseif( true == array_key_exists( 'AgreementTemplateTypeId', $this->getChangedColumns() ) ) { $strSql .= ' agreement_template_type_id = ' . $this->sqlAgreementTemplateTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_agreement_template_id = ' . $this->sqlParentAgreementTemplateId() . ','; } elseif( true == array_key_exists( 'ParentAgreementTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' parent_agreement_template_id = ' . $this->sqlParentAgreementTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' content = ' . $this->sqlContent() . ','; } elseif( true == array_key_exists( 'Content', $this->getChangedColumns() ) ) { $strSql .= ' content = ' . $this->sqlContent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' change_description = ' . $this->sqlChangeDescription() . ','; } elseif( true == array_key_exists( 'ChangeDescription', $this->getChangedColumns() ) ) { $strSql .= ' change_description = ' . $this->sqlChangeDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' template_datetime = ' . $this->sqlTemplateDatetime() . ','; } elseif( true == array_key_exists( 'TemplateDatetime', $this->getChangedColumns() ) ) { $strSql .= ' template_datetime = ' . $this->sqlTemplateDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_current = ' . $this->sqlIsCurrent() . ','; } elseif( true == array_key_exists( 'IsCurrent', $this->getChangedColumns() ) ) { $strSql .= ' is_current = ' . $this->sqlIsCurrent() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_included = ' . $this->sqlIsIncluded() . ','; } elseif( true == array_key_exists( 'IsIncluded', $this->getChangedColumns() ) ) { $strSql .= ' is_included = ' . $this->sqlIsIncluded() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; } elseif( true == array_key_exists( 'OrderNum', $this->getChangedColumns() ) ) { $strSql .= ' order_num = ' . $this->sqlOrderNum() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'agreement_template_type_id' => $this->getAgreementTemplateTypeId(),
			'parent_agreement_template_id' => $this->getParentAgreementTemplateId(),
			'ps_product_id' => $this->getPsProductId(),
			'name' => $this->getName(),
			'content' => $this->getContent(),
			'change_description' => $this->getChangeDescription(),
			'template_datetime' => $this->getTemplateDatetime(),
			'is_current' => $this->getIsCurrent(),
			'is_included' => $this->getIsIncluded(),
			'order_num' => $this->getOrderNum(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>