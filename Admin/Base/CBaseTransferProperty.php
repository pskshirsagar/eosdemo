<?php

class CBaseTransferProperty extends CEosSingularBase {

	const TABLE_NAME = 'public.transfer_properties';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intPreviousCid;
	protected $m_intPreviousPropertyId;
	protected $m_intTaskId;
	protected $m_strBillingStartDate;
	protected $m_intPreviousContractTerminationRequestId;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_strUpdatedOn = 'now()';
		$this->m_strCreatedOn = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['previous_cid'] ) && $boolDirectSet ) $this->set( 'm_intPreviousCid', trim( $arrValues['previous_cid'] ) ); elseif( isset( $arrValues['previous_cid'] ) ) $this->setPreviousCid( $arrValues['previous_cid'] );
		if( isset( $arrValues['previous_property_id'] ) && $boolDirectSet ) $this->set( 'm_intPreviousPropertyId', trim( $arrValues['previous_property_id'] ) ); elseif( isset( $arrValues['previous_property_id'] ) ) $this->setPreviousPropertyId( $arrValues['previous_property_id'] );
		if( isset( $arrValues['task_id'] ) && $boolDirectSet ) $this->set( 'm_intTaskId', trim( $arrValues['task_id'] ) ); elseif( isset( $arrValues['task_id'] ) ) $this->setTaskId( $arrValues['task_id'] );
		if( isset( $arrValues['billing_start_date'] ) && $boolDirectSet ) $this->set( 'm_strBillingStartDate', trim( $arrValues['billing_start_date'] ) ); elseif( isset( $arrValues['billing_start_date'] ) ) $this->setBillingStartDate( $arrValues['billing_start_date'] );
		if( isset( $arrValues['previous_contract_termination_request_id'] ) && $boolDirectSet ) $this->set( 'm_intPreviousContractTerminationRequestId', trim( $arrValues['previous_contract_termination_request_id'] ) ); elseif( isset( $arrValues['previous_contract_termination_request_id'] ) ) $this->setPreviousContractTerminationRequestId( $arrValues['previous_contract_termination_request_id'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setPreviousCid( $intPreviousCid ) {
		$this->set( 'm_intPreviousCid', CStrings::strToIntDef( $intPreviousCid, NULL, false ) );
	}

	public function getPreviousCid() {
		return $this->m_intPreviousCid;
	}

	public function sqlPreviousCid() {
		return ( true == isset( $this->m_intPreviousCid ) ) ? ( string ) $this->m_intPreviousCid : 'NULL';
	}

	public function setPreviousPropertyId( $intPreviousPropertyId ) {
		$this->set( 'm_intPreviousPropertyId', CStrings::strToIntDef( $intPreviousPropertyId, NULL, false ) );
	}

	public function getPreviousPropertyId() {
		return $this->m_intPreviousPropertyId;
	}

	public function sqlPreviousPropertyId() {
		return ( true == isset( $this->m_intPreviousPropertyId ) ) ? ( string ) $this->m_intPreviousPropertyId : 'NULL';
	}

	public function setTaskId( $intTaskId ) {
		$this->set( 'm_intTaskId', CStrings::strToIntDef( $intTaskId, NULL, false ) );
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function sqlTaskId() {
		return ( true == isset( $this->m_intTaskId ) ) ? ( string ) $this->m_intTaskId : 'NULL';
	}

	public function setBillingStartDate( $strBillingStartDate ) {
		$this->set( 'm_strBillingStartDate', CStrings::strTrimDef( $strBillingStartDate, -1, NULL, true ) );
	}

	public function getBillingStartDate() {
		return $this->m_strBillingStartDate;
	}

	public function sqlBillingStartDate() {
		return ( true == isset( $this->m_strBillingStartDate ) ) ? '\'' . $this->m_strBillingStartDate . '\'' : 'NULL';
	}

	public function setPreviousContractTerminationRequestId( $intPreviousContractTerminationRequestId ) {
		$this->set( 'm_intPreviousContractTerminationRequestId', CStrings::strToIntDef( $intPreviousContractTerminationRequestId, NULL, false ) );
	}

	public function getPreviousContractTerminationRequestId() {
		return $this->m_intPreviousContractTerminationRequestId;
	}

	public function sqlPreviousContractTerminationRequestId() {
		return ( true == isset( $this->m_intPreviousContractTerminationRequestId ) ) ? ( string ) $this->m_intPreviousContractTerminationRequestId : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, previous_cid, previous_property_id, task_id, billing_start_date, previous_contract_termination_request_id, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlCid() . ', ' .
 						$this->sqlPropertyId() . ', ' .
 						$this->sqlPreviousCid() . ', ' .
 						$this->sqlPreviousPropertyId() . ', ' .
 						$this->sqlTaskId() . ', ' .
 						$this->sqlBillingStartDate() . ', ' .
 						$this->sqlPreviousContractTerminationRequestId() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_cid = ' . $this->sqlPreviousCid() . ','; } elseif( true == array_key_exists( 'PreviousCid', $this->getChangedColumns() ) ) { $strSql .= ' previous_cid = ' . $this->sqlPreviousCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_property_id = ' . $this->sqlPreviousPropertyId() . ','; } elseif( true == array_key_exists( 'PreviousPropertyId', $this->getChangedColumns() ) ) { $strSql .= ' previous_property_id = ' . $this->sqlPreviousPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; } elseif( true == array_key_exists( 'TaskId', $this->getChangedColumns() ) ) { $strSql .= ' task_id = ' . $this->sqlTaskId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' billing_start_date = ' . $this->sqlBillingStartDate() . ','; } elseif( true == array_key_exists( 'BillingStartDate', $this->getChangedColumns() ) ) { $strSql .= ' billing_start_date = ' . $this->sqlBillingStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' previous_contract_termination_request_id = ' . $this->sqlPreviousContractTerminationRequestId() . ','; } elseif( true == array_key_exists( 'PreviousContractTerminationRequestId', $this->getChangedColumns() ) ) { $strSql .= ' previous_contract_termination_request_id = ' . $this->sqlPreviousContractTerminationRequestId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'previous_cid' => $this->getPreviousCid(),
			'previous_property_id' => $this->getPreviousPropertyId(),
			'task_id' => $this->getTaskId(),
			'billing_start_date' => $this->getBillingStartDate(),
			'previous_contract_termination_request_id' => $this->getPreviousContractTerminationRequestId(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>