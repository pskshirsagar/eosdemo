<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CHonourBadgeTypes
 * Do not add any new functions to this class.
 */

class CBaseHonourBadgeTypes extends CEosPluralBase {

	/**
	 * @return CHonourBadgeType[]
	 */
	public static function fetchHonourBadgeTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CHonourBadgeType', $objDatabase );
	}

	/**
	 * @return CHonourBadgeType
	 */
	public static function fetchHonourBadgeType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CHonourBadgeType', $objDatabase );
	}

	public static function fetchHonourBadgeTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'honour_badge_types', $objDatabase );
	}

	public static function fetchHonourBadgeTypeById( $intId, $objDatabase ) {
		return self::fetchHonourBadgeType( sprintf( 'SELECT * FROM honour_badge_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>