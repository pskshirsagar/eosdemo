<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplateTypeLibraries
 * Do not add any new functions to this class.
 */

class CBaseDocumentTemplateTypeLibraries extends CEosPluralBase {

	/**
	 * @return CDocumentTemplateTypeLibrary[]
	 */
	public static function fetchDocumentTemplateTypeLibraries( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CDocumentTemplateTypeLibrary', $objDatabase );
	}

	/**
	 * @return CDocumentTemplateTypeLibrary
	 */
	public static function fetchDocumentTemplateTypeLibrary( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CDocumentTemplateTypeLibrary', $objDatabase );
	}

	public static function fetchDocumentTemplateTypeLibraryCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'document_template_type_libraries', $objDatabase );
	}

	public static function fetchDocumentTemplateTypeLibraryById( $intId, $objDatabase ) {
		return self::fetchDocumentTemplateTypeLibrary( sprintf( 'SELECT * FROM document_template_type_libraries WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchDocumentTemplateTypeLibrariesByDocumentTypeId( $intDocumentTypeId, $objDatabase ) {
		return self::fetchDocumentTemplateTypeLibraries( sprintf( 'SELECT * FROM document_template_type_libraries WHERE document_type_id = %d', ( int ) $intDocumentTypeId ), $objDatabase );
	}

	public static function fetchDocumentTemplateTypeLibrariesByDocumentSubTypeId( $intDocumentSubTypeId, $objDatabase ) {
		return self::fetchDocumentTemplateTypeLibraries( sprintf( 'SELECT * FROM document_template_type_libraries WHERE document_sub_type_id = %d', ( int ) $intDocumentSubTypeId ), $objDatabase );
	}

	public static function fetchDocumentTemplateTypeLibrariesByDocumentTemplateLibraryId( $intDocumentTemplateLibraryId, $objDatabase ) {
		return self::fetchDocumentTemplateTypeLibraries( sprintf( 'SELECT * FROM document_template_type_libraries WHERE document_template_library_id = %d', ( int ) $intDocumentTemplateLibraryId ), $objDatabase );
	}

}
?>