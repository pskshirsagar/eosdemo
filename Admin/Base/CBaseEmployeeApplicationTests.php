<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationTests
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationTests extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationTest[]
	 */
	public static function fetchEmployeeApplicationTests( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationTest', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationTest
	 */
	public static function fetchEmployeeApplicationTest( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationTest', $objDatabase );
	}

	public static function fetchEmployeeApplicationTestCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_tests', $objDatabase );
	}

	public static function fetchEmployeeApplicationTestById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationTest( sprintf( 'SELECT * FROM employee_application_tests WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationTestsByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return self::fetchEmployeeApplicationTests( sprintf( 'SELECT * FROM employee_application_tests WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchEmployeeApplicationTestsByEmployeeApplicationTestTypeId( $intEmployeeApplicationTestTypeId, $objDatabase ) {
		return self::fetchEmployeeApplicationTests( sprintf( 'SELECT * FROM employee_application_tests WHERE employee_application_test_type_id = %d', ( int ) $intEmployeeApplicationTestTypeId ), $objDatabase );
	}

}
?>