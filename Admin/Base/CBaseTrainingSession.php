<?php

class CBaseTrainingSession extends CEosSingularBase {

	const TABLE_NAME = 'public.training_sessions';

	protected $m_intId;
	protected $m_intParentTrainingSessionId;
	protected $m_intTrainingId;
	protected $m_intInductionSessionId;
	protected $m_intTrainingTrainerAssociationId;
	protected $m_intOfficeRoomId;
	protected $m_intSurveyTemplateId;
	protected $m_strGoogleCalendarEventId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_strCountryCode;
	protected $m_strTrainingLocation;
	protected $m_intAvailableSeats;
	protected $m_strScheduledStartTime;
	protected $m_strScheduledEndTime;
	protected $m_strActualStartTime;
	protected $m_strActualEndTime;
	protected $m_intIsPrivate;
	protected $m_intIsPublished;
	protected $m_intIsELearning;
	protected $m_intIsCancelled;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intIsPrivate = '0';
		$this->m_intIsPublished = '0';
		$this->m_intIsELearning = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['parent_training_session_id'] ) && $boolDirectSet ) $this->set( 'm_intParentTrainingSessionId', trim( $arrValues['parent_training_session_id'] ) ); elseif( isset( $arrValues['parent_training_session_id'] ) ) $this->setParentTrainingSessionId( $arrValues['parent_training_session_id'] );
		if( isset( $arrValues['training_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingId', trim( $arrValues['training_id'] ) ); elseif( isset( $arrValues['training_id'] ) ) $this->setTrainingId( $arrValues['training_id'] );
		if( isset( $arrValues['induction_session_id'] ) && $boolDirectSet ) $this->set( 'm_intInductionSessionId', trim( $arrValues['induction_session_id'] ) ); elseif( isset( $arrValues['induction_session_id'] ) ) $this->setInductionSessionId( $arrValues['induction_session_id'] );
		if( isset( $arrValues['training_trainer_association_id'] ) && $boolDirectSet ) $this->set( 'm_intTrainingTrainerAssociationId', trim( $arrValues['training_trainer_association_id'] ) ); elseif( isset( $arrValues['training_trainer_association_id'] ) ) $this->setTrainingTrainerAssociationId( $arrValues['training_trainer_association_id'] );
		if( isset( $arrValues['office_room_id'] ) && $boolDirectSet ) $this->set( 'm_intOfficeRoomId', trim( $arrValues['office_room_id'] ) ); elseif( isset( $arrValues['office_room_id'] ) ) $this->setOfficeRoomId( $arrValues['office_room_id'] );
		if( isset( $arrValues['survey_template_id'] ) && $boolDirectSet ) $this->set( 'm_intSurveyTemplateId', trim( $arrValues['survey_template_id'] ) ); elseif( isset( $arrValues['survey_template_id'] ) ) $this->setSurveyTemplateId( $arrValues['survey_template_id'] );
		if( isset( $arrValues['google_calendar_event_id'] ) && $boolDirectSet ) $this->set( 'm_strGoogleCalendarEventId', trim( stripcslashes( $arrValues['google_calendar_event_id'] ) ) ); elseif( isset( $arrValues['google_calendar_event_id'] ) ) $this->setGoogleCalendarEventId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['google_calendar_event_id'] ) : $arrValues['google_calendar_event_id'] );
		if( isset( $arrValues['name'] ) && $boolDirectSet ) $this->set( 'm_strName', trim( stripcslashes( $arrValues['name'] ) ) ); elseif( isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		if( isset( $arrValues['description'] ) && $boolDirectSet ) $this->set( 'm_strDescription', trim( stripcslashes( $arrValues['description'] ) ) ); elseif( isset( $arrValues['description'] ) ) $this->setDescription( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['description'] ) : $arrValues['description'] );
		if( isset( $arrValues['country_code'] ) && $boolDirectSet ) $this->set( 'm_strCountryCode', trim( stripcslashes( $arrValues['country_code'] ) ) ); elseif( isset( $arrValues['country_code'] ) ) $this->setCountryCode( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['country_code'] ) : $arrValues['country_code'] );
		if( isset( $arrValues['training_location'] ) && $boolDirectSet ) $this->set( 'm_strTrainingLocation', trim( stripcslashes( $arrValues['training_location'] ) ) ); elseif( isset( $arrValues['training_location'] ) ) $this->setTrainingLocation( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['training_location'] ) : $arrValues['training_location'] );
		if( isset( $arrValues['available_seats'] ) && $boolDirectSet ) $this->set( 'm_intAvailableSeats', trim( $arrValues['available_seats'] ) ); elseif( isset( $arrValues['available_seats'] ) ) $this->setAvailableSeats( $arrValues['available_seats'] );
		if( isset( $arrValues['scheduled_start_time'] ) && $boolDirectSet ) $this->set( 'm_strScheduledStartTime', trim( $arrValues['scheduled_start_time'] ) ); elseif( isset( $arrValues['scheduled_start_time'] ) ) $this->setScheduledStartTime( $arrValues['scheduled_start_time'] );
		if( isset( $arrValues['scheduled_end_time'] ) && $boolDirectSet ) $this->set( 'm_strScheduledEndTime', trim( $arrValues['scheduled_end_time'] ) ); elseif( isset( $arrValues['scheduled_end_time'] ) ) $this->setScheduledEndTime( $arrValues['scheduled_end_time'] );
		if( isset( $arrValues['actual_start_time'] ) && $boolDirectSet ) $this->set( 'm_strActualStartTime', trim( $arrValues['actual_start_time'] ) ); elseif( isset( $arrValues['actual_start_time'] ) ) $this->setActualStartTime( $arrValues['actual_start_time'] );
		if( isset( $arrValues['actual_end_time'] ) && $boolDirectSet ) $this->set( 'm_strActualEndTime', trim( $arrValues['actual_end_time'] ) ); elseif( isset( $arrValues['actual_end_time'] ) ) $this->setActualEndTime( $arrValues['actual_end_time'] );
		if( isset( $arrValues['is_private'] ) && $boolDirectSet ) $this->set( 'm_intIsPrivate', trim( $arrValues['is_private'] ) ); elseif( isset( $arrValues['is_private'] ) ) $this->setIsPrivate( $arrValues['is_private'] );
		if( isset( $arrValues['is_published'] ) && $boolDirectSet ) $this->set( 'm_intIsPublished', trim( $arrValues['is_published'] ) ); elseif( isset( $arrValues['is_published'] ) ) $this->setIsPublished( $arrValues['is_published'] );
		if( isset( $arrValues['is_e_learning'] ) && $boolDirectSet ) $this->set( 'm_intIsELearning', trim( $arrValues['is_e_learning'] ) ); elseif( isset( $arrValues['is_e_learning'] ) ) $this->setIsELearning( $arrValues['is_e_learning'] );
		if( isset( $arrValues['is_cancelled'] ) && $boolDirectSet ) $this->set( 'm_intIsCancelled', trim( $arrValues['is_cancelled'] ) ); elseif( isset( $arrValues['is_cancelled'] ) ) $this->setIsCancelled( $arrValues['is_cancelled'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setParentTrainingSessionId( $intParentTrainingSessionId ) {
		$this->set( 'm_intParentTrainingSessionId', CStrings::strToIntDef( $intParentTrainingSessionId, NULL, false ) );
	}

	public function getParentTrainingSessionId() {
		return $this->m_intParentTrainingSessionId;
	}

	public function sqlParentTrainingSessionId() {
		return ( true == isset( $this->m_intParentTrainingSessionId ) ) ? ( string ) $this->m_intParentTrainingSessionId : 'NULL';
	}

	public function setTrainingId( $intTrainingId ) {
		$this->set( 'm_intTrainingId', CStrings::strToIntDef( $intTrainingId, NULL, false ) );
	}

	public function getTrainingId() {
		return $this->m_intTrainingId;
	}

	public function sqlTrainingId() {
		return ( true == isset( $this->m_intTrainingId ) ) ? ( string ) $this->m_intTrainingId : 'NULL';
	}

	public function setInductionSessionId( $intInductionSessionId ) {
		$this->set( 'm_intInductionSessionId', CStrings::strToIntDef( $intInductionSessionId, NULL, false ) );
	}

	public function getInductionSessionId() {
		return $this->m_intInductionSessionId;
	}

	public function sqlInductionSessionId() {
		return ( true == isset( $this->m_intInductionSessionId ) ) ? ( string ) $this->m_intInductionSessionId : 'NULL';
	}

	public function setTrainingTrainerAssociationId( $intTrainingTrainerAssociationId ) {
		$this->set( 'm_intTrainingTrainerAssociationId', CStrings::strToIntDef( $intTrainingTrainerAssociationId, NULL, false ) );
	}

	public function getTrainingTrainerAssociationId() {
		return $this->m_intTrainingTrainerAssociationId;
	}

	public function sqlTrainingTrainerAssociationId() {
		return ( true == isset( $this->m_intTrainingTrainerAssociationId ) ) ? ( string ) $this->m_intTrainingTrainerAssociationId : 'NULL';
	}

	public function setOfficeRoomId( $intOfficeRoomId ) {
		$this->set( 'm_intOfficeRoomId', CStrings::strToIntDef( $intOfficeRoomId, NULL, false ) );
	}

	public function getOfficeRoomId() {
		return $this->m_intOfficeRoomId;
	}

	public function sqlOfficeRoomId() {
		return ( true == isset( $this->m_intOfficeRoomId ) ) ? ( string ) $this->m_intOfficeRoomId : 'NULL';
	}

	public function setSurveyTemplateId( $intSurveyTemplateId ) {
		$this->set( 'm_intSurveyTemplateId', CStrings::strToIntDef( $intSurveyTemplateId, NULL, false ) );
	}

	public function getSurveyTemplateId() {
		return $this->m_intSurveyTemplateId;
	}

	public function sqlSurveyTemplateId() {
		return ( true == isset( $this->m_intSurveyTemplateId ) ) ? ( string ) $this->m_intSurveyTemplateId : 'NULL';
	}

	public function setGoogleCalendarEventId( $strGoogleCalendarEventId ) {
		$this->set( 'm_strGoogleCalendarEventId', CStrings::strTrimDef( $strGoogleCalendarEventId, 100, NULL, true ) );
	}

	public function getGoogleCalendarEventId() {
		return $this->m_strGoogleCalendarEventId;
	}

	public function sqlGoogleCalendarEventId() {
		return ( true == isset( $this->m_strGoogleCalendarEventId ) ) ? '\'' . addslashes( $this->m_strGoogleCalendarEventId ) . '\'' : 'NULL';
	}

	public function setName( $strName ) {
		$this->set( 'm_strName', CStrings::strTrimDef( $strName, 500, NULL, true ) );
	}

	public function getName() {
		return $this->m_strName;
	}

	public function sqlName() {
		return ( true == isset( $this->m_strName ) ) ? '\'' . addslashes( $this->m_strName ) . '\'' : 'NULL';
	}

	public function setDescription( $strDescription ) {
		$this->set( 'm_strDescription', CStrings::strTrimDef( $strDescription, 1000, NULL, true ) );
	}

	public function getDescription() {
		return $this->m_strDescription;
	}

	public function sqlDescription() {
		return ( true == isset( $this->m_strDescription ) ) ? '\'' . addslashes( $this->m_strDescription ) . '\'' : 'NULL';
	}

	public function setCountryCode( $strCountryCode ) {
		$this->set( 'm_strCountryCode', CStrings::strTrimDef( $strCountryCode, 2, NULL, true ) );
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function sqlCountryCode() {
		return ( true == isset( $this->m_strCountryCode ) ) ? '\'' . addslashes( $this->m_strCountryCode ) . '\'' : 'NULL';
	}

	public function setTrainingLocation( $strTrainingLocation ) {
		$this->set( 'm_strTrainingLocation', CStrings::strTrimDef( $strTrainingLocation, 500, NULL, true ) );
	}

	public function getTrainingLocation() {
		return $this->m_strTrainingLocation;
	}

	public function sqlTrainingLocation() {
		return ( true == isset( $this->m_strTrainingLocation ) ) ? '\'' . addslashes( $this->m_strTrainingLocation ) . '\'' : 'NULL';
	}

	public function setAvailableSeats( $intAvailableSeats ) {
		$this->set( 'm_intAvailableSeats', CStrings::strToIntDef( $intAvailableSeats, NULL, false ) );
	}

	public function getAvailableSeats() {
		return $this->m_intAvailableSeats;
	}

	public function sqlAvailableSeats() {
		return ( true == isset( $this->m_intAvailableSeats ) ) ? ( string ) $this->m_intAvailableSeats : 'NULL';
	}

	public function setScheduledStartTime( $strScheduledStartTime ) {
		$this->set( 'm_strScheduledStartTime', CStrings::strTrimDef( $strScheduledStartTime, -1, NULL, true ) );
	}

	public function getScheduledStartTime() {
		return $this->m_strScheduledStartTime;
	}

	public function sqlScheduledStartTime() {
		return ( true == isset( $this->m_strScheduledStartTime ) ) ? '\'' . $this->m_strScheduledStartTime . '\'' : 'NULL';
	}

	public function setScheduledEndTime( $strScheduledEndTime ) {
		$this->set( 'm_strScheduledEndTime', CStrings::strTrimDef( $strScheduledEndTime, -1, NULL, true ) );
	}

	public function getScheduledEndTime() {
		return $this->m_strScheduledEndTime;
	}

	public function sqlScheduledEndTime() {
		return ( true == isset( $this->m_strScheduledEndTime ) ) ? '\'' . $this->m_strScheduledEndTime . '\'' : 'NULL';
	}

	public function setActualStartTime( $strActualStartTime ) {
		$this->set( 'm_strActualStartTime', CStrings::strTrimDef( $strActualStartTime, -1, NULL, true ) );
	}

	public function getActualStartTime() {
		return $this->m_strActualStartTime;
	}

	public function sqlActualStartTime() {
		return ( true == isset( $this->m_strActualStartTime ) ) ? '\'' . $this->m_strActualStartTime . '\'' : 'NULL';
	}

	public function setActualEndTime( $strActualEndTime ) {
		$this->set( 'm_strActualEndTime', CStrings::strTrimDef( $strActualEndTime, -1, NULL, true ) );
	}

	public function getActualEndTime() {
		return $this->m_strActualEndTime;
	}

	public function sqlActualEndTime() {
		return ( true == isset( $this->m_strActualEndTime ) ) ? '\'' . $this->m_strActualEndTime . '\'' : 'NULL';
	}

	public function setIsPrivate( $intIsPrivate ) {
		$this->set( 'm_intIsPrivate', CStrings::strToIntDef( $intIsPrivate, NULL, false ) );
	}

	public function getIsPrivate() {
		return $this->m_intIsPrivate;
	}

	public function sqlIsPrivate() {
		return ( true == isset( $this->m_intIsPrivate ) ) ? ( string ) $this->m_intIsPrivate : '0';
	}

	public function setIsPublished( $intIsPublished ) {
		$this->set( 'm_intIsPublished', CStrings::strToIntDef( $intIsPublished, NULL, false ) );
	}

	public function getIsPublished() {
		return $this->m_intIsPublished;
	}

	public function sqlIsPublished() {
		return ( true == isset( $this->m_intIsPublished ) ) ? ( string ) $this->m_intIsPublished : '0';
	}

	public function setIsELearning( $intIsELearning ) {
		$this->set( 'm_intIsELearning', CStrings::strToIntDef( $intIsELearning, NULL, false ) );
	}

	public function getIsELearning() {
		return $this->m_intIsELearning;
	}

	public function sqlIsELearning() {
		return ( true == isset( $this->m_intIsELearning ) ) ? ( string ) $this->m_intIsELearning : '0';
	}

	public function setIsCancelled( $intIsCancelled ) {
		$this->set( 'm_intIsCancelled', CStrings::strToIntDef( $intIsCancelled, NULL, false ) );
	}

	public function getIsCancelled() {
		return $this->m_intIsCancelled;
	}

	public function sqlIsCancelled() {
		return ( true == isset( $this->m_intIsCancelled ) ) ? ( string ) $this->m_intIsCancelled : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, parent_training_session_id, training_id, induction_session_id, training_trainer_association_id, office_room_id, survey_template_id, google_calendar_event_id, name, description, country_code, training_location, available_seats, scheduled_start_time, scheduled_end_time, actual_start_time, actual_end_time, is_private, is_published, is_e_learning, is_cancelled, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlParentTrainingSessionId() . ', ' .
 						$this->sqlTrainingId() . ', ' .
 						$this->sqlInductionSessionId() . ', ' .
 						$this->sqlTrainingTrainerAssociationId() . ', ' .
 						$this->sqlOfficeRoomId() . ', ' .
 						$this->sqlSurveyTemplateId() . ', ' .
 						$this->sqlGoogleCalendarEventId() . ', ' .
 						$this->sqlName() . ', ' .
 						$this->sqlDescription() . ', ' .
 						$this->sqlCountryCode() . ', ' .
 						$this->sqlTrainingLocation() . ', ' .
 						$this->sqlAvailableSeats() . ', ' .
 						$this->sqlScheduledStartTime() . ', ' .
 						$this->sqlScheduledEndTime() . ', ' .
 						$this->sqlActualStartTime() . ', ' .
 						$this->sqlActualEndTime() . ', ' .
 						$this->sqlIsPrivate() . ', ' .
 						$this->sqlIsPublished() . ', ' .
 						$this->sqlIsELearning() . ', ' .
 						$this->sqlIsCancelled() . ', ' .
 						$this->sqlDeletedBy() . ', ' .
 						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' parent_training_session_id = ' . $this->sqlParentTrainingSessionId() . ','; } elseif( true == array_key_exists( 'ParentTrainingSessionId', $this->getChangedColumns() ) ) { $strSql .= ' parent_training_session_id = ' . $this->sqlParentTrainingSessionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_id = ' . $this->sqlTrainingId() . ','; } elseif( true == array_key_exists( 'TrainingId', $this->getChangedColumns() ) ) { $strSql .= ' training_id = ' . $this->sqlTrainingId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' induction_session_id = ' . $this->sqlInductionSessionId() . ','; } elseif( true == array_key_exists( 'InductionSessionId', $this->getChangedColumns() ) ) { $strSql .= ' induction_session_id = ' . $this->sqlInductionSessionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_trainer_association_id = ' . $this->sqlTrainingTrainerAssociationId() . ','; } elseif( true == array_key_exists( 'TrainingTrainerAssociationId', $this->getChangedColumns() ) ) { $strSql .= ' training_trainer_association_id = ' . $this->sqlTrainingTrainerAssociationId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' office_room_id = ' . $this->sqlOfficeRoomId() . ','; } elseif( true == array_key_exists( 'OfficeRoomId', $this->getChangedColumns() ) ) { $strSql .= ' office_room_id = ' . $this->sqlOfficeRoomId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId() . ','; } elseif( true == array_key_exists( 'SurveyTemplateId', $this->getChangedColumns() ) ) { $strSql .= ' survey_template_id = ' . $this->sqlSurveyTemplateId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' google_calendar_event_id = ' . $this->sqlGoogleCalendarEventId() . ','; } elseif( true == array_key_exists( 'GoogleCalendarEventId', $this->getChangedColumns() ) ) { $strSql .= ' google_calendar_event_id = ' . $this->sqlGoogleCalendarEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' name = ' . $this->sqlName() . ','; } elseif( true == array_key_exists( 'Name', $this->getChangedColumns() ) ) { $strSql .= ' name = ' . $this->sqlName() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; } elseif( true == array_key_exists( 'Description', $this->getChangedColumns() ) ) { $strSql .= ' description = ' . $this->sqlDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; } elseif( true == array_key_exists( 'CountryCode', $this->getChangedColumns() ) ) { $strSql .= ' country_code = ' . $this->sqlCountryCode() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' training_location = ' . $this->sqlTrainingLocation() . ','; } elseif( true == array_key_exists( 'TrainingLocation', $this->getChangedColumns() ) ) { $strSql .= ' training_location = ' . $this->sqlTrainingLocation() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' available_seats = ' . $this->sqlAvailableSeats() . ','; } elseif( true == array_key_exists( 'AvailableSeats', $this->getChangedColumns() ) ) { $strSql .= ' available_seats = ' . $this->sqlAvailableSeats() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_start_time = ' . $this->sqlScheduledStartTime() . ','; } elseif( true == array_key_exists( 'ScheduledStartTime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_start_time = ' . $this->sqlScheduledStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' scheduled_end_time = ' . $this->sqlScheduledEndTime() . ','; } elseif( true == array_key_exists( 'ScheduledEndTime', $this->getChangedColumns() ) ) { $strSql .= ' scheduled_end_time = ' . $this->sqlScheduledEndTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_start_time = ' . $this->sqlActualStartTime() . ','; } elseif( true == array_key_exists( 'ActualStartTime', $this->getChangedColumns() ) ) { $strSql .= ' actual_start_time = ' . $this->sqlActualStartTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_end_time = ' . $this->sqlActualEndTime() . ','; } elseif( true == array_key_exists( 'ActualEndTime', $this->getChangedColumns() ) ) { $strSql .= ' actual_end_time = ' . $this->sqlActualEndTime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_private = ' . $this->sqlIsPrivate() . ','; } elseif( true == array_key_exists( 'IsPrivate', $this->getChangedColumns() ) ) { $strSql .= ' is_private = ' . $this->sqlIsPrivate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; } elseif( true == array_key_exists( 'IsPublished', $this->getChangedColumns() ) ) { $strSql .= ' is_published = ' . $this->sqlIsPublished() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_e_learning = ' . $this->sqlIsELearning() . ','; } elseif( true == array_key_exists( 'IsELearning', $this->getChangedColumns() ) ) { $strSql .= ' is_e_learning = ' . $this->sqlIsELearning() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_cancelled = ' . $this->sqlIsCancelled() . ','; } elseif( true == array_key_exists( 'IsCancelled', $this->getChangedColumns() ) ) { $strSql .= ' is_cancelled = ' . $this->sqlIsCancelled() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'parent_training_session_id' => $this->getParentTrainingSessionId(),
			'training_id' => $this->getTrainingId(),
			'induction_session_id' => $this->getInductionSessionId(),
			'training_trainer_association_id' => $this->getTrainingTrainerAssociationId(),
			'office_room_id' => $this->getOfficeRoomId(),
			'survey_template_id' => $this->getSurveyTemplateId(),
			'google_calendar_event_id' => $this->getGoogleCalendarEventId(),
			'name' => $this->getName(),
			'description' => $this->getDescription(),
			'country_code' => $this->getCountryCode(),
			'training_location' => $this->getTrainingLocation(),
			'available_seats' => $this->getAvailableSeats(),
			'scheduled_start_time' => $this->getScheduledStartTime(),
			'scheduled_end_time' => $this->getScheduledEndTime(),
			'actual_start_time' => $this->getActualStartTime(),
			'actual_end_time' => $this->getActualEndTime(),
			'is_private' => $this->getIsPrivate(),
			'is_published' => $this->getIsPublished(),
			'is_e_learning' => $this->getIsELearning(),
			'is_cancelled' => $this->getIsCancelled(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>