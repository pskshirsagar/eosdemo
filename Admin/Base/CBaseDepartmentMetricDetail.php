<?php

class CBaseDepartmentMetricDetail extends CEosSingularBase {

	const TABLE_NAME = 'public.department_metric_details';

	protected $m_intId;
	protected $m_intDepartmentMetricId;
	protected $m_strDate;
	protected $m_fltValue;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;

	public function __construct() {
		parent::__construct();

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['department_metric_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentMetricId', trim( $arrValues['department_metric_id'] ) ); elseif( isset( $arrValues['department_metric_id'] ) ) $this->setDepartmentMetricId( $arrValues['department_metric_id'] );
		if( isset( $arrValues['date'] ) && $boolDirectSet ) $this->set( 'm_strDate', trim( $arrValues['date'] ) ); elseif( isset( $arrValues['date'] ) ) $this->setDate( $arrValues['date'] );
		if( isset( $arrValues['value'] ) && $boolDirectSet ) $this->set( 'm_fltValue', trim( $arrValues['value'] ) ); elseif( isset( $arrValues['value'] ) ) $this->setValue( $arrValues['value'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setDepartmentMetricId( $intDepartmentMetricId ) {
		$this->set( 'm_intDepartmentMetricId', CStrings::strToIntDef( $intDepartmentMetricId, NULL, false ) );
	}

	public function getDepartmentMetricId() {
		return $this->m_intDepartmentMetricId;
	}

	public function sqlDepartmentMetricId() {
		return ( true == isset( $this->m_intDepartmentMetricId ) ) ? ( string ) $this->m_intDepartmentMetricId : 'NULL';
	}

	public function setDate( $strDate ) {
		$this->set( 'm_strDate', CStrings::strTrimDef( $strDate, -1, NULL, true ) );
	}

	public function getDate() {
		return $this->m_strDate;
	}

	public function sqlDate() {
		return ( true == isset( $this->m_strDate ) ) ? '\'' . $this->m_strDate . '\'' : 'NOW()';
	}

	public function setValue( $fltValue ) {
		$this->set( 'm_fltValue', CStrings::strToFloatDef( $fltValue, NULL, false, 2 ) );
	}

	public function getValue() {
		return $this->m_fltValue;
	}

	public function sqlValue() {
		return ( true == isset( $this->m_fltValue ) ) ? ( string ) $this->m_fltValue : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, department_metric_id, date, value, updated_by, updated_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlDepartmentMetricId() . ', ' .
 						$this->sqlDate() . ', ' .
 						$this->sqlValue() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlUpdatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_metric_id = ' . $this->sqlDepartmentMetricId() . ','; } elseif( true == array_key_exists( 'DepartmentMetricId', $this->getChangedColumns() ) ) { $strSql .= ' department_metric_id = ' . $this->sqlDepartmentMetricId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' date = ' . $this->sqlDate() . ','; } elseif( true == array_key_exists( 'Date', $this->getChangedColumns() ) ) { $strSql .= ' date = ' . $this->sqlDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' value = ' . $this->sqlValue() . ','; } elseif( true == array_key_exists( 'Value', $this->getChangedColumns() ) ) { $strSql .= ' value = ' . $this->sqlValue() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'department_metric_id' => $this->getDepartmentMetricId(),
			'date' => $this->getDate(),
			'value' => $this->getValue(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn()
		);
	}

}
?>