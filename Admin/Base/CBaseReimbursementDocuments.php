<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReimbursementDocuments
 * Do not add any new functions to this class.
 */

class CBaseReimbursementDocuments extends CEosPluralBase {

	/**
	 * @return CReimbursementDocument[]
	 */
	public static function fetchReimbursementDocuments( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CReimbursementDocument', $objDatabase );
	}

	/**
	 * @return CReimbursementDocument
	 */
	public static function fetchReimbursementDocument( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CReimbursementDocument', $objDatabase );
	}

	public static function fetchReimbursementDocumentCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'reimbursement_documents', $objDatabase );
	}

	public static function fetchReimbursementDocumentById( $intId, $objDatabase ) {
		return self::fetchReimbursementDocument( sprintf( 'SELECT * FROM reimbursement_documents WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchReimbursementDocumentsByReferenceId( $intReferenceId, $objDatabase ) {
		return self::fetchReimbursementDocuments( sprintf( 'SELECT * FROM reimbursement_documents WHERE reference_id = %d', ( int ) $intReferenceId ), $objDatabase );
	}

	public static function fetchReimbursementDocumentsByPsDocumentId( $intPsDocumentId, $objDatabase ) {
		return self::fetchReimbursementDocuments( sprintf( 'SELECT * FROM reimbursement_documents WHERE ps_document_id = %d', ( int ) $intPsDocumentId ), $objDatabase );
	}

}
?>