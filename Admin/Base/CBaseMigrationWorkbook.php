<?php

class CBaseMigrationWorkbook extends CEosSingularBase {

	const TABLE_NAME = 'public.migration_workbooks';

	protected $m_intId;
	protected $m_intCid;
	protected $m_intPropertyId;
	protected $m_intMigrationWorkbookStatusTypeId;
	protected $m_arrintImportTypeIds;
	protected $m_intMigrationConsultantUserId;
	protected $m_intBackOfficeUserId;
	protected $m_intCoreConsultantUserId;
	protected $m_arrintPrimaryCompanyUserIds;
	protected $m_arrintSecondaryCompanyUserIds;
	protected $m_intSecondBackOfficeUserId;
	protected $m_intBackupCoreConsultantUserId;
	protected $m_intAccConsultantUserId;
	protected $m_intBackupAccConsultantUserId;
	protected $m_intImportDataTypeId;
	protected $m_intCompetitorId;
	protected $m_strAsOfDate;
	protected $m_strDataValues;
	protected $m_jsonDataValues;
	protected $m_strCustomMappingDataValues;
	protected $m_jsonCustomMappingDataValues;
	protected $m_intEntrataUnitCount;
	protected $m_intEntrataSpaceCount;
	protected $m_boolIsTestMigration;
	protected $m_boolIsTakeover;
	protected $m_strSignature;
	protected $m_intAccountingSentBy;
	protected $m_strAccountingSentOn;
	protected $m_intSentBy;
	protected $m_strSentOn;
	protected $m_intSignedBy;
	protected $m_strSignedOn;
	protected $m_intRejectedBy;
	protected $m_strRejectedOn;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strStatusDetails;
	protected $m_jsonStatusDetails;
	protected $m_boolIsInitialImport;
	protected $m_strMigrationStartDate;
	protected $m_intProjectManagerUserId;
	protected $m_intBackupProjectManagerUserId;
	protected $m_intBackupMigrationConsultantUserId;

	public function __construct() {
		parent::__construct();

		$this->m_intMigrationWorkbookStatusTypeId = '2';
		$this->m_boolIsTestMigration = false;
		$this->m_boolIsTakeover = false;
		$this->m_boolIsInitialImport = false;
		$this->m_strMigrationStartDate = 'now()';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['cid'] ) && $boolDirectSet ) $this->set( 'm_intCid', trim( $arrValues['cid'] ) ); elseif( isset( $arrValues['cid'] ) ) $this->setCid( $arrValues['cid'] );
		if( isset( $arrValues['property_id'] ) && $boolDirectSet ) $this->set( 'm_intPropertyId', trim( $arrValues['property_id'] ) ); elseif( isset( $arrValues['property_id'] ) ) $this->setPropertyId( $arrValues['property_id'] );
		if( isset( $arrValues['migration_workbook_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intMigrationWorkbookStatusTypeId', trim( $arrValues['migration_workbook_status_type_id'] ) ); elseif( isset( $arrValues['migration_workbook_status_type_id'] ) ) $this->setMigrationWorkbookStatusTypeId( $arrValues['migration_workbook_status_type_id'] );
		if( isset( $arrValues['import_type_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintImportTypeIds', trim( $arrValues['import_type_ids'] ) ); elseif( isset( $arrValues['import_type_ids'] ) ) $this->setImportTypeIds( $arrValues['import_type_ids'] );
		if( isset( $arrValues['migration_consultant_user_id'] ) && $boolDirectSet ) $this->set( 'm_intMigrationConsultantUserId', trim( $arrValues['migration_consultant_user_id'] ) ); elseif( isset( $arrValues['migration_consultant_user_id'] ) ) $this->setMigrationConsultantUserId( $arrValues['migration_consultant_user_id'] );
		if( isset( $arrValues['back_office_user_id'] ) && $boolDirectSet ) $this->set( 'm_intBackOfficeUserId', trim( $arrValues['back_office_user_id'] ) ); elseif( isset( $arrValues['back_office_user_id'] ) ) $this->setBackOfficeUserId( $arrValues['back_office_user_id'] );
		if( isset( $arrValues['core_consultant_user_id'] ) && $boolDirectSet ) $this->set( 'm_intCoreConsultantUserId', trim( $arrValues['core_consultant_user_id'] ) ); elseif( isset( $arrValues['core_consultant_user_id'] ) ) $this->setCoreConsultantUserId( $arrValues['core_consultant_user_id'] );
		if( isset( $arrValues['primary_company_user_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintPrimaryCompanyUserIds', trim( $arrValues['primary_company_user_ids'] ) ); elseif( isset( $arrValues['primary_company_user_ids'] ) ) $this->setPrimaryCompanyUserIds( $arrValues['primary_company_user_ids'] );
		if( isset( $arrValues['secondary_company_user_ids'] ) && $boolDirectSet ) $this->set( 'm_arrintSecondaryCompanyUserIds', trim( $arrValues['secondary_company_user_ids'] ) ); elseif( isset( $arrValues['secondary_company_user_ids'] ) ) $this->setSecondaryCompanyUserIds( $arrValues['secondary_company_user_ids'] );
		if( isset( $arrValues['second_back_office_user_id'] ) && $boolDirectSet ) $this->set( 'm_intSecondBackOfficeUserId', trim( $arrValues['second_back_office_user_id'] ) ); elseif( isset( $arrValues['second_back_office_user_id'] ) ) $this->setSecondBackOfficeUserId( $arrValues['second_back_office_user_id'] );
		if( isset( $arrValues['backup_core_consultant_user_id'] ) && $boolDirectSet ) $this->set( 'm_intBackupCoreConsultantUserId', trim( $arrValues['backup_core_consultant_user_id'] ) ); elseif( isset( $arrValues['backup_core_consultant_user_id'] ) ) $this->setBackupCoreConsultantUserId( $arrValues['backup_core_consultant_user_id'] );
		if( isset( $arrValues['acc_consultant_user_id'] ) && $boolDirectSet ) $this->set( 'm_intAccConsultantUserId', trim( $arrValues['acc_consultant_user_id'] ) ); elseif( isset( $arrValues['acc_consultant_user_id'] ) ) $this->setAccConsultantUserId( $arrValues['acc_consultant_user_id'] );
		if( isset( $arrValues['backup_acc_consultant_user_id'] ) && $boolDirectSet ) $this->set( 'm_intBackupAccConsultantUserId', trim( $arrValues['backup_acc_consultant_user_id'] ) ); elseif( isset( $arrValues['backup_acc_consultant_user_id'] ) ) $this->setBackupAccConsultantUserId( $arrValues['backup_acc_consultant_user_id'] );
		if( isset( $arrValues['import_data_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImportDataTypeId', trim( $arrValues['import_data_type_id'] ) ); elseif( isset( $arrValues['import_data_type_id'] ) ) $this->setImportDataTypeId( $arrValues['import_data_type_id'] );
		if( isset( $arrValues['competitor_id'] ) && $boolDirectSet ) $this->set( 'm_intCompetitorId', trim( $arrValues['competitor_id'] ) ); elseif( isset( $arrValues['competitor_id'] ) ) $this->setCompetitorId( $arrValues['competitor_id'] );
		if( isset( $arrValues['as_of_date'] ) && $boolDirectSet ) $this->set( 'm_strAsOfDate', trim( $arrValues['as_of_date'] ) ); elseif( isset( $arrValues['as_of_date'] ) ) $this->setAsOfDate( $arrValues['as_of_date'] );
		if( isset( $arrValues['data_values'] ) ) $this->set( 'm_strDataValues', trim( $arrValues['data_values'] ) );
		if( isset( $arrValues['custom_mapping_data_values'] ) ) $this->set( 'm_strCustomMappingDataValues', trim( $arrValues['custom_mapping_data_values'] ) );
		if( isset( $arrValues['entrata_unit_count'] ) && $boolDirectSet ) $this->set( 'm_intEntrataUnitCount', trim( $arrValues['entrata_unit_count'] ) ); elseif( isset( $arrValues['entrata_unit_count'] ) ) $this->setEntrataUnitCount( $arrValues['entrata_unit_count'] );
		if( isset( $arrValues['entrata_space_count'] ) && $boolDirectSet ) $this->set( 'm_intEntrataSpaceCount', trim( $arrValues['entrata_space_count'] ) ); elseif( isset( $arrValues['entrata_space_count'] ) ) $this->setEntrataSpaceCount( $arrValues['entrata_space_count'] );
		if( isset( $arrValues['is_test_migration'] ) && $boolDirectSet ) $this->set( 'm_boolIsTestMigration', trim( stripcslashes( $arrValues['is_test_migration'] ) ) ); elseif( isset( $arrValues['is_test_migration'] ) ) $this->setIsTestMigration( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_test_migration'] ) : $arrValues['is_test_migration'] );
		if( isset( $arrValues['is_takeover'] ) && $boolDirectSet ) $this->set( 'm_boolIsTakeover', trim( stripcslashes( $arrValues['is_takeover'] ) ) ); elseif( isset( $arrValues['is_takeover'] ) ) $this->setIsTakeover( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_takeover'] ) : $arrValues['is_takeover'] );
		if( isset( $arrValues['signature'] ) && $boolDirectSet ) $this->set( 'm_strSignature', trim( $arrValues['signature'] ) ); elseif( isset( $arrValues['signature'] ) ) $this->setSignature( $arrValues['signature'] );
		if( isset( $arrValues['accounting_sent_by'] ) && $boolDirectSet ) $this->set( 'm_intAccountingSentBy', trim( $arrValues['accounting_sent_by'] ) ); elseif( isset( $arrValues['accounting_sent_by'] ) ) $this->setAccountingSentBy( $arrValues['accounting_sent_by'] );
		if( isset( $arrValues['accounting_sent_on'] ) && $boolDirectSet ) $this->set( 'm_strAccountingSentOn', trim( $arrValues['accounting_sent_on'] ) ); elseif( isset( $arrValues['accounting_sent_on'] ) ) $this->setAccountingSentOn( $arrValues['accounting_sent_on'] );
		if( isset( $arrValues['sent_by'] ) && $boolDirectSet ) $this->set( 'm_intSentBy', trim( $arrValues['sent_by'] ) ); elseif( isset( $arrValues['sent_by'] ) ) $this->setSentBy( $arrValues['sent_by'] );
		if( isset( $arrValues['sent_on'] ) && $boolDirectSet ) $this->set( 'm_strSentOn', trim( $arrValues['sent_on'] ) ); elseif( isset( $arrValues['sent_on'] ) ) $this->setSentOn( $arrValues['sent_on'] );
		if( isset( $arrValues['signed_by'] ) && $boolDirectSet ) $this->set( 'm_intSignedBy', trim( $arrValues['signed_by'] ) ); elseif( isset( $arrValues['signed_by'] ) ) $this->setSignedBy( $arrValues['signed_by'] );
		if( isset( $arrValues['signed_on'] ) && $boolDirectSet ) $this->set( 'm_strSignedOn', trim( $arrValues['signed_on'] ) ); elseif( isset( $arrValues['signed_on'] ) ) $this->setSignedOn( $arrValues['signed_on'] );
		if( isset( $arrValues['rejected_by'] ) && $boolDirectSet ) $this->set( 'm_intRejectedBy', trim( $arrValues['rejected_by'] ) ); elseif( isset( $arrValues['rejected_by'] ) ) $this->setRejectedBy( $arrValues['rejected_by'] );
		if( isset( $arrValues['rejected_on'] ) && $boolDirectSet ) $this->set( 'm_strRejectedOn', trim( $arrValues['rejected_on'] ) ); elseif( isset( $arrValues['rejected_on'] ) ) $this->setRejectedOn( $arrValues['rejected_on'] );
		if( isset( $arrValues['deleted_by'] ) && $boolDirectSet ) $this->set( 'm_intDeletedBy', trim( $arrValues['deleted_by'] ) ); elseif( isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( $arrValues['deleted_by'] );
		if( isset( $arrValues['deleted_on'] ) && $boolDirectSet ) $this->set( 'm_strDeletedOn', trim( $arrValues['deleted_on'] ) ); elseif( isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( $arrValues['deleted_on'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['status_details'] ) ) $this->set( 'm_strStatusDetails', trim( $arrValues['status_details'] ) );
		if( isset( $arrValues['is_initial_import'] ) && $boolDirectSet ) $this->set( 'm_boolIsInitialImport', trim( stripcslashes( $arrValues['is_initial_import'] ) ) ); elseif( isset( $arrValues['is_initial_import'] ) ) $this->setIsInitialImport( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_initial_import'] ) : $arrValues['is_initial_import'] );
		if( isset( $arrValues['migration_start_date'] ) && $boolDirectSet ) $this->set( 'm_strMigrationStartDate', trim( $arrValues['migration_start_date'] ) ); elseif( isset( $arrValues['migration_start_date'] ) ) $this->setMigrationStartDate( $arrValues['migration_start_date'] );
		if( isset( $arrValues['project_manager_user_id'] ) && $boolDirectSet ) $this->set( 'm_intProjectManagerUserId', trim( $arrValues['project_manager_user_id'] ) ); elseif( isset( $arrValues['project_manager_user_id'] ) ) $this->setProjectManagerUserId( $arrValues['project_manager_user_id'] );
		if( isset( $arrValues['backup_project_manager_user_id'] ) && $boolDirectSet ) $this->set( 'm_intBackupProjectManagerUserId', trim( $arrValues['backup_project_manager_user_id'] ) ); elseif( isset( $arrValues['backup_project_manager_user_id'] ) ) $this->setBackupProjectManagerUserId( $arrValues['backup_project_manager_user_id'] );
		if( isset( $arrValues['backup_migration_consultant_user_id'] ) && $boolDirectSet ) $this->set( 'm_intBackupMigrationConsultantUserId', trim( $arrValues['backup_migration_consultant_user_id'] ) ); elseif( isset( $arrValues['backup_migration_consultant_user_id'] ) ) $this->setBackupMigrationConsultantUserId( $arrValues['backup_migration_consultant_user_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function sqlCid() {
		return ( true == isset( $this->m_intCid ) ) ? ( string ) $this->m_intCid : 'NULL';
	}

	public function setPropertyId( $intPropertyId ) {
		$this->set( 'm_intPropertyId', CStrings::strToIntDef( $intPropertyId, NULL, false ) );
	}

	public function getPropertyId() {
		return $this->m_intPropertyId;
	}

	public function sqlPropertyId() {
		return ( true == isset( $this->m_intPropertyId ) ) ? ( string ) $this->m_intPropertyId : 'NULL';
	}

	public function setMigrationWorkbookStatusTypeId( $intMigrationWorkbookStatusTypeId ) {
		$this->set( 'm_intMigrationWorkbookStatusTypeId', CStrings::strToIntDef( $intMigrationWorkbookStatusTypeId, NULL, false ) );
	}

	public function getMigrationWorkbookStatusTypeId() {
		return $this->m_intMigrationWorkbookStatusTypeId;
	}

	public function sqlMigrationWorkbookStatusTypeId() {
		return ( true == isset( $this->m_intMigrationWorkbookStatusTypeId ) ) ? ( string ) $this->m_intMigrationWorkbookStatusTypeId : '2';
	}

	public function setImportTypeIds( $arrintImportTypeIds ) {
		$this->set( 'm_arrintImportTypeIds', CStrings::strToArrIntDef( $arrintImportTypeIds, NULL ) );
	}

	public function getImportTypeIds() {
		return $this->m_arrintImportTypeIds;
	}

	public function sqlImportTypeIds() {
		return ( true == isset( $this->m_arrintImportTypeIds ) && true == valArr( $this->m_arrintImportTypeIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintImportTypeIds, NULL ) . '\'' : 'NULL';
	}

	public function setMigrationConsultantUserId( $intMigrationConsultantUserId ) {
		$this->set( 'm_intMigrationConsultantUserId', CStrings::strToIntDef( $intMigrationConsultantUserId, NULL, false ) );
	}

	public function getMigrationConsultantUserId() {
		return $this->m_intMigrationConsultantUserId;
	}

	public function sqlMigrationConsultantUserId() {
		return ( true == isset( $this->m_intMigrationConsultantUserId ) ) ? ( string ) $this->m_intMigrationConsultantUserId : 'NULL';
	}

	public function setBackOfficeUserId( $intBackOfficeUserId ) {
		$this->set( 'm_intBackOfficeUserId', CStrings::strToIntDef( $intBackOfficeUserId, NULL, false ) );
	}

	public function getBackOfficeUserId() {
		return $this->m_intBackOfficeUserId;
	}

	public function sqlBackOfficeUserId() {
		return ( true == isset( $this->m_intBackOfficeUserId ) ) ? ( string ) $this->m_intBackOfficeUserId : 'NULL';
	}

	public function setCoreConsultantUserId( $intCoreConsultantUserId ) {
		$this->set( 'm_intCoreConsultantUserId', CStrings::strToIntDef( $intCoreConsultantUserId, NULL, false ) );
	}

	public function getCoreConsultantUserId() {
		return $this->m_intCoreConsultantUserId;
	}

	public function sqlCoreConsultantUserId() {
		return ( true == isset( $this->m_intCoreConsultantUserId ) ) ? ( string ) $this->m_intCoreConsultantUserId : 'NULL';
	}

	public function setPrimaryCompanyUserIds( $arrintPrimaryCompanyUserIds ) {
		$this->set( 'm_arrintPrimaryCompanyUserIds', CStrings::strToArrIntDef( $arrintPrimaryCompanyUserIds, NULL ) );
	}

	public function getPrimaryCompanyUserIds() {
		return $this->m_arrintPrimaryCompanyUserIds;
	}

	public function sqlPrimaryCompanyUserIds() {
		return ( true == isset( $this->m_arrintPrimaryCompanyUserIds ) && true == valArr( $this->m_arrintPrimaryCompanyUserIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintPrimaryCompanyUserIds, NULL ) . '\'' : 'NULL';
	}

	public function setSecondaryCompanyUserIds( $arrintSecondaryCompanyUserIds ) {
		$this->set( 'm_arrintSecondaryCompanyUserIds', CStrings::strToArrIntDef( $arrintSecondaryCompanyUserIds, NULL ) );
	}

	public function getSecondaryCompanyUserIds() {
		return $this->m_arrintSecondaryCompanyUserIds;
	}

	public function sqlSecondaryCompanyUserIds() {
		return ( true == isset( $this->m_arrintSecondaryCompanyUserIds ) && true == valArr( $this->m_arrintSecondaryCompanyUserIds ) ) ? '\'' . CStrings::arrToStrIntDef( $this->m_arrintSecondaryCompanyUserIds, NULL ) . '\'' : 'NULL';
	}

	public function setSecondBackOfficeUserId( $intSecondBackOfficeUserId ) {
		$this->set( 'm_intSecondBackOfficeUserId', CStrings::strToIntDef( $intSecondBackOfficeUserId, NULL, false ) );
	}

	public function getSecondBackOfficeUserId() {
		return $this->m_intSecondBackOfficeUserId;
	}

	public function sqlSecondBackOfficeUserId() {
		return ( true == isset( $this->m_intSecondBackOfficeUserId ) ) ? ( string ) $this->m_intSecondBackOfficeUserId : 'NULL';
	}

	public function setBackupCoreConsultantUserId( $intBackupCoreConsultantUserId ) {
		$this->set( 'm_intBackupCoreConsultantUserId', CStrings::strToIntDef( $intBackupCoreConsultantUserId, NULL, false ) );
	}

	public function getBackupCoreConsultantUserId() {
		return $this->m_intBackupCoreConsultantUserId;
	}

	public function sqlBackupCoreConsultantUserId() {
		return ( true == isset( $this->m_intBackupCoreConsultantUserId ) ) ? ( string ) $this->m_intBackupCoreConsultantUserId : 'NULL';
	}

	public function setAccConsultantUserId( $intAccConsultantUserId ) {
		$this->set( 'm_intAccConsultantUserId', CStrings::strToIntDef( $intAccConsultantUserId, NULL, false ) );
	}

	public function getAccConsultantUserId() {
		return $this->m_intAccConsultantUserId;
	}

	public function sqlAccConsultantUserId() {
		return ( true == isset( $this->m_intAccConsultantUserId ) ) ? ( string ) $this->m_intAccConsultantUserId : 'NULL';
	}

	public function setBackupAccConsultantUserId( $intBackupAccConsultantUserId ) {
		$this->set( 'm_intBackupAccConsultantUserId', CStrings::strToIntDef( $intBackupAccConsultantUserId, NULL, false ) );
	}

	public function getBackupAccConsultantUserId() {
		return $this->m_intBackupAccConsultantUserId;
	}

	public function sqlBackupAccConsultantUserId() {
		return ( true == isset( $this->m_intBackupAccConsultantUserId ) ) ? ( string ) $this->m_intBackupAccConsultantUserId : 'NULL';
	}

	public function setImportDataTypeId( $intImportDataTypeId ) {
		$this->set( 'm_intImportDataTypeId', CStrings::strToIntDef( $intImportDataTypeId, NULL, false ) );
	}

	public function getImportDataTypeId() {
		return $this->m_intImportDataTypeId;
	}

	public function sqlImportDataTypeId() {
		return ( true == isset( $this->m_intImportDataTypeId ) ) ? ( string ) $this->m_intImportDataTypeId : 'NULL';
	}

	public function setCompetitorId( $intCompetitorId ) {
		$this->set( 'm_intCompetitorId', CStrings::strToIntDef( $intCompetitorId, NULL, false ) );
	}

	public function getCompetitorId() {
		return $this->m_intCompetitorId;
	}

	public function sqlCompetitorId() {
		return ( true == isset( $this->m_intCompetitorId ) ) ? ( string ) $this->m_intCompetitorId : 'NULL';
	}

	public function setAsOfDate( $strAsOfDate ) {
		$this->set( 'm_strAsOfDate', CStrings::strTrimDef( $strAsOfDate, -1, NULL, true ) );
	}

	public function getAsOfDate() {
		return $this->m_strAsOfDate;
	}

	public function sqlAsOfDate() {
		return ( true == isset( $this->m_strAsOfDate ) ) ? '\'' . $this->m_strAsOfDate . '\'' : 'NOW()';
	}

	public function setDataValues( $jsonDataValues ) {
		if( true == valObj( $jsonDataValues, 'stdClass' ) ) {
			$this->set( 'm_jsonDataValues', $jsonDataValues );
		} elseif( true == valJsonString( $jsonDataValues ) ) {
			$this->set( 'm_jsonDataValues', CStrings::strToJson( $jsonDataValues ) );
		} else {
			$this->set( 'm_jsonDataValues', NULL );
		}
		unset( $this->m_strDataValues );
	}

	public function getDataValues() {
		if( true == isset( $this->m_strDataValues ) ) {
			$this->m_jsonDataValues = CStrings::strToJson( $this->m_strDataValues );
			unset( $this->m_strDataValues );
		}
		return $this->m_jsonDataValues;
	}

	public function sqlDataValues() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getDataValues() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getDataValues() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setCustomMappingDataValues( $jsonCustomMappingDataValues ) {
		if( true == valObj( $jsonCustomMappingDataValues, 'stdClass' ) ) {
			$this->set( 'm_jsonCustomMappingDataValues', $jsonCustomMappingDataValues );
		} elseif( true == valJsonString( $jsonCustomMappingDataValues ) ) {
			$this->set( 'm_jsonCustomMappingDataValues', CStrings::strToJson( $jsonCustomMappingDataValues ) );
		} else {
			$this->set( 'm_jsonCustomMappingDataValues', NULL );
		}
		unset( $this->m_strCustomMappingDataValues );
	}

	public function getCustomMappingDataValues() {
		if( true == isset( $this->m_strCustomMappingDataValues ) ) {
			$this->m_jsonCustomMappingDataValues = CStrings::strToJson( $this->m_strCustomMappingDataValues );
			unset( $this->m_strCustomMappingDataValues );
		}
		return $this->m_jsonCustomMappingDataValues;
	}

	public function sqlCustomMappingDataValues() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getCustomMappingDataValues() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getCustomMappingDataValues() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setEntrataUnitCount( $intEntrataUnitCount ) {
		$this->set( 'm_intEntrataUnitCount', CStrings::strToIntDef( $intEntrataUnitCount, NULL, false ) );
	}

	public function getEntrataUnitCount() {
		return $this->m_intEntrataUnitCount;
	}

	public function sqlEntrataUnitCount() {
		return ( true == isset( $this->m_intEntrataUnitCount ) ) ? ( string ) $this->m_intEntrataUnitCount : 'NULL';
	}

	public function setEntrataSpaceCount( $intEntrataSpaceCount ) {
		$this->set( 'm_intEntrataSpaceCount', CStrings::strToIntDef( $intEntrataSpaceCount, NULL, false ) );
	}

	public function getEntrataSpaceCount() {
		return $this->m_intEntrataSpaceCount;
	}

	public function sqlEntrataSpaceCount() {
		return ( true == isset( $this->m_intEntrataSpaceCount ) ) ? ( string ) $this->m_intEntrataSpaceCount : 'NULL';
	}

	public function setIsTestMigration( $boolIsTestMigration ) {
		$this->set( 'm_boolIsTestMigration', CStrings::strToBool( $boolIsTestMigration ) );
	}

	public function getIsTestMigration() {
		return $this->m_boolIsTestMigration;
	}

	public function sqlIsTestMigration() {
		return ( true == isset( $this->m_boolIsTestMigration ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTestMigration ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setIsTakeover( $boolIsTakeover ) {
		$this->set( 'm_boolIsTakeover', CStrings::strToBool( $boolIsTakeover ) );
	}

	public function getIsTakeover() {
		return $this->m_boolIsTakeover;
	}

	public function sqlIsTakeover() {
		return ( true == isset( $this->m_boolIsTakeover ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsTakeover ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setSignature( $strSignature ) {
		$this->set( 'm_strSignature', CStrings::strTrimDef( $strSignature, 100, NULL, true ) );
	}

	public function getSignature() {
		return $this->m_strSignature;
	}

	public function sqlSignature() {
		return ( true == isset( $this->m_strSignature ) ) ? '\'' . addslashes( $this->m_strSignature ) . '\'' : 'NULL';
	}

	public function setAccountingSentBy( $intAccountingSentBy ) {
		$this->set( 'm_intAccountingSentBy', CStrings::strToIntDef( $intAccountingSentBy, NULL, false ) );
	}

	public function getAccountingSentBy() {
		return $this->m_intAccountingSentBy;
	}

	public function sqlAccountingSentBy() {
		return ( true == isset( $this->m_intAccountingSentBy ) ) ? ( string ) $this->m_intAccountingSentBy : 'NULL';
	}

	public function setAccountingSentOn( $strAccountingSentOn ) {
		$this->set( 'm_strAccountingSentOn', CStrings::strTrimDef( $strAccountingSentOn, -1, NULL, true ) );
	}

	public function getAccountingSentOn() {
		return $this->m_strAccountingSentOn;
	}

	public function sqlAccountingSentOn() {
		return ( true == isset( $this->m_strAccountingSentOn ) ) ? '\'' . $this->m_strAccountingSentOn . '\'' : 'NULL';
	}

	public function setSentBy( $intSentBy ) {
		$this->set( 'm_intSentBy', CStrings::strToIntDef( $intSentBy, NULL, false ) );
	}

	public function getSentBy() {
		return $this->m_intSentBy;
	}

	public function sqlSentBy() {
		return ( true == isset( $this->m_intSentBy ) ) ? ( string ) $this->m_intSentBy : 'NULL';
	}

	public function setSentOn( $strSentOn ) {
		$this->set( 'm_strSentOn', CStrings::strTrimDef( $strSentOn, -1, NULL, true ) );
	}

	public function getSentOn() {
		return $this->m_strSentOn;
	}

	public function sqlSentOn() {
		return ( true == isset( $this->m_strSentOn ) ) ? '\'' . $this->m_strSentOn . '\'' : 'NULL';
	}

	public function setSignedBy( $intSignedBy ) {
		$this->set( 'm_intSignedBy', CStrings::strToIntDef( $intSignedBy, NULL, false ) );
	}

	public function getSignedBy() {
		return $this->m_intSignedBy;
	}

	public function sqlSignedBy() {
		return ( true == isset( $this->m_intSignedBy ) ) ? ( string ) $this->m_intSignedBy : 'NULL';
	}

	public function setSignedOn( $strSignedOn ) {
		$this->set( 'm_strSignedOn', CStrings::strTrimDef( $strSignedOn, -1, NULL, true ) );
	}

	public function getSignedOn() {
		return $this->m_strSignedOn;
	}

	public function sqlSignedOn() {
		return ( true == isset( $this->m_strSignedOn ) ) ? '\'' . $this->m_strSignedOn . '\'' : 'NULL';
	}

	public function setRejectedBy( $intRejectedBy ) {
		$this->set( 'm_intRejectedBy', CStrings::strToIntDef( $intRejectedBy, NULL, false ) );
	}

	public function getRejectedBy() {
		return $this->m_intRejectedBy;
	}

	public function sqlRejectedBy() {
		return ( true == isset( $this->m_intRejectedBy ) ) ? ( string ) $this->m_intRejectedBy : 'NULL';
	}

	public function setRejectedOn( $strRejectedOn ) {
		$this->set( 'm_strRejectedOn', CStrings::strTrimDef( $strRejectedOn, -1, NULL, true ) );
	}

	public function getRejectedOn() {
		return $this->m_strRejectedOn;
	}

	public function sqlRejectedOn() {
		return ( true == isset( $this->m_strRejectedOn ) ) ? '\'' . $this->m_strRejectedOn . '\'' : 'NULL';
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->set( 'm_intDeletedBy', CStrings::strToIntDef( $intDeletedBy, NULL, false ) );
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function sqlDeletedBy() {
		return ( true == isset( $this->m_intDeletedBy ) ) ? ( string ) $this->m_intDeletedBy : 'NULL';
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->set( 'm_strDeletedOn', CStrings::strTrimDef( $strDeletedOn, -1, NULL, true ) );
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function sqlDeletedOn() {
		return ( true == isset( $this->m_strDeletedOn ) ) ? '\'' . $this->m_strDeletedOn . '\'' : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setStatusDetails( $jsonStatusDetails ) {
		if( true == valObj( $jsonStatusDetails, 'stdClass' ) ) {
			$this->set( 'm_jsonStatusDetails', $jsonStatusDetails );
		} elseif( true == valJsonString( $jsonStatusDetails ) ) {
			$this->set( 'm_jsonStatusDetails', CStrings::strToJson( $jsonStatusDetails ) );
		} else {
			$this->set( 'm_jsonStatusDetails', NULL );
		}
		unset( $this->m_strStatusDetails );
	}

	public function getStatusDetails() {
		if( true == isset( $this->m_strStatusDetails ) ) {
			$this->m_jsonStatusDetails = CStrings::strToJson( $this->m_strStatusDetails );
			unset( $this->m_strStatusDetails );
		}
		return $this->m_jsonStatusDetails;
	}

	public function sqlStatusDetails() {
		if( false == is_null( CStrings::jsonToStrDef( $this->getStatusDetails() ) ) ) {
			return	'\'' . addslashes( CStrings::jsonToStrDef( $this->getStatusDetails() ) ) . '\'';
		}
		return 'NULL';
	}

	public function setIsInitialImport( $boolIsInitialImport ) {
		$this->set( 'm_boolIsInitialImport', CStrings::strToBool( $boolIsInitialImport ) );
	}

	public function getIsInitialImport() {
		return $this->m_boolIsInitialImport;
	}

	public function sqlIsInitialImport() {
		return ( true == isset( $this->m_boolIsInitialImport ) ) ? '\'' . ( true == ( bool ) $this->m_boolIsInitialImport ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setMigrationStartDate( $strMigrationStartDate ) {
		$this->set( 'm_strMigrationStartDate', CStrings::strTrimDef( $strMigrationStartDate, -1, NULL, true ) );
	}

	public function getMigrationStartDate() {
		return $this->m_strMigrationStartDate;
	}

	public function sqlMigrationStartDate() {
		return ( true == isset( $this->m_strMigrationStartDate ) ) ? '\'' . $this->m_strMigrationStartDate . '\'' : 'NOW()';
	}

	public function setProjectManagerUserId( $intProjectManagerUserId ) {
		$this->set( 'm_intProjectManagerUserId', CStrings::strToIntDef( $intProjectManagerUserId, NULL, false ) );
	}

	public function getProjectManagerUserId() {
		return $this->m_intProjectManagerUserId;
	}

	public function sqlProjectManagerUserId() {
		return ( true == isset( $this->m_intProjectManagerUserId ) ) ? ( string ) $this->m_intProjectManagerUserId : 'NULL';
	}

	public function setBackupProjectManagerUserId( $intBackupProjectManagerUserId ) {
		$this->set( 'm_intBackupProjectManagerUserId', CStrings::strToIntDef( $intBackupProjectManagerUserId, NULL, false ) );
	}

	public function getBackupProjectManagerUserId() {
		return $this->m_intBackupProjectManagerUserId;
	}

	public function sqlBackupProjectManagerUserId() {
		return ( true == isset( $this->m_intBackupProjectManagerUserId ) ) ? ( string ) $this->m_intBackupProjectManagerUserId : 'NULL';
	}

	public function setBackupMigrationConsultantUserId( $intBackupMigrationConsultantUserId ) {
		$this->set( 'm_intBackupMigrationConsultantUserId', CStrings::strToIntDef( $intBackupMigrationConsultantUserId, NULL, false ) );
	}

	public function getBackupMigrationConsultantUserId() {
		return $this->m_intBackupMigrationConsultantUserId;
	}

	public function sqlBackupMigrationConsultantUserId() {
		return ( true == isset( $this->m_intBackupMigrationConsultantUserId ) ) ? ( string ) $this->m_intBackupMigrationConsultantUserId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, cid, property_id, migration_workbook_status_type_id, import_type_ids, migration_consultant_user_id, back_office_user_id, core_consultant_user_id, primary_company_user_ids, secondary_company_user_ids, second_back_office_user_id, backup_core_consultant_user_id, acc_consultant_user_id, backup_acc_consultant_user_id, import_data_type_id, competitor_id, as_of_date, data_values, custom_mapping_data_values, entrata_unit_count, entrata_space_count, is_test_migration, is_takeover, signature, accounting_sent_by, accounting_sent_on, sent_by, sent_on, signed_by, signed_on, rejected_by, rejected_on, deleted_by, deleted_on, updated_by, updated_on, created_by, created_on, status_details, is_initial_import, migration_start_date, project_manager_user_id, backup_project_manager_user_id, backup_migration_consultant_user_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlCid() . ', ' .
						$this->sqlPropertyId() . ', ' .
						$this->sqlMigrationWorkbookStatusTypeId() . ', ' .
						$this->sqlImportTypeIds() . ', ' .
						$this->sqlMigrationConsultantUserId() . ', ' .
						$this->sqlBackOfficeUserId() . ', ' .
						$this->sqlCoreConsultantUserId() . ', ' .
						$this->sqlPrimaryCompanyUserIds() . ', ' .
						$this->sqlSecondaryCompanyUserIds() . ', ' .
						$this->sqlSecondBackOfficeUserId() . ', ' .
						$this->sqlBackupCoreConsultantUserId() . ', ' .
						$this->sqlAccConsultantUserId() . ', ' .
						$this->sqlBackupAccConsultantUserId() . ', ' .
						$this->sqlImportDataTypeId() . ', ' .
						$this->sqlCompetitorId() . ', ' .
						$this->sqlAsOfDate() . ', ' .
						$this->sqlDataValues() . ', ' .
						$this->sqlCustomMappingDataValues() . ', ' .
						$this->sqlEntrataUnitCount() . ', ' .
						$this->sqlEntrataSpaceCount() . ', ' .
						$this->sqlIsTestMigration() . ', ' .
						$this->sqlIsTakeover() . ', ' .
						$this->sqlSignature() . ', ' .
						$this->sqlAccountingSentBy() . ', ' .
						$this->sqlAccountingSentOn() . ', ' .
						$this->sqlSentBy() . ', ' .
						$this->sqlSentOn() . ', ' .
						$this->sqlSignedBy() . ', ' .
						$this->sqlSignedOn() . ', ' .
						$this->sqlRejectedBy() . ', ' .
						$this->sqlRejectedOn() . ', ' .
						$this->sqlDeletedBy() . ', ' .
						$this->sqlDeletedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlStatusDetails() . ', ' .
						$this->sqlIsInitialImport() . ', ' .
						$this->sqlMigrationStartDate() . ', ' .
						$this->sqlProjectManagerUserId() . ', ' .
						$this->sqlBackupProjectManagerUserId() . ', ' .
						$this->sqlBackupMigrationConsultantUserId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' cid = ' . $this->sqlCid(). ',' ; } elseif( true == array_key_exists( 'Cid', $this->getChangedColumns() ) ) { $strSql .= ' cid = ' . $this->sqlCid() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' property_id = ' . $this->sqlPropertyId(). ',' ; } elseif( true == array_key_exists( 'PropertyId', $this->getChangedColumns() ) ) { $strSql .= ' property_id = ' . $this->sqlPropertyId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' migration_workbook_status_type_id = ' . $this->sqlMigrationWorkbookStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'MigrationWorkbookStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' migration_workbook_status_type_id = ' . $this->sqlMigrationWorkbookStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_type_ids = ' . $this->sqlImportTypeIds(). ',' ; } elseif( true == array_key_exists( 'ImportTypeIds', $this->getChangedColumns() ) ) { $strSql .= ' import_type_ids = ' . $this->sqlImportTypeIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' migration_consultant_user_id = ' . $this->sqlMigrationConsultantUserId(). ',' ; } elseif( true == array_key_exists( 'MigrationConsultantUserId', $this->getChangedColumns() ) ) { $strSql .= ' migration_consultant_user_id = ' . $this->sqlMigrationConsultantUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' back_office_user_id = ' . $this->sqlBackOfficeUserId(). ',' ; } elseif( true == array_key_exists( 'BackOfficeUserId', $this->getChangedColumns() ) ) { $strSql .= ' back_office_user_id = ' . $this->sqlBackOfficeUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' core_consultant_user_id = ' . $this->sqlCoreConsultantUserId(). ',' ; } elseif( true == array_key_exists( 'CoreConsultantUserId', $this->getChangedColumns() ) ) { $strSql .= ' core_consultant_user_id = ' . $this->sqlCoreConsultantUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_company_user_ids = ' . $this->sqlPrimaryCompanyUserIds(). ',' ; } elseif( true == array_key_exists( 'PrimaryCompanyUserIds', $this->getChangedColumns() ) ) { $strSql .= ' primary_company_user_ids = ' . $this->sqlPrimaryCompanyUserIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secondary_company_user_ids = ' . $this->sqlSecondaryCompanyUserIds(). ',' ; } elseif( true == array_key_exists( 'SecondaryCompanyUserIds', $this->getChangedColumns() ) ) { $strSql .= ' secondary_company_user_ids = ' . $this->sqlSecondaryCompanyUserIds() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' second_back_office_user_id = ' . $this->sqlSecondBackOfficeUserId(). ',' ; } elseif( true == array_key_exists( 'SecondBackOfficeUserId', $this->getChangedColumns() ) ) { $strSql .= ' second_back_office_user_id = ' . $this->sqlSecondBackOfficeUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' backup_core_consultant_user_id = ' . $this->sqlBackupCoreConsultantUserId(). ',' ; } elseif( true == array_key_exists( 'BackupCoreConsultantUserId', $this->getChangedColumns() ) ) { $strSql .= ' backup_core_consultant_user_id = ' . $this->sqlBackupCoreConsultantUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' acc_consultant_user_id = ' . $this->sqlAccConsultantUserId(). ',' ; } elseif( true == array_key_exists( 'AccConsultantUserId', $this->getChangedColumns() ) ) { $strSql .= ' acc_consultant_user_id = ' . $this->sqlAccConsultantUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' backup_acc_consultant_user_id = ' . $this->sqlBackupAccConsultantUserId(). ',' ; } elseif( true == array_key_exists( 'BackupAccConsultantUserId', $this->getChangedColumns() ) ) { $strSql .= ' backup_acc_consultant_user_id = ' . $this->sqlBackupAccConsultantUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' import_data_type_id = ' . $this->sqlImportDataTypeId(). ',' ; } elseif( true == array_key_exists( 'ImportDataTypeId', $this->getChangedColumns() ) ) { $strSql .= ' import_data_type_id = ' . $this->sqlImportDataTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId(). ',' ; } elseif( true == array_key_exists( 'CompetitorId', $this->getChangedColumns() ) ) { $strSql .= ' competitor_id = ' . $this->sqlCompetitorId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' as_of_date = ' . $this->sqlAsOfDate(). ',' ; } elseif( true == array_key_exists( 'AsOfDate', $this->getChangedColumns() ) ) { $strSql .= ' as_of_date = ' . $this->sqlAsOfDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' data_values = ' . $this->sqlDataValues(). ',' ; } elseif( true == array_key_exists( 'DataValues', $this->getChangedColumns() ) ) { $strSql .= ' data_values = ' . $this->sqlDataValues() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' custom_mapping_data_values = ' . $this->sqlCustomMappingDataValues(). ',' ; } elseif( true == array_key_exists( 'CustomMappingDataValues', $this->getChangedColumns() ) ) { $strSql .= ' custom_mapping_data_values = ' . $this->sqlCustomMappingDataValues() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_unit_count = ' . $this->sqlEntrataUnitCount(). ',' ; } elseif( true == array_key_exists( 'EntrataUnitCount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_unit_count = ' . $this->sqlEntrataUnitCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' entrata_space_count = ' . $this->sqlEntrataSpaceCount(). ',' ; } elseif( true == array_key_exists( 'EntrataSpaceCount', $this->getChangedColumns() ) ) { $strSql .= ' entrata_space_count = ' . $this->sqlEntrataSpaceCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_test_migration = ' . $this->sqlIsTestMigration(). ',' ; } elseif( true == array_key_exists( 'IsTestMigration', $this->getChangedColumns() ) ) { $strSql .= ' is_test_migration = ' . $this->sqlIsTestMigration() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_takeover = ' . $this->sqlIsTakeover(). ',' ; } elseif( true == array_key_exists( 'IsTakeover', $this->getChangedColumns() ) ) { $strSql .= ' is_takeover = ' . $this->sqlIsTakeover() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signature = ' . $this->sqlSignature(). ',' ; } elseif( true == array_key_exists( 'Signature', $this->getChangedColumns() ) ) { $strSql .= ' signature = ' . $this->sqlSignature() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accounting_sent_by = ' . $this->sqlAccountingSentBy(). ',' ; } elseif( true == array_key_exists( 'AccountingSentBy', $this->getChangedColumns() ) ) { $strSql .= ' accounting_sent_by = ' . $this->sqlAccountingSentBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' accounting_sent_on = ' . $this->sqlAccountingSentOn(). ',' ; } elseif( true == array_key_exists( 'AccountingSentOn', $this->getChangedColumns() ) ) { $strSql .= ' accounting_sent_on = ' . $this->sqlAccountingSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_by = ' . $this->sqlSentBy(). ',' ; } elseif( true == array_key_exists( 'SentBy', $this->getChangedColumns() ) ) { $strSql .= ' sent_by = ' . $this->sqlSentBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' sent_on = ' . $this->sqlSentOn(). ',' ; } elseif( true == array_key_exists( 'SentOn', $this->getChangedColumns() ) ) { $strSql .= ' sent_on = ' . $this->sqlSentOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signed_by = ' . $this->sqlSignedBy(). ',' ; } elseif( true == array_key_exists( 'SignedBy', $this->getChangedColumns() ) ) { $strSql .= ' signed_by = ' . $this->sqlSignedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' signed_on = ' . $this->sqlSignedOn(). ',' ; } elseif( true == array_key_exists( 'SignedOn', $this->getChangedColumns() ) ) { $strSql .= ' signed_on = ' . $this->sqlSignedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy(). ',' ; } elseif( true == array_key_exists( 'RejectedBy', $this->getChangedColumns() ) ) { $strSql .= ' rejected_by = ' . $this->sqlRejectedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn(). ',' ; } elseif( true == array_key_exists( 'RejectedOn', $this->getChangedColumns() ) ) { $strSql .= ' rejected_on = ' . $this->sqlRejectedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy(). ',' ; } elseif( true == array_key_exists( 'DeletedBy', $this->getChangedColumns() ) ) { $strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn(). ',' ; } elseif( true == array_key_exists( 'DeletedOn', $this->getChangedColumns() ) ) { $strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' status_details = ' . $this->sqlStatusDetails(). ',' ; } elseif( true == array_key_exists( 'StatusDetails', $this->getChangedColumns() ) ) { $strSql .= ' status_details = ' . $this->sqlStatusDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport(). ',' ; } elseif( true == array_key_exists( 'IsInitialImport', $this->getChangedColumns() ) ) { $strSql .= ' is_initial_import = ' . $this->sqlIsInitialImport() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' migration_start_date = ' . $this->sqlMigrationStartDate(). ',' ; } elseif( true == array_key_exists( 'MigrationStartDate', $this->getChangedColumns() ) ) { $strSql .= ' migration_start_date = ' . $this->sqlMigrationStartDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_manager_user_id = ' . $this->sqlProjectManagerUserId(). ',' ; } elseif( true == array_key_exists( 'ProjectManagerUserId', $this->getChangedColumns() ) ) { $strSql .= ' project_manager_user_id = ' . $this->sqlProjectManagerUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' backup_project_manager_user_id = ' . $this->sqlBackupProjectManagerUserId(). ',' ; } elseif( true == array_key_exists( 'BackupProjectManagerUserId', $this->getChangedColumns() ) ) { $strSql .= ' backup_project_manager_user_id = ' . $this->sqlBackupProjectManagerUserId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' backup_migration_consultant_user_id = ' . $this->sqlBackupMigrationConsultantUserId(). ',' ; } elseif( true == array_key_exists( 'BackupMigrationConsultantUserId', $this->getChangedColumns() ) ) { $strSql .= ' backup_migration_consultant_user_id = ' . $this->sqlBackupMigrationConsultantUserId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';

						$strSql .= ' WHERE
										id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'cid' => $this->getCid(),
			'property_id' => $this->getPropertyId(),
			'migration_workbook_status_type_id' => $this->getMigrationWorkbookStatusTypeId(),
			'import_type_ids' => $this->getImportTypeIds(),
			'migration_consultant_user_id' => $this->getMigrationConsultantUserId(),
			'back_office_user_id' => $this->getBackOfficeUserId(),
			'core_consultant_user_id' => $this->getCoreConsultantUserId(),
			'primary_company_user_ids' => $this->getPrimaryCompanyUserIds(),
			'secondary_company_user_ids' => $this->getSecondaryCompanyUserIds(),
			'second_back_office_user_id' => $this->getSecondBackOfficeUserId(),
			'backup_core_consultant_user_id' => $this->getBackupCoreConsultantUserId(),
			'acc_consultant_user_id' => $this->getAccConsultantUserId(),
			'backup_acc_consultant_user_id' => $this->getBackupAccConsultantUserId(),
			'import_data_type_id' => $this->getImportDataTypeId(),
			'competitor_id' => $this->getCompetitorId(),
			'as_of_date' => $this->getAsOfDate(),
			'data_values' => $this->getDataValues(),
			'custom_mapping_data_values' => $this->getCustomMappingDataValues(),
			'entrata_unit_count' => $this->getEntrataUnitCount(),
			'entrata_space_count' => $this->getEntrataSpaceCount(),
			'is_test_migration' => $this->getIsTestMigration(),
			'is_takeover' => $this->getIsTakeover(),
			'signature' => $this->getSignature(),
			'accounting_sent_by' => $this->getAccountingSentBy(),
			'accounting_sent_on' => $this->getAccountingSentOn(),
			'sent_by' => $this->getSentBy(),
			'sent_on' => $this->getSentOn(),
			'signed_by' => $this->getSignedBy(),
			'signed_on' => $this->getSignedOn(),
			'rejected_by' => $this->getRejectedBy(),
			'rejected_on' => $this->getRejectedOn(),
			'deleted_by' => $this->getDeletedBy(),
			'deleted_on' => $this->getDeletedOn(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'status_details' => $this->getStatusDetails(),
			'is_initial_import' => $this->getIsInitialImport(),
			'migration_start_date' => $this->getMigrationStartDate(),
			'project_manager_user_id' => $this->getProjectManagerUserId(),
			'backup_project_manager_user_id' => $this->getBackupProjectManagerUserId(),
			'backup_migration_consultant_user_id' => $this->getBackupMigrationConsultantUserId()
		);
	}

}
?>