<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeAssessmentTypeLevels
 * Do not add any new functions to this class.
 */

class CBaseEmployeeAssessmentTypeLevels extends CEosPluralBase {

	/**
	 * @return CEmployeeAssessmentTypeLevel[]
	 */
	public static function fetchEmployeeAssessmentTypeLevels( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeAssessmentTypeLevel', $objDatabase );
	}

	/**
	 * @return CEmployeeAssessmentTypeLevel
	 */
	public static function fetchEmployeeAssessmentTypeLevel( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeAssessmentTypeLevel', $objDatabase );
	}

	public static function fetchEmployeeAssessmentTypeLevelCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_assessment_type_levels', $objDatabase );
	}

	public static function fetchEmployeeAssessmentTypeLevelById( $intId, $objDatabase ) {
		return self::fetchEmployeeAssessmentTypeLevel( sprintf( 'SELECT * FROM employee_assessment_type_levels WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>