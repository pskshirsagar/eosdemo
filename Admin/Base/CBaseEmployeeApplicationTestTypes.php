<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationTestTypes
 * Do not add any new functions to this class.
 */

class CBaseEmployeeApplicationTestTypes extends CEosPluralBase {

	/**
	 * @return CEmployeeApplicationTestType[]
	 */
	public static function fetchEmployeeApplicationTestTypes( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeApplicationTestType', $objDatabase );
	}

	/**
	 * @return CEmployeeApplicationTestType
	 */
	public static function fetchEmployeeApplicationTestType( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CEmployeeApplicationTestType', $objDatabase );
	}

	public static function fetchEmployeeApplicationTestTypeCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'employee_application_test_types', $objDatabase );
	}

	public static function fetchEmployeeApplicationTestTypeById( $intId, $objDatabase ) {
		return self::fetchEmployeeApplicationTestType( sprintf( 'SELECT * FROM employee_application_test_types WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

}
?>