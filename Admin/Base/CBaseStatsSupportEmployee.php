<?php

class CBaseStatsSupportEmployee extends CEosSingularBase {

	const TABLE_NAME = 'public.stats_support_employees';

	protected $m_intId;
	protected $m_intEmployeeId;
	protected $m_intDepartmentId;
	protected $m_strDay;
	protected $m_intTicketContactCount;
	protected $m_intAvgMinutesToContact;
	protected $m_intFirstDayCompletions;
	protected $m_intNpsCount;
	protected $m_fltAverageNpsScore;
	protected $m_intQaCount;
	protected $m_fltQaExpertiseRating;
	protected $m_fltQaProfessionalismRating;
	protected $m_intNonEscalatedNewCalls;
	protected $m_intNonEscalatedNewChats;
	protected $m_intNonEscalatedNewTexts;
	protected $m_intNonEscalatedNewEmails;
	protected $m_intNonEscalatedNewTotal;
	protected $m_intNonEscalatedOpenCalls;
	protected $m_intNonEscalatedOpenChats;
	protected $m_intNonEscalatedOpenTexts;
	protected $m_intNonEscalatedOpenEmails;
	protected $m_intNonEscalatedOpenTotal;
	protected $m_intNonEscalatedClosedCalls;
	protected $m_intNonEscalatedClosedChats;
	protected $m_intNonEscalatedClosedTexts;
	protected $m_intNonEscalatedClosedEmails;
	protected $m_intNonEscalatedClosedTotal;
	protected $m_intNonEscalatedAvgOpenMins;
	protected $m_intEscalatedNew;
	protected $m_intEscalatedOpen;
	protected $m_intEscalatedClosed;
	protected $m_intEscalatedAvgOpenMins;
	protected $m_intReopenCount;
	protected $m_intAvgMinutesToClose;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	public function __construct() {
		parent::__construct();

		$this->m_intTicketContactCount = '0';
		$this->m_intAvgMinutesToContact = '0';
		$this->m_intFirstDayCompletions = '0';
		$this->m_intNpsCount = '0';
		$this->m_fltAverageNpsScore = '0';
		$this->m_intQaCount = '0';
		$this->m_fltQaExpertiseRating = '0';
		$this->m_fltQaProfessionalismRating = '0';
		$this->m_intNonEscalatedNewCalls = '0';
		$this->m_intNonEscalatedNewChats = '0';
		$this->m_intNonEscalatedNewTexts = '0';
		$this->m_intNonEscalatedNewEmails = '0';
		$this->m_intNonEscalatedNewTotal = '0';
		$this->m_intNonEscalatedOpenCalls = '0';
		$this->m_intNonEscalatedOpenChats = '0';
		$this->m_intNonEscalatedOpenTexts = '0';
		$this->m_intNonEscalatedOpenEmails = '0';
		$this->m_intNonEscalatedOpenTotal = '0';
		$this->m_intNonEscalatedClosedCalls = '0';
		$this->m_intNonEscalatedClosedChats = '0';
		$this->m_intNonEscalatedClosedTexts = '0';
		$this->m_intNonEscalatedClosedEmails = '0';
		$this->m_intNonEscalatedClosedTotal = '0';
		$this->m_intNonEscalatedAvgOpenMins = '0';
		$this->m_intEscalatedNew = '0';
		$this->m_intEscalatedOpen = '0';
		$this->m_intEscalatedClosed = '0';
		$this->m_intEscalatedAvgOpenMins = '0';
		$this->m_intReopenCount = '0';
		$this->m_intAvgMinutesToClose = '0';

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['department_id'] ) && $boolDirectSet ) $this->set( 'm_intDepartmentId', trim( $arrValues['department_id'] ) ); elseif( isset( $arrValues['department_id'] ) ) $this->setDepartmentId( $arrValues['department_id'] );
		if( isset( $arrValues['day'] ) && $boolDirectSet ) $this->set( 'm_strDay', trim( $arrValues['day'] ) ); elseif( isset( $arrValues['day'] ) ) $this->setDay( $arrValues['day'] );
		if( isset( $arrValues['ticket_contact_count'] ) && $boolDirectSet ) $this->set( 'm_intTicketContactCount', trim( $arrValues['ticket_contact_count'] ) ); elseif( isset( $arrValues['ticket_contact_count'] ) ) $this->setTicketContactCount( $arrValues['ticket_contact_count'] );
		if( isset( $arrValues['avg_minutes_to_contact'] ) && $boolDirectSet ) $this->set( 'm_intAvgMinutesToContact', trim( $arrValues['avg_minutes_to_contact'] ) ); elseif( isset( $arrValues['avg_minutes_to_contact'] ) ) $this->setAvgMinutesToContact( $arrValues['avg_minutes_to_contact'] );
		if( isset( $arrValues['first_day_completions'] ) && $boolDirectSet ) $this->set( 'm_intFirstDayCompletions', trim( $arrValues['first_day_completions'] ) ); elseif( isset( $arrValues['first_day_completions'] ) ) $this->setFirstDayCompletions( $arrValues['first_day_completions'] );
		if( isset( $arrValues['nps_count'] ) && $boolDirectSet ) $this->set( 'm_intNpsCount', trim( $arrValues['nps_count'] ) ); elseif( isset( $arrValues['nps_count'] ) ) $this->setNpsCount( $arrValues['nps_count'] );
		if( isset( $arrValues['average_nps_score'] ) && $boolDirectSet ) $this->set( 'm_fltAverageNpsScore', trim( $arrValues['average_nps_score'] ) ); elseif( isset( $arrValues['average_nps_score'] ) ) $this->setAverageNpsScore( $arrValues['average_nps_score'] );
		if( isset( $arrValues['qa_count'] ) && $boolDirectSet ) $this->set( 'm_intQaCount', trim( $arrValues['qa_count'] ) ); elseif( isset( $arrValues['qa_count'] ) ) $this->setQaCount( $arrValues['qa_count'] );
		if( isset( $arrValues['qa_expertise_rating'] ) && $boolDirectSet ) $this->set( 'm_fltQaExpertiseRating', trim( $arrValues['qa_expertise_rating'] ) ); elseif( isset( $arrValues['qa_expertise_rating'] ) ) $this->setQaExpertiseRating( $arrValues['qa_expertise_rating'] );
		if( isset( $arrValues['qa_professionalism_rating'] ) && $boolDirectSet ) $this->set( 'm_fltQaProfessionalismRating', trim( $arrValues['qa_professionalism_rating'] ) ); elseif( isset( $arrValues['qa_professionalism_rating'] ) ) $this->setQaProfessionalismRating( $arrValues['qa_professionalism_rating'] );
		if( isset( $arrValues['non_escalated_new_calls'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedNewCalls', trim( $arrValues['non_escalated_new_calls'] ) ); elseif( isset( $arrValues['non_escalated_new_calls'] ) ) $this->setNonEscalatedNewCalls( $arrValues['non_escalated_new_calls'] );
		if( isset( $arrValues['non_escalated_new_chats'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedNewChats', trim( $arrValues['non_escalated_new_chats'] ) ); elseif( isset( $arrValues['non_escalated_new_chats'] ) ) $this->setNonEscalatedNewChats( $arrValues['non_escalated_new_chats'] );
		if( isset( $arrValues['non_escalated_new_texts'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedNewTexts', trim( $arrValues['non_escalated_new_texts'] ) ); elseif( isset( $arrValues['non_escalated_new_texts'] ) ) $this->setNonEscalatedNewTexts( $arrValues['non_escalated_new_texts'] );
		if( isset( $arrValues['non_escalated_new_emails'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedNewEmails', trim( $arrValues['non_escalated_new_emails'] ) ); elseif( isset( $arrValues['non_escalated_new_emails'] ) ) $this->setNonEscalatedNewEmails( $arrValues['non_escalated_new_emails'] );
		if( isset( $arrValues['non_escalated_new_total'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedNewTotal', trim( $arrValues['non_escalated_new_total'] ) ); elseif( isset( $arrValues['non_escalated_new_total'] ) ) $this->setNonEscalatedNewTotal( $arrValues['non_escalated_new_total'] );
		if( isset( $arrValues['non_escalated_open_calls'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedOpenCalls', trim( $arrValues['non_escalated_open_calls'] ) ); elseif( isset( $arrValues['non_escalated_open_calls'] ) ) $this->setNonEscalatedOpenCalls( $arrValues['non_escalated_open_calls'] );
		if( isset( $arrValues['non_escalated_open_chats'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedOpenChats', trim( $arrValues['non_escalated_open_chats'] ) ); elseif( isset( $arrValues['non_escalated_open_chats'] ) ) $this->setNonEscalatedOpenChats( $arrValues['non_escalated_open_chats'] );
		if( isset( $arrValues['non_escalated_open_texts'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedOpenTexts', trim( $arrValues['non_escalated_open_texts'] ) ); elseif( isset( $arrValues['non_escalated_open_texts'] ) ) $this->setNonEscalatedOpenTexts( $arrValues['non_escalated_open_texts'] );
		if( isset( $arrValues['non_escalated_open_emails'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedOpenEmails', trim( $arrValues['non_escalated_open_emails'] ) ); elseif( isset( $arrValues['non_escalated_open_emails'] ) ) $this->setNonEscalatedOpenEmails( $arrValues['non_escalated_open_emails'] );
		if( isset( $arrValues['non_escalated_open_total'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedOpenTotal', trim( $arrValues['non_escalated_open_total'] ) ); elseif( isset( $arrValues['non_escalated_open_total'] ) ) $this->setNonEscalatedOpenTotal( $arrValues['non_escalated_open_total'] );
		if( isset( $arrValues['non_escalated_closed_calls'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedClosedCalls', trim( $arrValues['non_escalated_closed_calls'] ) ); elseif( isset( $arrValues['non_escalated_closed_calls'] ) ) $this->setNonEscalatedClosedCalls( $arrValues['non_escalated_closed_calls'] );
		if( isset( $arrValues['non_escalated_closed_chats'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedClosedChats', trim( $arrValues['non_escalated_closed_chats'] ) ); elseif( isset( $arrValues['non_escalated_closed_chats'] ) ) $this->setNonEscalatedClosedChats( $arrValues['non_escalated_closed_chats'] );
		if( isset( $arrValues['non_escalated_closed_texts'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedClosedTexts', trim( $arrValues['non_escalated_closed_texts'] ) ); elseif( isset( $arrValues['non_escalated_closed_texts'] ) ) $this->setNonEscalatedClosedTexts( $arrValues['non_escalated_closed_texts'] );
		if( isset( $arrValues['non_escalated_closed_emails'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedClosedEmails', trim( $arrValues['non_escalated_closed_emails'] ) ); elseif( isset( $arrValues['non_escalated_closed_emails'] ) ) $this->setNonEscalatedClosedEmails( $arrValues['non_escalated_closed_emails'] );
		if( isset( $arrValues['non_escalated_closed_total'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedClosedTotal', trim( $arrValues['non_escalated_closed_total'] ) ); elseif( isset( $arrValues['non_escalated_closed_total'] ) ) $this->setNonEscalatedClosedTotal( $arrValues['non_escalated_closed_total'] );
		if( isset( $arrValues['non_escalated_avg_open_mins'] ) && $boolDirectSet ) $this->set( 'm_intNonEscalatedAvgOpenMins', trim( $arrValues['non_escalated_avg_open_mins'] ) ); elseif( isset( $arrValues['non_escalated_avg_open_mins'] ) ) $this->setNonEscalatedAvgOpenMins( $arrValues['non_escalated_avg_open_mins'] );
		if( isset( $arrValues['escalated_new'] ) && $boolDirectSet ) $this->set( 'm_intEscalatedNew', trim( $arrValues['escalated_new'] ) ); elseif( isset( $arrValues['escalated_new'] ) ) $this->setEscalatedNew( $arrValues['escalated_new'] );
		if( isset( $arrValues['escalated_open'] ) && $boolDirectSet ) $this->set( 'm_intEscalatedOpen', trim( $arrValues['escalated_open'] ) ); elseif( isset( $arrValues['escalated_open'] ) ) $this->setEscalatedOpen( $arrValues['escalated_open'] );
		if( isset( $arrValues['escalated_closed'] ) && $boolDirectSet ) $this->set( 'm_intEscalatedClosed', trim( $arrValues['escalated_closed'] ) ); elseif( isset( $arrValues['escalated_closed'] ) ) $this->setEscalatedClosed( $arrValues['escalated_closed'] );
		if( isset( $arrValues['escalated_avg_open_mins'] ) && $boolDirectSet ) $this->set( 'm_intEscalatedAvgOpenMins', trim( $arrValues['escalated_avg_open_mins'] ) ); elseif( isset( $arrValues['escalated_avg_open_mins'] ) ) $this->setEscalatedAvgOpenMins( $arrValues['escalated_avg_open_mins'] );
		if( isset( $arrValues['reopen_count'] ) && $boolDirectSet ) $this->set( 'm_intReopenCount', trim( $arrValues['reopen_count'] ) ); elseif( isset( $arrValues['reopen_count'] ) ) $this->setReopenCount( $arrValues['reopen_count'] );
		if( isset( $arrValues['avg_minutes_to_close'] ) && $boolDirectSet ) $this->set( 'm_intAvgMinutesToClose', trim( $arrValues['avg_minutes_to_close'] ) ); elseif( isset( $arrValues['avg_minutes_to_close'] ) ) $this->setAvgMinutesToClose( $arrValues['avg_minutes_to_close'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->set( 'm_intDepartmentId', CStrings::strToIntDef( $intDepartmentId, NULL, false ) );
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function sqlDepartmentId() {
		return ( true == isset( $this->m_intDepartmentId ) ) ? ( string ) $this->m_intDepartmentId : 'NULL';
	}

	public function setDay( $strDay ) {
		$this->set( 'm_strDay', CStrings::strTrimDef( $strDay, -1, NULL, true ) );
	}

	public function getDay() {
		return $this->m_strDay;
	}

	public function sqlDay() {
		return ( true == isset( $this->m_strDay ) ) ? '\'' . $this->m_strDay . '\'' : 'NOW()';
	}

	public function setTicketContactCount( $intTicketContactCount ) {
		$this->set( 'm_intTicketContactCount', CStrings::strToIntDef( $intTicketContactCount, NULL, false ) );
	}

	public function getTicketContactCount() {
		return $this->m_intTicketContactCount;
	}

	public function sqlTicketContactCount() {
		return ( true == isset( $this->m_intTicketContactCount ) ) ? ( string ) $this->m_intTicketContactCount : '0';
	}

	public function setAvgMinutesToContact( $intAvgMinutesToContact ) {
		$this->set( 'm_intAvgMinutesToContact', CStrings::strToIntDef( $intAvgMinutesToContact, NULL, false ) );
	}

	public function getAvgMinutesToContact() {
		return $this->m_intAvgMinutesToContact;
	}

	public function sqlAvgMinutesToContact() {
		return ( true == isset( $this->m_intAvgMinutesToContact ) ) ? ( string ) $this->m_intAvgMinutesToContact : '0';
	}

	public function setFirstDayCompletions( $intFirstDayCompletions ) {
		$this->set( 'm_intFirstDayCompletions', CStrings::strToIntDef( $intFirstDayCompletions, NULL, false ) );
	}

	public function getFirstDayCompletions() {
		return $this->m_intFirstDayCompletions;
	}

	public function sqlFirstDayCompletions() {
		return ( true == isset( $this->m_intFirstDayCompletions ) ) ? ( string ) $this->m_intFirstDayCompletions : '0';
	}

	public function setNpsCount( $intNpsCount ) {
		$this->set( 'm_intNpsCount', CStrings::strToIntDef( $intNpsCount, NULL, false ) );
	}

	public function getNpsCount() {
		return $this->m_intNpsCount;
	}

	public function sqlNpsCount() {
		return ( true == isset( $this->m_intNpsCount ) ) ? ( string ) $this->m_intNpsCount : '0';
	}

	public function setAverageNpsScore( $fltAverageNpsScore ) {
		$this->set( 'm_fltAverageNpsScore', CStrings::strToFloatDef( $fltAverageNpsScore, NULL, false, 1 ) );
	}

	public function getAverageNpsScore() {
		return $this->m_fltAverageNpsScore;
	}

	public function sqlAverageNpsScore() {
		return ( true == isset( $this->m_fltAverageNpsScore ) ) ? ( string ) $this->m_fltAverageNpsScore : '0';
	}

	public function setQaCount( $intQaCount ) {
		$this->set( 'm_intQaCount', CStrings::strToIntDef( $intQaCount, NULL, false ) );
	}

	public function getQaCount() {
		return $this->m_intQaCount;
	}

	public function sqlQaCount() {
		return ( true == isset( $this->m_intQaCount ) ) ? ( string ) $this->m_intQaCount : '0';
	}

	public function setQaExpertiseRating( $fltQaExpertiseRating ) {
		$this->set( 'm_fltQaExpertiseRating', CStrings::strToFloatDef( $fltQaExpertiseRating, NULL, false, 1 ) );
	}

	public function getQaExpertiseRating() {
		return $this->m_fltQaExpertiseRating;
	}

	public function sqlQaExpertiseRating() {
		return ( true == isset( $this->m_fltQaExpertiseRating ) ) ? ( string ) $this->m_fltQaExpertiseRating : '0';
	}

	public function setQaProfessionalismRating( $fltQaProfessionalismRating ) {
		$this->set( 'm_fltQaProfessionalismRating', CStrings::strToFloatDef( $fltQaProfessionalismRating, NULL, false, 1 ) );
	}

	public function getQaProfessionalismRating() {
		return $this->m_fltQaProfessionalismRating;
	}

	public function sqlQaProfessionalismRating() {
		return ( true == isset( $this->m_fltQaProfessionalismRating ) ) ? ( string ) $this->m_fltQaProfessionalismRating : '0';
	}

	public function setNonEscalatedNewCalls( $intNonEscalatedNewCalls ) {
		$this->set( 'm_intNonEscalatedNewCalls', CStrings::strToIntDef( $intNonEscalatedNewCalls, NULL, false ) );
	}

	public function getNonEscalatedNewCalls() {
		return $this->m_intNonEscalatedNewCalls;
	}

	public function sqlNonEscalatedNewCalls() {
		return ( true == isset( $this->m_intNonEscalatedNewCalls ) ) ? ( string ) $this->m_intNonEscalatedNewCalls : '0';
	}

	public function setNonEscalatedNewChats( $intNonEscalatedNewChats ) {
		$this->set( 'm_intNonEscalatedNewChats', CStrings::strToIntDef( $intNonEscalatedNewChats, NULL, false ) );
	}

	public function getNonEscalatedNewChats() {
		return $this->m_intNonEscalatedNewChats;
	}

	public function sqlNonEscalatedNewChats() {
		return ( true == isset( $this->m_intNonEscalatedNewChats ) ) ? ( string ) $this->m_intNonEscalatedNewChats : '0';
	}

	public function setNonEscalatedNewTexts( $intNonEscalatedNewTexts ) {
		$this->set( 'm_intNonEscalatedNewTexts', CStrings::strToIntDef( $intNonEscalatedNewTexts, NULL, false ) );
	}

	public function getNonEscalatedNewTexts() {
		return $this->m_intNonEscalatedNewTexts;
	}

	public function sqlNonEscalatedNewTexts() {
		return ( true == isset( $this->m_intNonEscalatedNewTexts ) ) ? ( string ) $this->m_intNonEscalatedNewTexts : '0';
	}

	public function setNonEscalatedNewEmails( $intNonEscalatedNewEmails ) {
		$this->set( 'm_intNonEscalatedNewEmails', CStrings::strToIntDef( $intNonEscalatedNewEmails, NULL, false ) );
	}

	public function getNonEscalatedNewEmails() {
		return $this->m_intNonEscalatedNewEmails;
	}

	public function sqlNonEscalatedNewEmails() {
		return ( true == isset( $this->m_intNonEscalatedNewEmails ) ) ? ( string ) $this->m_intNonEscalatedNewEmails : '0';
	}

	public function setNonEscalatedNewTotal( $intNonEscalatedNewTotal ) {
		$this->set( 'm_intNonEscalatedNewTotal', CStrings::strToIntDef( $intNonEscalatedNewTotal, NULL, false ) );
	}

	public function getNonEscalatedNewTotal() {
		return $this->m_intNonEscalatedNewTotal;
	}

	public function sqlNonEscalatedNewTotal() {
		return ( true == isset( $this->m_intNonEscalatedNewTotal ) ) ? ( string ) $this->m_intNonEscalatedNewTotal : '0';
	}

	public function setNonEscalatedOpenCalls( $intNonEscalatedOpenCalls ) {
		$this->set( 'm_intNonEscalatedOpenCalls', CStrings::strToIntDef( $intNonEscalatedOpenCalls, NULL, false ) );
	}

	public function getNonEscalatedOpenCalls() {
		return $this->m_intNonEscalatedOpenCalls;
	}

	public function sqlNonEscalatedOpenCalls() {
		return ( true == isset( $this->m_intNonEscalatedOpenCalls ) ) ? ( string ) $this->m_intNonEscalatedOpenCalls : '0';
	}

	public function setNonEscalatedOpenChats( $intNonEscalatedOpenChats ) {
		$this->set( 'm_intNonEscalatedOpenChats', CStrings::strToIntDef( $intNonEscalatedOpenChats, NULL, false ) );
	}

	public function getNonEscalatedOpenChats() {
		return $this->m_intNonEscalatedOpenChats;
	}

	public function sqlNonEscalatedOpenChats() {
		return ( true == isset( $this->m_intNonEscalatedOpenChats ) ) ? ( string ) $this->m_intNonEscalatedOpenChats : '0';
	}

	public function setNonEscalatedOpenTexts( $intNonEscalatedOpenTexts ) {
		$this->set( 'm_intNonEscalatedOpenTexts', CStrings::strToIntDef( $intNonEscalatedOpenTexts, NULL, false ) );
	}

	public function getNonEscalatedOpenTexts() {
		return $this->m_intNonEscalatedOpenTexts;
	}

	public function sqlNonEscalatedOpenTexts() {
		return ( true == isset( $this->m_intNonEscalatedOpenTexts ) ) ? ( string ) $this->m_intNonEscalatedOpenTexts : '0';
	}

	public function setNonEscalatedOpenEmails( $intNonEscalatedOpenEmails ) {
		$this->set( 'm_intNonEscalatedOpenEmails', CStrings::strToIntDef( $intNonEscalatedOpenEmails, NULL, false ) );
	}

	public function getNonEscalatedOpenEmails() {
		return $this->m_intNonEscalatedOpenEmails;
	}

	public function sqlNonEscalatedOpenEmails() {
		return ( true == isset( $this->m_intNonEscalatedOpenEmails ) ) ? ( string ) $this->m_intNonEscalatedOpenEmails : '0';
	}

	public function setNonEscalatedOpenTotal( $intNonEscalatedOpenTotal ) {
		$this->set( 'm_intNonEscalatedOpenTotal', CStrings::strToIntDef( $intNonEscalatedOpenTotal, NULL, false ) );
	}

	public function getNonEscalatedOpenTotal() {
		return $this->m_intNonEscalatedOpenTotal;
	}

	public function sqlNonEscalatedOpenTotal() {
		return ( true == isset( $this->m_intNonEscalatedOpenTotal ) ) ? ( string ) $this->m_intNonEscalatedOpenTotal : '0';
	}

	public function setNonEscalatedClosedCalls( $intNonEscalatedClosedCalls ) {
		$this->set( 'm_intNonEscalatedClosedCalls', CStrings::strToIntDef( $intNonEscalatedClosedCalls, NULL, false ) );
	}

	public function getNonEscalatedClosedCalls() {
		return $this->m_intNonEscalatedClosedCalls;
	}

	public function sqlNonEscalatedClosedCalls() {
		return ( true == isset( $this->m_intNonEscalatedClosedCalls ) ) ? ( string ) $this->m_intNonEscalatedClosedCalls : '0';
	}

	public function setNonEscalatedClosedChats( $intNonEscalatedClosedChats ) {
		$this->set( 'm_intNonEscalatedClosedChats', CStrings::strToIntDef( $intNonEscalatedClosedChats, NULL, false ) );
	}

	public function getNonEscalatedClosedChats() {
		return $this->m_intNonEscalatedClosedChats;
	}

	public function sqlNonEscalatedClosedChats() {
		return ( true == isset( $this->m_intNonEscalatedClosedChats ) ) ? ( string ) $this->m_intNonEscalatedClosedChats : '0';
	}

	public function setNonEscalatedClosedTexts( $intNonEscalatedClosedTexts ) {
		$this->set( 'm_intNonEscalatedClosedTexts', CStrings::strToIntDef( $intNonEscalatedClosedTexts, NULL, false ) );
	}

	public function getNonEscalatedClosedTexts() {
		return $this->m_intNonEscalatedClosedTexts;
	}

	public function sqlNonEscalatedClosedTexts() {
		return ( true == isset( $this->m_intNonEscalatedClosedTexts ) ) ? ( string ) $this->m_intNonEscalatedClosedTexts : '0';
	}

	public function setNonEscalatedClosedEmails( $intNonEscalatedClosedEmails ) {
		$this->set( 'm_intNonEscalatedClosedEmails', CStrings::strToIntDef( $intNonEscalatedClosedEmails, NULL, false ) );
	}

	public function getNonEscalatedClosedEmails() {
		return $this->m_intNonEscalatedClosedEmails;
	}

	public function sqlNonEscalatedClosedEmails() {
		return ( true == isset( $this->m_intNonEscalatedClosedEmails ) ) ? ( string ) $this->m_intNonEscalatedClosedEmails : '0';
	}

	public function setNonEscalatedClosedTotal( $intNonEscalatedClosedTotal ) {
		$this->set( 'm_intNonEscalatedClosedTotal', CStrings::strToIntDef( $intNonEscalatedClosedTotal, NULL, false ) );
	}

	public function getNonEscalatedClosedTotal() {
		return $this->m_intNonEscalatedClosedTotal;
	}

	public function sqlNonEscalatedClosedTotal() {
		return ( true == isset( $this->m_intNonEscalatedClosedTotal ) ) ? ( string ) $this->m_intNonEscalatedClosedTotal : '0';
	}

	public function setNonEscalatedAvgOpenMins( $intNonEscalatedAvgOpenMins ) {
		$this->set( 'm_intNonEscalatedAvgOpenMins', CStrings::strToIntDef( $intNonEscalatedAvgOpenMins, NULL, false ) );
	}

	public function getNonEscalatedAvgOpenMins() {
		return $this->m_intNonEscalatedAvgOpenMins;
	}

	public function sqlNonEscalatedAvgOpenMins() {
		return ( true == isset( $this->m_intNonEscalatedAvgOpenMins ) ) ? ( string ) $this->m_intNonEscalatedAvgOpenMins : '0';
	}

	public function setEscalatedNew( $intEscalatedNew ) {
		$this->set( 'm_intEscalatedNew', CStrings::strToIntDef( $intEscalatedNew, NULL, false ) );
	}

	public function getEscalatedNew() {
		return $this->m_intEscalatedNew;
	}

	public function sqlEscalatedNew() {
		return ( true == isset( $this->m_intEscalatedNew ) ) ? ( string ) $this->m_intEscalatedNew : '0';
	}

	public function setEscalatedOpen( $intEscalatedOpen ) {
		$this->set( 'm_intEscalatedOpen', CStrings::strToIntDef( $intEscalatedOpen, NULL, false ) );
	}

	public function getEscalatedOpen() {
		return $this->m_intEscalatedOpen;
	}

	public function sqlEscalatedOpen() {
		return ( true == isset( $this->m_intEscalatedOpen ) ) ? ( string ) $this->m_intEscalatedOpen : '0';
	}

	public function setEscalatedClosed( $intEscalatedClosed ) {
		$this->set( 'm_intEscalatedClosed', CStrings::strToIntDef( $intEscalatedClosed, NULL, false ) );
	}

	public function getEscalatedClosed() {
		return $this->m_intEscalatedClosed;
	}

	public function sqlEscalatedClosed() {
		return ( true == isset( $this->m_intEscalatedClosed ) ) ? ( string ) $this->m_intEscalatedClosed : '0';
	}

	public function setEscalatedAvgOpenMins( $intEscalatedAvgOpenMins ) {
		$this->set( 'm_intEscalatedAvgOpenMins', CStrings::strToIntDef( $intEscalatedAvgOpenMins, NULL, false ) );
	}

	public function getEscalatedAvgOpenMins() {
		return $this->m_intEscalatedAvgOpenMins;
	}

	public function sqlEscalatedAvgOpenMins() {
		return ( true == isset( $this->m_intEscalatedAvgOpenMins ) ) ? ( string ) $this->m_intEscalatedAvgOpenMins : '0';
	}

	public function setReopenCount( $intReopenCount ) {
		$this->set( 'm_intReopenCount', CStrings::strToIntDef( $intReopenCount, NULL, false ) );
	}

	public function getReopenCount() {
		return $this->m_intReopenCount;
	}

	public function sqlReopenCount() {
		return ( true == isset( $this->m_intReopenCount ) ) ? ( string ) $this->m_intReopenCount : '0';
	}

	public function setAvgMinutesToClose( $intAvgMinutesToClose ) {
		$this->set( 'm_intAvgMinutesToClose', CStrings::strToIntDef( $intAvgMinutesToClose, NULL, false ) );
	}

	public function getAvgMinutesToClose() {
		return $this->m_intAvgMinutesToClose;
	}

	public function sqlAvgMinutesToClose() {
		return ( true == isset( $this->m_intAvgMinutesToClose ) ) ? ( string ) $this->m_intAvgMinutesToClose : '0';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, employee_id, department_id, day, ticket_contact_count, avg_minutes_to_contact, first_day_completions, nps_count, average_nps_score, qa_count, qa_expertise_rating, qa_professionalism_rating, non_escalated_new_calls, non_escalated_new_chats, non_escalated_new_texts, non_escalated_new_emails, non_escalated_new_total, non_escalated_open_calls, non_escalated_open_chats, non_escalated_open_texts, non_escalated_open_emails, non_escalated_open_total, non_escalated_closed_calls, non_escalated_closed_chats, non_escalated_closed_texts, non_escalated_closed_emails, non_escalated_closed_total, non_escalated_avg_open_mins, escalated_new, escalated_open, escalated_closed, escalated_avg_open_mins, reopen_count, avg_minutes_to_close, created_by, created_on )
					VALUES ( ' .
						$strId . ', ' .
 						$this->sqlEmployeeId() . ', ' .
 						$this->sqlDepartmentId() . ', ' .
 						$this->sqlDay() . ', ' .
 						$this->sqlTicketContactCount() . ', ' .
 						$this->sqlAvgMinutesToContact() . ', ' .
 						$this->sqlFirstDayCompletions() . ', ' .
 						$this->sqlNpsCount() . ', ' .
 						$this->sqlAverageNpsScore() . ', ' .
 						$this->sqlQaCount() . ', ' .
 						$this->sqlQaExpertiseRating() . ', ' .
 						$this->sqlQaProfessionalismRating() . ', ' .
 						$this->sqlNonEscalatedNewCalls() . ', ' .
 						$this->sqlNonEscalatedNewChats() . ', ' .
 						$this->sqlNonEscalatedNewTexts() . ', ' .
 						$this->sqlNonEscalatedNewEmails() . ', ' .
 						$this->sqlNonEscalatedNewTotal() . ', ' .
 						$this->sqlNonEscalatedOpenCalls() . ', ' .
 						$this->sqlNonEscalatedOpenChats() . ', ' .
 						$this->sqlNonEscalatedOpenTexts() . ', ' .
 						$this->sqlNonEscalatedOpenEmails() . ', ' .
 						$this->sqlNonEscalatedOpenTotal() . ', ' .
 						$this->sqlNonEscalatedClosedCalls() . ', ' .
 						$this->sqlNonEscalatedClosedChats() . ', ' .
 						$this->sqlNonEscalatedClosedTexts() . ', ' .
 						$this->sqlNonEscalatedClosedEmails() . ', ' .
 						$this->sqlNonEscalatedClosedTotal() . ', ' .
 						$this->sqlNonEscalatedAvgOpenMins() . ', ' .
 						$this->sqlEscalatedNew() . ', ' .
 						$this->sqlEscalatedOpen() . ', ' .
 						$this->sqlEscalatedClosed() . ', ' .
 						$this->sqlEscalatedAvgOpenMins() . ', ' .
 						$this->sqlReopenCount() . ', ' .
 						$this->sqlAvgMinutesToClose() . ', ' .
						( int ) $intCurrentUserId . ', ' .
 						$this->sqlCreatedOn() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; } elseif( true == array_key_exists( 'DepartmentId', $this->getChangedColumns() ) ) { $strSql .= ' department_id = ' . $this->sqlDepartmentId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' day = ' . $this->sqlDay() . ','; } elseif( true == array_key_exists( 'Day', $this->getChangedColumns() ) ) { $strSql .= ' day = ' . $this->sqlDay() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ticket_contact_count = ' . $this->sqlTicketContactCount() . ','; } elseif( true == array_key_exists( 'TicketContactCount', $this->getChangedColumns() ) ) { $strSql .= ' ticket_contact_count = ' . $this->sqlTicketContactCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_minutes_to_contact = ' . $this->sqlAvgMinutesToContact() . ','; } elseif( true == array_key_exists( 'AvgMinutesToContact', $this->getChangedColumns() ) ) { $strSql .= ' avg_minutes_to_contact = ' . $this->sqlAvgMinutesToContact() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' first_day_completions = ' . $this->sqlFirstDayCompletions() . ','; } elseif( true == array_key_exists( 'FirstDayCompletions', $this->getChangedColumns() ) ) { $strSql .= ' first_day_completions = ' . $this->sqlFirstDayCompletions() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' nps_count = ' . $this->sqlNpsCount() . ','; } elseif( true == array_key_exists( 'NpsCount', $this->getChangedColumns() ) ) { $strSql .= ' nps_count = ' . $this->sqlNpsCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' average_nps_score = ' . $this->sqlAverageNpsScore() . ','; } elseif( true == array_key_exists( 'AverageNpsScore', $this->getChangedColumns() ) ) { $strSql .= ' average_nps_score = ' . $this->sqlAverageNpsScore() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_count = ' . $this->sqlQaCount() . ','; } elseif( true == array_key_exists( 'QaCount', $this->getChangedColumns() ) ) { $strSql .= ' qa_count = ' . $this->sqlQaCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_expertise_rating = ' . $this->sqlQaExpertiseRating() . ','; } elseif( true == array_key_exists( 'QaExpertiseRating', $this->getChangedColumns() ) ) { $strSql .= ' qa_expertise_rating = ' . $this->sqlQaExpertiseRating() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' qa_professionalism_rating = ' . $this->sqlQaProfessionalismRating() . ','; } elseif( true == array_key_exists( 'QaProfessionalismRating', $this->getChangedColumns() ) ) { $strSql .= ' qa_professionalism_rating = ' . $this->sqlQaProfessionalismRating() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_new_calls = ' . $this->sqlNonEscalatedNewCalls() . ','; } elseif( true == array_key_exists( 'NonEscalatedNewCalls', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_new_calls = ' . $this->sqlNonEscalatedNewCalls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_new_chats = ' . $this->sqlNonEscalatedNewChats() . ','; } elseif( true == array_key_exists( 'NonEscalatedNewChats', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_new_chats = ' . $this->sqlNonEscalatedNewChats() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_new_texts = ' . $this->sqlNonEscalatedNewTexts() . ','; } elseif( true == array_key_exists( 'NonEscalatedNewTexts', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_new_texts = ' . $this->sqlNonEscalatedNewTexts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_new_emails = ' . $this->sqlNonEscalatedNewEmails() . ','; } elseif( true == array_key_exists( 'NonEscalatedNewEmails', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_new_emails = ' . $this->sqlNonEscalatedNewEmails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_new_total = ' . $this->sqlNonEscalatedNewTotal() . ','; } elseif( true == array_key_exists( 'NonEscalatedNewTotal', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_new_total = ' . $this->sqlNonEscalatedNewTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_open_calls = ' . $this->sqlNonEscalatedOpenCalls() . ','; } elseif( true == array_key_exists( 'NonEscalatedOpenCalls', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_open_calls = ' . $this->sqlNonEscalatedOpenCalls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_open_chats = ' . $this->sqlNonEscalatedOpenChats() . ','; } elseif( true == array_key_exists( 'NonEscalatedOpenChats', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_open_chats = ' . $this->sqlNonEscalatedOpenChats() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_open_texts = ' . $this->sqlNonEscalatedOpenTexts() . ','; } elseif( true == array_key_exists( 'NonEscalatedOpenTexts', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_open_texts = ' . $this->sqlNonEscalatedOpenTexts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_open_emails = ' . $this->sqlNonEscalatedOpenEmails() . ','; } elseif( true == array_key_exists( 'NonEscalatedOpenEmails', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_open_emails = ' . $this->sqlNonEscalatedOpenEmails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_open_total = ' . $this->sqlNonEscalatedOpenTotal() . ','; } elseif( true == array_key_exists( 'NonEscalatedOpenTotal', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_open_total = ' . $this->sqlNonEscalatedOpenTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_closed_calls = ' . $this->sqlNonEscalatedClosedCalls() . ','; } elseif( true == array_key_exists( 'NonEscalatedClosedCalls', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_closed_calls = ' . $this->sqlNonEscalatedClosedCalls() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_closed_chats = ' . $this->sqlNonEscalatedClosedChats() . ','; } elseif( true == array_key_exists( 'NonEscalatedClosedChats', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_closed_chats = ' . $this->sqlNonEscalatedClosedChats() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_closed_texts = ' . $this->sqlNonEscalatedClosedTexts() . ','; } elseif( true == array_key_exists( 'NonEscalatedClosedTexts', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_closed_texts = ' . $this->sqlNonEscalatedClosedTexts() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_closed_emails = ' . $this->sqlNonEscalatedClosedEmails() . ','; } elseif( true == array_key_exists( 'NonEscalatedClosedEmails', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_closed_emails = ' . $this->sqlNonEscalatedClosedEmails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_closed_total = ' . $this->sqlNonEscalatedClosedTotal() . ','; } elseif( true == array_key_exists( 'NonEscalatedClosedTotal', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_closed_total = ' . $this->sqlNonEscalatedClosedTotal() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' non_escalated_avg_open_mins = ' . $this->sqlNonEscalatedAvgOpenMins() . ','; } elseif( true == array_key_exists( 'NonEscalatedAvgOpenMins', $this->getChangedColumns() ) ) { $strSql .= ' non_escalated_avg_open_mins = ' . $this->sqlNonEscalatedAvgOpenMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalated_new = ' . $this->sqlEscalatedNew() . ','; } elseif( true == array_key_exists( 'EscalatedNew', $this->getChangedColumns() ) ) { $strSql .= ' escalated_new = ' . $this->sqlEscalatedNew() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalated_open = ' . $this->sqlEscalatedOpen() . ','; } elseif( true == array_key_exists( 'EscalatedOpen', $this->getChangedColumns() ) ) { $strSql .= ' escalated_open = ' . $this->sqlEscalatedOpen() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalated_closed = ' . $this->sqlEscalatedClosed() . ','; } elseif( true == array_key_exists( 'EscalatedClosed', $this->getChangedColumns() ) ) { $strSql .= ' escalated_closed = ' . $this->sqlEscalatedClosed() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' escalated_avg_open_mins = ' . $this->sqlEscalatedAvgOpenMins() . ','; } elseif( true == array_key_exists( 'EscalatedAvgOpenMins', $this->getChangedColumns() ) ) { $strSql .= ' escalated_avg_open_mins = ' . $this->sqlEscalatedAvgOpenMins() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' reopen_count = ' . $this->sqlReopenCount() . ','; } elseif( true == array_key_exists( 'ReopenCount', $this->getChangedColumns() ) ) { $strSql .= ' reopen_count = ' . $this->sqlReopenCount() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' avg_minutes_to_close = ' . $this->sqlAvgMinutesToClose() . ','; } elseif( true == array_key_exists( 'AvgMinutesToClose', $this->getChangedColumns() ) ) { $strSql .= ' avg_minutes_to_close = ' . $this->sqlAvgMinutesToClose() . ','; $boolUpdate = true; }

		$strSql = substr( $strSql, 0, -1 );

		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'employee_id' => $this->getEmployeeId(),
			'department_id' => $this->getDepartmentId(),
			'day' => $this->getDay(),
			'ticket_contact_count' => $this->getTicketContactCount(),
			'avg_minutes_to_contact' => $this->getAvgMinutesToContact(),
			'first_day_completions' => $this->getFirstDayCompletions(),
			'nps_count' => $this->getNpsCount(),
			'average_nps_score' => $this->getAverageNpsScore(),
			'qa_count' => $this->getQaCount(),
			'qa_expertise_rating' => $this->getQaExpertiseRating(),
			'qa_professionalism_rating' => $this->getQaProfessionalismRating(),
			'non_escalated_new_calls' => $this->getNonEscalatedNewCalls(),
			'non_escalated_new_chats' => $this->getNonEscalatedNewChats(),
			'non_escalated_new_texts' => $this->getNonEscalatedNewTexts(),
			'non_escalated_new_emails' => $this->getNonEscalatedNewEmails(),
			'non_escalated_new_total' => $this->getNonEscalatedNewTotal(),
			'non_escalated_open_calls' => $this->getNonEscalatedOpenCalls(),
			'non_escalated_open_chats' => $this->getNonEscalatedOpenChats(),
			'non_escalated_open_texts' => $this->getNonEscalatedOpenTexts(),
			'non_escalated_open_emails' => $this->getNonEscalatedOpenEmails(),
			'non_escalated_open_total' => $this->getNonEscalatedOpenTotal(),
			'non_escalated_closed_calls' => $this->getNonEscalatedClosedCalls(),
			'non_escalated_closed_chats' => $this->getNonEscalatedClosedChats(),
			'non_escalated_closed_texts' => $this->getNonEscalatedClosedTexts(),
			'non_escalated_closed_emails' => $this->getNonEscalatedClosedEmails(),
			'non_escalated_closed_total' => $this->getNonEscalatedClosedTotal(),
			'non_escalated_avg_open_mins' => $this->getNonEscalatedAvgOpenMins(),
			'escalated_new' => $this->getEscalatedNew(),
			'escalated_open' => $this->getEscalatedOpen(),
			'escalated_closed' => $this->getEscalatedClosed(),
			'escalated_avg_open_mins' => $this->getEscalatedAvgOpenMins(),
			'reopen_count' => $this->getReopenCount(),
			'avg_minutes_to_close' => $this->getAvgMinutesToClose(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn()
		);
	}

}
?>