<?php

/**
 * EOS BASE PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CActionTypeResults
 * Do not add any new functions to this class.
 */

class CBaseActionTypeResults extends CEosPluralBase {

	/**
	 * @return CActionTypeResult[]
	 */
	public static function fetchActionTypeResults( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CActionTypeResult', $objDatabase );
	}

	/**
	 * @return CActionTypeResult
	 */
	public static function fetchActionTypeResult( $strSql, $objDatabase ) {
		return parent::fetchObject( $strSql, 'CActionTypeResult', $objDatabase );
	}

	public static function fetchActionTypeResultCount( $strWhere, $objDatabase ) {
		return parent::fetchRowCount( $strWhere, 'action_type_results', $objDatabase );
	}

	public static function fetchActionTypeResultById( $intId, $objDatabase ) {
		return self::fetchActionTypeResult( sprintf( 'SELECT * FROM action_type_results WHERE id = %d', ( int ) $intId ), $objDatabase );
	}

	public static function fetchActionTypeResultsByActionTypeId( $intActionTypeId, $objDatabase ) {
		return self::fetchActionTypeResults( sprintf( 'SELECT * FROM action_type_results WHERE action_type_id = %d', ( int ) $intActionTypeId ), $objDatabase );
	}

	public static function fetchActionTypeResultsByActionResultId( $intActionResultId, $objDatabase ) {
		return self::fetchActionTypeResults( sprintf( 'SELECT * FROM action_type_results WHERE action_result_id = %d', ( int ) $intActionResultId ), $objDatabase );
	}

}
?>