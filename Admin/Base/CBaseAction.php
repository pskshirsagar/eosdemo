<?php

class CBaseAction extends CEosSingularBase {

	use TEosDetails;

	const TABLE_NAME = 'public.actions';

	protected $m_intId;
	protected $m_intActionTypeId;
	protected $m_intActionResultId;
	protected $m_intOldStatusTypeId;
	protected $m_intNewStatusTypeId;
	protected $m_intImplementationDelayTypeId;
	protected $m_intPsLeadId;
	protected $m_intContractId;
	protected $m_intPsLeadEventId;
	protected $m_intPersonId;
	protected $m_intPsProductId;
	protected $m_intEmployeeId;
	protected $m_intAccountId;
	protected $m_intPrimaryReference;
	protected $m_intSecondaryReference;
	protected $m_strActionDatetime;
	protected $m_strProjectedEndDate;
	protected $m_strResultDatetime;
	protected $m_intActionMinutes;
	protected $m_strActionDescription;
	protected $m_strNotes;
	protected $m_strProjectTitle;
	protected $m_fltCurrentProductAdoption;
	protected $m_fltExpectedGains;
	protected $m_fltActualGains;
	protected $m_strIpAddress;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;
	protected $m_strDetails;
	protected $m_jsonDetails;
	protected $m_intActionCategoryId;
	protected $m_boolIgnoreInReporting;
	protected $m_intPriorActionId;
	protected $m_intOnHoldReasonId;

	public function __construct() {
		parent::__construct();

		$this->m_strActionDatetime = 'now()';
		$this->m_boolIgnoreInReporting = false;

		return;
	}

	public function setDefaults() {
		return;
	}

	/**
	 * @SuppressWarnings( BooleanArgumentFlag )
	 */
	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		if( isset( $arrValues['id'] ) && $boolDirectSet ) $this->set( 'm_intId', trim( $arrValues['id'] ) ); elseif( isset( $arrValues['id'] ) ) $this->setId( $arrValues['id'] );
		if( isset( $arrValues['action_type_id'] ) && $boolDirectSet ) $this->set( 'm_intActionTypeId', trim( $arrValues['action_type_id'] ) ); elseif( isset( $arrValues['action_type_id'] ) ) $this->setActionTypeId( $arrValues['action_type_id'] );
		if( isset( $arrValues['action_result_id'] ) && $boolDirectSet ) $this->set( 'm_intActionResultId', trim( $arrValues['action_result_id'] ) ); elseif( isset( $arrValues['action_result_id'] ) ) $this->setActionResultId( $arrValues['action_result_id'] );
		if( isset( $arrValues['old_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intOldStatusTypeId', trim( $arrValues['old_status_type_id'] ) ); elseif( isset( $arrValues['old_status_type_id'] ) ) $this->setOldStatusTypeId( $arrValues['old_status_type_id'] );
		if( isset( $arrValues['new_status_type_id'] ) && $boolDirectSet ) $this->set( 'm_intNewStatusTypeId', trim( $arrValues['new_status_type_id'] ) ); elseif( isset( $arrValues['new_status_type_id'] ) ) $this->setNewStatusTypeId( $arrValues['new_status_type_id'] );
		if( isset( $arrValues['implementation_delay_type_id'] ) && $boolDirectSet ) $this->set( 'm_intImplementationDelayTypeId', trim( $arrValues['implementation_delay_type_id'] ) ); elseif( isset( $arrValues['implementation_delay_type_id'] ) ) $this->setImplementationDelayTypeId( $arrValues['implementation_delay_type_id'] );
		if( isset( $arrValues['ps_lead_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadId', trim( $arrValues['ps_lead_id'] ) ); elseif( isset( $arrValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrValues['ps_lead_id'] );
		if( isset( $arrValues['contract_id'] ) && $boolDirectSet ) $this->set( 'm_intContractId', trim( $arrValues['contract_id'] ) ); elseif( isset( $arrValues['contract_id'] ) ) $this->setContractId( $arrValues['contract_id'] );
		if( isset( $arrValues['ps_lead_event_id'] ) && $boolDirectSet ) $this->set( 'm_intPsLeadEventId', trim( $arrValues['ps_lead_event_id'] ) ); elseif( isset( $arrValues['ps_lead_event_id'] ) ) $this->setPsLeadEventId( $arrValues['ps_lead_event_id'] );
		if( isset( $arrValues['person_id'] ) && $boolDirectSet ) $this->set( 'm_intPersonId', trim( $arrValues['person_id'] ) ); elseif( isset( $arrValues['person_id'] ) ) $this->setPersonId( $arrValues['person_id'] );
		if( isset( $arrValues['ps_product_id'] ) && $boolDirectSet ) $this->set( 'm_intPsProductId', trim( $arrValues['ps_product_id'] ) ); elseif( isset( $arrValues['ps_product_id'] ) ) $this->setPsProductId( $arrValues['ps_product_id'] );
		if( isset( $arrValues['employee_id'] ) && $boolDirectSet ) $this->set( 'm_intEmployeeId', trim( $arrValues['employee_id'] ) ); elseif( isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( isset( $arrValues['account_id'] ) && $boolDirectSet ) $this->set( 'm_intAccountId', trim( $arrValues['account_id'] ) ); elseif( isset( $arrValues['account_id'] ) ) $this->setAccountId( $arrValues['account_id'] );
		if( isset( $arrValues['primary_reference'] ) && $boolDirectSet ) $this->set( 'm_intPrimaryReference', trim( $arrValues['primary_reference'] ) ); elseif( isset( $arrValues['primary_reference'] ) ) $this->setPrimaryReference( $arrValues['primary_reference'] );
		if( isset( $arrValues['secondary_reference'] ) && $boolDirectSet ) $this->set( 'm_intSecondaryReference', trim( $arrValues['secondary_reference'] ) ); elseif( isset( $arrValues['secondary_reference'] ) ) $this->setSecondaryReference( $arrValues['secondary_reference'] );
		if( isset( $arrValues['action_datetime'] ) && $boolDirectSet ) $this->set( 'm_strActionDatetime', trim( $arrValues['action_datetime'] ) ); elseif( isset( $arrValues['action_datetime'] ) ) $this->setActionDatetime( $arrValues['action_datetime'] );
		if( isset( $arrValues['projected_end_date'] ) && $boolDirectSet ) $this->set( 'm_strProjectedEndDate', trim( $arrValues['projected_end_date'] ) ); elseif( isset( $arrValues['projected_end_date'] ) ) $this->setProjectedEndDate( $arrValues['projected_end_date'] );
		if( isset( $arrValues['result_datetime'] ) && $boolDirectSet ) $this->set( 'm_strResultDatetime', trim( $arrValues['result_datetime'] ) ); elseif( isset( $arrValues['result_datetime'] ) ) $this->setResultDatetime( $arrValues['result_datetime'] );
		if( isset( $arrValues['action_minutes'] ) && $boolDirectSet ) $this->set( 'm_intActionMinutes', trim( $arrValues['action_minutes'] ) ); elseif( isset( $arrValues['action_minutes'] ) ) $this->setActionMinutes( $arrValues['action_minutes'] );
		if( isset( $arrValues['action_description'] ) && $boolDirectSet ) $this->set( 'm_strActionDescription', trim( $arrValues['action_description'] ) ); elseif( isset( $arrValues['action_description'] ) ) $this->setActionDescription( $arrValues['action_description'] );
		if( isset( $arrValues['notes'] ) && $boolDirectSet ) $this->set( 'm_strNotes', trim( $arrValues['notes'] ) ); elseif( isset( $arrValues['notes'] ) ) $this->setNotes( $arrValues['notes'] );
		if( isset( $arrValues['project_title'] ) && $boolDirectSet ) $this->set( 'm_strProjectTitle', trim( $arrValues['project_title'] ) ); elseif( isset( $arrValues['project_title'] ) ) $this->setProjectTitle( $arrValues['project_title'] );
		if( isset( $arrValues['current_product_adoption'] ) && $boolDirectSet ) $this->set( 'm_fltCurrentProductAdoption', trim( $arrValues['current_product_adoption'] ) ); elseif( isset( $arrValues['current_product_adoption'] ) ) $this->setCurrentProductAdoption( $arrValues['current_product_adoption'] );
		if( isset( $arrValues['expected_gains'] ) && $boolDirectSet ) $this->set( 'm_fltExpectedGains', trim( $arrValues['expected_gains'] ) ); elseif( isset( $arrValues['expected_gains'] ) ) $this->setExpectedGains( $arrValues['expected_gains'] );
		if( isset( $arrValues['actual_gains'] ) && $boolDirectSet ) $this->set( 'm_fltActualGains', trim( $arrValues['actual_gains'] ) ); elseif( isset( $arrValues['actual_gains'] ) ) $this->setActualGains( $arrValues['actual_gains'] );
		if( isset( $arrValues['ip_address'] ) && $boolDirectSet ) $this->set( 'm_strIpAddress', trim( $arrValues['ip_address'] ) ); elseif( isset( $arrValues['ip_address'] ) ) $this->setIpAddress( $arrValues['ip_address'] );
		if( isset( $arrValues['updated_by'] ) && $boolDirectSet ) $this->set( 'm_intUpdatedBy', trim( $arrValues['updated_by'] ) ); elseif( isset( $arrValues['updated_by'] ) ) $this->setUpdatedBy( $arrValues['updated_by'] );
		if( isset( $arrValues['updated_on'] ) && $boolDirectSet ) $this->set( 'm_strUpdatedOn', trim( $arrValues['updated_on'] ) ); elseif( isset( $arrValues['updated_on'] ) ) $this->setUpdatedOn( $arrValues['updated_on'] );
		if( isset( $arrValues['created_by'] ) && $boolDirectSet ) $this->set( 'm_intCreatedBy', trim( $arrValues['created_by'] ) ); elseif( isset( $arrValues['created_by'] ) ) $this->setCreatedBy( $arrValues['created_by'] );
		if( isset( $arrValues['created_on'] ) && $boolDirectSet ) $this->set( 'm_strCreatedOn', trim( $arrValues['created_on'] ) ); elseif( isset( $arrValues['created_on'] ) ) $this->setCreatedOn( $arrValues['created_on'] );
		if( isset( $arrValues['details'] ) ) $this->set( 'm_strDetails', trim( $arrValues['details'] ) );
		if( isset( $arrValues['action_category_id'] ) && $boolDirectSet ) $this->set( 'm_intActionCategoryId', trim( $arrValues['action_category_id'] ) ); elseif( isset( $arrValues['action_category_id'] ) ) $this->setActionCategoryId( $arrValues['action_category_id'] );
		if( isset( $arrValues['ignore_in_reporting'] ) && $boolDirectSet ) $this->set( 'm_boolIgnoreInReporting', trim( stripcslashes( $arrValues['ignore_in_reporting'] ) ) ); elseif( isset( $arrValues['ignore_in_reporting'] ) ) $this->setIgnoreInReporting( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['ignore_in_reporting'] ) : $arrValues['ignore_in_reporting'] );
		if( isset( $arrValues['prior_action_id'] ) && $boolDirectSet ) $this->set( 'm_intPriorActionId', trim( $arrValues['prior_action_id'] ) ); elseif( isset( $arrValues['prior_action_id'] ) ) $this->setPriorActionId( $arrValues['prior_action_id'] );
		if( isset( $arrValues['on_hold_reason_id'] ) && $boolDirectSet ) $this->set( 'm_intOnHoldReasonId', trim( $arrValues['on_hold_reason_id'] ) ); elseif( isset( $arrValues['on_hold_reason_id'] ) ) $this->setOnHoldReasonId( $arrValues['on_hold_reason_id'] );
		$this->m_boolInitialized = true;
	}

	public function setId( $intId ) {
		$this->set( 'm_intId', CStrings::strToIntDef( $intId, NULL, false ) );
	}

	public function getId() {
		return $this->m_intId;
	}

	public function sqlId() {
		return ( true == isset( $this->m_intId ) ) ? ( string ) $this->m_intId : 'NULL';
	}

	public function setActionTypeId( $intActionTypeId ) {
		$this->set( 'm_intActionTypeId', CStrings::strToIntDef( $intActionTypeId, NULL, false ) );
	}

	public function getActionTypeId() {
		return $this->m_intActionTypeId;
	}

	public function sqlActionTypeId() {
		return ( true == isset( $this->m_intActionTypeId ) ) ? ( string ) $this->m_intActionTypeId : 'NULL';
	}

	public function setActionResultId( $intActionResultId ) {
		$this->set( 'm_intActionResultId', CStrings::strToIntDef( $intActionResultId, NULL, false ) );
	}

	public function getActionResultId() {
		return $this->m_intActionResultId;
	}

	public function sqlActionResultId() {
		return ( true == isset( $this->m_intActionResultId ) ) ? ( string ) $this->m_intActionResultId : 'NULL';
	}

	public function setOldStatusTypeId( $intOldStatusTypeId ) {
		$this->set( 'm_intOldStatusTypeId', CStrings::strToIntDef( $intOldStatusTypeId, NULL, false ) );
	}

	public function getOldStatusTypeId() {
		return $this->m_intOldStatusTypeId;
	}

	public function sqlOldStatusTypeId() {
		return ( true == isset( $this->m_intOldStatusTypeId ) ) ? ( string ) $this->m_intOldStatusTypeId : 'NULL';
	}

	public function setNewStatusTypeId( $intNewStatusTypeId ) {
		$this->set( 'm_intNewStatusTypeId', CStrings::strToIntDef( $intNewStatusTypeId, NULL, false ) );
	}

	public function getNewStatusTypeId() {
		return $this->m_intNewStatusTypeId;
	}

	public function sqlNewStatusTypeId() {
		return ( true == isset( $this->m_intNewStatusTypeId ) ) ? ( string ) $this->m_intNewStatusTypeId : 'NULL';
	}

	public function setImplementationDelayTypeId( $intImplementationDelayTypeId ) {
		$this->set( 'm_intImplementationDelayTypeId', CStrings::strToIntDef( $intImplementationDelayTypeId, NULL, false ) );
	}

	public function getImplementationDelayTypeId() {
		return $this->m_intImplementationDelayTypeId;
	}

	public function sqlImplementationDelayTypeId() {
		return ( true == isset( $this->m_intImplementationDelayTypeId ) ) ? ( string ) $this->m_intImplementationDelayTypeId : 'NULL';
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->set( 'm_intPsLeadId', CStrings::strToIntDef( $intPsLeadId, NULL, false ) );
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function sqlPsLeadId() {
		return ( true == isset( $this->m_intPsLeadId ) ) ? ( string ) $this->m_intPsLeadId : 'NULL';
	}

	public function setContractId( $intContractId ) {
		$this->set( 'm_intContractId', CStrings::strToIntDef( $intContractId, NULL, false ) );
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function sqlContractId() {
		return ( true == isset( $this->m_intContractId ) ) ? ( string ) $this->m_intContractId : 'NULL';
	}

	public function setPsLeadEventId( $intPsLeadEventId ) {
		$this->set( 'm_intPsLeadEventId', CStrings::strToIntDef( $intPsLeadEventId, NULL, false ) );
	}

	public function getPsLeadEventId() {
		return $this->m_intPsLeadEventId;
	}

	public function sqlPsLeadEventId() {
		return ( true == isset( $this->m_intPsLeadEventId ) ) ? ( string ) $this->m_intPsLeadEventId : 'NULL';
	}

	public function setPersonId( $intPersonId ) {
		$this->set( 'm_intPersonId', CStrings::strToIntDef( $intPersonId, NULL, false ) );
	}

	public function getPersonId() {
		return $this->m_intPersonId;
	}

	public function sqlPersonId() {
		return ( true == isset( $this->m_intPersonId ) ) ? ( string ) $this->m_intPersonId : 'NULL';
	}

	public function setPsProductId( $intPsProductId ) {
		$this->set( 'm_intPsProductId', CStrings::strToIntDef( $intPsProductId, NULL, false ) );
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function sqlPsProductId() {
		return ( true == isset( $this->m_intPsProductId ) ) ? ( string ) $this->m_intPsProductId : 'NULL';
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->set( 'm_intEmployeeId', CStrings::strToIntDef( $intEmployeeId, NULL, false ) );
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function sqlEmployeeId() {
		return ( true == isset( $this->m_intEmployeeId ) ) ? ( string ) $this->m_intEmployeeId : 'NULL';
	}

	public function setAccountId( $intAccountId ) {
		$this->set( 'm_intAccountId', CStrings::strToIntDef( $intAccountId, NULL, false ) );
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function sqlAccountId() {
		return ( true == isset( $this->m_intAccountId ) ) ? ( string ) $this->m_intAccountId : 'NULL';
	}

	public function setPrimaryReference( $intPrimaryReference ) {
		$this->set( 'm_intPrimaryReference', CStrings::strToIntDef( $intPrimaryReference, NULL, false ) );
	}

	public function getPrimaryReference() {
		return $this->m_intPrimaryReference;
	}

	public function sqlPrimaryReference() {
		return ( true == isset( $this->m_intPrimaryReference ) ) ? ( string ) $this->m_intPrimaryReference : 'NULL';
	}

	public function setSecondaryReference( $intSecondaryReference ) {
		$this->set( 'm_intSecondaryReference', CStrings::strToIntDef( $intSecondaryReference, NULL, false ) );
	}

	public function getSecondaryReference() {
		return $this->m_intSecondaryReference;
	}

	public function sqlSecondaryReference() {
		return ( true == isset( $this->m_intSecondaryReference ) ) ? ( string ) $this->m_intSecondaryReference : 'NULL';
	}

	public function setActionDatetime( $strActionDatetime ) {
		$this->set( 'm_strActionDatetime', CStrings::strTrimDef( $strActionDatetime, -1, NULL, true ) );
	}

	public function getActionDatetime() {
		return $this->m_strActionDatetime;
	}

	public function sqlActionDatetime() {
		return ( true == isset( $this->m_strActionDatetime ) ) ? '\'' . $this->m_strActionDatetime . '\'' : 'NOW()';
	}

	public function setProjectedEndDate( $strProjectedEndDate ) {
		$this->set( 'm_strProjectedEndDate', CStrings::strTrimDef( $strProjectedEndDate, -1, NULL, true ) );
	}

	public function getProjectedEndDate() {
		return $this->m_strProjectedEndDate;
	}

	public function sqlProjectedEndDate() {
		return ( true == isset( $this->m_strProjectedEndDate ) ) ? '\'' . $this->m_strProjectedEndDate . '\'' : 'NULL';
	}

	public function setResultDatetime( $strResultDatetime ) {
		$this->set( 'm_strResultDatetime', CStrings::strTrimDef( $strResultDatetime, -1, NULL, true ) );
	}

	public function getResultDatetime() {
		return $this->m_strResultDatetime;
	}

	public function sqlResultDatetime() {
		return ( true == isset( $this->m_strResultDatetime ) ) ? '\'' . $this->m_strResultDatetime . '\'' : 'NULL';
	}

	public function setActionMinutes( $intActionMinutes ) {
		$this->set( 'm_intActionMinutes', CStrings::strToIntDef( $intActionMinutes, NULL, false ) );
	}

	public function getActionMinutes() {
		return $this->m_intActionMinutes;
	}

	public function sqlActionMinutes() {
		return ( true == isset( $this->m_intActionMinutes ) ) ? ( string ) $this->m_intActionMinutes : 'NULL';
	}

	public function setActionDescription( $strActionDescription ) {
		$this->set( 'm_strActionDescription', CStrings::strTrimDef( $strActionDescription, -1, NULL, true ) );
	}

	public function getActionDescription() {
		return $this->m_strActionDescription;
	}

	public function sqlActionDescription() {
		return ( true == isset( $this->m_strActionDescription ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strActionDescription ) : '\'' . addslashes( $this->m_strActionDescription ) . '\'' ) : 'NULL';
	}

	public function setNotes( $strNotes ) {
		$this->set( 'm_strNotes', CStrings::strTrimDef( $strNotes, -1, NULL, true ) );
	}

	public function getNotes() {
		return $this->m_strNotes;
	}

	public function sqlNotes() {
		return ( true == isset( $this->m_strNotes ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strNotes ) : '\'' . addslashes( $this->m_strNotes ) . '\'' ) : 'NULL';
	}

	public function setProjectTitle( $strProjectTitle ) {
		$this->set( 'm_strProjectTitle', CStrings::strTrimDef( $strProjectTitle, 200, NULL, true ) );
	}

	public function getProjectTitle() {
		return $this->m_strProjectTitle;
	}

	public function sqlProjectTitle() {
		return ( true == isset( $this->m_strProjectTitle ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strProjectTitle ) : '\'' . addslashes( $this->m_strProjectTitle ) . '\'' ) : 'NULL';
	}

	public function setCurrentProductAdoption( $fltCurrentProductAdoption ) {
		$this->set( 'm_fltCurrentProductAdoption', CStrings::strToFloatDef( $fltCurrentProductAdoption, NULL, false, 2 ) );
	}

	public function getCurrentProductAdoption() {
		return $this->m_fltCurrentProductAdoption;
	}

	public function sqlCurrentProductAdoption() {
		return ( true == isset( $this->m_fltCurrentProductAdoption ) ) ? ( string ) $this->m_fltCurrentProductAdoption : 'NULL';
	}

	public function setExpectedGains( $fltExpectedGains ) {
		$this->set( 'm_fltExpectedGains', CStrings::strToFloatDef( $fltExpectedGains, NULL, false, 2 ) );
	}

	public function getExpectedGains() {
		return $this->m_fltExpectedGains;
	}

	public function sqlExpectedGains() {
		return ( true == isset( $this->m_fltExpectedGains ) ) ? ( string ) $this->m_fltExpectedGains : 'NULL';
	}

	public function setActualGains( $fltActualGains ) {
		$this->set( 'm_fltActualGains', CStrings::strToFloatDef( $fltActualGains, NULL, false, 2 ) );
	}

	public function getActualGains() {
		return $this->m_fltActualGains;
	}

	public function sqlActualGains() {
		return ( true == isset( $this->m_fltActualGains ) ) ? ( string ) $this->m_fltActualGains : 'NULL';
	}

	public function setIpAddress( $strIpAddress ) {
		$this->set( 'm_strIpAddress', CStrings::strTrimDef( $strIpAddress, 50, NULL, true ) );
	}

	public function getIpAddress() {
		return $this->m_strIpAddress;
	}

	public function sqlIpAddress() {
		return ( true == isset( $this->m_strIpAddress ) ) ? ( $this->getDatabase() ? pg_escape_literal( $this->getDatabase()->getHandle(), $this->m_strIpAddress ) : '\'' . addslashes( $this->m_strIpAddress ) . '\'' ) : 'NULL';
	}

	public function setUpdatedBy( $intUpdatedBy ) {
		$this->set( 'm_intUpdatedBy', CStrings::strToIntDef( $intUpdatedBy, NULL, false ) );
	}

	public function getUpdatedBy() {
		return $this->m_intUpdatedBy;
	}

	public function sqlUpdatedBy() {
		return ( true == isset( $this->m_intUpdatedBy ) ) ? ( string ) $this->m_intUpdatedBy : 'NULL';
	}

	public function setUpdatedOn( $strUpdatedOn ) {
		$this->set( 'm_strUpdatedOn', CStrings::strTrimDef( $strUpdatedOn, -1, NULL, true ) );
	}

	public function getUpdatedOn() {
		return $this->m_strUpdatedOn;
	}

	public function sqlUpdatedOn() {
		return ( true == isset( $this->m_strUpdatedOn ) ) ? '\'' . $this->m_strUpdatedOn . '\'' : 'NOW()';
	}

	public function setCreatedBy( $intCreatedBy ) {
		$this->set( 'm_intCreatedBy', CStrings::strToIntDef( $intCreatedBy, NULL, false ) );
	}

	public function getCreatedBy() {
		return $this->m_intCreatedBy;
	}

	public function sqlCreatedBy() {
		return ( true == isset( $this->m_intCreatedBy ) ) ? ( string ) $this->m_intCreatedBy : 'NULL';
	}

	public function setCreatedOn( $strCreatedOn ) {
		$this->set( 'm_strCreatedOn', CStrings::strTrimDef( $strCreatedOn, -1, NULL, true ) );
	}

	public function getCreatedOn() {
		return $this->m_strCreatedOn;
	}

	public function sqlCreatedOn() {
		return ( true == isset( $this->m_strCreatedOn ) ) ? '\'' . $this->m_strCreatedOn . '\'' : 'NOW()';
	}

	public function setActionCategoryId( $intActionCategoryId ) {
		$this->set( 'm_intActionCategoryId', CStrings::strToIntDef( $intActionCategoryId, NULL, false ) );
	}

	public function getActionCategoryId() {
		return $this->m_intActionCategoryId;
	}

	public function sqlActionCategoryId() {
		return ( true == isset( $this->m_intActionCategoryId ) ) ? ( string ) $this->m_intActionCategoryId : 'NULL';
	}

	public function setIgnoreInReporting( $boolIgnoreInReporting ) {
		$this->set( 'm_boolIgnoreInReporting', CStrings::strToBool( $boolIgnoreInReporting ) );
	}

	public function getIgnoreInReporting() {
		return $this->m_boolIgnoreInReporting;
	}

	public function sqlIgnoreInReporting() {
		return ( true == isset( $this->m_boolIgnoreInReporting ) ) ? '\'' . ( true == ( bool ) $this->m_boolIgnoreInReporting ? 'true' : 'false' ) . '\'' : 'NULL';
	}

	public function setPriorActionId( $intPriorActionId ) {
		$this->set( 'm_intPriorActionId', CStrings::strToIntDef( $intPriorActionId, NULL, false ) );
	}

	public function getPriorActionId() {
		return $this->m_intPriorActionId;
	}

	public function sqlPriorActionId() {
		return ( true == isset( $this->m_intPriorActionId ) ) ? ( string ) $this->m_intPriorActionId : 'NULL';
	}

	public function setOnHoldReasonId( $intOnHoldReasonId ) {
		$this->set( 'm_intOnHoldReasonId', CStrings::strToIntDef( $intOnHoldReasonId, NULL, false ) );
	}

	public function getOnHoldReasonId() {
		return $this->m_intOnHoldReasonId;
	}

	public function sqlOnHoldReasonId() {
		return ( true == isset( $this->m_intOnHoldReasonId ) ) ? ( string ) $this->m_intOnHoldReasonId : 'NULL';
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strId = ( true == is_null( $this->getId() ) ) ?	'nextval( \'' . $this->getSequenceName() . '\' )' : $this->sqlId();

		$strSql = 'INSERT INTO
						' . static::TABLE_NAME . '( id, action_type_id, action_result_id, old_status_type_id, new_status_type_id, implementation_delay_type_id, ps_lead_id, contract_id, ps_lead_event_id, person_id, ps_product_id, employee_id, account_id, primary_reference, secondary_reference, action_datetime, projected_end_date, result_datetime, action_minutes, action_description, notes, project_title, current_product_adoption, expected_gains, actual_gains, ip_address, updated_by, updated_on, created_by, created_on, details, action_category_id, ignore_in_reporting, prior_action_id, on_hold_reason_id )
					VALUES ( ' .
						$strId . ', ' .
						$this->sqlActionTypeId() . ', ' .
						$this->sqlActionResultId() . ', ' .
						$this->sqlOldStatusTypeId() . ', ' .
						$this->sqlNewStatusTypeId() . ', ' .
						$this->sqlImplementationDelayTypeId() . ', ' .
						$this->sqlPsLeadId() . ', ' .
						$this->sqlContractId() . ', ' .
						$this->sqlPsLeadEventId() . ', ' .
						$this->sqlPersonId() . ', ' .
						$this->sqlPsProductId() . ', ' .
						$this->sqlEmployeeId() . ', ' .
						$this->sqlAccountId() . ', ' .
						$this->sqlPrimaryReference() . ', ' .
						$this->sqlSecondaryReference() . ', ' .
						$this->sqlActionDatetime() . ', ' .
						$this->sqlProjectedEndDate() . ', ' .
						$this->sqlResultDatetime() . ', ' .
						$this->sqlActionMinutes() . ', ' .
						$this->sqlActionDescription() . ', ' .
						$this->sqlNotes() . ', ' .
						$this->sqlProjectTitle() . ', ' .
						$this->sqlCurrentProductAdoption() . ', ' .
						$this->sqlExpectedGains() . ', ' .
						$this->sqlActualGains() . ', ' .
						$this->sqlIpAddress() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlUpdatedOn() . ', ' .
						( int ) $intCurrentUserId . ', ' .
						$this->sqlCreatedOn() . ', ' .
						$this->sqlDetails() . ', ' .
						$this->sqlActionCategoryId() . ', ' .
						$this->sqlIgnoreInReporting() . ', ' .
						$this->sqlPriorActionId() . ', ' .
						$this->sqlOnHoldReasonId() . ' ) ' . ' RETURNING id;';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		if( false == $this->getAllowDifferentialUpdate() ) {
			$boolUpdate = true;
		} else {
			$boolUpdate = false;
		}

		$strSql = 'UPDATE
						' . static::TABLE_NAME . '
					SET ';
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_type_id = ' . $this->sqlActionTypeId(). ',' ; } elseif( true == array_key_exists( 'ActionTypeId', $this->getChangedColumns() ) ) { $strSql .= ' action_type_id = ' . $this->sqlActionTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_result_id = ' . $this->sqlActionResultId(). ',' ; } elseif( true == array_key_exists( 'ActionResultId', $this->getChangedColumns() ) ) { $strSql .= ' action_result_id = ' . $this->sqlActionResultId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' old_status_type_id = ' . $this->sqlOldStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'OldStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' old_status_type_id = ' . $this->sqlOldStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' new_status_type_id = ' . $this->sqlNewStatusTypeId(). ',' ; } elseif( true == array_key_exists( 'NewStatusTypeId', $this->getChangedColumns() ) ) { $strSql .= ' new_status_type_id = ' . $this->sqlNewStatusTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' implementation_delay_type_id = ' . $this->sqlImplementationDelayTypeId(). ',' ; } elseif( true == array_key_exists( 'ImplementationDelayTypeId', $this->getChangedColumns() ) ) { $strSql .= ' implementation_delay_type_id = ' . $this->sqlImplementationDelayTypeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId(). ',' ; } elseif( true == array_key_exists( 'PsLeadId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_id = ' . $this->sqlPsLeadId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' contract_id = ' . $this->sqlContractId(). ',' ; } elseif( true == array_key_exists( 'ContractId', $this->getChangedColumns() ) ) { $strSql .= ' contract_id = ' . $this->sqlContractId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_lead_event_id = ' . $this->sqlPsLeadEventId(). ',' ; } elseif( true == array_key_exists( 'PsLeadEventId', $this->getChangedColumns() ) ) { $strSql .= ' ps_lead_event_id = ' . $this->sqlPsLeadEventId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' person_id = ' . $this->sqlPersonId(). ',' ; } elseif( true == array_key_exists( 'PersonId', $this->getChangedColumns() ) ) { $strSql .= ' person_id = ' . $this->sqlPersonId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId(). ',' ; } elseif( true == array_key_exists( 'PsProductId', $this->getChangedColumns() ) ) { $strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId(). ',' ; } elseif( true == array_key_exists( 'EmployeeId', $this->getChangedColumns() ) ) { $strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' account_id = ' . $this->sqlAccountId(). ',' ; } elseif( true == array_key_exists( 'AccountId', $this->getChangedColumns() ) ) { $strSql .= ' account_id = ' . $this->sqlAccountId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' primary_reference = ' . $this->sqlPrimaryReference(). ',' ; } elseif( true == array_key_exists( 'PrimaryReference', $this->getChangedColumns() ) ) { $strSql .= ' primary_reference = ' . $this->sqlPrimaryReference() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' secondary_reference = ' . $this->sqlSecondaryReference(). ',' ; } elseif( true == array_key_exists( 'SecondaryReference', $this->getChangedColumns() ) ) { $strSql .= ' secondary_reference = ' . $this->sqlSecondaryReference() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_datetime = ' . $this->sqlActionDatetime(). ',' ; } elseif( true == array_key_exists( 'ActionDatetime', $this->getChangedColumns() ) ) { $strSql .= ' action_datetime = ' . $this->sqlActionDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' projected_end_date = ' . $this->sqlProjectedEndDate(). ',' ; } elseif( true == array_key_exists( 'ProjectedEndDate', $this->getChangedColumns() ) ) { $strSql .= ' projected_end_date = ' . $this->sqlProjectedEndDate() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' result_datetime = ' . $this->sqlResultDatetime(). ',' ; } elseif( true == array_key_exists( 'ResultDatetime', $this->getChangedColumns() ) ) { $strSql .= ' result_datetime = ' . $this->sqlResultDatetime() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_minutes = ' . $this->sqlActionMinutes(). ',' ; } elseif( true == array_key_exists( 'ActionMinutes', $this->getChangedColumns() ) ) { $strSql .= ' action_minutes = ' . $this->sqlActionMinutes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_description = ' . $this->sqlActionDescription(). ',' ; } elseif( true == array_key_exists( 'ActionDescription', $this->getChangedColumns() ) ) { $strSql .= ' action_description = ' . $this->sqlActionDescription() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' notes = ' . $this->sqlNotes(). ',' ; } elseif( true == array_key_exists( 'Notes', $this->getChangedColumns() ) ) { $strSql .= ' notes = ' . $this->sqlNotes() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' project_title = ' . $this->sqlProjectTitle(). ',' ; } elseif( true == array_key_exists( 'ProjectTitle', $this->getChangedColumns() ) ) { $strSql .= ' project_title = ' . $this->sqlProjectTitle() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' current_product_adoption = ' . $this->sqlCurrentProductAdoption(). ',' ; } elseif( true == array_key_exists( 'CurrentProductAdoption', $this->getChangedColumns() ) ) { $strSql .= ' current_product_adoption = ' . $this->sqlCurrentProductAdoption() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' expected_gains = ' . $this->sqlExpectedGains(). ',' ; } elseif( true == array_key_exists( 'ExpectedGains', $this->getChangedColumns() ) ) { $strSql .= ' expected_gains = ' . $this->sqlExpectedGains() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' actual_gains = ' . $this->sqlActualGains(). ',' ; } elseif( true == array_key_exists( 'ActualGains', $this->getChangedColumns() ) ) { $strSql .= ' actual_gains = ' . $this->sqlActualGains() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress(). ',' ; } elseif( true == array_key_exists( 'IpAddress', $this->getChangedColumns() ) ) { $strSql .= ' ip_address = ' . $this->sqlIpAddress() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' details = ' . $this->sqlDetails(). ',' ; } elseif( true == array_key_exists( 'Details', $this->getChangedColumns() ) ) { $strSql .= ' details = ' . $this->sqlDetails() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' action_category_id = ' . $this->sqlActionCategoryId(). ',' ; } elseif( true == array_key_exists( 'ActionCategoryId', $this->getChangedColumns() ) ) { $strSql .= ' action_category_id = ' . $this->sqlActionCategoryId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' ignore_in_reporting = ' . $this->sqlIgnoreInReporting(). ',' ; } elseif( true == array_key_exists( 'IgnoreInReporting', $this->getChangedColumns() ) ) { $strSql .= ' ignore_in_reporting = ' . $this->sqlIgnoreInReporting() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' prior_action_id = ' . $this->sqlPriorActionId(). ',' ; } elseif( true == array_key_exists( 'PriorActionId', $this->getChangedColumns() ) ) { $strSql .= ' prior_action_id = ' . $this->sqlPriorActionId() . ','; $boolUpdate = true; }
						if( false == $this->getAllowDifferentialUpdate() ) { $strSql .= ' on_hold_reason_id = ' . $this->sqlOnHoldReasonId(). ',' ; } elseif( true == array_key_exists( 'OnHoldReasonId', $this->getChangedColumns() ) ) { $strSql .= ' on_hold_reason_id = ' . $this->sqlOnHoldReasonId() . ','; $boolUpdate = true; }
						$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
						$strSql .= ' updated_on = \'NOW()\' ';
	
		$strSql .= ' WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->resetChangedColumns();
						return true;
					}
				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase ); 

		$strSql = 'DELETE FROM ' . static::TABLE_NAME . ' WHERE id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

	public function toArray() {
		return array(
			'id' => $this->getId(),
			'action_type_id' => $this->getActionTypeId(),
			'action_result_id' => $this->getActionResultId(),
			'old_status_type_id' => $this->getOldStatusTypeId(),
			'new_status_type_id' => $this->getNewStatusTypeId(),
			'implementation_delay_type_id' => $this->getImplementationDelayTypeId(),
			'ps_lead_id' => $this->getPsLeadId(),
			'contract_id' => $this->getContractId(),
			'ps_lead_event_id' => $this->getPsLeadEventId(),
			'person_id' => $this->getPersonId(),
			'ps_product_id' => $this->getPsProductId(),
			'employee_id' => $this->getEmployeeId(),
			'account_id' => $this->getAccountId(),
			'primary_reference' => $this->getPrimaryReference(),
			'secondary_reference' => $this->getSecondaryReference(),
			'action_datetime' => $this->getActionDatetime(),
			'projected_end_date' => $this->getProjectedEndDate(),
			'result_datetime' => $this->getResultDatetime(),
			'action_minutes' => $this->getActionMinutes(),
			'action_description' => $this->getActionDescription(),
			'notes' => $this->getNotes(),
			'project_title' => $this->getProjectTitle(),
			'current_product_adoption' => $this->getCurrentProductAdoption(),
			'expected_gains' => $this->getExpectedGains(),
			'actual_gains' => $this->getActualGains(),
			'ip_address' => $this->getIpAddress(),
			'updated_by' => $this->getUpdatedBy(),
			'updated_on' => $this->getUpdatedOn(),
			'created_by' => $this->getCreatedBy(),
			'created_on' => $this->getCreatedOn(),
			'details' => $this->getDetails(),
			'action_category_id' => $this->getActionCategoryId(),
			'ignore_in_reporting' => $this->getIgnoreInReporting(),
			'prior_action_id' => $this->getPriorActionId(),
			'on_hold_reason_id' => $this->getOnHoldReasonId()
		);
	}

}
?>