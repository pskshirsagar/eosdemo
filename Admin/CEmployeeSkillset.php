<?php

class CEmployeeSkillset extends CBaseEmployeeSkillset {

	public function valSkillsetsId() {
		$boolIsValid = true;
		if( true == is_null( $this->getSkillsetsId() ) || 0 == ( $this->getSkillsetsId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'skill_id', 'Skill is required.' ) );
		}
		return $boolIsValid;
	}

	public function valRating() {
		$boolIsValid = true;
		if( true == is_null( $this->getRating() ) || 0 == ( $this->getRating() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'rating', 'Skill Rating is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSkillsetsId();
				$boolIsValid &= $this->valRating();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}
}
?>