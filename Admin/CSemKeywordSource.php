<?php

class CSemKeywordSource extends CBaseSemKeywordSource {

	protected $m_intSemAccountId;
	protected $m_intIsSystem;
	protected $m_intDeletedBy;

	protected $m_strKeywords;
	protected $m_strDeletedOn;
	protected $m_strSemAdGroupRemotePrimaryKey;

	// These two variables are used in reconciliation script.
	protected $m_fltCostPerClick;
	protected $m_fltCost;

	protected $m_intAssociatedPropertiesCount;

	/**
	 * Get Functions
	 */

	public function getSemAccountId() {
		return $this->m_intSemAccountId;
	}

	public function getKeywords() {
		return $this->m_strKeywords;
	}

	public function getSemAdGroupRemotePrimaryKey() {
		return $this->m_strSemAdGroupRemotePrimaryKey;
	}

	public function getAssociatedPropertiesCount() {
		return $this->m_intAssociatedPropertiesCount;
	}

	public function getCostPerClick() {
		return $this->m_fltCostPerClick;
	}

 	public function getCost() {
		return $this->m_fltCost;
 	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	/**
	 * Set Functions
	 */

	public function setSemAccountId( $intSemAccountId ) {
		$this->m_intSemAccountId = $intSemAccountId;
	}

	public function setKeywords( $strKeywords ) {
		$this->m_strKeywords = $strKeywords;
	}

	public function setSemAdGroupRemotePrimaryKey( $strSemAdGroupRemotePrimaryKey ) {
		$this->m_strSemAdGroupRemotePrimaryKey = $strSemAdGroupRemotePrimaryKey;
	}

	public function setAssociatedPropertiesCount( $intAssociatedPropertiesCount ) {
		$this->m_intAssociatedPropertiesCount = $intAssociatedPropertiesCount;
	}

	public function setCostPerClick( $fltCostPerClick ) {
		$this->m_fltCostPerClick = CStrings::strToFloatDef( $fltCostPerClick, NULL, false, 2 );
	}

	public function setCost( $fltCost ) {
		$this->m_fltCost = CStrings::strToFloatDef( $fltCost, NULL, false, 2 );
	}

	public function setIsSystem( $intIsSystem ) {
		$this->m_intIsSystem = $intIsSystem;
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->m_intDeletedBy = $intDeletedBy;
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->m_strDeletedOn = $strDeletedOn;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
 		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
 		if( true == isset( $arrValues['keywords'] ) ) $this->setKeywords( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['keywords'] ) : $arrValues['keywords'] );
		if( true == isset( $arrValues['sem_ad_group_remote_primary_key'] ) ) $this->setSemAdGroupRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sem_ad_group_remote_primary_key'] ) : $arrValues['sem_ad_group_remote_primary_key'] );
		if( true == isset( $arrValues['associated_properties_count'] ) ) $this->setAssociatedPropertiesCount( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['associated_properties_count'] ) : $arrValues['associated_properties_count'] );
		if( true == isset( $arrValues['sem_account_id'] ) ) $this->setSemAccountId( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sem_account_id'] ) : $arrValues['sem_account_id'] );
		if( true == isset( $arrValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['is_system'] ) : $arrValues['is_system'] );
 		if( true == isset( $arrValues['deleted_by'] ) ) $this->setDeletedBy( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['deleted_by'] ) : $arrValues['deleted_by'] );
		if( true == isset( $arrValues['deleted_on'] ) ) $this->setDeletedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['deleted_on'] ) : $arrValues['deleted_on'] );

		return;
	}

	/**
	 * Validate Functions
	 */

	public function valSemSourceId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemSourceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_source_id', 'Sem source ID is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSemAdGroupId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemAdGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_ad_group_id', 'Sem ad group ID is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSemKeywordId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemKeywordId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_keyword_id', 'Sem keyword ID is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSemStatusTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_status_type_id', 'Sem status type ID id required.' ) );
		}

		return $boolIsValid;
	}

	public function valAvgCostPerClick() {
		$boolIsValid = true;

		if( true == is_null( $this->getAvgCostPerClick() ) || false == is_numeric( $this->getAvgCostPerClick() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'avg_cost_per_click', 'Average cost per click is required and should be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valMaxCostPerClick() {
		$boolIsValid = true;

		if( true == is_null( $this->getMaxCostPerClick() ) || false == is_numeric( $this->getMaxCostPerClick() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_cost_per_click', 'Max cost per click is required and should be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;

		if( true == is_null( $this->getRemotePrimaryKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'remote_primary_key', 'Remote primary key is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valAvgCostPerClick();
				$boolIsValid &= $this->valMaxCostPerClick();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchSemKeyword( $objAdminDatabase ) {
		return CSemKeywords::fetchSemKeywordById( $this->getSemKeywordId(), $objAdminDatabase );
	}

}
?>