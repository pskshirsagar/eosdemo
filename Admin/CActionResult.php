<?php

class CActionResult extends CBaseActionResult {

	// Results Of Calls
	const INTERESTED				= 3;
	const CALL_LATER				= 4;
	const NOT_INTERESTED			= 5;
	const LEFT_VOICEMAIL			= 9;
	const NO_ANSWER					= 10;

	// Results Of System Emails
	const EMAIL_OPENED				= 11;
	const EMAIL_CLICKED				= 12;
	const EMAIL_RETURNED			= 13;
	const NO_RESPONSE				= 14;

	// Results of Demos
	const COMPLETED					= 16;
	const MISSED_BY_PS				= 17;
	const MISSED_BY_LEAD			= 18;

	const EMPLOYEE_APPLICATION		= 108;
	const REVIEW					= 109;
	const LIKE						= 110;
	const ATTENDANCE_REMARK			= 111;
	const TERMINATION				= 122;

	// Types of Tags
	const LEAD_TAG					= 101;
	const PERSON_TAG				= 102;
	const TASK_TAG					= 103;
	const EMPLOYEE_DOCUMENT_TAG		= 104;
	const HELPDESK_TAG				= 153;

	const TRAINER					= 105;
	const INTERESTED_TRAINER		= 106;
	const EXTERNAL_TRAINER			= 107;

	// Types of Reviewed
	const EDIT_TASK					= 112;
	const VIEW_TASK					= 113;

	// Types of Meeting
	const REMOTE					= 115;
	const OFFICE					= 116;
	const MEAL						= 117;
	const EVENT						= 118;

	// Contract Stipulations
	const CONTRACT_STIPULATIONS					= 123;

	// Types of Renewals Statues
	const SEND_INITIAL_LETTER					= 124;
	const SEND_INITIAL_LETTER_COMPLETE			= 125;
	const WAIVE_INITIAL_LETTER					= 126;
	const APPROVE_FOUR_PERCENTAGE				= 127;
	const WAIVE_FOUR_PERCENTAGE					= 128;
	const OLD_APPROVE_FOUR_PERCENTAGE			= 129;
	const OLD_WAIVE_FOUR_PERCENTAGE				= 130;
	const GENERATE_FINAL_LETTER					= 131;
	const EMAIL_LETTER_COMPLETE					= 132;
	const UPDATE_CHARGES						= 133;
	const MARK_AS_COMPLETED						= 134;

	const UNSATISFIED_WITH_IMPLEMENTATION		= 135;
	const UNSATISFIED_WITH_PRODUCTS_SERVICES	= 136;
	const RECENTLY_ADDED_PRODUCTS				= 137;
	const NEW_SALE_IN_PROGRESS					= 138;
	const NEW_SALE_ADDED_PRODUCTS				= 139;
	const NEW_SALE_ADDED_PROPERTIES				= 140;
	const IN_PILOT								= 141;
	const RECENTLY_ADDED_PROPERTIES				= 142;
	const SHOPPING_COMPETITORS					= 143;
	const TERMINATING							= 144;
	const BILLING_ISSUES						= 145;
	const OTHER									= 146;
	const DELINQUENCY_NOTE						= 149;
	const DELINQUENCY_REASON					= 152;
	const DELINQUENCY_NOTE_WITH_LEVEL			= 158;
	const DIGITAL_MARKETING						= 159;
	const CLIENT_TERMINATION					= 151;

	// Mass Email
	const MERGEFIELDS_WARNING					= 147;
	const PRODUCT_MANAGEMENT					= 160;

	const MIGRATION_WORKBOOK_ACCOUNTING_TAGGING = 161;
	const MIGRATION_WORKBOOK_CONSULTING_TAGGING = 162;
	// CA REPORT
	const REPORT								= 157;

	public static $c_arrintSuccessfulCallTypeIds = [
		self::INTERESTED,
		self::CALL_LATER,
		self::NOT_INTERESTED
	];

	public static $c_arrintTrainingResults = [
		self::TRAINER,
		self::INTERESTED_TRAINER,
		self::EXTERNAL_TRAINER
	];

	public static $c_arrintNotEditableActionResultIds = [
		self::SEND_INITIAL_LETTER,
		self::EMAIL_LETTER_COMPLETE,
		self::GENERATE_FINAL_LETTER,
		self::UPDATE_CHARGES,
		self::APPROVE_FOUR_PERCENTAGE,
		self::SEND_INITIAL_LETTER_COMPLETE,
		self::OLD_APPROVE_FOUR_PERCENTAGE
	];

	public static $c_arrintActivityFilterActionResultIds = [
		self::DIGITAL_MARKETING,
		self::CLIENT_TERMINATION
	];

	// TODO: remove it once we release task id 2498765.
	public static $c_arrintNewActivityFilterActionResultIds = [
		self::DIGITAL_MARKETING,
		self::CLIENT_TERMINATION
	];

	public static $c_arrintLikelihoodSettingsActionResultIds = [
		self::INTERESTED,
		self::CALL_LATER,
		self::NOT_INTERESTED,
		self::COMPLETED,
		self::MISSED_BY_PS,
		self::REMOTE,
		self::OFFICE,
		self::MEAL,
		self::EVENT
	];

	public static $c_arrmixClientRenewalActionResults = [
		self::SEND_INITIAL_LETTER_COMPLETE,
		self::WAIVE_INITIAL_LETTER,
		self::APPROVE_FOUR_PERCENTAGE,
		self::WAIVE_FOUR_PERCENTAGE,
		self::GENERATE_FINAL_LETTER,
		self::EMAIL_LETTER_COMPLETE,
		self::UPDATE_CHARGES
	];

	public static $c_arrintDefaultSelectedActionResultTypeIds = [
		self::CLIENT_TERMINATION
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>