<?php

class CSemAdSource extends CBaseSemAdSource {

	protected $m_strTitle;
	protected $m_strDescriptionLine1;
	protected $m_strDescriptionLine2;
	protected $m_strDisplayUrl;
	protected $m_strDestinationUrl;
	protected $m_strDeletedOn;
	protected $m_strSemAdUpdatedOn;
	protected $m_strSemAdGroupRemotePrimaryKey;

	protected $m_intIsSystem;
	protected $m_intDeletedBy;
	protected $m_intAssociatedPropertiesCount;

	/**
	 * Get Functions
	 */

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getDescriptionLine1() {
		return $this->m_strDescriptionLine1;
	}

	public function getDescriptionLine2() {
		return $this->m_strDescriptionLine2;
	}

	public function getDisplayUrl() {
		return $this->m_strDisplayUrl;
	}

	public function getDestinationUrl() {
		return $this->m_strDestinationUrl;
	}

	public function getSemAdGroupRemotePrimaryKey() {
		return $this->m_strSemAdGroupRemotePrimaryKey;
	}

	public function getAssociatedPropertiesCount() {
		return $this->m_intAssociatedPropertiesCount;
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function getSemAdUpdatedOn() {
		return $this->m_strSemAdUpdatedOn;
	}

	/**
	 * Set Functions
	 */

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setDescriptionLine1( $strDescriptionLine1 ) {
		$this->m_strDescriptionLine1 = $strDescriptionLine1;
	}

	public function setDescriptionLine2( $strDescriptionLine2 ) {
		$this->m_strDescriptionLine2 = $strDescriptionLine2;
	}

	public function setDisplayUrl( $strDisplayUrl ) {
		$this->m_strDisplayUrl = $strDisplayUrl;
	}

	public function setDestinationUrl( $strDestinationUrl ) {
		$this->m_strDestinationUrl = $strDestinationUrl;
	}

	public function setSemAdGroupRemotePrimaryKey( $strSemAdGroupRemotePrimaryKey ) {
		$this->m_strSemAdGroupRemotePrimaryKey = $strSemAdGroupRemotePrimaryKey;
	}

	public function setAssociatedPropertiesCount( $intAssociatedPropertiesCount ) {
		$this->m_intAssociatedPropertiesCount = $intAssociatedPropertiesCount;
	}

	public function setIsSystem( $intIsSystem ) {
		$this->m_intIsSystem = $intIsSystem;
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->m_intDeletedBy = $intDeletedBy;
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->m_strDeletedOn = $strDeletedOn;
	}

	public function setSemAdUpdatedOn( $strSemAdUpdatedOn ) {
		$this->m_strSemAdUpdatedOn = $strSemAdUpdatedOn;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['title'] ) ) $this->setTitle( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['title'] ) : $arrmixValues['title'] );
		if( true == isset( $arrmixValues['description_line1'] ) ) $this->setDescriptionLine1( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['description_line1'] ) : $arrmixValues['description_line1'] );
		if( true == isset( $arrmixValues['description_line2'] ) ) $this->setDescriptionLine2( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['description_line2'] ) : $arrmixValues['description_line2'] );
		if( true == isset( $arrmixValues['display_url'] ) ) $this->setDisplayUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['display_url'] ) : $arrmixValues['display_url'] );
		if( true == isset( $arrmixValues['destination_url'] ) ) $this->setDestinationUrl( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['destination_url'] ) : $arrmixValues['destination_url'] );
		if( true == isset( $arrmixValues['sem_ad_group_remote_primary_key'] ) ) $this->setSemAdGroupRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['sem_ad_group_remote_primary_key'] ) : $arrmixValues['sem_ad_group_remote_primary_key'] );
		if( true == isset( $arrmixValues['associated_properties_count'] ) ) $this->setAssociatedPropertiesCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['associated_properties_count'] ) : $arrmixValues['associated_properties_count'] );
		if( true == isset( $arrmixValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_system'] ) : $arrmixValues['is_system'] );
		if( true == isset( $arrmixValues['deleted_by'] ) ) $this->setDeletedBy( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['deleted_by'] ) : $arrmixValues['deleted_by'] );
		if( true == isset( $arrmixValues['deleted_on'] ) ) $this->setDeletedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['deleted_on'] ) : $arrmixValues['deleted_on'] );
		if( true == isset( $arrmixValues['sem_ad_updated_on'] ) ) $this->setSemAdUpdatedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['sem_ad_updated_on'] ) : $arrmixValues['sem_ad_updated_on'] );

		return;
	}

	/**
	 * Set Functions
	 */

	public function valSemSourceId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemSourceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_source_id', 'Sem source ID is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSemAdGroupId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemAdGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_ad_group_id', 'Sem ad group ID is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSemAdId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemAdId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_ad_id', 'Sem ad ID is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSemAdStatusTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemAdStatusTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_ad_status_type_id', 'Sem ad status type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;

		if( true == is_null( $this->getRemotePrimaryKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'remote_primary_key', 'Remote primary key is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function buildDestinationUrl( $objDatabase ) {

		if( 0 == substr_count( $this->getDestinationUrl(), '&s=' ) ) {
			$this->setDestinationUrl( $this->getDestinationUrl() . '&s=' . $this->getSemSourceId() );
		}

		if( 0 == substr_count( $this->getDestinationUrl(), '&adid=' ) && false == $this->getIsSystem() ) {
			$this->setDestinationUrl( 'http://www.vacancy.com/?module=properties&agid=' . $this->getSemAdGroupId() . '&kw={keyword:nil}&s=' . $this->getSemSourceId() . '&adid=' . $this->getSemAdId() );
		}

		return $this->getDestinationUrl();
	}

	/**
	 * Fetch Functions
	 */

	public function fetchSemAd( $objDatabase ) {
		return CSemAds::fetchSemAdById( $this->getSemAdId(), $objDatabase );
	}
}
?>