<?php

class CTemplateSlotType extends CBaseTemplateSlotType {

	const PROSPECT_PORTAL = 1;
	const RESIDENT_PORTAL = 2;
	const PROSPECT_PORTAL_MOBILE = 3;
	const SITE_TABLET = 4;
	const LOBBY_DISPLAY = 5;
	const PROSPECT_PORTAL_WIDGET = 6;
}
?>