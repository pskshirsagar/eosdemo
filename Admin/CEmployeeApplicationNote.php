<?php

class CEmployeeApplicationNote extends CBaseEmployeeApplicationNote {

	protected static $c_arrmixActionTypes = [
		CActionType::NOTE	=> 'Note',
		CActionType::CALL	=> 'Call',
		CActionType::EMAIL	=> 'Email'
	];
	/**
	 * Other Functions
	 */

	public function createEmployeeApplicationNote( $intEmployeeApplicationId, $strNote, $intUserId ) {
		$this->setEmployeeApplicationId( $intEmployeeApplicationId );
		$this->setNoteTypeId( CNoteType::EMPLOYEE_APPLICATION );
		$this->setNote( $strNote );
		$this->setAddedBy( $intUserId );
	}

	public function insertEmployeeApplicationNote( $intCurrentUserId, $intEmployeeApplicationId, $intNoteTypeId, $strNote, $objDatabase ) {

		$this->setEmployeeApplicationId( $intEmployeeApplicationId );
		$this->setNoteTypeId( $intNoteTypeId );
		$this->setNote( $strNote );
		$this->setAddedBy( $intCurrentUserId );

		if( false == $this->insert( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}
		return true;
	}

	public function valNote() {

		$boolIsValid = true;

		if( false == isset( $this->m_strNote ) || 0 > strlen( $this->m_strNote ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Note is required.' ) );

		} elseif( ( true == isset( $this->m_intNoteTypeId ) ) && ( true == is_numeric( $this->m_intNoteTypeId ) ) && ( CNoteType::EMPLOYEE_APPLICATION == $this->m_intNoteTypeId ) && ( 0 < strlen( $this->m_strNote ) ) && ( 0 == \Psi\CStringService::singleton()->strcasecmp( 'The employee application status has been changed to joined.', $this->m_strNote ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'This note is a reserved note. Please add another note.' ) );
		}

		return $boolIsValid;
	}

	public function valNoteTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intNoteTypeId ) || false == is_numeric( $this->m_intNoteTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note_type_id', 'Note type is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valNote();
				$boolIsValid &= $this->valNoteTypeId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function formatNoteDescription( $arrmixEmployeeApplicationStep ) {
		$strHtml = '<h4>' . $arrmixEmployeeApplicationStep['name'] . ' Notes</h4><br/>' . $this->getNote();
		$strHtml = $this->closeHtmlTags( $strHtml );
		$this->setNote( $strHtml );
	}

	public function closeHtmlTags( $strHtml ) {

		// Remove improper tag (Without End of open tag i.e '<div' ) from the end of string.
		if( '>' != \Psi\CStringService::singleton()->substr( $strHtml, -1 ) ) {
			$intPosOpenTag = \Psi\CStringService::singleton()->strrpos( $strHtml, '<', -1 );
			$intPosCloseTag = \Psi\CStringService::singleton()->strrpos( $strHtml, '>', -1 );
			if( false == $intPosOpenTag ) {
				return $strHtml;
			} elseif( $intPosOpenTag > $intPosCloseTag ) {
				$strHtml = \Psi\CStringService::singleton()->substr_replace( $strHtml, '', $intPosOpenTag );
			}
		}

		// Get number of occurrences of opened tags into an array.
		preg_match_all( '#<([a-z]+)( .*)?(?!/)>#iU', $strHtml, $arrstrResult );
		$arrstrOpenTags = array_count_values( $arrstrResult[1] );

		// Get number of occurrences of closed tags into an array.
		preg_match_all( '#</([a-z]+)>#iU', $strHtml, $arrstrResult );
		$arrstrCloseTags = array_count_values( $arrstrResult[1] );

		$arrstrFreeTags = [ 'br', 'meta' ];
		// Get the remainder of open tag occurrences that needs to be closed.
		foreach( $arrstrOpenTags as $strTagKey => $intOccurrence ) {
			if( true != in_array( $strTagKey, $arrstrFreeTags ) ) {
				if( true == isset( $arrstrCloseTags[$strTagKey] ) && $arrstrOpenTags[$strTagKey] != $arrstrCloseTags[$strTagKey] && $arrstrOpenTags[$strTagKey] > $arrstrCloseTags[$strTagKey] ) {
					$arrstrOpenTags[$strTagKey] -= $arrstrCloseTags[$strTagKey];
					// Closing open tags.
					for( $intStart = 0; $intStart < $arrstrOpenTags[$strTagKey]; $intStart++ ) {
						$strHtml .= '</' . $strTagKey . '>';
					}
				} elseif( true == isset( $arrstrCloseTags[$strTagKey] ) && ( $arrstrOpenTags[$strTagKey] == $arrstrCloseTags[$strTagKey] || $arrstrOpenTags[$strTagKey] < $arrstrCloseTags[$strTagKey] ) ) {
					unset( $arrstrOpenTags[$strTagKey] );
				}
			} else {
				unset( $arrstrOpenTags[$strTagKey] );
			}
		}

		return $strHtml;
	}

	public function formatEmployeeApplicationNote( $intEmployeeApplicationId, $objUser, $objAdminDatabse ) {

		$arrmixEmployeeApplicationFinalNotes = [];
		$arrstrStatusPatterns       = [ 'Applied', 'CCAT', 'MRAB', 'Technical', 'Tech Test', '1st Interview', '2nd Interview', 'HR Interview', 'Offer', 'References', 'No Show' ];
		$arrstrActionUpdatePatterns = [ 'Selected' => '', 'On Hold' => '', 'Rejected' => '', 'Scheduled' => '', 'Rescheduled' => '', 'Mail sent' => '', 'Associated' => 'PR', 'Extended' => '', 'Details Updated' => '', 'Accepted' => '', 'test generated' => '', 'Coding test generated' => '', 'Source Updated' => '', 'Call note added' => '', 'Note added' => '', 'Email note added' => '', 'Application updated' => '', 'test selected' => '', 'test rejected' => '', 'test score updated' => '', 'test sent' => '', 'status changed' => '', 'is Updated' => 'PR', 'Reopened' => 'PR' ];
		$arrmixEmployeeApplicationNotes = ( array ) \Psi\Eos\Admin\CEmployeeApplicationSteps::createService()->fetchEmployeeApplicationNotesByEmployeeApplicationId( $intEmployeeApplicationId, $objAdminDatabse );
		$arrintHrEmployees				= rekeyArray( 'id', ( array ) CEmployees::fetchEmployeesByDepartmentIds( [ CDepartment::HR, CDepartment::RECRUITING ], $objAdminDatabse ) );

		if( true == $objUser->getIsSuperUser() ) {
			$arrintHrEmployees[$objUser->getEmployeeId()] = $objUser->getEmployeeId();
		}

		if( true == valArr( $arrmixEmployeeApplicationNotes ) ) {
			foreach( $arrmixEmployeeApplicationNotes as $intKey => $arrmixEmployeeApplicationNote ) {

				if( true == valStr( $arrmixEmployeeApplicationNote['action_description'] ) ) {
					$arrmixEmployeeApplicationNote['action_description']	= ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $arrmixEmployeeApplicationNote['action_description'], CONFIG_SODIUM_KEY_EMPLOYEE_NOTE, [ 'legacy_secret_key' => CONFIG_KEY_EMPLOYEE_NOTE ] );
				}

				if( true == valStr( $arrmixEmployeeApplicationNote['notes'] ) ) {
					$arrmixEmployeeApplicationNote['notes'] = ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $arrmixEmployeeApplicationNote['notes'], CONFIG_SODIUM_KEY_EMPLOYEE_NOTE, [ 'legacy_secret_key' => CONFIG_KEY_EMPLOYEE_NOTE ] );
				}

				$arrmixEmployeeApplicationNote['note']					= $arrmixEmployeeApplicationNote['action_description'] . '<br/>' . $arrmixEmployeeApplicationNote['notes'];
				$boolNeedToSetStatusAsPr								= false;

				if( true == preg_match( '/Source Updated:/', $arrmixEmployeeApplicationNote['action_description'] ) && true == is_null( getArrayElementByKey( $objUser->getEmployeeId(), $arrintHrEmployees ) ) ) {
					continue;
				}

				if( false !== \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], 'at Step Status' ) || false !== \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], 'Reallocation' ) || false !== \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], 'Reopened' ) ) {
					$boolNeedToSetStatusAsPr = true;
				}

				foreach( $arrstrActionUpdatePatterns as $strActionUpdatePatternKey => $strActionUpdatePattern ) {
					if( false === $boolNeedToSetStatusAsPr && false !== \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], $strActionUpdatePatternKey ) ) {
						$arrmixEmployeeApplicationNote['action_update'] = $strActionUpdatePatternKey;
						if( 'test sent' == $strActionUpdatePatternKey ) {
							$arrmixEmployeeApplicationNote['action_update'] = $arrmixEmployeeApplicationNote['notes'];
						}
					} else if( false !== $boolNeedToSetStatusAsPr && false !== \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], $strActionUpdatePatternKey ) && false === \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], 'Application added' ) ) {
						$arrmixEmployeeApplicationNote['action_update'] = ( 'is Updated' === $strActionUpdatePatternKey ) ? 'Updated' : $strActionUpdatePatternKey;
						$arrmixEmployeeApplicationNote['status'] = $strActionUpdatePattern;
					}
				}

				if( true == \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], 'Flags' ) ) {
					if( true == \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], 'Associated' ) ) {
						$arrmixEmployeeApplicationNote['action_update'] = 'Associated';
						$arrmixEmployeeApplicationNote['status']		 = 'Recruiter Flag';
					}
					if( true == \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], 'Dissociated' ) ) {
						$arrmixEmployeeApplicationNote['action_update'] = 'Dissociated';
						$arrmixEmployeeApplicationNote['status']		= 'Recruiter Flag';
					}
				}

				foreach( $arrstrStatusPatterns as $strStatusPattern ) {
					if( false !== \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], $strStatusPattern ) && false === \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], 'at Step Status' ) ) {
						$arrmixEmployeeApplicationNote['status'] = $strStatusPattern;
					}
				}

				if( true == is_numeric( $arrmixEmployeeApplicationNote['ps_job_posting_step_type_id'] ) && false == is_numeric( $arrmixEmployeeApplicationNote['ps_job_posting_status_id'] ) ) {
					$arrstrPsJobPostingStepTypeClasses = [ CPsJobPostingStepType::INTERVIEW => 'followup' ];
					$arrmixEmployeeApplicationNote['class_name']	= $arrstrPsJobPostingStepTypeClasses[$arrmixEmployeeApplicationNote['ps_job_posting_step_type_id']];
					$arrmixEmployeeApplicationFinalNotes[]		= $arrmixEmployeeApplicationNote;
				} elseif( true == is_numeric( $arrmixEmployeeApplicationNote['ps_job_posting_step_type_id'] ) && true == is_numeric( $arrmixEmployeeApplicationNote['ps_job_posting_status_id'] ) ) {
					$arrmixPsJobPostingStepStatusClasses = [
						CPsJobPostingStepType::SCREENING	=> [ CPsJobPostingStatus::ON_HOLD => 'occupancy-limit', CPsJobPostingStatus::SELECTED => 'user-on', CPsJobPostingStatus::REJECTED => 'user-off' ],
						CPsJobPostingStepType::INTERVIEW 	=> [ CPsJobPostingStatus::ON_HOLD => 'occupancy-limit', CPsJobPostingStatus::SELECTED => 'user-on', CPsJobPostingStatus::REJECTED => 'user-off' ],
						CPsJobPostingStepType::REFERENCES => [ CPsJobPostingStatus::ON_HOLD => 'occupancy-limit', CPsJobPostingStatus::SELECTED => 'group-on', CPsJobPostingStatus::REJECTED => 'group-off' ],
						CPsJobPostingStepType::TEST       => [ CPsJobPostingStatus::ON_HOLD => 'occupancy-limit', CPsJobPostingStatus::SELECTED => 'group-on', CPsJobPostingStatus::REJECTED => 'group-off' ],
						CPsJobPostingStepType::OFFER      => [ CPsJobPostingStatus::IN_PROCESS => 'group-on', CPsJobPostingStatus::SELECTED => 'group-on', CPsJobPostingStatus::REJECTED => 'group-off' ],
					];

					$arrmixEmployeeApplicationNote['class_name'] = $arrmixPsJobPostingStepStatusClasses[$arrmixEmployeeApplicationNote['ps_job_posting_step_type_id']][$arrmixEmployeeApplicationNote['ps_job_posting_status_id']];
					$arrmixEmployeeApplicationFinalNotes[]		= $arrmixEmployeeApplicationNote;
				} else {
					$arrmixEmployeeApplicationNote['class_name']	= self::$c_arrmixActionTypes[$arrmixEmployeeApplicationNote['action_type_id']];
					if( false !== \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], 'Scheduled' ) ) {
						$arrmixEmployeeApplicationNote['class_name'] = 'followup';
					}
					if( false !== \Psi\CStringService::singleton()->strpos( $arrmixEmployeeApplicationNote['action_description'], 'Flags' ) ) {
						$arrmixEmployeeApplicationNote['class_name'] = 'admin tag';
					}
					$arrmixEmployeeApplicationFinalNotes[]		= $arrmixEmployeeApplicationNote;
				}
				if( true == preg_match( '/Source Updated:/', $arrmixEmployeeApplicationNote['action_description'] ) || true == preg_match( '/Dissociated Reference:/', $arrmixEmployeeApplicationNote['action_description'] ) ) {
					$arrmixEmployeeApplicationNote['class_name']	= self::$c_arrmixActionTypes[$arrmixEmployeeApplicationNote['action_type_id']];
					$arrmixEmployeeApplicationFinalNotes[]		= $arrmixEmployeeApplicationNote;
				}
			}
		}

		return $arrmixEmployeeApplicationFinalNotes;
	}

}
?>