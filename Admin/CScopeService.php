<?php

class CScopeService extends CBaseScopeService {

	public function __construct() {
		parent::__construct();
		// FIXME remove this static declaration once UI implemented at CA which allows user to select specific version.
		$this->m_intMinorApiVersionId = CApiVersion::TMP_DEFAULT_MINOR_API_VERSION;
		return;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>