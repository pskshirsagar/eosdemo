<?php

class COrderFormReference extends CBaseOrderFormReference {

	const NEW_ORDER_TO_ENTRATA					= 1;
	const TRANSFER_EXISTING_DOMAIN_TO_OTHER		= 2;
	const TRANSFER_EXISTING_DOMAIN_TO_ENTRATA	= 3;

	const FLOOR_PLAN_INFORMANTION			= 4;
	const FLOOR_PLAN_UPLOAD_FILES			= 5;
	const FLOOR_PLAN_SELECT_PLAN			= 6;
	const FLOOR_PLAN_SELECT_STYLES			= 7;
	const FLOOR_PLAN_CONTRACT_AND_PAYMENT	= 8;
    const FIXED_FLOOR_PLAN_AMOUNT	        = 125.00;

	const TO_CREATE_A_LOGO_BY_DESIGNER			= 9;
	const TO_CREATE_A_LOGO_WITH_INSTRUCTIONS	= 10;

	const ORDER_FORM_REFERENCE_OTHER	        = 17;

	const CHECK_SCANNER_PRICE_WITH_ONE_YEAR_WARRANTY	= 775;
	const CHECK_SCANNER_PRICE_WITH_TWO_YEAR_WARRANTY	= 835;
	const CHECK_SCANNER_PRICE_WITH_THREE_YEAR_WARRANTY	= 885;

	public static $c_arrstrOrderFormReferenceNames = [
		COrderFormType::DISTRIBUTION_TYPE_CHANGE_REQUEST			=> 'distribution_type_options',
		COrderFormType::CHECK_SCANNING_MAX_PAYMENT_INCREASE_REQUEST	=> 'reasons_for_change',
		COrderFormType::FLOOR_PLAN_PACKAGE							=> 'floor_plan_steps'
	];

	public static $c_arrintFloorPlanSteps = [
		self::FLOOR_PLAN_INFORMANTION			=> 4,
		self::FLOOR_PLAN_UPLOAD_FILES			=> 5,
		self::FLOOR_PLAN_SELECT_PLAN			=> 6,
		self::FLOOR_PLAN_SELECT_STYLES			=> 7,
		self::FLOOR_PLAN_CONTRACT_AND_PAYMENT	=> 8
	];

	public static $c_arrstrFloorPlanOrderFormTplNames = [
		self::FLOOR_PLAN_INFORMANTION			=> 'create_or_edit_order_form',
		self::FLOOR_PLAN_UPLOAD_FILES			=> 'floor_plan_upload_files',
		self::FLOOR_PLAN_SELECT_PLAN			=> 'select_floor_plans',
		self::FLOOR_PLAN_SELECT_STYLES			=> 'select_style_for_floor_plan',
		self::FLOOR_PLAN_CONTRACT_AND_PAYMENT	=> 'contract_and_payment'
	];

}
?>
