<?php

class CTaskReference extends CBaseTaskReference {

	public function valReferenceNumber() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getReferenceNumber() ) ) {
			$boolIsValid = false;
			if( CTaskReferenceType::REJECT_REASON == $this->getTaskReferenceTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference', 'Reject Reason is required.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference', 'Environment is required.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDomainId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getReferenceNumber() ) || $this->getReferenceNumber() <= 0 ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference', 'Invalid domain Id or name.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valReferenceNumber();
				break;

			case VALIDATE_DELETE:
				break;

			case VALIDATE_COMPANY_DOMAIN_TASK_INSERT:
				$boolIsValid &= $this->valDomainId();
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function createTerminationTaskNoteAndEmailSummary( $strTaskNote, $intUserId, $objAdminDatabase ) {
		if( false == valStr( $strTaskNote ) || false == valId( $intUserId ) ) {
			return true;
		}

		$objTaskNote = new CTaskNote();

		$objTaskNote->setTaskId( $this->getTaskId() );
		$objTaskNote->setUserId( $intUserId );
		$objTaskNote->setTaskNote( $strTaskNote );

		if( false == $objTaskNote->insert( $intUserId, $objAdminDatabase ) ) {
			return false;
		}

		$objEmailSummary = new CEmailSummary();
		$objEmailSummary->setReferenceNumber( $this->getTaskId() );
		if( false == $objEmailSummary->createEmailSummaryForTaskNote( $this->getTaskId(), $objTaskNote->getId(), $objAdminDatabase, $intUserId ) ) {
			return false;
		}

		return true;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( date( 'm/d/Y H:i:s' ) );

		$this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		return true;
	}

	public function createYearlyGoalReference( $intTaskId, $objUser, $intReferenceNumber = NULL, $isInsert = true, $objAdminDatabase ) {

		$objTaskReference = New CTaskReference();
		$objTaskReference->setTaskReferenceTypeId( CTaskReferenceType::REVIEW_GOALS );
		$objTaskReference->setTaskId( $intTaskId );
		$objTaskReference->setReferenceNumber( $intReferenceNumber );

		if( true == $isInsert ) {
			if( false == $objTaskReference->insert( $objUser->getId(), $objAdminDatabase ) ) {
				return false;
			} else {
				return true;
			}
		} else {
			return $objTaskReference;
		}
	}

}
?>