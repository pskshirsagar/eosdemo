<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdSources
 * Do not add any new functions to this class.
 */

class CSemAdSources extends CBaseSemAdSources {

	public static function fetchSemAdSourceBySemAdGroupIdBySemAdId( $intSemAdGroupId, $intSemAdId, $objDatabase ) {
		return self::fetchSemAdSources( sprintf( 'SELECT * FROM sem_ad_sources WHERE sem_ad_group_id = %d AND sem_ad_id = %d', ( int ) $intSemAdGroupId, ( int ) $intSemAdId ), $objDatabase );
	}

	public static function fetchSemAdSourceBySemSourceIdBySemAdGroupIdByRemotePrimaryKey( $intSemSourceId, $intSemAdGroupId, $strRemotePrimaryKey, $objAdminDatabase ) {
		return self::fetchSemAdSource( sprintf( 'SELECT * FROM sem_ad_sources WHERE sem_source_id = %d AND sem_ad_group_id = %d AND remote_primary_key = \'%s\'', ( int ) $intSemSourceId, ( int ) $intSemAdGroupId, ( string ) addslashes( $strRemotePrimaryKey ) ), $objAdminDatabase );
	}

	public static function fetchSemAdSourceBySemSourceIdBySemAdGroupIdByTitle( $intSemSourceId, $intSemAdGroupId, $strTitle, $objAdminDatabase ) {
		return self::fetchSemAdSource( sprintf( 'SELECT sas.* FROM sem_ad_sources sas, sem_ads sa WHERE sa.id = sas.sem_ad_id AND sas.sem_source_id = %d AND sas.sem_ad_group_id = %d AND sa.title = \'%s\'', ( int ) $intSemSourceId, ( int ) $intSemAdGroupId, ( string ) addslashes( $strTitle ) ), $objAdminDatabase );
	}

	public static function fetchSyncReadySemAdSourcesBySemAccountIdBySemSourceId( $intSemAccountId, $intSemSourceId, $objAdminDatabase ) {
		$strSql = 'SELECT
						sas.*,
						sa.title,
						sa.description_line1,
						sa.description_line2,
						sa.display_url,
						sa.destination_url,
						sa.sem_campaign_id,
						sa.is_system,
						sa.deleted_on,
						sa.updated_on as sem_ad_updated_on,
						sa.deleted_by,
						sags.remote_primary_key AS sem_ad_group_remote_primary_key
					FROM
						sem_ads sa
						JOIN sem_ad_sources sas ON sa.id = sas.sem_ad_id
						JOIN sem_ad_group_sources sags ON ( sas.sem_ad_group_id = sags.sem_ad_group_id  AND sas.sem_source_id = sags.sem_source_id AND sags.remote_primary_key IS NOT NULL )
					WHERE
						sags.sem_account_id = ' . ( int ) $intSemAccountId . '
					AND
						sags.sem_source_id = ' . ( int ) $intSemSourceId . '
					AND
						sas.sem_source_id = ' . ( int ) $intSemSourceId . '
					AND
						(
							sa.is_system = 1 AND sas.remote_primary_key IS NULL
						OR
							sa.is_system = 0 AND sas.sync_requested_on IS NOT NULL
						)';

		return self::fetchSemAdSources( $strSql, $objAdminDatabase );
	}
}
?>