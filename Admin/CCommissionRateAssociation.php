<?php

use Psi\Eos\Admin\CCommissionRateAssociations;
use Psi\Eos\Admin\CCommissionStructures;

class CCommissionRateAssociation extends CBaseCommissionRateAssociation {

	protected $m_intChargeCodeId;
	protected $m_intPsProductId;
	protected $m_intNumberOfUnits;
	protected $m_intFrontLoadPosts;
	protected $m_intAverageRenewalChange;
	protected $m_intCommissionRateAssociationId;
	protected $m_intCommissionBucketId;
	protected $m_fltCommissionAmount;
	protected $m_fltCommissionPercent;
	protected $m_fltFrontLoadAmount;
	protected $m_fltFrontLoadPercentage;
	protected $m_fltUnitCommissionAmount;
	protected $m_fltEffectiveCommissionAmount;
	protected $m_fltMonthlyRecurringAmount;
	protected $m_fltMonthlyChangeAmount;
	protected $m_strPsProductName;
	protected $m_boolBookingBased;

	/**
	 * Get Functions
	 *
	 */

	public function getChargeCodeId() {
		return $this->m_intChargeCodeId;
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function getPsProductName() {
		return $this->m_strPsProductName;
	}

	public function getCommissionRateAssociationId() {
		return $this->m_intCommissionRateAssociationId;
	}

	public function getCommissionAmount() {
		return $this->m_fltCommissionAmount;
	}

	public function getCommissionPercent() {
		return $this->m_fltCommissionPercent;
	}

	public function getFrontLoadAmount() {
		return $this->m_fltFrontLoadAmount;
	}

	public function getFrontLoadPercentage() {
		return $this->m_fltFrontLoadPercentage;
	}

	public function getFrontLoadPosts() {
		return $this->m_intFrontLoadPosts;
	}

	public function getUnitCommissionAmount() {
		return $this->m_fltUnitCommissionAmount;
	}

	public function getBookingBased() {
		return $this->m_boolBookingBased;
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function getMonthlyRecurringAmount() {
		return $this->m_fltMonthlyRecurringAmount;
	}

	public function getMonthlyChangeAmount() {
		return $this->m_fltMonthlyChangeAmount;
	}

	public function getAverageRenewalChange() {
		return $this->m_intAverageRenewalChange;
	}

	public function getCommissionBucketId() {
		return $this->m_intCommissionBucketId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setChargeCodeId( $intChargeCodeId ) {
		$this->m_intChargeCodeId = CStrings::strToIntDef( $intChargeCodeId, NULL, false );
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = CStrings::strToIntDef( $intPsProductId, NULL, false );
	}

	public function setPsProductName( $strPsProductName ) {
		$this->m_strPsProductName = $strPsProductName;
	}

	public function setCommissionRateAssociationId( $intCommissionRateAssociationId ) {
		$this->m_intCommissionRateAssociationId = $intCommissionRateAssociationId;
	}

	public function setCommissionAmount( $fltCommissionAmount ) {
		$this->m_fltCommissionAmount = $fltCommissionAmount;
	}

	public function setCommissionPercent( $fltCommissionPercent ) {
		$this->m_fltCommissionPercent = $fltCommissionPercent;
	}

	public function setFrontLoadAmount( $fltFrontLoadAmount ) {
		$this->m_fltFrontLoadAmount = $fltFrontLoadAmount;
	}

	public function setFrontLoadPercentage( $fltFrontLoadPercentage ) {
		$this->m_fltFrontLoadPercentage = $fltFrontLoadPercentage;
	}

	public function setFrontLoadPosts( $intFrontLoadPosts ) {
		$this->m_intFrontLoadPosts = $intFrontLoadPosts;
	}

	public function setUnitCommissionAmount( $fltUnitCommissionAmount ) {
		$this->m_fltUnitCommissionAmount = $fltUnitCommissionAmount;
	}

	public function setBookingBased( $boolBookingBased ) {
		$this->m_boolBookingBased = $boolBookingBased;
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->m_intNumberOfUnits = $intNumberOfUnits;
	}

	public function setEffectiveCommissionAmount( $fltEffectiveCommissionAmount ) {
		$this->m_fltEffectiveCommissionAmount = $fltEffectiveCommissionAmount;
	}

	public function setMonthlyRecurringAmount( $fltMonthlyRecurringAmount ) {
		$this->m_fltMonthlyRecurringAmount = $fltMonthlyRecurringAmount;
	}

	public function setMonthlyChangeAmount( $fltMonthlyChangeAmount ) {
		$this->m_fltMonthlyChangeAmount = $fltMonthlyChangeAmount;
	}

	public function setAverageRenewalChange( $intAverageRenewalChange ) {
		$this->m_intAverageRenewalChange = $intAverageRenewalChange;
	}

	public function setCommissionBucketId( $intCommissionBucketId ) {
		$this->m_intCommissionBucketId = $intCommissionBucketId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['charge_code_id'] ) ) {
			$this->setChargeCodeId( $arrmixValues['charge_code_id'] );
		}

		if( true == isset( $arrmixValues['ps_product_id'] ) ) {
			$this->setPsProductId( $arrmixValues['ps_product_id'] );
		}

		if( true == isset( $arrmixValues['name'] ) ) {
			$this->setPsProductName( $arrmixValues['name'] );
		}

		if( true == isset( $arrmixValues['commission_rate_association_id'] ) ) {
			$this->setCommissionRateAssociationId( $arrmixValues['commission_rate_association_id'] );
		}

		if( true == isset( $arrmixValues['commission_amount'] ) ) {
			$this->setCommissionAmount( $arrmixValues['commission_amount'] );
		}

		if( true == isset( $arrmixValues['commission_percent'] ) ) {
			$this->setCommissionPercent( $arrmixValues['commission_percent'] );
		}

		if( true == isset( $arrmixValues['front_load_amount'] ) ) {
			$this->setFrontLoadAmount( $arrmixValues['front_load_amount'] );
		}

		if( true == isset( $arrmixValues['front_load_percentage'] ) ) {
			$this->setFrontLoadPercentage( $arrmixValues['front_load_percentage'] );
		}

		if( true == isset( $arrmixValues['front_load_posts'] ) ) {
			$this->setFrontLoadPosts( $arrmixValues['front_load_posts'] );
		}

		if( true == isset( $arrmixValues['unit_commission_amount'] ) ) {
			$this->setUnitCommissionAmount( $arrmixValues['unit_commission_amount'] );
		}

		if( true == isset( $arrmixValues['is_booking_based'] ) ) {
			$this->setBookingBased( $arrmixValues['is_booking_based'] );
		}

		if( true == isset( $arrmixValues['number_of_units'] ) ) {
			$this->setNumberOfUnits( $arrmixValues['number_of_units'] );
		}

		if( true == isset( $arrmixValues['effective_commission_amount'] ) ) {
			$this->setEffectiveCommissionAmount( $arrmixValues['effective_commission_amount'] );
		}

		if( true == isset( $arrmixValues['monthly_recurring_amount'] ) ) {
			$this->setMonthlyRecurringAmount( $arrmixValues['monthly_recurring_amount'] );
		}

		if( true == isset( $arrmixValues['monthly_change_amount'] ) ) {
			$this->setMonthlyChangeAmount( $arrmixValues['monthly_change_amount'] );
		}

		if( true == isset( $arrmixValues['commission_bucket_id'] ) ) {
			$this->setCommissionBucketId( $arrmixValues['commission_bucket_id'] );
		}
		return;
	}

	public function setDefaults() {

		$this->m_strStartDate = date( 'm/d/Y' );
		$this->m_strEndDate   = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm' ), + date( 't' ), date( 'Y' ) ) );

		return;
	}

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCommissionRecipientId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCommissionRecipientId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_recipient_id', 'Commission recipient is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCommissionStructureId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCommissionStructureId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_structure_id', 'Commission structure is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCommissionRateId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCommissionRateId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_rate_id', 'Commission rate is required.' ) );
		}

		return $boolIsValid;
	}

	public function valStartDate() {

		$boolIsValid = true;

		if( true == is_null( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Start date is required.' ) );
			return $boolIsValid;
		}

		if( false == CValidation::validateDate( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Valid start date is required.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End date is required.' ) );
			return $boolIsValid;
		}

		if( false == CValidation::validateDate( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Valid end date is required.' ) );
			return $boolIsValid;
		}

		if( strtotime( $this->getEndDate() ) < strtotime( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End date should be greater than start date.' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valCascadingCommissionStructure( $objDatabase ) {
		$boolIsValid = true;

		/******************************************************************************************************************
		 * **** Cascading commission structures -																		*
		 * 			--  If the structure is associated to company then it should not be associated to accounts. 		*
		 * 			--  If the structure is associated to account then it can not be associated to company.				*
		 * **** Non-cascading commission structures -																	*
		 * 			--  It can be associated to both company as well as accounts either way.							*
		 ******************************************************************************************************************/

		$strSql = 'SELECT count(cra.id)
					FROM
						commission_rate_associations cra,
						commission_structures cs
					WHERE cra.cid = ' . ( int ) $this->getCid() . '
					 AND cra.commission_rate_id = ' . ( int ) $this->getCommissionRateId() . '
					 AND cra.commission_recipient_id = ' . ( int ) $this->getCommissionRecipientId() . '
					 AND cra.commission_structure_id =  ' . ( int ) $this->getCommissionStructureId() . '
					 AND cra.deleted_by IS NULL
					 AND cs.id = cra.commission_structure_id
					 AND cs.commission_structure_type_id = ' . ( int ) CCommissionStructureType::CASCADING;

		if( false == is_null( $this->getAccountId() ) && true == is_numeric( $this->getAccountId() ) ) {

			$strSql .= ' AND cra.account_id IS NULL ';

			$arrintCommissionWithoutAccountCount = fetchData( $strSql, $objDatabase );

			if( 0 != $arrintCommissionWithoutAccountCount[0]['count'] ) {
				$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Commissions have already been added directly to the client.' ) );
			}
		} else {

			$strSql .= ' AND cra.account_id IS NOT NULL ';

			$arrintCommissionWithAccountCount = fetchData( $strSql, $objDatabase );

			if( 0 != $arrintCommissionWithAccountCount[0]['count'] ) {
				$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Commissions have already been added directly to the account(s).' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCommissionDuplicity( $objDatabase ) {
		$boolIsValid = true;

		$strIdWhere = ( true == is_numeric( $this->m_intId ) ) ? ' id <> ' . ( int ) $this->m_intId : ' id IS NOT NULL ';
		$strAccountIdWhere = ( true == is_numeric( $this->m_intAccountId ) ) ? ' account_id = ' . ( int ) $this->m_intAccountId : ' account_id IS NULL ';
		$strEmployeeIdWhere  = ( true == is_numeric( $this->m_intCommissionStructureId ) ) ? ' commission_structure_id = ' . ( int ) $this->m_intCommissionStructureId : ' commission_structure_id IS NULL ';
		$strEmployeeIdWhere .= ( true == is_numeric( $this->m_intCommissionRateId ) ) ? ' AND commission_rate_id = ' . ( int ) $this->m_intCommissionRateId : ' AND commission_rate_id IS NULL ';

		$strWhere = ' WHERE '
						. $strIdWhere . '
						AND ( ( start_date <= \'' . $this->m_strEndDate . '\' AND start_date >= \'' . $this->m_strStartDate . '\' ) OR ( end_date >= \'' . $this->m_strStartDate . '\' AND end_date <= \'' . $this->m_strEndDate . '\' ) )
						AND cid = ' . ( int ) $this->m_intCid . '
						AND deleted_by IS NULL
						AND ' . $strAccountIdWhere . '
						AND ' . $strEmployeeIdWhere;

		$intCountCommissionRateAssociations = CCommissionRateAssociations::createService()->fetchCommissionRateAssociationCount( $strWhere, $objDatabase );

		if( 0 < $intCountCommissionRateAssociations ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'This employee commission rate conflicts with one already added.  Please edit the preexisting commission rate.' ) );
		}

		return $boolIsValid;
	}

	public function validateDuplicateCommissionRateAssociations( $objDatabase ) {
		$boolIsValid = true;

		$strIdWhere = ( true == is_numeric( $this->m_intId ) ) ? ' id <> ' . ( int ) $this->m_intId : ' id IS NOT NULL ';
		$strAccountIdWhere = ( true == is_numeric( $this->m_intAccountId ) ) ? ' account_id = ' . ( int ) $this->m_intAccountId : ' account_id IS NULL ';
		$strEmployeeIdWhere  = ( true == is_numeric( $this->m_intCommissionStructureId ) ) ? ' commission_structure_id = ' . ( int ) $this->m_intCommissionStructureId : ' commission_structure_id IS NULL ';
		$strEmployeeIdWhere .= ( true == is_numeric( $this->m_intPropertyId ) ) ? ' AND property_id = ' . ( int ) $this->m_intPropertyId : ' AND commission_rate_id IS NULL ';
		$strEmployeeIdWhere .= ( true == is_numeric( $this->m_intContractId ) ) ? ' AND contract_id = ' . ( int ) $this->m_intContractId : ' ';
		$strEmployeeIdWhere .= ( true == is_numeric( $this->m_intCommissionRateId ) ) ? ' AND commission_rate_id = ' . ( int ) $this->m_intCommissionRateId : ' AND commission_rate_id IS NULL ';

		$strWhere = ' WHERE '
				. $strIdWhere . '
						AND cid = ' . ( int ) $this->m_intCid . '
						AND deleted_by IS NULL
						AND ' . $strAccountIdWhere . '
						AND ' . $strEmployeeIdWhere;

		$intCountCommissionRateAssociations = CCommissionRateAssociations::createService()->fetchCommissionRateAssociationCount( $strWhere, $objDatabase );

		if( 0 < $intCountCommissionRateAssociations ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'This employee commission rate conflicts with one already added.  Please edit the preexisting commission rate.' ) );
		}

		return $boolIsValid;
	}

	public function validateStartDateWithEndDate( $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getStartDate() ) && false == is_null( $this->getEndDate() ) ) {

			 // Check the duration of start date and end date of the commission rate association is less or not.
			 // if it is more than the commission structure duration then add error message.

			$objCommissionStructure = CCommissionStructures::createService()->fetchCommissionStructureById( $this->getCommissionStructureId(), $objDatabase );

			$intStartTime = strtotime( $this->getStartDate() . ' +' . $objCommissionStructure->getDefaultPeriodMonths() . 'months' );
			$strEndDate   = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm', $intStartTime ), date( 'j', $intStartTime ) - 1, date( 'Y', $intStartTime ) ) );

			if( strtotime( $this->getEndDate() ) > strtotime( $strEndDate ) ) {
				$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Duration is more than allowed in the commission structure selected.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
 		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCommissionRecipientId();
				$boolIsValid &= $this->valCommissionStructureId();
				$boolIsValid &= $this->valCommissionRateId();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();

				if( true == $boolIsValid ) {
					$boolIsValid &= $this->valCommissionDuplicity( $objDatabase );
					$boolIsValid &= $this->validateStartDateWithEndDate( $objDatabase );
					$boolIsValid &= $this->valCascadingCommissionStructure( $objDatabase );
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function validateNewAssociation( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valCommissionRecipientId();
				$boolIsValid &= $this->valCommissionStructureId();
				$boolIsValid &= $this->valCommissionRateId();
				$boolIsValid &= $this->valStartDate();
				if( true == $boolIsValid ) {
					$boolIsValid &= $this->validateDuplicateCommissionRateAssociations( $objDatabase );
					$boolIsValid &= $this->valCascadingCommissionStructure( $objDatabase );
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );

		return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>