<?php

class CContractTerminationEventType extends CBaseContractTerminationEventType {

	const TERMINATE_PRODUCT 							= 1;
	const UPDATE_BILLING_INFORMATION 					= 2;
	const UPDATE_MERCHANT_BANKING_INFORMATION 			= 3;
	const SEND_EMAIL 									= 4;
	const CREATE_TASK 									= 5;
	const SYSTEM 										= 6;
	const TERMINATION_DOCUMENT							= 7;
	const TERMINATE_CONTRACT							= 8;
	const TERMINATE_BUNDLE_PRODUCT						= 9;
	const PROPERTY_TERMINATION_EMAIL					= 10;
	const TERMINATE_DOCUMENT_TEMPLATE					= 11;
	const TERMINATE_DEFAULT_MERCHANT_ACCOUNT			= 12;
	const TERMINATE_AUTOMATIC_POST_SCHEDULED_CHARGES	= 13;
	const TERMINATE_DISABLE_ILS_PORTAL					= 14;
	const TERMINATE_DISABLE_RESIDENT_PAY				= 15;
	const TERMINATE_ENABLE_RENT_REMINDER				= 16;
	const TERMINATE_DISABLE_LEASING_CENTER				= 17;
	const DISASSOCIATE_PROPERTY_INTEGRATION				= 18;
	const TERMINATE_DOCUMENT_PACKETS					= 19;
	const DISASSOCIATE_RENTAL_APPLICATION_DOCUMENT		= 20;
	const TERMINATE_PROPERTY_WEBSITES					= 21;
	const REMOVE_RESIDENT_RECURRING_PAYMENTS			= 22;
	const TERMINATE_CHARGE_CODE_SPECIFIC_MERCHANT_ACCOUNT = 23;
	const DEACTIVATE_WEBSITES							= 24;
	const DISABLE_PROPERTY								= 25;
	const TERMINATE_CONTRACT_PROPERTY							= 26;

	// list of event types for which it should generate only one termination event per termination request
	public static $c_arrintTermincationRequestLevelEvents 	= [ self::CREATE_TASK, self::SEND_EMAIL, self::PROPERTY_TERMINATION_EMAIL, self::TERMINATE_CONTRACT, self::TERMINATION_DOCUMENT ];

	// list of events need to be process at the time of generating termination request
	public static $c_arrintPressingEvents					= [ self::CREATE_TASK, self::SEND_EMAIL, self::TERMINATION_DOCUMENT ];

	// list of events which need to be process at the time of deactivation date
	public static $c_arrintEventsBasedOnDeactivationDate	= [ self::TERMINATE_PRODUCT ];

	// list of events which effective date is terminaion requested date
	public static $c_arrintEventsBasedOnRequestedDate		= [ self::CREATE_TASK, self::SEND_EMAIL, self::PROPERTY_TERMINATION_EMAIL, self::TERMINATE_CONTRACT, self::TERMINATION_DOCUMENT ];

	// list of events which need to pe process on termination date only if deactivation date is greter than termination date
	public static $c_arrintEventsBasedOnTerminationDate		= [ self :: TERMINATE_CONTRACT_PROPERTY ];

	public static $c_arrstrTerminationEventsTables = [
		self::TERMINATE_DEFAULT_MERCHANT_ACCOUNT              => 'property_merchant_accounts',
		self::TERMINATE_CHARGE_CODE_SPECIFIC_MERCHANT_ACCOUNT => 'property_merchant_accounts',
		self::TERMINATE_AUTOMATIC_POST_SCHEDULED_CHARGES      => 'property_charge_settings',
		self::TERMINATE_DISABLE_ILS_PORTAL                    => 'property_preferences',
		self::TERMINATE_DISABLE_RESIDENT_PAY                  => 'property_preferences',
		self::TERMINATE_ENABLE_RENT_REMINDER                  => 'property_preferences',
		self::TERMINATE_DOCUMENT_TEMPLATE                     => [
			'remove_association'  => 'property_group_documents',
			'deactivate_template' => 'documents'
		],
		self::TERMINATE_DOCUMENT_PACKETS                      => [
			'remove_association'  => 'property_group_documents',
			'deactivate_template' => 'documents'
		],
		self::TERMINATE_DISABLE_LEASING_CENTER                => [
			CPropertySettingKey::LEAD_CHAT_ROUTING_CONDITION                                    => 'property_preferences',
			CPropertySettingKey::PROPERTY_CALL_SETTINGS_MAINTENANCE_EMERGENCY_ROUTING_CONDITION => 'property_call_settings',
			CPropertySettingKey::PROPERTY_CALL_SETTINGS_MAINTENANCE_ROUTING_CONDITION           => 'property_call_settings',
			CPropertySettingKey::PROPERTY_CALL_SETTINGS_LEAD_ROUTING_CONDITION                  => 'property_call_settings'
		],
		self::TERMINATE_PROPERTY_WEBSITES                     => [
			'website'   => 'website_properties',
			'domain'    => 'CWebsiteDomains',
			'directory' => 'Directives'
		],
		self::DISASSOCIATE_PROPERTY_INTEGRATION               => [
			'database_ids'       => 'property_integration_databases',
			'primary_remote_key' => 'properties'
		],
		self::DEACTIVATE_WEBSITES                             => 'websites',
		self::DISASSOCIATE_RENTAL_APPLICATION_DOCUMENT        => 'company_applications',
		self::REMOVE_RESIDENT_RECURRING_PAYMENTS              => 'scheduled_payments',
		self::DISABLE_PROPERTY                                => 'properties'
	];

	/**
	public function getConstants() {
		$objClass = new ReflectionClass( __CLASS__ );
		return $objClass->getConstants();
	}
	*/

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>