<?php

class CSurveyReference extends CBaseSurveyReference {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSurveyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSurveyReferenceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSurveyQuestionAnswerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>