<?php

use Psi\Eos\Admin\CPsProductRelationships;
use Psi\Eos\Admin\CProductBundles;

class CProductBundle extends CBaseProductBundle {

	// Declaration Section

	protected $m_arrobjPsProductRelationships;

	// Added Global bundle product
	const DIGITAL_MARKETING			= 20616;
	const PRIOR_MONTH_ENROLLMENTS   = 56933;
	const PRIOR_MONTH_CANCELLATIONS = 56934;
	const CURRENT_MONTH_RENEWALS    = 56935;

	// Get Functions

	public function getPsProductRelationships() {
		return $this->m_arrobjPsProductRelationships;
	}

	// Set Functions

	public function setPsProductRelationships( $arrobjPsProductRelationships ) {
		$this->m_arrobjPsProductRelationships = $arrobjPsProductRelationships;
	}

	// Create Functions

	public function createPsProductRelationship() {
		$objPsProductRelationship = new CPsProductRelationship();
		$objPsProductRelationship->setBundlePsProductId( $this->getId() );
		$objPsProductRelationship->setPsLeadId( $this->getPsLeadId() );
		return $objPsProductRelationship;
	}

	public function createPsLeadCustomBundleFromGlobalBundle( $intPsLeadId, $intUserId, $objAdminDatabase, $arrobjPsProductRelationships = array() ) {

		$intBundlePsProductId = $this->getId();

		$this->setId( NULL );
		$this->setProductBundleId( Self::DIGITAL_MARKETING );
		$this->setPsLeadId( $intPsLeadId );

		// Creating child bundle product from global bundle.
		if( false == $this->insert( $intUserId, $objAdminDatabase ) ) {
			$objAdminDatabase->rollback();
			return NULL;
		}

		if( false == valArr( $arrobjPsProductRelationships ) ) {
			$arrobjPsProductRelationships = CPsProductRelationships::createService()->fetchPsProductRelationshipsByBundlePsProductIds( [ $intBundlePsProductId ], $objAdminDatabase );
		}

		$arrobjNewPsProductRelationships = [];
		foreach( $arrobjPsProductRelationships as $objPsProductRelationship ) {
			$objPsProductRelationship->setId( NULL );
			$objPsProductRelationship->setPsLeadId( $intPsLeadId );
			$objPsProductRelationship->setBundlePsProductId( $this->getId() );
			$arrobjNewPsProductRelationships[] = $objPsProductRelationship;
		}

		if( false == CPsProductRelationships::createService()->bulkInsert( $arrobjNewPsProductRelationships, $intUserId, $objAdminDatabase ) ) {
			$objAdminDatabase->rollback();
			return NULL;
		}

		return $this;
	}

	public function createClone( $intUserId, $objDatabase ) {
		$objProductBundle = clone $this;

		$objProductBundle->setId( $objProductBundle->fetchNextId( $objDatabase ) );
		$objProductBundle->setCreatedBy( $intUserId );
		$objProductBundle->setCreatedOn( 'NOW()' );
		$objProductBundle->setUpdatedBy( $intUserId );
		$objProductBundle->setUpdatedOn( 'NOW()' );

		return $objProductBundle;
	}

	// Validate Functions

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProductBundleId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsLeadId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductDisplayTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitMonthlyRecurring() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertySetup() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMinUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMaxUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCorporateEntity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolValidateDuplicateName = true, $boolAllowRoundBrackets = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_bundle':
				$boolIsValid &= $this->valPsProductBundleName( $objDatabase, $boolValidateDuplicateName, $boolAllowRoundBrackets );
				$boolIsValid &= $this->valRecurringAmount();
				$boolIsValid &= $this->valPropertySetup();
				$boolIsValid &= $this->valMinMaxUnits();
				break;

			case 'validate_stand_alone_bundle':
				$boolIsValid &= $this->valPsProductBundleName( $objDatabase, $boolValidateDuplicateName );
				$boolIsValid &= $this->valPropertySetup();
				$boolIsValid &= $this->valMinMaxUnits();
				$boolIsValid &= $this->checkDuplicateBundle( $objDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function valRecurringAmount() {
		$boolIsValid = true;

		if( ( 0 >= $this->getUnitMonthlyRecurring() && false == is_null( $this->getUnitMonthlyRecurring() ) ) || true == is_null( $this->getUnitMonthlyRecurring() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_type_id', 'Monthly large property recurring amount should be greater than zero.' ) );
		}

		return $boolIsValid;
	}

	public function valMinMaxUnits() {
		$boolIsValid = true;

		if( ( int ) $this->getMinUnits() > ( int ) $this->getMaxUnits() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_max_units', 'Max units should be greater than min units.' ) );
		}

		return $boolIsValid;
	}

	public function valCurrency( $strCurrency ) {
		$strCurrencyCheck = '/^(\d*)(\.\d{1,2})?$/';

		if( 1 !== \Psi\CStringService::singleton()->preg_match( $strCurrencyCheck, $strCurrency ) ) {
			return false;
		}

		return true;
	}

	public function valPsProductBundleName( $objDatabase = NULL, $boolValidateDuplicateName = true, $boolAllowRoundBrackets = false ) {
		$boolIsValid = true;

		$strMatchString = '/^[.a-zA-Z0-9&\s]+$/';
		if( true == $boolAllowRoundBrackets ) {
			$strMatchString = '/^[-.a-zA-Z0-9()&\s]+$/';
		}

		if( false == valStr( $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
			return false;
		}

		if( false == \Psi\CStringService::singleton()->preg_match( $strMatchString, $this->getName() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name should be in alphanumeric.' ) );
			return false;
		}

		if( true == $boolValidateDuplicateName && true == $boolIsValid ) {

			$strSql = ' WHERE
							name ilike \'' . trim( addslashes( $this->getName() ) ) . '\'
							AND ps_lead_id = ' . ( int ) $this->getPsLeadId() . ';';

			$intCount = CProductBundles::createService()->fetchRowCount( $strSql, 'product_bundles', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Bundle name already exists.' ) );
			}

		}
		return $boolIsValid;
	}

	// Other Functions

	public function checkDuplicateBundle( $objDatabase ) {

		if( true == valArr( $this->getPsProductRelationships() ) ) {
			$arrintPsProductIds = array_keys( rekeyObjects( 'PsProductId', $this->getPsProductRelationships() ) );
			if( true == valObj( $objDatabase, 'CDatabase' ) && true == valArr( $arrintPsProductIds ) ) {
				$strSql = 'SELECT
								count(bundle_ps_product_id) as count,
								max(existing_bundle_name) as existing_bundle_name
							FROM (
								SELECT
									DISTINCT ON( pr.bundle_ps_product_id )
									pr.bundle_ps_product_id,
									p.name as existing_bundle_name,
									p.is_corporate_entity,
									pr.ps_lead_id,
									SUM( CASE WHEN pr.ps_product_id IN( ' . implode( ',', $arrintPsProductIds ) . ' ) THEN 1 ELSE 0 END ) OVER ( PARTITION BY pr.bundle_ps_product_id ) as matched_count,
									COUNT(pr.id ) OVER ( PARTITION BY pr.bundle_ps_product_id ) as total_ps_products
								FROM
									ps_product_relationships pr
									JOIN product_bundles p ON pr.bundle_ps_product_id = p.id
								WHERE
									pr.ps_lead_id = ' . ( int ) $this->getPsLeadId() . ' 
									AND p.is_corporate_entity = ' . ( $this->getIsCorporateEntity() ? 'TRUE' : 'FALSE' ) . '
							) as sub
							WHERE
								matched_count = total_ps_products
								AND total_ps_products=' . \Psi\Libraries\UtilFunctions\count( $arrintPsProductIds ) . ';';

				$arrmixValues	= fetchData( $strSql, $objDatabase );
				$intCount 		= ( true == isset( $arrmixValues[0] ) ) ? ( int ) $arrmixValues[0]['count'] : 0;

				if( 0 < $intCount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Bundle "' . $arrmixValues[0]['existing_bundle_name'] . '" with same products already exists.' ) );
					return false;
				}
			}
		}

		return true;
	}

	public function checkDuplicatePsProductBundle( $arrintPsProductIds, $objDatabase ) {

		if( false == valArr( $arrintPsProductIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						count( bundle_ps_product_id ) as count,
						max( existing_bundle_name ) as existing_bundle_name
					FROM (
							SELECT
								DISTINCT ON( pr.bundle_ps_product_id ) pr.bundle_ps_product_id,
								pb.name as existing_bundle_name,
								SUM( CASE WHEN pr.ps_product_id IN( ' . implode( ',', $arrintPsProductIds ) . ' ) THEN 1 ELSE 0 END ) OVER ( PARTITION BY pr.bundle_ps_product_id ) as matched_count,
								COUNT( pr.id ) OVER ( PARTITION BY pr.bundle_ps_product_id ) as total_ps_products
							FROM
								ps_product_relationships pr
								JOIN product_bundles pb ON pr.bundle_ps_product_id = pb.id
							WHERE
								pr.ps_lead_id = ' . ( int ) $this->getPsLeadId() . '
						) as sub
					WHERE
						matched_count = total_ps_products
						AND total_ps_products=' . \Psi\Libraries\UtilFunctions\count( $arrintPsProductIds ) . ';';

		$arrmixValues	= fetchData( $strSql, $objDatabase );
		$intCount 		= ( true == isset( $arrmixValues[0] ) ) ? ( int ) $arrmixValues[0]['count'] : 0;

		if( 0 < $intCount ) {
			if( 1 < $intCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' <br /> Multiple bundles already exists with same products.' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' <br /> Bundle "' . $arrmixValues[0]['existing_bundle_name'] . '" already exists with same products.' ) );
			}

			return false;
		}

		return true;
	}

}
?>
