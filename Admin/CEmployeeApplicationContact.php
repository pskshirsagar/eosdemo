<?php

class CEmployeeApplicationContact extends CBaseEmployeeApplicationContact {

	/**
	 * Other Functions
	 */

	public function loadIconName() {

		switch( $this->getEmployeeApplicationContactTypeId() ) {

			case CEmployeeApplicationContactType::CALL_OUTBOUND:
			case CEmployeeApplicationContactType::CALL_INBOUND:
				return 'phone-icon';
				break;

			case CEmployeeApplicationContactType::EMAIL_ADVERTISEMENT:
			case CEmployeeApplicationContactType::NEWSLETTER:
			case CEmployeeApplicationContactType::MASS_EMAIL:
			case CEmployeeApplicationContactType::WEBSITE_NEWSLETTER:
			case CEmployeeApplicationContactType::WEBSITE_INFO:
				return 'newsletter-icon';
				break;

			case CEmployeeApplicationContactType::EMAIL_OUTBOUND:
			case CEmployeeApplicationContactType::EMAIL_INBOUND:
				return 'email-icon';
				break;

			case CEmployeeApplicationContactType::IN_PERSON_VISIT:
				return 'in-person-visit';
				break;

			case CEmployeeApplicationContactType::DIRECT_MAIL:
				return 'mail-icon';
				break;

			case CEmployeeApplicationContactType::INBOUND_SMS:
			case CEmployeeApplicationContactType::OUTBOUND_SMS:
				return 'SMS-icon';
				break;

			case CEmployeeApplicationContactType::XENTO_WEBSITE_VISIT:
				return 'ps-visit';
				break;

			default:
				return 'Symbols-Critical-icon';
			break;

		}

		return 'Symbols-Critical-icon';
	}

	public function insertEmployeeApplicationContact( $intCurrentUserId, $intEmployeeApplicationId, $intEmployeeApplicationContactTypeId, $strNote, $objDatabase ) {

		$this->setEmployeeApplicationId( $intEmployeeApplicationId );
		$this->setEmployeeApplicationContactTypeId( $intEmployeeApplicationContactTypeId );
		$this->setNote( $strNote );

		if( false == $this->insert( $intCurrentUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function setContactMinutes( $intContactMinutes ) {

		$intContactMinutes = trim( $intContactMinutes );
		$this->m_intContactMinutes = ( false == empty( $intContactMinutes ) ) ? $intContactMinutes : NULL;

		return;
	}

	/**
	 * validate Functions
	 */

	public function valEmployeeApplicationContactTypeId() {

		$boolIsValid = true;

		if( false == isset( $this->m_intEmployeeApplicationContactTypeId ) || false == is_numeric( $this->m_intEmployeeApplicationContactTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_application_contact_type_id', 'Employee Application Contact Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContactDatetime() {

		$boolIsValid = true;

		if( true == is_null( $this->m_strContactDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_datetime', ' Contact Datetime is required.' ) );
		}

		if( true == isset( $this->m_strContactDatetime ) && false == CValidation::validateDate( $this->m_strContactDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_datetime', ' Contact Datetime is not a valid date.' ) );
		}

		return $boolIsValid;
	}

	public function valContactMinutes() {

		$boolIsValid = true;

		if( false == empty( $this->m_intContactMinutes ) && false == is_numeric( $this->m_intContactMinutes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_minutes', 'Contact minutes must be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valNote() {

		$boolIsValid = true;

		if( false == isset( $this->m_strNote ) || 0 == strlen( $this->m_strNote ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', 'Note is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valEmployeeApplicationContactTypeId();
				$boolIsValid &= $this->valContactDatetime();
				$boolIsValid &= $this->valContactMinutes();
				$boolIsValid &= $this->valNote();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>