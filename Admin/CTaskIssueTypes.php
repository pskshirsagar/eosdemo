<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskIssueTypes
 * Do not add any new functions to this class.
 */

class CTaskIssueTypes extends CBaseTaskIssueTypes {

	public static function fetchTaskIssueTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTaskIssueType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTaskIssueType( $strSql, $objDatabase ) {
		return self::fetchObject( $strSql, 'CTaskIssueType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllTaskIssueTypes( $objDatabase, $boolSupport = false ) {

		$strSql = 'SELECT
						*
					FROM
						public.task_issue_types
					WHERE';
		if( true == $boolSupport )
			$strSql .=	' is_support = true';
		else
			$strSql .=	' is_support = false';

		return self::fetchTaskIssueTypes( $strSql, $objDatabase );
	}

}
?>