<?php

class CEmployeeDocumentCategory extends CBaseEmployeeDocumentCategory {

	// constant created for document type
	const ID_INTERNET_AND_MOBILE_ALLOWANCE				= 1;
	const ID_CLUB_AND_GYM_ALLOWANCE						= 2;
	const ID_ATTIRE_ALLOWANCE							= 3;
	const ID_HOUSE_RENT_ALLOWANCE						= 4;
	const ID_LEAVE_TRAVEL_ALLOWANCE						= 5;
	const ID_SELF_OCCUPIED_HOUSING_LOAN					= 6;
	const ID_LET_OUT_PROPERTY_HOUSING_LOAN				= 7;
	const ID_LIFE_INSURANCE_PREMIUM						= 8;
	const ID_PUBLIC_PF_INVESTMENT						= 9;
	const ID_NATIONAL_SAVING_CERTIFICATE				= 10;
	const ID_UNIT_LINKED_INSURANCE_PLAN					= 11;
	const ID_ELSS_MUTUAL_FUND_INVESTMENTS				= 12;
	const ID_TUITION_FEES								= 13;
	const ID_TAX_SAVER_FD_SCHEME						= 14;
	const ID_SUKANYA_SAMRIDDHI_ACCOUNT					= 15;
	const ID_PENSION_FUND_CONTRIBUTION					= 16;
	const ID_NATIONAL_PENSION_SCHEME					= 17;
	const ID_EDUCATION_PAYMENTS							= 18;
	const ID_DONATIONS									= 19;
	const ID_MEDICAL_INSURANCE							= 20;
	const ID_MEDICAL_INSURANCE_PARENTS					= 21;
	const ID_PHYSICAL_DISABILITY						= 22;
	const ID_MEDICAL_TREATMENT							= 23;
	const ID_PHYSICAL_DISABILITY_PERMANENT				= 24;
	const ID_OTHER_DEDUCTIONS							= 25;
	const ID_PREVIOUS_EMPLOYER_SALARY_DETAILS			= 26;
	const ID_INTEREST_OF_FIRST_TIME_HOME_OWNER			= 29;
	const ID_INTEREST_DEDUCTION_OF_AFFORDABLE_HOUSING	= 30;
	const ID_HOUSING_LOAN_PRINCIPAL_REPAYMENT			= 31;
	const ID_NCND										= 32;
	const ID_VPN										= 33;
	const ID_HARDWARE_ALLOCATION						= 34;
	const ID_REMOTE_WFH									= 35;

	const ID_DESIGNATION_CHANGE_REVIEW_LETTER			= 27;
	const ID_SALARY_CHANGE_REVIEW_LETTER				= 28;

	public static $c_arrmixEmployeeTaxSavingDocumentCategories = [
		'INTERNET_AND_MOBILE_ALLOWANCE' => self::ID_INTERNET_AND_MOBILE_ALLOWANCE,
		'CLUB_AND_GYM_ALLOWANCE' => self::ID_CLUB_AND_GYM_ALLOWANCE,
		'ATTIRE_ALLOWANCE' => self::ID_ATTIRE_ALLOWANCE,
		'HOUSE_RENT_ALLOWANCE' => self::ID_HOUSE_RENT_ALLOWANCE,
		'LEAVE_TRAVEL_ALLOWANCE' => self::ID_LEAVE_TRAVEL_ALLOWANCE,
		'SELF_OCCUPIED_HOUSING_LOAN' => self::ID_SELF_OCCUPIED_HOUSING_LOAN,
		'LET_OUT_PROPERTY_HOUSING_LOAN' => self::ID_LET_OUT_PROPERTY_HOUSING_LOAN,
		'LIFE_INSURANCE_PREMIUM' => self::ID_LIFE_INSURANCE_PREMIUM,
		'PUBLIC_PF_INVESTMENT' => self::ID_PUBLIC_PF_INVESTMENT,
		'NATIONAL_SAVING_CERTIFICATE' => self::ID_NATIONAL_SAVING_CERTIFICATE,
		'UNIT_LINKED_INSURANCE_PLAN' => self::ID_UNIT_LINKED_INSURANCE_PLAN,
		'ELSS_MUTUAL_FUND_INVESTMENTS' => self::ID_ELSS_MUTUAL_FUND_INVESTMENTS,
		'TUITION_FEES' => self::ID_TUITION_FEES,
		'TAX_SAVER_FD_SCHEME' => self::ID_TAX_SAVER_FD_SCHEME,
		'SUKANYA_SAMRIDDHI_ACCOUNT' => self::ID_SUKANYA_SAMRIDDHI_ACCOUNT,
		'PENSION_FUND_CONTRIBUTION' => self::ID_PENSION_FUND_CONTRIBUTION,
		'NATIONAL_PENSION_SCHEME' => self::ID_NATIONAL_PENSION_SCHEME,
		'EDUCATION_PAYMENTS' => self::ID_EDUCATION_PAYMENTS,
		'DONATIONS' => self::ID_DONATIONS,
		'MEDICAL_INSURANCE' => self::ID_MEDICAL_INSURANCE,
		'MEDICAL_INSURANCE_PARENTS' => self::ID_MEDICAL_INSURANCE_PARENTS,
		'PHYSICAL_DISABILITY' => self::ID_PHYSICAL_DISABILITY,
		'MEDICAL_TREATMENT' => self::ID_MEDICAL_TREATMENT,
		'PHYSICAL_DISABILITY_PERMANENT' => self::ID_PHYSICAL_DISABILITY_PERMANENT,
		'OTHER_DEDUCTIONS' => self::ID_OTHER_DEDUCTIONS,
		'PREVIOUS_EMPLOYER_SALARY_DETAILS,' => self::ID_PREVIOUS_EMPLOYER_SALARY_DETAILS,
		'INTEREST_OF_FIRST_TIME_HOME_OWNER' => self::ID_INTEREST_OF_FIRST_TIME_HOME_OWNER,
		'INTEREST_DEDUCTION_OF_AFFORDABLE_HOUSING' => self::ID_INTEREST_DEDUCTION_OF_AFFORDABLE_HOUSING,
		'HOUSING_LOAN_PRINCIPAL_REPAYMENT' => self::ID_HOUSING_LOAN_PRINCIPAL_REPAYMENT
	];

	public static $c_arrmixSpineUploadTypeCategoryA = [ self::ID_SELF_OCCUPIED_HOUSING_LOAN, self::ID_LET_OUT_PROPERTY_HOUSING_LOAN ];

	public static $c_arrmixSpineUploadTypeCategoryB = [
		self::ID_CLUB_AND_GYM_ALLOWANCE,
		self::ID_ATTIRE_ALLOWANCE,
		self::ID_HOUSING_LOAN_PRINCIPAL_REPAYMENT,
		self::ID_MEDICAL_INSURANCE_PARENTS,
		self::ID_MEDICAL_INSURANCE,
		self::ID_TUITION_FEES,
		self::ID_LIFE_INSURANCE_PREMIUM,
		self::ID_PUBLIC_PF_INVESTMENT,
		self::ID_INTERNET_AND_MOBILE_ALLOWANCE,
		self::ID_LEAVE_TRAVEL_ALLOWANCE,
		self::ID_PENSION_FUND_CONTRIBUTION,
		self::ID_NATIONAL_PENSION_SCHEME,
		self::ID_UNIT_LINKED_INSURANCE_PLAN,
		self::ID_ELSS_MUTUAL_FUND_INVESTMENTS,
		self::ID_SUKANYA_SAMRIDDHI_ACCOUNT,
		self::ID_PHYSICAL_DISABILITY,
		self::ID_DONATIONS,
		self::ID_TAX_SAVER_FD_SCHEME,
		self::ID_NATIONAL_SAVING_CERTIFICATE,
		self::ID_EDUCATION_PAYMENTS,
		self::ID_MEDICAL_TREATMENT,
		self::ID_PHYSICAL_DISABILITY_PERMANENT,
		self::ID_INTEREST_OF_FIRST_TIME_HOME_OWNER,
		self::ID_INTEREST_DEDUCTION_OF_AFFORDABLE_HOUSING
	];

	public static $c_arrmixSpineUploadTypeCategoryC = [ self::ID_HOUSE_RENT_ALLOWANCE ];

	public static $c_arrmixSpineUploadTypeCategoryD = [ self::ID_OTHER_DEDUCTIONS, self::ID_PREVIOUS_EMPLOYER_SALARY_DETAILS ];

	public static $c_arrintItPolicyCategories = [
		'entrata_vpn_policy'			=> self::ID_VPN,
		'hardware_allocation_form'		=> self::ID_HARDWARE_ALLOCATION,
		'xento_remote_wfm_agreement'	=> self::ID_REMOTE_WFH
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeDocumentTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeDocumentCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsDocumentRequired() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsVisibleToEmployee() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileExtensionIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>