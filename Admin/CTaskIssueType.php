<?php

class CTaskIssueType extends CBaseTaskIssueType {

	const NAVIGATION			= 17;
	const TRAINING				= 18;
	const NEW_FUNCTIONALITY		= 19;
	const SETTING				= 20;
	const THIRD_PARTY			= 21;
	const USER_ERROR			= 22;

	public static $c_arrintDesignIssueType = array(
		self::NAVIGATION,
		self::TRAINING,
		self::USER_ERROR
	);

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>