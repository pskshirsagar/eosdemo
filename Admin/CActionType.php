<?php

class CActionType extends CBaseActionType {

	// Published Types (Most Important Sales Stats)
	const NOTE							= 1;
	const CALL							= 2;
	const EMAIL							= 3;
	const DEMO							= 4;
	const FOLLOW_UP						= 5;
	const MEETING						= 7;
	const TAG							= 8;

	// System Types (To Track System Activity)
	const SUPPORT_PEEP_EMAIL			= 51;
	const SALES_PEEP_EMAIL				= 52;
	const MASS_EMAIL					= 53;
	const ENTRATA_AD					= 54;
	const PS_DOT_COM_VISIT				= 55;
	const NEWSLETTER					= 56;
	const DIRECT_MAIL					= 57;
	const SUCCESS_REVIEW				= 58;
	const ADOPTION						= 59;
	const NEW_LOGO_CLIENT_WELCOME_EMAIL	= 212;

	// Contract Change Log (For history / activity tab)
    const OPPORTUNITY_ADDED				= 101; // It should be "NEW_OPPORTUNITY" as per it's name.
	const ON_HOLD						= 102;
    const CONTACTED						= 103;
    const QUALIFIED						= 104;
    const DISCOVERY						= 105;
    const DEMOED						= 106;
    const PROPOSAL_SENT					= 107;
    const CONTRACT_NEGOTIATION			= 108;
    const CONTRACT_SENT					= 109;
    const CONTRACT_APPROVED				= 110;
    const CONTRACT_TERMINATED			= 111;
    const LOST							= 112;
    const CANCELLED						= 112; // @deprecated - Use "LOST" instead.

	const CONTRACT_BILLING_PAUSED		= 210;
	const COMPANY_CHARGES_POSTED		= 211;

	// Types of Updates Performed On A Contract (Mainly for audit purposes)
	const SALES_REP_CHANGES				= 201;
	const PROPERTY_COUNT				= 202;
	const UNIT_COUNT					= 203;
	const PRODUCT_ADDED					= 204;
	const PRICE_CHANGE					= 205;
	const NOT_INTERESTED				= 206;
	const PRODUCT_REMOVED				= 207;

	// Employee Types
	const EMPLOYEE						= 208;

	// Employee Application
	const REJECTED						= 222;
	const DECLINED						= 235;

	// Task Types
	const REVIEWED						= 209;
	const TASK_REOPEN					= 250;

	// Company risk type
	const RISK_STATUS					= 213;
	const SUPPORT_FLAG					= 214;

	// Reimbursements
	const REIMBURSEMENT_REQUEST			= 215;
	const REIMBURSEMENT_EXPENSE			= 216;

	// Reconciliations
	const RECONCILIATION_REQUEST		= 219;
	const RECONCILIATION_EXPENSE		= 220;
	const RECONCILIATION_BATCH			= 221;

	// Renewal Statuses
	const RENEWAL_STATUS				= 217;
	const RENEWAL_WAIVE_STATUS			= 218;

	// Release Notes
	const RELEASE_NOTES					= 223;

	// Delinquency
	const DELINQUENCY					= 224;
	const EMPLOYEE_HOUR_CHANGE			= 225;

	// Setting Profiles
	const SETTING_PROFILES				= 226;

	// Billing Request
	const BILLING_REQUEST_NEW							= 227;
	const BILLING_REQUEST_APPROVED						= 228;
	const BILLING_REQUEST_REJECTED						= 229;
	const BILLING_REQUEST_DOCUMENT_APPROVED				= 230;
	const BILLING_REQUEST_DOCUMENT_SIGNED				= 231;
	const BILLING_REQUEST_DOCUMENT_COUNTERSIGNED		= 232;
	const BILLING_REQUEST_NOTE							= 233;

	// COMPANY PAYMENT FAILURE
	const COMPANY_PAYMENT_FAILURE						= 241;

	const CONTRACT_DRAFT_NOTE							= 242;

	const CLIENT_ADMIN_REPORTS							= 234;

	// sales activity types to track on a lead/client
	const OFFICE_VISIT	= 236;
	const MAILER		= 237;
	const SOCIAL_MEDIA	= 238;
	const EVENT			= 239;

	// training activity to add records when product training takes place for a client or lead.
	const TRAINING	= 240;

	protected $m_boolIsSelected;

	public static $c_arrintPublishedActionTypeIds = array(
		CActionType::EMAIL,
		CActionType::NOTE,
		CActionType::CALL,
		CActionType::MEETING,
		CActionType::DEMO,
		CActionType::FOLLOW_UP,

		CActionType::SUCCESS_REVIEW,
		CActionType::ADOPTION,
		CActionType::TAG,
		CActionType::OFFICE_VISIT,
		CActionType::MAILER,
		CActionType::SOCIAL_MEDIA,
		CActionType::EVENT,
		CActionType::TRAINING
	);

	public static $c_arrintPrimaryActionTypeIds = array(
		CActionType::EMAIL,
		CActionType::NOTE,
		CActionType::CALL,
		CActionType::MEETING,
		CActionType::DEMO,
		CActionType::FOLLOW_UP,
		CActionType::SUPPORT_PEEP_EMAIL,
		CActionType::SALES_PEEP_EMAIL
	);

	public static $c_arrintPeepActionTypeIds = array(
		CActionType::SUPPORT_PEEP_EMAIL,
		CActionType::SALES_PEEP_EMAIL
	);

	public static $c_arrintStatusChangeActionTypeIds = array(
		self::OPPORTUNITY_ADDED,
		self::ON_HOLD,
		self::CONTACTED,
		self::QUALIFIED,
		self::PROPOSAL_SENT,
		self::CONTRACT_SENT,
		self::CONTRACT_APPROVED,
		self::CONTRACT_TERMINATED,
		self::CANCELLED,
		self::LOST
	);

	public static $c_arrintPipelineEntryActionTypeIds = array(
		self::QUALIFIED,
		self::PROPOSAL_SENT,
		self::CONTRACT_SENT,
		self::CONTRACT_APPROVED,
		self::CONTRACT_TERMINATED
	);

	public static $c_arrintAccountTouchedActionTypeIds = array(
		self::CALL,
		self::EMAIL,
		self::DEMO,
		self::MEETING,
		self::SUCCESS_REVIEW
	);

	// We probably should get rid of this.
	public static $c_arrintNotEditableActionTypeIds = array(
		CActionType::NEWSLETTER,
		CActionType::DIRECT_MAIL,
		CActionType::MASS_EMAIL,
		CActionType::PS_DOT_COM_VISIT,
		CActionType::RISK_STATUS,
		CActionType::SETTING_PROFILES
	);

	// We probably should get rid of this.
	public static $c_arrintFollowUpActionTypeIds = array(
		CActionType::DEMO,
		CActionType::FOLLOW_UP
	);

	public static $c_arrintNoDeleteableActionTypeIds = array(
		CActionType::RISK_STATUS,
		CActionType::SUPPORT_FLAG,
		CActionType::RENEWAL_STATUS,
		CActionType::SETTING_PROFILES
	);

	public static $c_arrintAllowEditActionTypeIds = array(
		CActionType::SUPPORT_FLAG
	);

	public static $c_arrintLastContactDateActionTypeIds = array(
		CActionType::NOTE,
		CActionType::CALL,
		CActionType::EMAIL,
		CActionType::DEMO,
		CActionType::MEETING
	);

	public static $c_arrintLikelihoodSettingsActionTypeIds = array(
		CActionType::CALL,
		CActionType::DEMO,
		CActionType::MEETING,
		CActionType::FOLLOW_UP
	);

	public static $c_arrmixActivityIconsWithActionTypeIds = [
		CActionType::EMAIL			=> 'email',
		CActionType::NOTE			=> 'note',
		CActionType::CALL			=> 'phone-incoming',
		CActionType::MEETING		=> 'people',
		CActionType::DEMO			=> 'desktop',
		CActionType::FOLLOW_UP		=> 'calendar-blank',
		CActionType::SUCCESS_REVIEW => 'pie-chart',
		CActionType::ADOPTION		=> 'chat',
		CActionType::TAG			=> 'price-tag',
		CActionType::OFFICE_VISIT	=> 'apartment',
		CActionType::MAILER			=> 'open-email',
		CActionType::SOCIAL_MEDIA	=> 'thumbs-up',
		CActionType::EVENT			=> 'calendar',
		CActionType::TRAINING		=> 'school'
	];

	public function setIsSelected( $boolIsSelected ) {
		$this->m_boolIsSelected = $boolIsSelected;
	}

	public function getIsSelected() {
		return $this->m_boolIsSelected;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// Default case
				break;
		}

		return $boolIsValid;
	}

}
?>