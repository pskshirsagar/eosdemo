<?php

class CReportGroupSchedule extends CBaseReportGroupSchedule {

	const REPORTS_PER_GROUP = 10;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupScheduleTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valScheduleDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportGroupFrequency( $intGroupScheduleTypeId, $strGroupScheduledDate ) {
		$boolIsValid = true;
		switch( $intGroupScheduleTypeId ) {
			case CGroupScheduleType::WEEKLY_GROUP_SCHEDLUE_TYPE:
				if( false == valStr( $strGroupScheduledDate ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_group_frequency', 'Day of the week is required. ' ) );
					return $boolIsValid;
				}
				break;

			case CGroupScheduleType::MONTHLY_GROUP_SCHEDLUE_TYPE:
				if( false == valStr( $strGroupScheduledDate ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_group_frequency', 'Day of the month is required. ' ) );
					return $boolIsValid;
				}
				break;

			case CGroupScheduleType::YEARLY_GROUP_SCHEDLUE_TYPE:
				if( false == valStr( $strGroupScheduledDate ) ) {
					$boolIsValid     = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_group_frequency', 'Day of the year is required. ' ) );
					return $boolIsValid;
				}
				break;

			default:
				if( false == valStr( $strGroupScheduledDate ) ) {
					$boolIsValid     = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_group_frequency', 'Next schedule is required. ' ) );
					return $boolIsValid;
				}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $intGroupScheduleTypeId, $strGroupScheduledDate ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valReportGroupFrequency( $intGroupScheduleTypeId, $strGroupScheduledDate );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>