<?php

class CSemTransaction extends CBaseSemTransaction {

	public function insert( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$objSemBudget = CSemBudgets::fetchSemBudgetByPropertyIdByCurrentMonth( $this->getPropertyId(), ( string ) date( 'm/01/Y' ), $objDatabase );

		if( true == valObj( $objSemBudget, 'CSemBudget' ) ) {
			$this->setSemBudgetId( $objSemBudget->getId() );
		}

		$boolIsValid = parent::insert( $intUserId, $objDatabase, $boolReturnSqlOnly );

		if( false == is_numeric( $this->getCompanyPaymentId() ) && true == is_numeric( $this->getSemBudgetId() ) ) {
			// We also need to fire a query to reduce the daily_remaining_budget for this property.
			$strSql = 'UPDATE
							sem_budgets
						SET
							daily_remaining_budget = ( daily_remaining_budget - ' . $this->getEstimatedAmount() . '::numeric )
						WHERE
							id = ' . ( int ) $objSemBudget->getId();

			fetchData( $strSql, $objDatabase );
		}

		return $boolIsValid;
	}

	public function fetchClient( $objAdminDatabase ) {
		return CClients::fetchClientById( $this->getCid(), $objAdminDatabase );
	}

	public function fetchProperty( $objAdminDatabase ) {
		return \Psi\Eos\Admin\CProperties::createService()->fetchPropertyByIdByCid( $this->getPropertyId(), $this->getCid(), $objAdminDatabase );
	}

	public function fetchKeyword( $objAdminDatabase ) {
		return CSemKeywords::fetchSemKeywordById( $this->getSemKeywordId(), $objAdminDatabase );
	}

}
?>