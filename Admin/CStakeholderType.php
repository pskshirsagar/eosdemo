<?php

class CStakeholderType extends CBaseStakeholderType {

	const EMPLOYEE_VACATION_REQUEST		= 1;
	const DISABLE_EMPLOYEE				= 2;
	const INTERNAL_TEST_NOTIFICATION	= 4;
	const SPRINT_STAKEHOLDER			= 6;
	const BILLING_REQUEST				= 5;
	const BACKLOG_STAKEHOLDER			= 7;
	const PURCHASE_REQUEST_STAKEHOLDER	= 8;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>