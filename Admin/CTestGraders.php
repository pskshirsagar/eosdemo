<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestGraders
 * Do not add any new functions to this class.
 */

class CTestGraders extends CBaseTestGraders {

	public static function fetchTestGradersEmployeeIdsByTestId( $intTestId, $objDatabase ) {

		$arrintEmployeeIds = fetchData( 'SELECT employee_id FROM test_graders WHERE test_id = ' . ( int ) $intTestId, $objDatabase );

		if( false != isset( $arrintEmployeeIds ) ) {
			foreach( $arrintEmployeeIds as $intKey => $arrintValue ) {
				$arrintEmployeeIds[$intKey] = $arrintValue['employee_id'];
			}
			return $arrintEmployeeIds;
		}
	}

	public static function fetchTestIdsByEmployeeId( $intEmployeeId, $objDatabase ) {

		$arrintTestIds = fetchData( 'SELECT test_id FROM test_graders WHERE employee_id = ' . $intEmployeeId, $objDatabase );

		if( false != isset( $arrintTestIds ) ) {
			foreach( $arrintTestIds as $intKey => $arrintValue ) {
				$arrintTestIds[$intKey] = $arrintValue['test_id'];
			}
			return $arrintTestIds;
		}
	}

	public static function fetchTestGradersNameByTestId( $intTestId, $objDatabase ) {

		$strSql = 'SELECT
						tg.id,
						e.id as employee_id,
						e.email_address,
						e.name_full
					FROM
						test_graders tg
						LEFT JOIN employees e ON e.id = tg.employee_id
					WHERE
						tg.test_id = ' . ( int ) $intTestId . '
					ORDER BY
						e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestGradersEmailIdsByTestId( $intTestId, $objDatabase ) {

		$strSql = 'SELECT
						e.email_address
					FROM
						employees e
					LEFT JOIN test_graders tg ON tg.employee_id = e.id
					WHERE
						tg.test_id =' . $intTestId;

		$arrintEmployeeIds = fetchData( $strSql, $objDatabase );

		if( false != isset( $arrintEmployeeIds ) ) {
			foreach( $arrintEmployeeIds as $intKey => $arrintValue ) {
				$arrintEmployeeIds[$intKey] = $arrintValue['email_address'];
			}
			return $arrintEmployeeIds;
		}
	}
}
?>