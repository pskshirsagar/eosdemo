<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyKeyValues
 * Do not add any new functions to this class.
 */

class CCompanyKeyValues extends CBaseCompanyKeyValues {

	public static function fetchCompanyKeyValueByCidByKey( $intCid, $strKey, $objDatabase ) {
		$strSql = 'SELECT * FROM company_key_values WHERE key = \'' . addslashes( $strKey ) . '\' AND cid = ' . ( int ) $intCid;

		return self::fetchCompanyKeyValue( $strSql, $objDatabase );
	}

	public static function fetchSimpleCompanyKeyValuesByKey( $strKey, $objDatabase ) {
		$strSql = 'SELECT cid, value FROM company_key_values WHERE key = \'' . $strKey . '\'';

		$arrstrCompanyKeyValues = fetchData( $strSql, $objDatabase );
		$arrstrRekeyedCompanyKeyValues = array();

		if( true == valArr( $arrstrCompanyKeyValues ) ) {
			foreach( $arrstrCompanyKeyValues as $arrstrCompanyKeyValue ) {
				$arrstrRekeyedCompanyKeyValues[$arrstrCompanyKeyValue['cid']] = $arrstrCompanyKeyValue['value'];
			}
		}

		return $arrstrRekeyedCompanyKeyValues;
	}

	public static function fetchCompanyKeyValuesByCidByKeys( $intCid, $arrstrKeys, $objDatabase ) {
		$strSql = 'SELECT * FROM company_key_values WHERE cid = ' . ( int ) $intCid . ' AND key IN (\'' . implode( '\',\'', $arrstrKeys ) . '\')';

		$arrobjCompanyKeyValues = self::fetchCompanyKeyValues( $strSql, $objDatabase );
		$arrobjRekeyedCompanyKeyValues = array();

		if( true == valArr( $arrobjCompanyKeyValues ) ) {
			foreach( $arrobjCompanyKeyValues as $objCompanyKeyValue ) {
				$arrobjRekeyedCompanyKeyValues[trim( $objCompanyKeyValue->getKey() )] = $objCompanyKeyValue;
			}
		}

		return $arrobjRekeyedCompanyKeyValues;
	}

	public static function fetchCompanyValueByCidByKey( $intCid, $strKey, $objDatabase ) {

		if( false == valId( $intCid ) ) {
			return;
		}
		$strSql = 'SELECT value FROM company_key_values WHERE cid = ' . ( int ) $intCid . ' AND key = \'' . $strKey . '\'';

		return self::fetchColumn( $strSql, 'value', $objDatabase );
	}

}
?>