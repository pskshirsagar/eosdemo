<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportDetails
 * Do not add any new functions to this class.
 */

class CReportDetails extends CBaseReportDetails {

	public static function fetchReportVersionsByCompanyReportId( $intCaReportId, $objDatabase ) {

		if( false == is_numeric( $intCaReportId ) ) {
			return false;
		}

		$strSql = 'SELECT 
						rd.id,  
						version_number,
						( CASE WHEN rd.is_current_version = true THEN 1
							ELSE 0 END ) AS current_version
						FROM 
							report_details rd
						WHERE 
							rd.company_report_id = ' . ( int ) $intCaReportId . '
						ORDER BY rd.created_on';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReportSqlDetailByReportDetailId( $intReportDetailId, $objDatabase ) {

		if( false == is_numeric( $intReportDetailId ) ) {
			return false;
		}

		$strSql = 'SELECT 
						rsd.query,
						array_to_string(array_agg(DISTINCT( rsdb.database_id ) ),\',\') AS database_ids
						FROM 
							report_details rd
						JOIN report_sql_details rsd ON ( rd.report_sql_detail_id = rsd.id )
						JOIN report_sql_databases rsdb ON ( rsdb.report_sql_detail_id = rsd.id )
						WHERE 
							rd.id = ' . ( int ) $intReportDetailId . '
						GROUP BY 
							rsd.query';

		$arrmixReportDetail = fetchData( $strSql, $objDatabase );
		return $arrmixReportDetail[0];

	}

	public static function fetchReportSpreadsheetByReportDetailId( $intReportDetailId, $objDatabase ) {

		if( false == is_numeric( $intReportDetailId ) ) {
			return false;
		}

		$strSql = 'SELECT 
						rd.id,
						rs.name
						FROM 
							report_details rd
						JOIN report_spreadsheets rs ON ( rd.report_spreadsheet_id = rs.id )
						WHERE 
							rd.id = ' . ( int ) $intReportDetailId;

		$arrmixReportDetail = fetchData( $strSql, $objDatabase );
		return $arrmixReportDetail[0];

	}

	public static function fetchReportDetailsByCompanyReportIdByCurrentVersion( $intCompanyReportId, $objDatabase ) {

		if( false == is_numeric( $intCompanyReportId ) ) {
			return false;
		}

		$strSql = 'SELECT 
						*
						FROM 
							report_details rd
						WHERE 
							rd.is_current_version = TRUE AND rd.company_report_id = ' . ( int ) $intCompanyReportId . '
						ORDER BY rd.created_on';

		return self::fetchReportDetail( $strSql, $objDatabase );
	}

	public static function fetchReportDetailByCompanyReportIdByReportDataTypeId( $intCompanyReportId, $intReportDataTypeId, $objDatabase ) {

		if( false == is_numeric( $intCompanyReportId ) && false == is_numeric( $intReportDataTypeId ) ) {
			return false;
		}
		$strSql = 'SELECT 
						*
						FROM 
							report_details rd
						WHERE 
							rd.company_report_id = ' . ( int ) $intCompanyReportId . '
							AND rd.report_data_type_id = ' . ( int ) $intReportDataTypeId;

		return self::fetchReportDetail( $strSql, $objDatabase );
	}

}
?>