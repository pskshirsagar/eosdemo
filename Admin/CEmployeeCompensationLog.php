<?php

class CEmployeeCompensationLog extends CBaseEmployeeCompensationLog {

	protected $m_strPreviousCompensationEncrypted;
	protected $m_strNewCompensationEncrypted;
	protected $m_strHourlyRateCompensationEncrypted;
	protected $m_strBonusCompensationEncrypted;
	protected $m_strReason;

	protected $m_intPreviousCompensationDecrypted;
	protected $m_intNewCompensationDecrypted;
	protected $m_intBonusCompensationDecrypted;
	protected $m_intHourlyRateCompensationDecrypted;
	protected $m_intBonusTypeId;
	protected $m_fltEmployeeCompensationIncrease;

	/**
	* Other Functions
	*/

	public static function insertEmployeeCompensationLog( $intEmployeeId, $strDateStarted, $objDatabase, $intUserId ) {

		$objEmployeeCompensationLog = new CEmployeeCompensationLog();

		$objEmployeeCompensationLog->setEmployeeId( $intEmployeeId );
		$objEmployeeCompensationLog->setNote( 'New employee added.' );
		$objEmployeeCompensationLog->setRequestedOn( $strDateStarted );
		$objEmployeeCompensationLog->setRequestedBy( $intUserId );

		if( false == $objEmployeeCompensationLog->insert( $intUserId, $objDatabase ) ) {
			return false;
		}
		return true;
	}

	/**
	 * Setter Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['previous_compensation_encrypted'] ) ) $this->setPreviousCompensationEncrypted( $arrmixValues['previous_compensation_encrypted'] );
		if( true == isset( $arrmixValues['new_compensation_encrypted'] ) ) $this->setNewCompensationEncrypted( $arrmixValues['new_compensation_encrypted'] );
		if( true == isset( $arrmixValues['hourly_rate_compensation_encrypted'] ) ) $this->setHourlyRateCompensationEncrypted( $arrmixValues['hourly_rate_compensation_encrypted'] );
		if( true == isset( $arrmixValues['bonus_compensation_encrypted'] ) ) $this->setBonusCompensationEncrypted( $arrmixValues['bonus_compensation_encrypted'] );
		if( true == isset( $arrmixValues['reason'] ) ) $this->setReason( $arrmixValues['reason'] );
		if( true == isset( $arrmixValues['bonus_type_id'] ) ) $this->setBonusTypeId( $arrmixValues['bonus_type_id'] );

	}

	public function setPreviousCompensationEncrypted( $strPreviousCompensationEncrypted ) {
		$this->m_strPreviousCompensationEncrypted = $strPreviousCompensationEncrypted;
	}

	public function setNewCompensationEncrypted( $strNewCompensationEncrypted ) {
		$this->m_strNewCompensationEncrypted = $strNewCompensationEncrypted;
	}

	public function setBonusCompensationEncrypted( $strBonusCompensationEncrypted ) {
		$this->m_strBonusCompensationEncrypted = $strBonusCompensationEncrypted;
	}

	public function setHourlyRateCompensationEncrypted( $strHourlyRateCompensationEncrypted ) {
		$this->m_strHourlyRateCompensationEncrypted = $strHourlyRateCompensationEncrypted;
	}

	public function setPreviousCompensationDecrypted( $intPreviousCompensationDecrypted ) {
		$this->m_intPreviousCompensationDecrypted = $intPreviousCompensationDecrypted;
	}

	public function setNewCompensationDecrypted( $intNewCompensationDecrypted ) {
		$this->m_intNewCompensationDecrypted = $intNewCompensationDecrypted;
	}

	public function setBonusCompensationDecrypted( $intBonusCompensationDecrypted ) {
		$this->m_intBonusCompensationDecrypted = $intBonusCompensationDecrypted;
	}

	public function setHourlyRateCompensationDecrypted( $intHourlyRateCompensationEncrypted ) {
		$this->m_intHourlyRateCompensationDecrypted = $intHourlyRateCompensationEncrypted;
	}

	public function setEmployeeCompensationLogData( $objPurchaseRequest, $objDatabase ) {
		if( true == valObj( $objPurchaseRequest, 'CPurchaseRequest' ) ) {
			$objRequestedByEmployee = CEmployees::fetchEmployeeByUserId( $objPurchaseRequest->getRequestedBy(), $objDatabase );

			if( true == valObj( $objRequestedByEmployee, 'CEmployee' ) ) {
				if( false == is_null( $objPurchaseRequest->getEmployeeId() ) ) {
					$this->setEmployeeId( $objPurchaseRequest->getEmployeeId() );
					$this->setPurchaseRequestId( $objPurchaseRequest->getId() );
					$this->setEstimatedHours( $objPurchaseRequest->getEstimatedHoursNew() );
					$this->setIsVerified( 1 );
					$this->setRequestedBy( $objRequestedByEmployee->getId() );
					$this->setRequestedOn( $objPurchaseRequest->getEffectiveDate() );
					$this->setNote( 'Purchase request approved.' );
				}
			}
		}
	}

	public function setEmployeeCompensationIncrease( $fltEmployeeCompensationIncrease ) {
		$this->m_fltEmployeeCompensationIncrease = $fltEmployeeCompensationIncrease;
	}

	public function setReason( $strReason ) {
		$this->m_strReason = $strReason;
	}

	public function setBonusTypeId( $intBonusTypeId ) {
		$this->m_intBonusTypeId = $intBonusTypeId;
	}

	/**
	 * Getter Functions
	 */

	public function getPreviousCompensationEncrypted() {
		return $this->m_strPreviousCompensationEncrypted;
	}

	public function getNewCompensationEncrypted() {
		return $this->m_strNewCompensationEncrypted;
	}

	public function getBonusCompensationEncrypted() {
		return $this->m_strBonusCompensationEncrypted;
	}

	public function getHourlyRateCompensationEncrypted() {
		return $this->m_strHourlyRateCompensationEncrypted;
	}

	public function getPreviousCompensationDecrypted() {
		return $this->m_intPreviousCompensationDecrypted;
	}

	public function getNewCompensationDecrypted() {
		return $this->m_intNewCompensationDecrypted;
	}

	public function getBonusCompensationDecrypted() {
		return $this->m_intBonusCompensationDecrypted;
	}

	public function getHourlyRateCompensationDecrypted() {
		return $this->m_intHourlyRateCompensationDecrypted;
	}

	public function getEmployeeCompensationIncrease() {
		return $this->m_fltEmployeeCompensationIncrease;
	}

	public function getReason() {
		return $this->m_strReason;
	}

	public function getBonusTypeId() {
		return $this->m_intBonusTypeId;
	}

	public function valNewCompensationAssociationId() {
		$boolIsValid = true;

		if( true == is_null( $this->getNewEmployeeEncryptionAssociationId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'new_employee_encryption_association_id', ' New Employee Encryption Association Id is missing.' ) );
		}

		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strNote ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', ' Note is required.' ) );
		} elseif( 26 > \Psi\CStringService::singleton()->strlen( $this->m_strNote ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'note', ' Note should contain at least 26 characters.' ) );
		}

		return $boolIsValid;
	}

	public function valRequestedOn( $strCountryCode ) {
		$boolIsValid = true;
		if( CCountry::CODE_INDIA == $strCountryCode ) {
			date_default_timezone_set( 'Asia/Calcutta' );
		}
		if( true == empty( $this->m_strRequestedOn ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_date', ' Effective date is required.' ) );
		} elseif( strtotime( date( 'Y/m/d', strtotime( $this->m_strRequestedOn ) ) ) > strtotime( date( 'Y/m/d' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requested_on', ' Effective date should not be greater than current date.' ) );
		} elseif( CCountry::CODE_USA == $strCountryCode && ( false == in_array( date( 'd', strtotime( $this->m_strRequestedOn ) ), array( 1, 16 ) ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'requested_on', ' Effective date day should be 1 or 16 only.' ) );
		}
		date_default_timezone_set( 'America/Denver' );
		return $boolIsValid;
	}

	public function valHoursWorked() {
		$boolIsValid = true;

		if( true == empty( $this->m_intEstimatedHours ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hours_worked', ' Estimated hours required.' ) );
		}

		return $boolIsValid;
	}

	public function valHourlyPayRate() {
		$boolIsValid = true;

		if( true == empty( $this->m_intHourlyRateEmployeeEncryptionAssociationId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hourly_pay_rate', ' Hourly rate is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolValidateHourlyPayRate = false, $strCountryCode = '' ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid = $this->valNote();
				if( true == $boolValidateHourlyPayRate ) {
					$boolIsValid &= $this->valHoursWorked();
					$boolIsValid &= $this->valHourlyPayRate();
				}
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_insert_comp_change':
				$boolIsValid &= $this->valNewCompensationAssociationId();
				$boolIsValid &= $this->valNote();
				$boolIsValid &= $this->valRequestedOn( $strCountryCode );
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>