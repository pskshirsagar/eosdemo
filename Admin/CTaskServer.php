<?php

class CTaskServer extends CBaseTaskServer {

	public function valHardwareId() {
		$boolIsValid = true;
		if( true == is_null( $this->getHardwareId() ) ) {
		  $boolIsValid = false;
		  $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hardware_id', 'hardware_id is required' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valHardwareId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>