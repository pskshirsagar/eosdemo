<?php

use Psi\Libraries\Cryptography\CCrypto;
use Psi\Libraries\UtilHash\CHash;

class CUser extends CBaseUser {

	// System users
	const ID_SYSTEM					= 1;
	const ID_ADMIN					= 2;
	const ID_UNKNOWN                = 3;
	const ID_APPLICATIONS			= 6; // Adding user for CAutoGenerateTieredRenewalQuotesScript
	const ID_CLIENT_ADMIN			= 7;
	const ID_ENTRATA_ID				= 10; // Currently disabled
	const ID_PROSPECT_PORTAL		= 11;
	const ID_RESIDENT_PORTAL		= 12; // Currently disabled
	const ID_SCRIPTS_LAYER			= 16;
	const ID_MIGRATION_IMPORT		= 18;
	const ID_RESIDENT_PORTAL_APP	= 4566;
	const ID_SYSTEM_RENEWAL_USER	= 20;
	const ID_VENDOR_ACCESS			= 21;
	const ID_UEM_IP					= 48;
	const ID_PMS_INTEGRATION		= 61;
	const ID_INSURANCE_PORTAL		= 64; // Currently disabled
	const ID_FIRST_RESPONSE			= 1131;
	const ID_IT_HELP_DESK			= 1819;
	const ID_SYSTEM_MONITOR			= 1826;
	const ID_HELP_DESK				= 2250; // Currently disabled
	const ID_ADMIN_HELP_DESK		= 2038;
	const ID_HR_HELP_DESK			= 2039;
	const ID_ACCOUNT_HELP_DESK		= 4928;
	const ID_RECRUITMENT_HELP_DESK	= 4929;
	const ID_TRAINING_HELP_DESK		= 4930;
	const ID_RP_FEEDBACK			= 6328;
	const ID_CHASE_HARRINGTON		= 492;
	const MAX_SYSTEM_USER_ID        = 99;
	const ID_TENANT_PORTAL          = 4;
	const ID_RESERVATIONHUB         = 5;
	const ID_RPWHITELABELEDAPP      = 9;

	// Other users
	const ID_DAVID_BATEMAN			= 100;
	const ID_JOHN_HANNA				= 102;
	const ID_RYAN_BYRD				= 1473;
	const ID_SANDEEP_GARUD			= 139;
	const ID_PREETAM_YADAV			= 140;
	const ID_YUSUF_POONAWALA		= 141;
	const ID_PRAVEEN_SHETTY			= 143;
	const ID_SANDEEP_CHAVAN			= 147;
	const ID_KURT_RADMALL			= 205;
	const ID_RUJUTA_GADRE			= 227;
	const ID_NETAJI_WAKDE			= 234;
	const ID_DHARMESH_SHROFF		= 252;
	const ID_TAPAN_BHATT			= 336;
	const ID_BRADLEY_CREER			= 449;
	const ID_SHRIRAM_BHANDARI		= 487;
	const ID_PRAVIN_LOHAKARE		= 1330;
	const ID_SHRIKANT_VYAWAHARE		= 3205;
	const ID_SUPRIYA_JADHAV			= 2686;
	const ID_VIKAS_GOTE				= 535;
	const ID_DIPESH_KUREKAR         = 2542;
	const ID_PRITI_CHAUDHRI         = 263;
	const ID_NISHANT_BANNAGARE      = 3329;
	const ID_JOSH_MENDENHALL		= 553;
	const ID_SACHINKUMAR_DURGE		= 558;
	const ID_RAHUL_WAGHMARE			= 399;
	const ID_KEDAR_LANGADE			= 187;
	const ID_VARUN_KAMBLE			= 3871;
	const ID_AMOL_DESHPANDE			= 798;
	const ID_ROHITSING_PARDESHI		= 1903;
	const ID_PUNEET_KATHURIA		= 943;
	const ID_MAHEK_JOSHIPURA		= 702;
	const ID_AMITESHWAR_PRASAD		= 430;
	const ID_SANJAY_SINGH			= 531;
	const ID_NITESH_KUMAR_TIWARI	= 1370;
	const ID_JASON_TIMPSON			= 740;
	const ID_WESTON_BELL			= 7338;
	const ID_RAHUL_NAGPURE			= 191;
	const ID_PRAFULL_JAISWAL		= 971;
	const ID_DEEPAK_KAMLE			= 532;
	const ID_JACOB_JONES			= 301;
	const ID_MELIA_CRANE			= 4300;
	const ID_SASHIKANT_MS			= 1592;
	const ID_SHAUNAK_SONTAKKE		= 1296;
	const ID_SARAH_JESPERSEN		= 1309;
	const ID_MITESH_DHEMAN			= 1236;
	const ID_MATTHEW_HAYNIE			= 2043;
	const ID_SWAPNIL_WAWGE			= 1028;
	const ID_ABHINAV_JOSHI			= 1703;
	const ID_WILLIAM_ROBINS			= 1973;
	const ID_SAMINA_BEGUM			= 1210;
	const ID_STEVE_NGUYEN			= 1697;
	const ID_MITCH_SLOAN			= 2138;
	const ID_GILBERT_THOMAS			= 1804;
	const ID_JEANINE_MILLER			= 981;
	const ID_ANNIE_WOUDEN			= 4792;
	const ID_LOKESH_AGRAWAL			= 271;
	const ID_BRANDON_MCCLOSKEY		= 1422;
	const ID_SHITAL_MASHALKAR		= 1743;
	const ID_PRASHANT_DHAKAD		= 2798;
	const ID_NAYAN_MATHUR			= 1459;
	const ID_DATTATRAY_PHADATARE	= 1301;
	const ID_SONAL_YADAV			= 1644;
	const ID_SOMESH_JADHAV			= 2689;
	const ID_RATNA_GANGULY			= 885;
	const ID_CHANDLER_SMOOT			= 2261;
	const ID_LINDSEY_ABPLANALP		= 1338;
	const ID_JENNY_PADILLA			= 2389;
	const ID_JANCY_VARELA			= 2271;
	const ID_JESSICA_NIELSEN		= 2783;
	const ID_MOHIT_MATHUR			= 1542;
	const ID_KATE_SHARP				= 3966;
	const ID_MEGAN_PARKER			= 4018;
	const ID_SWAPNIL_SAKHARKAR		= 2794;
	const ID_YOGESH_PADEKAR			= 1975;
	const ID_DINESH_VADHRI			= 1478;
	const ID_KELLY_CANEPA			= 2076;
	const ID_GARRET_BYBEE			= 2049;
	const ID_HEATHER_AKE			= 1133;
	const ID_DAVID_MICKELSON		= 2467;
	const ID_CHASE_CAMPBELL			= 2928;
	const ID_BRIAN_BLOOD			= 2069;
	const ID_TAYLOR_KING			= 3103;
	const ID_BRETT_TOBIASSON		= 3920;
	const ID_RUTH_LOWE				= 2095;
	const ID_VINCENT_BRIA			= 3875;
	const ID_AJ_HOMER				= 4043;
	const ID_KRISTINE_COLEMAN		= 3511;
	const ID_SAPNA_PISE				= 2625;
	const ID_VAIBHAV_DEVALE			= 2606;
	const ID_DIVYESHKUMAR_KIDECHA	= 4785;
	const ID_NILESH_TALELE			= 3852;
	const ID_GAYATRI_PEDDI			= 4360;
	const ID_NEETA_NETI				= 1646;
	const ID_ANIKET_PINGALE			= 3534;
	const ID_ANJAN_KUMAR			= 2457;
	const ID_UDAYKUMAR_SHINDE		= 2561;
	const ID_NARESH_KUNDUR			= 3851;
	const ID_DINESH_KUMBARE			= 3415;
	const ID_MAYURI_CHAUDHARI		= 605;
	const ID_SWAPNIL_GHARE			= 2500;
	const ID_JOHNNY_OOT				= 4351;
	const ID_SHASHANK_BHATT			= 1735;
	const ID_MICHELLE_CROOKS		= 2773;
	const ID_PRATIK_GANDHI			= 2409;
	const ID_SALIM_BAHRAINWALA		= 2509;
	const ID_ASHNA_DAS				= 1800;
	const ID_GANESH_CHAVAN			= 2475;
	const ID_RITIL_SONI				= 2387;
	const ID_PRASHANT_MANE			= 2546;
	const ID_SHIVRAJ_PARSHENE		= 424;
	const ID_SAMEER_SATARKE		 	= 3700;
	const ID_RITESH_SINGH			= 2510;
	const ID_ATUL_SINGH				= 5202;
	const ID_MOHAMMED_ARIEF		 	= 4454;
	const ID_RAKESH_KAMBLE			= 2582;
	const ID_AMOL_YADAV				= 3637;
	const ID_PAVAS_SHRIVASTAVA		= 2507;
	const ID_ANKITA_KUNCHHAL		= 5023;
	const ID_MCKAY_JONES			= 3124;
	const ID_MARK_ROMERO			= 3005;
	const ID_KATE_HAMPTON			= 2544;
	const ID_RUSSEL_JENSON			= 1720;
	const ID_SANTOSH_GUPTA			= 3298;
	const ID_ANNAPURNA_DAYMA		= 562;
	const ID_RAJESH_JAIN			= 3212;
	const ID_MATTHEW_WALKER			= 3774;
	const ID_HETIKA_MAHAJAN			= 2494;
	const ID_SAGAR_DALVI			= 4856;
	const ID_GANESH_MADANE			= 3337;
	const ID_SWAPNIL_MURKUTE		= 3446;
	const ID_RASHMI_BHUTADA			= 4865;
	const ID_BEN_BLAKER				= 2492;
	const ID_TYSON_MARKS			= 6367;
	const ID_ROHIT_SHUKLA			= 765;
	const ID_KEVIN_LAMBERT			= 385;
	const ID_GANESH_BHISE			= 1782;
	const ID_YOGESHWARI_KAPARE		= 3501;
	const ID_DAN_SAMPSON			= 4709;
	const ID_PRATEEK_MESHRAM		= 790;
	const ID_ASHISH_NAYYAR			= 5890;
	const ID_MATTHEW_BARNES			= 5622;
	const ID_ROBERT_WEATHERS		= 1807;
	const ID_JAMIE_SPRABERRY		= 2105;
	const ID_PRANAY_HEMBADE			= 3178;
	const ID_PAUL_DUNFORD			= 2090;
	const ID_RASIKA_THORAT			= 5275;
	const ID_BHAVANA_DHADGE			= 6841;
	const ID_BANDOPANT_NAGALE		= 2797;
	const ID_ALTAMASH_HEROLI		= 2856;
	const ID_BLAKE_BAIRD			= 2073;
	const ID_SUNNY_TEJWANI			= 5551;
	const ID_DARRIN_PERRY			= 109;
	const ID_SNEHAL_MAHAJAN			= 6227;
	const ID_NICK_HINTZE			= 6701;
	const ID_BRANDON_SCHANK			= 2412;
	const ID_SANTOSH_HARNEKAR		= 1719;
	const ID_ASHLEE_COWLEY          = 2256;
	const ID_RAKESH_PATIL			= 4420;
	const ID_NIKHIL_PATHAK			= 5700;
	const ID_NAGESH_KADAM           = 1297;
	const ID_COOPER_CLEMENTS        = 4096;
	const ID_THOMAS_HOLSTON			= 1382;
	const ID_MAC_SIMS				= 2565;
	const ID_TRENTON_WHITNEY        = 5835;
	const ID_STEVEN_PYPER           = 6773;
	const ID_KATIE_SEAMONS          = 6942;
	const ID_NATHAN_LAWYER          = 7064;
	const ID_ANIL_DHOBALE			= 4940;
	const ID_NAGNATH_MORE			= 5073;
	const ID_SACHIN_DHANAWADE		= 5876;
	const ID_BRETT_WEBER		    = 181;
	const ID_ALYSHA_LOW				= 5715;
	const ID_TANYA_GARCIA			= 2787;
	const ID_CHRISTOPHER_MARTIN		= 833;
	const ID_JACKSON_DEBBARMA		= 7220;
	const ID_GIRISH_KUSHWAHA		= 7410;
	const ID_OLA_TEOFILO			= 3198;
	const ID_LEANNE_VICTORY			= 3793;
	const ID_TIANA_KIMURA			= 4555;
	const ID_MANDAR_JOSHI			= 7659;
	const ID_GERRY_CROOKS			= 7629;
	const ID_HEATHER_TOMLINSON		= 2824;
	const ID_TYLER_DURRANS			= 6744;
	const ID_JESSE_WAYMAN			= 2872;
	const ID_NICHOLAS_MCBETH		= 3798;
	const ID_MATTHEW_WASSON			= 7425;
	const ID_JAKE_NICHOLS			= 7628;
	const ID_BRIGHAM_SHIPLEY		= 6053;
	const ID_CONNER_DALTON			= 6739;
	const ID_TRAGEN_HERRICK			= 7065;
	const ID_OLIVER_MORGAN			= 6691;
	const ID_ONICA_HANBY			= 5363;
	const ID_MEGAN_HALL				= 2007;
	const ID_BEN_FROHLICH			= 2989;
	const ID_MICHELLE_MARONEY		= 3249;
	const ID_BEN_MARTIN				= 2310;
	const ID_ASHLEY_CRANOR			= 3232;
	const ID_JESSICA_PIERCE			= 2576;
	const ID_BRENT_PAULSEN			= 4389;
	const ID_WHITNEY_LAMBERT		= 5407;
	const ID_LARA_NELSON			= 5473;
	const ID_BRYCE_SMALLEY			= 5795;
	const ID_BRANEN_STARTUP			= 6848;
	const ID_PRESTON_LYMAN			= 6466;
	const ID_BRYCE_NELSON			= 473;
	const ID_LEVI_WILLIAMS			= 5475;
	const ID_ADAM_PROVANCE			= 6461;
	const ID_REESE_HANSEN			= 6715;
	const ID_STEVEN_CARTER			= 7427;
	const ID_LAURA_CHILD			= 7463;
	const ID_JEFF_ROWELL			= 2602;
	const ID_EVAN_JOHNSON			= 4020;
	const ID_ROQUE_ALLEN			= 4093;
	const ID_JAMES_HANBY			= 5043;
	const ID_GREG_PHILLIPS			= 5119;
	const ID_ZANE_RIGBY				= 6250;
	const ID_VANESSA_LINEGAR		= 6876;
	const ID_SYDNEY_LOWE			= 5652;
	const ID_MADDIE_WHATCOTT		= 7443;
	const ID_WILLIAM_DOHERTY		= 7677;
	const ID_BREEZY_BOSEMAN			= 1700;
	const ID_TAWNI_ACHIMON			= 7658;
	const ID_COOPER_SAFFELL			= 7332;
	const ID_FRANKLIN_WORKMAN		= 5367;
	const ID_CRAIG_PACKARD			= 7389;
	const ID_KAYLIE_SMART			= 7788;
	const ID_CURTIS_JONES			= 6475;
	const ID_JARED_HIRSCHI			= 3451;
	const ID_JONATHAN_NOTRTHRUP		= 3541;
	const ID_MICHAEL_ALISA			= 6602;
	const ID_PRATHAMESH_AUSEKAR		= 5088;
	const ID_ASHLEY_BIRD			= 1093;
	const ID_ELIZEBETH_ERTMER		= 5296;
	const ID_JENNIFER_GREENACRE		= 7292;
	const ID_JONATHAN_PASSEY		= 559;
	const ID_KENNETH_JACKSON		= 7522;
	const ID_MEGAN_STEWART			= 7004;
	const ID_PAUL_MICHAEL			= 5196;
	const ID_SERGIO_BLANCO          = 7710;
	const ID_ISAAC_CARLISLE         = 7387;
	const ID_SAM_POND               = 7114;
	const ID_TRENA_KING             = 197;

	const ID_PUSHPENDER_KUMAR		= 1787;
	const ID_SHODHAN_SHETTY			= 1523;
	const ID_ASHUTOSH_MALVIYA		= 5508;
	const ID_JIGNESH_PATEL          = 2347;
	const ID_GAYATRI_GHOLE			= 5770;
	const ID_SACHIN_DHANAWDE        = 5876;
	const ID_DIVYESH_KIDECHA        = 4785;
	const ID_BIPIN_THOMAS			= 5553;
	const ID_AISHWARY_TIWARI		= 6458;
	const ID_TEST_AUTOMATION		= 7289;
	const ID_SAMUEL_HARPER			= 8195;
	const ID_JASON_HAMMOND			= 8128;
	const ID_KASSIDY_MORTIMER		= 3660;
	const ID_CALLIN_BAKER			= 5861;
	const ID_JORDAN_FRATTO			= 2990;
	const ID_POOJA_KOTHAWADE		= 7555;
	const ID_PRASAD_BHANARKAR		= 4152;
	const ID_YOGESH_KUMAR_TIWARI	= 4031;
	const ID_BISMILLA_SANADE		= 8532;
	const ID_ROHIT_INGULAKAR		= 5511;
	const ID_POOJA_GODSE			= 7011;
	const ID_ASHWINI_PATIL			= 7452;
	const ID_VARUN_PATIL			= 8233;
	const ID_LAXMAN_WAGHMARE		= 1024;
	const ID_NILESH_PAWAR			= 1610;
	const ID_LALITESH_SONAWANE		= 3680;
	const ID_VISHAL_NALAWADE		= 2673;
	const ID_BHAVANA_PATIL			= 7449;
	const ID_KATIE_BRANZ            = 205946;
	const ID_ADAM_COWAN				= 4859;
	const ID_HARSHAD_KATWATE		= 1892;
	const ID_VIKAS_GORE				= 6910;
	const ID_ANUJ_ADKINE			= 6927;
	const ID_DHANANJAY_NAIKWADI		= 7499;
	const ID_TEJAS_MOKASHI			= 6996;
	const ID_SHIVANI_JADHAV			= 5479;
	const ID_JAD_STARTIN			= 289;
	const ID_ALEXANDER_SESSIONS		= 7707;

	// Added for EEOC Details report data access.
	const ID_HEATHER_GAGON			= 1356;
	const ID_ANGELA_KAVANAUGH		= 1387;
	const ID_JENNIFER_HALL			= 901;
	const ID_JUSTIN_COOLE			= 3692;
	const ID_KIMBERLY_MOIR          = 4382;
	const ID_BRANDON_FISH           = 2024;

	// Added for permission to Ancillary Revenue property settings.
	const ID_PRASHANT_MALI			= 956;

	// This users are added for New Sale Email
	const ID_MICHAEL_HEYN			= 1355;
	const ID_MICHAEL_SWAN			= 694;
	const ID_KEVIN_CARPENTER		= 2874;
	const ID_ERIC_BJORNN			= 1487;
	const ID_TYLER_CARTER			= 2346;
	const ID_DBA					= 2880;
	const ID_BILLING_BILLING		= 2004;
	const ID_LAUREN_GIFFORD			= 1239;
	const ID_JERRY_BROOK		 	= 2290;
	const ID_SHAUN_MERRITT		 	= 3698;
	const ID_MATT_FRANDSEN			= 2439;
	const ID_DAN_MANION				= 2433;
	const ID_SARAH_TAFFE			= 1654;
	const ID_PECK_TERRANCE			= 802;
	const ID_LINDA_JONES			= 3697;
	const ID_DEREK_SLOAN			= 5921;
	const ID_JOSHUA_RENBERG			= 2873;
	const ID_ERICA_LOWDER			= 4230;

	// This user added for Annual pay review for india side.
	const ID_CHRISTINA_FERNANDES	= 1348;

	// Giving Access to Payroll reports and Employees modules for Account department.
	const ID_RUHINA_SHAIKH			= 2521;
	const ID_AMIT_SHELAR			= 2361;
	const ID_SANJAY_BODAKE			= 747;

	// To show preetam in review cycle. currently preetam shown under christina.
	const REPORTING_MANAGER_USER_ID_PREETAM      = self::ID_CHRISTINA_FERNANDES;

	// Access to implementation portal setting button and Settings Templates on/off button.
	const ID_KAIDEN_REDD			= 1890;
	const ID_CHRIS_HEUER			= 2607;
	const ID_SWAPNIL_BHASE			= 1737;
	const ID_STEPHEN_PORTER			= 897;
	const ID_JOSHUA_HARDMAN			= 2603;

	// Access to Settings Templates on/off button.
	const ID_JIM_HENINGER			= 730;
	const ID_BENJAMIN_BOSTER		= 625;

	// Added for having default user for NPS System
	const ID_BRANDON_BREIVIK		= 2464;

	// Added for QA Team Output Dashboard Administration
	const ID_JACOB_WHITTAKER		= 3452;

	// Giving Access to ILS API Sandbox & Leasing Center Reviews test tab
	const ID_SANTOSH_KHATTE			= 997;
	const ID_SHAM_SHRIWASTAV 		= 950;
	const ID_SACHIN_BARSE 			= 1911;
	const ID_KYLE_BROGDON 			= 965;
	const ID_ASHIT_VERMA			= 1560;
	const ID_ABHISHEK_JAMWADIKAR 	= 5100;
	const ID_SACHIN_SHIRSEKAR 		= 5641;
	const ID_ROHIT_PATIL			= 6209;
	const ID_AKSHAY_RAVTOLE			= 5827;
	const ID_BHAVESH_KHANDELWAL		= 5607;
	const ID_MELISSA_LECATES		= 3860;
	const ID_ILS_AUTOMATION         = 91970;
	const ID_VANDANA_SWAMI          = 6526;
	const ID_DEEPAK_BONGE           = 6825;
	const ID_POOJA_PATIL			= 7180;
	const ID_AUTOMATION_RAPID		= 225101;
	const ID_AUTOMATION_STANDARD	= 202090;

	// Added to provide Audit Tab Access
	const ID_CANDEN_SCHOW           = 2392;
	const ID_BRYSON_SICOTTE         = 3507;
	const ID_SEAN_SMITH             = 2753;
	const ID_SHAUN_PROVOST          = 3675;
	const ID_STEVEN_GROSS           = 4474;
	const ID_ADAMS_DODD             = 4800;
	const ID_PACE_LOMBARDI          = 4920;
	const ID_JORDAN_LEAVITT         = 4990;
	const ID_BRYCE_MACCABE          = 5294;
	const ID_MEGAN_MCNEIL           = 5448;
	const ID_CHAD_BURTON            = 6776;
	const ID_SHERIDAN_THOMPSON      = 5205;
	const ID_ALEC_CURTIS            = 6612;
	const ID_PRESTON_BROWN          = 4923;
	const ID_MAILE_TUTTLE           = 6639;
	const ID_CHELSEA_BURGOYNE       = 6738;
	const ID_RAHUL_IPPAR            = 3806;
	const ID_CAROL_DMELLO           = 6778;
	const ID_RAAVI_SUSMITHA         = 4863;
	const ID_MIKE_TOLMAN            = 7447;

	// Added for Performance Matrix
	const ID_DBA_HELPDESK = 3297;

	// Added for New Relic alerts task script
	const ID_SAGAR_ALURKAR  = 726;

	// Added for devops dba support tasks script
	const ID_ASHUTOSH_DURUGKAR = 982;

	// Disable access to receive emails of deleted help content pdf.
	const ID_JORDAN_BRERETON = 4019;

	const TOTAL_LOGIN_ATTEMPTS			= 5;
	const LOGIN_ATTEMPT_WAITING_TIME	= 1800;	// 30 minutes
	const LOGIN_ATTEMPT_PERIOD			= 300;	// 5 minutes
	const PASSWORD_ROTATION_DAYS		= 365;
	const PASSWORD_ROTATION_NUMBER		= 4;
	const TEMPORARY_PASSWORD_ENCRYPTED	= 'Ht/CspXtHc0Tu5ZxttiiOCn6yGSMsFBXnWp7LRBuKZI=';

	const MONTH_JAN						= '01';
	const MONTH_JULY					= '07';
	const MONTH_DEC						= '12';
	const JOINING_DATE					= '02/06/2014';
	const EXTERNAL_TOOL_DATE			= '2016-11-16';
	const EXTERNAL_TOOL_LOCK_DAYS		= 15;

	// Added for defect system
	const ID_PANKAJ_DYANDYAN			= 4041;
	const ID_SHANKAR_SHARMA				= 3493;
	const ID_SUBHASH_SODNAR				= 1574;
	const ID_SACHIN_DAMRE				= 2719;
	const ID_SWATI_RAJURE				= 4046;
	const ID_SIDDHANT_GAIKWAD			= 4686;
	const ID_PRADNYA_NAIK				= 4935;
	const USER_ID_BRANDON_MCCLOSKEY		= 1495;

	// Added for charge integration setting
	const ID_MAHENDRA_MUKKAWAR			= 1534;
	const ID_SUMIT_PATIL				= 3044;
	const ID_PANKAJ_SHARMA				= 2153;

	// Added for Mass email systems
	const ID_GANESH_PASARKAR			= 1202;
	const ID_PANDURANG_AGJAL			= 5019;

	// Script User
	const ID_SCRIPT_LAYER				= 16;

	// Added for indian reimbursement project
	const ID_BASWESHWAR_AMLAPURE		= 3027;

	// Added for stakeholders in created task after termination request created
	const ID_CHANTE_KING				= 736;
	const ID_BREANN_LEHR				= 960;
	const ID_JARED_T_HALL				= 4081;
	const ID_TEGAN_PERRY				= 5449;
	const ID_LAURA_PARKER				= 1983;
	const ID_BLAIKE_BAIRD				= 2073;
	const ID_DIOGO_ORDACOWSKI			= 2428;
	const ID_VERONICA_ROMNEY			= 2000;
	const ID_BENTRATA_BROWN				= 3509;
	const ID_TREVOR_RILEY				= 557;
	const ID_ALEX_BULLOCK				= 3659;
	const ID_TODD_PARKER				= 1702;
	const ID_LAUREN_SMITH				= 2754;
	const ID_BRITTEN_ROE				= 2472;
	const ID_JESSICA_CARDONA			= 2939;
	const ID_CHERYL_GALI				= 1055;
	const ID_ETHAN_BAWDEN				= 6205;
	const ID_DEREK_HORNBERGER			= 2048;
	const ID_JERON_MOLANO				= 3241;
	const ID_DANIEL_BULLOCK				= 762;
	const ID_BROCK_FAUBUS				= 2523;
    const ID_ELIJAH_MOLLER				= 6529;
	const ID_NAMDEO_RAJMANE				= 4109;

    const ID_SUDHAKAR_SEERAPU			= 4922;
    // Added For FrontEnd Certificaion Graing Access
	const ID_GAYTRI_GOLE                = 267;
	const ID_MAHESH_BHAND               = 4044;
	const ID_JAGDISH_JAGDALE            = 329;
	const ID_GEKU_MANIYARA              = 1814;
	const ID_SOMNATH_GHADGE             = 2533;
	const ID_PAYAL_BOTHARA              = 6618;
	const ID_WILL_ROBERTSHAW            = 4907;
	const ID_NICOLA_MOLLER              = 6204;
	const ID_ROHAN_VYAS     		    = 743;
	const ID_ABIN_THOMAS		        = 328;
	const ID_HEIDI_CHRISTENSEN		    = 7168;


	const ID_ASSOCIATE_ACCOUNT_MANAGEMENT		= 705;

	// Added for Folders Files Owner Association module
	const ID_CARL_LAWSON	= 3407;
	const ID_DIPIKA_GAIKWAD = 1335;

	const ID_SAM_THOMAS		= 4273;

	// Added for Leasing Center Support Queue
	const ID_LC_SUPPORT_QUEUE = 6697;

	// Added for send to incomplete invoices escalation
	const ID_ASHWINI_CHOUGULE = 3009;

	const IP_UEM_USERNAME = 'Invoice Processing / UEM';

	const ID_BHUSHAN_SHIVLE		= 4595;
	const ID_KAVERI_KHULE		= 6726;
	const ID_SHARAD_KARIMUNGI	= 6617;
	const ID_SHRIYASH_DINDORE	= 6901;
	const ID_YOGESH_SURYAWANSHI	= 6627;
	const ID_AVINASH_SATKAR		= 3271;
	const ID_PALLAVI_TELI		= 5382;
	const ID_VARSHARANI_JADHAV	= 6643;
	const ID_PAYAL_CHORDIYA		= 6976;
	const ID_SHRIKANT_THAKUR	= 7012;
	const ID_PRIYANKA_UTHALE	= 7014;
	const ID_GAURAV_ALGUR		= 7500;
	const ID_CORY_NEMELKA       = 1483;
	const ID_ERIC_RIOS          = 3201;
	const ID_TAUTOMATION        = 7289;

	const ID_DAN_WORWOOD		= 7331;

	protected $m_objEmployee;

	public static $c_arrintMigrationAccessUserIds = [
		self::ID_SHRIRAM_BHANDARI,
		self::ID_MAC_SIMS,
		self::ID_DHARMESH_SHROFF,
		self::ID_SHRIKANT_VYAWAHARE,
		self::ID_CHRISTOPHER_MARTIN
	];

	public static $c_arrintFrontendCertificationUserIds = [
		self::ID_GAYTRI_GOLE,
		self::ID_MAHESH_BHAND,
		self::ID_JAGDISH_JAGDALE,
		self::ID_GEKU_MANIYARA,
		self::ID_SOMNATH_GHADGE,
		self::ID_PAYAL_BOTHARA
	];

	public static $c_arrintMassEmailAccessUserIds = [
		self::ID_GANESH_PASARKAR,
		self::ID_SOMESH_JADHAV,
		self::ID_PANDURANG_AGJAL
	];

	public static $c_arrintIndiaAccountUsers = [
		self::ID_RUHINA_SHAIKH,
		self::ID_AMIT_SHELAR,
		self::ID_SANJAY_BODAKE
	];

	public static $c_arrintHelpdeskUsers	= [
		self::ID_ADMIN_HELP_DESK,
		self::ID_ACCOUNT_HELP_DESK,
		self::ID_TRAINING_HELP_DESK,
		self::ID_RECRUITMENT_HELP_DESK,
		self::ID_HR_HELP_DESK
	];

	protected $m_arrintEmployeeAssessmentsUsers = [
		100 => self::ID_DAVID_BATEMAN,
		102 => self::ID_JOHN_HANNA,
		139 => self::ID_SANDEEP_GARUD,
		140 => self::ID_PREETAM_YADAV,
		205 => self::ID_KURT_RADMALL,
		252 => self::ID_DHARMESH_SHROFF,
		449 => self::ID_BRADLEY_CREER
	];

	public static $c_arrintRemoveFromFailedEmailsAccessUsers = [
		self::ID_GANESH_PASARKAR,
		self::ID_DATTATRAY_PHADATARE,
		self::ID_SOMESH_JADHAV,
		self::ID_LOKESH_AGRAWAL,
		self::ID_BRANDON_MCCLOSKEY,
		self::ID_SHRIRAM_BHANDARI,
		self::ID_SANJAY_SINGH
	];

	// Array of the users who are having access for defect system
	public static $c_arrintDefectUsers = [
		self::ID_BRANDON_BREIVIK,
		self::ID_LOKESH_AGRAWAL,
		self::USER_ID_BRANDON_MCCLOSKEY,
		self::ID_SANJAY_SINGH,
		self::ID_SUBHASH_SODNAR,
		self::ID_SACHIN_DAMRE,
		self::ID_PANKAJ_DYANDYAN,
		self::ID_SHANKAR_SHARMA,
		self::ID_SWATI_RAJURE,
		self::ID_SIDDHANT_GAIKWAD
	];

	public static $c_arrstrViewHelpdeskTypes 	= [
		self::ID_ADMIN_HELP_DESK 		=> 'admin_helpdesk',
		self::ID_ACCOUNT_HELP_DESK		=> 'account_helpdesk',
		self::ID_HR_HELP_DESK			=> 'hr_helpdesk',
		self::ID_TRAINING_HELP_DESK 	=> 'training_helpdesk',
		self::ID_RECRUITMENT_HELP_DESK	=> 'recruitment_helpdesk'
	];

	// Not Allowed user Ids to update delinquency dashboard
	public static $c_arrintNotAllowedUserIdsToUpdateDelinquency	= [
		self::ID_GANESH_PASARKAR,
		self::ID_SOMESH_JADHAV,
		self::ID_SANJAY_SINGH
	];

	// Allowed user Ids to view delinquency dashboard
	public static $c_arrintAllowedUserIdsToViewDelinquency	= [
		self::ID_GANESH_PASARKAR,
		self::ID_SOMESH_JADHAV,
		self::ID_SANJAY_SINGH
	];

	// Users who are having all permissions for folders files owner associations module.
	public static $c_arrintFolderFileOwnerAssociationsUserIds = [
		self::ID_CARL_LAWSON,
		self::ID_DIPIKA_GAIKWAD
	];

	// Users having insurance property sync access
	public static $c_arrintInsurancePropertySyncUserIds = [
		self::ID_DANIEL_BULLOCK,
		self::ID_SWAPNIL_WAWGE,
		self::ID_SANTOSH_GUPTA,
		self::ID_COOPER_CLEMENTS,
		self::ID_WILL_ROBERTSHAW
	];

	// Users having access to re-route to new RI website
	public static $c_arrintWebsiteRerouteAccessUserIds = [
		self::ID_DANIEL_BULLOCK,
		self::ID_SWAPNIL_WAWGE
	];

	public static $c_arrintPsSecureAccess = [
		self::ID_RUSSEL_JENSON,
		self::ID_BEN_BLAKER,
		self::ID_ROHIT_SHUKLA,
		self::ID_KEVIN_LAMBERT,
		self::ID_YOGESHWARI_KAPARE,
		self::ID_GANESH_BHISE
	];

	public static $c_arrintPsPaymentGitAccess = [
		self::ID_ROHIT_SHUKLA,
		self::ID_GANESH_BHISE,
		self::ID_YOGESHWARI_KAPARE,
		self::ID_RUSSEL_JENSON,
		self::ID_BEN_BLAKER,
		self::ID_SANDEEP_GARUD
	];

	public static $c_arrintResidentInsureAccess = [
		self::ID_ABIN_THOMAS,
		self::ID_SWAPNIL_WAWGE,
		self::ID_ROHAN_VYAS,
		self::ID_AMITESHWAR_PRASAD,
		self::ID_YOGESHWARI_KAPARE
	];

	public static $c_arrintEntratamationAccess = [
		self::ID_YOGESHWARI_KAPARE,
		self::ID_ROHIT_SHUKLA,
		self::ID_PRATEEK_MESHRAM,
		self::ID_DAN_SAMPSON,
		self::ID_SANDEEP_GARUD
	];

	public static $c_arrintUsersForBugAllowanceReport = [
		self::ID_SYSTEM,
		self::ID_ADMIN
	];

	public static $c_arrintAllowReDeployUsers = [
		self::ID_ROHIT_SHUKLA
	];

	public static $c_arrintContractDraftsUserIds = [
		self::ID_ROBERT_WEATHERS,
		self::ID_JAMIE_SPRABERRY
	];

	public static $c_arrintAllowGlobalPartialPushDeployment = [
		self::ID_YOGESHWARI_KAPARE,
		self::ID_ROHIT_SHUKLA,
		self::ID_GANESH_BHISE,
		self::ID_SANDEEP_GARUD,
		self::ID_RAHUL_WAGHMARE
	];

	public static $c_arrintReputationAuditTabUserIds = [
		self::ID_KYLE_BROGDON,
		self::ID_MATT_FRANDSEN,
		self::ID_CANDEN_SCHOW,
		self::ID_BRYSON_SICOTTE,
		self::ID_DIOGO_ORDACOWSKI,
		self::ID_JERON_MOLANO,
		self::ID_SEAN_SMITH,
		self::ID_SHAUN_PROVOST,
		self::ID_STEVEN_GROSS,
		self::ID_ADAMS_DODD,
		self::ID_PACE_LOMBARDI,
		self::ID_JORDAN_LEAVITT,
		self::ID_BRYCE_MACCABE,
		self::ID_MEGAN_MCNEIL,
		self::ID_CHAD_BURTON,
		self::ID_SHERIDAN_THOMPSON,
		self::ID_ALEC_CURTIS,
		self::ID_PRESTON_BROWN,
		self::ID_MAILE_TUTTLE,
		self::ID_CHELSEA_BURGOYNE
	];

	public static $c_arrintResidentUtilityUsers = [
		self::ID_DEEPAK_KAMLE,
		self::ID_ABHINAV_JOSHI,
		self::ID_PANKAJ_SHARMA,
		self::ID_RAKESH_PATIL,
		self::ID_BHUSHAN_SHIVLE,
		self::ID_MAHENDRA_MUKKAWAR,
		self::ID_SHARAD_KARIMUNGI,
		self::ID_SHRIYASH_DINDORE,
		self::ID_SUMIT_PATIL,
		self::ID_ASHWINI_CHOUGULE,
		self::ID_AVINASH_SATKAR,
		self::ID_YOGESH_SURYAWANSHI,
		self::ID_NAGESH_KADAM,
		self::ID_PALLAVI_TELI,
		self::ID_VARSHARANI_JADHAV,
		self::ID_PAYAL_CHORDIYA,
		self::ID_SHRIKANT_THAKUR,
		self::ID_PRIYANKA_UTHALE,
		self::ID_GAURAV_ALGUR,
		self::ID_JACOB_JONES
	];

	public static $c_arrintRUModulePermissionUsers = [
		self::ID_DEEPAK_KAMLE,
		self::ID_ABHINAV_JOSHI,
		self::ID_PANKAJ_SHARMA,
		self::ID_MAHENDRA_MUKKAWAR,
		self::ID_SUMIT_PATIL,
		self::ID_NAGESH_KADAM,
		self::ID_JACOB_JONES,
		CUser::ID_ALEXANDER_SESSIONS
	];

	// Data Support users who can delete bills
	public static $c_arrintDeleteBillPermissionUserIds = [
		self::ID_NEETA_NETI,
		self::ID_RAKESH_KAMBLE,
		self::ID_SAPNA_PISE,
		self::ID_AMOL_YADAV,
		self::ID_MANDAR_JOSHI
	];


	public static $c_arrintOtherPermissionedUsersForReportSqlExport = [
		self::ID_JARED_HIRSCHI,
		self::ID_JONATHAN_NOTRTHRUP,
		self::ID_JAKE_NICHOLS,
		self::ID_MICHAEL_ALISA,
		self::ID_SHIVRAJ_PARSHENE,
		self::ID_CAROL_DMELLO,
		self::ID_RAAVI_SUSMITHA,
		self::ID_MIKE_TOLMAN,
		self::ID_BANDOPANT_NAGALE,
		self::ID_HARSHAD_KATWATE,
		self::ID_VIKAS_GORE,
		self::ID_ANUJ_ADKINE,
		self::ID_DHANANJAY_NAIKWADI,
		self::ID_TEJAS_MOKASHI,
		self::ID_SHIVANI_JADHAV,
		self::ID_SAMINA_BEGUM,
		self::ID_ERIC_RIOS,
		self::ID_JAD_STARTIN
	];

	// Users having access to paid verification module
	public static $c_arrintPaidVerificationAccessUserIds = [
		self::ID_DANIEL_BULLOCK,
		self::ID_COOPER_CLEMENTS,
		self::ID_PUSHPENDER_KUMAR
	];

	public static $c_arrintMigrationNewSimAccessUserIds = [
		self::ID_SHRIRAM_BHANDARI,
		self::ID_PRAVIN_LOHAKARE,
		self::ID_PRANAY_HEMBADE,
		self::ID_SAGAR_DALVI,
		self::ID_POOJA_KOTHAWADE,
		self::ID_SHRIKANT_VYAWAHARE,
		self::ID_PRASAD_BHANARKAR,
		self::ID_YOGESH_KUMAR_TIWARI,
		self::ID_BISMILLA_SANADE,
		self::ID_SUPRIYA_JADHAV,
		self::ID_ROHIT_INGULAKAR,
		self::ID_POOJA_GODSE,
		self::ID_ASHWINI_PATIL,
		self::ID_VARUN_PATIL,
		self::ID_LAXMAN_WAGHMARE,
		self::ID_DIPESH_KUREKAR,
		self::ID_BHAVANA_DHADGE,
		self::ID_RASIKA_THORAT,
		self::ID_NILESH_PAWAR,
		self::ID_GANESH_MADANE,
		self::ID_LALITESH_SONAWANE,
		self::ID_VISHAL_NALAWADE,
		self::ID_BHAVANA_PATIL,
		self::ID_PAUL_DUNFORD,
		self::ID_ASHLEE_COWLEY,
		self::ID_JERRY_BROOK,
		self::ID_KASSIDY_MORTIMER,
		self::ID_CALLIN_BAKER,
		self::ID_JORDAN_FRATTO,
		self::ID_STEPHEN_PORTER,
		self::ID_KATIE_BRANZ,
		self::ID_NEETA_NETI,
		self::ID_RITIL_SONI,
		self::ID_PRASHANT_MANE,
		self::ID_ANIKET_PINGALE,
		self::ID_SALIM_BAHRAINWALA,
		self::ID_GANESH_CHAVAN,
		self::ID_ADAM_COWAN
	];

	protected $m_arrobjUserGroups;
	protected $m_arrobjUserRoles;
	protected $m_arrobjUserPermissions;
	protected $m_arrobjTasks;

	protected $m_arrintAssociatedRoleIds;
	protected $m_arrintAssociatedGroupIds;

	protected $m_objUserAuthenticationLog;

	protected $m_strOldPassword;
	protected $m_strOldUsername;
	protected $m_strNewPassword;
	protected $m_strUnencryptedPassword;
	protected $m_strPassword;
	protected $m_strPasswordConfirm;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPreferredName;
	protected $m_strNameFull;
	protected $m_strEmailAddress;
	protected $m_strDnsHandle;
	protected $m_strCountryCode;
	protected $m_strDepartmentName;
	protected $m_strDesignationName;
	protected $m_strSecondPassword;
	protected $m_strWordPressUsername;

	protected $m_intManagerId;
	protected $m_intEnableNewTaskSystem;
	protected $m_intGroupId;
	protected $m_intOfficeId;
	protected $m_intDesignationId;
	protected $m_intWordPressUserId;

	protected $m_boolIsLoggedIn;
	protected $m_boolIsProjectManager;
	protected $m_boolIsActiveDirectory;
	protected $m_boolIsSsoEnabled;
	protected $m_boolIsPasswordRotation;
	protected $m_boolIsAdminUserPasswordUpdate;
	protected $m_boolShowSecurityPolicyNotification;
	protected $m_boolForceSecurityPolicyModule;
	protected $m_boolHidePhoneNumber;
	protected $m_boolShowExternalTools;
	protected $m_boolHardwareAllocationFormSigned;
	protected $m_boolVpnFormSigned;
	protected $m_boolRemoteWfhFormSigned;
	protected $m_boolForceEmployeePolicyModule;
	private $m_boolLdapAuthentication;
	protected $m_boolForceNcndPolicyModule;
	protected $m_boolForceWfhPolicyModule;

	protected $m_intEmployeeId;
	protected $m_intPhoneNumber;
	protected $m_strStatustype;
	protected $m_strEmployeeStatusTypeId;
	protected $m_intMessageOperatorId;
	protected $m_intForceFillOutSurveyId;
	protected $m_intWorkStationIpAddress;
	protected $m_intUserAuthenticationLogId;
	protected $m_intDeskNumber;
	protected $m_intDepartmentId;
	protected $m_strTagName;
	private $m_strLdapOu;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjUserGroups 			= [];
		$this->m_arrintAssociatedRoleIds	= [];
		$this->m_arrintAssociatedGroupIds 	= [];

		$this->m_boolIsLoggedIn 			= false;
		$this->m_boolIsAuthenticated 		= false;
		$this->m_boolIsPasswordRotation 	= false;
		$this->m_boolIsProjectManager		= false;

		return;
	}

	/**
	 * Add Functions
	 *
	 */

	public function addTask( $objTask ) {
		$this->m_arrobjTasks[$objTask->getId()] = $objTask;
	}

	public function addUserGroup( $objUserGroup ) {
		$this->m_arrobjUserGroups[$objUserGroup->getGroupId()] = $objUserGroup;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getDesignationName() {
		return $this->m_strDesignationName;
	}

	public function getNameFull() {
		if( true == empty( $this->m_strNameFull ) ) {
			$this->m_strNameFull = trim( $this->m_strNameFirst . ' ' . $this->m_strNameLast );
		}

		return $this->m_strNameFull;
	}

	public function getManagerId() {
		return $this->m_intManagerId;
	}

	public function getEnableNewTaskSystem() {
		return $this->m_intEnableNewTaskSystem;
	}

	public function getEmployee() {
		return $this->m_objEmployee;
	}

	public function getUserGroups() {
		return $this->m_arrobjUserGroups;
	}

	public function getEmployeeAssessmentsUsers() {
		return $this->m_arrintEmployeeAssessmentsUsers;
	}

	public function getUserRoles() {
		return $this->m_arrobjUserRoles;
	}

	public function getIsLoggedIn() {
		return $this->m_boolIsLoggedIn;
	}

	public function getTasks() {
		return $this->m_arrobjTasks;
	}

	public function getDecryptedSvnPassword() {
		if( false == is_null( $this->getSvnPassword() ) ) {
			return CEncryption::decryptText( trim( $this->getSvnPassword() ), CONFIG_KEY_LOGIN_PASSWORD );
		} else {
			return NULL;
		}
	}

	public function getIsPasswordRotation() {
		return $this->m_boolIsPasswordRotation;
	}

	public function getUserAuthenticationLogId() {
		return $this->m_intUserAuthenticationLogId;
	}

	public function getAssociatedRoleIds() {
		return $this->m_arrintAssociatedRoleIds;
	}

	public function getAssociatedGroupIds() {
		return $this->m_arrintAssociatedGroupIds;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getPhoneNumber() {
		return $this->m_intPhoneNumber;
	}

	public function getMessageOperatorId() {
		return $this->m_intMessageOperatorId;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function getNewPassword() {
		return $this->m_strNewPassword;
	}

	public function getStatusType() {
		return $this->m_strStatustype;
	}

	public function getEmployeeStatusTypeId() {
		return $this->m_strEmployeeStatusTypeId;
	}

	public function getHidePhoneNumber() {
		return $this->m_boolHidePhoneNumber;
	}

	public function getDnsHandle() {
		return $this->m_strDnsHandle;
	}

	public function getWorkStationIpAddress() {
		return $this->m_intWorkStationIpAddress;
	}

	public function getIsProjectManager() {
		return $this->m_boolIsProjectManager;
	}

	public function getTagName() {
		return $this->m_strTagName;
	}

	public function getSecondPassword() {
		return $this->m_strSecondPassword;
	}

	public function getGoogleAuthenticatorSecretKey() {
		return ( false == is_null( $this->m_strUsername ) ) ? \Psi\CStringService::singleton()->strtoupper( str_replace( [ 0, 1, 8, 9 ], [ 'A', 'D', 'Z', 'Q' ], md5( CEncryption::encryptText( $this->m_strUsername, CONFIG_KEY_GOOGLE_AUTHENTICATOR_SECRET ) ) ) ) : NULL;
	}

	public function getGroupId() {
		return $this->m_intGroupId;
	}

	public function getIsActiveDirectory() {
		return $this->m_boolIsActiveDirectory;
	}

	public function getIsSsoEnabled() {
		return $this->m_boolIsSsoEnabled;
	}

	public function getShowSecurityPolicyNotification() {
		return $this->m_boolShowSecurityPolicyNotification;
	}

	public function getForceSecurityPolicyModule() {
		return $this->m_boolForceSecurityPolicyModule;
	}

	public function getForceFillOutSurveyId() {
		return $this->m_intForceFillOutSurveyId;
	}

	public function getDeskNumber() {
		return $this->m_intDeskNumber;
	}

	public function getDepartmentId() {
		return $this->m_intDepartmentId;
	}

	public function getOfficeId() {
		return $this->m_intOfficeId;
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function getIsExternalTools() {
		return $this->m_boolShowExternalTools;
	}

	public function getIsVpnFormSigned() {
		return $this->m_boolVpnFormSigned;
	}

	public function getIsHardwareAllocationFormSigned() {
		return $this->m_boolHardwareAllocationFormSigned;
	}

	public function getIsRemoteWfhFormSigned() {
		return $this->m_boolRemoteWfhFormSigned;
	}

	public function getForceEmployeePolicyModule() {
		return $this->m_boolForceEmployeePolicyModule;
	}

	public function getLdapOu() {
		return $this->m_strLdapOu;
	}

	public function getForceNcndPolicyModule() {
		return $this->m_boolForceNcndPolicyModule;
	}

	public function getForceWfhPolicyModule() {
		return $this->m_boolForceWfhPolicyModule;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		/**
		 * We Need this before parent initialisation
		 */
		if( false == $this->m_boolInitialized && true == isset( $arrmixValues['username'] ) ) {
			$this->setOldUsername( $arrmixValues['username'] );
		}

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['old_password'] ) ) {
			$this->setOldPassword( $arrmixValues['old_password'] );
		}
		if( true == isset( $arrmixValues['new_password'] ) ) {
			$this->setNewPassword( $arrmixValues['new_password'] );
		}
		if( true == isset( $arrmixValues['unencrypted_password'] ) ) {
			$this->setUnencryptedPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['unencrypted_password'] ) : $arrmixValues['unencrypted_password'] );
		}
		if( true == isset( $arrmixValues['password'] ) ) {
			$this->setPassword( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['password'] ) : $arrmixValues['password'] );
		}
		if( true == isset( $arrmixValues['password_confirm'] ) ) {
			$this->setPasswordConfirm( $arrmixValues['password_confirm'] );
		}
		if( true == isset( $arrmixValues['name_first'] ) ) {
			$this->setNameFirst( $arrmixValues['name_first'] );
		}
		if( true == isset( $arrmixValues['name_last'] ) ) {
			$this->setNameLast( $arrmixValues['name_last'] );
		}
		if( true == isset( $arrmixValues['preferred_name'] ) ) {
			$this->setPreferredName( $arrmixValues['preferred_name'] );
		}
		if( true == isset( $arrmixValues['department_name'] ) ) {
			$this->setDepartmentName( $arrmixValues['department_name'] );
		}
		if( true == isset( $arrmixValues['designation_name'] ) ) {
			$this->setDesignationName( $arrmixValues['designation_name'] );
		}
		if( true == isset( $arrmixValues['manager_id'] ) ) {
			$this->setManagerId( $arrmixValues['manager_id'] );
		}
		if( true == isset( $arrmixValues['enable_new_task_system'] ) ) {
			$this->setEnableNewTaskSystem( $arrmixValues['enable_new_task_system'] );
		}
		if( true == isset( $arrmixValues['employee_id'] ) ) {
			$this->setEmployeeId( $arrmixValues['employee_id'] );
		}
		if( true == isset( $arrmixValues['phone_number'] ) ) {
			$this->setPhoneNumber( $arrmixValues['phone_number'] );
		}
		if( true == isset( $arrmixValues['email_address'] ) ) {
			$this->setEmailAddress( $arrmixValues['email_address'] );
		}
		if( true == isset( $arrmixValues['status_types'] ) ) {
			$this->setStatusType( $arrmixValues['status_types'] );
		}
		if( true == isset( $arrmixValues['employee_status_type_id'] ) ) {
			$this->setEmployeeStatusTypeId( $arrmixValues['employee_status_type_id'] );
		}
		if( true == isset( $arrmixValues['hide_phone_number'] ) ) {
			$this->setHidePhoneNumber( $arrmixValues['hide_phone_number'] );
		}
		if( true == isset( $arrmixValues['dns_handle'] ) ) {
			$this->setDnsHandle( $arrmixValues['dns_handle'] );
		}
		if( true == isset( $arrmixValues['work_station_ip_address'] ) ) {
			$this->setWorkStationIpAddress( $arrmixValues['work_station_ip_address'] );
		}
		if( true == isset( $arrmixValues['message_operator_id'] ) ) {
			$this->setMessageOperatorId( $arrmixValues['message_operator_id'] );
		}
		if( true == isset( $arrmixValues['country_code'] ) ) {
			$this->setCountryCode( $arrmixValues['country_code'] );
		}
		if( true == isset( $arrmixValues['is_project_manager'] ) ) {
			$this->setIsProjectManager( $arrmixValues['is_project_manager'] );
		}
		if( true == isset( $arrmixValues['tag_name'] ) ) {
			$this->setTagName( $arrmixValues['tag_name'] );
		}
		if( true == isset( $arrmixValues['second_password'] ) ) {
			$this->setSecondPassword( $arrmixValues['second_password'] );
		}
		if( true == isset( $arrmixValues['group_id'] ) ) {
			$this->setGroupId( $arrmixValues['group_id'] );
		}
		if( true == isset( $arrmixValues['department_id'] ) ) {
			$this->setDepartmentId( $arrmixValues['department_id'] );
		}
		if( true == isset( $arrmixValues['name_full'] ) ) {
			$this->setNameFull( $arrmixValues['name_full'] );
		}
		if( true == isset( $arrmixValues['office_id'] ) ) {
			$this->setOfficeId( $arrmixValues['office_id'] );
		}
		if( true == isset( $arrmixValues['designation_id'] ) ) {
			$this->setDesignationId( $arrmixValues['designation_id'] );
		}

		if( true == isset( $arrmixValues['is_active_directory'] ) ) {
			$this->setIsActiveDirectory( $arrmixValues['is_active_directory'] );
		} else {
			$boolIsActiveDirectoryUser = ( false == is_null( $this->getActiveDirectoryGuid() ) ) ? true : false;
			$this->setIsActiveDirectory( $boolIsActiveDirectoryUser );
		}
		if( true == isset( $arrmixValues['desk_number'] ) ) $this->setDeskNumber( $arrmixValues['desk_number'] );
		return;
	}

	public function setDefaults() {
		$this->m_intIsAdministrator = 0;
		$this->m_intIsDisabled = 0;
	}

	public function setTasks( $arrobjTasks ) {
		$this->m_arrobjTasks = $arrobjTasks;
	}

	public function setDesignationId( $intDesignationId ) {
		$this->m_intDesignationId = $intDesignationId;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setPreferredName( $strPreferredName ) {
		$this->m_strPreferredName = $strPreferredName;
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	public function setDesignationName( $strDesignationName ) {
		$this->m_strDesignationName = $strDesignationName;
	}

	public function setManagerId( $intManagerId ) {
		$this->m_intManagerId = $intManagerId;
	}

	public function setEnableNewTaskSystem( $intEnableNewTaskSystem ) {
		$this->m_intEnableNewTaskSystem = $intEnableNewTaskSystem;
	}

	public function setEmployee( $objEmployee ) {
		$this->m_objEmployee = $objEmployee;
	}

	public function setUserGroups( $arrobjUserGroups ) {
		$this->m_arrobjUserGroups = $arrobjUserGroups;
	}

	public function setEmployeeAssessmentsUsers( $arrintEmployeeAssessmentsUsers ) {
		$this->m_arrintEmployeeAssessmentsUsers = $arrintEmployeeAssessmentsUsers;
	}

	public function setUserRoles( $arrobjUserRoles ) {
		$this->m_arrobjUserRoles = $arrobjUserRoles;
	}

	public function setIsLoggedIn( $boolIsLoggedIn ) {
		$this->m_boolIsLoggedIn = ( bool ) $boolIsLoggedIn;
	}

	public function setUserPermissions( $arrobjUserPermissions ) {
		$this->m_arrobjUserPermissions = $arrobjUserPermissions;
	}

	public function setNewPassword( $strNewPassword ) {
		$this->set( 'm_strNewPassword', trim( $strNewPassword ) );
	}

	public function setUnencryptedPassword( $strUnencryptedPassword ) {
		$this->m_strUnencryptedPassword = trim( $strUnencryptedPassword );
		$this->setPassword( $this->m_strUnencryptedPassword );
	}

	public function setPassword( $strPassword ) {
		$this->m_strPassword = trim( $strPassword );

		if( true == valStr( $strPassword ) ) {
			if( 'stage' == CONFIG_ENVIRONMENT || true == extension_loaded( 'sodium' ) ) {
				$this->m_strPasswordEncrypted = ( CCrypto::create() )->hash( trim( $strPassword ) );
			} else {
				$this->m_strPasswordEncrypted = CHash::createService()->hashUserOld( trim( $strPassword ) );
			}
		}
	}

	public function setOldPassword( $strOldPassword ) {
		$this->m_strOldPassword = trim( $strOldPassword );
	}

	public function setOldUsername( $strOldUsername ) {
		$this->m_strOldUsername = $strOldUsername;
	}

	public function setPasswordConfirm( $strPasswordConfirm ) {
		$this->m_strPasswordConfirm = trim( $strPasswordConfirm );
	}

	public function setIsAdminUserPasswordUpdate( $boolIsAdminUserPasswordUpdate ) {
		$this->m_boolIsAdminUserPasswordUpdate = $boolIsAdminUserPasswordUpdate;
	}

	public function setErrorMsgs( $arrobjErrorMsgs ) {
		$this->m_arrobjErrorMsgs = $arrobjErrorMsgs;
	}

	public function setLoginAttemptData( $boolIsLoggedIn = false ) {
		if( true == $boolIsLoggedIn ) {
			$this->setLoginAttemptCount( 0 );
		} elseif( time() > ( strtotime( date( 'c', strtotime( $this->getLastLoginAttemptOn() ) ) ) + self::LOGIN_ATTEMPT_WAITING_TIME ) && 0 < $this->getLoginAttemptCount() ) {
			$this->setLoginAttemptCount( 1 );
		} else {
			$this->setLoginAttemptCount( $this->getLoginAttemptCount() + 1 );
		}

		$this->setLastLoginAttemptOn( date( 'c', time() ) );
	}

	public function setIsPasswordRotation( $boolIsPasswordRotation ) {
		$this->m_boolIsPasswordRotation = ( bool ) $boolIsPasswordRotation;
	}

	public function setUserAuthenticationLogId( $intUserAuthenticationLogId ) {
		$this->m_intUserAuthenticationLogId = ( int ) $intUserAuthenticationLogId;
	}

	public function setAssociatedRoleIds( $arrintAssociatedRoleIds ) {
		if( true == valArr( $arrintAssociatedRoleIds ) ) {
			$this->m_arrintAssociatedRoleIds = $arrintAssociatedRoleIds;
		}
	}

	public function setAssociatedGroupIds( $arrintAssociatedGroupIds ) {
		if( true == valArr( $arrintAssociatedGroupIds ) ) {
			$this->m_arrintAssociatedGroupIds = $arrintAssociatedGroupIds;
		}
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setPhoneNumber( $intPhoneNumber ) {
		$this->m_intPhoneNumber = $intPhoneNumber;
	}

	public function setMessageOperatorId( $intMessageOperatorId ) {
		$this->m_intMessageOperatorId = $intMessageOperatorId;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setCountryCode( $strCountryCode ) {
		$this->m_strCountryCode = $strCountryCode;
	}

	public function setStatusType( $strStatusType ) {
		$this->m_strStatustype = $strStatusType;
	}

	public function setEmployeeStatusTypeId( $strEmployeeStatusTypeId ) {
		$this->m_strEmployeeStatusTypeId = $strEmployeeStatusTypeId;
	}

	public function setHidePhoneNumber( $boolHidePhoneNumber ) {
		return $this->m_boolHidePhoneNumber = $boolHidePhoneNumber;
	}

	public function setDnsHandle( $strDnsHandle ) {
		$this->m_strDnsHandle = $strDnsHandle;
	}

	public function setWorkStationIpAddress( $intWorkStationIpAddress ) {
		$this->m_intWorkStationIpAddress = $intWorkStationIpAddress;
	}

	public function setIsProjectManager( $boolIsProjectManager ) {
		$this->m_boolIsProjectManager = ( bool ) $boolIsProjectManager;
	}

	public function setTagName( $strTagName ) {
		$this->m_strTagName = $strTagName;
	}

	public function setSecondPassword( $strSecondPassword ) {
		$this->m_strSecondPassword = $strSecondPassword;
	}

	public function setGroupId( $intGroupId ) {
		$this->m_intGroupId = CStrings::strToIntDef( $intGroupId, NULL, false );
	}

	public function setIsActiveDirectory( $boolIsActiveDirectory ) {
		$this->set( 'm_boolIsActiveDirectory', $boolIsActiveDirectory );
	}

	public function setIsSsoEnabled( $boolIsSsoEnabled ) {
		$this->set( 'm_boolIsSsoEnabled', $boolIsSsoEnabled );
	}

	public function setShowSecurityPolicyNotification( $boolShowSecurityPolicyNotification ) {
		$this->m_boolShowSecurityPolicyNotification = $boolShowSecurityPolicyNotification;
	}

	public function setForceSecurityPolicyModule( $boolForceSecurityPolicyModule ) {
		$this->m_boolForceSecurityPolicyModule = $boolForceSecurityPolicyModule;
	}

	public function setForceFillOutSurveyId( $intForceFillOutSurveyId ) {
		$this->m_intForceFillOutSurveyId = $intForceFillOutSurveyId;
	}

	public function setDeskNumber( $intDeskNumber ) {
		$this->m_intDeskNumber = $intDeskNumber;
	}

	public function setDepartmentId( $intDepartmentId ) {
		$this->m_intDepartmentId = $intDepartmentId;
	}

	public function setOfficeId( $intOfficeId ) {
		$this->m_intOfficeId = $intOfficeId;
	}

	public function setIsExternalTools( $boolShowExternalTools ) {
		$this->m_boolShowExternalTools = $boolShowExternalTools;
	}

	public function setIsVpnFormSigned( $boolVpnFormSigned ) {
		$this->m_boolVpnFormSigned = $boolVpnFormSigned;
	}

	public function setIsHardwareAllocationFormSigned( $boolHardwareAllocationFormSigned ) {
		$this->m_boolHardwareAllocationFormSigned = $boolHardwareAllocationFormSigned;
	}

	public function setIsRemoteWfhFormSigned( $boolRemoteWfhFormSigned ) {
		$this->m_boolRemoteWfhFormSigned = $boolRemoteWfhFormSigned;
	}

	public function setEnableISTTimezone( $intEnableIstTimezone ) {
		$this->m_arrmixCustomVariables['enable_ist_timezone'] = $intEnableIstTimezone;
	}

	public function setForceEmployeePolicyModule( $boolForceEmployeePolicyModule ) {
		$this->m_boolForceEmployeePolicyModule = $boolForceEmployeePolicyModule;
	}

	public function setWordPressUsername( $strWordPressUsername ) {
		$this->m_strWordPressUsername = $strWordPressUsername;
	}

	public function setWordPressUserId( $intWordPressUserId ) {
		$this->m_intWordPressUserId = $intWordPressUserId;
	}

	public function setLdapOu( $strLdapOu ) {
		$this->m_strLdapOu = $strLdapOu;
	}

	public function setForceNcndPolicyModule( $boolForceNcndPolicyModule ) {
		$this->m_boolForceNcndPolicyModule = $boolForceNcndPolicyModule;
	}

	public function setForceWfhPolicyModule( $boolForceWfhPolicyModule ) {
		$this->m_boolForceWfhPolicyModule = $boolForceWfhPolicyModule;
	}

	/**
	 * GetOrFetch Functions
	 *
	 */

	public function getOrFetchEmployee( $objDatabase, $boolFromLogin = false ) {
		if( false == valObj( $this->m_objEmployee, 'CEmployee' ) ) {
			if( true == $boolFromLogin ) {
				$this->m_objEmployee = \Psi\Eos\Admin\CEmployees::createService()->fetchLoggedInEmployeeWithDetailsByEmployeeId( $this->m_intEmployeeId, $objDatabase );
			} else {
				$this->m_objEmployee = \Psi\Eos\Admin\CEmployees::createService()->fetchEmployeeWithDetailsByEmployeeId( $this->m_intEmployeeId, $objDatabase );
			}
		}

		return $this->m_objEmployee;
	}

	public function getOrFetchUserRoles( $objDatabase ) {
		if( false == valArr( $this->m_arrobjUserRoles ) ) {
			$this->m_arrobjUserRoles = $this->fetchUserRoles( $objDatabase );
		}

		return $this->m_arrobjUserRoles;
	}

	public function getOrfetchUserRolesNotByAllowSuperUserOnly( $objDatabase, $boolIsSuperUser = false, $boolIsAdministrator = false ) {
		if( false == valArr( $this->m_arrobjUserRoles ) ) {
			$this->m_arrobjUserRoles = \Psi\Eos\Admin\CUserRoles::createService()->fetchUserRolesNotByAllowSuperUserOnly( $this->m_intId, $objDatabase, $boolIsSuperUser, $boolIsAdministrator );
		}

		return $this->m_arrobjUserRoles;
	}

	public function getOrFetchAssociatedGroupIds( $objDatabase ) {
		$arrobjGroups = [];

		if( true == $this->getIsSuperUser() ) {
			$arrobjGroups = CGroups::fetchAllGroups( $objDatabase );
		} elseif( false == valArr( $this->m_arrintAssociatedGroupIds ) ) {
			$arrobjGroups = $this->fetchGroups( $objDatabase );
		}

		if( true == valArr( $arrobjGroups ) ) {
			$this->m_arrintAssociatedGroupIds = array_keys( $arrobjGroups );
		}
		return $this->m_arrintAssociatedGroupIds;
	}

	public function getOrFetchAssociatedRoleIds( $objDatabase ) {
		if( false == valArr( $this->m_arrintAssociatedRoleIds ) ) {
			$arrobjUserRoles = $this->fetchUserRolesByUserType( $objDatabase );
			if( true == valArr( $arrobjUserRoles ) ) {
				$this->m_arrintAssociatedRoleIds = array_keys( rekeyObjects( 'RoleId', $arrobjUserRoles ) );
			}
		}
		return $this->m_arrintAssociatedRoleIds;
	}

	public function getPassword() {
		return $this->m_strPassword;
	}

	public function getOldUsername() {
		return $this->m_strOldUsername;
	}

	public function getOldPassword() {
		return $this->m_strOldPassword;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchPsLeadFilters( $objDatabase, $boolFetchAllPsLeadFilters = true ) {
		return \Psi\Eos\Admin\CPsLeadFilters::createService()->fetchPsLeadFiltersByUserId( $this->getId(), $objDatabase, $boolFetchAllPsLeadFilters );
	}

	public function fetchEmployee( $objDatabase ) {
		$this->m_objEmployee = CEmployees::fetchEmployeeById( $this->m_intEmployeeId, $objDatabase );
		return $this->m_objEmployee;
	}

	public function fetchCompanyUser( $objClientDatabase, $boolIsCheckForEnabled = false ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchCustomEmployeeCompanyUserByIdByIsDisabled( $this->getId(), $objClientDatabase, $boolIsCheckForEnabled );
	}

	public function fetchEmployeeByUserId( $objDatabase ) {
		$this->m_objEmployee = CEmployees::fetchEmployeeById( $this->m_intEmployeeId, $objDatabase );
		return $this->m_objEmployee;
	}

	public function fetchUserGroups( $objDatabase ) {
		$this->m_arrobjUserGroups = CUserGroups::fetchUserGroupsByUserId( $this->m_intId, $objDatabase );
		return $this->m_arrobjUserGroups;
	}

	public function fetchUserRoles( $objDatabase ) {
		return CUserRoles::fetchUserRolesByUserId( $this->m_intId, $objDatabase );
	}

	public function fetchUserRolesByUserType( $objDatabase ) {
		return CUserRoles::fetchUserRolesByUserAccess( $this->m_intId, $this->m_intIsSuperUser, $this->m_intIsAdministrator, $objDatabase );
	}

	public function fetchGroups( $objDatabase ) {
		return CGroups::fetchGroupsByUserId( $this->m_intId, $objDatabase );
	}

	public function fetchUserPermissions( $objDatabase ) {
		$this->m_arrobjUserPermissions = CUserPermissions::fetchUserPermissionsByUserId( $this->m_intId, $objDatabase );
		return $this->m_arrobjUserPermissions;
	}

	public function fetchUserAuthenticationLog( $objAdminDatabase ) {
		return CUserAuthenticationLogs::fetchUserAuthenticationLogsByIdByUserId( $this->getUserAuthenticationLogId(), $this->getId(), $objAdminDatabase );
	}

	public function fetchPermissionedModuleIds( $objDatabase ) {
		return CUsers::fetchPermissionedModuleIds( $this, $objDatabase );
	}

	public function fetchUserGroupsCount( $objDatabase ) {
		return CUserGroups::fetchUserGroupsCountByUserId( $this->m_intId, $objDatabase );
	}

	public function fetchUserModulePermissions( $objDatabase ) {
		return \Psi\Eos\Admin\CUserModulePermissions::createService()->fetchUserModulePermissionsByUserId( $this->getId(), $objDatabase );
	}

	public function fetchGroupModulePermissions( $objDatabase ) {
		return \Psi\Eos\Admin\CGroupModulePermissions::createService()->fetchGroupModulePermissionsByUserId( $this->getId(), $objDatabase );
	}

	public function fetchCompanyUserByIdByCid( $intCid, $objClientDatabase ) {
		return \Psi\Eos\Entrata\CCompanyUsers::createService()->fetchEmployeeCompanyUserByIdByCidByIsDisabled( $this->getId(), $intCid, $objClientDatabase, $boolIsCheckForEnabled = true );
	}

	/**
	 * Create Functions
	 *
	 */

	public function createEmployee() {
		$this->m_objEmployee = new CEmployee();

		return $this->m_objEmployee;
	}

	public function createUserGroup( $intGroupId ) {
		$this->m_objUserGroup = new CUserGroup();
		$this->m_objUserGroup->setUserId( $this->m_intId );
		$this->m_objUserGroup->setGroupId( $intGroupId );

		return $this->m_objUserGroup;
	}

	public function createUserRole( $intRoleId ) {
		$objUserRole = new CUserRole();
		$objUserRole->setUserId( $this->m_intId );
		$objUserRole->setRoleId( $intRoleId );

		return $objUserRole;
	}

	public function createUserAuthenticationLog() {
		$objUserAuthenticationLog = new CUserAuthenticationLog();
		$objUserAuthenticationLog->setUserId( $this->m_intId );

		return $objUserAuthenticationLog;
	}

	public function createUsername( $strUsername, $objDatabase, $intCounter = 0 ) {
		if( 0 < $intCounter ) {
			$strFinalUsername 	= \Psi\CStringService::singleton()->strtolower( $strUsername ) . sprintf( '%02d', $intCounter );
		} else {
			$strFinalUsername 	= \Psi\CStringService::singleton()->strtolower( $strUsername );
		}

		$strSql 				= ' WHERE ( username ILIKE \'' . $strFinalUsername . '\' OR svn_username ILIKE \'' . $strFinalUsername . '\' )' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
		$intUserCount 			= CUsers::fetchRowCount( $strSql, 'users', $objDatabase );

		$strSql 				= ' WHERE split_part(email_address, \'@\', 1 ) = \'' . $strFinalUsername . '\' ' . ( 0 < $this->getEmployeeId() ? ' AND id <> ' . $this->getEmployeeId() : '' );
		$intEmailAddressCount 	= CEmployees::fetchRowCount( $strSql, 'employees', $objDatabase );

		if( 0 < $intUserCount || 0 < $intEmailAddressCount ) {
			$intCounter++;
			$strFinalUsername = $this->createUsername( $strUsername, $objDatabase, $intCounter );
		}

		return $strFinalUsername;
	}

	public function createUserPermission( $intPsModuleId ) {
		$objUserPermission = new CUserPermission();
		$objUserPermission->setUserId( $this->getId() );
		$objUserPermission->setPsModuleId( $intPsModuleId );

		return $objUserPermission;
	}

	public function createUserModulePermission( $intModuleId ) {
		$objUserModulePermission = new CUserModulePermission();
		$objUserModulePermission->setUserId( $this->getId() );
		$objUserModulePermission->setModuleId( $intModuleId );

		return $objUserModulePermission;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valAutoGeneratedUsername( $strNameFirst, $strNameLast, $objDatabase ) {
		// No need of this function.
		$strNameLast = str_replace( ' ', '', $strNameLast );
		$strUserName = \Psi\CStringService::singleton()->ucwords( $strNameFirst[0] ) . \Psi\CStringService::singleton()->strtolower( $strNameLast );

		if( 8 > \Psi\CStringService::singleton()->strlen( $strUserName ) ) {
			$intOffset = 8 - \Psi\CStringService::singleton()->strlen( $strUserName );
			for( $intCount = 0; $intOffset > $intCount; $intCount++ ) {
				$strUserName = $strUserName . '0';
			}
		} else {
			$strUserName = $strUserName . '0';
		}

		$strUserName = $strUserName . '1';
		$strUserName = $this->createUsername( $strUserName, $objDatabase );
		$this->setUsername( $strUserName );
		return;
	}

	public function valUsername( $objDatabase ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strUsername ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Valid username is required.' ) );
		}

		$strUsernamePattern = '/^[A-Za-z]{1}[A-Za-z0-9._]{1,}$/';

		if( true == $boolIsValid && 1 != \Psi\CStringService::singleton()->preg_match( $strUsernamePattern, $this->m_strUsername ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Username should contain only alphabets and digits.' ) );
		}

		if( true == $boolIsValid && true == isset( $objDatabase ) ) {
			$strSql = ' WHERE LOWER(username) LIKE LOWER(\'' . $this->m_strUsername . '\')' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
			$intCount = CUsers::fetchRowCount( $strSql, 'users', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Username already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPassword() {
		// Validate password against password policy
		$boolIsValid = true;

		if( false == isset( $this->m_strPassword ) || 8 > \Psi\CStringService::singleton()->strlen( $this->m_strPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password must be at least 8 characters.' ) );
		}

		if( true == $boolIsValid ) {
			$intCriteriaCount 	= 0;
			$intCriteriaCount 	+= ( true == \Psi\CStringService::singleton()->preg_match( '/^(?=.*\d)\S*$/', $this->m_strPassword ) ) ? 1 : 0;
			$intCriteriaCount 	+= ( true == \Psi\CStringService::singleton()->preg_match( '/^(?=.*[a-z])\S*$/', $this->m_strPassword ) ) ? 1 : 0;
			$intCriteriaCount 	+= ( true == \Psi\CStringService::singleton()->preg_match( '/^(?=.*[A-Z])\S*$/', $this->m_strPassword ) ) ? 1 : 0;
			$intCriteriaCount 	+= ( true == \Psi\CStringService::singleton()->preg_match( '/[!@_#$%^&*()+=\-\[\]\';,.\/{}|":<>?~\\\\]/', $this->m_strPassword ) ) ? 1 : 0;
			$intCriteriaCount 	+= ( true == \Psi\CStringService::singleton()->preg_match( '/([\x{3000}-\x{303F}\x{FF5F}-\x{FF9F}\p{Katakana}\p{Hiragana}\p{Han}「」]+)/mu', $this->m_strPassword ) ) ? 1 : 0;

			if( 3 > $intCriteriaCount ) {
				$boolIsValid = false;
			}

			if( true == $boolIsValid ) {
				if( false !== \Psi\CStringService::singleton()->strpos( \Psi\CStringService::singleton()->strtolower( $this->m_strPassword ), \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) ) ) {
					$boolIsValid = false;
				}
			}

			if( true == $boolIsValid && true == valObj( $this->getEmployee(), 'CEmployee' ) ) {
				if( false !== \Psi\CStringService::singleton()->strpos( \Psi\CStringService::singleton()->strtolower( $this->m_strPassword ), \Psi\CStringService::singleton()->strtolower( $this->getEmployee()->getNameFirst() ) ) ) {
					$boolIsValid = false;
				}

				if( false !== \Psi\CStringService::singleton()->strpos( \Psi\CStringService::singleton()->strtolower( $this->m_strPassword ), \Psi\CStringService::singleton()->strtolower( $this->getEmployee()->getNameLast() ) ) ) {
					$boolIsValid = false;
				}
			}

			if( false == $boolIsValid ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'password', 'Password does not meet policy criteria.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valOldPassword() {
		$boolIsValid = true;

		if( false == $this->m_boolIsAdminUserPasswordUpdate ) {
			if( false == valStr( $this->m_strOldPassword ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'old_password', 'Old password is required.' ) );
				$boolIsValid = false;
				return $boolIsValid;
			}

			if( 'production' == CONFIG_ENVIRONMENT || ( true == isset( $_COOKIE['LDAP_ENABLED'] ) ) ) {

				require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Ldap/CClientAdminLdap.class.php' );

				$objClientAdminLdap = new CClientAdminLdap();
				$boolIsValid        = $objClientAdminLdap->authenticate( $this->getUsername(), $this->m_strOldPassword );
				if( false == $boolIsValid ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'old_password', 'Old password does not match.' ) );
				}
			}

		}

		return $boolIsValid;
	}

	public function valNewPassword() {
		$boolIsValid = true;

		$this->m_strPassword		= $this->m_strNewPassword;

		$boolIsValid				= $this->valPassword();

		if( trim( $this->m_strNewPassword ) != trim( $this->m_strPasswordConfirm ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'new_password', 'Confirmation password does not match new password.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valSvnUsername( $objDatabase ) {
		$boolIsValid = true;

		if( false == isset( $this->m_strSvnUsername ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'svn_username', 'SVN username is required.' ) );
		}

		if( true == isset( $objDatabase ) ) {
			$intConflictingSvnUserCount = CUsers::fetchConflictingSvnUserCountBySvnUsernameById( $this->m_strSvnUsername, $this->m_intId, $objDatabase );

			if( 0 < $intConflictingSvnUserCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'SVN username already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valSvnPassword() {
		$boolIsValid = true;

		if( false == isset( $this->m_strSvnPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'svn_password', 'SVN password cannot be empty.' ) );
		}

		return $boolIsValid;
	}

	public function valLogin() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strUsername ) || false == valStr( $this->m_strUnencryptedPassword ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unencrypted_password', 'Username and/or password is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPasswordExistence( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == $this->isExistsNewPasswordInAuthenticationLogs( $objAdminDatabase ) ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $strNameFirst = NULL, $strNameLast = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valUsername( $objDatabase );
				break;

			case 'validate_password':
				$boolIsValid &= $this->valPassword();
				break;

			case 'validate_update_username':
				$boolIsValid &= $this->valUsername( $objDatabase );
				break;

			case 'validate_update_user_password':
				$boolIsValid &= $this->valOldPassword();
				$boolIsValid &= $this->valNewPassword();
				break;

			case 'validate_login':
				$boolIsValid &= $this->valLogin();
				break;

			case 'validate_svn_username_and_password':
				$boolIsValid &= $this->valSvnUsername( $objDatabase );
				$boolIsValid &= $this->valSvnPassword();
				break;

			case 'validate_password_existence':
				$boolIsValid &= $this->valPasswordExistence( $objDatabase );
				break;

			case 'validate_auto_generated_username':
				$boolIsValid &= $this->valAutoGeneratedUsername( $strNameFirst, $strNameLast, $objDatabase );
				break;

			case VALIDATE_DELETE:
			default:
				// no validation avaliable for delete action
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( true == is_null( $this->getId() ) ) {
			return $this->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {
			return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		if( false == parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return false;
		}

		$boolChinaCloud = ( defined( 'CONFIG_CLOUD_REFERENCE_ID' ) && CONFIG_CLOUD_REFERENCE_ID == CCloud::REFERENCE_ID_CHINA );
		if( false == is_null( \CConfig::get( 'sso_url' ) ) && false == $boolChinaCloud ) {
			$boolIsSsoEnabled = $this->getIsSsoEnabled();
			// Add code to sync user with sso here for update user
			$arrstrSsoResponse = $this->syncUserToSso();
			if( false == $arrstrSsoResponse['status'] ) {
				$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', $arrstrSsoResponse['error'], 304 ) );

				return false;
			}

			// Update user record to set new password and is_sso_enabled flag, if new SSO record is created
			if( false == $boolIsSsoEnabled && true == $this->getIsSsoEnabled() && false == parent::update( $intCurrentUserId, $objDatabase ) ) {
				return false;
			}
		}

		if( ( 'production' == CONFIG_ENVIRONMENT || ( true == isset( $_COOKIE['LDAP_ENABLED'] ) ) ) && true == $this->getIsActiveDirectory() ) {
			require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Ldap/CClientAdminLdapUser.class.php' );
			$boolUseMasterServer = ( true == valStr( $this->getNewPassword() ) ); // use master AD server, if user password is being updated
			$objClientAdminLdapUser = new CClientAdminLdapUser( $boolUseMasterServer );
			$boolIsValid 			= $objClientAdminLdapUser->syncToActiveDirectory( $this, $objDatabase );

			if( true == $boolIsValid && false == is_null( $this->getActiveDirectoryGuid() ) ) {
				$boolIsValid = parent::update( $intCurrentUserId, $objDatabase );

				if( false == $boolIsValid ) {
					// To rollback from Active Directory
					$objClientAdminLdapUser->deleteUserFormActiveDirectory( $this );
					return false;
				}
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ldap_error', $objClientAdminLdapUser->getErrorMessage() . '. To resolve this either contact system administration team OR please uncheck Active Directory User to unlink this user from Active Directory.' ) );
				return false;
			}
		}

		return true;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolSyncWithLdap = true ) {
		// If user previously synced wih Active Directory and unchecked, then update GUID to unlink user.
		if( false == $this->getIsActiveDirectory() ) {
			$this->setActiveDirectoryGuid( NULL );
		}

		$boolRequireTransaction = $objDatabase->getIsTransaction();
		if( false == $boolRequireTransaction ) {
			$objDatabase->begin();
		}

		$strSecret = $this->getGoogleAuthenticatorSecretKey();
		if( true == empty( $this->getGoogleAuthenticatorKey() ) || $strSecret != $this->getGoogleAuthenticatorKey() ) {
			$this->setGoogleAuthenticatorKey( $strSecret );
		}
		if( false == parent::update( $intCurrentUserId, $objDatabase ) ) {
			if( false == $boolRequireTransaction ) {
				$objDatabase->rollback();
			}
			return false;
		}

		$boolChinaCloud = ( defined( 'CONFIG_CLOUD_REFERENCE_ID' ) && CONFIG_CLOUD_REFERENCE_ID == CCloud::REFERENCE_ID_CHINA );
		if( false == is_null( \CConfig::get( 'sso_url' ) ) && false == $boolChinaCloud ) {
			$boolIsSsoEnabled = $this->getIsSsoEnabled();
			// Add code to sync user with sso here for update user
			$arrstrSsoResponse = $this->syncUserToSso();
			if( false == $arrstrSsoResponse['status'] ) {
				if( false == $boolRequireTransaction ) {
					$objDatabase->rollback();
				}
				$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', $arrstrSsoResponse['error'], 304 ) );

				return false;
			}
			// Update user record to set new password and is_sso_enabled flag, if new SSO record is created
			if( false == $boolIsSsoEnabled && true == $this->getIsSsoEnabled() && false == parent::update( $intCurrentUserId, $objDatabase ) ) {
				if( false == $boolRequireTransaction ) {
					$objDatabase->rollback();
				}

				return false;
			}
		}

		if( ( 'production' == CONFIG_ENVIRONMENT || ( true == isset( $_COOKIE['LDAP_ENABLED'] ) ) ) && true == $boolSyncWithLdap && true == $this->getIsActiveDirectory() ) {
			require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Ldap/CClientAdminLdapUser.class.php' );
			$boolUseMasterServer = ( true == valStr( $this->getNewPassword() ) ); // use master AD server, if user password is being updated
			$objClientAdminLdapUser = new CClientAdminLdapUser( $boolUseMasterServer );
			$strActiveDirectoryGuid = $this->getActiveDirectoryGuid();
			$boolIsValid 			= $objClientAdminLdapUser->syncToActiveDirectory( $this, $objDatabase );
			// Update user only to update GUID
			if( true == $boolIsValid && false == is_null( $this->getActiveDirectoryGuid() ) ) {
				if( true == is_null( $strActiveDirectoryGuid ) ) {
					$boolIsValid = parent::update( $intCurrentUserId, $objDatabase );
					if( false == $boolRequireTransaction && false == $boolIsValid ) {
						$objDatabase->rollback();
					}
				}
			} else {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ldap_error', $objClientAdminLdapUser->getErrorMessage() . '. To resolve this either contact system administration team OR please uncheck Active Directory User to unlink this user from Active Directory.' ) );
				if( false == $boolRequireTransaction ) {
					$objDatabase->rollback();
				}
				return false;
			}
		}
		if( false == $boolRequireTransaction ) {
			$objDatabase->commit();
		}

		return true;
	}

	/**
	 * Other Functions
	 *
	 */

	public function acceptNewSvnPassword() {
		$this->encryptSvnPassword();
	}

	public function login( $objAdminDatabase, $boolIsSamlLogin = false, $arrmixUserDetails = NULL ) {
		$objUser = \Psi\Eos\Admin\CUsers::createService()->fetchActiveUserByUsername( $this->getUsername(), $objAdminDatabase );

		$boolIsChinaCloud = ( defined( 'CONFIG_CLOUD_REFERENCE_ID' ) && CONFIG_CLOUD_REFERENCE_ID == CCloud::REFERENCE_ID_CHINA );

		if( false == valObj( $objUser, 'CUser' ) ) {
			if( true == $boolIsChinaCloud && true == $boolIsSamlLogin ) {
				$objUser = $this->createUser( $arrmixUserDetails, $objAdminDatabase );

				if( false == valObj( $objUser, 'CUser' ) ) {
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );
					return false;
				}
			} else {
				$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'Invalid username and/or password', 304 ) );
				return false;
			}
		}

		if( false == $boolIsSamlLogin ) {
			if( false == is_null( \CConfig::get( 'sso_url' ) ) && true == $objUser->getIsSsoUser() ) {
				$objUser = $this->authenticateUserToSso( $objUser, $objAdminDatabase );
			} else {
				$objUser = $this->authenticateUser( $objUser, $objAdminDatabase );
			}
		}

		if( true == valObj( $objUser, 'CUser' ) ) {
			$objUser->setIsLoggedIn( true );

			$this->m_intId = $objUser->getId();

			$objUser->logUserAuthentication( $objAdminDatabase );

			if( !$boolIsChinaCloud ) $objUser->checkIsPasswordRotation( $objAdminDatabase );

			$objUser->checkIsShowExternalTools( $objAdminDatabase );

			$objUser->checkIsLaptopAssigned( $objAdminDatabase );

			$objUser->setLoginAttemptData( true );

			$objUser->setLastLogin( 'NOW()' );
			$objUser->setLastAccess( 'NOW()' );

			if( false == $objUser->update( $this->getId(), $objAdminDatabase, false ) ) {
				trigger_error( 'User record failed to update for User ID: ' . $this->getId(), E_USER_WARNING );
			}

			if( true == $this->m_boolLdapAuthentication && false == is_null( $objUser->getActiveDirectoryGuid() ) ) {
				require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Ldap/CClientAdminLdapGroup.class.php' );

				$objClientAdminLdapGroup = new CClientAdminLdapGroup();
				$boolIsValid = $objClientAdminLdapGroup->syncUserGroupsFromActiveDirectory( $objUser, $this->getId(), $objAdminDatabase );
				if( false == $boolIsValid && CLdap::ERROR_UNABLE_TO_CONNECT != $objClientAdminLdapGroup->getErrorNumber() ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ldap_error', $objClientAdminLdapGroup->getErrorMessage() . '. To resolve this contact system administration team.' ) );
					return false;
				}
			}

			return $objUser;
		}

		return false;
	}

	public function authenticateUser( $objUser, $objAdminDatabase ) {
		$strErrorMessage = 'Invalid username and/or password';
		if( true == valObj( $objUser, 'CUser' ) ) {
			$intUserLoginAttemptCount = ( int ) ( self::TOTAL_LOGIN_ATTEMPTS - $objUser->getLoginAttemptCount() );
			if( 0 < $intUserLoginAttemptCount ) {
				$strErrorMessage = 'Login failed, you have ' . $intUserLoginAttemptCount . ' more attempts before you will be locked out of the system for 30 minutes.';
			}
		}
		if( 'stage' == CONFIG_ENVIRONMENT || true == extension_loaded( 'sodium' ) ) {
			$objCrypto = CCrypto::create();
			if( ( 'production' == CONFIG_ENVIRONMENT || ( true == isset( $_COOKIE['LDAP_ENABLED'] ) ) ) && true == valObj( $objUser, 'CUser' ) && false == is_null( $objUser->getActiveDirectoryGuid() ) ) {
				require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Ldap/CClientAdminLdap.class.php' );

				$objClientAdminLdap = new CClientAdminLdap();
				$boolIsValid        = $objClientAdminLdap->authenticate( $this->getUsername(), $this->m_strUnencryptedPassword );

				if( true == $boolIsValid ) {
					$strOldPassword                 = $objUser->getPasswordEncrypted();
					$this->m_boolLdapAuthentication = true;

					// Login Success, Encrypt and update password
					$objUser->setPassword( $this->m_strUnencryptedPassword );

					// Log password, if updated
					if( false == $objCrypto->verifyHash( $this->getPassword(), $strOldPassword ) ) {
						$objUserAuthenticationLog = $objUser->createUserAuthenticationLog();
						$objUserAuthenticationLog->setPreviousPassword( $objUser->getPasswordEncrypted() );

						if( false == $objUserAuthenticationLog->insert( $objUser->getId(), $objAdminDatabase ) ) {
							$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'User record failed to update.', 304 ) );

							return false;
						}

						// Update password of Global user
						$objConnectDatabase = CDatabases::createConnectDatabase( false );
						$objGlobalUser      = CGlobalUsers::fetchGlobalUserByIsSystemUserByWorksId( $objUser->getId(), $objConnectDatabase );

						if( true == valObj( $objGlobalUser, 'CGlobalUser' ) ) {
							$objGlobalUser->setPasswordEncrypted( $objUser->getPasswordEncrypted() );
							$objGlobalUser->setIsActiveDirectoryUser( 1 );

							if( false == $objGlobalUser->update( $objUser->getId(), $objConnectDatabase ) ) {
								$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'User record failed to update.', 304 ) );
								$objConnectDatabase->rollback();

								return false;
							}
						}
					}

				} elseif( -1 == $objClientAdminLdap->getErrorNumber() && false == $objCrypto->verifyHash( $this->getPassword(), $objUser->getPasswordEncrypted() ) ) {
					// Unable to connect Active Directory server, so check password from database
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', $strErrorMessage, 304 ) );

					return false;
				} elseif( CLdap::ERROR_UNABLE_TO_CONNECT != $objClientAdminLdap->getErrorNumber() ) {
					// Invalid credentials
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', $strErrorMessage, 304 ) );

					return false;
				}

			} elseif( true == valObj( $objUser, 'CUser' ) && false == $objCrypto->verifyHash( $this->getPassword(), $objUser->getPasswordEncrypted() ) ) {
				$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', $strErrorMessage, 304 ) );
				return false;
			}

			if( \Psi\Libraries\Cryptography\CCryptoProviderFactory::CRYPTO_PROVIDER_MCRYPT == $objCrypto->getProviderNameForHash( $objUser->getPasswordEncrypted() ) ) {
				$objUser->setPassword( $this->m_strUnencryptedPassword );
			}

			return $objUser;

		} else {
			if( ( 'production' == CONFIG_ENVIRONMENT || ( true == isset( $_COOKIE['LDAP_ENABLED'] ) ) ) && true == valObj( $objUser, 'CUser' ) && false == is_null( $objUser->getActiveDirectoryGuid() ) ) {
				require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Ldap/CClientAdminLdap.class.php' );

				$objClientAdminLdap	= new CClientAdminLdap();
				$boolIsValid		= $objClientAdminLdap->authenticate( $this->getUsername(), $this->m_strUnencryptedPassword );

				if( true == $boolIsValid ) {
					$strOldPassword					= $objUser->getPassword();
					$this->m_boolLdapAuthentication = true;

					// Login Success, Encrypt and update password
					$objUser->setPassword( $this->m_strUnencryptedPassword );
					$objUser->encryptPassword();

					// Log password, if updated
					if( $strOldPassword != $objUser->getPassword() ) {
						$objUserAuthenticationLog = $objUser->createUserAuthenticationLog();
						$objUserAuthenticationLog->setPreviousPassword( $objUser->getPassword() );

						if( false == $objUserAuthenticationLog->insert( $objUser->getId(), $objAdminDatabase ) ) {
							$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'User record failed to update.', 304 ) );
							return false;
						}

						// Update password of Global user
						$objConnectDatabase = CDatabases::createConnectDatabase( false );
						$objGlobalUser		= CGlobalUsers::fetchGlobalUserByIsSystemUserByWorksId( $objUser->getId(), $objConnectDatabase );

						if( true == valObj( $objGlobalUser, 'CGlobalUser' ) ) {
							$objGlobalUser->setPasswordEncrypted( $objUser->getPassword() );
							$objGlobalUser->setIsActiveDirectoryUser( 1 );

							if( false == $objGlobalUser->update( $objUser->getId(), $objConnectDatabase ) ) {
								$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'User record failed to update.', 304 ) );
								$objConnectDatabase->rollback();
								return false;
							}
						}
					}

				} elseif( -1 == $objClientAdminLdap->getErrorNumber() && $this->getPasswordEncrypted() != $objUser->getPasswordEncrypted() ) {
					// Unable to connect Active Directory server, so check password from database
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', $strErrorMessage, 304 ) );
					return false;
				} elseif( CLdap::ERROR_UNABLE_TO_CONNECT != $objClientAdminLdap->getErrorNumber() ) {
					// Invalid credentials
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', $strErrorMessage, 304 ) );
					return false;
				}

			} elseif( true == valObj( $objUser, 'CUser' ) ) {
				if( $this->getPasswordEncrypted() != $objUser->getPasswordEncrypted() ) {
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', $strErrorMessage, 304 ) );
					return false;
				}
			}

			return $objUser;
		}
	}

	public function authenticateUserToSso( $objUser, $objAdminDatabase ) {
		$strErrorMessage = 'Invalid username and/or password';
		if( true == valObj( $objUser, 'CUser' ) ) {
			$intUserLoginAttemptCount = ( int ) ( self::TOTAL_LOGIN_ATTEMPTS - $objUser->getLoginAttemptCount() );
			if( 0 < $intUserLoginAttemptCount ) {
				$strErrorMessage = 'Login failed, you have ' . $intUserLoginAttemptCount . ' more attempts before you will be locked out of the system for 30 minutes.';
			}
		}

		require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Sso/CClientAdminSso.class.php' );
		$objClientAdminSso      = new CClientAdminSso();
		$arrmixResponse         = $objClientAdminSso->authenticateUser( $this->getUsername(), $this->m_strUnencryptedPassword );

		if( false == $arrmixResponse['status'] ) {
			if( true == checkDisplayDbInfoProcess() ) {
				$strErrorMessage .= ' ' . $arrmixResponse['error'];
			}
			$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', $strErrorMessage, 304 ) );
			return false;
		}

		$objCrypto = CCrypto::create();
		$strOldPassword                 = $objUser->getPasswordEncrypted();

		// Login Success, Encrypt and update password
		$objUser->setPassword( $this->m_strUnencryptedPassword );
		// Log password, if updated on SSO
		if( false == $objCrypto->verifyHash( $this->getPassword(), $strOldPassword ) ) {
			$objUserAuthenticationLog = $objUser->createUserAuthenticationLog();
			$objUserAuthenticationLog->setPreviousPassword( $objUser->getPasswordEncrypted() );

			if( false == $objUserAuthenticationLog->insert( $objUser->getId(), $objAdminDatabase ) ) {
				$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'User record failed to update.', 304 ) );

				return false;
			}

			// Update password of Global user
			$objConnectDatabase = CDatabases::createConnectDatabase( false );
			$objGlobalUser      = CGlobalUsers::fetchGlobalUserByIsSystemUserByWorksId( $objUser->getId(), $objConnectDatabase );

			if( true == valObj( $objGlobalUser, 'CGlobalUser' ) ) {
				$objGlobalUser->setPasswordEncrypted( $objUser->getPasswordEncrypted() );
				$objGlobalUser->setIsActiveDirectoryUser( 1 );

				if( false == $objGlobalUser->update( $objUser->getId(), $objConnectDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( E_USER_WARNING, 'username', 'User record failed to update.', 304 ) );
					$objConnectDatabase->rollback();

					return false;
				}
			}
		}

		return $objUser;

	}

	public function createUser( $arrmixUserDetails, $objAdminDatabase ) {

		$objUser = new CUser();
		$objUser->setDefaults();

		$objEmployee = $objUser->createEmployee();

		$arrmixUserAccessPortalDetails = json_decode( $arrmixUserDetails['access-portal-details'][0], true );
		$arrmixDepartmentDetails       = \Psi\Eos\Admin\CDepartments::createService()->fetchDepartmentsIdByName( $arrmixUserAccessPortalDetails['department_name'], $objAdminDatabase );
		$arrmixDesignationDetails      = \Psi\Eos\Admin\CDesignations::createService()->fetchDesignationsIdByName( $arrmixUserAccessPortalDetails['designation_name'], $objAdminDatabase );
		$arrmixGroupDetails            = [];

		if( true == $arrmixUserAccessPortalDetails['groups'] ) {
			$arrmixGroupDetails = \Psi\Eos\Admin\CGroups::create()->fetchGroupByNames( $arrmixUserAccessPortalDetails['groups'], $objAdminDatabase );
		}

		$objEmployee->setNameFirst( $arrmixUserDetails['first-name'][0] ?? NULL );
		$objEmployee->setNameLast( $arrmixUserDetails['last-name'][0] ?? NULL );
		$objEmployee->setEmployeeStatusTypeId( CEmployeeStatusType::CURRENT );
		$objEmployee->setDepartmentId( $arrmixDepartmentDetails[0]['id'] ?? NULL );
		$objEmployee->setDesignationId( $arrmixDesignationDetails[0]['id'] ?? NULL );
		$objEmployee->setEmailAddress( $arrmixUserDetails['email-address'][0] ?? NULL );

		$objAdminDatabase->begin();

		if( false == $objEmployee->insert( self::ID_SYSTEM, $objAdminDatabase ) ) {
			$objAdminDatabase->rollback();

			return false;
		}

		if( false == is_null( $arrmixUserAccessPortalDetails['country_code'] ) ) {
			$objPrimaryEmployeeAddress = $objEmployee->createEmployeeAddress();

			$objPrimaryEmployeeAddress->setEmployeeId( $objEmployee->getId() );
			$objPrimaryEmployeeAddress->setAddressTypeId( CAddressType::PRIMARY );
			$objPrimaryEmployeeAddress->setStreetLine1( '100 S 100 W' );
			$objPrimaryEmployeeAddress->setCity( 'Mayberry' );
			$objPrimaryEmployeeAddress->setStateCode( 'UT' );
			$objPrimaryEmployeeAddress->setCountryCode( $arrmixUserAccessPortalDetails['country_code'] );
			$objPrimaryEmployeeAddress->setPostalCode( '62817' );

			if( false == $objPrimaryEmployeeAddress->insert( self::ID_SYSTEM, $objAdminDatabase ) ) {
				$objAdminDatabase->rollback();

				return false;
			}
		}

		$objUser->setEmployeeId( $objEmployee->getId() );
		$objUser->setUsername( $arrmixUserDetails['username'][0] );
		$objUser->setPassword( $this->generatePassword() );

		if( false == $objUser->insert( self::ID_SYSTEM, $objAdminDatabase ) ) {
			$objAdminDatabase->rollback();

			return false;
		}

		if( true == valArr( $arrmixGroupDetails ) ) {
			$objUserGroup = new CUserGroup();

			foreach( $arrmixGroupDetails as $arrmixGroup ) {
				$objclonedUserGroup = clone $objUserGroup;
				$objclonedUserGroup->setUserId( $objUser->getId() );
				$objclonedUserGroup->setGroupId( $arrmixGroup['id'] );
				$arrobjUserGroup[] = $objclonedUserGroup;
			}
			if( false == \Psi\Eos\Admin\CUserGroups::createService()->bulkInsert( $arrobjUserGroup, $objUser->getId(), $objAdminDatabase ) ) {
				$objAdminDatabase->rollback();

				return false;
			}
		}
		$objAdminDatabase->commit();
		return $objUser;
	}

	public function checkin( $objDatabase ) {
		$this->setIsLoggedIn( false );

		$objDataset = $objDatabase->createDataset();
		$objDataset->initialize();

		$strSql = sprintf( 'SELECT * FROM %s WHERE (( username = %s) AND ( password_encrypted = %s ) AND ( NOW() - interval \'30 minute\' ) < last_access )',
		 'users',
		 $this->sqlUsername(),
		 $this->sqlPasswordEncrypted() );

		if( !$objDataset->execute( $strSql ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, 'Failed to check-in user. The following error was reported.' ) );
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, NULL, $objDatabase->errorMsg() ) );
			$objDataset->cleanup();
			return false;
		}

		if( $objDataset->getRecordCount() <> 1 ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Your session has expired.' ) );
			$objDataset->cleanup();
			return false;
		}

		$arrmixValues = $objDataset->fetchArray();
		$this->setValues( $arrmixValues );

		$objDataset->cleanup();

		$this->setLastAccess( 'NOW()' );
		$this->update( $this->getId(), $objDatabase );

		$this->setIsLoggedIn( true );

		return true;
	}

	public function logout() {
		$this->setIsLoggedIn( false );

		return true;
	}

	public function logUserAuthentication( $objAdminDatabase ) {
		if( false == is_null( $this->getUserAuthenticationLogId() ) && true == is_numeric( $this->getUserAuthenticationLogId() ) ) {
			$objUserAuthenticationLog = CUserAuthenticationLogs::fetchUserAuthenticationLogByIdByUserId( $this->getUserAuthenticationLogId(), $this->getId(), $objAdminDatabase );

			if( true == valObj( $objUserAuthenticationLog, 'CUserAuthenticationLog' ) ) {
				$objUserAuthenticationLog->setLogoutDateTime( 'NOW()' );
			}
		} else {
			$objUserAuthenticationLog = $this->createUserAuthenticationLog();
			$objUserAuthenticationLog->setLoginDatetime( 'NOW()' );
		}

		if( true == valObj( $objUserAuthenticationLog, 'CUserAuthenticationLog' ) && false == is_null( $objUserAuthenticationLog ) ) {
			if( false == $objUserAuthenticationLog->validate( VALIDATE_INSERT ) || false == $objUserAuthenticationLog->insertOrUpdate( $this->getId(), $objAdminDatabase ) ) {
				trigger_error( 'Failed to insert or update authentication log for the User : ' . $this->m_intId, E_USER_WARNING );
			}

			$this->setUserAuthenticationLogId( $objUserAuthenticationLog->getId() );
		}

		return true;
	}

	public function encryptSvnPassword() {
		return $this->setSvnPassword( CEncryption::encryptText( trim( $this->getSvnPassword() ), CONFIG_KEY_LOGIN_PASSWORD ) );
	}

	public function resetPassword() {
		$strPassword = $this->generatePassword();
		$this->setNewPassword( $strPassword );
		$this->setPassword( $strPassword );
	}

	public function generatePassword() {
		// Generate password with password complexity policy
		$strPassword		= md5( uniqid( mt_rand(), true ) );
		$strPassword		= \Psi\CStringService::singleton()->substr( $strPassword, 0, 8 );
		$intPasswordCount	= \Psi\CStringService::singleton()->strlen( $strPassword );

		// @TODO: Please remove magic numbers
		for( $intCount = 0; $intCount < $intPasswordCount; $intCount++ ) {
			if( ( int ) ord( $strPassword[$intCount] ) <= 122 && ( int ) ord( $strPassword[$intCount] ) >= 97 ) {
				$strPassword[$intCount] = \Psi\CStringService::singleton()->ucwords( $strPassword[$intCount] );
				break;
			}
		}

		$strPasswordPattern = '/^(?!.*([a-z\d])\1{1})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z\d\S]{8,}$/';

		if( 1 != \Psi\CStringService::singleton()->preg_match( $strPasswordPattern, $strPassword ) ) {
			$strPassword = $this->generatePassword();
		}

		return $strPassword;
	}

	public function allowAccessToUserSpecificUtilityModules( $intDepartmentId, $objAdminDatabase, $boolAllowRUDevelopmentUsers = false ) {
		$arrintDepartmentIds = [ $intDepartmentId ];
		if( true == $boolAllowRUDevelopmentUsers ) {
			$arrintDepartmentIds[] = CDepartment::DEVELOPMENT;
		}

		$arrintResidentUtilityQaUserIds = array_keys( ( array ) CUsers::fetchCurrentUserIdsByDepartmentIdsByOfficeIdsByPsProductId( $arrintDepartmentIds, [ COffice::TOWER_8, COffice::TOWER_9 ], CPsProduct::RESIDENT_UTILITY, $objAdminDatabase ) );

		if( true == in_array( $this->getId(), $arrintResidentUtilityQaUserIds ) ) {
			return true;
		}

		return false;
	}

	// Blog Wordpress functions.

	public function determineExistsInWordPress( $resWordPressDatabase ) {
		$strSql = 'SELECT * FROM wp_users WHERE lower( user_login ) = \'' . \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) . '\' LIMIT 1;';

		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			return false;
		}

		$intUserId = NULL;
		while( false !== ( $arrstrUsers = mysqli_fetch_array( $resResult, MYSQLI_ASSOC ) ) ) {
			$intUserId = $arrstrUsers['ID'];
			break;
		}

		if( true == is_numeric( $intUserId ) ) {
			return $intUserId;
		}

		return false;
	}

	public function resyncWordPressAuthentication( $resWordPressDatabase ) {
		$strSql = 'SELECT * FROM wp_users WHERE lower( user_login ) = \'' . \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) . '\' LIMIT 1;';

		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			return false;
		}

		$intWordPressUserId = NULL;

		$arrstrUsers = mysqli_fetch_array( $resResult, MYSQLI_ASSOC );

		if( true == valArr( $arrstrUsers ) ) {
			$intWordPressUserId = $arrstrUsers['ID'];
		}

		if( false == is_numeric( $intWordPressUserId ) ) {
			echo 'wp_user does not exist.';
			return false;
		}

		// Please Connect with Smishra01(2225) or Tbhatt for class-phpass.php
		// Hash the encrypted company user password, and truncate to 8 characters. Then use that password to apply to the word press users table.
		require_once( PATH_PHP_APPLICATIONS . 'Entrata/Library/WordPress/class-phpass.php' );

		$objWordPressHasher = new PasswordHash( 8, true );
		$strWordPressPassword = $objWordPressHasher->HashPassword( $this->generateBlogPassword() );

		$strSql = 'UPDATE wp_users SET user_pass = \'' . trim( $strWordPressPassword ) . '\' WHERE ID = ' . ( int ) $intWordPressUserId;
		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			return false;
		}

		$strSql = 'DELETE FROM wp_usermeta WHERE user_id = ' . ( int ) $intWordPressUserId . ' AND meta_key IN ( \'wp_capabilities\', \'wp_user_level\' ) ';
		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			return false;
		}

		$strWpCapabilities	= 'a:1:{s:11:\"contributor\";b:1;}';
		$intWpUserLevel		= 1;

		if( true == $this->getIsAdministrator() || true == $this->getIsSuperUser() ) {
			$strWpCapabilities	= 'a:1:{s:13:\"administrator\";b:1;}';
			$intWpUserLevel		= 10;
		}

		$strSql = 'INSERT INTO `wp_usermeta` (`user_id`, `meta_key`, `meta_value`)
						VALUES (' . $intWordPressUserId . ', \'wp_capabilities\', \'' . $strWpCapabilities . '\'),
								(' . $intWordPressUserId . ', \'wp_user_level\', \'' . $intWpUserLevel . '\')';

		if( false == mysqli_query( $resWordPressDatabase, $strSql ) ) {
			return false;
		}

		return true;
	}

	public function insertWordPressUser( $strWebsiteUrl, $resWordPressDatabase, $objDatabase ) {
		// Hash the encrypted company user password, and truncate to 8 characters. Then use that password to apply to the word press users table.
		require_once( PATH_PHP_APPLICATIONS . 'Entrata/Library/WordPress/class-phpass.php' );

		$objWordPressHasher		= new PasswordHash( 8, true );
		$strWordPressPassword	= $objWordPressHasher->HashPassword( $this->generateBlogPassword() );

		$objEmployee			= CEmployees::fetchEmployeeByUserId( $this->getId(), $objDatabase );
		$strEmailAddress		= ( true == valObj( $objEmployee, 'CEmployee' ) ) ? $objEmployee->getEmailAddress() : NULL;

		$strSql = 'INSERT INTO `wp_users` (`user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`)
					VALUES (\'' . \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) . '\',\'' . $strWordPressPassword . '\',\'' . \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) . '\',\'' . $strEmailAddress . '\',\'http://' . $strWebsiteUrl . '\',\'' . date( 'Y-m-d' ) . '\',\'\',0,\'' . \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) . '\'); ';
		if( false == mysqli_query( $resWordPressDatabase, $strSql ) ) {
			echo 'wp_user failed to insert.';
			echo $strSql;
			exit;
			return false;
		}

		$intWordPressUserId = NULL;

		$strSql = 'SELECT ID FROM wp_users WHERE user_login = \'' . \Psi\CStringService::singleton()->strtolower( $this->getUsername() ) . '\'';
		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			echo 'wp_user id failed to retrieve.';
			return false;
		}

		$arrstrUsers = mysqli_fetch_array( $resResult, MYSQLI_ASSOC );

		if( true == valArr( $arrstrUsers ) ) {
			$intWordPressUserId = $arrstrUsers['ID'];
		}

		if( false == is_numeric( $intWordPressUserId ) ) {
			echo 'wp_user does not exist.';
			return false;
		}

		$strWpCapabilities	= 'a:1:{s:11:\"contributor\";b:1;}';
		$intWpUserLevel		= 1;

		if( true == $this->getIsAdministrator() || true == $this->getIsSuperUser() ) {
			$strWpCapabilities	= 'a:1:{s:13:\"administrator\";b:1;}';
			$intWpUserLevel		= 10;
		}

		$strSql = 'INSERT INTO `wp_usermeta` (`user_id`, `meta_key`, `meta_value`)
						VALUES (' . $intWordPressUserId . ', \'wp_capabilities\', \'' . $strWpCapabilities . '\'),
								(' . $intWordPressUserId . ', \'wp_user_level\', \'' . $intWpUserLevel . '\')';

		if( false == mysqli_query( $resWordPressDatabase, $strSql ) ) {
			return false;
		}

		return $intWordPressUserId;
	}

	public function generateBlogPassword() {
		$strPassword = \Psi\CStringService::singleton()->substr( $this->getPasswordEncrypted(), 0, 8 );

		$strPassword = \Psi\CStringService::singleton()->substr( CHash::createService()->hashGeneric( $strPassword ), 0, 8 );

		return $strPassword;
	}

	public function fetchWordPressUserId( $resWordPressDatabase ) {

		if( false == valStr( $this->m_strWordPressUsername ) ) {
			return false;
		}

		$strSql = 'SELECT * FROM wp_users WHERE lower( user_login ) = \'' . \Psi\CStringService::singleton()->strtolower( $this->m_strWordPressUsername ) . '\' LIMIT 1;';

		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			return false;
		}

		$intWordPressUserId = NULL;

		$arrstrUsers = mysqli_fetch_array( $resResult, MYSQLI_ASSOC );

		if( true == valArr( $arrstrUsers ) ) {
			$intWordPressUserId = ( int ) $arrstrUsers['ID'];
		}

		return $intWordPressUserId;
	}

	public function deleteWordPressUser( $resWordPressDatabase ) {

		if( false == valId( $this->m_intWordPressUserId ) ) {
			return false;
		}

		$strSql = 'SELECT user_id FROM wp_usermeta WHERE user_id = ' . ( int ) $this->m_intWordPressUserId . ' AND meta_key IN ( \'wp_capabilities\', \'wp_user_level\' ) ';

		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			return false;
		}

		$arrstrUsers = mysqli_fetch_array( $resResult, MYSQLI_ASSOC );

		if( false == valArr( $arrstrUsers ) ) {
			return false;
		}

		$strSql = 'DELETE FROM wp_usermeta WHERE user_id = ' . ( int ) $this->m_intWordPressUserId . ' AND meta_key IN ( \'wp_capabilities\', \'wp_user_level\' ) ';

		if( false == ( $resResult = mysqli_query( $resWordPressDatabase, $strSql ) ) ) {
			return false;
		}

		return true;
	}

	public function activateWordPressUser( $boolIsAdministrator, $resWordPressDatabase ) {

		if( false == valId( $this->m_intWordPressUserId ) ) {
			return false;
		}

		$strWpCapabilities	= 'a:1:{s:13:\"administrator\";b:1;}';
		$intWpUserLevel		= 10;

		if( false == $boolIsAdministrator ) {
			$strWpCapabilities	= 'a:1:{s:11:\"contributor\";b:1;}';
			$intWpUserLevel		= 1;
		}

		$strSql = 'INSERT INTO `wp_usermeta` (`user_id`, `meta_key`, `meta_value`)
						VALUES (' . $this->m_intWordPressUserId . ', \'wp_capabilities\', \'' . $strWpCapabilities . '\'),
								(' . $this->m_intWordPressUserId . ', \'wp_user_level\', \'' . $intWpUserLevel . '\')';

		if( false == mysqli_query( $resWordPressDatabase, $strSql ) ) {
			return false;
		}

		return true;
	}

	public function loadNewProfileAccess() {
		$boolAccessNewProfile = false;
		if( $this->getIsSuperUser() || $this->getIsAdministrator() || $this->getEmployee()->getDepartmentId() == CDepartment::HR ) {
			$boolAccessNewProfile = true;
		}
		return $boolAccessNewProfile;
	}

	/**
	 * To check if user login requires password update
	 *
	 */

	public function checkIsPasswordRotation( $objAdminDatabase ) {
		$arrintRotationDays = CUserAuthenticationLogs::fetchLatestPasswordRotationDaysByUserId( $this->getId(), $objAdminDatabase );

		if( false == empty( $arrintRotationDays ) && false == is_null( $arrintRotationDays ) ) {
			if( 'production' == CONFIG_ENVIRONMENT && ( true == is_null( $arrintRotationDays[0]['last_login'] ) || true == is_null( $arrintRotationDays[0]['rotation_days'] ) || self::PASSWORD_ROTATION_DAYS <= ( int ) $arrintRotationDays[0]['rotation_days'] ) ) {
				$this->setIsPasswordRotation( true );
			}
		}

		unset( $arrintRotationDays );
	}

	public function checkIsShowExternalTools( $objAdminDatabase ) {
		$objEmployeePreference	= CEmployeePreferences::fetchEmployeePreferenceByEmployeeIdByKey( $this->getEmployeeId(), CEmployeePreference::IS_EXTERNAL_TOOLS_UPDATED, $objAdminDatabase );

		$strCurrentDate = new DateTime( getConvertedDateTimeByTimeZoneName( 'now', 'm/d/Y H:i:s', 'Asia/Kolkata' ) );
		$strReleaseDate = new DateTime( getConvertedDateTimeByTimeZoneName( self::EXTERNAL_TOOL_DATE, 'm/d/Y H:i:s', 'Asia/Kolkata' ) );
		$objInterval	= $strCurrentDate->diff( $strReleaseDate );

		( $objInterval->days >= self::EXTERNAL_TOOL_LOCK_DAYS && false == valObj( $objEmployeePreference, 'CEmployeePreference' ) ) ? $this->setIsExternalTools( true ) : $this->setIsExternalTools( false );
	}

	public function checkIsLaptopAssigned( $objAdminDatabase ) {
		$arrmixVpnForms                = [];
		$arrmixHardwareAllocationForms = [];
		$arrmixNotSignedAssets         = [];

		$arrobjEmployeePreferences	= \Psi\Eos\Admin\CEmployeePreferences::createService()->fetchEmployeePreferenceByEmployeeIdByKeys( $this->getEmployeeId(), [ CEmployeePreference::WORK_FORM_HOME_KEY, CEmployeePreference::VPN_ONLY ], $objAdminDatabase );
		if( true == valArr( $arrobjEmployeePreferences ) ) {
			$arrmixEmployeePreferences = rekeyArray( 'key', \Psi\Eos\Admin\CEmployeePreferences::createService()->fetchEmployeePreferencesByEmployeeIdByKeys( $this->getEmployeeId(), $objAdminDatabase, true ) );

			if( true == valArr( $arrmixEmployeePreferences ) ) {
				foreach( $arrmixEmployeePreferences as $arrmixEmployeePreference ) {
					if( true == in_array( $arrmixEmployeePreference['key'], [ CEmployeePreference::VPN_ONLY, CEmployeePreference::WORK_FORM_HOME_KEY ] ) && '1' == $arrmixEmployeePreference['value'] ) {
						$arrmixVpnForms[]        = $arrmixEmployeePreference;
						$arrmixNotSignedAssets[] = $arrmixEmployeePreference;
					}
					if( CEmployeePreference::WORK_FORM_HOME_KEY == $arrmixEmployeePreference['key'] && '1' == $arrmixEmployeePreference['value'] ) {
						$arrmixHardwareAllocationForms[] = $arrmixEmployeePreference;
						$arrmixNotSignedAssets[]         = $arrmixEmployeePreference;
					}
				}
			}

			( true == valArr( $arrmixVpnForms ) && false == array_key_exists( CEmployeePreference::ASSET_ID_FOR_VPN_POLICY_SIGNED, $arrmixEmployeePreferences ) ) ? $this->setIsVpnFormSigned( true ) : $this->setIsVpnFormSigned( false );
			( true == valArr( $arrmixHardwareAllocationForms ) && false == array_key_exists( CEmployeePreference::ASSET_ID_FOR_HARDWARE_ALLOCATION_FORM_SIGNED, $arrmixEmployeePreferences ) ) ? $this->setIsHardwareAllocationFormSigned( true ) : $this->setIsHardwareAllocationFormSigned( false );
			( true == valArr( $arrmixNotSignedAssets ) && false == array_key_exists( CEmployeePreference::ASSET_REMOTE_WFH_POLICY_SIGNED, $arrmixEmployeePreferences ) ) ? $this->setIsRemoteWfhFormSigned( true ) : $this->setIsRemoteWfhFormSigned( false );

		} else {
			$arrmixEmployeeAssets = rekeyArray( 'employee_asset_id', ( array ) \Psi\Eos\Admin\CEmployeeAssets::createService()->fetchEmployeeCurrentAssetByAssetTypeIdByEmployeeId( $this->getEmployeeId(), CPsAssetType::LAPTOP, $objAdminDatabase ) );

			if( true == valArr( $arrmixEmployeeAssets ) && false == is_null( $arrmixEmployeeAssets ) ) {
				$arrmixEmployeePreferences = CEmployeePreferences::fetchEmployeePreferencesByEmployeeIdByKeys( $this->getEmployeeId(), $objAdminDatabase );

				if( true == valArr( $arrmixEmployeePreferences ) ) {
					foreach( $arrmixEmployeePreferences as $arrmixEmployeePreference ) {
						if( 'ASSET_ID_FOR_VPN_POLICY_SIGNED' == $arrmixEmployeePreference['key'] ) {
							$arrmixVpnForms[] = $arrmixEmployeePreference;
						} elseif( 'ASSET_ID_FOR_HARDWARE_ALLOCATION_FORM_SIGNED' == $arrmixEmployeePreference['key'] ) {
							$arrmixHardwareAllocationForms[] = $arrmixEmployeePreference;
						}
					}
				}

				$arrmixNotSignedAssets = array_diff( array_keys( $arrmixEmployeeAssets ), array_keys( rekeyArray( 'value', $arrmixVpnForms ) ) );

				( true == valArr( $arrmixNotSignedAssets ) && 0 != \Psi\Libraries\UtilFunctions\count( array_filter( $arrmixNotSignedAssets ) ) && true == file_exists( PATH_MOUNTS_DOCUMENTS_EMPLOYEES . $this->getEmployeeId() . '/entrata_vpn_policy.pdf' ) ) ? $this->setIsVpnFormSigned( true ) : $this->setIsVpnFormSigned( false );

				$arrmixNotSignedAssets = array_diff( array_keys( $arrmixEmployeeAssets ), array_keys( rekeyArray( 'value', $arrmixHardwareAllocationForms ) ) );

				( true == valArr( $arrmixNotSignedAssets ) && 0 != \Psi\Libraries\UtilFunctions\count( array_filter( $arrmixNotSignedAssets ) ) && true == file_exists( PATH_MOUNTS_DOCUMENTS_EMPLOYEES . $this->getEmployeeId() . '/hardware_allocation_form.pdf' ) ) ? $this->setIsHardwareAllocationFormSigned( true ) : $this->setIsHardwareAllocationFormSigned( false );
			}
		}

	}

	/**
	 * To check if new password exists in set of previously used passwords
	 *
	 */

	public function isExistsNewPasswordInAuthenticationLogs( $objAdminDatabase ) {
		$boolExistsPassword			= false;
		$arrstrPreviousPasswords	= CUserAuthenticationLogs::fetchPreviousPasswordsByUserIdByRotationNumber( $this->getId(), self::PASSWORD_ROTATION_NUMBER, $objAdminDatabase );

		if( false == empty( $arrstrPreviousPasswords ) ) {
			foreach( $arrstrPreviousPasswords as $arrstrPreviousPassword ) {
				if( true == ( CCrypto::create() )->verifyHash( trim( $this->m_strNewPassword ), $arrstrPreviousPassword->getPreviousPassword() ) ) {
					$boolExistsPassword = true;
					break;
				}
			}
		}

		if( true == $boolExistsPassword ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'new_password', 'Password should be different from previously used ' . self::PASSWORD_ROTATION_NUMBER . ' passwords.' ) );
		}

		return $boolExistsPassword;
	}

	public function fetchGroupPermissions( $objDatabase ) {
		return CGroupPermissions::fetchGroupPermissionsByUserId( $this->getId(), $objDatabase );
	}

	public function fetchGroupRepositoryPermissions( $objDatabase ) {
		return \Psi\Eos\Admin\CGroupRepositoryPermissions::createService()->fetchGroupRepositoryPermissionsByUserId( $this->getId(), $objDatabase );
	}

	public function isManagingUsers( $strManageUserSessionData, $objSession, $objAdminDatabase ) {
		if( false == empty( $strManageUserSessionData ) && 0 < $strManageUserSessionData ) {
			return $this->m_boolIsManagingUser = true;
		} else {
			if( false == valObj( $this->getEmployee(), 'CEmployee' ) ) {
				$objSession->setValue( [ 'user', 'is_managing_users' ], 0 );
				return false;
			} else {
				$intManageEmployeesCount = $this->getEmployee()->isManagingUsers( $objAdminDatabase );

				if( $intManageEmployeesCount > 0 ) {
					$objSession->setValue( [ 'user', 'is_managing_users' ], $intManageEmployeesCount );
					return true;
				} else {
					$objSession->setValue( [ 'user', 'is_managing_users' ], 0 );
					return false;
				}
			}
		}

		return false;
	}

	/**
	 * To update the session click count when any request is generated in particular session.
	 *
	 */

	public function updateUserSessionClickCount( $objAdminDatabase ) {
		$boolIsUpdated = CUserAuthenticationLogs::updateUserSessionClickCount( $this->getUserAuthenticationLogId(), $objAdminDatabase );

		return $boolIsUpdated;
	}

	public function loadAndAssociateDefaultData( $objAdminDatabase ) {
		$objUser = CUsers::fetchUserDetailsById( $this->getId(), $objAdminDatabase );

		if( false == valObj( $objUser, 'CUser' ) ) {
			return false;
		}

		$this->m_objEmployee = $objUser->getOrFetchEmployee( $objAdminDatabase );

		$objUser->getOrFetchAssociatedGroupIds( $objAdminDatabase );
		$objUser->getOrFetchAssociatedRoleIds( $objAdminDatabase );

		$objEmployeeAddress = $this->m_objEmployee->fetchEmployeeAddressByAddressTypeId( CAddressType::PRIMARY, $objAdminDatabase );

		if( true == valObj( $objEmployeeAddress, 'CEmployeeAddress' ) ) {
			$this->m_objEmployee->setCountryCode( $objEmployeeAddress->getCountryCode() );
		}

		$objUser->setIsLoggedIn( true );

		$arrintEmployeePreferenceValue = $this->m_objEmployee->fetchEmployeePreferenceValueByKey( 'ENABLE_IST_TIMEZONE', $objAdminDatabase );

		if( true == valArr( $arrintEmployeePreferenceValue ) ) {
			$objUser->setEnableISTTimezone( current( $arrintEmployeePreferenceValue )['value'] );
		}

		return $objUser;
	}

	public function loadCallAgent( $objVoipDatabase ) {
		return CCallAgents::fetchCallAgentByUserId( $this->getId(), $objVoipDatabase );
	}

	public function syncUserToSso() {
		// Add code to sync user with sso here for update user
		require_once( PATH_APPLICATION_CLIENT_ADMIN . 'Library/Sso/CClientAdminSso.class.php' );
		$objSsoUser = new CClientAdminSso();
		return $objSsoUser->syncUserToSso( $this );
	}

	public function getMappedUsername() {

		return [
			'System User'                                           => __( 'System' ),
			'System'                                                => __( 'System' ),
			'release'                                               => __( 'System' ),
			'Unknown'                                               => __( 'System' ),
			'TenantPortal'                                          => 'Tenant Portal',
			'ReservationHub'                                        => 'Reservation Hub',
			'Applications'                                          => __( 'Applications' ),
			'client_admin'                                          => __( 'System' ),
			'dml_deployment'                                        => __( 'System' ),
			'rpwhitelabeledapp'                                     => 'White-Labled App',
			'Entrata'                                               => 'Entrata PaaS',
			'Prospect Portal'                                       => 'Prospect Portal',
			'Resident Portal'                                       => 'Resident Portal',
			'Resident Pay Desktop Edition'                          => 'Resident Pay Desktop',
			'Nacha Portal'                                          => __( 'System' ),
			'LeaseEditor'                                           => __( 'Lease Editor' ),
			'Scripts Layer'                                         => __( 'System' ),
			'Entrata Automated Initial Import'                      => __( 'Automated Initial Import' ),
			'Entratamation'                                         => 'Entratamation',
			'System Renewal User'                                   => __( 'Automated Renewal Offer ' ),
			'VendorAccess'                                          => 'Vendor Access',
			'Property Solutions'                                    => 'Entrata',
			'ResidentSync Client'                                   => __( 'System' ),
			'ResidentSync Server'                                   => __( 'System' ),
			'CPresentmentScript.class.php'                          => __( 'System' ),
			'CDisposalScript.class.php'                             => __( 'System' ),
			'CCcScript.class.php'                                   => __( 'System' ),
			'CRepgetScript.class.php'                               => __( 'System' ),
			'CPadeScript.class.php'                                 => __( 'System' ),
			'CVaultwareParser.class.php'                            => __( 'System' ),
			'CAutoPostChargesScript.class.php'                      => __( 'System Autopost Scheduled Charges' ),
			'CAutoPostPaymentsScript.class.php'                     => __( 'System Autopost Payments' ),
			'Resident Utility'                                      => 'Resident Utility',
			'CSurveyMassEnrollScript.class.php'                     => __( 'System' ),
			'CintegrationSetupScript.class.php'                     => __( 'System' ),
			'PMS Integration'                                       => __( 'PMS Integration' ),
			'CIntegrationQueueProcessorScript.class.php'            => __( 'PMS Integration' ),
			'SMS System'                                            => __( 'SMS System' ),
			'Insurance Portal'                                      => 'Resident Insure Portal',
			'Pricing System'                                        => 'Entrata Pricing',
			'entrataIlsApiUser'                                     => 'Entrata ILS API User',
			'ResidentVerify Portal'                                 => 'Resident Verify Portal',
			'CRentOptimizationScript.class.php'                     => __( 'System' ),
			'Pricing Auto Post'                                     => 'Entrata Pricing',
			'Pricing Clear Post'                                    => 'Entrata Pricing',
			'Pricing Advance Post'                                  => 'Entrata Pricing',
			'CComplianceDepotVendorManagementScript.class.php'      => __( 'System' ),
			'CImportYieldStarPricingScript.class.php'               => __( 'System' ),
			'entrataRvApiUser'                                      => 'Entrata RV API User'
		];
	}

}
?>
