<?php

class CTestQuestionTemplate extends CBaseTestQuestionTemplate {

	/**
	 * Validation Functions
	 *
	 */

	public function valTestQuestionTypeId() {
		 $boolIsValid = true;

		 if( true == is_null( $this->getTestQuestionTypeId() ) ) {
			 $boolIsValid = false;
			 $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_question_type_id', 'Question type is required.' ) );
		 }

		 return $boolIsValid;
	}

	public function valTestQuestionGroupId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTestQuestionGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_question_group_id', 'Question group is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTestQuestionTypeId();
				$boolIsValid &= $this->valTestQuestionGroupId();
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>