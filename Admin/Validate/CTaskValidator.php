<?php

use Psi\Eos\Admin\CPsProducts;

class CTaskValidator {

	protected $m_objTask;
	protected $m_objTaskDetail;
	protected $m_objTaskNote;
	private $m_intLoggedInUserDesignationId;

	const DAY_OF_WEEK_SATURDAY			= 6;
	const DAY_OF_WEEK_SUNDAY			= 7;
	const MAX_ROOT_CAUSE_DESCRIPTION	= 100;

	public function __construct() {
		return;
	}

	public function setTask( $objTask ) {
		$this->m_objTask = $objTask;
		return $this->m_objTask;

	}

	public function setTaskDetail( $objTaskDetail ) {
		$this->m_objTaskDetail = $objTaskDetail;
		return $this->m_objTaskDetail;
	}

	public function setLoggedInUserDesignationId( $intLoggedInUserDesignationId ) {
		$this->m_intLoggedInUserDesignationId = $intLoggedInUserDesignationId;
	}

	public function getLoggedInUserDesignationId() {
		return $this->m_intLoggedInUserDesignationId;
	}

	public function valQaApproved() {

		$boolIsValid = true;

		 if( true == is_null( $this->m_objTask->getQaApprovedBy() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_qa_approved', 'Please select QA Approved checkbox.' ) );
		 }

		return $boolIsValid;
	}

	public function valFollowUpDateTime( $objAdminDatabase ) {

		$boolIsValid = true;

		$strDate = date( 'Y-m-d', strtotime( $this->m_objTask->getFollowUpDateTime() ) );

		$strTime = date( 'H', strtotime( $this->m_objTask->getFollowUpDateTime() ) );

		if( false == is_null( $strDate ) ) {
			$arrintWeekends		= [ self::DAY_OF_WEEK_SUNDAY, self::DAY_OF_WEEK_SATURDAY ];

			$arrstrPaidHolidays = CPaidHolidays::fetchPaidHolidaysDateByCountryCode( CCountry::CODE_USA, $objAdminDatabase );
			if( false == is_null( $strDate ) && ( true == in_array( date( 'N', strtotime( $strDate ) ), $arrintWeekends ) || true == in_array( $strDate, $arrstrPaidHolidays ) ) ) {
				$boolIsValid = false;
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'follow_up_date_time', 'Follow-up date should not be sat/ sun/ paid holiday.' ) );
			}
		}

		if( false == is_null( $this->m_objTask->getFollowUpDateTime() ) && strtotime( 'now' ) >= strtotime( $this->m_objTask->getFollowUpDateTime() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'follow_up_date_time', 'Follow-up time should be greater than current time.' ) );
		}

		if( false == is_null( $strTime ) && ( ( $strTime < CTask::FOLLOW_UP_START_TIME || $strTime > CTask::FOLLOW_UP_END_TIME ) || ( CTask::FOLLOW_UP_END_TIME == $strTime && 0 != ( int ) date( 'i', strtotime( $this->m_objTask->getFollowUpDateTime() ) ) ) ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'follow_up_date_time', 'Follow-up time should be 6 AM to 6 PM.' ) );
		}

		return $boolIsValid;
	}

	public function valTaskTypeId() {

		$boolIsValid = true;

		if( false == valId( $this->m_objTask->getTaskTypeId() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_type_id', 'Task Type is required.' ) );

		}

		return $boolIsValid;
	}

	public function valTaskStatusId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objTask->getTaskStatusId() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_status_id', 'Status is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTaskPriorityId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objTask->getTaskPriorityId() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_priority_id', 'Priority is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUserId( $objDatabase, $objUser ) {
		$boolIsValid = true;
		$objEmployee = NULL;

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$objEmployee = CEmployees::fetchEmployeeByUserId( $this->m_objTask->getUserId(), $objDatabase );
		}

		if( CTaskStatus::READY_FOR_QA == $this->m_objTask->getTaskStatusId() || CTaskStatus::QA_IN_PROGRESS == $this->m_objTask->getTaskStatusId() ) {

			$arrobjUserGroup = CUserGroups::fetchUserGroupsByGroupId( CGroup::QA, $objDatabase );
			$arrobjUserGroup = rekeyObjects( 'UserId', $arrobjUserGroup );

			if( ( true == valObj( $objEmployee, 'CEmployee' ) && ( in_array( $objEmployee->getDepartmentId(), CDepartment::$c_arrintQaRequiredDepartmentIds ) || CDepartment::DATA_PROCESSING == $objEmployee->getDepartmentId() ) ) || ( true == valArr( $arrobjUserGroup ) && true == array_key_exists( $this->m_objTask->getUserId(), $arrobjUserGroup ) ) ) {
				if( CTaskType::BUG == $this->m_objTask->getTaskTypeId() || CTaskType::FEATURE == $this->m_objTask->getTaskTypeId() || CTaskType::AUTOMATION == $this->m_objTask->getTaskTypeId() || CTaskType::TEST_CASES == $this->m_objTask->getTaskTypeId() ) {

					if( true == is_null( $this->m_objTask->getCodeReviewedBy() ) && ( CTaskType::AUTOMATION != $this->m_objTask->getTaskTypeId() && CTaskType::TEST_CASES != $this->m_objTask->getTaskTypeId() ) ) {
						 $this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code_reviewed_by', 'Code reviewed by is required.' ) );
						$boolIsValid &= false;
					}
				}
			} else {

				if( true == valObj( $this->m_objTask, 'CTask' ) && CTaskType::AUTOMATION != $this->m_objTask->getTaskTypeId() && CTaskType::TEST_CASES != $this->m_objTask->getTaskTypeId() ) {
					$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'User Assigned must be QA.' ) );
					$boolIsValid &= false;
				}
			}
		}

		if( true == in_array( $this->m_objTask->getTaskStatusId(), array( CTaskStatus::READY_FOR_DEV, CTaskStatus::DEV_IN_PROGRESS, CTaskStatus::READY_FOR_RELEASE ) ) ) {
			$arrobjUserGroup = CUserGroups::fetchUserGroupsByGroupIds( array( CGROUP::DEVELOPMENT, CGROUP::INFORMATION_SECURITY ), $objDatabase );
			$arrobjUserGroup = rekeyArray( 'user_id', $arrobjUserGroup );
			if( ( true == valObj( $objEmployee, 'CEmployee' ) && CDepartment::QA == $objEmployee->getDepartmentId() ) || ( true == valArr( $arrobjUserGroup ) && false == array_key_exists( $this->m_objTask->getUserId(), $arrobjUserGroup ) && false == in_array( $this->m_objTask->getUserId(), CTaskReferenceType::$c_arrintReferenceTypesForHelpdeskSystem ) ) ) {
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'User Assigned must be Developer.' ) );
				$boolIsValid &= false;
			}
		}

		if( false == is_numeric( $this->m_objTask->getUserId() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'Assigned To is required. ' ) );
		}

		// Restrict all user to assign task to David Bateman(User id is 100)
		if( CUser::ID_DAVID_BATEMAN == $this->m_objTask->getUserId() ) {
			$objEmployee = CEmployees::fetchEmployeeByUserId( CUser::ID_DAVID_BATEMAN, $objDatabase );
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'You can not assign task to ' . $objEmployee->getNameFirst() . ' ' . $objEmployee->getNameLast() . '.' ) );
		}

		$arrobjUsers = \Psi\Eos\Admin\CUsers::createService()->fetchUsersByRoleId( CRole::SCHEMA_CHANGE_ACCESS, $objDatabase );

		if( true == valObj( $this->m_objTask, 'CTask' ) && CTaskType::SCHEMA_CHANGE == $this->m_objTask->getTaskTypeId() && CTaskStatus::SUBMITTED_FOR_APPROVAL == $this->m_objTask->getTaskStatusId() ) {
			$arrintDOEandArchitectEmployeeIds = array_column( CEmployees::fetchEmployeesDataByDesignationIds( CDesignation::$c_arrintSchemaApprovalDesignationIds, $objDatabase ), 'user_id' );
			$objAssignedUser = CUsers::fetchUserById( $this->m_objTask->getUserId(), $objDatabase );

			if( ( true == valObj( $objAssignedUser, 'CUser' ) && false == $objAssignedUser->getIsSuperUser() ) && false == in_array( $this->m_objTask->getUserId(), $arrintDOEandArchitectEmployeeIds ) && false == valArrKeyExists( $arrobjUsers, $this->m_objTask->getUserId() ) ) {
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'User Assigned must be Architect or DOE/who is having access to Schema Change Approval role.' ) );
				$boolIsValid &= false;
			}
		}

		if( false == $this->m_objTask->getSchemaApproved() && CTaskStatus::APPROVED == $this->m_objTask->getTaskStatusId() && CTaskType::SCHEMA_CHANGE == $this->m_objTask->getTaskTypeId() && false == in_array( $this->getLoggedInUserDesignationId(), CDesignation::$c_arrintSchemaApprovalDesignationIds ) && false == valArrKeyExists( $arrobjUsers, $objUser->getId() ) ) {
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'User Approving task must be Architect or DOE.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valTitle( $strValidationForField = NULL ) {
		$boolIsValid = true;
		$arrintWithoutTitleTaskTypes = array(
			CTaskType::BUG,
			CTaskType::FEATURE,
			CTaskType::TO_DO,
			CTaskType::PROJECT,
			CTaskType::TEST_CASES
		);

		if( false == is_null( $this->m_objTask->getTaskTypeId() ) && false == in_array( $this->m_objTask->getTaskTypeId(), $arrintWithoutTitleTaskTypes ) && true == is_null( $this->m_objTask->getTitle() ) ) {
			$boolIsValid = false;
			$strValidationForField = ( true == is_null( $strValidationForField ) ) ? ( ( CTaskType::EVENTS == $this->m_objTask->getSubTaskTypeId() ) ? 'Event Name' : 'Title' ) : $strValidationForField;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', $strValidationForField . ' is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valTaskReleaseId( $objDatabase = NULL, $boolIsRequired = false ) {

		$boolIsValid = true;

		if( CTaskStatus::READY_FOR_DEV == $this->m_objTask->getTaskStatusId() ) {
			return true;
		}

		if( true == $boolIsRequired ) {
			if( false == in_array( $this->m_objTask->getUserId(), CTaskReferenceType::$c_arrintReferenceTypesForHelpdeskSystem ) && true == is_null( $this->m_objTask->getTaskReleaseId() ) && $this->m_objTask->getTaskStatusId() != CTaskStatus::AWAITING_RESPONSE && ( true == in_array( $this->m_objTask->getTaskTypeId(), [ CTaskType::BUG, CTaskType::DESIGN, CTaskType::SCHEMA_CHANGE, CTaskType::FEATURE, CTaskType::AUTOMATION, CTaskType::TEST_CASES, CTaskType::PROJECT ] ) ) && false == in_array( $this->m_objTask->getTaskStatusId(), array( CTaskStatus::UNUSED, CTaskStatus::UNDER_REVIEW, CTaskStatus::ON_HOLD, CTaskStatus::CANCELLED, CTaskStatus::AWAITING_CUSTOMER_RESPONSE, CTaskStatus::BACKLOG ) ) ) {
				$boolIsValid = false;
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_release_id', 'Release date is required.' ) );
			}
		}

		if( false == is_null( $objDatabase ) && false == is_null( $this->m_objTask->getTaskReleaseId() ) ) {
			$objTaskRelease = CTaskReleases::fetchTaskReleaseById( $this->m_objTask->getTaskReleaseId(), $objDatabase );

			if( false == in_array( $this->m_objTask->getTaskTypeId(), array( CTaskType::QUESTION_RESEARCH, CTaskType::OUTAGE, CTaskType::IT, CTaskType::FEATURE, CTaskType::BUG ) ) && false == in_array( $this->m_objTask->getTaskStatusId(), array( CTaskStatus::CANCELLED, CTaskStatus::COMPLETED, CTaskStatus::READY_FOR_RELEASE, CTaskStatus::RELEASED_ON_STANDARD, CTaskStatus::VERIFIED_ON_STANDARD, CTaskStatus::RELEASED_ON_RAPID, CTaskStatus::VERIFIED_ON_RAPID, CTaskStatus::COMMITTED ) )
				&& false == is_null( $objTaskRelease ) && 1 == $objTaskRelease->getIsReleased() ) {
				$boolIsValid = false;
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_release_id', 'The selected sprint is marked as Released.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valProjectManagerId() {

		$boolIsValid = true;
		if( true == is_null( $this->m_objTask->getProjectManagerId() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'project_manager_id', 'Product development manager is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId( $objDatabase = NULL, $arrobjPsProducts = NULL ) {

		$boolIsValid = true;

		if( ( false == $this->m_objTask->getIsPsProductNotRequired() && true == in_array( $this->m_objTask->getTaskTypeId(), array( CTaskType::BUG, CTaskType::FEATURE, CTaskType::PROJECT ) ) && false == in_array( $this->m_objTask->getUserId(), CTaskReferenceType::$c_arrintReferenceTypesForHelpdeskSystem ) )
		|| ( CTaskType::SUPPORT == $this->m_objTask->getTaskTypeId() && CTaskMedium::LIVE_CHAT == $this->m_objTask->getTaskMediumId() ) ) {

			if( true == is_null( $this->m_objTask->getPsProductId() ) ) {
				$boolIsValid = false;
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is required. ' ) );
			}

			if( true == valObj( $objDatabase, 'CDatabase' ) && false == is_null( $this->m_objTask->getPsProductId() ) ) {

				if( false == valArr( $arrobjPsProducts ) ) {
					$arrobjPsProducts = CPsProducts::createService()->fetchAllPsProducts( $objDatabase );
				}

				if( true == valArr( $arrobjPsProducts ) ) {
					if( false == in_array( $this->m_objTask->getPsProductId(), array_keys( $arrobjPsProducts ) ) ) {
						$boolIsValid = false;
						$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Please enter valid product.' ) );
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function valTaskwithUser() {
		$boolIsValid = true;
		if( $this->m_objTask->getTaskTypeId() != CTaskType::FEATURE && true == in_array( $this->m_objTask->getUserId(), CTaskReferenceType::$c_arrintReferenceTypesForHelpdeskSystem ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_type_id', 'Only Feature task can be assigned to Helpdesk.' ) );
		}
		return $boolIsValid;
	}

	public function valAssignThisRcaTo() {
		$boolIsValid = true;

		if( false == is_numeric( $this->m_objTask->getUserId() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'Assigned This RCA To is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valDeveloperStoryPoints() {

		$boolIsValid = true;

		if( true == in_array( $this->m_objTask->getTaskTypeId(), array( CTaskType::BUG, CTaskType::FEATURE, CTaskType::DESIGN, CTaskType::QUESTION_RESEARCH, CTaskType::HELPDESK ) ) ) {

			if( true == valObj( $this->m_objTask->getTaskDetail(), 'CTaskDetail' ) && ( CTaskDetail::MIN_STANDARD_STORY_POINTS > $this->m_objTask->getTaskDetail()->getDeveloperStoryPoints() || CTaskDetail::MAX_STANDARD_STORY_POINTS < $this->m_objTask->getTaskDetail()->getDeveloperStoryPoints() ) ) {
				$boolIsValid = false;
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'developer_story_points', 'Developer story Points must be between 0 to 50.' ) );
			}

			if( true == valObj( $this->m_objTask->getTaskDetail(), 'CTaskDetail' ) && ( CTaskDetail::MIN_STANDARD_STORY_POINTS > $this->m_objTask->getTaskDetail()->getAuditStoryPoints() || CTaskDetail::MAX_STANDARD_STORY_POINTS < $this->m_objTask->getTaskDetail()->getAuditStoryPoints() ) ) {
				$boolIsValid = false;
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'audit_story_points', 'Audit Story Points must be between 0 to 50.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valQaStoryPoints() {
		$boolIsValid = true;

		if( true == in_array( $this->m_objTask->getTaskTypeId(), array( CTaskType::BUG, CTaskType::FEATURE, CTaskType::DESIGN, CTaskType::QUESTION_RESEARCH ) ) ) {

			if( true == valObj( $this->m_objTask->getTaskDetail(), 'CTaskDetail' ) && ( CTaskDetail::MIN_STANDARD_STORY_POINTS > $this->m_objTask->getTaskDetail()->getQaStoryPoints() || CTaskDetail::MAX_STANDARD_STORY_POINTS < $this->m_objTask->getTaskDetail()->getQaStoryPoints() ) ) {
				$boolIsValid = false;
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'qa_story_points', 'QA story Points must be between 0 to 50.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valTaskImpactType() {

		$boolIsValid = true;

		if( false == valObj( $this->m_objTask->getTaskDetail(), 'CTaskDetail' ) || NULL == $this->m_objTask->getTaskDetail()->getTaskImpactTypeId() && CTaskStatus::CANCELLED != $this->m_objTask->getTaskStatusId() ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_impact', 'Task Impact is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSeverityCount() {
		$boolIsValid = true;

		if( true == valObj( $this->m_objTask->getTaskDetail(), 'CTaskDetail' ) && true == is_null( $this->m_objTask->getTaskDetail()->getSeverityCount() ) && CTaskStatus::COMPLETED == $this->m_objTask->getTaskStatusId() ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_detail_severity_count', 'Severity count is required.' ) );
			return $boolIsValid;
		}

		if( true == valObj( $this->m_objTask->getTaskDetail(), 'CTaskDetail' ) && false == is_null( $this->m_objTask->getTaskDetail()->getSeverityCount() ) && 1 < $this->m_objTask->getTaskDetail()->getSeverityCount() && true == \Psi\CStringService::singleton()->preg_match( '/\d*(?:\.\d+)/', $this->m_objTask->getTaskDetail()->getSeverityCount() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_detail_severity_count', 'Invalid severity count, float value is allow below 1.' ) );
			return $boolIsValid;
		}

		if( true == valObj( $this->m_objTask->getTaskDetail(), 'CTaskDetail' ) && false == is_null( $this->m_objTask->getTaskDetail()->getSeverityCount() ) && 1 > $this->m_objTask->getTaskDetail()->getSeverityCount() && false == \Psi\CStringService::singleton()->preg_match( '/^\d*(\.{1}\d{1,2}){0,1}$/', $this->m_objTask->getTaskDetail()->getSeverityCount() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_detail_severity_count', 'Severity count should not be more than two decimal spaces.' ) );
			return $boolIsValid;
		}

		if( true == valObj( $this->m_objTask->getTaskDetail(), 'CTaskDetail' ) && CTaskStatus::COMPLETED == $this->m_objTask->getTaskStatusId() && ( 0 > $this->m_objTask->getTaskDetail()->getSeverityCount() || 1000 < $this->m_objTask->getTaskDetail()->getSeverityCount() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_detail_severity_count', 'Severity count must be between 0 to 1000.' ) );
		} elseif( true == valObj( $this->m_objTask->getTaskDetail(), 'CTaskDetail' ) && ( 0 > $this->m_objTask->getTaskDetail()->getSeverityCount() || 1000 < $this->m_objTask->getTaskDetail()->getSeverityCount() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_detail_severity_count', 'Severity count must be between 0 to 1000.' ) );
		}

		return $boolIsValid;
	}

	// Validate task details for David Bateman(User id = 100)

	public function valTaskDetail( $objDatabase ) {
		$boolIsValid = true;

		// Restrict any user to assign task to David Bateman as developer or QA
		if( true == valObj( $this->m_objTask, 'CTask' ) && true == valObj( $this->m_objTask->getTaskDetail(), 'CTaskDetail' ) ) {
			if( true == in_array( CUser::ID_DAVID_BATEMAN, array( $this->m_objTask->getTaskDetail()->getDeveloperEmployeeId(), $this->m_objTask->getTaskDetail()->getQaEmployeeId() ) ) ) {
				$boolIsValid = false;
				$objEmployee = CEmployees::fetchEmployeeByUserId( CUser::ID_DAVID_BATEMAN, $objDatabase );
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'You can not assign task to ' . $objEmployee->getNameFirst() . ' ' . $objEmployee->getNameLast() . '.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valSubTaskTypes( $boolHasSubTaskType, $objDatabase = NULL ) {

		$boolIsValid = true;

		if( true == $this->m_objTask->getAutomationFieldRequired() && false == $boolHasSubTaskType ) {
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_task_type_user_id', 'Please select atleast one sub task type.' ) );
			$boolIsValid = false;
		}

		if( CTaskType::FEATURE == $this->m_objTask->getTaskTypeId() && CTaskStatus::COMPLETED == $this->m_objTask->getTaskStatusId() ) {

			$objSubTask = \Psi\Eos\Admin\CTasks::createService()->fetchSubTaskByParentTaskIdByTaskTypeId( $this->m_objTask->getId(), array( CTaskType::TEST_CASES ), $objDatabase );
			if( true == valObj( $objSubTask, 'CTask' ) && CTaskStatus::COMPLETED != $objSubTask->getTaskStatusId() ) {
				if( CTaskStatus::CANCELLED == $objSubTask->getTaskStatusId() ) {
					$boolIsValid = true;
				} else {
					$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_task_type_id', ' You need to complete test case task ( id : ' . $objSubTask->getId() . ' ) before completing the parent task.' ) );
					$boolIsValid &= false;
				}
			}
		}

		return $boolIsValid;
	}

	public function valTaskAssociations( $arrobjTaskAssociations, $objAdminDatabase, $boolHasSubTaskType = false ) {

		$boolIsValid = true;

		if( false == valArr( $arrobjTaskAssociations ) ) return $boolIsValid;

		foreach( $arrobjTaskAssociations as $objTaskAssociation ) {
			if( true == valObj( $objTaskAssociation, 'CTaskAssociation' ) && false == $objTaskAssociation->validate( VALIDATE_INSERT, $objAdminDatabase, $boolHasSubTaskType ) ) {
				$this->m_objTask->addErrorMsgs( $objTaskAssociation->getErrorMsgs() );
				return false;
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {

		$boolIsValid = true;

		$strDescription	= str_replace( '&nbsp;', '', str_replace( '<br />', '', $this->m_objTask->getDescription() ) );
		$strDescription = strip_tags( html_entity_decode( nl2br( trim( $strDescription ) ), ENT_QUOTES ) );

		if( 0 >= \Psi\CStringService::singleton()->strlen( $strDescription ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required. ' ) );
		}

		$boolValidDescription = CTask::validateCreditCardNumber( $this->m_objTask->getDescription() );

		if( false == $boolValidDescription ) {
			$boolIsValid &= false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Please remove credit card number from task description.' ) );
		}

		return $boolIsValid;
	}

	public function valRootCauseDescription() {

		$boolIsValid = true;

		$strDescription	= str_replace( '&nbsp;', '', str_replace( '<br />', '', $this->m_objTask->getDescription() ) );
		$strDescription = strip_tags( html_entity_decode( nl2br( trim( $strDescription ) ), ENT_QUOTES ) );

		if( 0 >= isset( $strDescription[0] ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Root Cause is required. ' ) );
		} elseif( ( true == valStr( $strDescription ) ) && ( self::MAX_ROOT_CAUSE_DESCRIPTION > isset( $strDescription[self::MAX_ROOT_CAUSE_DESCRIPTION] ) ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Root Cause is not Descriptive, please provide minimum ' . self::MAX_ROOT_CAUSE_DESCRIPTION . ' characters for Root Cause Description. ' ) );
		}

		$boolValidDescription = CTask::validateCreditCardNumber( $this->m_objTask->getDescription() );

		if( false == $boolValidDescription ) {
			$boolIsValid &= false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Please remove credit card number from task description.' ) );
		}

		return $boolIsValid;
	}

	public function valDueDate() {

		$boolIsValid = true;

		if( 'mm/dd/yyyy' == $this->m_objTask->getDueDate() || '' == $this->m_objTask->getDueDate() ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'due_date', 'Due date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSdmSprintDate( $objUser ) {

		if( false == valObj( $objUser, 'CUser' ) ) return;

		$boolIsValid = true;

		if( true == in_array( CGroup::PRODUCT_MANAGERS, $objUser->getAssociatedGroupIds() ) || true == $objUser->getIsSuperUser() ) {
			if( ( true == is_null( $this->m_objTask->getPdmTaskReleaseId() ) || false == is_numeric( $this->m_objTask->getPdmTaskReleaseId() ) ) && true == is_numeric( $this->m_objTask->getTaskReleaseId() ) && true == in_array( $this->m_objTask->getTaskTypeId(), CTaskType::$c_arrintTaskTypesForDelayedTasks ) ) {
				$boolIsValid &= false;
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'SDM Sprint Date is required.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valReleaseNote() {

		$boolIsValid = true;

		if( true == valObj( $this->m_objTaskDetail, 'CTaskDetail' ) && NULL == $this->m_objTaskDetail->getTaskImpactTypeId() && CTaskStatus::CANCELLED != $this->m_objTask->getTaskStatusId() ) {
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_detail[task_impact_type_id]', 'Task Impact is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valReleaseDate( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $objAdminDatabase ) ) return $boolIsValid;

		$objTaskRelease = CTaskReleases::fetchTaskReleaseById( $this->m_objTask->getTaskReleaseId(), $objAdminDatabase );

		if( CCluster::RAPID == $this->m_objTask->getClusterId() && true == valObj( $objTaskRelease, 'CTaskRelease' ) && CCluster::STANDARD == $objTaskRelease->getClusterId() ) {
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_release_id', 'Please select release date from Rapid Release.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valTaskReviewedBy() {

		$boolIsValid = true;

		if( true == is_null( $this->m_objTask->getCodeReviewedBy() ) && CTaskStatus::COMMITTED == $this->m_objTask->getTaskStatusId() && true == in_array( $this->m_objTask->getTaskTypeId(), array( CTaskType::AUTOMATION, CTaskType::TEST_CASES ) ) ) {
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'code_reviewed_by', 'Reviewed by is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function loadTaskDetails( $objAdminDatabase ) {
		$objTaskDetail 	= \Psi\Eos\Admin\CTaskDetails::createService()->fetchTaskDetailByTaskId( $this->m_objTask->getId(), $objAdminDatabase );
		return $objTaskDetail;
	}

	public function valClusterId( $boolTrackTask, $objAdminDatabase ) {

		$boolIsValid = true;

		if( CTaskType::BUG == $this->m_objTask->getTaskTypeId() ) {
			if( 0 >= \Psi\Libraries\UtilFunctions\count( $this->m_objTask->getClusterId() ) ) {
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cluster_id', 'Please select where was the bug found.' ) );
				$boolIsValid &= false;
			}
		}

		if( CTaskType::FEATURE == $this->m_objTask->getTaskTypeId() && false == in_array( $this->m_objTask->getUserId(), CTaskReferenceType::$c_arrintReferenceTypesForHelpdeskSystem ) ) {
			if( 0 >= \Psi\Libraries\UtilFunctions\count( $this->m_objTask->getClusterId() ) ) {
				$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cluster_id', 'Please select where will be the feature release.' ) );
				$boolIsValid &= false;
			}
		}

		if( false == is_null( $this->m_objTask->getClusterId() ) && true == $boolTrackTask ) {
			$objTask = \Psi\Eos\Admin\CTasks::createService()->fetchTaskById( $this->m_objTask->getId(), $objAdminDatabase );
			$strClusterName = ( CCluster::RAPID == $objTask->getClusterId() )? 'standard track.' : 'rapid track.';
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cluster_id', 'Task is already created for ' . $strClusterName ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valReferenceId( $boolReferenceIdRequired, $boolReferenceIdTask, $boolReferenceIdExist ) {

		$boolIsValid = true;

		if( true == $boolReferenceIdRequired ) {
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference_id', 'Please enter System Error ID.' ) );
			$boolIsValid &= false;
		}

		if( true == $boolReferenceIdTask ) {
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference_id', 'Task is already associated to given System Error ID.' ) );
			$boolIsValid &= false;
		}

		if( false == $boolReferenceIdExist ) {
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reference_id', 'System Error ID is incorrect.' ) );
			$boolIsValid &= false;
		}

		return $boolIsValid;
	}

	public function valPrimaryContactPhoneNumber() {

		$boolIsValid = true;

		if( true == $this->m_objTask->getIsPublished() && false == is_null( $this->m_objTask->getContactPhoneNumber() ) && ( ( \Psi\CStringService::singleton()->strlen( $this->m_objTask->getContactPhoneNumber() ) < 10 ) || false == ( CValidation::validateFullPhoneNumber( $this->m_objTask->getContactPhoneNumber(), false ) ) ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_phone_number', 'Invalid contact phone number.' ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryContactPhoneExtension() {

		$boolIsValid = true;

		if( false == is_null( $this->m_objTask->getPhoneExtension() ) && true == is_numeric( $this->m_objTask->getPhoneExtension() ) && ( true == is_null( $this->m_objTask->getContactPhoneNumber() ) || '' == $this->m_objTask->getContactPhoneNumber() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_phone_number', 'Contact phone number required.' ) );
		}

		if( false == is_null( $this->m_objTask->getPhoneExtension() ) && false == is_numeric( $this->m_objTask->getPhoneExtension() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'extension', 'Invalid Extension.' ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryContactEmailAddress() {

		$boolIsValid = true;

		if( false == is_null( $this->m_objTask->getContactEmailAddress() ) && false == CValidation::validateEmailAddresses( $this->m_objTask->getContactEmailAddress() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contact_email_address', 'Invalid contact email address.' ) );
		}

		return $boolIsValid;
	}

	public function valShareWithClients() {
		$boolIsValid = true;
		if( true == is_null( $this->m_objTask->getCid() ) && true == $this->m_objTask->getIsPublished() ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'client_name', 'Client name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valHelpdeskUserId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->m_objTask->getUserId() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'Helpdesk User is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valGroupId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->m_objTask->getGroupId() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'group_id', 'Assigned To is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valSubTaskTypeId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->m_objTask->getSubTaskTypeId() ) ) {
			$boolIsValid = false;
			$strSubTypeValidate = ( CGroup::IT_SUPPORT == $this->m_objTask->getGroupId() ) ? 'Issue type is required. ' : 'Ticket type is required. ';
			$strMessage = ( CTaskType::HELPDESK == $this->m_objTask->getTaskTypeId() ) ? $strSubTypeValidate : 'Sub Type is required. ';
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sub_task_type_id', $strMessage ) );
		}
		return $boolIsValid;
	}

	public function valTaskCategoryId() {
		$boolIsValid = true;

		if( true == valObj( $this->m_objTaskDetail, 'CTaskDetail' ) && false == valId( $this->m_objTaskDetail->getTaskCategoryId() ) ) {
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_detail[task_parent_category_id]', 'Task Category is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->m_objTask->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', ' Property is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valTaskMediumId() {
		$boolIsValid = true;
		if( false == is_numeric( $this->m_objTask->getTaskMediumId() ) ) {
			$boolIsValid = false;
			$this->m_objTask->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_medium_id', ' Prefers Contact is required. ' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsRequired = false, $objAdminDatabase = NULL, $objUser = NULL, $arrobjPsProducts = NULL, $boolHasSubTaskType = false, $arrobjTaskAssociations = NULL, $boolTrackTask = false, $boolReferenceIdRequired = false, $boolReferenceIdTask = false, $boolReferenceIdExist = true, $boolIsQuickTask = false, $boolIsQaRequired = false, $boolSharedWithClient = false ) {

		$boolIsValid					= true;
		switch( $strAction ) {

			case VALIDATE_INSERT:
					if( true == $boolIsQuickTask ) {
						if( $this->m_objTask->getTaskTypeId() == CTaskType::BUG || $this->m_objTask->getTaskTypeId() == CTaskType::FEATURE ) {
							$boolIsValid &= $this->valClusterId( $boolTrackTask, $objAdminDatabase );
						}
					} else {
						$boolIsValid &= $this->valTitle();
					}
					$boolIsValid &= $this->valTaskTypeId();
					$boolIsValid &= $this->valDescription();
					$boolIsValid &= $this->valPsProductId();
					$boolIsValid &= $this->valUserId( $objAdminDatabase, $objUser );
					$boolIsValid &= $this->valTaskwithUser();

					if( CTaskType::TO_DO == $this->m_objTask->getTaskTypeId() || true == in_array( $this->m_objTask->getSubTaskTypeId(), array( CTaskType::CREATIVE_DESIGN, CTaskType::VIDEOS ) ) ) {
						$boolIsValid &= $this->valDueDate();
					}

					if( CTaskType::BUG == $this->m_objTask->getTaskTypeId() ) {
						$boolIsValid &= $this->valReferenceId( $boolReferenceIdRequired, $boolReferenceIdTask, $boolReferenceIdExist );
					}

					if( true == in_array( $this->m_objTask->getTaskTypeId(), array( CTaskType::MARKETING_CREATIVE_SERVICES, CTaskType::DESIGN ) ) ) {
						$boolIsValid &= $this->valSubTaskTypeId();
					}
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle();

				if( CTaskType::BUG == $this->m_objTask->getTaskTypeId() && false == is_null( $this->m_objTask->getClusterId() ) && CCluster::MOBILE != $this->m_objTask->getClusterId() ) {
					$boolIsValid &= $this->valReferenceId( $boolReferenceIdRequired, $boolReferenceIdTask, $boolReferenceIdExist );
				}

				if( false == $boolReferenceIdTask ) {
					$boolIsValid &= $this->valPsProductId( $objAdminDatabase, $arrobjPsProducts );
				} else {
					$boolIsValid &= $this->valTaskwithUser();
				}
				$boolIsValid &= $this->valUserId( $objAdminDatabase, $objUser );
				if( true == valObj( $objUser, 'CUser' ) && false == $boolIsQaRequired && false == $objUser->getIsSuperUser() && ( true == in_array( $objUser->getEmployee()->getDepartmentId(), [ CDepartment::DEVELOPMENT, CDepartment::QA, CDepartment::DATA_PROCESSING ] ) || true == in_array( CGroup::QA, $objUser->getAssociatedGroupIds() ) ) && true == in_array( $this->m_objTask->getTaskStatusId(), array( CTaskStatus::READY_FOR_RELEASE, CTaskStatus::VERIFIED_ON_RAPID, CTaskStatus::VERIFIED_ON_STANDARD, CTaskStatus::VERIFIED_ON_STAGE, CTaskStatus::RELEASED, CTaskStatus::VERIFIED ) ) && false == in_array( $this->m_objTask->getTaskTypeId(), array( CTaskType::AUTOMATION, CTaskType::TEST_CASES, CTaskType::MIGRATION ) ) ) {
					$boolIsValid &= $this->valQaApproved();
				}

				if( false == in_array( $this->m_objTask->getTaskTypeId(), [ CTaskType::SUPPORT, CTaskType::RP_FEEDBACK ] ) ) {
					$boolIsValid &= $this->valSdmSprintDate( $objUser );
					$boolIsValid &= $this->valTaskDetail( $objAdminDatabase );
				}

				if( CTaskType::SUPPORT == $this->m_objTask->getTaskTypeId() && true == is_numeric( $this->m_objTask->getCid() ) && true == $this->m_objTask->getIsPublished() ) {
					$boolIsValid &= $this->valFollowUpDateTime( $objAdminDatabase );
				}

				if( false == $boolReferenceIdTask ) {
					if( CCluster::MOBILE != $this->m_objTask->getClusterId() ) {
						$boolIsValid &= $this->valClusterId( $boolTrackTask, $objAdminDatabase );
					}
					$boolIsValid &= $this->valTaskPriorityId();
					$boolIsValid &= $this->valTaskTypeId();
					$boolIsValid &= $this->valTaskStatusId();
					$boolIsValid &= $this->valTaskReleaseId( $objAdminDatabase, $boolIsRequired );
					$boolIsValid &= $this->valDeveloperStoryPoints();
					$boolIsValid &= $this->valQaStoryPoints();
					$boolIsValid &= $this->valSubTaskTypes( $boolHasSubTaskType, $objAdminDatabase );
					$boolIsValid &= $this->valTaskAssociations( $arrobjTaskAssociations, $objAdminDatabase, $boolHasSubTaskType );
					$boolIsValid &= $this->valTaskReviewedBy();
					$boolIsValid &= $this->valReleaseDate( $objAdminDatabase );
					$boolIsValid &= $this->valTaskwithUser();
				}

				if( CTaskType::BUG == $this->m_objTask->getTaskTypeId() ) {
					$boolIsValid &= $this->valSeverityCount();
				}
				if( true == $this->m_objTask->getIsSdm() && ( CTaskType::FEATURE == $this->m_objTask->getTaskTypeId() || CTaskType::BUG == $this->m_objTask->getTaskTypeId() ) ) {
					$boolIsValid &= $this->valTaskImpactType();
				}
				if( true == in_array( $this->m_objTask->getSubTaskTypeId(), array( CTaskType::CREATIVE_DESIGN, CTaskType::VIDEOS ) ) ) {
					$boolIsValid &= $this->valDueDate();
				}
				break;

			case 'update_task_title':
				$boolIsValid = $this->valTitle();
				break;

			case 'update_task_description':
				$boolIsValid = $this->valDescription();
				break;

			case 'insert_root_cause_type':
				$boolIsValid &= $this->valRootCauseDescription();
			case 'update_root_cause_type':
				$boolIsValid &= $this->valAssignThisRcaTo();
				break;

			case 'validate_release_note':
				$boolIsValid &= $this->valReleaseNote( $objAdminDatabase );
				break;

			case 'validate_release_date':
				$boolIsValid &= $this->valReleaseDate( $objAdminDatabase );
				break;

			case 'update_task_primary_contact':
				if( true == $boolSharedWithClient ) {
					$boolIsValid &= $this->valShareWithClients();
					$boolIsValid &= $this->valPropertyId();
					$boolIsValid &= $this->valTaskMediumId();
				}
				$boolIsValid &= $this->valPrimaryContactPhoneExtension();
				$boolIsValid &= $this->valPrimaryContactEmailAddress();
				break;

			case 'validate_insert_helpdesk_task':
				if( false == $boolIsQuickTask ) {
					$boolIsValid &= $this->valTitle();
				} else {
					$boolIsValid &= $this->valHelpdeskUserId();
				}
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valTaskwithUser();
				if( CTaskType::HELPDESK == $this->m_objTask->getTaskTypeId() && true == is_numeric( $this->m_objTask->getGroupId() ) ) {
					$boolIsValid &= $this->valSubTaskTypeId();
					$boolIsValid &= $this->valTaskCategoryId();
				} elseif( CTaskType::HELPDESK == $this->m_objTask->getTaskTypeId() && false == is_numeric( $this->m_objTask->getGroupId() ) ) {
					$boolIsValid &= $this->valGroupId();
				}
				break;

			case 'validate_sub_task_type_id':
				$boolIsValid = $this->valSubTaskTypeId();
				break;

			case 'validate_task_category_id':
				$boolIsValid &= $this->valTaskCategoryId();
				break;

			default:
		}

		return $boolIsValid;
	}

}
?>