<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CContactSearchTypes
 * Do not add any new functions to this class.
 */

class CContactSearchTypes extends CBaseContactSearchTypes {

	public static function fetchContactSearchTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CContactSearchType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchContactSearchType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CContactSearchType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>