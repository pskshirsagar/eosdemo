<?php

class CEmployeeSwipe extends CBaseEmployeeSwipe {

	// Tower 8
	const GATE_NUMBER_MAIN_DOOR_ENTRY 				= '01-1';
	const GATE_NUMBER_MAIN_DOOR_EXIT 				= '01-5';

	const GATE_NUMBER_GAME_ZONE_ENTRY_ONE 			= '01-2';
	const GATE_NUMBER_GAME_ZONE_ENTRY_TWO 			= '01-3';
	const GATE_NUMBER_GAME_ZONE_EXIT_ONE 			= '01-6';
	const GATE_NUMBER_GAME_ZONE_EXIT_TWO 			= '01-7';

	const GATE_NUMBER_SERVER_ROOM_ENTRY 			= '01-4';

	const GATE_NUMBER_CAFETERIA_ENTRY_ONE 			= '02-2';
	const GATE_NUMBER_CAFETERIA_ENTRY_TWO			= '02-1';
	const GATE_NUMBER_LUNCH_ENTRY_ONE 				= '02-4';
	const GATE_NUMBER_LUNCH_ENTRY_TWO 				= '02-6';

	const GATE_NUMBER_FIRE_ENTRY 					= '02-3';
	const GATE_NUMBER_FIRE_EXIT 					= '02-7';

	// Tower 9
	const TOWER_9_GATE_NUMBER_MAIN_DOOR_ENTRY_ONE	= '04-1';
	const TOWER_9_GATE_NUMBER_MAIN_DOOR_ENTRY_TWO 	= '04-2';
	const TOWER_9_GATE_NUMBER_MAIN_DOOR_EXIT_ONE 	= '04-5';
	const TOWER_9_GATE_NUMBER_MAIN_DOOR_EXIT_TWO 	= '04-6';

	const TOWER_9_GATE_NUMBER_CAFETERIA_ENTRY_ONE 	= '06-1';
	const TOWER_9_GATE_NUMBER_CAFETERIA_ENTRY_TWO 	= '06-2';
	const TOWER_9_GATE_NUMBER_CAFETERIA_EXIT_ONE 	= '06-5';
	const TOWER_9_GATE_NUMBER_CAFETERIA_EXIT_TWO 	= '06-6';

	const TOWER_9_GATE_NUMBER_LUNCH_ENTRY_ONE 		= '03-3';
	const TOWER_9_GATE_NUMBER_LUNCH_ENTRY_TWO 		= '03-4';

	const TOWER_9_GATE_NUMBER_FIRE_ENTRY_WING_A 	= '05-1';
	const TOWER_9_GATE_NUMBER_FIRE_ENTRY_WING_B 	= '05-2';
	const TOWER_9_GATE_NUMBER_FIRE_EXIT_WING_A 		= '05-5';
	const TOWER_9_GATE_NUMBER_FIRE_EXIT_WING_B		= '05-6';

	const TOWER_9_GATE_NUMBER_SERVER_ROOM_ONE 		= '03-1';
	const TOWER_9_GATE_NUMBER_SERVER_ROOM_TWO 		= '03-2';

	// Tower Name Constants
	const TOWER_8									= '2';
	const TOWER_9									= '3';

	const MIN_IN_TIME_FOR_HALF_DAY 					= '10:31:00';
	const MIN_IN_TIME_FOR_FIRST_HALF_LATE_MARK		= '08:31:00';
	const MIN_IN_TIME_FOR_SECOND_HALF_LATE_MARK		= '13:31:00';
	const MIN_REQUIRED_ONFLOOR_HOURS_FOR_FULL_DAY	= '06:00:00';
	const MIN_REQUIRED_ONFLOOR_HOURS_FOR_PRESENT	= '04:00:00';
	const MIN_REQUIRED_ONFLOOR_HOURS_FOR_SUPPORT	= '07:00:00';

	const CLOCK_IN_OUT_DEVICE_ENTRY_DESKTOP 		= '10-01';
	const CLOCK_IN_OUT_DEVICE_ENTRY_MOBILE 			= '10-02';
	const CLOCK_IN_OUT_DEVICE_ENTRY_TABLET 			= '10-03';
	const CLOCK_IN_OUT_OTHER_DEVICE_ENTRY 		= '10-04';

	protected $m_strGateName;
	protected $m_strNote;
	protected $m_strNoteDatetime;

	/**
	 * Get Functions
	 */

	public function getGateName() {
		return $this->m_strGateName;
	}

	public function getDecryptedNote() {
		if( false == valStr( $this->m_strNote ) ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strNote, CONFIG_SODIUM_KEY_EMPLOYEE_NOTE, [ 'legacy_secret_key' => CONFIG_KEY_EMPLOYEE_NOTE ] );
	}

	public function getNoteDatetime() {
		return $this->m_strNoteDatetime;
	}

	/**
	 * Set Functions
	 */

	public function setGateName() {

		$strGateNumber = $this->getGateNumber();

		$arrstrGateNames = [
			self::GATE_NUMBER_MAIN_DOOR_ENTRY             => 'T8 - Main Door IN',
			self::GATE_NUMBER_MAIN_DOOR_EXIT              => 'T8 - Main Door OUT',
			self::GATE_NUMBER_CAFETERIA_ENTRY_ONE         => 'T8 - Cafereria One IN',
			self::GATE_NUMBER_CAFETERIA_ENTRY_TWO         => 'T8 - Cafereria Two IN',
			self::GATE_NUMBER_GAME_ZONE_ENTRY_ONE         => 'T8 - Game Zone One IN',
			self::GATE_NUMBER_GAME_ZONE_ENTRY_TWO         => 'T8 - Game Zone Two IN',
			self::GATE_NUMBER_GAME_ZONE_EXIT_ONE          => 'T8 - Game Zone One OUT',
			self::GATE_NUMBER_GAME_ZONE_EXIT_TWO          => 'T8 - Game Zone Two OUT',
			self::GATE_NUMBER_LUNCH_ENTRY_ONE             => 'T8 - Lunch One IN',
			self::GATE_NUMBER_LUNCH_ENTRY_TWO             => 'T8 - Lunch Two IN',
			self::GATE_NUMBER_FIRE_ENTRY                  => 'T8 - Fire IN',
			self::GATE_NUMBER_FIRE_EXIT                   => 'T8 - Fire OUT',
			self::GATE_NUMBER_SERVER_ROOM_ENTRY           => 'T8 - Server Room IN',
			self::TOWER_9_GATE_NUMBER_MAIN_DOOR_ENTRY_ONE => 'T9 - Main Door Entry One IN',
			self::TOWER_9_GATE_NUMBER_MAIN_DOOR_ENTRY_TWO => 'T9 - Main Door Entry Two IN',
			self::TOWER_9_GATE_NUMBER_MAIN_DOOR_EXIT_ONE  => 'T9 - Main Door Entry One OUT',
			self::TOWER_9_GATE_NUMBER_MAIN_DOOR_EXIT_TWO  => 'T9 - Main Door Entry Two OUT',
			self::TOWER_9_GATE_NUMBER_CAFETERIA_ENTRY_ONE => 'T9 - Cafeteria One IN',
			self::TOWER_9_GATE_NUMBER_CAFETERIA_ENTRY_TWO => 'T9 - Cafeteria Two IN',
			self::TOWER_9_GATE_NUMBER_CAFETERIA_EXIT_ONE  => 'T9 - Cafeteria One OUT',
			self::TOWER_9_GATE_NUMBER_CAFETERIA_EXIT_TWO  => 'T9 - Cafeteria Two OUT',
			self::TOWER_9_GATE_NUMBER_LUNCH_ENTRY_ONE     => 'T9 - Lunch One IN',
			self::TOWER_9_GATE_NUMBER_LUNCH_ENTRY_TWO     => 'T9 - Lunch Two IN',
			self::TOWER_9_GATE_NUMBER_FIRE_ENTRY_WING_A   => 'T9 - Fire Wing A IN',
			self::TOWER_9_GATE_NUMBER_FIRE_ENTRY_WING_B   => 'T9 - Fire Wing B IN',
			self::TOWER_9_GATE_NUMBER_FIRE_EXIT_WING_A    => 'T9 - Fire Wing A OUT',
			self::TOWER_9_GATE_NUMBER_FIRE_EXIT_WING_B    => 'T9 - Fire Wing B OUT',
			self::TOWER_9_GATE_NUMBER_SERVER_ROOM_ONE     => 'T9 - Server Room One IN',
			self::TOWER_9_GATE_NUMBER_SERVER_ROOM_TWO     => 'T9 - Server Room Two IN',

		];

		if( true == array_key_exists( $strGateNumber, $arrstrGateNames ) ) {
			$this->m_strGateName = $arrstrGateNames[$strGateNumber];
		} else {
			$this->m_strGateName = 'Invalid Door ' . $strGateNumber;
		}
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['note'] ) ) 			$this->setEncryptedNote( $arrmixValues['note'] );
		if( true == isset( $arrmixValues['note_datetime'] ) )	$this->setNoteDatetime( $arrmixValues['note_datetime'] );

		return;
	}

	public function setEncryptedNote( $strNote ) {
		$this->m_strNote = $strNote;
	}

	public function setNoteDatetime( $strNoteDatetime ) {
		$this->m_strNoteDatetime = $strNoteDatetime;
	}

}
?>