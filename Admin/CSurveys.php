<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveys
 * Do not add any new functions to this class.
 */

class CSurveys extends CBaseSurveys {

	public static function fetchRecentSurveyByResponderReferenceIdsBySurveyTypeId( $arrintResponderReferenceIds, $intSurveyTypeId, $objDatabase, $boolIsScheduleSurveyIdRequired = false, $boolIsSubmitted = false ) {
		if( false == valArr( $arrintResponderReferenceIds ) ) return NULL;

		$strWhereClause = ( true == $boolIsScheduleSurveyIdRequired ) ? ' AND s.scheduled_survey_id IS NOT NULL ' : '';

		$strWhereClause .= ( true == $boolIsSubmitted ) ? ' AND s.response_datetime IS NOT NULL ' : $strWhereClause;

		$strSql = 'SELECT
						*
					FROM (
							SELECT
								*,
								MAX( id ) OVER ( PARTITION BY s.responder_reference_id ) as latest_survey_id
							FROM
								surveys s
							WHERE
								s.responder_reference_id IN( ' . implode( ',', $arrintResponderReferenceIds ) . ' )
								AND s.survey_type_id = ' . ( int ) $intSurveyTypeId .
		          $strWhereClause . '
							) as sub
					WHERE id = latest_survey_id;';

		return parent::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesWithSurveyRatingsByManagerEmployeeId( $intManagerId, $objDatabase, $arrstrSelfSurveyRatingsFilter ) {
		$boolIsShowAll	= ( true == isset( $arrstrSelfSurveyRatingsFilter['show_all'] ) ) ? $arrstrSelfSurveyRatingsFilter['show_all'] : false;
		$intOffset 		= ( true == isset( $arrstrSelfSurveyRatingsFilter['page_no'] ) && true == isset( $arrstrSelfSurveyRatingsFilter['page_size'] ) ) ? $arrstrSelfSurveyRatingsFilter['page_size'] * ( $arrstrSelfSurveyRatingsFilter['page_no'] - 1 ) : 0;
		$intLimit 		= ( true == isset( $arrstrSelfSurveyRatingsFilter['page_size'] ) ) ? $arrstrSelfSurveyRatingsFilter['page_size'] : '';

		if( true == valStr( $strOrderByField = getArrayElementByKey( 'order_by_field', $arrstrSelfSurveyRatingsFilter ) ) ) {
			$strOrderByField = ' sub_query.' . $strOrderByField;
		}
		$strWhereClause = '';

		if( true == valArr( $arrstrSelfSurveyRatingsFilter['qam_employee_ids'] ) ) {
			$strWhereClause	.= ' e.id IN (' . implode( ',', $arrstrSelfSurveyRatingsFilter['qam_employee_ids'] ) . ') OR ';
		}
		$strOrderByType = ( 'DESC' == \Psi\CStringService::singleton()->strtoupper( getArrayElementByKey( 'order_by_type', $arrstrSelfSurveyRatingsFilter ) ) ) ? ' DESC ' : ' ASC ';
		$strFromClause 	= ' teams t ';
		$strJoinClause	= ' JOIN team_employees te ON ( t.id = te.team_id ) JOIN employees e ON ( te.employee_id = e.id AND e.date_terminated IS NULL AND te.is_primary_team = 1) ';

		if( true == valArr( $arrstrSelfSurveyRatingsFilter['dev_qa_employee_ids'] ) ) {
			$strWhereClause .= ' ( e.reporting_manager_id = ' . ( int ) $intManagerId . ' OR e.id IN ( ' . implode( ',', $arrstrSelfSurveyRatingsFilter['dev_qa_employee_ids'] ) . ' ) ) AND ';
		} else {
			$strWhereClause .= ' e.reporting_manager_id = ' . ( int ) $intManagerId . ' AND ';
		}

		if( true == $boolIsShowAll ) {
			$strFromClause 	= ' all_employees ';
			$strJoinClause	= ' JOIN employees e ON ( all_employees.id = e.id ) LEFT JOIN teams t ON ( all_employees.team_id = t.id ) ';
			$strWhereClause	= '';
		}

		$strRecursiveSql = ' WITH RECURSIVE all_employees( id, team_id ) AS (
								SELECT te.employee_id, te.team_id
								FROM team_employees te 
								LEFT JOIN employees e ON ( e.id = te.employee_id )
								WHERE e.reporting_manager_id = ' . ( int ) $intManagerId . ' AND te.is_primary_team = 1 AND te.employee_id <> ' . ( int ) $intManagerId . '
								UNION ALL
								SELECT te.employee_id, te.team_id
								FROM team_employees te
								JOIN employees e ON ( e.id = te.employee_id )
								JOIN all_employees al ON ( e.reporting_manager_id = al.id )
								WHERE te.is_primary_team = 1 AND te.employee_id <> ' . ( int ) $intManagerId . ' ) ';

		$strSql = ' SELECT
							*
					FROM(
							SELECT
								DISTINCT ON ( e.id ) t.id AS team_id,
								e.id AS employee_id,
								e.preferred_name AS employee_name,
								d.name AS designation_name,
								t.name AS team_name,
								sqa.numeric_weight AS rating,
								s.id,
								s.response_datetime AS date_of_rating,
								ss.frequency_id AS frequency_id ,
								ss.start_date AS start_date,
								ss.end_date AS end_date,
								ss.id AS scheduled_survey_id
							FROM
								' . $strFromClause . $strJoinClause . '
								LEFT JOIN designations d ON ( e.designation_id = d.id )
								LEFT JOIN surveys s ON ( s.subject_reference_id = e.id AND s.survey_type_id = ' . CSurveyType::PERSONAL_REVIEW . ' )
								LEFT JOIN survey_question_answers sqa ON (s.id = sqa.survey_id AND sqa.numeric_weight IS NOT NULL )
								LEFT JOIN scheduled_surveys ss ON ( e.id = ss.responder_reference_id AND e.id = ss.subject_reference_id AND ss.id = s.scheduled_survey_id )
								LEFT JOIN employee_addresses ea ON ( s.subject_reference_id = ea.employee_id )
							WHERE
								' . $strWhereClause . '
								e.date_terminated IS NULL
								AND e.id <> ' . ( int ) $intManagerId . '
								AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
								AND (
										( ea.country_code = \'' . CCountry::CODE_INDIA . '\' AND ss.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_IN . ' )
									OR
										( ea.country_code = \'' . CCountry::CODE_USA . '\' AND ss.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_US . ' )
									)
 							ORDER BY
								e.id,
								s.id DESC,
								s.response_datetime DESC
						) AS sub_query
					ORDER BY ' . $strOrderByField . $strOrderByType . ' OFFSET ' . ( int ) $intOffset;

		if( true == isset( $intLimit ) && 0 < $intLimit ) {
			$strSql .= ' LIMIT	' . ( int ) $intLimit;
		}

		if( true == $boolIsShowAll ) $strSql = $strRecursiveSql . $strSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedSurveyRatingsCountOrDataByEmployeeIdBySurveyTypeId( $intEmployeeId, $intSurveyTypeId, $objDatabase, $intPageSize = NULL, $intOffset = NULL ) {
		$strFromClause = ' teams t
							JOIN team_employees te ON ( t.id = te.team_id )
							JOIN employees e ON ( te.employee_id = e.id AND e.date_terminated IS NULL AND te.is_primary_team = 1 AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ')
							LEFT JOIN surveys s ON (s.responder_reference_id = e.id AND s.survey_type_id = ' . ( int ) $intSurveyTypeId . ' AND s.response_datetime IS NOT NULL )
							LEFT JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )';

		$strWhereClause = ' WHERE
								e.date_terminated IS NULL
								AND sqa.numeric_weight IS NOT NULL
								AND e.id = ' . ( int ) $intEmployeeId;

		if( true == is_null( $intPageSize ) ) {
			return self::fetchRowCount( $strWhereClause, $strFromClause, $objDatabase );
		} else {
			$strSql = 'SELECT
							t.id AS team_id,
							e.id AS employee_id,
							e.preferred_name AS employee_name,
							t.name AS team_name,
							sqa.numeric_weight AS rating,
							s.response_datetime AS date_of_rating
						FROM
							' . $strFromClause . '
							' . $strWhereClause . '
						ORDER BY
							e.id,
							s.response_datetime DESC
						OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . ( int ) $intPageSize;

			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchPeerEmployeeSurveyRatingsByManagerEmployeeId( $intManagerId, $objDatabase, $arrstrSurveyRatingsFilter, $boolIsEditFrequency = false ) {
		$boolIsShowAll	= ( true == isset( $arrstrSurveyRatingsFilter['show_all'] ) ) ? $arrstrSurveyRatingsFilter['show_all'] : false;
		$intOffset 		= ( true == isset( $arrstrSurveyRatingsFilter['page_no'] ) && true == isset( $arrstrSurveyRatingsFilter['page_size'] ) ) ? $arrstrSurveyRatingsFilter['page_size'] * ( $arrstrSurveyRatingsFilter['page_no'] - 1 ) : 0;
		$intLimit 		= ( true == isset( $arrstrSurveyRatingsFilter['page_size'] ) ) ? $arrstrSurveyRatingsFilter['page_size'] : '';

		if( true == valStr( $strOrderByField = getArrayElementByKey( 'order_by_field', $arrstrSurveyRatingsFilter ) ) ) {
			$strOrderByField = ' ' . $strOrderByField;
		}

		$strOrderByType = ( 'ASC' == \Psi\CStringService::singleton()->strtoupper( getArrayElementByKey( 'order_by_type', $arrstrSurveyRatingsFilter ) ) ) ? ' ASC NULLS LAST' : ' DESC NULLS LAST';

		$strFromClause 	= ' teams t ';
		$strJoinClause	= ' JOIN team_employees te ON ( t.id = te.team_id ) JOIN employees e ON ( te.employee_id = e.id AND e.date_terminated IS NULL AND te.is_primary_team = 1) ';

		if( true == valArr( $arrstrSurveyRatingsFilter['dev_qa_employee_ids'] ) ) {
			$strWhereClause = ' AND ( e.reporting_manager_id = ' . ( int ) $intManagerId . ' OR e.id IN ( ' . implode( ',', $arrstrSurveyRatingsFilter['dev_qa_employee_ids'] ) . ' ) ) ';
		} else {
			$strWhereClause = ' AND e.reporting_manager_id = ' . ( int ) $intManagerId;
		}

		if( true == $boolIsShowAll ) {
			$strFromClause 	= ' all_employees ';
			$strJoinClause	= ' JOIN employees e ON ( all_employees.id = e.id ) LEFT JOIN teams t ON ( all_employees.team_id = t.id ) ';
			$strWhereClause	= '';
		}
		if( true == valArr( $arrstrSurveyRatingsFilter['qam_employee_ids'] ) ) {
			$strWhereClause	.= ' OR e.id IN (' . implode( ',', $arrstrSurveyRatingsFilter['qam_employee_ids'] ) . ')';
		}
		$strRecursiveSql = ' WITH RECURSIVE all_employees( id, team_id ) AS (
								SELECT te.employee_id, te.team_id 
								FROM team_employees te 
								JOIN employees e ON ( e.id = te.employee_id )
								WHERE e.reporting_manager_id = ' . ( int ) $intManagerId . ' AND te.is_primary_team = 1 AND te.employee_id <> ' . ( int ) $intManagerId . '
								UNION ALL
								SELECT te.employee_id, te.team_id 
								FROM team_employees te
								JOIN employees e ON ( e.id = te.employee_id )
								JOIN all_employees al ON ( e.reporting_manager_id = al.id )
								WHERE te.is_primary_team = 1 AND te.employee_id <> ' . ( int ) $intManagerId . ' ) ';

		$strSql = 'SELECT
						e.id AS employee_id,
						e.name_first,
						e.name_last,
						e.preferred_name AS employee_name,
						d.name AS designation_name,
						t.name team_name,
						SUM( CASE WHEN e1.id IS NOT NULL THEN sqa.numeric_weight ELSE NULL END ) / COALESCE ( NULLIF ( COUNT (e1.id ), 0 ), 1 ) AS avg_rating,
						MAX( CASE WHEN e1.id IS NOT NULL THEN sqa.numeric_weight ELSE NULL END ) AS max_rating,
						MIN( CASE WHEN e1.id IS NOT NULL THEN sqa.numeric_weight ELSE NULL END ) AS min_rating,
						COUNT ( DISTINCT e1.id ) AS rated_by
					FROM
							' . $strFromClause . $strJoinClause . '
						LEFT JOIN surveys s ON ( s.subject_reference_id = e.id AND s.survey_type_id = ' . CSurveyType::PEER_REVIEW . ' AND s.response_datetime >= ( NOW() - INTERVAL \'6 months\' ) )
						LEFT JOIN survey_question_answers sqa ON (s.id = sqa.survey_id)
						LEFT JOIN employees e1 ON ( e1.id = s.responder_reference_id AND e1.date_terminated IS NULL AND e1.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						LEFT JOIN designations d ON ( e.designation_id = d.id )
					WHERE
							e.date_terminated IS NULL
							AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
							' . $strWhereClause . '
							AND e.id <> ' . ( int ) $intManagerId . '
					GROUP BY
						e.id,
						e.name_first,
						e.name_last,
						e.preferred_name,
						d.name,
						t.name
					ORDER BY
					' . $strOrderByField . $strOrderByType . ' OFFSET ' . ( int ) $intOffset;

		if( true == isset( $intLimit ) && 0 < $intLimit && false == $boolIsEditFrequency ) {
			$strSql .= ' LIMIT	' . ( int ) $intLimit;
		}

		if( true == $boolIsShowAll ) $strSql = $strRecursiveSql . $strSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedCompanySurveyRatingsEmployeesByManagerEmployeeId( $intManagerId, $objDatabase, $arrstrSurveyRatingsFilter ) {
		$boolIsShowAll 	= ( true == isset( $arrstrSurveyRatingsFilter['show_all'] ) ) ? $arrstrSurveyRatingsFilter['show_all'] : false;
		$intOffset 		= ( true == isset( $arrstrSurveyRatingsFilter['page_no'] ) && true == isset( $arrstrSurveyRatingsFilter['page_size'] ) ) ? $arrstrSurveyRatingsFilter['page_size'] * ( $arrstrSurveyRatingsFilter['page_no'] - 1 ) : 0;
		$intLimit 		= ( true == isset( $arrstrSurveyRatingsFilter['page_size'] ) ) ? $arrstrSurveyRatingsFilter['page_size'] : '';
		$strWhereClause = '';

		if( true == valStr( $strOrderByField = getArrayElementByKey( 'order_by_field', $arrstrSurveyRatingsFilter ) ) ) {
			$strOrderByField = $strOrderByField;
		}
		if( true == valArr( $arrstrSurveyRatingsFilter['qam_employee_ids'] ) ) {
			$strWhereClause	= 'e.id IN (' . implode( ',', $arrstrSurveyRatingsFilter['qam_employee_ids'] ) . ') OR';
		}

		$strOrderByType = ( 'ASC' == \Psi\CStringService::singleton()->strtoupper( getArrayElementByKey( 'order_by_type', $arrstrSurveyRatingsFilter ) ) ) ? ' ASC ' : ' DESC ';

		$strFromClause 	= ' teams t ';
		$strJoinClause 	= ' JOIN team_employees te ON ( t.id = te.team_id ) JOIN employees e ON ( te.employee_id = e.id AND e.date_terminated IS NULL AND te.is_primary_team = 1) ';

		if( true == valArr( $arrstrSurveyRatingsFilter['dev_qa_employee_ids'] ) ) {
			$strWhereClause .= ' ( e.reporting_manager_id = ' . ( int ) $intManagerId . ' OR e.id IN ( ' . implode( ',', $arrstrSurveyRatingsFilter['dev_qa_employee_ids'] ) . ' ) ) AND ';
		} else {
			$strWhereClause .= ' e.reporting_manager_id = ' . ( int ) $intManagerId . ' AND ';
		}

		if( true == $boolIsShowAll ) {
			$strFromClause 	= ' all_employees ';
			$strJoinClause 	= ' JOIN employees e ON ( all_employees.id = e.id ) LEFT JOIN teams t ON ( all_employees.team_id = t.id ) ';
			$strWhereClause = '';
		}

		$strRecursiveSql = ' WITH RECURSIVE all_employees( id, team_id ) AS (
							SELECT te.employee_id, te.team_id 
							FROM team_employees te 
							JOIN employees e ON ( e.id = te.employee_id )
							WHERE e.reporting_manager_id = ' . ( int ) $intManagerId . ' AND te.is_primary_team = 1 AND te.employee_id <> ' . ( int ) $intManagerId . '
							UNION ALL
							SELECT te.employee_id, te.team_id 
							FROM team_employees te
							JOIN employees e ON ( e.id = te.employee_id )
							JOIN all_employees al ON ( e.reporting_manager_id = al.id )
							WHERE te.is_primary_team = 1 AND te.employee_id <> ' . ( int ) $intManagerId . ' ) ';

		$strSql = ' SELECT
						*
						FROM(
							SELECT
								DISTINCT ON ( e.id )
								t.id AS team_id,
								e.id AS employee_id,
								e.preferred_name AS employee_name,
								d.name AS designation_name,
								t.name AS team_name,
								sqa.numeric_weight as rating,
								s.response_datetime AS date_of_rating
							FROM
								' . $strFromClause . $strJoinClause . '
								LEFT JOIN designations d ON ( e.designation_id = d.id )
								LEFT JOIN surveys s ON (s.responder_reference_id = e.id AND s.survey_type_id = ' . CSurveyType::EMPLOYEE_NPS . ' AND s.response_datetime IS NOT NULL)
								LEFT JOIN survey_question_answers sqa ON (s.id = sqa.survey_id)
							WHERE
								' . $strWhereClause . '
								e.date_terminated IS NULL
								AND e.id <> ' . ( int ) $intManagerId . '
								AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
								ORDER BY
								e.id,
								s.response_datetime DESC
						) AS sub_query
					ORDER BY ' . $strOrderByField . $strOrderByType . ' NULLS LAST
					OFFSET ' . ( int ) $intOffset;

		if( true == isset( $intLimit ) && 0 < $intLimit ) {
			$strSql .= ' LIMIT ' . ( int ) $intLimit;
		}

		if( true == $boolIsShowAll ) $strSql = $strRecursiveSql . $strSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedCompanySurveyRatingsEmployeesCountByManagerEmployeeId( $intManagerId, $objDatabase, $arrstrSurveyRatingsFilter ) {
		$boolIsShowAll 	= ( true == isset( $arrstrSurveyRatingsFilter['show_all'] ) ) ? $arrstrSurveyRatingsFilter['show_all'] : false;
		$strFromClause 	= ' teams t ';
		$strJoinClause 	= ' JOIN team_employees te ON ( t.id = te.team_id ) JOIN employees e ON ( te.employee_id = e.id AND e.date_terminated IS NULL AND te.is_primary_team = 1) ';

		if( true == valArr( $arrstrSurveyRatingsFilter['dev_qa_employee_ids'] ) ) {
			$strWhereClause = ' ( e.reporting_manager_id = ' . ( int ) $intManagerId . ' OR e.id IN ( ' . implode( ',', $arrstrSurveyRatingsFilter['dev_qa_employee_ids'] ) . ' ) ) AND ';
		} else {
			$strWhereClause = ' e.reporting_manager_id = ' . ( int ) $intManagerId . ' AND ';
		}

		if( true == $boolIsShowAll ) {
			$strFromClause = ' all_employees ';
			$strJoinClause = ' JOIN employees e ON ( all_employees.id = e.id ) LEFT JOIN teams t ON ( all_employees.team_id = t.id ) ';
			$strWhereClause = '';
		}

		$strRecursiveSql = ' WITH RECURSIVE all_employees( id, team_id ) AS (
							SELECT te.employee_id, te.team_id 
							FROM team_employees te 
							JOIN employees e ON ( e.id = te.employee_id )
							WHERE e.reporting_manager_id = ' . ( int ) $intManagerId . ' AND te.is_primary_team = 1 AND te.employee_id <> ' . ( int ) $intManagerId . '
							UNION ALL
							SELECT te.employee_id, te.team_id
							FROM team_employees te
							JOIN employees e ON ( e.id = te.employee_id )
							JOIN all_employees al ON ( e.reporting_manager_id = al.id )
							WHERE te.is_primary_team = 1 AND te.employee_id <> ' . ( int ) $intManagerId . ' ) ';

		$strSql = ' SELECT
						 count( sub_query.*)
					FROM(
						SELECT
								DISTINCT ON ( e.id ) t.id AS team_id
						FROM ' . $strFromClause . $strJoinClause . '
						WHERE ' . $strWhereClause . '
								e.date_terminated IS NULL
								AND e.id <> ' . ( int ) $intManagerId . '
								AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						) AS sub_query';

		if( true == $boolIsShowAll ) $strSql = $strRecursiveSql . $strSql;

		$arrintResponse = ( array ) fetchData( $strSql, $objDatabase );

		return ( int ) getArrayElementByKey( 'count', $arrintResponse[0] );
	}

	public static function fetchPeerSurveysRatingsWithRatedByEmployeeId( $intEmployeeId, $objDatabase, $boolIsPeerRatingHistory = NULL, $intPageSize = NULL, $intOffset = NULL ) {
		$strWhereClause = 'WHERE
								s.subject_reference_id = ' . ( int ) $intEmployeeId . '
							AND s.survey_type_id = ' . CSurveyType::PEER_REVIEW . '
							AND e.date_terminated IS NULL
							AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		$strFromClause = 'survey_question_answers sqa
							JOIN surveys s ON ( s.id = sqa.survey_id )
							JOIN employees e ON( s.responder_reference_id = e.id )';

		if( false == $boolIsPeerRatingHistory ) {
			$strWhereClause .= ' AND sqa.created_on >= ( NOW() - INTERVAL \'6 months\' )';
		} else {
			$strWhereClause .= ' AND sqa.created_on <= ( NOW() - INTERVAL \'6 months\' )';
		}

		if( 0 == ( int ) $intPageSize ) {
			return self::fetchRowCount( $strWhereClause, $strFromClause, $objDatabase );
		} else {

			$strSql = 'SELECT
							s.responder_reference_id,
							s.subject_reference_id,
							sqa.numeric_weight AS avg_score,
							STRING_AGG( sqa.answer, \', \' ) AS comments,
							e.name_full AS employee_name,
							e.id as team_employee_id,
							MAX(sqa.created_on) AS date_of_rating,
							sqa.id
						FROM
							' . $strFromClause . '
							' . $strWhereClause . '
						GROUP BY
							s.responder_reference_id,
							s.subject_reference_id,
							e.name_full,
							e.id,
							sqa.numeric_weight,
							sqa.id
						OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . ( int ) $intPageSize;

			$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

			if( true == valArr( $arrmixFetchedData ) ) {
				foreach( $arrmixFetchedData as &$arrmixData ) {
					$arrstrComment = array();
					if( true == valArr( $arrstrComments = explode( ',', $arrmixData['comments'] ) ) ) {
						foreach( $arrstrComments as $strComment ) {
							$arrstrComment[] = CSurvey::getDecryptedData( $strComment );
						}
						$arrmixData['comments'] = implode( ',', $arrstrComment );
					}
				}
			}

			return $arrmixFetchedData;
		}
	}

	public static function fetchNonSubmittedSurveysBySubjectReferenceId( $intEmployeeId, $arrintSurveyTypeIds, $objDatabase, $arrintSurveyIds = NULL ) {
		if( false == valArr( $arrintSurveyTypeIds ) ) return NULL;

		$strWhereClause = ( true == valArr( $arrintSurveyIds ) ) ? ' AND id IN ( ' . implode( ',', $arrintSurveyIds ) . ' )' : '';

		$strSql = 'SELECT
						s.*,
						e.name_full as subject_name
					FROM
						surveys s
						JOIN survey_templates st ON ( s.survey_template_id = st.id AND st.is_published = 1 )
						JOIN employees e ON ( ( e.id = s.subject_reference_id AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . ' )
												OR s.subject_reference_id IS NULL OR s.subject_reference_id = 1 )
					WHERE
						s.id IN ( SELECT
									max(id)
								FROM
									surveys
								WHERE
									responder_reference_id = ' . ( int ) $intEmployeeId . '
									AND response_datetime IS NULL
									AND declined_datetime IS NULL
									AND survey_type_id IN (' . implode( ',', $arrintSurveyTypeIds ) . ')
									' . $strWhereClause . '
								GROUP BY
									survey_type_id, subject_reference_id, responder_reference_id
								)';

		return self::fetchSurveys( $strSql, $objDatabase );
	}

	// Metrics Tab

	public static function fetchSurveyScoresByScoreDatesBySupportEmployeeIds( $strStartDate, $strEndDate, $arrintSupportEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintSupportEmployeeIds ) ) return NULL;

		$strSql = 'SELECT
						support_employee_id,
						SUM( survey_promoter_percentage ) - SUM( survey_detractor_percentage ) AS survey_score
					FROM (
							SELECT
								DISTINCT support_employee_id,
								COUNT(
										CASE WHEN survey_score_status = \'promoter\' THEN support_employee_id
										END
									) OVER ( PARTITION BY support_employee_id, survey_score_status ) * 100 / ( count( support_employee_id ) OVER ( PARTITION BY support_employee_id ) ) AS survey_promoter_percentage,
								COUNT(
										CASE WHEN survey_score_status = \'detractor\' THEN support_employee_id
										END
									) OVER ( PARTITION BY support_employee_id, survey_score_status ) * 100 / ( count( support_employee_id ) OVER ( PARTITION BY support_employee_id ) ) AS survey_detractor_percentage
							FROM (
									SELECT
										pld.support_employee_id,
										s.ps_lead_id,
										s.survey_type_id,
										sqa.numeric_weight,
										CASE WHEN sqa.numeric_weight >= ' . CSurvey::SCORE_PROMOTER . ' THEN \'promoter\'
											WHEN sqa.numeric_weight <= ' . CSurvey::SCORE_DETRACTOR . ' THEN \'detractor\'
											ELSE \'passive\'
										END AS survey_score_status
									FROM
										clients mc
										JOIN ps_lead_details pld ON ( mc.ps_lead_id = pld.ps_lead_id AND company_status_type_id = ' . CCompanyStatusType::CLIENT . ')
										JOIN surveys s ON ( s.ps_lead_id = pld.ps_lead_id )
										JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
									WHERE
										date_trunc( \'day\', s.response_datetime ) BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
										AND s.survey_type_id NOT IN ( ' . CSurveyType::PEER_REVIEW . ', ' . CSurveyType::EMPLOYEE_NPS . ', ' . CSurveyType::PERSONAL_REVIEW . ' )
										AND sqa.numeric_weight IS NOT NULL
										AND sqa.numeric_weight > ' . CSurvey::SCORE_NO . '
										AND pld.support_employee_id IN ( ' . implode( ',', $arrintSupportEmployeeIds ) . ' )
								) AS sub_survey_answers
						) AS sub_percentage_survey_answers
					GROUP BY
						support_employee_id';

		return fetchData( $strSql, $objDatabase );
	}

	// Net Promotor Score Tab : action - view_nps_ratings

	public static function fetchPaginatedSurveyScoresByCids( $arrintCids, $arrstrNetPromoterScoreFilter, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) return NULL;

		$intOffset 			= ( true == isset( $arrstrNetPromoterScoreFilter['page_no'] ) && true == isset( $arrstrNetPromoterScoreFilter['page_size'] ) ) ? $arrstrNetPromoterScoreFilter['page_size'] * ( $arrstrNetPromoterScoreFilter['page_no'] - 1 ) : 0;
		$intLimit 			= ( true == isset( $arrstrNetPromoterScoreFilter['page_size'] ) ) ? $arrstrNetPromoterScoreFilter['page_size'] : '';
		$strStartDate 		= ( true == isset( $arrstrNetPromoterScoreFilter['from_date'] ) ) ? $arrstrNetPromoterScoreFilter['from_date'] : date( 'm/d/Y', strtotime( '-2 month' ) );
		$strEndDate 		= ( true == isset( $arrstrNetPromoterScoreFilter['to_date'] ) ) ? $arrstrNetPromoterScoreFilter['to_date'] : date( 'm/d/Y' );
		$strOrderByField 	= ( true == isset( $arrstrNetPromoterScoreFilter['order_by_field'] ) ) ? $arrstrNetPromoterScoreFilter['order_by_field'] : 'date';
		$strOrderByType		= ( true == isset( $arrstrNetPromoterScoreFilter['order_by_type'] ) ) ? $arrstrNetPromoterScoreFilter['order_by_type'] : 'DESC';
		$strCondition 		= '';

		if( false == is_null( $arrstrNetPromoterScoreFilter['load_tab'] ) ) {
			if( 'detractors_tab' == $arrstrNetPromoterScoreFilter['load_tab'] ) {
				$strCondition = ' AND sqa.numeric_weight <= ' . CSurvey::SCORE_DETRACTOR;
			} elseif( 'promoters_tab' == $arrstrNetPromoterScoreFilter['load_tab'] ) {
				$strCondition = ' AND sqa.numeric_weight >= ' . CSurvey::SCORE_PROMOTER;
			} elseif( 'passives_tab' == $arrstrNetPromoterScoreFilter['load_tab'] ) {
				$strCondition = ' AND sqa.numeric_weight > ' . CSurvey::SCORE_DETRACTOR . ' AND sqa.numeric_weight < ' . CSurvey::SCORE_PROMOTER;
			}
		}

		switch( $strOrderByField ) {
			case 'company_name':
				$strOrderByField = ' mc.company_name';
				break;

			case 'company_user_fullname':
				$strOrderByField = ' company_user_fullname';
				break;

			case 'rating':
				$strOrderByField = ' sqa.numeric_weight';
				break;

			case 'comments':
				$strOrderByField = ' sqa.answer';
				break;

			case 'date':
				$strOrderByField = ' s.response_datetime';
				break;

			default:
				$strOrderByField = ' s.response_datetime';
				break;
		}

		$strSql = 'SELECT
						s.*,
						sqa.answer,
						sqa.numeric_weight as rating,
						( p.name_first || \' \' || p.name_last ) AS company_user_fullname,
						mc.company_name,
						mc.id as company_id
					FROM
						surveys s
						JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						JOIN persons p ON ( p.id = s.responder_reference_id )
						JOIN clients mc ON ( mc.id = p.cid )
					WHERE
						mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
						AND s.response_datetime >= \'' . $strStartDate . ' 00:00:00\'
						AND s.response_datetime <= \'' . $strEndDate . ' 23:59:59\'' . $strCondition . '
						AND sqa.numeric_weight > ' . CSurvey::SCORE_NO . '
						AND s.responder_reference_id IS NOT NULL
					ORDER BY
							' . $strOrderByField . ' ' . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;

		if( true == isset( $intLimit ) && 0 < $intLimit ) {
			$strSql .= ' LIMIT	' . ( int ) $intLimit;
		}

		return self::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchPaginatedSurveyScoresCountByCids( $arrintCids, $arrstrNetPromoterScoreFilter, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) return NULL;

		$strStartDate 	= ( true == isset( $arrstrNetPromoterScoreFilter['from_date'] ) ) ? $arrstrNetPromoterScoreFilter['from_date'] : date( 'm/d/Y', strtotime( '-2 month' ) );
		$strEndDate 	= ( true == isset( $arrstrNetPromoterScoreFilter['to_date'] ) ) ? $arrstrNetPromoterScoreFilter['to_date'] : date( 'm/d/Y' );
		$strCondition 	= '';

		if( false == is_null( $arrstrNetPromoterScoreFilter['load_tab'] ) ) {
			if( 'detractors_tab' == $arrstrNetPromoterScoreFilter['load_tab'] ) {
				$strCondition = ' AND sqa.numeric_weight <= ' . CSurvey::SCORE_DETRACTOR;
			} elseif( 'promoters_tab' == $arrstrNetPromoterScoreFilter['load_tab'] ) {
				$strCondition = ' AND sqa.numeric_weight >= ' . CSurvey::SCORE_PROMOTER;
			} elseif( 'passives_tab' == $arrstrNetPromoterScoreFilter['load_tab'] ) {
				$strCondition = ' AND sqa.numeric_weight > ' . CSurvey::SCORE_DETRACTOR . ' AND sqa.numeric_weight < ' . CSurvey::SCORE_PROMOTER;
			}
		}

		$strSql = 'SELECT
						count( s.id )
					FROM
						surveys s
						JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						JOIN persons p ON ( p.id = s.responder_reference_id )
						JOIN clients mc ON ( mc.id = p.cid )
					WHERE
						mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
						AND s.response_datetime >= \'' . $strStartDate . ' 00:00:00\'
						AND s.response_datetime <= \'' . $strEndDate . ' 23:59:59\'' . $strCondition . '
						AND sqa.numeric_weight > ' . CSurvey::SCORE_NO . '
						AND s.responder_reference_id IS NOT NULL';

		$arrintResponse = ( array ) fetchData( $strSql, $objDatabase );
		return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
	}

	// Net Promotor Score Tab : action - view_twelve_months_summary

	public static function fetchPaginatedTwelveMonthsSurveyScoresByCids( $arrintCids, $arrstrNetPromoterScoreFilter, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) return NULL;

		$intOffset 					= ( true == isset( $arrstrNetPromoterScoreFilter['page_no'] ) && true == isset( $arrstrNetPromoterScoreFilter['page_size'] ) ) ? $arrstrNetPromoterScoreFilter['page_size'] * ( $arrstrNetPromoterScoreFilter['page_no'] - 1 ) : 0;
		$intLimit 					= ( true == isset( $arrstrNetPromoterScoreFilter['page_size'] ) ) ? $arrstrNetPromoterScoreFilter['page_size'] : '';
		$strOrderByField 			= ( true == valArr( $arrstrNetPromoterScoreFilter ) && true == isset( $arrstrNetPromoterScoreFilter['order_by_field'] ) ) ? $arrstrNetPromoterScoreFilter['order_by_field'] : 'date';
		$strOrderByType				= ( true == valArr( $arrstrNetPromoterScoreFilter ) && true == isset( $arrstrNetPromoterScoreFilter['order_by_type'] ) ) ? $arrstrNetPromoterScoreFilter['order_by_type'] : 'DESC';

		switch( $strOrderByField ) {

			case 'company_name':
				$strOrderByField 		= ' subq.company_name';
				break;

			case 'company_user_fullname':
				$strOrderByField 		= ' company_user_fullname';
				break;

			case 'highest':
				$strOrderByField 		= ' max_score';
				break;

			case 'lowest':
				$strOrderByField 		= ' min_score';
				break;

			case 'most_recent':
				$strOrderByField 		= ' latest_score';
				break;

			default:
				$strOrderByField 		= ' subq.company_name';
				break;
		}

		$strSql = 'SELECT
						subq.id,
						subq.company_name,
						MAX( CASE WHEN subq.score_rank = 1 THEN subq.company_user_id END ) as company_user_id,
						MAX( CASE WHEN subq.score_rank = 1 THEN subq.company_user_fullname END ) as company_user_fullname,
						MAX( subq.numeric_weight ) as max_score,
						MIN( subq.numeric_weight ) as min_score,
						MAX( average_score ) as average_score,
						MAX( CASE WHEN subq.score_rank = 1 THEN subq.numeric_weight ELSE 0 END ) as latest_score
					FROM
						(
							SELECT
								mc.id,
								mc.company_name,
								p.company_user_id,
								( p.name_first || \' \' || p.name_last ) AS company_user_fullname,
								sqa.numeric_weight,
								RANK() OVER ( PARTITION BY p.cid ORDER BY s.response_datetime DESC ) as score_rank,
								ROUND( AVG( sqa.numeric_weight ) OVER( PARTITION BY p.cid ) ) as average_score
							FROM
								surveys s
								JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
								JOIN persons p ON ( s.responder_reference_id = p.id )
								JOIN clients mc ON ( mc.id = p.cid )
							WHERE
								mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
								AND s.response_datetime > ( NOW() - INTERVAL \'12 months\' )
								AND sqa.numeric_weight > 0
								AND s.responder_reference_id IS NOT NULL
						) as subq
					GROUP BY
					 	subq.id,
						subq.company_name
					ORDER BY
						' . $strOrderByField . ' ' . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset;

		if( true == isset( $intLimit ) && 0 < $intLimit ) {
			$strSql .= ' LIMIT	' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTwelveMonthsSurveyScoresCountByCids( $arrintCids, $objDatabase ) {
		if( false == valArr( $arrintCids ) ) return NULL;

		$strSql = 'SELECT
						count( cid )
					FROM
						(
							SELECT
								p.cid
							FROM
								surveys s
								JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
								JOIN persons p ON ( s.responder_reference_id = p.id )
								JOIN clients mc ON ( mc.id = p.cid )
							WHERE
								mc.id IN ( ' . implode( ',', $arrintCids ) . ' )
								AND s.response_datetime > ( NOW() - INTERVAL \'12 months\' )
								AND sqa.numeric_weight > ' . CSurvey::SCORE_NO . '
								AND s.responder_reference_id IS NOT NULL
							GROUP BY p.cid
						) as sub_query';

		$arrintResponse = ( array ) fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
	}

	// action : view_all_ratings_graph

	public static function fetchMonthlyRecentSurveyScoresByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						subq.id,
						subq.company_name,
						subq.company_user_id,
						MAX( CASE WHEN subq.score_rank = 1 THEN subq.answer ELSE NULL END ) as memo,
						subq.survey_datetime as survey_datetime,
						MAX(subq.company_user_fullname) as company_user_fullname,
						MAX( CASE WHEN subq.score_rank = 1 THEN CAST(subq.numeric_weight AS INT) ELSE 0 END ) as latest_score
					FROM (
							SELECT
								mc.id,
								mc.company_name,
								p.company_user_id,
								( p.name_first || \'\' || p.name_last ) AS company_user_fullname,
								sqa.numeric_weight,
								sqa.answer,
								RANK() OVER( PARTITION BY p.cid, p.company_user_id, DATE_TRUNC( \'month\', s.response_datetime ) ORDER BY s.response_datetime DESC ) as score_rank,
								DATE_TRUNC(\'month\', s.response_datetime) AS survey_datetime
							FROM
								surveys s
								JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
								JOIN persons p ON ( s.responder_reference_id = p.id )
								JOIN clients mc ON ( mc.id = p.cid )
							WHERE
								mc.id = ' . ( int ) $intCid . '
								AND mc.company_status_type_id <> ' . CCompanyStatusType::TERMINATED . '
								AND s.response_datetime >= ( DATE_TRUNC(\'month\', NOW() ) - INTERVAL \'11 months\' )
								AND s.responder_reference_id IS NOT NULL
							) as subq
					GROUP BY
							subq.id,
							subq.company_name,
							subq.company_user_id,
							subq.survey_datetime
					ORDER BY
						MAX( subq.company_user_fullname )';

		$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			foreach( $arrmixFetchedData as &$arrmixData ) {
				$arrmixData['memo'] = CSurvey::getDecryptedData( $arrmixData['memo'] );
			}
		}

		return $arrmixFetchedData;
	}

	public static function fetchLastMonthSurveyScoresBySurveyTypeIdsByCids( $arrintSurveyTypeIds, $objDatabase, $arrintCids = NULL ) {
		if( false == valArr( $arrintSurveyTypeIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT p.cid,
						s.survey_type_id,
						CASE
							WHEN s.response_datetime >= NOW() - INTERVAL \'1 months\' OR s.survey_datetime >= NOW() - INTERVAL \'1 months\'
							THEN sqa.numeric_weight
						END AS current_score,

						CASE
							WHEN ( s.response_datetime >=( ( NOW() - INTERVAL \'1 months\') - INTERVAL \'1 day\' ) AND s.response_datetime <= NOW() - INTERVAL \'1 day\' )
								OR ( s.survey_datetime >=( ( NOW() - INTERVAL \'1 months\') - INTERVAL \'1 day\' ) AND s.survey_datetime <= NOW() - INTERVAL \'1 day\' )
							THEN sqa.numeric_weight
						END AS previous_score
					FROM
						surveys s
						JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						JOIN persons p ON ( p.ps_lead_id = s.ps_lead_id )
					WHERE
						s.survey_type_id IN( ' . implode( ',', $arrintSurveyTypeIds ) . ' )
						AND ( s.response_datetime >=( ( NOW() - interval \'1 months\') - INTERVAL \'1 day\' ) OR s.survey_datetime >=( ( NOW() - interval \'1 months\') - INTERVAL \'1 day\' ) )
						AND sqa.numeric_weight IS NOT NULL
						AND sqa.numeric_weight > ' . CSurvey::SCORE_NO;

		if( true == valArr( $arrintCids ) ) {
			$strSql .= ' AND p.cid = ANY ( ARRAY [ ' . implode( ',', $arrintCids ) . ' ] )';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSurveyPreviousGoalsById( $intId, $objDatabase ) {
		$strSql = 'SELECT
						s.*,
						previous_s.goals AS previous_goals
					FROM
						surveys s
						LEFT JOIN surveys previous_s ON ( 	previous_s.subject_reference_id = s.subject_reference_id
															AND previous_s.survey_type_id = s.survey_type_id
															AND previous_s.id = ( SELECT
																					MAX(id)
																				FROM
																					surveys
																					LEFT JOIN survey_question_answers sqa ON ( id = sqa.survey_id )
																				WHERE
																					id < s.id
																					AND survey_type_id = s.survey_type_id
																					AND subject_reference_id = s.subject_reference_id
																					AND sqa.numeric_weight IS NOT NULL ) )
					WHERE
						s.id = ' . ( int ) $intId;

		return parent::fetchSurvey( $strSql, $objDatabase );
	}

	public static function fetchPaginatedNonSubmittedSurveysBySurveyTypeIdBySurveyTemplateIds( $objPagination, $intSurveyTypeId, $arrintSurveyTemplateIds, $objDatabase, $intDayInterval = NULL, $boolIsCountOnly = false ) {
		if( false == valId( $intDayInterval ) ) {
			$strSurveyDateTimeSelect = ' s.survey_datetime ';
		} else {
			$strSurveyDateTimeSelect = ' s.survey_datetime + interval \' ' . ( int ) $intDayInterval . ' day\' AS survey_datetime';
		}

		if( true == $boolIsCountOnly ) {
			$strSql = 'SELECT COUNT( s.id )';
		} else {
			$strSql = 'SELECT
						s.id,
						s.survey_template_id,
						s.survey_type_id,
						s.trigger_reference_id,
						s.responder_reference_id,
						s.subject_reference_id,' . $strSurveyDateTimeSelect . ',
						e.preferred_name AS employee_name_full,
						d.name AS department_name';
		}

		$strSql .= ' FROM
						surveys s
						JOIN employees e ON ( s.subject_reference_id = e.id )
						JOIN departments d ON ( d.id = e.department_id )';

		$strSql .= ' WHERE
						s.survey_type_id = ' . ( int ) $intSurveyTypeId . '
						AND s.survey_template_id IN ( ' . implode( ',', $arrintSurveyTemplateIds ) . ' )
						AND s.response_datetime IS NULL
						AND s.declined_datetime IS NULL';

		if( false == $boolIsCountOnly && true == valStr( $objPagination->getOrderByField() ) ) {
			$strSql .= ' ORDER BY ' . $objPagination->getOrderByField();
			$strSql .= ( true == valId( $objPagination->getOrderByType() ) ) ? ' ASC' : ' DESC';
		}

		if( false == $boolIsCountOnly && false == is_null( $objPagination->getOffset() ) && false == is_null( $objPagination->getPageSize() ) ) {
			$strSql .= ' OFFSET ' . ( int ) $objPagination->getOffset() . ' LIMIT ' . ( int ) $objPagination->getPageSize();
		}

		if( true == $boolIsCountOnly ) {
			$arrstrData = ( array ) fetchData( $strSql, $objDatabase );

			if( true == valArr( $arrstrData ) ) {
				return $arrstrData[0]['count'];
			}
		}

		return self::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchRecentSurveyByResponderReferenceIdByPsLeadId( $intResponderReferenceId, $intPsLeadId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						responder_reference_id = ' . ( int ) $intResponderReferenceId . '
						AND ps_lead_id = ' . ( int ) $intPsLeadId . '
						AND survey_type_id NOT IN ( ' . CSurveyType::SUPPORT_REVIEW . ', ' . CSurveyType::UTILITIES . ' )
						AND response_datetime IS NULL
						and declined_datetime IS NULL
					ORDER BY
						id desc
					LIMIT 1';

		return self::fetchSurvey( $strSql, $objDatabase );
	}

	public static function fetchCustomLeasingAgentQASurveyData( $intSurveyId, $objAdminDatabase ) {
		if( false == valId( $intSurveyId ) ) return NULL;

		$strSql = 'SELECT
						survey_id,
						ROUND(((basic_expectations_total_answer_numeric_weight / basic_expectations_total_question_numeric_weight) * 100), 2) AS basic_expectations,
						ROUND(((quality_of_interaction_total_answer_numeric_weight / quality_of_interaction_total_question_numeric_weight) * 100), 2) AS quality_of_interaction,
						ROUND(disqualifiers_total_answer_numeric_weight) AS disqualifiers_total_answer_numeric_weight,
						trigger_reference_id
					FROM (
							SELECT survey_id,
									SUM( CASE WHEN survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::BASIC_EXPECTATIONS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_BASIC_EXPECTATIONS . ' )
													AND numeric_weight IS NOT NULL
													AND question_numeric_weight > 0
											THEN question_numeric_weight ELSE NULL
										END ) AS basic_expectations_total_question_numeric_weight,
									SUM( CASE WHEN survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::BASIC_EXPECTATIONS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_BASIC_EXPECTATIONS . ' )
											THEN answer_numeric_weight ELSE NULL
										END ) AS basic_expectations_total_answer_numeric_weight,
									SUM( CASE WHEN survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::QUALITY_OF_INTERACTION . ', ' . CSurveyTemplateQuestionGroup::STANDARD_QUALITY_OF_INTERACTION . ' )
													AND numeric_weight IS NOT NULL AND question_numeric_weight > 0
												THEN question_numeric_weight ELSE NULL
										END ) AS quality_of_interaction_total_question_numeric_weight,
									SUM( CASE WHEN survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::QUALITY_OF_INTERACTION . ', ' . CSurveyTemplateQuestionGroup::STANDARD_QUALITY_OF_INTERACTION . ' )
											THEN answer_numeric_weight ELSE NULL
										END ) AS quality_of_interaction_total_answer_numeric_weight,
									SUM( CASE WHEN survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::DISQUALIFIERS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_DISQUALIFIERS . ' )
												THEN answer_numeric_weight ELSE NULL
										END ) AS disqualifiers_total_answer_numeric_weight,
										trigger_reference_id
							FROM (
									SELECT sqa.survey_template_question_id,
											sqa.survey_template_question_group_id,
											sqa.numeric_weight,
											sqa.survey_id as survey_id,
											stq.numeric_weight as question_numeric_weight,
											( ( stq.numeric_weight * sqa.numeric_weight ) / 100 ) as answer_numeric_weight,
											s.trigger_reference_id
									FROM
										survey_question_answers sqa
										LEFT JOIN survey_template_questions stq ON ( stq.survey_template_id = sqa.survey_template_id AND sqa.survey_template_question_id = stq.id )
										LEFT JOIN surveys AS s ON ( s.id = sqa.survey_id AND s.survey_template_id = sqa.survey_template_id )
									WHERE sqa.survey_id = ' . ( int ) $intSurveyId . '
								) AS innerquery
							GROUP BY
									innerquery.survey_id, innerquery.trigger_reference_id
						) AS subquery';

		$arrmixSurveyData	= ( array ) fetchData( $strSql, $objAdminDatabase );

		if( false == valArr( $arrmixSurveyData ) ) {
			return NULL;
		}

		$arrmixSurveyData[0]['total_score'] = ( ( ( $arrmixSurveyData[0]['basic_expectations'] + $arrmixSurveyData[0]['quality_of_interaction'] ) / 2 ) - $arrmixSurveyData[0]['disqualifiers_total_answer_numeric_weight'] );

		return $arrmixSurveyData;
	}

	public static function fetchCustomSupportQaSurveyStatistics( $objSurveysFilter = NULL, $objAdminDatabase, $boolIsProcessEmployeeAverage = false ) {
		$strWhereSql		= NULL;
		$strOffsetClause	= NULL;

		if( true == valStr( $objSurveysFilter->getStartDate() ) && true == valStr( $objSurveysFilter->getEndDate() ) ) {
			$strFieldName = 's.response_datetime';

			$strWhereSql .= ' AND ' . $strFieldName . ' BETWEEN \'' . date( 'Y-m-d', strtotime( $objSurveysFilter->getStartDate() ) ) . ' 00:00:00\' AND \'' . date( 'Y-m-d', strtotime( $objSurveysFilter->getEndDate() ) ) . ' 23:59:59\'';
		}

		if( true == valStr( $objSurveysFilter->getSurveyIds() ) ) {
			$strWhereSql .= ' AND s.id IN ( ' . $objSurveysFilter->getSurveyIds() . ' )';
		} elseif( true == valId( $objSurveysFilter->getSurveyId() ) ) {
			$strWhereSql .= ' AND s.id = ' . $objSurveysFilter->getSurveyId();
		}

		if( true == valId( $objSurveysFilter->getEmployeeId() ) ) {
			$strWhereSql .= ' AND s.subject_reference_id = ' . $objSurveysFilter->getEmployeeIds();
		}

		if( true == valId( $objSurveysFilter->getTeamId() ) ) {
			$strWhereSql .= ' AND te.team_id = ' . $objSurveysFilter->getTeamId();
		}

		if( true == valStr( $objSurveysFilter->getEmployeeIds() ) ) {
			$strWhereSql .= ' AND s.subject_reference_id IN ( ' . $objSurveysFilter->getEmployeeIds() . ' )';
		}

		if( true == valStr( $objSurveysFilter->getEmployeeGraderIds() ) ) {
			$strWhereSql .= ' AND s.responder_reference_id IN ( ' . $objSurveysFilter->getEmployeeGraderIds() . ' )';
		}

		if( true == valStr( $objSurveysFilter->getTeamIds() ) ) {
			$strWhereSql .= ' AND te.team_id IN ( ' . $objSurveysFilter->getTeamIds() . ' )';
		}

		if( true == valStr( $objSurveysFilter->getDesignationIds() ) ) {
			$strWhereSql .= ' AND e.designation_id IN ( ' . $objSurveysFilter->getDesignationIds() . ' )';
		}

		if( true == valStr( $objSurveysFilter->getEmployeeHireStartDate() ) && true == valStr( $objSurveysFilter->getEmployeeHireEndDate() ) ) {
			$strWhereSql .= ' AND e.date_started BETWEEN \'' . date( 'Y-m-d', strtotime( $objSurveysFilter->getEmployeeHireStartDate() ) ) . ' 00:00:00\' AND \'' . date( 'Y-m-d', strtotime( $objSurveysFilter->getEmployeeHireEndDate() ) ) . ' 23:59:59\'';
		}

		if( true == valId( $objSurveysFilter->getEmployeeStatusTypeId() ) ) {
			$strWhereSql .= 'AND e.employee_status_type_id = ' . $objSurveysFilter->getEmployeeStatusTypeId();
		} else {
			$strWhereSql .= 'AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		}

		$strOrderBy			= 's.survey_datetime DESC';
		$strInnerOrderBy	= 'innerquery.response_datetime DESC';

		if( true == valStr( $objSurveysFilter->getOrderBy() ) ) {
			$strOrderBy = $objSurveysFilter->getOrderBy();
		}

		if( true == $boolIsProcessEmployeeAverage ) {
			$strSqlWith = 'WITH survey_calls AS (
							SELECT
									s.id,
									s.trigger_reference_id,
									s.responder_reference_id,
									s.subject_reference_id,
									s.survey_datetime,
									s.response_datetime,
									s.survey_template_id,
									te.team_id,
									e.reporting_manager_id AS manager_employee_id,
									e.name_full,
									e.preferred_name
							FROM
								surveys s
								JOIN (
										SELECT
											e.id as employee_id,
											e.name_full,
											e.preferred_name,
											e.reporting_manager_id,
											e.date_started as start_date,
											e.date_started + INTERVAL \' 90 days\' as end_date
										FROM
											employees e
										WHERE
											e.id IN ( ' . $objSurveysFilter->getEmployeeIds() . ' )
											AND e.date_terminated IS NULL
											AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
									) AS e ON ( e.employee_id = s.subject_reference_id AND s.survey_datetime > e.start_date AND s.survey_datetime < e.end_date )
								LEFT JOIN team_employees te ON ( te.employee_id = e.employee_id AND te.is_primary_team = 1 )
							WHERE
								s.survey_type_id = ' . CSurveyType::SUPPORT_QA_CALLS . '
								AND s.survey_template_id IN ( ' . CSurveyTemplate::NEW_SUPPORT_QA_CALL_SURVEY . ', ' . CSurveyTemplate::NEW_SUPPORT_QA_CALL_STANDARD_SURVEY . ' )
								AND s.response_datetime IS NOT NULL
								AND s.declined_datetime IS NULL
						)';
		} else {
			$strSqlWith = 'WITH survey_calls AS (
							SELECT
								s.id,
								s.trigger_reference_id,
								s.responder_reference_id,
								s.subject_reference_id,
								s.survey_datetime,
								s.response_datetime,
								s.survey_template_id,
								te.team_id,
								e.reporting_manager_id AS manager_employee_id,
								e.name_full,
								e.preferred_name
							FROM
								surveys s
								LEFT JOIN employees e ON ( e.id = s.subject_reference_id )
								LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
							WHERE
								s.survey_type_id = ' . CSurveyType::SUPPORT_QA_CALLS . '
								AND s.survey_template_id IN ( ' . CSurveyTemplate::NEW_SUPPORT_QA_CALL_SURVEY . ',' . CSurveyTemplate::NEW_SUPPORT_QA_CALL_STANDARD_SURVEY . ' )
								AND s.response_datetime IS NOT NULL
								AND s.declined_datetime IS NULL
								AND e.date_terminated IS NULL
								' . $strWhereSql . '
							ORDER BY
								' . $strOrderBy . '
								' . $strOffsetClause . '
						)';
		}

		$strSql = $strSqlWith . '
					SELECT
						survey_id,
						call_id,
						employee_id,
						employee_name,
						preferred_name,
						team_id,
						manager_employee_id,
						survey_datetime,
						response_datetime,
						graded_by,
						survey_template_id,
						ROUND( ( ( greeting_total_answer_numeric_weight / greeting_total_question_numeric_weight ) * 100 ), 2 ) AS greeting,
						ROUND( ( ( ticket_creation_total_answer_numeric_weight / ticket_creation_total_question_numeric_weight ) * 100 ), 2 ) AS ticket_creation,
						ROUND( ( ( escalated_ticket_total_answer_numeric_weight / escalated_ticket_total_question_numeric_weight ) * 100 ), 2 ) AS escalated_ticket,
						ROUND( ( ( call_flow_total_answer_numeric_weight / call_flow_total_question_numeric_weight ) * 100 ), 2 ) AS call_flow,
						ROUND( ( ( etiquette_total_answer_numeric_weight / etiquette_total_question_numeric_weight ) * 100 ), 2 ) AS etiquette,
						ROUND( ( ( troubleshooting_and_accuracy_total_answer_numeric_weight / troubleshooting_and_accuracy_total_question_numeric_weight ) * 100 ), 2 ) AS troubleshooting_and_accuracy,
						ROUND( ( ( transfers_within_support_total_answer_numeric_weight / transfers_within_support_total_question_numeric_weight ) * 100 ), 2 ) AS transfers_within_support,
						ROUND( ( ( closure_total_answer_numeric_weight / closure_total_question_numeric_weight ) * 100 ), 2 ) AS closure,
						ROUND( ( ( auto_fails_total_answer_numeric_weight / auto_fails_total_question_numeric_weight ) * 100 ), 2 ) AS auto_fails,
						failed_questions_count,
						ROUND( greeting_total_question_numeric_weight ) AS greeting_total_question_numeric_weight,
						ROUND( greeting_total_answer_numeric_weight ) AS greeting_total_answer_numeric_weight,
						ROUND( ticket_creation_total_question_numeric_weight ) AS ticket_creation_total_question_numeric_weight,
						ROUND( ticket_creation_total_answer_numeric_weight ) AS ticket_creation_total_answer_numeric_weight,
						ROUND( escalated_ticket_total_question_numeric_weight ) AS escalated_ticket_total_question_numeric_weight,
						ROUND( escalated_ticket_total_answer_numeric_weight ) AS escalated_ticket_total_answer_numeric_weight,
						ROUND( call_flow_total_question_numeric_weight ) AS call_flow_total_question_numeric_weight,
						ROUND( call_flow_total_answer_numeric_weight ) AS call_flow_total_answer_numeric_weight,
						ROUND( etiquette_total_question_numeric_weight ) AS etiquette_total_question_numeric_weight,
						ROUND( etiquette_total_answer_numeric_weight ) AS etiquette_total_answer_numeric_weight,
						ROUND( troubleshooting_and_accuracy_total_question_numeric_weight ) AS troubleshooting_and_accuracy_total_question_numeric_weight,
						ROUND( troubleshooting_and_accuracy_total_answer_numeric_weight ) AS troubleshooting_and_accuracy_total_answer_numeric_weight,
						ROUND( transfers_within_support_total_question_numeric_weight ) AS transfers_within_support_total_question_numeric_weight,
						ROUND( transfers_within_support_total_answer_numeric_weight ) AS transfers_within_support_total_answer_numeric_weight,
						ROUND( closure_total_question_numeric_weight ) AS closure_total_question_numeric_weight,
						ROUND( closure_total_answer_numeric_weight ) AS closure_total_answer_numeric_weight,
						ROUND( auto_fails_total_question_numeric_weight ) AS auto_fails_total_question_numeric_weight,
						ROUND( auto_fails_total_answer_numeric_weight ) AS auto_fails_total_answer_numeric_weight
					FROM (
						SELECT
							survey_id,
							call_id,
							employee_id,
							employee_name,
							preferred_name,
							team_id,
							manager_employee_id,
							graded_by,
							survey_template_id,
							survey_datetime,
							response_datetime,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::GREETING . ', ' . CSurveyTemplateQuestionGroup::STANDARD_GREETING . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS greeting_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::GREETING . ', ' . CSurveyTemplateQuestionGroup::STANDARD_GREETING . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS greeting_total_answer_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::TICKET_CREATION . ', ' . CSurveyTemplateQuestionGroup::STANDARD_TICKET_CREATION . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS ticket_creation_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::TICKET_CREATION . ', ' . CSurveyTemplateQuestionGroup::STANDARD_TICKET_CREATION . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS ticket_creation_total_answer_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::ESCALATED_TICKET . ', ' . CSurveyTemplateQuestionGroup::STANDARD_ESCALATED_TICKET . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS escalated_ticket_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::ESCALATED_TICKET . ', ' . CSurveyTemplateQuestionGroup::STANDARD_ESCALATED_TICKET . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS escalated_ticket_total_answer_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::CALL_FLOW . ', ' . CSurveyTemplateQuestionGroup::STANDARD_CALL_FLOW . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS call_flow_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::CALL_FLOW . ', ' . CSurveyTemplateQuestionGroup::STANDARD_CALL_FLOW . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS call_flow_total_answer_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::ETIQUETTE . ', ' . CSurveyTemplateQuestionGroup::STANDARD_ETIQUETTE . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS etiquette_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::ETIQUETTE . ', ' . CSurveyTemplateQuestionGroup::STANDARD_ETIQUETTE . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS etiquette_total_answer_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::TROUBLESHOOTING_AND_ACCURACY . ', ' . CSurveyTemplateQuestionGroup::STANDARD_TROUBLESHOOTING_AND_ACCURACY . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS troubleshooting_and_accuracy_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::TROUBLESHOOTING_AND_ACCURACY . ', ' . CSurveyTemplateQuestionGroup::STANDARD_TROUBLESHOOTING_AND_ACCURACY . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS troubleshooting_and_accuracy_total_answer_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::TRANSFERS_WITHIN_SUPPORT . ', ' . CSurveyTemplateQuestionGroup::STANDARD_TRANSFERS_WITHIN_SUPPORT . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS transfers_within_support_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::TRANSFERS_WITHIN_SUPPORT . ', ' . CSurveyTemplateQuestionGroup::STANDARD_TRANSFERS_WITHIN_SUPPORT . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS transfers_within_support_total_answer_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::CLOSURE . ', ' . CSurveyTemplateQuestionGroup::STANDARD_CLOSURE . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS closure_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::CLOSURE . ', ' . CSurveyTemplateQuestionGroup::STANDARD_CLOSURE . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS closure_total_answer_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::AUTO_FAILS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_AUTO_FAILS . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS auto_fails_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::AUTO_FAILS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_AUTO_FAILS . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS auto_fails_total_answer_numeric_weight,
							COUNT( CASE WHEN
											is_percentage = 1
											AND question_numeric_weight = 0
											AND numeric_weight = 0
										THEN survey_template_question_id ELSE NULL
									END ) AS failed_questions_count
						FROM (
								SELECT
									sqa.id,
									sqa.survey_template_question_id,
									sqa.survey_template_question_group_id,
									sqa.answer,
									sqa.numeric_weight,
									s.id as survey_id,
									s.trigger_reference_id As call_id,
									s.responder_reference_id AS graded_by,
									s.subject_reference_id AS employee_id,
									s.name_full AS employee_name,
									s.preferred_name AS preferred_name,
									s.team_id,
									s.manager_employee_id,
									s.survey_datetime,
									s.response_datetime,
									s.survey_template_id,
									stq.is_percentage,
									stq.numeric_weight as question_numeric_weight,
									 ( ( stq.numeric_weight * sqa.numeric_weight ) / 100 ) as answer_numeric_weight,
									rank() OVER ( PARTITION BY s.trigger_reference_id ORDER BY s.survey_datetime DESC ) as rank
								FROM
									survey_question_answers sqa
									JOIN survey_calls s ON ( s.id = sqa.survey_id AND s.survey_template_id = sqa.survey_template_id)
									LEFT JOIN survey_template_questions stq ON ( stq.survey_template_id = sqa.survey_template_id AND sqa.survey_template_question_id = stq.id )
							 ) AS innerquery
						WHERE
							 innerquery.rank = 1
						GROUP BY
							innerquery.survey_id,
							innerquery.call_id,
							innerquery.graded_by,
							innerquery.employee_id,
							innerquery.employee_name,
							innerquery.preferred_name,
							innerquery.survey_datetime,
							innerquery.response_datetime,
							innerquery.team_id,
							innerquery.manager_employee_id,
							innerquery.survey_template_id
						ORDER BY
							' . $strInnerOrderBy . '
					) AS subquery';

		$arrmixSurveyCallsData = ( array ) fetchData( $strSql, $objAdminDatabase );

		if( false == valArr( $arrmixSurveyCallsData ) ) {
			return NULL;
		}

		$arrmixSurveyData = array();

		foreach( $arrmixSurveyCallsData as $arrmixSurveyCallData ) {
			$arrintTotalPercentage = array();

			if( true == isset( $arrmixSurveyCallData['answer'] ) ) {
				$arrmixSurveyCallData['answer'] = CSurvey::getDecryptedData( $arrmixSurveyCallData['answer'] );
			}

			$intTotalQuestionsNumericWeight = ( $arrmixSurveyCallData['greeting_total_question_numeric_weight'] + $arrmixSurveyCallData['ticket_creation_total_question_numeric_weight'] + $arrmixSurveyCallData['escalated_ticket_total_question_numeric_weight'] + $arrmixSurveyCallData['call_flow_total_question_numeric_weight'] + $arrmixSurveyCallData['etiquette_total_question_numeric_weight'] + $arrmixSurveyCallData['troubleshooting_and_accuracy_total_question_numeric_weight'] + $arrmixSurveyCallData['transfers_within_support_total_question_numeric_weight'] + $arrmixSurveyCallData['closure_total_question_numeric_weight'] );

			if( 0 < $intTotalQuestionsNumericWeight ) {
				$arrintTotalPercentage['total_score'] = ( ( $arrmixSurveyCallData['greeting_total_answer_numeric_weight'] + $arrmixSurveyCallData['ticket_creation_total_answer_numeric_weight'] + $arrmixSurveyCallData['escalated_ticket_total_answer_numeric_weight'] + $arrmixSurveyCallData['call_flow_total_answer_numeric_weight'] + $arrmixSurveyCallData['etiquette_total_answer_numeric_weight'] + $arrmixSurveyCallData['troubleshooting_and_accuracy_total_answer_numeric_weight'] + $arrmixSurveyCallData['transfers_within_support_total_answer_numeric_weight'] + $arrmixSurveyCallData['closure_total_answer_numeric_weight'] ) * 100 ) / $intTotalQuestionsNumericWeight;
			} else {
				$arrintTotalPercentage['total_score'] = 100;
			}

			if( ( 0 == $arrmixSurveyCallData['auto_fails'] ) && ( NULL != $arrmixSurveyCallData['auto_fails'] ) ) {
				$arrintTotalPercentage['total_score'] = 0;
			} elseif( 100 > $arrmixSurveyCallData['auto_fails'] ) {
				$arrintTotalPercentage['total_score'] = ( ( $arrintTotalPercentage['total_score'] * ( 100 - $arrmixSurveyCallData['auto_fails'] ) ) / 100 );
			}

			$arrmixSurveyData[] = array_merge( $arrmixSurveyCallData, $arrintTotalPercentage );
		}

		return $arrmixSurveyData;
	}

	public static function fetchLastSurveyByResponderReferenceIdBySurveyTypeId( $intResponderReferenceId, $intSurveyTypeId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						id IN (
								SELECT
									max( id )
								 FROM
								 	surveys
								 WHERE
									responder_reference_id = ' . ( int ) $intResponderReferenceId . '
									AND survey_type_id = ' . ( int ) $intSurveyTypeId . '
								 GROUP BY
									responder_reference_id,
								 	survey_type_id )';

		return self::fetchSurvey( $strSql, $objDatabase );
	}

	public static function fetchSurveysByCidBySurveyTypeIdsByMonth( $intCid, $arrintSurveyTypes = NULL, $intMonth, $objDatabase ) {
		( true == valArr( $arrintSurveyTypes ) ) ? $strCondition = ' AND s.survey_type_id IN ( ' . implode( ',', $arrintSurveyTypes ) . ' ) ' : $strCondition = '';

		$strSql = 'SELECT
						COUNT(
								CASE
									WHEN sqa.numeric_weight >= ' . CSurvey::SCORE_PROMOTER . ' THEN 1
								END
							) as promoter_count,
						COUNT(
								CASE
									WHEN sqa.numeric_weight <= ' . CSurvey::SCORE_DETRACTOR . ' THEN 1
								END
							) as detractor_count,
						COUNT(
								CASE
									WHEN sqa.numeric_weight > ' . CSurvey::SCORE_DETRACTOR . ' AND sqa.numeric_weight < ' . CSurvey::SCORE_PROMOTER . ' THEN 1
								END
							) as passive_count,
						EXTRACT( month from date_trunc( \'month\', s.response_datetime ) ) as score_month
					FROM
						surveys s
						JOIN persons p ON s.responder_reference_id = p.id
						JOIN survey_question_answers sqa ON s.id = sqa.survey_id
					WHERE
						p.cid = ' . ( int ) $intCid . '
						AND sqa.numeric_weight IS NOT NULL
						AND sqa.numeric_weight > ' . CSurvey::SCORE_NO . '
						' . $strCondition . '
						AND s.response_datetime BETWEEN date_trunc( \'month\', now() ) - INTERVAL \'' . $intMonth . ' months\' AND date_trunc( \'month\', now() ) - interval \'1 day\'
					GROUP BY
						date_trunc( \'month\', s.response_datetime)
					ORDER BY
						date_trunc( \'month\', s.response_datetime)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSurveyScoresByCid( $intCid, $objDatabase ) {
		$strSql = 'SELECT
						s.id,
						p.company_user_id,
						sqa.numeric_weight as score,
						sqa.answer as memo,
						DATE_TRUNC ( \'month\', s.response_datetime ) AS score_date,
						DATE_TRUNC ( \'month\', s.survey_datetime ) AS send_date
					FROM
						surveys s
						JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						JOIN persons p ON ( p.id = s.responder_reference_id AND p.cid = ' . ( int ) $intCid . ' )
					WHERE
						p.company_user_id IS NOT NULL
						AND	( s.response_datetime >= NOW() - INTERVAL \'1 year\'
							OR s.survey_datetime >= NOW() - INTERVAL \'1 year\' )
					ORDER BY
						s.response_datetime ASC';

		$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			foreach( $arrmixFetchedData as &$arrmixData ) {
				$arrmixData['memo'] = CSurvey::getDecryptedData( $arrmixData['memo'] );
			}
		}

		return $arrmixFetchedData;

	}

	public static function fetchSurveyScoresByCidByDatetimeRange( $intCid, $objStdScoreDatetime, $objDatabase ) {
		$strSql = 'SELECT
						s.id,
						p.company_user_id,
						sqa.numeric_weight as score,
						sqa.answer as memo,
						t.id as response_task_id,
						t.task_status_id,
						s.response_datetime as score_datetime,
						DATE_TRUNC( \'month\', s.response_datetime ) as score_date,
						DATE_TRUNC( \'month\', s.survey_datetime ) as send_date
					FROM
						surveys s
						JOIN persons p ON s.responder_reference_id = p.id
						JOIN survey_question_answers sqa ON s.id = sqa.survey_id
						LEFT JOIN survey_tasks st ON s.id = st.survey_id
						LEFT JOIN tasks t ON ( st.task_id = t.id AND t.task_type_id = ' . CTaskType::NPS . ' )
					WHERE
						p.company_user_id IS NOT NULL
						AND p.cid =' . ( int ) $intCid;

		if( true == isset( $objStdScoreDatetime->fromDate ) && '' != $objStdScoreDatetime->fromDate && true == isset( $objStdScoreDatetime->toDate ) && '' != $objStdScoreDatetime->toDate ) {
			$strSql .= ' AND ( ( s.response_datetime::date >= \'' . $objStdScoreDatetime->fromDate . '\'::date AND s.response_datetime::date <= \'' . $objStdScoreDatetime->toDate . '\'::date )
								 OR ( s.survey_datetime::date >= \'' . $objStdScoreDatetime->fromDate . '\'::date AND s.survey_datetime::date <= \'' . $objStdScoreDatetime->toDate . '\'::date ) ) ';
		} elseif( true == isset( $objStdScoreDatetime->fromDate ) && '' != $objStdScoreDatetime->fromDate && ( false == isset( $objStdScoreDatetime->toDate ) || '' == $objStdScoreDatetime->toDate ) ) {
			$strSql .= ' AND ( s.response_datetime::date >= \'' . $objStdScoreDatetime->fromDate . '\'::date OR s.survey_datetime::date >= \'' . $objStdScoreDatetime->fromDate . '\'::date )';
		} elseif( ( false == isset( $objStdScoreDatetime->fromDate ) || '' == $objStdScoreDatetime->fromDate ) && true == isset( $objStdScoreDatetime->toDate ) && '' != $objStdScoreDatetime->toDate ) {
			$strSql .= ' AND ( s.response_datetime::date <= \'' . $objStdScoreDatetime->toDate . '\'::date OR s.survey_datetime::date <= \'' . $objStdScoreDatetime->toDate . '\'::date )';
		}

		$strSql .= ' ORDER BY
						s.survey_datetime ASC';

		$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			foreach( $arrmixFetchedData as &$arrmixData ) {
				$arrmixData['memo'] = CSurvey::getDecryptedData( $arrmixData['memo'] );
			}
		}

		return $arrmixFetchedData;
	}

	public static function fetchLastWeekTicketSatisfactionRatingGradeWeekBySubjectReferenceId( $intSubjectReferenceId, $objDatabase ) {
		$strSql = ' SELECT
						CASE
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) >= 9.3 THEN \'A\'
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) >= 9 AND round ( AVG ( sqa.numeric_weight ), 1 ) < 9.3 THEN \'A-\'
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) >= 8.7 AND round ( AVG ( sqa.numeric_weight ), 1 ) < 9 THEN \'B+\'
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) >= 8.3 AND round ( AVG ( sqa.numeric_weight ), 1 ) < 8.7 THEN \'B\'
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) >= 8 AND round ( AVG ( sqa.numeric_weight ), 1 ) < 8.3 THEN \'B-\'
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) >= 7.7 AND round ( AVG ( sqa.numeric_weight ), 1 ) < 8 THEN \'C+\'
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) >= 7.3 AND round ( AVG ( sqa.numeric_weight ), 1 ) < 7.7 THEN \'C\'
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) >= 7 AND round ( AVG ( sqa.numeric_weight ), 1 ) < 7.3 THEN \'C-\'
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) >= 6.7 AND round ( AVG ( sqa.numeric_weight ), 1 ) < 7 THEN \'D+\'
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) >= 6.3 AND round ( AVG ( sqa.numeric_weight ), 1 ) < 6.7 THEN \'D\'
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) >= 6 AND round ( AVG ( sqa.numeric_weight ), 1 ) < 6.3 THEN \'D-\'
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) < 6 THEN \'F\'
							WHEN round ( AVG ( sqa.numeric_weight ), 1 ) IS NULL THEN \'NA\'
						END AS current_ticket_satisfaction_rating,
							EXTRACT ( WEEK FROM s.response_datetime ) AS weeks
					FROM
						surveys s
						JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						JOIN ps_leads pl ON ( pl.id = s.ps_lead_id )
					WHERE
						s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . '
						AND DATE ( s.response_datetime ) >= DATE ( CURRENT_DATE - INTERVAL \'1 week\' )
						AND sqa.numeric_weight > 0
						AND s.subject_reference_id = ' . ( int ) $intSubjectReferenceId . '
					GROUP BY
						weeks';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTwelveMonthsSupportSurveyRatingCountBySubjectReferenceId( $intSubjectReferenceId, $objDatabase ) {
		$strSql = '	SELECT
						count ( DISTINCT ( st.task_id ) )
					FROM
						surveys s
						JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						JOIN survey_tasks st ON ( s.id = st.survey_id )
						JOIN task_assignments ta ON ( ta.task_id = st.task_id AND ta.task_type_id = ' . CTaskType::NPS . ' )
					WHERE
						s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . '
						AND s.response_datetime > ( NOW ( ) - INTERVAL \'12 months\' )
						AND sqa.numeric_weight > 0
						AND s.subject_reference_id = ' . ( int ) $intSubjectReferenceId;

		$arrintResponse = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

	}

	public static function fetchTwelveMonthsFirstResponseTimeBySubjectReferenceId( $intSubjectReferenceId, $objDatabase ) {
		$strSql = 'SELECT
						DATE_PART( \'year\', ( age ( max( updated_on ), min( updated_on ) ) ) ) as year,
						DATE_PART( \'month\', ( age ( max( updated_on ), min( updated_on ) ) ) ) as month,
						DATE_PART( \'day\', ( age ( max( updated_on ), min( updated_on ) ) ) ) as day,
						DATE_PART( \'hour\', ( age ( max( updated_on ), min( updated_on ) ) ) ) as hour,
						DATE_PART( \'minute\', ( age ( max( updated_on ), min( updated_on ) ) ) ) as minute,
						round(DATE_PART( \'second\',( age ( max( updated_on ), min( updated_on ) ) ) ) ) as second,
						task_id
					FROM
						(
							SELECT
								ta.updated_on AS updated_on,
								ta.task_id AS task_id,
								rank ( ) over ( PARTITION BY ta.task_id
							ORDER BY
								ta.task_id, ta.updated_on ASC ) AS rank
							FROM
								surveys s
								JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
								JOIN survey_tasks st ON ( s.id = st.survey_id )
								JOIN task_assignments ta ON ( ta.task_id = st.task_id AND ta.task_type_id = ' . CTaskType::NPS . ' )
							WHERE
								s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . '
								AND s.response_datetime > ( NOW ( ) - INTERVAL \'12 months\' )
								AND sqa.numeric_weight > 0
								AND s.subject_reference_id = ' . ( int ) $intSubjectReferenceId . '
							GROUP BY
								ta.task_id,
								ta.updated_on
							ORDER BY
								ta.updated_on ASC
						) AS filtered_data
					WHERE
						filtered_data.rank <= 2
					GROUP BY
						task_id
					ORDER BY
						task_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSurveyDetailsBySubjectReferenceId( $intSubjectReferenceId, $objDatabase ) {
		$strSql = 'SELECT
						EXTRACT ( WEEK FROM s.response_datetime ) AS weeks,
						round ( avg ( sqa.numeric_weight ), 2 ) AS score
					FROM
						surveys s
						JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						JOIN ps_leads pl ON ( pl.id = s.ps_lead_id )
					WHERE
						s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . '
						AND DATE ( s.response_datetime ) >= DATE ( CURRENT_DATE - INTERVAL \'30 week\' )
						AND sqa.numeric_weight > 0
						AND s.subject_reference_id = ' . ( int ) $intSubjectReferenceId . '
					GROUP BY
						weeks
					ORDER BY
						weeks DESC
					LIMIT
						30';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTwelveMonthsSupportSurveyRatingsBySubjectReferenceId( $intSubjectReferenceId, $objDatabase, $intPageNo, $intPageSize ) {
		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit 	= ( int ) $intPageSize;

		$strSql = 'SELECT
						DISTINCT ( st.task_id ),
						mc.id,
						mc.company_name,
						s.responder_reference_id,
						( p.name_first || \' \' || p.name_last ) AS company_user_fullname,
						sqa.numeric_weight,
						sqa.answer,
						s.survey_datetime,
						p.permissioned_units,
						p.person_role_id,
						DATE_PART( \'year\', ( age ( max( ta.updated_on ), min( ta.updated_on ) ) ) ) as year,
						DATE_PART( \'month\', ( age ( max( ta.updated_on ), min( ta.updated_on ) ) ) ) as month,
						DATE_PART( \'day\', ( age ( max( ta.updated_on ), min( ta.updated_on ) ) ) ) as day,
						DATE_PART( \'hour\', ( age ( max( ta.updated_on ), min( ta.updated_on ) ) ) ) as hour,
						DATE_PART( \'minute\', ( age ( max( ta.updated_on ), min( ta.updated_on ) ) ) ) as minute,
						round(DATE_PART( \'second\',( age ( max( ta.updated_on ), min( ta.updated_on ) ) ) ) ) as second
					FROM
						surveys s
						JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						JOIN survey_tasks st ON ( st.survey_id = s.id )
						JOIN persons p ON ( p.id = s.responder_reference_id )
						JOIN clients mc ON ( mc.ps_lead_id = s.ps_lead_id )
						JOIN task_assignments ta ON ( ta.task_id = st.task_id AND ta.task_type_id = ' . CTaskType::NPS . ' )
					WHERE
						s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . '
						AND s.survey_datetime > ( NOW ( ) - INTERVAL \'12 months\' )
						AND sqa.numeric_weight > 0
						AND	s.subject_reference_id = ' . ( int ) $intSubjectReferenceId . '
					GROUP BY
						st.task_id,
						mc.id,
						mc.company_name,
						s.responder_reference_id,
						company_user_fullname,
						sqa.numeric_weight,
						s.survey_datetime,
						p.permissioned_units,
						sqa.answer,
						p.person_role_id
					ORDER BY
						 s.survey_datetime DESC
						 OFFSET ' . ( int ) $intOffset . '
						 LIMIT ' . ( int ) $intLimit;

		$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			foreach( $arrmixFetchedData as &$arrmixData ) {
				$arrmixData['answer'] = CSurvey::getDecryptedData( $arrmixData['answer'] );
			}
		}

		return $arrmixFetchedData;
	}

	public static function fetchSurveysByNpsFilter( $objSurveyFilter, $objDatabase ) {
		$strSql = 'SELECT
						s.id,
						p.id AS person_id,
						mc.id AS cid,
						p.title,
						p.email_address,
						subq.phone_number AS phone_number1,
						s.survey_type_id,
						s.trigger_reference_id,
						s.responder_reference_id,
						p.company_user_id,
						p.name_first || \' \' || p.name_last as company_user_fullname,
						p.permissioned_units,
						e.preferred_name AS psi_employee_name,
						TO_CHAR( s.response_datetime, \'mm/dd/yyyy\') AS score_datetime_detail,
						sqa.numeric_weight,
						sqa.answer,
						s.survey_datetime AS survey_date,
						s.response_datetime AS response_date,
						s.updated_by,
						e1.preferred_name AS support_user
					FROM
						surveys AS s
						LEFT JOIN clients AS mc ON ( mc.ps_lead_id = s.ps_lead_id AND s.survey_type_id <> ' . CSurveyType::EMPLOYEE_NPS . ' )
						LEFT JOIN survey_question_answers AS sqa ON ( sqa.survey_id = s.id )
						LEFT JOIN persons AS p ON ( p.id = s.responder_reference_id AND p.company_user_id IS NOT NULL AND s.survey_type_id <> ' . CSurveyType::EMPLOYEE_NPS . ' )
						LEFT JOIN (
									SELECT
										MAX ( phone_number ) AS phone_number,
										person_id
									FROM
										person_phone_numbers
									WHERE
										phone_number_type_id = ' . CPhoneNumberType::OFFICE . '
										AND is_preferred = TRUE
									GROUP BY
										person_id
									) AS subq ON ( p.id = subq.person_id )
						LEFT JOIN employees AS e ON ( ( s.responder_reference_id = e.id AND s.survey_type_id = ' . CSurveyType::EMPLOYEE_NPS . ' ) OR ( s.subject_reference_id = e.id AND s.survey_type_id = ' . CSurveyType::CLIENT_SUCCESS_MANAGER_REVIEW . ' ) )
						LEFT JOIN employees AS e1 ON ( s.subject_reference_id = e1.id )
					WHERE s.survey_type_id != ' . CSurveyType::PERSONAL_REVIEW . ' AND sqa.is_hidden != true';

		if( true == valObj( $objSurveyFilter, 'CSurveyFilter' ) ) {
			$strToDate				= $objSurveyFilter->getToDate();
			$strFromDate			= $objSurveyFilter->getFromDate();
			$arrintSurveyTypes		= $objSurveyFilter->getSurveyTypes();
			$arrintPersonRoles		= $objSurveyFilter->getPersonRoles();
			$arrintSupportEmployees	= $objSurveyFilter->getSupportEmployees();

			$arrintScoreRange = ( false == is_null( $objSurveyFilter->getFromScore() ) && false == is_null( $objSurveyFilter->getToScore() ) ) ? range( $objSurveyFilter->getFromScore(), $objSurveyFilter->getToScore() ) : array();

			if( true == valArr( $arrintSurveyTypes ) ) {
				if( true == in_array( CSurveyType::SUPPORT_REVIEW, $arrintSurveyTypes ) && true == valArr( $arrintSupportEmployees ) ) {
					$arrintSurveyTypes = array_diff( $arrintSurveyTypes, array( CSurveyType::SUPPORT_REVIEW ) );
					$strSql .= ( true == valArr( $arrintSurveyTypes ) ) ? ' AND ( s.survey_type_id IN( ' . implode( ',', $arrintSurveyTypes ) . ' ) OR ( s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . ' AND s.subject_reference_id IN ( ' . implode( ',', $arrintSupportEmployees ) . ' ) ) )' : ' AND s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . ' AND s.subject_reference_id IN ( ' . implode( ',', $arrintSupportEmployees ) . ' ) ';
				} else {
					$strSql .= ' AND s.survey_type_id IN( ' . implode( ',', $arrintSurveyTypes ) . ' )';
				}
			}

			if( true == valArr( $arrintPersonRoles ) ) {
				$strSql .= ' AND p.person_role_id IN ( ' . implode( ',', $arrintPersonRoles ) . ')';
			}

			if( true == valArr( $arrintScoreRange ) ) {
				$strSql .= ' AND sqa.numeric_weight IN ( ' . implode( ',', $arrintScoreRange ) . ')';
			}

			if( true == isset( $strFromDate ) && '' != $strFromDate ) {
				$strSql .= ' AND s.response_datetime::date >= \'' . $strFromDate . '\'::date';
			}

			if( true == isset( $strToDate ) && '' != $strToDate ) {
				$strSql .= ' AND s.response_datetime::date <= \'' . $strToDate . '\'::date';
			}
		}

		$strSql .= ' ORDER BY 
						response_date ASC';

		$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			foreach( $arrmixFetchedData as &$arrmixData ) {
				$arrmixData['answer'] = CSurvey::getDecryptedData( $arrmixData['answer'] );
			}
		}

		return $arrmixFetchedData;
	}

	public static function fetchLatestSurveysByCidsByCompanyUserIdsBySurveyTypeIds( $arrintCids, $arrintCompanyUserIds, $arrintSurveyTypeIds, $objDatabase, $objSurveyFilter = NULL ) {
		if( false == valArr( $arrintCids ) || false == valArr( $arrintCompanyUserIds ) || false == valArr( $arrintSurveyTypeIds ) ) return NULL;

		$strSql = 'SELECT
						max(s.id) as id,
						p.company_user_id,
						TO_CHAR( s.response_datetime, \'mm/dd/yyyy\') AS response_datetime,
						sqa.numeric_weight,
						sqa.answer,
						p.cid,
						s.survey_type_id,
						s.trigger_reference_id
					FROM
						surveys s
						JOIN survey_question_answers sqa ON ( sqa.survey_id = s.id )
						JOIN persons p ON ( p.id = s.responder_reference_id )
					WHERE
						p.cid = ANY ( ARRAY[ ' . implode( ',', $arrintCids ) . ' ] )
						AND s.survey_type_id = ANY ( ARRAY[ ' . implode( ',', $arrintSurveyTypeIds ) . ' ] )
						AND p.company_user_id = ANY ( ARRAY[ ' . implode( ',', $arrintCompanyUserIds ) . ' ] ) ';

		if( true == valObj( $objSurveyFilter, 'CSurveyFilter' ) ) {
			if( true == valStr( $objSurveyFilter->getFromDate() ) ) $strSql .= ' AND ( s.response_datetime::date <= \'' . $objSurveyFilter->getFromDate() . '\'::date ) ';
		}

		$strSql .= 'GROUP BY
						p.company_user_id, s.response_datetime, sqa.numeric_weight, sqa.answer, p.cid, s.survey_type_id, s.trigger_reference_id';

		$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			foreach( $arrmixFetchedData as &$arrmixData ) {
				$arrmixData['answer'] = CSurvey::getDecryptedData( $arrmixData['answer'] );
			}
		}

		return $arrmixFetchedData;
	}

	public static function fetchSurveysByFilterId( $objSurveyFilter, $arrmixFilterFactor, $objDatabase ) {
		if( false == valArr( $arrmixFilterFactor ) ) return NULL;

		$strSql = 'SELECT
						sqa.answer,
						p.title,
						p.permissioned_units,
						p.cid,
						p.name_first || \' \' ||p.name_last as company_user_fullname,
						sqa.numeric_weight,
						s.survey_datetime as survey_date,
						s.response_datetime as response_date,
						e.name_full
					FROM
						surveys s
						LEFT JOIN survey_question_answers sqa ON sqa.survey_id = s.id
						JOIN persons p ON ( p.id = s.responder_reference_id AND p.company_user_id IS NOT NULL )
						JOIN employees e ON ( e.id = s.subject_reference_id )
					WHERE
						' . key( $arrmixFilterFactor ) . ' = ' . ( int ) array_pop( $arrmixFilterFactor );

		if( true == valObj( $objSurveyFilter, 'CSurveyFilter' ) ) {
			$strToDate			= $objSurveyFilter->getToDate();
			$strFromDate		= $objSurveyFilter->getFromDate();
			$arrintSurveyTypes	= $objSurveyFilter->getSurveyTypes();
			$arrintPersonRoles	= $objSurveyFilter->getPersonRoles();

			$arrintScoreRange = ( false == is_null( $objSurveyFilter->getFromScore() ) && false == is_null( $objSurveyFilter->getToScore() ) ) ? range( $objSurveyFilter->getFromScore(), $objSurveyFilter->getToScore() ) : array();

			if( true == valArr( $arrintSurveyTypes ) ) {
				$strSql .= ' AND s.survey_type_id IN( ' . implode( ',', $arrintSurveyTypes ) . ' )';
			}

			if( true == valArr( $arrintPersonRoles ) ) {
				$strSql .= ' AND p.person_role_id IN ( ' . implode( ',', $arrintPersonRoles ) . ')';
			}

			if( true == valArr( $arrintScoreRange ) ) {
				$strSql .= ' AND sqa.numeric_weight IN ( ' . implode( ',', $arrintScoreRange ) . ')';
			}

			if( true == isset( $strFromDate ) && '' != $strFromDate ) {
				$strSql .= ' AND s.response_datetime::date >= \'' . $strFromDate . '\'::date';
			}

			if( true == isset( $strToDate ) && '' != $strToDate ) {
				$strSql .= ' AND s.response_datetime::date <= \'' . $strToDate . '\'::date';
			}
		}

		$strSql .= ' AND sqa.is_hidden != true ORDER BY s.response_datetime';

		$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			foreach( $arrmixFetchedData as &$arrmixData ) {
				$arrmixData['answer'] = CSurvey::getDecryptedData( $arrmixData['answer'] );
			}
		}

		return $arrmixFetchedData;
	}

	public static function fetchSurveyRatingsByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT ( s.subject_reference_id ),
						s.id,
						sqa.numeric_weight AS rating,
						s.response_datetime AS date_of_rating
					FROM
						surveys s
						JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id AND sqa.numeric_weight IS NOT NULL )
						JOIN scheduled_surveys ss ON ( ss.responder_reference_id = ss.subject_reference_id )
						JOIN employee_addresses ea ON( s.subject_reference_id = ea.employee_id )
					WHERE
						s.subject_reference_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ')
						AND (
								( ea.country_code = \'' . CCountry::CODE_INDIA . '\' AND sqa.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_IN . ' )
								OR
								( ea.country_code = \'' . CCountry::CODE_USA . '\' AND sqa.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_US . ' )
							)
					ORDER BY
						s.subject_reference_id,
						s.response_datetime';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSurveysByResponderReferenceIdBySurveyIds( $intResponderReferenceId, $arrintSurveyIds, $objDatabase, $boolIsView = false ) {
		if( false == valArr( $arrintSurveyIds ) ) return NULL;

		$strWhereClause = '';
		$strWhereClause .= ( false == $boolIsView ) ? ' AND s.response_datetime IS NULL AND s.declined_datetime IS NULL' : '';

		$strSql = '	SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						s.id,
						s.scheduled_survey_id,
						s.survey_template_id,
						s.survey_type_id,
						s.ps_lead_id,
						s.trigger_reference_id,
						s.responder_reference_id,
						s.details,
						CASE
							WHEN s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . '
							THEN u.employee_id
							ELSE s.subject_reference_id
						END AS subject_reference_id,
						s.survey_datetime,
						s.response_datetime,
						s.declined_datetime,
						CASE
							WHEN ( s.survey_type_id = ' . CSurveyType::TRAINING . ' AND s.subject_reference_id IS NULL )
							THEN ( st.name ||\' for \' || ea.name_first || \' \' || ea.name_last || \' (\' || pjps.name || \') \' || CASE WHEN( eai.scheduled_on::DATE < CURRENT_DATE ) THEN \'held on \' ELSE \'on \' END || to_char( eai.scheduled_on,\'Mon DD, YYYY HH12:MI AM\')|| \' applied for \' || pwjp.name )
							ELSE util_get_system_translated( \'name\', st.name, st.details )
						END as survey_name,
						e.name_full as responder_name,
						e1.name_full as subject_name,
						e1.preferred_name as Preferred_name,
						st.is_required
					FROM
						surveys s
						JOIN survey_templates st ON ( s.survey_template_id = st.id AND st.is_published = 1 )
						LEFT JOIN employees e ON( e.id = s.responder_reference_id )
						LEFT JOIN employees e1 ON ( e1.id = s.subject_reference_id AND e1.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						LEFT JOIN task_assignments ta ON ( s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . ' AND s.trigger_reference_id IS NOT NULL AND s.trigger_reference_id = ta.task_id
															AND ta.id = (
																	SELECT
																		 MAX( ta1.id )
																	FROM
																		task_assignments ta1
																		JOIN users u ON ( ta1.user_id = u.id )
																		JOIN employees e ON ( u.employee_id = e.id AND e.department_id = ' . CDepartment::TECHNICAL_SUPPORT . ' )
																	WHERE
																		s.trigger_reference_id = ta1.task_id
																		AND s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . '
																		AND s.trigger_reference_id IS NOT NULL
																)
														)
						LEFT JOIN users u ON ( s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . ' AND ta.user_id = u.id )
						LEFT JOIN employee_application_interviews eai ON ( s.trigger_reference_id = eai.id AND s.survey_type_id = ' . CSurveyType::TRAINING . ' AND s.subject_reference_id IS NULL )
						LEFT JOIN employee_applications ea ON ( eai.employee_application_id = ea.id )
						LEFT JOIN ps_job_posting_steps pjps ON ( eai.ps_job_posting_step_id = pjps.id )
						LEFT JOIN ps_website_job_postings pwjp ON ( ea.ps_website_job_posting_id = pwjp.id )
					WHERE
						CASE
							WHEN s.survey_type_id = ' . CSurveyType::PEER_REVIEW . ' or s.survey_type_id = ' . CSurveyType::CODE_AUDIT . '
								THEN subject_reference_id = ' . ( int ) $intResponderReferenceId . ' or responder_reference_id = ' . ( int ) $intResponderReferenceId . '
							WHEN s.survey_type_id = ' . CSurveyType::MANAGER_SURVEY . ' or s.survey_type_id = ' . CSurveyType::PERSONAL_REVIEW . ' or s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . ' or s.survey_type_id = ' . CSurveyType::LEASING_CENTER_QA . ' or s.survey_type_id = ' . CSurveyType::SUPPORT_QA_CALLS . ' or s.survey_type_id = ' . CSurveyType::SUPPORT_QA_TASKS . '
								THEN TRUE
							WHEN st.id IN( ' . implode( ', ', array_merge( CSurveyTemplate::$c_arrintNoDuesSurveyTemplates, array( CSurveyTemplate::EXIT_INTERVIEW ) ) ) . ' )
								THEN TRUE
							ELSE
								responder_reference_id = ' . ( int ) $intResponderReferenceId . '
						END
						AND s.id IN (' . implode( ',', $arrintSurveyIds ) . ')' .
		          $strWhereClause;

		return parent::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchSurveyResponseSummaryBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		$strSql = 'SELECT
						sqa.survey_template_question_id,
						stq.question,
						stq.survey_question_type_id,
						sqa.survey_template_option_id,
						sto.option_name,
						sqa.numeric_weight,
						sqa.answer
					FROM
						survey_question_answers sqa
						JOIN survey_template_questions stq ON ( stq.id = sqa.survey_template_question_id )
						LEFT JOIN survey_template_options sto ON ( sqa.survey_template_option_id = sto.id )
					WHERE
						sqa.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND stq.is_published = 1
					ORDER BY
						sqa.updated_on';

		$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			foreach( $arrmixFetchedData as &$arrmixData ) {
				$arrmixData['answer'] = CSurvey::getDecryptedData( $arrmixData['answer'] );
			}
		}

		return $arrmixFetchedData;
	}

	public static function fetchSurveysByResponderReferenceIds( $arrintResponderReferenceIds, $objDatabase, $boolIsViewHistory = false, $boolIsView = false, $boolIsReturnKeyedArray = false, $boolIsAddCompletedOnDate = false, $strFromDate = NULL, $strToDate = NULL, $arrstrReviewCycleDates = NULL, $boolCpaFeedback = false, $boolDashboardCpaRating = false ) {

		if( false == valArr( $arrintResponderReferenceIds ) ) {
			return NULL;
		}

		if( true == valArr( $arrstrReviewCycleDates ) ) {
			$strCase = '( CASE ';
			foreach( $arrstrReviewCycleDates AS $intKey => $strDate ) {
				if( true == valStr( $strDate ) ) {
					if( true == $boolDashboardCpaRating ) {
						$strCase .= 'WHEN s.subject_reference_id IN (' . ( int ) $intKey . ') AND s.survey_datetime::date >=  \'' . $strDate . '\'::date THEN \'' . 1 . '\'';
					} else {
						$strCase .= 'WHEN s.subject_reference_id IN (' . ( int ) $intKey . ') AND s.response_datetime::date >=  \'' . $strDate . '\'::date THEN \'' . 1 . '\'';
					}
				}

			}

			$strCase .= 'ELSE 0 END )AS review_cycle,';

			$strReviewCycle = 'WHERE sub.review_cycle = 1';
		}

		$strWhereManagerAdditionalClause = $strWhereEmployeeAdditionalClause = '';
		$strWhereClause = ( false == $boolIsViewHistory ) ? ' WHERE id = latest_survey_id' : '';
		$strWhereClause .= ( true == $boolIsView ) ? ' AND response_datetime IS NOT NULL' : '';
		if( true == $boolIsAddCompletedOnDate && true == valStr( $strFromDate ) && true == valStr( $strToDate ) && false == $boolCpaFeedback ) {
			$strWhereManagerAdditionalClause .= ' AND s.response_datetime::date >= \'' . $strFromDate . '\' AND s.response_datetime::date <= \'' . $strToDate . '\'';
		} elseif( true == $boolIsAddCompletedOnDate && true == valStr( $strFromDate ) && true == $boolCpaFeedback ) {
			$strWhereManagerAdditionalClause .= ' AND s.response_datetime::date >= \'' . $strFromDate . '\'';
		}

		$strWhereEmployeeAdditionalClause = ( true == $boolIsAddCompletedOnDate && true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? ' AND s.response_datetime::date >= \'' . $strFromDate . '\' AND s.response_datetime::date <= \'' . $strToDate . '\'': '';
		$strOrderByClause = ( true == $boolIsViewHistory ) ? ' ORDER BY survey_datetime DESC, survey_template_question_id' : '';

		$strSql = 'SELECT * FROM
					 	(
							SELECT
								DISTINCT ON ( s.id, sqa.id )
								s.*,
								' . $strCase . '
								sqa.id as survey_question_answer_id,
								sqa.survey_template_question_id,
								MAX( s.id ) OVER ( PARTITION BY s.subject_reference_id ) as latest_survey_id,
								ROW_NUMBER() OVER ( PARTITION BY s.subject_reference_id ORDER BY s.id DESC ) AS row_number,
								sqa.answer
							FROM
								surveys s
								LEFT JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id AND ( sqa.survey_question_type_id = ' . CSurveyQuestionType::TEXT_AREA . ' OR sqa.survey_question_type_id = ' . CSurveyQuestionType::RATING_W_OPTIONAL_TEXT . ' ) )
							WHERE
								s.subject_reference_id IN( ' . implode( ',', $arrintResponderReferenceIds ) . ' )
								AND s.survey_type_id = ' . CSurveyType::MANAGER_SURVEY . $strWhereManagerAdditionalClause . '
							) as sub ' . $strReviewCycle . $strWhereClause . '
						UNION
						SELECT * FROM
							(
								SELECT
									DISTINCT ON ( s.id, sqa.id )
									s.*,
									' . $strCase . '
									sqa.id as survey_question_answer_id,
									sqa.survey_template_question_id,
									MAX( s.id ) OVER ( PARTITION BY s.responder_reference_id ) as latest_survey_id,
									ROW_NUMBER() OVER ( PARTITION BY s.responder_reference_id ORDER BY s.id DESC ) AS row_number,
									sqa.answer
								FROM
									surveys s
									LEFT JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id AND ( sqa.survey_question_type_id = ' . CSurveyQuestionType::TEXT_AREA . ' OR sqa.survey_question_type_id = ' . CSurveyQuestionType::RATING_W_OPTIONAL_TEXT . ' ) )
								WHERE
									s.subject_reference_id IN( ' . implode( ',', $arrintResponderReferenceIds ) . ' )
									AND s.responder_reference_id IN( ' . implode( ',', $arrintResponderReferenceIds ) . ' )
									AND s.scheduled_survey_id IS NOT NULL
									AND survey_type_id = ' . CSurveyType::PERSONAL_REVIEW . $strWhereEmployeeAdditionalClause . '
									AND s.response_datetime IS NOT NULL
							) as sub ' . $strWhereClause . $strOrderByClause;

		return self::fetchSurveys( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchNonSubmittedSurveysByResponderReferenceIdBySurveyTypeId( $intResponderReferenceId, $intSurveyTypeId, $objDatabase, $intSurveyTemplateId = NULL, $boolIsScheduleSurveyIdRequired = false, $boolRemoveCacheObject = false ) {
		$strWhereClause = ( true == $boolIsScheduleSurveyIdRequired ) ? ' AND s.scheduled_survey_id IS NOT NULL ' : '';
		$strWhereClause .= ( 0 != ( int ) $intSurveyTemplateId ) ? ' AND s.survey_template_id = ' . ( int ) $intSurveyTemplateId : $strWhereClause;

		$strSql = 'SELECT
						s.*,
						ss.frequency_id
					FROM
						surveys s
						LEFT JOIN scheduled_surveys ss ON ( ss.id = s.scheduled_survey_id )
					WHERE
						s.subject_reference_id =' . ( int ) $intResponderReferenceId . '
						AND s.survey_type_id =' . ( int ) $intSurveyTypeId . '
						AND s.response_datetime IS NULL
						AND s.declined_datetime IS NULL'
					 . $strWhereClause;

		if( true == $boolRemoveCacheObject ) {

			if( true == isset( $_COOKIE['unset_cpa_reminder'] ) ) {
				return;
			}

			$arrobjSurveys = parent::fetchSurveys( $strSql, $objDatabase );
			if( true == valArr( $arrobjSurveys ) ) {
				return $arrobjSurveys;
			} else {
				setcookie( 'unset_cpa_reminder', '1', time() + CSurvey::CPA_LIFETIME );
			}
		} else {
			return parent::fetchSurveys( $strSql, $objDatabase );
		}
	}

	public static function fetchSurveyBySurveyTypeIdBySurveyTemplateIdByTriggerReferenceIdByResponderReferenceId( $intSurveyTypeId, $intSurveyTemplateId, $intTriggerReferenceId, $intResponderReferenceId, $objDatabase ) {
		if( false == valId( $intSurveyTypeId ) || false == valId( $intSurveyTemplateId ) || false == valId( $intTriggerReferenceId ) || false == valId( $intResponderReferenceId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						survey_type_id = ' . ( int ) $intSurveyTypeId . '
						AND survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND trigger_reference_id = ' . ( int ) $intTriggerReferenceId . '
						AND responder_reference_id = ' . ( int ) $intResponderReferenceId . '
					ORDER BY
						id desc
					LIMIT 1';

		return self::fetchSurvey( $strSql, $objDatabase );
	}

	public static function fetchSubmittedSurveysBySurveyTypeIdBySurveyTemplateIdByTriggerReferenceIdsByResponderReferenceId( $intSurveyTypeId, $intSurveyTemplateId, $arrintTriggerReferenceIds, $intResponderReferenceId, $objDatabase, $arrintResponderReferenceId = NULL ) {
		if( false == valId( $intSurveyTypeId ) || false == valId( $intSurveyTemplateId ) || false == valArr( $arrintTriggerReferenceIds ) || false == valId( $intResponderReferenceId ) ) return NULL;

		if( true == valArr( $arrintResponderReferenceId ) ) {
			$strWhere = 'AND responder_reference_id IN ( ' . implode( ',', $arrintResponderReferenceId ) . ' ) ';
		} else {
			$strWhere = 'AND responder_reference_id = ' . ( int ) $intResponderReferenceId;
		}
		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						survey_type_id = ' . ( int ) $intSurveyTypeId . '
						AND survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND trigger_reference_id IN ( ' . implode( ',', $arrintTriggerReferenceIds ) . ' )
						' . $strWhere . '
						AND response_datetime IS NOT NULL
					ORDER BY
						id desc';

		return parent::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchSimpleSubmittedSurveysBySurveyTypeIdBySurveyTemplateIdByTriggerReferenceIds( $intSurveyTypeId, $intSurveyTemplateId, $arrintTriggerReferenceIds, $objDatabase, $boolFromSupportTask = false ) {
		if( false == valId( $intSurveyTypeId ) || false == valId( $intSurveyTemplateId ) || false == valArr( $arrintTriggerReferenceIds ) ) return NULL;

		$strWhere = ' AND response_datetime IS NOT NULL ';

		if( true == $boolFromSupportTask ) {
			$strWhere = '';
		}

		$strSql = 'SELECT
						id,
						trigger_reference_id
					FROM
						surveys
					WHERE
						survey_type_id = ' . ( int ) $intSurveyTypeId . '
						AND survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND trigger_reference_id IN ( ' . implode( ',', $arrintTriggerReferenceIds ) . ' )
						' . $strWhere . '
					ORDER BY
						id desc';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSurveysByTriggerReferenceIdsBySurveyTypeId( $arrintTriggerReferenceIds, $intSurveyTypeId, $objDatabase, $boolSubjectReferenceIdIsNull = false ) {
		if( false == valId( $intSurveyTypeId ) || false == valArr( array_filter( $arrintTriggerReferenceIds ) ) ) return NULL;

		$strWhereCondition = ( true == $boolSubjectReferenceIdIsNull ) ? ' AND subject_reference_id IS NULL' : '';

		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						trigger_reference_id IN (' . implode( ',', $arrintTriggerReferenceIds ) . ')
						AND survey_type_id = ' . ( int ) $intSurveyTypeId . $strWhereCondition;

		return parent::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchNonSubmittedManagerSurveysBySubjectReferenceIds( $arrintSubjectReferenceIds, $objDatabase ) {
		if( false == valArr( $arrintSubjectReferenceIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						subject_reference_id IN (' . implode( ',', $arrintSubjectReferenceIds ) . ')
						AND response_datetime IS NULL
					 	AND declined_datetime IS NULL
						AND survey_type_id = ' . CSurveyType::MANAGER_SURVEY;

		return parent::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchSurveyCountBySurveyTemplateIds( $arrintSurveyTemplateId, $objDatabase ) {
		$strSql = ' SELECT
						count(id),
						survey_template_id
					FROM
						surveys
					WHERE
						survey_template_id IN ( ' . implode( ',', $arrintSurveyTemplateId ) . ' )
						AND response_datetime IS NULL
						AND declined_datetime IS NULL
					GROUP BY
						survey_template_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSurveys( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSurvey', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchLatestNonSubmittedSurveyByPsLeadIdByCompanyUserIdByPersonContactTypeIds( $intPsLeadId, $intCompanyUserId, $arrintPersonContactTypeIds, $objDatabase ) {

		if( false == valArr( $arrintPersonContactTypeIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						surveys s
						JOIN persons p ON ( s.responder_reference_id = p.id AND p.company_user_id = ' . ( int ) $intCompanyUserId . ' and p.ps_lead_id = ' . ( int ) $intPsLeadId . ' )
						JOIN person_contact_preferences pcp ON ( p.id = pcp.person_id )
					WHERE
						survey_type_id <> ' . CSurveyType::SUPPORT_REVIEW . '
						AND response_datetime IS NULL
						AND declined_datetime IS NULL
						AND pcp.person_contact_type_id IN ( ' . implode( ',', $arrintPersonContactTypeIds ) . ')
					ORDER BY
						s.id desc
					LIMIT 1';

		return parent::fetchSurvey( $strSql, $objDatabase );
	}

	public static function fetchEmployeesWithSurveysBySurveyTypeIdsBySurveyTemplateIdsByResponseDatetime( $arrintSurveyTypeIds, $arrintSurveyTemplateIds, $strResponseDatetime, $objDatabase ) {
		if( false == valArr( $arrintSurveyTypeIds ) || false == valArr( $arrintSurveyTemplateIds ) || false == valStr( $strResponseDatetime ) ) return NULL;

		$strSql = 'WITH survey_employees AS (
						SELECT
							s.id AS survey_id,
							s.survey_type_id,
							s.subject_reference_id AS subject_reference_id,
							s.trigger_reference_id AS trigger_reference_id,
							s.responder_reference_id AS responder_reference_id,
							e.id AS employee_id,
							e.email_address AS email_address,
							e.name_full AS name_full
						FROM
							surveys s
							JOIN employees e ON ( e.id = s.subject_reference_id )
						WHERE
							s.survey_type_id IN ( ' . implode( ',', $arrintSurveyTypeIds ) . ')
							AND s.survey_template_id IN ( ' . implode( ',', $arrintSurveyTemplateIds ) . ')
							AND s.response_datetime BETWEEN \'' . $strResponseDatetime . '\' AND NOW()
							AND s.declined_datetime IS NULL )
					SELECT
						se.*,
						e.name_full AS trigger_reference_name_full
					FROM
						employees AS e
						JOIN survey_employees AS se ON ( e.id = se.responder_reference_id )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomSurveysBySubjectReferenceIdBySuveyTemplateIds( $intSubjectReferenceId, $arrintSurveyTemplateIds, $objDatabse ) {
		$strSql = 'SELECT
						s.id,
						s.trigger_reference_id
					FROM
						surveys s
					WHERE
						s.subject_reference_id = ' . ( int ) $intSubjectReferenceId . '
						AND s.survey_template_id IN(' . implode( ',', $arrintSurveyTemplateIds ) . ')';

		return fetchData( $strSql, $objDatabse );
	}

	public static function fetchSubmittedSurveyByTriggerReferenceIdBySurveyTemplateId( $intTriggerReferenceId, $intSurveyTemplateId, $objDatabase ) {
		if( false == valId( $intTriggerReferenceId ) || false == valId( $intSurveyTemplateId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						trigger_reference_id = ' . ( int ) $intTriggerReferenceId . '
						AND survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND response_datetime IS NOT NULL';

		return parent::fetchSurvey( $strSql, $objDatabase );
	}

	public static function fetchSurveysByIds( $arrintSurveyIds, $objDatabase ) {
		if( false == valArr( $arrintSurveyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						id IN ( ' . implode( ',', $arrintSurveyIds ) . ' )';

		return parent::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchSurveyDataById( $intSurveyId, $objDatabase ) {
		$strSql = '	SELECT
						s.id, sqa.answer,
						sqa.answer_modified as edited_answer,
						sqa.numeric_weight as score,
						( p.name_first || \' \' || p.name_last ) as customer_name,
						p.company_name,
						p.id AS person_id,
						p.permissioned_units,
						p.email_address,
						pr.name,
						pr.is_executive,
						prp.total_units
					FROM
						surveys s
						LEFT JOIN survey_question_answers sqa on sqa.survey_id = s.id
						LEFT JOIN persons p ON p.id = s.responder_reference_id
						LEFT JOIN person_roles pr ON pr.id = p.person_role_id
						LEFT JOIN ps_lead_details pld ON pld.ps_lead_id = p.ps_lead_id
						LEFT JOIN
						(
							SELECT
								SUM ( p.number_of_units ) AS total_units,
								p.cid
							FROM
								properties p
								LEFT JOIN clients mc ON ( p.cid = mc.id )
								LEFT JOIN ps_leads pl ON ( mc.ps_lead_id = pl.id )
							WHERE
								p.is_disabled <> 1
								AND pl.id = mc.ps_lead_id
								AND p.termination_date IS NULL
							GROUP BY
								p.cid
						) prp ON prp.cid = pld.cid
					 WHERE
						 s.id = ' . ( int ) $intSurveyId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainingSurveyResponseDetailsBySurveyTemplateId( $intSurveyTemplateId, $boolIsSessionAssociated, $objDatabase, $strYears = NULL ) {
		$strSelect			= '';
		$strJoins			= '';
		$strWhereCondition	= '';

		if( true == valStr( $strYears ) ) {
			$strWhereCondition = ' AND date_part( \'year\', ts.scheduled_start_time::DATE ) IN (' . $strYears . ')';
		}

		if( true == $boolIsSessionAssociated ) {

			$strSelect = ' ,ts.id as training_session_id,
							ts.name AS session_name,
							ts.scheduled_start_time,
							e1.name_full AS trainer,
							t.name AS training,
							t1.name AS training_category';

			$strGroupByFields = ' ,subquery.training_session_id,
							subquery.session_name,
							subquery.scheduled_start_time,
							subquery.trainer,
							subquery.training,
							subquery.training_category';

			$strJoins = ' LEFT JOIN training_sessions ts ON ( s.trigger_reference_id = ts.id )
							LEFT JOIN trainings AS t ON ( t.id = ts.training_id )
							LEFT JOIN training_trainer_associations AS tta ON ( tta.id = ts.training_trainer_association_id )
							LEFT JOIN actions AS a ON ( tta.action_id = a.id )
							LEFT JOIN users AS u ON a.secondary_reference = u.id
							LEFT JOIN employees AS e1 ON u.employee_id = e1.id
							LEFT JOIN trainings AS t1 ON ( t1.id = t.parent_id )';
		}

		$strSql = ' SELECT
						stq.id AS survey_template_question_id,
						stq.survey_question_type_id,
						stq.survey_template_question_group_id,
						stq.survey_template_question_id AS template_question_id,
						stq.question,
						string_agg( sto.option_name, \',\' ) as option_name,
						sqa.answer,
						subquery.*
					FROM
						survey_template_questions stq
						LEFT JOIN survey_question_answers sqa ON ( stq.id = sqa.survey_template_question_id )
						LEFT JOIN survey_template_options sto ON ( sqa.survey_template_option_id = sto.id )
						JOIN
						(
							SELECT
								s.id AS survey_id,
								st.name AS survey_name,
								e.id AS employee_id,
								e.employee_number,
								e.name_full AS employee_name,
								e.email_address,
								d.name AS designation,
								dp.name AS department
								' . $strSelect . '
							FROM
								survey_templates st
								JOIN surveys s ON ( s.survey_template_id = st.id )
								JOIN employees e ON ( s.responder_reference_id = e.id )
								JOIN departments dp ON ( e.department_id = dp.id )
								JOIN designations d ON ( e.designation_id = d.id )
								' . $strJoins . '
							WHERE
								s.declined_datetime IS NULL
								AND st.id = ' . ( int ) $intSurveyTemplateId . '
								' . $strWhereCondition . '
						) AS subquery ON ( subquery.survey_id = sqa.survey_id )
					WHERE
						sqa.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND stq.is_published = 1
					GROUP BY
						stq.id,
						stq.survey_question_type_id,
						stq.survey_template_question_group_id,
						stq.survey_template_question_id,
						stq.question,
						sqa.answer,
						sqa.survey_id,
						subquery.survey_id,
						subquery.survey_name,
						subquery.employee_id,
						subquery.employee_number,
						subquery.employee_name,
						subquery.email_address,
						subquery.designation,
						subquery.department
						' . $strGroupByFields . '
					ORDER BY
						sqa.survey_id,
						stq.survey_template_question_group_id,
						stq.order_num';

		$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			foreach( $arrmixFetchedData as &$arrmixData ) {
				$arrmixData['answer'] = CSurvey::getDecryptedData( $arrmixData['answer'] );
			}
		}

		return $arrmixFetchedData;
	}

	public static function fetchTrainingSurveyNotResponseDetailsBySurveyTemplateId( $intSurveyTemplateId, $boolIsSessionAssociated, $objDatabase, $strYears = NULL, $intDays = NULL, $boolFetchData = true ) {
		$strSelect			= '';
		$strJoins			= '';
		$strWhereCondition	= '';

		if( true == valStr( $strYears ) ) {
			$strWhereCondition = ' AND date_part( \'year\', ts.scheduled_start_time::DATE ) IN (' . $strYears . ')';
		} elseif( true == valId( $intDays ) ) {
			$strWhereCondition = ' AND s.created_on::DATE <= ( NOW()::DATE - INTERVAL \'' . $intDays . ' days\' )';
		}

		if( true == $boolIsSessionAssociated ) {

			$strSelect = ' ,ts.id as training_session_id,
							ts.name AS session_name,
							ts.scheduled_start_time,
							e1.name_full AS trainer,
							t.name AS training,
							t1.name AS training_category';

			$strGroupByFields = ' ,training_session_id,
							session_name,
							scheduled_start_time,
							trainer,
							training,
							training_category';

			$strJoins = ' LEFT JOIN training_sessions ts ON ( s.trigger_reference_id = ts.id )
							LEFT JOIN trainings AS t ON ( t.id = ts.training_id )
							LEFT JOIN training_trainer_associations AS tta ON ( tta.id = ts.training_trainer_association_id )
							LEFT JOIN actions AS a ON ( tta.action_id = a.id )
							LEFT JOIN users AS u ON a.secondary_reference = u.id
							LEFT JOIN employees AS e1 ON u.employee_id = e1.id
							LEFT JOIN trainings AS t1 ON ( t1.id = t.parent_id )';
		}

		$strSql = '	SELECT
						s.id AS survey_id,
						s.*,
						st.name AS survey_name,
						e.id AS employee_id,
						e.employee_number,
						e.name_full AS employee_name,
						e.email_address,
						d.name AS designation,
						dp.name AS department
						' . $strSelect . '
					FROM
						survey_templates st
						JOIN surveys s ON ( s.survey_template_id = st.id )
						JOIN employees e ON ( s.responder_reference_id = e.id )
						JOIN departments dp ON ( e.department_id = dp.id )
						JOIN designations d ON ( e.designation_id = d.id )
						' . $strJoins . '
					WHERE s.response_datetime IS NULL
								AND s.declined_datetime IS NULL
								AND ( now()::DATE - s.survey_datetime::DATE ) >=30
								AND st.id = ' . ( int ) $intSurveyTemplateId . '
								' . $strWhereCondition . '
					GROUP BY
						s.id,
						survey_name,
						e.id,
						e.employee_number,
						employee_name,
						e.email_address,
						designation,
						department
						' . $strGroupByFields . '
					 ORDER BY
						survey_id';

		$arrmixFetchedData = fetchData( $strSql, $objDatabase );
		if( false == $boolFetchData ) {
			return self::fetchSurveys( $strSql, $objDatabase );
		}

		return $arrmixFetchedData;
	}

	public static function fetchNPSByPersonRoleId( $objDatabase, $strDateRange = NULL, $boolIsFilterNonCSMComments = false ) {
		$strResponseDateTime	= ( true == valStr( $strDateRange ) ) ? 'date_trunc( \'' . $strDateRange . '\', NOW() )' : 'date_trunc( \'year\', NOW() )';
		$strJoinCondition		= ( true == $boolIsFilterNonCSMComments ) ? ' JOIN ps_lead_details pld ON ( pld.ps_lead_id = s.ps_lead_id ) ' : '';
		$strWhereCondition		= ( true == $boolIsFilterNonCSMComments ) ? ' AND pld.support_employee_id IS NOT NULL AND pld.support_employee_id <> ' . CEmployee::ID_ASSOCIATE_ACCOUNT_MANAGEMENT : '';

		$strSql = 'SELECT
						CASE
							WHEN
								corporate_count > 0 THEN ROUND( ( ROUND( corporate_promoter::NUMERIC / corporate_count, 2 ) - ROUND( corporate_detractor::NUMERIC / corporate_count, 2 ) ) * 100 )
							ELSE
								0
						END AS corporate_score,
						CASE
							WHEN
								corporate_count > 0 THEN ROUND( ( corporate_promoter::NUMERIC / corporate_count ) * 100 )
							ELSE
								0
						END AS corporate_percent_promoter,
						CASE
							WHEN
								corporate_count > 0 THEN ROUND( ( corporate_detractor::NUMERIC / corporate_count ) * 100 )
							ELSE
								0
						END AS corporate_percent_detractor,
						CASE
							WHEN
								property_count > 0 THEN ROUND( ( ROUND( property_promoter::NUMERIC / property_count, 2 ) - ROUND( property_detractor:: NUMERIC / property_count, 2 ) ) * 100 )
							ELSE
								0
						END AS property_score,
						CASE
							WHEN
								property_count > 0 THEN ROUND( ( property_promoter::NUMERIC / property_count ) * 100 )
							ELSE
								0
						END AS property_percent_promoter,
						CASE
							WHEN
								property_count > 0 THEN ROUND( ( property_detractor::NUMERIC / property_count ) * 100 )
							ELSE
								0
						END AS property_percent_detractor,
						CASE
							WHEN
								overall_count > 0 THEN ROUND( ( ( overall_promoter::NUMERIC / overall_count ) - ( overall_detractor::NUMERIC / overall_count ) ) * 100 )
							ELSE
								0
						END AS overall_score

					FROM
						(
							SELECT
								SUM( CASE WHEN sqa.numeric_weight < 7 AND pr.is_executive = 1 THEN 1 ELSE 0 END ) AS corporate_detractor,
								SUM( CASE WHEN sqa.numeric_weight > 8 AND pr.is_executive = 1 THEN 1 ELSE 0 END ) AS corporate_promoter,
								SUM( CASE WHEN pr.is_executive = 1 THEN 1 ELSE 0 END ) AS corporate_count,
								SUM( CASE WHEN sqa.numeric_weight < 7 AND ( pr.is_executive = 0 OR pr.is_executive IS NULL ) THEN 1 ELSE 0 END ) AS property_detractor,
								SUM( CASE WHEN sqa.numeric_weight > 8 AND ( pr.is_executive = 0 OR pr.is_executive IS NULL ) THEN 1 ELSE 0 END ) AS property_promoter,
								SUM( CASE WHEN ( pr.is_executive = 0 OR pr.is_executive IS NULL ) THEN 1 ELSE 0 END ) AS property_count,
								SUM( CASE WHEN sqa.numeric_weight < 7 THEN 1 ELSE 0 END ) AS overall_detractor,
								SUM( CASE WHEN sqa.numeric_weight > 8 THEN 1 ELSE 0 END ) AS overall_promoter,
 								COUNT( s.id ) AS overall_count
							FROM
								surveys s
								JOIN survey_question_answers sqa ON ( sqa.survey_id = s.id )
								JOIN persons p ON ( p.id = s.responder_reference_id )
								LEFT JOIN person_roles pr ON ( pr.id = p.person_role_id )
								JOIN clients mc ON ( mc.ID = p.cid AND mc.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ', ' . CCompanyStatusType::TERMINATED . ' ) )
								' . $strJoinCondition . '
							WHERE
								s.survey_type_id = ' . CSurveyType::CUSTOMER_NPS . '
								AND sqa.is_hidden != true
								AND sqa.numeric_weight > 0
								AND s.response_datetime >= ' . $strResponseDateTime .
		          $strWhereCondition . '
						) subq;';

		$arrintFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrintFetchedData[0] ) ) ? $arrintFetchedData[0] : 0;
	}

	public static function fetchNPSRatingsForCorporateAndProperty( $objDatabase, $boolIsFromCompanyGoalsTvDisplay = false ) {

		if( false == $boolIsFromCompanyGoalsTvDisplay ) {
			$intInterval = 5;
		} else {
			$intLastSixMonthsInterval	= 6;
			$intInterval				= $intLastSixMonthsInterval + ( date( 'n' ) - 1 );
		}

		$strSql = 'SELECT subq.response_datetime::date,
							subq.corporate_count,
							CASE WHEN subq.corporate_count > 0 THEN ROUND( ( subq.corporate_detractor::NUMERIC / subq.corporate_count::NUMERIC ) * 100 ) ELSE 0 END AS corporate_detractor_percent,
							CASE WHEN subq.corporate_count > 0 THEN ROUND( ( subq.corporate_promoter::NUMERIC / subq.corporate_count::NUMERIC ) * 100 ) ELSE 0 END AS corporate_promoter_percent,
							CASE WHEN subq.corporate_count > 0 THEN ROUND( ( subq.corporate_passive::NUMERIC / subq.corporate_count::NUMERIC ) * 100 ) ELSE 0 END AS corporate_passive_percent,
							subq.property_count,
							CASE WHEN subq.property_count > 0 THEN ROUND( ( subq.property_detractor::NUMERIC / subq.property_count::NUMERIC ) * 100 ) ELSE 0 END AS property_detractor_percent,
							CASE WHEN subq.property_count > 0 THEN ROUND( ( subq.property_promoter::NUMERIC / subq.property_count::NUMERIC ) * 100 ) ELSE 0 END AS property_promoter_percent,
							CASE WHEN subq.property_count > 0 THEN ROUND( ( subq.property_passive::NUMERIC / subq.property_count::NUMERIC ) * 100 ) ELSE 0 END AS property_passive_percent
					FROM (
							SELECT
								MAX( date_trunc(\'month\',s.response_datetime::date ) ) AS response_datetime,
								SUM( CASE WHEN pr.is_executive = 1 THEN 1 ELSE 0 END ) AS corporate_count,
								SUM( CASE WHEN sqa.numeric_weight < 7 AND pr.is_executive = 1 THEN 1 ELSE 0 END ) AS corporate_detractor,
								SUM( CASE WHEN sqa.numeric_weight > 8 AND pr.is_executive = 1 THEN 1 ELSE 0 END ) AS corporate_promoter,
								SUM( CASE WHEN sqa.numeric_weight IN( 7, 8) AND pr.is_executive = 1 THEN 1 ELSE 0 END )AS corporate_passive,
								SUM( CASE WHEN ( pr.is_executive = 0 OR pr.is_executive IS NULL ) THEN 1 ELSE 0 END ) AS property_count,
								SUM( CASE WHEN sqa.numeric_weight < 7 AND ( pr.is_executive = 0 OR pr.is_executive IS NULL ) THEN 1 ELSE 0 END ) AS property_detractor,
								SUM( CASE WHEN sqa.numeric_weight > 8 AND ( pr.is_executive = 0 OR pr.is_executive IS NULL ) THEN 1 ELSE 0 END ) AS property_promoter,
								SUM( CASE WHEN sqa.numeric_weight IN( 7, 8 ) AND ( pr.is_executive = 0 OR pr.is_executive IS NULL ) THEN 1 ELSE 0 END ) AS property_passive
							FROM
								surveys s
								JOIN survey_question_answers sqa ON ( sqa.survey_id = s.id )
								JOIN persons p ON ( p.id = s.responder_reference_id )
								LEFT JOIN person_roles pr ON ( pr.id = p.person_role_id )
								JOIN clients mc ON ( mc.id = p.cid AND mc.company_status_type_id IN ( ' . CCompanyStatusType::CLIENT . ', ' . CCompanyStatusType::TERMINATED . ' ) )
								JOIN ps_lead_details pld ON ( pld.ps_lead_id = s.ps_lead_id )
							WHERE
								s.survey_type_id = ' . CSurveyType::CUSTOMER_NPS . '
								AND sqa.is_hidden != true
								AND sqa.numeric_weight > 0
								AND s.response_datetime >= date_trunc( \'month\', NOW() ) - INTERVAL \'' . $intInterval . ' months\'
								AND pld.support_employee_id IS NOT NULL
								AND pld.support_employee_id <> ' . CEmployee::ID_ASSOCIATE_ACCOUNT_MANAGEMENT . '
							GROUP BY
								date_part( \'year\', response_datetime::date ),
								date_part( \'month\', response_datetime::date )
					) AS subq
					ORDER BY
						subq.response_datetime ASC';

		$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		return $arrmixFetchedData;
	}

	public static function fetchSurveysBySurveyTemplateIdByTriggerReferenceIdsByResponderReferenceIds( $intSurveyTemplateId, $arrintTriggerReferenceIds, $arrintResponderReferenceIds, $objDatabase ) {
		if( false == valId( $intSurveyTemplateId ) || false == valArr( $arrintTriggerReferenceIds ) || false == valArr( $arrintResponderReferenceIds ) ) return NULL;

		$strSql = 'SELECT
						s.id,
						s.responder_reference_id,
						s.trigger_reference_id,
						s.declined_datetime,
						sqa.answer,
						sqa.numeric_weight,
						s.response_datetime
					FROM
						surveys s
						LEFT JOIN survey_question_answers sqa On(sqa.survey_id = s.id)
					WHERE
						s.responder_reference_id IN( ' . implode( ',', $arrintResponderReferenceIds ) . ' )
						AND s.trigger_reference_id IN( ' . implode( ',', $arrintTriggerReferenceIds ) . ' )
						AND s.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND s.declined_datetime IS NULL
					ORDER BY
						sqa.survey_template_question_id';

		$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			foreach( $arrmixFetchedData as &$arrmixData ) {
				$arrmixData['answer'] = CSurvey::getDecryptedData( $arrmixData['answer'] );
			}
		}

		return $arrmixFetchedData;
	}

	public static function fetchPaginatedTasksCountForBurnDownChart( $objDatabase, $objTaskFilter = NULL ) {
		$strCaseCondition			= '';
		$strDistinctOnClause 		= '';
		$strAdditionalConditions	= '';

		$strFromClause = '	FROM tasks t
							LEFT JOIN task_details td ON ( t.id = td.task_id)
							LEFT JOIN task_releases tr on ( t.task_release_id = tr.id )';

		$strCaseCondition = ' WHERE t.task_type_id IN ( ' . CTaskType::FEATURE . ', ' . CTaskType::BUG . ') ';

		if( true == valObj( $objTaskFilter, 'CTaskFilter' ) ) {

			if( true == valStr( $objTaskFilter->getTaskStatuses() ) ) {
				$strAdditionalConditions .= ' AND t.task_status_id IN ( ' . trim( $objTaskFilter->getTaskStatuses() ) . ' ) ';
			}

			if( true == valStr( $objTaskFilter->getProjectManagers() ) ) {
				$strAdditionalConditions .= ' AND t.project_manager_id IN ( ' . $objTaskFilter->getProjectManagers() . ' ) ';
			}

			if( true == valStr( $objTaskFilter->getPsProducts() ) && false == valStr( $objTaskFilter->getPsProductOptions() ) ) {
				$strAdditionalConditions .= ' AND t.ps_product_id IN ( ' . $objTaskFilter->getPsProducts() . ' ) AND t.ps_product_option_id IS NULL ';

			} elseif( true == valStr( $objTaskFilter->getPsProducts() ) && true == valStr( $objTaskFilter->getPsProductOptions() ) ) {
				$strAdditionalConditions .= ' AND ( t.ps_product_id IN ( ' . $objTaskFilter->getPsProducts() . ' ) OR t.ps_product_option_id IN ( ' . $objTaskFilter->getPsProductOptions() . ' ) ) ';
			} elseif( false == valStr( $objTaskFilter->getPsProducts() ) && true == valStr( $objTaskFilter->getPsProductOptions() ) ) {
				$strAdditionalConditions .= ' AND t.ps_product_option_id IN ( ' . $objTaskFilter->getPsProductOptions() . ' ) ';
			}

			if( true == valStr( $objTaskFilter->getStartDate() ) && true == valStr( $objTaskFilter->getEndDate() ) ) {
				$strAdditionalConditions .= 'AND ( ( t.due_date >= \'' . date( 'm/d/Y', strtotime( '-6 day', strtotime( $objTaskFilter->getStartDate() ) ) ) . '\' AND t.due_date <= \'' . date( 'm/d/Y', strtotime( $objTaskFilter->getEndDate() ) ) . '\' ) OR
											( tr.release_datetime BETWEEN \'' . $objTaskFilter->getStartDate() . '\' AND \'' . $objTaskFilter->getEndDate() . '\' ) )';
			}

		}

		$strSql = 'SELECT count(id)
					FROM (
							SELECT t.id '
		          . $strFromClause
		          . $strCaseCondition . $strAdditionalConditions;

		$strSql .= ' GROUP BY t.id ) as sub_query1';

		$arrintResponse = ( array ) fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;

	}

	public static function fetchAllPaginatedNpsSurveyTasksByTaskStatus( $arrstrCommentsFilter, $strSelectedTab = NULL, $objDatabase ) {
		$intOffset	= ( 0 < $arrstrCommentsFilter['page_no'] ) ? $arrstrCommentsFilter['page_size'] * ( $arrstrCommentsFilter['page_no'] - 1 ) : 0;
		$intLimit	= ( int ) $arrstrCommentsFilter['page_size'];

		$strSubSql = '';
		$strWhereCondition = '';

		if( $strSelectedTab == 'not_completed' ) {
			$strWhereCondition .= ' AND tss.id NOT IN (' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ' )';
		} else {
			$strWhereCondition .= ' AND tss.id IN (' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ' )';
		}

		$strSql = 'SELECT s.id,
						st.id AS survey_task_id,
						st.task_id AS task_id_against_survey,
						t.title AS task_title,
						t.created_on,
						tss.id as task_staus_id,
						tss.name AS status,
						t.user_id,
						e.name_full,
						e.preferred_name,
						d.name AS designation
					FROM surveys AS s
						JOIN survey_tasks AS st ON (st.survey_id = s.id)
						JOIN tasks AS t ON (t.id = st.task_id)
						JOIN users AS u on (u.id = t.user_id)
						JOIN employees AS e ON (e.id = u.employee_id)
						JOIN task_statuses AS tss ON (tss.id = t.task_status_id)
						JOIN designations AS d ON (d.id = e.designation_id)
					WHERE s.survey_type_id = ' . CSurveyType::CUSTOMER_NPS . '
						AND s.response_datetime > (now() - \'30 days\'::interval)::timestamp ' . $strWhereCondition . '
					Order by ' . $arrstrCommentsFilter['sorting_field'] . ' ' . $arrstrCommentsFilter['sorting_order'];

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strSql .= ' OFFSET	' . ( int ) $intOffset . ' LIMIT	' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNpsSurveyTasksCount( $objDatabase ) {
		$strWhereCondition = '';

		$strSql = 'SELECT
						CASE WHEN tss.id NOT IN (' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ' ) THEN \'Not Completed\' else \'Completed\' END AS status, COUNT( s.id )
					FROM surveys AS s
						JOIN survey_tasks AS st ON (st.survey_id = s.id)
						JOIN tasks AS t ON (t.id = st.task_id)
						JOIN task_statuses AS tss ON (tss.id = t.task_status_id)
					WHERE s.survey_type_id = ' . CSurveyType::CUSTOMER_NPS . '
						AND s.response_datetime > (now() - \'30 days\'::interval)::timestamp
						AND tss.id NOT IN (' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ' )' . $strWhereCondition . '
					GROUP BY status
					ORDER BY status DESC';

		$arrmixData = fetchData( $strSql, $objDatabase );
		return $arrmixData;
	}

	public static function fetchNpsSurveyTasksCountByTaskStatus( $strSelectedTab = NULL, $objDatabase ) {
		$strWhereCondition = '';

		if( $strSelectedTab == 'not_completed' ) {
			$strWhereCondition .= ' AND tss.id NOT IN (' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ' )';
		} else {
			$strWhereCondition .= ' AND tss.id IN (' . CTaskStatus::COMPLETED . ', ' . CTaskStatus::CANCELLED . ' )';
		}

		$strSql = 'SELECT COUNT(*)
					FROM surveys AS s
						JOIN survey_tasks AS st ON (st.survey_id = s.id)
						JOIN tasks AS t ON (t.id = st.task_id)
						JOIN task_statuses AS tss ON (tss.id = t.task_status_id)
					WHERE s.survey_type_id = ' . ( int ) CSurveyType::CUSTOMER_NPS . '
						AND s.response_datetime > (now() - \'30 days\'::interval)::timestamp ' . $strWhereCondition;

		return parent::fetchColumn( $strSql, 'count', $objDatabase );

	}

	public static function fetchUncommentedSurveys( $objDatabase ) {

		$strSql = 'SELECT
						p.name_first,
						p.name_last,
						p.email_address,
						p.company_user_id,
						sqa.numeric_weight,
						sqa.answer,
						p.cid,
						c.database_id
					FROM
						survey_question_answers AS sqa
						LEFT JOIN surveys AS s ON ( sqa.survey_id = s.id )
						LEFT JOIN persons AS p ON ( s.responder_reference_id = p.id )
						LEFT JOIN clients AS c ON ( c.id = p.cid )
					WHERE
						s.survey_type_id = ' . CSurveyType::CUSTOMER_NPS . '
						AND s.response_datetime > ( now() - \'1 hour\'::interval )::timestamp
						AND p.email_address IS NOT NULL
						AND p.deleted_on is NULL';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchLastSurveyBySubjectReferenceIdsByResponderReferenceIds( $arrintSubjectReferenceIds, $intSurveyTypeId, $objDatabase ) {
		if( false == valArr( $arrintSubjectReferenceIds ) ) {
			return NULL;
		}

		$strWhereClause = ( true == is_numeric( $intSurveyTypeId ) ) ? ' AND survey_type_id = ' . $intSurveyTypeId : '';

		$strSql = 'SELECT
						*
					FROM
						(
						SELECT
							id,
							subject_reference_id,
							responder_reference_id,
							created_on,
							MAX ( id ) OVER ( PARTITION BY subject_reference_id ) AS latest_survey_id
						FROM
							surveys
						WHERE
							declined_datetime IS NULL ' . $strWhereClause . '
							AND subject_reference_id IN ( ' . implode( ',', $arrintSubjectReferenceIds ) . ' )
						) AS sub
					WHERE
						id = latest_survey_id';

		return parent::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchNonSubmittedResponseCountByLatestSurveyIdBySubjectReferenceIds( $intManagerEmployeeId, $objDatabase ) {
		if( true == is_null( $intManagerEmployeeId ) ) return NULL;

		$strSql = 'SELECT
							COUNT( 1 ) AS count
						FROM
							(
								SELECT
									*,
									MAX ( id ) OVER ( PARTITION BY s.subject_reference_id ) AS latest_survey_id
								FROM
									surveys s
								WHERE
									s.subject_reference_id IN (
																SELECT
																	id
																FROM
																	employees e
																WHERE
																	e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
																	AND e.date_terminated IS NULL
																	AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
									)
									AND s.survey_type_id = ' . CSurveyType::MANAGER_SURVEY . '
									AND declined_datetime IS NULL
							) AS sub
						WHERE
							id = latest_survey_id
							AND responder_reference_id IS NULL';

		$arrintData = ( array ) fetchData( $strSql, $objDatabase );

		return $arrintData[0]['count'];
	}

	public static function fetchNonSubmittedNonDeclinedSurveysByTriggerReferenceIdsBySurveyTemplateId( $arrintTriggerReferenceIds, $intSurveyTemplateId, $objDatabase ) {
		if( false == valArr( $arrintTriggerReferenceIds ) || false == valId( $intSurveyTemplateId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						response_datetime IS NULL
						AND declined_datetime IS NULL
						AND survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND trigger_reference_id IN ( ' . implode( ',', $arrintTriggerReferenceIds ) . ' )
					ORDER BY id';

		return parent::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchNonSubmittedDeclinedSurveysHavingCommonEmployeeForSubjectReferenceIdAndResponderReferenceIdAndUserIdBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		if( false == valId( $intSurveyTemplateId ) ) return NULL;

		$strSql = 'SELECT
						s.*
					FROM
						surveys s
						JOIN users u ON ( u.id = s.updated_by )
						LEFT JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
					WHERE
						s.declined_datetime IS NOT NULL
						AND s.response_datetime IS NULL
						AND s.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND s.responder_reference_id = s.subject_reference_id
						AND s.subject_reference_id = u.employee_id
						AND sqa.survey_id IS NULL
					ORDER BY s.id';

		return parent::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchSurveyTemplateIdFromSessionId( $intTrainingSessionId, $objDatabase ) {
		if( true == is_null( $intTrainingSessionId ) ) return NULL;

		$strSql = 'SELECT
						survey_template_id
					FROM
						surveys
					WHERE
						survey_type_id = ' . CSurveyType::TRAINING . '
						AND trigger_reference_id = ' . ( int ) $intTrainingSessionId . '
						AND survey_template_id <> ' . CSurveyTemplate::JUSTIFICATION_FOR_NON_ATTENDENCE . '
						AND subject_reference_id IS NOT NULL
					GROUP BY
						survey_template_id';

		$arrintData = ( array ) fetchData( $strSql, $objDatabase );

		return $arrintData[0]['survey_template_id'];
	}

	public static function fetchSurveyFromSessionIdAndUserId( $intTrainingSessionId, $arrintResponderReferenceIds, $objDatabase ) {
		if( true == is_null( $intTrainingSessionId ) || false == valArr( $arrintResponderReferenceIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						trigger_reference_id = ' . ( int ) $intTrainingSessionId . '
						AND subject_reference_id IS NOT NULL
						AND survey_type_id = ' . CSurveyType::TRAINING . '
						AND responder_reference_id IN( ' . implode( ',', $arrintResponderReferenceIds ) . ' )';

		return parent::fetchSurveys( $strSql, $objDatabase );

	}

	public static function fetchSubmittedSurveyByIdBySurveyTypeId( $intId, $intSurveyTypeId, $objDatabase ) {
		if( true == is_null( $intId ) || true == is_null( $intSurveyTypeId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						id = ' . ( int ) $intId . '
						AND survey_type_id = ' . ( int ) $intSurveyTypeId . '
						AND response_datetime is NOT NULL';

		return parent::fetchSurvey( $strSql, $objDatabase );
	}

	public static function fetchSurveyByIds( $objDatabase, $arrintIds, $intSubjectReferenceId = NULL ) {
		if( false == valArr( array_filter( $arrintIds ) ) && false == valId( $intSubjectReferenceId ) ) return NULL;

		$strWhereSql	= NULL;
		$strWhereClause = NULL;

		if( true == valArr( array_filter( $arrintIds ) ) ) {
			$strWhereSql	 = ' AND id IN ( ' . implode( ',', array_filter( $arrintIds ) ) . ' )';
			$strWhereClause = ',' . CSurveyTemplate::EXIT_INTERVIEW;
		}

		if( true == valId( $intSubjectReferenceId ) ) {
			$strWhereSql 	= ' And declined_datetime IS NULL AND subject_reference_id = ' . ( int ) $intSubjectReferenceId;
		}

		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						survey_template_id IN(' . implode( ', ', CSurveyTemplate::$c_arrintNoDuesSurveyTemplates ) . $strWhereClause . ')'
					. $strWhereSql;

		return parent::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchSurveysBySubjectReferenceIdBySurveyTemplateId( $intSurveyTemplateId, $intSubjectReferenceId, $objDatabase ) {
		if( false == valId( $intSubjectReferenceId ) && false == valId( $intSurveyTemplateId ) ) return NULL;

		$strSql = ' SELECT
						id,
						responder_reference_id
					FROM
						surveys
					WHERE
						survey_template_id = ' . ( int ) $intSurveyTemplateId . ' AND
						subject_reference_id = ' . ( int ) $intSubjectReferenceId . ' AND
						declined_datetime IS NULL';

		$arrintResponse = ( array ) fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0] ) ) return $arrintResponse[0];
	}

	public static function fetchManagerSurveyByResponderReferenceIdsByDate( $arrintResponderReferenceIds, $strDate, $objDatabase ) {
		if( false == valArr( $arrintResponderReferenceIds ) || false == valStr( $strDate ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						s1.*
					FROM
						surveys s
						LEFT JOIN surveys s1 ON ( s.id = s1.trigger_reference_id )
					WHERE
						s.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_IN . '
						AND s.response_datetime IS NOT NULL
						AND s1.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_MANAGER_SURVEY_IN . '
						AND s1.survey_type_id = ' . CSurveyType::MANAGER_SURVEY . '
						AND s1.response_datetime IS NULL
						AND s.responder_reference_id IN ( ' . sqlIntImplode( $arrintResponderReferenceIds ) . ' )
						AND s.survey_datetime::DATE >= \'' . $strDate . '\'::DATE
						AND s1.survey_datetime::DATE >= \'' . $strDate . '\'::DATE
						AND s1.declined_datetime IS NULL';

		return parent::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchSubmittedSurveysBySessionIdBySurveyTemplateId( $intSessionId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						subject_reference_id IS NOT NULL
						AND survey_type_id = ' . CSurveyType::TRAINING . '
						AND trigger_reference_id = ' . ( int ) $intSessionId;

		return parent::fetchSurveys( $strSql, $objDatabase );
	}

	public static function fetchSurveyResponsesOfSurveysConductedBeforeOneMonth( $arrintSessionIds, $objAdminDatabase ) {
		if( false == valArr( $arrintSessionIds ) ) return NULL;

		$strSql = 'SELECT
						ts.id,
						ts.name,
						s.survey_template_id,
						s.trigger_reference_id,
						stq.question,
						stq.id AS survey_template_question_id,
						sqa.answer,
						sqa.numeric_weight,
						s.responder_reference_id,
						e.name_full,
						sto.option_name,
						s.subject_reference_id,
						e.email_address,
						ts.actual_start_time
					FROM
						training_sessions ts
						JOIN surveys s ON ( ts.id = s.trigger_reference_id )
						JOIN employees e ON( e.id = s.subject_reference_id )
						JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						JOIN survey_template_questions stq ON ( sqa.survey_template_question_id = stq.id )
						LEFT JOIN survey_template_options sto ON (sto.id = sqa.survey_template_option_id)
					WHERE
						ts.id IN (' . implode( ',', $arrintSessionIds ) . ')
						AND stq.survey_question_type_id <> ' . CSurveyQuestionType::MATRIX . '
						AND ts.actual_start_time::DATE = NOW()::date - INTERVAL \'30 days\'
						AND s.survey_template_id <> ' . CSurveyTemplate::JUSTIFICATION_FOR_NON_ATTENDENCE . '
						AND s.declined_datetime IS NULL
						AND stq.is_published = 1
						AND s.survey_datetime::DATE >= NOW()::date - INTERVAL \'30 days\'
						AND s.subject_reference_id IS NOT NULL
					ORDER BY
						s.responder_reference_id,
						stq.order_num';

		return fetchData( $strSql, $objAdminDatabase );

	}

	public static function fetchLatestSurveysBySubjectReferenceIdsByLatestMonth( $intSubjectReferenceId, $objDatabase ) {
		$strSql = 'SELECT
						s.subject_reference_id,
						s.id AS employee_survey_id,
						s1.id AS manager_survey_id,
						s.survey_datetime,
						s1.survey_datetime
					FROM
						surveys s
						JOIN surveys s1 ON( s.id = s1.trigger_reference_id )
					WHERE
						s.subject_reference_id = ' . ( int ) $intSubjectReferenceId . '
						AND s.subject_reference_id = s.responder_reference_id
						AND s.response_datetime IS NOT NULL
						AND s.id IN( SELECT max( id ) FROM surveys WHERE subject_reference_id = ' . ( int ) $intSubjectReferenceId . ' AND subject_reference_id = responder_reference_id AND response_datetime IS NOT NULL )
						AND s.survey_type_id = ' . CSurveyType::PERSONAL_REVIEW . '
						AND s1.survey_type_id = ' . CSurveyType::MANAGER_SURVEY . '
						AND s.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_IN . '
						AND s1.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_MANAGER_SURVEY_IN;

		$arrmixData = ( array ) fetchData( $strSql, $objDatabase );

		return $arrmixData[0];
	}

	public static function fetchSurveysByTriggerReferenceIdByResponderReferenceIds( $intTrainingSessionId, $arrintResponderIds, $objAdminDatabase ) {
		if( false == is_numeric( $intTrainingSessionId ) || false == valArr( $arrintResponderIds ) ) return NULL;

		$strSql	= 'SELECT
						s.id,
						s.*
					FROM
						surveys s
					WHERE
						s.trigger_reference_id = ' . ( int ) $intTrainingSessionId .
		             'AND s.responder_reference_id IN (' . implode( ',', $arrintResponderIds ) . ')';

		return parent::fetchSurveys( $strSql, $objAdminDatabase );
	}

	public static function fetchAdminHelpdeskSurveyByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						main.employee_id,
						main.name,
						main.answer,
						main.rating,
						main.rated_by,
						main.task_id,
						main.task_title,
						main.created_on,
						main.task_created_on,
						main.task_completed_on,
						CASE
						WHEN
							main.task_completed_on IS NOT NULL
						THEN
							age(main.task_completed_on::TIMESTAMP, main.task_created_on::TIMESTAMP)
						ELSE
							age(CURRENT_TIMESTAMP, main.task_created_on::TIMESTAMP)
						END AS task_time_interval,
						justify_interval( COALESCE ( SUM ( main.exclude_time ) ) ) AS exclude_time
					FROM (
							SELECT DISTINCT
								e.id AS employee_id,
								e1.name_full as name,
								sqa.answer as answer,
								sqa.numeric_weight as rating,
								e.preferred_name as rated_by,
								s.trigger_reference_id as task_id,
								t.title as task_title,
								s.created_on,
								t.created_on as task_created_on,
								t.completed_on as task_completed_on,
								CASE
									WHEN
										ta.task_status_id IN (' . CTaskStatus::ON_HOLD . ', ' . CTaskStatus::AWAITING_RESPONSE . ')
									THEN
										age(lead(ta.created_on::TIMESTAMP) OVER(PARTITION BY ta.task_id ORDER BY ta.created_on ASC), ta.created_on::TIMESTAMP)
								END AS exclude_time
							FROM surveys s
								LEFT JOIN survey_question_answers sqa ON ( sqa.survey_id = s.id )
								LEFT JOIN employees e ON ( e.id = s.responder_reference_id )
								LEFT JOIN employees e1 ON ( e1.id = s.subject_reference_id )
								LEFT JOIN tasks t ON ( t.id = s.trigger_reference_id )
								LEFT JOIN task_assignments ta ON (ta.task_id = t.id)
							WHERE s.survey_template_id = ' . CSurveyTemplate::HELPDESK_SURVEY . ' AND
								s.survey_type_id = ' . CSurveyType::TRAINING . ' AND
								s.subject_reference_id IN (' . $intEmployeeId . ') ORDER BY s.created_on DESC
				) main
					GROUP BY main.employee_id,
							 main.name,
							 main.answer,
							 main.rating,
							 main.rated_by,
							 main.task_id,
							 main.task_title,
							 main.created_on,
							 main.task_created_on,
							 main.task_completed_on';

		$arrmixFetchedData = ( array ) fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			foreach( $arrmixFetchedData as &$arrmixData ) {
				$arrmixData['answer'] = CSurvey::getDecryptedData( $arrmixData['answer'] );
			}
		}

		return $arrmixFetchedData;
	}

	public static function fetchNoDuesSurveyBySurveyTemplateId( $intSurveyTemplateId, $objAdminDatabase, $strKey = NULL, $boolUnapprovedNoDuesSurvey = false, $boolSuperUser = false ) {

		if( false == is_numeric( $intSurveyTemplateId ) ) return false;

		if( true == $boolUnapprovedNoDuesSurvey && false == $boolSuperUser ) {
			$strJoinCondition = ' JOIN employee_assessments ea ON ( ea.employee_id = e.id AND ea.employee_assessment_type_id = ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ' AND ea.completed_on IS NULL )
								  LEFT JOIN employee_preferences ep ON( ep.employee_id = s.subject_reference_id AND ep.key = \'' . $strKey . '\' )';
			$strWhereCondition = ' AND survey_template_id = ' . ( int ) $intSurveyTemplateId . ' AND s.response_datetime IS NOT NULL AND ep.key IS NULL;';
		} elseif( false == is_null( $strKey ) && false == $boolSuperUser ) {
			$strJoinCondition   = ' JOIN employee_preferences ep ON( ep.employee_id = s.subject_reference_id AND ep.key = \'' . $strKey . '\' )';
			$strWhereCondition  = ' AND survey_template_id = ' . ( int ) $intSurveyTemplateId . ' AND responder_reference_id = ' . ( int ) CEmployee::ID_IT_HELP_DESK . ' AND s.response_datetime IS NULL;';
		} elseif( true == $boolSuperUser && true == $boolUnapprovedNoDuesSurvey ) {
			$strJoinCondition  = ' JOIN employee_assessments ea ON ( ea.employee_id = e.id AND ea.employee_assessment_type_id = ' . CEmployeeAssessmentType::EXIT_INTERVIEW . ' AND ea.completed_on IS NULL )
								       LEFT JOIN employee_preferences ep ON( ep.employee_id = s.subject_reference_id AND ep.key IN( \'' . CEmployeePreference::NO_DUES_APPROVED_BY_DESKTOP_IT . '\', \'' . CEmployeePreference::NO_DUES_APPROVED_BY_LIVE_IT . '\' ) )';
			$strWhereCondition = ' AND ( survey_template_id = ' . CSurveyTemplate::NO_DUES_SYSTEM_ADMINISTRATION . '  AND s.subject_reference_id NOT IN (
																																								SELECT
																																									ep1.employee_id
																																								FROM
																																									employee_preferences ep1
																																								WHERE ep1.key = \'' . CEmployeePreference::NO_DUES_APPROVED_BY_DESKTOP_IT . '\' )
																														OR ( survey_template_id = ' . CSurveyTemplate::NO_DUES_IT . '
																															AND s.subject_reference_id NOT IN (
																																								SELECT
																																									ep1.employee_id
																																								FROM
																																									employee_preferences ep1
																																								WHERE
																																									ep1.key = \'' . CEmployeePreference::NO_DUES_APPROVED_BY_LIVE_IT . '\' ) ) )
																														AND s.response_datetime IS NOT NULL;';
		} elseif( true == $boolSuperUser && false == $boolUnapprovedNoDuesSurvey ) {
			$strJoinCondition  = ' LEFT JOIN employee_preferences ep on( ep.employee_id = s.subject_reference_id AND ep.key = \'' . CEmployeePreference::NO_DUES_APPROVED_BY_DESKTOP_IT . '\' )';
			$strWhereCondition = ' AND ( ( survey_template_id = ' . CSurveyTemplate::NO_DUES_IT . ' AND ep.key is NOT NULL ) OR ( survey_template_id = ' . CSurveyTemplate::NO_DUES_SYSTEM_ADMINISTRATION . ' ) ) AND s.response_datetime IS NULL AND responder_reference_id = ' . ( int ) CEmployee::ID_IT_HELP_DESK . '';
		} else {
			$strWhereCondition = ' AND survey_template_id = ' . ( int ) $intSurveyTemplateId . ' AND responder_reference_id = ' . ( int ) CEmployee::ID_IT_HELP_DESK . ';';
		}

		$strSql = 'SELECT
						distinct on(s.id)
						s.*,
						st.name as survey_name,
						e.preferred_name as subject_name
					FROM surveys s
						JOIN survey_templates st ON( s.survey_template_id = st.id )
						JOIN employees e ON ( s.subject_reference_id = e.id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' ) ' . $strJoinCondition . '
					WHERE
						s.declined_datetime IS NULL'
		          . $strWhereCondition;

		return parent::fetchSurveys( $strSql, $objAdminDatabase );
	}

	public static function fetchSurveyBySurveyTemplateIdBySubjectReferenceId( $intSurveyTemplateId, $intSubjectReferenceId, $objAdminDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						surveys
					WHERE survey_template_id = ' . ( int ) $intSurveyTemplateId . ' AND
						subject_reference_id = ' . ( int ) $intSubjectReferenceId . ' AND
						declined_datetime IS NULL';

		return parent::fetchSurvey( $strSql, $objAdminDatabase );
	}

	public static function fetchSurveysBySurveyIdsBySurveyTypeId( $arrintSurveyIds, $intSurveyTypeId, $arrstrQuestionNames, $arrintSurveyTemplateIds, $objAdminDatabase ) {

		if( false == valArr( $arrintSurveyIds ) || false == is_numeric( $intSurveyTypeId ) || false == valArr( $arrstrQuestionNames ) ) {
			return false;
		}

		$arrintSurveyTemplateIds[] = CSurveyTemplate::INTERVIEW_FEEDBACK_SURVEY;

		$strSql = ' SELECT s.id,
						eai.employee_application_id,
						eai.ps_job_posting_step_id,
						sqa.numeric_weight,
						stq.survey_question_type_id,
						stq.question,
						pjps.order_num
					FROM surveys s
						JOIN employee_application_interviews eai ON( s.trigger_reference_id = eai.id )
						JOIN survey_question_answers sqa ON(sqa.survey_id = s.id )
						JOIN survey_template_questions stq ON (stq.id = sqa.survey_template_question_id)
						JOIN ps_job_posting_steps pjps on ( pjps.id = eai.ps_job_posting_step_id )
					WHERE s.id IN ( ' . implode( ',', $arrintSurveyIds ) . ' )
						AND survey_type_id = ' . ( int ) $intSurveyTypeId . '
						AND response_datetime is NOT NULL
						AND s.subject_reference_id IS NULL
						AND ( stq.question ILIKE \'' . implode( '\' OR stq.question ILIKE \'', $arrstrQuestionNames ) . '\')
						AND s.survey_template_id IN ( ' . implode( ', ', $arrintSurveyTemplateIds ) . ' )
					ORDER BY stq.question';

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchCustomSupportTaskQaSurveyStatistics( $objSurveysFilter = NULL, $objAdminDatabase, $boolIsProcessEmployeeAverage = false ) {
		$strWhereSql		= NULL;
		$strOffsetClause	= NULL;

		if( true == valStr( $objSurveysFilter->getStartDate() ) && true == valStr( $objSurveysFilter->getEndDate() ) ) {
			$strWhereSql .= ' AND s.response_datetime BETWEEN \'' . date( 'Y-m-d', strtotime( $objSurveysFilter->getStartDate() ) ) . ' 00:00:00\' AND \'' . date( 'Y-m-d', strtotime( $objSurveysFilter->getEndDate() ) ) . ' 23:59:59\'';
		}

		if( true == valStr( $objSurveysFilter->getSurveyIds() ) ) {
			$strWhereSql .= ' AND s.id IN ( ' . $objSurveysFilter->getSurveyIds() . ' )';
		} elseif( true == valId( $objSurveysFilter->getSurveyId() ) ) {
			$strWhereSql .= ' AND s.id = ' . $objSurveysFilter->getSurveyId();
		}

		if( true == valId( $objSurveysFilter->getEmployeeId() ) ) {
			$strWhereSql .= ' AND s.subject_reference_id = ' . $objSurveysFilter->getEmployeeIds();
		}

		if( true == valId( $objSurveysFilter->getTeamId() ) ) {
			$strWhereSql .= ' AND te.team_id = ' . $objSurveysFilter->getTeamId();
		}

		if( true == valStr( $objSurveysFilter->getEmployeeIds() ) ) {
			$strWhereSql .= ' AND s.subject_reference_id IN ( ' . $objSurveysFilter->getEmployeeIds() . ' )';
		}

		if( true == valStr( $objSurveysFilter->getEmployeeGraderIds() ) ) {
			$strWhereSql .= ' AND s.responder_reference_id IN ( ' . $objSurveysFilter->getEmployeeGraderIds() . ' )';
		}

		if( true == valStr( $objSurveysFilter->getTeamIds() ) ) {
			$strWhereSql .= ' AND te.team_id IN ( ' . $objSurveysFilter->getTeamIds() . ' )';
		}

		if( true == valStr( $objSurveysFilter->getDesignationIds() ) ) {
			$strWhereSql .= ' AND e.designation_id IN ( ' . $objSurveysFilter->getDesignationIds() . ' )';
		}

		if( true == valStr( $objSurveysFilter->getEmployeeHireStartDate() ) && true == valStr( $objSurveysFilter->getEmployeeHireEndDate() ) ) {
			$strWhereSql .= ' AND e.date_started BETWEEN \'' . date( 'Y-m-d', strtotime( $objSurveysFilter->getEmployeeHireStartDate() ) ) . ' 00:00:00\' AND \'' . date( 'Y-m-d', strtotime( $objSurveysFilter->getEmployeeHireEndDate() ) ) . ' 23:59:59\'';
		}

		if( true == valId( $objSurveysFilter->getEmployeeStatusTypeId() ) ) {
			$strWhereSql .= 'AND e.employee_status_type_id = ' . $objSurveysFilter->getEmployeeStatusTypeId();
		} else {
			$strWhereSql .= 'AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		}
		$strOrderBy			= 's.survey_datetime DESC';
		$strInnerOrderBy	= 'innerquery.response_datetime DESC';

		if( true == valStr( $objSurveysFilter->getOrderBy() ) ) {
			$strOrderBy = $objSurveysFilter->getOrderBy();
		}

		if( true == $boolIsProcessEmployeeAverage ) {
			$strSqlWith = 'WITH survey_tasks AS (
							SELECT
									s.id,
									s.trigger_reference_id,
									s.responder_reference_id,
									s.subject_reference_id,
									s.survey_datetime,
									s.response_datetime,
									s.survey_template_id,
									te.team_id,
									e.reporting_manager_id AS manager_employee_id,
									e.preferred_name
							FROM
								surveys s
								JOIN (
										SELECT
											e.id as employee_id,
											e.preferred_name,
											e.date_started as start_date,
											e.date_started + INTERVAL \' 90 days\' as end_date
										FROM
											employees e
										WHERE
											e.id IN ( ' . $objSurveysFilter->getEmployeeIds() . ' )
											AND e.date_terminated IS NULL
											AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
									) AS e ON ( e.employee_id = s.subject_reference_id AND s.survey_datetime > e.start_date AND s.survey_datetime < e.end_date )
								LEFT JOIN team_employees te ON ( te.employee_id = e.employee_id AND te.is_primary_team = 1 )
							WHERE
								s.survey_type_id = ' . CSurveyType::SUPPORT_QA_TASKS . '
								AND s.survey_template_id IN ( ' . CSurveyTemplate::NEW_SUPPORT_TASK_QA_SURVEY . ', ' . CSurveyTemplate::NEW_SUPPORT_QA_TASK_STANDARD_SURVEY . ' )
								AND s.response_datetime IS NOT NULL
								AND s.declined_datetime IS NULL
						)';
		} else {
			$strSqlWith = 'WITH survey_tasks AS (
							SELECT
								s.id,
								s.trigger_reference_id,
								s.responder_reference_id,
								s.subject_reference_id,
								s.survey_datetime,
								s.response_datetime,
								s.survey_template_id,
								te.team_id,
								e.reporting_manager_id AS manager_employee_id,
								e.preferred_name 
							FROM
								surveys s
								LEFT JOIN employees e ON ( e.id = s.subject_reference_id )
								LEFT JOIN team_employees te ON ( te.employee_id = e.id AND te.is_primary_team = 1 )
							WHERE
								s.survey_type_id = ' . CSurveyType::SUPPORT_QA_TASKS . '
								AND s.survey_template_id IN ( ' . CSurveyTemplate::NEW_SUPPORT_TASK_QA_SURVEY . ', ' . CSurveyTemplate::NEW_SUPPORT_QA_TASK_STANDARD_SURVEY . ' )
								AND s.response_datetime IS NOT NULL
								AND s.declined_datetime IS NULL
								AND e.date_terminated IS NULL
								' . $strWhereSql . '
							ORDER BY
								' . $strOrderBy . '
								' . $strOffsetClause . '
						)';
		}

		$strSql = $strSqlWith . '
					SELECT
						survey_id,
						task_id,
						employee_id,
						employee_name,
						team_id,
						manager_employee_id,
						survey_datetime,
						response_datetime,
						graded_by,
						survey_template_id,
						ROUND( auto_fails_total_minus_percentage, 2 ) AS auto_fails_total_minus_percentage,
						ROUND( ( ( protocol_total_answer_numeric_weight / protocol_total_question_numeric_weight ) * 100 ), 2 ) AS protocol,
						ROUND( ( ( softskills_total_answer_numeric_weight / softskills_total_question_numeric_weight ) * 100 ), 2 ) AS softskills,
						ROUND( ( ( escalated_task_total_answer_numeric_weight / escalated_task_total_question_numeric_weight ) * 100 ), 2 ) AS escalated_task,
						ROUND( ( ( resolved_task_total_answer_numeric_weight / resolved_task_total_question_numeric_weight ) * 100 ), 2 ) AS resolved_task,
						ROUND( ( ( auto_fails_total_answer_numeric_weight / auto_fails_total_question_numeric_weight ) * 100 ), 2 ) AS auto_fails,
						failed_questions_count,
						ROUND( protocol_total_question_numeric_weight ) AS protocol_total_question_numeric_weight,
						ROUND( protocol_total_answer_numeric_weight ) AS protocol_total_answer_numeric_weight,
						ROUND( softskills_total_question_numeric_weight ) AS softskills_total_question_numeric_weight,
						ROUND( softskills_total_answer_numeric_weight ) AS softskills_total_answer_numeric_weight,
						ROUND( escalated_task_total_question_numeric_weight ) AS escalated_task_total_question_numeric_weight,
						ROUND( escalated_task_total_answer_numeric_weight ) AS escalated_task_total_answer_numeric_weight,
						ROUND( resolved_task_total_question_numeric_weight ) AS resolved_task_total_question_numeric_weight,
						ROUND( resolved_task_total_answer_numeric_weight ) AS resolved_task_total_answer_numeric_weight,
						ROUND( auto_fails_total_question_numeric_weight ) AS auto_fails_total_question_numeric_weight,
						ROUND( auto_fails_total_answer_numeric_weight ) AS auto_fails_total_answer_numeric_weight
					FROM (
						SELECT
							survey_id,
							task_id,
							employee_id,
							employee_name,
							team_id,
							manager_employee_id,
							graded_by,
							survey_template_id,
							survey_datetime,
							response_datetime,
							min( auto_fails_total_minus_percentage ) AS auto_fails_total_minus_percentage,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::PROTOCOL . ', ' . CSurveyTemplateQuestionGroup::STANDARD_PROTOCOL . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight
									END ) AS protocol_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::PROTOCOL . ', ' . CSurveyTemplateQuestionGroup::STANDARD_PROTOCOL . ')
										THEN answer_numeric_weight ELSE NULL
									END ) AS protocol_total_answer_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::SOFTSKILLS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_SOFTSKILLS . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS softskills_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::SOFTSKILLS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_SOFTSKILLS . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS softskills_total_answer_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::ESCALATED_TASKS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_ESCALATED_TASKS . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS escalated_task_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::ESCALATED_TASKS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_ESCALATED_TASKS . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS escalated_task_total_answer_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::RESOLVED_TASKS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_RESOLVED_TASKS . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS resolved_task_total_question_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::RESOLVED_TASKS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_RESOLVED_TASKS . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS resolved_task_total_answer_numeric_weight,
							SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::SUPPORT_TASK_QA_AUTO_FAILS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_SUPPORT_TASK_QA_AUTO_FAILS . ' )
											AND numeric_weight IS NOT NULL
											AND question_numeric_weight > 0
										THEN question_numeric_weight ELSE NULL
									END ) AS auto_fails_total_question_numeric_weight,
									SUM( CASE WHEN
											survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::SUPPORT_TASK_QA_AUTO_FAILS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_SUPPORT_TASK_QA_AUTO_FAILS . ' )
										THEN answer_numeric_weight ELSE NULL
									END ) AS auto_fails_total_answer_numeric_weight,
							COUNT( CASE WHEN
											is_percentage = 1
											AND question_numeric_weight = 0
											AND numeric_weight = 0
										THEN survey_template_question_id ELSE NULL
									END ) AS failed_questions_count
						FROM (
								SELECT
									sqa.id,
									sqa.survey_template_question_id,
									sqa.survey_template_question_group_id,
									sqa.answer,
									sqa.numeric_weight,
									s.id as survey_id,
									s.trigger_reference_id As task_id,
									s.responder_reference_id AS graded_by,
									s.subject_reference_id AS employee_id,
									s.preferred_name AS employee_name,
									s.team_id,
									s.manager_employee_id,
									s.survey_datetime,
									s.response_datetime,
									s.survey_template_id,
									stq.is_percentage,
									stq.numeric_weight as question_numeric_weight,
									 ( ( stq.numeric_weight * sqa.numeric_weight ) / 100 ) as answer_numeric_weight,
									 ( SELECT
										SUM( CASE
												WHEN
													sqa1.survey_template_question_group_id IN ( ' . CSurveyTemplateQuestionGroup::SUPPORT_TASK_QA_AUTO_FAILS . ', ' . CSurveyTemplateQuestionGroup::STANDARD_SUPPORT_TASK_QA_AUTO_FAILS . ' )
												THEN
													sqa1.numeric_weight
												ELSE
													NULL
												END ) * 20.01 AS auto_fails_total_minus_percentage
										FROM
											survey_question_answers AS sqa1 where sqa1.survey_id=s.id
									),
									rank() OVER ( PARTITION BY s.trigger_reference_id ORDER BY s.survey_datetime DESC ) as rank
								FROM
									survey_question_answers sqa
									JOIN survey_tasks s ON ( s.id = sqa.survey_id AND s.survey_template_id = sqa.survey_template_id)
									LEFT JOIN survey_template_questions stq ON ( stq.survey_template_id = sqa.survey_template_id AND sqa.survey_template_question_id = stq.id )
							 ) AS innerquery
						WHERE
							 innerquery.rank = 1
						GROUP BY
							innerquery.survey_id,
							innerquery.task_id,
							innerquery.graded_by,
							innerquery.employee_id,
							innerquery.employee_name,
							innerquery.survey_datetime,
							innerquery.response_datetime,
							innerquery.team_id,
							innerquery.manager_employee_id,
							innerquery.survey_template_id
						ORDER BY
							' . $strInnerOrderBy . '
					) AS subquery';

		$arrmixSurveyTasksData = ( array ) fetchData( $strSql, $objAdminDatabase );

		if( false == valArr( $arrmixSurveyTasksData ) ) {
			return NULL;
		}

		$arrmixSurveyData = array();

		foreach( $arrmixSurveyTasksData as $arrmixSurveyTaskData ) {
			$arrintTotalPercentage = array();

			$intTotalQuestionsNumericWeight = ( $arrmixSurveyTaskData['protocol_total_question_numeric_weight'] + $arrmixSurveyTaskData['softskills_total_question_numeric_weight'] + $arrmixSurveyTaskData['escalated_task_total_question_numeric_weight'] + $arrmixSurveyTaskData['resolved_task_total_question_numeric_weight'] );

			if( 0 < $intTotalQuestionsNumericWeight ) {
				$arrintTotalPercentage['total_score'] = ( ( $arrmixSurveyTaskData['protocol_total_answer_numeric_weight'] + $arrmixSurveyTaskData['softskills_total_answer_numeric_weight'] + $arrmixSurveyTaskData['escalated_task_total_answer_numeric_weight'] + $arrmixSurveyTaskData['resolved_task_total_answer_numeric_weight'] ) * 100 ) / $intTotalQuestionsNumericWeight;
			} else {
				$arrintTotalPercentage['total_score'] = 100;
			}

			if( ( 0 == $arrmixSurveyTaskData['auto_fails'] ) && ( NULL != $arrmixSurveyTaskData['auto_fails'] ) ) {
				$arrintTotalPercentage['total_score'] = 0;
			} elseif( 100 > $arrmixSurveyTaskData['auto_fails'] ) {
				$arrintTotalPercentage['total_score'] = ( ( $arrintTotalPercentage['total_score'] * ( 100 - $arrmixSurveyTaskData['auto_fails'] ) ) / 100 );
			}

			if( 0 < $arrmixSurveyTaskData['auto_fails_total_minus_percentage'] ) {
				$arrintTotalPercentage['total_score'] = $arrintTotalPercentage['total_score'] - $arrmixSurveyTaskData['auto_fails_total_minus_percentage'];
			}

			if( 0 >= $arrintTotalPercentage['total_score'] ) {
				$arrintTotalPercentage['total_score'] = 0;
			}

			$arrmixSurveyData[] = array_merge( $arrmixSurveyTaskData, $arrintTotalPercentage );

		}

		return $arrmixSurveyData;
	}

	public static function fetchSurveyByResponderReferenceIdBySurveyTypeIdBySurveyTemplateId( $intResponderReferenceId, $intSurveyTypeId, $intSurveyTemplateId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						surveys
					WHERE
						responder_reference_id = ' . ( int ) $intResponderReferenceId . '
						AND survey_type_id = ' . ( int ) $intSurveyTypeId . '
						AND survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND response_datetime IS NULL
						AND declined_datetime IS NULL';

		return self::fetchSurvey( $strSql, $objDatabase );
	}

	public static function fetchSurveyWithHonorBadgesByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == valId( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT sub_query.id as survey_id,
						sub_query.survey_datetime,
							count(DISTINCT CASE
								WHEN hb.honour_badge_type_id= ' . CHonourBadgeType::SILVER_HONOUR_BADGE . ' AND hb.honour_badge_datetime BETWEEN
										sub_query.previous_survey_time AND sub_query.survey_datetime
								THEN hb.id
								END) AS hb_silver_count,
							count(DISTINCT CASE
								WHEN hb.honour_badge_type_id= ' . CHonourBadgeType::BRONZE_HONOUR_BADGE . ' AND hb.honour_badge_datetime BETWEEN
								sub_query.previous_survey_time AND sub_query.survey_datetime
								THEN hb.id
								END) AS hb_bronze_count,
							count(DISTINCT CASE
								WHEN hb.honour_badge_type_id=' . CHonourBadgeType::GOLDEN_HONOUR_BADGE . ' AND hb.honour_badge_datetime BETWEEN
								sub_query.previous_survey_time and
								sub_query.survey_datetime THEN hb.id
								END) AS hb_golden_count
						FROM (
								SELECT s.id,
									s.survey_datetime,
									s.subject_reference_id,
									lag(s.survey_datetime) OVER(
									ORDER BY s.id) AS previous_survey_time
								FROM surveys s
									WHERE s.subject_reference_id = ' . ( int ) $intEmployeeId . '
									AND s.responder_reference_id = ' . ( int ) $intEmployeeId . '
									AND s.declined_datetime IS NULL
									AND s.survey_type_id = ' . CSurveyType::PERSONAL_REVIEW . '
									AND s.survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_IN . '
								ORDER BY s.id DESC
								) sub_query
						LEFT JOIN honour_badges hb ON (sub_query.subject_reference_id = hb.target_employee_id)
						GROUP BY sub_query.id, sub_query.survey_datetime
						ORDER BY sub_query.id DESC';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchSurveysByEmployeeApplicationIdBySurveyTypeIdByJobPostingStepId( $intEmployeeApplicationId, $intJobPostingStepId, $intSurveyTypeId, $objAdminDatabase ) {
		if( false == is_numeric( $intEmployeeApplicationId ) || false == is_numeric( $intJobPostingStepId ) ) return NULL;

		$strSql = 'SELECT
						s.*
					FROM
						surveys s
						LEFT JOIN employee_application_interviews eai ON ( eai.id = s.trigger_reference_id )
					WHERE
						eai.employee_application_id = ' . ( int ) $intEmployeeApplicationId . '
						AND eai.ps_job_posting_step_id = ' . ( int ) $intJobPostingStepId . '
						AND s.survey_type_id = ' . ( int ) $intSurveyTypeId;

		return parent::fetchSurveys( $strSql, $objAdminDatabase );
	}

}
?>