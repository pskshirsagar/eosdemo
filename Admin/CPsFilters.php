<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsFilters
 * Do not add any new functions to this class.
 */

class CPsFilters extends CBasePsFilters {

	public static function fetchPsFiltersByUserIdByFilterTypeId( $intUserId, $intFilterTypeId, $objDatabase, $arrmixEditFilter = NULL ) {

		$strLimitConditionSql = '';
		if( valArr( $arrmixEditFilter ) ) {
			$intOffset = ( int ) $arrmixEditFilter['intPageSize'] * ( ( int ) $arrmixEditFilter['intPageNo'] - 1 );
			$strLimitConditionSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $arrmixEditFilter['intPageSize'];
		}
		$strSql = ' SELECT * FROM 
                        ps_filters 
                    WHERE  
                        filter_type_id = ' . ( int ) $intFilterTypeId . ' 
                        AND ( user_id = ' . ( int ) $intUserId . ' OR is_system = 1 ) 
                        ORDER BY name ' . ( string ) $strLimitConditionSql;

		return self::fetchPsFilters( $strSql, $objDatabase );
	}

	public static function fetchPsFiltersByFilterTypeId( $intFilterTypeId, $objDatabase, $boolIsSystem = NULL ) {
		$strSql = 'SELECT * FROM ps_filters WHERE filter_type_id = ' . ( int ) $intFilterTypeId;

		if( true === $boolIsSystem ) {
			$strSql .= ' AND is_system = 1';
		} elseif( false === $boolIsSystem ) {
			$strSql .= ' AND is_system = 0';
		}

		$strSql .= ' ORDER BY name ';

		return self::fetchPsFilters( $strSql, $objDatabase );
	}

	public static function fetchPsFiltersListByFilterTypeId( $intFilterTypeId, $objDatabase, $boolIsSystem = NULL ) {
		$strSql = 'SELECT id, name FROM ps_filters WHERE filter_type_id = ' . ( int ) $intFilterTypeId;

		if( true === $boolIsSystem ) {
			$strSql .= ' AND is_system = 1';
		} elseif( false === $boolIsSystem ) {
			$strSql .= ' AND is_system = 0';
		}

		$strSql .= ' ORDER BY name ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPsFiltersListByUserIdByFilterTypeId( $intUserId, $intFilterTypeId, $objDatabase ) {
		$strSql = ' SELECT id, name FROM ps_filters WHERE  filter_type_id = ' . ( int ) $intFilterTypeId . ' AND ( user_id = ' . ( int ) $intUserId . ' OR is_system = 1 ) ORDER BY name';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchConflictingPsFilters( $objConflictingPsFilter, $objAdminDatabase ) {

		$intId = ( false == is_numeric( $objConflictingPsFilter->getId() ) ) ? 0 : $objConflictingPsFilter->getId();

		$strSql = ' SELECT
						pf.*
					FROM
						ps_filters pf
					WHERE
						pf.filter_type_id = ' . ( int ) $objConflictingPsFilter->getFilterTypeId() . '
						AND pf.user_id = ' . ( int ) $objConflictingPsFilter->getUserId() . '
						AND lower( pf.name ) = lower( \'' . trim( addslashes( $objConflictingPsFilter->getName() ) ) . '\' )
						AND pf.id <> ' . ( int ) $intId;

		return self::fetchPsFilters( $strSql, $objAdminDatabase );
	}

	public static function fetchPsFilterByFilterTypeIdByNameByCreatedBy( $intFilterTypeId, $strName, $intCreatedBy, $objAdminDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						ps_filters
					WHERE
						filter_type_id = ' . ( int ) $intFilterTypeId . '
						AND lower( name ) = lower( \'' . trim( addslashes( $strName ) ) . '\' )
						AND created_by = ' . ( int ) $intCreatedBy . '  LIMIT 1 ';

		return self::fetchPsFilter( $strSql, $objAdminDatabase );
	}

	public static function fetchSystemPsFilters( $objDatabase ) {
		$strSql = 'SELECT
							ps.*
						FROM
							ps_filters ps
						WHERE
							ps.is_system=1';

		return rekeyArray( 'name', fetchData( $strSql, $objDatabase ) );
	}

	// Functions from CTaskFilters

	public static function fetchConflictingPsFiltersCount( $objConflictingPsFilter, $objAdminDatabase ) {

		$intId = ( false == is_numeric( $objConflictingPsFilter->getId() ) ) ? 0 : $objConflictingPsFilter->getId();

		$strWhereSql = 'WHERE
							filter_type_id = ' . ( int ) $objConflictingPsFilter->getFilterTypeId() . '
							AND user_id = ' . ( int ) $objConflictingPsFilter->getUserId() . '
							AND lower( name ) = lower( \'' . trim( addslashes( $objConflictingPsFilter->getName() ) ) . '\' )
							AND id <> ' . ( int ) $intId;

		return self::fetchPsFilterCount( $strWhereSql, $objAdminDatabase );
	}

	public static function fetchPsFilterByIdByFilterTypeId( $intId, $intFilterTypeId, $objAdminDatabase ) {

		$strSql = ' SELECT
						pf.*
					FROM
						ps_filters pf
					WHERE
						pf.filter_type_id = ' . ( int ) $intFilterTypeId . '
						AND pf.id = ' . ( int ) $intId . '';

		return self::fetchPsFilter( $strSql, $objAdminDatabase );
	}

}
?>