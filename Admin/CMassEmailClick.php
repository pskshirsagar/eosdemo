<?php

class CMassEmailClick extends CBaseMassEmailClick {

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strEmailAddress;
	protected $m_strCompanyName;

	/**
	 * Get Functions
	 */

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	/**
	 * Set Functions
	 */

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name_first'] ) ) $this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) $this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['email_address'] ) ) $this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( $arrmixValues['company_name'] );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>