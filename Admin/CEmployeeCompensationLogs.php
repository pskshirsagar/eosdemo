<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeCompensationLogs
 * Do not add any new functions to this class.
 */

class CEmployeeCompensationLogs extends CBaseEmployeeCompensationLogs {

	public static function fetchLatestEmployeeCompensationLogByEmployeeId( $intViewerEmployeeId, $intEmployeeId, $objDatabase ) {
		$strSql = '
					SELECT ecl.*,
						new_comp.encrypted_amount AS new_compensation_encrypted,
						hourly_comp.encrypted_amount AS hourly_rate_compensation_encrypted
				 	FROM
						employee_compensation_logs ecl
						LEFT JOIN employee_encryption_associations new_comp ON ( new_comp.id = ecl.new_employee_encryption_association_id AND new_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND new_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
						LEFT JOIN employee_encryption_associations hourly_comp ON( hourly_comp.id = ecl.hourly_rate_employee_encryption_association_id AND hourly_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND hourly_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
					WHERE
						ecl.employee_id = ' . ( int ) $intEmployeeId . '
						AND ecl.is_verified = 1
						AND ecl.bonus_employee_encryption_association_id IS NULL
					ORDER BY
						ecl.id DESC
					LIMIT 1';

		return self::fetchEmployeeCompensationLog( $strSql, $objDatabase );
	}

	public static function fetchRecentEmployeeCompensationLogsByEmployeeIds( $intViewerEmployeeId, $arrintEmployeeIds, $objDatabase, $boolIsForBonus = false ) {
		if( false == valArr( $arrintEmployeeIds ) || true == empty( $intViewerEmployeeId ) ) return NULL;

		$strBonusAmountCondition = ( true == $boolIsForBonus ) ? 'AND bonus_employee_encryption_association_id IS NOT NULL' : 'AND bonus_employee_encryption_association_id IS NULL';

		$strSql = 'SELECT
						ecl.*,
						prev_comp.encrypted_amount AS previous_compensation_encrypted,
						new_comp.encrypted_amount AS new_compensation_encrypted,
						hourly_comp.encrypted_amount AS hourly_rate_encrypted,
						bonus_comp.encrypted_amount AS bonus_compensation_encrypted
					FROM
						employee_compensation_logs ecl
						LEFT JOIN employee_encryption_associations prev_comp ON ( prev_comp.id = ecl.previous_employee_encryption_association_id AND prev_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND prev_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
						LEFT JOIN employee_encryption_associations new_comp ON ( new_comp.id = ecl.new_employee_encryption_association_id AND new_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND new_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
						LEFT JOIN employee_encryption_associations hourly_comp ON ( hourly_comp.id = ecl.hourly_rate_employee_encryption_association_id AND hourly_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND hourly_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
						LEFT JOIN employee_encryption_associations bonus_comp ON ( bonus_comp.id = ecl.bonus_employee_encryption_association_id AND bonus_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND bonus_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
					WHERE
						ecl.requested_on =
						(
							SELECT
								max ( requested_on )
							FROM
								employee_compensation_logs
							WHERE
								employee_id = ecl.employee_id
								 AND is_verified = 1
								' . $strBonusAmountCondition . '
							GROUP BY employee_id
						)
						AND ecl.is_verified = 1
						AND ecl.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						' . $strBonusAmountCondition;

		return self::fetchEmployeeCompensationLogs( $strSql, $objDatabase );
	}

	public static function fetchEmployeeCompensationLogsByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						employee_compensation_logs
					WHERE
						employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return parent::fetchEmployeeCompensationLogs( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeeCompensationLogsByEmployeeIdsForTypeCompensation( $arrintEmployeeIds, $objDatabase, $boolTypeCompensation = true ) {

		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						employee_compensation_logs
					WHERE
						employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		if( true == $boolTypeCompensation ) {
			$strSql .= ' AND bonus_employee_encryption_association_id IS NULL';
		} else {
			$strSql .= ' AND bonus_employee_encryption_association_id IS NOT NULL';
		}

		return parent::fetchEmployeeCompensationLogs( $strSql, $objDatabase );
	}

	public static function fetchVerifyEmployeeCompensationLogsByEmployeeIds( $intViewerEmployeeId, $arrintEmployeeIds, $arrstrEmployeeCompensationFilter, $objAdminDatabase, $boolIsForCount = false ) {
		if( false == valArr( $arrintEmployeeIds ) || true == empty( $intViewerEmployeeId ) ) return false;

		$intOffset			= ( false == $boolIsForCount && false == empty( $arrstrEmployeeCompensationFilter['page_no'] ) && false == empty( $arrstrEmployeeCompensationFilter['page_size'] ) ) ? $arrstrEmployeeCompensationFilter['page_size'] * ( $arrstrEmployeeCompensationFilter['page_no'] - 1 ) : 0;
		$intLimit			= ( false == $boolIsForCount && true == isset( $arrstrEmployeeCompensationFilter['page_size'] ) ) ? $arrstrEmployeeCompensationFilter['page_size'] : '';
		$strOrderByClause 	= ( false == $boolIsForCount ) ? ' ORDER BY ecl.purchase_request_id DESC ' : '';

		if( false == $boolIsForCount ) {
			$strSelectClause = ' ecl.*,
				prev_comp.encrypted_amount AS previous_compensation_encrypted,
				new_comp.encrypted_amount AS new_compensation_encrypted,
				hourly_rate_comp.encrypted_amount AS hourly_rate_compensation_encrypted,
				bonus_comp.encrypted_amount AS bonus_compensation_encrypted ';
		} else {
			$strSelectClause = ' count( ecl.id ) ';
		}

		$strSql = ' SELECT
						' . $strSelectClause . '
					FROM
						employee_compensation_logs ecl
						JOIN (
								SELECT
									MAX( ecl.id ) AS employee_compensation_log_id
								FROM
									employee_compensation_logs ecl
								WHERE
									ecl.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
								GROUP BY
									ecl.employee_id,
									CASE WHEN ecl.bonus_employee_encryption_association_id IS NOT NULL THEN ecl.id ELSE NULL END
						) as subquery ON ( subquery.employee_compensation_log_id = ecl.id )
						JOIN purchase_requests pr ON ( ecl.purchase_request_id = pr.id )
						LEFT JOIN employee_encryption_associations prev_comp ON ( prev_comp.id = ecl.previous_employee_encryption_association_id AND prev_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND prev_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . '  )
						LEFT JOIN employee_encryption_associations new_comp ON ( new_comp.id = ecl.new_employee_encryption_association_id AND new_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND new_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
						LEFT JOIN employee_encryption_associations hourly_rate_comp ON ( hourly_rate_comp.id = ecl.hourly_rate_employee_encryption_association_id AND hourly_rate_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND hourly_rate_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
						LEFT JOIN employee_encryption_associations bonus_comp ON ( bonus_comp.id = ecl.bonus_employee_encryption_association_id AND bonus_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND bonus_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
					WHERE
						ecl.is_verified = 0
						AND ecl.requested_on <= NOW()
						AND pr.approved_on IS NOT NULL
						AND pr.denied_on IS NULL
						AND pr.deleted_on IS NULL
						' . $strOrderByClause . '
					OFFSET ' . ( int ) $intOffset;

		if( true == $boolIsForCount ) {
			$arrintEmployeeCompensationLogsCount = fetchData( $strSql, $objAdminDatabase );

			return ( true == valArr( $arrintEmployeeCompensationLogsCount ) ) ? $arrintEmployeeCompensationLogsCount[0]['count'] : 0;
		} else {
			if( 0 < $intLimit ) $strSql .= ' LIMIT ' . ( int ) $intLimit;
			return self::fetchEmployeeCompensationLogs( $strSql, $objAdminDatabase );
		}
	}

	public static function fetchEmployeeCompensationLogByIds( $intViewerEmployeeId, $arrintEmployeeCompensationLogIds, $objAdminDatabase ) {
		if( false == valArr( $arrintEmployeeCompensationLogIds ) || true == empty( $intViewerEmployeeId ) ) return NULL;

		$strSql = ' SELECT
						ecl.*,
						prev_comp.encrypted_amount AS previous_compensation_encrypted,
						new_comp.encrypted_amount AS new_compensation_encrypted,
						hourly_rate_comp.encrypted_amount AS hourly_rate_compensation_encrypted,
						bonus_comp.encrypted_amount AS bonus_compensation_encrypted
					FROM
						employee_compensation_logs ecl
						LEFT JOIN employee_encryption_associations prev_comp ON ( prev_comp.id = ecl.previous_employee_encryption_association_id AND prev_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND prev_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . '  )
						LEFT JOIN employee_encryption_associations new_comp ON ( new_comp.id = ecl.new_employee_encryption_association_id AND new_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND new_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
						LEFT JOIN employee_encryption_associations hourly_rate_comp ON ( hourly_rate_comp.id = ecl.hourly_rate_employee_encryption_association_id AND hourly_rate_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND hourly_rate_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
						LEFT JOIN employee_encryption_associations bonus_comp ON ( bonus_comp.id = ecl.bonus_employee_encryption_association_id AND bonus_comp.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND bonus_comp.viewer_employee_id = ' . ( int ) $intViewerEmployeeId . ' )
					WHERE
						ecl.id IN ( ' . implode( ',', $arrintEmployeeCompensationLogIds ) . ' )';

		return self::fetchEmployeeCompensationLogs( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeCompensationLogsByPurchaseRequestIds( $arrintPurchaseRequestIds, $objDatabase ) {
		if( true == empty( $arrintPurchaseRequestIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						employee_compensation_logs ecl
					WHERE
						ecl.is_verified = 1
						AND ecl.purchase_request_id IN ( ' . implode( ',', $arrintPurchaseRequestIds ) . ' )';

		return self::fetchEmployeeCompensationLogs( $strSql, $objDatabase );
	}

	public static function fetchLatestEmployeeCompensationLogByEmployeeIds( $arrintEmployeeIds, $objDatabase, $boolCompRequest = false ) {
		$strWhereCondition = '';

		if( false == valArr( $arrintEmployeeIds ) ) {
			return false;
		}
		if( true == $boolCompRequest ) {
			$strSelect = ' ecl.employee_id,
							EXTRACT( YEAR FROM age( NOW()::date, ecl.requested_on ) ) as year, 
							EXTRACT( MONTH FROM age( NOW()::date, ecl.requested_on ) ) as month';
			$strWhereCondition = ' AND ecl.bonus_employee_encryption_association_id IS NULL AND ecl.deleted_on IS NULL ';
		} else {
			$strSelect = ' ecl.* ';
		}

		$strSql = 'SELECT
						 ' . $strSelect . '
					FROM
						(
							SELECT
								ecl.*,
								rank ( ) OVER ( PARTITION BY ecl.employee_id ORDER BY ecl.id DESC ) AS rank
							FROM
								employee_compensations ec
								JOIN employee_compensation_logs ecl ON ( ecl.employee_id = ec.employee_id AND ecl.new_employee_encryption_association_id = ec.employee_encryption_association_id )
							WHERE
								ecl.is_verified = 1
								AND ( ecl.new_employee_encryption_association_id IS NOT NULL
								OR ecl.previous_employee_encryption_association_id IS NOT NULL ) ' . $strWhereCondition . '
						) ecl
					WHERE
						ecl.rank = 1 AND
						ecl.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		if( true == $boolCompRequest ) {
			return fetchData( $strSql, $objDatabase );
		} else {
			return self::fetchEmployeeCompensationLogs( $strSql, $objDatabase );
		}

	}

	public static function fetchAllEmployeeCompensationLogsByEmployeeIds( $arrintEmployeeIds, $arrstrEmployeeCompensationFilter, $objAdminDatabase ) {
		$strWhereClause 	= ' ecl.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ';

		if( false == empty( $arrstrEmployeeCompensationFilter['dates'] ) ) {
			if( 1 == $arrstrEmployeeCompensationFilter['dates'] && true == valStr( $arrstrEmployeeCompensationFilter['single_date'] ) ) {
				$strWhereClause	.= ' AND ecl.requested_on BETWEEN \'' . $arrstrEmployeeCompensationFilter['single_date'] . ' 00:00:00 \' AND \'' . $arrstrEmployeeCompensationFilter['single_date'] . ' 23:59:59 \'';
			} elseif( 2 == $arrstrEmployeeCompensationFilter['dates'] && true == valStr( $arrstrEmployeeCompensationFilter['start_date'] ) && true == valStr( $arrstrEmployeeCompensationFilter['end_date'] ) ) {
				$strWhereClause	.= ' AND ecl.requested_on BETWEEN \'' . $arrstrEmployeeCompensationFilter['start_date'] . ' 00:00:00 \' AND \'' . $arrstrEmployeeCompensationFilter['end_date'] . ' 23:59:59\'';
			}
		}

		if( true == $arrstrEmployeeCompensationFilter['show_bonus'] ) {
			$strWhereClause 	.= 'AND ecl.bonus_employee_encryption_association_id IS NOT NULL';
			$strSelectClause 	= 'ecl.*, eea2.encrypted_amount AS bonus_compensation_encrypted, pr.reason, pr.bonus_type_id, pr.annual_bonus_potential_encrypted,
									CASE WHEN e.bonus_potential_encrypted IS NULL THEN dbc.encrypted_annual_bonus_potential ELSE e.bonus_potential_encrypted END as bonus_potential_encrypted,
									CASE WHEN e.bonus_potential_encrypted IS NULL THEN dbc.annual_bonus_type_id ELSE e.annual_bonus_type_id END as bonus_frequency';
			$strJoins 			= 'LEFT JOIN employee_encryption_associations eea2 ON( eea2.id = ecl.bonus_employee_encryption_association_id AND eea2.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND eea2.viewer_employee_id = ' . $arrstrEmployeeCompensationFilter['viewer_employee_id'] . ' )
									LEFT JOIN purchase_requests pr ON( ecl.purchase_request_id = pr.id )
									LEFT JOIN designation_bonus_criterias dbc ON ( dbc.designation_id = e.designation_id AND dbc.approved_on IS NOT NULL AND CURRENT_DATE >= dbc.start_date::DATE AND ( dbc.end_date IS NULL OR CURRENT_DATE <= dbc.end_date::DATE ) )';
		} elseif( true == $arrstrEmployeeCompensationFilter['show_compensation'] ) {
			$strWhereClause 	.= 'AND ecl.bonus_employee_encryption_association_id IS NULL';
			$strSelectClause 	= ' ecl.*, eea.encrypted_amount AS previous_compensation_encrypted, eea1.encrypted_amount AS new_compensation_encrypted ';
			$strJoins 			= 'LEFT JOIN employee_encryption_associations eea ON( eea.id = ecl.previous_employee_encryption_association_id AND eea.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND eea.viewer_employee_id = ' . $arrstrEmployeeCompensationFilter['viewer_employee_id'] . ' )
									LEFT JOIN employee_encryption_associations eea1 ON( eea1.id = ecl.new_employee_encryption_association_id AND eea1.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND eea1.viewer_employee_id = ' . $arrstrEmployeeCompensationFilter['viewer_employee_id'] . ' )';
		} else {
			$strSelectClause 	= ' ecl.*, eea.encrypted_amount AS previous_compensation_encrypted, eea1.encrypted_amount AS new_compensation_encrypted, eea2.encrypted_amount AS bonus_compensation_encrypted, bonus_type_id ';
			$strJoins 			= 'LEFT JOIN employee_encryption_associations eea ON( eea.id = ecl.previous_employee_encryption_association_id AND eea.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND eea.viewer_employee_id = ' . $arrstrEmployeeCompensationFilter['viewer_employee_id'] . ' )
									LEFT JOIN employee_encryption_associations eea1 ON( eea1.id = ecl.new_employee_encryption_association_id AND eea1.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND eea1.viewer_employee_id = ' . $arrstrEmployeeCompensationFilter['viewer_employee_id'] . ' )
									LEFT JOIN employee_encryption_associations eea2 ON( eea2.id = ecl.bonus_employee_encryption_association_id AND eea2.encryption_system_type_id = ' . ( int ) CEncryptionSystemType::COMPENSATION . ' AND eea2.viewer_employee_id = ' . $arrstrEmployeeCompensationFilter['viewer_employee_id'] . ' )
									LEFT JOIN purchase_requests pr ON ( pr.id = ecl.purchase_request_id )';
		}

		$strSql = ' SELECT
						' . $strSelectClause . '
					FROM
						employee_compensation_logs ecl
						JOIN employees e ON ( e.id = ecl.employee_id )
						' . $strJoins . '
					WHERE
						ecl.is_verified = 1 AND ' . $strWhereClause . ' 
					ORDER BY e.name_full, ecl.id DESC, ecl.requested_on DESC';

		return fetchData( $strSql, $objAdminDatabase );
	}

}
?>