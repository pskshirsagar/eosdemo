<?php

class CEntityReferenceType extends CBaseEntityReferenceType {

	const COMPANY_USERS		= 1;
	const USERS				= 2;
	const CUSTOMERS			= 3;

	public static $c_arrintAllEnttityReferenceTypeIds = array(
		self::COMPANY_USERS => self::COMPANY_USERS,
		self::USERS => self::USERS,
		self::CUSTOMERS => self::CUSTOMERS
	);

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>