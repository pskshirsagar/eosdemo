<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyConditionTypes
 * Do not add any new functions to this class.
 */

class CSurveyConditionTypes extends CBaseSurveyConditionTypes {

	public static function fetchSurveyConditionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CSurveyConditionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSurveyConditionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CSurveyConditionType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>