<?php

class CEcsMessage extends CBaseEcsMessage {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSid( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getSid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sid', 'Message SID is required.' ) );
			return $boolIsValid;
		}

		$strSqlCondition = ( 0 < $this->getSid() ? ' AND id <>' . $this->getSid() : '' );
		$strWhere = 'WHERE lower( sid )= \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getSid() ) ) . '\' ' . $strSqlCondition;
		$intCount = CEcsMessages::fetchEcsMessageCount( $strWhere, $objAdminDatabase );
		if( 0 < $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Message SID ( ' . $this->getSid() . ' ) is already exists.' ) );
		}

		return $boolIsValid;
	}

	public function valMsgBody() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReceivedFrom() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSentTo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valSid( $objAdminDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createECSMessage( $objMessage ) {

		$objEcsMessage = new CEcsMessage();

		$objEcsMessage->setDefaults();
		$objEcsMessage->setId( $objEcsMessage->fetchNextId( $this->loadAdminDatabase() ) );
		$objEcsMessage->setSid( $objMessage->sid );
		$objEcsMessage->setMsgBody( $objMessage->body );
		$objEcsMessage->setReceivedFrom( $objMessage->from );
		$objEcsMessage->setSentTo( $objMessage->to );
		$objEcsMessage->setStatus( $objMessage->status );
		$objEcsMessage->setUpdatedOn( $objMessage->date_updated );
		$objEcsMessage->setCreatedOn( $objMessage->date_created );

		return $objEcsMessage;
	}

}
?>