<?php

class CImportRequestEntity extends CBaseImportRequestEntity {

	use Psi\Libraries\Container\TContainerized;
	use Psi\Libraries\EosFoundation\TEosStoredObject;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDestinationPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportEntityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valErrorMessages() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAcceptedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAcceptedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImportDataTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileSize() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValidationStartDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValidationEndDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSyncStartDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSyncEndDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValidationErrorDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSyncErrorDesciption() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getDetails( $boolReturnArray = false ) {
		if( false == $boolReturnArray ) {
			return parent::getDetails();
		}

		if( true == valObj( $this->m_jsonDetails, 'stdClass' ) ) {
			$this->m_strDetails = json_encode( $this->m_jsonDetails );
		}

		return json_decode( $this->m_strDetails, true );
	}

	public function calcStorageKey() {
		$strKey				= date( 'Y/m/d/', strtotime( $this->getCreatedOn() ) ) . $this->getId() . '/' . $this->getFileName();
		$strKey				= $this->getCid() . '/csv/' . $strKey;

		return $strKey;
	}

	protected function calcStorageContainer( $strVendor = NULL ) {
		return CONFIG_OSG_BUCKET_SYSTEM_MIGRATION;
	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

}
?>