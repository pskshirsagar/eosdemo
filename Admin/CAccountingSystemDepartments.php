<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CAccountingSystemDepartments
 * Do not add any new functions to this class.
 */

class CAccountingSystemDepartments extends CBaseAccountingSystemDepartments {

	public static function fetchAllAccountingSystemDepartments( $objDatabase ) {

		$strSql = 'SELECT * FROM accounting_system_departments WHERE department_id <> ' . CAccountingSystemDepartment::ADMINISTRATION . ' ORDER BY name ASC';
		return self::fetchAccountingSystemDepartments( $strSql, $objDatabase );
	}

}
?>