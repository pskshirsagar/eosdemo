<?php

class CEmploymentApplicationQuestion extends CBaseEmploymentApplicationQuestion {

	/**
	 * Fetch Functions
	 */

	public function fetchEmploymentApplication( $objDatabase ) {
		return CEmploymentApplications::fetchEmploymentApplicationById( $this->getEmploymentApplicationId(), $objDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valQuestion() {
		$boolIsValid = true;

		if( true == is_null( $this->getQuestion() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', 'Question is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valQuestion();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>