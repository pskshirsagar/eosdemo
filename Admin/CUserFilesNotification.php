<?php

class CUserFilesNotification extends CBaseUserFilesNotification {

	public function valRepositoryId( $intRepositoryId ) {
		$boolIsValid = true;
		if( true == empty( $intRepositoryId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'repository_id', 'Please select repository type.' ) );

		}
		return $boolIsValid;
	}

	public function valFileName( $objDatabase, $arrstrFilePaths, $intRepositoryId = NULL, $intUserId = NULL ) {

		$boolIsValid = true;

		if( true == empty( $arrstrFilePaths ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'File Name is required.' ) );

			return $boolIsValid;
		}

		$arrobjUserFileNotifications = CUserFilesNotifications::fetchFilePathByUserId( $intUserId, $objDatabase );
		$arrobjUserFileNotifications = rekeyArray( 'file_name', $arrobjUserFileNotifications );

		$strFileRepository 	 			 = '';
		$strFileRepositoryName   	 = '';

		if( CSvnRepository::PS_CORE == $intRepositoryId ) {
			$strFileRepository 			= PATH_DOCUMENT_ROOT;
			$strFileRepositoryName 	= 'PsCore';
		} elseif( CSvnRepository::PS_CORE_COMMON == $intRepositoryId ) {
			$strFileRepository 			= PATH_COMMON;
			$strFileRepositoryName 	= 'PsCoreCommon';
		}

		foreach( $arrstrFilePaths as $strFilePath ) {
			$boolIsValid = true;
			$strTrimmedFile = trim( $strFilePath );

			if( false == file_exists( $strFileRepository . $strTrimmedFile ) || true == empty( $strTrimmedFile ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', '\'' . $strFilePath . '\'' . ' file does not exist in ' . $strFileRepositoryName ) );
				continue;
			}

			if( true == isset( $arrobjUserFileNotifications[$strTrimmedFile] ) && $arrobjUserFileNotifications[$strTrimmedFile]['svn_repository_id'] == $intRepositoryId ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', $strTrimmedFile . ' File already exist for the same User.' ) );
				$boolIsValid = false;
				break;
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $arrstrFilePaths = array(), $intRepositoryId = NULL, $intUserId = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valRepositoryId( $intRepositoryId );
				$boolIsValid &= $this->valFileName( $objDatabase, $arrstrFilePaths, $intRepositoryId, $intUserId );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}
}
?>