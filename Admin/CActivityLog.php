<?php

class CActivityLog extends CBaseActivityLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActivityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>