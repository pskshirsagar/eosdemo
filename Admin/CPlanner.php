<?php

class CPlanner extends CBasePlanner {

	const LIMIT_FOR_PLANNER_NAME 	= 50;

	public function valId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planner_meeting_name', ' Enter valid planner Id.' ) );
		}

		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName( $boolPlannerTeamPermissions ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planner_meeting_name', 'Please add Sprint title.' ) );
			return $boolIsValid;
		}
		if( true == $boolPlannerTeamPermissions && ( false == \Psi\CStringService::singleton()->preg_match( '/^[ A-Za-z0-9\-]+$/', $this->getName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planner_meeting_name', 'No Special Characters allowed in Sprint Name. You can use - only.' ) );
			return $boolIsValid;
		} elseif( true == $boolPlannerTeamPermissions && self::LIMIT_FOR_PLANNER_NAME < \Psi\CStringService::singleton()->strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planner_meeting_name', 'Sprint name length should not be greater than 50 characters.' ) );
			return $boolIsValid;
		}
		if( false == $boolIsValid )
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planner_meeting_name', 'Valid Sprint name is required.' ) );

		return $boolIsValid;
	}

	public function valDevelopmentTeamId() {
		$boolIsValid = true;

		if( true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'planner_meeting_name', ' Enter valid planner Id.' ) );
		}

		return $boolIsValid;
	}

	public function valStartedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valCompletedOn() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDateComparison() {
		$boolIsValid = true;

		if( strtotime( $this->m_strStartDatetime ) >= strtotime( $this->m_strEndDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_datetime', 'Sprint start Date should be less than Sprint end date.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $boolPlannerTeamPermissions = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case 'validate_date':
				$boolIsValid &= $this->valDateComparison();
				break;

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valDateComparison();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				$boolIsValid &= $this->valName( $boolPlannerTeamPermissions );
				break;

			case VALIDATE_FETCH:
				$boolIsValid &= $this->valId();
				break;

			case 'sprint_form_insert':
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDevelopmentTeamId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>