<?php

use Psi\Eos\Admin\CEmailTemplates;

class CEmailTemplate extends CBaseEmailTemplate {

	const AMBIANCE_EMAIL_TEMPLATE 		= 3;
	const BASIC_TEMPLATE_ID				= 4;
	const CUSTOM_TEMPLATE_ID 			= 5;
	const SIMPLE_TEMPLATE_ID 			= 6;
	const ANNOUNCEMENT_EMAIL_TEMPLATE 	= 7;
	const FOCUS_EMAIL_TEMPLATE 			= 8;
	const HIGHLIGHT_TEMPLATE_ID 		= 9;
	const EMERGANCY_TEMPLATE_ID 		= 10;
	const DETAIL_EMAIL_TEMPLATE 		= 11;
	const INFORMATION_EMAIL_TEMPLATE 	= 12;
	const POSTER_TEMPLATE_ID 			= 13;
	const BULLETIN_TEMPLATE_ID 			= 14;

	protected $m_arrobjEmailTemplateSlots;

	public static $c_arrstrEmailTemplateKeys = array(
		self::AMBIANCE_EMAIL_TEMPLATE 		=> 'AMBIANCE_EMAIL_TEMPLATE',
		self::BASIC_TEMPLATE_ID				=> 'BASIC_EMAIL_TEMPLATE',
		self::CUSTOM_TEMPLATE_ID			=> 'CUSTOM_EMAIL_TEMPLATE',
		self::ANNOUNCEMENT_EMAIL_TEMPLATE 	=> 'ANNOUNCEMENT_EMAIL_TEMPLATE',
		self::FOCUS_EMAIL_TEMPLATE			=> 'FOCUS_EMAIL_TEMPLATE',
		self::HIGHLIGHT_TEMPLATE_ID			=> 'HIGHLIGHTS_EMAIL_TEMPLATE',
		self::DETAIL_EMAIL_TEMPLATE			=> 'DETAIL_EMAIL_TEMPLATE',
		self::INFORMATION_EMAIL_TEMPLATE	=> 'INFORMATION_EMAIL_TEMPLATE',
		self::POSTER_TEMPLATE_ID			=> 'POSTER_EMAIL_TEMPLATE',
		self::BULLETIN_TEMPLATE_ID			=> 'BULLETIN_EMAIL_TEMPLATE'
	);

	public function setDefaults() {
		$this->m_intIsPublished = 1;

		return;
	}

	public function setOrderNum( $intOrderNum ) {
		parent::setOrderNum( $intOrderNum );
		$this->m_intOrderNum = CStrings::strTrimDef( $intOrderNum, 3, NULL, true );
	}

	/**
	 * Set Functions
	 */

	public function setEmailTemplateSlots( $arrobjEmailTemplateSlots ) {
		$this->m_arrobjEmailTemplateSlots = $arrobjEmailTemplateSlots;
	}

	/**
	 * Get Functions
	 */

 	public function getEmailTemplateSlots() {
		return $this->m_arrobjEmailTemplateSlots;
 	}

	/**
	 * Validation Functions
	 */

	public function valName( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );

		} elseif( 50 < strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name should be less than or equal to 50 characters.' ) );

		} elseif( false == preg_match( '/^[a-zA-Z]*$/', $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name should contain only characters , space not allowed.' ) );

		} else {
			$intConflictEmailTemplateNameCount = $this->fetchEmailTemplateCountByName( $objAdminDatabase );

			if( 0 < $intConflictEmailTemplateNameCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name already exists.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valPath( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getPath() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'path', 'Path is required.' ) );

		} elseif( false == preg_match( '~^[a-zA-Z^/][a-zA-Z_^/]+$~', $this->getPath() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'path', 'Required valid path.' ) );

		} else {
			$intConflictEmailTemplatePathCount = $this->fetchEmailTemplateCountByPath( $objAdminDatabase );

			if( 0 < $intConflictEmailTemplatePathCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'path', 'Path already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPreviewImageUri() {
		$boolIsValid = true;

		if( false == is_null( $this->getPreviewImageUri() ) ) {

			if( false == preg_match( '~^[a-zA-Z/][a-zA-Z0-9_/]+[a-zA-Z0-9]+\.(?=(png|jpg|gif|jpeg|bmp)$)~', $this->getPreviewImageUri() ) ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'preview_image_uri', 'Required valid Preview Image URI.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valOrderNum() {

		$boolIsValid = true;

		if( false == is_null( $this->getOrderNum() ) && true == is_numeric( $this->getOrderNum() ) ) {

			if( 0 > $this->getOrderNum() || ( false == preg_match( '/^[0-9]*$/', $this->getOrderNum() ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', 'Order number should be positive.' ) );
			}

		} else {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'order_num', 'Order number should be numeric.' ) );
		}

		return $boolIsValid;
	}

	/**
	 * Validation Function
	 */

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valPath( $objDatabase );
				$boolIsValid &= $this->valPreviewImageUri();
				$boolIsValid &= $this->valOrderNum();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createEmailTemplateSlot() {

		$objEmailTemplateSlot = new CEmailTemplateSlot();
		$objEmailTemplateSlot->setEmailTemplateId( $this->getId() );

		return $objEmailTemplateSlot;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchEmailTemplateCountByName( $objAdminDatabase ) {

		if( false == is_null( $this->getId() ) ) {
			$strWhereSql = ' WHERE lower(name) = \'' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $this->getName() ) ) ) . '\' AND id != ' . ( int ) $this->getId();
		} else {
			$strWhereSql = ' WHERE lower(name) = \'' . \Psi\CStringService::singleton()->strtolower( trim( addslashes( $this->getName() ) ) ) . '\'';
		}
		return CEmailTemplates::createService()->fetchEmailTemplateCount( $strWhereSql, $objAdminDatabase );
	}

	public function fetchEmailTemplateCountByPath( $objAdminDatabase ) {

		if( false == is_null( $this->getId() ) ) {
			$strWhereSql = ' WHERE lower(path) = \'' . \Psi\CStringService::singleton()->strtolower( $this->getPath() ) . '\' AND id != ' . ( int ) $this->getId();
		} else {
			$strWhereSql = ' WHERE lower(path) = \'' . \Psi\CStringService::singleton()->strtolower( $this->getPath() ) . '\'';
		}
		return CEmailTemplates::createService()->fetchEmailTemplateCount( $strWhereSql, $objAdminDatabase );
	}

	public function fetchEmailTemplateSlots( $objAdminDatabase ) {
		return CEmailTemplateSlots::fetchEmailTemplateSlotsByEmailTemplateId( $this->getId(), $objAdminDatabase );
	}

}
?>