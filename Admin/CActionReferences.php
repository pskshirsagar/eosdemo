<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CActionReferences
 * Do not add any new functions to this class.
 */

class CActionReferences extends CBaseActionReferences {

	public static function fetchActionReferencesByActionId( $intActionId, $objDatabase ) {
		if( false == is_numeric( $intActionId ) ) return NULL;

		$strSql = 'SELECT * FROM action_references WHERE action_id =' . ( int ) $intActionId;

		$arrobjTempActionReferences = self::fetchActionReferences( $strSql, $objDatabase );

		if( false == valArr( $arrobjTempActionReferences ) ) return NULL;

		$arrobjActionReference = array();
		foreach( $arrobjTempActionReferences as $objActionReference ) {
			$arrobjActionReference[$objActionReference->getActionReferenceTypeId()][$objActionReference->getReferenceNumber()] = $objActionReference;
		}

		return $arrobjActionReference;
	}

	public static function fetchAllActionReferencesByActionId( $intActionId, $objDatabase ) {
		if( false == is_numeric( $intActionId ) ) return NULL;

		$strSql = 'SELECT * FROM action_references WHERE action_id =' . ( int ) $intActionId;

		return self::fetchActionReferences( $strSql, $objDatabase );
	}

	public static function fetchActionReferencesByActionIds( $arrintActionIds, $objDatabase, $boolClientAssignment = false ) {

		if( false == valArr( $arrintActionIds ) ) return NULL;

		$strSql = 'SELECT * FROM action_references WHERE action_id IN ( ' . implode( ',', $arrintActionIds ) . ' )';

		$arrobjTempActionReferences = self::fetchActionReferences( $strSql, $objDatabase );

		if( false == valArr( $arrobjTempActionReferences ) ) return NULL;

		$arrobjActionReference = array();

		if( true == $boolClientAssignment ) {
			foreach( $arrobjTempActionReferences as $objActionReference ) {
				$arrobjActionReference[$objActionReference->getReferenceNumber()] = $objActionReference;
			}
		} else {
			foreach( $arrobjTempActionReferences as $objActionReference ) {
				$arrobjActionReference[$objActionReference->getActionId()][$objActionReference->getActionReferenceTypeId()][$objActionReference->getReferenceNumber()] = $objActionReference;
			}
		}

		return $arrobjActionReference;
	}

	public static function fetchActionReferencesByActionIdByActionReferenceTypeId( $arrintActivivtyIds, $intActionReferenceTypeId, $objDatabase ) {
		if( false == valArr( $arrintActivivtyIds ) || false == is_numeric( $intActionReferenceTypeId ) ) return NULL;

		$strSql = 'SELECT
						p.property_name,
						ar.action_id
					FROM
				       action_references ar
				       JOIN properties p ON (ar.reference_number = p.id )
				    WHERE
				       action_id IN ( ' . implode( ',', $arrintActivivtyIds ) . ' )
				       and action_reference_type_id=' . ( int ) $intActionReferenceTypeId;

		return fetchdata( $strSql, $objDatabase );
	}

}
?>