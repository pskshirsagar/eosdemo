<?php

class CHelpResourceReviewStatus extends CBaseHelpResourceReviewStatus {

	const STATUS_NEW = 1;
	const STATUS_APPROVED = 2;
	const STATUS_ON_HOLD = 3;
	const STATUS_REVIEWING = 4;
	const STATUS_SCHEDULING = 5;
	const STATUS_RECORDING = 6;
	const STATUS_EDITING = 7;
	const STATUS_SETTING = 8;
	const STATUS_VIDEO_REVIEWING = 9;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>