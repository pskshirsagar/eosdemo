<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsTypes
 * Do not add any new functions to this class.
 */

class CStatsTypes extends CBaseStatsTypes {

	public static function fetchStatsTypesData( $objDatabase ) {

		$strSql = 'SELECT id, name FROM stats_types';

		return fetchData( $strSql, $objDatabase );
	}
}
?>