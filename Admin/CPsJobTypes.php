<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsJobTypes
 * Do not add any new functions to this class.
 */

class CPsJobTypes extends CBasePsJobTypes {

	public static function fetchPsJobTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CPsJobType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllPsJobTypes( $objDatabase ) {
		return self::fetchPsJobTypes( 'SELECT * FROM ps_job_types ORDER BY order_num', $objDatabase );
	}
}
?>