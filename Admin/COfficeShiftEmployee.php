<?php

class COfficeShiftEmployee extends CBaseOfficeShiftEmployee {

	const DAY_OF_WEEK_SATURDAY  = 6;
	const DAY_OF_WEEK_SUNDAY	= 7;
	const WEEKEND				= 0;
	const SHIFT_CHANGE			= 'shift_change';
	const RELEASE_SUPPORT		= 'release_support';
	const WEEKEND_COVERAGE		= 'weekend_coverage';

	protected $m_strPreferredName;
	protected $m_strDepartmentName;
	protected $m_strOfficeStartTimeLabel;

	public function createOfficeShiftEmployee( $objDatabase, $intEmployeeId, $intOfficeShiftId, $strStartDate, $strEndDate, $intUserId, $boolApprove = true ) {

		$objOfficeShift = COfficeShifts::fetchOfficeShiftById( $intOfficeShiftId, $objDatabase );

		if( true == valObj( $objOfficeShift, 'COfficeShift' ) ) {
			$strOfficeStartTime = $strStartDate . ' ' . getConvertedDateTimeByTimeZoneName( $objOfficeShift->getOfficeStartTime(), 'H:i:s-07' );
		}

		$this->setEmployeeId( $intEmployeeId );
		$this->setOfficeShiftId( $intOfficeShiftId );
		$this->setOfficeStartTime( $strOfficeStartTime );
		$this->setStartDate( $strStartDate );
		$this->setEndDate( $strEndDate );
		$this->setShiftRequestType( self::SHIFT_CHANGE );

		if( true == $boolApprove ) {
			$this->setApprovedBy( $intUserId );
			$this->setApprovedOn( 'now()' );
		}
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getOfficeStartTimeLabel() {
		return $this->m_strOfficeStartTimeLabel;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['preferred_name'] ) ) $this->setPreferredName( $arrmixValues['preferred_name'] );
		if( true == isset( $arrmixValues['department_name'] ) ) $this->setDepartmentName( $arrmixValues['department_name'] );
		if( true == isset( $arrmixValues['office_start_time_label'] ) ) $this->setOfficeStartTimeLabel( $arrmixValues['office_start_time_label'] );
		return;
	}

	public function setPreferredName( $strPreferredName ) {
		$this->m_strPreferredName = $strPreferredName;
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	public function setOfficeStartTimeLabel( $strOfficeStartTime ) {
		$this->m_strOfficeStartTimeLabel = $strOfficeStartTime;
	}

	public function valOfficeShiftId() {
		$boolValid = true;

		if( false == is_numeric( $this->getOfficeShiftId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'office_shift_id', 'Office shift is required.' ) );
		}

		return $boolValid;
	}

	public function valStartDate( $boolDuplicateRecord, $boolIsQamOrAdd, $boolWeekendSupport, $objAdminDatabase ) {
		$boolValid = true;
		if( true == is_null( $this->getStartDate() ) && false == $boolIsQamOrAdd && self::RELEASE_SUPPORT != $this->getShiftRequestType() ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Begin date is required.' ) );
		} elseif( false == is_null( $this->getStartDate() ) && false == CValidation::validateDate( $this->getStartDate() ) && false == $boolIsQamOrAdd ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Begin Date is not valid date.' ) );
		} elseif( true == $boolWeekendSupport ) {
			$arrintWeekends		= [ self::DAY_OF_WEEK_SUNDAY, self::DAY_OF_WEEK_SATURDAY ];
			$arrstrPaidHolidays = CPaidHolidays::fetchPaidHolidaysDateByCountryCode( CCountry::CODE_INDIA, $objAdminDatabase );
			if( false == is_null( $this->getStartDate() ) && false == in_array( date( 'N', strtotime( $this->getStartDate() ) ), $arrintWeekends ) && false == in_array( $this->getStartDate(), $arrstrPaidHolidays ) ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Weekend support is applicable only on sat/ sun/ paid holiday.' ) );
			}
		}

		if( true == $boolDuplicateRecord ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Weekend support already added for this employee on selected date.' ) );
		}
		if( self::RELEASE_SUPPORT == $this->getShiftRequestType() ) {
			if( true == is_null( $this->getStartDate() ) ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Release date is required.' ) );
			}
		}
		if( false == $boolWeekendSupport && true == is_numeric( $this->getEmployeeId() ) && true == valStr( $this->getStartDate() ) && true == is_numeric( $this->getOfficeShiftId() ) ) {
			$strSupportType = ( self::RELEASE_SUPPORT == $this->getShiftRequestType() ) ? self::RELEASE_SUPPORT : self::SHIFT_CHANGE;
			$arrobjOfficeShiftEmployee = rekeyObjects( 'EmployeeId', ( array ) \Psi\Eos\Admin\COfficeShiftEmployees::createService()->fetchOfficeShiftEmployeesByEmployeeIdsByDate( ( array ) $this->getEmployeeId(), $this->getStartDate(), $strSupportType, $objAdminDatabase, $this->getId(), false, true ) );

			if( self::RELEASE_SUPPORT == $this->getShiftRequestType() && true == valArr( $arrobjOfficeShiftEmployee ) && $this->getStartDate() == $arrobjOfficeShiftEmployee[$this->getEmployeeId()]->getStartDate() ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Release Support Request already added for selected date.' ) );
			} else {
				if( true == valObj( $arrobjOfficeShiftEmployee, 'COfficeShiftEmployee' ) ) {
					$boolValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Office Shift Request already added for selected date.' ) );
				}
			}
		}

		return $boolValid;
	}

	public function valEndDate() {
		$boolValid = true;

		if( false == is_null( $this->getStartDate() ) && false == is_null( $this->getEndDate() ) ) {
			if( strtotime( $this->getStartDate() ) > strtotime( $this->getEndDate() ) ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'End date should be after start date.' ) );
			}
		}

		return $boolValid;
	}

	public function valPsProductId( $boolWeekendSupport ) {
		$boolValid = true;

		if( true == $boolWeekendSupport && false == is_numeric( $this->getPsProductId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Please select product.' ) );
		}

		return $boolValid;
	}

	public function valEmployeeId() {
		$boolValid = true;

		if( false == is_numeric( $this->getEmployeeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Please select employee.' ) );
		}
		return $boolValid;
	}

	public function valReason() {
		$boolValid = true;

		if( true == is_null( $this->getReason() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reason', 'Reason is required.' ) );
		}
		return $boolValid;
	}

	public function valRequest( $objAdminDatabase ) {
		$boolValid = true;

			$intEmployeeId = $this->getEmployeeId();
			if( true == valStr( $this->getStartDate() ) && true == valId( $intEmployeeId ) ) {
				$objOldOfficeShiftEmployee = \Psi\Eos\Admin\COfficeShiftEmployees::createService()->fetchOfficeShiftEmployeesByEmployeeIdsByDate( [ $intEmployeeId ],  $this->getStartDate(), COfficeShiftEmployee::SHIFT_CHANGE, $objAdminDatabase, NULL, false, false, true );
			}
			if( true == valObj( $objOldOfficeShiftEmployee, 'COfficeShiftEmployee' ) && $this->getId() != $objOldOfficeShiftEmployee->getId() ) {
				$boolValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Same shift request is already added for selected date.' ) );
			}

		return $boolValid;
	}

	public function validate( $strAction, $objAdminDatabase, $boolDuplicateRecord =false, $boolIsQamOrAdd = false, $boolWeekendSupport = false, $boolShiftRequest = false, $boolWeekendWorkingRequest = false ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
					$boolValid &= $this->valPsProductId( $boolWeekendSupport );
					$boolValid &= $this->valEmployeeId();
					if( false == $boolWeekendWorkingRequest ) {
						$boolValid &= $this->valStartDate( $boolDuplicateRecord, $boolIsQamOrAdd, $boolWeekendSupport, $objAdminDatabase );
					}
					$boolValid &= $this->valOfficeShiftId();
					if( self::RELEASE_SUPPORT == $this->getShiftRequestType() ) {
						$boolValid &= $this->valReason();
					}
					if( true == $boolShiftRequest ) {
						$boolValid &= $this->valRequest( $objAdminDatabase );
					}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				break;
		}

		return $boolValid;
	}

	public function loadOfficeShiftCalendarData( $arrintEmployeeIds, $strBeginDate, $strEndDate, $objAdminDatabase, $arrobjOfficeShiftEmployees = NULL ) {

		$arrmixCalendarData			= array();
		$objEmployeeWeekend			= new CEmployeeWeekend();
		$arrmixEmployeesWeekends	= $objEmployeeWeekend->loadEmployeeWeekends( date( 'Y', strtotime( $strEndDate ) ), $objAdminDatabase, $arrintEmployeeIds, $boolFromOfficeShiftCalendar = true );

		$arrmixEmployeeVacationRequests		= ( array ) rekeyArray( 'employee_id', CEmployeeVacationRequests::fetchDetailEmployeeVacationRequestsByEmployeeIdsByDateRange( $arrintEmployeeIds, $strBeginDate, $strEndDate, CCountry::CODE_INDIA, $objAdminDatabase, NULL, false ), false, true );

		for( $intDate = strtotime( $strBeginDate ); ( $intDate <= strtotime( $strEndDate ) ); $intDate = strtotime( '+1 day', $intDate ) ) {
			$strDate = date( 'm/d/Y', $intDate );

			if( true == valArr( $arrintEmployeeIds ) ) {
				foreach( $arrintEmployeeIds as $intEmployeeId ) {
					if( true == valArr( $arrobjOfficeShiftEmployees[$intEmployeeId] ) ) {
						foreach( $arrobjOfficeShiftEmployees[$intEmployeeId] as $objOfficeShiftEmployee ) {
							if( true == valObj( $objOfficeShiftEmployee, 'COfficeShiftEmployee' ) && $intDate >= strtotime( $objOfficeShiftEmployee->getStartDate() ) && ( true == is_null( $objOfficeShiftEmployee->getEndDate() ) || $intDate <= strtotime( $objOfficeShiftEmployee->getEndDate() ) ) ) {
								if( false == in_array( date( 'w', $intDate ), [ CEmployeeVacationRequest::DAY_OF_WEEK_SATURDAY, CEmployeeVacationRequest::DAY_OF_WEEK_SUNDAY ] ) || ( true == valArr( $arrmixEmployeesWeekends[$intEmployeeId] ) && true == in_array( date( 'm/d/Y', $intDate ), ( array ) $arrmixEmployeesWeekends[$intEmployeeId]['swap_days'] ) ) ) {
									$arrmixCalendarData[$strDate][$intEmployeeId]['office_shift_id'] = $objOfficeShiftEmployee->getOfficeShiftId();
								}
							}
						}
					} else {
						$arrmixCalendarData[$strDate][$intEmployeeId]['office_shift_id'] = COfficeShift::DEFAULT_OFFICE_SHIFT;
					}

					if( ( true == valArr( $arrmixEmployeesWeekends[$intEmployeeId] ) && true == in_array( $strDate, ( array ) $arrmixEmployeesWeekends[$intEmployeeId]['weekends'] ) ) || ( true == in_array( date( 'w', $intDate ), [ CEmployeeVacationRequest::DAY_OF_WEEK_SATURDAY, CEmployeeVacationRequest::DAY_OF_WEEK_SUNDAY ] ) && ( false == valArr( $arrmixEmployeesWeekends[$intEmployeeId] ) || ( true == valArr( $arrmixEmployeesWeekends[$intEmployeeId] ) && false == in_array( $strDate, ( array ) $arrmixEmployeesWeekends[$intEmployeeId]['swap_days'] ) ) ) ) ) {
						$arrmixCalendarData[$strDate][$intEmployeeId]['office_shift_id'] = self::WEEKEND;
					}

					if( true == valArr( $arrmixEmployeeVacationRequests[$intEmployeeId] ) ) {
						foreach( $arrmixEmployeeVacationRequests[$intEmployeeId] as $arrmixEmployeeVacationRequest ) {
							if( strtotime( $strDate ) == strtotime( $arrmixEmployeeVacationRequest['date'] ) ) {
								$arrmixCalendarData[$strDate][$intEmployeeId]['employee_on_leave'] = true;
							}
						}
					}

				}
			}
		}

		return $arrmixCalendarData;
	}

	public function sendEmailNotification( $objUser, $objDatabase, $objEmailDatabase, $boolUpdate = false, $boolResponse = false, $boolFromOfficeShiftsCalendar = false, $boolAddedByManager = false ) {

		$objEmployee			= CEmployees::fetchEmployeeById( $this->getEmployeeId(), $objDatabase );
		$arrobjManagerEmployees = rekeyObjects( 'Id', ( array ) $objEmployee->fetchManagerEmployees( $objDatabase ) );
		$objOfficeShift			= COfficeShifts::fetchOfficeShiftById( $this->getOfficeShiftId(), $objDatabase );
		$objAddEmployee			= CEmployees::fetchAddEmployeeByEmployeeId( $this->getEmployeeId(), $objDatabase );

		if( CDepartment::QA == $objEmployee->getDepartmentId() ) {
			$objQamEmployee			= CEmployees::fetchQamEmployeeByEmployeeId( $this->getEmployeeId(), $objDatabase );
		} else {
			$objTpmEmployee			= CEmployees::fetchTpmEmployeeByEmployeeId( $this->getEmployeeId(), $objDatabase );
		}

		if( false == is_null( getArrayElementByKey( CEmployee::ID_DAVID_BATEMEN, $arrobjManagerEmployees ) ) ) {
			unset( $arrobjManagerEmployees[CEmployee::ID_DAVID_BATEMEN] );
		}

		if( 2 < \Psi\Libraries\UtilFunctions\count( $arrobjManagerEmployees ) && false == is_null( getArrayElementByKey( CEmployee::ID_PREETAM_YADAV, $arrobjManagerEmployees ) ) ) {
			unset( $arrobjManagerEmployees[CEmployee::ID_PREETAM_YADAV] );
		}

		if( 2 < \Psi\Libraries\UtilFunctions\count( $arrobjManagerEmployees ) ) {
			$arrobjManagerEmployees = array_slice( $arrobjManagerEmployees, \Psi\Libraries\UtilFunctions\count( $arrobjManagerEmployees ) - 3 );
		}

		$arrstrToEmailAddresses	= array();

		if( $this->getShiftRequestType() == self::RELEASE_SUPPORT ) {
			if( CCountry::CODE_INDIA == $objUser->getEmployee()->getCountryCode() ) {
				$arrstrHrGroupEmailAddresses = array( CSystemEmail::XENTO_HR_EMAIL_ADDRESS );
			}

			if( true == valObj( $objAddEmployee, 'CEmployee' ) ) {
				$arrstrToEmailAddresses = array_merge( $arrstrToEmailAddresses, [ $objAddEmployee->getEmailAddress() ] );
			}
			if( true == valObj( $objQamEmployee, 'CEmployee' ) ) {
				$arrstrToEmailAddresses = array_merge( $arrstrToEmailAddresses, [ $objQamEmployee->getEmailAddress() ] );
			}
			if( true == valObj( $objTpmEmployee, 'CEmployee' ) ) {
				$arrstrToEmailAddresses = array_merge( $arrstrToEmailAddresses, [ $objTpmEmployee->getEmailAddress() ] );
			}
			if( true == valArr( $arrstrToEmailAddresses ) && true == valArr( $arrstrHrGroupEmailAddresses ) ) {
				$arrstrToEmailAddresses = array_merge( $arrstrToEmailAddresses, $arrstrHrGroupEmailAddresses );
			}
		}
		if( true == valArr( $arrobjManagerEmployees ) ) {
			foreach( $arrobjManagerEmployees as $objManagerEmployee ) {
				if( true == valObj( $objManagerEmployee, 'CEmployee' ) )
					$arrstrToEmailAddresses[$objManagerEmployee->getId()] = $objManagerEmployee->getEmailAddress();
			}
		}
		// Hard coded constants for Neeta Neti and Sandy - adding their emails in notification of office shift change.
		if( $this->getShiftRequestType() != self::RELEASE_SUPPORT ) {
			if( $objUser->getEmployee()->getId() == CEmployee::ID_SANDEEP_GARUD ) {
				$arrstrToEmailAddresses[CEmployee::ID_SANDEEP_GARUD] = CSystemEmail::XENTO_SGARUD_EMAIL_ADDRESS;
			}
			if( $objUser->getEmployee()->getId() == CEmployee::ID_NEETA_NETI ) {
				$arrstrToEmailAddresses[CEmployee::ID_NEETA_NETI] = CSystemEmail::XENTO_NNETI_EMAIL_ADDRESS;
			}
		}
		$arrstrToEmailAddresses		= array_unique( $arrstrToEmailAddresses );

		if( true == $boolAddedByManager ) {
			$strSubject = ( $this->getShiftRequestType() == self::RELEASE_SUPPORT )? 'Release Support Request Changed' : 'Office shift changed';
		} else {
			if( true == $boolResponse ) {
				if( true == $boolFromOfficeShiftsCalendar ) {
					$strSubject = 'Approved:Office Shift Requests';
				} else {
					if( true == is_numeric( $this->getDeletedBy() ) ) {
						$strStatus = 'Deleted: ';
					} else {
						$strStatus = ( false == is_null( $this->getApprovedBy() ) ) ? 'Approved: ' : 'Denied: ';
					}
					$strSubject = $strStatus . ( ( $this->getShiftRequestType() == self::RELEASE_SUPPORT )? 'Release Support Request': 'Office shift requests' );
				}
			} else {
				if( true == $boolUpdate ) {
					$strSubject = ( $this->getShiftRequestType() == self::RELEASE_SUPPORT )? 'Update: Release Support Request' : 'Update: Office Shift Requests';
				} else {
					$strSubject = ( $this->getShiftRequestType() == self::RELEASE_SUPPORT )? 'Release Support Request': 'Office shift requests';
				}
			}
		}
		$arrstrApproveOrDeniedBy = NULL;
		if( $this->getShiftRequestType() == self::RELEASE_SUPPORT ) {
			$arrstrApproveOrDeniedBy = CEmployees::fetchEmployeeNameByOfficeShiftEmployeesIds( [ $this->getId() ], $objDatabase );
		}
		$objManagerEmployee = CEmployees::fetchEmployeeById( ( false == is_null( $this->getApprovedBy() ) ) ? $this->getApprovedBy() : $this->getDeniedBy(), $objDatabase );

		$strHtmlEmailContent	= $this->buildHtmlVacationRequestEmailContent( $objEmployee, $objManagerEmployee, $objUser->getEmployee()->getPreferredName(), $objOfficeShift, $this, $boolUpdate, $boolResponse, $boolAddedByManager, $arrstrApproveOrDeniedBy );

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, $strSubject, addslashes( $strHtmlEmailContent ), implode( ',', $arrstrToEmailAddresses ) );

		switch( NULL ) {
			default:

				if( false == $objSystemEmail->insert( $objUser->getId(), $objEmailDatabase ) ) {
					break;
				}
				$objEmailDatabase->commit();
				return true;
		}
		return false;
	}

	public function buildHtmlVacationRequestEmailContent( $objEmployee, $objManagerEmployee, $strUserName, $objOfficeShift, $objOfficeShiftEmployee, $boolUpdate, $boolResponse, $boolAddedByManager, $arrstrApproveOrDeniedBy ) {

		require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

		$objSmarty = new CPsSmarty( PATH_INTERFACES_ADMIN, false );
		if( COfficeShiftEmployee::RELEASE_SUPPORT == $this->getShiftRequestType() ) {
			$objSmarty->assign( 'approved_denied_by', $arrstrApproveOrDeniedBy[$this->getId()] );
		} else {
			$objSmarty->assign( 'approved_denied_by', ( true == valObj( $objManagerEmployee, 'CEmployee' ) ) ? $objManagerEmployee->getPreferredName() : NULL );
		}

		$objSmarty->assign( 'office_shift_type', $objOfficeShift->getName() );
		$objSmarty->assign( 'office_shift_employee', $objOfficeShiftEmployee );
		$objSmarty->assign( 'is_update', $boolUpdate );
		$objSmarty->assign( 'is_response', $boolResponse );
		$objSmarty->assign( 'support_type', $this->getShiftRequestType() );
		$objSmarty->assign( 'release_support', COfficeShiftEmployee::RELEASE_SUPPORT );

		$objSmarty->assign( 'is_added_by_manager', $boolAddedByManager );
		$objSmarty->assign( 'current_user_name', $strUserName );
		$objSmarty->assign( 'employee_name', $objEmployee->getPreferredName() );
		$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );

		return $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration\office_shifts_calendar\india_office_shift_request_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
	}

}
?>