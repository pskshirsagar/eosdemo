<?php

class CPsProductRelationship extends CBasePsProductRelationship {

	/**
	 * Validate Functions
	 *
	 */

	public function valPercentWeight() {
		$boolIsValid = true;

		if( true == is_null( $this->getPercentWeight() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'percent_weight', 'Percent weight amount is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valPercentWeight();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

	public function createClone( $intUserId, $intProductBundleId = NULL ) {
		$objPsProductRelationship = clone $this;

		$objPsProductRelationship->setId( NULL );
		$objPsProductRelationship->setBundlePsProductId( $intProductBundleId );
		$objPsProductRelationship->setCreatedBy( $intUserId );
		$objPsProductRelationship->setCreatedOn( 'NOW()' );
		$objPsProductRelationship->setUpdatedBy( $intUserId );
		$objPsProductRelationship->setUpdatedOn( 'NOW()' );

		return $objPsProductRelationship;
	}

}
?>