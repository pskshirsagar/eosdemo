<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationInterviews
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationInterviews extends CBaseEmployeeApplicationInterviews {

	public static function fetchInterviewTypeIdsByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		$strSql = 'SELECT
						*
				  FROM
						employee_application_interviews
				  WHERE
						employee_application_id = ' . ( int ) $intEmployeeApplicationId;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedInterviewDetailsCountByEmployeeId( $intEmployeeId, $boolIsInterviewed = false, $objDatabase, $strUserCountryCode = CCountry::CODE_USA ) {

		$strSql = 'SELECT
						count( * )
					FROM
						employee_application_interviews as eai
						LEFT JOIN employee_applications ea ON ( ea.id = eai.employee_application_id )
						LEFT JOIN ps_website_job_postings as ps ON ( ea.ps_website_job_posting_id = ps.id )
						LEFT JOIN employee_application_details ead ON ( ead.employee_application_id = eai.employee_application_id )
						LEFT JOIN employee_application_status_types east ON ( ea.employee_application_status_type_id = east.id )
						LEFT JOIN ps_job_posting_step_statuses pjpss ON ( pjpss.id = ea.ps_job_posting_step_status_id )
						LEFT JOIN ps_job_posting_steps pjps ON ( pjps.id = pjpss.ps_job_posting_step_id )
						LEFT JOIN ps_job_posting_steps pjpstp ON ( pjpstp.id = eai.ps_job_posting_step_id )
						LEFT JOIN ps_job_posting_statuses psstat ON ( psstat.id=pjpss.ps_job_posting_status_id )
					WHERE
						eai.employee_id =' . ( int ) $intEmployeeId . '
							AND ea.employee_application_status_type_id != ' . CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_NO_SHOW;

				if( false == $boolIsInterviewed ) {
					if( CCountry::CODE_INDIA == $strUserCountryCode ) {
						$strSql .= ' AND eai.interviewed_on IS NULL AND ea.rejected_on IS NULL AND eai.scheduled_on::Date >= \'' . date( 'Y-m-d' ) . '\'';
					} else {
						$strSql .= ' AND eai.interviewed_on IS NULL AND ea.rejected_on IS NULL AND eai.scheduled_on::date >= now()::date';
					}
				} else {
					$strSql .= ' AND eai.interviewed_on IS NOT NULL';
				}

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return $arrintResponse[0]['count'];
	}

	public static function fetchEmployeeApplicationInterviewsByIds( $arrintEmployeeApplicationInterviewIds, $objAdminDatabase ) {
		if( false == valArr( $arrintEmployeeApplicationInterviewIds ) ) return NULL;
		return self::fetchEmployeeApplicationInterviews( 'SELECT * FROM employee_application_interviews WHERE id IN( ' . implode( ',', $arrintEmployeeApplicationInterviewIds ) . ' ) ', $objAdminDatabase );
	}

	public static function fetchEmployeeApplicationInterviewersCountByIdByEmployeeId( $intId, $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
					    count( 1 ) as count
					FROM
					    employee_application_interviews eai
					    JOIN employee_application_interviews eai2 ON eai2.employee_application_id = eai.employee_application_id AND eai.id != eai2.id AND eai2.employee_id = ' . ( int ) $intEmployeeId . '
					WHERE
						eai.id = ' . ( int ) $intId;

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixData ) && true == isset( $arrmixData[0]['count'] ) ) {
			return ( int ) $arrmixData[0]['count'];
		} else {
			return 0;
		}
	}

	public static function fetchEmployeeApplicationInterviewsByEmployeeApplicationIds( $arrintEmployeeApplicationIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeApplicationIds ) ) return NULL;

		$strSql = 'SELECT * FROM employee_application_interviews WHERE employee_application_id IN ( ' . implode( ',', $arrintEmployeeApplicationIds ) . ')';

		return self::fetchEmployeeApplicationInterviews( $strSql, $objDatabase );
	}

	public static function fetchEmployeeApplicationInterviewByIdsByEmployeeId( $arrintEmployeeApplicationInterviewIds, $intEmployeeId, $objDatabase ) {

		if( false == valArr( $arrintEmployeeApplicationInterviewIds ) || true == empty( $intEmployeeId ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						employee_application_interviews
					WHERE
						id IN (' . implode( ',', $arrintEmployeeApplicationInterviewIds ) . ')
						AND employee_id = ' . ( int ) $intEmployeeId;

		return self::fetchEmployeeApplicationInterviews( $strSql, $objDatabase );
	}

	public static function fetchCountOfInterviewsTakenAndEmployeesJoinedByEmployeeId( $intEmployeeId, $arrintEmployeeIds, $strFromDate, $strToDate, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		if( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) {
			$strWhereClause = ' AND eai.interviewed_on > \'' . $strFromDate . '\' and eai.interviewed_on < \'' . $strToDate . '\'';
		} else {
			$strWhereClause = '';
		}

		$strSql = ' SELECT * FROM (
					SELECT
							eai.employee_id,
							count(eai.id) AS interviews_taken,
							dense_rank() over(ORDER BY count(eai.id) DESC) AS interviews_taken_rank,
							count(ea.employee_id) AS employees_joined,
							dense_rank() over(ORDER BY count(ea.employee_id) DESC) AS employees_joined_rank
					FROM employee_application_interviews eai
					LEFT JOIN employee_applications ea ON (ea.id = eai.employee_application_id AND ea.employee_id IS NOT NULL AND ea.employee_application_status_type_id = 12)
					WHERE eai.employee_id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' ) AND
						 eai.interviewed_on IS NOT NULL ' . $strWhereClause . '
					GROUP BY eai.employee_id
					) main WHERE main.employee_id = ' . ( int ) $intEmployeeId;

		return reset( fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchInterviewerIdByEmployeeApplicationIdByEmployeeIdByJobPostingStepName( $intEmployeeApplicationId, $intEmployeeId, $objDatabase ) {

		if( true == is_null( $intEmployeeApplicationId ) || true == is_null( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						eai.employee_id
				   FROM
						employee_application_interviews eai
						JOIN ps_job_posting_steps pjps on ( pjps.id = eai.ps_job_posting_step_id )
					WHERE
						eai.employee_application_id =' . ( int ) $intEmployeeApplicationId . '
						AND eai.employee_id = ' . ( int ) $intEmployeeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeesInterviewScheduledByEmployeeIdsByDate( $arrintEmployeeApplicationInterviewIds, $strScheduledOn, $intEmployeeApplicationId, $objDatabase ) {

		if( false == valArr( $arrintEmployeeApplicationInterviewIds ) || false == valStr( $strScheduledOn ) ) return NULL;

		$strSql = 'SELECT
					DISTINCT ( eai.employee_id ) ,
						eai.scheduled_on,
						e.preferred_name as employee_name
					FROM
						employee_application_interviews eai
						LEFT JOIN employees e ON ( eai.employee_id = e.id )
					WHERE
						eai.employee_id IN (' . implode( ',', $arrintEmployeeApplicationInterviewIds ) . ')
							AND eai.scheduled_on = \'' . $strScheduledOn . '\'
						AND eai . employee_application_id <> ' . ( int ) $intEmployeeApplicationId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeApplicationInterviewBySurveyIdBySurveyTypeId( $intSurveyId, $intSurveyTypeId, $objDatabase ) {

		if( false == valId( $intSurveyId ) || false == valId( $intSurveyTypeId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						employee_application_interviews eai
					JOIN surveys s ON ( s.trigger_reference_id = eai.id AND s.id = ' . ( int ) $intSurveyId . ' AND s.survey_type_id = ' . ( int ) $intSurveyTypeId . ' ) ';

		return self::fetchEmployeeApplicationInterview( $strSql, $objDatabase );
	}

	public static function fetchEmployeeApplicationInterviewsByCurrentDate( $objDatabase ) {

		$strSql = 'SELECT
						eai.*,
						ea.country_code as employee_application_country_code,
						interviewer.email_address as interviewer_email_address,
						interviewer.preferred_name as interviewer_name,
						recruiter.name_first as recruiter_name
					FROM
					    employee_application_interviews eai
					    JOIN employee_applications ea on ( eai.employee_application_id = ea.id )
						JOIN employees interviewer on ( eai.employee_id = interviewer.id )
						JOIN employees recruiter on ( ea.recruiter_employee_id = recruiter.id )
					WHERE 
					eai.feedback = \'NULL\' or eai.feedback IS NULL';

		return self::fetchEmployeeApplicationInterviews( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedScheduledDetailsByStartDateEndDate( $strStartDate, $strEndDate, $intRecruiterId, $intPageNo, $intPageSize, $objDatabase, $boolCount = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;
		$strWhere = ( true == valId( $intRecruiterId ) ) ? '  AND sq.recruiter_employee_id = ' . ( int ) $intRecruiterId : '';

		$strSql = ' SELECT sq.*,
                            e1.preferred_name AS recruiter_name
                    FROM 
                       ( SELECT eai.scheduled_on::DATE,
                                ea.id,
								ea.name_first,
								ea.name_last,
								ea.employee_application_status_type_id,
								CASE WHEN pjps.name IN ( \'' . CPsJObPostingStep::APPLIED . '\', \'' . CPsJObPostingStep::REFERENCES . '\' )  THEN 0 ELSE 1 END as turn_up_status,
								CONCAT( pjps.name,\' \',psstat.name ) AS current_step_status_name,
								CASE WHEN ea.recruiter_employee_id IS NOT NULL THEN ea.recruiter_employee_id
								ELSE a.employee_id END AS recruiter_employee_id,
								rank() OVER( PARTITION BY a.secondary_reference,ea.id ORDER BY a.created_on DESC,eai.id ASC)
						FROM
								employee_application_interviews AS eai
								LEFT JOIN employee_applications AS ea ON ( eai.employee_application_id  = ea.id )
								LEFT JOIN employee_application_status_types AS east ON (  ea.employee_application_status_type_id = east.id )
								LEFT JOIN ps_job_posting_step_statuses AS pjpss ON (pjpss.id = ea.ps_job_posting_step_status_id)
								LEFT JOIN ps_job_posting_steps AS pjps ON (pjps.id =  pjpss.ps_job_posting_step_id)
								LEFT JOIN ps_job_posting_steps AS pjps1 ON (pjps1.id =  eai.ps_job_posting_step_id)
								LEFT JOIN ps_job_posting_statuses AS psstat ON (psstat.id =  pjpss.ps_job_posting_status_id)
								LEFT JOIN  actions AS a ON ( ea.id = a.secondary_reference  AND a.action_result_id = 108 AND a.action_type_id = 1 AND a.employee_id IS NOT NULL )
								LEFT JOIN employees e ON ( e.id = a.employee_id)
						WHERE   pjps1.name = \'' . CPsJObPostingStep::FIRST_INTERVIEW . '\'
								AND eai.scheduled_on::DATE  BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
								AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'
						) sq
						LEFT JOIN employees AS e1 ON ( sq.recruiter_employee_id = e1.id )
				WHERE sq.rank = 1 ' . $strWhere . '
				ORDER BY sq.scheduled_on';

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) && false == $boolCount ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedDailyPerformanceTrackingDetailsByStartDateEndDate( $strStartDate, $strEndDate, $intRecruiterId, $intPageNo, $intPageSize, $objDatabase, $boolCount = false ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;
		$strWhere = ( true == valId( $intRecruiterId ) ) ? '  AND sq.recruiter_employee_id = ' . ( int ) $intRecruiterId : '';

		$strSql = ' SELECT sq.scheduled_date,
							sq.day,
							sum ( sq.scheduled_count ) AS scheduled_count,
							sum ( sq.turn_up_count ) AS turn_up_count,
							sum ( sq.select_count ) AS select_count,
							e1.preferred_name AS recruiter_name
                    FROM 
                        (SELECT to_char( eai.scheduled_on, \'DD-MM-YYYY\' ) AS scheduled_date,
                                to_char( eai.scheduled_on::DATE, \'Day\') as day,
								count ( eai.employee_application_id ) AS scheduled_count,
								count ( CASE
								WHEN pjps.name NOT IN ( \'' . CPsJObPostingStep::APPLIED . '\', \'' . CPsJObPostingStep::REFERENCES . '\' ) THEN ea.id
								END ) AS turn_up_count,
								count ( CASE
								WHEN ( ( pjps.name IN ( \'' . CPsJObPostingStep::OFFER_EXTENDED . '\',\'' . CPsJObPostingStep::OFFER_ACCEPTED . '\' ) ) OR east.id = ' . CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_JOINED . '  OR ( pjps.name = \'' . CPsJobPostingStep::HR_INTERVIEW . '\' AND psstat.id =  ' . CPsJobPostingStatus::SELECTED . ' )) AND east.id != ' . CEmployeeApplicationStatusType::EMPLOYEE_APPLICATION_TYPE_OFFER_DECLINED . ' THEN ea.id
								END ) AS select_count,
								CASE WHEN ea.recruiter_employee_id IS NOT NULL THEN ea.recruiter_employee_id
								ELSE a.employee_id END AS recruiter_employee_id,
								rank() OVER( PARTITION BY a.secondary_reference,ea.id ORDER BY a.created_on DESC,eai.id ASC)
						FROM
							employee_application_interviews AS eai
								LEFT JOIN employee_applications AS ea ON ( eai.employee_application_id  = ea.id )
								LEFT JOIN employee_application_status_types AS east ON (  ea.employee_application_status_type_id = east.id )
								LEFT JOIN ps_job_posting_step_statuses AS pjpss ON (pjpss.id = ea.ps_job_posting_step_status_id)
								LEFT JOIN ps_job_posting_steps AS pjps ON (pjps.id =  pjpss.ps_job_posting_step_id)
								LEFT JOIN ps_job_posting_steps AS pjps1 ON (pjps1.id =  eai.ps_job_posting_step_id)
								LEFT JOIN ps_job_posting_statuses AS psstat ON (psstat.id =  pjpss.ps_job_posting_status_id)
								LEFT JOIN  actions AS a ON ( ea.id = a.secondary_reference  AND a.action_result_id = 108 AND a.action_type_id = 1 AND a.employee_id IS NOT NULL )
								LEFT JOIN employees e ON ( e.id = a.employee_id)
						WHERE   pjps1.name = \'' . CPsJObPostingStep::FIRST_INTERVIEW . '\'
								AND eai.scheduled_on::DATE  BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'
								AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'
						GROUP BY
								eai.scheduled_on,
								ea.id,
								eai.id,
								a.employee_id,
								a.created_on,
								a.secondary_reference,
								recruiter_employee_id
						ORDER BY
								to_char( eai.scheduled_on, \'DD-MM-YYYY\' )
						) sq
						LEFT JOIN employees AS e1 ON ( sq.recruiter_employee_id = e1.id )
				WHERE sq.rank = 1 ' . $strWhere . '
				GROUP BY 
					sq.scheduled_date,
					sq.day,
					e1.preferred_name';

			if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) && false == $boolCount ) {
				$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
			}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedMonthlyPerformanceTrackingDetailsByStartDateEndDate( $strStartDate, $strEndDate, $intRecruiterId, $objDatabase, $intPageNo = NULL, $intPageSize = NULL ) {
		if( false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			return NULL;
		}

		$strOffsetClause	= NULL;
		$strWhereClause		= ( true == valId( $intRecruiterId ) ) ? '  AND sq.recruiter_employee_id = ' . ( int ) $intRecruiterId : '';

		if( true == valId( $intPageNo ) && true == valId( $intPageSize ) ) {
			$intOffset			= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit			= ( int ) $intPageSize;
			$strOffsetClause	= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$strSql = 'SELECT sq.scheduled_on,
						sum ( sq.scheduled_count ) AS scheduled_count,
						sum ( sq.turn_up_count ) AS turn_up_count,
						sum ( sq.select_count ) AS select_count,
						e1.preferred_name AS recruiter_name
					FROM (
						SELECT to_char(eai.scheduled_on, \'MM-YYYY\') as scheduled_on,
							count(eai.employee_application_id) AS scheduled_count,
							count(CASE
							WHEN pjps.name NOT IN ( \'' . CPsJObPostingStep::APPLIED . '\', \'' . CPsJObPostingStep::REFERENCES . '\' ) THEN ea.id
							END) AS turn_up_count,
							count(CASE
							WHEN ((pjps.name IN (\'' . CPsJObPostingStep::OFFER_EXTENDED . '\', \'' . CPsJObPostingStep::OFFER_ACCEPTED . '\'))
								OR east.id = 12 OR (pjps.name = \'' . CPsJObPostingStep::HR_INTERVIEW . '\' AND
								psstat.id = 4)) AND east.id != 51 THEN ea.id
							END) AS select_count,
						CASE
							WHEN ea.recruiter_employee_id IS NOT NULL THEN
								ea.recruiter_employee_id
								ELSE a.employee_id
								END AS recruiter_employee_id,
								rank() OVER(PARTITION BY a.secondary_reference, ea.id ORDER BY a.created_on,eai.id DESC)
						FROM employee_application_interviews AS eai
							LEFT JOIN employee_applications AS ea ON (eai.employee_application_id = ea.id)
							LEFT JOIN employee_application_status_types AS east ON (ea.employee_application_status_type_id = east.id)
							LEFT JOIN ps_job_posting_step_statuses AS pjpss ON (pjpss.id = ea.ps_job_posting_step_status_id)
							LEFT JOIN ps_job_posting_steps AS pjps ON (pjps.id = pjpss.ps_job_posting_step_id)
							LEFT JOIN ps_job_posting_steps AS pjps1 ON (pjps1.id = eai.ps_job_posting_step_id)
							LEFT JOIN ps_job_posting_statuses AS psstat ON (psstat.id = pjpss.ps_job_posting_status_id)
							LEFT JOIN actions AS a ON (ea.id = a.secondary_reference AND a.action_result_id = ' . CActionResult::EMPLOYEE_APPLICATION . ' AND a.action_type_id = ' . CActionType::NOTE . ' AND a.employee_id IS NOT NULL)
							LEFT JOIN employees e ON (e.id = a.employee_id)
						WHERE pjps1.name = \'' . CPsJObPostingStep::FIRST_INTERVIEW . '\' 
							AND eai.scheduled_on::DATE  BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\' 
							AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'
							GROUP BY ea.recruiter_employee_id,
							a.employee_id,
							a.secondary_reference,
							ea.id,
							eai.scheduled_on,
							a.created_on,
							eai.id
						) sq
						LEFT JOIN employees AS e1 ON (sq.recruiter_employee_id = e1.id)
					WHERE 
						sq.rank = 1 ' . $strWhereClause . ' 

					GROUP BY  recruiter_name, sq.scheduled_on' . $strOffsetClause;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllInterviewersWithTypeByFilter( $arrmixFilter, $objDatabase ) {
		if( false == valStr( $arrmixFilter['start_date'] ) || false == valStr( $arrmixFilter['end_date'] ) ) {
			return false;
		}

		$strDateRange	= ( false == valStr( $arrmixFilter['drive_day'] ) ) ? '\'' . $arrmixFilter['start_date'] . '\'::DATE AND \'' . $arrmixFilter['end_date'] . '\'::DATE' : ' \'' . $arrmixFilter['drive_day'] . '\'::DATE  - INTERVAL \'1 MONTH\' AND \'' . $arrmixFilter['drive_day'] . '\'::DATE';

		$strSql = 'SELECT
						array_agg( case when pjps.name = \'' . CPsJobPostingStep::FIRST_INTERVIEW . '\' then eai.employee_id end ) as first_interviewer,
						array_agg( case when pjps.name = \'' . CPsJobPostingStep::SECOND_INTERVIEW . '\' then eai.employee_id end ) as second_interviewer,
						array_agg( case when pjps.name = \'' . CPsJobPostingStep::HR_INTERVIEW . '\' then eai.employee_id end ) as hr_interviewer
					FROM
						employee_application_interviews eai
						JOIN employee_applications ea ON ( ea.id = eai.employee_application_id AND ea.country_code = \'IN\' )
						LEFT JOIN ps_job_posting_steps pjps ON ( pjps.id = eai.ps_job_posting_step_id )
					WHERE
						eai.scheduled_on::DATE BETWEEN ' . $strDateRange;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchOfferDeclinedEmployeeApplicationsByDateRange( $strStartDate, $strEndDate,$intRecruiterId, $objDatabase ) {

		if( false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			return NULL;
		}
		$strWhereClause		= ( true == valId( $intRecruiterId ) ) ? '  AND e.id = ' . ( int ) $intRecruiterId : '';

		$strSql = ' SELECT
						array_agg ( DISTINCT CASE
												WHEN eas.name = \'' . CPsJobPostingStep::OFFER_EXTENDED . '\' THEN ea.id
											 END ) AS offer_extended,
						array_agg ( DISTINCT CASE
												WHEN eas.name IN ( \'' . CPsJobPostingStep::OFFER_EXTENDED . '\', \'' . CPsJobPostingStep::OFFER_ACCEPTED . '\') AND eas.ps_job_posting_status_id = ' . CPsJobPostingStatus::REJECTED . ' THEN ea.id
											 END ) AS offer_declined,
						COALESCE ( e.preferred_name, \'NA\' ) AS recruiter_name
					FROM
						employee_applications ea
						LEFT JOIN 
						 (
							SELECT
								eas.employee_application_id,
								pjps.name,
								pjpss.ps_job_posting_status_id,
								rank ( ) OVER ( PARTITION BY eas.employee_application_id, pjps.name ORDER BY eas.id DESC )
							FROM
								employee_application_steps eas
								JOIN ps_job_posting_step_statuses pjpss ON ( pjpss.id = eas.ps_job_posting_step_status_id )
								JOIN ps_job_posting_steps pjps ON ( pjps.id = pjpss.ps_job_posting_step_id )
							WHERE
								( eas.created_on AT TIME ZONE \'IST\' )::DATE BETWEEN \'' . $strStartDate . '\' AND DATE \'' . $strEndDate . '\'
						) eas ON ( ea.id = eas.employee_application_id AND eas.rank = 1 )
						LEFT JOIN actions AS a ON ( ea.id = a.secondary_reference AND a.action_result_id = ' . CActionResult::EMPLOYEE_APPLICATION . ' AND a.action_type_id = ' . CActionType::NOTE . ' AND a.employee_id IS NOT NULL )
						LEFT JOIN employees e ON ( e.id IN ( a.employee_id, ea.recruiter_employee_id ) )
					WHERE
						ea.country_code = \'' . CCountry::CODE_INDIA . '\'
						AND ( a.id IS NULL OR ( ( a.created_on AT TIME ZONE \'IST\')::DATE BETWEEN \'' . $strStartDate . '\' AND DATE \'' . $strEndDate . '\' ) )
						' . $strWhereClause . '
					GROUP BY
						recruiter_name ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchOfferDeclinedEmployeeApplicationsDetailsByIds( $strEmployeeApplicationIds, $strOfferType, $objDatabase ) {

		if( false == valStr( $strEmployeeApplicationIds ) ) {
			return NULL;
		}

		$strWhereClause = ' pjps.name = \'' . CPsJobPostingStep::OFFER_EXTENDED . '\'';
		$strWhereClause .= ' AND pjpss.ps_job_posting_status_id <> 5';

		if( 'extend' != trim( $strOfferType ) ) {
			$strWhereClause = ' pjps.name IN ( \'' . CPsJobPostingStep::OFFER_EXTENDED . '\', \'' . CPsJobPostingStep::OFFER_ACCEPTED . '\')';
			$strWhereClause .= ' AND pjpss.ps_job_posting_status_id = 5';
		}

		$strSql = 'SELECT
						DISTINCT ON(ea.id)
						ea.id AS employee_application_id,
						CONCAT ( ea.name_first, \' \', ea.name_last ) AS candidate_name,
						ea.email_address AS candidate_email_address,
						eas.purchase_request_id,
						to_char( eas.created_on AT TIME ZONE \'IST\', \'DD-MM-YYYY\' ) AS offered_on,
						eas.comments
					FROM
						employee_applications ea
						LEFT JOIN employees e ON ( e.id = ea.recruiter_employee_id )
						LEFT JOIN 
						(
							SELECT
								eas.*,
								pjps.name,
								pjpss.ps_job_posting_status_id,
								rank ( ) OVER ( PARTITION BY eas.employee_application_id, pjps.name ORDER BY eas.id DESC )
							FROM
								employee_application_steps eas
								JOIN ps_job_posting_step_statuses pjpss ON ( pjpss.id = eas.ps_job_posting_step_status_id )
								JOIN ps_job_posting_steps pjps ON ( pjps.id = pjpss.ps_job_posting_step_id )
							WHERE
								' . $strWhereClause . '	
						) eas ON ( ea.id = eas.employee_application_id AND eas.rank = 1 )
					WHERE
						ea.id IN (' . $strEmployeeApplicationIds . ') ';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchInterviewerAvailability( $strBeginDate, $objDatabase ) {
		$strBeginDate = date( 'Y-m-d', strtotime( $strBeginDate ) );
		$strWhereClause = 'AND (evr.begin_datetime::date <= \'' . $strBeginDate . '\'';
		$strWhereClause = $strWhereClause . '' . ' AND evr.end_datetime::date >= \'' . $strBeginDate . '\')';

		 $strSql = 'SELECT
						DISTINCT ( e.preferred_name),e.id
					FROM
						employees e
						LEFT JOIN employee_vacation_requests evr ON (e.id = evr.employee_id ' . $strWhereClause . ' AND
													(evr.denied_by IS NULL AND
													evr.deleted_by IS NULL) )
						JOIN employee_addresses ea ON ( e.id = ea.employee_id )
						JOIN users u ON ( e.id = u.employee_id )
						JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )
					WHERE
						evr.employee_id IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ug.group_id = ' . ( int ) CGroup::INTERVIEWERS . ' AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'
						ORDER BY
						e.preferred_name';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchInactiveInterviewersDetailsByDateRange( $strStartDate, $strEndDate, $objAdminDatabase, $strDriveDate = NULL, $intPageNo = NULL, $intPageSize = NULL ) {

		if( false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			return NULL;
		}

		$strJoinClause	= $strOffsetClause = '';
		$strWhereClause = 'WHERE sq.rank = 1';
		$strDateBetween = ( false == valStr( $strDriveDate ) ) ? ' BETWEEN \'' . $strStartDate . '\' AND \'' . $strEndDate . '\'' : ' BETWEEN \'' . $strDriveDate . '\'::DATE  - INTERVAL \'3 MONTH\' AND \'' . $strDriveDate . '\'::DATE';

		if( true == valId( $intPageNo ) && true == valId( $intPageSize ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit = ( int ) $intPageSize;
			$strOffsetClause = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}

		if( true == valStr( $strDriveDate ) ) {
			$strJoinClause		= ' LEFT JOIN employee_applications ea ON ( ea.id = eai.employee_application_id )
									LEFT JOIN json_each_text( ( ea.details::json->>\'drive_days\' )::JSON ) dd ON true';
			$strWhereClause		.= ' AND ( ( eai.id IS NULL OR eai.scheduled_on::DATE > \'' . $strDriveDate . '\'::DATE ) OR dd.value::DATE = \'' . $strDriveDate . '\'::DATE )';
		}

		$strSql = ' 
				SELECT
					sq.preferred_name,
					\'Inactive\'AS interview_taken,
					array_to_string( array_agg( sq.id || \'-\' || sq.approved_on || \'-\' || sq.designation_name  ), \',\' ) as open_prs,
					count( sq.preferred_name ) OVER() as records_count
				FROM
					(
					SELECT
						DISTINCT
						pr.id,
						pr.approved_on,
						d.name as designation_name,
						e.id AS employee_id,
						e.preferred_name,
						rank ( ) OVER ( PARTITION BY pr.id ORDER BY pa.order_num ASC )
					FROM
						user_groups ug
						JOIN users u ON ( u.id = ug.user_id )
						JOIN employees e ON ( e.id = u.employee_id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						JOIN purchase_approvals pa ON ( e.id = pa.employee_id )
						JOIN purchase_requests pr ON ( pr.id = pa.purchase_request_id AND pr.purchase_request_type_id = ' . CPurchaseRequestType::NEW_HIRE . ' AND pr.deleted_by IS NULL )
						JOIN designations d ON ( d.id = pr.designation_id_old )
						JOIN resource_requisitions rr ON ( rr.purchase_request_id = pr.id )
						LEFT JOIN resource_allocations ra ON ( ra.resource_requisition_id = rr.id )
					WHERE
						ug.group_id = ' . CGroup::INTERVIEWERS . '
						AND ug.deleted_by IS NULL
						AND pr.approved_on ' . $strDateBetween . '
						AND ( ra.id IS NULL
						OR ( ( ra.created_on AT TIME ZONE \'IST\' )::DATE ' . $strDateBetween . ' ) )
					) sq
					LEFT JOIN employee_application_interviews eai ON ( sq.employee_id = eai.employee_id AND eai.scheduled_on::DATE ' . $strDateBetween . ' )
					LEFT JOIN 
					(
						SELECT
							eas.employee_application_id,
							pjps.name,
							pjpss.ps_job_posting_step_id,
							pjpss.ps_job_posting_status_id,
							rank ( ) OVER ( PARTITION BY eas.employee_application_id, pjps.name
						ORDER BY
							eas.id DESC )
						FROM
							employee_application_steps eas
							LEFT JOIN employee_application_interviews eai ON ( eas.employee_application_id = eai.employee_application_id )
							JOIN ps_job_posting_step_statuses pjpss ON ( pjpss.id = eas.ps_job_posting_step_status_id )
							JOIN ps_job_posting_steps pjps ON ( pjps.id = pjpss.ps_job_posting_step_id )
						WHERE
							( eas.created_on AT TIME ZONE \'IST\' )::DATE ' . $strDateBetween . '
					) eas ON ( eai.employee_application_id = eas.employee_application_id AND eas.ps_job_posting_step_id = eai.ps_job_posting_step_id AND eas.rank = 1 )
					' . $strJoinClause . '
					' . $strWhereClause . '
				GROUP BY
					sq.preferred_name
				HAVING
					COUNT ( DISTINCT CASE
						WHEN eas.name IN ( \'' . CPsJObPostingStep::FIRST_INTERVIEW . '\', \'' . CPsJObPostingStep::SECOND_INTERVIEW . '\', \'' . CPsJObPostingStep::HR_INTERVIEW . '\' ) AND eas.ps_job_posting_status_id NOT IN ( ' . CPsJobPostingStatus::NEW_STATUS . ', ' . CPsJobPostingStatus::IN_PROCESS . ' ) THEN 1
					END ) = 0'
				. $strOffsetClause;

		return fetchData( $strSql, $objAdminDatabase );
	}

}
?>