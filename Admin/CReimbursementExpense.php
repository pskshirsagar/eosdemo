<?php

class CReimbursementExpense extends CBaseReimbursementExpense {

	protected $m_strEmployeeExpenseTypeName;

	const EXCEPTION_RECEIPT_AMOUNT	= 25;
	const EXCEPTION_INTERNET_AMOUNT	= 600;
	const FOOD_AMOUNT_PER_EMPLOYEE	= 150;
	const EXCEPTION_IT_ACCESSORIES_AMOUNT	= 1000;

	/**
	 * Get Functions
	 *
	 */

	public function getEmployeeExpenseTypeName() {
		return $this->m_strEmployeeExpenseTypeName;
	}

	public function getPurpose() {
		return ( false == is_null( $this->getPurposeEncrypted() ) ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getPurposeEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] ) : NULL;
	}

	public function getAmount() {
		return ( false == is_null( $this->getAmountEncrypted() ) ) ? \Psi\Libraries\Cryptography\CCrypto::createService()->decrypt( $this->getAmountEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] ) : NULL;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setEmployeeExpenseTypeName( $strEmployeeExpenseType ) {
		$this->m_strEmployeeExpenseTypeName = $strEmployeeExpenseType;
	}

	public function setPurpose( $strPurpose ) {
		$this->setPurposeEncrypted( ( true == valStr( $strPurpose ) ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( preg_replace( '/[^a-z0-9,. \n]/i', '', $strPurpose ), CONFIG_SODIUM_KEY_TAX_NUMBER ) : NULL );
	}

	public function setAmount( $fltAmount ) {
		$this->setAmountEncrypted( ( true == valStr( $fltAmount ) ) ? ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( preg_replace( '/[^-a-z0-9.]/i', '', $fltAmount ), CONFIG_SODIUM_KEY_TAX_NUMBER ) : 0 );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['reimbursement_type_name'] ) ) $this->setEmployeeExpenseTypeName( $arrmixValues['reimbursement_type_name'] );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valEmployeeExpenseTypeId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getEmployeeExpenseTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'employee_expense_type_id', 'Expense type is required. ', NULL ) );
		}

		return $boolIsValid;
	}

	public function valDepartmentId() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getDepartmentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'department_id', 'Department is required. ', NULL ) );
		}

		return $boolIsValid;
	}

	public function valVendorName() {
		$boolIsValid = true;

		if( false == valStr( $this->getVendorName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'vendor_name', 'Vendor name is required. ', NULL ) );
		}

		return $boolIsValid;
	}

	public function valPurposeEncrypted() {
		$boolIsValid = true;

		if( false == valStr( $this->getPurposeEncrypted() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'purpose', 'Business Purpose is required. ', NULL ) );
		}

		return $boolIsValid;
	}

	public function valAmountEncrypted( $boolIsSubmit = false, $intEmployeeExpenseTypeId = NULL, $arrintResponsibleEmployeeIds = [], $intITAccessoriesBalanceAmount = NULL, $intITAccessoriesTempAmount = NULL ) {
		$boolIsValid = true;

		$this->setAmountEncrypted( str_replace( [ ',', '$' ], '', $this->getAmountEncrypted() ) );

		if( false == $boolIsSubmit ) {
			$fltAmount = $this->getAmountEncrypted();
		} else {
			$fltAmount = $this->getAmount();
		}

		if( true == is_null( $fltAmount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'amount', 'Amount is required. ', NULL ) );

			return $boolIsValid;
		}

		if( false == is_numeric( $fltAmount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'amount', 'Please enter valid amount. ', NULL ) );

			return $boolIsValid;
		}

		if( 0 >= $fltAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'amount', 'Amount should be greater than zero. ', NULL ) );

			return $boolIsValid;
		}

		if( CEmployeeExpenseType::FOOD == $intEmployeeExpenseTypeId && true == valArr( $arrintResponsibleEmployeeIds ) ) {
			$intTotalFoodReimbursementAmount = \Psi\Libraries\UtilFunctions\count( $arrintResponsibleEmployeeIds ) * self::FOOD_AMOUNT_PER_EMPLOYEE;
			if( $intTotalFoodReimbursementAmount < $fltAmount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, 'amount', 'Please enter valid amount.', NULL ) );
				return $boolIsValid;
			}
		}

		if( CEmployeeExpenseType::INTERNET == $intEmployeeExpenseTypeId && self::EXCEPTION_INTERNET_AMOUNT < $fltAmount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'amount', 'Amount for Internet should not be greater than Rs.600.', NULL ) );

			return $boolIsValid;
		}

		if( CEmployeeExpenseType::IT_ACCESSORIES == $intEmployeeExpenseTypeId ) {
			$fltAmount = $intITAccessoriesTempAmount;
			if( ( self::EXCEPTION_IT_ACCESSORIES_AMOUNT < $fltAmount || $intITAccessoriesBalanceAmount < $fltAmount ) ) {
				$boolIsValid = false;
				if( 0 == $intITAccessoriesBalanceAmount ) {
					$strMessage = 'Currently you are not able to add reimbursement for IT accessories.';
				} elseif( $intITAccessoriesBalanceAmount < $fltAmount && 1000 > $fltAmount ) {
					$strMessage = 'You can add maximum amount of Rs.' . $intITAccessoriesBalanceAmount . ' for IT accessories reimbursement.';
				} else {
					$strMessage  = 'Amount for IT Accessories should not be greater than Rs.1000.';
				}
				$this->addErrorMsg( new CErrorMsg( NULL, 'amount', $strMessage, NULL ) );
			}

			if( false == valStr( $this->getNotes() ) ) {
				$boolIsValid = false;
				$strMessage = 'Additional note for IT accessories is required.';
				$this->addErrorMsg( new CErrorMsg( NULL, 'notes', $strMessage, NULL ) );
			}

			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valPurchasedDate() {
		$boolIsValid = true;

		if( false == valStr( $this->getPurchasedDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'purchased_date', 'Please select valid date purchased. ', NULL ) );
		}

		if( strtotime( date( 'm/d/Y' ) ) < strtotime( $this->getPurchasedDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'purchased_date', 'Purchased date should not be greater than today\'s date. ', NULL ) );
		}

		return $boolIsValid;
	}

	public function valDistance( $intDistance ) {
		$boolIsValid = true;

		if( false == is_numeric( $intDistance ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'no_of_miles', 'Number of miles is required. ', NULL ) );
		}

		if( 0 >= $intDistance ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'no_of_miles', 'Number of miles should be greater than zero. ', NULL ) );

			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;

		if( false == valStr( $this->getNotes() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'notes', 'Destination is required. ', NULL ) );
		}

		return $boolIsValid;
	}

	public function valReceipts( $arrintReceiptIds ) {
		$boolIsValid = true;

		if( false == valArr( $arrintReceiptIds ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'receipt', 'Receipt(s) are required. ', NULL ) );
		}

		return $boolIsValid;
	}

	public function valResponsibleEmployeeIds( $arrintResponsibleEmployeeIds ) {
		$boolIsValid = true;

		if( false == valArr( $arrintResponsibleEmployeeIds ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'responsible_employee', 'Responsible Employee(s) are required. ', NULL ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $intDistance = NULL, $arrintReceiptIds = NULL, $boolIsSubmit = false, $intRequestTypeId = NULL, $arrintResponsibleEmployeeIds = NULL, $intITAccessoriesBalanceAmount = NULL, $intITAccessoriesTempAmount = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:

				$boolIsValid &= $this->valDepartmentId();
				$boolIsValid &= $this->valEmployeeExpenseTypeId();

				if( true == in_array( $intRequestTypeId, [ CRequestType::REIMBURSEMENT, CRequestType::INDIAN_REIMBURSEMENT ] ) ) {

					$boolIsValid &= $this->valPurchasedDate();

					if( CRequestType::INDIAN_REIMBURSEMENT == $intRequestTypeId && true == in_array( $this->getEmployeeExpenseTypeId(), [ CEmployeeExpenseType::INTERNET, CEmployeeExpenseType::FOOD, CEmployeeExpenseType::IT_ACCESSORIES ] ) ) {

						if( CEmployeeExpenseType::FOOD == $this->getEmployeeExpenseTypeId() ) {
							$boolIsValid &= $this->valResponsibleEmployeeIds( $arrintResponsibleEmployeeIds );
						}

						$boolIsValid &= $this->valAmountEncrypted( $boolIsSubmit, $intEmployeeExpenseTypeId = $this->getEmployeeExpenseTypeId(), $arrintResponsibleEmployeeIds, $intITAccessoriesBalanceAmount, $intITAccessoriesTempAmount );
					} else {
						$boolIsValid &= $this->valAmountEncrypted( $boolIsSubmit );
					}

					if( false == $boolIsSubmit ) {
						$fltAmount = $this->getAmountEncrypted();
					} else {
						$fltAmount = $this->getAmount();
					}

					if( true == in_array( $this->getEmployeeExpenseTypeId(), [ CEmployeeExpenseType::MILEAGE, CEmployeeExpenseType::CONVEYANCE ] ) ) {
						$boolIsValid &= $this->valNotes();
					}

					if( CRequestType::REIMBURSEMENT == $intRequestTypeId && CEmployeeExpenseType::MILEAGE != $this->getEmployeeExpenseTypeId() ) {

						$boolIsValid &= $this->valVendorName();

						if( self::EXCEPTION_RECEIPT_AMOUNT < $fltAmount ) {
							$boolIsValid &= $this->valReceipts( $arrintReceiptIds );
						}
					}

					if( CRequestType::INDIAN_REIMBURSEMENT == $intRequestTypeId ) {
						$boolIsValid &= $this->valReceipts( $arrintReceiptIds );
					} else {
						$boolIsValid &= $this->valPurposeEncrypted();
					}

				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'save_as_draft':
				break;

			case 'insert_imported_amex_xls_file':
				$boolIsValid &= $this->valPurchasedDate();
				$boolIsValid &= $this->valVendorName();
				break;

			case 'mileage':
			case 'conveyance':
				$boolIsValid &= $this->valPurchasedDate();
				$boolIsValid &= $this->valDistance( $intDistance );
				$boolIsValid &= $this->valNotes();
				if( CRequestType::REIMBURSEMENT == $intRequestTypeId ) {
					$boolIsValid &= $this->valPurposeEncrypted();
				}
				if( CEmployeeExpenseType::CONVEYANCE == $this->getEmployeeExpenseTypeId() ) {
					$boolIsValid &= $this->valReceipts( $arrintReceiptIds );
				}
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;

	}

}
?>
