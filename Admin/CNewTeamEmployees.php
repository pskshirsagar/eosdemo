<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CNewTeamEmployees
 * Do not add any new functions to this class.
 */

class CNewTeamEmployees extends CBaseNewTeamEmployees {

	public static function fetchTeamEmployeesByTeamIdByEmployeeId( $intTeamId, $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						new_team_employees
					WHERE
						team_id = ' . ( int ) $intTeamId . '
						AND employee_id = ' . ( int ) $intEmployeeId . '
					ORDER BY
						is_primary_team DESC,
						COALESCE( manager_employee_id, 0 ) DESC
					LIMIT 1';

		return self::fetchNewTeamEmployee( $strSql, $objDatabase );
	}

	public static function fetchCurrentTeamEmployeesCountByTeamIds( $arrintTeamEmployeeIds, $objDatabase, $boolIsPrimaryTeam = false ) {
		if( false == valArr( $arrintTeamEmployeeIds ) ) return NULL;

		$strWhereCondition = ( true == $boolIsPrimaryTeam ) ? 'AND te.is_primary_team = 1' : '';

		$strSql = ' SELECT
						team_id,count( te.id ) AS member_count
					FROM
						new_team_employees te
						JOIN employees e on te.employee_id = e.id
					WHERE
						te.team_id IN (' . implode( ',', array_keys( $arrintTeamEmployeeIds ) ) . ') AND
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						' . $strWhereCondition . '
					GROUP BY
						team_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeeByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT * FROM new_team_employees WHERE employee_id = ' . ( int ) $intEmployeeId . ' AND is_primary_team = 1 LIMIT 1';

		return self::fetchNewTeamEmployee( $strSql, $objDatabase );
	}

}
?>