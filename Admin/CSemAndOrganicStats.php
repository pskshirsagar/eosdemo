<?php

class CSemAndOrganicStats extends CBaseSemAndOrganicStats {

	public static function fetchTodaysSemAndOrganicStatsByPropertyIdsKeyedByPropertyIdBySemAdGroupIdBySemKeywordIdBySemSourceId( $arrintPropertyIds, $intSemAdGroupId, $intSemKeywordId, $intSemSourceId, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSqlSemAdGroup 	= ( false == is_null( $intSemAdGroupId ) && true == is_numeric( $intSemAdGroupId )? ' AND sem_ad_group_id = ' . ( int ) $intSemAdGroupId : ' AND sem_ad_group_id IS NULL' );
		$strSqlSemKeyword 	= ( false == is_null( $intSemKeywordId ) && true == is_numeric( $intSemKeywordId )? ' AND sem_keyword_id = ' . ( int ) $intSemKeywordId : ' AND sem_keyword_id IS NULL' );
		$strSqlSemSource 	= ( false == is_null( $intSemSourceId ) && true == is_numeric( $intSemSourceId )? ' AND sem_keyword_id = ' . ( int ) $intSemSourceId :  ' AND sem_source_id IS NULL' );

		$strSql = 'SELECT * FROM sem_and_organic_stats WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ' . $strSqlSemAdGroup . $strSqlSemKeyword . $strSqlSemKeyword . '  AND traffic_date = DATE_TRUNC ( \'day\', NOW() );';

		$arrobjSemAndOrganicStats = self::fetchSemAndOrganicStats( $strSql, $objDatabase );
		$arrobjRekeyedSemAndOrganicStats = array();

		if( true == valArr( $arrobjSemAndOrganicStats ) ) {
			foreach( $arrobjSemAndOrganicStats as $objSemAndOrganicStat ) {
				$arrobjRekeyedSemAndOrganicStats[$objSemAndOrganicStat->getPropertyId()] = $objSemAndOrganicStat;
			}
		}

		return $arrobjRekeyedSemAndOrganicStats;
	}

	public static function fetchSummarizedSemAndOrganicStatsByCidByDate( $intCid, $strStartDate, $objDatabase ) {

		$strSql = 'SELECT
						sem_ad_group_id, sag.name as sem_ad_group_name,
						sum( list_impressions ) 				as list_impressions ,
						sum( unique_full_impressions )			as unique_full_impressions,
						sum( full_impressions ) 				as full_impressions,
						sum( unique_website_visits )			as unique_website_visits ,
						sum( website_visits ) 					as website_visits ,
						sum( unique_click_throughs )			as unique_click_throughs ,
						sum( unique_list_featured_impressions ) as unique_list_featured_impressions ,
						sum( unique_list_impressions )			as unique_list_impressions ,
						sum( list_featured_impressions) 		as list_featured_impressions,
						sum( click_throughs ) + sum( full_impressions ) + sum( website_visits ) as click_throughs ,
						sum( guest_cards )						as guest_cards
					FROM
						sem_and_organic_stats saos,
						sem_ad_groups sag
					WHERE
						saos.sem_ad_group_id = sag.id
						AND saos.cid = ' . ( int ) $intCid . '
						AND DATE_TRUNC( \'month\', traffic_date ) =\'' . $strStartDate . '\'
						AND sem_ad_group_id IS NOT NULL
					GROUP BY
						sem_ad_group_id,
						sem_ad_group_name';

		return self::fetchSemAndOrganicStats( $strSql, $objDatabase );
	}

	public static function fetchAllSemAndOrganicStatsByStastisticsEmailFilter( $objStastisticsEmailFilter, $objDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) )
			|| ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = 'SELECT saos.property_id, traffic_date as xaxis,
						sum( list_impressions ) 				as list_impressions ,
						sum( unique_full_impressions )			as unique_full_impressions,
						sum( full_impressions ) 				as full_impressions,
						sum( unique_website_visits )			as unique_website_visits ,
						sum( website_visits )					as website_visits ,
						sum( unique_click_throughs )			as unique_click_throughs ,
						sum( unique_list_featured_impressions ) as unique_list_featured_impressions ,
						sum( unique_list_impressions )			as unique_list_impressions ,
						sum( list_featured_impressions) 		as list_featured_impressions,
						sum( click_throughs ) + sum( full_impressions ) + sum( website_visits ) as click_throughs ,
						sum( guest_cards )						as guest_cards
					FROM
						sem_and_organic_stats saos
					WHERE
						saos.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
					AND
						saos.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
					AND
						saos.sem_keyword_id IS NULL
					AND
						traffic_date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'
					AND
						traffic_date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'
					GROUP BY
						saos.property_id,
						xaxis
					ORDER BY
						saos.property_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>