<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyGoals
 * Do not add any new functions to this class.
 */

class CCompanyGoals extends CBaseCompanyGoals {

	public static function fetchCompanyGoalsByIds( $arrintCompanyGoalIds, $objDatabase ) {

		if( false == valArr( $arrintCompanyGoalIds ) ) return NULL;

		return self::fetchCompanyGoals( 'SELECT * FROM company_goals WHERE id IN ( ' . implode( ',', $arrintCompanyGoalIds ) . ' ) ', $objDatabase );
	}

	public static function fetchPaginatedCompanyGoals( $intPageNo, $intPageSize, $arrmixCompanyGoalFilter, $objDatabase ) {

		$intOffset			 = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		$strSql = ' SELECT
						*
					FROM
						company_goals
					ORDER BY
 							' . $arrmixCompanyGoalFilter['sort_by_field'] . ' ' . $arrmixCompanyGoalFilter['sort_by_type'] . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;

		return self::fetchCompanyGoals( $strSql, $objDatabase );
	}

	public static function fetchPaginatedCompanyGoalsCount( $objDatabase ) {

 		$strSql = 'SELECT count(id) FROM company_goals';

		$arrintCompanyGoalsCount = fetchData( $strSql, $objDatabase );
		return ( true == isset( $arrintCompanyGoalsCount[0]['count'] ) ) ? $arrintCompanyGoalsCount[0]['count'] : 0;
	}

	public static function fetchCompanyGoalsByNameByGoalYear( $arrstrFilteredExplodedSearch, $objAdminDatabase ) {

		if( false == valArr( $arrstrFilteredExplodedSearch ) ) return NULL;

		$strSql = ' SELECT
						cg.id,
						cg.name,
						cg.goal_year
					FROM company_goals cg
					WHERE
						cg.name ILIKE \'%' . implode( '%\' AND cg.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR to_char( cg.goal_year, \'999999\' ) LIKE \'%' . implode( '', $arrstrFilteredExplodedSearch ) . '%\'
					LIMIT 10';

		return self::fetchCompanyGoals( $strSql, $objAdminDatabase );
	}

	public static function fetchAllPublishedCompanyGoals( $objDatabase, $intGetTrainingSessionId = NULL ) {

		$strWhereCondition = ' NOW() ';
		if( false == is_null( $intGetTrainingSessionId ) ) {
			$strWhereCondition = '( SELECT
										created_on
									FROM training_sessions WHERE id=' . ( int ) $intGetTrainingSessionId . ' )';
		}

		$strSql = ' SELECT
						*
					FROM company_goals cg
					WHERE
						cg.goal_year =date_part( \'year\', ' . $strWhereCondition . ' ) AND cg.is_published = true ORDER BY cg.name';

		return self::fetchCompanyGoals( $strSql, $objDatabase );
	}

}
?>