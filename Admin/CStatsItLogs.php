<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsItLogs
 * Do not add any new functions to this class.
 */
class CStatsItLogs extends CBaseStatsItLogs {

	public static function fetchStatsItLogsData( $boolIsCount = false, $arrmixDates, $objDatabase ) {

		$strWhereCondition = ' WHERE 1 = 1 ';

		if( true == valArr( $arrmixDates ) ) {
			if( ( true == valStr( $arrmixDates['from_date'] ) && ( true == valStr( $arrmixDates['to_date'] ) ) ) ) {
				$strWhereCondition .= '	AND sit.log_date BETWEEN \'' . $arrmixDates['from_date'] . '\' AND \'' . $arrmixDates['to_date'] . '\'';
			} elseif( ( true == valStr( $arrmixDates['from_date'] ) && ( false == valStr( $arrmixDates['to_date'] ) ) ) ) {
				$strWhereCondition .= ' AND sit.log_date >= \'' . $arrmixDates['from_date'] . '\'';
			} elseif( ( false == valStr( $arrmixDates['from_date'] ) && ( true == valStr( $arrmixDates['to_date'] ) ) ) ) {
				$strWhereCondition .= ' AND sit.log_date <= \'' . $arrmixDates['to_date'] . '\'';
			}
		}

		$strSql = 'SELECT
						sit.log_date as "Date",
						SUM ( CASE
							WHEN sit.stats_type_id = 1 THEN sit.count
							ELSE NULL
							END ) AS "Solved Tickets",
						SUM ( CASE
							WHEN sit.stats_type_id = 2 THEN sit.count
							ELSE NULL
							END ) AS "Unsolved Tickets",
						SUM ( CASE
							WHEN sit.stats_type_id = 3 THEN sit.count
							ELSE NULL
							END ) AS "Tickets Tagged PreCog",
						SUM ( CASE
							WHEN sit.stats_type_id = 4 THEN sit.count
							ELSE NULL
							END ) AS "Tickets Tagged Incident",
						SUM ( CASE
							WHEN sit.stats_type_id = 5 THEN sit.count
							ELSE NULL
							END ) AS "Solved Tickets Tagged Cab",
						SUM ( CASE
							WHEN sit.stats_type_id = 6 THEN sit.count
							ELSE NULL
							END ) AS "Tickets Received Update",
						SUM ( CASE
							WHEN sit.stats_type_id = 7 THEN sit.count
							ELSE NULL
							END ) AS "Solved Tickets Not Tagged Failed",
						SUM ( CASE
							WHEN sit.stats_type_id = 8 THEN sit.count
							ELSE NULL
							END ) AS "Solved Tickets Not Tagged Incident"
					FROM
						stats_it_logs sit
						JOIN stats_types st ON st.id = sit.stats_type_id ' . $strWhereCondition . '
					GROUP BY
						sit.log_date
					ORDER BY
						sit.log_date';

		if( true == $boolIsCount ) {
			$arrintCount = fetchData( $strSql, $objDatabase );
			return \Psi\Libraries\UtilFunctions\count( $arrintCount );
		} else {
			return fetchData( $strSql, $objDatabase );
		}

	}

	public static function fetchStatsTypes( $objDatabase ) {

		$strSql = 'SELECT id, name FROM stats_types';

		return fetchData( $strSql, $objDatabase );
	}
}
?>