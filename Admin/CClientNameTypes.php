<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CClientNameTypes
 * Do not add any new functions to this class.
 */

class CClientNameTypes extends CBaseClientNameTypes {

	public static function fetchAllClientNameTypes( $objDatabase ) {
		$strSql = ' SELECT * FROM client_name_types';

		return parent::fetchClientNameTypes( $strSql, $objDatabase );
	}

}
?>