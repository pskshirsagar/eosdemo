<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseNotes
 * Do not add any new functions to this class.
 */

class CReleaseNotes extends CBaseReleaseNotes {

	public static function fetchReleaseNoteByTaskId( $intTaskId, $objDatabase ) {
		if( false == is_numeric( $intTaskId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						release_notes
					WHERE
						task_id = ' . ( int ) $intTaskId;

		return self::fetchReleaseNote( $strSql, $objDatabase );
	}

	public static function fetchReleaseNotesByTaskIds( $arrintTaskIds, $objDatabase ) {
		if( false == valArr( $arrintTaskIds ) ) return [];

		$strSql = 'SELECT rn.*,
						rnt.name AS release_note_type_name,
						e.preferred_name AS key_contact_employee_name
					FROM release_notes rn
					LEFT JOIN release_note_types rnt ON ( rn.release_note_type_id = rnt.id )
					LEFT JOIN employees e ON ( e.id = rn.employee_id )
					JOIN tasks t ON (rn.task_id = t.id)
					JOIN ps_products pp ON (pp.id = t.ps_product_id)
					LEFT JOIN ps_product_options ppo ON (t.ps_product_option_id = ppo.id)
					WHERE rn.task_id IN ( ' . implode( ',', $arrintTaskIds ) . ' ) ORDER BY pp.name,ppo.name,rn.task_id';

		return self::fetchReleaseNotes( $strSql, $objDatabase );
	}

	public static function fetchReleaseNotesByTaskReleaseIdsByTaskImpactId( $arrintTaskReleaseIds, $intTaskImpactTypeId, $objDatabase, $intLimit ) {

		if( false == valArr( $arrintTaskReleaseIds ) ) return [];

		$strLimit = ( true == is_numeric( $intLimit ) ) ? ' LIMIT ' . $intLimit : '';

		$strSql = 'SELECT
						count(t.id) OVER() as total_record_count,
						rn.description,
						t.title,
						t.id,
						pp.name
					FROM release_notes rn
					JOIN tasks t ON (rn.task_id = t.id)
					JOIN task_details td ON (td.task_id = t.id)
					JOIN ps_products pp ON (pp.id = t.ps_product_id)
					WHERE t.task_release_id IN( ' . implode( ',', $arrintTaskReleaseIds ) . ' )
					AND td.is_release_note_approved = 1
					AND td.task_impact_type_id = ' . ( int ) $intTaskImpactTypeId . $strLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchReleaseNoteTaskIdsByTaskIds( $arrintTaskIds, $objDatabase ) {

		if( false == valArr( $arrintTaskIds ) ) return [];

		$strSql = 'SELECT
						t.id
					FROM
						release_notes rn
						LEFT JOIN tasks t ON ( rn.task_id = t.id )
					WHERE
						rn.task_id IN ( ' . implode( ',', $arrintTaskIds ) . ' )';

		return fetchData( $strSql, $objDatabase );

	}

}
?>