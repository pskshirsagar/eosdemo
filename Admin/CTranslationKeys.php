<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTranslationKeys
 * Do not add any new functions to this class.
 */

class CTranslationKeys extends CBaseTranslationKeys {

	public static function fetchTranslationKeyByApplicationNameByNamespaceByKey( $strApplicationName, $strNamespace, $strKey, $objDatabase ) {

		$strSql = sprintf( '
				SELECT
					tk.*
				FROM
					translation_keys AS tk
				WHERE
					tk.application_name = %s
					AND tk.namespace = %s
					AND tk.key = %s',
			pg_escape_literal( $objDatabase->getHandle(), $strApplicationName ),
			pg_escape_literal( $objDatabase->getHandle(), $strNamespace ),
			pg_escape_literal( $objDatabase->getHandle(), trim( $strKey ) ) );

		return parent::fetchTranslationKey( $strSql, $objDatabase );
	}

	public static function fetchSystemTranslationKeys( $strApplicationName, $strNamespace, $arrstrTranslationKeys, $objDatabase ) {

		array_walk( $arrstrTranslationKeys, function( &$strValue ) use ( $objDatabase ) {
			$strValue = pg_escape_literal( $objDatabase->getHandle(), trim( $strValue ) );
		} );

		$strSql = sprintf( '
				SELECT
					tk.id,
					tk.key
				FROM
					translation_keys AS tk
				WHERE
					tk.application_name = %s
					AND tk.namespace = %s
					AND tk.key IN ( %s )
					AND tk.is_system
					AND tk.deleted_by IS NULL
					AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), $strApplicationName ),
			pg_escape_literal( $objDatabase->getHandle(), $strNamespace ),
			implode( ',', $arrstrTranslationKeys ) );

		$arrmixData = fetchData( $strSql, $objDatabase ) ?? [];

		foreach( $arrmixData as $mixData ) {
			$arrstrKeys[$mixData['id']] = $mixData['key'];
		}

		return $arrstrKeys ?? [];
	}

	public static function fetchAllSystemTranslationKeys( $strApplicationName, $strNamespace, $objDatabase ) {

		$strSql = sprintf( '
				SELECT
					tk.id,
					tk.key
				FROM
					translation_keys AS tk
				WHERE
					tk.application_name = %s
					AND tk.namespace = %s
					AND tk.is_system
					AND tk.deleted_by IS NULL
					AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), $strApplicationName ),
			pg_escape_literal( $objDatabase->getHandle(), $strNamespace ) );

		$arrmixData = fetchData( $strSql, $objDatabase ) ?? [];

		foreach( $arrmixData as $mixData ) {
			$arrstrKeys[$mixData['id']] = $mixData['key'];
		}

		return $arrstrKeys ?? [];
	}

	public static function fetchTranslationKeysByIds( $arrintIds, $objDatabase ) {
		return self::fetchTranslationKeys( sprintf( 'SELECT * FROM translation_keys WHERE id IN ( %s )', implode( ',', $arrintIds ) ), $objDatabase );
	}

	public static function fetchTranslationKeysById( $intId, $objDatabase ) {
		return self::fetchTranslationKey( sprintf( 'SELECT * FROM translation_keys WHERE id = ' . ( int ) $intId . ' ' ), $objDatabase );
	}

	public static function fetchTranslationKeysByDependentFile( $strFileName, $objDatabase ) {

		$strSql = sprintf( '
				SELECT
					tk.id,
					tk.application_name,
					tk.namespace,
					tk.key
				FROM
					translation_keys AS tk
				WHERE
					tk.details->\'dependent_files\' ? %s
					AND tk.deleted_by IS NULL
					AND tk.deleted_on IS NULL',
			pg_escape_literal( $objDatabase->getHandle(), $strFileName ) );

		$arrmixData = fetchData( $strSql, $objDatabase ) ?? [];

		return rekeyArray( 'id', $arrmixData ) ?? [];
	}

	public static function fetchAllPaginatedTranslationKeys( $arrmixFilteredData, $objDatabase ) {

		$intLimit = ( int ) $arrmixFilteredData['page_size'];
		$intOffset = ( 0 < $arrmixFilteredData['page_no'] ) ? $intLimit * ( $arrmixFilteredData['page_no'] - 1 ) : 0;
		$boolShowDeletedKey = $arrmixFilteredData['show_deleted_key'];
		$arrstrWhere = [];
		$strWhere = '';
		$arrstrJoin = [];
		$strJoin = '';
		$strLimit = '';
		$strOrderByKey = '';

		if( false == empty( $arrmixFilteredData['app_name'] ) ) {
			$arrstrWhere[] = 'tk.application_name = \'' . $arrmixFilteredData['app_name'] . '\'';
		}

		if( false == empty( $arrmixFilteredData['namespace'] ) ) {
			$arrstrWhere[] = 'tk.namespace = \'' . $arrmixFilteredData['namespace'] . '\'';
		}

		if( false == empty( $arrmixFilteredData['key'] ) ) {
			$arrstrWhere[] = 'tk.key ILIKE \'%' . $arrmixFilteredData['key'] . '%\'';
			$strOrderByKey = '( tk.key ILIKE \'' . $arrmixFilteredData['key'] . '%\' ) DESC, lower(tk.key), ';
		}

		$strSelect = '';
		if( false == empty( $arrmixFilteredData['locale_code'] ) ) {
			$arrstrJoin[] = ' JOIN translations t ON t.translation_key_id = tk.id AND t.locale_code = \'' . $arrmixFilteredData['locale_code'] . '\' AND util.util_to_bytea(value)::text !~* E\'^\\\\Bx(c2a0)+$\' AND t.details#>>\'{translation_vendor,received_on}\' IS NOT NULL';
			$strSelect = ', t.value as translated_value';

			if( false == empty( $arrmixFilteredData['search_text'] ) ) {
				$arrstrWhere[] = 't.value like \'%' . $arrmixFilteredData['search_text'] . '%\'';
			}

			if( false == empty( $arrmixFilteredData['skip_with_note'] ) ) {
				$arrstrJoin[] = ' LEFT JOIN translation_notes tn ON t.translation_key_id = tn.translation_key_id AND t.locale_code = tn.locale_code';
				$arrstrWhere[] = 'tn.note IS NULL ';
			}
		}

		if( true == getArrayElementByKey( 'is_note_added', $arrmixFilteredData ) ) {

			$strSubWhere = '';
			if( false == empty( $arrmixFilteredData['locale_code'] ) ) {

				$strSelect   .= ', n.status';
				$arrstrJoin[] = ' LEFT JOIN LATERAL 
								    (
								      SELECT
								          id,
								          CASE
								            WHEN ( ( details ->> \'translation_vendor\' ) :: json ->> \'received_on\' ) IS NOT NULL THEN \'Received\'
								            WHEN ( ( details ->> \'translation_vendor\' ) :: json ->> \'sent_on\' ) IS NOT NULL THEN \'Sent\'
								            ELSE \'New\'
								          END AS status
								      FROM
								          translation_notes tn
								      WHERE
								          tk.id = tn.translation_key_id
								          AND tn.locale_code = \'' . $arrmixFilteredData['locale_code'] . '\'
								      ORDER BY
								          id DESC
								      LIMIT
								          1
								    ) AS n ON TRUE';
				$strSubWhere = 'AND tn.locale_code = \'' . $arrmixFilteredData['locale_code'] . '\'';
			}

			$arrstrWhere[] = ' EXISTS(
							      SELECT
							          1
							      FROM
							          translation_notes tn
							      WHERE
							          tk.id = tn.translation_key_id ' . $strSubWhere . '
							      LIMIT 1
							    )';
		}

		if( true == valArr( $arrstrJoin ) ) {
			$strJoin = implode( ' ', $arrstrJoin );
		}

		if( true == $boolShowDeletedKey ) {
			$strWhere = 'WHERE tk.deleted_by IS NOT NULL AND tk.deleted_on IS NOT NULL';
		} else {
			$strWhere = 'WHERE tk.deleted_by IS NULL AND tk.deleted_on IS NULL';
		}
		if( true == valArr( $arrstrWhere ) ) {
			$strWhere .= ' AND ' . implode( ' AND ', $arrstrWhere );
		}

		if( true == empty( $arrmixFilteredData['is_select_all'] ) ) {
			$strLimit = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}

		if( true == $arrmixFilteredData['count'] ) {
			$strSql = ' SELECT 
							count(1) as count
						FROM 
							translation_keys tk
							' . $strJoin . '
							' . $strWhere;

			return self::fetchColumn( $strSql, 'count', $objDatabase );
		} else {
			$strSql = 'SELECT 
							tk.id,
							tk.application_name,
							tk.namespace,
							tk.key,
							tk.is_system,
							tk.details,
							tk.deleted_by, 
							tk.deleted_on ' . $strSelect . '
						FROM 
							translation_keys tk
							' . $strJoin . '
							' . $strWhere . '
						ORDER BY 
							' . $strOrderByKey . ' tk.id' . $strLimit;

			return fetchData( $strSql, $objDatabase );
		}
	}

	public static function fetchSearchedTranslationKeys( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						tk.*
					FROM 
						translation_keys as tk
					WHERE 
						( ( tk.application_name ILIKE \'%' . implode( '%\' AND tk.application_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
						OR ( tk.key ILIKE \'%' . implode( '%\' AND tk.key ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
						OR ( tk.namespace ILIKE \'%' . implode( '%\' AND tk.namespace ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' ) )
						AND tk.deleted_by IS NULL
						AND tk.deleted_on IS NULL
					ORDER BY 
						( tk.key ILIKE \'' . $arrstrFilteredExplodedSearch[0] . '%\' ) DESC, lower(tk.key), tk.id
					LIMIT 10';

		return self::fetchTranslationKeys( $strSql, $objDatabase );
	}

	public static function fetchApplicationNames( $objDatabase ) {
		return self::fetchTranslationKeys( sprintf( 'SELECT DISTINCT ( application_name ) FROM translation_keys' ), $objDatabase );
	}

	public static function fetchNamespaces( $objDatabase ) {
		return self::fetchTranslationKeys( sprintf( 'SELECT DISTINCT ( namespace ) FROM translation_keys' ), $objDatabase );
	}

	public static function fetchTranslationIdByApplicationNameByNamespaceByKey( $strApplicationName, $strNamespace, $strTranslationKey, $objDatabase ) {
		$strSql = sprintf( ' SELECT
						tk.id
					FROM
						translation_keys AS tk
					WHERE
						tk.application_name = %s
						AND tk.namespace = %s
						AND tk.key = %s ',
			pg_escape_literal( $objDatabase->getHandle(), $strApplicationName ),
			pg_escape_literal( $objDatabase->getHandle(), $strNamespace ),
			pg_escape_literal( $objDatabase->getHandle(), $strTranslationKey ) );
		return self::fetchColumn( $strSql, 'id', $objDatabase );
	}

	public function fetchTranslationKeysByDependentDatabasesTuple( $strDependentDatabaseKeyName, $arrmixDependentDatabase, $strKey, $objDatabase ) {

		$arrstrPkey = $arrmixDependentDatabase['tuples'][0]['pkey'];
		foreach( $arrstrPkey as $strPkey => $mixValue ) {
			$arrstrPkeyCondition[] = sprintf( 'tuples.pkey#>>%s = %s', pg_escape_literal( $objDatabase->getHandle(), '{' . $strPkey . '}' ), pg_escape_literal( $objDatabase->getHandle(), $mixValue ) );
		}

		$strSql = sprintf( 'SELECT
										DISTINCT tk.*
									FROM 
										translation_keys tk
										JOIN jsonb_to_recordset( details#>%s ) as tuples("pkey" jsonb, "table" text, "column" text)  
											ON %s AND tuples.table = %s AND tuples.column = %s
									WHERE 
										tk.details#>>%s IS NOT NULL
										AND tk.key != %s',
			pg_escape_literal( $objDatabase->getHandle(), '{dependent_databases,' . $strDependentDatabaseKeyName . ',tuples}' ),
			implode( ' AND ', $arrstrPkeyCondition ),
			pg_escape_literal( $objDatabase->getHandle(), $arrmixDependentDatabase['tuples'][0]['table'] ),
			pg_escape_literal( $objDatabase->getHandle(), $arrmixDependentDatabase['tuples'][0]['column'] ),
			pg_escape_literal( $objDatabase->getHandle(), '{dependent_databases,' . $strDependentDatabaseKeyName . ',tuples}' ),
			pg_escape_literal( $objDatabase->getHandle(), $strKey ) );

		return self::fetchTranslationKeys( $strSql, $objDatabase );
	}

}
?>
