<?php
use Psi\Eos\Voip\CPropertyCallSettings;
use Psi\Eos\Connect\CDatabases;

use Psi\Eos\Entrata\CPropertyMerchantAccounts;
use Psi\Eos\Entrata\CMerchantAccounts;
use Psi\Eos\Entrata\CPropertyChargeSettings;
use Psi\Eos\Entrata\CPropertyPreferences;
use Psi\Eos\Entrata\CPropertyGroupDocuments;
use Psi\Eos\Entrata\CDocuments;
use Psi\Eos\Entrata\CPropertyApplicationPreferences;
use Psi\Eos\Entrata\CPropertyApplications;
use Psi\Eos\Entrata\CPropertyIntegrationDatabases;
use Psi\Eos\Entrata\CScheduledPayments;
use Psi\Eos\Entrata\CAssetLocations;
use Psi\Eos\Entrata\CCompanyEmployees;
use Psi\Eos\Entrata\CWebsiteDomains;
use Psi\Eos\Entrata\CWebsiteProperties;
use Psi\Eos\Entrata\CWebsites;

use Psi\Eos\Admin\CContractProperties;
use Psi\Eos\Admin\CPsLeadDetails;
use Psi\Eos\Admin\CProperties;
use Psi\Eos\Admin\CPsProducts;
use Psi\Eos\Admin\CContractTerminationReasons;
use Psi\Eos\Admin\CPsLeads;
use Psi\Eos\Admin\CEmployees;
use Psi\Eos\Admin\CPersons;
use Psi\Eos\Admin\CClients;
use Psi\Eos\Admin\CContractProductStakeholders;
use Psi\Eos\Admin\CTaskWatchlists;
use Psi\Eos\Admin\CProductBundles;
use Psi\Eos\Admin\CPsProductRelationships;
use Psi\Eos\Admin\CContractProducts;
use Psi\Eos\Admin\CContractTerminationEvents;
use Psi\Eos\Admin\CAccounts;
use Psi\Eos\Admin\CAccountProperties;
use Psi\Eos\Admin\CContractProductOptions;
use Psi\Eos\Admin\CCompanyCharges;
use Psi\Eos\Admin\CContracts;
use Psi\Eos\Admin\CTaskReferences;
use Psi\Eos\Admin\CContractPropertyPricings;
use Psi\Eos\Admin\CContractTerminationRequests;
use Psi\Eos\Admin\CContractPropertyTerminations;
use Psi\Eos\Admin\CContractTerminationEventTypes;
use Psi\Eos\Admin\CPsDocuments;

class CContractTerminationRequest extends CBaseContractTerminationRequest {

	protected $m_objContract;

	protected $m_strClientName;

	protected $m_arrobjContracts;
	protected $m_arrobjContractProperties;
	protected $m_arrobjRollbackContractTerminationEvents;
	protected $m_arrobjPropertyUtilityTypes;
	protected $m_arrobjNewPropertyUtilityTypes;
	protected $m_arrobjUtilityUemSettings;
	protected $m_arrobjActivePropertyUtilitySettings;

	protected $m_arrstrPreferredPaymentMethods;

	protected $m_objRollbackContractTerminationEvent;

	protected $m_strContractTerminationReasonType;
	protected $m_strContractTerminationRequestStatus;
	protected $m_strContractTerminationReason;
	protected $m_strDeniedOn;

	protected $m_intPsProductId;

	protected $m_arrintTestClients	= [ CClient::ID_DB_XENTO_SYSTEMS, CClient::ID_DEMOOLD ]; // We are restricting all test database clients except Demo Account and Xento.

	protected $m_arrintDisabledPropertyIds;
	protected $m_arrstrFailedPropertyEventNames;
	protected $m_arrintFailedContractTerminationEventTypeIds;

	protected $m_arrobjContractTerminationEvents;

	protected $m_boolIsScriptApproval; // todo: Did not find any use of this var. Need to check and remove it's dependency.

	protected $m_boolIsSendEmails;
	protected $m_boolActiveProperty = false;

	const PROVIDE_ACH 		= 1;
	const CONVERT_TO_ACH	= 2;
	const PAY_INVOICE_NOW	= 3;
	const SKIP_PAYMENT		= 4;

	const TERMINATION_REMINDER_BEFORE_DAYS = 7;

	public static function createService() {
		return \Psi\Libraries\Container\CDependencyContainer::getInstance()->getService( static::class );
	}

	public function getPrefferedPaymentMethod() {
		$this->loadPrefferedPaymentMethod();

		return $this->m_arrstrPreferredPaymentMethods;
	}

	private function loadPrefferedPaymentMethod() {
		if( false == empty( $this->m_arrstrPreferredPaymentMethods ) ) {
			return;
		}

		$this->m_arrstrPreferredPaymentMethods = [
			self::PROVIDE_ACH => __( 'Provide ACH' ),
			self::CONVERT_TO_ACH => __( 'Convert to ACH' ),
			self::PAY_INVOICE_NOW => __( 'Pay Invoice(s) Now' ),
			self::SKIP_PAYMENT => __( 'Skip Payment' )
		];
	}

	public static $c_arrstrPreferredPaymentMethods = [
		self::PROVIDE_ACH => 'Provide ACH',
		self::CONVERT_TO_ACH => 'Convert to ACH',
		self::PAY_INVOICE_NOW => 'Pay Invoice(s) Now',
		self::SKIP_PAYMENT => 'Skip Payment'
	];

	public function __construct() {
		parent::__construct();
		$this->m_boolIsScriptApproval = false;
		$this->m_boolIsSendEmails = true;
	}

	/**
	*Get Functions
	*
	*/

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function getContractTerminationReasonType() {
		return $this->m_strContractTerminationReasonType;
	}

	public function getContractTerminationRequestStatus() {
		return $this->m_strContractTerminationRequestStatus;
	}

	public function getContractTerminationReason() {
		return $this->m_strContractTerminationReason;
	}

	public function getTerminatedContract() {
		return $this->m_objContract;
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function getDeniedOn() {
		return $this->m_strDeniedOn;
	}

	public function getIsSendEmails() {
		return $this->m_boolIsSendEmails;
	}

	/**
	*Set Functions
	*
	*/

	public function createContractTerminationEvent() {

		$objContractTerminationEvent = new CContractTerminationEvent();
		$objContractTerminationEvent->setDefaults();
  		$objContractTerminationEvent->setContractTerminationRequestId( $this->getId() );
  		$objContractTerminationEvent->setCid( $this->getCid() );

  		return $objContractTerminationEvent;
	}

	public function setClientName( $strClientName ) {
		$this->m_strClientName = $strClientName;
	}

	public function setContractTerminationReasonType( $strContractTerminationReasonType ) {
		$this->m_strContractTerminationReasonType = CStrings::strTrimDef( $strContractTerminationReasonType, 100, NULL, true );
	}

	public function setContractTerminationRequestStatus( $strContractTerminationRequestStatus ) {
		$this->m_strContractTerminationRequestStatus = CStrings::strTrimDef( $strContractTerminationRequestStatus, 100, NULL, true );
	}

	public function setContractTerminationReason( $strContractTerminationReason ) {
		$this->m_strContractTerminationReason = CStrings::strTrimDef( $strContractTerminationReason, 100, NULL, true );
	}

	public function setTerminatedContract( $objContract ) {
		$this->m_objContract = $objContract;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = $intPsProductId;
	}

	public function setIsScriptApproval( $boolIsScriptApproval ) {
		$this->m_boolIsScriptApproval = $boolIsScriptApproval;
	}

	public function setIsSendEmails( $boolIsSendEmails ) {
		$this->m_boolIsSendEmails = $boolIsSendEmails;
	}

	public function setDeniedOn( $strDeniedOn ) {
		$this->m_strDeniedOn = $strDeniedOn;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['company_name'] ) ) {
			$this->setClientName( $arrmixValues['company_name'] );
		}
		if( true == isset( $arrmixValues['contract_termination_reason_type'] ) ) {
			$this->setContractTerminationReasonType( $arrmixValues['contract_termination_reason_type'] );
		}
		if( true == isset( $arrmixValues['contract_termination_request_status'] ) ) {
			$this->setContractTerminationRequestStatus( $arrmixValues['contract_termination_request_status'] );
		}
		if( true == isset( $arrmixValues['ps_product_id'] ) ) {
			$this->setPsProductId( $arrmixValues['ps_psroduct_id'] );
		}
		if( true == isset( $arrmixValues['contract_termination_reason'] ) ) {
			$this->setContractTerminationReason( $arrmixValues['contract_termination_reason'] );
		}
		if( true == isset( $arrmixValues['denied_on'] ) ) {
			$this->setDeniedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['denied_on'] ) : $arrmixValues['denied_on'] );
		}

	}

	public function valCid() {
		$boolIsValid = true;
		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', __( 'client is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valContractTerminationReasonId() {
		$boolIsValid = true;
		if( true == is_null( $this->getContractTerminationReasonId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_termination_reason_id', __( 'Reason for Termination is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valTerminationDate() {
		$boolIsValid = true;
		if( true == is_null( $this->getTerminationDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_date', __( 'Termination date is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function valDeactivationDate( $boolIsUpdate ) {

		if( $boolIsUpdate == false ) {
			return true;
		}

		$boolIsValid = true;

		if( true == is_null( $this->getDeactivationDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deactivation_date', __( 'Last Day for Product Access is required.' ) ) );
		}

		if( false == is_null( $this->getDeactivationDate() ) && strtotime( date( 'Y-m-d' ) ) > strtotime( $this->getDeactivationDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deactivation_date', __( 'Last Day for Product Access should be equal/greater than today\'s date.' ) ) );
		}

		return $boolIsValid;
	}

	public function valTerminationReason( $boolValidateReason ) {

		$boolIsValid = true;
		if( true == is_null( $this->getTerminationReason() ) && true == $boolValidateReason ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'termination_reason', __( 'Additional Termination Note is required.' ) ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsUpdate = false, $boolValidateReason = true ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valContractTerminationReasonId();
				$boolIsValid &= $this->valTerminationReason( $boolValidateReason );
				$boolIsValid &= $this->valDeactivationDate( $boolIsUpdate );
				$boolIsValid &= $this->valTerminationDate();
				break;

			case 'appstore_validation':
				$boolIsValid &= $this->valDeactivationDate( $boolIsUpdate );

			case 'approval_or_dismiss':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valContractTerminationReasonId();
				$boolIsValid &= $this->valTerminationDate();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = false ) {

		return parent::update( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly );
	}

	/**
	*Other functions
	*
	*/

	public function sendTerminationRequestEmail( $arrobjContractProperties, $intUserId, $objAdminDatabase, $objEmailDatabase ) {

		if( false == valArr( $arrobjContractProperties ) ) {
			$arrobjContractProperties = CContractProperties::createService()->fetchContractPropertiesByContractTerminationRequestId( $this->getId(), $objAdminDatabase );
		}

		if( false == valArr( $arrobjContractProperties ) ) {
			return false;
		}

		$arrobjTempPsLeadDetails	= CPsLeadDetails::createService()->fetchPsLeadDetailsWithCompanyNameByCids( array_filter( array_keys( rekeyObjects( 'Cid', $arrobjContractProperties ) ) ), $objAdminDatabase );
		$objPsLeadDetail 			= array_pop( $arrobjTempPsLeadDetails );
		$arrintEmployeIds			= [];

		if( false == is_null( $objPsLeadDetail->getSupportEmployeeId() ) ) {
			$arrintEmployeIds[] = $objPsLeadDetail->getSupportEmployeeId();
		}

		if( false == is_null( $objPsLeadDetail->getSalesEmployeeId() ) ) {
			$arrintEmployeIds[] = $objPsLeadDetail->getSalesEmployeeId();
		}

		if( true == valArr( $arrintEmployeIds ) ) {

			require_once( PATH_PHP_INTERFACES . 'Interfaces.defines.php' );

			$arrobjProperties 				= CProperties::createService()->fetchPropertiesByIdsByCid( array_filter( array_keys( rekeyObjects( 'PropertyId', $arrobjContractProperties ) ) ), $objPsLeadDetail->getCid(), $objAdminDatabase );
			$arrobjPsProducts 				= CPsProducts::createService()->fetchPsProductsByIds( array_filter( array_keys( rekeyObjects( 'PsProductId', $arrobjContractProperties ) ) ), $objAdminDatabase );
			$objContractTerminationReason	= CContractTerminationReasons::createService()->fetchContractTerminationReasonById( $this->getContractTerminationReasonId(), $objAdminDatabase );
			$objPsLead						= CPsLeads::createService()->fetchPsLeadByCid( $objPsLeadDetail->getCid(), $objAdminDatabase );
			$arrobjEmployees 				= CEmployees::createService()->fetchEmployeesByIds( $arrintEmployeIds, $objAdminDatabase );

			$objSmarty 						= new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

			$objSmarty->assign( 'contract_properties', $arrobjContractProperties );
			$objSmarty->assign( 'ps_lead_detail', $objPsLeadDetail );
			$objSmarty->assign( 'properties', $arrobjProperties );
			$objSmarty->assign( 'ps_products', $arrobjPsProducts );
			$objSmarty->assign( 'employees', $arrobjEmployees );
			$objSmarty->assign( 'contract_termination_request', $this );

			$objSmarty->assign( 'termination_reason_type', $objContractTerminationReason->getName() );
			$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG );
			$objSmarty->assign( 'CONFIG_COMPANY_BASE_DOMAIN', CONFIG_COMPANY_BASE_DOMAIN );

			// Mail Body.
			$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'sales/contracts/termination_request_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

			$arrstrEmailIds	= [];

			// Fetch email addresses of users to trigger emails.
			if( true == valArr( $arrobjEmployees ) ) {
				$arrstrEmailIds = array_filter( array_keys( rekeyObjects( 'EmailAddress', $arrobjEmployees ) ), 'valStr' );
			}

            $arrmixReKeyContractProperties  = array_keys( rekeyObjects( 'PsProductId', $arrobjContractProperties ) );

            if( true == valArr( array_intersect( $arrmixReKeyContractProperties, CPsProduct::$c_arrintSeoProductIds ) ) ) {
                $arrstrEmailIds[] = CSystemEmail::SEO_EMAIL_ADDRESS;
            }

			if( true == in_array( CPsProduct::DOMAIN_RENEWAL, $arrmixReKeyContractProperties ) ) {
				$arrstrEmailIds[] = CSystemEmail::ABULLOCK_EMAIL_ADDRESS;
			}

			$arrstrEmailIds[] = CSystemEmail::CANCELLATIONS_EMAIL_ADDRESS;

			if( true == valArr( $arrstrEmailIds ) ) {

				$strSubject 		= 'Contract properties termination request of ' . $objPsLeadDetail->getClientName();

				$objSystemEmailLibrary	= new CSystemEmailLibrary();
				$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, $strSubject, $strHtmlContent, $strToEmailAddress = implode( ', ', $arrstrEmailIds ), $intIsSelfDestruct = 0, $strFromEmailAddress = CSystemEmail::INFO_EMAIL_ADDRESS );

				if( false == $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
					trigger_error( 'System emailed failed to insert.', E_USER_ERROR );
					return false;
				}
			}
			return true;
		}
	}

	public function sendPropertyTerminationEmail( $arrobjPropertyTerminatingContractProperties, $intUserId, $boolAppStore, $objAdminDatabase, $objEmailDatabase, $objConnectDatabase ) {

		$boolSent = false;

		if( false == valArr( $arrobjPropertyTerminatingContractProperties ) ) {
			return $boolSent;
		}

		$arrobjTerminatingProperties	= ( array ) CProperties::createService()->fetchPropertiesByIdsByCid( array_filter( array_keys( rekeyObjects( 'PropertyId', $arrobjPropertyTerminatingContractProperties ) ) ), $this->getCid(), $objAdminDatabase );
		$arrmixPersonsDetails			= CPersons::createService()->fetchPersonDetailsByCidByPersonTypeId( $this->getCid(), CPersonContactType::TERMINATION_EMAIL, $objAdminDatabase, true );

		if( false == valArr( $arrmixPersonsDetails ) ) {

			$arrobjPersons = CPersons::createService()->fetchPrimaryPersonsByCids( [ $this->getCid() ], $objAdminDatabase );

			if( false == valArr( $arrobjPersons ) ) {
				return $boolSent;
			}

			foreach( $arrobjPersons as $objPerson ) {
				$arrmixPersonsDetails[] = [ 'name_first' => $objPerson->getNameFirst(), 'email_address' => $objPerson->getEmailAddress(), 'company_user_id' => $objPerson->getCompanyUserId() ];
			}
		}

		require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';

		$objSystemEmailLibrary	= new CSystemEmailLibrary();

		$arrmixTemplateParameters								= [];
		$arrmixTemplateParameters['CONFIG_COMPANY_BASE_DOMAIN']	= CONFIG_COMPANY_BASE_DOMAIN;
		$arrmixTemplateParameters['logo_url']					= CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG;
		$arrmixTemplateParameters['terminating_properties']		= $arrobjTerminatingProperties;
		$arrmixTemplateParameters['last_day_of_product_access'] = $this->getDeactivationDate();

		$objLocaleContainer = CLocaleContainer::createService();

		if( false == valObj( $objConnectDatabase, 'CDatabase' ) ) {
			$objConnectDatabase = CDatabases::createService()->createConnectDatabase();
		}

		$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $this->getCid(), $objConnectDatabase );
		$objClientDatabase->open();
		$objClient = CClients::createService()->fetchClientById( $this->getCid(), $objClientDatabase );

		foreach( $arrmixPersonsDetails as $arrmixPersonDetails ) {

			if( false == CValidation::validateEmailAddresses( $arrmixPersonDetails['email_address'] ) ) {
				continue;
			}

			$objLocaleContainer->initializeByClient( $objClient, $objClientDatabase );

			if( true == valId( $arrmixPersonDetails['company_user_id'] ) ) {
				$objCompanyEmployee = CCompanyEmployees::createService()->fetchCompanyEmployeeByCompanyUserIdByCid( $arrmixPersonDetails['company_user_id'], $this->getCid(), $objClientDatabase );

				if( true == valObj( $objCompanyEmployee, 'CCompanyEmployee' ) && true == valStr( $objCompanyEmployee->getPreferredLocaleCode() ) ) {
					$objLocaleContainer->setLocaleCode( $objCompanyEmployee->getPreferredLocaleCode() );
				}
			}

			$arrmixTemplateParameters['name_first'] = $arrmixPersonDetails['name_first'];

			$objRenderTemplate		= new CRenderTemplate( 'sales/contracts/property_termination_email.tpl', PATH_INTERFACES_CLIENT_ADMIN, false, $arrmixTemplateParameters );
			$strHtmlContent			= $objRenderTemplate->nestedFetchTemplate( PATH_INTERFACES_CLIENT_ADMIN . 'sales/contracts/property_termination_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
			$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::COMPANY_EMPLOYEE, __( 'Property Termination: Best Practices' ), $strHtmlContent, $arrmixPersonDetails['email_address'], 0, CSystemEmail::INFO_EMAIL_ADDRESS );

			if( false == $objSystemEmail->validate( VALIDATE_INSERT ) || false == $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
				continue;
			}

			$boolSent = true;
		}

		$objLocaleContainer->setLocaleCode( true == $boolAppStore ? $objClient->getLocaleCode() : CLocale::DEFAULT_LOCALE );

		return $boolSent;
	}

	public function createTasksForTermination( $arrobjContractProperties, $objAdminDatabase, $intUserId, $objObjectStorageGateway = NULL ) {
		$arrobjTaskWatchLists = [];
		$arrintTaskWatchListUserIds = [ CUser::ID_LAURA_PARKER, CUser::ID_BLAIKE_BAIRD ];
		$strErrorMessage = '';
		// create task for termination
		$objClient = CClients::createService()->fetchClientById( $this->getCid(), $objAdminDatabase );
		$arrintPsProductIds = array_keys( rekeyObjects( 'PsProductId', $arrobjContractProperties ) );

		if( true == valArr( $arrintPsProductIds ) ) {
			$arrobjContractProductsWiseStakeholders = CContractProductStakeholders::createService()->fetchActiveContractProductTerminationStakeholdersByProductIds( $arrintPsProductIds, $objAdminDatabase );
			if( true == valArr( $arrobjContractProductsWiseStakeholders ) ) {
				$arrobjContractProductStakeholders = rekeyObjects( 'productId', $arrobjContractProductsWiseStakeholders, true );
			}
		}

		$intPropertyCount = \Psi\Libraries\UtilFunctions\count( array_keys( rekeyObjects( 'PropertyId', $arrobjContractProperties ) ) );
		$strPropertyName = '';
		if( 1 == $intPropertyCount ) {
			foreach( $arrobjContractProperties as $objContractProperties ) {
				$strPropertyName = $objContractProperties->getPropertyName();
				break;
			}
		} else {
			$strPropertyName .= $intPropertyCount . ' Properties';
		}

		$objTask = new CTask();
		$objTask->fetchNextId( $objAdminDatabase );
		$objTask->setTaskPriorityId( CTaskPriority::NORMAL );
		$objTask->setTaskStatusId( CTaskStatus::UNUSED );
		$objTask->setCid( $objClient->getId() );
		$objTask->setTitle( ' Term ' . $this->getTerminationDate() . ' ' . $objClient->getCompanyName() . ' ' . $strPropertyName );
		$strTerminationReason 			= CContractTerminationReason::$c_arrstrContractTerminationReasons[$this->getContractTerminationReasonId()];

		$arrstrContractTerminationEventDocs = [];

		$objContractTerminationEvent = CContractTerminationEvents::createService()->fetchContractTerminationEventDetailsByContractTerminationRequestIdByContractTerminationEventTypeId( $this->getId(), CContractTerminationEventType::TERMINATION_DOCUMENT, $objAdminDatabase );

		if( valObj( $objContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$arrstrContractTerminationEvent = json_decode( $objContractTerminationEvent->getEventParameters(), true );
			if( valArr( $arrstrContractTerminationEvent['DOCUMENT_NAME'] ) ) {
				$arrstrContractTerminationEventDocs = array_values( $arrstrContractTerminationEvent['DOCUMENT_NAME'] );
			}
		}

		$strHtmlBody = '<table border="0" style="font-size: 12px;">';
		$strHtmlBody .= '<tr><td>Termination Request for	</td><td>:' . $objClient->getCompanyName() . '</td></tr><tr><td>Termination Request Id</td><td> :' . ( int ) $this->getId() . '</td></tr>';
		$strHtmlBody .= '<tr><td>Reason for Termination		</td><td>:' . $strTerminationReason . '</td></tr><tr><td>Additional Comments</td><td> :' . $this->getTerminationReason() . '</td></tr>';
		$strHtmlBody .= '<tr><td>Last Day for Product Access	</td><td>:' . date( 'm-d-y', strtotime( $this->getDeactivationDate() ) ) . '</td></tr>';
		$strHtmlBody .= '<tr><td>Last Day Billed				</td><td>:' . date( 'm-d-y', strtotime( $this->getTerminationDate() ) ) . '</td></tr>';
		$strHtmlBody .= '</table>';

		$intTaskUserId = NULL;
		$arrmixBillingRepDetails = CPsLeadDetails::createService()->fetchBillingRepresentativeEmailAddressByCompanyId( $this->getCid(), $objAdminDatabase, $boolIsReturnWithUserId = true );

		$objSupportEmployee = CEmployees::createService()->fetchSupportEmployeeByCid( $this->getCid(), $objAdminDatabase );

		if( true == in_array( CPsProduct::RESIDENT_PAY, $arrintPsProductIds ) ) {
			$objTask->setTaskTypeId( CTaskType::MERCHANT_ACCOUNT );

			$arrobjContractProductStakeholdersForRP = $arrobjContractProductStakeholders[CPsProduct::RESIDENT_PAY];
			foreach( $arrobjContractProductStakeholdersForRP As $objContractProductStakeholderRP ) {
				if( true == $objContractProductStakeholderRP->getIsTerminationTaskUser() ) {
					$intTaskUserId = $objContractProductStakeholderRP->getUserId();
				}
			}

            if( \Psi\Libraries\UtilFunctions\count( $arrintPsProductIds ) > 1 ) {
				$arrintTaskWatchListUserIds[] = ( true == valArr( $arrmixBillingRepDetails ) ) ? $arrmixBillingRepDetails['id'] : CUser::ID_BILLING_BILLING;
			}
			$strHtmlBody .= '<br><div>Elijah Moller : Please disable this Merchant Account after all accounts have settled.</div>';

		} else {
			$objTask->setTaskTypeId( CTaskType::ACCOUNTING );
			// if no billing rep then assigning to Billing Billing
			$intTaskUserId = ( true == valArr( $arrmixBillingRepDetails ) ) ? $arrmixBillingRepDetails['id'] : CUser::ID_BILLING_BILLING;
		}

		if( true == valStr( $arrmixBillingRepDetails['preferred_name'] ) ) {
			$strHtmlBody .= '<div>' . $arrmixBillingRepDetails['preferred_name'] . ' : Please verify that billing has been disabled.' . '</div>';
		} elseif( true == valStr( $arrmixBillingRepDetails['name_first'] ) && true == valStr( $arrmixBillingRepDetails['name_last'] ) ) {
			$strHtmlBody .= '<div>' . $arrmixBillingRepDetails['name_first'] . ' ' . $arrmixBillingRepDetails['name_last'] . ' : Please verify that billing has been disabled.' . '</div>';
		}
		if( true == valArr( $arrobjContractProductsWiseStakeholders ) ) {
			$strProductStakeholderName   = '';
			$arrstrProductName = [];
			foreach( $arrobjContractProductsWiseStakeholders as $objContractProductStakeholder ) {
				$arrstrProductName[] = $objContractProductStakeholder->getProductName();
				$objContractProductStakeholderNext = next( $arrobjContractProductsWiseStakeholders );
				if( true == empty( $objContractProductStakeholderNext ) || ( false == empty( $objContractProductStakeholderNext ) && $objContractProductStakeholder->getUserId() != $objContractProductStakeholderNext->getUserId() ) ) {
					$arrintTaskWatchListUserIds[] = $objContractProductStakeholder->getUserId();
					$strProductStakeholderName = $objContractProductStakeholder->getEmployeeName();
					$strHtmlBody .= '<div>' . $strProductStakeholderName . ' : Please terminate ' . implode( ',', array_unique( $arrstrProductName ) ) . '.</div>';
					$arrstrProductName = [];
				}
			}
		}
		$arrintTaskWatchListUserIds = array_unique( $arrintTaskWatchListUserIds );

		if( true == valObj( $objSupportEmployee, 'CEmployee' ) && true == valStr( $objSupportEmployee->getPreferredName() ) ) {
			$strHtmlBody .= '<div>' . $objSupportEmployee->getPreferredName() . '  : Please see <a href=\'https://docs.google.com/a/entrata.com/spreadsheets/d/1VakqRV1OrKvNztOx776NG860Yoz7jbyzKj-UDnkp_R0/edit?usp=sharing' . '\'>Document</a></div>';
		}

		$strHtmlBody .= '<br><div>If there are changes to this termination or you are unable to disable a property please email ';
		$strHtmlBody .= '<a href=\'mailto:' . CSystemEmail::CANCELLATIONS_EMAIL_ADDRESS . '\'>' . CSystemEmail::CANCELLATIONS_EMAIL_ADDRESS . '</a></div>';

		$objTask->setDescription( $strHtmlBody );
		$objTask->setUserId( $intTaskUserId );

		$arrintTaskWatchListUserIds[] = $intTaskUserId;
		$arrintSupportUserId = CPsLeadDetails::createService()->fetchSupportUserIdByCid( $this->getCid(), $objAdminDatabase );
		$arrintTaskWatchListUserIds[] = ( true == valArr( $arrintSupportUserId ) && true == valArr( $arrintSupportUserId[0] ) && true == valId( $arrintSupportUserId[0]['id'] ) && ( $arrintSupportUserId[0]['id'] != CUser::ID_ASSOCIATE_ACCOUNT_MANAGEMENT ) ) ? $arrintSupportUserId[0]['id'] : CUser::ID_TEGAN_PERRY;

		$objTaskWatchList = $objTask->createTaskWatchList();
		foreach( $arrintTaskWatchListUserIds as $intTaskWatchListUserId ) {
			$objCloneTaskWatchList = clone $objTaskWatchList;
			$objCloneTaskWatchList->setUserId( $intTaskWatchListUserId );
			$arrobjTaskWatchLists[] = $objCloneTaskWatchList;
		}

		if( false == $objTask->insert( $intUserId, $objAdminDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to create termination task.' ) );
			return false;
		}

		if( true == valObj( $objTask, 'CTask' ) ) {
			$objTaskReference = new CTaskReference();
			$objTaskReference->setTaskReferenceTypeId( CTaskReferenceType::CONTRACT_TERMINATION );
			$objTaskReference->setReferenceNumber( ( int ) $this->getId() );
			$objTaskReference->setTaskId( $objTask->getId() );

			$objTaskCompany = new CTaskCompany();
			$objTaskCompany->setCid( $objClient->getId() );
			$objTaskCompany->setTaskId( $objTask->getId() );
			$objTaskCompany->setUserId( $intUserId );
			$objTaskCompany->setIsPublished( 0 );
		}

		if( true == valArr( $arrobjTaskWatchLists ) && false == CTaskWatchlists::createService()->bulkInsert( $arrobjTaskWatchLists, $intUserId, $objAdminDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to add stakeholders.' ) );
			return false;
		}

		if( true == valObj( $objTaskReference, 'CTaskReference' ) && ( false == $objTaskReference->insert( $intUserId, $objAdminDatabase ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to add task references.' ) );
			return false;
		}

		if( true == valObj( $objTaskCompany, 'CTaskCompany' ) && ( false == $objTaskCompany->insert( $intUserId, $objAdminDatabase ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to add task company.' ) );
			return false;
		}

				// Validate Task Attachment objects.
				if( true == valArr( $arrstrContractTerminationEventDocs ) ) {
					$arrobjTaskAttachments	= [];
					$objTaskAttachment				= $objTask->createTaskAttachment();

					$intAttachmentCount				= \Psi\Libraries\UtilFunctions\count( $arrstrContractTerminationEventDocs );
					for( $intCounter = 0; $intCounter < $intAttachmentCount; $intCounter++ ) {
						if( 0 < \Psi\Libraries\UtilFunctions\count( $arrstrContractTerminationEventDocs[$intCounter] ) ) {
							$objCloneTaskAttachment = clone $objTaskAttachment;
							$objCloneTaskAttachment->setFileName( $arrstrContractTerminationEventDocs[$intCounter] );
							$objCloneTaskAttachment->setFileTmpName( PATH_MOUNTS_DOCUMENTS_PS_LEADS . $objClient->getPsLeadId() . '/' . $arrstrContractTerminationEventDocs[$intCounter] );
							$objCloneTaskAttachment->setUrl( $arrstrContractTerminationEventDocs[$intCounter] );
							$objCloneTaskAttachment->setId( NULL );

							if( true == valObj( $objCloneTaskAttachment, 'CTaskAttachment' ) && false == $objCloneTaskAttachment->validate( VALIDATE_INSERT, $objAdminDatabase ) ) {
									$strErrorMessage .= getConsolidatedErrorMsg( $objCloneTaskAttachment );
									$boolIsValid = false;
									break;
							}

							$arrobjTaskAttachments[] = $objCloneTaskAttachment;
						}
					}
				}

				// Inserting Into Task Attachments Table.
				if( true == valArr( $arrstrContractTerminationEventDocs ) ) {
					foreach( $arrobjTaskAttachments as $objTaskAttachment ) {
						$objTaskAttachment->setTaskId( $objTask->getId() );
						if( false == is_null( $objTaskAttachment->getUrl() ) && 0 < strlen( $objTaskAttachment->getUrl() ) ) {

							$strFileTmpPath = NULL;

							$objPsDocument = CPsDocuments::createService()->fetchPsDocumentByFileNameByPsLeadId( $objTaskAttachment->getFileName(), $objClient->getPsLeadId(), $objAdminDatabase );

							if( valObj( $objPsDocument, 'CPsDocument' ) ) {
								$strFileTmpPath = $objPsDocument->downloadObject( $objObjectStorageGateway, $objTaskAttachment->getFileName(), 'attachment', true );
							} else {
								$objTempHelper						= new \Psi\Libraries\ObjectStorage\Utils\CTempObjectStorageHelper( $objObjectStorageGateway, CONFIG_OSG_BUCKET_TEMP_DOCUMENTS );
								$arrmixRequest						= [];
								$arrmixRequest['key'] = sprintf( '%d/%s/%d/%d/%d/%s', $this->getCid(), 'documents', date( 'Y' ), date( 'm' ), date( 'd' ), $objTaskAttachment->getFileName() );
								$arrmixRequest['outputFile'] = 'temp';
								$objObjectStorageGatewayResponse	= $objTempHelper->getObject( $arrmixRequest );
								if( false == $objObjectStorageGatewayResponse->hasErrors() ) {
									$strFileTmpPath = $objObjectStorageGatewayResponse['outputFile'];
								}
							}

							$objTaskAttachment->setFileTmpName( $strFileTmpPath );
							$objTaskAttachment->setObjectStorageGateway( $objObjectStorageGateway );

							if( valStr( $objTaskAttachment->getFileTmpName() ) && !$objTaskAttachment->insert( $intUserId, $objAdminDatabase, true ) ) {
								$objAdminDatabase->rollback();
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to add task attachment.' ) );
								return false;
							}
						}
					}
				}

		$objAdminDatabase->commit();

		$objCompanyUser = new CCompanyUser();
		$objCompanyUser->setId( $intUserId );
		$boolIsSendEmailAlert = $objTask->sendEmailAlert( $objCompanyUser, $objAdminDatabase, NULL, true );
		if( false == $boolIsSendEmailAlert ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to send email for termination task.' ) );
			return false;
		}

		return true;
	}

	public function generateTerminationRequestAndProcessTerminationEvents( $arrobjContractProperties, $objUser, $objAdminDatabase, $objEmailDatabase, $boolIsAppstore = false, $arrmixContractTerminationEvents = NULL, $boolDraftRequest = false, $boolIsTestDatabase = false, $boolIsFromContract = false, $boolIsContractTerminationRequest = false, $objConnectDatabase = NULL, $objVoipDatabase = NULL, $boolSendMail = true, $objObjectStorageGateway = NULL ) {

		if( false == valArr( $arrobjContractProperties ) ) {
			return;
		}

		// If property is fully terminating, add event "Property Termination Email".
		$arrobjPropertyTerminatingContractProperties = self::getPropertyTerminatingContractProperties( $arrobjContractProperties, $objAdminDatabase );
		if( true == valArr( $arrobjPropertyTerminatingContractProperties ) && true == valArr( $arrmixContractTerminationEvents ) && true == $boolSendMail ) {

			foreach( $arrmixContractTerminationEvents as $intContractPropertyId => $arrmixContractTerminationEvent ) {

				if( true == array_key_exists( $intContractPropertyId, $arrobjPropertyTerminatingContractProperties ) ) {

					$arrmixContractTerminationEvent[CContractTerminationEventType::PROPERTY_TERMINATION_EMAIL] = NULL;
					$arrmixContractTerminationEvents[$intContractPropertyId] = $arrmixContractTerminationEvent;
				}
			}
		}

		if( strtotime( $this->getDeactivationDate() ) > strtotime( $this->getTerminationDate() ) ) {
			foreach( $arrobjContractProperties as $objContractProperty ) {
				$arrmixContractTerminationEvents[$objContractProperty->getId()][CContractTerminationEventType::TERMINATE_CONTRACT_PROPERTY] = [];
			}
		}

		$boolIsAutoApproved = false;

		// validation for auto approval...
		if( ( true == in_array( $this->getContractTerminationReasonId(), [ CContractTerminationReason::LOST_MANAGEMENT, CContractTerminationReason::SOLD_ASSET_MANAGEMENT_LOST ] ) || true == $boolIsTestDatabase || ( false == $boolIsAppstore && strtotime( date( 'Y-m-d' ) ) == strtotime( $this->getDeactivationDate() ) ) )
			 && false == is_null( $this->getDeactivationDate() )
			 && false == is_null( $this->getTerminationDate() )
			 && false == $boolDraftRequest ) {
			$boolIsAutoApproved = true;
			$this->setPostedBy( SYSTEM_USER_ID );
			$this->setPostedOn( 'NOW()' );
		}

		// if request comes from app store then we need to set its status as client approved (2)
		if( true == $boolIsAppstore && false == $boolDraftRequest && false == $boolIsTestDatabase ) {
			$this->setContractTerminationRequestStatusId( CContractTerminationRequestStatus::CLIENT_APPROVED );
		}

		// process contract termination request...
		$intUserId = ( true == valObj( $objUser, 'CUser' ) ) ? $objUser->getId() : SYSTEM_USER_ID;

		switch( NULL ) {
			default:

				$boolIsValid = true;

				if( true == $boolIsAppstore ) {
					if( false == $this->validate( 'appstore_validation' ) ) {
						$boolIsValid = false;
						break;
					}
				} else {
					if( false == $this->validate( VALIDATE_INSERT, $boolIsFromContract ) ) {
						$boolIsValid = false;
						break;
					}
				}

				$objAdminDatabase->begin();

				if( false == $this->insertOrUpdate( $intUserId, $objAdminDatabase ) ) {
					$this->addErrorMsgs( $this->getErrorMsgs() );
					$objAdminDatabase->rollback();
					break;
				}

				if( false == $boolDraftRequest ) {
					if( false == $this->createContractTerminationEvents( $arrmixContractTerminationEvents, $intUserId, $objAdminDatabase, $arrobjContractProperties ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_DATABASE, '', __( 'Unable to create contract termination events.' ) ) );
						$objAdminDatabase->rollback();
						break;
					}

					// Single Product Bundle Termination from bundle
					$this->singlePsProductBundleTermination( $this->getId(), array_keys( $arrobjContractProperties ), $intUserId, $objAdminDatabase );

					$strCondition = '';

					if( false == $boolIsContractTerminationRequest ) {
						$strCondition = ' AND contract_termination_request_id IS NULL ';
					}

					$strUpdateSql = 'UPDATE
									 contract_properties
									SET
									 contract_termination_request_id = ' . $this->getId() . '
									WHERE
									 id IN ( ' . implode( ',', array_keys( $arrobjContractProperties ) ) . ' )' . $strCondition;

					if( false == $objAdminDatabase->execute( $strUpdateSql ) ) {
						$this->addErrorMsg( new CErrorMsg( '', '', __( 'Unable to update contract properties.' ) ) );
						$objAdminDatabase->rollback();
						break;
					}

					$boolExecutePressingEvents = true;

					if( true == $boolIsAutoApproved ) {
						$boolExecutePressingEvents = false;
					}

					if( false == $this->processContractTerminationEvents( $intUserId, $objAdminDatabase, $objEmailDatabase, $arrobjContractProperties, $boolExecutePressingEvents, $objLogDatabase = NULL, $boolIsAppstore, $arrobjPropertyTerminatingContractProperties, $objConnectDatabase, $objVoipDatabase, $objObjectStorageGateway ) ) {
						$this->addErrorMsg( new CErrorMsg( '', '', __( 'Unable to process immediate events.' ) ) );
						$objAdminDatabase->rollback();
						break;
					}
				}

				if( 'production' != CONFIG_ENVIRONMENT || $this->getCid() == CClient::ID_DB_XENTO_SYSTEMS ) {

					$arrobjContractPropertyTerminations = [];

					if( valId( $this->getId() ) ) {

						$arrobjTerminatedContractProperties = CContractProperties::createService()->fetchContractPropertiesByContractTerminationRequestId( $this->getId(), $objAdminDatabase );

						if( valArr( $arrobjTerminatedContractProperties ) ) {
							$intContractTerminationReasonId = $this->getContractTerminationReasonId();
							array_walk( $arrobjTerminatedContractProperties, function ( $objTerminatedContractProperty ) use ( &$arrobjContractPropertyTerminations, &$intContractTerminationReasonId ) {
								$arrobjContractPropertyTerminations[] = $objTerminatedContractProperty->createContractPropertyTermination( $intContractTerminationReasonId );
							} );
						}
					}

					if( valArr( $arrobjContractPropertyTerminations ) && !CContractPropertyTerminations::createService()->bulkInsert( $arrobjContractPropertyTerminations, $intUserId, $objAdminDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( '', '', __( 'Failed to insert Contract Property Termination record(s).' ) ) );
						$objAdminDatabase->rollback();
						break;
					}
				}

				$objAdminDatabase->commit();

				return $this;
		}

		return $this;
	}

	public function singlePsProductBundleTermination( $intContractTerminationRequestId, $arrintContractPropertiesIds, $intUserId, $objAdminDatabase ) {

		$arrobjContractPropertiesBundleDetails	= CContractProperties::createService()->fetchAssociateContractPropertiesIdsOfBundleIdsByContractPropertyIds( $arrintContractPropertiesIds, true, $objAdminDatabase );

		if( false == valArr( $arrobjContractPropertiesBundleDetails ) ) {
			return;
		}

		$arrobjProductsBundlesDetails		= [];
		$arrobjPsProductRelationshipDetails	= [];
		$arrobjContractProductDetails		= [];
		$objContractTerminationEventDetails	= [];
		$arrstrPsProductBundleNames			= [];
		$strOldPsProductBundleName			= '';
		$intOldBundleid						= 0;

		foreach( $arrobjContractPropertiesBundleDetails as $arrstrContractPropertiesBundleDetails ) {

			if( $intOldBundleid != $arrstrContractPropertiesBundleDetails['bundle_ps_product_id'] ) {
				$strOldPsProductBundleName = '';
			}

			$intOldBundleid = $arrstrContractPropertiesBundleDetails['bundle_ps_product_id'];

			// Array which stores the contract property ids Which suppose to terminate
			$arrintTerminatedBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']] = [];

			// Array which stores the contract property ids for which new bundle id needs to be update
			$arrintNewBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']] = explode( ',', $arrstrContractPropertiesBundleDetails['contract_property_id'] );

			$intCount = 0;

			foreach( $arrintContractPropertiesIds as $intContractPropertyId ) {

				if( ( $intKey = array_search( $intContractPropertyId, $arrintNewBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']] ) ) !== false ) {
					$intCount ++;
					if( $intCount > 1 ) {
						unset( $arrintNewBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']] );
						unset( $arrintTerminatedBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']] );
						break;
					}

					unset( $arrintNewBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']][$intKey] );
					$arrintTerminatedBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']][] = $intContractPropertyId;
				}
			}

			$strJson = '{';

			// check whether only single product need to terminate from bundle.
			if( true == valArr( $arrintNewBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']] ) && true == valArr( $arrintTerminatedBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']] ) ) {

				// Calculating ps product bundle weight
				$intCid								= 0;
				$intSumOfTotalRecuringAmount		= 0;
				$fltSumOfActiveProductPercentage	= 0;
				$fltTerminatedProductPercentage		= 0;
				$fltActiveProductPercentage			= 0;
				$fltActiveProductPercentageNew		= 0;

				$arrfltActiveProductPercentage			= [];
				$arrfltActiveProductAmount				= [];
				$arrfltPsProductRelationshipPercentage	= [];
				$arrfltContractProductRecuringAmount	= [];
				$arrintProductUnitCount					= [];

				// Create New bundle (ps_product) from old bundle
				$arrstrSeperateBundleName = [];

				$arrobjProductBundles = CProductBundles::createService()->fetchProductbundlesByIdsOrderByName( explode( ',', $arrstrContractPropertiesBundleDetails['bundle_ps_product_id'] ), $objAdminDatabase );

				if( true == valArr( $arrobjProductBundles ) ) {

					foreach( $arrobjProductBundles as $objProductBundles ) {

						$strPsProductBundleName		= $objProductBundles->getName();
						$arrstrSeperateBundleName	= explode( '-', $strPsProductBundleName );

						// Creating Name For New PS Product Bundle
						if( true == is_null( $arrstrSeperateBundleName[1] ) ) {
							$strPsProductBundleName .= ' (Auto)-1';
						} else {
							$strPsProductBundleName = $arrstrSeperateBundleName[0] . '-' . ( $arrstrSeperateBundleName[1] + 1 );
						}

						$intCntPsBundleName = CProductBundles::createService()->fetchProductBundleIdsCountByBundleName( $strPsProductBundleName, $objProductBundles->getPsLeadId(), $objAdminDatabase );

						if( 0 < $intCntPsBundleName ) {
							$strPsProductBundleName = CProductBundles::createService()->fetchProductBundlesByBundleName( $strPsProductBundleName, $objAdminDatabase );
						}

						if( 0 <= \Psi\CStringService::singleton()->strcmp( $strOldPsProductBundleName, $strPsProductBundleName ) ) {

							$arrstrSeperatePsProductBundleName = explode( '-', $strOldPsProductBundleName );
							$strPsProductBundleName = $arrstrSeperatePsProductBundleName[0] . '-' . ( $arrstrSeperatePsProductBundleName[1] + 1 );
						}

						if( true == in_array( $strPsProductBundleName, $arrstrPsProductBundleNames ) ) {
							$arrstrSeperatePsProductBundleName = explode( '-', $arrstrPsProductBundleNames[\Psi\Libraries\UtilFunctions\count( $arrstrPsProductBundleNames ) - 1] );
							$strPsProductBundleName = $arrstrSeperatePsProductBundleName[0] . '-' . ( $arrstrSeperatePsProductBundleName[1] + 1 );
						}

						$strOldPsProductBundleName = $strPsProductBundleName;

						$arrstrPsProductBundleNames[] = $strOldPsProductBundleName;

						$intBundelNewId = $objProductBundles->fetchNextId( $objAdminDatabase );

						$objProductBundles->setId( $intBundelNewId );
						$objProductBundles->setName( $strPsProductBundleName );
						$objProductBundles->setDescription( $strPsProductBundleName );
						$objProductBundles->setPsProductDisplayTypeId( CPsProductDisplayType::CLIENT_ADMIN_ONLY );
						$objProductBundles->setCreatedOn( 'NOW()' );
						$objProductBundles->setUpdatedOn( 'NOW()' );

						$arrobjProductsBundlesDetails[] = $objProductBundles;
					}
				}

				// Calculating Ps Product Bundle weight.

				$intContractId			= CContractProperties::createService()->fetchContractIdByContractPropertyIds( $arrintNewBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']], $objAdminDatabase );
				$arrobjProductDetails	= CContractProperties::createService()->fetchProductsDetailsByContractIdByPsBundleIdByPropertyId( $intContractId, $arrstrContractPropertiesBundleDetails['bundle_ps_product_id'], $arrstrContractPropertiesBundleDetails['property_id'], $objAdminDatabase );

				if( false == valArr( $arrobjProductDetails ) ) {
					return NULL;
				}

				$strContractPropertyidCommaSeperated 			= '';
				$strContractPropertyidPercentageCommaSeperated	= '';

				foreach( $arrobjProductDetails as $arrstrProductDetails ) {

					$intCid = $arrstrProductDetails['cid'];
					$intSumOfTotalRecuringAmount += $arrstrProductDetails['monthly_recurring_amount'];
					$strContractPropertyidCommaSeperated 			= $strContractPropertyidCommaSeperated . ',' . $arrstrProductDetails['contract_property_id'];
					$strContractPropertyidPercentageCommaSeperated	= $strContractPropertyidPercentageCommaSeperated . ',' . $arrstrProductDetails['percent_weight'];

					if( false == in_array( $arrstrProductDetails['contract_property_id'], $arrintTerminatedBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']] ) ) {
						$fltSumOfActiveProductPercentage += $arrstrProductDetails['percent_weight'];
						$arrfltActiveProductPercentage[$arrstrProductDetails['contract_property_id']]	= $arrstrProductDetails['percent_weight'];
						$arrintProductUnitCount[$arrstrProductDetails['contract_property_id']]			= $arrstrProductDetails['unit_count'];

					} else {
						$fltTerminatedProductPercentage += $arrstrProductDetails['percent_weight'];
					}
				}

				if( 0 >= $fltSumOfActiveProductPercentage ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'At least one product have percentage weight' ) );
					$objAdminDatabase->rollback();
					return NULL;
					break 1;
				}

				$strContractPropertyidCommaSeperated 			= ltrim( $strContractPropertyidCommaSeperated, ',' );
				$strContractPropertyidPercentageCommaSeperated	= ltrim( $strContractPropertyidPercentageCommaSeperated, ',' );

				$strJson .= '"contract_id":"' . $intContractId . '","bundle_id":"' . $arrstrContractPropertiesBundleDetails['bundle_ps_product_id'] . '","new_bundle_id":"' . $intBundelNewId . '","total_bundle_amount":"' . $intSumOfTotalRecuringAmount . '","contract_property_ids":"' . $strContractPropertyidCommaSeperated . '","contract_property_id_percentage":"' . $strContractPropertyidPercentageCommaSeperated . '"}';

				foreach( $arrintNewBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']] as $intContractPropertyId ) {

					$fltActiveProductPercentage			= ( $arrfltActiveProductPercentage[$intContractPropertyId] * $fltTerminatedProductPercentage ) / 100;
					$fltActiveProductPercentageNew	= ( $fltActiveProductPercentage * 100 ) / $fltSumOfActiveProductPercentage;

					$arrfltActiveProductPercentage[$intContractPropertyId] += $fltActiveProductPercentageNew;
					$arrfltActiveProductAmount[$intContractPropertyId]		= __( '{%f, 0, p:2}', [ ( $arrfltActiveProductPercentage[$intContractPropertyId] * $intSumOfTotalRecuringAmount ) / 100 ] );
					$arrobjProductId = CContractProperties::createService()->fetchProductIdsByContractProperyId( $intContractPropertyId, $objAdminDatabase );

					if( 0 == $arrintProductUnitCount[$intContractPropertyId] ) {
						$arrfltContractProductRecuringAmount[$arrobjProductId[0]['bundle_ps_product_id']][$arrobjProductId[0]['ps_product_id']] = 0;
					} else {
						$arrfltContractProductRecuringAmount[$arrobjProductId[0]['bundle_ps_product_id']][$arrobjProductId[0]['ps_product_id']] = $arrfltActiveProductAmount[$intContractPropertyId] / $arrintProductUnitCount[$intContractPropertyId];
					}

					$arrfltPsProductRelationshipPercentage[$arrobjProductId[0]['bundle_ps_product_id']][$arrobjProductId[0]['ps_product_id']] = $arrfltActiveProductPercentage[$intContractPropertyId];
				}

				// Inserting record in ps relationship
				$arrobjPsProductRelationship = CPsProductRelationships::createService()->fetchPsProductRelationshipsByBundlePsProductIdByContractPropertyIds( $arrstrContractPropertiesBundleDetails['bundle_ps_product_id'], $arrintNewBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']], $objAdminDatabase );

				if( false == valArr( $arrobjPsProductRelationship ) ) {
					return NULL;
				}
				foreach( $arrobjPsProductRelationship as $objPsProductRelationship ) {
					$objPsProductRelationship->setId( NULL );
					$objPsProductRelationship->setPercentWeight( $arrfltPsProductRelationshipPercentage[$objPsProductRelationship->getBundlePsProductId()][$objPsProductRelationship->getPsProductId()] );
					$objPsProductRelationship->setBundlePsProductId( $intBundelNewId );
					$objPsProductRelationship->setCreatedOn( 'NOW()' );
					$objPsProductRelationship->setUpdatedOn( 'NOW()' );
				}
				$arrobjPsProductRelationshipDetails[] = $arrobjPsProductRelationship;

				// Inserting new bundle product entries in Contract_Product
				$arrobjContractProducts = CContractProducts::createService()->fetchContractProductDetailsByCidByBundleIdByContractIdByContractPropertyIds( $arrstrContractPropertiesBundleDetails['cid'], $arrstrContractPropertiesBundleDetails['bundle_ps_product_id'], $arrstrContractPropertiesBundleDetails['contract_id'], $arrintNewBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']], $objAdminDatabase );

				if( false == valArr( $arrobjContractProducts ) ) {
					return NULL;
				}
				foreach( $arrobjContractProducts as $objContractProduct ) {
					$objContractProduct->setId( NULL );
					$objContractProduct->setRecurringAmount( $arrfltContractProductRecuringAmount[$objContractProduct->getBundlePsProductId()][$objContractProduct->getPsProductId()] );
					$objContractProduct->setBundlePsProductId( $intBundelNewId );
				}
				$arrobjContractProductDetails[] = $arrobjContractProducts;

				// Log Maintainace for Reactivation.
				$objContractTerminationEvent = new CContractTerminationEvent();
				$objContractTerminationEvent->setCid( $intCid );
				$objContractTerminationEvent->setContractTerminationRequestId( $intContractTerminationRequestId );
				$objContractTerminationEvent->setContractTerminationEventTypeId( CContractTerminationEventType::TERMINATE_BUNDLE_PRODUCT );
				$objContractTerminationEvent->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
				$objContractTerminationEvent->setEffectiveDate( date( 'm/d/Y' ) );
				$objContractTerminationEvent->setContractPropertyId( implode( ',', $arrintTerminatedBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']] ) );
				$objContractTerminationEvent->setEventParameters( $strJson );

				$arrobjContractTerminationEventDetails[] = $objContractTerminationEvent;

				// Updating old bundle id with new bundle id
				$objContractProperty = new CContractProperty();

				foreach( $arrintNewBundleContractPropertyIds[$arrstrContractPropertiesBundleDetails['bundle_ps_product_id']] as $intContractPropertyId ) {
					$objContractProperty = CContractProperties::createService()->fetchContractPropertyById( $intContractPropertyId, $objAdminDatabase );
					$objContractProperty->setBundlePsProductId( $intBundelNewId );

					if( ( !valId( $objContractProperty->getPackageProductId() ) && !$objContractProperty->applyContractPropertyPricing( $arrfltActiveProductAmount[$intContractPropertyId], $intUserId, $objAdminDatabase, $this->getTerminationDate() ) )
					    || !$objContractProperty->update( $intUserId, $objAdminDatabase ) ) {
						$objAdminDatabase->rollback();
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Failed to Update Contract Properties' ) ) );
						break 2;
					}

				}

				$objAdminDatabase->commit();
			}
		}

		switch( NULL ) {
			default:
				if( true == valArr( $arrobjProductsBundlesDetails ) && false == CProductBundles::createService()->bulkInsert( $arrobjProductsBundlesDetails, SYSTEM_USER_ID, $objAdminDatabase ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Failed to create new bundle' ) ) );
						break;
				}

				if( true == valArr( $arrobjPsProductRelationshipDetails ) ) {
					foreach( $arrobjPsProductRelationshipDetails as $objPsProductRelationshipDetails ) {
						if( false == CPsProductRelationships::createService()->bulkInsert( $objPsProductRelationshipDetails, SYSTEM_USER_ID, $objAdminDatabase ) ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Failed to insert in ps product relationship' ) ) );
							break 2;
						}
					}
				}

				if( true == valArr( $arrobjContractProductDetails ) ) {
					foreach( $arrobjContractProductDetails AS $objContractProductDetails ) {
						if( false == CContractProducts::createService()->bulkInsert( $objContractProductDetails, SYSTEM_USER_ID, $objAdminDatabase ) ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Failed to insert and associate new bundle with properties and products' ) ) );
							break 2;
						}
					}
				}

				if( true == valArr( $arrobjContractTerminationEventDetails ) && false == CContractTerminationEvents::createService()->bulkInsert( $arrobjContractTerminationEventDetails, SYSTEM_USER_ID, $objAdminDatabase ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', __( 'Failed to create log' ) ) );
					break;
				}

				$objAdminDatabase->commit();
				return;
		}

		$objAdminDatabase->rollback();
	}

	public function createContractTerminationEvents( $arrmixContractTerminationEvents, $intUserId, $objAdminDatabase, $arrobjContractProperties = NULL ) {

		if( false == valArr( $arrobjContractProperties ) ) {
			return false;
		}

		$arrobjContractTerminationEvents 		= [];
		$arrintTerminationRequestLevelEvents 	= [];

		foreach( $arrobjContractProperties as $objContractProperty ) {

			if( false == valArr( $arrmixContractTerminationEvents ) || false == array_key_exists( $objContractProperty->getId(), $arrmixContractTerminationEvents ) ) {
				$objContractTerminationEvent = $this->createContractTerminationEvent();
				$objContractTerminationEvent->setEffectiveDate( $this->getEffectiveDate( $objContractTerminationEvent->getContractTerminationEventTypeId(), $boolCurDate = true ) );
				$objContractTerminationEvent->setEventParameters( NULL );
				$objContractTerminationEvent->setContractPropertyId( $objContractProperty->getId() );
				$arrobjContractTerminationEvents[$intCount]	= $objContractTerminationEvent;
			}

			if( true == valArr( $arrmixContractTerminationEvents ) && true == array_key_exists( $objContractProperty->getId(), $arrmixContractTerminationEvents ) ) {
				foreach( $arrmixContractTerminationEvents[$objContractProperty->getId()] as $intContractTerminationEventTypeId => $arrmixContractTerminationEvent ) {

					if( true == in_array( $intContractTerminationEventTypeId, $arrintTerminationRequestLevelEvents ) ) {
						continue;
					}

					if( true == in_array( $intContractTerminationEventTypeId, CContractTerminationEventType::$c_arrintTermincationRequestLevelEvents ) ) {
						$objContractTerminationEvent = $this->createContractTerminationEvent();
						$objContractTerminationEvent->setContractTerminationEventTypeId( $intContractTerminationEventTypeId );
						$objContractTerminationEvent->setEffectiveDate( ( true == isset( $arrmixContractTerminationEvent['EFFECTIVE_DATE'] ) ) ? $arrmixContractTerminationEvent['EFFECTIVE_DATE'] : $this->getEffectiveDate( $objContractTerminationEvent->getContractTerminationEventTypeId() ) );
						$objContractTerminationEvent->setContractPropertyId( $objContractProperty->getId() );
						$objContractTerminationEvent->setEventParameters( ( true == isset( $arrmixContractTerminationEvent['EVENT_PARAMETERS'] ) ) ? $arrmixContractTerminationEvent['EVENT_PARAMETERS'] : NULL );
						$arrobjContractTerminationEvents[]		= $objContractTerminationEvent;
						$arrintTerminationRequestLevelEvents[] 	= $intContractTerminationEventTypeId;
						continue;
					}

					$objContractTerminationEvent = $this->createContractTerminationEvent();
					$objContractTerminationEvent->setContractTerminationEventTypeId( $intContractTerminationEventTypeId );
					$objContractTerminationEvent->setEffectiveDate( ( true == isset( $arrmixContractTerminationEvent['EFFECTIVE_DATE'] ) ) ? $arrmixContractTerminationEvent['EFFECTIVE_DATE'] : $this->getEffectiveDate( $objContractTerminationEvent->getContractTerminationEventTypeId() ) );
					$objContractTerminationEvent->setContractPropertyId( $objContractProperty->getId() );
					$objContractTerminationEvent->setEventParameters( ( true == isset( $arrmixContractTerminationEvent['EVENT_PARAMETERS'] ) ) ? $arrmixContractTerminationEvent['EVENT_PARAMETERS'] : NULL );
					$arrobjContractTerminationEvents[]	= $objContractTerminationEvent;
				}
			}
		}

		switch( NULL ) {
			default:

				$boolIsValid = true;

				if( true == valArr( $arrobjContractTerminationEvents ) ) {
					foreach( $arrobjContractTerminationEvents as $objContractTerminationEvent ) {
						if( false == $objContractTerminationEvent->validate( VALIDATE_INSERT ) ) {
							$this->addErrorMsgs( $objContractTerminationEvent->getErrorMsgs() );
							$boolIsValid = false;
						}
					}

					if( false == $boolIsValid ) {
						break;
					}

					if( false == CContractTerminationEvents::createService()->bulkInsert( $arrobjContractTerminationEvents, $intUserId, $objAdminDatabase ) ) {
						$this->addErrorMsg( __( 'Failed to insert contract termination events.' ) );
						break;
					}

				}
				return true;
		}

		return false;
	}

	public function processContractTerminationEvents( $intUserId, $objAdminDatabase, $objEmailDatabase = NULL, $arrobjContractProperties = NULL, $boolExecutePressingEvents = false, $objLogDatabase = NULL, $boolIsAppstore = false, $arrobjPropertyTerminatingContractProperties = NULL, $objConnectDatabase = NULL, $objVoipDatabase = NULL, $objObjectStorageGateway = NULL ) {

		$boolTerminateProductProcessed = false;

		if( true == $boolExecutePressingEvents ) {
			if( true == valArr( $arrobjPropertyTerminatingContractProperties ) && false == in_array( CContractTerminationEventType::PROPERTY_TERMINATION_EMAIL, CContractTerminationEventType::$c_arrintPressingEvents ) ) {
				CContractTerminationEventType::$c_arrintPressingEvents[] = CContractTerminationEventType::PROPERTY_TERMINATION_EMAIL;
			}
			$this->m_arrobjContractTerminationEvents	= CContractTerminationEvents::createService()->fetchPendingContractTerminationEventsByContractTerminationRequestIdByContractTerminationEventTypeIds( $this->getId(), CContractTerminationEventType::$c_arrintPressingEvents, $objAdminDatabase );
		} else {
			$this->m_arrobjContractTerminationEvents	= CContractTerminationEvents::createService()->fetchPendingContractTerminationEventsByContractTerminationRequestId( $this->getId(), $objAdminDatabase, $boolEffectiveDate = true );
		}

		if( true == valArr( $this->m_arrobjContractTerminationEvents ) ) {

			foreach( $this->m_arrobjContractTerminationEvents as $intContractTerminationEventId => $objContractTerminationEvent ) {

				if( true == $boolTerminateProductProcessed && $objContractTerminationEvent->getContractTerminationEventTypeId() == CContractTerminationEventType::TERMINATE_PRODUCT && strtotime( date( 'Y-m-d' ) ) >= strtotime( date( 'Y-m-d', strtotime( $objContractTerminationEvent->getEffectiveDate() ) ) ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					continue;
				}

				if( true == $this->processContractTerminationEvent( $objContractTerminationEvent, $intUserId, $objAdminDatabase, $objEmailDatabase, $arrobjContractProperties, $objLogDatabase, $boolIsAppstore, $arrobjPropertyTerminatingContractProperties, $objConnectDatabase, $objVoipDatabase, $objObjectStorageGateway ) ) {
					if( $objContractTerminationEvent->getContractTerminationEventTypeId() == CContractTerminationEventType::TERMINATE_PRODUCT ) {
						$boolTerminateProductProcessed = true;
					}
					continue;
				} else {

					if( true == array_key_exists( $objContractTerminationEvent->getContractTerminationEventTypeId(), CContractTerminationEventType::$c_arrstrTerminationEventsTables ) ) {
						$this->m_arrintFailedContractTerminationEventTypeIds[$objContractTerminationEvent->getId()] = $objContractTerminationEvent->getContractTerminationEventTypeId();
						continue;
					}
				}
				return false;
			}

			if( false == CContractTerminationEvents::createService()->bulkUpdate( $this->m_arrobjContractTerminationEvents, [ 'contract_termination_event_status_id' ], $intUserId, $objAdminDatabase ) ) {
				return false;
			}

			$this->m_arrobjContractTerminationEvents	= CContractTerminationEvents::createService()->fetchPendingContractTerminationEventsByContractTerminationRequestId( $this->getId(), $objAdminDatabase, $boolEffectiveDate = true );

			if( true == valArr( $this->m_arrobjContractTerminationEvents ) ) {
				$arrintCompletedEventTypeIds = [];
				foreach( $this->m_arrobjContractTerminationEvents as $intContractTerminationEventId => $objContractTerminationEvent ) {
					if( ( false == in_array( $objContractTerminationEvent->getContractTerminationEventTypeId(), $arrintCompletedEventTypeIds ) ) && ( true == $this->checkIsContractTerminationEventCompleted( $objContractTerminationEvent->getContractTerminationEventTypeId(), $objAdminDatabase ) ) ) {
						$arrintCompletedEventTypeIds[] = $objContractTerminationEvent->getContractTerminationEventTypeId();
					}
					if( true == in_array( $objContractTerminationEvent->getContractTerminationEventTypeId(), $arrintCompletedEventTypeIds ) ) {
						$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );

						if( true == valArr( $this->m_arrintFailedContractTerminationEventTypeIds ) && true == array_key_exists( $objContractTerminationEvent->getId(), $this->m_arrintFailedContractTerminationEventTypeIds ) ) {
							unset( $this->m_arrintFailedContractTerminationEventTypeIds[$objContractTerminationEvent->getId()] );
						}
					}
				}
			}

			if( true == valArr( $this->m_arrintFailedContractTerminationEventTypeIds ) ) {

				foreach( $this->m_arrintFailedContractTerminationEventTypeIds as $intEventId => $intEventTypeId ) {

					$objContractTerminationEvent = \Psi\Eos\Admin\CContractTerminationEvents::createService()->fetchContractTerminationEventById( $intEventId, $objAdminDatabase );
					if( true == valObj( $objContractTerminationEvent, 'CContractTerminationEvent' ) ) {

						$objProperty = CProperties::createService()->fetchPropertiesByContractPropertyId( $objContractTerminationEvent->getContractPropertyId(), $objAdminDatabase );
						if( true == valObj( $objProperty, 'CProperty' ) ) {
							$this->m_arrstrFailedPropertyEventNames[$objProperty->getId() . '-' . $objProperty->getPropertyName()][$intEventTypeId] = $objProperty->getPropertyName();
						}
					}
				}

				$strCSMEmailAddress = \Psi\Eos\Admin\CEmployees::createService()->fetchCsmEmployeesByCids( [ $this->getCid() ], $objAdminDatabase, true );
				if( true == valStr( $strCSMEmailAddress ) ) {
					$this->sendFailedTerminationEventNotification( $intUserId, $strCSMEmailAddress, $objAdminDatabase, $objEmailDatabase );
				}
			}
		}

		switch( NULL ) {
			default:
				$boolIsValid = true;

				if( true == valArr( $this->m_arrobjContractTerminationEvents ) ) {
					foreach( $this->m_arrobjContractTerminationEvents as $objContractTerminationEvent ) {
						if( false == $objContractTerminationEvent->validate( VALIDATE_UPDATE ) ) {
							$boolIsValid = false;
						}
					}
				}

				if( false == $boolIsValid ) {
					return false;
				}

				if( false == valArr( $arrobjContractProperties ) ) {
					$objAdminDatabase->begin();
				}

				if( true == valArr( $this->m_arrobjContractTerminationEvents ) ) {
					if( false == CContractTerminationEvents::createService()->bulkUpdate( $this->m_arrobjContractTerminationEvents, [ 'contract_termination_event_status_id' ], $intUserId, $objAdminDatabase ) ) {
						$boolIsValid = false;
					}
				}

				if( false == $boolIsValid ) {
					if( false == valArr( $arrobjContractProperties ) ) {
						$objAdminDatabase->rollback();
					}
					return false;
				}

				if( false == $this->m_boolIsScriptApproval &&
					false == $boolExecutePressingEvents &&
					false == in_array( $this->getContractTerminationRequestStatusId(), [ CContractTerminationRequestStatus::MANAGEMENT_APPROVED, CContractTerminationRequestStatus::COMPLETE ] ) ) {
					$this->setContractTerminationRequestStatusId( CContractTerminationRequestStatus::MANAGEMENT_APPROVED );

					if( false == $this->update( $intUserId, $objAdminDatabase ) ) {
						if( false == valArr( $arrobjContractProperties ) ) {
							$objAdminDatabase->rollback();
						}
						return false;
					}
				}

				if( false == valArr( $arrobjContractProperties ) ) {
					$objAdminDatabase->commit();
				}
		}
		return true;
	}

	public function processContractTerminationEvent( $objContractTerminationEvent, $intUserId, $objAdminDatabase, $objEmailDatabase = NULL, $arrobjContractProperties = NULL, $objLogDatabase = NULL, $boolIsAppstore = false, $arrobjPropertyTerminatingContractProperties = NULL, $objConnectDatabase = NULL, $objVoipDatabase = NULL, $objObjectStorageGateway = NULL ) {

		if( true == valArr( $arrobjPropertyTerminatingContractProperties ) && false == in_array( CContractTerminationEventType::PROPERTY_TERMINATION_EMAIL, CContractTerminationEventType::$c_arrintPressingEvents ) ) {
			CContractTerminationEventType::$c_arrintPressingEvents[] = CContractTerminationEventType::PROPERTY_TERMINATION_EMAIL;
		}

		if( false == in_array( $objContractTerminationEvent->getContractTerminationEventTypeId(), CContractTerminationEventType::$c_arrintPressingEvents ) &&
			( false == is_null( $objContractTerminationEvent->getEffectiveDate() ) && strtotime( date( 'Y-m-d' ) ) < strtotime( date( 'Y-m-d', strtotime( $objContractTerminationEvent->getEffectiveDate() ) ) ) ) &&
			strtotime( date( 'Y-m-d' ) ) < strtotime( $this->getDeactivationDate() ) ) {
			return true;
		}

		$boolIsValid = false;

		if( true == in_array( $objContractTerminationEvent->getContractTerminationEventTypeId(), array_keys( CContractTerminationEventType::$c_arrstrTerminationEventsTables ) ) ) {

			if( false == valObj( $objContractTerminationEvent, 'CContractTerminationEvent' ) ) return false;

			if( false == valStr( $objContractTerminationEvent->getEventParameters() ) ) return false;
		}

		switch( $objContractTerminationEvent->getContractTerminationEventTypeId() ) {

			case CContractTerminationEventType::TERMINATE_PRODUCT:
				if( false != $this->processTerminateProductEvent( $intUserId, $objAdminDatabase, $arrobjContractProperties, $objLogDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				}
				break;

			case CContractTerminationEventType::TERMINATE_CONTRACT:
				if( false != $this->processTerminateContractEvent( $intUserId, $objAdminDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				}
				break;

			case CContractTerminationEventType::UPDATE_BILLING_INFORMATION:
				if( false != $this->processUpdateBillingInformationEvent( $objContractTerminationEvent, $intUserId, $objAdminDatabase, $arrobjContractProperties ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				}
				break;

			case CContractTerminationEventType::UPDATE_MERCHANT_BANKING_INFORMATION:
				$boolIsValid = true;
				break;

			case CContractTerminationEventType::TERMINATION_DOCUMENT:
				if( false != $this->processTerminationDocument( $objContractTerminationEvent, $objAdminDatabase, $arrobjContractProperties, $objObjectStorageGateway ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				}
				break;

			case CContractTerminationEventType::CREATE_TASK:
				if( valId( $this->getCompanyUserId() ) ) {

					if( !valArr( $arrobjContractProperties ) ) {
						$arrobjContractProperties = CContractProperties::createService()->fetchAllContractPropertiesByContractTerminationRequestId( $this->getId(), $objAdminDatabase );
					}

					if( valId( $this->getPostedBy() ) && valArr( $arrobjContractProperties ) && $this->createTasksForTermination( $arrobjContractProperties, $objAdminDatabase, $intUserId, $objObjectStorageGateway ) ) {
						$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					}
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
				}

				$boolIsValid = true;
				break;

			case CContractTerminationEventType::SEND_EMAIL:
				if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_db_validation', __( 'Email database is down, system failed to send email.' ) ) );
					break;
				}

				if( false != $this->sendTerminationRequestEmail( $arrobjContractProperties, $intUserId, $objAdminDatabase, $objEmailDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				}
				break;

			case CContractTerminationEventType::SYSTEM:
				$boolIsValid = true;
				break;

			case CContractTerminationEventType::PROPERTY_TERMINATION_EMAIL:
				if( false == valObj( $objEmailDatabase, 'CDatabase' ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_db_validation', __( 'Email Database is down. Please try after sometime.' ) ) );
					break;
				}

				$this->sendPropertyTerminationEmail( $arrobjPropertyTerminatingContractProperties, $intUserId, $boolIsAppstore, $objAdminDatabase, $objEmailDatabase, $objConnectDatabase );

				// Though the email is sent or not, we've to mark the event as COMPLETED. This is because the request won't be completed unless all events are completed.
				$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
				$boolIsValid = true;
				break;

			case CContractTerminationEventType::TERMINATE_CONTRACT_PROPERTY:
				if( false != $this->processTerminateContractProperty( $intUserId, $objAdminDatabase, $arrobjContractProperties ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::TERMINATE_DEFAULT_MERCHANT_ACCOUNT:
				if( false != $this->processTerminateDefaultMerchantAccount( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::TERMINATE_CHARGE_CODE_SPECIFIC_MERCHANT_ACCOUNT:
				if( false != $this->processTerminateMerchantAccounts( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::TERMINATE_AUTOMATIC_POST_SCHEDULED_CHARGES:
				if( false != $this->processTerminateAutomaticPostScheduledCharges( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::TERMINATE_DISABLE_ILS_PORTAL:
				if( false != $this->processTerminateDisableILSPortal( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase ) ) {

					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::TERMINATE_DISABLE_RESIDENT_PAY:
				if( false != $this->processTerminateDisableResidentPay( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::TERMINATE_ENABLE_RENT_REMINDER:
				if( false != $this->processTerminateEnbleRentReminder( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::TERMINATE_DISABLE_LEASING_CENTER:
				if( false != $this->processTerminateDisableLeasingCenter( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase, $objVoipDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::TERMINATE_DOCUMENT_PACKETS:
			case CContractTerminationEventType::TERMINATE_DOCUMENT_TEMPLATE:
				if( false != $this->processTerminateDocumentTemplate( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::TERMINATE_PROPERTY_WEBSITES:
				if( false != $this->processTerminatePropertyWebsites( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::DEACTIVATE_WEBSITES:
				if( false != $this->processDeactivateWebsites( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::DISASSOCIATE_RENTAL_APPLICATION_DOCUMENT:
				if( false != $this->processDisassociateRentalApplicationDocument( $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::DISASSOCIATE_PROPERTY_INTEGRATION:
				if( false != $this->processDisassociatePropertyIntegration( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::REMOVE_RESIDENT_RECURRING_PAYMENTS:
				if( false != $this->processRemoveResidentRecurringPayment( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase, $objEmailDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
					$boolIsValid = false;
				}
				break;

			case CContractTerminationEventType::DISABLE_PROPERTY:
				$this->m_boolActiveProperty = false;
				if( false != $this->processDisableProperty( $intUserId, $objContractTerminationEvent->getEventParameters(), $objAdminDatabase, $objConnectDatabase ) ) {
					$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::COMPLETED );
					$boolIsValid = true;
				} else {
					if( $this->m_boolActiveProperty ) {
						$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::CANCELLED );
						$boolIsValid = true;
					} else {
						$this->m_arrobjContractTerminationEvents[$objContractTerminationEvent->getId()]->setContractTerminationEventStatusId( CContractTerminationEventStatus::FAILED );
						$boolIsValid = false;
					}
				}
				break;

			default:
				$boolIsValid = true;
		}
		return $boolIsValid;
	}

	public function processTerminateContractProperty( $intUserId, $objAdminDatabase, $arrobjContractProperties = NULL ) {

		if( true == is_null( $this->getTerminationDate() ) ) {
			return false;
		}

		if( false == valArr( $arrobjContractProperties ) ) {
			$arrobjContractProperties	= CContractProperties::createService()->fetchContractPropertiesByContractTerminationRequestId( $this->getId(), $objAdminDatabase );
		}

		if( false == valArr( $arrobjContractProperties ) ) {
			return false;
		}

		$arrobjCompanyCharges = [];

		foreach( $arrobjContractProperties as $objContractProperty ) {
			$arrobjTempCompanyCharges = $this->getMultipleAssociatedCompanyCharges( $objContractProperty->getId(), $objAdminDatabase );

			if( false == valArr( $arrobjTempCompanyCharges ) ) {
				continue;
			}
			$arrobjCompanyCharges = array_merge( $arrobjCompanyCharges, $arrobjTempCompanyCharges );
		}
		$objAdminDatabase->begin();
		switch( NULL ) {
			default:
				$strSql = 'UPDATE
								contract_properties
							SET
								termination_date = \'' . $this->getTerminationDate() . '\',
								updated_by = ' . ( int ) $intUserId . ',
								updated_on = \'NOW()\'
							WHERE
								id IN ( ' . implode( ',', array_keys( $arrobjContractProperties ) ) . ' );';

				// set end_date in contract_property_pricings table
				$strSql .= 'UPDATE
								contract_property_pricings
							SET
								end_date = \'' . date( 'Y-m-d', strtotime( $this->getTerminationDate() ) ) . '\',
								updated_by = ' . ( int ) $intUserId . ',
								updated_on = \'NOW()\'
							WHERE
								end_date IS NULL AND
								contract_property_id IN ( ' . implode( ',', array_keys( $arrobjContractProperties ) ) . ' );';

				if( true == valArr( $arrobjCompanyCharges ) ) {

					$strSql .= 'UPDATE
									company_charges cc
								SET
									charge_end_date = \'' . $this->getTerminationDate() . '\',
									account_id = ( CASE WHEN cp.recurring_account_id <> account_id AND cc.is_from_contract = FALSE THEN cp.recurring_account_id ELSE account_id END),
									updated_by = ' . ( int ) $intUserId . ',
									updated_on = \'NOW()\'
								FROM
									contract_properties cp
									JOIN charge_codes ch ON ( ch.ps_product_id = cp.ps_product_id )
								WHERE
									cp.cid = cc.cid
									AND cp.property_id = cc.property_id
									AND cc.charge_code_id = ch.id
									AND cc.id IN ( ' . implode( ',', array_keys( rekeyObjects( 'Id', $arrobjCompanyCharges ) ) ) . ' )
									AND cc.deleted_on IS NULL
									AND ( cc.charge_end_date IS NULL OR ( cc.is_from_contract = TRUE AND cc.charge_end_date > \'' . $this->getTerminationDate() . '\' ) )
									AND cc.is_disabled = 0;';
				}
				if( false == $objAdminDatabase->execute( $strSql ) ) {
					$objAdminDatabase->rollback();
					break;
				}
				return true;
		}
		return false;
	}

	public function checkIsContractTerminationEventCompleted( $intContractTerminationEventTYpeId, $objAdminDatabase ) {

		$strWhere = '
					WHERE
						contract_termination_request_id = ' . ( int ) $this->getId() . '
						AND contract_termination_event_type_id = ' . ( int ) $intContractTerminationEventTYpeId . '
						AND contract_termination_event_status_id = ' . CContractTerminationEventStatus::COMPLETED . ' ';

		$intCount = \Psi\Eos\Admin\CContractTerminationEvents::createService()->fetchContractTerminationEventCount( $strWhere, $objAdminDatabase );

		return valId( $intCount );
	}

	public function processUpdateBillingInformationEvent( $objContractTerminationEvent, $intUserId, $objAdminDatabase, $arrobjContractProperties = NULL ) {

		if( false == valId( $objContractTerminationEvent->getContractPropertyId() ) || true == is_null( $objContractTerminationEvent->getEventParameters() ) ) {
			return false;
		}

		$objJsonEventParameters = json_decode( $objContractTerminationEvent->getEventParameters() );

		// checking whether json array decoded correctly or not
		if( json_last_error() != JSON_ERROR_NONE || true == is_null( $objJsonEventParameters->NEW_ACCOUNT_ID ) || 0 >= $objJsonEventParameters->NEW_ACCOUNT_ID ) {
			return false;
		}

		// fetching company charges associated with current contract property
		$arrobjCompanyCharges = $this->getMultipleAssociatedCompanyCharges( $objContractTerminationEvent->getContractPropertyId(), $objAdminDatabase );

		$objOldAccount = NULL;

		// fetching account object from contract property if recurring account id exists
		if( true == valArr( $arrobjContractProperties ) && true == array_key_exists( $objContractTerminationEvent->getContractPropertyId(), $arrobjContractProperties ) ) {
			if( true == valId( $arrobjContractProperties[$objContractTerminationEvent->getContractPropertyId()]->getRecurringAccountId() ) ) {
				$objOldAccount = CAccounts::createService()->fetchAccountById( $arrobjContractProperties[$objContractTerminationEvent->getContractPropertyId()]->getRecurringAccountId(), $objAdminDatabase );
			}
		} else {
			$objContractProperty = CContractProperties::createService()->fetchContractPropertyById( $objContractTerminationEvent->getContractPropertyId(), $objAdminDatabase );

			if( true == valObj( $objContractProperty, 'CContractProperty' ) && true == valId( $objContractProperty->getRecurringAccountId() ) ) {
				$objOldAccount = CAccounts::createService()->fetchAccountById( $objContractProperty->getRecurringAccountId(), $objAdminDatabase );
			}
		}

		$arrintPropertyId = CContractProperties::createService()->fetchContractPropertiesPropertyIdById( $objContractTerminationEvent->getContractPropertyId(), $objAdminDatabase );

		$objAccountProperty = CAccountProperties::createService()->fetchAccountPropertyByAccountIdByPropertyId( $objOldAccount->getId(), $arrintPropertyId[0]['property_id'], $objAdminDatabase );

		// verifying that new account is already exist in the system
		$objNewAccount = CAccounts::createService()->fetchAccountById( $objJsonEventParameters->NEW_ACCOUNT_ID, $objAdminDatabase );

		switch( NULL ) {
			default:

				$strSql = 'UPDATE
								contract_properties
							 SET
								recurring_account_id = ' . ( int ) $objJsonEventParameters->NEW_ACCOUNT_ID . ',
								updated_by = ' . ( int ) $intUserId . ',
								updated_on = \'NOW()\'
							WHERE
								id = ' . ( int ) $objContractTerminationEvent->getContractPropertyId();

				if( false == $objAdminDatabase->execute( $strSql ) ) {
					break;
				}

				if( true == valArr( $arrobjCompanyCharges ) ) {

					$strSql = 'UPDATE
									company_charges cc
								SET
									account_id = ' . ( int ) $objJsonEventParameters->NEW_ACCOUNT_ID . ',
									updated_by = ' . ( int ) $intUserId . ',
									updated_on = \'NOW()\'
								FROM
									contract_properties cp
									JOIN charge_codes ch ON ( ch.ps_product_id = cp.ps_product_id )
								WHERE
									cp.cid = cc.cid
									AND cp.property_id = cc.property_id
									AND cc.charge_code_id = ch.id
									AND cc.id IN ( ' . implode( ',', array_keys( $arrobjCompanyCharges ) ) . ' );';

					if( false == $objAdminDatabase->execute( $strSql ) ) {
						break;
					}
				}

				if( true == valObj( $objAccountProperty, 'CAccountProperty' ) && false == $objAccountProperty->delete( SYSTEM_USER_ID, $objAdminDatabase ) ) {
					$objAdminDatabase->rollback();
					return false;
				}

				if( true == valObj( $objNewAccount, 'CAccount' ) && true == valArr( $arrintPropertyId ) ) {
					$objAccountProperty = new CAccountProperty();
					$objAccountProperty->setCid( $objNewAccount->getCid() );
					$objAccountProperty->setAccountId( $objNewAccount->getId() );
					$objAccountProperty->setPropertyId( $arrintPropertyId[0]['property_id'] );
				}

				if( false == $objAccountProperty->insert( SYSTEM_USER_ID, $objAdminDatabase ) ) {
					$objAdminDatabase->rollback();
					return false;
				}

				if( true == valObj( $objOldAccount, 'CAccount' ) && true == valObj( $objNewAccount, 'CAccount' ) && false == $objOldAccount->transferFullBalance( $objNewAccount->getId(), $objAdminDatabase, $intUserId, $objContractTerminationEvent->getContractPropertyId() ) ) {
					break;
				}

				return true;
		}
		return false;
	}

	public function processTerminateProductEvent( $intUserId, $objAdminDatabase, $arrobjContractProperties = NULL, $objLogDatabase = NULL ) {
		if( true == is_null( $this->getTerminationDate() ) ) {
			return false;
		}

		if( false == valArr( $arrobjContractProperties ) ) {
			$arrobjContractProperties	= CContractProperties::createService()->fetchContractPropertiesByContractTerminationRequestId( $this->getId(), $objAdminDatabase );
		}

		if( false == valArr( $arrobjContractProperties ) ) {
			return false;
		}

		$arrobjCompanyCharges = [];

		foreach( $arrobjContractProperties as $objContractProperty ) {
			$arrobjTempCompanyCharges = $this->getMultipleAssociatedCompanyCharges( $objContractProperty->getId(), $objAdminDatabase );

			if( false == valArr( $arrobjTempCompanyCharges ) ) {
				continue;
			}
			$arrobjCompanyCharges = array_merge( $arrobjCompanyCharges, $arrobjTempCompanyCharges );
		}
		$objAdminDatabase->begin();
		$arrobjContractProductOptions 	= CContractProductOptions::createService()->fetchContractProductOptionsByContractPropertyIds( array_keys( rekeyObjects( 'Id', $arrobjContractProperties ) ), $objAdminDatabase );

		switch( NULL ) {
			default:
				$strSql = 'UPDATE
								contract_properties
							SET
								termination_date = \'' . $this->getTerminationDate() . '\',
								deactivation_date = \'' . $this->getDeactivationDate() . '\',
								updated_by = ' . ( int ) $intUserId . ',
								updated_on = \'NOW()\'
							WHERE
								id IN ( ' . implode( ',', array_keys( $arrobjContractProperties ) ) . ' );';

				if( false == $objAdminDatabase->execute( $strSql ) ) {
					$objAdminDatabase->rollback();
					break;
				} else {
					foreach( $arrobjContractProperties as $objContractProperty ) {
						if( CPsProduct::PRICING == $objContractProperty->getPsProductId() ) {
							try {
								$objCachedUnitAvailabilityMessage = new CPricingRatesDeletionMessage();
								$objCachedUnitAvailabilityMessage->setCid( $objContractProperty->getCid() );
								$objCachedUnitAvailabilityMessage->setPropertyId( $objContractProperty->getPropertyId() );
								$objEntrataMessageSender = ( new Psi\Libraries\Queue\Factory\CMessageSenderFactory() )->createEntrataSender( new \Psi\Libraries\Queue\Factory\CAmqpMessageFactory(), $objCachedUnitAvailabilityMessage->getMessageType(), CONFIG_CLUSTER_NAME );
								$objEntrataMessageSender->send( $objCachedUnitAvailabilityMessage, [ 'delivery_mode' => 2 ] );
							} catch( Exception $objException ) {
								$this->addErrorMsg( __( 'Failed to queue pricing property for hard delete rates' ) );
							}
						}
					}
				}

				// set end_date in contract_property_pricings table
				$strSql = 'UPDATE
								contract_property_pricings
							SET
								end_date = \'' . date( 'Y-m-d', strtotime( $this->getTerminationDate() ) ) . '\',
								updated_by = ' . ( int ) $intUserId . ',
								updated_on = \'NOW()\'
							WHERE
								end_date IS NULL AND
								contract_property_id IN ( ' . implode( ',', array_keys( $arrobjContractProperties ) ) . ' );';

				if( false == $objAdminDatabase->execute( $strSql ) ) {
					$objAdminDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjContractProductOptions ) && false == CContractProductOptions::createService()->bulkDelete( $arrobjContractProductOptions, $objAdminDatabase ) ) {
					$objAdminDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjCompanyCharges ) ) {

					$strSql = 'UPDATE
									company_charges cc
								SET
									charge_end_date = \'' . $this->getTerminationDate() . '\',
									account_id = ( CASE WHEN cp.recurring_account_id <> account_id AND cc.is_from_contract = FALSE THEN cp.recurring_account_id ELSE account_id END),
									updated_by = ' . ( int ) $intUserId . ',
									updated_on = \'NOW()\'
								FROM
									contract_properties cp
									JOIN charge_codes ch ON ( ch.ps_product_id = cp.ps_product_id )
								WHERE
									cp.cid = cc.cid
									AND cp.property_id = cc.property_id
									AND cc.charge_code_id = ch.id
									AND cc.id IN ( ' . implode( ',', array_keys( rekeyObjects( 'Id', $arrobjCompanyCharges ) ) ) . ' )
									AND cc.deleted_on IS NULL
									AND ( cc.charge_end_date IS NULL OR ( cc.is_from_contract = TRUE AND cc.charge_end_date > \'' . $this->getTerminationDate() . '\' ) )
									AND cc.is_disabled = 0;';
					if( false == $objAdminDatabase->execute( $strSql ) ) {
						$objAdminDatabase->rollback();
						break;
					}
				}

				$objPropertyProductPermissionsLibrary = new CPropertyProductPermissionsLibrary();

				$arrobjClients = CClients::createService()->fetchClientsByIds( array_keys( rekeyObjects( 'CId', $arrobjContractProperties ) ), $objAdminDatabase );

				if( false == valArr( $arrobjClients ) ) {
					$this->cprintln( SCRIPT_NONE, __( 'Failed to load clients to rebuild the permissions.' ) );
				} else {
					if( true == valObj( $objLogDatabase, 'CDatabase' ) ) {
						$objLogDatabase->open();
						$objPropertyProductPermissionsLibrary->rebuildCompanyPermissions( $intUserId, $arrobjClients, $objAdminDatabase, $objClientDatabase = NULL, $objLogDatabase );
						$objLogDatabase->close();
					}
				}

				$strSql = 'UPDATE
								contract_property_terminations
							SET
								termination_date = \'' . $this->getTerminationDate() . '\',
								deactivation_date = \'' . $this->getDeactivationDate() . '\',
								updated_by = ' . ( int ) $intUserId . ',
								updated_on = \'NOW()\'
							WHERE
								contract_termination_request_id = ' . $this->getId();

				if( !$objAdminDatabase->execute( $strSql ) ) {
					$objAdminDatabase->rollback();
					break;
				}

				self::rebuildContractPackageDiscounts( $arrobjContractProperties, $objAdminDatabase );

				return true;
		}

		return false;
	}

	public static function rebuildContractPackageDiscounts( $arrobjContractProperties, $objDatabase, $boolTerminate = true ) {

		if( !valArr( $arrobjContractProperties ) ) {
			return NULL;
		}

		$intCid = reset( $arrobjContractProperties )->getCid();

		// TODO - Remove this `if` condition once we've to roll-out Sales Opportunity Redesign changes to all clients.
		if( !in_array( $intCid, CClient::$c_arrintSalesOpportunityRedesignCids ) ) {
			return NULL;
		}

		$arrintPackageProductIds = array_unique( array_filter( array_keys( rekeyObjects( 'PackageProductId', $arrobjContractProperties ) ) ) );

		if( !valIntArr( $arrintPackageProductIds ) ) {
			return NULL;
		}

		$arrobjContractProperties = ( array ) CContractProperties::createService()->fetchActiveContractPropertiesByPackageProductIds( $arrintPackageProductIds, $objDatabase );

		$arrobjUpdateContractProperties = [];

		foreach( $arrobjContractProperties as $objContractProperty ) {

			if( $objContractProperty->applyPackageProductDiscount( $objDatabase, $boolTerminate ) ) {
				$arrobjUpdateContractProperties[] = $objContractProperty;
			}
		}

		if( valArr( $arrobjUpdateContractProperties ) && CContractProperties::createService()->bulkUpdate( $arrobjUpdateContractProperties, [ 'monthly_recurring_amount', 'monthly_change_amount' ], CUser::ID_SYSTEM, $objDatabase ) ) {
			$objDatabase->commit();
			return NULL;
		}
	}

	public function processTerminateContractEvent( $intUserId, $objAdminDatabase ) {

		if( true == is_null( $this->getTerminationDate() ) ) {
			return false;
		}

		$arrobjContracts = $this->fetchContracts( $objAdminDatabase );

		if( false == valArr( $arrobjContracts ) ) {
			return false;
		}

		foreach( $arrobjContracts as $objContract ) {
			$objContract->setTerminationDate( $this->getTerminationDate() );

			if( strtotime( date( 'm/d/Y', time() ) ) >= strtotime( $this->getTerminationDate() ) ) {
				$objContract->setContractStatusTypeId( CContractStatusType::CONTRACT_TERMINATED );
			}

			if( false == $objContract->update( $intUserId, $objAdminDatabase ) ) {
				return false;
			}
		}

		return true;
	}

	public function getMultipleAssociatedCompanyCharges( $intContractPropertyId, $objAdminDatabase ) {

		$strSql = 'WITH dups_cc AS (
						WITH dups_cp AS (
							SELECT
								cc.*,
								count(cc.id) OVER (PARTITION BY cp.id) as cc_cnt
							FROM
								contract_properties cp
								JOIN company_charges cc ON ( cc.cid = cp.cid AND cc.property_id = cp.property_id AND cc.is_disabled = 0 AND ( cc.deleted_on IS NULL OR cc.deleted_on > NOW ( ) ) )
								JOIN charge_codes ch ON ( ch.id = cc.charge_code_id AND ch.ps_product_id = cp.ps_product_id )
							 WHERE
							  cp.cid = ' . ( int ) $this->getCid() . '
							  AND cp.id = ' . ( int ) $intContractPropertyId . '
							  AND cp.termination_date IS NULL
							  AND cp.renewal_contract_property_id IS NULL
							ORDER BY
							  cp.id
						)
						SELECT
							  dcp.*,
							  COUNT(0) OVER(PARTITION BY dcp.id) as cp_cnt
						 FROM
							  dups_cp dcp
							  JOIN contract_properties cp ON cp.cid = dcp.cid AND cp.property_id = dcp.property_id AND cp.termination_date IS NULL AND cp.renewal_contract_property_id IS NULL AND cp.is_last_contract_record = TRUE
							  JOIN charge_codes ch ON ch.id = dcp.charge_code_id AND ch.ps_product_id = cp.ps_product_id
						  )
					 SELECT
						  dcc.*
						FROM
						  dups_cc dcc
					 WHERE
						  dcc.cp_cnt = 1';

		return CCompanyCharges::createService()->fetchCompanyCharges( $strSql, $objAdminDatabase );
	}

	public function processTerminationDocument( $objContractTerminationEvent, $objAdminDatabase, $arrobjContractProperties = NULL, $objObjectStorageGateway = NULL ) {

		if( false == valArr( $arrobjContractProperties ) ) {
			$arrobjContracts = $this->fetchContracts( $objAdminDatabase );
		} else {
			$arrobjContracts = CContracts::createService()->fetchContractsByIds( array_keys( rekeyObjects( 'ContractId', $arrobjContractProperties ) ), $objAdminDatabase );
		}

		if( false == valArr( $arrobjContracts ) ) {
			return false;
		}

		// we are getting 'DOCUMENT_NAME' as a std object and we need Array type thats why intentionally we are decoding to array instead of stdObject.
		$arrmixJsonEventParameters = json_decode( $objContractTerminationEvent->getEventParameters(), true );

		// checking whether json array decoded correctly or not
		if( json_last_error() != JSON_ERROR_NONE || false == valArr( $arrmixJsonEventParameters['DOCUMENT_NAME'] ) ) {
			return false;
		}

		$arrstrContractDocumentNames = $arrmixJsonEventParameters['DOCUMENT_NAME'];

		$boolIsValid = true;

		foreach( $arrobjContracts as $objContract ) {
			if( false == isset( $arrstrContractDocumentNames[$objContract->getId()] ) ) {
				$boolIsValid = false;
				break;
			}

			$strDocumentName = $arrstrContractDocumentNames[$objContract->getId()];

			$objTempHelper						= new \Psi\Libraries\ObjectStorage\Utils\CTempObjectStorageHelper( $objObjectStorageGateway, CONFIG_OSG_BUCKET_TEMP_DOCUMENTS );
			$arrmixRequest						= [];
			$arrmixRequest['key'] = sprintf( '%d/%s/%d/%d/%d/%s', $this->getCid(), 'documents', date( 'Y' ), date( 'm' ), date( 'd' ), $strDocumentName ); // object key
			$arrmixRequest['outputFile'] = 'temp';
			$objObjectStorageGatewayResponse	= $objTempHelper->getObject( $arrmixRequest );
			if( false == $objObjectStorageGatewayResponse->hasErrors() ) {
				$strFullPath = $objObjectStorageGatewayResponse['outputFile'];
			} else {
				$boolIsValid = false;
				break;
			}

			$objContract->setContractDocumentFileName( $strDocumentName );

			$boolIsValid = $objContract->insertContractDocument( $objAdminDatabase, $strFullPath, $objObjectStorageGateway );
		}

		return $boolIsValid;
	}

	public function fetchContractTerminationEvents( $objAdminDatabase ) {
		return CContractTerminationEvents::createService()->fetchCustomContractTerminationEventsByContractTerminationRequestId( $this->getId(), $objAdminDatabase );
	}

	public function getEffectiveDate( $intContractTerminationEventTypeId, $boolCurDate = false ) {

		$strCurDate = date( 'Y-m-d h:i:s' );

		if( true == $boolCurDate ) {
			return $strCurDate;
		}

		if( true == is_null( $this->getTerminationDate() ) ) {
			return $strCurDate;
		}

		if( true == in_array( $intContractTerminationEventTypeId, CContractTerminationEventType::$c_arrintEventsBasedOnRequestedDate ) ) {
			return $strCurDate;
		}

		if( true == in_array( $intContractTerminationEventTypeId, CContractTerminationEventType::$c_arrintEventsBasedOnDeactivationDate ) ) {
			return $this->getDeactivationDate();
		}

		if( true == in_array( $intContractTerminationEventTypeId, array_keys( CContractTerminationEventType::$c_arrstrTerminationEventsTables ) ) && true == ( CContractTerminationEventType::DISABLE_PROPERTY == $intContractTerminationEventTypeId ) && true == ( strtotime( $this->getDeactivationDate() ) > strtotime( $this->getTerminationDate() ) ) ) {
				return $this->getDeactivationDate();
		}

		return $this->getTerminationDate();
	}

	public function fetchContracts( $objAdminDatabase ) {
		return CContracts::createService()->fetchContractsByContractTerminationRequestId( $this->getId(), $objAdminDatabase );
	}

	public function fetchContractProperties( $objAdminDatabase ) {
		return CContractProperties::createService()->fetchContractPropertiesByContractTerminationRequestId( $this->getId(), $objAdminDatabase );
	}

	public function validateNotAssociateDuplicateContractProperties( $arrobjContractProperties, $objAdminDatabase, $intExcludeContractId = NULL ) {

		$arrobjActiveContractProperties	= CContractProperties::createService()->fetchDuplicateActiveContractPropertiesByContractPropertyIds( $arrobjContractProperties, $this->getCid(), $objAdminDatabase, $intExcludeContractId );

		$strMsg = '';
		if( true == valArr( $arrobjActiveContractProperties ) ) {
			foreach( $arrobjActiveContractProperties as $objActiveContractProperty ) {

				$strBundleName = ( false == is_null( $objActiveContractProperty->getBundleProductName() ) ) ? ' in bundle ' . $objActiveContractProperty->getBundleProductName() : '';
				$strMsg = $strMsg . $objActiveContractProperty->getProductName() . ' product is already associated with active contract ' . $objActiveContractProperty->getContractId() . $strBundleName . '.';

			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', $strMsg ) );
			return false;
		}

		return true;
	}

	public function validateDuplicateContractProperties( $arrobjContractProperties, $objAdminDatabase, $intExcludeContractId = NULL, $boolIsActive = false ) {

		if( true == valArr( $arrobjContractProperties ) ) {
			$arrintPsProductIds	= array_filter( array_keys( rekeyObjects( 'PsProductId', $arrobjContractProperties ) ) );
			$arrintPropertyIds	= array_filter( array_keys( rekeyObjects( 'PropertyId', $arrobjContractProperties ) ) );
		}

		$arrobjActiveContractProperties	= CContractProperties::createService()->fetchActiveContractPropertiesByPropertyIdsByPsProductIdsByCid( $arrintPropertyIds, $arrintPsProductIds, $this->getCid(), $objAdminDatabase, $intExcludeContractId, $boolIsActive );

		$strMsg = '';
		if( true == valArr( $arrobjActiveContractProperties ) ) {
			foreach( $arrobjActiveContractProperties as $objActiveContractProperty ) {

				$strBundleName = ( false == is_null( $objActiveContractProperty->getBundleProductName() ) ) ? ' in bundle ' . $objActiveContractProperty->getBundleProductName() : '';
				$strMsg = $strMsg . $objActiveContractProperty->getProductName() . ' product is already associated with active contract ' . $objActiveContractProperty->getContractId() . $strBundleName . '.';
			}

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', $strMsg ) );
			return false;
		}

		return true;
	}

	public function processContractTerminationRequests( $intContractTerminationRequestId, $objContractTerminationRequest, $objEmployee, $objUser, $strTerminationStatus, $boolRequestFromEntrata, $objAdminDatabase, $objEmailDatabase, $objConnectDatabase = NULL, $objVoipDatabase = NULL, $objUtilitiesDatabase = NULL, $objObjectStorageGateway = NULL ) {

		$arrobjBundleContractProperty = [];
		$arrintUemContractPropertyIds = [];

		if( true == $boolRequestFromEntrata ) {
			$intUserId = ENTRATA_USER_ID;
		} else {
			$intUserId = $objUser->getId();
		}

		$arrstrReactivationContractPropertyPricingsfields	= [ 'end_date' ];
		$arrobjReactivateContractPropertyPricings			= [];

		$objTaskReference = CTaskReferences::createService()->fetchTaskReferenceByTaskReferenceTypeIdByReferenceNumber( CTaskReferenceType::CONTRACT_TERMINATION, $intContractTerminationRequestId, $objAdminDatabase );
		if( 1 == $strTerminationStatus && false == $boolRequestFromEntrata ) {
			if( true == in_array( $objEmployee->getDepartmentId(), CDepartment::$c_arrintSalesDepartmentIds ) ) {
				$objContractTerminationRequest->setSalesApprovedBy( $objEmployee->getId() );
				$objContractTerminationRequest->setSalesApprovedOn( 'NOW()' );
			}

			$objContractTerminationRequest->setPostedBy( $objEmployee->getId() );
			$objContractTerminationRequest->setPostedOn( 'NOW()' );

			if( false == $objContractTerminationRequest->processContractTerminationEvents( $objUser->getId(), $objAdminDatabase, $objEmailDatabase, $arrobjContractProperties = NULL, $boolExecutePressingEvents = false, NULL, false, NULL, $objConnectDatabase, $objVoipDatabase, $objObjectStorageGateway ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Application Error: Unable to process contract termination events.' ) ) );
				return;
			}

			$this->m_arrobjContracts = $objContractTerminationRequest->fetchContracts( $objAdminDatabase );

			$strMessage  = __( 'Contract termination approved.' );
			if( CContractTerminationReason::LOST_MANAGEMENT != $objContractTerminationRequest->getContractTerminationReasonId() ) {
				$strTaskNote = __( 'Termination request has been approved.' );

				if( true == valObj( $objTaskReference, 'CTaskReference' ) && false == $objTaskReference->createTerminationTaskNoteAndEmailSummary( $strTaskNote, $intUserId, $objAdminDatabase ) ) {
					$this->addErrorMsgs( __( 'Application Error: Unable to add task notes.' ) );
					return;
				}
			}

		} else {
			$this->m_arrobjContractProperties 	= CContractProperties::createService()->fetchAllContractPropertiesByContractTerminationRequestId( $intContractTerminationRequestId, $objAdminDatabase );

			if( true == valArr( $this->m_arrobjContractProperties ) ) {
				$arrintContractPropertiesIds = array_keys( $this->m_arrobjContractProperties );
			}

			$this->m_arrintDisabledPropertyIds = CContractProperties::createService()->fetchDisabledPropertiesByIds( $arrintContractPropertiesIds, $objAdminDatabase );

			if( true == valArr( $this->m_arrintDisabledPropertyIds ) && false == $this->activateDisabledProperties( $intUserId, $objAdminDatabase, $objConnectDatabase ) ) {
				$this->addErrorMsgs( $this->getErrorMsgs() );
				return;
			}

			if( true == valArr( $this->m_arrobjContractProperties ) ) {
				$this->m_arrobjContracts = CContracts::createService()->fetchContractsByIds( array_keys( rekeyObjects( 'ContractId', $this->m_arrobjContractProperties ) ), $objAdminDatabase );

				if( true == valArr( $this->m_arrobjContracts ) ) {
					foreach( $this->m_arrobjContracts as $objContract ) {

						$arrboolProductAssignmentError[$objContract->getId()] = $objContract->validatePropertyProductAssignment( $this->m_arrobjContractProperties, $objContract->getCid(), $objAdminDatabase );

						if( true == $arrboolProductAssignmentError[$objContract->getId()]['is_associate_in_different_contract'] ) {
							$this->addErrorMsgs( $objContract->getErrorMsgs() );
							return;
						}
					}
				}

				$this->m_arrmixDisabledProperties = CProperties::createService()->fetchPropertiesByIds( array_keys( rekeyObjects( 'PropertyId', $this->m_arrobjContractProperties ) ), $objAdminDatabase, true );

				if( true == valArr( $this->m_arrmixDisabledProperties ) ) {
					if( true == $boolRequestFromEntrata ) {
						echo json_encode( __( 'Cannot cancel the termination request as it has few properties disabled.' ) );
					} else {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, __( 'Cannot deny the request as it has few disabled properties. You can deny single property-product combination by clicking on "+".' ) ) );
						echo json_encode( [ 'success' => false ] );
					}
					return;
				}
			}

			$arrobjContractTerminationEvents = CContractTerminationEvents::createService()->fetchContractTerminationEventsByContractTerminationRequestIdByContractTerminationEventTypeId( $objContractTerminationRequest->getId(), CContractTerminationEventType::TERMINATE_BUNDLE_PRODUCT, $objAdminDatabase );

			if( false == $boolRequestFromEntrata ) {
				$strAppendContractPropertyIds = ' for Contract Properties ' . implode( ',', $arrintContractPropertiesIds );
				$objContractTerminationRequest->setTerminationReason( $objContractTerminationRequest->getTerminationReason() . ' - Denied by ' . $objEmployee->getNameFull() . $strAppendContractPropertyIds );
			}

			if( false == $boolRequestFromEntrata && true == in_array( $objEmployee->getDepartmentId(), CDepartment::$c_arrintSalesDepartmentIds ) ) {
				$objContractTerminationRequest->setSalesApprovedBy( NULL );
				$objContractTerminationRequest->setSalesApprovedOn( NULL );
			}

			$objContractTerminationRequest->setPostedBy( NULL );
			$objContractTerminationRequest->setPostedOn( NULL );

			if( true == $boolRequestFromEntrata ) {
				$objContractTerminationRequest->setContractTerminationRequestStatusId( CContractTerminationRequestStatus::CANCELLED );
			} else {
				$objContractTerminationRequest->setContractTerminationRequestStatusId( CContractTerminationRequestStatus::DENIED );
			}

			// Activating terminated contract properties.
			$arrintUnlimitedPsProductsIds = array_keys( rekeyArray( 'ps_product_id', ( array ) CContractProperties::createService()->fetchAllUnlimitedPsProductsByCid( $this->getCid(), $objAdminDatabase ) ) );
			if( true == valArr( $this->m_arrobjContractProperties ) ) {

				$arrobjReactivateContractPropertyPricings = CContractPropertyPricings::createService()->fetchLatestContractPropertyPricingIdByContractPropertyIds( array_keys( $this->m_arrobjContractProperties ), $objAdminDatabase );

				$this->m_arrobjActivePropertyUtilitySettings = $this->m_arrobjUtilityUemSettings = $this->m_arrobjPropertyUtilityTypes = $this->m_arrobjNewPropertyUtilityTypes = $arrobjPropertyUtilityTypes = [];

				if( true == valObj( $objUtilitiesDatabase, 'CDatabase' ) ) {

					$arrintPropertyIds = array_keys( rekeyObjects( 'PropertyId', $this->m_arrobjContractProperties ) );
					$arrintCids = array_keys( rekeyObjects( 'Cid', $this->m_arrobjContractProperties ) );
					$arrobjActivePropertyUtilitySettings = ( array ) ( rekeyObjects( 'PropertyId', \Psi\Eos\Utilities\CPropertyUtilitySettings::createService()->fetchActivePropertyUtilitySettingsByCidsByPropertyIds( array_unique( $arrintCids ), array_unique( $arrintPropertyIds ), $objUtilitiesDatabase ) ) );
					$arrobjUtilityUemSettings = ( array ) ( rekeyObjects( 'PropertyId', \Psi\Eos\Utilities\CUtilityUemSettings::createService()->fetchOnlyUtilityUemSettingsByPropertyIds( array_unique( $arrintPropertyIds ), $objUtilitiesDatabase ) ) );
					$arrobjActivePropertyUtilityTypes = ( array ) ( \Psi\Eos\Utilities\CPropertyUtilityTypes::createService()->fetchAllPropertyUtilityTypesByPropertyIds( array_unique( $arrintPropertyIds ), $objUtilitiesDatabase ) );

					foreach( $arrobjActivePropertyUtilityTypes as $objActivePropertyUtilityType ) {
						$arrobjPropertyUtilityTypes[$objActivePropertyUtilityType->getPropertyId()][$objActivePropertyUtilityType->getUtilityTypeId()] = $objActivePropertyUtilityType;
					}
				}

				foreach( $this->m_arrobjContractProperties as $objContractProperty ) {
					if( true == in_array( $objContractProperty->getPsProductId(), $arrintUnlimitedPsProductsIds ) ) {
						$objContractProperty->setIsUnlimited( 1 );
					}

					if( true == valObj( $objUtilitiesDatabase, 'CDatabase' ) && false == empty( $objContractProperty->getTerminationDate() ) && strtotime( $objContractProperty->getTerminationDate() ) <= strtotime( date( 'Y-m-d' ) ) ) {

						if( CPsProduct::RESIDENT_UTILITY == $objContractProperty->getPsProductId() && true == array_key_exists( $objContractProperty->getPropertyId(), $arrobjActivePropertyUtilitySettings ) ) {

							$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setIsInImplementation( true );
							$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setBillingImplementationEmployeeId( NULL );
							$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setBillingImplementationAssignedOn( NULL );
							$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setBillingImplementationScheduledOn( NULL );
							$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setBillingImplementationCompletedOn( NULL );
							$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setUpdatedBy( $intUserId );
							$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setUpdatedOn( 'NOW()' );
							$this->m_arrobjActivePropertyUtilitySettings[] = $arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()];
						}

						if( CPsProduct::UTILITY_EXPENSE_MANAGEMENT == $objContractProperty->getPsProductId() ) {

							if( true == array_key_exists( $objContractProperty->getPropertyId(), $arrobjActivePropertyUtilitySettings ) ) {

								$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setUemImplementationEmployeeId( NULL );
								$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setUemImplementationAssignedOn( NULL );
								$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setUemImplementationScheduledOn( NULL );
								$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setUemImplementationCompletedOn( NULL );
								$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setUpdatedBy( $intUserId );
								$arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()]->setUpdatedOn( 'NOW()' );
								$this->m_arrobjActivePropertyUtilitySettings[] = $arrobjActivePropertyUtilitySettings[$objContractProperty->getPropertyId()];

								$arrobjUtilityUemSettings[$objContractProperty->getPropertyId()]->setIsUemImplementation( true );
								$arrobjUtilityUemSettings[$objContractProperty->getPropertyId()]->setUpdatedBy( $intUserId );
								$arrobjUtilityUemSettings[$objContractProperty->getPropertyId()]->setUpdatedOn( 'NOW()' );
								$this->m_arrobjUtilityUemSettings[] = $arrobjUtilityUemSettings[$objContractProperty->getPropertyId()];
							}

							$arrintUemContractPropertyIds[$objContractProperty->getPropertyId()] = $objContractProperty->getPropertyId();

						}

						if( true == array_key_exists( $objContractProperty->getPropertyId(), $arrobjPropertyUtilityTypes ) ) {

							foreach( $arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()] as $objPropertyUtilityType ) {
								$this->m_arrobjPropertyUtilityTypes[] = $objPropertyUtilityType;
							}
						}

						if( true == in_array( $objContractProperty->getPsProductId(), [ CPsProduct::RESIDENT_UTILITY, CPsProduct::UTILITY_EXPENSE_MANAGEMENT ] ) ) {

							$arrmixUtilityTypes = array( CUtilityType::WATER => 'Water', CUtilityType::SEWER => 'Sewer', CUtilityType::TRASH => 'Trash', CUtilityType::GAS => 'Gas', CUtilityType::ELECTRIC => 'Electric' );

							foreach( $arrmixUtilityTypes as $intKey => $strValue ) {

								if( false == array_key_exists( $objContractProperty->getPropertyId(), $arrobjPropertyUtilityTypes ) || false == array_key_exists( $intKey, $arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()] ) ) {

									$objPropertyUtilityType = new CPropertyUtilityType();
									$objPropertyUtilityType->setCid( current( $arrintCids ) );
									$objPropertyUtilityType->setPropertyId( $objContractProperty->getPropertyId() );
									$objPropertyUtilityType->setUtilityTypeId( $intKey );
									$objPropertyUtilityType->setName( $strValue );
									$objPropertyUtilityType->setCreatedBy( $intUserId );
									$objPropertyUtilityType->setCreatedOn( 'NOW()' );
									$objPropertyUtilityType->setUpdatedBy( $intUserId );
									$objPropertyUtilityType->setUpdatedOn( 'NOW()' );

									$this->m_arrobjNewPropertyUtilityTypes[$objContractProperty->getPropertyId() . '_' . $intKey] = $objPropertyUtilityType;
								} else if( false == empty( $arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey]->getDeletedBy() ) || false == empty( $arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey]->getDisabledBy() ) ) {

									$intPropretyUtilitytypeId = $arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey]->getId();
									$arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey] = new CPropertyUtilityType();
									$arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey]->setId( $intPropretyUtilitytypeId );
									$arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey]->setCid( current( $arrintCids ) );
									$arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey]->setPropertyId( $objContractProperty->getPropertyId() );
									$arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey]->setUtilityTypeId( $intKey );
									$arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey]->setName( $strValue );
									$arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey]->setCreatedBy( $intUserId );
									$arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey]->setCreatedOn( 'NOW()' );
									$arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey]->setUpdatedBy( $intUserId );
									$arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey]->setUpdatedOn( 'NOW()' );

									$this->m_arrobjPropertyUtilityTypes[] = $arrobjPropertyUtilityTypes[$objContractProperty->getPropertyId()][$intKey];
								}
							}
						}

					}

					$objContractProperty->setTerminationDate( NULL );
					$objContractProperty->setDeactivationDate( NULL );
					$objContractProperty->setContractTerminationRequestId( NULL );
					$arrintContractPropertyIds[] = $objContractProperty->getId();

				}

				$arrobjCompanyCharges	= CCompanyCharges::createService()->fetchCompanyChargesByCidByContractPropertyIds( $objContractTerminationRequest->getCid(), array_filter( array_keys( $this->m_arrobjContractProperties ), 'is_numeric' ), $objAdminDatabase );
				$arrobjAccounts			= CAccounts::createService()->fetchAccountsByIds( array_filter( array_keys( rekeyObjects( 'RecurringAccountId', $this->m_arrobjContractProperties ) ), 'is_numeric' ), $objAdminDatabase );
			}

			// Reactivation of single terminated product from bundle
			$arrobjPsProductBundleContractProperties = CContractProperties::createService()->fetchAssociateContractPropertiesIdsOfBundleIdsByContractPropertyIds( $arrintContractPropertiesIds, false, $objAdminDatabase );

			if( true == valArr( $arrintContractPropertiesIds ) ) {
				foreach( $arrintContractPropertiesIds as $objContractPropertisId ) {
					$objContractTerminationHistory = new CContractTerminationHistory();
					$objContractTerminationHistory->setCid( $objContractTerminationRequest->getCid() );
					$objContractTerminationHistory->setContractTerminationRequestId( $intContractTerminationRequestId );
					$objContractTerminationHistory->setDeniedBy( $objUser->getId() );
					$objContractTerminationHistory->setContractPropertyId( $objContractPropertisId );

					if( false == $objContractTerminationHistory->insert( $objUser->getId(), $objAdminDatabase ) ) {
						$strFailledMsg = __( 'Failed to insert contract termination history.' );
					}
				}
			}

			if( false == $boolRequestFromEntrata ) {
				$strMessage = __( 'Contract termination denied.' );
			}

			if( valStr( $objContractTerminationRequest->getReasonForDeny() ) ) {
				$objContractTerminationRequest->sendTerminationDenyEmail( $intUserId, $objAdminDatabase, $objEmailDatabase );
			}
		}

		$strFailledMsg = __( 'Failed to update approval status.' );

		switch( NULL ) {
			default:

				$strErrorMsgs = '';

				$objAdminDatabase->begin();

				if( true == $boolRequestFromEntrata ) {
					if( false == $objContractTerminationRequest->validate( VALIDATE_DELETE ) ) {
						$strErrorMsgs .= getConsolidatedErrorMsg( $objContractTerminationRequest );
						$boolIsValid  = false;
						break;
					}

					if( false == $objContractTerminationRequest->update( $intUserId, $objAdminDatabase ) ) {
						$objAdminDatabase->rollback();
						$strErrorMsgs .= __( 'Failed to update contract termination request.' );
						$boolIsValid  = false;
						break;
					}

					$strTaskNote = __( 'Termination request is cancelled.' );
					if( true == valObj( $objTaskReference, 'CTaskReference' ) && false == $objTaskReference->createTerminationTaskNoteAndEmailSummary( $strTaskNote, $intUserId, $objAdminDatabase ) ) {
						$strFailledMsg .= __( 'Failed to add task notes.' );
						$boolIsValid   = false;
						break;
					}
				}

				if( false == $objContractTerminationRequest->validate( 'approval_or_dismiss' ) ) {
					break;
				}

				// Soft deleting the denied termination request document.
				if( CContractTerminationRequestStatus::DENIED == $objContractTerminationRequest->getContractTerminationRequestStatusId() || CContractTerminationRequestStatus::CANCELLED == $objContractTerminationRequest->getContractTerminationRequestStatusId() ) {
					$arrobjContractTerminationEventsByType = rekeyObjects( 'ContractTerminationEventTypeId', $arrobjContractTerminationEvents );
					$arrobjDeletePsDocuments = [];
					if( array_key_exists( CContractTerminationEventType::TERMINATION_DOCUMENT, $arrobjContractTerminationEventsByType ) ) {
						$strContractTerminationEventParams    = $arrobjContractTerminationEventsByType[CContractTerminationEventType::TERMINATION_DOCUMENT]->getEventParameters();
						$arrContractTerminationEventParams    = json_decode( $strContractTerminationEventParams, true );
						$arrContractTerminationEventDocuments = $arrContractTerminationEventParams['DOCUMENT_NAME'];
						if( valArr( $arrContractTerminationEventDocuments ) ) {
							$arrstrPsDocuments = array_values( $arrContractTerminationEventDocuments );
							$arrintContractIds = array_keys( $arrContractTerminationEventDocuments );
							$objPsDocuments    = CPsDocuments::createService()->fetchPsDocumentsByFileNamesByContractIds( $arrstrPsDocuments, $arrintContractIds, $objAdminDatabase );
							if( valArr( $objPsDocuments ) ) {
								foreach( $objPsDocuments as $objPsDocument ) {
									$objPsDocument->setDeletedOn( 'NOW()' );
									$objPsDocument->setDeletedBy( $intUserId );
									$arrobjDeletePsDocuments[] = $objPsDocument;
								}
							}
						}
						if( valArr( $arrobjDeletePsDocuments ) && !CPsDocuments::createService()->bulkUpdate( $arrobjDeletePsDocuments, [ 'deleted_on', 'deleted_by' ], $intUserId, $objAdminDatabase ) ) {
							$objAdminDatabase->rollback();
							return false;
						}
					}
				}

				if( valArr( $arrobjContractTerminationEvents ) ) {
					foreach( $arrobjContractTerminationEvents as $objContractTerminationEvent ) {
						$objContractTerminationEvent->setContractTerminationEventStatusId( CContractTerminationEventStatus::CANCELLED );
					}
				}

				if( valArr( $arrobjContractTerminationEvents ) && !CContractTerminationEvents::createService()->bulkUpdate( $arrobjContractTerminationEvents, [ 'contract_termination_event_status_id' ], $intUserId, $objAdminDatabase ) ) {
					$objAdminDatabase->rollback();
					break;
				}

				if( true == valArr( $arrobjContractTerminationEvents ) ) {
						$arrboolPropertyidWithEventTypeId = [];
						foreach( $arrobjContractTerminationEvents As $objContractTerminationEvent ) {
							$boolFailed = false;
							$objExistingContractProperty = CContractProperties::createService()->fetchContractPropertyById( $objContractTerminationEvent->getContractPropertyId(), $objAdminDatabase );

							if( true == valObj( $objExistingContractProperty, 'CContractProperty' ) ) {

								if( true != $arrboolPropertyidWithEventTypeId[$objExistingContractProperty->getPropertyId()][$objContractTerminationEvent->getContractTerminationEventTypeId()] ) {

									$this->m_objRollbackContractTerminationEvent = $objContractTerminationEvent;

									// RollBack Functionality for Default Merchant Account.
									if( CContractTerminationEventType::TERMINATE_DEFAULT_MERCHANT_ACCOUNT == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackDefaultMerchantAccountSettings( $intUserId, $objAdminDatabase, $objConnectDatabase ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for charge code specific Default Merchant Account.
									if( CContractTerminationEventType::TERMINATE_CHARGE_CODE_SPECIFIC_MERCHANT_ACCOUNT == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackMerchantAccountSettings( $intUserId, $objAdminDatabase, $objConnectDatabase ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for Automatic Post Scheduled Charges.
									if( CContractTerminationEventType::TERMINATE_AUTOMATIC_POST_SCHEDULED_CHARGES == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackAutomaticPostScheduledChargesSettings( $intUserId, $objAdminDatabase, $objConnectDatabase ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for Disable ILS Portal.
									if( CContractTerminationEventType::TERMINATE_DISABLE_ILS_PORTAL == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackDisableILSPortalSettings( $intUserId, $objAdminDatabase, $objConnectDatabase ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for Disable Resident Pay.
									if( CContractTerminationEventType::TERMINATE_DISABLE_RESIDENT_PAY == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackDisableResidentPaySettings( $intUserId, $objAdminDatabase, $objConnectDatabase ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for Disable Rent Reminder Setting.
									if( CContractTerminationEventType::TERMINATE_ENABLE_RENT_REMINDER == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackEnableRentReminderSettings( $intUserId, $objAdminDatabase, $objConnectDatabase ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for Disable Leasing Center Setting.
									if( CContractTerminationEventType::TERMINATE_DISABLE_LEASING_CENTER == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackEnableLeasingCenterSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objVoipDatabase ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for Disable Document Template.
									if( CContractTerminationEventType::TERMINATE_DOCUMENT_TEMPLATE == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackDocumentTemplateSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $this->m_arrobjRollbackContractTerminationEvents, CDocumentAssociationType::DOCUMENT_TEMPLATE ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for Disable Document Packets.
									if( CContractTerminationEventType::TERMINATE_DOCUMENT_PACKETS == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackDocumentTemplateSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $this->m_arrobjRollbackContractTerminationEvents, CDocumentAssociationType::PACKET ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for Terminate Property Websites.
									if( CContractTerminationEventType::TERMINATE_PROPERTY_WEBSITES == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackTerminatePropertyWebsiteSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $this->m_arrobjRollbackContractTerminationEvents ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for Deactivating Websites.
									if( CContractTerminationEventType::DEACTIVATE_WEBSITES == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackDeactivatedWebsiteSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $this->m_arrobjRollbackContractTerminationEvents ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for Dissassociating Rental applications.
									if( CContractTerminationEventType::DISASSOCIATE_RENTAL_APPLICATION_DOCUMENT == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackDisassociateRentalApplicationsSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $this->m_arrobjRollbackContractTerminationEvents ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for Dissassociating Property Integration.
									if( CContractTerminationEventType::DISASSOCIATE_PROPERTY_INTEGRATION == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackDisassociateIntegratedPropertySettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $this->m_arrobjRollbackContractTerminationEvents ) ) {
										$boolFailed = true;
									}

									// RollBack Functionality for removing recurring payments.
									if( CContractTerminationEventType::REMOVE_RESIDENT_RECURRING_PAYMENTS == $objContractTerminationEvent->getContractTerminationEventTypeId() && false == $this->rollBackRemoveRecidentRecurringPaymentsSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $this->m_arrobjRollbackContractTerminationEvents ) ) {
										$boolFailed = true;
									}

									if( true == $boolFailed && true == array_key_exists( $objContractTerminationEvent->getContractTerminationEventTypeId(), CContractTerminationEventType::$c_arrstrTerminationEventsTables ) ) {
										$this->m_arrintFailedContractTerminationEventTypeIds[$objContractTerminationEvent->getId()] = $objContractTerminationEvent->getContractTerminationEventTypeId();
										$objProperty = CProperties::createService()->fetchPropertiesByContractPropertyId( $objContractTerminationEvent->getContractPropertyId(), $objAdminDatabase );
										$this->m_arrstrFailedPropertyEventNames[$objProperty->getId() . '-' . $objProperty->getPropertyName()][$objContractTerminationEvent->getContractTerminationEventTypeId()] = $objProperty->getPropertyName();
									}

								}

								if( true != $arrboolPropertyidWithEventTypeId[$objExistingContractProperty->getPropertyId()][$objContractTerminationEvent->getContractTerminationEventTypeId()] ) {
									$arrboolPropertyidWithEventTypeId[$objExistingContractProperty->getPropertyId()][$objContractTerminationEvent->getContractTerminationEventTypeId()] = true;
								}

							}
						}

					if( true == valArr( $this->m_arrintFailedContractTerminationEventTypeIds ) ) {

						$strCSMEmailAddress = CEmployees::createService()->fetchCsmEmployeesByCids( [ $objContractTerminationRequest->getCid() ], $objAdminDatabase, true );

						if( true == valStr( $strCSMEmailAddress ) ) {
							$this->sendFailedTerminationEventNotification( $intUserId, $strCSMEmailAddress, $objAdminDatabase, $objEmailDatabase );
						}

					}

				}

				if( false == $objContractTerminationRequest->update( $intUserId, $objAdminDatabase ) ) {
					$objAdminDatabase->rollback();
					break;
				}

				if( true == valArr( $this->m_arrobjContractProperties ) ) {
					foreach( $this->m_arrobjContractProperties as $objContractProperty ) {
						if( true == valArr( $arrobjPsProductBundleContractProperties ) ) {
							foreach( $arrobjPsProductBundleContractProperties as $arrobjPsProductBundleContractProperty ) {
								if( 1 == $arrobjPsProductBundleContractProperty['product_count_in_bundle'] && true == in_array( $objContractProperty->getId(), explode( ',', $arrobjPsProductBundleContractProperty['contract_property_id'] ) ) ) {

									if( false == self::reactivateSinglePsProductBundle( $objUser->getId(), $objContractProperty->getId(), $objAdminDatabase ) ) {
										$strFailledMsg = __( 'Property Product Combinations Already Exists' );
										$objAdminDatabase->rollback();
										break 3;
									}
								} elseif( 1 < $arrobjBundleContractProperty['product_count_in_bundle'] ) {

								$strContractPropertyIdToDelete = CContractProperties::createService()->fetchConflictingContractPropertyByPsProductIdByPropertyIdByCidByContractPropertyId( $objContractProperty->getPsProductId(), $objContractProperty->getPropertyId(), $objContractProperty->getCid(), $objContractProperty->getId(), $objAdminDatabase );

									if( true == valStr( $strContractPropertyIdToDelete ) ) {

										$arrintContractPropertyIdsToDelete = explode( ',', $strContractPropertyIdToDelete );

										if( true == valArr( $arrintContractPropertyIdsToDelete ) && false == CContractProperties::createService()->deleteContractPropertiesByIds( $arrintContractPropertyIdsToDelete, $objAdminDatabase ) ) {
										return false;
										}
									}
								}
							}
						} else {
							if( true == $arrboolProductAssignmentError[$objContractProperty->getContractId()]['is_associate_in_same_contract'] ) {
								$strFailledMsg = __( 'Property Product Combinations Already Exists' );
								$objAdminDatabase->rollback();
								break 2;
							}
						}
					}

					if( true == valArr( $arrobjReactivateContractPropertyPricings ) ) {
						foreach( $arrobjReactivateContractPropertyPricings as $objReactivateContractPropertyPricing ) {
							$objReactivateContractPropertyPricing->setEndDate( NULL );
						}
					}

					if( true == valArr( $arrobjReactivateContractPropertyPricings ) && false == CContractPropertyPricings::createService()->bulkUpdate( $arrobjReactivateContractPropertyPricings, $arrstrReactivationContractPropertyPricingsfields, $intUserId, $objAdminDatabase ) ) {
						$objAdminDatabase->rollback();
						$strErrorMsgs .= __( 'Failed to update contract property pricings.' );
						$boolIsValid = false;
						break;
					}

					foreach( $this->m_arrobjContractProperties as $objContractProperty ) {
						if( false == $objContractProperty->update( $intUserId, $objAdminDatabase ) ) {
							$objAdminDatabase->rollback();
							break 2;
						}
					}
				}

				if( true == valArr( $arrobjCompanyCharges ) ) {
					foreach( $arrobjCompanyCharges as $objCompanyCharge ) {

						if( false == is_null( $objCompanyCharge->getChargeEndDate() ) ) {
							$objCompanyCharge->setChargeEndDate( NULL );
						}

						if( false == $objCompanyCharge->update( $intUserId, $objAdminDatabase ) ) {
							$objAdminDatabase->rollback();
							break 2;
						}
					}
				}

				if( true == valArr( $arrobjAccounts ) ) {
					foreach( $arrobjAccounts as $objAccount ) {

						if( $objAccount->getIsDisabled() == 1 ) {
							$objAccount->setIsDisabled( 0 );
						}

						if( false == $objAccount->update( $intUserId, $objAdminDatabase ) ) {
							$objAdminDatabase->rollback();
							break 2;
						}
					}
				}

				if( true == valArr( $this->m_arrobjContracts ) ) {
					foreach( $this->m_arrobjContracts as $objContract ) {
						$objContract->setContractTerminationDate( $objAdminDatabase );

						if( false == is_null( $objContract->getTerminationDate() ) ) {
							$objContract->setContractStatusTypeId( CContractStatusType::CONTRACT_TERMINATED );
						} else {
							$objContract->setContractStatusTypeId( CContractStatusType::CONTRACT_APPROVED );
						}

						if( false == $objContract->update( $intUserId, $objAdminDatabase ) ) {
							$objAdminDatabase->rollback();
							break 2;
						}
					}
				}

				// Recalculating percentage renewed for this contract.
				$arrobjContractDetails = self::calculatePercentageRenewed( $objAdminDatabase );

				if( true == valArr( $arrobjContractDetails ) ) {
					foreach( $arrobjContractDetails as $objContractDetail ) {
						if( false == $objContractDetail->update( $intUserId, $objAdminDatabase ) ) {
							$objAdminDatabase->rollback();
							break 2;
						}
					}
				}

				$objUtilitiesDatabase->begin();

				if( true == valArr( $this->m_arrobjPropertyUtilityTypes ) ) {

					foreach( $this->m_arrobjPropertyUtilityTypes as $objPropertyUtilityType ) {

						if( true == valObj( $objPropertyUtilityType, 'CPropertyUtilityType' ) ) {

							if( false == $objPropertyUtilityType->update( $intUserId, $objUtilitiesDatabase ) ) {
								$objAdminDatabase->rollback();
								$objUtilitiesDatabase->rollback();
								$this->addErrorMsgs( $objPropertyUtilityType->getErrorMsgs() );
								break;
							}

							$boolIsUemProduct = ( true == array_key_exists( $objPropertyUtilityType->getPropertyId(), $arrintUemContractPropertyIds ) ) ? true : false;

							if( false == $objPropertyUtilityType->mapPropertyUtilityCommodity( $intUserId, $objUtilitiesDatabase, $boolIsUemProduct ) ) {
								$objAdminDatabase->rollback();
								$objUtilitiesDatabase->rollback();
								$this->addErrorMsgs( $objPropertyUtilityType->getErrorMsgs() );
								break;
							}
						}
					}
				}

				if( true == valArr( $this->m_arrobjActivePropertyUtilitySettings ) ) {

					foreach( $this->m_arrobjActivePropertyUtilitySettings as $objActivePropertyUtilitySetting ) {

						if( true == valObj( $objActivePropertyUtilitySetting, 'CPropertyUtilitySetting' ) ) {

							if( false == $objActivePropertyUtilitySetting->update( $intUserId, $objUtilitiesDatabase ) ) {
								$objAdminDatabase->rollback();
								$objUtilitiesDatabase->rollback();
								$this->addErrorMsgs( $objActivePropertyUtilitySetting->getErrorMsgs() );
								break;
							}
						}
					}
				}

				if( true == valArr( $this->m_arrobjUtilityUemSettings ) ) {

					foreach( $this->m_arrobjUtilityUemSettings as $objUtilityUemSetting ) {

						if( true == valObj( $objUtilityUemSetting, 'CUtilityUemSetting' ) ) {

							if( false == $objUtilityUemSetting->update( $intUserId, $objUtilitiesDatabase ) ) {
								$objAdminDatabase->rollback();
								$objUtilitiesDatabase->rollback();
								$this->addErrorMsgs( $objUtilityUemSetting->getErrorMsgs() );
								break;
							}
						}
					}
				}

				if( true == valArr( $this->m_arrobjNewPropertyUtilityTypes ) ) {

					foreach( $this->m_arrobjNewPropertyUtilityTypes as $objPropertyUtilityType ) {

						if( true == valObj( $objPropertyUtilityType, 'CPropertyUtilityType' ) ) {

							if( false == $objPropertyUtilityType->insert( $intUserId, $objUtilitiesDatabase ) ) {
								$objAdminDatabase->rollback();
								$objUtilitiesDatabase->rollback();
								$this->addErrorMsgs( $objPropertyUtilityType->getErrorMsgs() );
								break;
							}

							$boolIsUemProduct = ( true == array_key_exists( $objPropertyUtilityType->getPropertyId(), $arrintUemContractPropertyIds ) ) ? true : false;

							if( false == $objPropertyUtilityType->mapPropertyUtilityCommodity( $intUserId, $objUtilitiesDatabase, $boolIsUemProduct ) ) {
								$objAdminDatabase->rollback();
								$objUtilitiesDatabase->rollback();
								$this->addErrorMsgs( $objPropertyUtilityType->getErrorMsgs() );
								break;
							}
						}
					}
				}

				$objUtilitiesDatabase->commit();
				$objAdminDatabase->commit();

				$this->addSuccessMsg( $strMessage );

				self::rebuildContractPackageDiscounts( $this->m_arrobjContractProperties, $objAdminDatabase, ( bool ) $strTerminationStatus );

				if( false == $boolRequestFromEntrata ) {
					if( true == valObj( $objTaskReference, 'CTaskReference' ) && CContractTerminationRequestStatus::DENIED == $objContractTerminationRequest->getContractTerminationRequestStatusId() ) {

						$strTaskNote = __( 'Termination request is Denied.' );

						if( !$objTaskReference->createTerminationTaskNoteAndEmailSummary( $strTaskNote, $intUserId, $objAdminDatabase ) ) {
							$this->addErrorMsg( 'Failed to add task notes.' );
							return;
						}
					}
					return;
				}

				if( true == $boolRequestFromEntrata ) {
					$objAdminDatabase->commit();
					$this->addSuccessMsg( __( 'Contract termination request cancelled successfully.' ) );
					echo json_encode( [ 'success' => 'success' ] );
					return;
				}
		}
		if( false == $boolRequestFromEntrata ) {
			if( false == valArr( $this->getErrorMsgs() ) && false == valObj( current( $this->getErrorMsgs() ), 'CErrorMsg' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, $strFailledMsg ) );
			}
			echo json_encode( [ 'success' => false ] );
			return;
		}

		if( true == $boolRequestFromEntrata && false == $boolIsValid ) {
			echo json_encode( $strErrorMsgs );
			exit;
		}
	}

	public function reactivateSinglePsProductBundle( $intUserId, $intCurrentContractPropertyId, $objAdminDatabase ) {

		// Fetching the log data by contract property id
		$fltMonthlyRecuringAmount					= 0;
		$arrfltContractPropertyAssociatePercentage	= [];

		$arrobjContractTerminationEvents	= CContractTerminationEvents::createService()->fetchContractTerminationEventsLogDetailsByContractPropertyId( $intCurrentContractPropertyId, $objAdminDatabase );
		$strJson							= $arrobjContractTerminationEvents[0]['event_parameters'];
		$arrobjJson							= json_decode( $strJson );
		$intPsProductBundleId				= $arrobjJson->{'bundle_id'};
		$fltTotalBundleAmount				= $arrobjJson->{'total_bundle_amount'};
		$strContractPropertyIds				= $arrobjJson->{'contract_property_ids'};
		$strContractPropertyIdsPercentage	= $arrobjJson->{'contract_property_id_percentage'};

		$arrintContractPropertyIds 				= explode( ',', $strContractPropertyIds );
		$arrfltContractPropertyIdsPercentage	= explode( ',', $strContractPropertyIdsPercentage );

		if( false == valArr( $arrintContractPropertyIds ) ) {
			return false;
		}

		if( false == valArr( $arrfltContractPropertyIdsPercentage ) ) {
			return false;
		}

		$intCount = 0;
		foreach( $arrintContractPropertyIds as $intContractPropertyId ) {
			$arrfltContractPropertyAssociatePercentage[$intContractPropertyId] = $arrfltContractPropertyIdsPercentage[$intCount];
			$intCount++;
		}

		// This code is for stand alone bundle termination.
		if( true == empty( $arrintContractPropertyIds[0] ) ) {
			return true;
		}

		$arrobjContractProperties = CContractProperties::createService()->fetchContractPropertiesByIds( $arrintContractPropertyIds, $objAdminDatabase );

		if( false == valArr( $arrobjContractProperties ) ) {
			return false;
		}

		// This code is for canceling the termination request of child after reactivation of parent

		$arrintContractTerminationRequestIds = rekeyObjects( 'ContractTerminationRequestId', $arrobjContractProperties );
		if( true == valArr( $arrintContractTerminationRequestIds ) ) {

			$arrintContractTerminationRequestIds = array_keys( $arrintContractTerminationRequestIds );

			if( false == valArr( $arrintContractTerminationRequestIds ) ) {
				return false;
			}

			$arrintContractTerminationRequestIds = array_filter( $arrintContractTerminationRequestIds );

			if( false == valArr( $arrintContractTerminationRequestIds ) ) {
				return false;
			}

			$arrobjContractTerminationRequests = CContractTerminationRequests::createService()->fetchContractTerminationRequestsByIds( $arrintContractTerminationRequestIds, $objAdminDatabase );

			if( false == valArr( $arrobjContractTerminationRequests ) ) {
				return false;
			}

			foreach( $arrobjContractTerminationRequests as $objContractTerminationRequest ) {
				$objContractTerminationRequest->setContractTerminationRequestStatusId( CContractTerminationRequestStatus::CANCELLED );
			}

			if( true == valArr( $arrobjContractTerminationRequests ) && false == CContractTerminationRequests::createService()->bulkUpdate( $arrobjContractTerminationRequests, [ 'contract_termination_request_status_id' ], $intUserId, $objAdminDatabase ) ) {
				$objAdminDatabase->rollback();
				return false;
			}
		}

		$arrobjReactivateContractPropertyPricings = CContractPropertyPricings::createService()->fetchLatestContractPropertyPricingIdByContractPropertyIds( array_keys( $arrobjContractProperties ), $objAdminDatabase );

		if( true == valArr( $arrobjReactivateContractPropertyPricings ) ) {
			foreach( $arrobjReactivateContractPropertyPricings as $objContractPropertyPricing ) {
				$objContractPropertyPricing->setEndDate( NULL );
			}

			if( false == CContractPropertyPricings::createService()->bulkUpdate( $arrobjReactivateContractPropertyPricings, [ 'end_date' ], $intUserId, $objAdminDatabase ) ) {
				$objAdminDatabase->rollback();
				return false;
			}
		}

		foreach( $arrobjContractProperties as $objContractProperty ) {

			$strContractPropertyIdToDelete = CContractProperties::createService()->fetchConflictingContractPropertyByPsProductIdByPropertyIdByCidByContractPropertyId( $objContractProperty->getPsProductId(), $objContractProperty->getPropertyId(), $objContractProperty->getCid(), $objContractProperty->getId(), $objAdminDatabase );
			if( false == is_null( $strContractPropertyIdToDelete ) && false == CContractProperties::createService()->deleteContractPropertiesByIds( explode( ',', $strContractPropertyIdToDelete ), $objAdminDatabase ) ) {
				return false;
			}

			$fltMonthlyRecuringAmount	= ( $arrfltContractPropertyAssociatePercentage[$objContractProperty->getId()] * $fltTotalBundleAmount ) / 100;

			// Setting Contract Properties column values
			$objContractProperty->setBundlePsProductId( $intPsProductBundleId );
			$objContractProperty->setContractTerminationRequestId( NULL );
			$objContractProperty->setTerminationDate( NULL );
			$objContractProperty->setDeactivationDate( NULL );

			if( ( !valId( $objContractProperty->getPackageProductId() ) && !$objContractProperty->applyContractPropertyPricing( $fltMonthlyRecuringAmount, $intUserId, $objAdminDatabase ) )
			    || !$objContractProperty->update( $intUserId, $objAdminDatabase ) ) {
				$objAdminDatabase->rollback();
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to update contract properties.' ) );
				return false;
			}
		}
		$objAdminDatabase->commit();
		return true;
	}

	public function calculatePercentageRenewed( $objAdminDatabase ) {
		$arrobjContractDetails	= [];

		if( true == valArr( $this->m_arrobjContracts ) ) {
			foreach( $this->m_arrobjContracts as $objContract ) {

				$boolIsCalculatePercentageRenewed		= false;
				$fltPercentageRenewed					= 0;
				$intTotalContractProperty				= 0;
				$intRenewedContractPropertyCounter		= 0;

				$arrobjContractProperties = $objContract->fetchContractProperties( $objAdminDatabase, $boolIsOnlyNonTerminatedPsProducts = true );

				if( true == valArr( $arrobjContractProperties ) ) {

					$intTotalContractProperty = \Psi\Libraries\UtilFunctions\count( $arrobjContractProperties );

					foreach( $arrobjContractProperties as $objContractProperty ) {
						if( false == is_null( $objContractProperty->getTempRenewalContractPropertyId() ) ) {
							$boolIsCalculatePercentageRenewed = true;
							$intRenewedContractPropertyCounter++;
						}
					}
				}

				if( true == $boolIsCalculatePercentageRenewed ) {

					$intRenewableContractProperty = $intTotalContractProperty - $intRenewedContractPropertyCounter;

					$fltPercentageRenewed = ( ( $intTotalContractProperty - $intRenewableContractProperty ) / $intTotalContractProperty ) * 100;

					$objContractDetail = $objContract->fetchContractDetail( $objAdminDatabase );
					$objContractDetail->setPercentageRenewed( $fltPercentageRenewed );
					$arrobjContractDetails[$objContractDetail->getId()] = $objContractDetail;
				}
			}
		}
		return $arrobjContractDetails;
	}

	public function updateContractTerminationRequestEvents( $intUserId, $objAdminDatabase ) {
		$boolIsValid = true;

		$arrobjContractTerminationEvents = CContractTerminationEvents::createService()->fetchPendingContractTerminationEventsByContractTerminationRequestId( $this->getId(), $objAdminDatabase, $boolEffectiveDate = false, $boolExcludeEventsOfRequestedDate = true );
		if( true == valArr( $arrobjContractTerminationEvents ) ) {
			foreach( $arrobjContractTerminationEvents as $intKey => $objContractTerminationEvent ) {
				$arrobjContractTerminationEvents[$intKey]->setEffectiveDate( $this->getEffectiveDate( $objContractTerminationEvent->getContractTerminationEventTypeId() ) );
			}
			if( false == CContractTerminationEvents::createService()->bulkUpdate( $arrobjContractTerminationEvents, [ 'effective_date' ], $intUserId, $objAdminDatabase ) ) {
				$boolIsValid = false;
			}
		}
		return $boolIsValid;
	}

	public static function getPropertyTerminatingContractProperties( $arrobjTerminatingContractProperties, $objAdminDatabase ) {

		$arrobjPropertyTerminatingContractProperties = [];

		if( true == valArr( $arrobjTerminatingContractProperties ) ) {

			$arrmixPropertywiseContractProperties	= rekeyObjects( 'PropertyId', $arrobjTerminatingContractProperties, true );
			$arrintPropertyIds						= array_filter( array_keys( $arrmixPropertywiseContractProperties ) );

			$arrmixActiveContractPropertiesCount				= ( array ) CContractProperties::createService()->fetchActiveContractPropertiesCountByPropertyIds( $arrintPropertyIds, $objAdminDatabase );
			$arrmixPropertywiseActiveContractPropertiesCount	= rekeyArray( 'property_id', $arrmixActiveContractPropertiesCount );

			foreach( $arrmixPropertywiseContractProperties as $intPropertyId => $arrobjContractProperties ) {

				if( true === isset( $arrmixPropertywiseActiveContractPropertiesCount[$intPropertyId]['count'] ) && \Psi\Libraries\UtilFunctions\count( $arrobjContractProperties ) >= ( int ) $arrmixPropertywiseActiveContractPropertiesCount[$intPropertyId]['count'] ) {
					$arrobjPropertyTerminatingContractProperties = $arrobjPropertyTerminatingContractProperties + rekeyObjects( 'Id', $arrobjContractProperties );
				}
			}
		}

		return $arrobjPropertyTerminatingContractProperties;
	}

	public function processTerminateDefaultMerchantAccount( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase ) {

		$boolSuccess = false;

		$arrmixPropertyMerchantAccountDetails = json_decode( $strEventParameters, true );

		if( true == valId( $arrmixPropertyMerchantAccountDetails['company_merchant_account_id'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixPropertyMerchantAccountDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyMerchantAccountDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixPropertyMerchantAccountDetails['property_id']]->getCid();
			} else {
				$this->addErrorMsg( __( 'Database failed to load' ) );
				return false;
			}

			if( true == valId( $intClientId ) ) {
				$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
					return false;
				}

				$objClientDatabase->open();
				$objClientDatabase->begin();
				$objPropertyMerchantAccount = CPropertyMerchantAccounts::createService()->fetchDefaultPropertyMerchantAccountByPropertyIdByCid( $arrmixPropertyMerchantAccountDetails['property_id'], $intClientId, $objClientDatabase );

				if( true == valObj( $objPropertyMerchantAccount, 'CPropertyMerchantAccount' ) ) {

					$objPropertyMerchantAccount->setDisabledOn( 'NOW()' );
					$objPropertyMerchantAccount->setDisabledBy( CUser::ID_SYSTEM );

					if( false == $objPropertyMerchantAccount->update( $intUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
						$objClientDatabase->close();
						return false;
					} else {
						$arrmixMerchantAccountDetails = CMerchantAccounts::createService()->fetchMerchantAccountNamesByMerchantAccountIdsByCid( [ $objPropertyMerchantAccount->getCompanyMerchantAccountId() ], $intClientId, $objClientDatabase );

						// Keeping log for Default Merchant Account.
						if( true == valArr( $arrmixMerchantAccountDetails ) ) {
							$objPropertySettingKey = new CPropertySettingKey();
							$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyMerchantAccount->getCompanyMerchantAccountId(), $arrmixMerchantAccountDetails[0]['account_name'], $arrmixPropertyMerchantAccountDetails['property_id'], $intClientId, CPropertySettingKey::DEFAULT_MERCHANT_ACCOUNT );

							if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
								$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
							}
						}

						$objClientDatabase->commit();
					}
				}

				$boolSuccess = true;
				$objClientDatabase->close();
			}
		}

		return $boolSuccess;
	}

	public function processTerminateMerchantAccounts( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase ) {

		$boolSuccess = false;

		$arrmixPropertyMerchantAccountDetails = json_decode( $strEventParameters, true );

		if( true == valArr( $arrmixPropertyMerchantAccountDetails['accounts'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixPropertyMerchantAccountDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyMerchantAccountDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixPropertyMerchantAccountDetails['property_id']]->getCid();
			} else {
				return false;
			}

			if( true == valId( $intClientId ) ) {
				$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
					$this->addErrorMsg( __( 'Database failed to load' ) );
					return false;
				}

				$objClientDatabase->open();
				$objClientDatabase->begin();

				$arrobjPropertyMerchantAccounts = CPropertyMerchantAccounts::createService()->fetchPropertyMerchantAccountDetailsByArCodeIdsByPropertyIdByCid( array_keys( $arrmixPropertyMerchantAccountDetails['accounts'] ), $arrmixPropertyMerchantAccountDetails['property_id'], $intClientId, $objClientDatabase );

				foreach( $arrobjPropertyMerchantAccounts AS $intPropertyMerchantAccountId => $objPropertyMerchantAccount ) {
					$objPropertyMerchantAccount->setDisabledOn( 'NOW()' );
					$objPropertyMerchantAccount->setDisabledBy( CUser::ID_SYSTEM );
				}

				if( true == valArr( $arrobjPropertyMerchantAccounts ) && false == CPropertyMerchantAccounts::createService()->bulkUpdate( $arrobjPropertyMerchantAccounts, [ 'disabled_on', 'disabled_by' ], $intUserId, $objClientDatabase ) ) {
					$objClientDatabase->rollback();
					return false;
				}

				$objClientDatabase->commit();
				$objClientDatabase->close();

				$boolSuccess = true;
			}
		}

		return $boolSuccess;
	}

	public function processTerminateAutomaticPostScheduledCharges( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase ) {

		$boolSuccess = false;

		$arrmixPropertyChargeSettingDetails = json_decode( $strEventParameters, true );

		if( true == isset( $arrmixPropertyChargeSettingDetails['auto_post_scheduled_charges'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixPropertyChargeSettingDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyChargeSettingDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixPropertyChargeSettingDetails['property_id']]->getCid();
			} else {
				return false;
			}

			if( true == valId( $intClientId ) ) {
				$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
					$this->addErrorMsg( __( 'Database failed to load' ) );
					return false;
				}

				$objClientDatabase->open();
				$objPropertyChargeSetting = \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPropertyChargeSettingDetailsByPropertyIdByCid( $arrmixPropertyChargeSettingDetails['property_id'], $intClientId, $objClientDatabase );

				if( true == valObj( $objPropertyChargeSetting, 'CPropertyChargeSetting' ) ) {

					$objPropertyChargeSetting->setAutoPostScheduledCharges( false );

					if( false == $objPropertyChargeSetting->update( $intUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
					} else {
						$boolSuccess = true;

						// Keeping log for Automatic post schedule charge.
						$objPropertySettingKey = new CPropertySettingKey();
						$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', false == $objPropertyChargeSetting->getAutoPostScheduledCharges() ? '0' : '1', false == $objPropertyChargeSetting->getAutoPostScheduledCharges() ? 'No' : 'Yes', $arrmixPropertyChargeSettingDetails['property_id'], $intClientId, CPropertySettingKey::AUTO_POST_RECURRING_CHARGES );

						if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
							$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
						}

						$objClientDatabase->commit();
					}
				}
				$objClientDatabase->close();
			}
		}

		return $boolSuccess;
	}

	public function processTerminateDisableILSPortal( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase ) {
		$boolSuccess = false;

		$arrmixPropertyChargeSettingDetails = json_decode( $strEventParameters, true );

		if( true == isset( $arrmixPropertyChargeSettingDetails['property_id'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixPropertyChargeSettingDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyChargeSettingDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixPropertyChargeSettingDetails['property_id']]->getCid();
			} else {
				return false;
			}

			if( true == valId( $intClientId ) ) {
				$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
					$this->addErrorMsg( __( 'Database failed to load' ) );
					return false;
				}

				$objClientDatabase->open();
				$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByCidByPropertyIdByKey( $intClientId, $arrmixPropertyChargeSettingDetails['property_id'], 'DISABLE_ILSPORTAL', $objClientDatabase );

				if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {

					$objPropertyPreference->setValue( 1 );
					if( false == $objPropertyPreference->update( $intUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
					} else {
						$boolSuccess = true;

						// Keeping log for ILS PORTAL.
						$objPropertySettingKey = new CPropertySettingKey();
						$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyPreference->getValue(), '1' == $objPropertyPreference->getValue() ? 'Yes' : 'No', $arrmixPropertyChargeSettingDetails['property_id'], $intClientId, CPropertySettingKey::DISABLE_ILSPORTAL );

						if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
							$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
						}
						$objClientDatabase->commit();
					}
				} else {
					$objPropertyPreference = new CPropertyPreference();
					$objPropertyPreference->setCid( $intClientId );
					$objPropertyPreference->setPropertyId( $arrmixPropertyChargeSettingDetails['property_id'] );
					$objPropertyPreference->setPropertySettingKeyId( CPropertySettingKey::DISABLE_ILSPORTAL );
					$objPropertyPreference->setKey( CPropertySettingKey::$c_arrstrPropertySettingKey[CPropertySettingKey::DISABLE_ILSPORTAL] );
					$objPropertyPreference->setValue( '1' );

					if( false == $objPropertyPreference->insert( $intUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
					} else {
						$boolSuccess = true;
						// Keeping log for ILS PORTAL.
						$objPropertySettingKey = new CPropertySettingKey();
						$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyPreference->getValue(), '1' == $objPropertyPreference->getValue() ? 'Yes' : 'No', $arrmixPropertyChargeSettingDetails['property_id'], $intClientId, CPropertySettingKey::DISABLE_ILSPORTAL );

						if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
							$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
						}
						$objClientDatabase->commit();
					}
				}
				$objClientDatabase->close();
			}
		}

		return $boolSuccess;
	}

	public function processTerminateDisableResidentPay( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase ) {
		$boolSuccess = false;

		$arrmixPropertyPreferenceDetails = json_decode( $strEventParameters, true );

		if( true == isset( $arrmixPropertyPreferenceDetails['property_id'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixPropertyPreferenceDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyPreferenceDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixPropertyPreferenceDetails['property_id']]->getCid();
			} else {
				return false;
			}

			if( true == valId( $intClientId ) ) {
				$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
					$this->addErrorMsg( __( 'Database failed to load' ) );
					return false;
				}

				$objClientDatabase->open();
				$objClientDatabase->begin();

				$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'DISABLE_RESIDENTPORTAL_RESIDENTPAY', $arrmixPropertyPreferenceDetails['property_id'], $intClientId, $objClientDatabase );

				if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {

					$objPropertyPreference->setValue( 1 );
					if( false == $objPropertyPreference->update( $intUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
					} else {
						$boolSuccess = true;

						// Keeping log for Resident Pay.
						$objPropertySettingKey = new CPropertySettingKey();
						$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyPreference->getValue(), '1' == $objPropertyPreference->getValue() ? 'No' : 'Yes', $arrmixPropertyPreferenceDetails['property_id'], $intClientId, CPropertySettingKey::DISABLE_RESIDENT_PAY );

						if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
							$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
						}

						$objClientDatabase->commit();
					}
				} else {
					$objPropertyPreference = new CPropertyPreference();
					$objPropertyPreference->setCid( $intClientId );
					$objPropertyPreference->setPropertyId( $arrmixPropertyPreferenceDetails['property_id'] );
					$objPropertyPreference->setPropertySettingKeyId( CPropertySettingKey::DISABLE_RESIDENT_PAY );
					$objPropertyPreference->setKey( CPropertySettingKey::$c_arrstrPropertySettingKey[CPropertySettingKey::DISABLE_RESIDENT_PAY] );
					$objPropertyPreference->setValue( 1 );

					if( false == $objPropertyPreference->insert( $intUserId, $objClientDatabase ) ) {
						$this->addErrorMsg( __( 'Database Error: Disable ILS portal setting not disabled.' ) );
						$objClientDatabase->rollback();
					} else {
						$boolSuccess = true;

						// Keeping log for Resident Pay.
						$objPropertySettingKey = new CPropertySettingKey();
						$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyPreference->getValue(), '1' == $objPropertyPreference->getValue() ? 'No' : 'Yes', $arrmixPropertyPreferenceDetails['property_id'], $intClientId, CPropertySettingKey::DISABLE_RESIDENT_PAY );

						if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
							$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
						}

						$objClientDatabase->commit();
					}
				}
				$objClientDatabase->close();
			}
		}

		return $boolSuccess;
	}

	public function processTerminateEnbleRentReminder( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase ) {
		$boolSuccess = false;

		$arrmixPropertyPreferenceDetails = json_decode( $strEventParameters, true );

		if( true == isset( $arrmixPropertyPreferenceDetails['property_id'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixPropertyPreferenceDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyPreferenceDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixPropertyPreferenceDetails['property_id']]->getCid();
			} else {
				return false;
			}

			if( true == valId( $intClientId ) ) {
				$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
					$this->addErrorMsg( __( 'Database failed to load' ) );
					return false;
				}

				$objClientDatabase->open();
				$objClientDatabase->begin();
				$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'DONT_SEND_RENT_REMINDERS', $arrmixPropertyPreferenceDetails['property_id'], $intClientId, $objClientDatabase );

				if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {

					$objPropertyPreference->setValue( 1 );
					if( false == $objPropertyPreference->update( $intUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
					} else {
						$boolSuccess = true;

						// Keeping log for Resident Pay.
						$objPropertySettingKey = new CPropertySettingKey();
						$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyPreference->getValue(), '1' == $objPropertyPreference->getValue() ? 'No' : 'Yes', $arrmixPropertyPreferenceDetails['property_id'], $intClientId, CPropertySettingKey::DONT_SEND_RENT_REMINDERS );

						if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
							$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
						}

						$objClientDatabase->commit();
					}
				} else {
					$objPropertyPreference = new CPropertyPreference();
					$objPropertyPreference->setCid( $intClientId );
					$objPropertyPreference->setPropertyId( $arrmixPropertyPreferenceDetails['property_id'] );
					$objPropertyPreference->setPropertySettingKeyId( CPropertySettingKey::DONT_SEND_RENT_REMINDERS );
					$objPropertyPreference->setKey( CPropertySettingKey::$c_arrstrPropertySettingKey[CPropertySettingKey::DONT_SEND_RENT_REMINDERS] );
					$objPropertyPreference->setValue( 1 );

					if( false == $objPropertyPreference->insert( $intUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
					} else {
						$boolSuccess = true;

						// Keeping log for Resident Pay.
						$objPropertySettingKey = new CPropertySettingKey();
						$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyPreference->getValue(), '1' == $objPropertyPreference->getValue() ? 'No' : 'Yes', $arrmixPropertyPreferenceDetails['property_id'], $intClientId, CPropertySettingKey::DONT_SEND_RENT_REMINDERS );

						if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
							$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
						}

						$objClientDatabase->commit();
					}
				}
				$objClientDatabase->close();
			}
		}

		return $boolSuccess;
	}

	public function processTerminateDisableLeasingCenter( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase, $objVoipDatabase = NULL ) {

		$boolSuccess = false;

		$arrmixPropertyPreferenceDetails = json_decode( $strEventParameters, true );

		if( true == valId( $arrmixPropertyPreferenceDetails['property_id'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixPropertyPreferenceDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyPreferenceDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixPropertyPreferenceDetails['property_id']]->getCid();
			} else {
				return false;
			}

			if( true == isset( $arrmixPropertyPreferenceDetails['LEAD_CHAT_ROUTING_CONDITION']['value'] ) ) {

				if( true == valId( $intClientId ) ) {
					$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

					if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
						$this->addErrorMsg( __( 'Database failed to load' ) );
						return false;
					}

					$objClientDatabase->open();
					$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByCidByPropertyIdByKey( $intClientId, $arrmixPropertyPreferenceDetails['property_id'], 'LEAD_CHAT_ROUTING_CONDITION', $objClientDatabase );

					if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {

						$objPropertyPreference->setValue( 'NONE' );

						if( false == $objPropertyPreference->update( $intUserId, $objClientDatabase ) ) {
							$objClientDatabase->rollback();
						} else {
							$boolSuccess = true;

							// Keeping log for Leasing Center.
							$objPropertySettingKey = new CPropertySettingKey();
							$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', 'NONE', \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $objPropertyPreference->getValue() ) ), $arrmixPropertyPreferenceDetails['property_id'], $intClientId, CPropertySettingKey::LEAD_CHAT_ROUTING_CONDITION );

							if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
								$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
							}

							$objClientDatabase->commit();
						}
					} else {

						$objPropertyPreference = new CPropertyPreference();
						$objPropertyPreference->setCid( $intClientId );
						$objPropertyPreference->setPropertyId( $arrmixPropertyPreferenceDetails['property_id'] );
						$objPropertyPreference->setPropertySettingKeyId( CPropertySettingKey::LEAD_CHAT_ROUTING_CONDITION );
						$objPropertyPreference->setKey( CPropertySettingKey::$c_arrstrPropertySettingKey[CPropertySettingKey::LEAD_CHAT_ROUTING_CONDITION] );
						$objPropertyPreference->setValue( 'NONE' );

						if( false == $objPropertyPreference->insert( $intUserId, $objClientDatabase ) ) {
							$this->addSessionErrorMsg( 'Database Error: Disable ILS portal setting not disabled.' );
							$objClientDatabase->rollback();
						} else {
							$boolSuccess = true;

							// Keeping log for Resident Pay.
							$objPropertySettingKey = new CPropertySettingKey();
							$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyPreference->getValue(), \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $objPropertyPreference->getValue() ) ), $arrmixPropertyPreferenceDetails['property_id'], $intClientId, CPropertySettingKey::LEAD_CHAT_ROUTING_CONDITION );

							if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
								$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
							}

							$objClientDatabase->commit();
						}
					}
					$objClientDatabase->close();
				}
			}

			if( true == valArr( $arrmixPropertyPreferenceDetails[$arrmixPropertyPreferenceDetails['property_id']] ) ) {
				if( true == valId( $intClientId ) ) {
					$objVoipDatabase->open();

					$objPropertyCallSetting = CPropertyCallSettings::createService()->fetchPropertyCallSettingByCidByPropertyId( $intClientId, $arrmixPropertyPreferenceDetails['property_id'], $objVoipDatabase );

					if( true == valObj( $objPropertyCallSetting, 'CPropertyCallSetting' ) ) {

						$objPropertyCallSetting->setLeadRoutingCondition( 'NONE' );
						$objPropertyCallSetting->setMaintenanceRoutingCondition( 'NONE' );
						$objPropertyCallSetting->setMaintenanceEmergencyRoutingCondition( 'NONE' );

						if( false == $objPropertyCallSetting->update( $intUserId, $objVoipDatabase ) ) {
							$objVoipDatabase->rollback();
						} else {
							$boolSuccess = true;

							$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );
							$objClientDatabase->open();

							if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
								return false;
							}

							// Keeping log for PROPERTY_CALL_SETTINGS_LEAD_ROUTING_CONDITION
							$objPropertySettingKey = new CPropertySettingKey();
							$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyCallSetting->getLeadRoutingCondition(), \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $objPropertyCallSetting->getLeadRoutingCondition() ) ), $arrmixPropertyPreferenceDetails['property_id'], $intClientId, CPropertySettingKey::PROPERTY_CALL_SETTINGS_LEAD_ROUTING_CONDITION );

							if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
								$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
							}

							// Keeping log for PROPERTY_CALL_SETTINGS_MAINTENANCE_ROUTING_CONDITION
							$objPropertySettingKey = new CPropertySettingKey();
							$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyCallSetting->getMaintenanceRoutingCondition(), \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $objPropertyCallSetting->getMaintenanceRoutingCondition() ) ), $arrmixPropertyPreferenceDetails['property_id'], $intClientId, CPropertySettingKey::PROPERTY_CALL_SETTINGS_MAINTENANCE_ROUTING_CONDITION );

							if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
								$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
							}

							// Keeping log for PROPERTY_CALL_SETTINGS_MAINTENANCE_EMERGENCY_ROUTING_CONDITION
							$objPropertySettingKey = new CPropertySettingKey();
							$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyCallSetting->getMaintenanceEmergencyRoutingCondition(), \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $objPropertyCallSetting->getMaintenanceEmergencyRoutingCondition() ) ), $arrmixPropertyPreferenceDetails['property_id'], $intClientId, CPropertySettingKey::PROPERTY_CALL_SETTINGS_MAINTENANCE_EMERGENCY_ROUTING_CONDITION );

							if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
								$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
							}

							$objVoipDatabase->commit();
							$objClientDatabase->commit();
						}
					}

					$objVoipDatabase->close();
					$objClientDatabase->close();
				}
			}
		}

		return $boolSuccess;
	}

	public function processTerminateDocumentTemplate( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase ) {

		$boolSuccess = false;

		$arrmixPropertyPreferenceDetails = json_decode( $strEventParameters, true );

		if( true == valId( $arrmixPropertyPreferenceDetails['property_id'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixPropertyPreferenceDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyPreferenceDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixPropertyPreferenceDetails['property_id']]->getCid();
			} else {
				return false;
		    }

			if( true == valArr( $arrmixPropertyPreferenceDetails['document_ids'] ) ) {

				if( true == valId( $intClientId ) ) {

					$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

					if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
						$this->addErrorMsg( __( 'Database failed to load' ) );
						return false;
					}

					$objClientDatabase->open();
					$arrobjPropertyGroupDocuments	= CPropertyGroupDocuments::createService()->fetchPropertyGroupDocumentsByPropertyGroupIdByDocumentIdsByCid( $arrmixPropertyPreferenceDetails['property_id'], array_keys( $arrmixPropertyPreferenceDetails['document_ids'] ), $intClientId, $objClientDatabase );
					$arrobjDocuments				= CDocuments::createService()->fetchDocumentsByIdsByCid( array_keys( $arrmixPropertyPreferenceDetails['document_ids'] ), $intClientId, $objClientDatabase );

					if( true == valArr( $arrobjPropertyGroupDocuments ) ) {

						$arrobjPropertyGroupDocuments = rekeyObjects( 'DocumentId', $arrobjPropertyGroupDocuments );

						if( true == valArr( $arrobjPropertyGroupDocuments ) ) {

							foreach( $arrobjPropertyGroupDocuments AS $intDocumentId => $objCPropertyGroupDocument ) {

								$arrobjAssociatedPropertyGroupDocuments = CPropertyGroupDocuments::createService()->fetchPropertyGroupDocumentIdsByDocumentIdByCid( $intDocumentId, $intClientId, $objClientDatabase );

								if( 1 == \Psi\Libraries\UtilFunctions\count( $arrobjAssociatedPropertyGroupDocuments ) ) {
									$arrobjDocuments[$intDocumentId]->setIsPublished( 0 );
									$arrobjSinglePropertyGroupDocuments[$intDocumentId] = $arrobjDocuments[$intDocumentId];
								}
							}

							if( false == CPropertyGroupDocuments::createService()->bulkDelete( $arrobjPropertyGroupDocuments, $objClientDatabase ) ) {
								$objClientDatabase->rollback();
								return false;
							}

							if( true == valArr( $arrobjSinglePropertyGroupDocuments ) ) {
								if( false == CDocuments::createService()->bulkUpdate( $arrobjSinglePropertyGroupDocuments, [ 'is_published' ], $intUserId, $objClientDatabase ) ) {
									$objClientDatabase->rollback();
									return false;
								}
							}
						}

					}

					$boolSuccess = true;
					$objClientDatabase->close();

				}
			}
		}

		return $boolSuccess;
	}

	public function processTerminatePropertyWebsites( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase ) {

		$boolSuccess = false;
		$arrmixPropertyPreferenceDetails = json_decode( $strEventParameters, true );

		$objClientDatabase			= NULL;
		$objDnsDatabase				= NULL;
		$arrobjConnectDatabases		= [];
		$arrobjWebsiteProperties	= [];
		$arrobjWebsiteDomains		= [];

		if( true == valId( $arrmixPropertyPreferenceDetails['property_id'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixPropertyPreferenceDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyPreferenceDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixPropertyPreferenceDetails['property_id']]->getCid();
			} else {
				return false;
		    }

			$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

			if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
				$this->addErrorMsg( __( 'Failed to load database.' ) );
				return false;
			}

			if( CCloud::REFERENCE_ID_LINDON == CONFIG_CLOUD_REFERENCE_ID ) {
				$objDnsDatabase = CDatabases::createService()->loadDatabaseByDatabaseTypeIdByDatabaseUserTypeId( CDatabaseType::DNS, CDatabaseUserType::PS_PROPERTYMANAGER );
			}

			$arrobjConnectDatabases = self::createLocalConnectDatabases();

			self::performDatabaseOperation( $arrobjConnectDatabases, $objClientDatabase, $objDnsDatabase, 'begin' );

			if( true == valArr( $arrmixPropertyPreferenceDetails['website_domains'] ) ) {
				$arrintWebsiteDomains = [];
				foreach( $arrmixPropertyPreferenceDetails['website_domains'] as $intWebsiteId => $arrmixWebsiteDomainDetails ) {
					foreach( $arrmixWebsiteDomainDetails as $intWebsiteDomainId => $strDomainName ) {
						$arrintWebsiteDomains[$intWebsiteDomainId] = $strDomainName;
					}
				}
			}

			if( true == valArr( $arrintWebsiteDomains ) ) {
				$arrobjWebsiteDomains	= CWebsiteDomains::createService()->fetchWebsiteDomainDetailsByIds( array_keys( $arrintWebsiteDomains ), $objClientDatabase );
			}

			if( true == valArr( $arrmixPropertyPreferenceDetails['website_properties'] ) ) {
				$arrobjWebsiteProperties = CWebsiteProperties::createService()->fetchWebsitePropertiesByIds( array_keys( $arrmixPropertyPreferenceDetails['website_properties'] ), $arrmixPropertyPreferenceDetails['property_id'], $objClientDatabase );
			}

			if( true == valArr( $arrobjWebsiteDomains ) ) {

				foreach( $arrobjWebsiteDomains as $objWebsiteDomain ) {

					$boolSubDomain = ( 2 <= substr_count( $objWebsiteDomain->getWebsiteDomain(), '.' ) ? true : false );

					if( false == $objWebsiteDomain->delete( $intUserId, $objClientDatabase, $arrobjConnectDatabases, $boolSubDomain, $objDnsDatabase, false ) ) {
						self::performDatabaseOperation( $arrobjConnectDatabases, $objClientDatabase, $objDnsDatabase, 'rollback' );
						return false;
					}
				}
			}

			if( true == valArr( $arrobjWebsiteProperties ) && false == CWebsiteProperties::createService()->bulkDelete( $arrobjWebsiteProperties, $objClientDatabase ) ) {
				self::performDatabaseOperation( $arrobjConnectDatabases, $objClientDatabase, $objDnsDatabase, 'rollback' );
				return false;
			}

			$boolSuccess = true;
		}

		self::performDatabaseOperation( $arrobjConnectDatabases, $objClientDatabase, $objDnsDatabase, 'commit' );
		return $boolSuccess;
	}

	public function processDeactivateWebsites( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase ) {

		$boolSuccess = false;
		$arrmixPropertyPreferenceDetails = json_decode( $strEventParameters, true );

		if( true == valId( $arrmixPropertyPreferenceDetails['property_id'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixPropertyPreferenceDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyPreferenceDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixPropertyPreferenceDetails['property_id']]->getCid();
			} else {
				return false;
			}

			$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

			if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
				$this->addErrorMsg( __( 'Database failed to load' ) );
				return false;
			}

			$objClientDatabase->open();

			if( true == valArr( $arrmixPropertyPreferenceDetails['website_ids'] ) ) {

				$arrobjWebsites = CWebsites::createService()->fetchAllWebsitesByIdsByCid( $arrmixPropertyPreferenceDetails['website_ids'], $intClientId, $objClientDatabase );

				foreach( $arrobjWebsites AS $intWebsiteId => $objWebsite ) {
					$objWebsite->setIsDisabled( 1 );

					$objTableLog = new CTableLog();
					$objTableLog->setCid( $intClientId );
					$objTableLog->setTableName( 'website_info' );
					$objTableLog->setReferenceNumber( $intWebsiteId );
					$objTableLog->setAction( 'UPDATE' );
					$objTableLog->setDescription( 'Website website_info has been updated !' );
					$objTableLog->setOldData( 'NULL' );
					$objTableLog->setNewData( 'NULL' );

					$arrobjTableLogs[] = $objTableLog;
				}

				if( true == valArr( $arrobjWebsites ) && false == CWebsites::createService()->bulkUpdate( $arrobjWebsites, [ 'is_disabled' ], $intUserId, $objClientDatabase ) ) {
					$objClientDatabase->rollback();
					return false;
				}

				foreach( $arrobjTableLogs AS $objTableLog ) {
					if( true == valObj( $objTableLog, 'CTableLog' ) && false == $objTableLog->insert( $intUserId, $objClientDatabase, $objTableLog->getTableName(), $objTableLog->getReferenceNumber(), $objTableLog->getAction(), $intClientId, NULL, NULL, $objTableLog->getDescription() ) ) {
						$objClientDatabase->rollback();
						return false;
					}
				}

				$boolSuccess = true;
			}

			$objClientDatabase->close();

		}

		return $boolSuccess;
	}

	public function processDisassociateRentalApplicationDocument( $strEventParameters, $objAdminDatabase, $objConnectDatabase ) {

		$boolSuccess = false;

		$arrmixRentalApplicationsDetails = json_decode( $strEventParameters, true );

		if( true == valId( $arrmixRentalApplicationsDetails['property_id'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixRentalApplicationsDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixRentalApplicationsDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixRentalApplicationsDetails['property_id']]->getCid();
			} else {
				return false;
		    }

			$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

			if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
				$this->addErrorMsg( __( 'Database failed to load' ) );
				return false;
			}

			$objClientDatabase->open();

			if( true == valArr( $arrmixRentalApplicationsDetails['ids'] ) ) {
				$arrobjPropertyApplicationPreferences	= CPropertyApplicationPreferences::createService()->fetchPropertyApplicationPreferencesByCompanyApplicationIdsByPropertyIdByCid( $arrmixRentalApplicationsDetails['ids'], $arrmixRentalApplicationsDetails['property_id'], $intClientId, $objClientDatabase );
				$arrobjPropertyApplications				= CPropertyApplications::createService()->fetchPropertyApplicationByCompanyApplicationIdsByPropertyIdByCid( $arrmixRentalApplicationsDetails['ids'], $arrmixRentalApplicationsDetails['property_id'], $intClientId, $objClientDatabase );

				if( true == valArr( $arrobjPropertyApplicationPreferences ) && false == CPropertyApplicationPreferences::createService()->bulkDelete( $arrobjPropertyApplicationPreferences, $objClientDatabase ) ) {
					$objClientDatabase->rollback();
					return false;
				}

				if( true == valArr( $arrobjPropertyApplications ) && false == CPropertyApplications::createService()->bulkDelete( $arrobjPropertyApplications, $objClientDatabase ) ) {
					$objClientDatabase->rollback();
					return false;
				}

				$boolSuccess = true;
			}

			$objClientDatabase->close();

		}

		return $boolSuccess;
	}

	public function processDisassociatePropertyIntegration( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase ) {

		$boolSuccess = false;

		$arrmixPropertyIntegrationDetails = json_decode( $strEventParameters, true );

		if( true == valId( $arrmixPropertyIntegrationDetails['property_id'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixPropertyIntegrationDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyIntegrationDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixPropertyIntegrationDetails['property_id']]->getCid();
			} else {
				return false;
			}

			$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

			if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
				$this->addErrorMsg( __( 'Database failed to load' ) );
				return false;
			}

			$objClientDatabase->open();

			$objClientDatabase->begin();
			$objAdminDatabase->begin();

			if( true == valArr( $arrmixPropertyIntegrationDetails['database_id'] ) ) {
				$arrobjPropertyIntegrationDatabases	= CPropertyIntegrationDatabases::createService()->fetchPropertyIntegrationDatabasesByPropertyIdsByCid( [ $arrmixPropertyIntegrationDetails['property_id'] ], $intClientId, $objClientDatabase );
				$objProperty						= CProperties::createService()->fetchPropertyByIdByCid( $arrmixPropertyIntegrationDetails['property_id'], $intClientId, $objClientDatabase );

				if( true == valObj( $objProperty, 'CProperty' ) ) {
					$objProperty->setRemotePrimaryKey( NULL );
					$objProperty->setLookupCode( NULL );

					if( true == valArr( $arrobjPropertyIntegrationDatabases ) && false == CPropertyIntegrationDatabases::createService()->bulkDelete( $arrobjPropertyIntegrationDatabases, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
						return false;
					}

					if( false == $objProperty->update( $intUserId, $objClientDatabase, $objAdminDatabase ) ) {
						$objClientDatabase->rollback();
						$objAdminDatabase->rollback();
						$objClientDatabase->close();
						return false;
					}

					$boolSuccess = true;
				}
			}

			$objClientDatabase->commit();
			$objAdminDatabase->commit();
			$objClientDatabase->close();

		}

		return $boolSuccess;
	}

	public function processRemoveResidentRecurringPayment( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase, $objEmailDatabase ) {

		$boolSuccess = false;

		$arrmixScheduledPaymentDetails = json_decode( $strEventParameters, true );

		if( true == valId( $arrmixScheduledPaymentDetails['property_id'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixScheduledPaymentDetails['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixScheduledPaymentDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixScheduledPaymentDetails['property_id']]->getCid();
			} else {
				return false;
			}

			$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

			if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
				$this->addErrorMsg( __( 'Database failed to load' ) );
				return false;
			}

			$objClientDatabase->open();

			if( true == valArr( $arrmixScheduledPaymentDetails['ids'] ) ) {
				foreach( $arrmixScheduledPaymentDetails['ids'] AS $intScheduldePaymentId ) {
					$objScheduledPayment	= CScheduledPayments::createService()->fetchScheduledPaymentByIdByCid( $intScheduldePaymentId, $intClientId, $objClientDatabase );

					if( false == valObj( $objScheduledPayment, 'CScheduledPayment' ) ) {
						return false;
					}

					$boolIsValid = true;
					$boolIsValid &= $objScheduledPayment->validateResidentWorksScheduledPayment( 'validate_resident_works_delete', $objClientDatabase );

					if( false == $boolIsValid ) {
						return false;
					}

					$objScheduledPayment->setIsTranslateEmail( true );
					$boolStoreActivityLog = $this->m_boolIsSendEmails;
					if( false == $objScheduledPayment->delete( $intUserId, $objClientDatabase, NULL, NULL, false, false, $this->m_boolIsSendEmails, $boolStoreActivityLog, $objEmailDatabase ) ) {
						$objClientDatabase->rollback();
						return false;
					}

				}

				$boolSuccess = true;
			}

			$objClientDatabase->commit();
			$objClientDatabase->close();

		}

		return $boolSuccess;
	}

	public function processDisableProperty( $intUserId, $strEventParameters, $objAdminDatabase, $objConnectDatabase ) {

		$boolSuccess = false;

		$arrmixPropertyDetails = json_decode( $strEventParameters, true );

		if( true == valId( $arrmixPropertyDetails['property_id'] ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixPropertyDetails['property_id'] ], $objAdminDatabase );

			$intCountOfActiveContractProperties = CContractProperties::createService()->fetchActiveContractPropertiesCountByPropertyId( $arrmixPropertyDetails['property_id'], $objAdminDatabase );

			if( valId( $intCountOfActiveContractProperties ) ) {
				$this->m_boolActiveProperty = true;
				return false; // The property is active with other products.
			}

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyDetails['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixPropertyDetails['property_id']]->getCid();
			} else {
				return true; // The property is already disabled.
			}

			$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

			if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
				$this->addErrorMsg( __( 'Database failed to load' ) );
				return false;
			}

			$objClientDatabase->open();

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixPropertyDetails['property_id']], 'CProperty' ) ) {

				$objAssetLocation	= CAssetLocations::createService()->fetchAssetLocationByPropertyIdByCid( $arrobjProperties[$arrmixPropertyDetails['property_id']]->getId(), $intClientId, $objClientDatabase );

				$arrobjProperties[$arrmixPropertyDetails['property_id']]->setIsDisabled( true );
				$arrobjProperties[$arrmixPropertyDetails['property_id']]->setDisabledOn( 'NOW()' );

				if( false == $arrobjProperties[$arrmixPropertyDetails['property_id']]->update( $intUserId, $objClientDatabase, $objAdminDatabase ) ) {
					$objClientDatabase->rollback();
					$objAdminDatabase->rollback();
					return false;
				}

				// disable asset_locations
				if( true == valObj( $objAssetLocation, 'CAssetLocation' ) ) {

					$objAssetLocation->setDeletedOn( 'NOW()' );
					$objAssetLocation->setDeletedBy( $intUserId );

					if( false == $objAssetLocation->update( $intUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
						$objAdminDatabase->rollback();
						return false;
					}
				}

				$boolSuccess = true;
			}

			$objClientDatabase->commit();
			$objClientDatabase->close();

		}

		return $boolSuccess;
	}

	public function rollBackDefaultMerchantAccountSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objRollbackContractTerminationEvent = NULL ) {

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldPropertyMerchantAccountDetails = json_decode( $strEventParameters, true );
			if( true == valId( $arrmixOldPropertyMerchantAccountDetails['company_merchant_account_id'] ) ) {
				$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldPropertyMerchantAccountDetails['property_id'] ], $objAdminDatabase );

				if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldPropertyMerchantAccountDetails['property_id']], 'CProperty' ) ) {
					$intClientId = $arrobjProperties[$arrmixOldPropertyMerchantAccountDetails['property_id']]->getCid();
				}

				if( true == valId( $intClientId ) ) {
					$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );
					if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
						return false;
					}

					$objClientDatabase->open();
					$arrobjPropertyMerchantAccount = CPropertyMerchantAccounts::createService()->fetchCustomPropertyMerchantAccountsByPropertyIdByCid( $arrmixOldPropertyMerchantAccountDetails['property_id'], $intClientId, $objClientDatabase, true );

					if( true == valArr( $arrobjPropertyMerchantAccount ) ) {
						$arrobjPropertyMerchantAccount = rekeyObjects( 'PropertyId', $arrobjPropertyMerchantAccount );
						$objPropertyMerchantAccount = $arrobjPropertyMerchantAccount[$arrmixOldPropertyMerchantAccountDetails['property_id']];
					}

					if( true == valObj( $objPropertyMerchantAccount, 'CPropertyMerchantAccount' ) ) {

						$objPropertyMerchantAccount->setCompanyMerchantAccountId( $arrmixOldPropertyMerchantAccountDetails['company_merchant_account_id'] );
						$objPropertyMerchantAccount->setDisabledOn( NULL );
						$objPropertyMerchantAccount->setDisabledBy( NULL );

						if( false == $objPropertyMerchantAccount->update( $intUserId, $objClientDatabase ) ) {
							$objClientDatabase->rollback();
							return false;
						} else {

							$arrmixMerchantAccountDetails = CMerchantAccounts::createService()->fetchMerchantAccountNamesByMerchantAccountIdsByCid( [ $objPropertyMerchantAccount->getCompanyMerchantAccountId() ], $intClientId, $objClientDatabase );

							// Keeping log for Default Merchant Account.
							if( true == valArr( $arrmixMerchantAccountDetails ) ) {
								$objPropertySettingKey = new CPropertySettingKey();
								$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyMerchantAccount->getCompanyMerchantAccountId(), $arrmixMerchantAccountDetails[0]['account_name'], $arrmixOldPropertyMerchantAccountDetails['property_id'], $intClientId, CPropertySettingKey::DEFAULT_MERCHANT_ACCOUNT );

								if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
									$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
								}
							}

							$objClientDatabase->commit();
						}
					}
					$objClientDatabase->close();
				}
			}
		}
		return true;
	}

	public function rollBackMerchantAccountSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objRollbackContractTerminationEvent = NULL ) {

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldPropertyMerchantAccountDetails = json_decode( $strEventParameters, true );
			if( true == valArr( $arrmixOldPropertyMerchantAccountDetails['accounts'] ) ) {
				$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldPropertyMerchantAccountDetails['property_id'] ], $objAdminDatabase );

				if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldPropertyMerchantAccountDetails['property_id']], 'CProperty' ) ) {
					$intClientId = $arrobjProperties[$arrmixOldPropertyMerchantAccountDetails['property_id']]->getCid();
				}

				if( true == valId( $intClientId ) ) {
					$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );
					if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
						return false;
					}

					$objClientDatabase->open();
					$arrobjPropertyMerchantAccounts = CPropertyMerchantAccounts::createService()->fetchPropertyMerchantAccountDetailsByArCodeIdsByPropertyIdByCid( array_keys( $arrmixOldPropertyMerchantAccountDetails['accounts'] ), $arrmixOldPropertyMerchantAccountDetails['property_id'], $intClientId, $objClientDatabase );

					if( true == valArr( $arrobjPropertyMerchantAccounts ) ) {
						foreach( $arrobjPropertyMerchantAccounts AS $intPropertyMerchantAccountId => $objPropertyMerchantAccount ) {
							$objPropertyMerchantAccount->setDisabledOn( NULL );
							$objPropertyMerchantAccount->setDisabledBy( NULL );
							$objPropertyMerchantAccount->setCompanyMerchantAccountId( $arrmixOldPropertyMerchantAccountDetails['accounts'][$objPropertyMerchantAccount->getArCodeId()] );
						}

						if( true == valArr( $arrobjPropertyMerchantAccounts ) && false == CPropertyMerchantAccounts::createService()->bulkUpdate( $arrobjPropertyMerchantAccounts, [ 'disabled_on', 'disabled_by' ], $intUserId, $objClientDatabase ) ) {
							$objClientDatabase->rollback();
							return false;
						}
					}
					$objClientDatabase->close();
				}
			}
		}
		return true;
	}

	public function rollBackAutomaticPostScheduledChargesSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objRollbackContractTerminationEvent = NULL ) {

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldPropertyChargeSetting = json_decode( $strEventParameters, true );
			if( true == isset( $arrmixOldPropertyChargeSetting['auto_post_scheduled_charges'] ) ) {

				$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldPropertyChargeSetting['property_id'] ], $objAdminDatabase );

				if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldPropertyChargeSetting['property_id']], 'CProperty' ) ) {
					$intClientId = $arrobjProperties[$arrmixOldPropertyChargeSetting['property_id']]->getCid();
				}

				if( true == valId( $intClientId ) ) {
					$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );
					if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
						return false;
					}

					$objClientDatabase->open();
					$objPropertyChargeSetting = \Psi\Eos\Entrata\CPropertyChargeSettings::createService()->fetchPropertyChargeSettingDetailsByPropertyIdByCid( $arrmixOldPropertyChargeSetting['property_id'], $intClientId, $objClientDatabase );

					if( true == valObj( $objPropertyChargeSetting, 'CPropertyChargeSetting' ) ) {

						$objPropertyChargeSetting->setAutoPostScheduledCharges( $arrmixOldPropertyChargeSetting['auto_post_scheduled_charges'] );

						if( false == $objPropertyChargeSetting->update( $intUserId, $objClientDatabase ) ) {
							$objClientDatabase->rollback();
							return false;
						} else {

							// Keeping log for ILS PORTAL.
							$objPropertySettingKey = new CPropertySettingKey();
							$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', false == $objPropertyChargeSetting->getAutoPostScheduledCharges() ? '0' : '1', false == $objPropertyChargeSetting->getAutoPostScheduledCharges() ? 'No' : 'Yes', $arrmixOldPropertyChargeSetting['property_id'], $intClientId, CPropertySettingKey::AUTO_POST_RECURRING_CHARGES );

							if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
								$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
							}
							$objClientDatabase->commit();
						}
					}
					$objClientDatabase->close();
				}
			}
		}
		return true;
	}

	public function rollBackDisableILSPortalSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objRollbackContractTerminationEvent = NULL ) {

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldPropertyPreferenceSetting = json_decode( $strEventParameters, true );
			if( true == isset( $arrmixOldPropertyPreferenceSetting['property_id'] ) ) {

				$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldPropertyPreferenceSetting['property_id'] ], $objAdminDatabase );

				if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldPropertyPreferenceSetting['property_id']], 'CProperty' ) ) {
					$intClientId = $arrobjProperties[$arrmixOldPropertyPreferenceSetting['property_id']]->getCid();
				}

				if( true == valId( $intClientId ) ) {
					$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );
					if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
						return false;
					}

					$objClientDatabase->open();
					$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByCidByPropertyIdByKey( $intClientId, $arrmixOldPropertyPreferenceSetting['property_id'], 'DISABLE_ILSPORTAL', $objClientDatabase );

					if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {

						$objPropertyPreference->setValue( $arrmixOldPropertyPreferenceSetting['value'] );

						if( false == $objPropertyPreference->update( $intUserId, $objClientDatabase ) ) {
							$this->addSessionErrorMsg( __( 'Database Error: Automatic Post Scheduled Charges setting was reverted.' ) );
							$objClientDatabase->rollback();
						} else {

							// Keeping log for ILS PORTAL.
							$objPropertySettingKey = new CPropertySettingKey();
							$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyPreference->getValue(), '1' == $objPropertyPreference->getValue() ? 'Yes' : 'No', $arrmixOldPropertyPreferenceSetting['property_id'], $intClientId, CPropertySettingKey::DISABLE_ILSPORTAL );

							if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
								$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
							}
							$objClientDatabase->commit();
						}
					}
					$objClientDatabase->close();
				}
			}
		}
		return true;
	}

	public function rollBackDisableResidentPaySettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objRollbackContractTerminationEvent = NULL ) {

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldPropertyPreferenceSetting = json_decode( $strEventParameters, true );
			if( true == isset( $arrmixOldPropertyPreferenceSetting['property_id'] ) ) {

				$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldPropertyPreferenceSetting['property_id'] ], $objAdminDatabase );

				if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldPropertyPreferenceSetting['property_id']], 'CProperty' ) ) {
					$intClientId = $arrobjProperties[$arrmixOldPropertyPreferenceSetting['property_id']]->getCid();
				}

				if( true == valId( $intClientId ) ) {
					$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );
					if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
						return false;
					}

					$objClientDatabase->open();
					$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'DISABLE_RESIDENTPORTAL_RESIDENTPAY', $arrmixOldPropertyPreferenceSetting['property_id'], $intClientId, $objClientDatabase );

					if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {

						$objPropertyPreference->setValue( $arrmixOldPropertyPreferenceSetting['value'] );

						if( false == $objPropertyPreference->update( $intUserId, $objClientDatabase ) ) {
							$objClientDatabase->rollback();
						} else {

							// Keeping log for Resident Pay.
							$objPropertySettingKey = new CPropertySettingKey();
							$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyPreference->getValue(), '1' == $objPropertyPreference->getValue() ? 'No' : 'Yes', $arrmixOldPropertyPreferenceSetting['property_id'], $intClientId, CPropertySettingKey::DISABLE_RESIDENT_PAY );

							if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
								$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
							}

							$objClientDatabase->commit();
						}
					}
					$objClientDatabase->close();
				}
			}
		}
		return true;
	}

	public function rollBackEnableRentReminderSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objRollbackContractTerminationEvent = NULL ) {

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldPropertyPreferenceSetting = json_decode( $strEventParameters, true );
			if( true == isset( $arrmixOldPropertyPreferenceSetting['property_id'] ) ) {

				$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldPropertyPreferenceSetting['property_id'] ], $objAdminDatabase );

				if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldPropertyPreferenceSetting['property_id']], 'CProperty' ) ) {
					$intClientId = $arrobjProperties[$arrmixOldPropertyPreferenceSetting['property_id']]->getCid();
				}

				if( true == valId( $intClientId ) ) {
					$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );
					if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
						return false;
					}

					$objClientDatabase->open();
					$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferencesByKeyByPropertyIdByCid( 'DONT_SEND_RENT_REMINDERS', $arrmixOldPropertyPreferenceSetting['property_id'], $intClientId, $objClientDatabase );

					if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {

						$objPropertyPreference->setValue( $arrmixOldPropertyPreferenceSetting['value'] );

						if( false == $objPropertyPreference->update( $intUserId, $objClientDatabase ) ) {
							$objClientDatabase->rollback();
						} else {

							// Keeping log for Resident Pay.
							$objPropertySettingKey = new CPropertySettingKey();
							$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyPreference->getValue(), '1' == $objPropertyPreference->getValue() ? 'No' : 'Yes', $arrmixOldPropertyPreferenceSetting['property_id'], $intClientId, CPropertySettingKey::DONT_SEND_RENT_REMINDERS );

							if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
								$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
							}

							$objClientDatabase->commit();
						}
					}
					$objClientDatabase->close();
				}
			}
		}
		return true;
	}

	public function rollBackDocumentTemplateSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objRollbackContractTerminationEvent = NULL, $intDocumentAssociationTypeId = NULL ) {

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldDocumentTemplateSetting = json_decode( $strEventParameters, true );
			if( true == valId( $arrmixOldDocumentTemplateSetting['property_id'] ) ) {

				$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldDocumentTemplateSetting['property_id'] ], $objAdminDatabase );

				if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldDocumentTemplateSetting['property_id']], 'CProperty' ) ) {
					$intClientId = $arrobjProperties[$arrmixOldDocumentTemplateSetting['property_id']]->getCid();
				}

				if( true == valArr( $arrmixOldDocumentTemplateSetting['document_ids'] ) ) {
					if( true == valId( $intClientId ) ) {
						$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

						if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
							return false;
						}

						$objClientDatabase->open();

						$arrobjDocuments = CDocuments::createService()->fetchDocumentsByIdsByCid( array_keys( $arrmixOldDocumentTemplateSetting['document_ids'] ), $intClientId, $objClientDatabase );
						if( true == valArr( $arrobjDocuments ) ) {
							foreach( $arrobjDocuments AS $intDocumentId => $objDocument ) {

								$objDuplicatePropertyGroupDocument = CPropertyGroupDocuments::createService()->fetchPropertyGroupDocumentByDocumentIdByPropertyIdByDocumentAssociationTypeIdByCid( $intDocumentId, $arrmixOldDocumentTemplateSetting['property_id'], $intDocumentAssociationTypeId, $intClientId, $objClientDatabase );

								if( false == valObj( $objDuplicatePropertyGroupDocument, 'CPropertyGroupDocument' ) ) {

									$objPropertyGroupDocument = new CPropertyGroupDocument();
									$objPropertyGroupDocument->setCid( $intClientId );
									$objPropertyGroupDocument->setDocumentId( $intDocumentId );
									$objPropertyGroupDocument->setPropertyGroupId( $arrmixOldDocumentTemplateSetting['property_id'] );
									$objPropertyGroupDocument->setDocumentAssociationTypeId( $intDocumentAssociationTypeId );
									$objPropertyGroupDocument->setIsPublished( 1 );
									$objPropertyGroupDocument->setIsSystem( 0 );

									$arrobjPropertyGroupDocuments[$intDocumentId] = $objPropertyGroupDocument;

									$objDocument->setIsPublished( 1 );
									$arrobjDocuments[$intDocumentId] = $objDocument;
								} else {

									$objDuplicatePropertyGroupDocument->setIsPublished( 1 );
									$arrobjUpdatedPropertyGroupDocuments[$intDocumentId] = $objDuplicatePropertyGroupDocument;

									$objDocument->setIsPublished( 1 );
									$arrobjDocuments[$intDocumentId] = $objDocument;
								}
							}

							if( true == valArr( $arrobjDocuments ) ) {
								if( false == CDocuments::createService()->bulkUpdate( $arrobjDocuments, [ 'is_published' ], $intUserId, $objClientDatabase ) ) {
									$objClientDatabase->rollback();
									return false;
								}
							}

							if( true == valArr( $arrobjUpdatedPropertyGroupDocuments ) ) {
								if( false == CPropertyGroupDocuments::createService()->bulkUpdate( $arrobjUpdatedPropertyGroupDocuments, [ 'is_published' ], $intUserId, $objClientDatabase ) ) {
									$objClientDatabase->rollback();
									return false;
								}
							}

							if( false == CPropertyGroupDocuments::createService()->bulkInsert( $arrobjPropertyGroupDocuments, $intUserId, $objClientDatabase ) ) {
								$objClientDatabase->rollback();
								return false;
							}

							$objClientDatabase->Commit();
							$objClientDatabase->close();
						}
					}
				}
			}
		}
		return true;
	}

	public function rollBackTerminatePropertyWebsiteSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objRollbackContractTerminationEvent = NULL ) {

		$objClientDatabase			= NULL;
		$intClientId				= NULL;
		$arrobjConnectDatabases		= [];
		$arrobjWebsiteProperties	= [];
		$arrobjWebsiteDomains		= [];

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldTerminatedWebsiteSettings = json_decode( $strEventParameters, true );

			if( true == valId( $arrmixOldTerminatedWebsiteSettings['property_id'] ) ) {

				$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldTerminatedWebsiteSettings['property_id'] ], $objAdminDatabase );

				if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldTerminatedWebsiteSettings['property_id']], 'CProperty' ) ) {
					$intClientId = $arrobjProperties[$arrmixOldTerminatedWebsiteSettings['property_id']]->getCid();
				}

				$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
					return false;
				}

				$arrobjConnectDatabases = self::createLocalConnectDatabases();

				self::performDatabaseOperation( $arrobjConnectDatabases, $objClientDatabase, NULL, 'begin' );

				if( true == valId( $intClientId ) ) {

					if( true == valArr( $arrmixOldTerminatedWebsiteSettings['website_properties'] ) ) {

						foreach( $arrmixOldTerminatedWebsiteSettings['website_properties'] as $intWebsiteId => $mixOldTerminatedWebsiteSetting ) {
							$objWebsiteProperty = new CWebsiteProperty();
							$objWebsiteProperty->setCid( $intClientId );
							$objWebsiteProperty->setWebsiteId( $intWebsiteId );
							$objWebsiteProperty->setPropertyId( $arrmixOldTerminatedWebsiteSettings['property_id'] );
							$objWebsiteProperty->setHideOnProspectPortal( $mixOldTerminatedWebsiteSetting['PP'] );
							$objWebsiteProperty->setHideOnResidentPortal( $mixOldTerminatedWebsiteSetting['RP'] );
							$objWebsiteProperty->setHideOnJobPortal( $mixOldTerminatedWebsiteSetting['JP'] );
							$objWebsiteProperty->setHideOnMobilePortal( $mixOldTerminatedWebsiteSetting['MP'] );
							$objWebsiteProperty->setHideOnMobilePortalCorporatePage( $mixOldTerminatedWebsiteSetting['MPCP'] );
							$objWebsiteProperty->setHideMoreInfoButton( $mixOldTerminatedWebsiteSetting['info_button'] );
							$objWebsiteProperty->setShowApplicationFeeOption( $mixOldTerminatedWebsiteSetting['SAFO'] );

							$arrobjDublicateWebsiteProperty = CWebsiteProperties::createService()->fetchWebsitePropertiesByWebsiteIdByPropertyIdsByCid( $objWebsiteProperty->getWebsiteId(), [ $objWebsiteProperty->getPropertyId() ], $objWebsiteProperty->getCid(), $objClientDatabase );

							$arrboolUpdate[$intWebsiteId] = true;

							if( true == valArr( $arrobjDublicateWebsiteProperty ) ) {
								$arrboolUpdate[$intWebsiteId] = false;
							}

							if( true == $arrboolUpdate[$intWebsiteId] ) {
								$arrobjWebsiteProperties[] = $objWebsiteProperty;
							}
						}
					}

					if( true == valArr( $arrmixOldTerminatedWebsiteSettings['website_domains'] ) ) {

						$arrobjWebsiteDomains	= [];

						foreach( $arrmixOldTerminatedWebsiteSettings['website_domains'] As $intWebsiteId => $arrmixOldTerminatedWebsiteSetting ) {
							foreach( $arrmixOldTerminatedWebsiteSetting as $intWebsiteDomainId => $strDomainName ) {
								$objWebsiteDomain = new CWebsiteDomain();
								$objWebsiteDomain->setCid( $intClientId );
								$objWebsiteDomain->setWebsiteId( $intWebsiteId );
								$objWebsiteDomain->setPropertyId( $arrmixOldTerminatedWebsiteSettings['property_id'] );
								$objWebsiteDomain->setWebsiteDomain( $strDomainName );
								$objWebsiteDomain->setEmailDomain( $strDomainName );

								if( true == $arrboolUpdate[$intWebsiteId] ) {
									$arrobjWebsiteDomains[] = $objWebsiteDomain;
								}
							}
						}
					}

					if( true == valArr( $arrobjWebsiteProperties ) && false == CWebsiteProperties::createService()->bulkInsert( $arrobjWebsiteProperties, $intUserId, $objClientDatabase ) ) {
						self::performDatabaseOperation( $arrobjConnectDatabases, $objClientDatabase, NULL, 'rollback' );
						return false;
					}

					if( true == valArr( $arrobjWebsiteDomains ) ) {
						foreach( $arrobjWebsiteDomains as $objWebsiteDomain ) {
							if( false == $objWebsiteDomain->insert( $intUserId, $objClientDatabase, $arrobjConnectDatabases ) ) {
								self::performDatabaseOperation( $arrobjConnectDatabases, $objClientDatabase, NULL, 'rollback' );
								return false;
							}
						}
					}
				}
			}
		}

		self::performDatabaseOperation( $arrobjConnectDatabases, $objClientDatabase, NULL, 'commit' );
		return true;
	}

	public function rollBackDeactivatedWebsiteSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objRollbackContractTerminationEvent = NULL ) {

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldTerminatedWebsiteSettings = json_decode( $strEventParameters, true );
			if( true == valId( $arrmixOldTerminatedWebsiteSettings['property_id'] ) ) {

				$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldTerminatedWebsiteSettings['property_id'] ], $objAdminDatabase );

				if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldTerminatedWebsiteSettings['property_id']], 'CProperty' ) ) {
					$intClientId = $arrobjProperties[$arrmixOldTerminatedWebsiteSettings['property_id']]->getCid();
				}

				$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
					return false;
				}

				$objClientDatabase->open();

				if( true == valId( $intClientId ) ) {
					if( true == valArr( $arrmixOldTerminatedWebsiteSettings['website_ids'] ) ) {

						$arrobjWebsites = CWebsites::createService()->fetchAllWebsitesByIdsByCid( $arrmixOldTerminatedWebsiteSettings['website_ids'], $intClientId, $objClientDatabase );

						foreach( $arrobjWebsites AS $intWebsiteId => $objWebsite ) {
							$objWebsite->setIsDisabled( 0 );

							$objTableLog = new CTableLog();
							$objTableLog->setCid( $intClientId );
							$objTableLog->setTableName( 'website_info' );
							$objTableLog->setReferenceNumber( $intWebsiteId );
							$objTableLog->setAction( 'UPDATE' );
							$objTableLog->setDescription( 'Website website_info has been updated !' );
							$objTableLog->setOldData( 'NULL' );
							$objTableLog->setNewData( 'NULL' );

							$arrobjTableLogs[] = $objTableLog;
						}

						if( true == valArr( $arrobjWebsites ) && false == CWebsites::createService()->bulkUpdate( $arrobjWebsites, [ 'is_disabled' ], $intUserId, $objClientDatabase ) ) {
							$objClientDatabase->rollback();
							return false;
						}

						foreach( $arrobjTableLogs AS $objTableLog ) {
							if( true == valObj( $objTableLog, 'CTableLog' ) && false == $objTableLog->insert( $intUserId, $objClientDatabase, $objTableLog->getTableName(), $objTableLog->getReferenceNumber(), $objTableLog->getAction(), $intClientId, NULL, NULL, $objTableLog->getDescription() ) ) {
								$objClientDatabase->rollback();
								return false;
							}
						}

					}
				}

			}
		}
		return true;
	}

	public function rollBackDisassociateRentalApplicationsSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objRollbackContractTerminationEvent = NULL ) {

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldRentalApplicationsSettings = json_decode( $strEventParameters, true );
			if( true == valId( $arrmixOldRentalApplicationsSettings['property_id'] ) ) {

				$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldRentalApplicationsSettings['property_id'] ], $objAdminDatabase );

				if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldRentalApplicationsSettings['property_id']], 'CProperty' ) ) {
					$intClientId = $arrobjProperties[$arrmixOldRentalApplicationsSettings['property_id']]->getCid();
				}

				$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
					return false;
				}

				$objClientDatabase->open();

				if( true == valId( $intClientId ) ) {
					if( true == valArr( $arrmixOldRentalApplicationsSettings['ids'] ) ) {

						$arrobjPropertyApplicationPreferences	= [];
						$arrobjPropertyApplications				= [];

						foreach( $arrmixOldRentalApplicationsSettings['ids'] AS $intKeys => $intCompanyApplicationId ) {
							$objPropertyApplication = new CPropertyApplication();
							$objPropertyApplication->setCid( $intClientId );
							$objPropertyApplication->setPropertyId( $arrmixOldRentalApplicationsSettings['property_id'] );
							$objPropertyApplication->setCompanyApplicationId( $intCompanyApplicationId );
							$objPropertyApplication->setCustomerRelationships( 'NULL' );
							$objPropertyApplication->setIsPublished( '1' );
							$objPropertyApplication->setOrderNum( 0 );

							$arrobjPropertyApplications[] = $objPropertyApplication;

							// preparing property application preferences array
							$arrobjTempPropertyApplicationPreference = CPropertyApplicationPreferences::createService()->fetchPropertyApplicationPreferenceByCidByPropertyIdByCompanyApplicationIdByKey( $intClientId, $arrmixOldRentalApplicationsSettings['property_id'], $intCompanyApplicationId, 'HIDE_BASIC_INFO_NOT_US_RESIDENT', $objClientDatabase );

							if( false == valArr( $arrobjTempPropertyApplicationPreference ) ) {
								$objPropertyApplicationPreference = $objPropertyApplication->createPropertyApplicationPreference();
								$objPropertyApplicationPreference->setKey( 'HIDE_BASIC_INFO_NOT_US_RESIDENT' );
								$objPropertyApplicationPreference->setValue( 1 );
								array_push( $arrobjPropertyApplicationPreferences, $objPropertyApplicationPreference );
							}

							$arrobjTempPropertyApplicationPreference = CPropertyApplicationPreferences::createService()->fetchPropertyApplicationPreferenceByCidByPropertyIdByCompanyApplicationIdByKey( $intClientId, $arrmixOldRentalApplicationsSettings['property_id'], $intCompanyApplicationId, 'HIDE_BASIC_INFO_GENDER', $objClientDatabase );
							if( false == valArr( $arrobjTempPropertyApplicationPreference ) ) {
								$objPropertyApplicationPreference = $objPropertyApplication->createPropertyApplicationPreference();
								$objPropertyApplicationPreference->setKey( 'HIDE_BASIC_INFO_GENDER' );
								$objPropertyApplicationPreference->setValue( 1 );
								array_push( $arrobjPropertyApplicationPreferences, $objPropertyApplicationPreference );
							}

							$arrobjTempPropertyApplicationPreference = CPropertyApplicationPreferences::createService()->fetchPropertyApplicationPreferenceByCidByPropertyIdByCompanyApplicationIdByKey( $intClientId, $arrmixOldRentalApplicationsSettings['property_id'], $intCompanyApplicationId, 'REQUIRE_BASIC_INFO_MIDDLE_NAME', $objClientDatabase );
							if( false == valArr( $arrobjTempPropertyApplicationPreference ) ) {
								$objPropertyApplicationPreference = $objPropertyApplication->createPropertyApplicationPreference();
								$objPropertyApplicationPreference->setKey( 'REQUIRE_BASIC_INFO_MIDDLE_NAME' );
								$objPropertyApplicationPreference->setValue( 1 );
								array_push( $arrobjPropertyApplicationPreferences, $objPropertyApplicationPreference );
							}
						}

						if( true == valArr( $arrobjPropertyApplications ) && false == CPropertyApplications::createService()->bulkInsert( $arrobjPropertyApplications, $intUserId, $objClientDatabase ) ) {
							$objClientDatabase->rollback();
							return false;
						}

						if( true == valArr( $arrobjPropertyApplicationPreferences ) && false == CPropertyApplicationPreferences::createService()->bulkInsert( $arrobjPropertyApplicationPreferences, $intUserId, $objClientDatabase ) ) {
							$objClientDatabase->rollback();
							return false;
						}

					}
				}

				$objClientDatabase->close();

			}
		}
		return true;
	}

	public function rollBackDisassociateIntegratedPropertySettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objRollbackContractTerminationEvent = NULL ) {

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldPropertyIntegrationSettings = json_decode( $strEventParameters, true );
			if( true == valId( $arrmixOldPropertyIntegrationSettings['property_id'] ) ) {

				$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldPropertyIntegrationSettings['property_id'] ], $objAdminDatabase );

				if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldPropertyIntegrationSettings['property_id']], 'CProperty' ) ) {
					$intClientId = $arrobjProperties[$arrmixOldPropertyIntegrationSettings['property_id']]->getCid();
				}

				$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
					return false;
				}

				$objClientDatabase->open();
				$objClientDatabase->begin();
				$objAdminDatabase->begin();

				if( true == valId( $intClientId ) ) {
					if( true == valArr( $arrmixOldPropertyIntegrationSettings['database_id'] ) ) {

						$arrobjPropertyIntegrationDatabases = [];
						$objProperty                        = CProperties::createService()->fetchPropertyByIdByCid( $arrmixOldPropertyIntegrationSettings['property_id'], $intClientId, $objClientDatabase );

						if( true == valObj( $objProperty, 'CProperty' ) ) {

							$objProperty->setRemotePrimaryKey( $arrmixOldPropertyIntegrationSettings['remote_primary_key'] );
							$objProperty->setLookupCode( $arrmixOldPropertyIntegrationSettings['lookup_code'] );

							foreach( $arrmixOldPropertyIntegrationSettings['database_id'] AS $intKey => $intDatabaseId ) {

								$objPropertyIntegrationDatabase = CPropertyIntegrationDatabases::createService()->fetchCustomPropertyIntegrationDatabaseByPropertyIdByCidByIntegrationDatabaseId( $arrmixOldPropertyIntegrationSettings['property_id'], $intClientId, $intDatabaseId, $objClientDatabase );

								if( false == valObj( $objPropertyIntegrationDatabase, 'CPropertyIntegrationDatabase' ) ) {

									$objPropertyIntegartionDatabase = new CPropertyIntegrationDatabase();
									$objPropertyIntegartionDatabase->setCid( $intClientId );
									$objPropertyIntegartionDatabase->setIntegrationDatabaseId( $intDatabaseId );
									$objPropertyIntegartionDatabase->setPropertyId( $arrmixOldPropertyIntegrationSettings['property_id'] );

									$arrobjPropertyIntegrationDatabases[] = $objPropertyIntegartionDatabase;
								}
							}

							if( true == valArr( $arrobjPropertyIntegrationDatabases ) && false == CPropertyIntegrationDatabases::createService()->bulkInsert( $arrobjPropertyIntegrationDatabases, $intUserId, $objClientDatabase ) ) {
								$objClientDatabase->rollback();
								return false;
							}

							if( false == $objProperty->update( $intUserId, $objClientDatabase, $objAdminDatabase ) ) {
								$objClientDatabase->rollback();
								$objAdminDatabase->rollback();
								$objClientDatabase->close();
								return false;
							}
						}
					}
				}
				$objClientDatabase->commit();
				$objAdminDatabase->commit();
				$objClientDatabase->close();

			}
		}
		return true;
	}

	public function rollBackRemoveRecidentRecurringPaymentsSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objRollbackContractTerminationEvent = NULL ) {

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldScheduledPaymentsSettings = json_decode( $strEventParameters, true );
			if( true == valId( $arrmixOldScheduledPaymentsSettings['property_id'] ) ) {

				$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldScheduledPaymentsSettings['property_id'] ], $objAdminDatabase );

				if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldScheduledPaymentsSettings['property_id']], 'CProperty' ) ) {
					$intClientId = $arrobjProperties[$arrmixOldScheduledPaymentsSettings['property_id']]->getCid();
				}

				if( true == valId( $intClientId ) ) {

					$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

					if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
						return false;
					}

					$objClientDatabase->open();

					if( true == valArr( $arrmixOldScheduledPaymentsSettings['ids'] ) ) {
						$arrobjScheduledPayments = CScheduledPayments::createService()->fetchViewScheduledPaymentByIdsByCid( $arrmixOldScheduledPaymentsSettings['ids'], $intClientId, $objClientDatabase, true );

						if( true == valArr( $arrobjScheduledPayments ) ) {
							foreach( $arrobjScheduledPayments As $intScheduledPaymentId => $objScheduledPayment ) {
								$objScheduledPayment->setDeletedBy( NULL );
								$objScheduledPayment->setDeletedOn( NULL );
							}

							if( false == CScheduledPayments::createService()->bulkUpdate( $arrobjScheduledPayments, [ 'deleted_by', 'deleted_on' ], $intUserId, $objClientDatabase ) ) {
								$objClientDatabase->rollback();
								return false;
							}
						}
					}
					$objClientDatabase->close();
				}
			}
		}
		return true;
	}

	public function rollBackEnableLeasingCenterSettings( $intUserId, $objAdminDatabase, $objConnectDatabase, $objVoipDatabase, $objRollbackContractTerminationEvent = NULL ) {

		if( true == is_null( $objRollbackContractTerminationEvent ) ) {
			$strEventParameters = $this->m_objRollbackContractTerminationEvent->getEventParameters();
		} elseif( true == valObj( $objRollbackContractTerminationEvent, 'CContractTerminationEvent' ) ) {
			$strEventParameters = $objRollbackContractTerminationEvent->getEventParameters();
		}

		if( true == valStr( $strEventParameters ) ) {
			$arrmixOldPropertyPreferenceSetting = json_decode( $strEventParameters, true );

			if( false == valId( $arrmixOldPropertyPreferenceSetting['property_id'] ) ) return false;

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( [ $arrmixOldPropertyPreferenceSetting['property_id'] ], $objAdminDatabase );

			if( true == valArr( $arrobjProperties ) && true == valObj( $arrobjProperties[$arrmixOldPropertyPreferenceSetting['property_id']], 'CProperty' ) ) {
				$intClientId = $arrobjProperties[$arrmixOldPropertyPreferenceSetting['property_id']]->getCid();
			}

			if( false == valId( $intClientId ) ) return false;

			if( true == isset( $arrmixOldPropertyPreferenceSetting['LEAD_CHAT_ROUTING_CONDITION']['value'] ) ) {

				$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );
				if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
					return false;
				}

				$objClientDatabase->open();
				$objPropertyPreference = CPropertyPreferences::createService()->fetchPropertyPreferenceByCidByPropertyIdByKey( $intClientId, $arrmixOldPropertyPreferenceSetting['property_id'], 'LEAD_CHAT_ROUTING_CONDITION', $objClientDatabase );

				if( true == valObj( $objPropertyPreference, 'CPropertyPreference' ) ) {

					$objPropertyPreference->setValue( $arrmixOldPropertyPreferenceSetting['LEAD_CHAT_ROUTING_CONDITION']['value'] );

					if( false == $objPropertyPreference->update( $intUserId, $objClientDatabase ) ) {
						$objClientDatabase->rollback();
					} else {

						// Keeping log for Resident Pay.
						$objPropertySettingKey = new CPropertySettingKey();
						$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyPreference->getValue(), \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $objPropertyPreference->getValue() ) ), $arrmixOldPropertyPreferenceSetting['property_id'], $intClientId, CPropertySettingKey::LEAD_CHAT_ROUTING_CONDITION );

						if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
							$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
						}

						$objClientDatabase->commit();
					}
				}
				$objClientDatabase->close();
			}

			if( true == valArr( $arrmixOldPropertyPreferenceSetting[$arrmixOldPropertyPreferenceSetting['property_id']] ) ) {

				$objVoipDatabase->open();
				$objPropertyCallSetting = CPropertyCallSettings::createService()->fetchPropertyCallSettingByCidByPropertyId( $intClientId, $arrmixOldPropertyPreferenceSetting['property_id'], $objVoipDatabase );

				if( true == valObj( $objPropertyCallSetting, 'CPropertyCallSetting' ) ) {

					$objPropertyCallSetting->setLeadRoutingCondition( $arrmixOldPropertyPreferenceSetting[$arrmixOldPropertyPreferenceSetting['property_id']]['lead_routing_condition'] );
					$objPropertyCallSetting->setMaintenanceRoutingCondition( $arrmixOldPropertyPreferenceSetting[$arrmixOldPropertyPreferenceSetting['property_id']]['maintenance_routing_condition'] );
					$objPropertyCallSetting->setMaintenanceEmergencyRoutingCondition( $arrmixOldPropertyPreferenceSetting[$arrmixOldPropertyPreferenceSetting['property_id']]['maintenance_emergency_routing_condition'] );

					if( false == $objPropertyCallSetting->update( $intUserId, $objVoipDatabase ) ) {
						$objVoipDatabase->rollback();
					} else {

						$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );
						$objClientDatabase->open();
						if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
							return false;
						}

						// Keeping log for PROPERTY_CALL_SETTINGS_LEAD_ROUTING_CONDITION
						$objPropertySettingKey = new CPropertySettingKey();
						$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyCallSetting->getLeadRoutingCondition(), \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $objPropertyCallSetting->getLeadRoutingCondition() ) ), $arrmixOldPropertyPreferenceSetting['property_id'], $intClientId, CPropertySettingKey::PROPERTY_CALL_SETTINGS_LEAD_ROUTING_CONDITION );

						if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
							$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
						}

						// Keeping log for PROPERTY_CALL_SETTINGS_MAINTENANCE_ROUTING_CONDITION
						$objPropertySettingKey = new CPropertySettingKey();
						$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyCallSetting->getMaintenanceRoutingCondition(), \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $objPropertyCallSetting->getMaintenanceRoutingCondition() ) ), $arrmixOldPropertyPreferenceSetting['property_id'], $intClientId, CPropertySettingKey::PROPERTY_CALL_SETTINGS_MAINTENANCE_ROUTING_CONDITION );

						if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
							$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
						}

						// Keeping log for PROPERTY_CALL_SETTINGS_MAINTENANCE_EMERGENCY_ROUTING_CONDITION
						$objPropertySettingKey = new CPropertySettingKey();
						$objPropertySettingLog = $objPropertySettingKey->createPropertySettingLog( 'UPDATE', $objPropertyCallSetting->getMaintenanceEmergencyRoutingCondition(), \Psi\CStringService::singleton()->ucwords( str_replace( '_', ' ', $objPropertyCallSetting->getMaintenanceEmergencyRoutingCondition() ) ), $arrmixOldPropertyPreferenceSetting['property_id'], $intClientId, CPropertySettingKey::PROPERTY_CALL_SETTINGS_MAINTENANCE_EMERGENCY_ROUTING_CONDITION );

						if( true == valObj( $objPropertySettingLog, 'CPropertySettingLog' ) ) {
							$objPropertySettingLog->insert( $intUserId, $objClientDatabase );
						}

						$objVoipDatabase->commit();
						$objClientDatabase->commit();
					}
				}
				$objClientDatabase->close();
				$objVoipDatabase->commit();
				$objVoipDatabase->close();
			}
		}
		return true;
	}

	public function sendTerminationDenyEmail( $intUserId, $objAdminDatabase, $objEmailDatabase ) {

		require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';

		$arrmixTemplateParameters = [];

		$objClient = CClients::createService()->fetchClientById( $this->getCid(), $objAdminDatabase );

		if( !valObj( $objClient, 'CClient' ) ) {
			return false;
		}

		$arrmixTemplateParameters['CONFIG_COMPANY_BASE_DOMAIN']		= CONFIG_COMPANY_BASE_DOMAIN;
		$arrmixTemplateParameters['logo_url']						= CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG;
		$arrmixTemplateParameters['company_name']					= $objClient->getCompanyName();
		$arrmixTemplateParameters['reason_for_deny']				= $this->getReasonForDeny();

		$objRenderTemplate		= new CRenderTemplate( 'sales/contract_terminations/termination_request_deny_email.tpl', PATH_INTERFACES_CLIENT_ADMIN, false, $arrmixTemplateParameters );
		$strHtmlContent			= $objRenderTemplate->nestedFetchTemplate( PATH_INTERFACES_CLIENT_ADMIN . 'sales/contract_terminations/termination_request_deny_email.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::COMPANY_EMPLOYEE, __( 'Termination Request Denied' ), $strHtmlContent, CSystemEmail::TERMINATIONS_EMAIL_ADDRESS, 0, CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

		$arrstrCcEmailAddresses = CPsLeadDetails::createService()->fetchTerminationDenyEmailRecipientsByPsLeadId( $objClient->getPsLeadId(), $objAdminDatabase );

		if( valArr( $arrstrCcEmailAddresses ) ) {
			$objSystemEmail->setCcEmailAddress( implode( ', ', $arrstrCcEmailAddresses ) );
		}

		if( $objSystemEmail->validate( VALIDATE_INSERT ) && $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
			return true;
		}

		return false;
	}

	public function sendFailedTerminationEventNotification( $intUserId, $strSendToEmailAddress, $objAdminDatabase, $objEmailDatabase ) {

		$arrobjContractTerminationEventTypes = CContractTerminationEventTypes::createService()->fetchContractTerminationEventTypeDetailsByIds( $this->m_arrintFailedContractTerminationEventTypeIds, $objAdminDatabase );

		$objClient = \Psi\Eos\Admin\CClients::createService()->fetchClientById( $this->getCid(), $objAdminDatabase );

		if( true == valArr( $arrobjContractTerminationEventTypes ) ) {

			require_once PATH_PHP_INTERFACES . 'Interfaces.defines.php';

			$arrmixTemplateParameters = [];

			$arrmixTemplateParameters['CONFIG_COMPANY_BASE_DOMAIN']       = CONFIG_COMPANY_BASE_DOMAIN;
			$arrmixTemplateParameters['logo_url']                         = CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . CONFIG_EMAIL_PSI_LOGO_PNG;
			$arrmixTemplateParameters['failed_property_event_details']    = $this->m_arrstrFailedPropertyEventNames;
			$arrmixTemplateParameters['contract_termination_event_types'] = $arrobjContractTerminationEventTypes;
			$arrmixTemplateParameters['client_name']						= $objClient->getCompanyName();
			$arrmixTemplateParameters['ps_lead_id']							= $objClient->getPsLeadId();
			$arrmixTemplateData['clientadmin_url']							= CONFIG_SECURE_HOST_PREFIX . 'clientadmin.' . CONFIG_COMPANY_BASE_DOMAIN;

			$objRenderTemplate     = new CRenderTemplate( 'sales/contracts/failed_contract_termination_event_notification.tpl', PATH_INTERFACES_CLIENT_ADMIN, false, $arrmixTemplateParameters );
			$strHtmlContent        = $objRenderTemplate->nestedFetchTemplate( PATH_INTERFACES_CLIENT_ADMIN . 'sales/contracts/failed_contract_termination_event_notification.tpl', PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );
			$objSystemEmailLibrary = new CSystemEmailLibrary();
			$objSystemEmail        = $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, 'Termination Automation : Failed Contract Termination Settings Notification.', $strHtmlContent, $strSendToEmailAddress, 0, CSystemEmail::PROPERTYSOLUTIONS_SYSTEM_EMAIL_ADDRESS );

			if( true == $objSystemEmail->validate( VALIDATE_INSERT ) && true == $objSystemEmail->insert( $intUserId, $objEmailDatabase ) ) {
				return true;
			}
		}

		return false;
	}

	public function activateDisabledProperties( $intUserId, $objAdminDatabase, $objConnectDatabase ) {

		if( true == valArr( $this->m_arrintDisabledPropertyIds ) ) {

			$arrobjProperties = CProperties::createService()->fetchCustomPropertiesByIds( $this->m_arrintDisabledPropertyIds, $objAdminDatabase, false );

			if( true == valArr( $arrobjProperties ) ) {

				foreach( $arrobjProperties AS $objProperty ) {

					if( true == valObj( $objProperty, 'CProperty' ) ) {

						$intClientId = $objProperty->getCid();

						if( true == valId( $intClientId ) ) {

							$objClientDatabase = CDatabases::createService()->fetchDatabaseByCid( $intClientId, $objConnectDatabase );

							if( false == valObj( $objClientDatabase, 'CDatabase' ) ) {
								return false;
							}

							$objClientDatabase->open();

							$objAssetLocation = CAssetLocations::createService()->fetchAssetLocationByPropertyIdByCid( $objProperty->getId(), $intClientId, $objClientDatabase );

							$objProperty->setIsDisabled( 0 );
							$objProperty->setDisabledOn( NULL );

							if( false == $objProperty->update( $intUserId, $objClientDatabase, $objAdminDatabase ) ) {
								$objClientDatabase->rollback();
								$objAdminDatabase->rollback();
								return false;
							}

							// disable asset_locations
							if( true == valObj( $objAssetLocation, 'CAssetLocation' ) ) {

								$objAssetLocation->setDeletedOn( NULL );
								$objAssetLocation->setDeletedBy( NULL );

								if( false == $objAssetLocation->update( $intUserId, $objClientDatabase ) ) {
									$objClientDatabase->rollback();
									$objAdminDatabase->rollback();
									return false;
								}
							}

							$objClientDatabase->commit();
							$objAdminDatabase->commit();

							$objClientDatabase->close();
						}
					}
				}
			}
		}
		return true;
	}

	private static function createLocalConnectDatabases() {

		$objConnectDatabase = CDatabases::createService()->createConnectDatabase( false );
		$arrobjConnectDatabases = CDatabases::createService()->loadAlternateConnectDatabases( $objConnectDatabase );
		$arrobjConnectDatabases[$objConnectDatabase->getIpAddress()] = $objConnectDatabase;

		return $arrobjConnectDatabases;
	}

	private static function performDatabaseOperation( $arrobjConnectDatabases, $objClientDatabase, $objDnsDatabase, $strAction = 'begin' ) {

		if( false == valArr( $arrobjConnectDatabases ) || false == valObj( $objClientDatabase, 'CDatabase' ) ) {
			return;
		}

		if( 'begin' == $strAction ) {
			array_walk( $arrobjConnectDatabases, function( $objConnectDatabase ) {
				$objConnectDatabase->begin();
			} );
			$objClientDatabase->open();
			$objClientDatabase->begin();

			if( true == valObj( $objDnsDatabase, 'CDatabase' ) ) {
				$objDnsDatabase->begin();
			}
		}

		if( 'commit' == $strAction ) {
			array_walk( $arrobjConnectDatabases, function( $objConnectDatabase ) {
				$objConnectDatabase->commit();
				$objConnectDatabase->close();
			} );
			$objClientDatabase->commit();
			$objClientDatabase->close();

			if( true == valObj( $objDnsDatabase, 'CDatabase' ) ) {
				$objDnsDatabase->commit();
				$objDnsDatabase->close();
			}
		}

		if( 'rollback' == $strAction ) {
			array_walk( $arrobjConnectDatabases, function( $objConnectDatabase ) {
				$objConnectDatabase->rollback();
				$objConnectDatabase->close();
			} );
			$objClientDatabase->rollback();
			$objClientDatabase->close();

			if( true == valObj( $objDnsDatabase, 'CDatabase' ) ) {
				$objDnsDatabase->rollback();
				$objDnsDatabase->close();
			}
		}
	}

}
?>