<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPolicies
 * Do not add any new functions to this class.
 */

class CPolicies extends CBasePolicies {

	public static function fetchPolicyDesignations( $objDatabase ) {

		$strSql = 'SELECT 
						pm.id,
						array_to_string( array( SELECT d.name FROM
																policies p
																JOIN employee_policy_associations epa ON epa.policy_id = p.id
																JOIN designations d ON d.id = ANY( epa.designation_ids )
															   WHERE p.id IN ( pm.id )
										), \',\' ) as designation_name from policies pm;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllStates( $objDatabase ) {

		$strSql = 'SELECT
						array_to_string( array( SELECT s.name FROM
																states s WHERE s.country_code = \'' . CCountry::CODE_USA . '\'
										), \',\' ) as state_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPolicyStates( $objDatabase ) {

		$strSql = 'SELECT 
						pm.id,
						array_to_string( array( SELECT s.name FROM
																policies p
																JOIN employee_policy_associations epa ON epa.policy_id = p.id
																JOIN states s ON s.code = ANY( epa.state_codes )
															   WHERE p.id IN ( pm.id )
										), \',\' ) as state_name from policies pm;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPoliciesByIds( $arrintPoliciesIds, $objDatabase ) {

		if( false == valArr( $arrintPoliciesIds ) ) return NULL;
		$strSql = 'SELECT
					p.*
				FROM
					policies p
				WHERE  
					p.id IN( ' . implode( ',', $arrintPoliciesIds ) . ' )';

		return self::fetchPolicies( $strSql, $objDatabase );

	}

	public static function fetchAllPoliciesByCountryCode( $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT
					p.id,
					p.name
				FROM
					policies p
				WHERE 
					p.country_code = \'' . $strCountryCode . '\'
					AND p.deleted_by IS NULL';

		return self::fetchPolicies( $strSql, $objDatabase );

	}

	public static function fetchPoliciesCountByName( $strPolicyName, $strCountryCode, $objDatabase ) {

		if( false == valStr( $strPolicyName ) ) return NULL;

		$strSql = 'SELECT
						count( p.id )
					FROM
						policies p
					WHERE
						LOWER( p.name ) = E\'' . \Psi\CStringService::singleton()->strtolower( addslashes( $strPolicyName ) ) . '\'
						AND p.country_code = \'' . $strCountryCode . '\'
						AND p.deleted_on IS NULL
						AND p.deleted_by IS NULL';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) {
			return $arrintResponse[0]['count'];
		}

		return 0;
	}

	public static function fetchPolicyDesignationsByPolicyId( $intPolicyId, $objDatabase ) {

		$strSql = 'SELECT
						d.id,
						d.name
					FROM
						policies p
						JOIN employee_policy_associations epa ON epa.policy_id = p.id
						JOIN designations d ON d.id = ANY( epa.designation_ids )
					WHERE
						p.id = ' . ( int ) $intPolicyId . '
						AND p.deleted_on IS NULL
						AND p.deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPolicyStatesByPolicyId( $intPolicyId, $objDatabase ) {

		$strSql = 'SELECT
						s.code,
						s.name
					FROM
						policies p
						JOIN employee_policy_associations epa ON epa.policy_id = p.id
						JOIN states s ON s.code = ANY( epa.state_codes )
					WHERE
						p.id = ' . ( int ) $intPolicyId . '
						AND p.deleted_on IS NULL
						AND p.deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPaginatedPoliciesByFilter( $arrmixPolicyFilter, $intPageNo, $intPageSize, $strOrderByType = NULL, $strOrderByField = NULL, $boolIsDownloadRequest = false, $objDatabase ) {

		if( false == is_numeric( $intPageNo ) && false == is_numeric( $intPageSize ) ) {
			return NULL;
		}

		$strWhereCondition = '';

		if( ( true == array_key_exists( 'from_date', $arrmixPolicyFilter ) && true == valStr( $arrmixPolicyFilter['from_date'] ) )
			&& ( true == array_key_exists( 'to_date', $arrmixPolicyFilter ) && true == valStr( $arrmixPolicyFilter['to_date'] ) ) ) {
			$strWhereCondition .= ' AND p.created_on BETWEEN \'' . $arrmixPolicyFilter['from_date'] . ' 00:00:00\' AND \'' . $arrmixPolicyFilter['to_date'] . ' 23:59:59\'';
		}

		if( true == array_key_exists( 'policies_added_by', $arrmixPolicyFilter ) && true == valArr( $arrmixPolicyFilter['policies_added_by'] ) ) {
			$arrmixPolicyFilter['policies_added_by'] = str_replace( ' ', '|', $arrmixPolicyFilter['policies_added_by'] );
			$strWhereCondition .= ' AND p.created_by IN( ' . implode( ',', $arrmixPolicyFilter['policies_added_by'] ) . ' )';
		}

		if( true == array_key_exists( 'is_published', $arrmixPolicyFilter ) && true == valStr( $arrmixPolicyFilter['is_published'] ) ) {
			if( 1 == $arrmixPolicyFilter['is_published'] ) {
				$strWhereCondition .= ' AND p.is_published = true ';
			} else {
				$strWhereCondition .= ' AND p.is_published = false ';
			}
		}

		$intOffset		= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit 		= ( int ) $intPageSize;

		$strOrderClause = ' ORDER BY  ' . $strOrderByField . ' ' . $strOrderByType;

		$strSql = 'SELECT
					p.id,
					p.ps_document_id,
					p.name as policy_name,
					p.updated_on,
					p.created_on,
					( CASE WHEN TRUE = p.is_published THEN 1 ELSE 0 END ) AS is_published
				FROM
					policies p
				WHERE 
					p.deleted_by IS NULL
					AND p.country_code = \'' . $arrmixPolicyFilter['login_user_countrycode'] . '\'
					' . $strWhereCondition . '
					' . $strOrderClause;

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) && false == $boolIsDownloadRequest ) {
			$strSql .= ' OFFSET	' . ( int ) $intOffset . ' LIMIT	' . ( int ) $intLimit;
		}

		$arrmixPolicies['data'] = fetchData( $strSql, $objDatabase );

		if( false == $boolIsDownloadRequest ) {
			$strCountSql = 'SELECT
								count( p.id )
							FROM
								policies p
							WHERE 
								p.deleted_by IS NULL
								AND p.country_code = \'' . $arrmixPolicyFilter['login_user_countrycode'] . '\'
								' . $strWhereCondition;

			$arrintResponse = fetchData( $strCountSql, $objDatabase );
			if( true == isset( $arrintResponse[0]['count'] ) ) $arrmixPolicies['count'] = $arrintResponse[0]['count'];

		}

		return $arrmixPolicies;
	}

}
?>