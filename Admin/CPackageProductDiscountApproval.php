<?php

class CPackageProductDiscountApproval extends CBasePackageProductDiscountApproval {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPackageProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBundlePsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApproverEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSetupDiscountPercentage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRecurringDiscountPercentage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReasonForDiscount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReasonForDeny() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeniedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeniedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function deleteByPackageProductIds( $arrintPackageProductIds, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDatabase( $objDatabase );

		if( !valArr( $arrintPackageProductIds ) ) {
			return NULL;
		}

		$strSql = 'DELETE FROM ' . self::TABLE_NAME . ' WHERE package_product_id IN ( ' . sqlIntImplode( $arrintPackageProductIds ) . ' );';

		if( true == $boolReturnSqlOnly ) {
			return $strSql;
		} else {
			return $this->executeSql( $strSql, $this, $objDatabase );
		}
	}

}
?>