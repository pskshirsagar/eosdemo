<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyAddresses
 * Do not add any new functions to this class.
 */

class CCompanyAddresses extends CBaseCompanyAddresses {

	/**
	 * Get Functions
	 */

	public static function fetchPrimaryCompanyAddressByCid( $intCid, $objAdminDatabase ) {

		$strSql = 'SELECT
						ca.*, c.company_name
					FROM
						company_addresses ca INNER JOIN clients c ON ( ca.cid = c.id )
					WHERE
						cid = ' . ( int ) $intCid . '
						AND address_type_id = ' . ( int ) CAddressType::PRIMARY;

		return self::fetchCompanyAddress( $strSql, $objAdminDatabase );
	}

	public static function fetchCompanyAddressByCidByPhoneNumberTypeId( $intCid, $intAddressTypeId, $objDatabase ) {

		$strSql = 'SELECT
						ca.*
					FROM
						company_addresses ca
					WHERE
						cid = ' . ( int ) $intCid . '
						AND address_type_id = ' . ( int ) $intAddressTypeId . '
					LIMIT 1';

		return self::fetchCompanyAddress( $strSql, $objDatabase );
	}

}
?>