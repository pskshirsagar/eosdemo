<?php

class CUserCommitCheckType extends CBaseUserCommitCheckType {

	const CODE_SNIFFER 				= 1;
	const SECURITY 					= 2;
	const MANAGE_COMPANY_PATTERNS 	= 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>