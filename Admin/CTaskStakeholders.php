<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskStakeholders
 * Do not add any new functions to this class.
 */

class CTaskStakeholders extends CBaseTaskStakeholders {

	public static function validateDuplicateRecords( $intEmployeeId, $intTaskTypeId, $intTaskPriorityId, $intPsProductId, $intPsProductOptionId, $intGroupId, $strEmail, $objDatabase ) {

		if( true == isset( $intTaskTypeId ) ) {
			$strWhere = ' task_type_id = ' . ( int ) $intTaskTypeId . ' AND ';
		} else {
			$strWhere = ' task_type_id IS NULL ' . ' AND ';
		}

		if( true == isset( $intTaskPriorityId ) ) {
			$strWhere .= ' task_priority_id = ' . ( int ) $intTaskPriorityId . ' AND ';
		} else {
			$strWhere .= ' task_priority_id IS NULL ' . ' AND ';
		}

		if( true == isset( $intPsProductId ) ) {
			$strWhere .= ' ps_product_id = ' . ( int ) $intPsProductId . ' AND ';
		} else {
			$strWhere .= ' ps_product_id IS NULL ' . ' AND ';
		}

		if( true == isset( $intPsProductOptionId ) ) {
			$strWhere .= ' ps_product_option_id = ' . ( int ) $intPsProductOptionId;
		} else {
			$strWhere .= ' ps_product_option_id IS NULL ';
		}

        if( true == isset( $strEmail ) ) {
            $strSql = ' SELECT
						*
						FROM
							task_stakeholders
						WHERE lower( email ) = ' . strtolower( ( string ) $strEmail ) . ' AND ' . $strWhere;

            return parent::fetchTaskStakeholder( $strSql, $objDatabase );
        }

		if( true == isset( $intGroupId ) ) {
			$strSql = ' SELECT
						*
						FROM
							task_stakeholders
						WHERE group_id = ' . ( int ) $intGroupId . ' AND ' . $strWhere;

			return parent::fetchTaskStakeholder( $strSql, $objDatabase );
		}

		$strSql = ' SELECT
						*
					FROM
						task_stakeholders
					WHERE employee_id = ' . ( int ) $intEmployeeId . ' AND ' . $strWhere;

		return parent::fetchTaskStakeholder( $strSql, $objDatabase );

	}

	public static function fetchTaskStakeholdersByTaskTypeIdByTaskPriorityId( $intTaskTypeId, $intTaskPriorityId, $intPsProductId, $intPsProductOptionId, $objDatabase ) {

		$strSql = '	SELECT
						u1.id,
						ug.user_id,
						ts.email
					FROM
						task_stakeholders ts
						LEFT JOIN user_groups ug ON ( ug.group_id = ts.group_id AND ug.deleted_by IS NULL )
						LEFT JOIN users u1 ON ( ts.employee_id = u1.employee_id )
						LEFT JOIN users u2 ON ( ug.user_id = u2.id )
						LEFT JOIN employees e ON ( u2.employee_id = e.id OR u1.employee_id = e.id )
						LEFT JOIN groups g ON ( g.id = ts.group_id )
					WHERE
						( e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::NO_SHOW . ' ) OR e.employee_status_type_id is NULL)AND
						( ts.task_priority_id = ' . ( int ) $intTaskPriorityId . ' OR ts.task_priority_id IS NULL ) AND ' .
						'( ts.ps_product_option_id = ' . ( int ) $intPsProductOptionId . ' OR ts.ps_product_option_id IS NULL ) AND ' .
						'( ts.task_type_id = ' . ( int ) $intTaskTypeId . ' OR ts.task_type_id IS NULL ) AND ( ts.ps_product_id = ' . ( int ) $intPsProductId . ' OR ts.ps_product_id IS NULL )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPaginatedTaskStakeholdersCount( $intTaskTypeId, $intTaskPriorityId, $intPsProductId, $intPsProductOptionId, $intEmployeeId, $intGroupId, $intPageNo, $intPageSize, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSubSql = NULL;

		if( true == is_numeric( $intTaskTypeId ) ) {
			$strSubSql = ' ts.task_type_id = ' . ( int ) $intTaskTypeId;
		}

		if( true == is_numeric( $intTaskPriorityId ) ) {
			$strSubSql .= ' AND ts.task_priority_id = ' . ( int ) $intTaskPriorityId;
		}

		if( true == is_numeric( $intPsProductId ) ) {
			$strSubSql .= ' AND ts.ps_product_id = ' . ( int ) $intPsProductId;
		}

		if( true == is_numeric( $intPsProductOptionId ) ) {
			$strSubSql .= ' AND ts.ps_product_option_id = ' . ( int ) $intPsProductOptionId;
		}

		if( true == is_numeric( $intEmployeeId ) ) {
			$strSubSql .= ' AND ts.employee_id = ' . ( int ) $intEmployeeId;
		}

		if( true == is_numeric( $intGroupId ) ) {
			$strSubSql .= ' AND ts.group_id = ' . ( int ) $intGroupId;
		}

		$strSql = 'SELECT count(id) FROM task_stakeholders ts';

		if( NULL != $strSubSql ) {
			$strSql .= ' WHERE ' . ltrim( $strSubSql, ' AND ' );
		}

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return ( true == isset( $arrintResponse[0]['count'] ) ) ?  $arrintResponse[0]['count'] : 0;

	}

	public static function fetchPaginatedTaskStakeholders( $intTaskTypeId, $intTaskPriorityId, $intPsProductId, $intPsProductOptionId, $intEmployeeId, $intGroupId, $intPageNo, $intPageSize, $strOrderByField = NULL, $strOrderByType = NULL, $objDatabase ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSubSql = NULL;

		if( true == is_numeric( $intTaskTypeId ) ) {
			$strSubSql = ' ts.task_type_id = ' . ( int ) $intTaskTypeId;
		}

		if( true == is_numeric( $intTaskPriorityId ) ) {
			$strSubSql .= ' AND ts.task_priority_id = ' . ( int ) $intTaskPriorityId;
		}

		if( true == is_numeric( $intPsProductId ) ) {
			$strSubSql .= ' AND ts.ps_product_id = ' . ( int ) $intPsProductId;
		}

		if( true == is_numeric( $intPsProductOptionId ) ) {
			$strSubSql .= ' AND ts.ps_product_option_id = ' . ( int ) $intPsProductOptionId;
		}

		if( true == is_numeric( $intEmployeeId ) ) {
			$strSubSql .= ' AND ts.employee_id = ' . ( int ) $intEmployeeId;
		}

		if( true == is_numeric( $intGroupId ) ) {
			$strSubSql .= ' AND ts.group_id = ' . ( int ) $intGroupId;
		}

		$strSql = ' SELECT
						ts.id AS id,
						ts.employee_id AS employee_id,
						ts.group_id AS group_id,
						tt.name AS task_type,
						tp.name AS task_priority,
						pp.name AS product,
						ppo.name AS product_option,
						e.name_full AS employee,
						g.name AS group,
						ts.email AS email
					FROM task_stakeholders ts
						LEFT JOIN task_types tt ON ( ts.task_type_id = tt.id )
						LEFT JOIN task_priorities tp ON ( ts.task_priority_id = tp.id )
						LEFT JOIN ps_products pp ON ( ts.ps_product_id = pp.id )
						LEFT JOIN ps_product_options ppo ON ( ts.ps_product_option_id = ppo.id )
						LEFT JOIN groups g ON ( ts.group_id = g.id )
						LEFT JOIN employees e ON ( ts.employee_id = e.id ) ';

		if( NULL != $strSubSql )
			$strSql .= 'WHERE ' . ltrim( $strSubSql, ' AND ' );

		$strSql .= ' GROUP BY
						ts.id,
						tt.name,
						tp.name,
						pp.name,
						ppo.name,
						e.name_full,
						g.name
					ORDER BY
						' . $strOrderByField . ' ' . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

}
?>