<?php

class CReportSqlDatabase extends CBaseReportSqlDatabase {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportSqlDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDatabaseId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>