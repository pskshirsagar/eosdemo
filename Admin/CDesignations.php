<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDesignations
 * Do not add any new functions to this class.
 */

class CDesignations extends CBaseDesignations {

	public static function fetchPaginatedDesignationsByFilter( $objDesignationsFilter, $objDatabase, $strCountryCode = NULL ) {

		$intOffset 		= ( 0 < $objDesignationsFilter->getPageNo() ) ? $objDesignationsFilter->getPageSize() * ( $objDesignationsFilter->getPageNo() - 1 ) : 0;
		$intLimit  		= ( int ) $objDesignationsFilter->getPageSize();
		$strOrderClause = ' ORDER BY department_name, d.order_num ';
		$strOrder 		= '';

		if( false == is_null( $objDesignationsFilter ) && false == is_null( $objDesignationsFilter->getOrderByField() ) && false == is_null( $objDesignationsFilter->getOrderByType() ) ) {
			$strOrder 		= ( 1 == $objDesignationsFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';
			$strOrderClause = ' ORDER BY ' . $objDesignationsFilter->getOrderByField() . $strOrder . ', department_name ';

			if( 'department_name' == $objDesignationsFilter->getOrderByField() ) {
				$strOrderClause = ' ORDER BY ' . $objDesignationsFilter->getOrderByField() . '' . $strOrder;
			}
		}

		$strWhere 		= self::getSearchCriteria( $objDesignationsFilter, $objDatabase );
		$strCondition 	= $strWhere;

		if( CCountry::CODE_INDIA == ( $strCountryCode ) || CCountry::CODE_USA == ( $strCountryCode ) ) {
			$strCondition = $strWhere . ' AND d.country_code = \'' . $strCountryCode . '\'';

			if( '' == $strWhere ) {

				$strCondition = ' where d.country_code = \'' . $strCountryCode . '\'';
			}

		}

		$strSql = 'SELECT
						d.*,
						dep.name as department_name,
						emp.preferred_name AS employee_name,
						dl.name AS designation_level_name,
						( SELECT
								count( e.id )
							FROM
								employees e
							WHERE
								d.id = e.designation_id
							AND
								e.employee_status_type_id = 1 ) as employee_count,
						( SELECT
								count ( pwjp.id )
							FROM
								ps_website_job_postings pwjp
							WHERE
								d.id = pwjp.designation_id
							AND pwjp.deleted_by IS NULL
						) AS job_postings_count
					FROM
						designations d
						LEFT JOIN departments dep ON( dep.id = d.department_id )
						LEFT JOIN designation_levels dl ON( dl.id = d.designation_level_id )
						LEFT JOIN employees emp ON( emp.id = d.approver_employee_id ) ' . $strCondition . $strOrderClause . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit . '';

		return self::fetchDesignations( $strSql, $objDatabase );
	}

	public static function fetchDesignationsCountByFilter( $objDesignationsFilter, $objDatabase, $strCountryCode = NULL ) {

		$strWhere 					= self::getSearchCriteria( $objDesignationsFilter, $objDatabase );

		$arrintDesignationsCount 	= self::fetchRowCount( $strWhere, 'designations d', $objDatabase );

		if( CCountry::CODE_INDIA == ( $strCountryCode ) || CCountry::CODE_USA == ( $strCountryCode ) ) {
			$strCondition = $strWhere . ' AND d.country_code = \'' . $strCountryCode . '\'';

			if( '' == $strWhere ) {
				$strCondition = ' where d.country_code = \'' . $strCountryCode . '\'';
			}

			$arrintDesignationsCount = self::fetchRowCount( $strCondition, 'designations d', $objDatabase );

		}

		return ( true == isset( $arrintDesignationsCount ) ) ? ( int ) $arrintDesignationsCount : 0;

	}

	public static function fetchDesignationsByIds( $arrintDesignationIds, $objDatabase ) {
		if( false == valArr( $arrintDesignationIds ) ) return NULL;
		return self::fetchDesignations( sprintf( 'SELECT * FROM designations WHERE id IN ( %s )', implode( ',', $arrintDesignationIds ) ), $objDatabase );
	}

	public static function fetchDesignationsByDesignationIds( $arrintDesignationIds, $objDatabase ) {
		if( false == valArr( $arrintDesignationIds ) ) return NULL;
		return self::fetchDesignations( sprintf( 'SELECT * FROM designations WHERE designation_id IN ( %s )', implode( ',', $arrintDesignationIds ) ), $objDatabase );
	}

	public static function fetchAllDesignations( $objDatabase, $strCountryCode = NULL, $boolIsShowDeletedDesignation = false, $boolIsPublished = true ) {

		$strSql		= 'SELECT * FROM public.designations';
		$strWhere	= ' WHERE deleted_by IS NULL';

		if( true == $boolIsShowDeletedDesignation ) {
			$strWhere = ( true == $boolIsPublished ) ? ' WHERE is_published = 1' : '';
		} else {
			$strWhere .= ( false == $boolIsPublished ) ? '' : ' AND is_published = 1';
		}

		$strWhere .= ( false == is_null( $strCountryCode ) ) ? ' AND country_code = \'' . addslashes( $strCountryCode ) . '\' ' : '';

		$strSql .= $strWhere . ' ORDER BY name ASC';

		return self::fetchDesignations( $strSql, $objDatabase );
	}

	public static function fetchRemovedDesignationByNameAndCountry( $strName, $strCountry, $objDatabase ) {

		$strSql = 'WHERE deleted_by IS NOT NULL AND name ILIKE \'' . trim( addslashes( $strName ) ) . '\' AND country_code ILIKE \'' . trim( addslashes( $strCountry ) ) . '\' ';

		$intCountDesignation = CDesignations::fetchRowCount( $strSql, 'designations', $objDatabase );

		return ( 0 < ( $intCountDesignation ) ) ? true : false;
	}

	public static function fetchDesignationsByNameByDescription( $arrstrFilteredExplodedSearch, $objDatabase, $boolIsShowDisabledData ) {

		$strSubSql = ( false == $boolIsShowDisabledData ) ? ' AND d.deleted_on IS NULL ':'';

		$strSql = 'SELECT
						id,
						name,
						description,
						country_code,
						( SELECT
								count( e.id )
							FROM
								employees e
							WHERE d.id = e.designation_id
								AND e.employee_status_type_id = 1 ) as employee_count
					FROM designations d
					WHERE ( d.name ILIKE \'%' . implode( '%\' AND d.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' OR d.description ILIKE \'%' . implode( '%\' AND d.description ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\') ' . $strSubSql .
		          ' ORDER BY d.name
					LIMIT 10';

		return self::fetchDesignations( $strSql, $objDatabase );
	}

	public static function fetchAllDesignationsByDesignationId( $intActionResultId, $intDesignationId, $objDatabase ) {
		$strSql = 'SELECT ds.name,
									e.name_first,
									e.name_last,
									e.preferred_name,
									e.email_address,
									ea.current_title as current_title,
									d.name as department_name,
									count(ts.id) as training_sessions_count,
									a.action_result_id as action_result_id,
									t.name as tag_name
								FROM training_trainer_associations as tta
									LEFT JOIN actions as a ON a.id = tta.action_id
									LEFT JOIN employee_assessments as ea ON ( ea.id = tta.employee_assessment_id AND ea.deleted_by IS NULL )
									LEFT JOIN training_sessions ts ON ts.training_trainer_association_id = tta.id
									LEFT JOIN users as u ON a.secondary_reference = u.id
									LEFT JOIN employees as e ON u.employee_id = e.id
									LEFT JOIN departments as d ON e.department_id = d.id
									LEFT JOIN designations as ds ON e.designation_id = ds.id
									LEFT JOIN tags as t ON t.id = a.primary_reference
								WHERE ds.deleted_by IS NULL
									AND tta.is_published = 1
									AND a.action_result_id = ' . ( int ) $intActionResultId . '
									AND ds.id = ' . ( int ) $intDesignationId . '
									AND ts.deleted_on IS NULL';

		if( CActionResult::EXTERNAL_TRAINER != $intActionResultId ) {
			$strSql .= ' AND e.employee_status_type_id= ' . CEmployeeStatusType::CURRENT;
		}

		$strSql .= ' GROUP BY d.name,
									e.name_first,
									e.name_last,
									e.preferred_name,
									e.email_address,
									ea.current_title,
									ds.name,
									a.action_result_id,
									t.name
								ORDER BY e.name_first,
									e.name_last';
		return self::fetchDesignations( $strSql, $objDatabase );
	}

	public static function fetchApproverEmployeeIdByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) return NULL;

		$strSql = 'SELECT
						e.id as employee_id,
						ds.approver_employee_id,
						ds.comp_change_approver_id,
						ds.bonus_approver_employee_id
					FROM
						designations ds
						JOIN employees e ON ( ds.id = e.designation_id )
					WHERE
						e.id = ' . ( int ) $intEmployeeId . ' ';
		return fetchData( $strSql, $objDatabase );
	}

	public function getSearchCriteria( $objDesignationsFilter ) {

		$strWhereCondition	= '';

		if( false == is_null( $objDesignationsFilter->getCountryCodes() ) ) {
			if( true == valArr( $objDesignationsFilter->getCountryCodes() ) ) {
				$strCountryCodes	= implode( '\', \'', $objDesignationsFilter->getCountryCodes() );
			} else {
				$strCountryCodes	= $objDesignationsFilter->getCountryCodes();
			}
		}

		if( false == valObj( $objDesignationsFilter, 'CDesignationsFilter' ) ) return $strWhereCondition;

		$strWhereCondition .= ( false == $objDesignationsFilter->getShowDisabledData() ) ? ' d.deleted_by IS NULL ' : '';
		$strWhereCondition .= ( true == valStr( $objDesignationsFilter->getDepartmentIds() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' d.department_id  IN ( ' . $objDesignationsFilter->getDepartmentIds() . ' )' : '';
		$strWhereCondition .= ( true == valStr( $objDesignationsFilter->getDesignationIds() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' d.id IN ( ' . $objDesignationsFilter->getDesignationIds() . ' )' : '';
		$strWhereCondition .= ( true == valStr( $strCountryCodes ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' d.country_code IN ( \'' . $strCountryCodes . '\' )' : '';
		$strWhereCondition .= ( true == valStr( $objDesignationsFilter->getIspublished() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' d.is_published = ' . $objDesignationsFilter->getIspublished() : '';
		$strWhereCondition .= ( true == valStr( $objDesignationsFilter->getShowUnapprovedDesignations() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' d.approved_on IS NULL' : '';
		$strWhereCondition .= ( true == valStr( $objDesignationsFilter->getDesignationLevelIds() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' d.designation_level_id IN ( ' . $objDesignationsFilter->getDesignationLevelIds() . ' )' : '';
		return ( true == valStr( $strWhereCondition ) ) ? ' WHERE ' . $strWhereCondition : '';

	}

	public static function fetchAssignedDesignationsByTestId( $intTestId, $objDatabase ) {

		$strSql = 'SELECT
						d.*
					FROM
						designations d,
						test_groups et
					WHERE
						d.id = et.designation_id
						AND et.test_id::integer = ' . ( int ) $intTestId . '
					ORDER BY
						lower ( d.name )';

		return self::fetchDesignations( $strSql, $objDatabase );
	}

	public static function fetchUnAssignedDesignationsByTestId( $intTestId, $objDatabase ) {

		$strSql = 'SELECT
						d.*
					FROM
						designations d
					WHERE
						d.id NOT IN (
										SELECT
											DISTINCT ds.id
										FROM
											designations ds
										JOIN
											test_groups tg ON ( ds.id = tg.designation_id )
										WHERE
											tg.test_id = ' . ( int ) $intTestId .
		          ' )
					ORDER BY
						d.name';

		return self::fetchDesignations( $strSql, $objDatabase );
	}

	public static function fetchDesignationsByUserIds( $arrintUserIds, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						designations d
						LEFT JOIN employees e ON e.designation_id = d.id
						LEFT JOIN users u ON u.employee_id = e.id
					WHERE
						u.id IN ( ' . implode( ',', $arrintUserIds ) . ' )';

		return self::fetchDesignations( $strSql, $objDatabase );
	}

	public static function fetchDesignationByUserId( $intUserId, $objDatabase ) {

		if( false == valId( $intUserId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						d.*
					FROM
						designations AS d
						JOIN employees AS e ON e.designation_id = d.id
						JOIN users AS u ON u.employee_id = e.id
					WHERE
						u.id = ' . ( int ) $intUserId;

		return self::fetchDesignation( $strSql, $objDatabase );
	}

	public static function fetchDesignationsByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) return NULL;

		$strSql = ' SELECT
						d.id
					FROM
						designations d
					WHERE
						( d.comp_change_approver_id =' . $intEmployeeId . '
						OR d.replacement_approver_id =' . $intEmployeeId . ' )';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchAllCountryCodes( $objDatabase ) {
		$strSql = 'SELECT DISTINCT ( country_code ), c.name as country_name FROM designations d LEFT JOIN countries c ON ( d.country_code = c.code )';

		$arrmixCountries = fetchData( $strSql, $objDatabase );

		$arrstrCountryCodes = array();

		if( true == valArr( $arrmixCountries ) ) {
			foreach( $arrmixCountries as $strCode ) {
				$strCode['country_code'] = $strCode['country_code'];
				$strCode['country_name'] = $strCode['country_name'];
				$arrstrCountryCodes[] = $strCode;
			}
		}

		return $arrstrCountryCodes;
	}

	public static function fetchAllDesignationsByCountryCodeByDepartmentIds( $objDatabase, $strCountryCode = NULL, $arrintDepartmentIds = NULL, $boolIsShowDeletedDesignation = false, $boolIsPublished = true ) {

		$strSql		= 'SELECT id, name, country_code FROM public.designations';

		$strWhere	= ' WHERE deleted_by IS NULL';

		if( true == $boolIsShowDeletedDesignation ) {
			$strWhere = ( true == $boolIsPublished ) ? ' WHERE is_published = 1' : '';
		} else {
			$strWhere .= ( false == $boolIsPublished ) ? '' : ' AND is_published = 1';
		}

		$strWhere .= ( false == is_null( $strCountryCode ) ) ? ' AND country_code IN( \'' . ( $strCountryCode ) . '\' )' : '';
		$strWhere .= ( false == is_null( $arrintDepartmentIds ) ) ? ' AND department_id IN( ' . $arrintDepartmentIds . ' ) ' : '';

		$strSql .= $strWhere . ' ORDER BY name ASC';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDesignationsByPsWebsiteJobPostingId( $intPsWebsiteJobPostingId, $objDatabase, $strCountryCode = NULL ) {

		if( false == is_numeric( $intPsWebsiteJobPostingId ) ) return NULL;

		$strWhereCondition = ( true == valStr( $strCountryCode ) ) ? ' AND d.country_code = \'' . $strCountryCode . '\'' : '';

		$strSql = 'SELECT
						d.id,
						d.name,
						d.department_id
					FROM
						designations d
						LEFT JOIN departments dept ON ( dept.id = d.department_id )
						LEFT JOIN ps_website_job_postings pwjp ON ( pwjp.department_id = dept.id )
					WHERE
						pwjp.id = ' . ( int ) $intPsWebsiteJobPostingId . $strWhereCondition;

		return self::fetchDesignations( $strSql, $objDatabase );
	}

	public static function fetchAllDesignationsByCountryCodeByDepartmentId( $objDatabase, $strCountryCode = NULL, $intDepartmentId = NULL ) {

		$strWhere	= ' WHERE deleted_by IS NULL AND d.is_published = 1';

		$strWhere .= ( true == is_numeric( $intDepartmentId ) ) ? ' AND d.department_id = ' . ( int ) $intDepartmentId : '';

		if( false == is_null( $strCountryCode ) ) {
            $strWhere .= ( true == valArr($strCountryCode) ) ? ' AND d.country_code IN (\'' . join("','",$strCountryCode) . '\')' : ' AND d.country_code = \'' . $strCountryCode . '\'';
        }

		$strSql		= 'SELECT
						d.id AS designation_id,
						CASE
						  WHEN dep.id IS NULL THEN \'No Department\'
						  ELSE dep.name
						END AS department_name,
						d.name AS name,
						dep.id as department_id
					FROM
						designations d
						LEFT JOIN departments AS dep ON ( dep.id = d.department_id ) ' . $strWhere . '
					GROUP BY
						dep.id,
						d.name,
						d.id
					ORDER BY
						dep.name,
						d.name';

		return self::fetchDesignations( $strSql, $objDatabase );
	}

	public static function fetchDesignationsByCountryCodeByDepartmentId( $objDatabase, $strCountryCode = NULL, $intDepartmentId = NULL ) {

		$strWhere	= ' WHERE deleted_by IS NULL AND d.is_published = 1';

		$strWhere .= ( true == is_numeric( $intDepartmentId ) ) ? ' AND d.department_id = ' . ( int ) $intDepartmentId : '';
		$strWhere .= ( false == is_null( $strCountryCode ) ) ? ' AND d.country_code = \'' . $strCountryCode . '\'' : '';

		$strSql		= 'SELECT
						d.*,
						CASE
						  WHEN dep.id IS NULL THEN \'No Department\'
						  ELSE dep.name
						END AS department_name,
						d.name AS name,
						dep.id as department_id
					FROM
						designations d
						LEFT JOIN departments AS dep ON ( dep.id = d.department_id ) ' . $strWhere . '
					GROUP BY
						dep.id,
						d.name,
						d.id
					ORDER BY
						dep.name,
						d.name';

		return self::fetchDesignations( $strSql, $objDatabase );
	}

	public static function fetchAllDesignationsDetails( $objDatabase ) {

		$strSql = 'SELECT
						id, name
					FROM
						designations
					WHERE
						deleted_by IS NULL
					ORDER BY
						name';

		return self::fetchDesignations( $strSql, $objDatabase );
	}

	public static function fetchDesignationByNameAndCountry( $strName, $strCountry, $objDatabase ) {
		$strSql = 'SELECT * FROM designations WHERE name ILIKE \'' . trim( addslashes( $strName ) ) . '\' AND country_code ILIKE \'' . trim( addslashes( $strCountry ) ) . '\' ';
		return self::fetchDesignation( $strSql, $objDatabase );
	}

}
?>