<?php

class CTransactionReversalType extends CBaseTransactionReversalType {

	const BILLING_ERROR 			= 1;
	const DELAYED_IMPLEMENTATION 	= 2;
	const SLA_INFRACTION			= 3;
	const BAD_DEBT					= 4;
	const NSF						= 5;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case self::VALIDATE_INSERT:
			case self::VALIDATE_UPDATE:
			case self::VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}
}
?>