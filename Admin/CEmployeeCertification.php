<?php

use Psi\Libraries\ExternalFileUpload\CFileUpload;

class CEmployeeCertification extends CBaseEmployeeCertification {
	const MAX_CERTIFICATION_SCORE = 100;

	protected $m_strEmployeeNameFirst;
	protected $m_strEmployeeNameLast;
	protected $m_strEmployeePreferredName;
	protected $m_strCertificateName;
	protected $m_strScores;
	protected $m_strCertificateIds;
	protected $m_strDepartmentName;
	protected $m_strDesignationName;

	protected $m_intIsExternal;
	protected $m_intTotalEmployeeCertificateCount;
    protected $m_objStorageGateway;

	/**
	 * Get Functions
	 */

	public function getEmployeeNameFirst() {
		return $this->m_strEmployeeNameFirst;
	}

	public function getEmployeeNameLast() {
		return $this->m_strEmployeeNameLast;
	}

	public function getEmployeePreferredName() {
		return $this->m_strEmployeePreferredName;
	}

	public function getCertificateName() {
		return $this->m_strCertificateName;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getDesignationName() {
		return $this->m_strDesignationName;
	}

	public function getCertificateIds() {
		return $this->m_strCertificateIds;
	}

	public function getScores() {
		return $this->m_strScores;
	}

	public function getTotalEmployeeCertificateCount() {
		return $this->m_intTotalEmployeeCertificateCount;
	}

	public function getIsExternal() {
		return $this->m_intIsExternal;
	}

    public function getObjectStorageGateway() {
        return $this->m_objStorageGateway;
    }

   /**
	* Set Functions
	*/

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

	  	if( true == isset( $arrmixValues['name_first'] ) ) 		$this->setEmployeeNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) 			$this->setEmployeeNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['preferred_name'] ) ) 	$this->setEmployeePreferredName( $arrmixValues['preferred_name'] );
		if( true == isset( $arrmixValues['certificate_name'] ) ) 	$this->setCertificateName( $arrmixValues['certificate_name'] );
		if( true == isset( $arrmixValues['designation_name'] ) )	$this->setDepartmentName( $arrmixValues['designation_name'] );
		if( true == isset( $arrmixValues['department_name'] ) ) 	$this->setDesignationName( $arrmixValues['department_name'] );
		if( true == isset( $arrmixValues['certificate_ids'] ) )	$this->setCertificateIds( $arrmixValues['certificate_ids'] );
		if( true == isset( $arrmixValues['scores'] ) ) 							$this->setScores( $arrmixValues['scores'] );
		if( true == isset( $arrmixValues['total_employee_certificate_count'] ) ) 	$this->setTotalEmployeeCertificateCount( $arrmixValues['total_employee_certificate_count'] );
		if( true == isset( $arrmixValues['is_external'] ) ) 						$this->setIsExternal( $arrmixValues['is_external'] );

		return;
	}

	public function setEmployeeNameFirst( $strEmployeeNameFirst ) {
		$this->m_strEmployeeNameFirst = $strEmployeeNameFirst;
	}

	public function setEmployeeNameLast( $strEmployeeNameLast ) {
		$this->m_strEmployeeNameLast = $strEmployeeNameLast;
	}

	 public function setEmployeePreferredName( $strEmployeePreferredName ) {
	 	$this->m_strEmployeePreferredName = $strEmployeePreferredName;
	 }

	public function setCertificateName( $strCertificateName ) {
		$this->m_strCertificateName = $strCertificateName;
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	public function setDesignationName( $strDesignationName ) {
		$this->m_strDesignationName = $strDesignationName;
	}

	public function setCertificateIds( $strCertificateIds ) {
		$this->m_strCertificateIds = $strCertificateIds;
	}

	public function setScores( $strScores ) {
		$this->m_strScores = $strScores;
	}

	public function setTotalEmployeeCertificateCount( $intTotalEmployeeCertificateCount ) {
		$this->m_intTotalEmployeeCertificateCount = $intTotalEmployeeCertificateCount;
	}

	public function setIsExternal( $intIsExternal ) {
		$this->m_intIsExternal = $intIsExternal;
	}

    public function setObjectStorageGateway( $objStorage ) {
        $this->m_objStorageGateway = $objStorage;
    }

	/**
	 * Validate Functions
	 */

	public function valEmployeeId() {
		$boolIsValid = 1;

		if( true == is_null( $this->getEmployeeId() ) || false == is_numeric( $this->getEmployeeId() ) ) {
			$boolIsValid = 0;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', ' Employee is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCertificationId() {
		$boolIsValid = 1;

		if( true == is_null( $this->getCertificationId() ) || false == is_numeric( $this->getCertificationId() ) ) {
			$boolIsValid = 0;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'certification_id', ' Certification is required.' ) );
		}
		return $boolIsValid;
	}

	public function valCertificationDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getCertificationDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'certification_id', ' Certification date is required.' ) );
		}

		if( true == $boolIsValid && false == is_numeric( strtotime( $this->getCertificationDatetime() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'certification_id', ' Please enter date in mm/dd/yyyy format.' ) );
		}

		return $boolIsValid;
	}

	public function valCertificationScore() {
		$boolIsValid = true;

		if( false == empty( $this->m_strScore ) && self::MAX_CERTIFICATION_SCORE < $this->m_strScore ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'certification_id', ' Please enter valid score.' ) );
		}

		return $boolIsValid;
	}

	public function valCertificateFile() {
		$boolIsValid = true;

		if( true == empty( $_FILES['employee_certificate']['name'] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_certificate', ' Please upload certificate.' ) );
		}

		$strExtension = pathinfo( $_FILES['employee_certificate']['name'], PATHINFO_EXTENSION );

		if( true == $boolIsValid && false == in_array( $strExtension, [ 'jpg', 'jpeg', 'png', 'pdf' ] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_certificate', ' Invalid file, please upload jpg/jpeg/png/pdf file.' ) );
		}

		return $boolIsValid;
	}

	public function valIsCertified() {
		$boolValid = true;
		return $boolValid;
	}

	public function validate( $strAction, $boolCertificateRequired = true ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valCertificationId();
				$boolIsValid &= $this->valCertificationDatetime();
				$boolIsValid &= $this->valCertificationScore();
				if( true == $boolCertificateRequired ) {
					$boolIsValid &= $this->valCertificateFile();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsCertificateRequired = false, $boolIsFromDashboard = false, $objEmailDatabase = NULL ) {

		$boolIsValid = true;

		$objDatabase->begin();

		if( true == $boolIsCertificateRequired ) {

			if( true == empty( $_FILES['employee_certificate']['name'] ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_certificate', ' Certificate is required.' ) );
				return false;
			}

			$strExtension		= pathinfo( $_FILES['employee_certificate']['name'], PATHINFO_EXTENSION );
			$objFileExtension	= \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $strExtension, $objDatabase );

			if( false == valObj( $objFileExtension, 'CFileExtension' ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_certificate', $strExtension . ' is not a supported extension.' ) );
				return false;
			}

			$objPsDocument = new CPsDocument();
			$objPsDocument->setPsDocumentTypeId( CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_CERTIFICATE );
			$objPsDocument->setFileExtensionId( $objFileExtension->getId() );
			$objPsDocument->setTitle( 'Employee Certificate' );
            $strFileName = \Psi\CStringService::singleton()->strstr( $_FILES['employee_certificate']['name'], '.', true ) . '_' . date( 'Ymdhis' ) . '.' . $strExtension;
			$objPsDocument->setFileName( CFileUpload::cleanFilename( $strFileName ) );
			$objPsDocument->setEmployeeId( $this->getEmployeeId() );

			if( false == $boolIsFromDashboard ) {
				$objPsDocument->setApprovedOn( 'now()' );
				$objPsDocument->setApprovedBy( $intCurrentUserId );
			}

            if( true == $objPsDocument->validate( 'validate_employee_document', $objDatabase ) && $this->uploadFile( $objPsDocument ) && $objPsDocument->insert( $intCurrentUserId, $objDatabase ) ) {
                $this->setPsDocumentId( $objPsDocument->getId() );
            } else {
                $boolIsValid &= false;
            }

		}

        $boolIsValid = $boolIsValid ? parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly ) : false;

		if( false == $boolIsValid ) {
			$objDatabase->rollback();
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Failed to add certificate.' ) );
			return false;
		}

		if( true == $boolIsFromDashboard ) {
			$objCertification	= \Psi\Eos\Admin\CCertifications::createService()->fetchCertificationById( $this->getCertificationId(), $objDatabase );
			$objEmployee		= \Psi\Eos\Admin\CEmployees::createService()->fetchEmployeeById( $this->getEmployeeId(), $objDatabase );

			if( true == valObj( $objCertification, 'CCertification' ) && true == valObj( $objEmployee, 'CEmployee' ) ) {
				$strHtmlContent	= 'Dear Training Team, <br/><br/>';
				$strHtmlContent .= $objEmployee->getNameFull() . ' has requested <b>' . $objCertification->getName() . '</b> certificate on their profile, request you to proceed with the further process. <br/><br/> Thanks,';

				if( true == valStr( $objEmployee->getEmailSignature() ) ) {
					$strHtmlContent .= '<br/>' . $objEmployee->getEmailSignature();
				} else {
					$strHtmlContent .= '<br/>' . $objEmployee->getNameFull();
				}

				$strSubject = $objEmployee->getNameFull() . ' Certificate Request.';

				$objSmarty = new CPsSmarty( PATH_INTERFACES_CLIENT_ADMIN, false );

				$objSmarty->assign( 'content', $strHtmlContent );
				$objSmarty->assign( 'logo_url', CONFIG_COMMON_PATH . PATH_COMMON_EMAIL_IMAGES . 'update_employee_application/xento_logo.png' );

				$strHtmlContent = $objSmarty->nestedFetch( PATH_INTERFACES_CLIENT_ADMIN . 'user_administration/client_admin_email_template.tpl' );

				$objSystemEmailLibrary	= new CSystemEmailLibrary();
				$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::PS_EMPLOYEE, $strSubject, $strHtmlContent, CSystemEmail::XENTO_TRAINING_EMAIL_ADDRESS, 0, $objEmployee->getEmailAddress() );

				$objSystemEmail->setSystemEmailPriorityId( CSystemEmailPriority::CRITICAL );

				if( false == $objSystemEmail->validate( VALIDATE_INSERT ) || false == $objSystemEmail->insert( $intCurrentUserId, $objEmailDatabase ) ) {
					$objDatabase->rollback();
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', ' Failed to add certificate and send email.' ) );
					return false;
				}
			}
		}

		$objDatabase->commit();

		return $boolIsValid;
	}

	public function uploadFile( $objPsDocument ) {

        $boolValid = true;
        $objObjectStorage = $this->getObjectStorageGateway();
        if( false == isset( $objObjectStorage ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_resume', 'Unable to get object storage gateway. Invalid dependency container.' ) );
            return false;
        }
        $objPsDocument->setCid( CClient::ID_DEFAULT );
        if( false == $objPsDocument->uploadObject( $objObjectStorage, $_FILES['employee_certificate']['tmp_name'] ) ) {
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_certificate', 'Invalid file specified for certificate. ' ) );
            $boolValid = false;
        }
        return $boolValid;

	}

}
?>