<?php

class CDepartmentMetric extends CBaseDepartmentMetric {

	protected $m_intId;
	protected $m_intDivisionId;
	protected $m_intDepartmentId;
	protected $m_strName;
	protected $m_strDescription;
	protected $m_intDataType;
	protected $m_intDeletedBy;
	protected $m_strDeletedOn;
	protected $m_intUpdatedBy;
	protected $m_strUpdatedOn;
	protected $m_intCreatedBy;
	protected $m_strCreatedOn;

	const UNIQUE_CONTRACT_PROCESSED 			= 108;
	const UNIQUE_CLIENTS 						= 109;
	const PROPERTIES_ADDED 						= 110;
	const UNITS_ON_CONTRACT 					= 111;
	const TOTAL_PRODUCTS_ADDED 					= 112;
	const TOTAL_PRODUCTS_COMPLETED 				= 113;
	const AVERAGE_PRODUCTS_COUNT_PER_CONTRACT 	= 114;
	const TOTAL_ACV_ADDED 						= 115;
	const TOTAL_SETUP_FEES 						= 116;

	protected $m_objDepartmentMetric;

	/**
	 * Set Functions
	 *
	 */

	public function setDefaults() {

		$this->setRequestDatetime( date( 'm/d/Y H:i:s' ) );
	}

	public function setRequestDatetime( $strRequestDatetime ) {
		$this->m_strRequestDatetime = CStrings::strTrimDef( $strRequestDatetime, -1, NULL, true );
	}

	public function setDepartmentMetricDetail( $objDepartmentMetricDetail ) {
		$this->m_objDepartmentMetric = $objDepartmentMetricDetail;
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valRequiredField() {
		$boolIsValid = true;
		if( 0 == strlen( $this->getDepartmentId() ) || 0 == strlen( $this->getDivisionId() ) || 0 == strlen( $this->getName() || 0 == strlen( $this->getDataType() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'Department, Division, name, Data Type is required .' ) );
		}
		return $boolIsValid;
	}

	/**
	 * Create Functions
	 *
	 */

	public function valDivisionId() {
		$boolIsValid = true;
		if( false == isset( $this->m_intDivisionId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'department_division_id', 'Division is required' ) );
		}
		return $boolIsValid;
	}

	public function valDepartmentId() {

		$boolIsValid = true;

		if( false == isset( $this->m_intDepartmentId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'department_id', 'Deparment is required' ) );
		}

		return $boolIsValid;

	}

	public function valName() {
		$boolIsValid = true;
		if( false == isset( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'department_metric_name', 'Name is required' ) );
		}
		return $boolIsValid;
	}

	public function valDataType() {
		$boolIsValid = true;
		if( false == isset( $this->m_intDataType ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'department_data_type', 'Data type is required' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

	switch( $strAction ) {
		case VALIDATE_INSERT:
		case VALIDATE_UPDATE:
			$boolIsValid &= $this->valName();
			break;

		case VALIDATE_DELETE:
			break;

		default:
			$boolIsValid = false;
	}

	return $boolIsValid;
	}
}
?>
