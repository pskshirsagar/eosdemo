<?php

class CStatsUnitTransactionalAverage extends CBaseStatsUnitTransactionalAverage {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProductName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalUnits() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalUnitsWeighted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionRevenue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTransactionalAverageByMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valUnitTransactionalAverage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>