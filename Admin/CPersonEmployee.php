<?php

class CPersonEmployee extends CBasePersonEmployee {

	public function valDuplicatePersonEmployee( $objDatabase ) {
		$boolIsValid = true;

   		if( false == valObj( $objDatabase, 'CDatabase' ) ) {
			trigger_error( 'Database object CDatabase was not sent to the validation function on person_employee', E_USER_ERROR );
			exit;
   		}

		$intConflictingPersonEmployeeCount = CPersonEmployees::fetchConflictingPersonEmployeeCountByPersonEmployee( $this, $objDatabase );

		if( 0 < $intConflictingPersonEmployeeCount ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDuplicatePersonEmployee( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>