<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPurchaseRequestTypes
 * Do not add any new functions to this class.
 */

class CPurchaseRequestTypes extends CBasePurchaseRequestTypes {

	public static function fetchPurchaseRequestTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CPurchaseRequestType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPurchaseRequestType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CPurchaseRequestType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllPurchaseRequestTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM purchase_request_types order by id asc';
		return parent::fetchPurchaseRequestTypes( $strSql, $objDatabase );
	}

}
?>