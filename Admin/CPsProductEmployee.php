<?php

class CPsProductEmployee extends CBasePsProductEmployee {

	public function valPsProductId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductId() ) && false == is_numeric( $this->getPsProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_employee', 'Product id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductOptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getEmployeeId() ) && false == is_numeric( $this->getEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_employee', 'Employee id required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductEmployeeTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductEmployeeTypeId() ) && false == is_numeric( $this->getPsProductEmployeeTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_employee', 'Employee type id is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valPsProductEmployeeTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>