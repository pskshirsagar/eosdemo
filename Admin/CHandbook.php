<?php

class CHandbook extends CBaseHandbook {

	public function valVersionNumber() {
		$boolIsValid = true;

		if( false == isset( $this->m_strVersionNumber ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'version_number', 'Version number is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsDocumentId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intPsDocumentId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_document_id', 'Document Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valPsDocumentId();
				$boolIsValid &= $this->valVersionNumber();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>