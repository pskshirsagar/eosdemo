<?php

class CUserGroup extends CBaseUserGroup {

	/**
	 * Set Functions
	 *
	 */

	public function setGroupIdByDepartmentName( $strDepartmentName, $objDatabase ) {

		$strDepartmentName = str_replace( ' ', '_', $strDepartmentName );

		switch( $strDepartmentName ) {

			case 'Account_Management':
			case 'Accounting':
				$strGroupName = 'Accounting';
				break;

			case 'Development':
				$strGroupName = 'Development';
				break;

			case 'HR':
				$strGroupName = 'HR';
				break;

			case 'IT':
				$strGroupName = 'IT';
				break;

			case 'Insurance':
				$strGroupName = 'Insurance';
				break;

			case 'Legal':
				$strGroupName = 'Legal';
				break;

			case 'Marketing':
				$strGroupName = 'Marketing';
				break;

			case 'Merchant_Processing':
				$strGroupName = 'Merchant';
				break;

			case 'QA':
				$strGroupName = 'QA';
				break;

			case 'Sales':
				$strGroupName = 'Sales';
				break;

			case 'Support':
				$strGroupName = 'Support';
				break;

			case 'Utility_Billing':
				$strGroupName = 'Utility Billing';
				break;

			case 'Internal_Training':
				$strGroupName = 'Training';
				break;

			default:
				$strGroupName = $strDepartmentName;
		}

		$objGroup = new CGroup();
		$objGroup = CGroups::fetchGroupByName( $strGroupName, $objDatabase );
		if( true == valObj( $objGroup, 'CGroup' ) ) {
			$this->setGroupId( $objGroup->getId() );
		}

		return;
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valUser( $objDatabase ) {

		$boolIsValid = true;

		if( true == valObj( $objDatabase, 'CDatabase' ) ) {
			$strSql = ' WHERE user_id = ' . $this->m_intUserId . ' AND group_id = ' . $this->m_intGroupId . ' AND deleted_by IS NULL';
			$intCount = CUserGroups::fetchUserGroupCount( $strSql, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'Employee already exists in group.' ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_user':
				$boolIsValid &= $this->valUser( $objDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>