<?php

class CIlsErrorFixLog extends CBaseIlsErrorFixLog {

	public function valIlsEmailErrorTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->m_intIlsEmailErrorTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ils_email_error_type_id', 'Error Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFixDescription() {
		$boolIsValid = true;

		$strDescription	= str_replace( '&nbsp;', '', str_replace( '<br />', '', $this->m_strFixDescription ) );
		$strDescription = strip_tags( html_entity_decode( nl2br( trim( $strDescription ) ), ENT_QUOTES ) );

		if( 0 >= strlen( $strDescription ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valReleaseDate() {
		$boolIsValid = true;

		if( true == is_null( $this->m_strReleaseDate ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'release_date', 'Release date is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valReleaseDate();
				$boolIsValid &= $this->valFixDescription();
				$boolIsValid &= $this->valIlsEmailErrorTypeId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>