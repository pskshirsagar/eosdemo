<?php

class CEmployeeOption extends CBaseEmployeeOption {

	protected $m_strEmployeeName;
	protected $m_strVestingScheduleName;
	protected $m_strOptionEncrypted;
	protected $m_strOptionDecrypted;
	protected $m_strExercisePriceEncrypted;
	protected $m_intExercisePriceDecrypted;

	/**
	 * Get Functions
	 */

	public function getEmployeeName() {
		return $this->m_strEmployeeName;
	}

	public function getVestingScheduleName() {
		return $this->m_strVestingScheduleName;
	}

	public function getOptionEncrypted() {
		return $this->m_strOptionEncrypted;
	}

	public function getOptionDecrypted() {
		return $this->m_strOptionDecrypted;
	}

	public function getExercisePriceEncrypted() {
		return $this->m_strExercisePriceEncrypted;
	}

	public function getExercisePriceDecrypted() {
		return $this->m_intExercisePriceDecrypted;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['employee_name'] ) ) 				$this->setEmployeeName( $arrmixValues['employee_name'] );
		if( true == isset( $arrmixValues['vesting_schedule_name'] ) ) 		$this->setVestingScheduleName( $arrmixValues['vesting_schedule_name'] );
		if( true == isset( $arrmixValues['option_encrypted'] ) ) 			$this->setOptionEncrypted( $arrmixValues['option_encrypted'] );
		if( true == isset( $arrmixValues['exercise_price_encrypted'] ) ) 	$this->setExercisePriceEncrypted( $arrmixValues['exercise_price_encrypted'] );
	}

	public function setEmployeeName( $strEmployeeName ) {
		$this->m_strEmployeeName = $strEmployeeName;
	}

	public function setVestingScheduleName( $strVestingScheduleName ) {
		$this->m_strVestingScheduleName = $strVestingScheduleName;
	}

	public function setOptionEncrypted( $strOptionEncrypted ) {
		$this->m_strOptionEncrypted = $strOptionEncrypted;
	}

	public function setOptionDecrypted( $strOptionDecrypted ) {
		$this->m_strOptionDecrypted = $strOptionDecrypted;
	}

	public function setExercisePriceEncrypted( $strExercisePriceEncrypted ) {
		$this->m_strExercisePriceEncrypted = $strExercisePriceEncrypted;
	}

	public function setExercisePriceDecrypted( $intExercisePriceDecrypted ) {
		$this->m_intExercisePriceDecrypted = $intExercisePriceDecrypted;
	}

	/**
	 * Validation Functions
	 */

	public function valEmployeeId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intEmployeeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_name', 'Employee name is required.' ) );
		}
		return $boolIsValid;
	}

	public function valVestingScheduleId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intVestingScheduleId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'vesting_schedule', 'Vesting schedule is required.' ) );
		}
		return $boolIsValid;
	}

	public function valEmployeeOptionAssociationId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intEmployeeOptionAssociationId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'option', 'Option is required.' ) );
		}
		return $boolIsValid;
	}

	public function valExercisePriceAssociationId() {
		$boolIsValid = true;

		if( true == empty( $this->m_intExercisePriceAssociationId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'exercise_price_encrypted', 'Exercise price is required.' ) );
		}
		return $boolIsValid;
	}

	public function valOptionDatetime() {
		$boolIsValid = true;

		if( true == empty( $this->m_strOptionDatetime ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'option_datetime', 'Option datetime is required.' ) );
		}
		return $boolIsValid;
	}

	public function valFinalVestingDate() {
		$boolIsValid = true;

		if( false == is_null( $this->m_strVestingCommencementDate ) && false == is_null( $this->m_strFinalVestingDate ) && strtotime( $this->m_strVestingCommencementDate ) > strtotime( $this->m_strFinalVestingDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'final_vesting_date', 'Vesting commencement date should be less than final vesting date.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valVestingScheduleId();
				$boolIsValid &= $this->valEmployeeOptionAssociationId();
				$boolIsValid &= $this->valExercisePriceAssociationId();
				$boolIsValid &= $this->valOptionDatetime();
				$boolIsValid &= $this->valFinalVestingDate();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>