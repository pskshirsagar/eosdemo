<?php

class CTaskNote extends CBaseTaskNote {

	private $m_intTaskAttachmentId;
	private $m_strUrl;
	private $m_intCid;

	protected $m_strEmployeeNameFirst;
	protected $m_strEmployeeNameLast;
	protected $m_strEmployeePreferredName;
	protected $m_strNameFull;
	protected $m_strTaskNoteType;

	const RCA_PRECAUTIONARY_MEASUREMENT = 1;
	const RCA_FOLLOW_UP_STATUS			= 2;
	const SUPPORT_CALL_REQUEST			= 3;
	const RESOLVE_TASK_NOTE 			= 'Entrata User has marked this task as resolved.';

	/**
	 * Set Functions
	 */

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function setEmployeeNameFirst( $strEmployeeNameFirst ) {
		$this->m_strEmployeeNameFirst = $strEmployeeNameFirst;
	}

	public function setEmployeeNameLast( $strEmployeeNameLast ) {
		$this->m_strEmployeeNameLast = $strEmployeeNameLast;
	}

	public function setEmployeePreferredName( $strEmployeePreferredName ) {
		$this->m_strEmployeePreferredName = $strEmployeePreferredName;
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setTaskNoteType( $strTaskNoteType ) {
		$this->m_strTaskNoteType = $strTaskNoteType;
	}

	/**
	 * Get Functions
	 */

	public function getCid() {
		return $this->m_intCid;
	}

	public function getEmployeeNameFirst() {
		return $this->m_strEmployeeNameFirst;
	}

	public function getEmployeeNameLast() {
		return $this->m_strEmployeeNameLast;
	}

	public function getEmployeePreferredName() {
		return $this->m_strEmployeePreferredName;
	}

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function getTaskNoteType() {
		return $this->m_strTaskNoteType;
	}

	public function createTaskAttachment() {

		$objTaskAttachment = new CTaskAttachment();
		$objTaskAttachment->setTaskNoteId( $this->m_intId );
		$objTaskAttachment->setTaskId( $this->m_intTaskId );

		return $objTaskAttachment;
	}

	public function createTaskCompany() {

		$objTaskCompany = new CTaskCompany();
		$objTaskCompany->setTaskNoteId( $this->m_intId );
		$objTaskCompany->setTaskId( $this->m_intTaskId );
		$objTaskCompany->setUserId( $this->getUserId() );

		return $objTaskCompany;
	}

	public function createOutboundCall( $objCallQueue, $objCallAgent ) {

		$objOutboundCall = new COutboundCall();

		$objOutboundCall->setTaskId( $this->getTaskId() );
		$objOutboundCall->setTaskNoteId( $this->getId() );
		$objOutboundCall->setCallTypeId( CCallType::OUTBOUND_SUPPORT );
		$objOutboundCall->setOutboundCallStatusTypeId( COutboundCallStatusType::PENDING );
		$objOutboundCall->setReceipientName( $this->getContactFullName() );
		$objOutboundCall->setPhoneNumber( \Psi\CStringService::singleton()->preg_replace( '/[^0-9]/s', '', $this->getPhoneNumber() ) );
		$objOutboundCall->setOutboundCallCampaignId( COutboundCallCampaign::SUPPORT_OUTBOUND_DIALER );
		$objOutboundCall->setInitiatedOn( date( 'Y-m-d H:m:i' ) );
		$objOutboundCall->setAssignedOn( date( 'Y-m-d H:m:i' ) );
		$objOutboundCall->setStartTime( date( 'H:m:i' ) );

		if( true == valObj( $objCallQueue, 'CCallQueue' ) ) $objOutboundCall->setCallQueueId( $objCallQueue->getId() );
		if( true == valObj( $objCallAgent, 'CCallAgent' ) ) $objOutboundCall->setCallAgentId( $objCallAgent->getId() );

		return $objOutboundCall;
	}

	public function createDiagnosticNote( $intDiagnosticId, $intEmployeeId ) {
		$objDiagnosticNote = new CDiagnosticNote();
		$objDiagnosticNote->setDiagnosticNoteTypeId( CDiagnosticNoteType::TASK_NOTE );
		$objDiagnosticNote->setEmployeeId( $intEmployeeId );
		$objDiagnosticNote->setDiagnosticId( $intDiagnosticId );
		$objDiagnosticNote->setDiagnosticNote( $this->getTaskNote() );
		$objDiagnosticNote->setDiagnosticNoteDatetime( 'NOW()' );
		return $objDiagnosticNote;
	}

	/**
	 * Validation Functions
	 */

	public function valTaskNote() {
		$boolIsValid = true;
		if( ( CTaskNoteType::CORRECTIVE_ACTION_TAKEN == $this->getTaskNoteTypeId() ) && ( true == is_null( $this->getTaskNote() ) ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'corrective_action_taken', 'Corrective Action Taken is required.' ) );
		} elseif( ( CTaskNoteType::BLOCKER_NOTE == $this->getTaskNoteTypeId() && true == $this->getBlockerResponseId() ) && ( true == is_null( $this->getTaskNote() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_note', 'Answer is required.' ) );
		} elseif( ( CTaskNoteType::BLOCKER_NOTE == $this->getTaskNoteTypeId() ) && ( true == is_null( $this->getTaskNote() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_note', 'Blocker is required.' ) );
		} elseif( true == is_null( $this->getTaskNote() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_note', __( 'Task note is required.' ) ) );
		}

		if( true == valObj( $this, 'CTaskNote' ) ) {
			$boolValidNote = CTask::validateCreditCardNumber( $this->getTaskNote() );
			if( false == $boolValidNote ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_note', 'Please remove credit card number from task note.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valTimeSpent() {
		$boolIsValid = true;
		if( false == is_null( $this->getTimeSpent() ) && false == is_numeric( $this->getTimeSpent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'time_spent', 'Time spent must be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valPrecautionaryMeasurement() {
		$boolIsValid = true;
		if( true == is_null( $this->getTaskNote() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_note', 'Precautionary Measurement is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFollowUpDetail( $strTaskFollowUpStatus ) {
		$boolIsValid = true;
		if( true == is_null( $this->getTaskNote() ) && 'Not-Followed' == $strTaskFollowUpStatus ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_note', 'Follow-Up note is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $strTaskFollowUpStatus = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTaskNote();
				$boolIsValid &= $this->valTimeSpent();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_precautionary_measurement':
				$boolIsValid &= $this->valPrecautionaryMeasurement();
				break;

			case 'insert_follow_up_note':
				$boolIsValid &= $this->valFollowUpDetail( $strTaskFollowUpStatus );
				break;

			case 'insert_val_task_note':
				$boolIsValid &= $this->valTaskNote();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchTaskAttachment( $objDatabase ) {

		return \Psi\Eos\Admin\CTaskAttachments::createService()->fetchTaskAttachmentByTaskNoteId( $this->m_intId, $objDatabase );
	}

	public function fetchTask( $objDatabase ) {
		return \Psi\Eos\Admin\CTasks::createService()->fetchTaskById( $this->getTaskId(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['url'] ) )						$this->setUrl( $arrmixValues['url'] );
		if( true == isset( $arrmixValues['task_attachment_id'] ) )		$this->setTaskAttachmentId( $arrmixValues['task_attachment_id'] );
		if( true == isset( $arrmixValues['cid'] ) )						$this->setCid( $arrmixValues['cid'] );
		if( true == isset( $arrmixValues['employee_name_first'] ) )		$this->setEmployeeNameFirst( $arrmixValues['employee_name_first'] );
		if( true == isset( $arrmixValues['employee_name_last'] ) )		$this->setEmployeeNameLast( $arrmixValues['employee_name_last'] );
		if( true == isset( $arrmixValues['name_full'] ) )				$this->setNameFull( $arrmixValues['name_full'] );
		if( true == isset( $arrmixValues['note_type'] ) )				$this->setTaskNoteType( $arrmixValues['note_type'] );
		if( true == isset( $arrmixValues['employee_preferred_name'] ) )	$this->setEmployeePreferredName( $arrmixValues['employee_preferred_name'] );
		return;
	}

	public function setUrl( $strUrl ) {
		$this->m_strUrl = $strUrl;
	}

	public function setTaskAttachmentId( $intTaskAttachmentId ) {
		$this->m_intTaskAttachmentId = $intTaskAttachmentId;
	}

	public function getUrl() {
		return $this->m_strUrl;
	}

	public function getTaskAttachmentId() {
		return $this->m_intTaskAttachmentId;
	}

}
?>