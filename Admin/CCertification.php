<?php

class CCertification extends CBaseCertification {

    use Psi\Libraries\EosFoundation\TEosStoredObject;

	protected $m_strDepartmentName;
	protected $m_strDesignationName;
	protected $m_boolIsExistingCertificate = false;
    protected $m_strTitle;
    protected $m_strFileName;

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['department_name'] ) )	$this->setDepartmentName( trim( $arrmixValues['department_name'] ) );
		if( true == isset( $arrmixValues['designation_name'] ) )	$this->setDesignationName( trim( $arrmixValues['designation_name'] ) );

		return;
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	public function setDesignationName( $strDesignationName ) {
		$this->m_strDesignationName = $strDesignationName;
	}

    public function setTitle( $strTitle ) {
        $this->m_strTitle = $strTitle;
    }

    public function setFileName( $strFileName ) {
        $this->set( 'm_strFileName', CStrings::strTrimDef( $strFileName, 240, NULL, true ) );
    }

	/**
	 * Get Functions
	 *
	 */

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getDesignationName() {
		return $this->m_strDesignationName;
	}

    public function getTitle() {
        return $this->m_strTitle;
    }

    public function getFileName() {
        return $this->m_strFileName;
    }

	/**
	 * Validate Functions
	 *
	 */

	public function valCertificationTypeId() {
		$boolValid = true;

		if( false == is_numeric( $this->getCertificationTypeId() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'certification_type_id', 'Please select certification type.' ) );
		}

		return $boolValid;
	}

	public function valName( $objDatabase ) {
		$boolValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', ' Certification name required.' ) );
			return $boolValid;
		}

		$intCertificationId = CCertifications::fetchCertificationIdByCertificationTypeIdByCertificationLevelTypeIdByDepartmentIdByDesignationId( $this->getCertificationTypeId(), $this->getCertificationLevelTypeId(), $this->getDepartmentId(), $this->getDesignationId(), $this->getIsExternal(), $objDatabase, NULL, true, $this->getName() );

		if( true == valStr( $this->getName() ) && true == is_numeric( $intCertificationId ) && $intCertificationId != $this->getId() ) {
			$boolValid = false;
			$this->m_boolIsExistingCertificate = true;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'id', 'This certification already exists.' ) );
		}

		return $boolValid;
	}

	public function valCertificatePath( $objDatabase ) {

		$boolValid = true;

		$arrstrFileParts = pathinfo( $this->m_strLogoPath );
		$strSupportedExtentionType = 'pdf';
		if( true == is_null( $this->m_strLogoPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Sample Certificate is Required.', NULL ) );
			$boolValid = false;
		}else if( false == is_null( $this->m_strLogoPath ) && $arrstrFileParts['extension'] != $strSupportedExtentionType ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Please upload pdf file as sample certificate.', NULL ) );
			$boolValid = false;
		}

		return $boolValid;
	}

	public function valMinimumScore() {
		$boolValid = true;

		if( true == is_null( $this->m_intMinimumScore ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_score', 'Minimum score is required' ) );
		} elseif( false == is_numeric( $this->m_intMinimumScore ) || $this->m_intMinimumScore < 1 || $this->m_intMinimumScore > 100 ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'minimum_score', 'Please enter valid minimum score.' ) );
		}

		return $boolValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $boolIsTestAssociated = false, $boolValCertificate = true ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolValid &= $this->valCertificationTypeId();
				$boolValid &= $this->valName( $objDatabase );
				if( true == $boolValCertificate ) {
					$boolValid &= $this->valCertificatePath( $objDatabase );
				}
				if( false == $boolIsTestAssociated ) {
					$boolValid &= $this->valMinimumScore();
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_new_certification':
				$boolValid &= $this->valCertificationTypeId();
				$boolValid &= $this->valName( $objDatabase );
				break;

			default:
				// default case
				break;
		}

		return $boolValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( date( 'm/d/Y H:i:s' ) );

	 	return $this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

    protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {

        switch( $strVendor ) {
            case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
                return $strPath = '/certifications/' . $this->getLogoPath();
                break;

            default:
                $strPath = sprintf( '%d/%s%d/%02d/%02d/%d/', CClient::ID_DEFAULT, 'documents/certifications/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getId() ) . $this->getFileName() ;

        }

        return $strPath;
    }

    protected function calcStorageContainer( $strVendor = NULL ) {

        switch( $strVendor ) {
            case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
                return 'admin_medias';
                break;

            default:
                return CONFIG_OSG_BUCKET_SYSTEM_EMPLOYEES;
        }
    }

    protected function createStoredObject() {
        return new \CStoredObject();
    }

    protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
        return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
    }


}
?>