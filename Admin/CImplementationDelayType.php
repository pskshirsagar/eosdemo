<?php

class CImplementationDelayType extends CBaseImplementationDelayType {

	const CLIENT_FAILED_TO_PROVIDE_CONTENT 		= 1;
	const CLIENT_NOT_SATISFIED_WITH_PRODUCT 	= 2;
	const INSUFFICIENT_RESOURCES_ON_CLIENT_SIDE	= 3;
	const INSUFFICIENT_RESOURCES_AT_PS			= 4;
	const BILLING_OR_CONTRACT_CONCERNS			= 5;
	const PHASED_IMPLEMENTATION					= 6;
	const BUNDLE_OR_NO_INTENT_TO_USE			= 7;

	public static $c_arrintImplementationDelayTypeIds	= array( self::CLIENT_FAILED_TO_PROVIDE_CONTENT, self::CLIENT_NOT_SATISFIED_WITH_PRODUCT,
														self::INSUFFICIENT_RESOURCES_ON_CLIENT_SIDE, self::INSUFFICIENT_RESOURCES_AT_PS,
														self::BILLING_OR_CONTRACT_CONCERNS, self::PHASED_IMPLEMENTATION );

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>