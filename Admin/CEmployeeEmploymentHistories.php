<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeEmploymentHistories
 * Do not add any new functions to this class.
 */

class CEmployeeEmploymentHistories extends CBaseEmployeeEmploymentHistories {

	public static function fetchEmployeeEmploymentHistories( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmployeeEmploymentHistory', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEmployeePreviousExperienceWithUsById( $intEmployeeId, $objDatabase ) {

		$strSql = ' SELECT
						extract ( year FROM Sum ( subQuery.previous_experience ) ) AS previous_experience_years,
						extract ( month FROM Sum ( subQuery.previous_experience ) ) AS previous_experience_months
					FROM
						(
						 SELECT
							age ( eeh.date_terminated, eeh.date_started ) as previous_experience
						 FROM
							employee_employment_histories eeh
						 WHERE
							eeh.employee_id = ' . ( int ) $intEmployeeId . '
						) AS subQuery ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEmploymentHistoryByEmployeeId( $intEmployeeId, $objDatabase ) {
		if( false == is_numeric( $intEmployeeId ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						employee_employment_histories
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND country_code IS NOT NULL
					ORDER BY
						id
					DESC LIMIT 1';

		return self::fetchEmployeeEmploymentHistory( $strSql, $objDatabase );

	}

	public static function fetchLastTerminationDateByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT max( date_terminated ) AS date_terminated FROM employee_employment_histories WHERE employee_id = ' . ( int ) $intEmployeeId;
		return fetchData( $strSql, $objDatabase );
	}
}
?>