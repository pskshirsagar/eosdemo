<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsEventTypes
 * Do not add any new functions to this class.
 */

class CPsEventTypes extends CBasePsEventTypes {

	public static function fetchPsEventTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPsEventType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPsEventType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPsEventType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPublishedPsEventTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM ps_event_types WHERE is_published = 1 ORDER BY order_num';
		return self::fetchPsEventTypes( $strSql, $objDatabase );
	}
}
?>