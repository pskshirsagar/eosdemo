<?php

class CTeamBacklogDetail extends CBaseTeamBacklogDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTeamBacklogId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTeamId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductOptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAssociatedProducts( $arrintPsProductIds, $arrintPsProductOptionIds ) {
		$boolIsValid = true;

		if( false == valArr( $arrintPsProductIds ) && false == valArr( $arrintPsProductOptionIds ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'associated_products', 'Associated product is required.' ) );
		}
		return $boolIsValid;
	}

	public function valAssociatedTeams( $arrintTeamIds ) {
		$boolIsValid = true;

		if( false == valArr( $arrintTeamIds ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'associated_teams', 'Please select Team.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $arrintPsProductIds, $arrintPsProductOptionIds, $arrintTeamIds ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valAssociatedProducts( $arrintPsProductIds, $arrintPsProductOptionIds );
				$boolIsValid &= $this->valAssociatedTeams( $arrintTeamIds );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
