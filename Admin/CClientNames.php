<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CClientNames
 * Do not add any new functions to this class.
 */

class CClientNames extends CBaseClientNames {

	public static function fetchClientNamesByCid( $intCid, $objDatabase ) {

		if( true == empty( $intCid ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						client_names
					WHERE
						cid = ' . ( int ) $intCid . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return parent::fetchClientNames( $strSql, $objDatabase );
	}

	public static function fetchClientNamesByClientNames( $arrstrClientNames, $objDatabase, $intCid ) {

		if( false == valArr( $arrstrClientNames ) || false == valId( $intCid ) ) return NULL;

		$strSql = ' SELECT
						id,
						client_name
					FROM
						client_names
					WHERE
						client_name IN ( ' . implode( ',', $arrstrClientNames ) . ' ) 
						AND cid = ' . ( int ) $intCid . '
						AND deleted_on IS NULL';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchClientNamesByCidByByClientNameTypeId( $intCid, $intClientNameTypeId, $objDatabase, $boolStartDate = false ) {

		if( true == empty( $intCid ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						client_names
					WHERE
						cid = ' . ( int ) $intCid . '
						AND client_name_type_id = ' . ( int ) $intClientNameTypeId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';
		if( true == $boolStartDate ) {
			$strSql .= ' AND start_datetime <= NOW()';
		}

		return self::fetchClientNames( $strSql, $objDatabase );
	}

}
?>