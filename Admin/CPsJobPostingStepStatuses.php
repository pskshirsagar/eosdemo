<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsJobPostingStepStatuses
 * Do not add any new functions to this class.
 */

class CPsJobPostingStepStatuses extends CBasePsJobPostingStepStatuses {

	public static function fetchPsJobPostingStepStatuses( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPsJobPostingStepStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPsJobPostingStepStatus( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPsJobPostingStepStatus', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPsJobPostingStepStatusByStepIdByStatusId( $intPsJobPostingStepId, $intPsJobPostingStatusId, $objDatabase ) {

		if( false == is_numeric( $intPsJobPostingStepId ) || false == is_numeric( $intPsJobPostingStatusId ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						ps_job_posting_step_statuses pjpss
					WHERE
						pjpss.ps_job_posting_status_id = ' . ( int ) $intPsJobPostingStatusId . '
						AND pjpss.ps_job_posting_step_id = ' . ( int ) $intPsJobPostingStepId . '
					LIMIT 1';

		return self::fetchObject( $strSql, 'CPsJobPostingStepStatus', $objDatabase );
	}

	public static function fetchPsJobPostingStepStatusesByIds( $arrintPsJobPostingStepStatusIds, $objDatabase ) {

		if( false == valArr( $arrintPsJobPostingStepStatusIds ) ) return NULL;

		$strSql = 'SELECT
						pjpss.id AS step_status_id,
						pjps.name,
						pjpss1.*
					FROM
						ps_job_posting_step_statuses pjpss
						JOIN ps_job_posting_step_statuses pjpss1 ON ( pjpss.ps_job_posting_step_id = pjpss1.ps_job_posting_step_id AND pjpss1.ps_job_posting_status_id = ' . CPsJobPostingStatus::REJECTED . ')
						LEFT JOIN ps_job_posting_steps pjps ON ( pjpss.ps_job_posting_step_id = pjps.id )
						LEFT JOIN employee_application_steps eas ON ( pjpss.id = eas.ps_job_posting_step_status_id )
					WHERE
						pjpss.id IN ( ' . implode( ',', $arrintPsJobPostingStepStatusIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPsJobPostingStepStatusByPsJobPostingIdByPsJobPostingStatusIdByPsJobPostingStepOrderNum( $intPsJobPostingId, $intPsJobPostingStatusId, $intPsJobPostingStepOrderNum, $objDatabase ) {

		if( false == is_numeric( $intPsJobPostingId ) || false == is_numeric( $intPsJobPostingStatusId ) || false == is_numeric( $intPsJobPostingStepOrderNum ) ) return NULL;

		$strSql = 'SELECT
						pjpss.id
					FROM
						ps_job_posting_steps pjps
						LEFT JOIN ps_job_posting_step_statuses pjpss ON pjpss.ps_job_posting_step_id = pjps.id
					WHERE
						pjps.ps_job_posting_id = ' . ( int ) $intPsJobPostingId . '
						AND pjpss.ps_job_posting_status_id = ' . ( int ) $intPsJobPostingStatusId . '
						AND pjps.order_num = ' . ( int ) $intPsJobPostingStepOrderNum;

		return self::fetchObject( $strSql, 'CPsJobPostingStepStatus', $objDatabase );
	}

	public static function fetchPsJobPostingStepStatusByPsJobPostingIdByPsJobPostingStatusIdByPsJobPostingStepName( $intPsJobPostingId, $intPsJobPostingStatusId, $strPsJobPostingStepName, $objDatabase, $intJobPostingStepTypeId = NULL ) {

		if( false == is_numeric( $intPsJobPostingId ) || false == is_numeric( $intPsJobPostingStatusId ) || false == valStr( $strPsJobPostingStepName ) ) return NULL;

		$strWhereCondition = ( true == is_numeric( $intJobPostingStepTypeId ) ) ? ' AND pjps.ps_job_posting_step_type_id = ' . ( int ) $intJobPostingStepTypeId : '';

		$strPsJobPostingStepName = str_replace( "'", "''", $strPsJobPostingStepName );
		$strSql = 'SELECT
						pjpss.id
					FROM
						ps_job_posting_steps pjps
						LEFT JOIN ps_job_posting_step_statuses pjpss ON pjpss.ps_job_posting_step_id = pjps.id
					WHERE
						pjps.ps_job_posting_id = ' . ( int ) $intPsJobPostingId . '
						AND pjpss.ps_job_posting_status_id = ' . ( int ) $intPsJobPostingStatusId . '
						AND pjps.name ILIKE \'%' . $strPsJobPostingStepName . '%\'' . $strWhereCondition . ' ORDER BY pjpss.id DESC LIMIT 1';

		return parent::fetchPsJobPostingStepStatus( $strSql, $objDatabase );
	}

	public static function fetchPsJobPostingStepStatusNameById( $intPsJobPostingStepStatusId, $objDatabase ) {
		if( false == is_numeric( $intPsJobPostingStepStatusId ) ) return NULL;

		$strSql = 'SELECT
						pjps.name AS current_step,
						pjps.ps_job_posting_step_type_id AS current_step_type_id,
						psstat.name AS current_status,
						psstat.id AS status_id
					FROM
						ps_job_posting_step_statuses pjss
						LEFT JOIN ps_job_posting_steps pjps ON ( pjps.id = pjss.ps_job_posting_step_id )
						LEFT JOIN ps_job_posting_statuses psstat ON ( psstat.id = pjss.ps_job_posting_status_id )
					WHERE
						pjss.id = ' . ( int ) $intPsJobPostingStepStatusId;

		$arrstrStepStatus = fetchData( $strSql, $objDatabase );
		return $arrstrStepStatus[0];
	}

}
?>