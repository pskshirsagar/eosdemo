<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPostalCodes
 * Do not add any new functions to this class.
 */

class CPostalCodes extends CBasePostalCodes {

	public static function fetchPostalCodeByPostalCode( $strPostalCode, $objDatabase ) {
		$strSql = 'SELECT * FROM postal_codes WHERE postal_code = \'' . trim( $strPostalCode ) . '\'';
		return self::fetchPostalCode( $strSql, $objDatabase );
	}

	public static function fetchStatesPostalCodeCount( $objDatabase ) {

		$strSql = 'select state_code ,count(postal_code) from postal_codes group by state_code';
		$arrStatePostalCodeCount = NULL;
		$objPostalCode = NULL;

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$arrStatePostalCodeCount = array();

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();
				$arrStatePostalCodeCount[$arrmixValues['state_code']] = $arrmixValues;
				$objDataset->next();
			}
		}
		$objDataset->cleanup();

		return $arrStatePostalCodeCount;
	}

	public static function fetchPostalCodesByStateCode( $strStateCode, $objDatabase ) {
		return self::fetchPostalCodes( sprintf( 'SELECT * FROM postal_codes WHERE state_code = \'%s\'', $strStateCode ), $objDatabase );
	}

	public static function fetchPostalCodes( $strSql, $objDatabase ) {
		$arrobjPostalCodes = NULL;
		$objPostalCode = NULL;

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			$objDataset->cleanup();
			return NULL;
		}

		if( 0 < $objDataset->getRecordCount() ) {
			$arrobjPostalCodes = array();

			while( false == $objDataset->eof() ) {
				$arrmixValues = $objDataset->fetchArray();

				$objPostalCode = new CPostalCode();
				$objPostalCode->setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );
				$objPostalCode->setSerializedOriginalValues( serialize( $arrmixValues ) );

				if( false == is_null( $objPostalCode->getPostalCode() ) ) {
					$arrobjPostalCodes[$objPostalCode->getPostalCode()] = $objPostalCode;
				} else {
					$arrobjPostalCodes[] = $objPostalCode;
				}

				$objDataset->next();
			}
		}

		$objDataset->cleanup();

		return $arrobjPostalCodes;
	}

	public static function fetchPostalCode( $strSql, $objDatabase ) {
		$objPostalCode = NULL;

		$arrobjPostalCodes = self::fetchPostalCodes( $strSql, $objDatabase );
		if( true == valArr( $arrobjPostalCodes ) ) {
			if( 1 < \Psi\Libraries\UtilFunctions\count( $arrobjPostalCodes ) ) {
				trigger_error( 'Expecting a single record when multiple records returned. Sql = ' . $strSql, E_USER_WARNING );
				return NULL;
			}

			$objPostalCode = array_shift( $arrobjPostalCodes );
		}

		return $objPostalCode;
	}

}
?>