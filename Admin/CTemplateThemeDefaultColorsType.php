<?php

class CTemplateThemeDefaultColorsType extends CBaseTemplateThemeDefaultColorsType {

    public function valId() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function valColorName() {
        $boolIsValid = true;
        return $boolIsValid;
    }

    public function validate( $strAction ) {
        $boolIsValid = true;

        switch( $strAction ) {
        	case VALIDATE_INSERT:
        	case VALIDATE_UPDATE:
        	case VALIDATE_DELETE:
        		break;

        	default:
        	$boolIsValid = false;
        }

        return $boolIsValid;
    }

}
?>