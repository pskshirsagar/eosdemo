<?php

use Psi\Eos\Admin\CCommissionRecipients;
use Psi\Eos\Admin\CCommissions;
use Psi\Eos\Admin\CCommissionPayments;
use Psi\Eos\Admin\CCommissionBatches;
use Psi\Eos\Admin\CCommissionRateAssociations;
use Psi\Eos\Admin\CFedAchParticipants;

class CCommissionRecipient extends CBaseCommissionRecipient {

	const ID_RIVERSTONE 	= 211;

	protected $m_fltReadyCommissionAmount;
	protected $m_fltCommissionAmountDue;
	protected $m_fltReadyTransactionTotal;
	protected $m_fltOutstandingCommissionAmount;

	protected $m_strNameFull;

	protected $m_intReadyFloatLoadCount;

	/**
	 * Get Functions
	 *
	 */

	public function getNameFull() {
		return $this->getNameFirst() . ' ' . $this->getNameLast();
	}

	public function getCheckAccountNumber() {
		if( true == valStr( $this->m_strCheckAccountNumberEncrypted ) ) {
			return( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->m_strCheckAccountNumberEncrypted, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_ACH_ACCOUNT_NUMBER ] ) );
		}
		return NULL;
	}

	public function getReadyCommissionAmount() {
		return $this->m_fltReadyCommissionAmount;
	}

	public function getOutstandingCommissionAmount() {
		return $this->m_fltOutstandingCommissionAmount;
	}

	public function getCommissionAmountDue() {
		return $this->m_fltCommissionAmountDue;
	}

	public function getReadyTransactionTotal() {
		return $this->m_fltReadyTransactionTotal;
	}

	public function getReadyFrontLoadCount() {
		return $this->m_intReadyFloatLoadCount;
	}

	public function getCheckAccountNumberMasked() {
		$strCheckAccountNumber = $this->getCheckAccountNumber();

		if( false == valStr( $strCheckAccountNumber ) ) {
			return NULL;
		}
		$intStringLength = strlen( $strCheckAccountNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['check_account_number'] ) ) {
			$this->setCheckAccountNumber( $arrmixValues['check_account_number'] );
		}

		if( true == isset( $arrmixValues['ready_commission_amount'] ) ) {
			$this->setReadyCommissionAmount( $arrmixValues['ready_commission_amount'] );
		}

		if( true == isset( $arrmixValues['outstanding_commission_amount'] ) ) {
			$this->setOutstandingCommissionAmount( $arrmixValues['outstanding_commission_amount'] );
		}

		if( true == isset( $arrmixValues['transaction_total'] ) ) {
			$this->setReadyTransactionTotal( $arrmixValues['transaction_total'] );
		}

		if( true == isset( $arrmixValues['front_load_count'] ) ) {
			$this->setReadyFrontLoadCount( $arrmixValues['front_load_count'] );
		}

		return;
	}

	public function setReadyTransactionTotal( $fltReadyTransactionTotal ) {
		$this->m_fltReadyTransactionTotal = $fltReadyTransactionTotal;
	}

	public function setReadyFrontLoadCount( $intReadyFrontLoadCount ) {
		$this->m_intReadyFloatLoadCount = $intReadyFrontLoadCount;
	}

	public function setReadyCommissionAmount( $fltReadyCommissionAmount ) {
		$this->m_fltReadyCommissionAmount = $fltReadyCommissionAmount;
	}

	public function setOutstandingCommissionAmount( $fltOutstandingCommissionAmount ) {
		$this->m_fltOutstandingCommissionAmount = $fltOutstandingCommissionAmount;
	}

	public function setCommissionAmountDue( $fltCommissionAmountDue ) {
		$this->m_fltCommissionAmountDue = $fltCommissionAmountDue;
	}

	public function setCheckAccountNumber( $strCheckAccountNumber ) {
		$strCheckAccountNumber = trim( $strCheckAccountNumber );
		if( true == \Psi\CStringService::singleton()->stristr( $strCheckAccountNumber, 'X' ) ) {
			return;
		}
		$strCheckAccountNumber = CStrings::strTrimDef( $strCheckAccountNumber, 20, NULL, true );
		if( true == valStr( $strCheckAccountNumber ) ) {
			$this->setCheckAccountNumberEncrypted( \Psi\Libraries\Cryptography\CCrypto::createService()->encrypt( $strCheckAccountNumber, CONFIG_SODIUM_KEY_ACH_ACCOUNT_NUMBER ) );
		}
	}

	/**
	 * validate Functions
	 *
	 */

	public function valNameFirst( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First Name is required.' ) );
		}
		$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
		$intCount = CCommissionRecipients::createService()->fetchCommissionRecipientCount( ' WHERE deleted_on IS NULL AND lower( name_first ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getNameFirst() ) ) . '\'AND lower( name_last ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getNameLast() ) ) . '\'' . $strSqlCondition, $objDatabase );

		if( 0 < $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Employee is already in use.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckBankName() {
		return true;
	}

	public function valCheckAccountTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCheckAccountTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_type_id', 'Check account type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckRoutingNumber( $objPaymentDatabase = NULL ) {
		$boolIsValid = true;

		if( false == is_null( $this->getCheckRoutingNumber() ) && false == is_numeric( $this->getCheckRoutingNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number should be numeric.' ) );
		}

		if( false == is_null( $this->getCheckRoutingNumber() ) && 9 != strlen( $this->getCheckRoutingNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be 9 digits long.' ) );
		}

		if( false == is_null( $objPaymentDatabase ) ) {
			$objFedAchParticipant = CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( $this->getCheckRoutingNumber(), $objPaymentDatabase );

			if( false == is_null( $this->getCheckRoutingNumber() ) && true == is_null( $objFedAchParticipant ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Valid routing number is required.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCheckAccountNumberEncrypted() {
		$boolIsValid = true;

		if( false == is_null( $this->getCheckAccountNumber() ) && false == is_numeric( $this->getCheckAccountNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number', 'Account number should be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function valIsDirectPay() {
		$boolIsValid = true;

		if( true == is_null( $this->getIsDirectPay() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_direct_pay', 'Is Direct pay is required.' ) );
		}

		return $boolIsValid;
	}

	 public function valSuspendPaymentsUntil() {
		$boolIsValid = true;

		$strTodaysDate = strtotime( date( 'm/d/Y' ) );

		if( false == is_null( $this->getSuspendPaymentsUntil() ) && $strTodaysDate > strtotime( $this->getSuspendPaymentsUntil() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'suspend_payments_until', 'Suspend payments date should be greater than or equal to todays date.' ) );
		}

		return $boolIsValid;
	 }

	public function validate( $strAction, $objDatabase = NULL, $objPaymentDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNameFirst( $objDatabase );
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valCheckBankName();
				$boolIsValid &= $this->valCheckRoutingNumber( $objPaymentDatabase );
				$boolIsValid &= $this->valCheckAccountNumberEncrypted();
				$boolIsValid &= $this->valIsDirectPay();
				$boolIsValid &= $this->valSuspendPaymentsUntil();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createCommissionPayment( $objDatabase ) {

		$objCommissionPayment = new CCommissionPayment();
		$objCommissionPayment->setDefaults();

		$objCommissionPayment->setCommissionRecipientId( $this->getId() );
		$objCommissionPayment->setCommissionBatchId( $this->getId() );
		$objCommissionPayment->setCommissionId( $this->getId() );
		$objCommissionPayment->setPaymentAmount( $this->fetchCommissionAmountDue( $objDatabase ) );

		return $objCommissionPayment;
	}

	public function createCommission() {

		$objCommission = new CCommission();

		$objCommission->setCommissionRecipientId( $this->getId() );
		$objCommission->setCommissionDatetime( date( 'm/d/Y' ) );

		return $objCommission;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchCommissionById( $intCommissionId, $objDatabase ) {
		return CCommissions::createService()->fetchCommissionByCommissionRecipientIdById( $this->getId(), $intCommissionId, $objDatabase );
	}

	public function fetchPaginatedCommissions( $intPageNo, $intPageSize, $intShowDisabledData = 0, $objDatabase ) {
		return CCommissions::createService()->fetchPaginatedCommissionsByCommissionRecipientId( $this->getId(), $intPageNo, $intPageSize, $intShowDisabledData, $objDatabase );
	}

	public function fetchPaginatedCommissionsCount( $intShowDisabledData = 0, $objDatabase ) {
		return CCommissions::createService()->fetchPaginatedCommissionsCountByCommissionRecipientId( $this->getId(), $intShowDisabledData, $objDatabase );
	}

	public function fetchCommissionAmountDue( $objDatabase ) {

		$strSql = 'SELECT
						SUM( commission_amount ) as balance
					FROM
						commissions
					WHERE
						commission_recipient_id::integer = ' . $this->m_intId . '::integer
						AND deleted_on IS NULL
					GROUP BY
						commission_recipient_id';

		$arrmixData = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrmixData ) && true == isset ( $arrmixData[0] ) ) {
			$this->m_fltCommissionAmountDue = $arrmixData[0]['balance'];
		} else {
			$this->m_fltCommissionAmountDue = 0;
		}

		return $this->m_fltCommissionAmountDue;
	}

	public function fetchCommissionPaymentById( $intCommissionPaymentId, $objDatabase ) {
		return CCommissionPayments::createService()->fetchCommissionPaymentsByCommissionRecipientIdById( $this->getId(), $intCommissionPaymentId, $objDatabase );
	}

	public function fetchCommissionPayments( $intLimit, $objDatabase ) {
		return CCommissionPayments::createService()->fetchCommissionPaymentsByCommissionRecipientIdsByLimit( $this->getId(), $intLimit, $objDatabase );
	}

	public function fetchCommissionBatches( $intLimit, $objDatabase ) {
		// TODO: While working on Namespaces changes, noticed this method is not defined.
		return CCommissionBatches::createService()->fetchCommissionBatchesByCommissionRecipientIdByLimit( $this->getId(), $intLimit, $objDatabase );
	}

	public function fetchCommissionRateAssociation( $objDatabase ) {
		// TODO: While working on Namespaces changes, noticed this method is not defined.
		return CCommissionRateAssociations::createService()->fetchCommissionRateAssociationByCommissionRecipientId( $this->getId(), $objDatabase );
	}

	public function fetchUnbatchedCommissionSummaries( $objDatabase ) {
		return CCommissions::createService()->fetchUnbatchedCommissionSummariesByCommissionRecipientId( $this->getId(), $objDatabase );
	}

	public function fetchCommissionSummaryForGraph( $objDatabase ) {
		return CCommissions::createService()->fetchCommissionSummaryForGraphByCommissionRecipientId( $this->getId(), $objDatabase );
	}

	/**
	 * Other Functions
	 *
	 */

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );

		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

}
?>