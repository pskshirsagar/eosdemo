<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsDocumentCategories
 * Do not add any new functions to this class.
 */

class CPsDocumentCategories extends CBasePsDocumentCategories {

	public static function fetchAllPsDocumentCategories( $objAdminDatabase, $strOrderBy = 'order_num' ) {
		return self::fetchPsDocumentCategories( 'SELECT * FROM ps_document_categories ORDER BY ' . $strOrderBy, $objAdminDatabase );
	}

	public static function fetchPsDocumentCategoryById( $intDocumentCategoryId, $objAdminDatabase ) {
		return self::fetchPsDocumentCategory( 'SELECT * FROM ps_document_categories WHERE id = ' . ( int ) $intDocumentCategoryId, $objAdminDatabase );
	}

	public static function fetchPsDocumentCategoriesByIds( $arrintDocumentCategoryIds, $objAdminDatabase ) {
		return self::fetchPsDocumentCategories( 'SELECT * FROM ps_document_categories WHERE id IN( ' . implode( ',', $arrintDocumentCategoryIds ) . ' ) ORDER BY order_num', $objAdminDatabase );
	}

	public static function fetchPsDocumentCategoriesCount( $objAdminDatabase ) {

		$arrintResponse	= fetchData( 'SELECT count(id) as count FROM ps_document_categories', $objAdminDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchAllPaginatedPsDocumentCategories( $intPageNo, $intPageSize, $objAdminDatabase, $arrmixPsDocumentCategoryFilter ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strSql = 'SELECT
						ps.*,
						psd.name AS document_type
					FROM
						ps_document_categories AS ps
						JOIN ps_document_types AS psd ON ( ps.ps_document_type_id = psd.id )
					ORDER BY
						' . $arrmixPsDocumentCategoryFilter['sort_by_field'] . ' ' . $arrmixPsDocumentCategoryFilter['sort_by_type'] . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return self::fetchPsDocumentCategories( $strSql, $objAdminDatabase );
	}

	public static function fetchDocumentCategoriesByName( $arrstrFilteredExplodedSearch, $objAdminDatabase ) {

		$strSql = ' SELECT
						ps.*,
						psd.name AS document_type
					FROM
						ps_document_categories AS ps
						JOIN ps_document_types AS psd ON ( ps.ps_document_type_id = psd.id )
					WHERE
						ps.name ILIKE \'%' . implode( '%\' AND ps.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR psd.name ILIKE \'%' . implode( '%\' AND psd.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
					LIMIT 10';

		return self::fetchPsDocumentCategories( $strSql, $objAdminDatabase );
	}
}
?>