<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingTrainerAssociations
 * Do not add any new functions to this class.
 */
class CTrainingTrainerAssociations extends CBaseTrainingTrainerAssociations {

	public static function fetchTrainingTrainerAssociationsByActionIds( $arrintActionIds, $objDatabase ) {
		if( false == valArr( $arrintActionIds ) ) return NULL;

		$strSql = 'SELECT tta.*,
						eat.id as employee_assessment_type_id
					FROM training_trainer_associations as tta
						LEFT JOIN employee_assessments as ea ON ( tta.employee_assessment_id = ea.id AND ea.deleted_by IS NULL )
						LEFT JOIN employee_assessment_types AS eat ON ea.employee_assessment_type_id = eat.id
						LEFT JOIN employee_assessment_type_levels AS eatl ON eat.employee_assessment_type_level_id = eatl.id
					WHERE tta.action_id IN ( ' . implode( ',', $arrintActionIds ) . ')';

		return self::fetchTrainingTrainerAssociations( $strSql, $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationsByActionResultId( $intActionResultId, $objDatabase ) {

		$strSql = 'SELECT tta.*,
						a.id as action_id,
						t.name,eat.id as employee_assessment_type_id
					FROM training_trainer_associations as tta
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN tags as t ON t.id = a.primary_reference
						LEFT JOIN employee_assessments as ea ON ( tta.employee_assessment_id = ea.id AND ea.deleted_by IS NULL )
						LEFT JOIN employee_assessment_types AS eat ON ea.employee_assessment_type_id = eat.id
						LEFT JOIN employee_assessment_type_levels AS eatl ON eat.employee_assessment_type_level_id = eatl.id
					WHERE t.action_result_id = ' . ( int ) $intActionResultId;

		return self::fetchTrainingTrainerAssociations( $strSql, $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationswithEmployeeDetailsByActionResultId( $intActionResultId, $objDatabase, $strCountryCode = NULL, $boolIsShowPreviousEmployees = false ) {

		$strJoinCondition = $strWhereCondition = '';

		if( false == $boolIsShowPreviousEmployees ) {
			$strWhereCondition = ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		}

		if( false == is_null( $strCountryCode ) ) {
			$strJoinCondition	= 'LEFT JOIN employee_addresses AS ea1 ON ( e.id = ea1.employee_id AND ea1.address_type_id = ' . CAddressType::PRIMARY . ' )';
			$strWhereCondition	.= ' AND ea1.country_code = \'' . $strCountryCode . '\'';
		}

		$strSql = 'SELECT tta.id,
						e.name_first as name_first,
						e.name_last as name_last,
						e.preferred_name as preferred_name,
						u.id as user_id,
						t.name as tag_name,
						ea.current_title as current_title
					FROM training_trainer_associations as tta
						LEFT JOIN actions as a ON tta.action_id = a.id
						LEFT JOIN employee_assessments as ea ON ( ea.id = tta.employee_assessment_id AND ea.deleted_by IS NULL )
						LEFT JOIN users as u ON a.secondary_reference = u.id
						LEFT JOIN employees as e ON u.employee_id = e.id
						' . $strJoinCondition . '
						JOIN tags as t ON t.id = a.primary_reference and a.action_result_id = ' . ( int ) $intActionResultId . '
					WHERE tta.is_published = 1
						 ' . $strWhereCondition . '
					ORDER BY
						e.preferred_name';

		return self::fetchTrainingTrainerAssociations( $strSql, $objDatabase );
	}

	public static function fetchAllTrainingTrainerActionsCount( $intActionResultId, $objDatabase ) {

		$strSql = 'SELECT
						count( tta.id )
					FROM training_trainer_associations as tta
						LEFT JOIN actions as a ON a.id = tta.action_id
						LEFT JOIN users as u ON a.secondary_reference = u.id
						LEFT JOIN employees as e ON u.employee_id = e.id
					WHERE
						tta.is_published = 1
						AND a.action_result_id = ' . ( int ) $intActionResultId;

		if( CActionResult::EXTERNAL_TRAINER != $intActionResultId ) {
			$strSql .= ' AND e.employee_status_type_id= ' . ( int ) CEmployeeStatusType::CURRENT;
		}

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if ( true == isset( $arrintResponse[0]['count'] ) )
			return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchPaginatedTrainingTrainerAssociationsByActionResultIdFilter( $objTrainersFilter, $intActionResultId, $objDatabase ) {

		$intOffset = ( 0 < $objTrainersFilter->getPageNo() ) ? $objTrainersFilter->getPageSize() * ( $objTrainersFilter->getPageNo() - 1 ) : 0;
		$intLimit = ( int ) $objTrainersFilter->getPageSize();
		$strOrderClause = ' ORDER BY tta.updated_on DESC ';
		$strOrder = '';

		if( false == is_null( $objTrainersFilter ) && false == is_null( $objTrainersFilter->getOrderByField() ) && false == is_null( $objTrainersFilter->getOrderByType() ) ) {
			$strOrder = ( 1 == $objTrainersFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';
			$strOrderClause = ' ORDER BY ' . $objTrainersFilter->getOrderByField() . '' . $strOrder;
		}

		$strSql = 'SELECT tta.*,
							e.id as employee_id,
							e.name_first,
							e.name_last,
							e.preferred_name,
							u.id as user_id,
							d.name as department_name,
							ds.name as designation_name,
							t.name as tag_name,
							a.action_result_id
					FROM training_trainer_associations as tta
							LEFT JOIN actions a ON ( a.id = tta.action_id )
							LEFT JOIN users as u ON a.secondary_reference = u.id
							LEFT JOIN employees as e ON u.employee_id = e.id
							LEFT JOIN departments as d ON e.department_id = d.id
							LEFT JOIN designations as ds ON e.designation_id = ds.id
							LEFT JOIN tags as t ON t.id = a.primary_reference
					WHERE e.employee_status_type_id= ' . ( int ) CEmployeeStatusType::CURRENT . ' AND tta.is_published = 1 and a.action_result_id = ' . ( int ) $intActionResultId . ' ' . $strOrderClause . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchTrainingTrainerAssociations( $strSql, $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationwithUserIdById( $intId, $objDatabase ) {

		$strSql = 'SELECT u.id as user_id
					FROM training_trainer_associations AS tta
						 JOIN actions AS a ON tta.action_id = a.id
						 JOIN users AS u ON u.id = a.secondary_reference
					WHERE tta.id = ' . ( int ) $intId;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['user_id'] ) )
			return $arrintResponse[0]['user_id'];

		return NULL;
	}

	public static function fetchTrainerInformationByActionResultIdById( $intActionResultId, $intId, $objDatabase ) {

		$strSql = 'SELECT tta.id,
							u.id as user_id,
							e.id as employee_id,
							e.name_first as name_first,
							e.name_last as name_last,
							d.name as department_name,
							ds.name as designation_name,
							t.name as tag_name,
							t.action_result_id as action_result_id,
							ea.current_title as current_title
					FROM training_trainer_associations as tta
							LEFT JOIN actions as a ON tta.action_id = a.id
							LEFT JOIN users as u ON a.secondary_reference = u.id
							LEFT JOIN employees as e ON u.employee_id = e.id
							LEFT JOIN departments as d ON e.department_id = d.id
							LEFT JOIN designations as ds ON e.designation_id = ds.id
							LEFT JOIN tags as t ON ( t.id = a.primary_reference and t.action_result_id = ' . ( int ) $intActionResultId . ' )
							LEFT JOIN employee_assessments as ea ON ( ea.id = tta.employee_assessment_id AND ea.deleted_by IS NULL )
					WHERE tta.is_published = 1 and tta.id = ' . ( int ) $intId;

		return self::fetchTrainingTrainerAssociation( $strSql, $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationwithDetailsById( $intId, $objDatabase ) {

		$strSql = 'SELECT
						tta.*,
						e.id as employee_id,
						e.name_first as name_first,
						e.name_last as name_last,
						e.preferred_name as preferred_name,
						a.action_result_id AS action_result_id
					FROM
						training_trainer_associations tta
						JOIN actions a ON a.id = tta.action_id
						JOIN users u ON u.id = a.secondary_reference
						JOIN employees e ON e.id = u.employee_id
					WHERE
						tta.id = ' . ( int ) $intId . '
					UNION
					SELECT
						tta.*,
						ea.id AS employee_id,
						ea.name_first AS name_first,
						ea.name_last AS name_last,
						( ea.name_first || \' \' || ea.name_last) AS preferred_name,
						a.action_result_id AS action_result_id
					FROM
						training_trainer_associations tta
						JOIN actions a ON a.id = tta.action_id
						LEFT JOIN employee_applications ea ON ea.id = a.secondary_reference
					WHERE
						tta.id = ' . ( int ) $intId . '
					AND a.action_result_id = ' . CActionResult::EXTERNAL_TRAINER;

		return self::fetchTrainingTrainerAssociation( $strSql, $objDatabase );
	}

	public static function fetchQuickSearchTrainer( $intActionResultId, $arrstrFilteredExplodedSearch, $objDatabase ) {

		if( CActionResult::EXTERNAL_TRAINER == $intActionResultId ) {

			$strSql = 'SELECT
							tta.*,
							ea.id as employee_application_id,
							ea.name_first,
							ea.name_last,
							ea.email_address,
							ea.cell_number
						FROM
							training_trainer_associations tta
							LEFT JOIN actions a ON tta.action_id = a.id
							LEFT JOIN employee_applications ea ON a.secondary_reference = ea.id
							LEFT JOIN tags t ON a.primary_reference = t.id
						WHERE
							t.action_result_id = ' . ( int ) $intActionResultId . ' AND tta.is_published = 1
							AND ea.name_first ILIKE E\'%' . implode( '%\' AND ea.name_last ILIKE E\'%', $arrstrFilteredExplodedSearch ) . '%\'
							LIMIT 10';
		} else {
			$strSql = ' SELECT
							tta.*,
							e.id as employee_id,
							e.name_first,
							e.name_last,
							e.preferred_name,
							u.id as user_id,
							ea.current_title,
							d.name as department_name,
							ds.name as designation_name,
							t.name as tag_name,
							tta.is_published
						FROM training_trainer_associations as tta
							LEFT JOIN actions as a ON a.id = tta.action_id
							LEFT JOIN employee_assessments as ea ON ( ea.id = tta.employee_assessment_id AND ea.deleted_by IS NULL )
							LEFT JOIN users as u ON a.secondary_reference = u.id
							LEFT JOIN employees as e ON u.employee_id = e.id
							LEFT JOIN departments as d ON e.department_id = d.id
							LEFT JOIN designations as ds ON e.designation_id = ds.id
							LEFT JOIN tags as t ON t.id = a.primary_reference
						WHERE e.employee_status_type_id= ' . CEmployeeStatusType::CURRENT . ' AND tta.is_published = 1 AND a.action_result_id = ' . ( int ) $intActionResultId . '
							AND ( ea.current_title ILIKE E\'%' . implode( '%', $arrstrFilteredExplodedSearch ) . '%\'
							OR e.preferred_name ILIKE E\'%' . implode( '%', $arrstrFilteredExplodedSearch ) . '%\'
							OR e.name_full ILIKE E\'%' . implode( '%', $arrstrFilteredExplodedSearch ) . '%\')';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationsByExternalTrainerActionResultId( $intActionResultId, $objTrainersFilter, $objDatabase ) {

		$intOffset = ( 0 < $objTrainersFilter->getPageNo() ) ? $objTrainersFilter->getPageSize() * ( $objTrainersFilter->getPageNo() - 1 ) : 0;
		$intLimit = ( int ) $objTrainersFilter->getPageSize();
		$strOrderClause = ' ORDER BY tta.updated_on DESC ';
		$strOrder = '';

		if( false == is_null( $objTrainersFilter ) && false == is_null( $objTrainersFilter->getOrderByField() ) && false == is_null( $objTrainersFilter->getOrderByType() ) ) {
			$strOrder = ( 1 == $objTrainersFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';
			$strOrderClause = ' ORDER BY ' . $objTrainersFilter->getOrderByField() . '' . $strOrder;
		}

		$strSql = 'SELECT
 						tta.*,
						ea.id as employee_application_id,
 						ea.name_first,
 						ea.name_last,
 						ea.email_address,
 						ea.cell_number,
						a.action_result_id
 					FROM
 						training_trainer_associations tta
 						JOIN actions a ON tta.action_id = a.id
 						JOIN employee_applications ea ON a.secondary_reference = ea.id
 					WHERE
 						a.action_result_id = ' . ( int ) $intActionResultId . ' AND tta.is_published = 1 ' . $strOrderClause . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchTrainingTrainerAssociations( $strSql, $objDatabase );
	}

	public static function fetchAllExternalTrainers( $objDatabase, $strCountryCode = NULL ) {

		$strSql = 'SELECT
						tta.*,
						ea.name_first,
						ea.name_last,
						ea.email_address,
						ea.cell_number
					FROM
						training_trainer_associations tta
						JOIN actions a ON tta.action_id = a.id
						JOIN employee_applications ea ON a.secondary_reference = ea.id
						JOIN tags t ON a.primary_reference = t.id
					WHERE
						t.action_result_id = ' . ( int ) CActionResult::EXTERNAL_TRAINER . '
						AND tta.is_published = 1';

		if( true == valStr( $strCountryCode ) )
			$strSql .= ' AND ea.country_code = \'' . $strCountryCode . '\'';

		return self::fetchTrainingTrainerAssociations( $strSql, $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationsInterestedTrainers( $objDatabase ) {

		$strSql = 'SELECT
						u.*
					FROM
						training_trainer_associations AS tta
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN users AS u ON a.secondary_reference = u.id
					WHERE
						tta.is_published = 1
						AND a.action_result_id IN ( ' . ( int ) CActionResult::TRAINER . ' , ' . ( int ) CActionResult::INTERESTED_TRAINER . ' )';

		return self::fetchTrainingTrainerAssociations( $strSql, $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationByActionId( $intActionId, $objDatabase ) {
		return self::fetchTrainingTrainerAssociation( sprintf( 'SELECT * FROM training_trainer_associations WHERE action_id = %d', ( int ) $intActionId ), $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {

		$strSql = ' SELECT
						tta.*
					FROM
						training_trainer_associations AS tta
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN employee_applications AS ea ON ea.id = a.secondary_reference
					WHERE
						a.action_result_id = ' . ( int ) CActionResult::EXTERNAL_TRAINER . ' AND ea.id = ' . ( int ) $intEmployeeApplicationId;

		return self::fetchTrainingTrainerAssociation( $strSql, $objDatabase );
	}

	public static function fetchAllTrainerReports( $objDatabase ) {

		$strSql = 'SELECT
						ar.name	AS trainer_type,
						CASE
							WHEN ( a.action_result_id = ' . ( int ) CActionResult::EXTERNAL_TRAINER . ' ) THEN ea.name_first || \' \' || ea.name_last
							ELSE e.name_first || \' \' || e.name_last
						END AS NAME,
						CASE
							WHEN ( a.action_result_id = ' . ( int ) CActionResult::EXTERNAL_TRAINER . ' ) THEN ea.email_address
							ELSE e.email_address
						END AS email_address,
						ed.name	AS designation,
						d.name	AS department,
						CASE
							WHEN ( a.action_result_id = ' . ( int ) CActionResult::INTERESTED_TRAINER . ' OR a.action_result_id = ' . ( int ) CActionResult::TRAINER . ' ) THEN t.name
						END AS tag_name
					FROM
						training_trainer_associations tta
						LEFT JOIN actions a ON tta.action_id = a.id
						LEFT JOIN action_results ar ON ( a.action_result_id = ar.id )
						LEFT JOIN employee_applications ea ON a.secondary_reference = ea.id
						LEFT JOIN users AS u ON a.secondary_reference = u.id
						LEFT JOIN employees AS e ON u.employee_id = e.id
						LEFT JOIN designations AS ed ON e.designation_id = ed.id
						LEFT JOIN departments AS d ON e.department_id = d.id
						LEFT JOIN tags AS t ON t.id = a.primary_reference
					WHERE
						tta.is_published = 1
						AND a.action_result_id IN ( ' . ( int ) CActionResult::EXTERNAL_TRAINER . ' , ' . ( int ) CActionResult::INTERESTED_TRAINER . ', ' . ( int ) CActionResult::TRAINER . ' )
						AND ( e.employee_status_type_id = 1 OR e.employee_status_type_id IS NULL )
					ORDER BY
							ar.id ASC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainingTrainerAssociationsByIds( $arrintTrainingTrainerAssociationIds, $objDatabase ) {
		return self::fetchTrainingTrainerAssociations( sprintf( 'SELECT * FROM %s WHERE id IN( %s )', 'training_trainer_associations', implode( ',', $arrintTrainingTrainerAssociationIds ) ), $objDatabase );
	}

	public static function fetchTrainingUnAssociatedTrainer( $strCountryCode, $objDatabase ) {
		$strJoinCondition = $strWhereCondition = '';

		if( false == is_null( $strCountryCode ) ) {
			$strJoinCondition	= 'JOIN employee_addresses AS ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )';
			$strWhereCondition	.= ' AND ea.country_code = \'' . $strCountryCode . '\'';
		}
		$strSql = 'SELECT 
						e.name_first as name_first,
						e.name_last as name_last,
						e.preferred_name as preferred_name,
						u.id as user_id
					FROM users u
						 JOIN employees e ON e.id = u.employee_id
						 ' . $strJoinCondition . '
					WHERE u.id NOT IN (
									SELECT u.id
									FROM training_trainer_associations AS tta
										 LEFT JOIN actions AS a ON a.id = tta.action_id
										 LEFT JOIN users AS u ON a.secondary_reference = u.id
										 ' . $strJoinCondition . '
									WHERE tta.is_published = 1 AND
										a.action_result_id IN (
										' . ( int ) CActionResult::TRAINER . ',
										' . ( int ) CActionResult::INTERESTED_TRAINER . '
										) )
					AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					' . $strWhereCondition . '
				ORDER BY
				e.preferred_name';

		return self::fetchTrainingTrainerAssociations( $strSql, $objDatabase );
	}

}
?>