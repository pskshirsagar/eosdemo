<?php

class CPackageProductDiscount extends CBasePackageProductDiscount {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPackageProductRelationshipId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDependentProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSetupDiscountPercentage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRecurringDiscountPercentage() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function createClone( $intUserId, $intPackageProductsRelationshipId = NULL ) {
		$objPackageProductDiscount = clone $this;

		$objPackageProductDiscount->setId( NULL );
		$objPackageProductDiscount->setPackageProductRelationshipId( $intPackageProductsRelationshipId );
		$objPackageProductDiscount->setCreatedBy( $intUserId );
		$objPackageProductDiscount->setCreatedOn( 'NOW()' );
		$objPackageProductDiscount->setUpdatedBy( $intUserId );
		$objPackageProductDiscount->setUpdatedOn( 'NOW()' );

		return $objPackageProductDiscount;
	}

}
?>