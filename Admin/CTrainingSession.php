<?php

use Psi\Libraries\UtilDates\CDates;

class CTrainingSession extends CBaseTrainingSession {

	const MIN_SESSION_MINUTE    = 30;

	const AT_DESK               = 1;
	const GTM_CALL              = 2;
	const HANGOUT_CALL          = 3;

	protected $m_strNameFull;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPreferredName;
	protected $m_strTrainingName;
	protected $m_strActionResultName;
	protected $m_strParentTrainingName;
	protected $m_strGoals;
	protected $m_strActualHour;
	protected $m_strHour;
	protected $m_strMinute;
	protected $m_strOfficeRoomName;

	protected $m_intUserId;
	protected $m_intActionResultId;
	protected $m_intPendingAttendeeCount;

	/**
	 * Get Functions
	 *
	 */

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getTrainingName() {
		return $this->m_strTrainingName;
	}

	public function getParentTrainingName() {
		return $this->m_strParentTrainingName;
	}

	public function getActionResultName() {
		return $this->m_strActionResultName;
	}

	public function getGoals() {
		return $this->m_strGoals;
	}

	public function getActionResultId() {
		return $this->m_intActionResultId;
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function getPendingAttendeeCount() {
		return $this->m_intPendingAttendeeCount;
	}

	public function getActualHour() {
		return $this->m_strActualHour;
	}

	public function getHour() {
		return $this->m_strHour;
	}

	public function getMinute() {
		return $this->m_strMinute;
	}

	public function getOfficeRoomName() {
		return $this->m_strOfficeRoomName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name_full'] ) )					$this->setNameFull( $arrmixValues['name_full'] );
		if( true == isset( $arrmixValues['name_first'] ) )					$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) )					$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['preferred_name'] ) )				$this->setPreferredName( $arrmixValues['preferred_name'] );
		if( true == isset( $arrmixValues['user_id'] ) ) 					$this->setUserId( $arrmixValues['user_id'] );
		if( true == isset( $arrmixValues['training_name'] ) ) 				$this->setTrainingName( $arrmixValues['training_name'] );
		if( true == isset( $arrmixValues['parent_training_name'] ) )		$this->setParentTrainingName( $arrmixValues['parent_training_name'] );
		if( true == isset( $arrmixValues['action_result_name'] ) )			$this->setActionResultName( $arrmixValues['action_result_name'] );
		if( true == isset( $arrmixValues['goals'] ) ) 						$this->setGoals( $arrmixValues['goals'] );
		if( true == isset( $arrmixValues['action_result_id'] ) )			$this->setActionResultId( $arrmixValues['action_result_id'] );
		if( true == isset( $arrmixValues['pending_attendee_count'] ) )		$this->setPendingAttendeeCount( $arrmixValues['pending_attendee_count'] );
		if( true == isset( $arrmixValues['actual_hour'] ) )					$this->setActualHour( $arrmixValues['actual_hour'] );
		if( true == isset( $arrmixValues['hour'] ) )						$this->setHour( $arrmixValues['hour'] );
		if( true == isset( $arrmixValues['minute'] ) )						$this->setMinute( $arrmixValues['minute'] );
		if( true == isset( $arrmixValues['office_room_name'] ) )			$this->setOfficeRoomName( $arrmixValues['office_room_name'] );

		return;
	}

	public function setActualHour( $strActualHour ) {
		$this->m_strActualHour = $strActualHour;
	}

	public function setHour( $strHour ) {
		$this->m_strHour = $strHour;
	}

	public function setMinute( $strMinute ) {
		$this->m_strMinute = $strMinute;
	}

	public function setTrainerType( $strTrainerType ) {
		$this->m_strTrainerType = $strTrainerType;
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setTrainingName( $strTrainingName ) {
		$this->m_strTrainingName = $strTrainingName;
	}

	public function setParentTrainingName( $strParentTrainingName ) {
		$this->m_strParentTrainingName = $strParentTrainingName;
	}

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setActionResultName( $strActionResultName ) {
		$this->m_strActionResultName = $strActionResultName;
	}

	public function setGoals( $strGoals ) {
		$this->m_strGoals = $strGoals;
	}

	public function setActionResultId( $intActionResultId ) {
		$this->m_intActionResultId = $intActionResultId;
	}

	public function setPreferredName( $strPreferredName ) {
		$this->m_strPreferredName = $strPreferredName;
	}

	public function setPendingAttendeeCount( $intPendingAttendeeCount ) {
		$this->m_intPendingAttendeeCount = $intPendingAttendeeCount;
	}

	public function setOfficeRoomName( $strOfficeRoomName ) {
		$this->m_strOfficeRoomName = $strOfficeRoomName;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valTrainingId() {
		$boolIsValid = true;
		if( true == empty( $this->m_intTrainingId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' Training name is required. ' ) );

		}
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		if( true == empty( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', ' Session name is required. ' ) );
		}
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		if( true == empty( $this->m_strCountryCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', ' Please select Country. ' ) );
		}
		return $boolIsValid;
	}

	public function valScheduledEndTime() {
		$boolIsValid = true;
		if( false == empty( $this->m_strScheduledStartTime ) ) {
			if( $this->m_strScheduledEndTime < $this->m_strScheduledStartTime ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Schedule', ' Please select proper end time. ' ) );
			}
		}
		if( false == empty( $this->m_intOfficeRoomId ) && false == valStr( $this->m_strScheduledStartTime ) && false == valStr( $this->m_strScheduledEndTime ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Scheduled Date', ' Please enter scheduled date. ' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valActualEndTime() {
		$boolIsValid = true;

		if( true == valStr( $this->m_strActualStartTime ) && true == valStr( $this->m_strActualEndTime ) ) {
			if( strtotime( $this->m_strActualStartTime ) >= strtotime( $this->m_strActualEndTime ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_end_time', ' End time should be greater than the start time. ' ) );
				return $boolIsValid;
			}

			if( strtotime( $this->m_strActualEndTime ) < strtotime( $this->m_strActualStartTime ) + 30 * 60 ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'actual_end_time', ' You can not end session before 30 minutes. ' ) );
				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valSessionDuration() {
		$boolIsValid = true;
		if( false == empty( $this->m_strScheduledStartTime ) && false == empty( $this->m_strScheduledEndTime ) ) {
			$arrmixDateDifference = CDates::createService()->getDateDifference( $this->m_strScheduledStartTime, $this->m_strScheduledEndTime );
			if( $arrmixDateDifference['minutes_total'] < CTrainingSession::MIN_SESSION_MINUTE ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Schedule', ' Session can not be added for less than 30 minutes. ' ) );
			}
		}
		return $boolIsValid;
	}

	public function valAvailableSeats( $objDatabase ) {
		$boolIsValid = true;
		if( false == $this->m_intIsPrivate ) {
			$arrmixTrainingSessionGroups = CTrainingSessionGroups::fetchTrainingSessionGroupsByTrainingSessionId( $this->getId(), $objDatabase );
			if( true == valArr( $arrmixTrainingSessionGroups ) && true == empty( $this->m_intAvailableSeats ) && CTraining::INDUCTION != $this->m_intTrainingId ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Available Seats', ' Please enter available seats. ' ) );
			} elseif( ( 10000 <= $this->m_intAvailableSeats || false == is_int( $this->m_intAvailableSeats ) ) && false == empty( $this->m_intAvailableSeats ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Available Seats', ' Please enter valid available seats. ' ) );
			}
		}
		return $boolIsValid;
	}

	public function valOfficeRoomId( $objDatabase ) {

		$boolValid = true;
		if( false == empty( $this->m_strScheduledStartTime ) && false == empty( $this->m_strScheduledEndTime ) ) {

			$arrintOfficeRoomIds = array( CTrainingSession::AT_DESK, CTrainingSession::GTM_CALL, CTrainingSession::HANGOUT_CALL );
			if( false == is_null( $this->m_intOfficeRoomId ) && false == in_array( $this->m_intOfficeRoomId, $arrintOfficeRoomIds ) ) {
				$strWhereCondition = 'WHERE ( scheduled_start_time BETWEEN ( \'' . $this->m_strScheduledStartTime . '\'::TIMESTAMP - INTERVAL \'1 minute\' ) AND ( \'' . $this->m_strScheduledEndTime . '\'::TIMESTAMP - INTERVAL \'1 minute\' ) OR scheduled_end_time BETWEEN ( \'' . $this->m_strScheduledStartTime . '\'::TIMESTAMP + INTERVAL \'1 minute\' ) AND ( \'' . $this->m_strScheduledEndTime . '\'::TIMESTAMP + INTERVAL \'1 minute\' ) ) AND office_room_id = ' . $this->m_intOfficeRoomId . ' AND deleted_by IS NULL';
				if( true == $this->getId() ) {
					$strWhereCondition .= ' AND id <>' . ( int ) $this->getId();
				}

				$intTrainingSessions = CTrainingSessions::fetchTrainingSessionCount( $strWhereCondition, $objDatabase );

				if( 0 < $intTrainingSessions ) {
					$boolValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Training room', ' Selected training location is already booked. ' ) );
				}
			}
		}
		return $boolValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valTrainingId();
				$boolIsValid &= $this->valCountryCode();
				$boolIsValid &= $this->valSessionDuration();
				$boolIsValid &= $this->valScheduledEndTime();
				$boolIsValid &= $this->valActualEndTime();
				$boolIsValid &= $this->valAvailableSeats( $objDatabase );
				$boolIsValid &= $this->valOfficeRoomId( $objDatabase );
				break;

			case VALIDATE_DELETE:
		   		break;

			default:
				// default case
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrenUserId, $objDataBase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intCurrenUserId );
		$this->setDeletedOn( 'NOW()' );
		$this->setUpdatedBy( $intCurrenUserId );
		$this->setUpdatedOn( 'NOW()' );

		if( $this->update( $intCurrenUserId, $objDataBase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

	public function insertTrainingSessionTrainer( $objUser, $objAdminDatabase ) {

			$boolValid = true;

			$objAction = new CAction();

			$objAction->setActionTypeId( CActionType::TAG );
			$objAction->setPrimaryReference( CTag::BEGINNER_LEVEL );
			$objAction->setSecondaryReference( $this->getTrainingTrainerAssociationId() );
			$objAction->setActionResultId( CActionResult::INTERESTED_TRAINER );
			$objAction->setActionDatetime( 'NOW()' );

			if( true == is_null( $objAction->getSecondaryReference() ) ) {
				$objAction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interested_trainer', 'Trainer name is required.' ) );
				$boolValid &= false;
			} else {
				$objEmployee = CEmployees::fetchEmployeeByUserId( $objAction->getSecondaryReference(), $objAdminDatabase );

				if( false == valObj( $objEmployee, 'CEmployee' ) ) {
					$objAction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interested_trainer', 'Trainer not found.' ) );
					$boolValid &= false;
				} elseif( CEmployeeStatusType::CURRENT != $objEmployee->getEmployeeStatusTypeId() ) {
					$objAction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interested_trainer', 'Trainer not found.' ) );
					$boolValid &= false;
				}

				$arrobjTrainingTrainerAssociations = CTrainingTrainerAssociations::fetchTrainingTrainerAssociationsInterestedTrainers( $objAdminDatabase );

				if( true == valArr( $arrobjTrainingTrainerAssociations ) ) {
					if( true == array_key_exists( $objAction->getSecondaryReference(), $arrobjTrainingTrainerAssociations ) ) {
						$objAction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interested_trainer', 'Trainer already exists.' ) );
						$boolValid &= false;
					}
				}
			}

			if( true == is_null( $objAction->getPrimaryReference() ) ) {
				$objAction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interested_trainer', 'Level is required.' ) );
				$boolValid &= false;
			} else {
				$objTag = CTags::fetchTagById( $objAction->getPrimaryReference(), $objAdminDatabase );

				if( false == valObj( $objTag, 'CTag' ) ) {
					$objAction->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'interested_trainer', 'Level not found.' ) );
					$boolValid &= false;
				}
			}

			switch( NULL ) {
				default:
					if( false == $boolValid || false == $objAction->insertOrUpdate( $objUser->getId(), $objAdminDatabase ) ) {
						break;
					}

					if( true == empty( $intActionId ) ) {

						$objTrainingTrainerAssociation = $objAction->createTrainingTrainerAssociation();
						$objTrainingTrainerAssociation->setIsPublished( 1 );

						if( false == $objTrainingTrainerAssociation->insert( $objUser->getId(), $objAdminDatabase ) ) {
							$this->addSessionErrorMsg( 'Unable to add trainer.' );
							break;
						}
					} else {
						$objTrainingTrainerAssociation = CTrainingTrainerAssociations::fetchTrainingTrainerAssociationByActionId( $objAction->getId(), $objAdminDatabase );

						if( false == valObj( $objTrainingTrainerAssociation, 'CTrainingTrainerAssociation' ) ) {
							$this->displayMessage( 'Error: ', 'Failed to load training trainer association. Press ok to get redirected.', '/?module=trainers-new' );
						}

						$objTrainingTrainerAssociation->update( $objUser->getId(), $objAdminDatabase );

						$this->addSessionSuccessMsg( 'Interested trainer updated successfully.' );
					}

					$this->setTrainingTrainerAssociationId( $objTrainingTrainerAssociation->getId() );
					return true;
			}

			return false;
	}

}
?>