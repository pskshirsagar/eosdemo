<?php

use Psi\Eos\Admin\CCommissionRecipients;
use Psi\Eos\Admin\CFedAchParticipants;

class CCommissionPayment extends CBaseCommissionPayment {

	public function valCommissionRecipientId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCommissionRecipientId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_recipient_id', 'Commission recipient is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCommissionBatchId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCommissionBatchId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_batch_id', 'Commission batch is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPaymentDatetime() {
		$boolIsValid = true;
		$this->setPaymentDatetime( date( 'Y-m-d', strtotime( $this->getPaymentDatetime() ) ) );
		if( true == is_null( $this->getPaymentDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_datetime', 'Date is required.' ) );
		} elseif( false == CValidation::validateDate( $this->getPaymentDatetime(), false ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_datetime', 'Valid date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPaymentAmount() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getPaymentAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', 'Valid payment amount is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getCheckDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_date', 'Check date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckNumber() {
		$boolIsValid = true;

		if( true == is_null( $this->getCheckNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_number', 'Check number is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckPayableTo() {
		$boolIsValid = true;

		if( true == is_null( $this->getCheckPayableTo() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_payable_to', 'Check payable to is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckBankName() {
		$boolIsValid = true;

		if( true == is_null( $this->getCheckBankName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_bank_name', 'Bank name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckAccountTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCheckAccountTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_type_id', 'Check account type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCheckRoutingNumber( $objPaymentDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getCheckRoutingNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Bank routing number is required for payment.' ) );
		}

		if( 0 !== preg_match( '/[^0-9]/', $this->getCheckRoutingNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be a number.' ) );
			return false;
		}

		if( 9 != strlen( $this->getCheckRoutingNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number must be 9 digits long.' ) );
			return false;
		}

		if( false == is_null( $objPaymentDatabase ) ) {

			$objFedAchParticipant = CFedAchParticipants::createService()->fetchFedAchParticipantByRoutingNumber( $this->getCheckRoutingNumber(), $objPaymentDatabase );

			if( true == is_null( $objFedAchParticipant ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_routing_number', 'Routing number was not found.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCheckAccountNumberEncrypted() {
		$boolIsValid = true;

		if( true == is_null( $this->getCheckAccountNumberEncrypted() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', 'Check account number is required.' ) );
		} // elseif( false == is_numeric( $this->getCheckAccountNumberEncrypted() ) ) {
			// $boolIsValid = false;
			// $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'check_account_number_encrypted', 'Valid check account number is required.' ) );
		// }

		return $boolIsValid;
	}

	public function getCheckAccountNumberMasked() {
		$strCheckAccountNumber = $this->m_strCheckAccountNumberEncrypted;
		$intStringLength = strlen( $strCheckAccountNumber );
		$strLastFour = \Psi\CStringService::singleton()->substr( $strCheckAccountNumber, -4 );
		return \Psi\CStringService::singleton()->str_pad( $strLastFour, $intStringLength, 'X', STR_PAD_LEFT );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPaymentDatetime();
				$boolIsValid &= $this->valPaymentAmount();

				// $boolIsValid &= $this->valCheckDate();

				// $boolIsValid &= $this->valCheckNumber();
				// $boolIsValid &= $this->valCheckPayableTo();
				// $boolIsValid &= $this->valCheckBankName();
				// $boolIsValid &= $this->valCheckAccountTypeId();
				// $boolIsValid &= $this->valCheckRoutingNumber( $objDatabase );
				// $boolIsValid &= $this->valCheckAccountNumberEncrypted();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchCommissionRecipient( $objDatabase ) {
		return CCommissionRecipients::createService()->fetchCommissionRecipientById( $this->getCommissionRecipientId(), $objDatabase );
	}

	/**
	 * Create Functions
	 */

	public function createCommisson() {

		$objCommission = new CCommission();
		$objCommission->setCommissionPaymentId( $this->getId() );
		$objCommission->setChargeCodeId( CChargeCode::PAYMENT_RECEIVED );
		$objCommission->setCommissionRecipientId( $this->getCommissionRecipientId() );
		$objCommission->setCommissionBatchId( $this->getCommissionBatchId() );
		$objCommission->setCommissionDatetime( $this->getPaymentDatetime() );
		$objCommission->setCommissionAmount( $this->getPaymentAmount() );
		$objCommission->setCommissionMemo( $this->getPaymentMemo() );

		return $objCommission;
	}

	/**
	 * Other Functions
	 */

	public function getBaseMonthTimestamp() {
		return strtotime( date( 'm', strtotime( $this->getPaymentDatetime() ) ) . '/1/' . date( 'Y', strtotime( $this->getPaymentDatetime() ) ) );
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );
		$this->setUpdatedBy( $intUserId );
		$this->setUpdatedOn( ' NOW() ' );

		if( $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}
	}

}

?>