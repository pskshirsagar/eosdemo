<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeCertifications
 * Do not add any new functions to this class.
 */

class CEmployeeCertifications extends CBaseEmployeeCertifications {

	public static function fetchAllEmployeeCertificationsCount( $objDatabase, $arrmixFilteredExplodedSearch, $objEmployeeCertificationsFilter ) {

		$strSearchCondition = '';
		$strFromSearchCondition = '';

		if( true == valArr( $arrmixFilteredExplodedSearch ) ) {

			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) && is_numeric( $arrmixFilteredExplodedSearch[0] ) ) {
				$strSearchCondition = 'AND to_char( e.employee_number, \'99999\' ) LIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' ';
			} elseif( 1 < \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) ) {
				if( true == array_key_exists( 2, $arrmixFilteredExplodedSearch ) ) {
					$strSearchCondition = 'AND ( ( ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' AND e.name_last ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\' ) OR e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\')	OR ( ( ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' AND e.name_last ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\') OR e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\') ) OR d.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR de.name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\'	OR de.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' )';
				} else {
					$strSearchCondition = 'AND ( ( ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' ) OR e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\')	OR ( ( ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' ) OR e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\') ) OR d.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR de.name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\'	OR de.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' )';
				}
			} else {
				$strSearchCondition = 'AND ( e.name_first ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.name_last ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.preferred_name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR d.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR de.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\')';
			}
		}

		if( true == valObj( $objEmployeeCertificationsFilter, 'CEmployeeCertificationsFilter' ) ) {
			if( true == valArr( $objEmployeeCertificationsFilter->getDesignationIds() ) ) {
				$strSearchCondition .= ' AND e.designation_id IN ( ' . implode( ',', $objEmployeeCertificationsFilter->getDesignationIds() ) . ' )';
			}
			if( true == valArr( $objEmployeeCertificationsFilter->getDepartmentIds() ) ) {
				$strSearchCondition .= ' AND e.department_id IN ( ' . implode( ',', $objEmployeeCertificationsFilter->getDepartmentIds() ) . ' )';
			}
			if( true == valArr( $objEmployeeCertificationsFilter->getCertificationIds() ) ) {
				$strSearchCondition .= ' AND c.id IN ( ' . implode( ',', $objEmployeeCertificationsFilter->getCertificationIds() ) . ' )';
			}
			if( true == $objEmployeeCertificationsFilter->getIsApproved() ) {
				$strSearchCondition .= ' AND ps.approved_by IS NOT NULL';
				$strFromSearchCondition .= 'LEFT JOIN ps_documents AS ps ON ( ec.ps_document_id = ps.id )';
			}
			if( true == valArr( $objEmployeeCertificationsFilter->getAddEmployeeIds() ) ) {
				$strSearchCondition .= ' AND t.add_employee_id IN ( ' . implode( ',', $objEmployeeCertificationsFilter->getAddEmployeeIds() ) . ' )';
				$strFromSearchCondition .= ' LEFT JOIN team_employees AS te ON ( e.id = te.employee_id )
											 LEFT JOIN teams AS t ON ( t.id = te.team_id )';
			}
		}

		$strSql = 'SELECT
						COUNT ( DISTINCT( e.id ) ) AS certification_count
					FROM
						employee_certifications ec
						LEFT JOIN certifications AS c ON ( ec.certification_id = c.id )
						LEFT JOIN employees e ON ( ec.employee_id = e.id )
						LEFT JOIN departments d ON( e.department_id = d.id )
						LEFT JOIN designations AS de ON ( e.designation_id = de.id )'
						. $strFromSearchCondition . '
					WHERE
						ec.is_certified = TRUE 
						AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::NO_SHOW . ' )
						AND c.deleted_by IS NULL
						AND ( e.date_terminated IS NULL
						OR e.date_terminated > NOW ( ) )
						' . $strSearchCondition . ' ';
		$arrintEmployeesCount = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintEmployeesCount ) ) {
			return $arrintEmployeesCount[0];
		} else {
			return NULL;
		}
	}

	public static function fetchPaginatedEmployeeCertifications( $intPageNo, $intPageSize, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL, $arrmixFilteredExplodedSearch, $objEmployeeCertificationsFilter ) {

		$strSearchCondition = '';
		$strFromSearchCondition = '';

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		if( true == valArr( $arrmixFilteredExplodedSearch ) ) {

			if( 1 == \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) && is_numeric( $arrmixFilteredExplodedSearch[0] ) ) {
				$strSearchCondition = 'AND to_char( e.employee_number, \'99999\' ) LIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\' ';
			} elseif( 1 < \Psi\Libraries\UtilFunctions\count( $arrmixFilteredExplodedSearch ) ) {
				if( true == array_key_exists( 2, $arrmixFilteredExplodedSearch ) ) {
					$strSearchCondition = 'AND ( ( ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' AND e.name_last ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\' ) OR e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\')	OR ( ( ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' AND e.name_last ILIKE \'%' . $arrmixFilteredExplodedSearch[2] . '%\') OR e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\') ) OR d.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR de.name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\'	OR de.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' )';
				} else {
					$strSearchCondition = 'AND ( ( ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' ) OR e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\')	OR ( ( ( e.name_first ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' ) OR e.preferred_name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\') ) OR d.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR de.name ILIKE \'%' . $arrmixFilteredExplodedSearch[0] . '%\'	OR de.name ILIKE \'%' . $arrmixFilteredExplodedSearch[1] . '%\' )';
				}
			} else {
				$strSearchCondition = 'AND ( e.name_first ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.name_last ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR e.preferred_name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR d.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\' OR de.name ILIKE \'%' . implode( $arrmixFilteredExplodedSearch ) . '%\')';
			}
		}

		if( true == valObj( $objEmployeeCertificationsFilter, 'CEmployeeCertificationsFilter' ) ) {
			if( true == valArr( $objEmployeeCertificationsFilter->getDesignationIds() ) ) {
				$strSearchCondition .= ' AND e.designation_id IN ( ' . implode( ',', $objEmployeeCertificationsFilter->getDesignationIds() ) . ' )';
			}
			if( true == valArr( $objEmployeeCertificationsFilter->getDepartmentIds() ) ) {
				$strSearchCondition .= ' AND e.department_id IN ( ' . implode( ',', $objEmployeeCertificationsFilter->getDepartmentIds() ) . ' )';
			}
			if( true == valArr( $objEmployeeCertificationsFilter->getCertificationIds() ) ) {
				$strSearchCondition .= ' AND c.id IN ( ' . implode( ',', $objEmployeeCertificationsFilter->getCertificationIds() ) . ' )';
			}
			if( true == $objEmployeeCertificationsFilter->getIsApproved() ) {
				$strSearchCondition .= ' AND ps.approved_by IS NOT NULL';
			}
			if( true == valArr( $objEmployeeCertificationsFilter->getAddEmployeeIds() ) ) {
				$strSearchCondition .= ' AND t.add_employee_id IN ( ' . implode( ',', $objEmployeeCertificationsFilter->getAddEmployeeIds() ) . ' )';
				$strFromSearchCondition = ' LEFT JOIN team_employees AS te ON ( e.id = te.employee_id )
											LEFT JOIN teams AS t ON ( t.id = te.team_id )';
			}
		}

		$strSql = 'SELECT
						COUNT ( e.id ) AS certification_count,
						COUNT ( ec.ps_document_id ) AS ps_document_count,
						COUNT ( ps.approved_by ) as approval_status,
						e.id AS employee_id,
						e.name_full,
						d.name AS department_name,
						de.name AS designation_name
					FROM
						employee_certifications ec
						LEFT JOIN certifications AS c ON ( ec.certification_id = c.id )
						LEFT JOIN employees e ON ( ec.employee_id = e.id )
						LEFT JOIN departments d ON( e.department_id = d.id )
						LEFT JOIN designations AS de ON ( e.designation_id = de.id )
						LEFT JOIN ps_documents AS ps ON ( ec.ps_document_id = ps.id )
						' . $strFromSearchCondition . '
					WHERE
						ec.is_certified = TRUE 
						AND e.employee_status_type_id NOT IN ( ' . CEmployeeStatusType::PREVIOUS . ',' . CEmployeeStatusType::NO_SHOW . ' )
						AND c.deleted_by IS NULL
						AND ( e.date_terminated IS NULL
						OR e.date_terminated > NOW ( ) )
						' . $strSearchCondition . '
					GROUP BY
						e.id,
						e.name_full,
						d.name ,
						de.name,
						ec.updated_on
					ORDER BY ' . $strOrderByField . ' ' . $strOrderByType . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeCertificatesByEmployeeId( $intEmployeeId, $objDatabase, $intPageNo = NULL, $intPageSize = NULL, $strOrderByField = NULL, $strOrderByType = NULL, $boolIsFromProfile = false, $strLastAssessedOnDate = NULL, $strConsideredTillDate = NULL ) {

		if( false == is_numeric( $intEmployeeId ) ) return NULL;

		$strWhere			= NULL;
		$strOffsetClause	= NULL;
		$strSelectClause	= NULL;
		$strJoinClause		= NULL;
		$strOrderByClause	= ' ec.id DESC';

		if( ( true == valStr( $strOrderByField ) || valStr( $strOrderByType ) ) && $strOrderByField != 'first_time' ) {
			$strOrderByClause = $strOrderByField . ' ' . $strOrderByType;
		}

		if( true == is_numeric( $intPageNo ) && true == is_numeric( $intPageSize ) ) {
			$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$intLimit = ( int ) $intPageSize;
			$strOffsetClause = 'OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}

		if( true == $boolIsFromProfile ) {
			$strWhere = ' AND ps.approved_by IS NOT NULL';
		}

		if( true == valStr( $strLastAssessedOnDate ) && true == valStr( $strConsideredTillDate ) ) {
			$strLastAssessedOnDate = date( 'm/d/Y', strtotime( $strLastAssessedOnDate ) );
			$strConsideredTillDate = date( 'm/d/Y', strtotime( $strConsideredTillDate ) );
			$strWhere .= ' AND ec.certification_datetime::date BETWEEN \'' . $strLastAssessedOnDate . '\' AND \'' . $strConsideredTillDate . '\'';
		}

		$strJoinClause = ' LEFT JOIN (
									SELECT
										c1.id as certification_id,
										count ( tg.test_id ) AS test_count
									FROM
										certifications c1
										JOIN test_groups tg ON ( c1.id = tg.certification_id )
									GROUP BY
										c1.id ) as test_counts ON ( test_counts.certification_id = c.id )';
		$strSelectClause = ' , test_counts.test_count';

		$strSql = 'SELECT
					ec.id,
					ec.certification_datetime::date,
					ec.score,
					ec.ps_document_id,
					e.id AS employee_id,
					e.name_full,
					c.is_external,
					ct.name AS certificate_type_name,
					clt.name AS certificate_level_name,
					ps.approved_by,
					CASE
						WHEN ( c.is_external = 1 ) THEN c.name || \' | External\'
						WHEN ( c.is_external = 0 ) THEN c.name || \' | Internal\'
					END AS certificate_name ' . $strSelectClause . '
				FROM
					employee_certifications AS ec
					LEFT JOIN employees AS e ON ( ec.employee_id = e.id )
					LEFT JOIN certifications AS c ON ( ec.certification_id = c.id )
					LEFT JOIN certification_types AS ct ON ( c.certification_type_id = ct.id )
					LEFT JOIN certification_level_types AS clt ON ( c.certification_level_type_id = clt.id )
					LEFT JOIN ps_documents AS ps ON ( ps.id = ec.ps_document_id ) ' . $strJoinClause . '
				WHERE
					ec.is_certified = TRUE
					AND ec.employee_id = ' . $intEmployeeId . '
					AND c.deleted_by IS NULL ' . $strWhere . '
				ORDER BY
					' . $strOrderByClause . '
					' . $strOffsetClause;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeCertificationsByCertificationIds( $arrintCertificationIds, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						employee_certifications
					WHERE
						is_certified = TRUE 
						AND certification_id IN ( ' . implode( ', ', $arrintCertificationIds ) . ' )';

		return parent::fetchEmployeeCertifications( $strSql, $objDatabase );
	}

	public static function fetchEmployeeCertificationsByCertificationId( $intCertificationId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						employee_certifications
					WHERE
						certification_id = ' . ( int ) $intCertificationId;

		return parent::fetchEmployeeCertifications( $strSql, $objDatabase );
	}

	public static function fetchEmployeeCertificatesCountByEmployeeId( $intEmployeeId, $objDatabase, $boolIsFromProfile = false ) {

		if( false == is_numeric( $intEmployeeId ) ) return NULL;

		$strWhere = NULL;
		if( true == $boolIsFromProfile ) {
			$strWhere = ' AND ps.approved_by IS NOT NULL';
		}

		$strSql = 'SELECT
					COUNT( ec.id )
				FROM
					employee_certifications AS ec
					LEFT JOIN employees AS e ON ( ec.employee_id = e.id )
					LEFT JOIN certifications AS c ON ( ec.certification_id = c.id )
					LEFT JOIN certification_types AS ct ON ( c.certification_type_id = ct.id )
					LEFT JOIN certification_level_types AS clt ON ( c.certification_level_type_id = clt.id )
					LEFT JOIN ps_documents AS ps ON ( ps.id = ec.ps_document_id )
				WHERE
					ec.is_certified = TRUE 
					AND c.deleted_by IS NULL
					AND ec.employee_id = ' . $intEmployeeId . $strWhere;

		$arrintEmployeeCertificates = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrintEmployeeCertificates ) ) {
			return $arrintEmployeeCertificates[0]['count'];
		} else {
			return NULL;
		}
	}

	public static function fetchEmployeeCertificationCountByCertificationIdByEmployeeId( $intCertificationId, $intEmployeeId, $objDatabase, $intEmployeeCertificationId = NULL ) {
		$strWhere = '';
		if( false == empty( $intEmployeeCertificationId ) ) {
			$strWhere = ' AND id <> ' . ( int ) $intEmployeeCertificationId;
		}

		$strSql = ' SELECT
						count(id)
					FROM
						employee_certifications
					WHERE
						is_certified = TRUE
						AND certification_id = ' . ( int ) $intCertificationId . '
						AND employee_id = ' . ( int ) $intEmployeeId . $strWhere;

		$arrintCount = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrintCount ) ) {
			return $arrintCount[0]['count'];
		} else {
			return NULL;
		}
	}

	public static function fetchEmployeeCertificationsByEmployeeIdsByCertificationIds( $arrintEmployeeIds, $arrintCertificationIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) || false == valArr( $arrintCertificationIds ) ) {
			return NULL;
		}
		$strSql = 'SELECT
						*
					FROM
						employee_certifications
					WHERE
						employee_id IN ( ' . implode( ', ', $arrintEmployeeIds ) . ' )
						AND certification_id IN ( ' . implode( ', ', $arrintCertificationIds ) . ' )';
		return parent::fetchEmployeeCertifications( $strSql, $objDatabase );
	}

	public static function fetchEmployeeCertificationsBetweenReviewDates( $intEmployeeId, $strLastAssessedOnDate, $strConsideredTillDate, $objDatabase ) {

		$strLastAssessedOnDate = date( 'Y-m-d', strtotime( $strLastAssessedOnDate ) );
		$strConsideredTillDate = date( 'Y-m-d', strtotime( $strConsideredTillDate ) );

		$strSql = 'SELECT
						count ( ec.id )
					FROM
						employee_certifications ec
						LEFT JOIN ps_documents pd ON ( ec.ps_document_id = pd.id )
					WHERE
					    ec.is_certified = TRUE 
					    AND ec.employee_id = ' . ( int ) $intEmployeeId . '
						AND pd.approved_by IS NOT NULL
						AND ec.certification_datetime::date BETWEEN \'' . $strLastAssessedOnDate . '\' AND \'' . $strConsideredTillDate . '\'';

		return fetchData( $strSql, $objDatabase );
	}

}
?>