<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CBonusTypes
 * Do not add any new functions to this class.
 */

class CBonusTypes extends CBaseBonusTypes {

	public static function fetchBonusTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CBonusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchBonusType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CBonusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllBonusTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM bonus_types order by id asc';
		return parent::fetchBonusTypes( $strSql, $objDatabase );
	}
}
?>