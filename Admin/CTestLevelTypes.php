<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestLevelTypes
 * Do not add any new functions to this class.
 */

class CTestLevelTypes extends CBaseTestLevelTypes {

	public static function fetchTestLevelTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CTestLevelType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchTestLevelType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CTestLevelType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedTestLevelTypes( $objDatabase ) {
		return self::fetchTestLevelTypes( 'SELECT * FROM test_level_types WHERE is_published = 1 ORDER BY order_num', $objDatabase );
	}
}
?>