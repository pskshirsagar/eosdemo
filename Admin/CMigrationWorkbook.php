<?php

class CMigrationWorkbook extends CBaseMigrationWorkbook {

	/**
	 * Create Functions
	 **/

	public function createMigrationWorkbookComment() {
		$objMigrationWorkbookComment = new CMigrationWorkbookComment();
		$objMigrationWorkbookComment->setMigrationWorkbookId( $this->m_intId );

		return $objMigrationWorkbookComment;
	}

	/**
	 * Validation Functions
	 **/

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
		}

		return $boolIsValid;
	}

	public function valImportTypeIds() {
		$boolIsValid = true;

		if( true == is_null( $this->getImportTypeIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'import_type_ids', 'Migration Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPrimaryCompanyUserIds() {
		$boolIsValid = true;

		if( true == is_null( $this->getPrimaryCompanyUserIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'primary_company_user_ids', 'Contacts Responsible For Signing Approval Docs is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCoreConsultantUserId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCoreConsultantUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'core_consultant_user_id', 'Core Consultant is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAccountingConsultantUserId() {
		$boolIsValid = true;

		if( true == is_null( $this->getAccConsultantUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'acc_consultant_user_id', 'Accounting Consultant is required.' ) );
		}

		return $boolIsValid;
	}

	public function valImportDataTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getImportDataTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'import_data_type_id', 'Property Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAsOfDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getAsOfDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'as_of_date', 'As of date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEntrataUnitCount() {
		$boolIsValid = true;

		if( true == is_null( $this->getEntrataUnitCount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entrata_unit_count', 'Unit Count is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEntrataSpaceCount() {
		$boolIsValid = true;

		if( true == is_null( $this->getEntrataSpaceCount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entrata_space_count', 'Space Count is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsTakeover() {
		$boolIsValid = true;

		if( true == is_null( $this->getIsTakeover() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_takeover', 'Is TakeOver is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMigrationConsultantUserId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMigrationConsultantUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'migration_consultant_user_id', 'Migration Consultant is required.' ) );
		}

		return $boolIsValid;
	}

	public function valCompetitorId() {
		$boolIsValid = true;

		if( true == is_null( $this->getCompetitorId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'competitor_id', 'Former property management software is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMigrationStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getMigrationStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'migration_start_date', 'Scheduled Migration Date is required.' ) );
		}

		if( false == is_null( $this->getMigrationStartDate() ) && strtotime( $this->getMigrationStartDate() ) < strtotime( date( 'Y-m-d' ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'migration_start_date', 'Scheduled Migration Date should not be Past Date.' ) );
		}

		return $boolIsValid;
	}

	public function valProjectManagerUserId() {
		$boolIsValid = true;

		if( true == is_null( $this->getProjectManagerUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'project_manager_user_id', 'Project Manager is required.' ) );
		}

		return $boolIsValid;
	}

	public function valBackupMigrationConsultantUserId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valImportTypeIds();
				$boolIsValid &= $this->valPrimaryCompanyUserIds();
				$boolIsValid &= $this->valCoreConsultantUserId();
				$boolIsValid &= $this->valAccountingConsultantUserId();
				$boolIsValid &= $this->valImportDataTypeId();
				$boolIsValid &= $this->valMigrationStartDate();
				$boolIsValid &= $this->valProjectManagerUserId();
				break;

			case 'validate_send_to_sign':
				$boolIsValid &= $this->valPrimaryCompanyUserIds();
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_operation_checklist':
				$boolIsValid &= $this->valCoreConsultantUserId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valAsOfDate();
				$boolIsValid &= $this->valIsTakeover();
				$boolIsValid &= $this->valEntrataUnitCount();
				$boolIsValid &= $this->valEntrataSpaceCount();
				$boolIsValid &= $this->valImportTypeIds();
				$boolIsValid &= $this->valPrimaryCompanyUserIds();
				$boolIsValid &= $this->valCompetitorId();
				break;

			case 'validate_consultant_checklist':
				$boolIsValid &= $this->valMigrationConsultantUserId();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getDataValues( $boolReturnArray = false ) {
		if( false == $boolReturnArray ) {
			return parent::getDataValues();
		}

		if( true == valObj( $this->m_jsonDataValues, 'stdClass' ) ) {
			$this->m_strDataValues = json_encode( $this->m_jsonDataValues );
		}

		return json_decode( $this->m_strDataValues, true );
	}

	public function getCustomMappingDataValues( $boolReturnArray = false ) {
		if( false == $boolReturnArray ) {
			return parent::getCustomMappingDataValues();
		}

		if( true == valObj( $this->m_jsonCustomMappingDataValues, 'stdClass' ) ) {
			$this->m_strCustomMappingDataValues = json_encode( $this->m_jsonCustomMappingDataValues );
		}

		return json_decode( $this->m_strCustomMappingDataValues, true );
	}

	public static function getWorkbookAccountingReports() {
		$arrstrWorkbookAccountingReports = [
			[ 'title' => 'Vendor Files', 'consultant_checklist_title' => 'Vendor Files Report', 'report_id' => CMigrationWorkbookKey::VENDOR_FILE_REPORT ],
			[ 'title' => 'Vendor 1099 Balances', 'consultant_checklist_title' => 'Vendor 1099 Balances Report', 'report_id' => CMigrationWorkbookKey::VENDOR_1099_BALANCE_REPORT ],
			[ 'title' => 'Approved Purchase Orders', 'consultant_checklist_title' => 'Approved Purchase Orders Report', 'report_id' => CMigrationWorkbookKey::APPROVED_PURCHASE_ORDERS_REPORT ],
			[ 'title' => 'Open Invoices', 'consultant_checklist_title' => 'Open Invoices Report', 'report_id' => CMigrationWorkbookKey::OPEN_INVOICES_REPORT ],
			[ 'title' => 'Trial Balances', 'consultant_checklist_title' => 'Trial Balances Report', 'report_id' => CMigrationWorkbookKey::TRIAL_BALANCE_REPORT ],
			[ 'title' => 'Budgets', 'consultant_checklist_title' => 'Budgets Report', 'report_id' => CMigrationWorkbookKey::BUDGETS_REPORT ]
		];

		return $arrstrWorkbookAccountingReports;
	}

}
?>