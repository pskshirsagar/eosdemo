<?php

class CEmployeeApplicationAnswer extends CBaseEmployeeApplicationAnswer {

	public function valAnswer() {
		$boolIsValid = true;
		$boolIsValid = true;
		$arrmixRequireEmployeeApplicationAnswers = array(
				'POSITION_APPLIED_FOR',
				'PROFESSIONAL_REFERENCES_1_NAME',
				'PROFESSIONAL_REFERENCES_1_COMPANY_TITLE',
				'PROFESSIONAL_REFERENCES_1_RELATIONSHIP',
				'PROFESSIONAL_REFERENCES_1_PHONE_NUMBER',
				'PROFESSIONAL_REFERENCES_2_NAME',
				'PROFESSIONAL_REFERENCES_2_COMPANY_TITLE',
				'PROFESSIONAL_REFERENCES_2_RELATIONSHIP',
				'PROFESSIONAL_REFERENCES_2_PHONE_NUMBER',
				'PROFESSIONAL_REFERENCES_3_NAME',
				'PROFESSIONAL_REFERENCES_3_COMPANY_TITLE',
				'PROFESSIONAL_REFERENCES_3_RELATIONSHIP',
				'PROFESSIONAL_REFERENCES_3_PHONE_NUMBER',
				'PREVIOUS_EMPLOYMENT_1_COMPANY',
				'PREVIOUS_EMPLOYMENT_1_JOB_TITLE',
				'PREVIOUS_EMPLOYMENT_1_PHONE_NUMBER',
				'PREVIOUS_EMPLOYMENT_1_STREET_ADDRESS',
				'PREVIOUS_EMPLOYMENT_1_CITY',
				'PREVIOUS_EMPLOYMENT_1_STATE_ZIP',
				'PREVIOUS_EMPLOYMENT_1_SUPERVISOR',
				'PREVIOUS_EMPLOYMENT_1_SALARY',
				'PREVIOUS_EMPLOYMENT_1_FROM',
				'PREVIOUS_EMPLOYMENT_1_TO',
				'PREVIOUS_EMPLOYMENT_1_RESPONSIBILITES',
				'PREVIOUS_EMPLOYMENT_1_REASON_FOR_LEAVING' );

		if( true == in_array( $this->m_strQuestion, $arrmixRequireEmployeeApplicationAnswers ) ) {

			if( false == isset( $this->m_strAnswer ) || ( 1 > strlen( $this->m_strAnswer ) ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, $this->m_strQuestion, \Psi\CStringService::singleton()->ucfirst( \Psi\CStringService::singleton()->strtolower( str_replace( '_', ' ', $this->m_strQuestion ) ) ) . ' is required.' ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valAnswer();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>