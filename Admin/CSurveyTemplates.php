<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplates
 * Do not add any new functions to this class.
 */

class CSurveyTemplates extends CBaseSurveyTemplates {

	public static function fetchPaginatedSurveyTemplates( $intPageNo, $intPageSize, $objDatabase, $objSurveySystemFilter = NULL ) {

		$strWhereClause = '';
		if( false == is_null( $objSurveySystemFilter->getSurveyTypeId() ) )
			$strWhereClause = ' AND stp.id = ' . ( int ) $objSurveySystemFilter->getSurveyTypeId();

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		if( true == valObj( $objSurveySystemFilter, 'CSurveySystemFilter' ) && true == valStr( $objSurveySystemFilter->getSurveyName() ) ) {
			$strWhereClause .= ' AND ( st.name ILIKE \'%' . trim( $objSurveySystemFilter->getSurveyName() ) . '%\' OR stp.name ILIKE \'%' . trim( $objSurveySystemFilter->getSurveyName() ) . '%\' )';
		}

		$strSql = ' SELECT
						st.id,
						st.name,
						stp.id AS survey_type_id,
						stp.name AS survey_type,
						st.is_published,
						COUNT ( stq.id ) AS question
					FROM
						survey_templates st
						FULL OUTER JOIN survey_template_questions stq ON ( st.id = stq.survey_template_id )
						INNER JOIN survey_types stp ON ( stp.id = st.survey_type_id )
					WHERE
						st.deleted_on IS NULL ' . $strWhereClause . '
					GROUP BY
						st.id,
						stp.id,
						stp.name,
						st.name,
						st.is_published,
						st.survey_type_id
					ORDER BY
						stp.name,
						st.name ' . $objSurveySystemFilter->getSortOrder() . '
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intPageSize;

		return self::fetchSurveyTemplates( $strSql, $objDatabase );
	}

	public static function fetchSurveyTemplatesCount( $objDatabase, $objSurveySystemFilter = NULL ) {

		$strWhereClause = '';
		if( true == valId( $objSurveySystemFilter->getSurveyTypeId() ) )
			$strWhereClause = ' AND stp.id = ' . ( int ) $objSurveySystemFilter->getSurveyTypeId();

		if( true == valObj( $objSurveySystemFilter, 'CSurveySystemFilter' ) && true == valStr( $objSurveySystemFilter->getSurveyName() ) ) {
			$strWhereClause .= ' AND ( st.name ILIKE \'%' . trim( $objSurveySystemFilter->getSurveyName() ) . '%\' OR stp.name ILIKE \'%' . trim( $objSurveySystemFilter->getSurveyName() ) . '%\' )';
		}

		$strSql = 'SELECT
						count(1)
					FROM
						survey_templates st
						INNER JOIN survey_types stp ON ( stp.id = st.survey_type_id )
					WHERE
						st.deleted_on IS NULL ' . $strWhereClause;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		return ( isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
	}

	public static function fetchPublishedSurveyTemplatesBySurveyTypeId( $intSurveyTypeId, $objDatabase ) {
		return self::fetchSurveyTemplate( sprintf( 'SELECT * FROM survey_templates WHERE is_published = 1 AND survey_type_id = %d LIMIT 1', ( int ) $intSurveyTypeId ), $objDatabase );
	}

	public static function fetchActiveSurveyTemplatesBySurveyTypeIds( $arrintSurveyTypeIds, $objDatabase ) {

		if( false == valArr( $arrintSurveyTypeIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						survey_templates
					WHERE
						survey_type_id IN (' . implode( ',', $arrintSurveyTypeIds ) . ')
						AND is_published = 1';

		return self::fetchSurveyTemplates( $strSql, $objDatabase );
	}

	public static function fetchSubmittedSurveysCountBySurveyTemplateIds( $arrintSurveyTemplateIds, $objDatabase ) {

		if( false == valArr( $arrintSurveyTemplateIds ) ) return NULL;

		$strSql = 'SELECT
						survey_template_id,
						count(id)
					FROM
						surveys s
					WHERE
						s.survey_template_id IN (' . implode( ',', $arrintSurveyTemplateIds ) . ')
						AND s.response_datetime IS NOT NULL
					GROUP BY
						survey_template_id';

		$arrmixSurveysResponseCount = fetchData( $strSql, $objDatabase );

		$arrintSubmittedSurveyCount = array();

		if( true == valArr( $arrmixSurveysResponseCount ) ) {
			foreach( $arrmixSurveysResponseCount as $arrmixSurveyResponseCount ) {
				$arrintSubmittedSurveyCount[$arrmixSurveyResponseCount['survey_template_id']] = $arrmixSurveyResponseCount['count'];
			}
		}
		return $arrintSubmittedSurveyCount;
	}

	public static function fetchConflictingSurveyTemplatesCountByName( $intId, $strName, $objDatabase ) {

		$strWhereSql = ' WHERE
							id <>' . ( int ) $intId . '
							AND lower( name ) = lower( ' . $strName . ' ) AND deleted_by IS NULL';

		return self::fetchSurveyTemplateCount( $strWhereSql, $objDatabase );
	}

	public static function fetchSurveyTemplateBySurveyTriggerReferenceIdBySurveyTypeId( $strTriggerReferenceIds, $intSurveyTypeId, $objDatabase ) {

		if( false == valStr( $strTriggerReferenceIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						st.*
					FROM
						survey_templates st
						LEFT JOIN surveys s ON ( s.survey_template_id = st.id )
						LEFT JOIN training_sessions ts ON ( s.trigger_reference_id = ts.id )
					WHERE
						s.trigger_reference_id IN ( ' . $strTriggerReferenceIds . ' )
						AND s.survey_type_id =' . ( int ) $intSurveyTypeId . '
						AND s.survey_template_id NOT IN ( ' . CSurveyTemplate::JUSTIFICATION_FOR_NON_ATTENDENCE . ' , ' . CSurveyTemplate::INTERVIEW_FEEDBACK_SURVEY . ' )
					LIMIT 1 ';

		return self::fetchSurveyTemplate( $strSql, $objDatabase );
	}

	public static function fetchSurveyTemplatesBySurveyTypeId( $intSurveyTypeId, $objDatabase, $boolIsShowDeletedSurveyTemplates = false ) {

		$strSubSql = '';

		if( false == $boolIsShowDeletedSurveyTemplates ) {
			$strSubSql = ' AND st. deleted_by IS NULL ';
		}

		$strSql = 'SELECT
						sub.id,
						sub.name,
						sub.survey_type_id,
						sub.survey_type
					FROM
						(
						SELECT
							st.id,
							st.name,
							stp.id AS survey_type_id,
							stp.name AS survey_type,
							COUNT ( stq.id ) AS question_count
						FROM
							survey_templates st
							FULL OUTER JOIN survey_template_questions stq ON ( st.id = stq.survey_template_id )
							INNER JOIN survey_types stp ON ( stp.id = st.survey_type_id )
						WHERE
							survey_type_id = ' . ( int ) $intSurveyTypeId . '
							AND stq.is_published <> 0 ' . ' 
							AND st.is_published <> 0 ' . $strSubSql . '
							AND st.id <> ' . CSurveyTemplate::JUSTIFICATION_FOR_NON_ATTENDENCE . '
						GROUP BY
							st.id,
							stp.id,
							stp.name,
							st.name,
							st.survey_type_id
						) AS sub
						WHERE
							sub.question_count <> 0
							ORDER by sub.name';

		return self::fetchSurveyTemplates( $strSql, $objDatabase );
	}

	public static function fetchInterviewSurveyTemplateIds( $objDatabase ) {
		$strSql = ' SELECT
						distinct st.id
					FROM 
						survey_templates st
						JOIN ps_job_posting_steps pjps ON ( pjps.survey_template_id = st.id )
						JOIN ps_website_job_postings pwjp ON ( pjps.ps_job_posting_id = pwjp.id )
					WHERE 
						pwjp.country_code = \'IN\'';

		return fetchData( $strSql, $objDatabase );
	}

}
?>