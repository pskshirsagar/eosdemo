<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsModules
 * Do not add any new functions to this class.
 */

class CPsModules extends CBasePsModules {

	public static function fetchPublishedModules( $objDatabase, $boolOptionalOrderBy = false ) {
		$strOrderBy = '';
		if( true == $boolOptionalOrderBy ) {
			$strOrderBy = ' ps_module_id,id,';
		}

		return self::fetchPsModules( 'SELECT * FROM ps_modules WHERE is_published = 1 ORDER BY ' . $strOrderBy . 'order_num', $objDatabase );
	}

	public static function fetchAllPsModules( $objDatabase ) {
		return self::fetchPsModules( 'SELECT * FROM ps_modules ORDER BY id', $objDatabase );
	}

	public static function fetchPsModulesByIds( $arrintPsModuleIds, $objDatabase ) {

		if( false == valArr( $arrintPsModuleIds ) ) return NULL;

		$strSql = 'SELECT *	FROM ps_modules	WHERE id IN ( ' . implode( ',', $arrintPsModuleIds ) . ' )';

		return self::fetchPsModules( $strSql, $objDatabase );
	}

	public static function fetchSortedPsModulesByPsModuleId( $intPsModuleId, $objDatabase ) {

		if( false == is_numeric( $intPsModuleId ) ) return NULL;

		$strSql = 'SELECT *	FROM ps_modules	WHERE ps_module_id = ' . ( int ) $intPsModuleId . ' ORDER BY order_num';

		return self::fetchPsModules( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedPsModules( $intPageNo, $intPageSize, $objAdminDatabase, $arrmixPsModulesFilter, $intPsParentModuleId = NULL ) {

		if( false == is_numeric( $intPageNo ) || false == is_numeric( $intPageSize ) ) return NULL;

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		$strWhereCondition = 'WHERE ps_module_id IS NULL';

		if( false == empty( $intPsParentModuleId ) ) {
			$strWhereCondition = 'WHERE ps_module_id = ' . ( int ) $intPsParentModuleId;
		}
		$strSql = 'SELECT
   						*
					FROM
   						ps_modules
					' . $strWhereCondition . '
   					ORDER BY
						' . $arrmixPsModulesFilter['sort_by_field'] . ' ' . $arrmixPsModulesFilter['sort_by_type'] . '
					OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		return self::fetchPsModules( $strSql, $objAdminDatabase );
	}

	public static function fetchPsModulesCount( $objAdminDatabase, $intPsParentModuleId = NULL ) {

		$strWhereCondition = 'ps_module_id IS NULL';

		if( false == empty( $intPsParentModuleId ) ) {
			$strWhereCondition = ' ps_module_id = ' . ( int ) $intPsParentModuleId;
		}

		$arrintResponse	= fetchData( 'SELECT count(id) FROM ps_modules WHERE ' . $strWhereCondition . ' ', $objAdminDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
		return 0;
	}

	public static function fetchParentModuleIdsByPsModuleIds( $intPsModuleIds, $objDatabase ) {
		$strSql = 'SELECT
						DISTINCT( ps_module_id )
					FROM
						ps_modules
					WHERE
						ps_module_id IN( ' . implode( ',', $intPsModuleIds ) . ' )';

		$arrintParentModules = fetchData( $strSql, $objDatabase );
		return rekeyArray( 'ps_module_id', $arrintParentModules );
	}

	public static function fetchQuickSearchPsModules( $arrstrFilteredExplodedSearch, $objAdminDatabase ) {

		if( false == valArr( $arrstrFilteredExplodedSearch ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
   						ps_modules AS ps
					WHERE
						ps.module_name ILIKE \'%' . implode( '%\' AND ps.module_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR ps.action_name ILIKE \'%' . implode( '%\' AND ps.action_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR ps.title ILIKE \'%' . implode( '%\' AND ps.title ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
					LIMIT 10';

		return self::fetchPsModules( $strSql, $objAdminDatabase );
	}

	public static function fetchChildModulesByPsModuleIdByModuleLevel( $intPsModuleId, $intModuelLevel, $objDatabase ) {

		if( false == is_numeric( $intPsModuleId ) || false == is_numeric( $intModuelLevel ) ) return NULL;

		$strSql = 'WITH RECURSIVE p AS (
                        SELECT
                            pm.id,
                            pm.ps_module_id,
                            pm.title,
                            pm.is_published,
                            pm.is_public,
							( ' . $intModuelLevel . ' + 1 ) as level
                        FROM
                            ps_modules pm
                        WHERE
                            ps_module_id = ' . $intPsModuleId . '
                        UNION ALL
                        SELECT
                            pm.id,
                            pm.ps_module_id,
                            pm.title,
                            pm.is_published,
                            pm.is_public,
                            p.level + 1 as level
                        FROM
                            ps_modules pm
                            JOIN p ON p.id = pm.ps_module_id
                    )
				SELECT
				    p.id
				FROM
				    p
				WHERE
				    is_published = 1
				    AND level < 5
				ORDER BY
				    p.ps_module_id,
				    level';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPermissionedUsersAndGroupsCount( $objDatabase ) {

		$strSql = 'SELECT
   						pm.id as ps_module_id,
						COALESCE ( gp_sub.group_count, 0 ) AS group_count,
						COALESCE ( up_sub.user_count, 0 ) AS user_count
					FROM
						ps_modules pm
						LEFT JOIN
					    (
					      SELECT
					          gp.ps_module_id,
					          COUNT ( g.id ) AS group_count
					      FROM
					          groups g
					          JOIN group_permissions gp ON ( gp.group_id = g.id )
					      WHERE
					          g.deleted_on IS NULL
					          AND g.deleted_by IS NULL
					          AND gp.is_allowed = 1
					      GROUP BY
					          gp.ps_module_id
					    ) gp_sub ON ( pm.id = gp_sub.ps_module_id )
					    LEFT JOIN
					    (
					      SELECT
					          ps_module_id,
					          COUNT ( 1 ) AS user_count
					      FROM
					          users u
					          JOIN user_permissions up ON ( up.user_id = u.id )
					      WHERE
					          u.is_disabled = 0
					          and u.is_administrator = 0
					          AND up.is_allowed = 1
					      GROUP BY
					          ps_module_id
					    ) up_sub ON ( pm.id = up_sub.ps_module_id )
					WHERE
					    pm.is_published = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllChildModulesByPsModuleId( $intPsModuleId, $objDatabase ) {

		$strSql = '
			WITH RECURSIVE child_modules AS (
				SELECT
					pm.*,
					1 as level
				FROM
					ps_modules pm
				WHERE
					ps_module_id = ' . ( int ) $intPsModuleId . '
				UNION ALL
				SELECT
					pm.*,
					cm.level + 1 as level
				FROM
					ps_modules pm
					JOIN child_modules cm ON cm.id = pm.ps_module_id
			)
			SELECT
				cm.*
			FROM
				child_modules cm
			WHERE
				is_published = 1
			ORDER BY
				level,
				order_num';

		return self::fetchPsModules( $strSql, $objDatabase );
	}

	public static function fetchMaxOrderNumByPsModuleId( $intPsModuleId, $objDatabase ) {
		$strSql = '
			SELECT
				MAX( order_num ) AS max_order_num
			FROM
				ps_modules pm
			WHERE
				pm.ps_module_id = ' . ( int ) $intPsModuleId . ';
		';

		return fetchData( $strSql, $objDatabase );
	}

}
?>