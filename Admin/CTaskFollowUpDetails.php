<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskFollowUpDetails
 * Do not add any new functions to this class.
 */

class CTaskFollowUpDetails extends CBaseTaskFollowUpDetails {

	public static function fetchTaskFollowUpDetailsByTaskTypeIdByTaskPriorityIdByTaskStatusId( $intTaskTypeId, $intTaskPriorityId, $intTaskStatusId, $objDatabase ) {

		if( false == is_numeric( $intTaskTypeId ) || false == is_numeric( $intTaskPriorityId ) || false == is_numeric( $intTaskStatusId ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						follow_up_time
					FROM task_follow_up_details
					WHERE task_type_id = ' . ( int ) $intTaskTypeId . '
						AND task_priority_id = ' . ( int ) $intTaskPriorityId . '
						AND task_status_id = ' . ( int ) $intTaskStatusId;

		return fetchData( $strSql, $objDatabase )[0];
	}

}
?>