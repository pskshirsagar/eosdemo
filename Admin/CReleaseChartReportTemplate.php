<?php

class CReleaseChartReportTemplate extends CBaseReleaseChartReportTemplate {

	const TREND_LINE_FOR_RELEASE_DOWNTIME 												= 1;
	const TREND_LINE_FOR_NON_QA_URGENT_IMMEDIATE_SPRINT_NOT_UPDATED_TASKS				= 2;
	const TREND_LINE_FOR_TASK_SUMMARY													= 3;
	const TREND_LINE_FOR_PERCENT_NON_QA_TASKS											= 4;
	const APPROX_ESTIMATION_OF_ADD_QA_WISE_BIFURCATION_OF_TEST_CASES					= 5;
	const APPROX_ESTIMATION_OF_ADD_QA_WISE_TEST_CASE_COVERAGE_VS_TEST_CASES_LAGGING		= 6;
	const ADD_QAM_WISE_QA_VS_NON_QA_TASKS												= 7;
	const URGENT_VS_IMMEDIATE_OVERDUE_BUG_TASKS											= 8;
	const ADD_QAM_WISE_BIFURCATION_OF_QA_NOT_REQUIRED_TASKS								= 9;
	const ADD_QAM_WISE_AND_PRIORITY_WISE_CARRY_FORWARDED_TASKS							= 10;
	const BUG_VS_FEATURE_TASKS_RELEASED													= 11;
	const ADD_QAM_WISE_PERCENT_OF_NON_QA_TASKS											= 12;
	const QA_VS_NON_QA_TASKS															= 13;
	const ADD_QAM_WISE_PERCENT_OF_SPRINT_NOT_UPDATED_TASKS								= 14;
	const ADD_QAM_WISE_BIFURCATION_OF_NON_QA_TASKS 										= 15;
	const ADD_QAM_WISE_BIFURCATION_OF_NON_QA_BUG_TASKS									= 16;
	const BU_WISE_QA_VS_NON_QA_TASKS													= 17;
	const BU_WISE_SPRINT_NOT_UPDATED_TASKS												= 18;
	const TREND_LINE_FOR_RCA_VS_NON_RCA_BUG_TASKS										= 19;
	const ADD_QAM_WISE_RCA_VS_NON_RCA_BUG_TASKS											= 20;
	const RCA_VS_NON_RCA_BUG_TASKS														= 21;
	const CARRY_FORWARDED_VS_RELEASED_TASKS												= 22;
	const TREND_LINE_FOR_ADD_QAM_WISE_CARRY_FORWARDED_TASKS								= 23;
	const ADD_QAM_WISE_CARRY_FORWARDED_TASKS											= 24;

	public static $c_arrintTrendLineChartReportTemplates = [
		self::TREND_LINE_FOR_RELEASE_DOWNTIME,
		self::TREND_LINE_FOR_NON_QA_URGENT_IMMEDIATE_SPRINT_NOT_UPDATED_TASKS,
		self::TREND_LINE_FOR_TASK_SUMMARY,
		self::TREND_LINE_FOR_PERCENT_NON_QA_TASKS,
		self::ADD_QAM_WISE_BIFURCATION_OF_NON_QA_TASKS,
		self::TREND_LINE_FOR_RCA_VS_NON_RCA_BUG_TASKS,
		self::CARRY_FORWARDED_VS_RELEASED_TASKS,
		self::TREND_LINE_FOR_ADD_QAM_WISE_CARRY_FORWARDED_TASKS
	];

	public static $c_arrintImplementedReleaseChartReportTemplates = [
		self::TREND_LINE_FOR_RELEASE_DOWNTIME,
		self::CARRY_FORWARDED_VS_RELEASED_TASKS,
		self::TREND_LINE_FOR_RCA_VS_NON_RCA_BUG_TASKS,
		self::TREND_LINE_FOR_TASK_SUMMARY,
		self::TREND_LINE_FOR_PERCENT_NON_QA_TASKS,
		self::TREND_LINE_FOR_ADD_QAM_WISE_CARRY_FORWARDED_TASKS,
		self::ADD_QAM_WISE_CARRY_FORWARDED_TASKS,
		self::ADD_QAM_WISE_AND_PRIORITY_WISE_CARRY_FORWARDED_TASKS,
		self::ADD_QAM_WISE_BIFURCATION_OF_NON_QA_TASKS,
		self::APPROX_ESTIMATION_OF_ADD_QA_WISE_BIFURCATION_OF_TEST_CASES,
		self::ADD_QAM_WISE_QA_VS_NON_QA_TASKS,
		self::URGENT_VS_IMMEDIATE_OVERDUE_BUG_TASKS,
		self::ADD_QAM_WISE_BIFURCATION_OF_QA_NOT_REQUIRED_TASKS,
		self::BUG_VS_FEATURE_TASKS_RELEASED,
		self::ADD_QAM_WISE_PERCENT_OF_NON_QA_TASKS,
		self::QA_VS_NON_QA_TASKS,
		self::ADD_QAM_WISE_RCA_VS_NON_RCA_BUG_TASKS,
		self::RCA_VS_NON_RCA_BUG_TASKS
	];

	public static $c_arrintNonClickableReleaseChartReportTemplates = [
		self::TREND_LINE_FOR_RELEASE_DOWNTIME,
		self::TREND_LINE_FOR_PERCENT_NON_QA_TASKS
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReleaseChartTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReleaseChartCategoriesId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChartDataset() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChartParameters() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>