<?php

class CSemAdGroupSource extends CBaseSemAdGroupSource {

	protected $m_intAssociatedPropertiesCount;
	protected $m_intIsSystem;
	protected $m_intDeletedBy;

	protected $m_strName;
	protected $m_strDeletedOn;
	protected $m_strSemAdGroupUpdatedOn;
	protected $m_strSemCampignRemotePrimaryKey;

	/**
	 * Get Functions
	 */

	public function getName() {
		return $this->m_strName;
	}

	public function getSemCampignRemotePrimaryKey() {
		return $this->m_strSemCampignRemotePrimaryKey;
	}

	public function getAssociatedPropertiesCount() {
		return $this->m_intAssociatedPropertiesCount;
	}

	public function getIsSystem() {
		return $this->m_intIsSystem;
	}

	public function getDeletedBy() {
		return $this->m_intDeletedBy;
	}

	public function getDeletedOn() {
		return $this->m_strDeletedOn;
	}

	public function getSemAdGroupUpdatedOn() {
		return $this->m_strSemAdGroupUpdatedOn;
	}

	/**
	 * Set Functions
	 */

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function setSemCampignRemotePrimaryKey( $strSemCampignRemotePrimaryKey ) {
		$this->m_strSemCampignRemotePrimaryKey = $strSemCampignRemotePrimaryKey;
	}

	public function setAssociatedPropertiesCount( $intAssociatedPropertiesCount ) {
		$this->m_intAssociatedPropertiesCount = $intAssociatedPropertiesCount;
	}

	public function setIsSystem( $intIsSystem ) {
		$this->m_intIsSystem = $intIsSystem;
	}

	public function setDeletedBy( $intDeletedBy ) {
		$this->m_intDeletedBy = $intDeletedBy;
	}

	public function setDeletedOn( $strDeletedOn ) {
		$this->m_strDeletedOn = $strDeletedOn;
	}

	public function setSemAdGroupUpdatedOn( $strSemAdGroupUpdatedOn ) {
		$this->m_strSemAdGroupUpdatedOn = $strSemAdGroupUpdatedOn;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['associated_properties_count'] ) ) $this->setAssociatedPropertiesCount( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['associated_properties_count'] ) : $arrmixValues['associated_properties_count'] );
		if( true == isset( $arrmixValues['sem_campign_remote_primary_key'] ) ) $this->setSemCampignRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['sem_campign_remote_primary_key'] ) : $arrmixValues['sem_campign_remote_primary_key'] );
		if( true == isset( $arrmixValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['name'] ) : $arrmixValues['name'] );
		if( true == isset( $arrmixValues['is_system'] ) ) $this->setIsSystem( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['is_system'] ) : $arrmixValues['is_system'] );
		if( true == isset( $arrmixValues['deleted_by'] ) ) $this->setDeletedBy( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['deleted_by'] ) : $arrmixValues['deleted_by'] );
		if( true == isset( $arrmixValues['deleted_on'] ) ) $this->setDeletedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['deleted_on'] ) : $arrmixValues['deleted_on'] );
		if( true == isset( $arrmixValues['sem_ad_updated_on'] ) ) $this->setSemAdGroupUpdatedOn( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['sem_ad_updated_on'] ) : $arrmixValues['sem_ad_updated_on'] );
		return;
	}

	/**
	 * Validate Functions
	 */

	public function valMaxCostPerClick() {
		$boolIsValid = true;

		if( true == is_null( $this->getMaxCostPerClick() ) || false == is_numeric( $this->getMaxCostPerClick() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_cost_per_click', 'Max cost per click is required and should be numeric.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valMaxCostPerClick();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchSemAdGroup( $objAdminDatabase ) {
		return CSemAdGroups::fetchSemAdGroupById( $this->getSemAdGroupId(), $objAdminDatabase );
	}

	public function fetchSemAdSourceByRemotePrimaryKey( $strRemotePrimaryKey, $objAdminDatabase ) {
		return CSemAdSources::fetchSemAdSourceBySemSourceIdBySemAdGroupIdByRemotePrimaryKey( $this->getSemSourceId(), $this->getSemAdGroupId(), $strRemotePrimaryKey, $objAdminDatabase );
	}

	public function fetchSemAdSourceByTitle( $strTitle, $objAdminDatabase ) {
		return CSemAdSources::fetchSemAdSourceBySemSourceIdBySemAdGroupIdByTitle( $this->getSemSourceId(), $this->getSemAdGroupId(), $strTitle, $objAdminDatabase );
	}

	public function fetchSemKeywordSourceByRemotePrimaryKey( $strRemotePrimaryKey, $objAdminDatabase ) {
		return CSemKeywordSources::fetchSemKeywordSourceBySemSourceIdBySemAdGroupIdByRemotePrimaryKey( $this->getSemSourceId(), $this->getSemAdGroupId(), $strRemotePrimaryKey, $objAdminDatabase );
	}

	public function fetchSemKeywordSourceByKeywords( $strKeywords, $objAdminDatabase ) {
		return CSemKeywordSources::fetchSemKeywordSourceBySemSourceIdBySemAdGroupIdByKeywords( $this->getSemSourceId(), $this->getSemAdGroupId(), $strKeywords, $objAdminDatabase );
	}
}
?>