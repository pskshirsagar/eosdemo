<?php

use Psi\Libraries\ExternalFileUpload\CFileUpload;

class CEmployeeApplicationDetail extends CBaseEmployeeApplicationDetail {

	protected $m_intCellNumber;
	protected $m_intOfficeId;
	protected $m_intDesignationId;
	protected $m_intPsWebsiteJobPostingId;
	protected $m_intResourceRequisitionId;
	protected $m_intPurchaseRequestId;
	protected $m_strNameFull;
	protected $m_strOfferDate;
	protected $m_strDeskNumber;
	protected $m_strCountryCode;
	protected $m_strEmailAddress;
	protected $m_strDesignationName;
	protected $m_strDepartmentName;
	protected $m_strPsWebsiteJobPostingName;
	protected $m_strRecruiterName;
	protected $m_strRequirementType;
	protected $m_strReplacementEmployeeName;

	protected $m_boolIsFormerEmployee;
	protected $m_boolIsArchived;
	protected $m_strEaUpdatedOn;
	protected $m_strEaCreatedOn;
	protected $m_intPsJobPostingStatusId;
	protected $m_boolIsRejectedEmployeeApplication;
	protected $m_boolIsLatestNewEmployeeApplication;
	protected $m_boolIsNewEmployeeApplication;
	protected $m_intEmployeeApplicationStatusTypeId;

	public function getBaseWage() {
		if( false == $this->getBaseWageEncrypted() ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getBaseWageEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
	}

	public function getBonusAmount() {
		if( false == $this->getBonusAmountEncrypted() ) {
			return NULL;
		}

		if( CCountry::CODE_USA == $this->m_strCountryCode ) {
			return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getBonusAmountEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
		} else {
			return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getBonusAmountEncrypted(), CONFIG_SODIUM_KEY_EMPLOYEE_BONUS, [ 'legacy_secret_key' => CONFIG_KEY_EMPLOYEE_BONUS ] );
		}
	}

	public function getBonusDescription() {
		if( false == $this->getBonusDescriptionEncrypted() ) {
			return NULL;
		}

		if( CCountry::CODE_USA == $this->m_strCountryCode ) {
			return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getBonusDescriptionEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
		} else {
			return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getBonusDescriptionEncrypted(), CONFIG_SODIUM_KEY_EMPLOYEE_BONUS, [ 'legacy_secret_key' => CONFIG_KEY_EMPLOYEE_BONUS ] );
		}
	}

	public function getRelocationAllowance() {
		if( false == $this->getRelocationAllowanceEncrypted() ) {
			return NULL;
		}

		if( CCountry::CODE_USA == $this->m_strCountryCode ) {
			return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getRelocationAllowanceEncrypted(), CONFIG_SODIUM_KEY_TAX_NUMBER, [ 'legacy_secret_key' => CONFIG_KEY_TAX_NUMBER ] );
		} else {
			return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getRelocationAllowanceEncrypted(), CONFIG_SODIUM_KEY_EMPLOYEE_BONUS, [ 'legacy_secret_key' => CONFIG_KEY_EMPLOYEE_BONUS ] );
		}
	}

	public function getDeskNumber() {
		return $this->m_strDeskNumber;
	}

	public function getPsWebsiteJobPostingId() {
		return $this->m_intPsWebsiteJobPostingId;
	}

	public function getPsWebsiteJobPostingName() {
		return $this->m_strPsWebsiteJobPostingName;
	}

	public function getDesignationId() {
		return $this->m_intDesignationId;
	}

	public function getOfficeId() {
		return $this->m_intOfficeId;
	}

	public function getCountryCode() {
		return $this->m_strCountryCode;
	}

	public function getNameFull() {
		return $this->m_strNameFull;
	}

	public function getDesignationName() {
		return $this->m_strDesignationName;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getRecruiterName() {
		return $this->m_strRecruiterName;
	}

	public function getCellNumber() {
		return $this->m_intCellNumber;
	}

	public function getOfferDate() {
		return $this->m_strOfferDate;
	}

	public function getIsFormerEmployee() {
		return $this->m_boolIsFormerEmployee;
	}

	public function getPurchaseRequestId() {
		return $this->m_intPurchaseRequestId;
	}

	public function getResourceRequisitionId() {
		return $this->m_intResourceRequisitionId;
	}

	public function getRequirementType() {
		return $this->m_strRequirementType;
	}

	public function getReplacementEmployeeName() {
		return $this->m_strReplacementEmployeeName;
	}

	public function getExpectedAnnualCompensation() {
		if( false == $this->getExpectedAnnualCompensationEncrypted() ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getExpectedAnnualCompensationEncrypted(), CONFIG_SODIUM_KEY_SALARY_COMPENSATION, [ 'legacy_secret_key' => CONFIG_KEY_SALARY_COMPENSATION ] );
	}

	public function getCurrentAnnualCompensation() {
		if( false == $this->getCurrentAnnualCompensationEncrypted() ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getCurrentAnnualCompensationEncrypted(), CONFIG_SODIUM_KEY_SALARY_COMPENSATION, [ 'legacy_secret_key' => CONFIG_KEY_SALARY_COMPENSATION ] );
	}

	public function getNegotiatedAnnualCompensation() {
		if( false == $this->getNegotiatedAnnualCompensationEncrypted() ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getNegotiatedAnnualCompensationEncrypted(), CONFIG_SODIUM_KEY_SALARY_COMPENSATION, [ 'legacy_secret_key' => CONFIG_KEY_SALARY_COMPENSATION ] );
	}

	public function getCriminalBackground() {
		if( false == $this->getCriminalBackgroundEncrypted() ) {
			return NULL;
		}
		return ( \Psi\Libraries\Cryptography\CCrypto::createService() )->decrypt( $this->getCriminalBackgroundEncrypted(), CONFIG_SODIUM_KEY_SALARY_COMPENSATION, [ 'legacy_secret_key' => CONFIG_KEY_SALARY_COMPENSATION ] );
	}

	public function getIsArchived() {
		return $this->m_boolIsArchived;
	}

	public function getEaCreatedOn() {
		return $this->m_strEaCreatedOn;
	}

	public function getEaUpdatedOn() {
		return $this->m_strEaUpdatedOn;
	}

	public function getPsJobPostingStatusId() {
		return $this->m_intPsJobPostingStatusId;
	}

	public function getEmployeeApplicationStatusTypeId() {
		return $this->m_intEmployeeApplicationStatusTypeId;
	}

	public function getIsRejectedEmployeeApplication() {
		return $this->m_boolIsRejectedEmployeeApplication;
	}

	public function getIsLatestNewEmployeeApplication() {
		return $this->m_boolIsLatestNewEmployeeApplication;
	}

	public function getIsNewEmployeeApplication() {
		return $this->m_boolIsNewEmployeeApplication;
	}

	public function setExperienceYear( $intExperienceYear ) {
		$this->m_intExperienceYear = $intExperienceYear;
	}

	public function setExperienceMonth( $intExperienceMonth ) {
		$this->m_intExperienceMonth = $intExperienceMonth;
	}

	public function setBaseWage( $strBaseWage ) {
		if( true == valStr( $strBaseWage ) ) {
			$this->setBaseWageEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strBaseWage, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		}
	}

	public function setBonusAmount( $strBonusAmount ) {
		$intBonusAmount = is_numeric( $strBonusAmount ) ? ( int ) abs( $strBonusAmount ) : $strBonusAmount;

		if( CCountry::CODE_USA == $this->m_strCountryCode ) {
			$this->setBonusAmountEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $intBonusAmount, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		} else {
			$this->setBonusAmountEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $intBonusAmount, CONFIG_SODIUM_KEY_EMPLOYEE_BONUS ) );
		}
	}

	public function setBonusDescription( $strBonusDescription ) {
		if( CCountry::CODE_USA == $this->m_strCountryCode ) {
			$this->setBonusDescriptionEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strBonusDescription, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		} else {
			$this->setBonusDescriptionEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strBonusDescription, CONFIG_SODIUM_KEY_EMPLOYEE_BONUS ) );
		}
	}

	public function setRelocationAllowance( $strRelocationAllowance ) {
		$intRelocationAllowance = is_numeric( $strRelocationAllowance ) ? ( int ) abs( $strRelocationAllowance ) : $strRelocationAllowance;

		if( CCountry::CODE_USA == $this->m_strCountryCode ) {
			$this->setRelocationAllowanceEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $intRelocationAllowance, CONFIG_SODIUM_KEY_TAX_NUMBER ) );
		} else {
			$this->setRelocationAllowanceEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $intRelocationAllowance, CONFIG_SODIUM_KEY_EMPLOYEE_BONUS ) );
		}
	}

	public function setDeskNumber( $strDeskNumber ) {
		$this->m_strDeskNumber = CStrings::strTrimDef( $strDeskNumber, 100, NULL, true );
	}

	public function setDesignationId( $intDesignationId ) {
		$this->m_intDesignationId = $intDesignationId;
	}

	public function setPsWebsiteJobPostingId( $intPsWebsiteJobPostingId ) {
		$this->m_intPsWebsiteJobPostingId = $intPsWebsiteJobPostingId;
	}

	public function setPsWebsiteJobPostingName( $strPsWebsiteJobPostingName ) {
		$this->m_strPsWebsiteJobPostingName = $strPsWebsiteJobPostingName;
	}

	public function setOfficeId( $intOfficeId ) {
		$this->m_intOfficeId = $intOfficeId;
	}

	public function setCountryCode( $strCountryCode ) {
		$this->m_strCountryCode = $strCountryCode;
	}

	public function setNameFull( $strNameFull ) {
		$this->m_strNameFull = $strNameFull;
	}

	public function setDesignationName( $strDesignationName ) {
		$this->m_strDesignationName = $strDesignationName;
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setRecruiterName( $strRecruiterName ) {
		$this->m_strRecruiterName = $strRecruiterName;
	}

	public function setCellNumber( $intCellNumber ) {
		$this->m_intCellNumber = $intCellNumber;
	}

	public function setOfferedDate( $strOfferDate ) {
		$this->m_strOfferDate = $strOfferDate;
	}

	public function setIsFormerEmployee( $boolIsFormerEmployee ) {
		$this->m_boolIsFormerEmployee = $boolIsFormerEmployee;
	}

	public function setPurchaseRequestId( $intPurchaseRequestId ) {
		$this->m_intPurchaseRequestId = $intPurchaseRequestId;
	}

	public function setResourceRequisitionId( $intResourceRequisitionId ) {
		$this->m_intResourceRequisitionId = $intResourceRequisitionId;
	}

	public function setRequirementType( $strRequirementType ) {
		$this->m_strRequirementType = $strRequirementType;
	}

	public function setReplacementEmployeeName( $strReplacementEmployeeName ) {
		$this->m_strReplacementEmployeeName = $strReplacementEmployeeName;
	}

	public function setExpectedAnnualCompensation( $intExpectedAnnualCompensation ) {
		if( true == valStr( $intExpectedAnnualCompensation ) ) {
			$this->setExpectedAnnualCompensationEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $intExpectedAnnualCompensation, CONFIG_SODIUM_KEY_SALARY_COMPENSATION ) );
		}
	}

	public function setCurrentAnnualCompensation( $intCurrentAnnualCompensation ) {
		if( true == valStr( $intCurrentAnnualCompensation ) ) {
			$this->setCurrentAnnualCompensationEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $intCurrentAnnualCompensation, CONFIG_SODIUM_KEY_SALARY_COMPENSATION ) );
		}
	}

	public function setNegotiatedAnnualCompensation( $strNegotiatedAnnualCompensation ) {
		$intNegotiatedAnnualCompensation = is_numeric( $strNegotiatedAnnualCompensation ) ? ( int ) abs( $strNegotiatedAnnualCompensation ) : $strNegotiatedAnnualCompensation;

		$this->setNegotiatedAnnualCompensationEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $intNegotiatedAnnualCompensation, CONFIG_SODIUM_KEY_SALARY_COMPENSATION ) );
	}

	public function setCriminalBackground( $strCriminalBackground ) {
		if( true == valStr( $strCriminalBackground ) ) {
			$this->setCriminalBackgroundEncrypted( ( \Psi\Libraries\Cryptography\CCrypto::createService() )->encrypt( $strCriminalBackground, CONFIG_SODIUM_KEY_SALARY_COMPENSATION ) );
		}
	}

	public function setIsArchived( $boolIsArchived ) {
		$this->set( 'm_boolIsArchived', CStrings::strToBool( $boolIsArchived ) );
	}

	public function setEaCreatedOn( $strEaCreatedOn ) {
		$this->m_strEaCreatedOn = $strEaCreatedOn;
	}

	public function setEaUpdatedOn( $strEaUpdatedOn ) {
		$this->m_strEaUpdatedOn = $strEaUpdatedOn;
	}

	public function setPsJobPostingStatusId( $inPsJobPostingStatusId ) {
		$this->m_intPsJobPostingStatusId = $inPsJobPostingStatusId;
	}

	public function setIsRejectedEmployeeApplication( $boolIsRejectedEmployeeApplication ) {
		$this->set( 'm_boolIsRejectedEmployeeApplication', CStrings::strToBool( $boolIsRejectedEmployeeApplication ) );
	}

	public function setIslatestNewEmployeeApplication( $boolIsLatestNewEmployeeApplication ) {
		$this->set( 'm_boolIsLatestNewEmployeeApplication', CStrings::strToBool( $boolIsLatestNewEmployeeApplication ) );
	}

	public function setIsNewEmployeeApplication( $boolIsNewEmployeeApplication ) {
		$this->set( 'm_boolIsNewEmployeeApplication', CStrings::strToBool( $boolIsNewEmployeeApplication ) );
	}

	public function setEmployeeApplicationStatusTypeId( $intEmployeeApplicationStatusTypeId ) {
		$this->set( 'm_intEmployeeApplicationStatusTypeId', CStrings::strToBool( $intEmployeeApplicationStatusTypeId ) );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['base_wage'] ) )						$this->setBaseWage( $arrmixValues['base_wage'] );
		if( true == isset( $arrmixValues['bonus_amount'] ) )					$this->setBonusAmount( $arrmixValues['bonus_amount'] );
		if( true == isset( $arrmixValues['bonus_description'] ) )				$this->setBonusDescription( $arrmixValues['bonus_description'] );
		if( true == isset( $arrmixValues['relocation_allowance'] ) )			$this->setRelocationAllowance( $arrmixValues['relocation_allowance'] );
		if( true == isset( $arrmixValues['designation_id'] ) )					$this->setDesignationId( $arrmixValues['designation_id'] );
		if( true == isset( $arrmixValues['desk_number'] ) )						$this->setDeskNumber( $arrmixValues['desk_number'] );
		if( true == isset( $arrmixValues['office_id'] ) )						$this->setOfficeId( $arrmixValues['office_id'] );
		if( true == isset( $arrmixValues['country_code'] ) )					$this->setCountryCode( $arrmixValues['country_code'] );
		if( true == isset( $arrmixValues['name_full'] ) )						$this->setNameFull( $arrmixValues['name_full'] );
		if( true == isset( $arrmixValues['designation_name'] ) )				$this->setDesignationName( $arrmixValues['designation_name'] );
		if( true == isset( $arrmixValues['department_name'] ) )					$this->setDepartmentName( $arrmixValues['department_name'] );
		if( true == isset( $arrmixValues['email_address'] ) )					$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['recruiter_name'] ) )					$this->setRecruiterName( $arrmixValues['recruiter_name'] );
		if( true == isset( $arrmixValues['cell_number'] ) )						$this->setCellNumber( $arrmixValues['cell_number'] );
		if( true == isset( $arrmixValues['offered_date'] ) )					$this->setOfferedDate( $arrmixValues['offered_date'] );
		if( true == isset( $arrmixValues['ps_website_job_posting_id'] ) )		$this->setPsWebsiteJobPostingId( $arrmixValues['ps_website_job_posting_id'] );
		if( true == isset( $arrmixValues['ps_website_job_posting_name'] ) )		$this->setPsWebsiteJobPostingName( $arrmixValues['ps_website_job_posting_name'] );
		if( true == isset( $arrmixValues['is_former_employee'] ) )				$this->setIsFormerEmployee( $arrmixValues['is_former_employee'] );
		if( true == isset( $arrmixValues['purchase_request_id'] ) )				$this->setPurchaseRequestId( $arrmixValues['purchase_request_id'] );
		if( true == isset( $arrmixValues['resource_requisitions_id'] ) )		$this->setResourceRequisitionId( $arrmixValues['resource_requisitions_id'] );
		if( true == isset( $arrmixValues['requirement_type'] ) )				$this->setRequirementType( $arrmixValues['requirement_type'] );
		if( true == isset( $arrmixValues['replacement_employee_name'] ) )		$this->setReplacementEmployeeName( $arrmixValues['replacement_employee_name'] );
		if( true == isset( $arrmixValues['is_archived'] ) )						$this->setIsArchived( $arrmixValues['is_archived'] );
		if( true == isset( $arrmixValues['negotiated_annual_compensation'] ) )	$this->setNegotiatedAnnualCompensation( $arrmixValues['negotiated_annual_compensation'] );
		if( true == isset( $arrmixValues['ea_created_on'] ) )				$this->setEaCreatedOn( $arrmixValues['ea_created_on'] );
		if( true == isset( $arrmixValues['ea_updated_on'] ) )				$this->setEaUpdatedOn( $arrmixValues['ea_updated_on'] );
		if( true == isset( $arrmixValues['ps_job_posting_status_id'] ) )	$this->setPsJobPostingStatusId( $arrmixValues['ps_job_posting_status_id'] );
		if( true == isset( $arrmixValues['is_rejected_employee_application'] ) )	$this->setIsRejectedEmployeeApplication( $arrmixValues['is_rejected_employee_application'] );
		if( true == isset( $arrmixValues['is_latest_new_employee_application'] ) )	$this->setIslatestNewEmployeeApplication( $arrmixValues['is_latest_new_employee_application'] );
		if( true == isset( $arrmixValues['is_new_employee_application'] ) )	$this->setIsNewEmployeeApplication( $arrmixValues['is_new_employee_application'] );
		if( true == isset( $arrmixValues['employee_application_status_type_id'] ) )	$this->setEmployeeApplicationStatusTypeId( $arrmixValues['employee_application_status_type_id'] );

		return;
	}

	public function valReviewCycle() {
		$boolIsValid = true;

		if( false == valStr( $this->getReviewCycle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'reivew_cycle', ' Review cycle is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valPtoDays() {
		$boolIsValid = true;

		if( true == valStr( $this->getPtoDays() ) && false == is_numeric( $this->getPtoDays() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'pto_days', ' PTO days should be numeric. ' ) );
		}

		return $boolIsValid;
	}

	public function valWorkExperience() {
		$boolIsValid = true;

		if( true == is_null( $this->getWorkExperience() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'work_experience', 'Total work experience is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valCurrentLocation() {
		$boolIsValid = true;

		if( false == isset( $this->m_strCurrentLocation ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_location', 'Current location is required. ' ) );
		}

		if( true == isset( $this->m_strCurrentLocation ) && 0 < \Psi\CStringService::singleton()->strlen( $this->m_strCurrentLocation ) && false == ctype_alpha( $this->m_strCurrentLocation ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_location', 'Current location must contains only alphabets. ' ) );
		}
		return $boolIsValid;
	}

	public function valCurrentAnnualCompensation() {

		$boolIsValid = true;

		if( true == is_null( $this->getCurrentAnnualCompensation() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Current_annual_compensation', 'Current annual compensation is required. ' ) );
		} elseif( false == is_null( $this->getCurrentAnnualCompensation() ) && false == is_numeric( $this->getCurrentAnnualCompensation() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Current_annual_compensation', 'Valid Current annual compensation required. ' ) );
		}

		return $boolIsValid;
	}

	public function valExpectedAnnualCompensation() {
		$boolIsValid = true;

		if( true == is_null( $this->getExpectedAnnualCompensation() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expected_annual_compensation', 'Expected annual compensation is required. ' ) );
		} elseif( false == is_null( $this->getExpectedAnnualCompensation() ) && false == is_numeric( $this->getExpectedAnnualCompensation() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expected_annual_compensation', 'Valid expected annual compensation required. ' ) );
		}

		return $boolIsValid;
	}

	public function valSkillSets() {
		$boolIsValid = true;

		if( false == valStr( $this->getSkillSets() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'skill_sets', 'Skills set is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valHasLegalStatus() {
		$boolIsValid = true;

		if( false == isset( $this->m_intHasLegalStatus ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'has_legal_status', 'Legal status field required. ' ) );
		}

		return $boolIsValid;
	}

	public function valJoiningDate( $strEmployeeApplicationScheduledOn = NULL, $boolIsFromDocumentTab = false ) {

		$boolIsValid = true;

		if( true == $boolIsFromDocumentTab && true == is_null( $this->getJoiningDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'joining_date', 'Joining date is required. ' ) );
		}

		if( false == is_null( $this->getJoiningDate() ) && false == is_null( $strEmployeeApplicationScheduledOn ) && ( strtotime( \Psi\CStringService::singleton()->substr( $strEmployeeApplicationScheduledOn, 0, 11 ) ) > strtotime( $this->getJoiningDate() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'joining_date', 'Joining date must be greater than scheduled on date. ' ) );
		}

		return $boolIsValid;
	}

	public function valEmployeeApplicationDetailCurrentLocation() {
		$boolIsValid = true;

		if( false == isset( $this->m_strCurrentLocation ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_location', 'Current Location is required. ' ) );
		}
		if( true == isset( $this->m_strCurrentLocation ) && false == \Psi\CStringService::singleton()->preg_match( '/^[\p{L&} -]+$/u', $this->m_strCurrentLocation ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_location', 'Current location must contains only alphabets. ' ) );
		}
		return $boolIsValid;
	}

	public function valExperience( $objDatabase = NULL, $objEmployeeApplication = NULL ) {
		$boolIsValid = true;
		if( true == isset( $objDatabase ) && true == valObj( $objDatabase, 'CDatabase' ) && true == valObj( $objEmployeeApplication, 'CEmployeeApplication' ) && false == is_null( $objEmployeeApplication->getPsWebsiteJobPostingId() ) ) {
			$objPsWebsiteJobPosting = \Psi\Eos\Admin\CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $objEmployeeApplication->getPsWebsiteJobPostingId(), $objDatabase );
			if( ( true == valObj( $objPsWebsiteJobPosting, 'CPsWebsiteJobPosting' ) && CPsWebsiteJobPosting::EXTERNAL_TRAINER == $objPsWebsiteJobPosting->getId() ) ) {
				if( 0 < $this->getExperienceYear() || 0 < $this->getExperienceMonth() ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'referee_employee_id', 'Relevant Experience should be 0 year/month. ' ) );
				} else {
					return true;
				}
			}
		}

		if( '0.0' == $this->getWorkExperience() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'work_experience_year', 'Total Experience is required. ' ) );
		}
		if( 0 == $this->getExperienceYear() && 0 == $this->getExperienceMonth() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'experience_year', 'Relevant Experience is required. ' ) );
		}
		$arrintWorkExperience = explode( '.', $this->getWorkExperience() );

		if( true == $boolIsValid && false == is_null( $this->getWorkExperience() ) && ( $arrintWorkExperience[0] < $this->getExperienceYear() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'experience_year', 'Relevant Experience should not be greater than Total Experience. ' ) );
			return $boolIsValid;
		}

		if( $arrintWorkExperience[0] == $this->getExperienceYear() && $arrintWorkExperience[1] < $this->getExperienceMonth() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'experience_month', 'Relevant Experience should not be greater than Total Experience. ' ) );
		}

		if( true == valObj( $objEmployeeApplication, 'CEmployeeApplication' ) && CCountry::CODE_INDIA == $objEmployeeApplication->getCountryCode() ) {
			$objPsWebsiteJobPosting = \Psi\Eos\Admin\CPsWebsiteJobPostings::createService()->fetchPsWebsiteJobPostingById( $objEmployeeApplication->getPsWebsiteJobPostingId(), $objDatabase );

			$intExperienceYear	= $this->getExperienceYear() . '.' . $this->getExperienceMonth();

			if( true == valObj( $objPsWebsiteJobPosting, 'CPsWebsiteJobPosting' ) && 0 < $intExperienceYear && true == valId( $objPsWebsiteJobPosting->getDetails()->max_exp ) && ( $intExperienceYear <= $objPsWebsiteJobPosting->getDetails()->min_exp || $intExperienceYear > $objPsWebsiteJobPosting->getDetails()->max_exp ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Relevant Experience should be within ' . $objPsWebsiteJobPosting->getDetails()->min_exp . '+ to ' . $objPsWebsiteJobPosting->getDetails()->max_exp . ' years. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valRelevantExperience() {

		$boolIsValid = true;

		if( 0 == $this->getExperienceYear() && 0 == $this->getExperienceMonth() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'experience_year', 'Relevant Experience is required. ' ) );
		}

		if( true == $boolIsValid && false == is_null( $this->getWorkExperience() ) && ( floatval( $this->getWorkExperience() ) < floatval( $this->getExperienceYear() . '.' . $this->getExperienceMonth() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'experience_year', 'Relevant Experience should not be greater than Total Experience. ' ) );
		}

		return $boolIsValid;
	}

	public function valBaseWage( $objEmployeeApplication ) {
		$boolIsValid = true;

		if( CCountry::CODE_USA == $objEmployeeApplication->getCountryCode() && ( false == valStr( $this->getBaseWage() ) || '0' == $this->getBaseWage() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_wage', ' Base wage is required. ' ) );
		}

		if( true == valStr( $this->getBaseWage() ) && false == is_numeric( $this->getBaseWage() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'base_wage', ' Base wage should be numeric. ' ) );
		}

		return $boolIsValid;
	}

	public function valBonusAmount() {
		if( true == valStr( $this->getBonusAmount() ) && false == is_numeric( $this->getBonusAmount() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bonus_amount', ' Bonus amount should be numeric. ' ) );
			return false;
		}

		return true;
	}

	public function valBonusDescription() {
		$boolIsValid = true;

		if( true == valStr( $this->getBonusDescription() ) && false == valStr( $this->getBonusAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'bonus_amount', ' Bonus amount is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valRelocationAllowance() {
		if( true == valStr( $this->getRelocationAllowance() ) && false == is_numeric( $this->getRelocationAllowance() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'relocation_allowance', ' Relocation allowance should be numeric.' ) );
			return false;
		}

		return true;
	}

	public function valStartDate( $boolIsRequired = false ) {
		$boolIsValid			= true;

		if( true == $boolIsRequired && true == empty( $this->m_strJoiningDate ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'joining_date', ' Start date is required. ' ) );
		}

		if( true == isset( $this->m_strJoiningDate ) && false == CValidation::isValidDate( __( '{%t,0,DATE_NUMERIC_MMDDYYYY}', [ $this->m_strJoiningDate ] ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'joining_date', ' Invalid start date. ' ) );
		}

		return $boolIsValid;
	}

	public function valQuestionarie( $objEmployeeApplication ) {
		$boolIsValid = true;

		if( false == valStr( $this->getPsFamiliarityDescription() ) || true == is_null( $this->getExpectedAnnualCompensation() ) || false == valStr( $this->getLegalAgreementDescription() ) || true == is_null( $this->getCurrentAnnualCompensation() ) || true == is_null( $this->getExpectedEmploymentDays() ) || ( true == valObj( $objEmployeeApplication, 'CEmployeeApplication' ) && CEmployeeApplicationSource::ENTRATA_EMPLOYEE == $objEmployeeApplication->getEmployeeApplicationSourceId() && true == is_null( $objEmployeeApplication->getReferredByName() ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_familiarity_description', 'Answer all * marked questions of Questionnarie section. ' ) );
		}

		if( true == valObj( $objEmployeeApplication, 'CEmployeeApplication' ) && CEmployeeApplicationSource::ENTRATA_EMPLOYEE == $objEmployeeApplication->getEmployeeApplicationSourceId() && true == \Psi\CStringService::singleton()->preg_match( '/[^A-Za-z\s]/', $objEmployeeApplication->getReferredByName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expected_annual_compensation', 'Enter valid Entrata employee name. ' ) );
		}

		if( false == is_null( $this->getExpectedAnnualCompensation() ) && false == is_numeric( $this->getExpectedAnnualCompensation() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'current_annual_compensation', 'Current total compensation should be numeric. ' ) );
		}

		if( false == is_null( $this->getExpectedAnnualCompensation() ) && false == is_numeric( $this->getExpectedAnnualCompensation() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'expected_annual_compensation', 'Total compensation expectations should be numeric. ' ) );
		}

		return $boolIsValid;
	}

	public function valCoverLetterFileType() {

		$boolIsValid = true;

		$intMaxUpload 	= 20 * 1024 * 1024;

		$arrstrMimeTypes 	= array( '', 'application/rtf', 'application/octet-stream', 'application/msword', 'application/ms-word', 'application/pdf', 'application/x-pdf', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' );

		if( true == array_key_exists( 'employee_cover_letter', $_FILES ) && false == empty( $_FILES['employee_cover_letter']['name'] ) ) {

			$strExtension  	= \Psi\CStringService::singleton()->substr( $_FILES['employee_cover_letter']['name'], ( \Psi\CStringService::singleton()->strrpos( $_FILES['employee_cover_letter']['name'], '.' ) + 1 ) );

			if( 0 != \Psi\CStringService::singleton()->strlen( $strExtension ) && ( 'pdf' != \Psi\CStringService::singleton()->strtolower( $strExtension ) && 'docx' != \Psi\CStringService::singleton()->strtolower( $strExtension ) ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_cover_letter']['name'] . ' is not a supported extension. Please upload .pdf or .docx file for cover letter. ', NULL ) );
				return false;
			}

			if( false == in_array( $_FILES['employee_cover_letter']['type'], $arrstrMimeTypes ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_cover_letter']['name'] . ' is not a supported extension. Please upload .pdf or .docx file for cover letter. ', NULL ) );
				return false;
			}

			if( $_FILES['employee_cover_letter']['size'] > $intMaxUpload || UPLOAD_ERR_INI_SIZE == $_FILES['employee_cover_letter']['error'] ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_cover_letter']['name'] . ' File failed to upload. File size is greater than permitted size of 2MB.', NULL ) );
				return false;
			}
		}

		return $boolIsValid;
	}

	public function valExpectedEmploymentDays() {
		$boolValid = true;

		if( false == valId( $this->getExpectedEmploymentDays() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, 'expected_employment_days', 'Notice period is required. ', NULL ) );
		}

		return $boolValid;
	}

	public function valNegotiatedAmount() {
		if( true == valStr( $this->getNegotiatedAnnualCompensation() ) && false == is_numeric( $this->getNegotiatedAnnualCompensation() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'negotiated_annual_compensation', ' Negotiated amount should be numeric.' ) );
			return false;
		}

		return true;
	}

	public function validate( $strAction, $objEmployeeApplication = NULL, $objDatabase = NULL, $boolIsFromDocumentTab = false, $boolSendEditLink = false, $boolCheckLocation = true ) {

		$boolIsValid = true;
		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valHasLegalStatus();
				$boolIsValid &= $this->valExpectedAnnualCompensation();
				break;

			case 'validate_employee_application_detail_fresher':
				$boolIsValid &= $this->valHasLegalStatus();

				if( true == $boolCheckLocation ) {
					$boolIsValid &= $this->valEmployeeApplicationDetailCurrentLocation();
				}

				if( true == valObj( $objEmployeeApplication, 'CEmployeeApplication' ) ) {
					$boolIsValid &= $this->valJoiningDate( $objEmployeeApplication->getScheduledOn() );
				}
				break;

			case 'validate_employee_application_detail':
				if( false == $boolIsFromDocumentTab ) {
					$boolIsValid &= $this->valHasLegalStatus();

					if( true == $boolCheckLocation ) {
						$boolIsValid &= $this->valEmployeeApplicationDetailCurrentLocation();
					}

					if( false == $boolSendEditLink ) {
						$boolIsValid &= $this->valCurrentAnnualCompensation();
						$boolIsValid &= $this->valExpectedAnnualCompensation();
						$boolIsValid &= $this->valExperience( $objDatabase, $objEmployeeApplication );
					}
				}

				if( true == valObj( $objEmployeeApplication, 'CEmployeeApplication' ) ) {
					$boolIsValid &= $this->valJoiningDate( $objEmployeeApplication->getScheduledOn(), $boolIsFromDocumentTab );
				}
				break;

			case 'validate_employee_application_detail_experience':
				$boolIsValid &= $this->valHasLegalStatus();
				$boolIsValid &= $this->valCurrentLocation();
				$boolIsValid &= $this->valCurrentAnnualCompensation();
				$boolIsValid &= $this->valExpectedAnnualCompensation();
				$boolIsValid &= $this->valExperience();

				if( true == valObj( $objEmployeeApplication, 'CEmployeeApplication' ) ) {
					$boolIsValid &= $this->valJoiningDate( $objEmployeeApplication->getScheduledOn() );
				}
				break;

			case 'validate_xento_employee_application':
				$boolIsValid &= $this->valWorkExperience();
				break;

			case 'validate_employee_experience':
				if( CEmploymentOccupationType::PROFESSIONAL == $objEmployeeApplication->getEmploymentOccupationTypeId() ) {
					$boolIsValid &= $this->valExperience( $objDatabase, $objEmployeeApplication );
				}
				break;

			case 'validate_offer_details':
				$boolIsValid &= $this->valBaseWage( $objEmployeeApplication );
				$boolIsValid &= $this->valBonusAmount();
				$boolIsValid &= $this->valBonusDescription();
				$boolIsValid &= $this->valPtoDays();
				$boolIsValid &= $this->valRelocationAllowance();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valNegotiatedAmount();
				break;

			case 'validate_offer_accepted_india':
				$boolIsValid &= $this->valStartDate( true );
				$boolIsValid &= $this->valReviewCycle();
				break;

			case 'validate_employee_application_details':
				$boolIsValid &= $this->valHasLegalStatus();
				$boolIsValid &= $this->valQuestionarie( $objEmployeeApplication );
				break;

			case 'validate_employee_cover_letter':
				$boolIsValid &= $this->valCoverLetterFileType();
				break;

			case 'validate_applicant_basic_details':
				$boolIsValid &= $this->valSkillSets();
				break;

			case 'validate_applicant_professional_details':
				$boolIsValid &= $this->valWorkExperience();
				$boolIsValid &= $this->valExperience( $objDatabase, $objEmployeeApplication );
				break;

			case 'validate_applicant_resume_details':
				if( CEmploymentOccupationType::PROFESSIONAL == $objEmployeeApplication->getEmploymentOccupationTypeId() ) {
					$boolIsValid &= $this->valCurrentAnnualCompensation();
					$boolIsValid &= $this->valExpectedAnnualCompensation();
					$boolIsValid &= $this->valExpectedEmploymentDays();
				}

				$boolIsValid &= $this->valCoverLetterFileType();
				break;

			case 'validate_mandatory_details':
				$boolIsValid &= $this->valCurrentLocation();
				if( CEmploymentOccupationType::PROFESSIONAL == $objEmployeeApplication->getEmploymentOccupationTypeId() ) {
					$boolIsValid &= $this->valCurrentAnnualCompensation();
					$boolIsValid &= $this->valExpectedAnnualCompensation();
					$boolIsValid &= $this->valWorkExperience();
					$boolIsValid &= $this->valExperience();
				}
				break;

			case 'validate_applicant_details':
				$boolIsValid &= $this->valRelevantExperience();
				$boolIsValid &= $this->valCurrentAnnualCompensation();
				$boolIsValid &= $this->valExpectedAnnualCompensation();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			default:
			// no validation avaliable for delete and update action
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolValid = $this->valCoverLetterFileType();

		if( true == $boolValid && false == empty( $_FILES['employee_cover_letter']['name'] ) ) {
			$objPsDocument		= new CPsDocument();
			$objPsDocument->setCid( CClient::ID_DEFAULT );
			$objPsDocument->setPsDocumentTypeId( CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_APPLICATION_RESUME );
			$objPsDocument->setDescription( 'employee_cover_letter/' . $this->getId() );
			$intPsDocumentId	= $objPsDocument->fetchNextId( $objDatabase );
			$objPsDocument->setId( $intPsDocumentId );
			$arrstrPathInfo		= pathinfo( $_FILES['employee_cover_letter']['name'] );
			$objFileExtension	= \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrPathInfo['extension'], $objDatabase );

			if( true == valObj( $objPsDocument, 'CPsDocument' ) && true == valObj( $objFileExtension, 'CFileExtension' ) ) {
				$objPsDocument->setFileExtensionId( $objFileExtension->getId() );
				$objPsDocument->setEmployeeApplicationId( $this->getEmployeeApplicationId() );
				$strFileName = $arrstrPathInfo['filename'] . '.' . $arrstrPathInfo['extension'];
				$objPsDocument->setFileName( $strFileName );
				$objPsDocument->setTitle( $arrstrPathInfo['filename'] );
				$boolValid = $objPsDocument->insert( $intCurrentUserId, $objDatabase );
				$this->setPsDocumentId( $intPsDocumentId );
			}
			$boolValid = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			if( true == $boolValid ) {
				$boolValid = $this->uploadFile( $objPsDocument, $objDatabase );
			}
		} else {
			$boolValid = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return $boolValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$boolValid		= $this->valCoverLetterFileType();
		$boolIsInsert	= false;

		if( true == $boolValid && false == empty( $_FILES['employee_cover_letter']['name'] ) ) {

			$intPsDocumentId = $this->getPsDocumentId();

			if( true == empty( $intPsDocumentId ) ) {
				$objPsDocument = new CPsDocument();

				$intPsDocumentId  = $objPsDocument->fetchNextId( $objDatabase );
				$objPsDocument->setId( $intPsDocumentId );

				$this->setPsDocumentId( $intPsDocumentId );
				$boolIsInsert = true;
			} else {
				$objPsDocument = \Psi\Eos\Admin\CPsDocuments::createService()->fetchPsDocumentById( $intPsDocumentId, $objDatabase );
			}

			if( true == valObj( $objPsDocument, 'CPsDocument' ) ) {
				$objPsDocument->setPsDocumentTypeId( CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_APPLICATION_RESUME );
				$objPsDocument->setDescription( 'employee_cover_letter/' . $this->getId() );
				$objPsDocument->setEmployeeApplicationId( $this->getEmployeeApplicationId() );
				$objPsDocument->setCid( CClient::ID_DEFAULT );
			}

			$arrstrPathInfo = pathinfo( $_FILES['employee_cover_letter']['name'] );
			$objFileExtension = \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrPathInfo['extension'], $objDatabase );

			if( true == valObj( $objPsDocument, 'CPsDocument' ) ) {
				$objPsDocument->setFileExtensionId( $objFileExtension->getId() );
				$objPsDocument->setTitle( $arrstrPathInfo['filename'] );

				if( true == $boolIsInsert ) {
					$objPsDocument->setFileName( $arrstrPathInfo['basename'] );
					$boolValid = $objPsDocument->insert( $intCurrentUserId, $objDatabase );
				} else {
					$boolValid = $objPsDocument->update( $intCurrentUserId, $objDatabase );
				}
			}
			$boolValid = $this->uploadFile( $objPsDocument, $objDatabase );

		}
		$boolValid = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		return $boolValid;
	}

	public function uploadFile( $objPsDocument, $objDatabase ) {
		$boolValid		= true;

		if( false === valStr( $_FILES['employee_cover_letter']['tmp_name'] ) || false === file_exists( $_FILES['employee_cover_letter']['tmp_name'] ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_cover_letter']['name'] . ' Unable to upload cover letter file. ', NULL ) );
			return false;
		}

		$objStorageGateway					= CObjectStorageGatewayFactory::createObjectStorageGateway( CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_MULTI_SOURCED );

		// Delete object in case of update file.
		$objStoredObject				= $objPsDocument->fetchStoredObject( $objDatabase );
		$arrmixGatewayRequest			= $objStoredObject->createGatewayRequest( [ 'mountSystem' => 'NON_BACKUP_MOUNTS' ] );
		$objDeleteObjectResponse 		= $objStorageGateway->deleteObject( $arrmixGatewayRequest );
		if( true == $objDeleteObjectResponse->hasErrors() || ( true === valId( $objStoredObject->getId() ) && false == $objPsDocument->deleteStoredObject( CUSER::ID_SYSTEM, $objDatabase ) ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $_FILES['employee_cover_letter']['name'] . ' Unable to delete existing cover letter file. ', NULL ) );
			return false;
		}

		$arrstrPathInfo		= pathinfo( $_FILES['employee_cover_letter']['name'] );
		$strFileName		= 'cover_letter_' . $this->getEmployeeApplicationId() . '_' . strtotime( 'now' ) . '.' . $arrstrPathInfo['extension'];
		$objPsDocument->setFileName( $strFileName );

		$arrmixGatewayRequest				= $objPsDocument->createPutGatewayRequest( [ 'data' => file_get_contents( $_FILES['employee_cover_letter']['tmp_name'] ) ] );
		$objObjectStorageGatewayResponse	= $objStorageGateway->putObject( $arrmixGatewayRequest );
		if( true === $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Unable to upload cover letter.' ) ) );
			return false;
		}

		if( false === $objPsDocument->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse )->update( CUSER::ID_SYSTEM, $objDatabase ) ) {
			$boolValid = false;
		}
		return $boolValid;
	}

}
?>