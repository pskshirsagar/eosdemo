<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CInductionSessions
 * Do not add any new functions to this class.
 */

class CInductionSessions extends CBaseInductionSessions {

	public static function fetchActiveInductionSessionsByInductionTypeId( $intInductionTypeId, $objDatabase ) {
		if( true == is_null( $intInductionTypeId ) ) return NULL;

		$strSql = ' SELECT
						id,
						dependant_session_id,
						training_id,
						name,
						week_day,
						EXTRACT ( \'hours\' FROM start_time ) as start_time_hour,
						EXTRACT ( \'min\' FROM start_time ) as start_time_min,
						EXTRACT ( \'hours\' FROM end_time ) as end_time_hour,
						EXTRACT ( \'min\' FROM  end_time ) as end_time_min
					FROM
						induction_sessions
					WHERE
						induction_type_id = ' . ( int ) $intInductionTypeId . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchActiveInductionSessionsByInductionTypeIdByDepartmentId( $intInductionTypeId, $intDepartmentId, $objDatabase, $intTechnologyId = NULL ) {
		if( true == is_null( $intInductionTypeId ) || true == is_null( $intDepartmentId ) ) return NULL;

		$strWhere = '';

		if( false == is_null( $intTechnologyId ) ) {
			$strWhere = ' AND isa.technology_id = ' . ( int ) $intTechnologyId;
		}

		$strSql = ' SELECT
						iss.id,
						iss.training_id,
						iss.name,
						iss.week_day,
						EXTRACT ( \'hours\' FROM iss.start_time ) as start_time_hour,
						EXTRACT ( \'min\' FROM iss.start_time ) as start_time_min,
						EXTRACT ( \'hours\' FROM iss.end_time ) as end_time_hour,
						EXTRACT ( \'min\' FROM  iss.end_time ) as end_time_min
					FROM
						induction_sessions iss
						JOIN induction_session_associations isa ON ( iss.id = isa.induction_session_id )
					WHERE
						induction_type_id = ' . ( int ) $intInductionTypeId . '
						AND iss.deleted_by IS NULL
						AND iss.deleted_on IS NULL
						AND isa.department_id = ' . ( int ) $intDepartmentId . $strWhere;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchInductionSessionsByIds( $arrintInductionSessionIds, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {
		if( false == valArr( $arrintInductionSessionIds ) ) {
			return NULL;
		}

		$strOrderBy = NULL;

		if( true == valStr( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . $strOrderByField;
			if( true == valStr( $strOrderByType ) ) {
				$strOrderBy .= ' ' . $strOrderByType;
			}
		}

		$strSql = ' SELECT
						*
					FROM
						induction_sessions
					WHERE
						id IN ( ' . implode( ',', $arrintInductionSessionIds ) . ')' . $strOrderBy;

		return parent::fetchInductionSessions( $strSql, $objDatabase );
	}

}
?>