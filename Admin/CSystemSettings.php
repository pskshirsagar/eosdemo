<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSystemSettings
 * Do not add any new functions to this class.
 */

class CSystemSettings extends CBaseSystemSettings {

	const CACHE_SYSTEM_SETTINGS 		= 86400;

	public static function fetchSystemSettingByKey( $strKey, $objDatabase ) {
		return self::fetchSystemSetting( 'SELECT * FROM system_settings WHERE key=\'' . trim( addslashes( $strKey ) ) . '\' ORDER BY id DESC LIMIT 1;', $objDatabase );
	}

	public static function fetchSystemSettingsKeyedByKey( $objDatabase, $arrstrExcludeKeys = NULL ) {
		$strWhereClause = ( true == valArr( $arrstrExcludeKeys ) ) ? ' WHERE key NOT IN ( ' . implode( ',', $arrstrExcludeKeys ) . ' )' : '';

		$strSql = 'SELECT * FROM system_settings' . $strWhereClause;

		$arrobjSystemSettings = self::fetchSystemSettings( $strSql, $objDatabase );

		$arrobjKeyedSystemSettings = array();

		if( true == valArr( $arrobjSystemSettings ) ) {
			foreach( $arrobjSystemSettings as $objSystemSetting ) {
				$arrobjKeyedSystemSettings[$objSystemSetting->getKey()] = $objSystemSetting;
			}
			return $arrobjKeyedSystemSettings;
		}
		return $arrobjSystemSettings;
	}

	public static function fetchCachedSystemSettingByKey( $strKey, $objDatabase ) {
		$strSql = 'SELECT * FROM system_settings WHERE key=\'' . trim( addslashes( $strKey ) ) . '\'';

		return self::fetchCachedObject( $strSql, 'CSystemSetting', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = 300, $boolIsAppendDatabaseId = false );
	}

	public static function fetchSystemSettingValueByKeys( $arrstrKeys, $objDatabase, $boolRemoveCacheObject = NULL ) {

		if( false == valArr( $arrstrKeys ) ) return;

		$strSql = 'SELECT key, value FROM system_settings WHERE key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';
		$strCacheKey = $strSql . date( 'm/1/Y' );
		if( true == $boolRemoveCacheObject ) CCache::removeObject( $strCacheKey );

		// Fetch units data and cache it for specified hours.
		$arrstrData = fetchOrCacheData( $strSql, self::CACHE_SYSTEM_SETTINGS, $strCacheKey, $objDatabase );

		return $arrstrData;
	}

	public static function fetchSystemSettingsByKeys( $arrstrKeys, $objDatabase ) {

		if( false == valArr( $arrstrKeys ) ) return;

		$strSql = 'SELECT * FROM system_settings WHERE key IN ( \'' . implode( '\',\'', $arrstrKeys ) . '\' )';

		return self::fetchSystemSettings( $strSql, $objDatabase );
	}

}
?>