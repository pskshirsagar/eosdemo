<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestQuestions
 * Do not add any new functions to this class.
 */

class CTestQuestions extends CBaseTestQuestions {

	public static function fetchTestQuestionsByTestQuestionsFilter( $objTestQuestionsFilter, $objDatabase, $intCurrentUserId = NULL, $boolAllowPhpCertificationTest = NULL, $boolAllowQaCertificationTest = NULL, $boolAllowDbaCertificationTest = NULL ) {

		$objTestQuestionsFilter = ( true == valObj( $objTestQuestionsFilter, 'CQuestionsFilter' ) ) ? $objTestQuestionsFilter : new CQuestionsFilter();
		$strTableNames 		= 'test_questions tq';
		if( valStr( $objTestQuestionsFilter->getRequiredFields() ) ) $strFields = $objTestQuestionsFilter->getRequiredFields();
		else {
			$strFields 		= 'tq.*, CASE WHEN tq.is_render_html IS TRUE THEN 1 ELSE 0 END AS is_render_html';
		}
		$strCustomFields	= '';
		$arrstrCustomFields	= array();
		$strConditions 		= '';
		$strSortBy			= '';
		$strGroupBy			= '';

		if( valStr( $objTestQuestionsFilter->getGroupBy() ) ) {
			$strGroupBy = ' GROUP BY ' . $objTestQuestionsFilter->getGroupBy();
		}

		if( true == $objTestQuestionsFilter->getIsFromListingPage() ) $strSortBy = 'ORDER BY tq.updated_on DESC';

		$intOffset			= ( 0 < $objTestQuestionsFilter->getPageNo() ) ? ( int ) $objTestQuestionsFilter->getPageSize() * ( $objTestQuestionsFilter->getPageNo() - 1 ) : 0;
		$intLimit			= ( int ) $objTestQuestionsFilter->getPageSize();

		$arrstrAndSearchParameters = self::fetchSearchCriteria( $objTestQuestionsFilter, $intCurrentUserId, $boolAllowPhpCertificationTest, $boolAllowQaCertificationTest, $boolAllowDbaCertificationTest );

		if( true == valStr( $objTestQuestionsFilter->getOrderByField() ) )
			$strSortBy = self::fetchSortByCriteria( $objTestQuestionsFilter );

		if( true == valArr( $objTestQuestionsFilter->getCustomFields() ) ) {
			foreach( $objTestQuestionsFilter->getCustomFields() as $strCustomFieldName ) {
				switch( $strCustomFieldName ) {
					case 'test_question_groups':
						$arrstrCustomFields['test_question_groups'] = 'tqg.name as test_question_group';
						$strTableNames .= ' JOIN test_question_groups tqg ON( tqg.id = tq.test_question_group_id)';
						break;

					case 'test_question_types':
						$arrstrCustomFields['test_question_types'] = ' tqt.name as test_question_type';
						$strTableNames .= '	JOIN test_question_types tqt ON( tqt.id = tq.test_question_type_id )';
						break;

					case 'test_level_types':
						$arrstrCustomFields['test_level_types'] = ' tlt.name as test_level_type';
						$strTableNames .= '	JOIN test_level_types tlt ON( tlt.id = tq.test_level_type_id )';
						break;

					default:
						// default case
						break;
				}
			}

			if( true == valArr( $arrstrCustomFields ) ) {
				$strCustomFields = implode( ',', $arrstrCustomFields );
			}
		}

		$strConditions	.= ( true == valArr( $arrstrAndSearchParameters ) ) ? implode( ' AND ', $arrstrAndSearchParameters ) : '';

		$strOffSetCondition	= ( 0 < $intLimit ) ? ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit : '';

		switch( $objTestQuestionsFilter->getReturnCriteria() ) {
			case CEosFilter::RETURN_CRITERIA_COUNT:
				$strSql = 'SELECT COUNT(tq.id) FROM ' . $strTableNames . ' WHERE ' . $strConditions;
				$arrintCount = fetchData( $strSql, $objDatabase );
				return ( true == isset ( $arrintCount[0]['count'] ) ) ? ( int ) $arrintCount[0]['count'] : 0;
				break;

			case CEosFilter::RETURN_CRITERIA_CUSTOM_ARRAY:
				if( true == valStr( $strCustomFields ) ) {
					$strSql = 'SELECT ' . $strCustomFields . ' FROM ' . $strTableNames . ' WHERE ' . $strConditions . $strSortBy . $strOffSetCondition;
				}

			case CEosFilter::RETURN_CRITERIA_ARRAY:

				if( true == valStr( $strFields ) ) {
					$strFields	= ( true == valStr( $strCustomFields ) ) ? $strCustomFields . ', ' . $strFields : $strFields;
					$strSql 	= 'SELECT ' . $strFields . ' FROM ' . $strTableNames . ' WHERE ' . $strConditions . $strGroupBy . $strSortBy . $strOffSetCondition;
				}

				return fetchData( $strSql, $objDatabase );
				break;

			default:
				$strFields	= ( true == valStr( $strCustomFields ) ) ? $strCustomFields . ', ' . $strFields : $strFields;
				$strSql		= 'SELECT ' . $strFields . ' FROM ' . $strTableNames . ' WHERE ' . $strConditions . $strSortBy;

				if( true == $objTestQuestionsFilter->getReturnSingleRecord() ) {
					$strSql = $strSql . ' LIMIT 1;';
					return parent::fetchTestQuestion( $strSql, $objDatabase );
				}

				$strSql .= $strOffSetCondition;
				return parent::fetchTestQuestions( $strSql, $objDatabase );
				break;
		}
	}

	public static function fetchUserSubmittedTestQuestions( $intTestSubmissionId, $objDatabase ) {
		$strSql = 'SELECT
						tq.*,
						CASE WHEN tq.is_render_html IS TRUE THEN 1 ELSE 0 END AS is_render_html
					FROM
						test_questions tq
						JOIN test_submission_answers tsa ON ( tsa.test_submission_id =' . ( int ) $intTestSubmissionId . '  AND tq.id = tsa.test_question_id )
					ORDER BY
						tq.order_num';

		return self::fetchTestQuestions( $strSql, $objDatabase );
	}

	public static function fetchTestQuestionByQuestionTypeByQuestionGroupTestLevel( $arrintTestQuestionIds, $intQuestionType, $intQuestionGroup, $intTestLevelTypeId, $objDatabase, $boolShowPrivateQuestions = false, $boolShowSecurityQuestions = false ) {
		$strWhereCondition = '';
		if( true == valArr( $arrintTestQuestionIds ) )
			$strWhereCondition .= ' AND tq.id NOT IN ( ' . implode( ',', $arrintTestQuestionIds ) . ' )';

		if( true == $boolShowSecurityQuestions && false == $boolShowPrivateQuestions ) {
			$strWhereCondition .= ' AND ( tq.is_public = 1 OR ( tq.is_public = 0 AND tq.test_question_group_id = ' . CTestQuestionGroup::SECURITY . ' ) ) ';
		} elseif( false == $boolShowPrivateQuestions ) {
			$strWhereCondition .= ' AND tq.is_public = 1 ';
		} else {
			$strWhereCondition .= ' AND tq.is_public = 0 ';
		}

		$strSql = 'SELECT * FROM ( SELECT
										DISTINCT tq.id
									FROM
										test_questions tq
									WHERE
										tq.test_question_type_id = ' . ( int ) $intQuestionType . '
										AND tq.test_question_group_id = ' . ( int ) $intQuestionGroup . '
										AND tq.test_level_type_id = ' . ( int ) $intTestLevelTypeId . '
										AND tq.deleted_by IS NULL' . $strWhereCondition . '
										AND tq.is_published = 1

			 					) t ORDER BY RANDOM()
									LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSearchCriteria( $objTestQuestionsFilter, $intCurrentUserId, $boolAllowPhpCertificationTest, $boolAllowQaCertificationTest, $boolAllowDbaCertificationTest ) {
		$arrstrAndSearchParameters			= [];
		$strExcludedTestQuestionGroupIds	= '';

		if( true == is_null( $objTestQuestionsFilter->getIsDeleted() ) )
			$arrstrAndSearchParameters[] = ' tq.deleted_by IS NULL ';

		// to exclude php test question group
		if( false == is_null( $boolAllowPhpCertificationTest ) && false == $boolAllowPhpCertificationTest ) {
			$strExcludedTestQuestionGroupIds .= implode( ',', array_keys( CTestQuestionGroup::$c_arrstrPhpTestGroups ) );
		}

		// to exclude qa test question group
		if( false == is_null( $boolAllowQaCertificationTest ) && false == $boolAllowQaCertificationTest ) {
			$strExcludedTestQuestionGroupIds .= ',' . implode( ',', array_keys( CTestQuestionGroup::$c_arrstrQaTestGroups ) );
		}

		// to exclude dba test question group
		if( false == is_null( $boolAllowDbaCertificationTest ) && false == $boolAllowDbaCertificationTest ) {
			$strExcludedTestQuestionGroupIds .= ',' . implode( ',', array_keys( CTestQuestionGroup::$c_arrstrDbaTestGroups ) );
		}

		if( true == valStr( trim( $strExcludedTestQuestionGroupIds, ',' ) ) ) {
			$arrstrAndSearchParameters[] = ' tq.test_question_group_id NOT IN ( ' . trim( $strExcludedTestQuestionGroupIds, ',' ) . ' ) ';
		}

		if( true == valArr( $objTestQuestionsFilter->getSearchCriteria() ) )
			$arrstrAndSearchParameters[] = ' tq.question ILIKE E\'%' . addslashes( implode( '%\' AND tq.question ILIKE E\'%', $objTestQuestionsFilter->getSearchCriteria() ) ) . '%\'';

		if( false == is_null( $objTestQuestionsFilter->getQuestionType() ) )
			$arrstrAndSearchParameters[] = ' tqt.id = ' . ( int ) $objTestQuestionsFilter->getQuestionType();

		if( false == is_null( $objTestQuestionsFilter->getQuestionGroup() ) )
			$arrstrAndSearchParameters[] = ' tqg.id = ' . ( int ) $objTestQuestionsFilter->getQuestionGroup();

		if( false == is_null( $objTestQuestionsFilter->getQuestionLevel() ) )
			$arrstrAndSearchParameters[] = ' tlt.id = ' . ( int ) $objTestQuestionsFilter->getQuestionLevel();

		if( true == valStr( $objTestQuestionsFilter->getQuestionName() ) )
			$arrstrAndSearchParameters[] = ' tq.question ILIKE E\'%' . addslashes( $objTestQuestionsFilter->getQuestionName() ) . '%\'';

		if( valArr( $objTestQuestionsFilter->getQuestionIds() ) )
			$arrstrAndSearchParameters[] = ' tq.id IN ( ' . implode( ',', $objTestQuestionsFilter->getQuestionIds() ) . ' ) ';

		if( true == $objTestQuestionsFilter->getIsSecurityTestGreader() && false == $objTestQuestionsFilter->getIsShowPrivate() ) {
			$arrstrAndSearchParameters[] = ( false == is_null( $intCurrentUserId ) ) ? ' ( tq.is_public = 1 OR tq.created_by = ' . ( int ) $intCurrentUserId . ' OR (  tq.is_public = 0 AND tq.test_question_group_id = ' . CTestQuestionGroup::SECURITY . ' ) ) ' : ' ( tq.is_public = 1 OR (  tq.is_public = 0 AND tq.test_question_group_id = ' . CTestQuestionGroup::SECURITY . ' ) ) ';
		} elseif( false == $objTestQuestionsFilter->getIsShowPrivate() ) {
			$arrstrAndSearchParameters[] = ( false == is_null( $intCurrentUserId ) ) ? ' ( tq.is_public = 1 OR tq.created_by = ' . ( int ) $intCurrentUserId . ' ) ' : ' tq.is_public = 1 ';
		}

		if( true == $objTestQuestionsFilter->getIsShowPublished() ) {
			$arrstrAndSearchParameters[] = ( false == is_null( $intCurrentUserId ) ) ? ' ( tq.is_published = 1 OR tq.created_by = ' . ( int ) $intCurrentUserId . ' ) ' : ' tq.is_published = 1 ';
		}

		return $arrstrAndSearchParameters;
	}

	public static function fetchSortByCriteria( $objTestQuestionsFilter ) {
		$strSortBy = ' ORDER BY ';

		switch( $objTestQuestionsFilter->getOrderByField() ) {

			case 'question_name':
				$strSortBy .= ' tq.';
				break;

			case 'test_question_group':
				$strSortBy .= ' tqg.';
				break;

			case 'test_question_type':
				$strSortBy .= ' tqt.';
				break;

			case 'test_question_level':
				$strSortBy .= ' tlt.';
				break;

			default:
				$strSortBy .= '';
				break;
		}

		if( 'question_name' == $objTestQuestionsFilter->getOrderByField() ) {
			$strSortBy .= 'question ' . $objTestQuestionsFilter->getOrderByType();
		} else {
			$strSortBy .= 'name ' . $objTestQuestionsFilter->getOrderByType();
		}

		return $strSortBy;
	}

	public static function fetchTestQuestionsByTestIdByGroupId( $intTestId, $intTestQuestionGroupId, $objDatabase ) {

		$strSql = 'SELECT
						tq.*
					FROM
						test_question_associations tqa
						JOIN test_questions tq ON ( tq.id = tqa.test_question_id )
					WHERE
						tqa.test_id = ' . ( int ) $intTestId . '
						AND tq.deleted_on IS NULL
						AND tq.test_question_group_id = ' . ( int ) $intTestQuestionGroupId . '
					ORDER BY
						tq.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestQuestionIdsCountByTestSubmissionId( $intTestSubmissionId, $objDatabase ) {

		$strSql = 'SELECT
						count ( tsa.test_question_id )
					FROM
						test_submission_answers tsa
						JOIN test_questions tq ON tq.id = tsa.test_question_id
					WHERE
						( 
							( tsa.submission_answer IS NULL
								AND tq.test_question_type_id = ' . CTestQuestionType::OPEN_ANSWER . '
							)
							OR ( tsa.file_path IS NULL
								AND tq.test_question_type_id = ' . CTestQuestionType::AUDIO_ANSWERS . '
							)
							OR tq.test_question_type_id IN ( ' . CTestQuestionType::MULTIPLE_CHOICE . ' , ' . CTestQuestionType::TRUE_OR_FALSE . ', ' . CTestQuestionType::CODING_TEST . ' ) 
						)
						AND  tsa.test_submission_id = ' . ( int ) $intTestSubmissionId;

		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
		return 0;
	}

	public static function fetchTestQuestionsByTestId( $intTestId, $objDatabase ) {

		if( true == is_null( $intTestId ) ) return false;

		$strSql = ' SELECT
						tq.*,
						tqa.test_id
					FROM
						test_question_associations tqa
						LEFT JOIN test_questions tq ON ( tq.id = tqa.test_question_id )
					WHERE
						tq.deleted_on IS NULL
						AND tq.is_published = 1
						AND tqa.test_id = ' . ( int ) $intTestId;

		return self::fetchTestQuestions( $strSql, $objDatabase );
	}

	public static function fetchTestQuestionIdAndTestIdByPsWebsiteJobPostingIdByTestQuestionTypeId( $intPsWebsiteJobPostingId, $objDatabase ) {

		$intTestQuestionTypeId = CTestQuestionType::CODING_TEST;

		$strSql = 'SELECT
						*
					FROM
						(
						  SELECT
							  DISTINCT ON ( tq.id ) tq.id AS test_question_id,
							  t.id AS test_id
						  FROM
							  test_groups tg
							  JOIN tests t ON ( t.id = tg.test_id )
							  JOIN test_question_associations tqa ON ( tqa.test_id = t.id )
							  JOIN test_questions tq ON ( tq.id = tqa.test_question_id )
						  WHERE
							  t.is_published = 1
							  AND t.max_questions = 1
							  AND tq.is_published = 1
							  AND tq.test_question_type_id =' . ( int ) $intTestQuestionTypeId . '
							  AND tg.ps_website_job_posting_id =' . ( int ) $intPsWebsiteJobPostingId . '
							  AND tq.deleted_on IS NULL
							  AND tq.deleted_by IS NULL
						  UNION
						  SELECT
							  DISTINCT ON ( tq.id ) tq.id AS test_question_id,
							  t.id AS test_id
						  FROM
							  test_groups tjp
							  JOIN tests t ON tjp.test_id = t.id AND t.deleted_on IS NULL AND t.deleted_by IS NULL
							  JOIN test_question_template_associations tqta ON tqta.test_id = t.id
							  JOIN test_templates tt ON ( tt.id = tqta.test_template_id )
							  JOIN test_question_templates tqt ON ( tt.id = tqt.test_template_id )
							  JOIN test_questions tq ON ( tq.test_question_type_id = tqt.test_question_type_id AND tq.test_question_group_id = tqt.test_question_group_id AND tq.test_level_type_id = tt.test_level_type_id )
						  WHERE
							  t.is_published = 1
							  AND t.max_questions = 1
							  AND tq.is_published = 1
							  AND tq.test_question_type_id =' . ( int ) $intTestQuestionTypeId . '
							  AND tjp.ps_website_job_posting_id =' . ( int ) $intPsWebsiteJobPostingId . '
							  AND tt.deleted_on IS NULL
							  AND tt.deleted_by IS NULL
							  AND tq.deleted_on IS NULL
							  AND tq.deleted_by IS NULL
						) t
					ORDER BY
						RANDOM()
					LIMIT
						1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestQuestionDetailsByIds( $intTestQuestionIds, $objDatabase ) {
		if( false == valArr( $intTestQuestionIds ) ) return;

		$strSql = 'SELECT
						tq.id,
						tq.question
					FROM
						test_questions AS tq
					WHERE
						tq.id IN ( ' . implode( ',', $intTestQuestionIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCodingTestQuestionsCount( $objDatabase ) {

		$strSql = 'SELECT
					    count ( t.id ) AS count
					FROM
					    test_questions t
					WHERE
					    t.test_question_type_id = ' . CTestQuestionType::CODING_TEST;

		$arrintResult = ( array ) fetchData( $strSql, $objDatabase );
		return ( true == isset( $arrintResult[0] ) ) ? $arrintResult[0]['count'] : 0;

	}

	public static function fetchPaginatedTestQuestions( $intPageNo, $intPageSize, $objDatabase ) {

		$intOffset			 = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit			 = ( int ) $intPageSize;

		$strSql = ' SELECT
					    t.id AS test_question_id,
					    t.additional_fields ->> \'language\' AS language,
					    t.question
					FROM
					    test_questions t
					WHERE
					    t.test_question_type_id = ' . ( int ) CTestQuestionType::CODING_TEST . '
					ORDER BY
						t.id
					OFFSET ' .
						( int ) $intOffset . '
					LIMIT ' .
						( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchEmployeeApplicationsByTestQuestionId( $intTestQuestionId, $objDatabase, $objPagination = NULL, $boolIsCount = false ) {

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strOrderByType	= ( 0 == $objPagination->getOrderByType() ) ? ' ASC ' : ' DESC ';
			$intOffset		= ( 0 < $objPagination->getPageNo() ) ? ( $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) ) : 0;
			$intLimit		= ( int ) $objPagination->getPageSize();
		}

		if( true == $boolIsCount ) {
			$strSql = 'SELECT count( tsa.id ) ';
		} else {
			$strSql = 'SELECT
					    tsa.id,
					    tsa.additional_fields ->> \'submitted_on\' AS submitted_on,
                        tsa.additional_fields ->> \'started_on\' AS started_on,
					    ts.employee_application_id,
					    ts.created_on,
					    ts.score,
					    ea.name_first ||  \' \' ||ea.name_last as name_full';
		}

		$strSql .= '
					FROM
					    test_submission_answers tsa
					    JOIN test_submissions ts ON ( tsa.test_submission_id = ts.id )
					    JOIN employee_applications ea ON ( ts.employee_application_id = ea.id )
					    JOIN test_questions tq ON ( tq.id = tsa.test_question_id )
					WHERE
					    tq.test_question_type_id = ' . ( int ) CTestQuestionType::CODING_TEST . ' 
					    AND tsa.test_question_id= ' . ( int ) $intTestQuestionId;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' ORDER BY ' . $objPagination->getOrderByField() . ' ' . $strOrderByType;
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$arrstrResult = ( array ) fetchData( $strSql, $objDatabase );

		if( true == $boolIsCount ) {
			return $arrstrResult[0]['count'];
		}

		return $arrstrResult;
	}

	public static function fetchEmployeesByTestQuestionId( $intTestQuestionId, $objDatabase, $objPagination = NULL, $boolIsCount = false ) {

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strOrderByType	= ( 0 == $objPagination->getOrderByType() ) ? ' ASC ' : ' DESC ';
			$intOffset		= ( 0 < $objPagination->getPageNo() ) ? ( $objPagination->getPageSize() * ( $objPagination->getPageNo() - 1 ) ) : 0;
			$intLimit		= ( int ) $objPagination->getPageSize();
		}

		if( true == $boolIsCount ) {
			$strSql = 'SELECT count( tsa.id ) ';
		} else {
			$strSql = 'SELECT
					    tsa.id,
					    tsa.additional_fields ->> \'submitted_on\' AS submitted_on,
                        tsa.additional_fields ->> \'started_on\' AS started_on,
					    ts.employee_id,
					    ts.created_on,
					    ts.score,
					    e.name_first ||  \' \' ||e.name_last as name_full';
		}

		$strSql .= '
					FROM
					    test_submission_answers tsa
					    JOIN test_submissions ts ON ( tsa.test_submission_id = ts.id )
					    JOIN employees e ON ( ts.employee_id = e.id )
					    JOIN test_questions tq ON ( tq.id = tsa.test_question_id )
					WHERE
					    tq.test_question_type_id = ' . ( int ) CTestQuestionType::CODING_TEST . '
					    AND tsa.test_question_id= ' . ( int ) $intTestQuestionId;

		if( true == valObj( $objPagination, \Psi\Libraries\UtilPagination\CPagination::class ) ) {
			$strSql .= ' ORDER BY ' . $objPagination->getOrderByField() . ' ' . $strOrderByType;
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;
		}

		$arrstrResult = ( array ) fetchData( $strSql, $objDatabase );

		if( true == $boolIsCount ) {
			return $arrstrResult[0]['count'];
		}

		return $arrstrResult;
	}

	public static function fetchPublicAndPublishedTestQuestions( $intTestId, $objDatabase ) {
		if( false == is_numeric( $intTestId ) ) {
			return false;
		}
		$strSql = 'SELECT 
						tq.*,
						CASE WHEN tq.is_render_html IS TRUE THEN 1 ELSE 0 END AS is_render_html
					FROM
						test_questions tq
					WHERE
						tq.id NOT IN ( 
							SELECT
								test_question_id
							FROM
								test_question_associations
							WHERE
								test_id = ' . ( int ) $intTestId . '
								)
						AND tq.deleted_on IS NULL
						AND tq.is_published = 1
					ORDER BY tq.question ASC';

		return parent::fetchTestQuestions( $strSql, $objDatabase );
	}

	public static function fetchTestQuestionsByIds( $arrintTestQuestionIds, $objDatabase ) {

		if( false == valArr( $arrintTestQuestionIds ) ) {
			return false;
		}

		$strSql = 'SELECT 
						tq.*
					FROM
						test_questions tq
					WHERE
						tq.id IN ( ' . implode( ',', $arrintTestQuestionIds ) . ' )
						AND tq.deleted_by IS NULL';

		return parent::fetchTestQuestions( $strSql, $objDatabase );
	}

}
?>