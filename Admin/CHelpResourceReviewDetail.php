<?php

class CHelpResourceReviewDetail extends CBaseHelpResourceReviewDetail {

	protected $m_strReviewerEmployeeNames;

	public function getReviewerEmployeeNames() {
		return $this->m_strReviewerEmployeeNames;
	}

	public function setReviewerEmployeeNames( $strReviewerEmployeeNames ) {
		$this->m_strReviewerEmployeeNames = $strReviewerEmployeeNames;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['reviewer_employee_names'] ) )     $this->setReviewerEmployeeNames( $arrmixValues['reviewer_employee_names'] );

	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpResourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewerEmployeeIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewFrequencyDays() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewDueDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContentReviewStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReviewedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>