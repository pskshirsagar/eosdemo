<?php

class CTransportEnrollmentHistory extends CBaseTransportEnrollmentHistory {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransportEnrollmentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransportRouteId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransportPickupPointId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransportRequestStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransportRequestStatusReason() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>