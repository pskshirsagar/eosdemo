<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemKeywordSources
 * Do not add any new functions to this class.
 */

class CSemKeywordSources extends CBaseSemKeywordSources {

	public static function fetchSemKeywordSourcesBySemSourceIdBySemKeywordIds( $intSemSourceId, $arrintSemKeywordIds, $objAdminDatabase ) {
		if( false == valArr( $arrintSemKeywordIds ) ) return NULL;

		$strSql = 'SELECT
						sks.*,
						sags.remote_primary_key AS sem_ad_group_remote_primary_key,
						sags.sem_account_id
					FROM
						sem_keyword_sources sks
						JOIN sem_ad_group_sources sags ON ( sks.sem_ad_group_id = sags.sem_ad_group_id AND sks.sem_source_id = sags.sem_source_id AND sags.sem_source_id = ' . ( int ) $intSemSourceId . ' )
					WHERE
						sks.sem_source_id = ' . ( int ) $intSemSourceId . '
					AND
						sks.sem_keyword_id IN (' . implode( ',', $arrintSemKeywordIds ) . ')
					AND
						sks.remote_primary_key IS NOT NULL
					AND
						sags.remote_primary_key IS NOT NULL';

		return self::fetchSemKeywordSources( $strSql, $objAdminDatabase );
	}

	public static function fetchSemKeywordSourceBySemSourceIdBySemAdGroupIdByRemotePrimaryKey( $intSemSourceId, $intSemAdGroupId, $strRemotePrimaryKey, $objAdminDatabase ) {
		return self::fetchSemKeywordSource( sprintf( 'SELECT sks.*,sk.keywords FROM sem_keyword_sources sks, sem_keywords sk WHERE sk.id = sks.sem_keyword_id AND sks.sem_source_id = %d AND sks.sem_ad_group_id = %d AND sks.remote_primary_key = \'%s\'', ( int ) $intSemSourceId, ( int ) $intSemAdGroupId, ( string ) addslashes( trim( $strRemotePrimaryKey ) ) ), $objAdminDatabase );
	}

	public static function fetchSemKeywordSourceBySemSourceIdBySemAdGroupIdByKeywords( $intSemSourceId, $intSemAdGroupId, $strKeywords, $objAdminDatabase ) {
		return self::fetchSemKeywordSource( sprintf( 'SELECT sks.*, sk.keywords FROM sem_keyword_sources sks, sem_keywords sk WHERE sk.id = sks.sem_keyword_id AND sks.sem_source_id = %d AND sks.sem_ad_group_id = %d AND sk.keywords = \'%s\'', ( int ) $intSemSourceId, ( int ) $intSemAdGroupId, ( string ) addslashes( $strKeywords ) ), $objAdminDatabase );
	}

 	public static function fetchSemKeywordSourceBySemKeywordIdBySemAdGroupIdBySemSourceId( $intSemKeywordId, $intSemAdGroupId, $intSemSourceId, $objAdminDatabase ) {
		return self::fetchSemKeywordSource( sprintf( 'SELECT * FROM sem_keyword_sources WHERE sem_keyword_id = %d  AND sem_ad_group_id = %d AND sem_source_id = %d', ( int ) $intSemKeywordId, ( int ) $intSemAdGroupId, ( int ) $intSemSourceId ), $objAdminDatabase );
 	}

	public static function fetchSemKeywordSourcesBySemKeywordId( $intSemKeywordId, $objDatabase ) {
		return self::fetchSemKeywordSources( sprintf( 'SELECT * FROM sem_keyword_sources WHERE sem_keyword_id = %d', ( int ) $intSemKeywordId ), $objDatabase );
	}

	public static function fetchSyncReadySemKeywordSourcesBySemAccountIdBySemSourceId( $intSemAccountId, $intSemSourceId, $objAdminDatabase ) {

		$strSql = 'SELECT
						sks1.*,
						sk1.keywords,
						sk1.is_system,
						sk1.deleted_by,
						sk1.deleted_on,
						sags1.remote_primary_key AS sem_ad_group_remote_primary_key,
						COALESCE( inner_table.associated_properties_count, 0 ) AS associated_properties_count
					FROM
						sem_keywords sk1
						 LEFT OUTER JOIN (
							SELECT
								sk.id,
								COUNT(p.id) as associated_properties_count
							FROM
								sem_keywords sk
								JOIN sem_keyword_associations ska ON ( ska.sem_keyword_id = sk.id AND ska.sem_account_id =  ' . ( int ) $intSemAccountId . '  )
								JOIN sem_ad_groups sag ON sag.id = sk.sem_ad_group_id
								JOIN sem_ad_group_sources sags ON ( sag.id = sags.sem_ad_group_id AND sags.sem_account_id =  ' . ( int ) $intSemAccountId . ' AND sags.sem_source_id = ' . ( int ) $intSemSourceId . '  )
								JOIN properties p ON ( ska.property_id = p.id AND p.is_disabled <> 1 AND p.property_type_id = ' . CPropertyType::APARTMENT . ' )
								JOIN sem_property_details spd ON ( spd.property_id = p.id AND spd.sem_property_status_type_id = ' . CSemPropertyStatusType::ENABLED . '  AND spd.longitude IS NOT NULL AND spd.latitude IS NOT NULL )
								JOIN sem_settings ss ON ( ss.property_id = p.id )
								JOIN postal_codes pc ON ( pc.postal_code = spd.postal_code AND pc.longitude IS NOT NULL AND pc.latitude IS NOT NULL )
								LEFT OUTER JOIN sem_budgets sb ON ( p.id = sb.property_id AND DATE_TRUNC( \'month\', budget_date ) = DATE_TRUNC ( \'month\', NOW() ))
							WHERE
								sb.daily_remaining_budget > 0
							GROUP BY sk.id
						) AS inner_table ON ( sk1.id = inner_table.id )
						JOIN sem_keyword_sources sks1 ON ( sk1.id = sks1.sem_keyword_id AND  sks1.sem_source_id = ' . ( int ) $intSemSourceId . ' )
						JOIN sem_ad_groups sag1 ON sag1.id = sk1.sem_ad_group_id
						JOIN sem_ad_group_sources sags1 ON ( sag1.id = sags1.sem_ad_group_id AND sags1.remote_primary_key IS NOT NULL AND sags1.sem_account_id =  ' . ( int ) $intSemAccountId . ' AND sags1.sem_source_id = ' . ( int ) $intSemSourceId . ' )
						LEFT OUTER JOIN sem_keyword_associations ska1 ON ( ska1.sem_keyword_id = sk1.id )
					WHERE
						sks1.sync_requested_on IS NOT NULL
					AND
						sags1.remote_primary_key IS NOT NULL';

			// $strSql = 'SELECT
				//				sks.*,
				//				sk.keywords,
					//			sags.remote_primary_key AS sem_ad_group_remote_primary_key,
						//		1 AS associated_properties_count
						// FROM
						  //	  sem_keywords sk
							//	JOIN sem_keyword_sources sks ON ( sk.id = sks.sem_keyword_id  )
							  //  JOIN sem_ad_group_sources sags ON ( sks.sem_ad_group_id = sags.sem_ad_group_id  AND sags.sem_source_id = sks.sem_source_id )

						// WHERE
						  //	  sks.remote_primary_key IS NULL
						// AND
							// sags.sem_account_id = ' . ( int ) $objSemAccount->getId() . '
						// AND
							// sags.sem_source_id = ' . ( int ) $this->m_objSemSource->getId() . '
						// LIMIT 2';
		return self::fetchSemKeywordSources( $strSql, $objAdminDatabase );
	}

}
?>