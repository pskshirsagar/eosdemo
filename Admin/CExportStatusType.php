<?php

class CExportStatusType extends CBaseExportStatusType {

	const NETSUITE_COMPLETED = 1;

	const NETSUITE_FAILED = 2;

	const NETSUITE_INCOMPLETE = 3;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportPartnerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>