<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmailTemplateSlots
 * Do not add any new functions to this class.
 */

class CEmailTemplateSlots extends CBaseEmailTemplateSlots {

	public static function fetchEmailTemplateSlotsByEmailTemplateId( $intEmailTemplateId, $objDatabase ) {
		$strSql = 'SELECT * FROM email_template_slots WHERE email_template_slots.email_template_id = ' . ( int ) $intEmailTemplateId . ' ORDER BY order_num';
		return self::fetchEmailTemplateSlots( $strSql, $objDatabase );
	}

	public static function fetchEmailTemplateSlotByIds( $arrintIds, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM email_template_slots WHERE id in ( ' . implode( ',', $arrintIds ) . ' ) ';
		return self::fetchEmailTemplateSlots( $strSql, $objAdminDatabase );
	}

	public static function fetchNextEmailTemplateSlotOrderNum( $objAdminDatabase ) {
		$strSql = 'SELECT MAX( order_num )+1 as order_number FROM email_template_slots';

		$arrintResponse	= fetchData( $strSql, $objAdminDatabase );

		if( true == isset( $arrintResponse[0]['order_number'] ) ) return $arrintResponse[0]['order_number'];
		return 0;
	}

}
?>