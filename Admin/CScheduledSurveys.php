<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CScheduledSurveys
 * Do not add any new functions to this class.
 */

class CScheduledSurveys extends CBaseScheduledSurveys {

	public static function fetchScheduledSurveysByIds( $arrintScheduledSurveyIds, $objDatabase ) {
		if( false == valArr( $arrintScheduledSurveyIds ) ) return NULL;

		$strSql = 'SELECT * FROM scheduled_surveys WHERE id IN ( ' . implode( ',', $arrintScheduledSurveyIds ) . ' )';

		return self::fetchScheduledSurveys( $strSql, $objDatabase );
	}

	public static function fetchScheduledSurveysByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT 
                        * 
                    FROM 
                        scheduled_surveys 
                    WHERE 
                        responder_reference_id = ' . ( int ) $intEmployeeId . ' 
                        AND subject_reference_id =' . ( int ) $intEmployeeId . ' 
                        AND deleted_on is NULL
                    ORDER BY id DESC 
                    LIMIT 1';

		return self::fetchScheduledSurvey( $strSql, $objDatabase );
	}

	public static function fetchScheduledSurveysPeerEmployees( $intEmployeeId, $objDatabase, $boolIsRatedToEmployees ) {

		if( true == is_null( $intEmployeeId ) ) return NULL;

		if( true == $boolIsRatedToEmployees ) {
			$strJoinOnCondition = ' e.id = ss.subject_reference_id AND ss.responder_reference_id = ' . ( int ) $intEmployeeId;
		} else {
			$strJoinOnCondition = ' e.id = ss.responder_reference_id AND ss.subject_reference_id = ' . ( int ) $intEmployeeId;
		}

		$strSql = 'SELECT
						e.id AS employee_id,
						e.preferred_name AS employee_name,
						d.name AS designation_name,
						ss.start_date,
						ss.frequency_id,
						ss.end_date,
						ss.id AS scheduled_id
					FROM
						employees e
						LEFT JOIN designations d ON ( e.designation_id = d.id )
						JOIN scheduled_surveys ss ON( ' . $strJoinOnCondition . ' )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ss.responder_reference_id <> ss.subject_reference_id
 					GROUP BY
						e.id,
						e.preferred_name,
						ss.start_date,
						ss.frequency_id,
						ss.end_date,
						ss.id,
						d.name
					ORDER BY e.id ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScheduledSurveysBySurveyTemplateIdByIsPersonSurvey( $intSurveyTemplateId, $intSurveyTypeId, $boolIsPersonSurvey = false, $objSurveySystemFilter, $intPageNo, $intPageSize, $objDatabase ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		$strWhere = $strSqlForNeverFrequency = '';

		if( CSurveyType::CUSTOMER_NPS == $intSurveyTypeId || CSurveyType::CLIENT_SUCCESS_MANAGER_REVIEW == $intSurveyTypeId
			|| CSurveyType::EMPLOYEE_NPS == $intSurveyTypeId || CSurveyType::UTILITIES == $intSurveyTypeId ) {
			$strReference = 'ss.responder_reference_id';
		} else {
			$strReference = 'ss.subject_reference_id';
		}

		if( true == valObj( $objSurveySystemFilter, 'CSurveySystemFilter' ) ) {

			if( true == valArr( $arrintCids = $objSurveySystemFilter->getCids() ) ) {
				$strWhere .= ' AND p.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ';
			}

			if( true == valArr( $arrintDesignationIds = $objSurveySystemFilter->getDesignationIds() ) ) {
				$strWhere .= ' AND e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' ) ';
			}

			if( true == valStr( $objSurveySystemFilter->getRecipientName() ) ) {
				if( true == $boolIsPersonSurvey ) {
					$strWhere .= ' AND ( p.name_first ILIKE \'%' . trim( $objSurveySystemFilter->getRecipientName() ) . '%\' OR p.name_last ILIKE \'%' . trim( $objSurveySystemFilter->getRecipientName() ) . '%\' )';
				} else {
					$strWhere .= ' AND ( e.name_first ILIKE \'%' . trim( $objSurveySystemFilter->getRecipientName() ) . '%\' OR e.name_last ILIKE \'%' . trim( $objSurveySystemFilter->getRecipientName() ) . '%\' )';
				}
			}
		}

		$strJoin = ' INNER JOIN employees e ON ( e.id =' . $strReference . ' )
					INNER JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
					INNER JOIN	teams t ON ( te.team_id = t.id )
					INNER JOIN designations d ON ( d.id = e.designation_id )';
		$strSelect = ' e.name_first, e.name_last, e.id as employee_id, t.name as team_name, d.name,';

		if( true == $boolIsPersonSurvey ) {

			$strWhere .= ' AND p.is_disabled <> 1';

			$strJoin	= ' INNER JOIN persons p ON ( p.id =' . $strReference . ')
							LEFT JOIN person_roles pr ON ( p.person_role_id = pr.id )';
			$strSelect = ' p.name_first, p.name_last, p.id as employee_id, p.company_name, pr.name as role, p.email_address,';
		} else {
			$strWhere .= ' AND e.date_terminated IS NULL AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		}

		if( true == valObj( $objSurveySystemFilter, 'CSurveySystemFilter' ) && true == valArr( array_filter( ( array ) $objSurveySystemFilter ) ) && ( ( false == valArr( $objSurveySystemFilter->getFrequencyIds() ) ) || ( true == valArr( $objSurveySystemFilter->getFrequencyIds() ) && in_array( 0, $objSurveySystemFilter->getFrequencyIds() ) ) ) ) {

			if( false == $boolIsPersonSurvey ) {

				$strSqlForNeverFrequency = 'SELECT
													e.name_first,
													e.name_last,
													e.id as employee_id,
													t.name as team_name,
													d.name,
													CASE
														WHEN ss.survey_template_id IS NULL THEN \'Never\'
														WHEN ss.survey_template_id != ' . ( int ) $intSurveyTemplateId . ' THEN \'Never\'
													END AS frequency_name,
													ss.*
												FROM
													employees e
													INNER JOIN team_employees te ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
													INNER JOIN teams t ON ( te.team_id = t.id )
													INNER JOIN designations d ON ( d.id = e.designation_id )
													LEFT JOIN scheduled_surveys ss ON ( e.id = ' . $strReference . '
																						AND ss.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
																						AND ss.deleted_by IS NULL)
													LEFT JOIN frequencies f ON ( f.id = ss.frequency_id )
												WHERE
													e.id NOT IN (
														SELECT
															DISTINCT ' . $strReference . '
														FROM
															scheduled_surveys ss
														WHERE
															ss.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
															AND ss.deleted_by IS NULL
															AND ' . $strReference . ' IS NOT NULL
													)' . $strWhere . ' UNION ';
			} else {
				$strSqlForNeverFrequency = 'SELECT
												p.name_first,
												p.name_last,
												p.id AS employee_id,
												p.company_name,
												pr.name as role,
												p.email_address,
												CASE
													WHEN ss.survey_template_id IS NULL THEN \'Never\'
													WHEN ss.survey_template_id != ' . ( int ) $intSurveyTemplateId . ' THEN \'Never\'
												END AS frequency_name,
												ss.*
											FROM
												persons p
												LEFT JOIN person_roles pr ON ( p.person_role_id = pr.id )
												LEFT JOIN scheduled_surveys ss ON ( p.id = ' . $strReference . '
																					AND ss.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
																					AND ss.deleted_by IS NULL)
											WHERE
												p.id NOT IN (
												SELECT
													DISTINCT ' . $strReference . '
												FROM
													scheduled_surveys ss
												WHERE
													ss.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
													AND ss.deleted_by IS NULL
													AND ' . $strReference . ' IS NOT NULL
											)' . $strWhere . ' UNION ';

			}
		}

		if( true == valObj( $objSurveySystemFilter, 'CSurveySystemFilter' ) && true == valArr( $arrintFrequencyIds = $objSurveySystemFilter->getFrequencyIds() ) ) {
			$strWhere .= ' AND ss.frequency_id IN ( ' . implode( ',', $arrintFrequencyIds ) . ' ) ';
		}

		$strSql = $strSqlForNeverFrequency . ' SELECT' .
													$strSelect . '
													f.name as frequency_name,
													ss.*
												FROM
													scheduled_surveys ss ' .
													$strJoin . '
													INNER JOIN frequencies f ON ( f.id = ss.frequency_id )
												WHERE
													ss.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
													AND ss.deleted_by IS NULL
													' . $strWhere . '
													ORDER BY
															employee_id
													OFFSET ' . ( int ) $intOffset . '
													LIMIT ' . $intLimit;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScheduledSurveysCountBySurveyTemplateIdByIsPersonSurvey( $intSurveyTemplateId, $intSurveyTypeId, $boolIsPersonSurvey = false, $objSurveySystemFilter, $objDatabase ) {

			$strWhere = $strSqlForNeverFrequency = '';

			if( CSurveyType::CUSTOMER_NPS == $intSurveyTypeId || CSurveyType::CLIENT_SUCCESS_MANAGER_REVIEW == $intSurveyTypeId
				|| CSurveyType::EMPLOYEE_NPS == $intSurveyTypeId || CSurveyType::UTILITIES == $intSurveyTypeId ) {
				$strReference = 'ss.responder_reference_id';
			} else {
				$strReference = 'ss.subject_reference_id';
			}

			if( true == valObj( $objSurveySystemFilter, 'CSurveySystemFilter' ) ) {

				if( true == valArr( $arrintCids = $objSurveySystemFilter->getCids() ) ) {
					$strWhere .= ' AND p.cid IN ( ' . implode( ',', $arrintCids ) . ' ) ';
				}

				if( true == valArr( $arrintDesignationIds = $objSurveySystemFilter->getDesignationIds() ) ) {
					$strWhere .= ' AND e.designation_id IN ( ' . implode( ',', $arrintDesignationIds ) . ' ) ';
				}

				if( true == valStr( $objSurveySystemFilter->getRecipientName() ) ) {
					if( true == $boolIsPersonSurvey ) {
						$strWhere .= ' AND ( p.name_first ILIKE \'%' . trim( $objSurveySystemFilter->getRecipientName() ) . '%\' OR p.name_last ILIKE \'%' . trim( $objSurveySystemFilter->getRecipientName() ) . '%\' )';
					} else {
						$strWhere .= ' AND ( e.name_first ILIKE \'%' . trim( $objSurveySystemFilter->getRecipientName() ) . '%\' OR e.name_last ILIKE \'%' . trim( $objSurveySystemFilter->getRecipientName() ) . '%\' )';
					}
				}
			}

			$strJoin = ' INNER JOIN employees e ON ( e.id = ' . $strReference . ' )
						 INNER JOIN designations d ON ( d.id = e.designation_id )';
			$strSelect = ' e.name_first, e.name_last, e.id as employee_id, d.name,';

			if( true == $boolIsPersonSurvey ) {

				$strWhere .= ' AND p.is_disabled != 1 ';

				$strJoin   = ' INNER JOIN persons p ON ( p.id = ' . $strReference . ')';
				$strSelect = ' p.name_first, p.name_last, p.id as employee_id, p.company_name, p.email_address,';
			} else {
				$strWhere .= ' AND e.date_terminated IS NULL AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
			}

			if( true == valObj( $objSurveySystemFilter, 'CSurveySystemFilter' ) && true == valArr( array_filter( ( array ) $objSurveySystemFilter ) ) && ( ( false == valArr( $objSurveySystemFilter->getFrequencyIds() ) ) || ( true == valArr( $objSurveySystemFilter->getFrequencyIds() ) && in_array( 0, $objSurveySystemFilter->getFrequencyIds() ) ) ) ) {

				if( false == $boolIsPersonSurvey ) {

					$strSqlForNeverFrequency = 'SELECT
													e.name_first,
													e.name_last,
													e.id as employee_id,
													d.name,
													CASE
														WHEN ss.survey_template_id IS NULL THEN \'Never\'
														WHEN ss.survey_template_id != ' . ( int ) $intSurveyTemplateId . ' THEN \'Never\'
													END AS frequency_name,
													ss.*
												FROM
													employees e
													INNER JOIN designations d ON ( d.id = e.designation_id )
													LEFT JOIN scheduled_surveys ss ON ( e.id = ' . $strReference . '
																						AND ss.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
																						AND ss.deleted_by IS NULL)
													LEFT JOIN frequencies f ON ( f.id = ss.frequency_id )
												WHERE
													e.id NOT IN (
														SELECT
															DISTINCT ' . $strReference . '
														FROM
															scheduled_surveys ss
														WHERE
															ss.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
															AND ss.deleted_by IS NULL
															AND ' . $strReference . ' IS NOT NULL
													)' . $strWhere . ' UNION ';
				} else {
					$strSqlForNeverFrequency = 'SELECT
													p.name_first,
													p.name_last,
													p.id AS employee_id,
													p.company_name,
													p.email_address,
													CASE
														WHEN ss.survey_template_id IS NULL THEN \'Never\'
														WHEN ss.survey_template_id != ' . ( int ) $intSurveyTemplateId . ' THEN \'Never\'
													END AS frequency_name,
													ss.*
												FROM
													persons p
													LEFT JOIN scheduled_surveys ss ON ( p.id = ' . $strReference . '
																						AND ss.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
																						AND ss.deleted_by IS NULL )
												WHERE
													p.id NOT IN (
														SELECT
															DISTINCT ' . $strReference . '
														FROM
															scheduled_surveys ss
														WHERE
															ss.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
															AND ss.deleted_by IS NULL
															AND ' . $strReference . ' IS NOT NULL
													)' . $strWhere . ' UNION ';
				}
			}

			if( true == valObj( $objSurveySystemFilter, 'CSurveySystemFilter' ) && true == valArr( $arrintFrequencyIds = $objSurveySystemFilter->getFrequencyIds() ) ) {
				$strWhere .= ' AND ss.frequency_id IN ( ' . implode( ',', $arrintFrequencyIds ) . ' ) ';
			}

			$strSql = ' SELECT
							COUNT( * )
						FROM
							(
								' . $strSqlForNeverFrequency . ' SELECT' .
																		$strSelect . '
																		f.name as frequency_name,
																		ss.*
																	FROM
																		scheduled_surveys ss ' .
																		$strJoin . '
																		INNER JOIN frequencies f ON ( f.id = ss.frequency_id )
																	WHERE
																		ss.survey_template_id = ' . ( int ) $intSurveyTemplateId . '
																		AND ss.deleted_by IS NULL
																		' . $strWhere . '
							) AS subSql';

			$arrintResponse = fetchData( $strSql, $objDatabase );

			return ( isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;
	}

	public static function fetchCurrentScheduledSurveysCountBySubjectReferenceId( $intSubjectReferenceId, $objDatabase ) {

		$strSql = 'SELECT
						count(ss.id)
					FROM
						scheduled_surveys ss
						JOIN employees e ON ( ss.responder_reference_id = e.id )
					WHERE
						e.employee_status_type_id = 1
						AND ss.subject_reference_id = ' . ( int ) $intSubjectReferenceId;

		$arrintResponse = fetchData( $strSql, $objDatabase );
		$intCount = ( isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;

		return $intCount;
	}

	public static function fetchCurrentScheduledSurveyCountByResponderReferenceId( $intResponderReferenceId, $objDatabase ) {

		$strSql = 'SELECT
						count(ss.id)
					FROM
						scheduled_surveys ss
						JOIN employees e ON ( ss.subject_reference_id = e.id )
					WHERE
						e.employee_status_type_id = 1
						AND ss.responder_reference_id =' . ( int ) $intResponderReferenceId;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		$intCount = ( isset( $arrintResponse[0]['count'] ) ) ? $arrintResponse[0]['count'] : 0;

		return $intCount;

	}

	public static function fetchActiveScheduledSurveysBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						scheduled_surveys
					WHERE
						survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND deleted_by IS NULL';

		return self::fetchScheduledSurveys( $strSql, $objDatabase );
	}

	public static function fetchEmployeeIdsbyResponderReferenceIds( $arrintResponderReferenceIds, $objDatabase ) {
		if( false == valArr( $arrintResponderReferenceIds ) ) return NULL;

		$strSql = 'SELECT
						responder_reference_id AS employee_id
					FROM
						scheduled_surveys
					WHERE
						responder_reference_id IN ( ' . implode( ',', $arrintResponderReferenceIds ) . ') AND
						survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_IN . ' AND
						frequency_id IN ( ' . CFrequency::MONTHLY . ',' . CFrequency::QUARTERLY . ')
						AND deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeIdsbyResponderReferenceIdBySurveyTemplateId( $intResponderReferenceId, $intSurveyTemplateId, $objDatabase ) {
		if( false == is_numeric( $intResponderReferenceId ) ) return NULL;

		$strSql = 'SELECT
						responder_reference_id AS employee_id,
						frequency_id
					FROM
						scheduled_surveys
					WHERE
						responder_reference_id = ' . ( int ) $intResponderReferenceId . ' AND
						survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScheduledSurveyFrequencyByResponderReferenceId( $intResponderReferenceId, $objDatabase ) {

		if( false == is_numeric( $intResponderReferenceId ) ) return NULL;
		$strSql = 'SELECT
						f.name
					FROM
						scheduled_surveys as ss
						JOIN frequencies as f ON f.id = ss.frequency_id
					WHERE
						ss.responder_reference_id =' . ( int ) $intResponderReferenceId .
						' AND subject_reference_id =' . ( int ) $intResponderReferenceId .
					'ORDER BY ss.id DESC
					LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchScheduledSurveysByFrequencyId( $arrintResponderReferenceIds, $objDatabase ) {

		if( false == valArr( $arrintResponderReferenceIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT
						*
					FROM
						scheduled_surveys
					WHERE
						frequency_id = ' . CFrequency::WEEKLY . '
						AND survey_type_id =' . CSurveyType::PERSONAL_REVIEW . '
						AND survey_template_id = ' . CSurveyTemplate::PERSONAL_REVIEW_IN . '
						AND deleted_by IS NULL
						AND deleted_on IS NULL
						AND responder_reference_id IN ( ' . implode( ',', $arrintResponderReferenceIds ) . ' )
					ORDER BY
						id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>