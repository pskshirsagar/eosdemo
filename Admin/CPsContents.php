<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsContents
 * Do not add any new functions to this class.
 */

class CPsContents extends CBasePsContents {

	public static function fetchPsContentsByKeys( $arrstrKeys, $objDatabase ) {

		$strSql = 'SELECT
						id, key, value
					FROM
						ps_contents
					WHERE
						id IN( ' . implode( ',', $arrstrKeys ) . ')';

		return self::fetchPsContents( $strSql, $objDatabase );
	}
}
?>