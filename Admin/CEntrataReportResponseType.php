<?php

class CEntrataReportResponseType extends CBaseEntrataReportResponseType {

	const HELPFUL = '1';
	const MISSING_DATA = '2';
	const INCORRECT_DATA = '3';
	const PERFORMANCE_ISSUE = '4';
	const FILTER_ISSUE = '5';
	const INCORRECT_PERMISSIONS = '6';
	const NAVIGATION_ISSUE = '7';
	const FIELD_ISSUE = '8';
	const OTHER = '9';

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpful() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>