<?php
use Psi\Eos\Admin\CPersonContactPreferences;
use Psi\Eos\Admin\CPersonOrigins;
use Psi\Eos\Admin\CContracts;
use Psi\Eos\Admin\CPersonPhoneNumbers;

class CPerson extends CBasePerson {

	const COMPANY_PERSON			= 'company_person';
	const LEAD_PERSON				= 'lead_person';
	const CORPORATE_ENTITY_PERSON	= 'corporate_entity_person';
	const PRIMARY_PERSON	= 1;

	const OUTAGE_NOTIFICATION_STATUS_NONE	= 0;
	const OUTAGE_NOTIFICATION_STATUS_SMS	= 1;
	const OUTAGE_NOTIFICATION_STATUS_EMAIL	= 2;
	const OUTAGE_NOTIFICATION_STATUS_BOTH	= 3;

	const PROMOTER_TYPE_DETRACTOR	= 0;
	const PROMOTER_TYPE_NEUTRAL		= 1;
	const PROMOTER_TYPE_PROMOTER	= 2;

	protected $m_intPersonEmployeeId;
	protected $m_intPersonContactTypeId;
	protected $m_intActionsCount;
	protected $m_intIsChecked;
	protected $m_intEmployeeId;
	protected $m_intSalesEmployeeId;
	protected $m_intPersonContactPreferenceId;

	protected $m_strPhoneNumber1;
	protected $m_strPhoneNumber2;
	protected $m_strPhoneNumber3;
	protected $m_strLastEmailedOn;
	protected $m_strRelationshipDescription;
	protected $m_strHtmlContent;
	protected $m_strPersonNameFull;
	protected $m_strPersonRole;
	protected $m_strPersonType;
	protected $m_arrstrPersons;
	protected $m_arrstrPersonOldContactPreferences;
	protected $m_arrstrPersonNewContactPreferences;
	protected $m_arrstrPersonDifferencePersonContactTypes;
	protected $m_strPhoneNumberExtension;
	protected $m_strPhoneNumber;

	protected $m_arrobjPersonPhoneNumbers;
	protected $m_arrobjPersonPhoneNumber;

	/**
	 * Create Functions
	 */

	public function createPersonContactPreference() {

		$objPersonContactPreference = new CPersonContactPreference();
		$objPersonContactPreference->setPersonId( $this->getId() );
		$objPersonContactPreference->setPsLeadId( $this->getPsLeadId() );

		return $objPersonContactPreference;
	}

	public function createPersonEmployee() {

		$objPersonEmployee = new CPersonEmployee();
		$objPersonEmployee->setPersonId( $this->getId() );
		$objPersonEmployee->setPsLeadId( $this->getPsLeadId() );

		return $objPersonEmployee;
	}

	public function createPersonOrigin() {

		$objPersonOrigin = new CPersonOrigin();
		$objPersonOrigin->setPersonId( $this->getId() );

		return $objPersonOrigin;
	}

	public function createSystemEmail( $objMassEmail, $intActionId ) {

		if( false == isset( $objMassEmail ) || false == valObj( $objMassEmail, 'CMassEmail' ) ) {
			trigger_error( 'Mass email not passed into the function.', E_USER_ERROR );
		}

		$strHtmlContent		= $this->getHtmlContent();
		$strBaseUrl			= ( 'production' == CONFIG_ENVIRONMENT ) ? CONFIG_INSECURE_HOST_PREFIX . 'www.' . CONFIG_COMPANY_BASE_DOMAIN : 'http://www.propertysolutions.lcl';
		$strHtmlContent		.= '<img src="' . $strBaseUrl . '/?module=mass_emails&action=update_open_count&mass_email_id=' . ( int ) $objMassEmail->getId() . '&action[id]=' . ( int ) $intActionId . '&extra_params=1" border="0" width="1" height="1">';

		$objSystemEmailLibrary	= new CSystemEmailLibrary();
		$objSystemEmail			= $objSystemEmailLibrary->prepareSystemEmail( CSystemEmailType::SALES_SUPPORT_PEEP_EMAIL, $strSubject = $objMassEmail->getSubject(), $strHtmlContent, $strToEmailAddress = $this->getEmailAddress(), $intIsSelfDestruct = 0, $strFromEmailAddress = $objMassEmail->getFromEmailAddress() );

		$objSystemEmail->setScheduledSendDatetime( date( 'm/d/Y' ) );
		$objSystemEmail->setCid( $this->getCid() );
		$objSystemEmail->setEmployeeId( $this->getEmployeeId() );
		$objSystemEmail->setMassEmailId( $objMassEmail->getId() );

		return $objSystemEmail;
	}

	public function createAction( $intActionTypeId = NULL ) {

		$objAction = new CAction();
		$objAction->setPsLeadId( $this->getPsLeadId() );
		$objAction->setPersonId( $this->getId() );
		$objAction->setActionDatetime( date( 'm/d/Y H:i:s' ) );
		$objAction->setActionTypeId( $intActionTypeId );

		return $objAction;
	}

	/**
	 * Get Functions
	 */

	public function getPersonEmployeeId() {
		return $this->m_intPersonEmployeeId;
	}

	public function getPersonContactTypeId() {
		return $this->m_intPersonContactTypeId;
	}

	public function getActionsCount() {
		return $this->m_intActionsCount;
	}

	public function getRelationshipDescription() {
		return $this->m_strRelationshipDescription;
	}

	public function getCompanyUserId() {
		return $this->m_intCompanyUserId;
	}

	public function getLastEmailedOn() {
		return $this->m_strLastEmailedOn;
	}

	public function getIsChecked() {
		return $this->m_intIsChecked;
	}

	public function getHtmlContent() {
		return $this->m_strHtmlContent;
	}

	public function getPersonContactPreferenceId() {
		return $this->m_intPersonContactPreferenceId;
	}

	public function getPersonNameFull() {
		return $this->m_strPersonNameFull;
	}

	public function getPersonType() {
		return $this->m_strPersonType;
	}

	public function getPersonRole() {
		return $this->m_strPersonRole;
	}

	public function getSalesEmployeeId() {
		return $this->m_intSalesEmployeeId;
	}

	public function getPhoneNumbers() {
		return $this->m_arrobjPersonPhoneNumbers;
	}

	public function getPhoneNumber() {
		return $this->m_arrobjPersonPhoneNumber;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['person_employee_id'] ) ) $this->setPersonEmployeeId( $arrmixValues['person_employee_id'] );
		if( true == isset( $arrmixValues['person_contact_type_id'] ) ) $this->setPersonContactTypeId( $arrmixValues['person_contact_type_id'] );
		if( true == isset( $arrmixValues['ps_lead_id'] ) ) $this->setPsLeadId( $arrmixValues['ps_lead_id'] );
		if( true == isset( $arrmixValues['person_contact_preference_id'] ) ) $this->setPersonContactPreferenceId( $arrmixValues['person_contact_preference_id'] );
		if( true == isset( $arrmixValues['relationship_description'] ) ) $this->setRelationshipDescription( $arrmixValues['relationship_description'] );
		if( true == isset( $arrmixValues['person_name_full'] ) ) $this->setPersonNameFull( $arrmixValues['person_name_full'] );
		if( true == isset( $arrmixValues['person_type'] ) ) $this->setPersonType( $arrmixValues['person_type'] );
		if( true == isset( $arrmixValues['person_role'] ) ) $this->setPersonRole( $arrmixValues['person_role'] );
		if( true == isset( $arrmixValues['sales_employee_id'] ) ) $this->setSalesEmployeeId( $arrmixValues['sales_employee_id'] );
		if( true == isset( $arrmixValues['phone_number'] ) ) $this->setPhoneNumber( $arrmixValues['phone_number'] );
	}

	public function setNameFirst( $strNameFirst ) {
		parent::setNameFirst( getSanitizedFormField( $strNameFirst, true ) );
	}

	public function setNameLast( $strNameLast ) {
		parent::setNameLast( getSanitizedFormField( $strNameLast, true ) );
	}

	public function setCity( $strCity ) {
		parent::setCity( getSanitizedFormField( $strCity, true ) );
	}

	public function setCompanyName( $strCompanyName ) {
		parent::setCompanyName( getSanitizedFormField( $strCompanyName, true ) );
	}

	public function setTitle( $strTitle ) {
		parent::setTitle( getSanitizedFormField( $strTitle, true ) );
	}

	public function setPersonEmployeeId( $intPersonEmployeeId ) {
		$this->m_intPersonEmployeeId = $intPersonEmployeeId;
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->m_intPsLeadId = $intPsLeadId;
	}

	public function setPersonContactTypeId( $intPersonContactTypeId ) {
		$this->m_intPersonContactTypeId = $intPersonContactTypeId;
	}

	public function setLastEmailedOn( $strLastEmailedOn ) {
		$this->m_strLastEmailedOn = $strLastEmailedOn;
	}

	public function setIsChecked( $intIsChecked ) {
		$this->m_intIsChecked = $intIsChecked;
	}

	public function setHtmlContent( $strHtmlContent ) {
		$this->m_strHtmlContent = $strHtmlContent;
	}

	public function setRelationshipDescription( $strRelationshipDescription ) {
		$this->m_strRelationshipDescription = $strRelationshipDescription;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function setActionsCount( $intActionsCount ) {
		$this->m_intActionsCount = $intActionsCount;
	}

	public function setPersonContactPreferenceId( $intPersonContactPreferenceId ) {
		$this->m_intPersonContactPreferenceId = $intPersonContactPreferenceId;
	}

	public function setPersonNameFull( $strPersonNameFull ) {
		$this->m_strPersonNameFull = $strPersonNameFull;
	}

	public function setPersonType( $strPersonType ) {
		$this->m_strPersonType = $strPersonType;
	}

	public function setPersonRole( $strPersonRole ) {
		$this->m_strPersonRole = $strPersonRole;
	}

	public function setSalesEmployeeId( $intSalesEmployeeId ) {
		$this->m_intSalesEmployeeId = CStrings::strToIntDef( $intSalesEmployeeId, NULL, false );
	}

	public function setPhoneNumbers( $arrobjPersonPhoneNumbers ) {
		$this->m_arrobjPersonPhoneNumbers = $arrobjPersonPhoneNumbers;
	}

	public function setPhoneNumber( $arrobjPersonPhoneNumber ) {
		$this->m_arrobjPersonPhoneNumber = $arrobjPersonPhoneNumber;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPersonEmployees( $objDatabase ) {
		return CPersonEmployees::fetchCustomPersonEmployeesByPersonId( $this->getId(), $objDatabase );
	}

	public function fetchContracts( $objDatabase ) {
		return CContracts::createService()->fetchContractsByPersonId( $this->getId(), $objDatabase );
	}

	public function fetchActions( $objDatabase ) {
		return CActions::fetchActionsByPersonId( $this->getId(), $objDatabase );
	}

	public function fetchPsLead( $objDatabase ) {
		return \Psi\Eos\Admin\CPsLeads::createService()->fetchPsLeadById( $this->getPsLeadId(), $objDatabase );
	}

	public function fetchPersonOrigins( $objDatabase ) {
		return CPersonOrigins::createService()->fetchPersonOriginsByPersonId( $this->getId(), $objDatabase );
	}

	public function fetchTagActions( $objAdminDatabase ) {

		return CActions::fetchActionsByActionResultIdByPersonIds( CActionResult::PERSON_TAG, [ $this->getId() ], $objAdminDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valNameFirst( $strReference, $objClient = NULL, $boolIsRequired = false, $strPersonContactType = NULL ) {
		$boolIsValid = true;

		if( $strReference == self::COMPANY_PERSON ) {

			if( true == $boolIsRequired && CCompanyStatusType::CLIENT == $objClient->getCompanyStatusTypeId() && ( false == isset ( $this->m_strNameFirst ) || 0 == strlen( trim( $this->m_strNameFirst ) ) ) ) {
				$boolIsValid = false;

				if( true == is_null( $strPersonContactType ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_name', 'First name is required. ' ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( NULL, 'name_first', $strPersonContactType . ' contact first name is required.' ) );
				}
			}
		} elseif( $strReference == self::LEAD_PERSON ) {

			if( true == is_null( $this->getNameFirst() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_name', 'First name is required. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valNameLast( $strReference, $objClient = NULL, $boolIsRequired = false, $strPersonContactType = NULL ) {
		$boolIsValid = true;

		if( $strReference == self::COMPANY_PERSON ) {

			if( true == $boolIsRequired && CCompanyStatusType::CLIENT == $objClient->getCompanyStatusTypeId() && ( false == isset( $this->m_strNameLast ) || 0 == strlen( trim( $this->m_strNameLast ) ) ) ) {
				$boolIsValid = false;

				if( true == is_null( $strPersonContactType ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_name', 'Last name is required. ' ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( NULL, 'name_last', $strPersonContactType . ' contact last name is required.' ) );
				}
			}
		} elseif( $strReference == self::LEAD_PERSON ) {

			if( true == is_null( $this->getNameLast() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'last_name', 'Last name is required. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valOfficePhone( $strReference, $objClient = NULL, $boolIsRequired = false, $strPersonContactType = NULL ) {
		$boolIsValid = true;

		if( $strReference == self::COMPANY_PERSON && true == $boolIsRequired && CCompanyStatusType::CLIENT == $objClient->getCompanyStatusTypeId() && ( false == isset( $this->m_strPhoneNumber1 ) || 0 == strlen( trim( $this->m_strPhoneNumber1 ) ) ) ) {
			$boolIsValid = false;

			if( true == is_null( $strPersonContactType ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_name', 'Mobile number is required. ' ) );
			} else {
				$this->addErrorMsg( new CErrorMsg( NULL, 'office_phone_number', $strPersonContactType . ' contact phone number is required.' ) );
			}

		}

		return $boolIsValid;
	}

	public function valEmailAddress( $strReference, $objClient = NULL, $boolIsRequired = false, $strPersonContactType = NULL ) {
		$boolIsValid = true;

		if( $strReference == self::COMPANY_PERSON ) {

			if( true == $boolIsRequired && CCompanyStatusType::CLIENT == $objClient->getCompanyStatusTypeId() && ( false == isset( $this->m_strEmailAddress ) || 0 == strlen( trim( $this->m_strEmailAddress ) ) ) ) {
				$boolIsValid = false;

				if( true == is_null( $strPersonContactType ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_name', 'Email id is required. ' ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( NULL, 'email_address', $strPersonContactType . ' contact email address is required.' ) );
				}
			}

			if( true == isset( $this->m_strEmailAddress ) && 0 < strlen( trim( $this->m_strEmailAddress ) ) && false == CValidation::validateEmailAddresses( $this->m_strEmailAddress ) ) {
				$boolIsValid = false;

				if( true == is_null( $strPersonContactType ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'first_name', 'Email id is not valid.' ) );
				} else {
					$this->addErrorMsg( new CErrorMsg( NULL, 'email_address', $strPersonContactType . ' contact email address is not valid.' ) );
				}
			}
		} elseif( false == is_null( $this->getEmailAddress() ) ) {

			if( false == CValidation::validateEmailAddresses( $this->getEmailAddress() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Please enter a valid email address.' ) );
			} elseif( false == $this->validateEmailDomain( $this->getEmailAddress(), [ 'xento.com', 'entrata.com', 'propertysolutions.com' ] ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address with suffix as @xento.com, @entrata.com, @propertysolutions.com are not allowed.' ) );
			}
		} elseif( self::CORPORATE_ENTITY_PERSON == $strReference && false == valStr( $this->getEmailAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email id is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirstNameLastEmailAddress( $strReference, $strPersonContactType ) {
		$boolIsValid = true;

		if( $strReference == self::COMPANY_PERSON ) {
			if( ( false == is_null( $this->m_strTitle ) && 0 < strlen( trim( $this->m_strTitle ) ) ) || ( false == is_null( $this->m_strPhoneNumber1 ) && 0 < strlen( trim( $this->m_strPhoneNumber1 ) ) ) ) {
				if( true == is_null( $this->m_strNameFirst ) && true == is_null( $this->m_strNameLast ) && true == is_null( $this->m_strEmailAddress ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, 'name_first', 'First name , Last Name and Email is required for person type ' . \Psi\CStringService::singleton()->ucfirst( $strPersonContactType ) ) );
				}
			}
		}

		return $boolIsValid;
	}

	public function valOutageNotificationStatus( $arrintPersonContactTypes ) {
		$boolIsValid = true;

		if( !( $this->getEmailAddress() ) && ( in_array( CPersonContactType::OUTAGE_EMAIL, $arrintPersonContactTypes ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'Email required for opting Outage Email. ' ) );
		}

		return $boolIsValid;
	}

	public function valPersonalEmailAddress() {
		$boolIsValid = true;

		if( isset( $this->m_strPersonalEmailAddress ) && 0 < strlen( trim( $this->m_strPersonalEmailAddress ) ) && !CValidation::validateEmailAddresses( \Psi\CStringService::singleton()->strtolower( $this->m_strPersonalEmailAddress ) ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'personal_email_address', 'Personal email address is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objClient = NULL, $arrintPersonContactTypes = NULL ) {

		$boolIsValid			= true;
		$boolIsRequired			= false;
		$strPersonContactType	= '';

		switch( $strAction ) {

			case CPersonContactType::ACCOUNTING:
				$strPersonContactType	= 'Billing';
				$boolIsRequired	= true;
				$strAction		= self::COMPANY_PERSON;
				break;

			case CPersonContactType::FAILED_SETTLEMENT:
				$strPersonContactType	= 'Failed Settlement';
				$boolIsRequired	= true;
				$strAction		= self::COMPANY_PERSON;
				break;

			case CPersonContactType::OTHER:
				$strPersonContactType	= 'Miscellaneous';
				$boolIsRequired	= true;
				$strAction		= self::COMPANY_PERSON;
				break;

			case CPersonContactType::OWNER:
				$strPersonContactType	= 'Owner';
				$strAction				= self::COMPANY_PERSON;
				break;

			case CPersonContactType::IMPLEMENTATION:
				$strPersonContactType	= 'Implementation';
				$strAction				= self::COMPANY_PERSON;
				break;

			case CPersonContactType::MAINTENANCE:
				$strPersonContactType	= 'Property maintenence';
				$strAction				= self::COMPANY_PERSON;
				break;

			case CPersonContactType::MANAGER:
				$strPersonContactType	= 'Manager';
				$strAction				= self::COMPANY_PERSON;
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		switch( $strAction ) {

			case self::COMPANY_PERSON:
				$boolIsValid &= $this->valNameFirst( self::COMPANY_PERSON, $objClient, $boolIsRequired, $strPersonContactType );
				$boolIsValid &= $this->valNameLast( self::COMPANY_PERSON, $objClient, $boolIsRequired, $strPersonContactType );
				$boolIsValid &= $this->valEmailAddress( self::COMPANY_PERSON, $objClient, $boolIsRequired, $strPersonContactType );
				$boolIsValid &= $this->valOfficePhone( self::COMPANY_PERSON, $objClient, $boolIsRequired, $strPersonContactType );
				$boolIsValid &= $this->valNameFirstNameLastEmailAddress( self::COMPANY_PERSON, $strPersonContactType );
				if( true == valArr( $arrintPersonContactTypes ) ) {
					$boolIsValid &= $this->valOutageNotificationStatus( $arrintPersonContactTypes );
				}
				break;

			case 'validate_email':
				$boolIsValid &= $this->valEmailAddress( self::LEAD_PERSON );
				break;

			case 'validate_corporate_entity_email':
				$boolIsValid &= $this->valEmailAddress( self::CORPORATE_ENTITY_PERSON );
				break;

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valNameFirst( self::LEAD_PERSON );
				$boolIsValid &= $this->valNameLast( self::LEAD_PERSON );
				$boolIsValid &= $this->valEmailAddress( self::LEAD_PERSON );
				$boolIsValid &= $this->valPersonalEmailAddress();

				if( true == valArr( $arrintPersonContactTypes ) ) {
					$boolIsValid &= $this->valOutageNotificationStatus( $arrintPersonContactTypes );
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_corporate_entity':
			case 'validate_client_person':
				$boolIsValid &= $this->valNameFirst( self::LEAD_PERSON );
				$boolIsValid &= $this->valNameLast( self::LEAD_PERSON );
				$boolIsValid &= $this->valEmailAddress( self::CORPORATE_ENTITY_PERSON );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPersonContactPreferences( $objDatabase ) {

		return CPersonContactPreferences::createService()->fetchPersonContactPreferencesByPersonId( $this->getId(), $objDatabase );
	}

	/**
	 * Other Functions
	 */

	public function mergePersons( $intUserId, $arrobjMergePersons, $objDatabase ) {

		if( false == valArr( $arrobjMergePersons ) ) return false;

		foreach( $arrobjMergePersons as $objMergePerson ) {
			$this->m_arrstrPersons[] = $objMergePerson->getId();
		}

		foreach( $arrobjMergePersons as $objMergePerson ) {

			$this->setPsLeadId( $objMergePerson->getPsLeadId() );

			if( true == is_null( $this->getNameFirst() ) && false == is_null( $objMergePerson->getNameFirst() ) ) {
				$this->setNameFirst( $objMergePerson->getNameFirst() );
			}

			if( true == is_null( $this->getNameLast() ) && false == is_null( $objMergePerson->getNameLast() ) ) {
				$this->setNameLast( $objMergePerson->getNameLast() );
			}

			if( true == is_null( $this->getTitle() ) && false == is_null( $objMergePerson->getTitle() ) ) {
				$this->setTitle( $objMergePerson->getTitle() );
			}

			if( true == is_null( $this->getPersonSalesRoleId() ) && false == is_null( $objMergePerson->getPersonSalesRoleId() ) ) {
				$this->setPersonSalesRoleId( $objMergePerson->getPersonSalesRoleId() );
			}

			if( true == is_null( $this->getPersonRoleId() ) && false == is_null( $objMergePerson->getPersonRoleId() ) ) {
				$this->setPersonRoleId( $objMergePerson->getPersonRoleId() );
			}

			if( true == is_null( $this->getCompanyUserId() ) && false == is_null( $objMergePerson->getCompanyUserId() ) ) {
				$this->setCompanyUserId( $objMergePerson->getCompanyUserId() );
			}

			if( true == is_null( $this->getEmailAddress() ) && false == is_null( $objMergePerson->getEmailAddress() ) ) {
				$this->setEmailAddress( $objMergePerson->getEmailAddress() );
			}

			if( true == is_null( $this->getAddressLine1() ) && false == is_null( $objMergePerson->getAddressLine1() ) ) {
				$this->setAddressLine1( $objMergePerson->getAddressLine1() );
			}

			if( true == is_null( $this->getAddressLine2() ) && false == is_null( $objMergePerson->getAddressLine2() ) ) {
				$this->setAddressLine2( $objMergePerson->getAddressLine2() );
			}

			if( true == is_null( $this->getCity() ) && false == is_null( $objMergePerson->getCity() ) ) {
				$this->setCity( $objMergePerson->getCity() );
			}

			if( true == is_null( $this->getStateCode() ) && false == is_null( $objMergePerson->getStateCode() ) ) {
				$this->setStateCode( $objMergePerson->getStateCode() );
			}

			if( true == is_null( $this->getPostalCode() ) && false == is_null( $objMergePerson->getPostalCode() ) ) {
				$this->setPostalCode( $objMergePerson->getPostalCode() );
			}

			switch( NULL ) {
				default:
					$boolIsValid = true;
					$boolIsValid &= $this->validate( VALIDATE_UPDATE );
					$boolIsValid &= $objMergePerson->validate( VALIDATE_DELETE, $objDatabase );

					if( false == $boolIsValid ) {
						return false;
					}

					if( false == $this->update( $intUserId, $objDatabase ) ) {
						return false;
					}

					if( false == $objMergePerson->delete( $intUserId, $objDatabase ) ) {
						return false;
					}
			}
		}

		$this->processPersonPhoneNumberMerging( $this->m_arrstrPersons, $objDatabase, $intUserId );

		// Updating person origin into merged value
		$arrobjUpdatingPersonOrigins		= $this->fetchPersonOrigins( $objDatabase );
		$arrobjUpdatingPersonOrigins		= rekeyObjects( 'PsLeadOriginId', $arrobjUpdatingPersonOrigins );
		$arrobjReassigningPersonOrigins		= CPersonOrigins::createService()->fetchPersonOriginsByPersonIds( $this->m_arrstrPersons, $objDatabase );
		$arrobjUpadtingReassigningPersonOrigins = [];
		if( true == valArr( $arrobjReassigningPersonOrigins ) ) {
			foreach( $arrobjReassigningPersonOrigins as $objReassigningPersonOrigin ) {
				if( false == valArr( $arrobjUpdatingPersonOrigins ) || false == array_key_exists( $objReassigningPersonOrigin->getPsLeadOriginId(), $arrobjUpdatingPersonOrigins ) || false == valObj( $arrobjUpdatingPersonOrigins[$objReassigningPersonOrigin->getPsLeadOriginId()], 'CPersonOrigin' ) ) {

					$objReassigningPersonOrigin->setPersonId( $this->getId() );

					$boolIsValid = $objReassigningPersonOrigin->validate( VALIDATE_UPDATE, $boolCheckPersonId = false, $objDatabase );
					if( false == $boolIsValid ) {
						return false;
					}

					$arrobjUpadtingReassigningPersonOrigins[] = $objReassigningPersonOrigin;
				}
			}
		}
		if( valArr( $arrobjUpadtingReassigningPersonOrigins ) && !CPersonOrigins::createService()->bulkUpdate( $arrobjUpadtingReassigningPersonOrigins, [ 'person_id' ], $intUserId, $objDatabase ) ) {
			return false;
		}

		// Updating contacts into merged value
		$arrobjActions = CActions::fetchActionsByPersonIds( $this->m_arrstrPersons, $objDatabase );
		$arrobjUpdatingActions = [];
		if( true == valArr( $arrobjActions ) ) {
			foreach( $arrobjActions as $objAction ) {
				$objAction->setPersonId( $this->getId() );

				$boolIsValid = $objAction->validate( VALIDATE_UPDATE, false );
				if( false == $boolIsValid ) {
					return false;
				}

				$arrobjUpdatingActions[] = $objAction;

			}
		}
		if( valArr( $arrobjUpdatingActions ) && !CActions::bulkUpdate( $arrobjUpdatingActions, [ 'person_id' ], $intUserId, $objDatabase ) ) {
			return false;
		}

		// Updating contracts into merged value
		$arrobjContracts = CContracts::createService()->fetchContractsByPersonIds( $this->m_arrstrPersons, $objDatabase );
		$arrobjUpdatingContracts = [];
		if( true == valArr( $arrobjContracts ) ) {
			foreach( $arrobjContracts as $objContract ) {
				if( $this->getPsLeadId() == $objContract->getPsLeadId() ) {

					$objContract->setPersonId( $this->getId() );

					$boolIsValid = $objContract->validate( VALIDATE_UPDATE, $objDatabase );
					if( false == $boolIsValid ) {
						return false;
					}
					$arrobjUpdatingContracts[] = $objContract;
				}
			}
		}
		if( valArr( $arrobjUpdatingContracts ) && !CContracts::createService()->bulkUpdate( $arrobjUpdatingContracts, [ 'person_id' ], $intUserId, $objDatabase ) ) {
			return false;
		}

		// Updating person employee info into merged value
		$arrobjPersonEmployees	= CPersonEmployees::fetchPersonEmployeesByPersonIds( $this->m_arrstrPersons, $objDatabase );
		if( true == valArr( $arrobjPersonEmployees ) ) {
			foreach( $arrobjPersonEmployees as $objPersonEmployee ) {

				$objPersonEmployee->setPersonId( $this->getId() );
				$objPersonEmployee->setPsLeadId( $this->getPsLeadId() );
				$boolIsValid = $objPersonEmployee->validate( VALIDATE_UPDATE, $objDatabase );
				if( false == $boolIsValid ) {

					$boolIsValid = $objPersonEmployee->validate( VALIDATE_DELETE, $objDatabase );
					if( false == $boolIsValid ) {
						return false;
					}

					if( false == $objPersonEmployee->delete( $intUserId, $objDatabase ) ) {
						return false;
					}
				} elseif( false == $objPersonEmployee->update( $intUserId, $objDatabase ) ) {
					return false;
				}
			}
		}
		// Updating tags into merged value
		$arrobjAssociatedTagActions				= CActions::fetchActionsByActionResultIdByPersonIds( CActionResult::PERSON_TAG, $this->m_arrstrPersons, $objDatabase );
		$arrobjCurrentUserAssociatedTagActions	= $this->fetchTagActions( $objDatabase );
		$arrobjCurrentUserAssociatedTagActions	= rekeyObjects( 'PrimaryReference', $arrobjCurrentUserAssociatedTagActions );
		$arrobjUpdatingAssociatedTagActions = [];

		if( true == valArr( $arrobjAssociatedTagActions ) ) {
			foreach( $arrobjAssociatedTagActions as $objAssociatedTagAction ) {
				if( $this->getId() != $objAssociatedTagAction->getPersonId() ) {
					if( false == valArr( $arrobjCurrentUserAssociatedTagActions ) || ( true == valArr( $arrobjCurrentUserAssociatedTagActions ) && false == isset( $arrobjCurrentUserAssociatedTagActions[$objAssociatedTagAction->getPrimaryReference()] ) ) ) {

						$objAssociatedTagAction->setPersonId( $this->getId() );

						$boolIsValid = $objAssociatedTagAction->validate( VALIDATE_UPDATE, $objDatabase );
						if( false == $boolIsValid ) {
							return false;
						}

						$arrobjUpdatingAssociatedTagActions[] = $objAssociatedTagAction;
					}

				}
			}
		}
		if( valArr( $arrobjUpdatingAssociatedTagActions ) && !CActions::bulkUpdate( $arrobjUpdatingAssociatedTagActions, [ 'person_id' ], $intUserId, $objDatabase ) ) {
			return false;
		}

		// Updating person types into merged value
		// fetching additional person contact types and inserting in the row in which to be merged
		$arrobjPersonContactPreferences		= CPersonContactPreferences::createService()->fetchPersonContactPreferencesToMergeByPersonIds( $this->m_arrstrPersons, $objDatabase );
		$arrobjPersonOldContactPreferences	= CPersonContactPreferences::createService()->fetchPersonContactPreferencesByPersonId( $this->getId(), $objDatabase );

		foreach( $arrobjPersonOldContactPreferences as $objPersonOldContactPreference ) {
			$this->m_arrstrPersonOldContactPreferences[] = $objPersonOldContactPreference->getPersonContactTypeId();
		}

		foreach( $arrobjPersonContactPreferences as $objPersonContactPreference ) {
			$this->m_arrstrPersonNewContactPreferences[] = $objPersonContactPreference->getPersonContactTypeId();
		}

		foreach( $this->m_arrstrPersonNewContactPreferences as $strNewContactType ) {
			if( in_array( $strNewContactType, $this->m_arrstrPersonOldContactPreferences ) ) {
				continue;
			}

			$this->m_arrstrPersonDifferencePersonContactTypes[] = $strNewContactType;
		}

		if( false == empty( $this->m_arrstrPersonDifferencePersonContactTypes ) ) {
			$this->m_arrstrPersonDifferencePersonContactTypes = array_unique( $this->m_arrstrPersonDifferencePersonContactTypes );
		}

		if( true == valArr( $arrobjPersonContactPreferences ) ) {

			if( true == valArr( $this->m_arrstrPersonDifferencePersonContactTypes ) ) {

				foreach( $this->m_arrstrPersonDifferencePersonContactTypes as $strPersonDifferenceContactType ) {

					$objPersonContactPreferenceNew = new CPersonContactPreference();

					$objPersonContactPreferenceNew->setPersonId( $this->getId() );
					$objPersonContactPreferenceNew->setPsLeadId( $this->getPsLeadId() );
					$objPersonContactPreferenceNew->setPersonContactTypeId( $strPersonDifferenceContactType );

					if( false == $objPersonContactPreferenceNew->insert( $intUserId, $objDatabase ) ) {
						return false;
					}
				}

			}

			foreach( $arrobjPersonContactPreferences as $objPersonContactPreference ) {

				if( false == $objPersonContactPreference->delete( $intUserId, $objDatabase ) ) {
					return false;
				}
			}

		}

		return true;
	}

	public function processPersonPhoneNumberMerging( $arrintPersonIds, $objDatabase,$intUserId ) {

		$arrobjInsertingPersonPhoneNumbers = [];

		if( !valArr( $arrintPersonIds ) ) {
			return false;
		}

		$arrobjPersonPhoneNumbers			= CPersonPhoneNumbers::createService()->fetchPersonPhoneNumbersByPersonIds( [ $this->getId() ], $objDatabase );
		$arrobjAllPersonPhoneNumbers		= rekeyObjects( 'PersonId', CPersonPhoneNumbers::createService()->fetchPersonPhoneNumbersByPersonIds( $arrintPersonIds, $objDatabase ), true );
		$arrobjMergingPersonPhoneNumbers	= [];

		if( !valArr( $arrobjAllPersonPhoneNumbers ) ) {
			return false;
		}

		foreach( $arrobjAllPersonPhoneNumbers as $arrobjPhoneNumbers ) {
			foreach( $arrobjPhoneNumbers as $objPhoneNumber ) {
				if( in_array( $objPhoneNumber->getPhoneNumberTypeId(), [ CPhoneNumberType::OFFICE, CPhoneNumberType::MOBILE, CPhoneNumberType::FAX ] ) ) {
					$arrobjMergingPersonPhoneNumbers[$objPhoneNumber->getPhoneNumberTypeId()][] = $objPhoneNumber;
				} else {
					$arrobjMergingPersonPhoneNumbers[CPhoneNumberType::OTHER][] = $objPhoneNumber;
				}
			}
		}

		for( $intPhoneNumberCount = \Psi\Libraries\UtilFunctions\count( $arrobjPersonPhoneNumbers ); $intPhoneNumberCount < CPersonPhoneNumber::MAX_NUMBER_OF_PHONE_NUMBERS; $intPhoneNumberCount++ ) {
			if( isset( $arrobjMergingPersonPhoneNumbers[CPhoneNumberType::OFFICE] ) && valArr( $arrobjMergingPersonPhoneNumbers[CPhoneNumberType::OFFICE] ) ) {
				$arrobjInsertingPersonPhoneNumbers[] = array_shift( $arrobjMergingPersonPhoneNumbers[CPhoneNumberType::OFFICE] );
			} elseif( isset( $arrobjMergingPersonPhoneNumbers[CPhoneNumberType::MOBILE] ) && valArr( $arrobjMergingPersonPhoneNumbers[CPhoneNumberType::MOBILE] ) ) {
				$arrobjInsertingPersonPhoneNumbers[] = array_shift( $arrobjMergingPersonPhoneNumbers[CPhoneNumberType::MOBILE] );
			} elseif( isset( $arrobjMergingPersonPhoneNumbers[CPhoneNumberType::FAX] ) && valArr( $arrobjMergingPersonPhoneNumbers[CPhoneNumberType::FAX] ) ) {
				$arrobjInsertingPersonPhoneNumbers[] = array_shift( $arrobjMergingPersonPhoneNumbers[CPhoneNumberType::FAX] );
			} elseif( isset( $arrobjMergingPersonPhoneNumbers[CPhoneNumberType::OTHER] ) && valArr( $arrobjMergingPersonPhoneNumbers[CPhoneNumberType::OTHER] ) ) {
				$arrobjInsertingPersonPhoneNumbers[] = array_shift( $arrobjMergingPersonPhoneNumbers[CPhoneNumberType::OTHER] );
			}
		}

		if( !valArr( $arrobjInsertingPersonPhoneNumbers ) ) {
			return false;
		}

		$objPersonPhoneNumber = new CPersonPhoneNumber();
		$arrobjNewPhoneNumbers = [];
		$arrobjDeletingPhoneNumbers = [];

		foreach( $arrobjInsertingPersonPhoneNumbers as $objPhoneNumber ) {
			$objClonedPhoneNumber = clone $objPersonPhoneNumber;
			$objClonedPhoneNumber->setPersonId( $this->getId() );
			$objClonedPhoneNumber->setPsLeadId( $this->getPsLeadId() );
			$objClonedPhoneNumber->setPhoneNumberTypeId( $objPhoneNumber->getPhoneNumberTypeId() );
			$objClonedPhoneNumber->setPhoneNumber( $objPhoneNumber->getPhoneNumber() );
			$objClonedPhoneNumber->setExtension( $objPhoneNumber->getExtension() );
			$objClonedPhoneNumber->setIsPreferred( $objPhoneNumber->getIsPreferred() );
			$arrobjNewPhoneNumbers[] = $objClonedPhoneNumber;

			$objPhoneNumber->setDeletedOn( 'NOW()' );
			$objPhoneNumber->setDeletedBy( $intUserId );
			$arrobjDeletingPhoneNumbers[] = $objPhoneNumber;
		}

		if( valArr( $arrobjNewPhoneNumbers ) && !CPersonPhoneNumbers::createService()->bulkInsert( $arrobjNewPhoneNumbers, $intUserId, $objDatabase ) ) {
			return false;
		}

		if( valArr( $arrobjDeletingPhoneNumbers ) && !CPersonPhoneNumbers::createService()->bulkUpdate( $arrobjDeletingPhoneNumbers, [ 'deleted_by', 'deleted_on' ], $intUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsDefaultPersonContactTypesRequire = true ) {

		$strSql = '';

		if( false == $boolReturnSqlOnly ) {
			if( false == parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) ) return false;
		} else {
			$strSql = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		}

		if( true == $boolIsDefaultPersonContactTypesRequire ) {
			$arrstrDefaultPersonContactTypes = [ CPersonContactType::ADVERTISEMENT, CPersonContactType::SALES_CALL ];

			foreach( $arrstrDefaultPersonContactTypes as $strDefaultPersonContactType ) {
				$objContactPreference = new CPersonContactPreference();
				$objContactPreference->setPersonId( $this->getId() );
				$objContactPreference->setPsLeadId( $this->getPsLeadId() );
				$objContactPreference->setPersonContactTypeId( $strDefaultPersonContactType );

				if( false == $boolReturnSqlOnly ) {
					if( false == $objContactPreference->insert( $intCurrentUserId, $objDatabase ) ) return false;
				} else {
					$strSql .= $objContactPreference->insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
				}
			}
		}

		if( false == $boolReturnSqlOnly ) {
			return true;
		}
		return $strSql;
	}

	public function getPhoneNumberByPhoneNumberTypeId( $intPhoneNumberTypeId, $arrobjPersonPhoneNumbers ) {

		$this->m_strPhoneNumber = NULL;

		if( valArr( $arrobjPersonPhoneNumbers ) && valArr( $arrobjPersonPhoneNumbers = rekeyObjects( 'PhoneNUmberTypeId', $arrobjPersonPhoneNumbers ) ) && isset( $arrobjPersonPhoneNumbers[$intPhoneNumberTypeId] ) ) {
			$this->m_strPhoneNumber = $arrobjPersonPhoneNumbers[$intPhoneNumberTypeId]->getPhoneNumber();
		}
		return $this->m_strPhoneNumber;
	}

	public function checkDuplicatePerson( $objDataBase ) {

		$objPerson = \Psi\Eos\Admin\CPersons::createService()->fetchPersonByFirstNameByLastNameByCidByEmailAddress( $this->getNameFirst(), $this->getNameLast(), $this->getCid(), $this->getEmailAddress(), $objDataBase );

		if( true == valObj( $objPerson, 'CPerson' ) ) {
			return $objPerson->getId();
		}
		return NULL;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( 'NOW()' );

		return parent::update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function validateEmailDomain( $strEmailAddress, $arrstrRestrictedDomains ) {

		if( false == valStr( $strEmailAddress ) ) return false;

		$strDomainName = array_pop( explode( '@', $strEmailAddress ) );

		return !( true == in_array( \Psi\CStringService::singleton()->strtolower( $strDomainName ), $arrstrRestrictedDomains ) );
	}

}
?>