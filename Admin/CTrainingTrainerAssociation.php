<?php

class CTrainingTrainerAssociation extends CBaseTrainingTrainerAssociation {

	protected $m_intEmployeeId;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPreferredName;
	protected $m_strDepartmentName;
	protected $m_strDesignationName;
	protected $m_intUserId;
	protected $m_strTagName;
	protected $m_intEmployeeAssessmentTypeId;
	protected $m_strCurrentTitle;
	protected $m_strEmailAddress;
	protected $m_intCellNumber;
	protected $m_intEmployeeApplicationId;
	protected $m_intActionResultId;

	/**
	 * GET FUNCTIONS
	 *
	 */

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getDesignationName() {
		return $this->m_strDesignationName;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getTagName() {
		return $this->m_strTagName;
	}

	public function getEmployeeAssessmentTypeId() {
		return $this->m_intEmployeeAssessmentTypeId;
	}

	public function getCurrentTitle() {
		return $this->m_strCurrentTitle;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getCellNumber() {
		return  $this->m_intCellNumber;
	}

	public function getEmployeeApplicationId() {
		return $this->m_intEmployeeApplicationId;
	}

	public function getActionResultId() {
		return $this->m_intActionResultId;
	}

	/**
	 * SET FUNCTIONS
	 */

	public function setDepartmentName( $m_strDepartmentName ) {
		$this->m_strDepartmentName = $m_strDepartmentName;
	}

	public function setDesignationName( $m_strDesignationName ) {
		$this->m_strDesignationName = $m_strDesignationName;
	}

	public function setEmployeeId( $m_intEmployeeId ) {
		$this->m_intEmployeeId = $m_intEmployeeId;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setTagName( $m_strTagName ) {
		$this->m_strTagName = $m_strTagName;
	}

	public function setEmployeeAssessmentTypeId( $m_intEmployeeAssessmentTypeId ) {
		$this->m_intEmployeeAssessmentTypeId = $m_intEmployeeAssessmentTypeId;
	}

	public function setCurrentTitle( $m_strCurrentTitle ) {
		$this->m_strCurrentTitle = $m_strCurrentTitle;
	}

	public function setEmailAddress( $m_strEmailAddress ) {
		$this->m_strEmailAddress = $m_strEmailAddress;
	}

	public function setCellNumber( $m_intCellNumber ) {
		$this->m_intCellNumber = $m_intCellNumber;
	}

	public function setEmployeeApplicationId( $m_intEmployeeApplicationId ) {
		$this->m_intEmployeeApplicationId = $m_intEmployeeApplicationId;
	}

	public function setPreferredName( $strPreferredName ) {
		$this->m_strPreferredName = $strPreferredName;
	}

	public function setActionResultId( $intActionResultId ) {
		$this->m_intActionResultId = $intActionResultId;
	}

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrValues['employee_id'] ) ) $this->setEmployeeId( $arrValues['employee_id'] );
		if( true == isset( $arrValues['department_name'] ) ) $this->setDepartmentName( $arrValues['department_name'] );
		if( true == isset( $arrValues['designation_name'] ) ) $this->setDesignationName( $arrValues['designation_name'] );
		if( true == isset( $arrValues['name_first'] ) ) $this->setNameFirst( $arrValues['name_first'] );
		if( true == isset( $arrValues['name_last'] ) ) $this->setNameLast( $arrValues['name_last'] );
		if( true == isset( $arrValues['preferred_name'] ) ) $this->setPreferredName( $arrValues['preferred_name'] );
		if( true == isset( $arrValues['user_id'] ) ) $this->setUserId( $arrValues['user_id'] );
		if( true == isset( $arrValues['tag_name'] ) ) $this->setTagName( $arrValues['tag_name'] );
		if( true == isset( $arrValues['employee_assessment_type_id'] ) ) $this->setEmployeeAssessmentTypeId( $arrValues['employee_assessment_type_id'] );
		if( true == isset( $arrValues['current_title'] ) ) $this->setCurrentTitle( $arrValues['current_title'] );
		if( true == isset( $arrValues['email_address'] ) ) $this->setEmailAddress( $arrValues['email_address'] );
		if( true == isset( $arrValues['cell_number'] ) ) $this->setCellNumber( $arrValues['cell_number'] );
		if( true == isset( $arrValues['employee_application_id'] ) ) $this->setEmployeeApplicationId( $arrValues['employee_application_id'] );
		if( true == isset( $arrValues['action_result_id'] ) ) $this->setActionResultId( $arrValues['action_result_id'] );

		return;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				break;
		}
		return $boolIsValid;
	}

}
?>