<?php

class CCommunicationTemplateType extends CBaseCommunicationTemplateType {

	const APPLICANT_TRACKING_SYSTEM	= 1;
	const TRAINING_AND_DEVELOPMENT	= 2;
	const EMAIL_SIGNATURE			= 3;
	const RP_FEEDBACK				= 4;
	const COMPENSATION_REVIEW_SYSTEM = 5;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>