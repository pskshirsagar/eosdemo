<?php

class CLeadPackageProductDiscount extends CBaseLeadPackageProductDiscount {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadPackageProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDependentProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSetUpDiscountAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRecurringDiscountAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>