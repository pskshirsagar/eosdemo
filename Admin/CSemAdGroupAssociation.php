<?php

class CSemAdGroupAssociation extends CBaseSemAdGroupAssociation {

	/**
	 * Val Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createSemAdGroupAssociationSource() {

		$objSemAdGroupAssociationSource = new CSemAdGroupAssociationSource();

		$objSemAdGroupAssociationSource->setSemAdGroupAssociationId( $this->getId() );

		return $objSemAdGroupAssociationSource;
	}

	/**
	 * Insert Functions
	 */

	public function insert( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly = false ) {

		$boolIsValid = parent::insert( $intCurrentUserId, $objAdminDatabase, $boolReturnSqlOnly );

		// We need to create sem ad group association sources records too
		$objSemAdGroup = $this->fetchSemAdGroup( $objAdminDatabase );

		$arrobjSemAdGroupSources 	= $objSemAdGroup->fetchSemAdGroupSources( $objAdminDatabase );

		if( false == valArr( $arrobjSemAdGroupSources ) ) {
			return $boolIsValid;
		}

		$arrobjSemAdGroupAssociationSources = array();

		foreach( $arrobjSemAdGroupSources as $objSemAdGroupSource ) {
			$objSemAdGroupAssociationSource = $this->createSemAdGroupAssociationSource();

			$objSemAdGroupAssociationSource->setSemSourceId( $objSemAdGroupSource->getSemSourceId() );

			$arrobjSemAdGroupAssociationSources[] = $objSemAdGroupAssociationSource;
		}

		switch( NULL ) {
			default:
				if( true == valArr( $arrobjSemAdGroupAssociationSources ) ) {
					foreach( $arrobjSemAdGroupAssociationSources as $objSemAdGroupAssociationSource ) {
						$boolIsValid &= $objSemAdGroupAssociationSource->validate( VALIDATE_INSERT );
					}
				}

				if( false == $boolIsValid ) {
					break;
				}

				if( true == valArr( $arrobjSemAdGroupAssociationSources ) ) {
					foreach( $arrobjSemAdGroupAssociationSources as $objSemAdGroupAssociationSource ) {
						if( false == $objSemAdGroupAssociationSource->insert( $intCurrentUserId, $objAdminDatabase ) ) {
							$boolIsValid = false;
							break;
						}
					}
				}

		}

		return $boolIsValid;
	}

	/**
	 * fetch Functions
	 */

	public function fetchSemAdGroup( $objAdminDatabase ) {
		return CSemAdGroups::fetchSemAdGroupById( $this->getSemAdGroupId(), $objAdminDatabase );
	}

	public function fetchSemAdGroupAssociationSources( $objAdminDatabase ) {
		return CSemAdGroupAssociationSources::fetchSemAdGroupAssociationSourcesBySemAdGroupAssociationId( $this->getId(), $objAdminDatabase );
	}

	/**
	 * Other Functions
	 */

	public function delete( $intCurrentUserId, $objProperty, $objAdminDatabase = NULL ) {

		$boolIsValid = true;
		// delte all sem kewords associations related to this ad group and property
		$arrobjSemKeywordAssociations 	= $objProperty->fetchSemKeywordAssociationsBySemAdGroupId( $this->getSemAdGroupId(), $objAdminDatabase );

		// We need to create sem ad group association sources records too
		$arrobjSemAdGroupAssociationSources = $this->fetchSemAdGroupAssociationSources( $objAdminDatabase );

		if( true == valArr( $arrobjSemKeywordAssociations ) ) {
			foreach( $arrobjSemKeywordAssociations as $objSemKeywordAssociation ) {
				$boolIsValid &= $objSemKeywordAssociation->delete( $intCurrentUserId, $objAdminDatabase );
			}
		}

		if( true == valArr( $arrobjSemAdGroupAssociationSources ) ) {
			switch( NULL ) {
				default:

					if( true == valArr( $arrobjSemAdGroupAssociationSources ) ) {
						foreach( $arrobjSemAdGroupAssociationSources as $objSemAdGroupAssociationSource ) {
							$boolIsValid &= $objSemAdGroupAssociationSource->validate( VALIDATE_DELETE );
						}
					}

					if( false == $boolIsValid ) {
						break;
					}

					if( true == valArr( $arrobjSemAdGroupAssociationSources ) ) {
						foreach( $arrobjSemAdGroupAssociationSources as $objSemAdGroupAssociationSource ) {
							if( false == $objSemAdGroupAssociationSource->delete( $intCurrentUserId, $objAdminDatabase ) ) {
								$boolIsValid &= false;
								break;
							}
						}
					}

			}
		}

		$boolIsValid &= parent::delete( $intCurrentUserId, $objAdminDatabase );

		return $boolIsValid;
	}
}
?>