<?php

class CHelpResourcesTrainerAssociation extends CBaseHelpResourcesTrainerAssociation {

	const DEFAULT_TRAINER_PERMISSION	= 1;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTrainerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpResourcesTrainerPermissionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>