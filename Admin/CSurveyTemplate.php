<?php

class CSurveyTemplate extends CBaseSurveyTemplate {

	protected $m_strSurveyType;

	protected $m_intQuestion;
	protected $m_intSurveyTypeId;

	protected $m_intSurveyOrSurveyTemplateQuestionCount;

	protected $m_boolIsAllowEdit;
	protected $m_boolIsAllowDelete;

	const EMPLOYEE_NPS							= 1;
	const CUSTOMER_NPS							= 2;
	const PERSONAL_REVIEW_US					= 4;
	const PEER_REVIEW							= 5;
	const SUPPORT_REVIEW 						= 6;
	const LEASING_CENTER_QA_SURVEY				= 10;
	const SUPPORT_QA_CALLS_SURVEY				= 11;
	const SUPPORT_QA_CHATS_SURVEY				= 12;
	const TASK_AUDIT_SURVEY						= 15;
	const PERSONAL_REVIEW_MANAGER_SURVEY		= 17;
	const LEASING_CENTER_QA_DISPUTE_SURVEY		= 18;
	const PERSONAL_REVIEW_IN					= 19;
	const DEMO_POST_TRAINING_SURVEY 			= 20;
	const JUSTIFICATION_FOR_NON_ATTENDENCE 		= 22;
	const PERSONAL_REVIEW_MANAGER_SURVEY_IN 	= 25;
	const DEMO_TRAINER_SURVEY					= 29;
	const POST_TRAINING_FEEDBACK 				= 30;
	const POST_TRAINING_FEEDBACK_SEPT			= 31;
	const XENTO_FUN_EXPRESS						= 33;
	const TEST_LINK_SESSION_FEEDBACK 			= 34;
	const TRAINING_SESSION_FEEDBACK_BEST_TEAM 	= 35;
	const TSD_SESSION_FEEDBACK					= 36;
	const ANDROID_SESSION_FEEDBACK 				= 37;
	const DATABASE_INDEXES_SESSION_FEEDBACK 	= 38;
	const LEASING_CENTER_QA_STANDARD_SURVEY		= 40;
	const SESSION								= 42;
	const NOW_HABBITS_AT_WORK					= 43;
	const TNA_EMPLOYEES							= 44;
	const TNA_ACCOUNTS_DEPARTMENT				= 45;
	const TNA_ADMIN_DEPARTMENT					= 46;
	const TNA_HR_OPERATIONS						= 47;
	const TNA_HR_RECRUITMENT					= 48;
	const TNA_PHP_DEVELOPERS					= 49;
	const TNA_DOTNET_DEVELOPERS					= 51;
	const TNA_UI_DEVELOPERS						= 52;
	const TNA_MOBILITY_TEAM						= 53;
	const TNA_SUPPORT							= 54;
	const TNA_QA								= 55;
	const TNA_IT								= 57;
	const POST_JOINING_SATISFACTION_SURVEY		= 58;
	const WHOLE_TEAM_APPROCH_SURVEY 			= 61;
	const INTERVIEW_FEEDBACK_SURVEY				= 60;
	const NO_DUES_FINANCE						= 83;
	const NO_DUES_ADMIN							= 84;
	const NO_DUES_TRAINING						= 85;
	const NO_DUES_SYSTEM_ADMINISTRATION			= 86;
	const NO_DUES_IT							= 87;
	const NO_DUES_SECURITY						= 88;
	const NO_DUES_HR							= 89;
	const NO_DUES_MANAGER						= 90;
	const NEW_LEASING_CENTER_QA_SURVEY			= 94;
	const NEW_LEASING_CENTER_QA_DISPUTE_SURVEY	= 95;
	const NEW_LEASING_CENTER_QA_STANDARD_SURVEY	= 96;
	const QUARTERLY_FEEDBACK_NEW_JOINEES		= 97;
	const XENTO_ENHANCED_LEARNING_PROGRAM		= 98;
	const NEW_LEASING_CENTER_QA_REVIEW_SURVEY	= 104;
	const HELPDESK_SURVEY						= 115;

	const SUPPORT_QA_CALL_STANDARD_SURVEY		= 117;
	const SUPPORT_QA_CALL_DISPUTE_SURVEY		= 118;
	const EXIT_INTERVIEW						= 121;

	const NEW_SUPPORT_QA_CALL_SURVEY			= 133;
	const NEW_SUPPORT_QA_CALL_STANDARD_SURVEY	= 134;
	const NEW_SUPPORT_QA_CALL_DISPUTE_SURVEY	= 135;

	const NEW_SUPPORT_TASK_QA_SURVEY			= 141;
	const NEW_SUPPORT_QA_TASK_STANDARD_SURVEY	= 140;
	const NEW_SUPPORT_QA_TASK_DISPUTE_SURVEY	= 142;

	const PRE_BILL_APPROVAL_SURVEY              = 145;
	const REJECT_CANDIDATE						= 156;
	const IT_HELPDESK_SURVEY					= 220;

	public static $c_arrintSurveyTemplatesForFiveStarRating	= array(
		self::PERSONAL_REVIEW_IN,
		self::PERSONAL_REVIEW_MANAGER_SURVEY_IN,
		self::PERSONAL_REVIEW_US,
		self::DEMO_POST_TRAINING_SURVEY,
		self::POST_TRAINING_FEEDBACK,
		self::POST_TRAINING_FEEDBACK_SEPT,
		self::XENTO_FUN_EXPRESS,
		self::TEST_LINK_SESSION_FEEDBACK,
		self::TRAINING_SESSION_FEEDBACK_BEST_TEAM,
		self::TSD_SESSION_FEEDBACK,
		self::ANDROID_SESSION_FEEDBACK,
		self::DATABASE_INDEXES_SESSION_FEEDBACK,
		self::SESSION,
		self::NOW_HABBITS_AT_WORK,
		self::POST_JOINING_SATISFACTION_SURVEY,
		self::WHOLE_TEAM_APPROCH_SURVEY,
		self::XENTO_ENHANCED_LEARNING_PROGRAM,
		self::HELPDESK_SURVEY,
		self::TASK_AUDIT_SURVEY,
		self::EMPLOYEE_NPS,
		self::PEER_REVIEW
	);

	public static $c_arrintNoDuesSurveyTemplates = array(
		self::NO_DUES_FINANCE,
		self::NO_DUES_ADMIN,
		self::NO_DUES_TRAINING,
		self::NO_DUES_SYSTEM_ADMINISTRATION,
		self::NO_DUES_IT,
		self::NO_DUES_SECURITY,
		self::NO_DUES_HR,
		self::NO_DUES_MANAGER
	);

	public function __construct() {
		parent::__construct();

		$this->m_boolIsAllowEdit	= true;
		$this->m_boolIsAllowDelete	= true;

		return;
	}

	public function valSurveyTypeId() {
		$boolIsValid = true;

		if( 0 == ( int ) $this->m_intSurveyTypeId ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_type_id', 'Survey type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Survey template name is required.' ) );
		}

		$intConflictingSurveyTemplateCount = CSurveyTemplates::fetchConflictingSurveyTemplatesCountByName( $this->m_intId, $this->sqlName(), $objDatabase );

		if( true == valStr( $this->sqlName() ) && 0 < $intConflictingSurveyTemplateCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Survey template name is already exists.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valSurveyTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	*Create Functions
	*
	*/

	public function createSurveyTemplateQuestion() {

		$objSurveyTemplateQuestion = new CSurveyTemplateQuestion();

		$objSurveyTemplateQuestion->setSurveyTemplateId( $this->m_intId );

		return $objSurveyTemplateQuestion;
	}

	/**
	 * set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['survey_type_id'] ) )	$this->setSurveyTypeId( $arrmixValues['survey_type_id'] );
		if( isset( $arrmixValues['survey_type'] ) )		$this->setSurveyType( $arrmixValues['survey_type'] );
		if( isset( $arrmixValues['question'] ) )		$this->setQuestion( $arrmixValues['question'] );

		return;
	}

	public function setSurveyTypeId( $intSurveyTypeId ) {
		$this->m_intSurveyTypeId = $intSurveyTypeId;
	}

	public function setSurveyType( $strSurveyType ) {
		$this->m_strSurveyType = $strSurveyType;
	}

	public function setQuestion( $intQuestion ) {
		$this->m_intQuestion = $intQuestion;
	}

	public function setIsAllowEdit( $boolIsAllowEdit ) {
		$this->m_boolIsAllowEdit = $boolIsAllowEdit;
	}

	public function setIsAllowDelete( $boolIsAllowDelete ) {
		$this->m_boolIsAllowDelete = $boolIsAllowDelete;
	}

	public function setSurveyOrSurveyTemplateQuestionCount( $intSurveyOrSurveyTemplateQuestionCount ) {
		$this->m_intSurveyOrSurveyTemplateQuestionCount = $intSurveyOrSurveyTemplateQuestionCount;
	}

	/**
	 * get Functions
	 */

	public function getSurveyTypeId() {
		return $this->m_intSurveyTypeId;
	}

	public function getSurveyType() {
		return $this->m_strSurveyType;
	}

	public function getQuestion() {
		return $this->m_intQuestion;
	}

	public function getIsAllowEdit() {
		return $this->m_boolIsAllowEdit;
	}

	public function getIsAllowDelete() {
		return $this->m_boolIsAllowDelete;
	}

	public function getSurveyOrSurveyTemplateQuestionCount() {
		return $this->m_intSurveyOrSurveyTemplateQuestionCount;
	}

	/**
	 * create Functions
	 */

	public function createSurveyTemplateQuestionGroup() {
		$objSurveyTemplateQuestionGroup = new CSurveyTemplateQuestionGroup();
		$objSurveyTemplateQuestionGroup->setSurveyTemplateId( $this->getId() );

		return $objSurveyTemplateQuestionGroup;
	}

	/**
	 * fetch Functions
	 */

	public function fetchSurveyTemplateQuestionGroupById( $intId, $objDatabase ) {
		return CSurveyTemplateQuestionGroups::fetchSurveyTemplateQuestionGroupByIdBySurveyTemplateId( $intId, $this->getId(), $objDatabase );
	}

	public function fetchSurveyQuestionAnswers( $objDatabase ) {
		return CSurveyQuestionAnswers::fetchSurveyQuestionAnswersBySurveyTemplateId( $this->getId(), $objDatabase );
	}

	public function fetchSurveys( $objDatabase ) {
		return CSurveys::fetchSurveysBySurveyTemplateId( $this->getId(), $objDatabase );
	}

	public function fetchSurveyTemplateQuestionGroups( $objDatabase ) {
		return CSurveyTemplateQuestionGroups::fetchSurveyTemplateQuestionGroupsBySurveyTemplateId( $this->getId(), $objDatabase );
	}

	public function fetchSurveyTemplateQuestions( $objDatabase, $objSurveyTemplateFilter = NULL ) {
		return CSurveyTemplateQuestions::fetchSurveyTemplateQuestionsBySurveyTemplateId( $this->getId(), $objDatabase, $objSurveyTemplateFilter );
	}

	public function fetchSurveyTemplateChildQuestions( $objDatabase, $objSurveyTemplateFilter ) {
		return CSurveyTemplateQuestions::fetchSurveyTemplateQuestionsBySurveyTemplateId( $this->getId(), $objDatabase, $objSurveyTemplateFilter );
	}

	public function fetchSurveyTemplateOptions( $objDatabase ) {
		return CSurveyTemplateOptions::fetchSurveyTemplateOptionsBySurveyTemplateId( $this->getId(), $objDatabase );
	}

	public function fetchSurveyTemplateQuestionsBySurveyTemplateQuestionIds( $arrintSurveyTemplateQuestionIds, $objDatabase ) {
		return CSurveyTemplateQuestions::fetchSurveyTemplateQuestionsByIdsBySurveyTemplateId( $arrintSurveyTemplateQuestionIds, $this->getId(), $objDatabase );
	}

	public function fetchSurveyTemplateQuestionById( $intSurveyTemplateQuestionId, $objDatabase ) {
		return CSurveyTemplateQuestions::fetchSurveyTemplateQuestionByIdBySurveyTemplateId( $intSurveyTemplateQuestionId, $this->getId(), $objDatabase );
	}

	public function fetchSubmittedSurveysCount( $objDatabase ) {
		return getArrayElementByKey( $this->getId(), CSurveyTemplates::fetchSubmittedSurveysCountBySurveyTemplateIds( array( $this->getId() ), $objDatabase ) );
	}

	/**
	 * other Functions
	 */

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );

			// decline all surveys
			$this->setSurveyOrSurveyTemplateQuestionCount( 1 );
			return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		return parent::delete( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

	public function update( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		// if Question count == 1 or Non submitted survey count > 0 then decline surveys and unpublish template
		if( 0 < ( int ) $this->getSurveyOrSurveyTemplateQuestionCount() ) {
			$this->setIsPublished( 0 );
			if( false == CScheduledSurvey::declineSurveyData( $intUserId, $this->getId(), $objDatabase ) ) return false;
		}

		return parent::update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>