<?php

class CRole extends CBaseRole {

	const RECEIVES_NEW_CONTRACTS								= 1;
	const RECEIVES_VANITY_PHONE_NUMBER_REQUESTS					= 2;
	const RECEIVES_CLIENT_VOLUME_CHANGE_SUMMARY_EMAIL			= 3;
	const RECEIVES_PAYMENT_ALERTS								= 4;
	const RECEIVES_SCRIPT_MONITORING_ALERTS						= 5;
	const RECEIVES_MONTHLY_PAYMENT_VOLUME_SUMMARY_EMAIL			= 6;
	const ACCESS_CUSTOM_TEMPLATES								= 7;
	const IS_A_CONTRACT_MIGRATOR								= 8;
	const RECEIVES_NEW_SALE_EMAILS_WITH_3_OR_MORE_PROPERTIES	= 9;
	const RECEIVES_ALL_NEW_SALE_EMAILS							= 10;
	const RECEIVES_PROPERTY_SOLUTIONS_WEBSITE_LEADS				= 11;
	const ACCESS_SCRIPT_TRANSACTIONS							= 12;
	const SPEAKS_SPANISH										= 13;
	const HAS_USER_EDIT_GRANTS									= 14;
	const ASSOCIATE_DIRECTOR									= 15;
	const DIRECTOR												= 16;
	const DEPLOY												= 17;
	const RECEIVES_RENT_REMINDER_EMAIL_ALERTS					= 18;
	const RECEIVES_RECONCILIATION_EMAILS						= 19;
	const EMPLOYEE_APPLICATIONS_MONITORING						= 20;
	const MANAGES_INVOICING										= 21;
	const RECEIVES_ILS_PORTAL_EMAILS							= 22;
	const MANAGES_BASE_DEMO_SYNC								= 23;
	const EMPLOYEE_ASSESSMENT_MONITORING						= 24;
	const HR_VIEW_EMPLOYEE_DOCUMENTS							= 25;
	const VIEW_SALARIES											= 26;
	const DOES_BUG_ROOT_CAUSE_ANALYSIS							= 27;
	const ACCESS_SSN											= 28;
	const ALLOW_EMPLOYEE_MANAGEMENT								= 29;
	const ALLOW_SCRIPT_ADMIN									= 31;
	const DATABASE_READ_ACCESS									= 33;
	const ALLOW_STORY_POINT_EDITING								= 36;
	const MANAGE_MONEYGRAM_LOCATIONS							= 37;
	const TRAINING												= 38;
	const VIEW_CONFIDENTIAL_EMAILS								= 39;
	const VIEW_SUPPORT_TICKETS_ROUTER							= 40;
	const VIEW_SUPPORT_TICKETS_CALLER							= 41;
	const APPROVES_CONTRACT_TERMINATIONS						= 43;
	const ALLOW_SELENIUM_TASK_CREATION							= 45;
	const OUTAGE_STAKEHOLDER									= 47;
	const RECEIVES_NPS_REPORT_EMAIL								= 48;
	const ASSIGN_ROLES_TO_OTHER_USERS							= 49;
	const APPROVE_RECONCILIATION 								= 50;
	const APPROVES_PURCHASE_REQUESTS							= 55;
	const RECEIVES_RECONCILIATION_ALERTS 						= 56;
	const IS_DEPARTMENT_MANAGER 								= 58;
	const IS_COLLECTIONS_MANAGER 								= 59;
	const IS_HR_DIRECTOR 										= 60;
	const EMPLOYEE_QUEUES 										= 61;
	const RECEIVE_DATABASE_DEPLOYMENT_REPORT					= 62;
	const CODE_AUDITOR	 										= 64;
	const ALLOW_PURCHASE_REQUEST_ACCESS							= 65;
	const RECEIVE_PRS											= 66;
	const PERFORMS_SUPPORT_TASK_QA								= 67;
	const ALLOW_ADD_EDIT_CTC									= 69;
	const ALLOW_VIEW_CTC										= 70;
	const RAPID_ACCESS											= 72;
	const STANDARD_ACCESS										= 73;
	const ALLOW_RESOURCE_REQUISITION_ACCESS						= 79;
	const ALLOW_ADD_EDIT_SURVEY_TEMPLATE						= 80;
	const ALLOW_VIEW_RESPONSE_SUMMARY							= 81;
	const ALLOW_MANAGE_RECIPIENT_LIST							= 82;
	const ENTRATA_MIGRATION_MODE								= 83;
	const ENTRATA_RESIDENT_VERIFY_FULL_ACCESS					= 84;
	const LEASING_CENTER_WORKFORCE_MANAGER						= 85;
	const RU_IMPLEMENTATION										= 71;
	const SMALL_PROPERTY_TERMINATION							= 86;
	const ROLE_ENTRATA_RESIDENT_VERIFY_FULL_ACCESS				= 84;
	const ENTRATA_SSN_ACCESS									= 87;
	const ALLOW_DIAGNOSTIC_REPAIR								= 88;
	const EDIT_INTEGRATION_UUTILITY_RELEASED_VERSION			= 89;
	const VIEW_ASSET_PURCHASE_REQUESTS							= 90;
	const MANAGE_DEPARTMENT_METRICS								= 91;
	const LEASING_CENTER_WORKFORCE_INTRADAY_SCHEDULE_MANAGER	= 92;
	const TRAINING_HELP_CONTENT_MANAGER							= 93;
	const RVSECURE_DEPLOYMENT_ACCESS							= 97;
	const IT_DELETE_ASSET_PERMISSION							= 99;
	const NPS_REVIEWER											= 100;
	const RECEIVE_DAILY_NUGGETS_EMAIL							= 101;
	const ENTRATA_USE_WEAK_VALIDATION							= 102;
	const UPDATE_RENEWAL_DATE									= 103;
	const EDIT_DECISION_MAKERS_PROMOTER							= 105;
	const PAYMENT_DEPLOYMENT_ACCESS								= 106;
	const REIMBURSEMENT_APPROVER								= 107;
	const RECEIVE_NEW_HIRE_PR_EMAILS							= 110;
	const RECEIVE_COMP_AND_BONUS_PR_EMAILS						= 111;
	const SEND_EMERGENCY_MESSAGES								= 112;

	const ALLOW_PUBLISH_API										= 114;
	const ALLLOW_EDIT_AUTOMATION_TEST_CLIENT					= 115;
	const COMPANY_CARDHOLDER									= 116;
	const AMEX_ADMIN											= 117;
	const ALLOW_CALL_SHADOWING									= 118;
	const ALLOW_EXECUTE_SCRIPT									= 119;
	const ALLOW_CONTRACT_COMMISSION								= 120;
	const FORCEULLY_COMPLETE_REVIEW								= 121;
	const ALLOW_RECRUITING_MANAGER_ACCESS						= 122;
	const AMEX_ASSISTANT										= 123;
	const ALLOW_COPY_PROPERTY_SETTINGS							= 124;
	const LESS_VAR_CACHE_CLEAR_DEPLOYMENT_ALL					= 125;
	const LESS_VAR_CACHE_CLEAR_DEPLOYMENT_SPECIFIC				= 126;
	const DATA_CACHE_CLEAR_DEPLOYMENT							= 127;
	const ALLOW_TO_VIEW_SUPER_USER_CPA							= 128;
	const PR_RECRUITERS											= 129;
	const ROLE_CREATE_PUBLIC_TAGS								= 131;
	const ALLOW_TO_UPDATE_DELINQUENCY							= 132;
	const ALLOW_USER_TO_SHOW_RECRUITEMENT_DATA					= 134;
	const ANNUAL_REVIEW_ADMIN									= 135;
	const IT_NO_DUES_SURVEY_APPROVAL							= 136;
	const VIRTUAL_DBAS											= 137;
	const ALLOW_TEAMS_ASSOCIATION								= 138;
	const ALLOW_US_HR_EMPLOYEE_PAYROLL_ACCESS					= 146;

	const WORKFRONT_STAKEHOLDERS								= 144;

	const ALLOW_PROPERTY_SETTINGS_TEMPLATE_ACCESS				= 140;
	const FULL_SALES_DATA_ACCESS								= 139;
	const CRITICAL_CODE_DEPLOYMENT_APPROVAL						= 141;
	const ALLOW_PHP_CERTIFICATION_TESTS							= 142;
	const SDM_DASHBOARD_EDIT_PERMISSION							= 143;
	const CA_EXECUTE_SQL_SWITCH_MASTER							= 145;
	const REIMBURSEMENT_HR_HEAD									= 147;

	const ALLOW_SUPER_USERS_LEAVE_ACCESS_EMPLOYEES				= 149;
	const COMPANY_DOMAIN_FULL_ACCESS							= 148;
	const HR_EMPLOYEE_POLICY_MANAGER							= 150;

	const ALLOW_EDIT_HONOUR_BADGE_RATIO							= 151;

	const ALLOW_QA_CERTIFICATION_TESTS							= 153;
	const GO_LIVE_WITH_ENTRATA									= 154;
	const ALLOW_DBA_CERTIFICATION_TESTS							= 155;

	const FULL_COMMISSIONS_DATA_ACCESS							= 152;
	const SALES_MANAGER											= 156;

	const HA_DEPLOYMENT_ACCESS									= 157;

	const REVIEW_VENDOR_ENTITY									= 158;
	const ALLOW_PIP_REASONS										= 159;

	const ALLOW_CLIENT_DATA_INSTANCE_APPROVAL					= 160;

	const RVSECURE_MANUAL_SQL_DEPLOYMENT_ACCESS					= 161;
	const CDN_CACHE_CLEAR_DEPLOYMENT							= 162;
	const ALLOW_USER_HONOUR_BADGE_SEMESTER_RESET				= 163;
	const ALLOW_CREATE_BULK_COMP_CHANGE_PRS						= 164;
	const US_REIMBURSEMENT_HR_ADMIN								= 165;
	const PAYROLL_AND_BENEFIT_ACCESS							= 166;

	const ALLOW_OFFER_RELEASED_ACCEPTED							= 167;
	const ALLOW_QA_TO_ADD_OR_EDIT_CLIENT_STATUS					= 168;
	const RECEIVE_RISK_STATUS_UPDATE							= 169;
	const STAGE_PRODUCTION_SQL_COMMIT_APPROVAL					= 170;

	const REPORT_ADMIN											= 171;
	const REPORT_HEAD											= 172;

	const STANDARD_DEPLOYMENT_APPROVE_WITHOUT_QA				= 173;
	const TRAINING_SESSION_STAKEHOLDER							= 174;
	const TEST_MODULE_ACCESS									= 175;

	const PSSECURE_DEPLOYMENT_ACCESS							= 176;
	const ALLOW_DATABASE_DEPLOYMENT_EXECUTION					= 177;
	const RELEASE_REPORT_GENERATION								= 178;

	const DOE_ROLE_ACCESS_FOR_SYSTEM_UP_TIME					= 179;
	const HAS_BACKLOG_CREATION_GRANT							= 180;

	const ALLOW_TO_LOG_OUT_CHAT_AGENTS							= 181;

	const ALLOW_ONLY_VIEW_ASSET_PURCHASE_REQUESTS				= 182;

	const DELAYED_OR_SUSPENDED_BILLING_APPROVER					= 183;
	const DELAYED_OR_SUSPENDED_BILLING_PROCESSOR				= 184;
	const ALLOW_TO_ADD_TEMPLATE_COMPANY							= 185;

	const ALLOW_MANUAL_CONTACT_QUEUE_ACCESS						= 186;
	const ALLOW_IMPLEMENTATION_PORTAL_ACCESS					= 187;

	const ALLOW_PRODUCT_WISE_DEPLOYMENT							= 188;
	const GLOBAL_DEPLOY											= 189;

	const ALLOW_CHECK_SCANNER_ORDER_FORM						= 190;
	const ALLOW_FLOOR_PLAN_ORDER_FORM							= 191;
	const ALLOW_SITE_PLAN_ORDER_FORM							= 192;
	const ALLOW_CUSTOM_LOGO_ORDER_FORM							= 193;
	const ALLOW_TEMPLATE_LOGO_ORDER_FORM						= 194;
	const ALLOW_DOMAIN_ORDER_FORM								= 195;
	const ALLOW_DISTRIBUTION_TYPE_CHANGE_REQUEST_ORDER_FORM		= 197;
	const ALLOW_CHECK_SCANNING_MAX_PAYMENT_ORDER_FORM			= 198;
	const SHOW_NEW_HIRE_PR_AMOUNT								= 199;
	const ALLOW_CALL_CONVERSATION_DOWNLOAD						= 200;

	const TEST_CASE_TESTER										= 202;
	const TEST_CASE_LEADER										= 201;
	const SCHEMA_CHANGE_ACCESS									= 203;
	const ALLOW_INVESTMENT_DOCUMENT_ACCESS						= 204;
	const ENTRATA_BANK_ACCOUNT_SIGNER							= 205;
	const COMMISSIONS_ADMIN										= 206;
	const DATA_ENTRY_DASHBOARD        							= 207;
	const ENTRATA_TEST_CASE_ADMIN        						= 208;
	const VIEW_EMPLOYEE_ENCRYPTION_PERMISSIONS                  = 209;
	const ALLOW_SIMPORT_DEACTIVATION							= 210;
	const ALLOW_EDIT_CONTRACT_DRAFT								= 211;

	const DEAL_DESK_MEETING_ORGANIZER							= 212;
	const DEAL_DESK_MEETING_ATTENDEE							= 213;
	const COMPANY_VALUES_RECOGNITION                            = 214;

	public static $c_arrintRolesForDbWatches = [ CRole::RECEIVES_SCRIPT_MONITORING_ALERTS, CRole::RECEIVES_MONTHLY_PAYMENT_VOLUME_SUMMARY_EMAIL ];

	public static $c_arrintRolesForCodeDeployments = [ self::RAPID_ACCESS, self::STANDARD_ACCESS, self::PAYMENT_DEPLOYMENT_ACCESS, self::RVSECURE_DEPLOYMENT_ACCESS, self::HA_DEPLOYMENT_ACCESS, self::PSSECURE_DEPLOYMENT_ACCESS, self::DEPLOY, self::ALLOW_PRODUCT_WISE_DEPLOYMENT ];

	public static $c_arrintOrderFormTypeIdsByUserRoles = [
		self::ALLOW_CHECK_SCANNER_ORDER_FORM                    => COrderFormType::CHECK_SCANNER,
		self::ALLOW_FLOOR_PLAN_ORDER_FORM                       => COrderFormType::FLOOR_PLAN_PACKAGE,
		self::ALLOW_SITE_PLAN_ORDER_FORM                        => COrderFormType::SITE_PLAN,
		self::ALLOW_CUSTOM_LOGO_ORDER_FORM                      => COrderFormType::CUSTOM_LOGO,
		self::ALLOW_TEMPLATE_LOGO_ORDER_FORM                    => COrderFormType::TEMPLATE_LOGO,
		self::ALLOW_DOMAIN_ORDER_FORM                           => COrderFormType::DOMAIN_ORDER_TRANSFER,
		self::ALLOW_DISTRIBUTION_TYPE_CHANGE_REQUEST_ORDER_FORM => COrderFormType::DISTRIBUTION_TYPE_CHANGE_REQUEST,
		self::ALLOW_CHECK_SCANNING_MAX_PAYMENT_ORDER_FORM       => COrderFormType::CHECK_SCANNING_MAX_PAYMENT_INCREASE_REQUEST
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required. ' ) );
		}
		return $boolValid;
	}

	public function valAllowSuperUserOnly() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAllowAdminUsers() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOwner() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		return parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false );

	}
}
?>
