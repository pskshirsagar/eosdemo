<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePolicyAssociations
 * Do not add any new functions to this class.
 */

class CEmployeePolicyAssociations extends CBaseEmployeePolicyAssociations {

	public static function fetchEmployeePolicyAssociationByPolicyId( $intPolicyId, $objDatabase ) {
		return self::fetchEmployeePolicyAssociation( sprintf( 'SELECT * FROM employee_policy_associations WHERE policy_id = %d', ( int ) $intPolicyId ), $objDatabase );
	}
}
?>