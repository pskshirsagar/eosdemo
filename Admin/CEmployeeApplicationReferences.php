<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationReferences
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationReferences extends CBaseEmployeeApplicationReferences {

	public static function fetchEmployeeApplicationReferencesByEmployeeApplicationIds( $arrintEmployeeApplicationIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeApplicationIds ) ) return NULL;

		$strSql = 'SELECT * FROM employee_application_references WHERE employee_application_id IN ( ' . implode( ',', $arrintEmployeeApplicationIds ) . ')';

		return self::fetchEmployeeApplicationReferences( $strSql, $objDatabase );
	}
}
?>