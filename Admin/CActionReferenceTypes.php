<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CActionReferenceTypes
 * Do not add any new functions to this class.
 */

class CActionReferenceTypes extends CBaseActionReferenceTypes {

	public static function fetchActionReferenceTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CActionReferenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchActionReferenceType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CActionReferenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedActionReferenceTypes( $objDatabase ) {
		$strSql = 'SELECT * FROM action_reference_types WHERE is_published = 1';

		return parent::fetchCachedObjects( $strSql, 'CActionReferenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}
}
?>