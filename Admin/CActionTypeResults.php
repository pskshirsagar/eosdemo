<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CActionTypeResults
 * Do not add any new functions to this class.
 */

class CActionTypeResults extends CBaseActionTypeResults {

	public static function fetchActionTypeResults( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CActionTypeResult', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchActionTypeResult( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CActionTypeResult', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllActionTypeResults( $objDatabase, $intExcludeActionResultId = NULL ) {

		$strWhereClause = '';

		if( false == is_null( $intExcludeActionResultId ) ) $strWhereClause = ' WHERE atr.action_result_id <> ' . ( int ) $intExcludeActionResultId;
			$strSql = ' SELECT
						atr.action_type_id,
						aty.name,
						atr.action_result_id,
						ar.name AS action_result_name
					FROM
						action_type_results atr
						JOIN action_types aty ON ( atr.action_type_id = aty.id )
						JOIN action_results ar ON ( atr.action_result_id = ar.id )
					' . $strWhereClause . ' Order BY action_type_id ASC, action_result_name ASC ';

		$arrstrTempActionTypeResults = fetchData( $strSql, $objDatabase );

		$arrstrActionTypeResults = array();
		if( true == valArr( $arrstrTempActionTypeResults ) ) {
			foreach( $arrstrTempActionTypeResults as $arrstrActionTypeResult ) {
				$arrstrActionTypeResults[$arrstrActionTypeResult['action_type_id']][$arrstrActionTypeResult['action_result_id']] = array( 'action_result_id' => $arrstrActionTypeResult['action_result_id'], 'action_result_name' => $arrstrActionTypeResult['action_result_name'] );
			}
		}

		return $arrstrActionTypeResults;
	}

	public static function fetchRenewalActionTypeResults( $objDatabase ) {

		$strSql = ' SELECT
					   	atr.action_result_id,
					   	ar.name AS action_result_name
					FROM
					   	action_type_results atr
					   	JOIN action_types aty ON ( atr.action_type_id = aty.id )
					   	JOIN action_results ar ON ( atr.action_result_id = ar.id )
					WHERE
					   	atr.action_type_id = ' . CActionType::RENEWAL_WAIVE_STATUS . '
					ORDER BY
					   	action_type_id ASC, action_result_name ASC ';

		return fetchData( $strSql, $objDatabase );
	}

}
?>