<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStakeholders
 * Do not add any new functions to this class.
 */

class CStakeholders extends CBaseStakeholders {

	public static function fetchstakeholdersByStakeholderReferenceIdByStakeholderTypeId( $intStakeholderReferenceId, $intStakeholderTypeId, $objDatabase, $boolShowCurrentEmployee = false, $boolShowExtraEmployeeDetails = false ) {

		if( true == is_null( $intStakeholderTypeId ) || true == is_null( $intStakeholderReferenceId ) ) return NULL;

		$strSubSql = ( true == $boolShowCurrentEmployee ) ? ' AND emp.employee_status_type_id = ' . CEmployeeStatusType::CURRENT : '';

		$strSelectSql = '';
		$strJoinSql = '';
		if( true == $boolShowExtraEmployeeDetails ) {
			$strJoinSql = ' JOIN departments d ON ( emp.department_id = d.id )';
			$strSelectSql = ',d.name AS employee_department_name, emp.id as employee_id';
		}

		$strSql = '
				SELECT
					 stk.*,
					 emp.preferred_name as employee_name_full,
					 emp.email_address as employee_email_address
					' . $strSelectSql . '
				FROM
					stakeholders stk
					JOIN employees emp ON ( stk.employee_id = emp.id )
					' . $strJoinSql . '
				WHERE
					stakeholder_reference_id = ' . ( int ) $intStakeholderReferenceId . '
					AND stakeholder_type_id  = ' . ( int ) $intStakeholderTypeId . $strSubSql;

		return self::fetchStakeholders( $strSql, $objDatabase );
	}

	public static function fetchNotifcationstakeholdersByStakeholderTypeId( $intStakeholderTypeId, $objDatabase ) {

		$strSql = ' SELECT
						st.employee_id,
						e.email_address
					FROM
						stakeholders st
						JOIN employees e ON( e.id = st.employee_id )
					WHERE
						st.stakeholder_type_id = ' . ( int ) $intStakeholderTypeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchStakeholdersByStakeholderReferenceIdByEmployeeIdsByStakeholderType( $intStakeholderReferenceId, $arrintEmployeeIds, $intStakeholderType, $objDatabase ) {

		if( false == is_numeric( $intStakeholderReferenceId ) || false == valArr( $arrintEmployeeIds ) || false == valId( $intStakeholderType ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						*
					FROM
						Stakeholders
					WHERE
						stakeholder_reference_id = ' . ( int ) $intStakeholderReferenceId . '
						AND stakeholder_type_id = ' . ( int ) $intStakeholderType . '
						AND employee_id IN( ' . implode( ',', $arrintEmployeeIds ) . ')';

		return self::fetchStakeholders( $strSql, $objDatabase );
	}

	public static function fetchSprintStakeholdersByStakeholderReferenceIdByStakeholderTypeId( $intStakeholderReferenceId, $intStakeholderTypeId, $objDatabase ) {

		if( true == is_null( $intStakeholderTypeId ) || true == is_null( $intStakeholderReferenceId ) ) return NULL;

		$strSql = 'SELECT
						s.stakeholder_reference_id as planner_id,
						s.employee_id,
						e.preferred_name,
						ea.country_code,
						d.name as department_name
					FROM
						stakeholders AS s
						JOIN employees AS e ON ( s.employee_id = e.id AND s.stakeholder_type_id= ' . ( int ) $intStakeholderTypeId . ' )
						JOIN employee_addresses AS ea ON ( ea.employee_id = e.id )
						JOIN departments AS d ON ( e.department_id = d.id )
						
					WHERE
						stakeholder_reference_id = ' . ( int ) $intStakeholderReferenceId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function insertSelectedStakeholders( $arrmixSelectedStakeholders, $intEmployeeVacationRequestId, $objDatabase, $intUserId ) {
		if( false == valArr( $arrmixSelectedStakeholders ) ) {
			return false;
		}

		$arrobjStakeholders		= [];
		$objTempStakeholders 	= new CStakeholder();

		$objTempStakeholders->setStakeholderTypeId( CStakeholderType::EMPLOYEE_VACATION_REQUEST );
		$objTempStakeholders->setStakeholderReferenceId( $intEmployeeVacationRequestId );

		foreach( $arrmixSelectedStakeholders as $intKeyEmployeeId => $arrmixSelectedStakholder ) {
			$objStakeholder = clone $objTempStakeholders;
			$objStakeholder->setEmployeeId( $intKeyEmployeeId );
			$arrobjStakeholders[] = $objStakeholder;
		}

		if( true == valArr( $arrobjStakeholders ) && false == self::bulkInsert( $arrobjStakeholders, $intUserId, $objDatabase ) ) {
			return false;
		}

		return true;
	}

	public static function deleteStakeholders( $arrintDeleteStakeholders, $arrobjSelectedStakeholders, $objDatabase ) {
		if( true == valArr( $arrintDeleteStakeholders ) && true == valArr( $arrobjSelectedStakeholders ) ) {

			foreach( $arrobjSelectedStakeholders as $intEmployeeId => $objStakeholder ) {
				if( false == in_array( $intEmployeeId, $arrintDeleteStakeholders ) ) {
					unset( $arrobjSelectedStakeholders[$intEmployeeId] );
				}
			}

			if( true == valArr( $arrobjSelectedStakeholders ) && false == self::bulkDelete( $arrobjSelectedStakeholders, $objDatabase ) ) {
				return false;
			}
		}

		return true;
	}

}
?>