<?php

class CReimbursementExpensePsDocuments extends CBaseReimbursementExpensePsDocuments {

	public static function fetchReimbursementExpensePsDocuments( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CReimbursementExpensePsDocument', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchReimbursementExpensePsDocument( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CReimbursementExpensePsDocument', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>