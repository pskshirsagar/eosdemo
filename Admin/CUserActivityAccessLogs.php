<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserActivityAccessLogs
 * Do not add any new functions to this class.
 */

class CUserActivityAccessLogs extends CBaseUserActivityAccessLogs {

	public static function fetchUserActivityAccessLogs( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CUserActivityAccessLog', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchUserActivityAccessLog( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CUserActivityAccessLog', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>