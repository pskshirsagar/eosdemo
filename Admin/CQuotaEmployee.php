<?php

class CQuotaEmployee extends CBaseQuotaEmployee {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>