<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CInductionTypes
 * Do not add any new functions to this class.
 */

class CInductionTypes extends CBaseInductionTypes {

	public static function fetchInductionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CInductionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchInductionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CInductionType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>