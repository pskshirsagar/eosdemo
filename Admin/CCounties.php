<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCounties
 * Do not add any new functions to this class.
 */

class CCounties extends CBaseCounties {

	public static function fetchCounties( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCounty', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchCounty( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCounty', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchCountiesByStateCode( $strStateCode, $objDatabase ) {
		return self::fetchCounties( sprintf( 'SELECT * FROM counties WHERE state_code = \'%s\'', $strStateCode ), $objDatabase );
	}

	public static function fetchCountiesByCountyCodesByStateCode( $arrintCountyCodes, $strStateCode, $objDatabase ) {
		if( false == valArr( $arrintCountyCodes ) || false == valStr( $strStateCode ) ) {
			return NULL;
		}

		$strSql = 'SELECT state_code, county_code, county_name FROM counties WHERE county_code IN ( \'' . implode( '\', \'', $arrintCountyCodes ) . '\' ) AND state_code = \'' . $strStateCode . '\'';

		return self::fetchCounties( $strSql, $objDatabase );
	}

}
?>