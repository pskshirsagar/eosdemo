<?php

class CEntrataDataServicesLog extends CBaseEntrataDataServicesLog {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCids() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEventName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valQuantity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCost() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>