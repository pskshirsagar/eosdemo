<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeHandbooks
 * Do not add any new functions to this class.
 */

class CEmployeeHandbooks extends CBaseEmployeeHandbooks {

	public static function fetchEmployeeHandbookCurrentVersionByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						employee_handbooks
					WHERE
						employee_id = ' . $intEmployeeId . '
					ORDER BY
						created_on DESC
					LIMIT 1 ';

		return self::fetchEmployeeHandbook( $strSql, $objDatabase );
	}

	public static function fetchEmployeeHandbookByEmployeeIdByHandbookId( $intEmployeeId, $intHandbookId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) || false == is_numeric( $intHandbookId ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						employee_handbooks
					WHERE
						employee_id = ' . $intEmployeeId . '
					AND
						handbook_id	= ' . $intHandbookId;

		return self::fetchEmployeeHandbook( $strSql, $objDatabase );
	}

	public static function fetchSignedEmployeeHandbooksByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						employee_handbooks eh
					JOIN
						handbooks h ON ( h.id = eh.handbook_id )
					JOIN
						ps_documents psd ON ( psd.id = h.ps_document_id )
					WHERE
						eh.employee_id = ' . ( int ) $intEmployeeId . '
					AND
						isfinite(eh.signed_on)';
		return self::fetchEmployeeHandbooks( $strSql, $objDatabase );
	}

	public static function fetchEmployeeHandbooksByFilter( $objAdminDatabase, $arrmixFilter = NULL ) {

		if( false == $arrmixFilter['year'] ) return NULL;

		$intSigned 		= 1;
		$intUnsigned	= 2;

		$strPaginationFilter	= '';
		$strFilterCondition 	= '';

		if( false == is_null( $arrmixFilter['page_size'] ) ) {
			$intOffset = ( 0 < $arrmixFilter['page_no'] ) ? $arrmixFilter['page_size'] * ( $arrmixFilter['page_no'] - 1 ) : 0;
			$intLimit = ( int ) $arrmixFilter['page_size'];

			$strPaginationFilter = ' OFFSET ' . $intOffset . '	LIMIT ' . $intLimit;
		}

		if( false == is_null( $arrmixFilter['employee_id'] ) ) {

			$strFilterCondition = ' AND ep.id = ' . $arrmixFilter['employee_id'];

		} elseif( false == is_null( $arrmixFilter['employee_name'] ) ) {

			$strFilterCondition = ' AND ep.name_full ILIKE \'%' . $arrmixFilter['employee_name'] . '%\' ';

		}

		if( false == is_null( $arrmixFilter['signature_status'] ) ) {

			if( $intSigned == $arrmixFilter['signature_status'] ) {

				$strFilterCondition .= ' AND isfinite(eh.signed_on)';

			} elseif( $intUnsigned == $arrmixFilter['signature_status'] ) {

				$strFilterCondition .= ' AND eh.signed_on IS NULL';
			}

		}

		if( false == is_null( $arrmixFilter['sorting_order'] ) ) {

			$strFilterCondition .= ' ORDER BY ' . $arrmixFilter['sort_by'] . ' ' . $arrmixFilter['sorting_order'];
		}

		$strSql = ' SELECT
						ep.id as employee_id, ep.name_full as employee_name, hb.version_number as version_number, eh.signed_on as signed_on, dp.name as department
					FROM
						employee_handbooks eh
					JOIN
						handbooks hb ON ( eh.handbook_id = hb.id )
					JOIN
						employees ep ON ( eh.employee_id = ep.id AND ep.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
					JOIN
						departments dp ON ( ep.department_id = dp.id )
					WHERE
						extract( year from eh.created_on ) = ' . $arrmixFilter['year'] . $strFilterCondition . $strPaginationFilter;

		return fetchData( $strSql, $objAdminDatabase );

	}

	public static function fetchSignedOnOfCurrentVersionByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) return NULL;

		$strSql = ' SELECT
						id,
						signed_on
					FROM
						employee_handbooks
					WHERE
						employee_id = ' . $intEmployeeId . '
					ORDER BY
						created_on DESC
					LIMIT 1 ';

		$arrstrSignedOn = fetchData( $strSql, $objDatabase );

		return $arrstrSignedOn[0];
	}

}
?>