<?php

class CPsWebsiteLegalItemAttachment extends CBasePsWebsiteLegalItemAttachment {

	use Psi\Libraries\EosFoundation\TEosStoredObject;

	protected $m_intCid;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsWebsiteLegalItemId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getFileUrl() {
		return CONFIG_ENTRATA_WEBSITE_URL . '/?module=entrata_website_legal&action=view_ps_website_legal_item_attachment&id=' . $this->getId();
	}

	public function getAbsoluteFilePath() {
		return PATH_MOUNTS . 'Global/' . $this->getFilePath();
	}

	public function getTitle() {
		return $this->getFileName();
	}

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function uploadObject( $objObjectStorageGateway, $strUploadPath, $intCurrentUserId, $objAdminDatabase ) {

		if( !valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			trigger_error( __( 'Failed to get IObjectStorage' ), E_USER_WARNING );
			return false;
		}

		$arrmixGatewayRequest = $this->createPutGatewayRequest( [ 'data' => CFileIo::fileGetContents( $strUploadPath ) ] );

		if( valStr( $this->calcStorageContainer() ) ) {
			$arrmixGatewayRequest['container'] = $this->calcStorageContainer();
		}
		$objObjectStorageGatewayResponse = $objObjectStorageGateway->putObject( $arrmixGatewayRequest );

		if( $objObjectStorageGatewayResponse->hasErrors() ) {

			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to put storage object.' ) ) );
			return false;
		}
		$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse );

		if( !$this->insertorUpdateStoredObject( $intCurrentUserId, $objAdminDatabase ) ) {
			return false;
		}

		return true;
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {

		switch( $strVendor ) {

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				$strPath = '../Global/' . $this->getFilePath();
				break;

			default:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), 'legal_items_attachments/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getId() );
		}

		return $strPath . $this->getFileName();
	}

	protected function calcStorageContainer( $strVendor = NULL ) {

		switch( $strVendor ) {

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return PATH_MOUNTS_DOCUMENTS;

			default:
				return CONFIG_OSG_BUCKET_DOCUMENTS;

		}

	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

	public function getDocumentFromCloud( $objObjectStorageGateway ) {

		$arrmixStorageArgs = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest( [ 'outputFile' => 'temp' ] );
		return $objObjectStorageGateway->getObject( $arrmixStorageArgs );

	}

	public function downloadObject( $objObjectStorageGateway, $strFileName, $strDispositionType, $boolIsFullPath = false ) {

		$boolIsMount = false;
		$arrmixStorageArgs = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest( [ 'checkExists' => true ] );
		$arrobjObjectStorageResponse = $objObjectStorageGateway->getObject( $arrmixStorageArgs );

		if( !$arrobjObjectStorageResponse['isExists'] ) {
			$arrobjObjectStorageResponse = $this->getDocumentFromCloud( $objObjectStorageGateway );
			if( $arrobjObjectStorageResponse->hasErrors() ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not present or has been deleted.' ) ) );
				return false;
			}
		} else {
			unset( $arrmixStorageArgs['checkExists'] );
			$arrmixStorageArgs['outputFile'] = 'temp';
			$arrobjObjectStorageResponse = $objObjectStorageGateway->getObject( $arrmixStorageArgs );
		}

		$strFullPath = $arrobjObjectStorageResponse['outputFile'];

		if( $boolIsFullPath ) {
			return $strFullPath;
		}

		header( 'Pragma:public' );
		header( 'Expires:0' );
		header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control:public' );
		header( 'Content-Description:File Transfer' );
		header( 'Content-type: application/pdf', true );
		header( 'Content-Disposition: ' . $strDispositionType . '; filename="' . $strFileName . '"' );
		echo CFileIo::fileGetContents( $strFullPath );

		if( !$boolIsMount ) {
			CFileIo::deleteFile( $strFullPath );
		}
		exit();
	}

	public function deleteObject( $objObjectStorageGateway, $intUserId ) {

		$arrmixGatewayRequest         = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest();
		$objDeleteObjectResponse      = $objObjectStorageGateway->deleteObject( $arrmixGatewayRequest );

		if( valObj( $objDeleteObjectResponse, 'CRouterObjectStorageGateway' ) && $objDeleteObjectResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to remove the file.' ) );
			return false;
		}

		if( !$this->deleteStoredObject( $intUserId, $this->m_objDatabase ) ) {
			return false;
		}

		return true;
	}

}
?>
