<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPurchaseRequests
 * Do not add any new functions to this class.
 */

class CPurchaseRequests extends CBasePurchaseRequests {

	public static function fetchNonEncryptedPurchaseRequests( $intCurrentUserEmployeeId, $intCurrentUserId, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT pr.*
					FROM
						purchase_requests pr
						JOIN purchase_approvals pa ON pr.id = pa.purchase_request_id
					WHERE
						pr.is_reencrypted = 0
						AND pr.purchase_request_type_id != ' . CPurchaseRequestType::ASSET_PURCHASE . '
						AND ( pr.approver_employee_id = ' . ( int ) $intCurrentUserEmployeeId . ' OR pa.employee_id = ' . ( int ) $intCurrentUserEmployeeId . ' OR pr.requested_by = ' . ( int ) $intCurrentUserId . ' )
					ORDER BY pr.updated_on';

		return parent::fetchPurchaseRequests( $strSql, $objDatabase );
	}

	public static function fetchSimplePurchaseRequestCountByIdByEmployeeIdByUserId( $arrintPurchaseRequestIds, $intEmployeeId, $intUserId, $objDatabase ) {

		if( false == valArr( $arrintPurchaseRequestIds ) ) {
			return NULL;
		} else {
			$intCountForBulkApproveRequests = \Psi\Libraries\UtilFunctions\count( $arrintPurchaseRequestIds );
		}

		$strSql = ' SELECT
						count( DISTINCT( pr.id ) ) as count
					FROM
						purchase_requests pr
						LEFT JOIN purchase_approvals pa ON ( pa.purchase_request_id = pr.id )
					WHERE
						pr.id IN( ' . implode( ',', $arrintPurchaseRequestIds ) . ' )
						AND ( pa.employee_id = ' . ( int ) $intEmployeeId . ' OR pr.created_by = ' . ( int ) $intUserId . ') ';

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) && ( $arrintResponse[0]['count'] == $intCountForBulkApproveRequests ) ) {
			return true;
		}

		return false;
	}

	public static function fetchSimplePuchaseRequestDetailsById( $intPurchaseRequestId, $objDatabase ) {

		$arrintResponse = [];

		$strSql = 'SELECT
						(e.preferred_name) as submitted_by,
						(employee_manager.preferred_name) as reporting_manager,
						rr.requirement_due_date,
						pr.created_on as submitted_on,
						prt.name as purchase_request_type,
						pr.reason as reason,
						(e1.preferred_name) as reopened_by,
						rt.type as requirement_type,
						pr.is_internal_hire
					FROM
						purchase_requests pr
						JOIN users u ON u.id = pr.requested_by
						JOIN employees e ON e.id = u.employee_id
						LEFT JOIN employees e1 ON e1.id = pr.reopened_by
						LEFT JOIN resource_requisitions rr ON ( rr.purchase_request_id = pr.id )
						LEFT JOIN employees employee_manager ON ( employee_manager.id = rr.manager_employee_id )
						LEFT JOIN requirement_types rt ON ( rr.requirement_type_id = rt.id )
						JOIN purchase_request_types prt ON prt.id = pr.purchase_request_type_id
					WHERE
						pr.id = ' . ( int ) $intPurchaseRequestId;

		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrintResponse ) ) {
			return $arrintResponse[0];
		}
		return $arrintResponse;
	}

	public static function fetchSimpleApprovedAssetPurchaseRequests( $strCountryCode, $intPsAssetTypeId, $objDatabase ) {

		$strSql = ' SELECT
						pr.id,
						pr.purchase_request_type_id,
						pr.effective_date,
						pr.ps_asset_type_id,
						pat.name as ps_asset_type_name,
						pat.ps_asset_type_id as ps_asset_type
					FROM
						purchase_requests pr
						JOIN ps_asset_types pat ON ( pr.ps_asset_type_id = pat.id )';

		$strWhereCondition = ' WHERE pr.approved_on IS NOT NULL
					AND pr.denied_on IS NULL
					AND pr.purchase_request_type_id = ' . CPurchaseRequestType::ASSET_PURCHASE;

		$strOrderBy = ' ORDER BY pr.approved_on';

		if( false == is_null( $strCountryCode ) ) {
			$strSql .= ' LEFT JOIN users u ON ( u.id = pr.requested_by)
						 LEFT JOIN employee_addresses ea ON ( ea.employee_id = u.employee_id AND ea.address_type_id = ' . CEmailAddressType::PRIMARY . ' ) ';

			$strWhereCondition .= ' AND ea.country_code = \'' . $strCountryCode . '\'';

		}

		if( false == is_null( $intPsAssetTypeId ) ) {
			$strWhereCondition .= ' AND pr.ps_asset_type_id = ' . ( int ) $intPsAssetTypeId;
		}

		$strSql .= $strWhereCondition . ' ' . $strOrderBy;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleNonApprovedPurchaseRequestsByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) return NULL;

		$strSql = ' SELECT
						pr.id
					FROM
						purchase_requests pr
					WHERE
						pr.approver_employee_id =' . ( int ) $intEmployeeId . '
						AND approved_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestsByPurchaseRequestIds( $arrintPurchaseRequestIds, $objDatabase ) {

		if( false == valArr( $arrintPurchaseRequestIds ) ) return NULL;

		$strSql = ' SELECT
						pr.id,
						pr.purchase_request_id
					FROM
						purchase_requests pr
					WHERE
						pr.purchase_request_id IN ( ' . implode( ',', $arrintPurchaseRequestIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApprovedPurchaseRequestCompChangeAndAnnualBonusData( $arrstrPurchasweReportFilter, $strUserCountryCode, $objDatabase, $boolTotalRecords = false ) {

		$strOrderByField = 'approved_on';

		if( true == valArr( $arrstrPurchasweReportFilter ) ) {
			if( true == isset( $arrstrPurchasweReportFilter['order_by_field'] ) ) {

				switch( $arrstrPurchasweReportFilter['order_by_field'] ) {
					case 'employee_name':
						$strOrderByField = 'employee_name';
						break;

					case 'purchase_request_type_name':
						$strOrderByField = 'purchase_request_type_name';
						break;

					case 'designation_name_old':
						$strOrderByField = 'designation_name_old';
						break;

					case 'designation_name_new':
						$strOrderByField = 'designation_name_new';
						break;

					case 'effective_date':
						$strOrderByField = 'effective_date';
						break;

					case 'requested_by':
						$strOrderByField = 'requested_by_employee_name';
						break;

					case 'approve_on':
						$strOrderByField = 'approve_on';
						break;

					default:
						$strOrderByField = 'approved_on';
						break;
				}
			}
		}

		$strOrderByType 	= ( true == isset( $arrstrPurchasweReportFilter['order_by_type'] ) && 'ASC' == $arrstrPurchasweReportFilter['order_by_type'] ) ? ' ASC ' : ' DESC ';
		$intOffset 			= ( true == isset( $arrstrPurchasweReportFilter['page_no'] ) && true == isset( $arrstrPurchasweReportFilter['page_size'] ) ) ? $arrstrPurchasweReportFilter['page_size'] * ( $arrstrPurchasweReportFilter['page_no'] - 1 ) : 0;
		$intLimit 			= ( true == isset( $arrstrPurchasweReportFilter['page_size'] ) ) ? $arrstrPurchasweReportFilter['page_size'] : '';

		$strSql = ' SELECT
						DISTINCT( pr.id ),
						pr.purchase_request_type_id,
						prt.name AS purchase_request_type_name,
						pr.annual_bonus_potential_encrypted,
						pr.bonus_type_id,
						pr.approved_amount_dollars_encrypted,
						pr.base_amount_dollars_encrypted,
						pr.hourly_rate_old_encrypted,
						pr.hourly_rate_new_encrypted,
						pr.estimated_hours_old,
						pr.estimated_hours_new,
						pr.current_salary_type_id,
						pr.proposed_salary_type_id,
						pr.designation_id_new,
						pr.designation_id_old,
						pr.dollar_to_rupee_exchange_rate,
						pr.effective_date,
						e.preferred_name AS employee_name,
						pr.employee_id,
						pr.approved_on,
						e1.preferred_name AS requested_by_employee_name,
						pr.is_reencrypted,
						e.payroll_remote_primary_key AS payroll_number,
						( SELECT name FROM designations WHERE id= pr.designation_id_old ) AS designation_name_old,
						( SELECT name FROM designations WHERE id= pr.designation_id_new ) AS designation_name_new
					FROM
						purchase_requests pr
						JOIN purchase_approvals pa ON ( pa.purchase_request_id = pr.id )
						JOIN purchase_request_types prt ON( prt.id = pr.purchase_request_type_id )
						JOIN employees e ON( e.id = pr.employee_id)
						JOIN users u ON( u.id = pr.requested_by )
						JOIN employees e1 ON ( e1.id = u.employee_id )
						JOIN employee_addresses ea ON( ea.employee_id = pr.employee_id AND ea.country_code = \'' . $strUserCountryCode . '\' )
					WHERE
						pr.purchase_request_type_id IN ( ' . CPurchaseRequestType::COMP_CHANGE . ' , ' . CPurchaseRequestType::EMPLOYEE_BONUS . ' )
						AND pr.approved_on IS NOT NULL
						AND pr.denied_on IS NULL
					ORDER BY ' . $strOrderByField . $strOrderByType;

		if( false == $boolTotalRecords ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset;
			if( true == isset( $intLimit ) && 0 < $intLimit ) {
				$strSql .= ' LIMIT	' . ( int ) $intLimit;
			}
		}

		return parent::fetchPurchaseRequests( $strSql, $objDatabase );
	}

	public static function fetchNewHireRequestStatusForResourceAllocation( $intPurchaseRequestId, $objDatabase ) {

			$strSql = '
					SELECT
						max ( is_allocated ) as is_allocated
					FROM
						( SELECT
						( CASE
							WHEN ( max ( rr1.number_of_resources ) ) =
						(
						  SELECT
							  count ( ra3.* )
						  FROM
							  resource_allocations ra3
							  LEFT JOIN resource_requisitions rr3 ON ( rr3.id = ra3.resource_requisition_id )
							  JOIN purchase_requests pr3 ON ( pr3.id = rr3.purchase_request_id )
						  WHERE
							  pr3.id = ' . ( int ) $intPurchaseRequestId . '
							  AND pr3.approved_on IS NOT NULL
						) THEN 1
							ELSE 0
						  END ) AS is_allocated
					FROM
						purchase_requests pr1
						LEFT JOIN resource_requisitions rr1 ON ( rr1.purchase_request_id = pr1.id )
					WHERE
						pr1.purchase_request_type_id = 1
						AND pr1.id = ' . ( int ) $intPurchaseRequestId . '
					GROUP BY
						pr1.id ) AS sub';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestsByIds( $arrintPurchaseRequestIds, $objDatabase ) {
		if( true == empty( $arrintPurchaseRequestIds ) ) return false;

		$strSql = ' SELECT 
						pr.*,
						d.name as designation_name_new,
						dep.id AS department_id 
					FROM purchase_requests pr
						LEFT JOIN designations d ON ( pr.designation_id_new = d.id OR pr.designation_id_old = d.id )
						LEFT JOIN designations d1 ON ( pr.designation_id_new = d1.id )
						LEFT JOIN departments dep ON ( d1.department_id = dep.id )
					WHERE pr.id IN ( ' . implode( ',', $arrintPurchaseRequestIds ) . ' )';

		return self::fetchPurchaseRequests( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestReportCardCount( $arrstrPurchaseRequestReportCardFilter, $objDatabase, $boolIsRoleAllowUserToShowRecruitmentData = false ) {

		$strJOINCondition	= '';
		$strWhereCondition	= '';
		$strCondition		= '';
		$arrstrPurchaseRequestReportCardFilter['start_date']			= date( 'Y/m/d', strtotime( '-3 month' ) );
		$arrstrPurchaseRequestReportCardFilter['end_date']				= date( 'Y/m/d' );

		if( true == $boolIsRoleAllowUserToShowRecruitmentData && true == $arrstrPurchaseRequestReportCardFilter['is_hr_representative'] && true == is_numeric( $arrstrPurchaseRequestReportCardFilter['employee_id'] ) && false == isset( $arrstrPurchaseRequestReportCardFilter['show_all_requests'] ) ) {
		$strWhereCondition  = ' t.hr_representative_employee_id = ' . $arrstrPurchaseRequestReportCardFilter['employee_id'] . ' AND ';
			$strJOINCondition	= ' LEFT JOIN resource_requisitions rr ON( rr.purchase_request_id = pr.id )
									LEFT JOIN team_employees te ON ( CASE WHEN pr.purchase_request_type_id = 1 THEN te.employee_id = rr.manager_employee_id ELSE te.employee_id = pr.employee_id END AND te.is_primary_team = 1 )
									LEFT JOIN teams t ON ( CASE WHEN pr.team_id IS NOT NULL THEN t.id = pr.team_id WHEN rr.manager_employee_id = ' . CEmployee::ID_PREETAM_YADAV . ' THEN t.manager_employee_id=rr.manager_employee_id ELSE te.team_id = t.id END)';
		} elseif( true == $boolIsRoleAllowUserToShowRecruitmentData && false == isset( $arrstrPurchaseRequestReportCardFilter['show_all_requests'] ) ) {
			$strCondition 		= ' AND ( pa.employee_id = ' . $arrstrPurchaseRequestReportCardFilter['employee_id'] . ' OR pr.created_by = ' . $arrstrPurchaseRequestReportCardFilter['user_id'] . ' ) ';
			$strJOINCondition	= ' LEFT JOIN purchase_approvals pa ON ( pa.purchase_request_id = pr.id )';
		}

		$strSql = ' SELECT
						count ( DISTINCT(CASE WHEN  pr.denied_on IS NULL AND pr.deleted_on IS NOT NULL THEN NULL WHEN 1=1 ' . $strCondition . '  THEN pr.id ELSE NULL END ) ) AS all_count,
						count ( DISTINCT(CASE WHEN pr.approved_on IS NULL AND pr.denied_on IS NULL AND pr.deleted_on IS NULL AND ( pr.base_amount_dollars_encrypted IS NOT NULL OR pr.bonus_type_id =' . CBonusType::PAID_DAY_OFF . ')' . $strCondition . ' THEN pr.id ELSE NULL END ) ) AS pending_count,
						count ( DISTINCT( CASE WHEN pr.approved_on IS NOT NULL AND pr.denied_on IS NULL ' . $strCondition . ' THEN pr.id ELSE NULL END ) ) AS approved_count,
						count ( DISTINCT(CASE WHEN pr.denied_on IS NOT NULL AND pr.approved_on IS NULL ' . $strCondition . '  THEN pr.id ELSE NULL END ) ) AS denied_count
					FROM
						purchase_requests pr ' . $strJOINCondition . '
						JOIN users u ON( u.id = pr.requested_by )
						JOIN employee_addresses ea ON ( ea.employee_id = u.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ')
					WHERE
						' . $strWhereCondition . '
						ea.country_code = \'' . $arrstrPurchaseRequestReportCardFilter['country_code'] . '\'
						AND pr.purchase_request_type_id NOT IN ( ' . CPurchaseRequestType::ASSET_PURCHASE . ', ' . CPurchaseRequestType::NEW_HIRE . ' )
						AND pr.created_on BETWEEN \'' . $arrstrPurchaseRequestReportCardFilter['start_date'] . ' 00:00:00\' AND \'' . $arrstrPurchaseRequestReportCardFilter['end_date'] . ' 23:59:59\' ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestReportCardData( $intPageNo, $intPageSize, $strOrderByField, $strOrderByType, $arrstrPurchaseRequestReportCardFilter, $objDatabase, $boolShowCount = false, $strSelectedDeniedType = 'DeniedByFinalApprover', $boolIsFromHrDashbaord = false ) {
		$strJOINCondition	= '';
		$strWhereCondition	= ' 1=1 ';
		$strHavingClause	= '';

		$strGroupBy			= ' GROUP BY
									pr.id,
									e.preferred_name,
									e.name_full,
									requested_by_employee_name,
									prt.name,
									prst.name,
									d.name,
									purchase_request_status';

		$strStartDate 	= date( 'Y/m/d', strtotime( '-3 month' ) );
		$strEndDate		= date( 'Y/m/d' );

		$strPurchaseRequestTypeCondition = ' AND pr.purchase_request_type_id NOT IN ( ' . CPurchaseRequestType::ASSET_PURCHASE . ' )';

		if( true == $boolIsFromHrDashbaord ) {
			$strPurchaseRequestTypeCondition = ' AND pr.purchase_request_type_id NOT IN ( ' . CPurchaseRequestType::ASSET_PURCHASE . ', ' . CPurchaseRequestType::NEW_HIRE . ' )';
		}

		if( true == isset( $arrstrPurchaseRequestReportCardFilter['selected_tab'] ) && 'PendingByMe' == $arrstrPurchaseRequestReportCardFilter['selected_tab'] ) {
			$strWhereCondition .= ' AND pr.approved_on IS NULL AND pr.denied_on IS NULL AND pr.deleted_on IS NULL AND ( pr.base_amount_dollars_encrypted IS NOT NULL OR pr.bonus_type_id =' . CBonusType::PAID_DAY_OFF . ' ) ';
		}

		if( true == isset( $arrstrPurchaseRequestReportCardFilter['selected_tab'] ) && 'AmountPending' == $arrstrPurchaseRequestReportCardFilter['selected_tab'] ) {
			$strWhereCondition .= ' AND pr.approved_on IS NULL AND pr.denied_on IS NULL AND pr.base_amount_dollars_encrypted IS NULL AND ( pr.bonus_type_id IS NULL OR pr.bonus_type_id <>' . CBonusType::PAID_DAY_OFF . ')  ';
		}

		if( true == isset( $arrstrPurchaseRequestReportCardFilter['selected_tab'] ) && 'Approved' == $arrstrPurchaseRequestReportCardFilter['selected_tab'] ) {
			$strWhereCondition .= ' AND pr.approved_on IS NOT NULL AND pr.denied_on IS NULL ';
		}

		if( true == isset( $arrstrPurchaseRequestReportCardFilter['selected_tab'] ) && 'Denied' == $arrstrPurchaseRequestReportCardFilter['selected_tab'] ) {
			$strWhereCondition .= ' AND pr.approved_on IS NULL ';

			if( 'DeniedByFinalApprover' == $strSelectedDeniedType ) {
				$strWhereCondition .= ' AND pr.denied_on IS NOT NULL ';
			} elseif( 'DeniedByManagers' == $strSelectedDeniedType ) {
				$strWhereCondition 	.= ' AND pr.denied_on IS NULL ';
				$strHavingClause 	.= ' HAVING MAX( CASE WHEN pa.employee_id <> pr.approver_employee_id THEN pa.denied_on ELSE NULL END ) IS NOT NULL ';
			} elseif( 'all' == $strSelectedDeniedType ) {
				$strWhereCondition 	.= ' AND ( pr.denied_on IS NOT NULL OR pa.denied_on IS NOT NULL ) ';
			}
		}

		if( true == $arrstrPurchaseRequestReportCardFilter['is_hr_representative'] && true == is_numeric( $arrstrPurchaseRequestReportCardFilter['employee_id'] ) && false == $arrstrPurchaseRequestReportCardFilter['show_all_requests'] ) {
			$strWhereCondition  .= ' AND t.hr_representative_employee_id = ' . $arrstrPurchaseRequestReportCardFilter['employee_id'];
			$strJOINCondition	.= 'LEFT JOIN resource_requisitions rr ON( rr.purchase_request_id = pr.id )
									LEFT JOIN team_employees te ON ( CASE WHEN pr.purchase_request_type_id = 1 THEN te.employee_id = rr.manager_employee_id ELSE te.employee_id = pr.employee_id END AND te.is_primary_team = 1 )
									LEFT JOIN teams t ON ( CASE WHEN pr.team_id IS NOT NULL THEN t.id = pr.team_id WHEN rr.manager_employee_id = ' . CEmployee::ID_PREETAM_YADAV . ' THEN t.manager_employee_id=rr.manager_employee_id ELSE te.team_id = t.id END)';
		} elseif( false == $arrstrPurchaseRequestReportCardFilter['show_all_requests'] && 'Denied' != $arrstrPurchaseRequestReportCardFilter['selected_tab'] ) {
			$strWhereCondition 	.= 'AND ( pa.employee_id = ' . $arrstrPurchaseRequestReportCardFilter['employee_id'] . ' OR pr.created_by = ' . $arrstrPurchaseRequestReportCardFilter['user_id'] . ' ) AND ';
			$strJOINCondition	.= 'LEFT JOIN purchase_approvals pa ON ( pa.purchase_request_id = pr.id )';
		}

		if( 'Denied' == $arrstrPurchaseRequestReportCardFilter['selected_tab'] ) {
			$strJOINCondition	.= ' JOIN purchase_approvals pa ON ( pa.purchase_request_id = pr.id )';
		}

		$strOrderBy = ' ORDER BY ';
		if( false == empty( $strOrderByField ) ) {
			$strOrderBy .= addslashes( $strOrderByField );
			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
			$strOrderBy .= ' , ';
		}

		$strOrderBy .= ' pr.created_on ASC';

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		$strSql = ' SELECT
						pr.*,
						e.preferred_name AS requested_by_employee_name,
						d.name AS designation_name,
						prt.name AS purchase_request_type_name,
						prst.name AS purchase_request_status
					FROM
						purchase_requests pr ' . $strJOINCondition . '
						JOIN purchase_request_types prt ON ( prt.id = pr.purchase_request_type_id )
						JOIN purchase_request_status_types prst ON ( prst.id = pr.purchase_request_status_type_id )
						JOIN users u ON( u.id = pr.requested_by )
						LEFT JOIN designations d ON ( d.id = pr.designation_id_old )
						JOIN employees e ON ( e.id = u.employee_id )
						JOIN employee_addresses ea ON ( ea.employee_id = u.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' AND ea.country_code = \'' . $arrstrPurchaseRequestReportCardFilter['country_code'] . '\')
					WHERE
						' . $strWhereCondition . '
						 AND pr.created_on BETWEEN \'' . $strStartDate . ' 00:00:00\' AND \'' . $strEndDate . ' 23:59:59\'
						 ' . $strPurchaseRequestTypeCondition . '
						 AND ( ( pr.denied_on IS NOT NULL AND pr.deleted_on IS NOT NULL ) OR pr.deleted_on IS NULL )';

		$strSql .= $strGroupBy . $strHavingClause . $strOrderBy . ', pr.id DESC';

		if( false == $boolShowCount ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset;

			if( true == isset( $intLimit ) && 0 < $intLimit ) {
				$strSql .= '  LIMIT	' . ( int ) $intLimit;
			}
		}

		return parent::fetchPurchaseRequests( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestsByResourceRequisitionIds( $arrintResourceRequisitionIds, $objDatabase ) {

		$strSql	= 'SELECT
						pr.*,
						rr.id AS resource_requisition_id
					FROM
						purchase_requests pr
						JOIN resource_requisitions rr ON( pr.id = rr.purchase_request_id )
					WHERE
						rr.id IN( ' . implode( ',', $arrintResourceRequisitionIds ) . ' ) ';

		return self::fetchPurchaseRequests( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestsByEmployeeIdByPurchaseRequestTypeIdByBonusTypeId( $intEmployeeId, $intPurchaseRequestTypeId, $strEffectiveDate = NULL, $boolIsConsiderApprovedOnly = false, $objDatabase ) {
		if( false == is_numeric( $intEmployeeId ) || false == is_numeric( $intPurchaseRequestTypeId ) ) return NULL;

		$strOrderBy = ' ORDER BY pr.id DESC ';
		if( true == valStr( $strEffectiveDate ) ) {
			$strWhere = ' AND EXTRACT( YEAR FROM pr.effective_date ) in ( EXTRACT( YEAR FROM \'' . $strEffectiveDate . '\'::DATE ) , EXTRACT( YEAR FROM NOW() ) )';
		} else {
			$strWhere = ' AND EXTRACT( YEAR FROM pr.effective_date ) = EXTRACT( YEAR FROM NOW() ) ';
		}

		$strWhere .= ( true == $boolIsConsiderApprovedOnly ) ? ' AND pr.approved_on IS NOT NULL' : ' AND CASE WHEN pr.denied_on IS NOT NULL THEN pr.deleted_on IS NOT NULL ELSE pr.deleted_on IS NULL END ';

		$strSql	= ' SELECT
						pr.*,
						prs.name as purchase_request_status,
						e1.preferred_name as requested_by_employee_name
					FROM
						purchase_requests pr
						LEFT JOIN users u ON ( u.id = pr.requested_by )
						LEFT JOIN employees e1 ON ( u.employee_id = e1.id )
						LEFT JOIN purchase_request_status_types prs ON ( prs.id = pr.purchase_request_status_type_id )
					WHERE
						pr.employee_id = ' . ( int ) $intEmployeeId . '
						AND pr.bonus_type_id <> ' . CBonusType::PAID_DAY_OFF . '
						AND pr.purchase_request_type_id =' . ( int ) $intPurchaseRequestTypeId . $strWhere . $strOrderBy;

			return self::fetchPurchaseRequests( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestsByCountryCode( $strCountryCode, $objDatabase, $strStartDate = NULL, $strEndDate = NULL, $intRecruiterEmployeeId = NULL ) {

		if( false == valStr( $strCountryCode ) ) return NULL;

		$strWhere = '';

		if( true == is_numeric( $intRecruiterEmployeeId ) ) {
			$strWhere = ' AND rr.recruiter_employee_id = ' . ( int ) $intRecruiterEmployeeId;
		}

		$strSql = ' SELECT
				 		COALESCE ( e.id, 0 ) AS recruiter_id,
						COALESCE ( e.preferred_name, \'NA\' ) AS recruiter_name,
						count ( pr.id ) AS purchase_request_closed_count,
						AVG ( ea.applicant_offerred_on::DATE - rr.recruiter_assigned_on::DATE ) AS hire_rate,
						AVG ( ra.created_on::DATE - rr.recruiter_assigned_on::DATE ) AS pr_closed
					FROM
						purchase_requests pr
						LEFT JOIN resource_requisitions rr ON ( rr.purchase_request_id = pr.id )
						LEFT JOIN employee_applications ea ON ( ea.purchase_request_id = pr.id )
						JOIN resource_allocations ra ON ( ra.resource_requisition_id = rr.id )
						LEFT JOIN employees e ON ( e.id = rr.recruiter_employee_id )
						LEFT JOIN employee_addresses eas ON ( rr.manager_employee_id = eas.employee_id AND eas.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						pr.denied_on IS NULL
						AND pr.purchase_request_status_type_id NOT IN ( ' . CPurchaseRequestStatusType::ID_DENIED_BY_MANAGER . ',' . CPurchaseRequestStatusType::ID_HOLD . ',' . CPurchaseRequestStatusType::ID_SPLITED . ',' . CPurchaseRequestStatusType::ID_MERGED . ',' . CPurchaseRequestStatusType::ID_REMOVED . ',' . CPurchaseRequestStatusType::ID_PENDING_FOR_REMOVE_FROM_HR . ',' . CPurchaseRequestStatusType::ID_PENDING_BY_HR . ' )
						AND rr.deleted_by IS NULL
						AND eas.country_code = \'' . $strCountryCode . '\'' . $strWhere . '
						AND date_trunc( \'day\', ( rr.created_on AT TIME ZONE \'IST\') ) BETWEEN \'' . addslashes( $strStartDate ) . '\' AND \'' . addslashes( $strEndDate ) . '\'
					GROUP BY
						e.preferred_name,
						e.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeBonusApprovedPurchaseRequestsByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) return false;

		$strSql	= 'SELECT
						pr.*
					FROM
						purchase_requests pr
						LEFT JOIN employees e ON ( e.id = pr.employee_id )
						LEFT JOIN users u ON ( u.id = pr.requested_by )
						LEFT JOIN employees e1 ON ( u.employee_id = e1.id )
						LEFT JOIN purchase_request_status_types prs ON ( prs.id = pr.purchase_request_status_type_id )
					WHERE
						pr.purchase_request_type_id = ' . CPurchaseRequestType::EMPLOYEE_BONUS . '
						AND pr.bonus_type_id <> ' . CBonusType::PAID_DAY_OFF . '
						AND pr.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND pr.approved_on IS NOT NULL';

		return self::fetchPurchaseRequests( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestPositionsOpenClosedByStartDateByEndDate( $strCountryCode, $strStartDate, $strEndDate, $objDatabase, $intRecruiterId = NULL ) {

		if( true == is_null( $strStartDate ) && true == is_null( $strEndDate ) ) return NULL;
		$strSubSql = '';
		$strWhere = '';

		if( false == is_null( $intRecruiterId ) && 0 != $intRecruiterId ) {
			$strWhere	= ' AND rr.recruiter_employee_id = ' . ( int ) $intRecruiterId;
		}

		$strCondition	= ' AND ea.employee_application_status_type_id NOT IN ( 12, 38, 39, 16, 40 ) ';

		$strSql = 'SELECT
					rt.type,
					SUM ( CASE
							WHEN pr.approved_on < \'' . addslashes( $strStartDate ) . '\' AND ra.created_on IS NULL THEN 1
							ELSE 0
						END ) AS past_pr,
					SUM ( CASE
							WHEN pr.approved_on >= \'' . addslashes( $strStartDate ) . '\' AND pr.approved_on <=  \'' . addslashes( $strEndDate ) . '\' AND ra.created_on IS NULL THEN 1
							ELSE 0
						END ) AS new_added_pr,
					SUM ( CASE
							WHEN ea.applicant_offerred_on ::date< \'' . addslashes( $strStartDate ) . '\' ' . $strCondition . ' THEN 1
							ELSE 0
						END ) AS past_shortlisted,
					SUM ( CASE
						WHEN ea.applicant_offerred_on ::date >= \'' . addslashes( $strStartDate ) . '\' AND ea.applicant_offerred_on ::date <= \'' . addslashes( $strEndDate ) . '\' ' . $strCondition . ' THEN 1
						ELSE 0
					END ) AS current_month_shortlisted,
					string_agg ( CASE
							WHEN pr.approved_on < \'' . addslashes( $strStartDate ) . '\' AND ra.created_on IS NULL THEN pr.id::TEXT
						END, \',\' )::TEXT AS past_pr_ids,
					string_agg ( CASE
							WHEN pr.approved_on >= \'' . addslashes( $strStartDate ) . '\' AND pr.approved_on <=  \'' . addslashes( $strEndDate ) . '\' AND ra.created_on IS NULL THEN pr.id::TEXT
						END, \',\' )::TEXT AS new_added_pr_ids,
					string_agg ( CASE
							WHEN ea.applicant_offerred_on ::date< \'' . addslashes( $strStartDate ) . '\' ' . $strCondition . ' THEN ea.id::TEXT
						END, \',\' )::TEXT AS past_shortlisted_ids,
					string_agg ( CASE
						WHEN ea.applicant_offerred_on ::date >= \'' . addslashes( $strStartDate ) . '\' AND ea.applicant_offerred_on ::date <= \'' . addslashes( $strEndDate ) . '\' ' . $strCondition . ' THEN ea.id::TEXT
					END, \',\' )::TEXT AS current_month_shortlisted_ids
				FROM
					purchase_requests pr
					JOIN resource_requisitions rr ON ( rr.purchase_request_id = pr.id )
					JOIN requirement_types rt ON ( rr.requirement_type_id = rt.id )
					LEFT JOIN resource_allocations ra ON ( rr.id = ra.resource_requisition_id )
					LEFT JOIN employee_applications ea ON ( ea.purchase_request_id = pr.id )
					LEFT JOIN employee_addresses eas ON ( rr.manager_employee_id = eas.employee_id AND eas.address_type_id = 1 )
				WHERE
					pr.deleted_on IS NULL
					AND eas.country_code = \'' . addslashes( $strCountryCode ) . '\'
					' . $strWhere . '
				GROUP BY
					rt.id ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestByStartDateByEndDate( $strStartDate, $strEndDate, $objDatabase, $arrmixPurchaseRequestFilter = NULL, $intPageNo = NULL, $intPageSize = NULL, $boolIsDownloadReport = false ) {
		if( false == valStr( $strStartDate ) && false == valStr( $strEndDate ) ) return NULL;

		$strOrderByClause	= ' ORDER BY pr.id';
		$strSubSelectClause = ( true == $boolIsDownloadReport ) ? 'CASE WHEN TRUE = pr.is_internal_hire THEN \'Internal Hire\' ELSE \'External Hire\' END AS is_internal_hire,' : 'pr.is_internal_hire,';

		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$intOffset 	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
			$strSubSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;
		}

		$strWhereCondition	= '';
		if( true == valArr( $arrmixPurchaseRequestFilter ) ) {
			if( false == $boolIsDownloadReport ) {
				$strOrderByClause = ' ORDER BY ' . $arrmixPurchaseRequestFilter['sort_by_field'] . ' ' . $arrmixPurchaseRequestFilter['sort_by_type'] . '';
			}

			if( true == valArr( $arrmixPurchaseRequestFilter['add_employees'] ) ) {
				$strWhereCondition .= ' AND t.add_employee_id IN ( ' . implode( ',', $arrmixPurchaseRequestFilter['add_employees'] ) . ' )';
			}

			if( true == valArr( $arrmixPurchaseRequestFilter['hr_representatives'] ) ) {
				$strWhereCondition .= ' AND t.hr_representative_employee_id IN ( ' . implode( ',', $arrmixPurchaseRequestFilter['hr_representatives'] ) . ' )';
			}

			if( true == valArr( $arrmixPurchaseRequestFilter['tpm_employees'] ) ) {
				$strWhereCondition .= ' AND t.tpm_employee_id IN ( ' . implode( ',', $arrmixPurchaseRequestFilter['tpm_employees'] ) . ' )';
			}

			if( true == valArr( $arrmixPurchaseRequestFilter['requirement_types'] ) ) {
				$strWhereCondition .= ' AND rt.id IN ( ' . implode( ',', $arrmixPurchaseRequestFilter['requirement_types'] ) . ' )';
			}
		}

		$strSql = 'SELECT
					pr.id AS purchase_request_id,
					' . $strSubSelectClause . '
					rt.type AS requirement_type,
					pr.created_on::DATE AS requested_on,
					ra.created_on::DATE AS closed_on,
					eap.applicant_offerred_on::DATE as offer_extended_on,
					e.preferred_name AS new_employee_name,
					e.employee_number AS new_employee_number,
					d.name as new_employee_designation,
					e.date_started as date_of_joining,
					e1.preferred_name AS previous_employee_name,
					e1.employee_number AS previous_employee_number,
					e1.date_terminated AS last_working_date,
					e2.preferred_name AS add_employee_name,
					e3.preferred_name AS hr_representative_employee_name,
					e4.preferred_name AS tpm_employee_name
				FROM
					purchase_requests pr
					LEFT JOIN resource_requisitions rr ON ( pr.id = rr.purchase_request_id )
					LEFT JOIN resource_allocations ra ON ( ra.resource_requisition_id = rr.id )
					LEFT JOIN requirement_types rt ON ( rt.id = rr.requirement_type_id )
					LEFT JOIN employee_applications eap ON ( pr.id = eap.purchase_request_id )
					LEFT JOIN employees e ON ( ra.employee_id = e.id )
					LEFT JOIN employees e1 ON ( e1.id ::TEXT IN  ( rr.replacement_employee_ids ) )
					LEFT JOIN teams t ON ( t.id = pr.team_id )
					LEFT JOIN employees e2 ON ( t.add_employee_id = e2.id )
					LEFT JOIN employees e3 ON ( t.hr_representative_employee_id = e3.id )
					LEFT JOIN employees e4 ON ( t.tpm_employee_id = e4.id )
					LEFT JOIN designations d on ( e.designation_id = d.id )
					JOIN employee_addresses ea ON ( e2.id = ea.employee_id AND ea.address_type_id = 1 )
				WHERE
					pr.purchase_request_type_id = 1
					AND pr.deleted_by IS NULL
					AND pr.approved_on IS NOT NULL
					AND pr.created_on::DATE BETWEEN \'' . $strStartDate . '\'
					AND \'' . $strEndDate . '\'
					AND pr.purchase_request_status_type_id NOT IN ( ' . CPurchaseRequestStatusType::ID_HOLD . ', ' . CPurchaseRequestStatusType::ID_MERGED . ', ' . CPurchaseRequestStatusType::ID_SPLITED . ',' . CPurchaseRequestStatusType::ID_REMOVED . ',' . CPurchaseRequestStatusType::ID_PENDING_FOR_REMOVE_FROM_HR . ',' . CPurchaseRequestStatusType::ID_PENDING_BY_HR . ' ) 
					' . $strWhereCondition . '
					' . $strOrderByClause . '
					' . $strSubSql . '';

		return fetchData( $strSql, $objDatabase );
	}

	public function fetchPurchaseRequestByIdWithRecentApproverDetails( $intPurchaseRequestId, $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						pr.*,
						MAX( CASE WHEN pa.approved_on IS NOT NULL or pa.denied_on IS NOT NULL THEN pa.order_num ELSE NULL END ) AS  recent_approver_order_num,
						MAX( CASE WHEN pa.employee_id = ' . ( int ) $intEmployeeId . ' THEN pa.denied_on ELSE NULL END ) AS approvals_denied_on
					FROM
						purchase_requests pr
						LEFT JOIN purchase_approvals pa ON ( pa.purchase_request_id = pr.id )
					WHERE
						pr.id = ' . ( int ) $intPurchaseRequestId . '
					GROUP BY
						pr.id';
		return parent::fetchPurchaseRequest( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestNewHireCount( $intHrRepresentativeEmployeeId, $boolShowAllRequests, $objDatabase ) {

		$strWhereCondition  = '';

		if( false == is_null( $intHrRepresentativeEmployeeId ) && false == $boolShowAllRequests ) {
			if( true == valStr( $intHrRepresentativeEmployeeId ) ) {
				$strHrRepresentativeEmployeeIds = $intHrRepresentativeEmployeeId;
				$strWhereCondition = ' AND t.hr_representative_employee_id IN ( ' . $strHrRepresentativeEmployeeIds . ' ) ';
			} else {
				$strWhereCondition = ' AND t.hr_representative_employee_id in ( ' . ( int ) $intHrRepresentativeEmployeeId . ' ) ';
			}
		}

		$strWhere = ' AND rr.recruiter_assigned_on IS NOT NULL';
		$strSql = 'SELECT
						SUM( CASE
								WHEN ra.id IS NOT NULL AND ra.created_on::DATE >= NOW()::DATE - INTERVAL \'30\' DAY ' . $strWhere . ' THEN 1
								ELSE 0
							END ) AS closed_purchase_request,
						SUM( CASE
								WHEN ra.id IS NULL AND ( pr.approved_on::DATE >= NOW()::DATE - INTERVAL \'30\' DAY OR ea.applicant_offerred_on::DATE >= NOW()::DATE - INTERVAL \'30\' DAY ) ' . $strWhere . ' THEN 1
								ELSE 0
							END ) AS current_open_purchase_request,
						SUM( CASE
								WHEN pjps.ps_job_posting_step_type_id = ' . CPsJobPostingStepType::OFFER . ' AND pjpss.ps_job_posting_status_id IN (' . CPsJobPostingStatus::IN_PROCESS . ',' . CPsJobPostingStatus::SELECTED . ') AND ra.id IS NULL AND ea.applicant_offerred_on::DATE >= NOW()::DATE - INTERVAL \'30\' DAY ' . $strWhere . ' THEN 1
								ELSE 0
							END ) as tagged_requests,
						SUM( CASE
								WHEN ea.purchase_request_id IS NULL AND pr.approved_on::DATE >= NOW()::DATE - INTERVAL \'30\' DAY ' . $strWhere . ' THEN 1
								ELSE 0
							END ) as untagged_requests,
						SUM( CASE
								WHEN ea.applicant_offerred_on IS NULL AND pr.approved_on::DATE <= NOW()::DATE - INTERVAL \'30\' DAY AND date_part( \'year\', pr.approved_on) = date_part(\'year\', CURRENT_DATE ) THEN 1
								ELSE 0
							END ) as aging

					FROM
						purchase_requests pr
						JOIN resource_requisitions rr ON (rr.purchase_request_id = pr.id)
						JOIN requirement_types rt ON (rr.requirement_type_id = rt.id)
						LEFT JOIN team_employees te ON ( te.employee_id = rr.manager_employee_id AND te.is_primary_team = 1 )
						LEFT JOIN teams t ON ( CASE WHEN pr.team_id IS NOT NULL THEN t.id = pr.team_id WHEN rr.manager_employee_id = ' . CEmployee::ID_PREETAM_YADAV . ' THEN t.manager_employee_id=rr.manager_employee_id ELSE te.team_id = t.id END)
						LEFT JOIN resource_allocations ra ON (rr.id = ra.resource_requisition_id)
						LEFT JOIN employee_applications ea ON (ea.purchase_request_id = pr.id)
						LEFT JOIN ps_job_posting_step_statuses pjpss ON (ea.ps_job_posting_step_status_id = pjpss.id)
						LEFT JOIN ps_job_posting_steps pjps ON (pjpss.ps_job_posting_step_id = pjps.id)
						LEFT JOIN employee_addresses eas ON (rr.manager_employee_id = eas.employee_id AND eas.address_type_id = 1)
					WHERE
						pr.approved_on IS NOT NULL
						 ' . $strWhereCondition . '
						AND pr.deleted_by IS NULL
						AND pr.deleted_on IS NULL
						AND eas.country_code = \'IN\'';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchApprovedPurchaseRequestsByEmployeeIdByPurchaseRequestTypeIds( $intEmployeeId, $arrintPurchaseRequestTypeIds, $intPageNo, $intPageSize, $boolIsCount, $objDatabase ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;
		$strSql = 'SELECT
						id,
						purchase_request_type_id,
						CASE WHEN purchase_request_status_type_id = ' . CPurchaseRequestStatusType::ID_APPROVED . ' THEN \'Approved\' END as status,
						created_on
					FROM
						purchase_requests
					WHERE
						employee_id = ' . ( int ) $intEmployeeId . '
						AND purchase_request_type_id IN (' . implode( ',', $arrintPurchaseRequestTypeIds ) . ')
						AND purchase_request_status_type_id = ' . CPurchaseRequestStatusType::ID_APPROVED . '
					ORDER BY created_on DESC';
		if( false == $boolIsCount ) $strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUnApprovedPurchaseRequestsByPurchaseRequestTypeIds( $arrintPurchaseRequestTypeIds, $arrstrEmployeeCompensationFilter, $strCountryCode, $objDatabase, $boolCount = false ) {
		$intOffset			= ( false == $boolCount && false == empty( $arrstrEmployeeCompensationFilter['page_no'] ) && false == empty( $arrstrEmployeeCompensationFilter['page_size'] ) ) ? $arrstrEmployeeCompensationFilter['page_size'] * ( $arrstrEmployeeCompensationFilter['page_no'] - 1 ) : 0;
		$intLimit			= ( false == $boolCount && true == isset( $arrstrEmployeeCompensationFilter['page_size'] ) ) ? $arrstrEmployeeCompensationFilter['page_size'] : '';
		$strOrderByClause	= ( false == $boolCount ) ? ' ORDER BY pr.id DESC ' : '';
		$strWhereClause		= ( false == empty( $arrstrEmployeeCompensationFilter['department_ids'] ) && true == valArr( array_filter( $arrstrEmployeeCompensationFilter['department_ids'] ) ) ) ? ' AND e.department_id IN ( ' . implode( ',', $arrstrEmployeeCompensationFilter['department_ids'] ) . ') ' : '';

		$strSelectClause 	= ( false == $boolCount ) ? ' DISTINCT( pr.id ), pr.purchase_request_type_id, e.id AS employee_id, e.name_full ' : ' count( DISTINCT( pr.id ) )';
			$strSql = ' SELECT '
						. $strSelectClause . '
					FROM
						purchase_requests pr
						JOIN employees e ON( pr.employee_id = e.id AND e.employee_status_type_id = 1 )
						JOIN employee_addresses ea ON( pr.employee_id = ea.employee_id AND ea.country_code = \'' . $strCountryCode . '\' )
					WHERE
						pr.deleted_by IS NULL
						AND pr.purchase_request_type_id IN (' . implode( ',', $arrintPurchaseRequestTypeIds ) . ')
						AND pr.purchase_request_status_type_id <> ' . CPurchaseRequestStatusType::ID_APPROVED .
						$strWhereClause . $strOrderByClause . '
					OFFSET ' . ( int ) $intOffset;

		if( true == $boolCount ) {
			$arrmixPurchaseRequests = fetchData( $strSql, $objDatabase );
			return ( true == valArr( $arrmixPurchaseRequests ) ) ? $arrmixPurchaseRequests[0]['count'] : 0;
		} else {
			if( 0 < $intLimit ) {
				$strSql .= ' LIMIT ' . ( int ) $intLimit;
			}
			return fetchData( $strSql, $objDatabase );
		}

	}

	public static function fetchPurchaseRequestsByReportingManagerEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT   STRING_AGG( rr.replacement_employee_ids, \',\') As replacement_ids,
							COUNT( CASE WHEN pr.purchase_request_status_type_id = ' . CPurchaseRequestStatusType::ID_APPROVED . ' THEN 1 END ) AS filling_count,
							COUNT( CASE WHEN pr.purchase_request_status_type_id = ' . CPurchaseRequestStatusType::ID_HOLD . ' THEN 1 END ) AS holding_count
					FROM purchase_requests pr
						 LEFT JOIN resource_requisitions rr ON ( pr.id = rr.purchase_request_id )
						 LEFT JOIN employee_applications  ea ON ( pr.id = ea.purchase_request_id ) 
					WHERE pr.deleted_by IS NULL AND
						  pr.deleted_on IS NULL AND
						  ea.employee_id IS NULL
						  AND rr.manager_employee_id = ' . ( int ) $intEmployeeId . '';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestRequesters( $intLoginEmployeeId, $objDatabase, $strActionRequest = NULL ) {
		$strWhereCondition = '';
		$strSelectCondition = '';
		if( false == is_null( $strActionRequest ) ) {
			$strWhereCondition .= ' AND pr.purchase_request_status_type_id NOT IN ( ' . implode( ',', CPurchaseRequestStatusType::$c_arrintClosedPurchaseRequestStatusTypeIds ) . ' )';
			switch( $strActionRequest ) {
				case 'all-pending':
					$strWhereCondition .= ' AND pr.purchase_request_status_type_id IN ( ' . implode( ', ', CPurchaseRequestStatusType::$c_arrintPendingPurchaseRequestStatusTypeIds ) . ' ) ';
					break;

				case 'new-hires':
					$strWhereCondition .= ' AND pr.purchase_request_type_id =' . CPurchaseRequestType::NEW_HIRE;
					break;

				case 'comp-changes':
					$strSelectCondition .= ' , e1.id as manager_employee_id,
												e1.preferred_name as manager_employee_name ';
					$strWhereCondition  .= ' AND pr.purchase_request_type_id =' . CPurchaseRequestType::COMP_CHANGE;
					break;

				case 'assets':
					$strWhereCondition .= ' AND pr.purchase_request_type_id =' . CPurchaseRequestType::ASSET_PURCHASE;
					break;

				case 'bonuses':
					$strSelectCondition .= ' , e1.id as manager_employee_id,
												e1.preferred_name as manager_employee_name ';
					$strWhereCondition  .= ' AND pr.purchase_request_type_id =' . CPurchaseRequestType::EMPLOYEE_BONUS;
					break;

				default:
					$strWhereCondition .= ' AND pr.purchase_request_status_type_id IN ( ' . implode( ', ', CPurchaseRequestStatusType::$c_arrintPendingPurchaseRequestStatusTypeIds ) . ' ) ';
					break;
			}
		}

		$strSql = 'SELECT 
						DISTINCT  
							u.id as requester_id,
							e.preferred_name as requester_name
							' . $strSelectCondition . '
					FROM 
						purchase_requests pr 
						LEFT JOIN users u ON( pr.requested_by = u.id )
						LEFT JOIN employees e ON ( u.employee_id = e.id )
						LEFT JOIN purchase_approvals pa ON ( pr.id = pa.purchase_request_id )
						LEFT JOIN employees em ON ( pr.employee_id = em.id )
						LEFT JOIN employees e1 ON ( e1.id = em.reporting_manager_id )
						LEFT JOIN resource_requisitions rr ON ( pr.id = rr.purchase_request_id )
						LEFT JOIN requirement_types rt ON ( rr.requirement_type_id = rt.id )
					WHERE 
						pr.deleted_on IS NULL
						AND pa.employee_id =' . ( int ) $intLoginEmployeeId . $strWhereCondition;
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPRFilledEmployeesByCountryCode( $strCountryCode, $objDatabase ) {

		if( false == valStr( $strCountryCode ) ) return NULL;

		$strSql = 'SELECT
						array_to_string( array_agg( DISTINCT( e.id ) ), \',\') AS pr_employee_ids
					FROM
						purchase_requests AS pr
						JOIN resource_requisitions AS rq ON ( pr.id = rq.purchase_request_id AND pr.purchase_request_status_type_id = ' . CPurchaseRequestStatusType::ID_APPROVED . ' AND rq.requirement_type_id = ' . CRequirementType::REPLACEMENT . ' )
						JOIN employees AS e ON ( e.id ::TEXT IN ( rq.replacement_employee_ids ) AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						JOIN employee_addresses AS ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' AND ea.country_code = \'' . $strCountryCode . '\' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNewHirePurchaseRequestsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		if( false == valId( $intManagerEmployeeId ) ) {
			return false;
		}

		$strSql = ' SELECT
						pr.id as purchase_request_id,
						d.name as designation_name
					FROM
						purchase_requests pr
						JOIN teams t ON ( t.id = pr.team_id )
						LEFT JOIN designations d ON ( d.id = pr.designation_id_new OR d.id = pr.designation_id_old )
					WHERE
						pr.purchase_request_type_id = ' . CPurchaseRequestType::NEW_HIRE . '
						AND t.manager_employee_id = ' . ( int ) $intManagerEmployeeId . '
						AND pr.purchase_request_status_type_id = ' . CPurchaseRequestStatusType::ID_APPROVED . '
						AND pr.approved_on IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestByPurchaseRequestIdByPurchaseRequestTypeIdByPurchaseRequestStatusTypeId( $intPurchaseRequestId, $intPurchaseRequestType, $intParentPurchaseRequestStatusTypeId, $intChildPurchaseRequestStatusTypeId, $objDatabase ) {

		if( false == valId( $intPurchaseRequestId ) || false == valId( $intPurchaseRequestType ) || false == valId( $intParentPurchaseRequestStatusTypeId ) || false == valId( $intChildPurchaseRequestStatusTypeId ) ) {
			return false;
		}

		$strSql = 'SELECT
						*
					FROM
						purchase_requests pr
						JOIN purchase_requests pr1 on( pr.id = pr1.purchase_request_id AND pr.id = ' . ( int ) $intPurchaseRequestId . ' )
					WHERE
						pr1.purchase_request_status_type_id = ' . ( int ) $intChildPurchaseRequestStatusTypeId . '
						AND pr1.approved_on IS NULL
						AND pr1.purchase_request_type_id = ' . ( int ) $intPurchaseRequestType . '
						AND pr.purchase_request_status_type_id = ' . ( int ) $intParentPurchaseRequestStatusTypeId . ' 
					ORDER BY pr1.id DESC LIMIT 1';

		return self::fetchPurchaseRequest( $strSql, $objDatabase );

	}

	public static function fetchPurchaseRequestsByApproverEmployeeId( $intApproverEmployeeId, $objDatabase ) {
		if( false == valId( $intApproverEmployeeId ) ) {
			return false;
		}

		$strSql = ' SELECT
						*
					FROM
						purchase_requests pr
						LEFT JOIN purchase_approvals pa on ( pr.id = pa.purchase_request_id )
					WHERE
						( pr.approver_employee_id = ' . ( int ) $intApproverEmployeeId . ' OR pa.employee_id = ' . ( int ) $intApproverEmployeeId . ' )
						AND ( pr.deleted_by IS NULL OR pr.purchase_request_status_type_id = ' . CPurchaseRequestStatusType::ID_REMOVED . ')';

		return self::fetchPurchaseRequests( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestById( $intPurchaseRequestId, $objDatabase ) {

		if( false == valId( $intPurchaseRequestId ) ) return NULL;

		$strSql = 'SELECT
						pr.*,
						rr.product_id,
						t.name as team_name,
						d.name as designation,
						d.employee_salary_type_id
					FROM
						purchase_requests pr
						LEFT JOIN resource_requisitions rr ON ( pr.id = rr.purchase_request_id )
						LEFT JOIN designations d ON ( d.id = pr.designation_id_new )
						LEFT JOIN teams AS t ON ( pr.team_id = t.id )
					WHERE pr.id = ' . ( int ) $intPurchaseRequestId;

		return parent::fetchPurchaseRequest( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestsByManagerEmployeeIdsByPurchaseRequestStatusTypeIds( $arrintManagerEmployeeIds, $arrintPurchaseRequestStatusTypeIds, $objDatabase ) {
		if( false == valArr( $arrintManagerEmployeeIds ) || false == valArr( $arrintPurchaseRequestStatusTypeIds ) ) {
			return false;
		}

			$strSql = 'SELECT
				 			pr.id,
				 			prst.name as status,
				 			d.name as designation,
				 			d.id as designation_id,
				 			d.employee_salary_type_id as salary_type_id,
				 			e.name_full as manager_name,
							pr.purchase_request_status_type_id
						FROM
							purchase_requests pr
							LEFT JOIN purchase_requests prs ON ( prs.purchase_request_id = pr.id )
							LEFT JOIN resource_requisitions rr ON ( rr.purchase_request_id = pr.id )
							LEFT JOIN purchase_request_status_types prst ON ( pr.purchase_request_status_type_id = prst.id )
							LEFT JOIN designations d ON ( rr.designation_id = d.id )
							LEFT JOIN employees e ON ( rr.manager_employee_id = e.id )
						WHERE
							pr.denied_on IS NULL
							AND rr.deleted_by IS NULL
							AND pr.purchase_request_status_type_id IN ( ' . sqlIntImplode( $arrintPurchaseRequestStatusTypeIds ) . ' )
							AND rr.manager_employee_id IN ( ' . sqlIntImplode( $arrintManagerEmployeeIds ) . ' ) ';

			return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPurchaseRequestsByDesignationId( $intDesignationId, $objDatabase ) {

		if( false == valId( $intDesignationId ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						purchase_requests pr
					WHERE
						pr.purchase_request_type_id = ' . CPurchaseRequestType::COMP_CHANGE . '
						AND pr.designation_id_new = ' . ( int ) $intDesignationId;

		return parent::fetchPurchaseRequests( $strSql, $objDatabase );
	}

	public static function fetchApprovedPurchaseRequestsByPurchaseRequestTypeIdByCountryCode( $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT pr.id
					FROM purchase_requests pr
						JOIN resource_requisitions rr ON (rr.purchase_request_id = pr.id)
						JOIN designations d ON (rr.designation_id = d.id)
					WHERE d.country_code =  \'' . $strCountryCode . '\' AND
						pr.deleted_on IS NULL AND
						pr.purchase_request_status_type_id = ' . CPurchaseRequestStatusType::ID_APPROVED . ' AND
						pr.purchase_request_type_id = ' . CPurchaseRequestType::NEW_HIRE . '
					ORDER BY pr.id ASC';

		return parent::fetchPurchaseRequests( $strSql, $objDatabase );
	}

	public static function fetchTeamByPurchaseRequestId( $intEmployeeApplicationId = NULL, $objDatabase ) {

		if( false == valId( $intEmployeeApplicationId ) ) return NULL;

		$strSql = 'SELECT t.id as team_id,t.name as team_name
					FROM
						purchase_requests pr
						JOIN employee_applications ea on ( ea.purchase_request_id = pr.id )
						JOIN teams t ON ( t.id = pr.team_id )
					WHERE
						ea.id = ' . ( int ) $intEmployeeApplicationId;

		return fetchData( $strSql, $objDatabase );
	}

}

?>