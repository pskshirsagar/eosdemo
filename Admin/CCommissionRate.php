<?php

use Psi\Eos\Admin\CCommissionRates;
use Psi\Eos\Admin\CCommissionRateAssociations;

class CCommissionRate extends CBaseCommissionRate {

	protected $m_intCid;
	protected $m_intAccountId;
	protected $m_intCommissionRecipientId;
	protected $m_strStartDate;
	protected $m_strEndDate;

	public function __construct() {
		parent::__construct();

		$this->m_intCid = NULL;
		$this->m_intAccountId = NULL;
		$this->m_intCommissionRecipientId = NULL;
		$this->m_strStartDate = NULL;
		$this->m_strEndDate = NULL;

		return;
	}

	/**
	 * Get Functions
	 */

	public function getCid() {
		return $this->m_intCid;
	}

	public function getAccountId() {
		return $this->m_intAccountId;
	}

	public function getCommissionRecipientId() {
		return $this->m_intCommissionRecipientId;
	}

	public function getStartDate() {
		return $this->m_strStartDate;
	}

	public function getEndDate() {
		return $this->m_strEndDate;
	}

	public function getChargeCodeName() {
		return $this->m_strChargeCodeName;
	}

	/**
	 * Set Functions
	 */

	public function setCid( $intCid ) {
		$this->m_intCid = CStrings::strToIntDef( $intCid, NULL, false );
	}

	public function setAccountId( $intAccountId ) {
		$this->m_intAccountId = CStrings::strToIntDef( $intAccountId, NULL, false );
	}

	public function setCommissionRecipientId( $intCommissionRecipientId ) {
		$this->m_intCommissionRecipientId = CStrings::strToIntDef( $intCommissionRecipientId, NULL, false );
	}

	public function setStartDate( $strStartDate ) {
		$this->m_strStartDate = CStrings::strTrimDef( $strStartDate, -1, NULL, true );
	}

	public function setEndDate( $strEndDate ) {
		$this->m_strEndDate = CStrings::strTrimDef( $strEndDate, -1, NULL, true );
	}

	public function setChargeCodeName( $strChargeCodeName ) {
		$this->m_strChargeCodeName = CStrings::strTrimDef( $strChargeCodeName, 100, NULL, true );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

	 	parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['cid'] ) ) {
			$this->setCid( $arrmixValues['cid'] );
		}

		if( true == isset( $arrmixValues['account_id'] ) ) {
			$this->setAccountId( $arrmixValues['account_id'] );
		}

		if( true == isset( $arrmixValues['commission_recipient_id'] ) ) {
			$this->setCommissionRecipientId( $arrmixValues['commission_recipient_id'] );
		}

		if( true == isset( $arrmixValues['start_date'] ) ) {
			$this->setStartDate( $arrmixValues['start_date'] );
		}

		if( true == isset( $arrmixValues['end_date'] ) ) {
			$this->setEndDate( $arrmixValues['end_date'] );
		}

		return;
	}

	/**
	 * Validate Functions
	 */

	public function valCommissionStructureId() {
		$boolIsValid = true;

	if( true == is_null( $this->getCommissionStructureId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_structure_id', 'Commission structure is required.' ) );
	}

		return $boolIsValid;
	}

	public function valChargeCodeId( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getChargeCodeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_code_id', 'Charge code is required.' ) );
		} elseif( false == is_null( $this->getCommissionStructureId() ) ) {
			// No double entries for the charge code for a commission structure allowed.

			$arrobjCommissionRates = CCommissionRates::createService()->fetchCommissionRatesByCommissionStructureIdByChargeCodeId( $this->getCommissionStructureId(), $this->getChargeCodeId(), $objDatabase );

			if( false == is_null( $arrobjCommissionRates ) && 1 <= \Psi\Libraries\UtilFunctions\count( $arrobjCommissionRates ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'charge_code_id', 'Charge code already exists for this commission structure.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valPsProductId( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is required.' ) );
		} elseif( false == is_null( $this->getCommissionStructureId() ) ) {

			$arrobjCommissionRates = CCommissionRates::createService()->fetchCommissionRatesByCommissionStructureIdByPsProductIdByCommissionBucketId( $this->getCommissionStructureId(), $this->getPsProductId(), $this->getCommissionBucketId(), $objDatabase );

			if( false == is_null( $arrobjCommissionRates ) && 1 <= \Psi\Libraries\UtilFunctions\count( $arrobjCommissionRates ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product already exists for this commission structure.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCommissionAmount() {

		$boolIsValid = true;

		if( true == is_null( $this->getCommissionAmount() ) && true == is_null( $this->getCommissionPercent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_amount', 'Commission amount is required.' ) );
		}

		if( false == is_null( $this->getCommissionAmount() ) && ( false == CValidation::checkCurrency( $this->getCommissionAmount() ) || false == is_numeric( $this->getCommissionAmount() ) || 0 == $this->getCommissionAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Commission amount should be greater than zero.' ) );
		}

		return $boolIsValid;
	}

	public function valCommissionPercent() {
		$boolIsValid = true;

		if( true == is_null( $this->getCommissionAmount() ) && true == is_null( $this->getCommissionPercent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_percent', 'Commission percent is required.' ) );
		}

		if( false == is_null( $this->getCommissionPercent() ) && ( false == is_numeric( $this->getCommissionPercent() ) || 0 >= $this->getCommissionPercent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Valid commission percent is required.' ) );
		}
		if( true == is_numeric( $this->getCommissionPercent() ) && 100 < ( $this->getCommissionPercent() * 100 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'commission_percent', 'Commission percent should not be more than 100%.' ) );
		}
		return $boolIsValid;
	}

	public function valTransactionCommissionPercent() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getTransactionCommissionPercent() ) || 0 > $this->getTransactionCommissionPercent() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Valid transactional commission percent is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFrontLoadPosts() {
		$boolIsValid = true;

		if( true == is_null( $this->getFrontLoadPosts() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'front_load_posts', 'Front load count is required.' ) );
		} elseif( 0 >= $this->getFrontLoadPosts() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'front_load_posts', 'Front load count should be greater than zero.' ) );
		}

		return $boolIsValid;
	}

	public function valNewFrontLoadPosts() {
		$boolIsValid = true;

		if( 0 > $this->getFrontLoadPosts() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'front_load_posts', 'Front load count should be greater than zero..' ) );
		}

		return $boolIsValid;
	}

	public function valFrontLoadAmount() {

		$boolIsValid = true;

		if( true == is_null( $this->getFrontLoadAmount() ) && true == is_null( $this->getFrontLoadPercentage() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'front_load_amount', 'Front load amount or percentage is required.' ) );
		}

		if( false == is_null( $this->getFrontLoadAmount() ) && ( false == CValidation::checkCurrency( $this->getFrontLoadAmount() ) || 0 >= $this->getFrontLoadAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Front load amount should be greater than zero.' ) );
		}

		return $boolIsValid;
	}

	public function valNewFrontLoadAmount() {

		$boolIsValid = true;

		if( false == is_null( $this->getFrontLoadAmount() ) && ( false == CValidation::checkCurrency( $this->getFrontLoadAmount() ) || 0 > $this->getFrontLoadAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Valid front load amount is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFrontLoadPercentage() {
		$boolIsValid = true;

		if( true == is_null( $this->getFrontLoadPercentage() ) && true == is_null( $this->getFrontLoadAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'front_load_percentage', 'Front load amount or percentage is required.' ) );
 		}

		if( false == is_null( $this->getFrontLoadPercentage() ) && ( false == is_numeric( $this->getFrontLoadPercentage() ) || 0 >= $this->getFrontLoadPercentage() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'front_load_percentage', 'Front load percentage should be greater than zero.' ) );
		}

		if( true == is_numeric( $this->getFrontLoadPercentage() ) && 100 < ( $this->getFrontLoadPercentage() * 100 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'front_load_percentage', 'Front load percent should not be more than 100%.' ) );
		}
		return $boolIsValid;
	}

	public function valNewFrontLoadPercentage() {
		$boolIsValid = true;

		if( false == is_null( $this->getFrontLoadPercentage() ) && ( false == is_numeric( $this->getFrontLoadPercentage() ) || 0 > $this->getFrontLoadPercentage() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Valid front load percent is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOngoingFrontLoadPosts() {
		$boolIsValid = true;

		if( 0 > $this->getOngoingFrontLoadPosts() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Valid ongoing front load count is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOngoingFrontLoadAmount() {

		$boolIsValid = true;

		if( false == is_null( $this->getOngoingFrontLoadAmount() ) && ( false == CValidation::checkCurrency( $this->getOngoingFrontLoadAmount() ) || 0 > $this->getongoingFrontLoadAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Valid ongoing front load amount is required.' ) );
		}

		return $boolIsValid;
	}

	public function valOngoingFrontLoadPercentage() {
		$boolIsValid = true;

		if( false == is_null( $this->getOngoingFrontLoadPercent() ) && ( false == is_numeric( $this->getOngoingFrontLoadPercent() ) || 0 > $this->getOngoingFrontLoadPercent() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Valid ongoing front load percent is required.' ) );
		}

		return $boolIsValid;
	}

	public function validateUnitCommissionAmount() {

		$boolIsValid = true;

		if( true == is_null( $this->getUnitCommissionAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unit_commission_amount', 'Unit commission amount is required.' ) );
		}

		if( false == is_null( $this->getCommissionAmount() ) && ( false == CValidation::checkCurrency( $this->getCommissionAmount() ) || false == is_numeric( $this->getCommissionAmount() ) || 0 == $this->getCommissionAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Valid unit commission amount is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSetupCommissionPercent() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getSetupCommissionPercent() ) || 0 > $this->getSetupCommissionPercent() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Valid setup commission percent is required.' ) );
		}

		if( true == is_numeric( $this->getSetupCommissionPercent() ) && 0 < $this->getSetupCommissionPercent() && 100 < ( $this->getSetupCommissionPercent() * 100 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'setup_commission_percent', 'Setup percent should not be more than 100%.' ) );
		}
		return $boolIsValid;
	}

	public function valContractBasedFrontLoadPosts() {
		$boolIsValid = true;

		if( true == is_numeric( $this->getFrontLoadPosts() ) && 0 > $this->getFrontLoadPosts() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Valid front load count is required.' ) );
		}

		return $boolIsValid;
	}

	public function valContractBasedFrontLoadPercentage() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getFrontLoadPercentage() ) || 0 > $this->getFrontLoadPercentage() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Valid Front load percent is required.' ) );
		}
		if( true == is_numeric( $this->getFrontLoadPercentage() ) && 0 < $this->getFrontLoadPercentage() && 100 < ( $this->getFrontLoadPercentage() * 100 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'setup_commission_percent', 'Front load percent should not be more than 100%.' ) );
		}

		return $boolIsValid;
	}

	public function valContractBasedCommissionPercent() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getCommissionPercent() ) || 0 > $this->getCommissionPercent() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Valid commission percent is required.' ) );
		}
		if( true == is_numeric( $this->getCommissionPercent() ) && 0 < $this->getCommissionPercent() && 100 < ( $this->getCommissionPercent() * 100 ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'setup_commission_percent', 'Commission percent should not be more than 100%.' ) );
		}

		return $boolIsValid;
	}

	/*****************************************************************************************
	 ********************************  V A L I D A T I O N ************************************
	 *  We are having two groups here for validation:- 1. Amount/Percent 2. Front Load Values *
	 *  Conditions:-																		 *
	 *  1. Either of the group details should be filled.									 *
	 *  2. If Amount is entered then percent is not mandatory( or viceversa).				 *
	 *  3. Count is compulsary in 2nd group and One of Amount and Percentage is mandatory.	 *
	 *  No Problem with all details.														 *
	 *****************************************************************************************/

	public function validateCommissionRate() {
		$boolIsNullGroup1 = false;
		$boolIsNullGroup2 = false;

		if( true == is_null( $this->getCommissionAmount() ) && true == is_null( $this->getCommissionPercent() ) && ( 0 >= $this->getCommissionAmount() || 0 >= $this->getCommissionPercent() ) ) {
				$boolIsNullGroup1 = true;
		}

		if( true == is_null( $this->getFrontLoadPosts() ) && true == is_null( $this->getFrontLoadAmount() ) && true == is_null( $this->getFrontLoadPercentage() ) && ( 0 >= $this->getFrontLoadPosts() || 0 >= $this->getFrontLoadAmount() || 0 >= $this->getFrontLoadPercentage() ) ) {
				$boolIsNullGroup2 = true;
		}

		if( true == $boolIsNullGroup1 && true == $boolIsNullGroup2 ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, true, 'Amount or percent is required.' ) );
			$boolIsValid = false;

		} elseif( false == $boolIsNullGroup1 && false == $boolIsNullGroup2 ) {

			$boolIsValid = $this->valCommissionAmount();
			$boolIsValid &= $this->valCommissionPercent();

			$boolIsValid &= $this->valFrontLoadAmount();

		} elseif( true == $boolIsNullGroup1 && false == $boolIsNullGroup2 ) {

			$boolIsValid = $this->valFrontLoadPosts();
			$boolIsValid &= $this->valFrontLoadAmount();
			$boolIsValid &= $this->valFrontLoadPercentage();

		} elseif( false == $boolIsNullGroup1 && true == $boolIsNullGroup2 ) {

			$boolIsValid = $this->valCommissionAmount();
			$boolIsValid &= $this->valCommissionPercent();
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->validateCommissionRate();
				$boolIsValid &= $this->valChargeCodeId( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid = $this->validateCommissionRate();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function validateNewCommissionRate( $strAction ) {

		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valContractBasedCommissionPercent();
				$boolIsValid &= $this->valContractBasedFrontLoadPosts();
				$boolIsValid &= $this->valSetupCommissionPercent();
				$boolIsValid &= $this->valContractBasedFrontLoadPercentage();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valContractBasedCommissionPercent();
				$boolIsValid &= $this->valContractBasedFrontLoadPosts();
				$boolIsValid &= $this->valSetupCommissionPercent();
				$boolIsValid &= $this->valContractBasedFrontLoadPercentage();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function fetchChargeCode( $objDatabase ) {
		return \Psi\Eos\Admin\CChargeCodes::createService()->fetchChargeCodeById( $this->getChargeCodeId(), $objDatabase );
	}

	public function fetchCommissionRateAssociations( $objDatabase ) {
		return CCommissionRateAssociations::createService()->fetchCommissionRateAssociationsByCommissionRateId( $this->getId(), $objDatabase );
	}

	/**
	 * Create Functions
	 */

	public function createCommission() {

		$objCommission = new CCommission();

		$objCommission->setCid( $this->getCid() );
		$objCommission->setAccountId( $this->getAccountId() );
		$objCommission->setCommissionRecipientId( $this->getCommissionRecipientId() );
		$objCommission->setCommissionStructureId( $this->getCommissionStructureId() );
		$objCommission->setCommissionRateId( $this->getId() );
		$objCommission->setChargeCodeId( $this->getChargeCodeId() );
		$objCommission->setCommissionDatetime( date( 'm/d/Y' ) );

		return $objCommission;
	}

	public function createCommissionRateAssociation( $intCid, $intDefaultPeriodMonths, $intAccountId = NULL, $intContractId = NULL ) {

		$strStartDate = date( 'm/d/Y', time() );
		$intStartTime = strtotime( $strStartDate . ' +' . $intDefaultPeriodMonths . 'months' );
		$strEndDate   = date( 'm/d/Y', mktime( 0, 0, 0, date( 'm', $intStartTime ), date( 'j', $intStartTime ) - 1, date( 'Y', $intStartTime ) ) );

		$objCommissionRateAssociation = new CCommissionRateAssociation();
		$objCommissionRateAssociation->setDefaults();
		$objCommissionRateAssociation->setCommissionRateId( $this->getId() );
		$objCommissionRateAssociation->setCommissionStructureId( $this->getCommissionStructureId() );
		$objCommissionRateAssociation->setCid( $intCid );
		$objCommissionRateAssociation->setAccountId( $intAccountId );
		$objCommissionRateAssociation->setContractId( $intContractId );
		$objCommissionRateAssociation->setStartDate( $strStartDate );
		$objCommissionRateAssociation->setEndDate( $strEndDate );

		return $objCommissionRateAssociation;
	}

	/**
	 * Other Functions
	 */

	public function applyRequestForm( $arrmixRequestForm, $arrmixFormFields = NULL ) {

		if( true == isset( $arrmixRequestForm['commission_percent'] ) ) {
			$arrmixRequestForm['commission_percent']	= ( 0 < $arrmixRequestForm['commission_percent'] ) ? $arrmixRequestForm['commission_percent'] / 100 : NULL;
		}
		if( true == isset( $arrmixRequestForm['front_load_percentage'] ) ) {
			$arrmixRequestForm['front_load_percentage'] = ( 0 < $arrmixRequestForm['front_load_percentage'] / 100 ) ? $arrmixRequestForm['front_load_percentage'] / 100 : NULL;
		}
		if( true == isset( $arrmixRequestForm['ongoing_front_load_percent'] ) ) {
			$arrmixRequestForm['ongoing_front_load_percent'] = ( 0 < $arrmixRequestForm['ongoing_front_load_percent'] / 100 ) ? $arrmixRequestForm['ongoing_front_load_percent'] / 100 : NULL;
		}
		if( true == isset( $arrmixRequestForm['setup_commission_percent'] ) ) {
			$arrmixRequestForm['setup_commission_percent']	= ( 0 < $arrmixRequestForm['setup_commission_percent'] ) ? $arrmixRequestForm['setup_commission_percent'] / 100 : NULL;
		}

		parent::applyRequestForm( $arrmixRequestForm, $arrmixFormFields );

		return;
	}

	public function delete( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );

		return $this->update( $intUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>