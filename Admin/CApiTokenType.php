<?php

class CApiTokenType extends CBaseApiTokenType {

	const OAUTH_ACCESS_TOKEN                = 1;
	const SITETABLET_REFRESH_TOKEN          = 2;
	const ENTRATA_MAINTENANCE_REFRESH_TOKEN = 3;
	const EXTERNAL_REFRESH_TOKEN            = 4;
	const DOCSCAN_REFRESH_TOKEN             = 5;
	const AUTH_CODE                         = 6;

	public static $c_arrintAllApiTokenTypes = [
		self::OAUTH_ACCESS_TOKEN                => self::OAUTH_ACCESS_TOKEN,
		self::SITETABLET_REFRESH_TOKEN          => self::SITETABLET_REFRESH_TOKEN,
		self::ENTRATA_MAINTENANCE_REFRESH_TOKEN => self::ENTRATA_MAINTENANCE_REFRESH_TOKEN,
		self::EXTERNAL_REFRESH_TOKEN            => self::EXTERNAL_REFRESH_TOKEN,
		self::DOCSCAN_REFRESH_TOKEN             => self::DOCSCAN_REFRESH_TOKEN,
		self::AUTH_CODE                         => self::AUTH_CODE
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>