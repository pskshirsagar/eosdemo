<?php

class CSurveyTask extends CBaseSurveyTask {

	protected $m_intTaskStatusId;

	protected $m_strDueDate;
	protected $m_strCompletedOn;
	protected $m_strTaskDescription;
	protected $m_intTaskReferenceId;
	protected $m_boolYearly;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['due_date'] ) ) 				$this->setDueDate( $arrmixValues['due_date'] );
		if( true == isset( $arrmixValues['completed_on'] ) ) 			$this->setCompletedOn( $arrmixValues['completed_on'] );
		if( true == isset( $arrmixValues['task_status_id'] ) ) 		    $this->setTaskStatusId( $arrmixValues['task_status_id'] );
		if( true == isset( $arrmixValues['task_description'] ) ) 		$this->setTaskDescription( $arrmixValues['task_description'] );
		if( true == isset( $arrmixValues['task_reference_id'] ) ) 		$this->setTaskReferenceId( $arrmixValues['task_reference_id'] );
		if( true == isset( $arrmixValues['is_yearly'] ) )               $this->setYearly( $arrmixValues['is_yearly'] );
	}

	//	 Setter functions

	public function setTaskDescription( $strTaskDescription ) {
		$this->m_strTaskDescription = $strTaskDescription;
	}

	public function setTaskStatusId( $intTaskStatusid ) {
		$this->m_intTaskStatusId = $intTaskStatusid;
	}

	public function setDueDate( $strDueDate ) {
		$this->m_strDueDate = $strDueDate;
	}

	public function setCompletedOn( $strCompletedOn ) {
		$this->m_strCompletedOn = $strCompletedOn;
	}

	public function setTaskReferenceId( $intTaskReferenceId ) {
		$this->m_intTaskReferenceId = $intTaskReferenceId;
	}

	public function setYearly( $boolYearly ) {
		return $this->m_boolYearly = $boolYearly;
	}

	// 	Setter functions

	public function getTaskDescription() {
		return $this->m_strTaskDescription;
	}

	public function getTaskStatusId() {
		return $this->m_intTaskStatusId;
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function getCompletedOn() {
		return $this->m_strCompletedOn;
	}

	public function getTaskReferenceId() {
		return $this->m_intTaskReferenceId;
	}

	public function getYearly() {
		return $this->m_boolYearly;
	}

	public function valSurveyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSurveyId() ) || false == is_numeric( $this->getSurveyId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'survey_id', 'Invalid Survey Id.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaskId() ) || false == is_numeric( $this->getTaskId() ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, 'task_id', 'Invalid Task Id.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTaskId();
				$boolIsValid &= $this->valSurveyId();

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>