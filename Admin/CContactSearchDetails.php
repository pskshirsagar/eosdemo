<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CContactSearchDetails
 * Do not add any new functions to this class.
 */

class CContactSearchDetails extends CBaseContactSearchDetails {

	public static function fetchContactSearchDetailsByReferenceIdByContactSearchTypeIdBySearchString( $intReferenceId, $intContactSearchTypeId, $strSearchString, $objAdminDatabase ) {
		if( true == empty( $intReferenceId ) || true == empty( $intContactSearchTypeId ) || true == empty( $strSearchString ) ) {
			return NULL;
		}

		$strSql = ' SELECT
		 				*
					FROM
						contact_search_details csd
					WHERE
						csd.reference_id = ' . ( int ) $intReferenceId . '
						AND csd.contact_search_type_id = ' . ( int ) $intContactSearchTypeId . '
						AND csd.search_string = \'' . $strSearchString . '\'';

		return self::fetchContactSearchDetail( $strSql, $objAdminDatabase );
	}

}
?>