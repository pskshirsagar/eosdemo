<?php

class CTaskNoteType extends CBaseTaskNoteType {

	const RCA_PRECAUTIONARY_MEASUREMENT            = 1;
	const RCA_FOLLOW_UP                            = 2;
	const SUPPORT_CALL_REQUEST                     = 3;
	const SUPPORT_EMAIL_REQUEST                    = 4;
	const SUPPORT_LIVE_CHAT_REQUEST                = 5;
	const CORRECTIVE_ACTION_TAKEN                  = 6;
	const HELPDESK_ESCALATION_EMAILS               = 7;
	const BLOCKER_NOTE                             = 8;
	const TASK_NOTE_FOR_BOUNCE_EMAIL               = 9;
	const TASK_REASONING_NOTE                      = 10;
	const TEST_EXECUTION_NOTE                      = 11;
	const TASK_MAINTENANCE_RESONING_NOTE           = 12;
	const TASK_ASSIGNMENT_UPDATE                   = 13;
	const TASK_NOTE_FOR_ESCALATION_EMAIL_DELIVERED = 14;
	const SUPPORT_VIDEO_CALL_REQUEST               = 15;
	const TASK_SMS                                 = 16;
	const TASK_UAT_COMPLETED                       = 17;
	const PERSONAL_INDENTIFIABLE_INFORMATION       = 18;

	public static $c_arrintTaskNoteTypeIds = [
		self::PERSONAL_INDENTIFIABLE_INFORMATION,
		self::TASK_ASSIGNMENT_UPDATE,
		self::TASK_UAT_COMPLETED
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}

?>