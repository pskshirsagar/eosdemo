<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemBudgets
 * Do not add any new functions to this class.
 */

class CSemBudgets extends CBaseSemBudgets {

	public static function fetchLastPaidClickedPropertyIdByPropertyIdsBySemKeywordIdByIsDemo( $arrintPropertyIds, $intSemKeywordId, $boolIsDemo, $objDatabase, $boolAllowZeroOrNegativeBudgets = false ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						property_id,
						CASE
							WHEN max_transaction_datetime IS NULL
							THEN \'1/1/1980\'
							ELSE max_transaction_datetime
						END as max_transaction_datetime

						FROM ( SELECT
								DISTINCT ON ( sb.property_id )
								sb.property_id,
								( SELECT max( transaction_datetime ) FROM sem_transactions WHERE property_id = sb.property_id AND is_internal = 0 ) as max_transaction_datetime
							FROM
								sem_keywords sk
								JOIN sem_ad_groups sag ON ( sk.sem_ad_group_id = sag.id )
								JOIN sem_ad_group_associations saga ON ( saga.sem_ad_group_id = sag.id AND saga.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ))
								JOIN sem_budgets sb ON ( saga.property_id = sb.property_id )
								JOIN sem_settings ss ON ( saga.property_id = ss.property_id )
								JOIN sem_property_details spd ON ( ss.property_id = spd.property_id )
								JOIN sem_keyword_associations ska ON ( sk.id = ska.sem_keyword_id AND saga.property_id = ska.property_id )
							WHERE
								sk.id = ' . ( int ) $intSemKeywordId . '
								AND ska.id IS NOT NULL
								AND sb.budget_date = DATE_TRUNC( \'month\', NOW() ) ';

		if( false == $boolAllowZeroOrNegativeBudgets ) {
			$strSql .= ' AND sb.daily_remaining_budget > 0 ';
		} else {
			$strSql .= ' AND sb.daily_remaining_budget <= 0 ';
		}

		if( true == $boolIsDemo ) {
			$strSql .= ' AND spd.sem_property_status_type_id = ' . ( int ) CSemPropertyStatusType::DEMO . ' ';
		} else {
			$strSql .= ' AND spd.sem_property_status_type_id = ' . ( int ) CSemPropertyStatusType::ENABLED . ' ';
		}

		$strSql .= ' ) AS SUB_QUERY1

					ORDER BY max_transaction_datetime ASC
					LIMIT 1';

		$arrstrResults = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrResults ) && true == isset ( $arrstrResults[0]['property_id'] ) ) {
			return $arrstrResults[0]['property_id'];
		} else {
			return NULL;
		}
	}

	public static function fetchLastOrganicClickedPropertyIdByPropertyIdsByIsDemo( $arrintPropertyIds, $boolIsDemo, $objDatabase ) {

		if( false == valArr( $arrintPropertyIds ) ) return NULL;

		$strSql = 'SELECT
						property_id,
						CASE
							WHEN max_transaction_datetime IS NULL
							THEN \'1/1/1980\'
							ELSE max_transaction_datetime
						END as max_transaction_datetime

						FROM ( SELECT
								sb.property_id,
								( SELECT max( transaction_datetime ) FROM sem_transactions WHERE property_id = sb.property_id AND is_internal = 1 ) as max_transaction_datetime
							FROM
								sem_budgets sb,
								sem_settings ss,
								sem_property_details spd
							WHERE
								sb.property_id = ss.property_id
								AND ss.property_id = spd.property_id
								AND ss.organic_feature_fee > 0
								AND ss.organic_activated_on IS NOT NULL
								AND ss.organic_disabled_on IS NULL ';

		if( true == $boolIsDemo ) {
			$strSql .= ' AND spd.sem_property_status_type_id = ' . CSemPropertyStatusType::DEMO . ' ';
		}

		$strSql .= '
								AND sb.budget_date = DATE_TRUNC( \'month\', NOW() )
								AND sb.daily_remaining_budget > 0
								AND sb.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ) AS SUB_QUERY1

					ORDER BY max_transaction_datetime ASC
					LIMIT 1';

		$arrstrResults = fetchData( $strSql, $objDatabase );

		if( true == valArr( $arrstrResults ) && true == isset ( $arrstrResults[0]['property_id'] ) ) {
			return $arrstrResults[0]['property_id'];
		} else {
			return NULL;
		}
	}

	public static function fetchSemBudgetsByPropertyIds( $arrintPropertyIds, $objDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		$strSql = 'SELECT * FROM sem_budgets WHERE property_id IN ( ' . ( string ) implode( ',', $arrintPropertyIds ) . ' )';
		return self::fetchSemBudgets( $strSql, $objDatabase );
	}

	public static function fetchSemBudgetsByCurrentMonth( $strDate, $objDatabase ) {
		$strSql = 'SELECT * FROM sem_budgets WHERE  to_char( budget_date, \'MM/DD/YYYY\' )::date = \'' . $strDate . '\'::date ';
		return self::fetchSemBudgets( $strSql, $objDatabase );
	}

	public static function fetchSemBudgetByPropertyIdByCurrentMonth( $intPropertyId, $strDate, $objDatabase ) {
		$strSql = 'SELECT * FROM sem_budgets WHERE property_id = ' . ( int ) $intPropertyId . ' AND to_char( budget_date, \'MM/DD/YYYY\' )::date = \'' . $strDate . '\'::date LIMIT 1 ';
		return self::fetchSemBudget( $strSql, $objDatabase );
	}

}
?>