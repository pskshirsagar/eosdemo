<?php

use Psi\Eos\Admin\CCompanyPayments;

use Psi\Libraries\ExternalPrinceWrapper\CPrinceFactory;

class CDeposit extends CBaseDeposit {

	protected $m_arrobjCompanyPayments;
	protected $m_intSelected;

	protected $m_arrobjClients;
	protected $m_arrobjAccounts;

	/**
	 * Get Functions
	 *
	 */

	public function getCompanyPayments() {
		return $this->m_arrobjCompanyPayments;
	}

	public function getSelected() {
		return $this->m_intSelected;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setDefaults() {

		$this->setDepositDate( date( 'm/d/Y' ) );
		$this->setDepositAmount( 0 );
	}

	public function setCompanyPayments( $arrobjCompanyPayments ) {
		$this->m_arrobjCompanyPayments = $arrobjCompanyPayments;
	}

	public function setSelected( $intSelected ) {
		$this->m_intSelected = $intSelected;
	}

	/**
	 * Fetch Functions
	 *
	 */

	 public function fetchCompanyPayments( $objDatabase, $boolIsViewDeposit = false ) {

		$this->m_arrobjCompanyPayments = CCompanyPayments::createService()->fetchCompanyPaymentsByDepositIdOrderByPaymentDateTime( $this->m_intId, $objDatabase, $boolIsViewDeposit );

		return $this->m_arrobjCompanyPayments;
	 }

	/**
	 * Validation Functions
	 *
	 */

	public function valDepositDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getDepositDate() ) || false == CValidation::validateDate( $this->getDepositDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deposit_date', 'A valid deposit date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valChartOfAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getChartOfAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deposit_date', 'Deposit account is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDepositTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDepositTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deposit_date', 'Deposit type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDepositAmount() {
		$boolIsValid = true;

		if( true == is_null( $this->getDepositAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'deposit_amount', 'Deposit amount is required.' ) );
			return false;
		}

		return $boolIsValid;
	}

	public function valExportBatchIdOnDelete() {
		$boolIsValid = true;

		if( false == is_null( $this->getExportBatchId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'export_batch_id', 'A deposit cannot be deleted after it has been exported.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDepositDate();
				$boolIsValid &= $this->valChartOfAccountId();
				$boolIsValid &= $this->valDepositTypeId();
				$boolIsValid &= $this->valDepositAmount();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valExportBatchIdOnDelete();
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public static function buildAndInsertDeposit( $arrintCompanyPaymentIds, $intDepositTypeId, $intExportBatchTypeId, $objDatabase, $intEftBatchId = NULL, $strDepositDate = NULL, $fltDepositAmount = 0 ) {

		if( true == valArr( $arrintCompanyPaymentIds ) ) {

			if( 0 == $fltDepositAmount ) {
				$fltDepositAmount = CCompanyPayments::createService()->fetchPaymentsSumByCompanyPaymentIds( $arrintCompanyPaymentIds, $objDatabase );
			}

			switch( $intExportBatchTypeId ) {

				case CExportBatchType::RESIDENT_INSURE_MARKEL:
					$intChartOfAccountId = CChartOfAccount::PSIA_OPERATING_MARKEL;
					break;

				case CExportBatchType::RESIDENT_INSURE_KEMPER:
					$intChartOfAccountId = CChartOfAccount::PSIA_OPERATING_KEMPER;
					break;

				case CExportBatchType::RESIDENT_INSURE_QBE:
					$intChartOfAccountId = CChartOfAccount::PSIA_OPERATING_QBE;
					break;

				case CExportBatchType::RESIDENT_INSURE_IDTHEFT:
					$intChartOfAccountId = CChartOfAccount::PSIA_OPERATING_IDTHEFT;
					break;

				default:
					switch( $intDepositTypeId ) {
						case CDepositType::MANUAL:
							$intChartOfAccountId = CChartOfAccount::OPERATING_ZIONS; // Operating Zions
							break;

						case CDepositType::DI:
						case CDepositType::AE:
						case CDepositType::VIMC:
						case CDepositType::ACH:
							$intChartOfAccountId = CChartOfAccount::REVENUE_ZIONS; // Revenue Zions
							break;

						default:
							$intChartOfAccountId = CChartOfAccount::OPERATING_ZIONS;
							break;
					}
					break;
			}

			$objDeposit = new CDeposit();

			$objDeposit->setDefaults();
			$objDeposit->setDepositDate( ( false == is_null( $strDepositDate ) ? $strDepositDate : date( 'm/d/Y' ) ) );
			$objDeposit->setDepositTypeId( $intDepositTypeId );
			$objDeposit->setDepositAmount( $fltDepositAmount );
			$objDeposit->setDepositMemo( 'Auto generated during monthly payment posting.' . ( false == is_null( $intEftBatchId ) ? ' Eft Batch ID: ' . ( int ) $intEftBatchId : '' ) );
			$objDeposit->setChartOfAccountId( $intChartOfAccountId );
			$objDeposit->setExportBatchTypeId( $intExportBatchTypeId );
			$objDeposit->setRemotePrimaryKey( $intEftBatchId );

			if( true == $objDeposit->insert( SYSTEM_USER_ID, $objDatabase ) ) {
				$strSql = 'UPDATE company_payments SET deposit_id = ' . $objDeposit->getId() . ' WHERE id IN ( ' . implode( ', ', $arrintCompanyPaymentIds ) . ' )';
				fetchData( $strSql, $objDatabase );
			} else {
				return false;
			}
		}

		return $objDeposit;
	}

	public function generatePdfDepositSlip( $objDatabase, $strCurrencySymbol ) {
		$intCids = array_keys( rekeyObjects( 'Cid', $this->m_arrobjCompanyPayments ) );
		$intAccountIds = array_keys( rekeyObjects( 'AccountId', $this->m_arrobjCompanyPayments ) );

		$this->m_arrobjClients = \Psi\Eos\Admin\CClients::createService()->fetchClientsByIds( $intCids, $objDatabase );
		$this->m_arrobjAccounts = \Psi\Eos\Admin\CAccounts::createService()->fetchAccountsByIds( $intAccountIds, $objDatabase );

		$strHtmlContent = $this->buildHtmlDeposit( $strCurrencySymbol );

		$strTitle = 'Deposit Slip ID#' . $this->getId();

		$objPdfDocument = CPrinceFactory::createPrince();
		$objPdfDocument->setPDFTitle( $strTitle );
		$objPdfDocument->setBaseURL( PATH_COMMON );
		$objPdfDocument->setHTML( true );
		$objPdfDocument->setJavaScript( true );
		header( 'Content-type: application/pdf' );

		$objPdfDocument->convert_string_to_passthru( $strHtmlContent );
	}

	public function buildCompanyPaymentAchReturnDeposits( $arrintCompanyPaymentIds, $intUserId, $objAdminDatabase ) {

		if( false == valArr( $arrintCompanyPaymentIds ) ) {
			return;
		}

		$this->generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCompanyPaymentIds, CExportBatchType::RESIDENT_INSURE_MARKEL, CChartOfAccount::PSIA_OPERATING_MARKEL, [ CAccountType::INSURANCE_MARKEL ], NULL, $objAdminDatabase );
		$this->generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCompanyPaymentIds, CExportBatchType::RESIDENT_INSURE_KEMPER, CChartOfAccount::PSIA_OPERATING_KEMPER, [ CAccountType::INSURANCE_KEMPER ], NULL, $objAdminDatabase );
		$this->generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCompanyPaymentIds, CExportBatchType::RESIDENT_INSURE_QBE, CChartOfAccount::PSIA_OPERATING_QBE, [ CAccountType::INSURANCE_QBE ], NULL, $objAdminDatabase );
		$this->generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCompanyPaymentIds, CExportBatchType::RESIDENT_INSURE_IDTHEFT, CChartOfAccount::PSIA_OPERATING_IDTHEFT, [ CAccountType::INSURANCE_IDTHEFT ], NULL, $objAdminDatabase );
		$this->generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCompanyPaymentIds, CExportBatchType::MASTER_POLICY, CChartOfAccount::MASTER_POLICY, [ CAccountType::MASTER_POLICY ], NULL, $objAdminDatabase );
		$this->generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCompanyPaymentIds, CExportBatchType::PROPERTY_SOLUTIONS, CChartOfAccount::REVENUE_ZIONS, NULL, CAccountType::$c_arrintInsuranceAccountTypeIds, $objAdminDatabase );

	}

	public function generateCompanyPaymentDepositByAccountIds( $intUserId, $arrintCollapsedCompanyPaymentIds, $intExportBatchTypeId, $intChartOfAccountId, $arrintAccountTypeIds, $arrintBannedAccountTypeIds, $objAdminDatabase ) {
		$strSql = 'SELECT
						cp.*
					FROM
						company_payments cp
						JOIN accounts a ON ( cp.account_id = a.id AND cp.cid = a.cid )
					WHERE
						cp.id IN ( ' . implode( ',', $arrintCollapsedCompanyPaymentIds ) . ' )
						' . ( ( true == valArr( $arrintAccountTypeIds ) ) ? ' AND a.account_type_id IN ( ' . implode( ',', $arrintAccountTypeIds ) . ' ) ' : '' ) . '
						' . ( ( true == valArr( $arrintBannedAccountTypeIds ) ) ? ' AND a.account_type_id NOT IN ( ' . implode( ',', $arrintBannedAccountTypeIds ) . ' ) ' : '' ) . '
						AND cp.returned_on IS NOT NULL
						AND cp.deposit_id IS NULL;';

		$arrobjCompanyPayments = CCompanyPayments::createService()->fetchCompanyPayments( $strSql, $objAdminDatabase );

		if( true == valArr( $arrobjCompanyPayments ) ) {
			$objDeposit = \Psi\Eos\Admin\CDeposits::createService()->createDeposit();
			$objDeposit->setCompanyPayments( $arrobjCompanyPayments );

			// Calculate the total on the deposit
			$fltDepositTotal = 0;

			foreach( $arrobjCompanyPayments as $objCompanyPayment ) {
				$fltDepositTotal += $objCompanyPayment->getPaymentAmount();
			}

			$objDeposit->setDepositAmount( $fltDepositTotal );
			$objDeposit->setExportBatchTypeId( $intExportBatchTypeId );
			$objDeposit->setChartOfAccountId( $intChartOfAccountId );
			$objDeposit->setDepositTypeId( CDepositType::ACH );
			$objDeposit->setDepositDate( date( 'm/d/Y' ) );
			$objDeposit->setDepositMemo( 'Automated Deposit Creation From Nacha Return File ' . $this->getId() );

			if( false == $objDeposit->insert( $intUserId, $objAdminDatabase ) ) {
				return true;
			}

			foreach( $arrobjCompanyPayments as $objCompanyPayment ) {
				$objCompanyPayment->setDepositId( $objDeposit->getId(), $objAdminDatabase );
				if( false == $objCompanyPayment->update( $intUserId, $objAdminDatabase ) ) {
					return true;
				}
			}
		}

		return true;
	}

	public function buildHtmlDeposit( $strCurrencySymbol ) {

		$arrmixTemplateParameters = [];
		$arrmixTemplateParameters['company_payments']	= $this->m_arrobjCompanyPayments;
		$arrmixTemplateParameters['clients']			= $this->m_arrobjClients;
		$arrmixTemplateParameters['accounts']			= $this->m_arrobjAccounts;
		$arrmixTemplateParameters['currency_symbol']	= $strCurrencySymbol;
		$arrmixTemplateParameters['title']				= $strTitle;
		$arrmixTemplateParameters['id']					= $this->getId();
		$arrmixTemplateParameters['date']				= $this->m_strDepositDate;
		$arrmixTemplateParameters['logo']				= PATH_WWW_ROOT . '/Admin/images/logos/' . CONFIG_ENTRATA_LOGO_PREFIX . 'invoice_header_simple2.jpg';

		$objRenderTemplate	= new CRenderTemplate( 'accounting/deposits/view_deposit_slip.tpl', PATH_INTERFACES_CLIENT_ADMIN, false, $arrmixTemplateParameters );
		$strHtmlContent		= $objRenderTemplate->fetchTemplate( 'accounting/deposits/view_deposit_slip.tpl' );

		return $strHtmlContent;
	}

}

?>