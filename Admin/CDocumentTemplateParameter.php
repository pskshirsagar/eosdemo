<?php

class CDocumentTemplateParameter extends CBaseDocumentTemplateParameter {

	/**
	 * Create Functions
	 */

	public function createDocumentParameter( $objDocumentComponent ) {

		$objDocumentParameter = new CDocumentParameter();

		$objDocumentParameter->setCid( $objDocumentComponent->getCid() );
		$objDocumentParameter->setDocumentId( $objDocumentComponent->getDocumentId() );
		$objDocumentParameter->setDocumentComponentId( $objDocumentComponent->getId() );
		$objDocumentParameter->setKey( $this->getKey() );
		$objDocumentParameter->setValue( $this->getValue() );
		$objDocumentParameter->setOrderNum( $this->getOrderNum() );
		$objDocumentParameter->setIsModifiable( $this->getIsModifiable() );
		$objDocumentParameter->setIsRemovable( $this->getIsRemovable() );

		return $objDocumentParameter;
	}
}
?>