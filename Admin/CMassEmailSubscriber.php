<?php

class CMassEmailSubscriber extends CBaseMassEmailSubscriber {

	protected $m_strEmailAddress;

	/**
	 * Get Functions
	 *
	 */

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['email_address'] ) )	$this->setEmailAddress( $arrstrValues['email_address'] );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>