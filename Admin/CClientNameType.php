<?php

class CClientNameType extends CBaseClientNameType {

	const LEGAL_ENTITY_NAME = 1;
	const MARKETING_NAME = 2;
	const FORMER_NAME = 3;
	const AUTO_CONTRACT_NAME = 4;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>