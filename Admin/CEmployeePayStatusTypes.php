<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeePayStatusTypes
 * Do not add any new functions to this class.
 */

class CEmployeePayStatusTypes extends CBaseEmployeePayStatusTypes {

	public static function fetchEmployeePayStatusTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmployeePayStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEmployeePayStatusType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEmployeePayStatusType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedEmployeePayStatusTypes( $objDatabase, $arrintEmployeePayStatusTypeIds = NULL ) {

		$strWhereCondition = '';

		if( true == valArr( $arrintEmployeePayStatusTypeIds ) ) {
			$strWhereCondition = ' AND id IN( ' . implode( ',', $arrintEmployeePayStatusTypeIds ) . ' )';
		}

		$strSql = 'SELECT * FROM employee_pay_status_types WHERE is_published IS TRUE AND is_reimbursement IS TRUE' . $strWhereCondition;
		return self::fetchEmployeePayStatusTypes( $strSql, $objDatabase );
	}

}
?>