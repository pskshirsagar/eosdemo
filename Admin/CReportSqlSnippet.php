<?php

class CReportSqlSnippet extends CBaseReportSqlSnippet {

	const LIMIT_FOR_SQL_SNIPPET_NAME 	= 50;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == empty( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Snippet name is required. ' ) );
		} elseif( $this->m_strName != strip_tags( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Valid snippet name is required. ' ) );
			return $boolIsValid;
		}

		if( isset( $this->m_strName[self::LIMIT_FOR_SQL_SNIPPET_NAME] ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Snippet name should not be greater than ' . self::LIMIT_FOR_SQL_SNIPPET_NAME . ' characters. ' ) );
			return $boolIsValid;
		}

		if( true == $boolIsValid && true == isset( $objDatabase ) ) {
			$strSql 	= ' WHERE deleted_by IS NULL AND name ILIKE E\'' . $this->getName() . '\'' . ( 0 < $this->getId() ? ' AND id <> ' . $this->m_intId : '' );
			$intCount 	= CReportSqlSnippets::fetchRowCount( $strSql, 'report_sql_snippets', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Snippet name ' . $this->m_strName . ' already exist. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valQuery() {
		$boolIsValid = true;

		if( true == empty( $this->m_strQuery ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'snippet_sql', 'Sql is required.' ) );
		} else {
			if( 0 == \Psi\CStringService::singleton()->strpos( $this->m_strQuery, ';' ) ) {
				$this->m_strQuery .= ';';
			}

			$strSqlCommand = preg_replace( '/^(\S*).*/s', '$1', $this->m_strQuery );
			$arrstrSql     = explode( ';', $this->m_strQuery, -1 );

			$arrstrSql[0] .= ';';

			if( 'select' == \Psi\CStringService::singleton()->strtolower( $strSqlCommand ) && 1 != \Psi\Libraries\UtilFunctions\count( $arrstrSql ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'query', 'Enter only one SELECT query.' ) );
			}

			if( !( 'select' == \Psi\CStringService::singleton()->strtolower( $strSqlCommand ) || 'explain' == \Psi\CStringService::singleton()->strtolower( $strSqlCommand ) || ( 0 == \Psi\CStringService::singleton()->strcasecmp( 'WITH', $strSqlCommand ) ) ) && 1 == \Psi\Libraries\UtilFunctions\count( $arrstrSql ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'query', 'Enter SELECT query only.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valBoolPrivate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valQuery();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>