<?php

class CAgreementTemplate extends CBaseAgreementTemplate {

	public function valAgreementTemplateTypeId() {

		$boolIsValid = true;

		if( true == is_null( $this->getAgreementTemplateTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'agreement_template_type_id', 'Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {

		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valChangeDescription( $objAdminDatabase ) {
		$boolIsValid = true;

		$objAgreementTemplateCount = CAgreementTemplates::fetchConflictingAgreementTemplateCountByAgreementTemplateTypeId( $this->getAgreementTemplateTypeId(), $objAdminDatabase, $this->getPsProductId() );

		if( true == is_null( $this->getChangeDescription() ) && ( 0 < $objAgreementTemplateCount ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'change_description', 'Change description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTemplateDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getTemplateDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'template_datetime', 'Tenplate date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicates( $objAdminDatabase ) {
		$boolIsValid = true;

		$intConflictingAgreementTemplateCount = CAgreementTemplates::fetchConflictingAgreementTemplateCountByAgreementTemplateTypeId( $this->getAgreementTemplateTypeId(), $objAdminDatabase, $this->getPsProductId() );

		if( 0 < $intConflictingAgreementTemplateCount ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'There is already an agreement template of the same type and product.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {

			case VALIDATE_INSERT:
				$boolIsValid &= $this->valAgreementTemplateTypeId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valDuplicates( $objAdminDatabase );
				$boolIsValid &= $this->valChangeDescription( $objAdminDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valAgreementTemplateTypeId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valTemplateDatetime();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>