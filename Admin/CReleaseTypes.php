<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseTypes
 * Do not add any new functions to this class.
 */

class CReleaseTypes extends CBaseReleaseTypes {

	public static function fetchReleaseTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CReleaseType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchReleaseType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CReleaseType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>