<?php

class CEmployeeApplicationScoreType extends CBaseEmployeeApplicationScoreType {

	const MRAB = 1;
	const CCAT = 2;

	const TEST_MRAB = 'MRAB';
	const TEST_CCAT = 'CCAT';

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>