<?php

class CDbWatchType extends CBaseDbWatchType {

	const ADMIN_USERS 				= 1;
	const AR_PAYMENTS 				= 2;
	const MODULES 					= 3;
	const COMPANY_ACCOUNTS 			= 4;
	const COMPANY_MERCHANT_ACCOUNTS = 5;
	const MISCELLANEOUS 			= 6;
	const SETTLEMENT_DISTRIBUTIONS	= 7;
}
?>