<?php

define( 'PHP_MAX_POST', 	ini_get( 'post_max_size' ) );
define( 'PHP_MAX_UPLOAD', 	ini_get( 'upload_max_filesize' ) );

use Psi\Libraries\ExternalFileUpload\CFileUpload;

class CTaskAttachment extends CBaseTaskAttachment {

	use Psi\Libraries\EosFoundation\TEosStoredObject;
	const UPLOAD_ACTION_OVERWRITE 					= 1;
	const UPLOAD_ACTION_RENAME 						= 2;

	const ACTION_DELETE 			= 1;
	const ACTION_REMOVE_AND_UPDATE 	= 2;
	const MAX_FILE_SIZE				= 13;
	const MEDIA_PATH				= '/admin_medias/task/images/';

	protected $m_intTaskAttachmentActionId;
	protected $m_boolTaskAttachmentIsWrittenToFileSystem;

	protected $m_strFileName;
	protected $m_strFileTmpName;
	protected $m_intFileSize;
	protected $m_strFileError;
	protected $m_strFileType;
	protected $m_strEmployeeNameFirst;
	protected $m_strEmployeeNameLast;
	protected $m_strEmployeePreferredName;
	protected $m_strParentTaskAttachmentId;
	protected $m_strTaskAttachmentPath;
	protected $m_strTitle;
	protected $m_intCid;

	protected $m_objObjectStorageGateway;

	public function __construct() {
		parent::__construct();

		$this->m_boolTaskAttachmentIsWrittenToFileSystem = false;

		return;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['task_attachment_action_id'] ) ) $this->setTaskAttachmentActionId( ( true == $boolStripSlashes ) ? stripslashes( $arrmixValues['task_attachment_action_id'] ) : $arrmixValues['task_attachment_action_id'] );
		if( true == isset( $arrmixValues['employee_name_first'] ) ) $this->setEmployeeNameFirst( $arrmixValues['employee_name_first'] );
		if( true == isset( $arrmixValues['employee_name_last'] ) ) $this->setEmployeeNameLast( $arrmixValues['employee_name_last'] );
		if( true == isset( $arrmixValues['employee_preferred_name'] ) ) $this->setEmployeePreferredName( $arrmixValues['employee_preferred_name'] );
		if( true == isset( $arrmixValues['parent_task_attachment_id'] ) ) $this->setParentTaskAttachmentId( $arrmixValues['parent_task_attachment_id'] );
		if( true == isset( $arrmixValues['task_attachment_path'] ) ) $this->setTaskAttachmentPath( $arrmixValues['task_attachment_path'] );
		return;
	}

	public function setTaskAttachmentActionId( $intTaskAttachmentActionId ) {
		$this->m_intTaskAttachmentActionId = $intTaskAttachmentActionId;
	}

	public function setFileName( $strFileName ) {
		$this->m_strFileName = $strFileName;
	}

	public function setFileTmpName( $strTmpFileName ) {
		$this->m_strFileTmpName = $strTmpFileName;
	}

	public function setFileSize( $intFileSize ) {
		$this->m_intFileSize = $intFileSize;
	}

	public function setFileError( $strFileError ) {
		$this->m_strFileError = $strFileError;
	}

	public function setFileType( $strFileType ) {
		$this->m_strFileType = $strFileType;
	}

	public function setFileData( $arrstrFileData, $intCounter ) {

		$this->setFileTmpName( $arrstrFileData['tmp_name'][$intCounter] );
		$this->setFileError( $arrstrFileData['error'][$intCounter] );
		$this->setFileName( $arrstrFileData['name'][$intCounter] );
		$this->setUrl( $arrstrFileData['name'][$intCounter] );
	}

	public function setEmployeeNameFirst( $strEmployeeNameFirst ) {
		$this->m_strEmployeeNameFirst = $strEmployeeNameFirst;
	}

	public function setEmployeeNameLast( $strEmployeeNameLast ) {
		$this->m_strEmployeeNameLast = $strEmployeeNameLast;
	}

	public function setEmployeePreferredName( $strEmployeePreferredName ) {
		$this->m_strEmployeePreferredName = $strEmployeePreferredName;
	}

	public function setParentTaskAttachmentId( $intParentTaskAttachmentId ) {
		$this->m_strParentTaskAttachmentId = $intParentTaskAttachmentId;
	}

	public function setTaskAttachmentPath( $strTaskAttachmentPath ) {
		$this->m_strTaskAttachmentPath = $strTaskAttachmentPath;
	}

	public function setCid( $intCid ) {
		$this->m_intCid = $intCid;
	}

	public function setTitle( $strTitle ) {
		$this->m_strTitle = $strTitle;
	}

	public function setObjectStorageGateway( $objObjectStorageGateway ) {
		$this->m_objObjectStorageGateway = $objObjectStorageGateway;
	}

	/**
	 * Get Functions
	 */

	public function getTaskAttachmentIsWrittenToFileSystem() {
		return $this->m_boolTaskAttachmentIsWrittenToFileSystem;
	}

	public function getTaskAttachmentActionId() {
		return $this->m_intTaskAttachmentActionId;
	}

	public function getFileName() {
		return $this->m_strFileName;
	}

	public function getFileTmpName() {
		return $this->m_strFileTmpName;
	}

	public function getFileSize() {
		return $this->m_intFileSize;
	}

	public function getFileError() {
		return $this->m_strFileError;
	}

	public function getFileType() {
		return $this->m_strFileType;
	}

	public function getEmployeeNameFirst() {
		return $this->m_strEmployeeNameFirst;
	}

	public function getEmployeeNameLast() {
		return $this->m_strEmployeeNameLast;
	}

	public function getEmployeePreferredName() {
		return $this->m_strEmployeePreferredName;
	}

	public function getTaskAttachmentPath() {
		return $this->m_strTaskAttachmentPath;
	}

	public function getParentTaskAttachmentId() {
		return $this->m_strParentTaskAttachmentId;
	}

	public function getObjectStorageGateway() {
		if( false == valObj( $this->m_objObjectStorageGateway, 'CProxyObjectStorageGateway' ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to get object storage gateway. Invalid dependency container.' ) );
			return false;
		}

		return $this->m_objObjectStorageGateway;
	}

	public function getCid() {
		if( true == valId( $this->m_intCid ) ) {
			return $this->m_intCid;
		} else {
			return CClient::ID_DEFAULT;
		}
	}

	public function getTitle() {
		return $this->m_strTitle;
	}

	public function getName() {
		return \Psi\CStringService::singleton()->substr( $this->getUrl(), \Psi\CStringService::singleton()->stripos( $this->getUrl(), '_' ) + 1 );
	}

	/**
	 * Fetch Functions
	 */

	public function fetchTaskAttachment( $objDatabase ) {
		return \Psi\Eos\Admin\CTaskAttachments::createService()->fetchTaskAttachmentById( $this->getId(), $objDatabase );
	}

	/**
	 * Validation Functions
	 */

	public function valFileError( $objDatabase = NULL ) {

		$boolIsValid = true;

		if( false == is_null( $objDatabase ) && UPLOAD_ERR_NO_FILE != $this->m_strFileError ) {

			$arrstrFileInfo = pathinfo( $this->getUrl() );

			if( true == valArr( $arrstrFileInfo ) && true == array_key_exists( 'extension', $arrstrFileInfo ) ) {
				if( 'bmp' == $arrstrFileInfo['extension'] ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' You cannot upload a .bmp file. You must convert it to a supported file format ( .jpg, .gif, .png ). ' ) );
					return $boolIsValid;
				}

				if( 'json' != $arrstrFileInfo['extension'] && 'rar' != $arrstrFileInfo['extension'] ) {
					$objFileExtension = \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileInfo['extension'], $objDatabase );

					if( false == is_object( $objFileExtension ) && false == isset( $objFileExtension ) ) {

						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, $this->m_strUrl . ' Unrecognized File extension. ' ) );

						return $boolIsValid;
					}

					$arrintDisallowedFileExtensionIds = $objFileExtension->getDisallowedFileExtensionIds();

					if( false !== array_search( $objFileExtension->getId(), $arrintDisallowedFileExtensionIds ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' You cannot upload a .' . $arrstrFileInfo['extension'] . ' file.' ) );

						return $boolIsValid;
					}
				}
			} else {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $this->m_strUrl . ' file is without extension. Please attach file with valid extension.' ) );
					return $boolIsValid;
			}
		}

		if( UPLOAD_ERR_INI_SIZE == $this->m_strFileError ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $this->m_strUrl . ' failed to upload properly. File cannot be larger than ' . PHP_MAX_UPLOAD . 'B.' ) );
			return $boolIsValid;

		} elseif( UPLOAD_ERR_NO_TMP_DIR == $this->m_strFileError ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' No file was uploaded , Missing a temporary folder.' ) );
			return $boolIsValid;

		}

		return $boolIsValid;
	}

	public function valReleaseNoteAttachment( $objDatabase = NULL, $intFileSize ) {

		$boolIsValid = true;
		if( false == is_null( $objDatabase ) && UPLOAD_ERR_NO_FILE != $this->getFileError() ) {

			$arrstrFileInfo = pathinfo( $this->getUrl() );
			if( true == valArr( $arrstrFileInfo ) && true == array_key_exists( 'extension', $arrstrFileInfo ) ) {

				$objFileExtension = \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileInfo['extension'], $objDatabase );

				if( false == is_object( $objFileExtension ) && false == isset( $objFileExtension ) ) {

					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $this->m_strUrl . ' Unrecognized File extension. ' ) );

					return $boolIsValid;
				}

				if( false == in_array( $objFileExtension->getId(), CFileExtension::$c_arrintReleaseNoteImageExtensionIds ) ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Please upload file with extensions jpeg, png, jpg ' ) );

					return $boolIsValid;
				}

				if( 10485760 < $intFileSize ) {
					$boolIsValid = false;
					$this->addErrorMsg( new CErrorMsg( NULL, NULL, $this->getUrl() . ' File size should be less than 10 MB' ) );

					return $boolIsValid;
				}

			} else {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, $this->getUrl() . ' file is without extension. Please attach file with valid extension.' ) );
				return $boolIsValid;
			}
		} else {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Please add attachment(s).' ) );
			return $boolIsValid;
		}

		if( UPLOAD_ERR_INI_SIZE == $this->getFileError() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $this->m_strUrl . ' failed to upload properly. File cannot be larger than ' . PHP_MAX_UPLOAD . 'B.' ) );
			return $boolIsValid;

		} elseif( UPLOAD_ERR_NO_TMP_DIR == $this->getFileError() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' No file was uploaded , Missing a temporary folder.' ) );
			return $boolIsValid;

		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase=NULL, $intFileSize = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valFileError( $objDatabase );
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valFileError( $objDatabase );
				break;

			case 'VALIDATE_RELEASE_NOTE_ATTACHMENT':
				$boolIsValid &= $this->valReleaseNoteAttachment( $objDatabase, $intFileSize );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Database Functions
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolUploadFile = true, $boolUploadForStandard = false, $boolUploadForDuplicate = false ) {

		$boolIsValid	= true;

		// If $boolUploadFile is true then fetch the latest task attachment id and create a unique filename. This step would be skip only if the attachment is already uploaded.
		if( true == $boolUploadFile ) {
			$intId = $this->fetchNextId( $objDatabase );
			$this->setUrl( $intId . '_' . CFileUpload::cleanFilename( $this->getFileName() ) );
		}

		if( true == $boolUploadForStandard ) {
			$this->fetchNextId( $objDatabase );
		}

		if( true == $boolUploadForDuplicate ) {
			$intId = $this->fetchNextId( $objDatabase );
			$this->m_strFileTmpName = $this->getUrl();
			$this->setUrl( $intId . '_' . CFileUpload::cleanFilename( $this->getFileName() ) );
			$this->setFileTmpName( $this->getFullTaskAttachmentPath() . $this->m_strFileTmpName );
		}

		$boolIsValid 	= parent::insert( $intCurrentUserId, $objDatabase );

		if( true == $boolUploadFile && false == $boolUploadForStandard ) {
			$boolIsValid = $this->uploadTaskAttachmentsToStorageGateway( $intCurrentUserId, $objDatabase );
		}

		// FixMe: This flag is not in use
		if( true == $boolUploadForDuplicate ) {
			$boolIsValid = $this->writeTaskAttachmentToFileSystem( true );
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolIsValid 		= true;

		$objTaskAttachment	= $this->fetchTaskAttachment( $objDatabase );
		$this->m_strUrl		= explode( '_', $objTaskAttachment->getUrl() );

		$intId	= $objTaskAttachment->getId();

		$this->setUrl( CFileUpload::cleanFilename( $this->getFileName() ) );

		$this->m_strUrl = $intId . '_' . $this->m_strUrl;

		$boolIsValid 	= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == $boolIsValid ) {
			$boolIsValid = $this->uploadTaskAttachmentsToStorageGateway( $intCurrentUserId, $objDatabase );
		}

		$this->setUrl( $objTaskAttachment->getUrl() );
		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolIsValid = parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( true == $boolIsValid ) {
			$boolIsValid = $this->deleteTaskAttachmentFromFileSystem();

			// Delete stored object
			$boolIsValid = $this->deleteTaskAttachmentFromObjectStorage( $intCurrentUserId, $objDatabase );
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function writeTaskAttachmentToFileSystem( $boolIsDuplicate = false ) {

		$boolIsValid = true;

		if( false == CFileIo::isDirectory( $this->getFullTaskAttachmentPath() ) ) {
			if( false == CFileIo::recursiveMakeDir( $this->getFullTaskAttachmentPath() ) ) {
				trigger_error( 'Couldn\'t create directory(' . $this->getFullTaskAttachmentPath() . ' ). ', E_USER_WARNING );
				$boolIsValid = false;
			}
		}

		if( true == $boolIsDuplicate ) {
			if( false == CFileIo::copyFile( $this->getFullTaskAttachmentPath() . $this->m_strFileTmpName, $this->getFullTaskAttachmentPath() . $this->m_strUrl ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' No file was uploaded.' ) );
				$boolIsValid = false;
			}
		} else {
			if( false == move_uploaded_file( $this->m_strFileTmpName, $this->getFullTaskAttachmentPath() . $this->m_strUrl ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' No file was uploaded.' ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function uploadTaskAttachmentsToStorageGateway( $intCurrentUserId, $objDatabase ) {
		$objStorageGateway       = $this->getObjectStorageGateway();
		$arrmixRequest           = $this->createPutGatewayRequest( [ 'data' => file_get_contents( $this->getFileTmpName() ) ] );
		$arrmixPutObjectResponse = $objStorageGateway->putObject( $arrmixRequest );

		if( true == $arrmixPutObjectResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $arrmixPutObjectResponse['messages'] ) );
			return false;
		}

		if( false == $this->setObjectStorageGatewayResponse( $arrmixPutObjectResponse )->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Failed to upload task attachment.' ) );
			return false;
		}

		return true;
	}

	public function downloadTaskAttachment( $intCurrentUserId, $objDatabase, $boolReturnPath = false, $boolDisposition = false, $boolReleaseNotes = false ) {
		if( true == $boolReturnPath ) {
			$arrmixRequest = $this->fetchStoredObject( $objDatabase )->createGatewayRequest( [ 'outputFile' => 'temp' ] );
		} else {
			$arrmixRequest = $this->fetchStoredObject( $objDatabase )->createGatewayRequest();
		}

		$objStorageGateway = $this->getObjectStorageGateway();

		$arrmixGetObjectResponse = $objStorageGateway->getObject( $arrmixRequest );

		if( $arrmixGetObjectResponse->hasErrors() ) {

			$this->addErrorMsg( new CErrorMsg( NULL, NULL, $arrmixGetObjectResponse['messages'] ) );

			return false;

		} else {
			if( true == $boolReturnPath ) {
				return $arrmixGetObjectResponse['outputFile'];
			}

			$strDisposition = ( false == $boolDisposition ) ? 'attachment' : 'inline';
			$arrstrFileParts = pathinfo( $this->getUrl() );
			header( 'Pragma: public' );
			header( 'Expires: 0' );
			header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
			header( 'Cache-Control: public' );
			header( 'Content-Description: File Transfer' );
			header( 'Content-Length: ' . $arrmixGetObjectResponse['contentLength'] );
			header( 'Content-type: ' . CFileExtension::$c_arrmixAllFileExtensions[$arrstrFileParts['extension']] );
			header( 'Content-Disposition: ' . $strDisposition . '; filename=' . $this->getName() );
			header( 'Content-Transfer-Encoding:binary' );

			echo $arrmixGetObjectResponse['data'];
			exit();
		}

		if( true == $boolReleaseNotes ) {
			$strPath = PATH_MOUNTS . 'admin_medias/task/images/';
		} else {
			$strPath = $this->getFullTaskAttachmentPath();
		}

		if( true == CFileIo::fileExists( $strPath . $this->getUrl() ) ) {
			$this->setFileTmpName( $strPath . $this->getUrl() );

			// ToDo: Failing to upload file to s3 if same file is associated with mutliple tasks. Once this issue is fixed we can remove this code.
			$intTaskAttachmentId = \Psi\CStringService::singleton()->strstr( $this->getUrl(), '_', true );

			if( !$intTaskAttachmentId || $intTaskAttachmentId != $this->getId() ) {
				$this->setFileName( $this->getUrl() );

				$this->update( $intCurrentUserId, $objDatabase );
			} else {
				$this->uploadTaskAttachmentsToStorageGateway( $intCurrentUserId, $objDatabase );
			}

			if( true == $boolReturnPath ) {
				return $strPath . $this->getUrl();
			}
		}

		$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to download file.' ) );
		return false;
	}

	public function deleteTaskAttachmentFromObjectStorage( $intCurrentUserId, $objDatabase ) {
		$objStorageGateway = $this->getObjectStorageGateway();
		$arrmixRequest = $this->fetchStoredObject( $objDatabase )->createGatewayRequest();
		$arrmixDeleteObjectResponse = $objStorageGateway->deleteObject( $arrmixRequest );

		if( $arrmixDeleteObjectResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to delete task attachment file.' ) );
			return false;
		}

		if( !$this->deleteStoredObject( $intCurrentUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to delete task attachment file.' ) );
			return false;
		}

		return true;
	}

	public function deleteTaskAttachmentFromFileSystem() {
		$boolIsValid = true;

		if( true == CFileIo::fileExists( $this->getFullTaskAttachmentPath() . $this->getUrl() ) ) {
			if( false == CFileIo::deleteFile( $this->getFullTaskAttachmentPath() . $this->getUrl() ) ) {
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Task attachment failed to delete.' ) );
				trigger_error( 'Task attachment failed to delete', E_USER_WARNING );
			}
		}

		return $boolIsValid;
	}

	public function getFullTaskAttachmentPath() {

		date_default_timezone_set( 'America/Denver' );

		$intDate = ( true == is_null( $this->getCreatedOn() ) ) ? time() : strtotime( $this->getCreatedOn() );

		return PATH_NON_BACKUP_MOUNTS_GLOBAL_TASK_ATTACHMENTS . date( 'Y', $intDate ) . '/' . date( 'm', $intDate ) . '/' . date( 'd', $intDate ) . '/';

	}

	public function getAttachmentFileSize( $objDatabase, $objObjectStorageGateway = NULL ) {
		$arrmixRequest = $this->fetchStoredObject( $objDatabase )->createGatewayRequest();
		$arrmixGetObjectResponse = $objObjectStorageGateway->getObject( $arrmixRequest );

		if( $arrmixGetObjectResponse->hasErrors() ) {
			return NULL;
		}

		return $arrmixGetObjectResponse['contentLength'];
	}

	protected function calcStorageKey() {
		return CClient::ID_DEFAULT . '/tasks/task_attachments/' . date( 'Y/m/d' ) . '/' . $this->getId() . '/' . $this->getUrl();
	}

	protected function calcStorageContainer( $strVendor = NULL ) {
		return CConfig::get( 'OSG_BUCKET_SYSTEM_DOCUMENTS' );
	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( CClient::ID_DEFAULT, $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

}

?>