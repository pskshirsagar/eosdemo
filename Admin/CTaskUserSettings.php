<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskUserSettings
 * Do not add any new functions to this class.
 */

class CTaskUserSettings extends CBaseTaskUserSettings {

	public static function fetchTaskUserSettings( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CUserSetting::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTaskUserSetting( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CUserSetting::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTaskUserSettingByUserId( $intUserId, $objAdminDatabase ) {

		if( false == is_numeric( $intUserId ) ) {
			return false;
		}

		$strSql = 'SELECT * FROM task_user_settings WHERE user_id = ' . ( int ) $intUserId . ' ORDER BY id DESC LIMIT 1';
		return parent::fetchTaskUserSetting( $strSql, $objAdminDatabase );
	}

}
?>