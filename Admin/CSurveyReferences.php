<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyReferences
 * Do not add any new functions to this class.
 */

class CSurveyReferences extends CBaseSurveyReferences {

	public static function fetchSurveyReferenceBySurveyQuestionAnswerIdBySurveyReferenceTypeId( $intSurveyQuestionAnswerId, $intSurveyReferenceTypeId, $objDatabase ) {

		$strSql = 'SELECT
						sr.*
					FROM
						survey_references sr
					WHERE
						sr.survey_question_answer_id = ' . ( int ) $intSurveyQuestionAnswerId . '
						AND sr.survey_reference_type_id = ' . ( int ) $intSurveyReferenceTypeId;

		return self::fetchSurveyReference( $strSql, $objDatabase );
	}

	public static function fetchSurveyReferencesBySuveryQuestionAnswerIds( $arrintSurveyQuestionAnswerIds, $objDatabase ) {
		if( false == valArr( $arrintSurveyQuestionAnswerIds ) ) return NULL;

		$strSql = 'SELECT
 						sr.survey_question_answer_id,
						sr.value,
						e.preferred_name
					FROM
						survey_references sr
						JOIN users u ON ( u.id = sr.updated_by )
						JOIN employees e ON ( e.id = u.employee_id )
					WHERE
						sr.survey_question_answer_id IN (' . implode( ',', $arrintSurveyQuestionAnswerIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

}
?>