<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSmsResponsePhoneNumbers
 * Do not add any new functions to this class.
 */

class CSmsResponsePhoneNumbers extends CBaseSmsResponsePhoneNumbers {

	public static function fetchSmsResponsePhoneNumbersByKeywordId( $intKeywordId, $objDatabase ) {
		$strSql = 'SELECT * FROM sms_response_phone_numbers WHERE keyword_id = ' . ( int ) $intKeywordId;
		return self::fetchSmsResponsePhoneNumbers( $strSql, $objDatabase );
	}

	public static function fetchSmsResponsePhoneNumbersByIdByCellNumber( $intId, $strCellNumber, $objDatabase ) {

		if( 10 != strlen( $strCellNumber ) ) {
			$strCellNumber = \Psi\CStringService::singleton()->substr( $strCellNumber, 1 );
		}

		$strSql = 'SELECT * FROM sms_response_phone_numbers WHERE id = ' . ( int ) $intId . ' AND cell_number = \'' . ( string ) addslashes( $strCellNumber ) . '\'';
		return self::fetchSmsResponsePhoneNumber( $strSql, $objDatabase );
	}

}
?>