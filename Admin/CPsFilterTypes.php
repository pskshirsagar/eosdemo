<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsFilterTypes
 * Do not add any new functions to this class.
 */

class CPsFilterTypes extends CBasePsFilterTypes {

	public static function fetchPsFilterTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPsFilterType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPsFilterType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPsFilterType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>