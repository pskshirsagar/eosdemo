<?php

class CSurveyType extends CBaseSurveyType {

	const EMPLOYEE_NPS						= 1;
	const CUSTOMER_NPS						= 2;
	const IMPLEMENTATION_SURVEY				= 3;
	const PERSONAL_REVIEW					= 4;
	const PEER_REVIEW						= 5;
	const SUPPORT_REVIEW					= 6;
	const SALES_ENGINEER_REVIEW				= 7;
	const SALES_CLOSER_REVIEW				= 8;
	const CLIENT_SUCCESS_MANAGER_REVIEW		= 9;
	const LEASING_CENTER_QA					= 10;
	const SUPPORT_QA_CALLS					= 11;
	const SUPPORT_QA_CHATS					= 12;
	const CODE_AUDIT						= 13;
	const MANAGER_SURVEY					= 14;
	const TRAINING 							= 15;
	const CAUCUS_SUPPORTER_CHECK1			= 16;
	const SUPPORT_QA_TASKS					= 17;
	const UTILITIES							= 18;
	const EMPLOYEE_APPLICATION_INTERVIEW	= 19;

	public static $c_arrintPersonSurveyTypes						= [ self::CUSTOMER_NPS, self::SUPPORT_REVIEW, self::CLIENT_SUCCESS_MANAGER_REVIEW ];
	public static $c_arrintEmployeeSurveyTypes						= [ self::EMPLOYEE_NPS, self::IMPLEMENTATION_SURVEY, self::PERSONAL_REVIEW, self::PEER_REVIEW, self::SALES_ENGINEER_REVIEW, self::SALES_CLOSER_REVIEW, self::LEASING_CENTER_QA ];
	public static $c_arrintPendingSurveyTypes						= [ self::PEER_REVIEW, self::PERSONAL_REVIEW, self::CODE_AUDIT, self::EMPLOYEE_NPS, self::IMPLEMENTATION_SURVEY, self::SUPPORT_QA_CALLS, self::SUPPORT_QA_CHATS, self::TRAINING, self::SUPPORT_QA_TASKS ];
	public static $c_arrstrReviewSurveyTypes						= [ self::PEER_REVIEW => 'Peer review', self::EMPLOYEE_NPS => 'Employee NPS', self::CUSTOMER_NPS => 'Customer NPS', self::SUPPORT_REVIEW => 'Support review', self::PERSONAL_REVIEW => 'Personal review', self::SALES_CLOSER_REVIEW => 'Sales closer review', self::SALES_ENGINEER_REVIEW => 'Sales engineer review', self::IMPLEMENTATION_SURVEY => 'Implementation survey', self::CLIENT_SUCCESS_MANAGER_REVIEW => 'Client success manager review' ];
	public static $c_arrintFiveStarSurveyTypes						= [ self::PERSONAL_REVIEW, self::CODE_AUDIT, self::TRAINING, self::MANAGER_SURVEY, self::EMPLOYEE_NPS, self::PEER_REVIEW ];
	public static $c_arrintRatingDescriptionSurveyTypes				= [ self::PERSONAL_REVIEW, self::MANAGER_SURVEY ];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public static function getGracePeriodByFrequencyId( $intFrequencyId, $intSurveyTypeId, $strCountryCode ) {

		if( false == valId( $intSurveyTypeId ) || false == valStr( $strCountryCode ) ) {
			return NULL;
		}

		if( true == in_array( $intSurveyTypeId, [ CSurveyType::TRAINING, CSurveyType::EMPLOYEE_APPLICATION_INTERVIEW ] ) ) {
			$intFrequencyId = ( 'IN' == $strCountryCode ) ? CFrequency::MONTHLY : CFrequency::WEEKLY;
		}

		if( false == valId( $intFrequencyId ) ) {
			return NULL;
		}

		if( 'IN' == $strCountryCode ) {
			switch( $intFrequencyId ) {

				case CFrequency::QUARTERLY:
					$strGracePeriod = '+15 days';
					break;

				case CFrequency::MONTHLY:
					$strGracePeriod = '+1 week';
					break;

				case CFrequency::WEEKLY:
				default:
					$strGracePeriod = '+2 days';
					break;
			}

			return $strGracePeriod;
		}

		switch( $intFrequencyId ) {

			case CFrequency::DAILY:
				$strGracePeriod = '+1 day';
				break;

			case CFrequency::MONTHLY:
				$strGracePeriod = '+30 days';
				break;

			case CFrequency::QUARTERLY:
				$strGracePeriod = '+3 month';
				break;

			case CFrequency::YEARLY:
				$strGracePeriod = '+1 year';
				break;

			case CFrequency::WEEKLY:
			default:
				$strGracePeriod = '+1 week';
				break;
		}

		return $strGracePeriod;
	}

}
?>