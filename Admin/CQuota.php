<?php

use Psi\Eos\Admin\CQuotas;
use Psi\Eos\Admin\CQuotaRequirements;

class CQuota extends CBaseQuota {

	protected $m_arrobjQuotaRequirements;

	protected $m_strPsProductName;
	protected $m_strPsLeadFilterId;
	protected $m_strRequiredAmount;
	protected $m_strActualAmount;
	protected $m_strOverAchievedAmount;
	protected $m_intPsProductId;
	protected $m_intQuotaRequirementId;
	protected $m_strEmployeeName;

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ps_product_name'] ) ) $this->setPsProductName( $arrmixValues['ps_product_name'] );
		if( true == isset( $arrmixValues['ps_lead_filter_id'] ) ) $this->setPsLeadFilterId( $arrmixValues['ps_lead_filter_id'] );
		if( true == isset( $arrmixValues['actual_amount'] ) ) $this->setActualAmount( $arrmixValues['actual_amount'] );
		if( true == isset( $arrmixValues['required_amount'] ) ) $this->setRequiredAmount( $arrmixValues['required_amount'] );
		if( true == isset( $arrmixValues['over_achieved_amount'] ) ) $this->setOverAchievedAmount( $arrmixValues['over_achieved_amount'] );
		if( true == isset( $arrmixValues['ps_product_id'] ) ) $this->setPsProductId( $arrmixValues['ps_product_id'] );
		if( true == isset( $arrmixValues['quota_requirement_id'] ) ) $this->setQuotaRequirementId( $arrmixValues['quota_requirement_id'] );
		if( true == isset( $arrmixValues['employee_name'] ) ) $this->setEmployeeName( $arrmixValues['employee_name'] );
		return;
	}

	/**
	 * Create Functions
	 */

	public function createQuotaRequirement( $intPsLeadFilterId, $intPsProductId ) {

		$objQuotaRequirement = new CQuotaRequirement();
		$objQuotaRequirement->setDefaults();
		$objQuotaRequirement->setQuotaId( $this->getId() );
		$objQuotaRequirement->setPsLeadFilterId( $intPsLeadFilterId );
		$objQuotaRequirement->setPsProductId( $intPsProductId );

		return $objQuotaRequirement;
	}

	public function createQuotaEmployee( $intSalesEmployeeId ) {

		$objQuotaEmployee = new CQuotaEmployee();
		$objQuotaEmployee->setDefaults();
		$objQuotaEmployee->setQuotaId( $this->getId() );
		$objQuotaEmployee->setEmployeeId( $intSalesEmployeeId );

		return $objQuotaEmployee;
	}

	/**
	 * get Functions
	 */

	public function getPsProductName() {
		return $this->m_strPsProductName;
	}

	public function getPsLeadFilterId() {
		return $this->m_strPsLeadFilterId;
	}

	public function getRequiredAmount() {
		return $this->m_strRequiredAmount;
	}

	public function getActualAmount() {
		return $this->m_strActualAmount;
	}

	public function getOverAchievedAmount() {
		return $this->m_strOverAchievedAmount;
	}

	public function getPsProductId() {
		return $this->m_intPsProductId;
	}

	public function getQuotaRequirements() {
		return $this->m_arrobjQuotaRequirements;
	}

	public function getQuotaRequirementId() {
		return $this->m_intQuotaRequirementId;
	}

	public function getEmployeeName() {
		return $this->m_strEmployeeName;
	}

	/**
	 * Set Functions
	 */

	public function setPsProductName( $strPsProductName ) {
		$this->m_strPsProductName = $strPsProductName;
	}

	public function setPsLeadFilterId( $strPsLeadFilterId ) {
		$this->m_strPsLeadFilterId = $strPsLeadFilterId;
	}

	public function setRequiredAmount( $strRequiredAmount ) {
		$this->m_strRequiredAmount = $strRequiredAmount;
	}

	public function setActualAmount( $strActualAmount ) {
		$this->m_strActualAmount = $strActualAmount;
	}

	public function setOverAchievedAmount( $strOverAchievedAmount ) {
		$this->m_strOverAchievedAmount = $strOverAchievedAmount;
	}

	public function setPsProductId( $intPsProductId ) {
		$this->m_intPsProductId = $intPsProductId;
	}

	public function setQuotaRequirements( $arrobjQuotaRequirements ) {
		$this->m_arrobjQuotaRequirements = $arrobjQuotaRequirements;
	}

	public function setQuotaRequirementId( $intQuotaRequirementId ) {
		$this->m_intQuotaRequirementId = $intQuotaRequirementId;
	}

	public function setEmployeeName( $strEmployeeName ) {
		$this->m_strEmployeeName = $strEmployeeName;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchPossibleDuplicateQuotasCount( $objDatabase ) {
		return CQuotas::createService()->fetchPossibleDuplicateQuotasCount( $this, $objDatabase );
	}

	public function fetchQuotaRequirementsByQuotaId( $objDatabase ) {
		return CQuotaRequirements::createService()->fetchQuotaRequirementsByQuotaId( $this->getId(), $objDatabase );
	}

	/**
	 * Validate Functions
	 */

	public function valName() {
		$boolIsValid = true;

		if( 0 == strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Quota name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valStartDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getStartDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'start_date', 'Quota Start Date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEndDate() {
		$boolIsValid = true;

		if( true == is_null( $this->getEndDate() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Quota End Date is required.' ) );
		}

		return $boolIsValid;
	}

	public function valQuotaDate() {

		$boolIsValid = true;

		$fltDateDiff = abs( strtotime( $this->getEndDate() ) - strtotime( $this->getStartDate() ) );

		$fltDaysBetween = ceil( abs( strtotime( $this->getEndDate() ) - strtotime( $this->getStartDate() ) ) / 86400 );

		$intEndDateMonth   = date( 'm', strtotime( $this->getEndDate() ) );
		$intStartDateMonth = date( 'm', strtotime( $this->getStartDate() ) );

		$intEndDateYear   = date( 'Y', strtotime( $this->getEndDate() ) );
		$intStartDateYear = date( 'Y', strtotime( $this->getStartDate() ) );

		$intEndDateDay   = date( 'd', strtotime( $this->getEndDate() ) );
		$intStartDateDay = date( 'd', strtotime( $this->getStartDate() ) );

		if( $intStartDateYear > $intEndDateYear ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Quota End Date should be greater than Quota Start Date.' ) );
		} elseif( $intStartDateMonth > $intEndDateMonth && $intStartDateYear >= $intEndDateYear ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Quota End Date should be greater than Quota Start Date.' ) );
		} elseif( $intStartDateDay > $intEndDateDay && $intStartDateMonth >= $intEndDateMonth && $intStartDateYear >= $intEndDateYear ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'Quota End Date should be greater than Quota Start Date.' ) );
		} else {

			$fltYears = floor( $fltDateDiff / ( 365 * 60 * 60 * 24 ) );
			$fltMonths = floor( ( $fltDateDiff - $fltYears * 365 * 60 * 60 * 24 ) / ( 30 * 60 * 60 * 24 ) );
			$fltDays = floor( ( $fltDateDiff - $fltYears * 365 * 60 * 60 * 24 - $fltMonths * 30 * 60 * 60 * 24 ) / ( 60 * 60 * 24 ) );

			$fltDaysBetween = ceil( abs( strtotime( $this->getEndDate() ) - strtotime( $this->getStartDate() ) ) / 86400 );

			$intRequiredDateDiff = 27;

			if( $intRequiredDateDiff > $fltDaysBetween ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'end_date', 'There should be at least 1 month difference between Quota Start Date and Quota End Date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valStartDate();
				$boolIsValid &= $this->valEndDate();
				if( $boolIsValid == true ) {
					$boolIsValid &= $this->valQuotaDate();
				}
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>