<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsDocumentTypes
 * Do not add any new functions to this class.
 */

class CPsDocumentTypes extends CBasePsDocumentTypes {

	public static function fetchPsDocumentTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPsDocumentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchPsDocumentType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPsDocumentType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAllClientPsDocumentTypes( $objDatabase ) {
		return self::fetchPsDocumentTypes( 'SELECT * FROM ps_document_types WHERE is_company = 1 ORDER BY order_num', $objDatabase );
	}

	public static function fetchPsDocumentTypesByIds( $arrintDocumentTypeIds, $objDatabase ) {
		if( false == valArr( $arrintDocumentTypeIds ) ) return NULL;
		return self::fetchPsDocumentTypes( sprintf( 'SELECT * FROM ps_document_types WHERE id IN( %s ) ORDER BY order_num', implode( ',', $arrintDocumentTypeIds ) ), $objDatabase );
	}

	public static function fetchEnabledClientPsDocumentTypes( $objDatabase ) {
		return self::fetchPsDocumentTypes( 'SELECT * FROM ps_document_types WHERE is_company = 1 AND  is_published = 1 ORDER BY order_num', $objDatabase );
	}

	public static function fetchPsDocumentTypesByIsEmployee( $objDatabase, $arrintIds = NULL ) {
		$strSubSql = '';
		if( true == valArr( $arrintIds ) ) {
			$strSubSql = ' AND id NOT IN ( ' . implode( ',', $arrintIds ) . ' ) ';
		}

		$strSql = ' SELECT
						*
					FROM
						ps_document_types
					WHERE
						is_employee = 1 ' . $strSubSql . '
					ORDER BY
						order_num';

		return self::fetchPsDocumentTypes( $strSql, $objDatabase );

	}

	public static function fetchPsDocumentTypesByIsEmployeeByIsPublished( $objDatabase, $arrintIds = NULL ) {
		$strSubSql = '';
		if( true == valArr( $arrintIds ) ) {
			$strSubSql = ' AND id NOT IN ( ' . implode( ',', $arrintIds ) . ' ) ';
		}

		$strSql = ' SELECT
						 *
					FROM
						ps_document_types
					WHERE
						is_employee = 1
						AND is_published = 1 ' . $strSubSql . '
					ORDER BY
						order_num';

		return self::fetchPsDocumentTypes( $strSql, $objDatabase );

	}

	public static function fetchEnabledSystemPsDocumentTypes( $objDatabase ) {
		return self::fetchPsDocumentTypes( 'SELECT * FROM ps_document_types WHERE is_system = 1 AND is_published = 1 ORDER BY order_num', $objDatabase );
	}

	public static function fetchPublishedPsDocumentTypes( $objDatabase ) {
		return self::fetchPsDocumentTypes( 'SELECT * FROM ps_document_types WHERE is_published = 1 ORDER BY order_num', $objDatabase );
	}

	public static function fetchAllPsDocumentTypes( $objDatabase ) {
		return self::fetchPsDocumentTypes( 'SELECT * FROM ps_document_types ORDER BY order_num', $objDatabase );
	}

	public static function fetchPsDocumentTypesByIsEmployeeByPsDocumentTypeId( $objDatabase, $strCountryCode = NULL ) {

		$strWhereCondition = NULL;

		if( false == is_null( $strCountryCode ) && CCountry::CODE_USA == $strCountryCode ) {
			$strWhereCondition = ' AND id NOT IN ( ' . CPsDocumentType::DOCUMENT_TYPE_HEALTH_RELATED . ' ) ';
		}

		$strSql = ' SELECT
						 *
					FROM
						ps_document_types
					WHERE
						is_employee = 1
						AND ps_document_type_id IS NOT NULL ' . $strWhereCondition . '
					ORDER BY
						order_num';

		return self::fetchPsDocumentTypes( $strSql, $objDatabase );
	}

}
?>