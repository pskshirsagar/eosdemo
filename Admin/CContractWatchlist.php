<?php

class CContractWatchlist extends CBaseContractWatchlist {

	public function valPsLeadId() {
		$boolIsValid = true;

// 		Validation example

		if( true == is_null( $this->getPsLeadId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_lead_id', 'Lead id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Employee id is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPsLeadId();
				$boolIsValid &= $this->valEmployeeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>