<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CAgreementTemplates
 * Do not add any new functions to this class.
 */

class CAgreementTemplates extends CBaseAgreementTemplates {

	public static function fetchTotalCountCurrentAgreementTemplates( $objAdminDatabase ) {

		$strSql = 'SELECT
					count(id)
					FROM
					agreement_templates
					WHERE
					deleted_on IS NULL AND is_current=1';

		$arrintResponse = fetchData( $strSql, $objAdminDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
		return 0;
	}

	public static function fetchPaginatedCurrentAgreementTemplates( $intPageNo, $intPageSize, $objAdminDatabase ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= $intPageSize;

		$strSql 	= 'SELECT
						*
						FROM
							agreement_templates
						WHERE
							deleted_on IS NULL
						ORDER BY agreement_template_type_id, order_num ASC OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchAgreementTemplates( $strSql, $objAdminDatabase );
	}

	public static function fetchConflictingAgreementTemplateCountByAgreementTemplateTypeId( $intAgreementTemplateTypeId, $objAdminDatabase, $intPsProductId = NULL ) {

		$strCondition = '';

		if( false == is_null( $intPsProductId ) ) {
			$strCondition = ' AND ps_product_id = ' . ( int ) $intPsProductId;
		} else {
			$strCondition = ' AND ps_product_id IS NULL';
		}

		$strWhereSql = ' WHERE deleted_on IS NULL AND agreement_template_type_id = ' . ( int ) $intAgreementTemplateTypeId . $strCondition . ' LIMIT 1 ';

		return self::fetchAgreementTemplateCount( $strWhereSql, $objAdminDatabase );
	}

	public static function fetchAgreementTemplatesByAgreementTemplateTypeId( $intAgreementTemplateTypeId, $objAdminDatabase, $intPsProductId = NULL ) {

		$strCondition = '';

		if( false == is_null( $intPsProductId ) ) {
			$strCondition = ' AND ps_product_id = ' . ( int ) $intPsProductId;
		} else {
			$strCondition = ' AND ps_product_id IS NULL';
		}

		$strSql = 'SELECT
					*
					FROM
						agreement_templates
					WHERE
						agreement_template_type_id = ' . ( int ) $intAgreementTemplateTypeId . $strCondition . '
					ORDER BY
						id DESC';

		return self::fetchAgreementTemplates( $strSql, $objAdminDatabase );
	}

	public static function fetchCurrentAgreementTemplatesByAgreementTemplateTypeId( $intAgreementTemplateTypeId, $objAdminDatabase ) {

		$strSql = 'SELECT
					*
					FROM
						agreement_templates
					WHERE
						is_current=1
					AND
						agreement_template_type_id = ' . ( int ) $intAgreementTemplateTypeId . '
					ORDER BY name ASC';
		return self::fetchAgreementTemplates( $strSql, $objAdminDatabase );
	}

}
?>