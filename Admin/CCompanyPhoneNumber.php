<?php

class CCompanyPhoneNumber extends CBaseCompanyPhoneNumber {

	/**
	 * Validation Functions
	 *
	 */

	public function validate( $strAction, $strPhoneNumberType = 'Phone Number' ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>