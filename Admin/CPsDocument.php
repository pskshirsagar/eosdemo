<?php

use Psi\Libraries\ExternalFileUpload\CFileUpload;

class CPsDocument extends CBasePsDocument {

	use Psi\Libraries\EosFoundation\TEosStoredObject;
	protected $m_strFilePath;
	protected $m_intFileSize;
	protected $m_strFileError;
	protected $m_intIsApproved;
	protected $m_intContractId;
	protected $m_strTempFileName;
	protected $m_intExtensionId = 0;
	protected $m_intPsDocumentsCategoryCount;
	protected $m_intCategorySupportPsDocumentsCount;
	protected $m_intContractDocumentTypeId;

	protected $m_boolIsRenewalContractUpdate;
	protected $m_strContractDocumentTypeName;

	protected $m_objObjectStorageGatewayResponse;
	protected $m_strUniqueId;
	protected $m_strMountsPath;

	protected $m_objObjectStorageGateway;

	const THUMBNAIL_WIDTH  = 46;
	const THUMBNAIL_HEIGHT = 59;

	const IMAGE_MAX_X 	= 2048;
	const IMAGE_MAX_Y	= 2048;

	const IMAGE_THUMB_X	= 150;
	const IMAGE_THUMB_Y	= 150;

	const IMAGE_QUALITY_CROPPED	= 85;
	const IMAGE_QUALITY_MAX		= 85;

	const EXTENSION_WWW			= 1;

	const FILE_MAX_SIZE 	= 12582912;

	const REPORT_DOCUMENT_TYPE 	= 40;

	public function __construct() {
		parent::__construct();

		$this->m_intIsApproved = 0;

		return;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function getFileError() {
		return $this->m_strFileError;
	}

	public function getFileSize() {
		return $this->m_intFileSize;
	}

	public function getCategorySupportPsDocumentsCount() {
		return $this->m_intCategorySupportPsDocumentsCount;
	}

	public function getPsDocumentsCategoryCount() {
		return $this->m_intPsDocumentsCategoryCount;
	}

	public function getIsApproved() {
		return $this->m_intIsApproved;
	}

	public function getContractId() {
		return $this->m_intContractId;
	}

	public function getIsRenewalContractUpdate() {
		return $this->m_boolIsRenewalContractUpdate;
	}

	public function getContractDocumentTypeName() {
		return $this->m_strContractDocumentTypeName;
	}

	public function getContractDocumentTypeId() {
		return $this->m_intContractDocumentTypeId;
	}

	public function getExtensionId() {
		return $this->m_intExtensionId;
	}

	public function getFilePath() {

		if( false == is_null( $this->getPsLeadId() ) && true == is_numeric( $this->getPsLeadId() ) ) {

			$this->m_strFilePath = 'ps_leads/' . $this->getPsLeadId() . '/';

		} elseif( false == is_null( $this->getEmployeeId() ) && true == is_numeric( $this->getEmployeeId() ) ) {
			$this->m_strFilePath = PATH_MOUNTS_DOCUMENTS_EMPLOYEES . $this->getEmployeeId() . '/';

			if( false == CFileIo::isDirectory( $this->m_strFilePath ) ) {
				if( false == CFileIo::recursiveMakeDir( $this->m_strFilePath ) ) {
					trigger_error( 'Couldn\'t create directory(' . $this->m_strFilePath . ').', E_USER_ERROR );

					return false;
				}
			}

		} elseif( ( true == is_null( $this->getCid() ) || CClients::$c_intNullCid == $this->getCid() ) && true == is_null( $this->getEmployeeId() ) && false == is_numeric( $this->getEmployeeId() ) && true == is_null( $this->getEmployeeApplicationId() ) && false == is_numeric( $this->getEmployeeApplicationId() ) ) {

			if( false == is_null( $this->getPsDocumentTypeId() ) && ( CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_POLICY == $this->getPsDocumentTypeId() || CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_HANDBOOK == $this->getPsDocumentTypeId() ) ) {
				$this->m_strFilePath = PATH_MOUNTS_DOCUMENTS_PUBLIC . 'employee_policy/';
			} elseif( false == is_null( $this->getPsDocumentTypeId() ) && CPsDocumentType::DOCUMENT_TYPE_RESIGNATION_LETTER == $this->getPsDocumentTypeId() ) {
				$this->m_strFilePath = PATH_MOUNTS_DOCUMENTS_EMPLOYEES . 'resignation_letter/';

				if( false == CFileIo::isDirectory( $this->m_strFilePath ) ) {
					if( false == CFileIo::recursiveMakeDir( $this->m_strFilePath ) ) {
						trigger_error( 'Couldn\'t create directory(' . $this->m_strFilePath . ').', E_USER_ERROR );

						return false;
					}
				}
			} else {
				$this->m_strFilePath = PATH_MOUNTS_DOCUMENTS_PUBLIC . 'system/';

				if( false == CFileIo::isDirectory( $this->m_strFilePath ) ) {
					if( false == CFileIo::recursiveMakeDir( $this->m_strFilePath ) ) {
						trigger_error( 'Couldn\'t create directory(' . $this->m_strFilePath . ').', E_USER_ERROR );

						return false;
					}
				}
			}
		} elseif( true == valId( $this->getEmployeeApplicationId() ) && true === in_array( $this->getPsDocumentTypeId(), CPsDocumentType::$c_arrintEmployeeApplicationDocumentTypeIds ) ) {
				$this->m_strFilePath = 'ps_website/employee_resumes/' . $this->getEmployeeApplicationId() . '/';

				// for fallback container for cover letter
				if( true === valStr( $this->getDescription() ) && true == preg_match( '/cover_letter/', $this->getDescription() ) && $this->getPsDocumentTypeId() === CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_APPLICATION_RESUME ) {
					$this->m_strFilePath = 'ps_website/' . $this->getDescription() . '/';
				}

		}

		return $this->m_strFilePath;
	}

	public function getFullPsNotificationDocumentPath() {
		$intDate = ( true == is_null( $this->getCreatedOn() ) ) ? time() : strtotime( $this->getCreatedOn() );
		return  date( 'Y', $intDate ) . '/' . date( 'm', $intDate ) . '/' . date( 'd', $intDate ) . '/' . $this->getFileName();
	}

	public function getIsAssociated() {
		return $this->m_intIsAssociated;
	}

	// This is Totally new function and not tested. Please discuss with Shriram and accordingly proceed.

	public function buildStorageFilePath( $boolPrepareStructure = true ) {

		switch( $this->getPsDocumentTypeId() ) {

			case CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_APPLICATION_RESUME:
				$strFilePath	= PATH_NON_BACKUP_MOUNTS . 'ClientAdmin/employee_resumes/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/';
				$strOldFilePath = PATH_NON_BACKUP_MOUNTS . 'Global/ps_website/employee_resumes/';
				break;

			// As of now we are storing documents at old path only. In future we'll move it in Y/m/d format.
			case CPsDocumentType::DOCUMENT_TYPE_TRAINING_SESSION_STUDY_MATERIAL:
				$strOldFilePath	= PATH_MOUNTS . 'ClientAdmin/training_seeeion_document/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/';
				$strFilePath = PATH_MOUNTS . 'Global/training_session_documents/';
				break;

			default:
				if( $this->getIsMounts() ) {
					$strFilePath = PATH_MOUNTS . $this->getBaseDirectoryName() . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/';
				} else {
					$strFilePath = PATH_NON_BACKUP_MOUNTS . $this->getBaseDirectoryName() . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/';
				}
				break;
		}

		if( true == $boolPrepareStructure ) {
			if( false == CFileIo::isDirectory( $strFilePath ) ) {
				if( false == CFileIo::recursiveMakeDir( $strFilePath ) ) {
					trigger_error( 'Couldn\'t create directory(' . $strFilePath . ').', E_USER_ERROR );
					return false;
				}
			}
		}

		return $strFilePath;
	}
	public function fetchTemporaryPath( $strSignedFilePath = NULL ) {
		$strFilePath = '';
		if( CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_POLICY == $this->getPsDocumentTypeId() ) {
			$strFilePath = $strSignedFilePath . 'agreement_' . $this->getEmployeeId() . '_' . $this->getFileName();
		}
		return $strFilePath;
	}

	public function getObjectStorageGateway() {
		return $this->m_objObjectStorageGateway;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function setFileError( $strFileError ) {
		$this->m_strFileError = $strFileError;
	}

	public function setFileSize( $intFileSize ) {
		$this->m_intFileSize = $intFileSize;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['is_approved'] ) ) $this->setIsApproved( $arrmixValues['is_approved'] );
		if( true == isset( $arrmixValues['category_support_ps_documents_count'] ) ) $this->setCategorySupportPsDocumentsCount( $arrmixValues['category_support_ps_documents_count'] );
		if( true == isset( $arrmixValues['ps_documents_category_count'] ) ) $this->setPsDocumentsCategoryCount( $arrmixValues['ps_documents_category_count'] );
		if( true == isset( $arrmixValues['contract_document_type_name'] ) ) $this->setContractDocumentTypeName( $arrmixValues['contract_document_type_name'] );
		if( true == isset( $arrmixValues['contract_document_type_id'] ) ) $this->setContractDocumentTypeId( $arrmixValues['contract_document_type_id'] );
		if( true == isset( $arrmixValues['extension_id'] ) ) $this->setExtensionId( $arrmixValues['extension_id'] );
		if( true == isset( $arrmixValues['is_associated'] ) ) $this->setIsAssociated( $arrmixValues['is_associated'] );
		if( true == isset( $arrmixValues['contract_id'] ) ) $this->setContractId( $arrmixValues['contract_id'] );

		return;
	}

	public function setCategorySupportPsDocumentsCount( $intCategorySupportPsDocumentsCount ) {
		$this->m_intCategorySupportPsDocumentsCount = $intCategorySupportPsDocumentsCount;
	}

	public function setPsDocumentsCategoryCount( $intPsDocumentsCategoryCount ) {
		$this->m_intPsDocumentsCategoryCount = $intPsDocumentsCategoryCount;
	}

	public function setIsApproved( $intIsApproved ) {
		$this->m_intIsApproved = $intIsApproved;
	}

	public function setContractId( $intContractId ) {
		$this->m_intContractId = $intContractId;
	}

	public function setIsRenewalContractUpdate( $boolIsRenewalContractUpdate ) {
		$this->m_boolIsRenewalContractUpdate = $boolIsRenewalContractUpdate;
	}

	public function setContractDocumentTypeName( $strContractDocumentTypeName ) {
		$this->m_strContractDocumentTypeName = $strContractDocumentTypeName;
	}

	public function setContractDocumentTypeId( $intContractDocumentTypeId ) {
		$this->m_intContractDocumentTypeId = $intContractDocumentTypeId;
	}

	public function setExtensionId( $intExtensionId ) {
		$this->m_intExtensionId = $intExtensionId;
	}

	public function setIsAssociated( $intIsAssociated ) {
		$this->m_intIsAssociated = $intIsAssociated;
	}

	public function setFilePath( $strFilePath ) {
		$this->set( 'm_strFilePath', CStrings::strTrimDef( $strFilePath, 4096, NULL, true ) );
	}

	public function setObjectStorageGateway( $objObjectStorageGateway ) {
		$this->m_objObjectStorageGateway = $objObjectStorageGateway;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valPsDocumentTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsDocumentTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_document_type_id', __( 'Document type is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valPsDocumentCategoryId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsDocumentCategoryId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_document_category_id', 'Document category is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', __( 'Title is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;

		if( true == isset( $_FILES['file_name'] ) && false != ( $_FILES['file_name']['error'] ) ) {
			return 	$boolIsValid;
		}

		if( true == is_null( $this->getFileName() ) && self::FILE_MAX_SIZE >= $this->getFileSize() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'File is required.' ) );
		}

		return ( self::FILE_MAX_SIZE < $this->getFileSize() ) ? false : true;
	}

	public function valFileUpload() {
		$boolIsValid = true;

		if( true == is_null( $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'File is required.' ) ) );
		}

		return $boolIsValid;
	}

	public function valFileSize() {
		$boolIsValid = true;

		if( self::FILE_MAX_SIZE < $this->getFileSize() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'File size exceeds 12 mb limit.' ) );
		}

		return $boolIsValid;
	}

	public function valDocumentFileExtensions( $objDatabase ) {

		$boolIsValid = true;

		$arrintDocumentFileExtensions = array();
		$objFileExtension = $this->fetchFileExtension( $objDatabase );

		if( false == $objFileExtension ) {
			if( false == is_null( $this->getFileName() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'Invalid file specified.' ) ) );
			}
			return $boolIsValid;
		}

		$arrintDocumentFileExtensions 	= CPsDocumentType::getDocumentTypePermissioning( $this->getPsDocumentTypeId() );

		if( true == valArr( $arrintDocumentFileExtensions ) && true == valObj( $objFileExtension, 'CFileExtension' ) ) {
			if( false == in_array( $objFileExtension->getId(), $arrintDocumentFileExtensions ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', __( 'Invalid file specified.' ) ) );
				return $boolIsValid;
			}
		}

		// Valid File is uploaded then set file type id
		$this->setFileExtensionId( $objFileExtension->getId() );
		return $boolIsValid;
	}

	public function valPsDocumentEmployeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'employee_id', 'Employee name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valYouTubeUrl() {
		$boolIsValid = true;

		if( false == valStr( $this->getFileName() ) || 0 == preg_match( '%^https?://(?:www\.)?youtube\.com/([^&]+)%', $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'Invalid Youtube Url.' ) );
		}
		return $boolIsValid;
	}

	public function valUrl( $intExistingDuplicateFileCount ) {
		$boolIsValid = true;

		if( false == valStr( $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'Url is required.' ) );
		} elseif( false == valStr( $this->getFileName() ) || 0 == preg_match( '/\b(?:(?:https?|ftp):\/\/)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i', $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'file_name', 'Invalid Url.' ) );
		}

		if( 0 < $intExistingDuplicateFileCount && true == is_null( $this->getId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'Url', 'Url already exists.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase,  $boolIsForBulkInsertion = false, $intExistingDuplicateFileCount = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valFileUpload();
				$boolIsValid &= $this->valPsDocumentTypeId();

				if( CPsDocumentType::DOCUMENT_TYPE_SUPPORT_DOCUMENT == $this->getPsDocumentTypeId() ) {
					$boolIsValid &= $this->valPsDocumentCategoryId();
					$boolIsValid &= $this->valPsProductId();
				}

				if( self::EXTENSION_WWW == $this->getExtensionId() && CPsDocumentType::DOCUMENT_TYPE_TRAINING_SESSION_STUDY_MATERIAL == $this->getPsDocumentTypeId() ) {
					$boolIsValid &= $this->valUrl( $intExistingDuplicateFileCount );
					break;
				} elseif( self::EXTENSION_WWW == $this->getExtensionId() ) {
					$boolIsValid &= $this->valYouTubeUrl();
					break;
				}

				$boolIsValid &= $this->valDocumentFileExtensions( $objDatabase );
				break;

			case 'validate_contract_document':
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valPsDocumentTypeId();
				$boolIsValid &= $this->valDocumentFileExtensions( $objDatabase );
				break;

			case 'validate_reimbursement_document':
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valPsDocumentTypeId();
				$boolIsValid &= $this->valDocumentFileExtensions( $objDatabase );
				break;

			case 'validate_employee_document':
				$boolIsValid &= $this->valTitle( $objDatabase );
				$boolIsValid &= $this->valPsDocumentEmployeeId();
				$boolIsValid &= $this->valPsDocumentTypeId();

				if( true == $boolIsForBulkInsertion ) {
					$boolIsValid &= $this->valFileUpload();
					$boolIsValid &= $this->valFileSize();
				}

				$boolIsValid &= $this->valDocumentFileExtensions( $objDatabase );

				if( false == $boolIsForBulkInsertion ) {
					$boolIsValid &= $this->valFileName();
				}
				break;

			case 'validate_ps_notification':
				$boolIsValid &= $this->valDocumentFileExtensions( $objDatabase );
				break;

			case 'validate_file':
				$boolIsValid &= $this->valFileSize();
				break;

			case 'validate_employee_handbook':
				$boolIsValid &= $this->valDocumentFileExtensions( $objDatabase );
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $boolIsValid;
				break;

			case 'validate_employee_policy_document':
				$boolIsValid &= $this->valPsDocumentTypeId();
				$boolIsValid &= $this->valFileUpload();
				$boolIsValid &= $this->valDocumentFileExtensions( $objDatabase );
				break;

			case 'validate_dm_document':
				$boolIsValid &= $this->valPsDocumentTypeId();
				$boolIsValid &= $this->valFileSize();
				$boolIsValid &= $this->valFileUpload();
				$boolIsValid &= $this->valDocumentFileExtensions( $objDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchFileExtension( $objDatabase ) {

		if( false == is_null( $this->getFileName() ) ) {
			$arrstrFileParts = pathinfo( $this->getFileName() );
			if( true == valArr( $arrstrFileParts ) && true == array_key_exists( 'extension', $arrstrFileParts ) && false == is_null( $arrstrFileParts['extension'] ) ) {
				return \Psi\Eos\Admin\CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileParts['extension'], $objDatabase );
			}
		}

		return false;
	}
	public function renameFileName(  ) {
		if( false == is_null( $this->getFileName() ) ) {
			$arrstrFileParts = pathinfo( $this->getFileName() );
			if( true == valArr( $arrstrFileParts ) ) {
				$this->setFileName( $arrstrFileParts['filename'] . '_copy' . rand() . '.' . $arrstrFileParts['extension'] );
			}
		}
	}
	/**
	 * Other Functions
	 *
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $strReferenceTag = NULL ) {

		// set last_updated_by and last_updated_on
		$this->setLastUpdatedBy( $intCurrentUserId );
		$this->setLastUpdatedOn( 'NOW()' );

		$boolIsValid = true;

		if( true == $this->getIsRenewalContractUpdate() ) {
			if( false == is_null( $this->getPsLeadId() ) && false == CFileIo::fileExists( PATH_MOUNTS_DOCUMENTS_PS_LEADS . $this->getPsLeadId() ) ) {
				if( false == mkdir( PATH_MOUNTS_DOCUMENTS_PS_LEADS . $this->getPsLeadId() ) ) {
					$boolIsValid = false;
				}
			}

			if( false == move_uploaded_file( $this->getTempFileName(), PATH_MOUNTS_DOCUMENTS_PS_LEADS . $this->getPsLeadId() . '/' . $this->getFileName() ) ) {
				$this->addErrorMsgs( new CErrorMsg( E_USER_ERROR, 'ps_document', 'Document failed to upload.' ) );
				$objDatabase->rollback();
				$boolIsValid = false;
			}
		}

		if( $boolIsValid ) {
			$boolIsValid = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

			if( $boolIsValid && !$boolReturnSqlOnly && $this->getObjectStorageGateway() ) {
				if( false == $this->uploadObject( $this->getObjectStorageGateway(), $this->getTempFileName() ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_document', 'Document failed to upload.' ) );
					$objDatabase->rollback();
					$boolIsValid = false;
				}
			}

			if( $boolIsValid && !$boolReturnSqlOnly ) {
				$boolIsValid = $this->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase, NULL, $strReferenceTag );
			}
		}
		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolIsFromReoder = false ) {

		// update last_update_by and last_updated_on if not reordering the documents.
		if( false == $boolIsFromReoder ) {
			$this->setLastUpdatedBy( $intCurrentUserId );
			$this->setLastUpdatedOn( 'NOW()' );
		}

		$boolUpdate = true;

		if( true == $this->getAllowDifferentialUpdate() ) {
			$this->unSerializeAndSetOriginalValues();
			$arrstrOriginalValueChanges = array();
			$boolUpdate = false;
		}
		$strSql = 'UPDATE
					  public.ps_documents
					SET ';
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' cid = ' . $this->sqlCid() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlCid() ) != $this->getOriginalValueByFieldName( 'cid' ) ) {
						$arrstrOriginalValueChanges['cid'] = $this->sqlCid();
						$strSql .= ' cid = ' . $this->sqlCid() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlEmployeeId() ) != $this->getOriginalValueByFieldName( 'employee_id' ) ) {
						$arrstrOriginalValueChanges['employee_id'] = $this->sqlEmployeeId();
						$strSql .= ' employee_id = ' . $this->sqlEmployeeId() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' ps_document_type_id = ' . $this->sqlPsDocumentTypeId() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlPsDocumentTypeId() ) != $this->getOriginalValueByFieldName( 'ps_document_type_id' ) ) {
						$arrstrOriginalValueChanges['ps_document_type_id'] = $this->sqlPsDocumentTypeId();
						$strSql .= ' ps_document_type_id = ' . $this->sqlPsDocumentTypeId() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' ps_document_category_id = ' . $this->sqlPsDocumentCategoryId() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlPsDocumentCategoryId() ) != $this->getOriginalValueByFieldName( 'ps_document_category_id' ) ) {
						$arrstrOriginalValueChanges['ps_document_category_id'] = $this->sqlPsDocumentCategoryId();
						$strSql .= ' ps_document_category_id = ' . $this->sqlPsDocumentCategoryId() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlPsProductId() ) != $this->getOriginalValueByFieldName( 'ps_product_id' ) ) {
						$arrstrOriginalValueChanges['ps_product_id'] = $this->sqlPsProductId();
						$strSql .= ' ps_product_id = ' . $this->sqlPsProductId() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' file_extension_id = ' . $this->sqlFileExtensionId() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlFileExtensionId() ) != $this->getOriginalValueByFieldName( 'file_extension_id' ) ) {
						$arrstrOriginalValueChanges['file_extension_id'] = $this->sqlFileExtensionId();
						$strSql .= ' file_extension_id = ' . $this->sqlFileExtensionId() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' title = ' . $this->sqlTitle() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlTitle() ) != $this->getOriginalValueByFieldName( 'title' ) ) {
						$arrstrOriginalValueChanges['title'] = $this->sqlTitle();
						$strSql .= ' title = ' . $this->sqlTitle() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' description = ' . $this->sqlDescription() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlDescription() ) != $this->getOriginalValueByFieldName( 'description' ) ) {
						$arrstrOriginalValueChanges['description'] = $this->sqlDescription();
						$strSql .= ' description = ' . $this->sqlDescription() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' file_name = ' . $this->sqlFileName() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlFileName() ) != $this->getOriginalValueByFieldName( 'file_name' ) ) {
						$arrstrOriginalValueChanges['file_name'] = $this->sqlFileName();
						$strSql .= ' file_name = ' . $this->sqlFileName() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' is_exported = ' . $this->sqlIsExported() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlIsExported() ) != $this->getOriginalValueByFieldName( 'is_exported' ) ) {
						$arrstrOriginalValueChanges['is_exported'] = $this->sqlIsExported();
						$strSql .= ' is_exported = ' . $this->sqlIsExported() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' is_published = ' . $this->sqlIsPublished() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlIsPublished() ) != $this->getOriginalValueByFieldName( 'is_published' ) ) {
						$arrstrOriginalValueChanges['is_published'] = $this->sqlIsPublished();
						$strSql .= ' is_published = ' . $this->sqlIsPublished() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' is_shared = ' . $this->sqlIsShared() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlIsShared() ) != $this->getOriginalValueByFieldName( 'is_shared' ) ) {
						$arrstrOriginalValueChanges['is_shared'] = $this->sqlIsShared();
						$strSql .= ' is_shared = ' . $this->sqlIsShared() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' order_num = ' . $this->sqlOrderNum() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlOrderNum() ) != $this->getOriginalValueByFieldName( 'order_num' ) ) {
						$arrstrOriginalValueChanges['order_num'] = $this->sqlOrderNum();
						$strSql .= ' order_num = ' . $this->sqlOrderNum() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlApprovedBy() ) != $this->getOriginalValueByFieldName( 'approved_by' ) ) {
						$arrstrOriginalValueChanges['approved_by'] = $this->sqlApprovedBy();
						$strSql .= ' approved_by = ' . $this->sqlApprovedBy() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlApprovedOn() ) != $this->getOriginalValueByFieldName( 'approved_on' ) ) {
						$arrstrOriginalValueChanges['approved_on'] = $this->sqlApprovedOn();
						$strSql .= ' approved_on = ' . $this->sqlApprovedOn() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' last_updated_by = ' . $this->sqlLastUpdatedBy() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlLastUpdatedBy() ) != $this->getOriginalValueByFieldName( 'last_updated_by' ) ) {
						$arrstrOriginalValueChanges['last_updated_by'] = $this->sqlLastUpdatedBy();
						$strSql .= ' last_updated_by = ' . $this->sqlLastUpdatedBy() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' last_updated_on = ' . $this->sqlLastUpdatedOn() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlLastUpdatedOn() ) != $this->getOriginalValueByFieldName( 'last_updated_on' ) ) {
						$arrstrOriginalValueChanges['last_updated_on'] = $this->sqlLastUpdatedOn();
						$strSql .= ' last_updated_on = ' . $this->sqlLastUpdatedOn() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlDeletedBy() ) != $this->getOriginalValueByFieldName( 'deleted_by' ) ) {
						$arrstrOriginalValueChanges['deleted_by'] = $this->sqlDeletedBy();
						$strSql .= ' deleted_by = ' . $this->sqlDeletedBy() . ',';
						$boolUpdate = true;
					}
					if( false == $this->getAllowDifferentialUpdate() ) {
						$strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ',';
					} elseif( CStrings::reverseSqlFormat( $this->sqlDeletedOn() ) != $this->getOriginalValueByFieldName( 'deleted_on' ) ) {
						$arrstrOriginalValueChanges['deleted_on'] = $this->sqlDeletedOn();
						$strSql .= ' deleted_on = ' . $this->sqlDeletedOn() . ',';
						$boolUpdate = true;
					}
					$strSql .= ' updated_by = ' . ( int ) $intCurrentUserId . ', ';
					$strSql .= ' updated_on = \'NOW()\'
					WHERE
						id = ' . ( int ) $this->sqlId() . ';';

		if( true == $boolReturnSqlOnly ) {
			return ( true == $boolUpdate ) ? $strSql : false;
		} else {
			if( true == $boolUpdate ) {
				if( true == $this->executeSql( $strSql, $this, $objDatabase ) ) {
					if( true == $this->getAllowDifferentialUpdate() ) {
						$this->reSerializeAndSetOriginalValues( $arrstrOriginalValueChanges );
						return true;
					}

					$this->setTitle( $this->getFileName() );
					if( $this->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase ) ) {
						return true;
					}

				} else {
					return false;
				}
			}
			return true;
		}
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSoftDelete = false ) {

		if( false == $boolSoftDelete ) {

			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {

			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );

			return parent::update( $intCurrentUserId, $objDatabase );
		}
		return;
	}

	/**
	 * Other Functions
	 *
	 */

	public function uploadDocument( $arrmixUploadedFileData, $strFilePath, $strAppendString = NULL ) {

		$boolIsValid = true;

		$strFileName = $arrmixUploadedFileData['name'];

		if( 1 > strlen( $strFileName ) ) {

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_file', 'Document is required, please upload a document.' ) );
			$boolIsValid = false;

		} else {

			// Create recursive path directory if path not exist.
			if( false == CFileIo::isDirectory( $strFilePath ) ) {
				if( false == CFileIo::recursiveMakeDir( $strFilePath ) ) {
					$boolIsValid = false;
				}
			}

			$strFileName = CFileUpload::cleanFilename( $strFileName );

			if( true == CFileIo::fileExists( $strFilePath . '/' . $strFileName ) ) {

				$arrmixFileDetails = pathinfo( $strFileName );

				if( 1 > strlen( $arrmixFileDetails['extension'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_file', 'Please upload a valid File.' ) );
					return false;
				}

				$strFileName = $arrmixFileDetails['filename'] . '_' . $strAppendString . '.' . $arrmixFileDetails['extension'];
			}

			if( false == move_uploaded_file( $arrmixUploadedFileData['tmp_name'], $strFilePath . $strFileName ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'document_file', 'Document failed to upload.' ) );
				$boolIsValid = false;
			}

			if( true == $boolIsValid ) {
				$this->setFileName( $strFileName );
			}
		}

		return $boolIsValid;
	}

	public function downloadDocument( $objCompanyUser, $objAdminDatabase, $objClientDatabase ) {

		$objFileExtension = $this->fetchFileExtension( $objAdminDatabase );

		$strFullpath 	  = $this->getFilePath() . '/' . $this->getFileName();

		if( false == CFileIo::fileExists( $strFullpath ) || false == valObj( $objFileExtension, 'CFileExtension', 'MimeType' ) ) {
			return false;
		}

		$objPsDocumentDownload = new CPsDocumentDownload();
		$objPsDocumentDownload->setPsDocumentId( $this->getId() );
		$objPsDocumentDownload->setCid( $objCompanyUser->getCid() );
		$objPsDocumentDownload->setCompanyUserId( $objCompanyUser->getId() );
		$objPsDocumentDownload->setDownloadedOn( date( 'm/d/Y H:i:s' ) );

		$objClientDatabase->begin();

		if( false == $objPsDocumentDownload->insert( $objCompanyUser->getId(), $objClientDatabase ) ) {
			$objClientDatabase->rollback();
			return false;
		}

		$objClientDatabase->commit();

		header( 'Pragma: public' );
		header( 'Expires: 0' );
		header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control: public' );
		header( 'Content-Description: File Transfer' );
		header( 'Content-Length: ' . CFileIo::getFileSize( $strFullpath ) );
		header( 'Content-Disposition: attachment; filename="' . $this->getFileName() . '"' );
		header( 'Content-type: ' . $objFileExtension->getMimeType() );
		header( 'Content-Transfer-Encoding:binary' );

		echo CFileIo::fileGetContents( $strFullpath );
		exit();
	}

	public static function fetchPsAndPolicyDocumentById( $intId, $objEmployee,$objAdminDatabase ) {

		if( true == valObj( $objEmployee, 'CEmployee' ) ) {
			$strJoinCondition = ' LEFT JOIN policies p ON ( p.ps_document_id = pd.id )
								  LEFT JOIN employee_policies ep ON ( ep.policy_id = p.id)
								  LEFT JOIN employee_policy_associations epa ON (  p.id = epa.policy_id AND ' . $objEmployee->getDesignationId() . ' = ANY ( epa.designation_ids ) )';
			if( CCountry::CODE_USA == $objEmployee->getCountryCode() ) {
				$strJoinCondition .= 'LEFT JOIN employee_addresses ea ON ( ep.employee_id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ')';
				$strJoinCondition .= ' AND 1 =
												CASE
													WHEN epa.state_codes IS NOT NULL AND ea.state_code = ANY( epa.state_codes ) THEN 1
													WHEN epa.state_codes IS NULL THEN 1
												ELSE 0
										END ';
			}
		}
		$strSql = ' SELECT
						pd.*
					FROM
						ps_documents pd
						' . $strJoinCondition . '
					WHERE pd.id = ' . ( int ) $intId . '
					AND ( ( pd.employee_id = ' . ( int ) $objEmployee->getId() . '  )
							OR ( pd.employee_id IS NULL AND ep.id IS NOT NULL AND ep.employee_id = ' . ( int ) $objEmployee->getId() . ' ) )';

		return self::fetchPsDocument( $strSql, $objAdminDatabase );
	}

	public function setMountsPath( $strMountsPath ) {
		$this->m_strMountsPath = $strMountsPath;
	}

	public function getMountsPath() {
		return $this->m_strMountsPath;
	}

	public function setUniqueDocumentId( $strUniqueId ) {
		$this->m_strUniqueId = $strUniqueId;
	}

	public function getUniqueDocumentId() {
		return $this->m_strUniqueId;
	}

	public function getUniqueFileName( $strOldFileName ) {

		$strFileInfo = pathinfo( CFileUpload::cleanFilename( $strOldFileName ) );
		$strFileName = strtotime("now") . '_' . $strFileInfo['filename'] . '.' . $strFileInfo['extension'];
		return $strFileName;
	}

	public function uploadObject( $objObjectStorageGateway, $strUploadPath ) {

		if( !is_dir( $strUploadPath ) && !CFileIo::recursiveMakeDir( $strUploadPath ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to create directory path.' ) ) );
			return false;
		}

		$strDocumentContent = CFileIo::fileGetContents( $strUploadPath );
		$arrmixGatewayRequest = $this->createPutGatewayRequest( [ 'data' => $strDocumentContent ] );
		$arrmixGatewayRequest['container'] = $this->calcStorageContainer();

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->putObject( $arrmixGatewayRequest );
		if( $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to put storage object.' ) ) );
			return false;
		}

		$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse );
		return true;
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {
		if( $this->getPsDocumentTypeId() == self::REPORT_DOCUMENT_TYPE  ) {
			$strKeyPrefix = CClient::ID_DEFAULT . '/reports/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getId() . '/';
			$strName	= $this->getId() . '_' . CFileUpload::cleanFilename( $this->getFileName() );

			return $strKeyPrefix . $strName;
		}

		$strDate	= $this->getCreatedOn();
		$intId		= $this->getPsLeadId();
		if( !valId( $intId ) ) {
			$intId = $this->getUniqueDocumentId();
		}

		if( \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM == $strVendor ) {
			if( CPsDocumentType::DOCUMENT_TYPE_REIMBURSEMENT_PROOF == $this->getPsDocumentTypeId() ) {
				return 'reimbursements/' . $this->getId() . '_' . $this->getFileName();
			}

			if( true == valId( $this->getEmployeeId() ) && true == in_array( $this->getPsDocumentTypeId(), CPsDocumentType::$c_arrintEmployeeDocumentTypeIds ) ) {
				return 'employees/' . $this->getEmployeeId() . '/' . $this->getFileName();
			}

			if( $this->getPsDocumentTypeId() == CPsDocumentType::DOCUMENT_TYPE_RESIGNATION_LETTER ) {
				return 'employees/resignation_letter/' . $this->getFileName();
			}

			if( true == valId( $this->getId() ) && $this->getPsDocumentTypeId() == CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_POLICY ) {
				return 'employee_policy/' . $this->getFileName();
			}

			if( $this->getPsDocumentTypeId() == CPsDocumentType::EMPLOYEE_APPLICATION_PROFILE_PHOTO ) {
				return $this->getFilePath() . $this->getEmployeeApplicationId() . '.jpg';
			}

			return $this->getFilePath() . $this->getFileName();
		}

		switch( $this->getPsDocumentTypeId() ) {
			case \CPsDocumentType::DOCUMENT_TYPE_SUCESS_REVIEW:
				$intCid = $this->getCid();
				if( !valId( $this->getCid() ) ) {
					$intCid = 1;
				}

				return sprintf( '%d/%s%d/%d/%d/%s/', $intCid, 'ps_leads/activity/', date( 'Y' ), date( 'm' ), date( 'd' ), $intId ) . $this->getFileName();
				break;

			case \CPsDocumentType::DOCUMENT_TYPE_SUPPORT_DOCUMENT:
				return sprintf( '%d/%s%d/%d/%d/%s/', $this->getCid(), 'ps_leads/', date( 'Y' ), date( 'm' ), date( 'd' ), $intId ) . $this->getFileName();
				break;

			case \CPsDocumentType::DOCUMENT_TYPE_PS_ASSETS:
				$intDate		= $this->getCreatedOn();
				return 'IT/asset_invoices/' . date( 'Y', strtotime( $intDate ) ) . '/' . date( 'm', strtotime( $intDate ) ) . '/' . date( 'd', strtotime( $intDate ) ) . '/' . $this->getId() . '/' . $this->getFileName();
				break;

			case CPsDocumentType::DOCUMENT_TYPE_REIMBURSEMENT_PROOF:
				return sprintf( '%d/%s%d/%d/%d/%d/', CClient::ID_DEFAULT, 'expenses/reimbursements/', date( 'Y', strtotime( $strDate ) ), date( 'm', strtotime( $strDate ) ), date( 'd', strtotime( $strDate ) ), $this->getId() ) . $this->getFileName();
				break;

			case CPsDocumentType::DOCUMENT_TYPE_RESIGNATION_LETTER:
				return sprintf( '%d/%s/%d/%d/%d/%02d/%02d/', CClient::ID_DEFAULT, 'employee_documents/resignation_letters', $this->getEmployeeId(), $this->getPsDocumentTypeId(), date( 'Y' ), date( 'm' ), date( 'd' ) ) . $this->getFileName();
				break;

			case CPsDocumentType::DOCUMENT_TYPE_PS_NOTIFICATION:
				return 'ps_notifications/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getId() . '/' . $this->getFileName();
				break;

			case CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_POLICY:
				if( is_numeric( $this->getId() ) ) {
					return sprintf( '%d/%s%d/%d/%d/%d/', CClient::ID_DEFAULT, 'employee_documents/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getId() ) . $this->getFileName();
					break;
				}

            case CPsDocumentType::DOCUMENT_TYPE_EMPLOYEE_CERTIFICATE:
                return sprintf( '%d/%s%d/%02d/%02d/%d/%d/', CClient::ID_DEFAULT, 'employee_documents/certifications/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getEmployeeId(), $this->getPsDocumentTypeId() ) . $this->getFileName();
                break;

			default:
				if( true == valId( $this->getEmployeeId() ) && true == in_array( $this->getPsDocumentTypeId(), CPsDocumentType::$c_arrintEmployeeDocumentTypeIds ) ) {
					return sprintf( '%d/%s%d/%d/%d/%d/', CClient::ID_DEFAULT, 'employee_documents/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getEmployeeId() ) . $this->getFileName();
				}

				if( true === valId( $this->getEmployeeApplicationId() ) && true === in_array( $this->getPsDocumentTypeId(), CPsDocumentType::$c_arrintEmployeeApplicationDocumentTypeIds ) ) {
					return sprintf( '%d/%s/%d/%d/%d/%02d/%02d/%d/', CClient::ID_DEFAULT, 'employee_application_documents', $this->getEmployeeApplicationId(), $this->getPsDocumentTypeId(), date( 'Y' ), date( 'm' ), date( 'd' ), $this->getId() ) . $this->getFileName();
				}

				if( array_key_exists( $this->getPsDocumentTypeId(), CPsDocumentType::$c_arrmixProductManagementDocument ) ) {
					$strFolder = str_replace( ' ', '_', strtolower( CPsDocumentType::$c_arrmixProductManagementDocument[$this->getPsDocumentTypeId()] ) );
					$strKeyPrefix = CClient::ID_DEFAULT . '/product_management/' . $strFolder . '/' . date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' . $this->getId() . '/';
					$strName	= CFileUpload::cleanFilename( $this->getFileName() );
					return $strKeyPrefix . $strName;
				}

				if( in_array( $this->getPsDocumentTypeId(), CPsDocumentType::$c_arrintPsLeadDocumentTypeIds ) ) {
					$strDate	= ( !\valStr( $this->getCreatedOn() ) ) ? date( 'Y-m-d' ) : $this->getCreatedOn();
					return $this->getCid() . '/ps_leads/' . date( 'Y', strtotime( $strDate ) ) . '/' . date( 'm', strtotime( $strDate ) ) . '/' . date( 'd', strtotime( $strDate ) ) . '/' . $intId . '/' . $this->getFileName();
				}

				return sprintf( '%d/%s%d/%d/%d/%s/%s/', $this->getCid(), 'ps_leads/', date( 'Y' ), date( 'm' ), date( 'd' ), date( 'm', strtotime( date( 'Y-m-d' ) ) ), $intId ) . $this->getFileName();
		}

		return false;
	}

	protected function calcStorageContainer( $strVendor = NULL ) {

		if( array_key_exists( $this->getPsDocumentTypeId(), CPsDocumentType::$c_arrmixProductManagementDocument ) || in_array( $this->getPsDocumentTypeId(), [ CPsDocumentType::DOCUMENT_TYPE_PS_ASSETS, self::REPORT_DOCUMENT_TYPE ] ) ) {
			return CConfig::get( 'OSG_BUCKET_SYSTEM_DOCUMENTS' );
		}

		switch( $strVendor ) {
			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				if( true == is_numeric( $this->getEmployeeId() ) && true == in_array( $this->getPsDocumentTypeId(), CPsDocumentType::$c_arrintEmployeeDocumentTypeIds ) ) {
					return 'documents/';
				}

				if( ( CPsDocumentType::DOCUMENT_TYPE_REIMBURSEMENT_PROOF == $this->getPsDocumentTypeId() ) || ( true == is_numeric( $this->getEmployeeApplicationId() ) && true === in_array( $this->getPsDocumentTypeId(), CPsDocumentType::$c_arrintEmployeeApplicationDocumentTypeIds ) ) ) {
					return 'Global/';
				}

				return PATH_MOUNTS_DOCUMENTS;
				break;

			default:
				if( ( true === valId( $this->getEmployeeId() ) && true == in_array( $this->getPsDocumentTypeId(), CPsDocumentType::$c_arrintEmployeeDocumentTypeIds ) ) || ( true === valId( $this->getEmployeeApplicationId() ) && true === in_array( $this->getPsDocumentTypeId(), CPsDocumentType::$c_arrintEmployeeApplicationDocumentTypeIds ) ) ) {
					return CONFIG_OSG_BUCKET_SYSTEM_EMPLOYEES;
				}
				if( CPsDocumentType::DOCUMENT_TYPE_REIMBURSEMENT_PROOF == $this->getPsDocumentTypeId() || CPsDocumentType::DOCUMENT_TYPE_RESIGNATION_LETTER == $this->getPsDocumentTypeId() ) {
					return CConfig::get( 'OSG_BUCKET_SYSTEM_EMPLOYEES' );
				}

				if( CPsDocumentType::DOCUMENT_TYPE_PS_NOTIFICATION == $this->getPsDocumentTypeId() ) {
					return CConfig::get( 'OSG_BUCKET_SYSTEM_DOCUMENTS' );
				}

				return CONFIG_OSG_BUCKET_DOCUMENTS;
		}
	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

	public function getDocumentFromCloud( $objObjectStorageGateway ) {

		$strCloudPath = $this->getFilePath() . $this->getFileName();

		$strFilePath = $this->getFilePath();

		$this->setFilePath( $strCloudPath );
		$arrmixStorageArgs = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest( [ 'outputFile' => 'temp' ] );

		$this->setFilePath( $strFilePath );
		return $objObjectStorageGateway->getObject( $arrmixStorageArgs );
	}

	public function downloadObject( $objObjectStorageGateway, $strFileName, $strDispositionType, $boolIsFullPath = false, $strReferenceTag = NULL ) {

		$arrmixStorageArgs = $this->fetchStoredObject( $this->m_objDatabase, $strReferenceTag )->createGatewayRequest( [ 'checkExists' => true ] );
		unset( $arrmixStorageArgs['cid'] );
		if( $arrmixStorageArgs['checkExists'] ) {
			unset( $arrmixStorageArgs['checkExists'] );
			$arrmixStorageArgs['outputFile'] = 'temp';
			$arrobjObjectStorageResponse = $objObjectStorageGateway->getObject( $arrmixStorageArgs );
			if( $arrobjObjectStorageResponse->hasErrors() ) {
				return false;
			}

			$strFullPath = $arrobjObjectStorageResponse['outputFile'];
		}

		if( $boolIsFullPath ) {
			return $strFullPath;
		}

		$objFileExtension = $this->fetchFileExtension( $this->m_objDatabase );
		$strContentType = ' application/pdf';
		if( valObj( $objFileExtension, 'CFileExtension' ) ) {
			$strContentType = $objFileExtension->getMimeType();
		}

		header( 'Pragma:public' );
		header( 'Expires:0' );
		header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control:public' );
		header( 'Content-Description:File Transfer' );
		header( 'Content-Type: ' . $strContentType );
		header( 'Content-Disposition: ' . $strDispositionType . '; filename="' . $strFileName . '"' );
		echo CFileIo::fileGetContents( $strFullPath );
		exit();
	}

	public function deleteObject( $objObjectStorageGateway, $intUserId ) {

		$arrmixGatewayRequest         = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest();
		$objDeleteObjectResponse      = $objObjectStorageGateway->deleteObject( $arrmixGatewayRequest );
		if( valObj( $objDeleteObjectResponse, 'CRouterObjectStorageGateway' ) && $objDeleteObjectResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to remove the file.' ) );
			return false;
		}

		if( !$this->deleteStoredObject( $intUserId, $this->m_objDatabase ) ) {
			return false;
		}

		return true;
	}

	public function putTempObject( $objTempHelper, $strFileName, $intCid, $strTempName ) {
		try {
			$strNewFileName                  = $this->getUniqueFileName( CFileUpload::cleanFilename( $strFileName ) );
			$arrmixRequest                   = [];
			$arrmixRequest['data']           = CFileIo::fileGetContents( $strTempName );
			$arrmixRequest['key']            = sprintf( '%d/%s/%d/%d/%d/%s', $intCid, 'documents', date( 'Y' ), date( 'm' ), date( 'd' ), $strNewFileName );
			$objObjectStorageGatewayResponse = $objTempHelper->putObject( $arrmixRequest );
			if( !$objObjectStorageGatewayResponse->hasErrors() ) {
				return $strNewFileName;
			}
		} catch( \Exception $objException ) {
			return false;
		}
	}

	public function getTempObject( $objTempHelper, $strFileName, $intCid, $strDispositionType, $boolIsFullPath = false ) {
		try {
			$arrmixRequest                   = [];
			$arrmixRequest['key']            = sprintf( '%d/%s/%d/%d/%d/%s', $intCid, 'documents', date( 'Y' ), date( 'm' ), date( 'd' ), $strFileName );
			$arrmixRequest['outputFile']     = 'temp';
			$objObjectStorageGatewayResponse = $objTempHelper->getObject( $arrmixRequest );
			if( false == $objObjectStorageGatewayResponse->hasErrors() ) {
				$strTempFilePath = $objObjectStorageGatewayResponse['outputFile'];
				if( $boolIsFullPath ) {
					return $strTempFilePath;
				}
				$objFileExtension = $this->fetchFileExtension( $this->m_objDatabase );
				$strContentType   = ' application/pdf';
				if( valObj( $objFileExtension, 'CFileExtension' ) ) {
					$strContentType = $objFileExtension->getMimeType();
				}

				header( 'Pragma:public' );
				header( 'Expires:0' );
				header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
				header( 'Cache-Control:public' );
				header( 'Content-Description:File Transfer' );
				header( 'Content-Type: ' . $strContentType );
				header( 'Content-Disposition: ' . $strDispositionType . '; filename="' . $strFileName . '"' );
				echo CFileIo::fileGetContents( $strTempFilePath );
				exit();
			}
		} catch( \Exception $objException ) {
			return false;
		}
	}

}
?>