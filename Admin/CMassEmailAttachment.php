<?php

use Psi\Eos\Admin\CMassEmailAttachments;
use Psi\Libraries\ExternalFileUpload\CFileUpload;
use Psi\Eos\Admin\CFileExtensions;

class CMassEmailAttachment extends CBaseMassEmailAttachment {

	use Psi\Libraries\EosFoundation\TEosStoredObject;

	const ACTION_UPDATE 		= 3;

	protected $m_intFileSize;
	protected $m_intActionId;
	protected $m_intCid;

	protected $m_strTempFileName;
	protected $m_strFileError;
	protected $m_strTitle;

	/**
	 * Get Functions
	 */

	public function getTempFileName() {
		return $this->m_strTempFileName;
	}

	public function getFileSize() {
		return $this->m_intFileSize;
	}

	public function getFileError() {
		return $this->m_strFileError;
	}

	public function getActionId() {
		return $this->m_intActionId;
	}

	public function getFileFullPath( $boolIsWithFileName = false ) {

		if( true == $boolIsWithFileName ) {
			return PATH_MOUNTS_GLOBAL_MASS_EMAIL_ATTACHMENTS . $this->getFilePath() . $this->getFileName();
		}

		return PATH_MOUNTS_GLOBAL_MASS_EMAIL_ATTACHMENTS . $this->getFilePath();
	}

	/**
	 * Set Functions
	 */

	public function setTempFileName( $strTempFileName ) {
		$this->m_strTempFileName = $strTempFileName;
	}

	public function setFileSize( $intFileSize ) {
		$this->m_intFileSize = $intFileSize;
	}

	public function setFileError( $strFileError ) {
		$this->m_strFileError = $strFileError;
	}

	public function setActionId( $intActionId ) {
		$this->m_intActionId = $intActionId;
	}

	public function setFileRequestData( $arrstrFile, $strTempFilePath ) {

		$this->setTempFileName( $strTempFilePath );
		$this->setFileName( $this->getUniqueFileName( $arrstrFile['basename'] ) );
		$this->setFilePath( date( 'Y' ) . '/' . date( 'm' ) . '/' . date( 'd' ) . '/' );
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['action_id'] ) ) 				$this->setActionId( $arrmixValues['action_id'] );
	}

	public function getUniqueFileName( $strOldFileName ) {

		$strFileInfo = pathinfo( CFileUpload::cleanFilename( $strOldFileName ) );
		$strFileName = strtotime( 'now' ) . '_' . $strFileInfo['filename'] . '.' . $strFileInfo['extension'];
		return $strFileName;
	}

	public function getTitle() {
		return $this->m_strFileName;
	}

	public function getName() {
		return $this->m_strFileName;
	}

	public function getBatchName() {
		return $this->m_strFileName;
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchMassEmailAttachment( $objDatabase ) {
		return CMassEmailAttachments::createService()->fetchMassEmailAttachmentById( $this->getId(), $objDatabase );
	}

	/**
	 * Create Functions
	 */

	public function createEmailAttachment( $objDatabase, $objObjectStorageGateway = NULL ) {

		$objEmailAttachment = new CEmailAttachment();
		$objEmailAttachment->setEmailAttachmentFile( 'mass_email', $objDatabase, $this->getFileFullPath( true ) );
		$objEmailAttachment->setSourceFilePath( $this->getFileFullPath( true ) );

		$this->setCid( CClient::ID_DEFAULT );

		if( valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			$strFileFullPath = $this->downloadObject( $objObjectStorageGateway, $this->getFileName(), 'inline', true );
			$objEmailAttachment->setFilePath( dirname( $strFileFullPath ) );
			$objEmailAttachment->setFileName( basename( $strFileFullPath ) );
		}

		$objEmailAttachment->setCreatedOn( date( 'Y/m/d' ) );

		return $objEmailAttachment;
	}

	/**
	 * Database Functions
	 */

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolClone = false ) {

		$boolIsValid	= true;
		$this->setId( $this->fetchNextId( $objDatabase ) );

		$this->setFileName( $this->getId() . '_' . CFileUpload::cleanFilename( $this->getFileName() ) );

		$boolIsValid 	= parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( $boolIsValid ) {
			$boolIsValid = $this->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase );
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		$boolIsValid 		= true;

		$objMassEmailAttachment	= $this->fetchMassEmailAttachment( $objDatabase );

		if( file_exists( $objMassEmailAttachment->getFileFullPath( true ) ) ) {
			unlink( $objMassEmailAttachment->getFileFullPath( true ) );
		}

		$this->setFileName( $this->getId() . '_' . CFileUpload::cleanFilename( $this->getFileName() ) );

		$boolIsValid 	= parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		if( $boolIsValid ) {
			$boolIsValid = $this->insertOrUpdateStoredObject( $intCurrentUserId, $objDatabase );
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

	/**
	 * Other Functions
	 */

	public function setCid( $intCid ) {
		$this->set( 'm_intCid', CStrings::strToIntDef( $intCid, NULL, false ) );
	}

	public function getCid() {
		return $this->m_intCid;
	}

	public function uploadObject( $objObjectStorageGateway ) {

		if( !valObj( $objObjectStorageGateway, 'IObjectStorageGateway' ) ) {
			trigger_error( __( 'Failed to get IObjectStorage' ), E_USER_WARNING );
			return false;
		}

		$arrmixGatewayRequest = $this->createPutGatewayRequest( [ 'data' => CFileIo::fileGetContents( $this->getTempFileName() ) ] );

		$objObjectStorageGatewayResponse = $objObjectStorageGateway->putObject( $arrmixGatewayRequest );

		if( $objObjectStorageGatewayResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'Failed to put storage object.' ) ) );
			return false;
		}

		$this->setObjectStorageGatewayResponse( $objObjectStorageGatewayResponse );
		return true;
	}

	protected function calcStorageKey( $strReferenceTag = NULL, $strVendor = NULL ) {

		switch( $strVendor ) {

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				$strPath = '../Global/mass_email_attachments/' . $this->getFilePath();
				break;

			default:
				$strPath = sprintf( '%d/%s%d/%d/%d/%d/', $this->getCid(), 'mass_email_attachments/', date( 'Y' ), date( 'm' ), date( 'd' ), $this->getId() );
		}

		return $strPath . $this->getFileName();
	}

	protected function calcStorageContainer( $strVendor = NULL ) {

		switch( $strVendor ) {

			case \CObjectStorageGatewayFactory::OBJECT_STORAGE_GATEWAY_SHARED_FILE_SYSTEM:
				return PATH_MOUNTS_DOCUMENTS;

			default:
				return CONFIG_OSG_BUCKET_DOCUMENTS;

		}

	}

	protected function createStoredObject() {
		return new \CStoredObject();
	}

	protected function fetchStoredObjectFromDb( $objDatabase, $strReferenceTag = NULL ) {
		return \Psi\Eos\Entrata\CStoredObjects::createService()->fetchStoredObjectByCidByReferenceIdTableTag( $this->getCid(), $this->getId(), static::TABLE_NAME, $strReferenceTag, $objDatabase );
	}

	public function downloadObject( $objObjectStorageGateway, $strFileName, $strDispositionType, $boolIsFullPath = false ) {

		$arrmixStorageArgs = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest( [ 'outputFile' => 'temp' ] );

		$arrobjObjectStorageResponse = $objObjectStorageGateway->getObject( $arrmixStorageArgs );
		if( $arrobjObjectStorageResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, __( 'File is not present or has been deleted.' ) ) );
			return false;
		}

		$strFullPath = $arrobjObjectStorageResponse['outputFile'];

		if( $boolIsFullPath ) {
			return $strFullPath;
		}

		$arrstrFileInfo = pathinfo( $strFullPath );

		if( valArr( $arrstrFileInfo ) && !is_null( $arrstrFileInfo['extension'] ) ) {
			$objFileExtension = CFileExtensions::createService()->fetchFileExtensionByExtension( $arrstrFileInfo['extension'], $this->m_objDatabase );
		}

		header( 'Pragma:public' );
		header( 'Expires:0' );
		header( 'Cache-Control:must-revalidate, post-check=0, pre-check=0' );
		header( 'Cache-Control:public' );
		header( 'Content-Description:File Transfer' );
		header( 'Content-type: ' . $objFileExtension->getMimeType() );
		header( 'Content-Disposition: ' . $strDispositionType . '; filename="' . $strFileName . '"' );
		echo CFileIo::fileGetContents( $strFullPath );
		exit();
	}

	public function deleteObject( $objObjectStorageGateway, $intUserId ) {

		$arrmixGatewayRequest         = $this->fetchStoredObject( $this->m_objDatabase )->createGatewayRequest();
		$objDeleteObjectResponse      = $objObjectStorageGateway->deleteObject( $arrmixGatewayRequest );

		if( valObj( $objDeleteObjectResponse, 'CRouterObjectStorageGateway' ) && $objDeleteObjectResponse->hasErrors() ) {
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, 'Unable to remove the file.' ) );
			return false;
		}

		if( !$this->deleteStoredObject( $intUserId, $this->m_objDatabase ) ) {
			return false;
		}

		return true;
	}

}
?>