<?php

class CProjectRequirement extends CBaseProjectRequirement {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductOptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPriority() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRequirementType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSource() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStatus() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTagIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsDocumentIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>