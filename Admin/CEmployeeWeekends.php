<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeWeekends
 * Do not add any new functions to this class.
 */

class CEmployeeWeekends extends CBaseEmployeeWeekends {

	public static function fetchEmployeeWeekendsByEmployeeIds( $objDatabase, $arrintEmployeeIds = NULL, $strSelectedYear = NULL ) {

		$strWhereCondition = '';
		if( true == valArr( $arrintEmployeeIds ) ) {
			$strWhereCondition .= ' AND ( ew.employee_id IS NULL OR ew.employee_id IN (' . implode( ',', $arrintEmployeeIds ) . ') )';
		} else {
			$strWhereCondition .= ' AND ew.employee_id IS NULL';
		}

		if( true == valStr( $strSelectedYear ) ) {
				$strWhereCondition .= ' AND to_char( ew.month, \'YYYY\' ) BETWEEN \'' . ( $strSelectedYear - 1 ) . '\' AND \'' . $strSelectedYear . '\'';
		}

		$strSql = 'SELECT
						ew.employee_id,
						ew.support_data ->> \'weekends\' as weekends,
						ew.month,
						e.preferred_name
					FROM
						employee_weekends ew
						LEFT JOIN employees e ON ( ew.employee_id = e.id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ')
		 			WHERE
		 				ew.id IS NOT NULL'
						. $strWhereCondition;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeWeekendsByDatesByEmployeeIds( $arrstrDate, $objDatabase, $arrintEmployeeIds = NULL ) {
		if( false == valArr( $arrstrDate ) ) return false;

		if( true == valArr( $arrintEmployeeIds ) ) {
			$strWhereClause = ' AND employee_id IN (' . implode( ',', $arrintEmployeeIds ) . ')';
		} else {
			$strWhereClause = ' AND employee_id IS NULL';
		}

		$strSql = 'SELECT
						*
					FROM
						employee_weekends
		 			WHERE
						month IN (\'' . implode( "','", $arrstrDate ) . '\')
						' . $strWhereClause . '';

		return self::fetchEmployeeWeekends( $strSql, $objDatabase );
	}

	public static function fetchAllEmployeesWeekends( $objDatabase ) {

		$strSql = 'SELECT
						id,
						support_data ->> \'weekends\' as weekends
					FROM
						employee_weekends
		 			WHERE
		 				employee_id IS NULL
		 			ORDER BY
		 				month DESC';

		return fetchData( $strSql, $objDatabase );
	}

}
?>
