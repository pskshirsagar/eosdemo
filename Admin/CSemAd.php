<?php

class CSemAd extends CBaseSemAd {

	public function createSemAdSource() {

		$objSemAd = new CSemAdSource();

		$objSemAd->setSemAdGroupId( $this->getSemAdGroupId() );
		$objSemAd->setSemAdStatusTypeId( CSemAdStatusType::APPROVED );
		$objSemAd->setSemStatusTypeId( CSemStatusType::ENABLED );

		return $objSemAd;

	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchSemAdSources( $objAdminDatabase ) {
		return CSemAdSources::fetchSemAdSourcesBySemAdId( $this->getId(), $objAdminDatabase );
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valSemCampaignId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemCampaignId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_campaign_id', 'Sem campaign ID is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSemAdGroupId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemAdGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_ad_group_id', 'Sem ad group ID is reauired.' ) );
		}

		return $boolIsValid;
	}

	public function valTitle( $objDatabase = NULL ) {

		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required.' ) );

		} elseif( true == valObj( $objDatabase, 'CDatabase' ) ) {
			if( 25 < strlen( preg_replace( '/^{KeyWord\:(.*)\}/', '\1', $this->getTitle() ) ) ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title length exceeds limit of 25 characters.' ) );
				return false;
			}

			$intCountCompetingSemAd = CSemAds::fetchCompetingSemAdBySemAdGroupIdByTitleByNonCompetingSemAdId( $this->getSemAdGroupId(), $this->getTitle(), $this->getId(), $objDatabase );

			if( 0 < $intCountCompetingSemAd ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'username', 'Title already in use.' ) );
				return $boolIsValid;
			}
		}

		return $boolIsValid;
	}

	public function valDescriptionLine1() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescriptionLine1() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description_line1', 'Description line1 is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescriptionLine2() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescriptionLine2() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description_line2', 'Description line2 is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDisplayUrl() {

		$boolIsValid = true;

		if( true == is_null( $this->getDisplayUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'display_url', 'A valid display URL is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDestinationUrl() {

		$boolIsValid = true;

		if( true == is_null( $this->getDestinationUrl() ) || false == CValidation::checkUrl( $this->getDestinationUrl() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'destination_url', 'A valid destination URL is required.' ) );
		}

		return $boolIsValid;
	}

	public function valAdDatetime() {
		$boolIsValid = true;

		if( true == is_null( $this->getAdDatetime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ad_datetime', 'Ad datetime is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsSystem() {
		$boolIsValid = true;

		if( true == is_null( $this->getIsSystem() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'is_system', 'Is system is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSemCampaignId();
				$boolIsValid &= $this->valTitle( $objDatabase );
				$boolIsValid &= $this->valDescriptionLine1();
				$boolIsValid &= $this->valDescriptionLine2();
				$boolIsValid &= $this->valDisplayUrl();
				$boolIsValid &= $this->valDestinationUrl();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function setDestinationUrl( $strDestinationUrl ) {
		$this->m_strDestinationUrl = 'http://' . str_replace( 'http://', '',  $strDestinationUrl );
	}

	public function setDisplayUrl( $strDisplayUrl ) {

		$arrmixUrlPieces = parse_url( $strDisplayUrl );

		if( true == valArr( $arrmixUrlPieces ) && true == ( array_key_exists( 'host', $arrmixUrlPieces ) ) ) {
			$this->m_strDisplayUrl = $arrmixUrlPieces['host'];
		} elseif( true == valArr( $arrmixUrlPieces ) && true == ( array_key_exists( 'path', $arrmixUrlPieces ) ) ) {
			$this->m_strDisplayUrl = $arrmixUrlPieces['path'];
		}
	}

	/**
	 * Other Functions
	 *
	 */

	public function delete( $intUserId, $objAdminDatabase, $boolReturnSqlOnly = false ) {

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );

		if( parent::update( $intUserId, $objAdminDatabase, $boolReturnSqlOnly ) ) {
			return true;
		}

		return false;
	}
}
?>