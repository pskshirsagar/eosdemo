<?php

class CDocumentTemplateLibrary extends CBaseDocumentTemplateLibrary {

	protected $m_arrobjDocumentTemplatePlugins;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjDocumentTemplatePlugins = array();

		return;
	}

	public function getDocumentTemplatePlugins() {
		return $this->m_arrobjDocumentTemplatePlugins;
	}

	public function getDocumentPlugins() {
		return $this->m_arrobjDocumentTemplatePlugins;
	}

	public function addDocumentTemplatePlugin( $objDocumentTemplatePlugin ) {
		$this->m_arrobjDocumentTemplatePlugins[$objDocumentTemplatePlugin->getId()] = $objDocumentTemplatePlugin;
	}
}
?>