<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingSessionCompanyGoals
 * Do not add any new functions to this class.
 */

class CTrainingSessionCompanyGoals extends CBaseTrainingSessionCompanyGoals {

	public static function fetchCompanyGoalIdsByTrainingSessionId( $intTrainingSessionId, $objDatabase ) {

		$strSql = '	SELECT
						company_goal_id
					FROM training_session_company_goals
					WHERE
						training_session_id =' . ( int ) $intTrainingSessionId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionCompanyGoalsByCompanyGoalIds( $arrintCompanyGoalIds, $objDatabase ) {
		if( false == valArr( $arrintCompanyGoalIds ) ) {
			return NULL;
		}

		return self::fetchTrainingSessionCompanyGoals( 'SELECT * FROM training_session_company_goals WHERE company_goal_id IN ( ' . implode( ',', $arrintCompanyGoalIds ) . ' ) ', $objDatabase );
	}

	public static function fetchCompanyGoalsByTrainingId( $intTrainingId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM training_session_company_goals
					WHERE
						training_id =' . ( int ) $intTrainingId;

		return parent::fetchTrainingSessionCompanyGoals( $strSql, $objDatabase );
	}

	public static function fetchAllTrainingGoals( $objDatabase ) {
		$strSql = ' SELECT
						*
					FROM
						training_session_company_goals
					WHERE
						training_session_id IS NULL
						AND training_id IS NOT NULL';

		return parent::fetchTrainingSessionCompanyGoals( $strSql, $objDatabase );
	}

}
?>
