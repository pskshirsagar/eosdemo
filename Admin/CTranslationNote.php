<?php

class CTranslationNote extends CBaseTranslationNote {

	private $m_strTranslationKey;
	private $m_strTranslationValue;
	private $m_strEmployeeName;

	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( isset( $arrValues['translation_key'] ) && $boolDirectSet ) {
			$this->set( 'm_strTranslationKey', trim( $arrValues['vendor_batch_id'] ) );
		} elseif( isset( $arrValues['translation_key'] ) ) {
			$this->setTranslationKey( $arrValues['translation_key'] );
		}

		if( isset( $arrValues['translation_value'] ) ) {
			$this->setTranslationValue( $arrValues['translation_value'] );
		}

		if( isset( $arrValues['employee_name'] ) ) {
			$this->setEmployeeName( $arrValues['employee_name'] );
		}

		$this->setAllowDifferentialUpdate( true );
	}

	public function setTranslationKey( $strTranslationKey ) {
		$this->m_strTranslationKey = $strTranslationKey;
	}

	public function setTranslationValue( $strTranslationValue ) {
		$this->m_strTranslationValue = $strTranslationValue;
	}

	public function setEmployeeName( $strEmployeeName ) {
		$this->m_strEmployeeName = $strEmployeeName;
	}

	public function getTranslationKey() {
		return $this->m_strTranslationKey;
	}

	public function getTranslationValue() {
		return $this->m_strTranslationValue;
	}

	public function getEmployeeName() {
		return $this->m_strEmployeeName;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTranslationKeyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLocaleCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNote() {
		$boolIsValid = true;
		if( false == valStr( $this->getNote() ) ) {
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valNote();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>