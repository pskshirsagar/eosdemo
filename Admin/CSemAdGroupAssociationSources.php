<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemAdGroupAssociationSources
 * Do not add any new functions to this class.
 */

class CSemAdGroupAssociationSources extends CBaseSemAdGroupAssociationSources {

	public static function fetchSemAdGroupAssociationSourcesBySemAdGroupAssociationId( $intSemAdGroupAssociationId, $objDatabase ) {
		return self::fetchSemAdGroupAssociationSources( sprintf( 'SELECT * FROM sem_ad_group_association_sources WHERE sem_ad_group_association_id = %d', ( int ) $intSemAdGroupAssociationId ), $objDatabase );
	}
}
?>