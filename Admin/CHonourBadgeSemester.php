<?php

class CHonourBadgeSemester extends CBaseHonourBadgeSemester {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDiscription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublish() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>