<?php

class CPsProductDisplayType extends CBasePsProductDisplayType {

	const SHOW_IN_APP_STORE 	= 1;
	const CLIENT_ADMIN_ONLY 	= 2;
	const HIDE_ALL 				= 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>