<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeInteractionTypes
 * Do not add any new functions to this class.
 */

class CEmployeeInteractionTypes extends CBaseEmployeeInteractionTypes {

	public static function fetchEmployeeInteractionTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CEmployeeInteractionType::class, $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEmployeeInteractionType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CEmployeeInteractionType::class, $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>