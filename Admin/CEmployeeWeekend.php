<?php

class CEmployeeWeekend extends CBaseEmployeeWeekend {

	public function createEmployeeWeekend( $intEmployeeId, $arrstrSelectedDate ) {
		$this->setEmployeeId( $intEmployeeId );
		$this->setSupportData( json_encode( [ 'weekends' => $arrstrSelectedDate ] ) );
		$this->setMonth( getConvertedDateTimeByTimeZoneName( $arrstrSelectedDate[0], 'Y/m', 'Asia/Calcutta' ) . '/01' );
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSuportData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function loadEmployeeWeekends( $strSelectedYear, $objAdminDatabase, $arrintEmployeeIds = NULL, $boolFromOfficeShiftCalendar = false, $boolFromPayroll = false ) {

		$arrmixEmployeesWeekendsDays				= array();

		$arrmixEmployeesWeekends = CEmployeeWeekends::fetchEmployeeWeekendsByEmployeeIds( $objAdminDatabase, $arrintEmployeeIds, $strSelectedYear );

		if( false == valArr( $arrmixEmployeesWeekends ) ) {
			if( true == valArr( $arrintEmployeeIds ) ) {
				foreach( $arrintEmployeeIds as $intEmployeeId ) {
					$arrmixEmployeesWeekendsDays[$intEmployeeId]['weekends']  = array();
					$arrmixEmployeesWeekendsDays[$intEmployeeId]['swap_days'] = array();
				}
			} else {
				$arrmixEmployeesWeekendsDays[NULL]['weekends'][]  = array();
				$arrmixEmployeesWeekendsDays[NULL]['swap_days'][] = array();
			}
		}

		if( true == valArr( $arrmixEmployeesWeekends ) ) {
			$arrstrEmployeeWeekends = array();
			foreach( $arrmixEmployeesWeekends as $arrmixEmployeeWeekend ) {
				$arrstrEmployeeWeekends[$arrmixEmployeeWeekend['employee_id']] = array_merge( ( array ) $arrstrEmployeeWeekends[$arrmixEmployeeWeekend['employee_id']], ( array ) json_decode( $arrmixEmployeeWeekend['weekends'] ) );
			}
		}

		$arrstrAllEmployeeWeekends = $arrstrEmployeeWeekends[NULL];
		unset( $arrstrEmployeeWeekends[NULL] );

		if( true == valArr( $arrstrAllEmployeeWeekends ) ) {
			if( true == valArr( $arrintEmployeeIds ) ) {
				foreach( $arrintEmployeeIds as $intEmployeeId ) {
					foreach( $arrstrAllEmployeeWeekends as $arrstrAllEmployeeWeekend ) {
						$arrmixEmployeesWeekendsDays[$intEmployeeId]['weekends'][]  = ( true == $boolFromPayroll ) ? strtotime( $arrstrAllEmployeeWeekend->week_off ) : $arrstrAllEmployeeWeekend->week_off;
						$arrmixEmployeesWeekendsDays[$intEmployeeId]['swap_days'][] = ( true == $boolFromPayroll ) ? strtotime( $arrstrAllEmployeeWeekend->swap_day ) : $arrstrAllEmployeeWeekend->swap_day;
					}
				}
			} else {
				foreach( $arrstrAllEmployeeWeekends as $arrstrAllEmployeeWeekend ) {
					$arrmixEmployeesWeekendsDays[NULL]['weekends'][]  = ( true == $boolFromPayroll ) ? strtotime( $arrstrAllEmployeeWeekend->week_off ) : $arrstrAllEmployeeWeekend->week_off;
					$arrmixEmployeesWeekendsDays[NULL]['swap_days'][] = ( true == $boolFromPayroll ) ? strtotime( $arrstrAllEmployeeWeekend->swap_day ) : $arrstrAllEmployeeWeekend->swap_day;
				}
			}
		}

		if( true == valArr( $arrstrEmployeeWeekends ) ) {
			foreach( $arrstrEmployeeWeekends as $intIndex => $arrstrEmployeeWeekend ) {
				foreach( $arrstrEmployeeWeekend as $strEmployeeWeekend ) {
					$strWeekendDate	= date( 'm/d/Y', strtotime( $strEmployeeWeekend ) );
					$arrmixEmployeesWeekendsDays[$intIndex]['weekends'][]	= ( true == $boolFromPayroll ) ? strtotime( $strWeekendDate ) : date( 'm/d/Y', strtotime( $strWeekendDate ) );

					if( CEmployeeVacationRequest::DAY_OF_WEEK_SUNDAY != date( 'w', strtotime( $strEmployeeWeekend ) ) ) {
						if( true == in_array( ( true == $boolFromPayroll ) ? strtotime( 'next Saturday', strtotime( $strWeekendDate ) ) : date( 'm/d/Y', strtotime( 'next Saturday', strtotime( $strWeekendDate ) ) ), ( array ) $arrmixEmployeesWeekendsDays[$intIndex]['swap_days'] ) || CEmployeeVacationRequest::DAY_OF_WEEK_SATURDAY == date( 'w', strtotime( $strEmployeeWeekend ) ) ) {
							$arrmixEmployeesWeekendsDays[$intIndex]['swap_days'][] = ( true == $boolFromPayroll ) ? strtotime( 'next Sunday', strtotime( $strWeekendDate ) ) : date( 'm/d/Y', strtotime( 'next Sunday', strtotime( $strWeekendDate ) ) );
						} elseif( false == in_array( ( true == $boolFromPayroll ) ? strtotime( 'next Saturday', strtotime( $strWeekendDate ) ) : date( 'm/d/Y', strtotime( 'next Saturday', strtotime( $strWeekendDate ) ) ), $arrmixEmployeesWeekendsDays[$intIndex]['weekends'] ) ) {
							$arrmixEmployeesWeekendsDays[$intIndex]['swap_days'][] = ( true == $boolFromPayroll ) ? strtotime( 'next Saturday', strtotime( $strWeekendDate ) ) : date( 'm/d/Y', strtotime( 'next Saturday', strtotime( $strWeekendDate ) ) );
						}
					}
					if( true == in_array( ( true == $boolFromPayroll ) ? strtotime( $strWeekendDate ) : $strWeekendDate, ( array ) $arrmixEmployeesWeekendsDays[$intIndex]['swap_days'] ) && CEmployeeVacationRequest::DAY_OF_WEEK_SATURDAY == date( 'w', strtotime( $strEmployeeWeekend ) ) ) {
						unset( $arrmixEmployeesWeekendsDays[$intIndex]['swap_days'][( true == $boolFromPayroll ) ? strtotime( $strWeekendDate ) : $strWeekendDate] );
					}
				}
			}
		}

		return ( false == $boolFromPayroll && false == $boolFromOfficeShiftCalendar ) ? array_shift( $arrmixEmployeesWeekendsDays ) : $arrmixEmployeesWeekendsDays;
	}

}
?>