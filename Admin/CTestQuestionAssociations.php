<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestQuestionAssociations
 * Do not add any new functions to this class.
 */

class CTestQuestionAssociations extends CBaseTestQuestionAssociations {

	public static function fetchTestQuestionAssociationCountByTestId( $intTestId, $objDatabase ) {
		return self::fetchTestQuestionAssociationCount( 'WHERE test_id = ' . ( int ) $intTestId, $objDatabase );
	}

	public static function fetchTestQuestionIdsByTestId( $intTestId, $objDatabase ) {

		$strSql = ' SELECT
						tq.id
					FROM
						test_question_associations tqa
						LEFT JOIN test_questions tq ON ( tq.id = tqa.test_question_id )
					WHERE
						tq.deleted_on IS NULL
						AND tq.is_published = 1
						AND tqa.test_id = ' . ( int ) $intTestId;
		return self::fetchTestQuestionAssociations( $strSql, $objDatabase );
	}

	public static function fetchTestQuestionAssociationsByTestQuestionIds( $arrintTestQuestionIds, $objDatabase, $intTestId = NULL ) {
		if( false == valArr( $arrintTestQuestionIds ) ) {
			return NULL;
		}

		$strWhere = NULL;

		if( false == empty( $intTestId ) ) {
			$strWhere = ' AND test_id = ' . ( int ) $intTestId;
		}

		$strSql = ' SELECT
						*
					FROM
						test_question_associations
					WHERE
						test_question_id IN ( ' . implode( ',', $arrintTestQuestionIds ) . ' )' . $strWhere;

		return self::fetchTestQuestionAssociations( $strSql, $objDatabase );
	}

	public static function fetchTestQuestionTypeByTestId( $intTestId, $objDatabase ) {

		$strSql = ' SELECT
						count( DISTINCT tq.id )
					FROM
						test_question_associations tqa
						LEFT JOIN test_questions tq ON ( tq.id = tqa.test_question_id )
					WHERE
						tq.deleted_on IS NULL
						AND tq.is_published = 1
						AND tq.test_question_type_id = ' . CTestQuestionType::AUDIO_ANSWERS . '
						AND tqa.test_id = ' . ( int ) $intTestId;

		$arrmixResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrmixResponse[0]['count'] ) ) {
			return $arrmixResponse[0]['count'];
		}

		return 0;
	}

}
?>