<?php

class CProjectAttributeType extends CBaseProjectAttributeType {
	const PROBLEM_TO_SOLVE		= 1;
	const CROSS_PRODUCT_REQUEST	= 2;
	const QUESTIONS_TO_ANSWER	= 3;
	const DECISION_LOG			= 4;
	const DESIGN_ASSETS			= 5;
	const PROTOTYPES			= 6;

	public static $c_arrstrProjectAttributeTypes = [
		self::PROBLEM_TO_SOLVE			=> 'Problem To Solve',
		self::CROSS_PRODUCT_REQUEST		=> 'Cross Product Request',
		self::QUESTIONS_TO_ANSWER		=> 'Questions To Answer',
		self::DECISION_LOG				=> 'Decision Log',
		self::DESIGN_ASSETS				=> 'Design Asset',
		self::PROTOTYPES				=> 'Prototype'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>