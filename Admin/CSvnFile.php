<?php

class CSvnFile extends CBaseSvnFile {

	protected $m_intTaskId;

	protected $m_arrstrNonRopFileNames;
	protected $m_arrstrDatabaseNames;
	protected $m_arrstrRopFileNames;
	protected $m_strDatabaseName;

	public function __construct() {
		parent::__construct();

		$this->m_boolAllowDifferentialUpdate = true;

		return;
	}

	/**
	 * Get Functions
	 */

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function getDatabaseName() {
		return $this->m_strDatabaseName;
	}

	/**
	 * Set Functions
	 */

	public function setTaskId( $intId ) {
		$this->m_intTaskId = CStrings::strToIntDef( $intId, NULL, false );
	}

	public function setDefaults() {

		$this->setIsPublished( 1 );
		return;
	}

	public function setDatabaseName( $strDatabaseName ) {
		$this->m_strDatabaseName = $strDatabaseName;
		return;
	}

	/**
	 * Validate Functions
	 */

	public function valDatabaseTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDatabaseTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Database type ID is required.' ) );
		}

		return $boolIsValid;
	}

	public function valFileName( $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		if( true == is_null( $this->getFileName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'File name is required.' ) );
		} elseif( false == is_null( $objAdminDatabase ) ) {

			$intConflictingSvnFileCount = CSvnFiles::fetchConflictingSvnFileCount( $this, $objAdminDatabase );

			if( 0 < $intConflictingSvnFileCount ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', $this->getFileName() . ' file already exists.' ) );
				$boolIsValid = false;
			}
		}

		return $boolIsValid;
	}

	public function valSqlReviewedBy() {
		$boolIsValid = true;

		if( true == is_null( $this->getSqlReviewedBy() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sql_reviewed_by', 'Please select sql reviewed by for ' . $this->getFileName() ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDatabaseTypeId();
				$boolIsValid &= $this->valFileName( $objAdminDatabase );
				$boolIsValid &= $this->valSqlReviewedBy();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public static function getDatabaseScriptPrefix( $strClusterName, $boolIsReadyToRun, $boolIsManual ) {

		$strPrefix = 'R';

		if( true == $boolIsReadyToRun ) {
			$strPrefix .= 'F';
		} else {
			$strPrefix .= 'O';
		}

		$strClusterPrefix = \Psi\CStringService::singleton()->substr( \Psi\CStringService::singleton()->strtoupper( $strClusterName ), 0, 1 );

		$strPrefix .= $strClusterPrefix;

		if( true == $boolIsManual ) {
			$strPrefix .= 'M';
		}

		return $strPrefix;
	}

}
?>