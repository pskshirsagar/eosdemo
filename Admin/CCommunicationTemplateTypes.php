<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCommunicationTemplateTypes
 * Do not add any new functions to this class.
 */

class CCommunicationTemplateTypes extends CBaseCommunicationTemplateTypes {

	public static function fetchCommunicationTemplateTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CCommunicationTemplateType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchCommunicationTemplateType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CCommunicationTemplateType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedCommunicationTemplateTypes( $objDatabase ) {
		$strSql = 'SELECT
						id,
						name
					FROM
						communication_template_types
					WHERE
						is_published = true
					ORDER BY
						name ASC';

		return self::fetchCommunicationTemplateTypes( $strSql, $objDatabase );
	}

	public static function fetchCommunicationTemplateTypesByIds( $arrintCommunicationTemplateTypeIds, $objDatabase ) {
		if( false == valArr( $arrintCommunicationTemplateTypeIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						id,
						name
					FROM
						communication_template_types
					WHERE
						id IN( ' . implode( ', ', $arrintCommunicationTemplateTypeIds ) . ' )
					ORDER BY
						name ASC';

		return self::fetchCommunicationTemplateTypes( $strSql, $objDatabase );
	}

}
?>