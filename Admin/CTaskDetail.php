<?php

class CTaskDetail extends CBaseTaskDetail {

	protected $m_strDeveloperEmployeeNameFull;
	protected $m_strQaEmployeeNameFull;
	protected $m_strTaskNote;
	protected $m_strTaskNoteTypeId;
	protected $m_strTaskDescription;
	protected $m_strTaskNoteTypePrecautionaryMeasurement;
	protected $m_strTaskNoteTypeFollowUpNote;
	protected $m_strTaskImpactTypeName;
	protected $m_strTaskQuarterName;
	protected $m_strTaskStatusName;
	protected $m_strTaskTypeName;
	protected $m_strClientName;
	protected $m_strDeveloperFullName;
	protected $m_strTaskUserName;
	protected $m_strDueDate;
	protected $m_strTaskReleaseDateTime;
	protected $m_strTaskImpact;

	protected $m_intParentTaskId;
	protected $m_intTaskTypeId;
	protected $m_intTaskPriorityId;
	protected $m_intUserId;
	protected $m_intTaskReleaseId;
	protected $m_intClusterId;
	protected $m_intTaskId;
	protected $m_intTaskTitle;
	protected $m_intTaskPriorityName;
	protected $m_intTaskStatusId;

	const MIN_STANDARD_STORY_POINTS	= 0;
	const MAX_STANDARD_STORY_POINTS	= 50;

	public function getDeveloperEmployeeNameFull() {
		return $this->m_strDeveloperEmployeeNameFull;
	}

	public function getQaEmployeeNameFull() {
		return $this->m_strQaEmployeeNameFull;
	}

	public function getTaskNote() {
		return $this->m_strTaskNote;
	}

	public function getTaskNoteTypeId() {
		return $this->m_strTaskNoteTypeId;
	}

	public function getDescription() {
		return $this->m_strTaskDescription;
	}

	public function getTaskNoteTypePrecautionaryMeasurement() {
		return $this->m_strTaskNoteTypePrecautionaryMeasurement;
	}

	public function getTaskNoteTypeFollowUpNote() {
		return $this->m_strTaskNoteTypeFollowUpNote;
	}

	public function getTaskStatusId() {
		return $this->m_intTaskStatusId;
	}

	public function getTaskImpactTypeName() {
		return $this->m_strTaskImpactTypeName;
	}

	public function getTaskQuarterName() {
		return $this->m_strTaskQuarterName;
	}

	public function getTaskTypeId() {
		return $this->m_intTaskTypeId;
	}

	public function getTaskTypeName() {
		return $this->m_strTaskTypeName;
	}

	public function getTaskStatusName() {
		return $this->m_strTaskStatusName;
	}

	public function getTaskPriorityId() {
		return $this->m_intTaskPriorityId;
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function getDeveloperFullName() {
		return $this->m_strDeveloperFullName;
	}

	public function getTaskUserName() {
		return $this->m_strTaskUserName;
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function getTaskReleaseId() {
		return $this->m_intTaskReleaseId;
	}

	public function getTaskReleaseDateTime() {
		return $this->m_strTaskReleaseDateTime;
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function getTaskImpact() {
		return $this->m_strTaskImpact;
	}

	public function getTaskId() {
		return $this->m_intTaskId;
	}

	public function getTaskTitle() {
		return $this->m_intTaskTitle;
	}

	public function getTaskPriorityName() {
		return $this->m_intTaskPriorityName;
	}

	public function getParentTaskId() {
		return $this->m_intParentTaskId;
	}

	public function setDeveloperEmployeeNameFull( $strDeveloperEmployeeNameFull ) {
		$this->m_strDeveloperEmployeeNameFull = $strDeveloperEmployeeNameFull;
	}

	public function setQaEmployeeNameFull( $strQaEmployeeNameFull ) {
		$this->m_strQaEmployeeNameFull = $strQaEmployeeNameFull;
	}

	public function setTaskNote( $strTaskNote ) {
		$this->m_strTaskNote = $strTaskNote;
	}

	public function setTaskNoteTypeId( $strTaskNoteTypeId ) {
		$this->m_strTaskNoteTypeId = $strTaskNoteTypeId;
	}

	public function setTaskDescription( $strTaskDescription ) {
		$this->m_strTaskDescription = $strTaskDescription;
	}

	public function setTaskNoteTypePrecautionaryMeasurement( $strTaskNoteTypePrecautionaryMeasurement ) {
		$this->m_strTaskNoteTypePrecautionaryMeasurement = $strTaskNoteTypePrecautionaryMeasurement;
	}

	public function setTaskNoteTypeFollowUpNote( $strTaskNoteTypeFollowUpNote ) {
		$this->m_strTaskNoteTypeFollowUpNote = $strTaskNoteTypeFollowUpNote;
	}

	public function setTaskStatusId( $intTaskStatusId ) {
		$this->m_intTaskStatusId = $intTaskStatusId;
	}

	public function setTaskImpactTypeName( $strTaskImpactTypeName ) {
		$this->m_strTaskImpactTypeName = $strTaskImpactTypeName;
	}

	public function setTaskQuarterName( $strTaskQuarterName ) {
		$this->m_strTaskQuarterName = $strTaskQuarterName;
	}

	public function setSmsNumber( $strSmsNumber ) {
		$this->m_strSmsNumber = \Psi\CStringService::singleton()->preg_replace( '/[^0-9 +]/', '', $strSmsNumber );
	}

	public function setTaskTypeId( $intTaskTypeId ) {
		$this->m_intTaskTypeId = $intTaskTypeId;
	}

	public function setTaskTypeName( $strTaskTypeName ) {
		$this->m_strTaskTypeName = $strTaskTypeName;
	}

	public function setTaskStatusName( $strTaskStatusName ) {
		$this->m_strTaskStatusName = $strTaskStatusName;
	}

	public function setTaskPriorityId( $intTaskPriorityId ) {
		$this->m_intTaskPriorityId = $intTaskPriorityId;
	}

	public function setClientName( $strClientName ) {
		$this->m_strClientName = $strClientName;
	}

	public function setDeveloperFullName( $strDeveloperFullName ) {
		$this->m_strDeveloperFullName = $strDeveloperFullName;
	}

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setDueDate( $strDueDate ) {
		$this->m_strDueDate = $strDueDate;
	}

	public function setTaskReleaseId( $intTaskReleaseId ) {
		$this->m_intTaskReleaseId = $intTaskReleaseId;
	}

	public function setTaskReleaseDateTime( $strTaskReleaseDateTime ) {
		$this->m_strTaskReleaseDateTime = $strTaskReleaseDateTime;
	}

	public function setClusterId( $intClusterId ) {
		$this->m_intClusterId = $intClusterId;
	}

	public function setTaskImpact( $strTaskImpact ) {
		$this->m_strTaskImpact = $strTaskImpact;
	}

	public function setTaskId( $intTaskId ) {
		$this->m_intTaskId = $intTaskId;
	}

	public function setTaskTitle( $strTaskTitle ) {
		$this->m_intTaskTitle = $strTaskTitle;
	}

	public function setTaskPriorityName( $strTaskPriorityName ) {
		$this->m_intTaskPriorityName = $strTaskPriorityName;
	}

	public function setTaskUserName( $strTaskUserName ) {
		$this->m_strTaskUserName = $strTaskUserName;
	}

	public function setParentTaskId( $intParentTaskId ) {
		$this->m_intParentTaskId = $intParentTaskId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
			if( true == isset( $arrmixValues['developer_employee_name_full'] ) ) 				$this->setDeveloperEmployeeNameFull( $arrmixValues['developer_employee_name_full'] );
			if( true == isset( $arrmixValues['qa_employee_name_full'] ) ) 						$this->setQaEmployeeNameFull( $arrmixValues['qa_employee_name_full'] );
			if( true == isset( $arrmixValues['task_note'] ) ) 									$this->setTaskNote( $arrmixValues['task_note'] );
			if( true == isset( $arrmixValues['task_note_type_id'] ) ) 							$this->setTaskNoteTypeId( $arrmixValues['task_note_type_id'] );
			if( true == isset( $arrmixValues['task_description'] ) ) 							$this->setTaskDescription( $arrmixValues['task_description'] );
			if( true == isset( $arrmixValues['task_note_type_precautionary_measurement'] ) )	$this->setTaskNoteTypePrecautionaryMeasurement( $arrmixValues['task_note_type_precautionary_measurement'] );
			if( true == isset( $arrmixValues['task_note_type_follow_up_note'] ) ) 				$this->setTaskNoteTypeFollowUpNote( $arrmixValues['task_note_type_follow_up_note'] );
			if( true == isset( $arrmixValues['task_status_id'] ) )								$this->setTaskStatusId( $arrmixValues['task_status_id'] );
			if( true == isset( $arrmixValues['task_impact_type_name'] ) )						$this->setTaskImpactTypeName( $arrmixValues['task_impact_type_name'] );
			if( true == isset( $arrmixValues['task_quarter_name'] ) )							$this->setTaskQuarterName( $arrmixValues['task_quarter_name'] );
			if( true == isset( $arrmixValues['task_type_id'] ) ) 								$this->setTaskTypeId( $arrmixValues['task_type_id'] );
			if( true == isset( $arrmixValues['task_type_name'] ) ) 								$this->setTaskTypeName( $arrmixValues['task_type_name'] );
			if( true == isset( $arrmixValues['task_status_name'] ) ) 							$this->setTaskStatusName( $arrmixValues['task_status_name'] );
			if( true == isset( $arrmixValues['task_priority_id'] ) )							$this->setTaskPriorityId( $arrmixValues['task_priority_id'] );
			if( true == isset( $arrmixValues['client_name'] ) )									$this->setClientName( $arrmixValues['client_name'] );
			if( true == isset( $arrmixValues['developer_full_name'] ) ) 						$this->setDeveloperFullName( $arrmixValues['developer_full_name'] );
			if( true == isset( $arrmixValues['user_id'] ) )										$this->setUserId( $arrmixValues['user_id'] );
			if( true == isset( $arrmixValues['due_date'] ) ) 									$this->setDueDate( $arrmixValues['due_date'] );
			if( true == isset( $arrmixValues['task_release_id'] ) )								$this->setTaskReleaseId( $arrmixValues['task_release_id'] );
			if( true == isset( $arrmixValues['task_release_datetime'] ) ) 						$this->setTaskReleaseDateTime( $arrmixValues['task_release_datetime'] );
			if( true == isset( $arrmixValues['cluster_id'] ) )									$this->setClusterId( $arrmixValues['cluster_id'] );
			if( true == isset( $arrmixValues['task_impact'] ) )        							$this->setTaskImpact( $arrmixValues['task_impact'] );
			if( true == isset( $arrmixValues['task_id'] ) )										$this->setTaskId( $arrmixValues['task_id'] );
			if( true == isset( $arrmixValues['task_title'] ) ) 									$this->setTaskTitle( $arrmixValues['task_title'] );
			if( true == isset( $arrmixValues['task_priority_name'] ) )							$this->setTaskPriorityName( $arrmixValues['task_priority_name'] );
			if( true == isset( $arrmixValues['task_user_name'] ) )								$this->setTaskUserName( $arrmixValues['task_user_name'] );
			if( true == isset( $arrmixValues['parent_task_id'] ) )								$this->setParentTaskId( $arrmixValues['parent_task_id'] );

	}

	public function valDeveloperStoryPoints() {
		$boolIsValid = true;

		if( CTaskDetail::MIN_STANDARD_STORY_POINTS > $this->getDeveloperStoryPoints() || CTaskDetail::MAX_STANDARD_STORY_POINTS < $this->getDeveloperStoryPoints() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'developer_story_points', 'Developer Story Points must be between 0 to 50.' ) );
		}
		return $boolIsValid;
	}

	public function valQaStoryPoints() {
	    $boolIsValid = true;

	    if( CTaskDetail::MIN_STANDARD_STORY_POINTS > $this->getQaStoryPoints() || CTaskDetail::MAX_STANDARD_STORY_POINTS < $this->getQaStoryPoints() ) {
	        $boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'qa_story_points', 'QA Story Points must be between 0 to 50.' ) );
	    }
	    return $boolIsValid;
	}

	public function valPenaltyStoryPoints() {
		$boolIsValid = true;
		if( CTaskDetail::MAX_STANDARD_STORY_POINTS < $this->getPenaltyStoryPoints() || CTaskDetail::MIN_STANDARD_STORY_POINTS > $this->getPenaltyStoryPoints() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'penalty_story_points', 'Penalty Story Points must be between 0 to 50.' ) );
		}
	return $boolIsValid;
	}

	public function valRootCauseTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getRootCauseTypeIds() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'root_cause_type_id', 'Root Cause Type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTaskImpactTypeId() {
		$boolIsValid = true;
		if( true == is_null( $this->getTaskImpactTypeId() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'root_cause_type_id', 'Impact of this Bug is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSeverityCount() {
		$boolIsValid = true;

		if( true == is_null( $this->getSeverityCount() ) && CTaskStatus::COMPLETED == $this->getTaskStatusId() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_detail_severity_count', 'Severity count is required.' ) );
			return $boolIsValid;
		}

		if( false == is_null( $this->getSeverityCount() ) && 1 < $this->getSeverityCount() && true == \Psi\CStringService::singleton()->preg_match( '/\d*(?:\.\d+)/', $this->getSeverityCount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_detail_severity_count', 'Invalid severity count, float value is allow below 1.' ) );
			return $boolIsValid;
		}

		if( false == is_null( $this->getSeverityCount() ) && 1 > $this->getSeverityCount() && false == \Psi\CStringService::singleton()->preg_match( '/^\d*(\.{1}\d{1,2}){0,1}$/', $this->getSeverityCount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_detail_severity_count', 'Severity count should not be more than two decimal spaces.' ) );
			return $boolIsValid;
		}

		if( CTaskStatus::COMPLETED == $this->getTaskStatusId() && ( 0 > $this->getSeverityCount() || 1000 < $this->getSeverityCount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_detail_severity_count', 'Invalid severity count.' ) );
		} elseif( 0 > $this->getSeverityCount() || 1000 < $this->getSeverityCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_detail_severity_count', 'Invalid severity count.' ) );
		}

		return $boolIsValid;
	}

	public function valSmsNumber() {
		$boolIsValid = true;

		if( false == valStr( $this->getSmsNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sms_number', __( 'Mobile number is required.' ) ) );
			return $boolIsValid;
		}

		$strSmsNumberPattern = '/\b\d{10,11}\b/';

		if( false == \Psi\CStringService::singleton()->preg_match( $strSmsNumberPattern, $this->getSmsNumber() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sms_number', __( 'Please enter valid mobile number.' ) ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valIsUatCompleted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsRequired = false, $objAdminDatabase = NULL, $objUser = NULL, $boolIsSeverityCount = false ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDeveloperStoryPoints();
				$boolIsValid &= $this->valQaStoryPoints();

				if( true == $boolIsSeverityCount ) {
					$boolIsValid &= $this->valSeverityCount();
				}

				if( true == $this->getSmsEnabled() ) {
					$boolIsValid &= $this->valSmsNumber();
				}
				break;

			case VALIDATE_DELETE:
				break;

			case 'update_root_cause_type':
			case 'insert_root_cause_type':
				$boolIsValid &= $this->valRootCauseTypeId();
				$boolIsValid &= $this->valTaskImpactTypeId();
				$boolIsValid &= $this->valPenaltyStoryPoints();
				break;

			case 'validate_severity_count':
				$boolIsValid &= $this->valSeverityCount();
				break;

			case 'insert_va_ticket':
				if( true == $this->getSmsEnabled() ) {
					$boolIsValid &= $this->valSmsNumber();
				}
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>