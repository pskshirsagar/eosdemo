<?php

class CNoteType extends CBaseNoteType {

	const EMPLOYEE_APPLICATION	= 1;
	const GENERAL				= 2;
	const REVIEW  				= 3;
	const LIKE					= 4;
	const ATTENDANCE_REMARK 	= 5;
	const EXIT_PROCESS 			= 6;
	const EMPLOYEE_APPLICATION_NOTES 	= 7;
	const TEAM_MODIFICATION_NOTES = 8;
	const EMPLOYEE_APPLICATION_HISTORY = 10;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>