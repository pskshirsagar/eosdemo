<?php

class CEmployeeApplicationShortlistDetail extends CBaseEmployeeApplicationShortlistDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeApplicationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valManagerEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPurchaseRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsShortlisted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>