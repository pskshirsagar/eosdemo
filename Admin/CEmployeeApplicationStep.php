<?php

class CEmployeeApplicationStep extends CBaseEmployeeApplicationStep {

	protected $m_intPsJobPostingStatusId;
	protected $m_intOrderNum;

	/**
	 * Get Functions
	 *
	 */

	public function getPsJobPostingStatusId() {
		return $this->m_intPsJobPostingStatusId;
	}

	public function getOrderNum() {
		return $this->m_intOrderNum;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['ps_job_posting_status_id'] ) ) {
			$this->setPsJobPostingStatusId( $arrmixValues['ps_job_posting_status_id'] );
		}

		if( true == isset( $arrmixValues['order_num'] ) ) {
			$this->setOrderNum( $arrmixValues['order_num'] );
		}

		return;
	}

	public function setPsJobPostingStatusId( $intPsJobPostingStatusId ) {
		$this->m_intPsJobPostingStatusId = $intPsJobPostingStatusId;
	}

	public function setOrderNum( $intOrderNum ) {
		$this->m_intOrderNum = $intOrderNum;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valPurchaseRequestId( $objDatabase ) {
		$boolValid = true;

		if( true == valId( $this->getPurchaseRequestId() ) ) {
			$arrobjEmployeeApplications = rekeyObjects( 'Id', \Psi\Eos\Admin\CEmployeeApplications::createService()->fetchEmployeeApplicationsByPurchaseRequestId( $this->getPurchaseRequestId(), $objDatabase ) );

			if( true == valArr( $arrobjEmployeeApplications ) && false == array_key_exists( $this->getEmployeeApplicationId(), $arrobjEmployeeApplications ) ) {
				$objEmployeeApplication = array_shift( $arrobjEmployeeApplications );
				$boolValid				= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'purchase_request_id', '<strong>Employee Application: ' . $objEmployeeApplication->getId() . '</strong> has been Offered against this <strong>PR ID: ' . $this->getPurchaseRequestId() . '.</strong> Please associate New PR ID.' ) );
			}
		}

		return $boolValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'update_basic_info':
				$boolValid &= $this->valPurchaseRequestId( $objDatabase );
				break;

			default:
			$boolValid = false;
		}

		return $boolValid;
	}

}
?>