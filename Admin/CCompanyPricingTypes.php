<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CCompanyPricingTypes
 * Do not add any new functions to this class.
 */

class CCompanyPricingTypes extends CBaseCompanyPricingTypes {

	public static function fetchAllCompanyPricingTypes( $objDatabase ) {
		$strSql = 'SELECT *	FROM company_pricing_types ORDER BY name';
		return self::fetchCompanyPricingTypes( $strSql, $objDatabase );
	}
}
?>