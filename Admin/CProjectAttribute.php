<?php

class CProjectAttribute extends CBaseProjectAttribute {
	protected $m_boolHasConversation;

	protected $m_strProjectAttributeTypeName;

	protected $m_intNumberOfClientAnswer;

	protected $m_intNumberOfAnswer;

	protected $m_strClientName;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectAttributeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectAttributeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setHasConversations( $boolHasConversation ) {
		$this->set( 'm_boolHasConversation', CStrings::strToBool( $boolHasConversation ) );
	}

	public function getHasConversations() {
		return $this->m_boolHasConversation;
	}

	public function setProjectAttributeTypeName( $strProjectAttributeTypeName ) {
		$this->set( 'm_strProjectAttributeTypeName', CStrings::strTrimDef( $strProjectAttributeTypeName, -1, NULL, true ) );
	}

	public function getProjectAttributeTypeName() {
		return $this->m_strProjectAttributeTypeName;
	}

	public function setNumberOfClientAnswer( $intClientsCount ) {
		$this->set( 'm_intNumberOfClientAnswer', CStrings::strToIntDef( $intClientsCount, NULL, false ) );
	}

	public function getNumberOfClientAnswer() {
		return $this->m_intNumberOfClientAnswer;
	}

	public function setNumberOfAnswer( $intAnswerCount ) {
		$this->set( 'm_intNumberOfAnswer', CStrings::strToIntDef( $intAnswerCount, NULL, false ) );
	}

	public function getNumberOfAnswer() {
		return $this->m_intNumberOfAnswer;
	}

	public function setClientName( $strClientName ) {
		$this->set( 'm_strClientName', CStrings::strTrimDef( $strClientName, -1, NULL, true ) );
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == array_key_exists( 'has_conversation', $arrmixValues ) ) {
			$this->setHasConversations( $arrmixValues['has_conversation'] );
		}

		if( true == array_key_exists( 'project_attribute_type_name', $arrmixValues ) ) {
			$this->setProjectAttributeTypeName( $arrmixValues['project_attribute_type_name'] );
		}

		if( true == array_key_exists( 'answers_count', $arrmixValues ) ) {
			$this->setNumberOfAnswer( $arrmixValues['answers_count'] );
		}

		if( true == array_key_exists( 'clients_count', $arrmixValues ) ) {
			$this->setNumberOfClientAnswer( $arrmixValues['clients_count'] );
		}

		if( true == array_key_exists( 'client_name', $arrmixValues ) ) {
			$this->setClientName( $arrmixValues['client_name'] );
		}

	}

}
?>