<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsProductCompetitors
 * Do not add any new functions to this class.
 */

class CPsProductCompetitors extends CBasePsProductCompetitors {

	public static function fetchPsProductCompetitorsByCompetitorId( $intCompetitorId, $objDatabase ) {
		$strSql = 'SELECT * FROM ps_product_competitors WHERE competitor_id  =' . ( int ) $intCompetitorId;
		return self::fetchPsProductCompetitors( $strSql, $objDatabase );
	}

	public static function fetchPsProductCompetitorsByPsProductsIds( $arrintPsProdcutIds, $objDatabase ) {
		$strSql = 'SELECT * FROM ps_product_competitors WHERE ps_product_id  in (' . implode( ',', $arrintPsProdcutIds ) . ')';
		return self::fetchPsProductCompetitors( $strSql, $objDatabase );
	}

	public static function fetchAllPsProductCompetitorsDetails( $objDatabase ) {

		 $strSql = 'SELECT * FROM (
				 			SELECT DISTINCT
								ppc.ps_product_id,
								c.id,
								CASE
									WHEN ppc.product_name IS NOT NULL
									THEN ppc.product_name
									ELSE c.name
								END AS product_name,
								CASE
									WHEN ppc.product_name IS NOT NULL
									THEN c.name
									ELSE NULL
								END AS competitor_name
							FROM
								ps_product_competitors ppc
								JOIN competitors c ON ( ppc.competitor_id = c.id )
							WHERE
								c.is_published = 1 ) AS sub
			 			ORDER BY
			 				ps_product_id,
			 				lower(product_name)';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAllPsProductCompetitorsByPsProductsIds( $arrintPsProdcutIds, $objDatabase ) {

		$strSql = 'SELECT * FROM ps_product_competitors WHERE id  in (' . implode( ',', $arrintPsProdcutIds ) . ')';

		return self::fetchPsProductCompetitors( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedPsProductCompetitorsByCompetitorId( $intPageNo, $intPageSize, $intCompetitorId, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSubSql = '';
		if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) {
			$strSubSql = ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}
		$strSql = 'SELECT
					*
					FROM
					ps_product_competitors';

		$strSql .= ' WHERE competitor_id  =' . ( int ) $intCompetitorId;

		$strSql .= ' ORDER BY ' . ' ' . $strOrderByField . ' ' . $strOrderByType . $strSubSql;

		return self::fetchPsProductCompetitors( $strSql, $objDatabase );
	}

	public static function fetchPsProductCompetitorsCountByCompetitorId( $intCompetitorId, $objDatabase ) {

		$strSql = ' WHERE competitor_id  =' . ( int ) $intCompetitorId;

		return self::fetchPsProductCompetitorCount( $strSql, $objDatabase );
	}
}
?>