<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmploymentApplicationQuestions
 * Do not add any new functions to this class.
 */

class CEmploymentApplicationQuestions extends CBaseEmploymentApplicationQuestions {

	public static function fetchEmploymentApplicationQuestionsByEmploymentApplicationId( $intEmploymentApplicationId, $objDatabase, $boolShowDeleted = false ) {

		$strSql = 'SELECT
						*
					FROM
						employment_application_questions
					WHERE
						employment_application_id = ' . ( int ) $intEmploymentApplicationId;

		if( false == $boolShowDeleted ) {
			$strSql .= ' AND deleted_by IS NULL';
		}

		$strSql .= ' ORDER BY deleted_by NULLS FIRST, id DESC';

		return self::fetchEmploymentApplicationQuestions( $strSql, $objDatabase );
	}

}
?>