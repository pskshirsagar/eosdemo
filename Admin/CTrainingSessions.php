<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingSessions
 * Do not add any new functions to this class.
 */
class CTrainingSessions extends CBaseTrainingSessions {

	public static function fetchAllTrainingSessions( $objDatabase ) {
		$strSql = 'SELECT
						ts.*,
						u.id AS user_id
					FROM
						training_sessions as ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts. training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
					WHERE
						ts.deleted_by IS NULL
						AND ts.deleted_on IS NULL
					ORDER BY
						ts.name';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTrainingSessionsByFilter( $objTrainingSessionFilter, $objDatabase, $strCountryCode = NULL ) {
		$intOffset	= ( 0 < $objTrainingSessionFilter->getPageNo() ) ? $objTrainingSessionFilter->getPageSize() * ( $objTrainingSessionFilter->getPageNo() - 1 ) : 0;
		$intLimit	= ( int ) $objTrainingSessionFilter->getPageSize();
		$strOrderClause = ' ORDER BY ts.id DESC ';
		$strOrder = '';

		if( false == is_null( $objTrainingSessionFilter ) && false == is_null( $objTrainingSessionFilter->getOrderByField() ) && false == is_null( $objTrainingSessionFilter->getOrderByType() ) ) {
			$strOrder = ( 1 == $objTrainingSessionFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';
			$strOrderClause = ' ORDER BY ' . $objTrainingSessionFilter->getOrderByField() . '' . $strOrder;
		}

		$strWhere = ' WHERE ts.deleted_by IS NULL ';

		if( true == valStr( $strCountryCode ) )
			$strWhere .= ' AND ( ts.country_code = \'' . $strCountryCode . '\' OR ts.country_code IS NULL )';

		$strSql = 'SELECT ts.*,
						e.preferred_name AS name_full,
						ar.name AS action_result_name,
						date (ts.scheduled_start_time),
						ofr.name as office_room_name
					FROM training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN action_results ar ON ar.id = a.action_result_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees AS e ON e.id = u.employee_id
						LEFT JOIN office_rooms ofr ON ofr.id = ts.office_room_id';

		$strSql = $strSql . $strWhere . '' . $strOrderClause . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit . '';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTrainingSessionsByTrainingIdByFilter( $intTrainingId, $objTrainingSessionFilter, $objDatabase, $strCountryCode = NULL ) {

		$intOffset = ( 0 < $objTrainingSessionFilter->getPageNo() ) ? $objTrainingSessionFilter->getPageSize() * ( $objTrainingSessionFilter->getPageNo() - 1 ) : 0;
		$intLimit = ( int ) $objTrainingSessionFilter->getPageSize();
		$strOrderClause = ' ORDER BY ts.id DESC ';
		$strOrder = '';

		if( false == is_null( $objTrainingSessionFilter ) && false == is_null( $objTrainingSessionFilter->getOrderByField() ) && false == is_null( $objTrainingSessionFilter->getOrderByType() ) ) {
			$strOrder = ( 1 == $objTrainingSessionFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';
			$strOrderClause = ' ORDER BY ' . $objTrainingSessionFilter->getOrderByField() . '' . $strOrder;
		}

		$strWhere = ' WHERE ts.deleted_by IS NULL AND ts.training_id = ' . ( int ) $intTrainingId;

		if( true == valStr( $strCountryCode ) )
			$strWhere .= ' AND ( ts.country_code = \'' . $strCountryCode . '\' OR ts.country_code IS NULL )';

		$strSql = 'SELECT ts.*,
						e.preferred_name AS name_full,
						ar.name as action_result_name,
						ofr.name as office_room_name
					FROM training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN tags t ON t.id = a.primary_reference
						LEFT JOIN action_results ar ON ar.id = t.action_result_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees AS e ON e.id = u.employee_id
						LEFT JOIN office_rooms ofr ON ofr.id = ts.office_room_id';

		$strSql = $strSql . $strWhere . '' . $strOrderClause . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit . '';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTrainingSessionsByUserIdByFilter( $intUserId, $objTrainingSessionFilter, $objDatabase, $strCountryCode = NULL, $intParentSessionId ) {

		$intOffset	= ( 0 < $objTrainingSessionFilter->getPageNo() ) ? $objTrainingSessionFilter->getPageSize() * ( $objTrainingSessionFilter->getPageNo() - 1 ) : 0;
		$intLimit	= ( int ) $objTrainingSessionFilter->getPageSize();
		$strOrderClause = ' ORDER BY id DESC ';
		$strOrder = '';

		if( false == is_null( $objTrainingSessionFilter ) && false == is_null( $objTrainingSessionFilter->getOrderByField() ) && false == is_null( $objTrainingSessionFilter->getOrderByType() ) ) {
			$strOrder = ( 1 == $objTrainingSessionFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';
			$strOrderClause = ' ORDER BY ' . $objTrainingSessionFilter->getOrderByField() . '' . $strOrder;
		}

		$strWhere = ' WHERE ts.deleted_by IS NULL AND ( a.secondary_reference = ' . ( int ) $intUserId . ' OR ts.created_by = ' . ( int ) $intUserId . ' )';

		if( true == valStr( $strCountryCode ) ) {
			$strWhere .= ' AND ( ts.country_code = \'' . $strCountryCode . '\' OR ts.country_code IS NULL )';
		}
		if( false == is_null( $intParentSessionId ) ) {
			$strWhere .= ' AND ts.parent_training_session_id = ' . ( int ) $intParentSessionId;
		}

		$strSql = 'SELECT
						ts.*,
						e.name_full AS name_full,
						ofr.name AS office_room_name,
						u.id AS user_id
					FROM
						training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts. training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees AS e ON e.id = u.employee_id
						LEFT JOIN office_rooms ofr ON ofr.id = ts.office_room_id';

		$strSql = $strSql . $strWhere . '' . $strOrderClause . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit . '';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsByIds( $arrintTrainingSessionIds, $objDatabase ) {
		if( false == valArr( $arrintTrainingSessionIds ) ) return NULL;

		$strSql = 'SELECT * FROM training_sessions WHERE id IN ( ' . implode( ',', $arrintTrainingSessionIds ) . ' )';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchUndeletedTrainingSessionById( $intTrainingSessionId, $objDatabase ) {

		$strSql = 'SELECT * FROM training_sessions WHERE deleted_by IS NULL AND id = ' . ( int ) $intTrainingSessionId;

		return self::fetchTrainingSession( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsByTrainingTrainerAssociationIdByMonthByYear( $intTrainingTrainerAssociationId, $intMonth, $intYear, $objDatabase ) {
		$strSql = 'SELECT ts.*,
						t.name as training_name
					FROM training_sessions as ts
						LEFT JOIN trainings as t ON ts.training_id = t.id
					WHERE ts.deleted_on IS NULL AND ts.deleted_by IS NULL AND ts.training_trainer_association_id = ' . ( int ) $intTrainingTrainerAssociationId . ' AND ';

		$strSql .= ( true == empty( $intMonth ) ) ? 	' date_part( \'month\', ( ts.scheduled_start_time) ) IS NULL;' : ' date_part( \'month\', ( ts.scheduled_start_time) ) = ' . ( int ) $intMonth;
		$strSql .= ( true == empty( $intYear ) ) ? 	' AND date_part( \'year\', ( ts.scheduled_start_time) ) IS NULL;' : ' AND date_part( \'year\', ( ts.scheduled_start_time) ) = ' . ( int ) $intYear;

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchAllTrainingSessionsByTrainingTrainerAssociationId( $intTrainingTrainerAssociationId, $objDatabase ) {
		$strSql = 'SELECT
						ts.name,
						e.name_full
					FROM
						training_sessions ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees e ON e.id = u.employee_id
					WHERE
						ts.deleted_on IS NULL
						AND training_trainer_association_id = ' . ( int ) $intTrainingTrainerAssociationId;
		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchAllTrainingSessionsByTrainingTrainerAssociationIds( $arrintTrainingTrainerAssociationIds, $objDatabase ) {
		$strSql = 'SELECT ts.name,
						e.name_full,
						tta.id
					FROM training_sessions ts
						LEFT JOIN training_trainer_associations as tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions as a ON a.id = tta.action_id
						LEFT JOIN users as u ON u.id = a.secondary_reference
						LEFT JOIN employees e ON e.id = u.employee_id
					WHERE tta.is_published = 1 AND
						ts.deleted_on IS NULL AND
						tta.id IN (' . implode( ', ', $arrintTrainingTrainerAssociationIds ) . ')';
		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchUndeletedTrainingSessionsByTrainingTrainerAssociationId( $intTrainingTrainerAssociationId, $objDatabase ) {
		$strSql = 'SELECT name,
						scheduled_start_time,
						scheduled_end_time,
						actual_start_time,
						actual_end_time,
						training_location,
						is_private
					FROM
						training_sessions
					WHERE deleted_on IS NULL AND training_trainer_association_id = ' . ( int ) $intTrainingTrainerAssociationId;
		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsByTrainingIdByYear( $intTrainingId, $intTrainingTrainerAssociationId, $intYear, $objDatabase ) {
		$strSql = 'SELECT t.name as training_name, ts.name,
						ts.scheduled_start_time,
						ts.scheduled_end_time,
						ts.actual_start_time,
						ts.actual_end_time,
						date_part( \'year\', ( ts.scheduled_start_time) ) as year
					FROM trainings as t
						LEFT JOIN training_sessions as ts ON ts.training_id = t.id
					WHERE ts.deleted_on IS NULL
						AND date_part(\'year\', ( ts.scheduled_start_time) ) = ' . ( int ) $intYear . '
						 AND t.id = ' . ( int ) $intTrainingId . '
						AND ts.training_trainer_association_id = ' . ( int ) $intTrainingTrainerAssociationId;

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsByTrainingIdByMonthByYear( $intTrainingId, $intMonth = NULL, $intYear, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$strSql = 'SELECT t.name as training_name,
						ts.id,
						ts.training_id,
						tta.id as training_trainer_association_id,
						ts.name,
						ts.scheduled_start_time,
						ts.scheduled_end_time,
						ts.actual_start_time,
						ts.actual_end_time,
						ar.name as action_result_name,
						ar.id as action_result_id,
						e.preferred_name AS name_full,
						date_part( \'year\', ( ts.scheduled_start_time) ) as year
					FROM training_sessions as ts
						LEFT JOIN trainings as t ON ts.training_id = t.id
						LEFT JOIN training_trainer_associations as tta ON ts.training_trainer_association_id = tta.id
						LEFT JOIN actions a on tta.action_id = a.id
						LEFT JOIN tags as tg ON tg.id = a.primary_reference
						LEFT JOIN action_results as ar ON ar.id = tg.action_result_id
						LEFT JOIN users u on a.secondary_reference = u.id
						LEFT JOIN employees e on u.employee_id = e.id
					WHERE ts.deleted_on IS NULL
						 AND t.id = ' . ( int ) $intTrainingId . '
						AND date_part(\'year\', ( ts.scheduled_start_time) ) = ' . ( int ) $intYear;

		if( false == empty( $intMonth ) ) {
			$strSql .= ' AND date_part(\'month\', scheduled_start_time ) = ' . ( int ) $intMonth;
		}

		if( false == empty( $strOrderByField ) && false == empty( $strOrderByType ) ) {
			$strSql .= ' ORDER BY ' . $strOrderByField . ' ' . $strOrderByType;
		}

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsByTrainingIdByMonthsByYear( $intTrainingId, $arrintMonths = NULL, $intYear, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$strSql = 'SELECT t.name as training_name,
						ts.id,
						ts.training_id,
						tta.id as training_trainer_association_id,
						ts.name,
						ts.scheduled_start_time,
						ts.scheduled_end_time,
						ts.actual_start_time,
						ts.actual_end_time,
						ar.name as action_result_name,
						ar.id as action_result_id,
						e.name_full as name_full,
						date_part( \'year\', ( ts.scheduled_start_time) ) as year
					FROM training_sessions as ts
						LEFT JOIN trainings as t ON ts.training_id = t.id
						LEFT JOIN training_trainer_associations as tta ON ts.training_trainer_association_id = tta.id
						LEFT JOIN actions a on tta.action_id = a.id
						LEFT JOIN tags as tg ON tg.id = a.primary_reference
						LEFT JOIN action_results as ar ON ar.id = tg.action_result_id
						LEFT JOIN users u on ta.secondary_reference = u.id
						LEFT JOIN employees e on u.employee_id = e.id
					WHERE ts.deleted_on IS NULL
						AND date_part(\'year\', ( ts.scheduled_start_time) ) = ' . ( int ) $intYear . '
						AND t.id = ' . ( int ) $intTrainingId;

		if( false == empty( $arrintMonths ) ) {
			$strSql .= ' AND date_part(\'month\', scheduled_start_time ) IN( ' . $arrintMonths . ')';
		}

		if( false == empty( $strOrderByField ) && false == empty( $strOrderByType ) ) {
			$strSql .= ' ORDER BY ' . $strOrderByField . ' ' . $strOrderByType;
		}

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsByTrainingTrainerAssociationIdByMonthsByYear( $intTrainingTrainerAssociationId, $arrintMonths = NULL, $intYear, $objDatabase ) {

		$strSql = 'SELECT ts.*,
						t.name as training_name,
						COALESCE( EXTRACT (  \'hour\' FROM ( ts.actual_end_time - ts.actual_start_time ) ), 0 ) AS hour,
						COALESCE ( EXTRACT ( \'minute\' FROM ( ts.actual_end_time - ts.actual_start_time ) ), 0 ) AS minute
					FROM training_sessions as ts
						LEFT JOIN trainings as t ON ts.training_id = t.id
					WHERE ts.deleted_on IS NULL AND ts.deleted_by IS NULL AND ts.training_trainer_association_id = ' . ( int ) $intTrainingTrainerAssociationId;

		if( false == empty( $arrintMonths ) ) {
			$strSql .= ' AND date_part(\'month\', scheduled_start_time ) IN( ' . $arrintMonths . ')';
		}
		$strSql .= ( true == empty( $intYear ) ) ? 	' AND date_part( \'year\', ( ts.scheduled_start_time) ) IS NULL;' : ' AND date_part( \'year\', ( ts.scheduled_start_time) ) = ' . ( int ) $intYear;

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsByTrainingTrainerAssociationIdByHalfYearlyByYear( $intTrainingTrainerAssociationId, $arrintMonths = NULL, $intYear, $objDatabase ) {

		$strSql = 'SELECT ts.*,
						t.name as training_name
					FROM training_sessions as ts
						LEFT JOIN trainings as t ON ts.training_id = t.id
					WHERE ts.deleted_on IS NULL AND ts.deleted_by IS NULL AND ts.training_trainer_association_id = ' . ( int ) $intTrainingTrainerAssociationId;

		if( false == empty( $arrintMonths ) ) {
			$strSql .= ' AND date_part(\'month\', scheduled_start_time ) IN( ' . $arrintMonths . ')';
		}
		$strSql .= ( true == empty( $intYear ) ) ? 	' AND date_part( \'year\', ( ts.scheduled_start_time) ) IS NULL;' : ' AND date_part( \'year\', ( ts.scheduled_start_time) ) = ' . ( int ) $intYear;

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsByTrainingIds( $arrintTrainingIds, $objDatabase ) {
		$strSql = 'SELECT ts.*
					FROM
						training_sessions ts
					WHERE
						deleted_on IS NULL
						AND deleted_by IS NULL
						AND training_id IN( ' . implode( ',', $arrintTrainingIds ) . ')';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsCountByUserId( $intUserId, $objDatabase, $strCountryCode = NULL ) {
		$strSql = 'SELECT
						count(ts.*)
					FROM
						training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts. training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees AS e ON e.id = u.employee_id
					WHERE
						ts.deleted_by IS NULL
						AND ( a.secondary_reference = ' . ( int ) $intUserId . ' OR ts.created_by = ' . ( int ) $intUserId . ')';

		if( true == valStr( $strCountryCode ) )
			$strSql .= ' AND ( ts.country_code = \'' . $strCountryCode . '\' OR ts.country_code IS NULL )';

		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchTrainingSessionsByDayAndMonthAndYear( $intDay, $intMonth, $intYear, $boolIsAdmin, $objDatabase ) {

		$strSql = 'SELECT ts.*
					FROM training_sessions as ts
					WHERE deleted_on IS NULL
						AND deleted_by IS NULL
						AND is_published = 1';

		if( false == $boolIsAdmin ) {
			$strSql .= 'AND is_private = 0';
		}

			$strSql .= 'AND date_part(\'month\', scheduled_start_time ) = ' . ( int ) $intMonth . '
							AND date_part(\'year\', scheduled_start_time ) = ' . ( int ) $intYear;

		if( false == empty( $intDay ) ) {
			$strSql .= ' AND date_part(\'day\', scheduled_start_time ) = ' . ( int ) $intDay;
		}

		return self::fetchTrainingSessions( $strSql, $objDatabase );

	}

	public static function fetchPaginatedTrainingSessionsBySessionFilter( $objTrainingSessionFilter, $objDatabase, $boolIsDownload = false, $strCountryCode = NULL, $intParentSessionId = NULL ) {

		$intOffset	= ( 0 < $objTrainingSessionFilter->getPageNo() ) ? $objTrainingSessionFilter->getPageSize() * ( $objTrainingSessionFilter->getPageNo() - 1 ) : 0;
		$intLimit	= ( int ) $objTrainingSessionFilter->getPageSize();

		$strPagination = ( false == $boolIsDownload ) ? ' OFFSET ' . ( int ) $intOffset . ' LIMIT	' . ( int ) $objTrainingSessionFilter->getPageSize() : '';

		$strOrderClause = ' ORDER BY ts.id DESC ';
		$strOrder = '';

		if( false == is_null( $objTrainingSessionFilter ) && false == is_null( $objTrainingSessionFilter->getOrderByField() ) && false == is_null( $objTrainingSessionFilter->getOrderByType() ) ) {
			$strOrder = ( 1 == $objTrainingSessionFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';
			$strOrderClause = ' ORDER BY ' . $objTrainingSessionFilter->getOrderByField() . '' . $strOrder;
		}

		$strWhere = ' WHERE ts.deleted_by IS NULL';

		if( false == is_null( $intParentSessionId ) ) {
			$strWhere .= ' AND ts.parent_training_session_id = ' . ( int ) $intParentSessionId;
		} else {
				$strWhere .= ( true == is_null( $objTrainingSessionFilter->getIsShowSubSessions() ) ) ? ' AND ts.parent_training_session_id IS NULL' : NULL;
		}

		if( true == valObj( $objTrainingSessionFilter, 'CTrainingSessionsFilter' ) ) {
				if( true == valStr( $objTrainingSessionFilter->getSessionName() ) ) {
					$strWhere .= ' AND ts.name ILIKE \'%' . addslashes( $objTrainingSessionFilter->getSessionName() ) . '%\'';
				}

				if( true == valStr( $objTrainingSessionFilter->getScheduledStartTime() ) ) {
					if( '' == $objTrainingSessionFilter->getScheduledEndTime() ) {
						$strWhere .= ' AND date( scheduled_start_time ) = \'' . $objTrainingSessionFilter->getScheduledStartTime() . '\'';
					} else {
						$strWhere .= ' AND date( scheduled_start_time ) BETWEEN \'' . $objTrainingSessionFilter->getScheduledStartTime() . '\'';
					}
				}

				if( true == valStr( $objTrainingSessionFilter->getScheduledEndTime() ) ) {
					$strWhere .= ' AND \'' . $objTrainingSessionFilter->getScheduledEndTime() . '\'';
				}

				if( true == valStr( $objTrainingSessionFilter->getCountry() ) ) {
					$strWhere .= ' AND ts.country_code = \'' . $objTrainingSessionFilter->getCountry() . '\'';
				} else {
					if( false == valStr( $objTrainingSessionFilter->getSessionName() ) && true == valStr( $strCountryCode ) )
						$strWhere .= ' AND ( ts.country_code = \'' . $strCountryCode . '\' OR ts.country_code IS NULL ) ';
				}

				if( true == valStr( $objTrainingSessionFilter->getActionResultId() ) ) {
					$strWhere .= ' AND a.action_result_id = ' . $objTrainingSessionFilter->getActionResultId();
				}

				if( true == valStr( $objTrainingSessionFilter->getTrainingTrainerAssociationId() ) ) {
					$strWhere .= ' AND ts.training_trainer_association_id = ' . $objTrainingSessionFilter->getTrainingTrainerAssociationId();
				}

				if( true == valStr( $objTrainingSessionFilter->getTrainingId() ) ) {
					$strWhere .= ' AND ts.training_id = ' . $objTrainingSessionFilter->getTrainingId();
				}

				if( true == valStr( $objTrainingSessionFilter->getIsAttendancePending() ) ) {
					$strWhere .= ' AND ( ts.actual_start_time IS NULL OR ts.actual_end_time IS NULL ) AND DATE ( ts.scheduled_start_time ) < CURRENT_DATE ';
				}

				if( false == is_null( $objTrainingSessionFilter->getIsPublished() ) ) {
					$strWhere .= ' AND ts.is_published = ' . ( int ) $objTrainingSessionFilter->getIsPublished();
				}

				if( false == is_null( $objTrainingSessionFilter->getIsPrivate() ) ) {
					$strWhere .= ' AND ts.is_private = ' . ( int ) $objTrainingSessionFilter->getIsPrivate();
				}

				if( true == $objTrainingSessionFilter->getIsInduction() ) {
					$strWhere .= ' AND ts.induction_session_id IS NOT NULL';
				}
		}

		$strSql = 'SELECT ts.*,
						e.preferred_name AS name_full,
						ar.name as action_result_name,
						ea.name_first as name_first,
						ea.name_last as name_last,
						a.action_result_id as action_result_id,
						ofr.name as office_room_name
					FROM
						training_sessions ts
						LEFT JOIN training_trainer_associations tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions a ON a.id = tta.action_id
						LEFT JOIN action_results AS ar ON ar.id = a.action_result_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees AS e ON e.id = u.employee_id
						LEFT JOIN employee_applications ea ON ea.id = a.primary_reference
						LEFT JOIN office_rooms ofr ON ofr.id = ts.office_room_id';

		$strSql = $strSql . $strWhere . '' . $strOrderClause . $strPagination;
		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsCountBySessionFilter( $objTrainingSessionFilter, $objDatabase, $strCountryCode = NULL, $intParentSessionId = NULL ) {

		$strWhere = ' WHERE deleted_by IS NULL';

		if( false == is_null( $intParentSessionId ) ) {
			$strWhere .= ' AND ts.parent_training_session_id = ' . ( int ) $intParentSessionId;
		} else {
			$strWhere .= ( true == is_null( $objTrainingSessionFilter->getIsShowSubSessions() ) ) ? ' AND ts.parent_training_session_id IS NULL' : NULL;
		}

		if( true == valObj( $objTrainingSessionFilter, 'CTrainingSessionsFilter' ) ) {

			if( true == valStr( $objTrainingSessionFilter->getSessionName() ) ) {
				$strWhere .= ' AND ts.name ILIKE \'%' . addslashes( $objTrainingSessionFilter->getSessionName() ) . '%\'';
			}

			if( true == valStr( $objTrainingSessionFilter->getScheduledStartTime() ) ) {
				if( '' == $objTrainingSessionFilter->getScheduledEndTime() ) {
					$strWhere .= ' AND date( scheduled_start_time ) = \'' . $objTrainingSessionFilter->getScheduledStartTime() . '\'';
				} else {
					$strWhere .= ' AND date( scheduled_start_time ) BETWEEN \'' . $objTrainingSessionFilter->getScheduledStartTime() . '\'';
				}
			}

			if( true == valStr( $objTrainingSessionFilter->getScheduledEndTime() ) ) {
				$strWhere .= ' AND \'' . $objTrainingSessionFilter->getScheduledEndTime() . '\'';
			}

			if( true == valStr( $objTrainingSessionFilter->getCountry() ) ) {
				$strWhere .= ' AND ts.country_code = \'' . $objTrainingSessionFilter->getCountry() . '\'';
			} else {
				if( false == valStr( $objTrainingSessionFilter->getSessionName() ) && true == valStr( $strCountryCode ) )
					$strWhere .= ' AND ( ts.country_code = \'' . $strCountryCode . '\' OR ts.country_code IS NULL )';
			}

			if( true == valStr( $objTrainingSessionFilter->getActionResultId() ) ) {
				$strWhere .= ' AND a.action_result_id = ' . $objTrainingSessionFilter->getActionResultId();
			}

			if( true == valStr( $objTrainingSessionFilter->getTrainingTrainerAssociationId() ) ) {
				$strWhere .= ' AND ts.training_trainer_association_id = ' . $objTrainingSessionFilter->getTrainingTrainerAssociationId();
			}

			if( true == valStr( $objTrainingSessionFilter->getTrainingId() ) ) {
				$strWhere .= ' AND ts.training_id = ' . $objTrainingSessionFilter->getTrainingId();
			}

			if( true == valStr( $objTrainingSessionFilter->getIsAttendancePending() ) ) {
				$strWhere .= ' AND ( ts.actual_start_time IS NULL OR ts.actual_end_time IS NULL ) AND DATE ( ts.scheduled_start_time ) < CURRENT_DATE ';
			}

			if( false == is_null( $objTrainingSessionFilter->getIsPublished() ) ) {
				$strWhere .= ' AND ts.is_published = ' . ( int ) $objTrainingSessionFilter->getIsPublished();
			}

			if( false == is_null( $objTrainingSessionFilter->getIsPrivate() ) ) {
				$strWhere .= ' AND ts.is_private = ' . ( int ) $objTrainingSessionFilter->getIsPrivate();
			}

			if( true == $objTrainingSessionFilter->getIsInduction() ) {
				$strWhere .= ' AND ts.induction_session_id IS NOT NULL';
			}
		}

		$strSql = 'SELECT
						count(ts.*)
					FROM
						training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts. training_trainer_association_id
						LEFT JOIN actions a ON a.id = tta.action_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees AS e ON e.id = u.employee_id';

		$strSql = $strSql . $strWhere;
		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchQuickSearchSession( $arrstrFilteredExplodedSearch, $objDatabase ) {

		$strSql = ' SELECT
						e.preferred_name AS name_full,
						ea.name_first,
						ea.name_last,
						ts.name,
						ts.id,
						ts.scheduled_start_time,
						ts.scheduled_end_time,
						ts.country_code,
						a.action_result_id
					FROM
						training_sessions ts
						LEFT JOIN training_trainer_associations tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions a on a.id = tta.action_id
						LEFT JOIN employee_applications ea on ea.id = a.secondary_reference
						LEFT JOIN users u on u.id = a.secondary_reference
						LEFT JOIN employees e on e.id = u.employee_id
					WHERE ts.deleted_by IS NULL
						AND ts.name ILIKE \'%' . implode( '%\' AND ts.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.preferred_name ILIKE \'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR ea.name_first ILIKE \'%' . implode( '%\' AND e.name_first ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR ea.name_last ILIKE \'%' . implode( '%\' AND e.name_last ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
					ORDER BY ts.id DESC LIMIT 10';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionYearlyCountByIdByTrainingYear( $intYear, $intTrainingId, $objDatabase ) {

		$strSql = 'SELECT count(ts.*) AS count,
						date_part( \'month\', ( ts.scheduled_start_time) ) as month
					FROM training_sessions as ts
					WHERE ts.scheduled_start_time IS NOT NULL AND
						ts.deleted_by IS NULL AND ts.deleted_on IS NULL AND
						ts.training_id = ' . ( int ) $intTrainingId . ' AND
						date_part(\'year\', ( ts.scheduled_start_time) ) = ' . ( int ) $intYear . '
					GROUP BY date_part( \'month\', ( ts.scheduled_start_time) )';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchPrivateTrainingSessionsByUserIdByDateByMonthByYear( $intUserId, $intDay, $intMonth, $intYear, $objDatabase ) {

		$strSql = 'SELECT DISTINCT(ts.id),
						ts.name,
						ts.is_private,
						ts.scheduled_start_time,
						ts.scheduled_end_time,
						ts.actual_start_time,
						ts.actual_end_time,
						ts.available_seats,
						ts.training_location,
						ts.country_code,
						ts.training_trainer_association_id
					FROM training_sessions as ts
						JOIN employee_training_sessions ets ON ets.training_session_id = ts.id
						LEFT JOIN training_trainer_associations tta on ts.training_trainer_association_id = tta.id
						LEFT JOIN actions a on tta.action_id = a.id
					WHERE deleted_on IS NULL
						AND ts.is_published = 1
						AND ts.is_private = 1
						AND ( ets.user_id = ' . ( int ) $intUserId . ' OR a.secondary_reference = ' . ( int ) $intUserId . ' )
						AND date_part(\'month\', ts.scheduled_start_time ) = ' . ( int ) $intMonth . '
						AND date_part(\'year\', ts.scheduled_start_time ) = ' . ( int ) $intYear;

					if( false == empty( $intDay ) ) {
						$strSql .= ' AND date_part(\'day\', scheduled_start_time ) = ' . ( int ) $intDay;
					}

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchAllUndeletedTrainingSessionsByTrainingIdsByYearByCountry( $arrintTrainingIds, $intYear, $strCountry, $objDatabase ) {

		$strSql = 'SELECT ts.*,
							t.name as training_name,
							e.name_full,
							ea.name_first,
							ea.name_last,
							e.preferred_name,
							a.action_result_id
					FROM trainings as t
							LEFT JOIN training_sessions ts ON ts.training_id = t.id
							LEFT JOIN training_trainer_associations tta ON tta.id = ts.training_trainer_association_id
							LEFT JOIN actions a ON a.id = tta.action_id
							LEFT JOIN employee_applications ea on ea.id = a.secondary_reference
							LEFT JOIN users u ON a.secondary_reference = u.id
							LEFT JOIN employees e ON u.employee_id = e.id
					WHERE t.deleted_by IS NULL
							AND ts.deleted_on IS NULL
							AND t.id IN (' . implode( ',', $arrintTrainingIds ) . ')';

		if( false == empty( $intYear ) ) {
			$strSql .= ' AND date_part(\'year\', ( ts.scheduled_start_time) ) = ' . ( int ) $intYear;
		}

		if( false == empty( $strCountry ) ) {
			$strSql .= ' AND ts.country_code = \'' . $strCountry . '\'';
		}

		return self::fetchTrainingSessions( $strSql, $objDatabase );

	}

	public static function fetchAllUndeletedTrainingSessionsByYearByCountry( $intYear, $strCountry, $objDatabase ) {

		$strSql = 'SELECT t.id,
						t.name as training_name,
						t1.name as training_parent_name,
						count(ts.id) as total_count_session
					FROM trainings t
						LEFT JOIN trainings t1 ON t1.id = t.parent_id
						JOIN training_sessions ts ON t.id = ts.training_id
					WHERE t.deleted_by IS NULL AND
						ts.deleted_by IS NULL';

		if( false == empty( $intYear ) ) {
			$strSql .= ' AND date_part(\'year\', ( ts.scheduled_start_time) ) = ' . ( int ) $intYear;
		}

		if( true == valStr( $strCountry ) ) {
			$strSql .= ' AND ts.country_code = \'' . $strCountry . '\'';
		}

		$strSql .= ' GROUP BY t.id, t.name, t1.name';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchTrainingSessionsBySessionFilter( $arrmixSessionsFilter, $objDatabase ) {
		$strOrderClause = ' ORDER BY ts.name DESC ';

		$strWhere = ' WHERE ts.deleted_by IS NULL';

		if( true == valArr( $arrmixSessionsFilter ) ) {
			foreach( $arrmixSessionsFilter as $strKey => $mixValue ) {
				if( 'session_name' == $strKey && false == empty( $mixValue ) ) {
					$strWhere .= ' AND ts.name LIKE \'%' . $mixValue . '%\'';
				} elseif( 'scheduled_start_time' == $strKey && false == empty( $mixValue ) ) {
					if( '' == $arrmixSessionsFilter['scheduled_end_time'] ) {
						$strWhere .= ' AND date( scheduled_start_time ) = \'' . $mixValue . '\'';
					} else {
						$strWhere .= ' AND date( scheduled_start_time ) BETWEEN \'' . $mixValue . '\'';
					}
				} elseif( 'scheduled_end_time' == $strKey && false == empty( $mixValue ) ) {
					$strWhere .= ' AND \'' . $mixValue . '\'';
				} elseif( 'country_code' == $strKey && false == empty( $mixValue ) ) {
					$strWhere .= ' AND country_code = \'' . $mixValue . '\'';
				} elseif( 'action_result_id' == $strKey && false == empty( $mixValue ) ) {
					$strWhere .= ' AND action_result_id = \'' . $mixValue . '\'';
				} elseif( 'training_trainer_association_id' == $strKey && false == empty( $mixValue ) ) {
					$strWhere .= ' AND training_trainer_association_id = ' . $mixValue;
				} elseif( false == empty( $mixValue ) ) {
					$strWhere .= ' AND ts.' . $strKey . ' = ' . $mixValue;
				}
			}
		}

		$strSql = 'SELECT
						ts.*
					FROM
						training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts. training_trainer_association_id
						LEFT JOIN actions a ON a.id = tta.action_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees AS e ON e.id = u.employee_id';

		$strSql = $strSql . $strWhere . $strOrderClause;
		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchDisapprovedTrainingSessionsByUserId( $intUserId, $objDatabase ) {
		$strSql = 'SELECT ts.*
					FROM employee_training_sessions AS ets
						JOIN training_sessions AS ts ON ts.id = ets.training_session_id
					WHERE ets.user_id = ' . ( int ) $intUserId . '
						AND ets.is_available = 0
						AND ets.user_id <> ets.updated_by
						AND ts.deleted_by is null';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionwithActionResultById( $intId, $objDatabase ) {

		$strSql = 'SELECT
						ts.*,
						a.action_result_id
					FROM
						training_sessions ts
						LEFT JOIN training_trainer_associations tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions a ON tta.action_id = a.id
					WHERE ts.deleted_by IS NULL AND
						ts.id = ' . ( int ) $intId;

		return self::fetchTrainingSession( $strSql, $objDatabase );
	}

	public static function fetchAllExternalTrainers( $objDatabase ) {
		$strSql = 'SELECT ts.id,
						ea.name_first,
						ea.name_last,
						ar.name as action_result_name,
						ar.id as action_result_id,
						tta.id as training_trainer_association_id
					FROM employee_applications ea
						JOIN actions a ON a.secondary_reference = ea.id
						JOIN training_trainer_associations tta ON tta.action_id = a.id
						JOIN training_sessions ts ON ts.training_trainer_association_id = tta.id
						JOIN tags t ON a.primary_reference = t.id
						JOIN action_results ar ON t.action_result_id = ar.id
					WHERE t.action_result_id = ' . ( int ) CActionResult::EXTERNAL_TRAINER;
		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchAllTrainingSessionsWithExternalTrainer( $objDatabase ) {
		$strSql = 'SELECT
						ts.id,
						ea.name_first as name_first,
						ea.name_last as name_last
					FROM training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN employee_applications AS ea ON ea.id = a.secondary_reference
					WHERE tta.is_published = 1
						AND a.primary_reference = ' . ( int ) CActionResult::EXTERNAL_TRAINER;

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionWithTrainerDetailsById( $intId, $objDatabase ) {
		$strSql = 'SELECT
						ts.*,
						e.preferred_name AS name_full,
						ea.name_first as name_first,
						ea.name_last as name_last,
						ar.name as action_result_name,
						ar.id as action_result_id,
						u.id as user_id,
						ofr.name as office_room_name
					FROM
						training_sessions ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN tags as tg ON tg.id = a.primary_reference
						LEFT JOIN action_results as ar ON ar.id = tg.action_result_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees e ON e.id = u.employee_id
						LEFT JOIN employee_applications AS ea ON ea.id = a.secondary_reference
						LEFT JOIN office_rooms ofr ON ofr.id = ts.office_room_id
					WHERE
						ts.deleted_on IS NULL
						AND ts.id = ' . ( int ) $intId;

		return self::fetchTrainingSession( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionWithExternalTrainerDetailsByIds( $arrintTrainingSessionIds, $objDatabase ) {

		if( false == valArr( $arrintTrainingSessionIds ) ) return NULL;

		$strSql = 'SELECT
						ts.*,
						ea.name_first,
						ea.name_last
					FROM
						training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN employee_applications AS ea ON ea.id = a.secondary_reference
					WHERE
						tta.is_published = 1
						AND a.action_result_id = ' . ( int ) CActionResult::EXTERNAL_TRAINER . '
						AND ts.id IN ( ' . implode( ',', $arrintTrainingSessionIds ) . ' ) ';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchPastTrainingSessionsByDateByMonthByYear( $intDay, $intMonth, $intYear, $objDatabase ) {
		$strSql = 'SELECT
						ts.id,
						ts.name
					FROM
						training_sessions as ts
					WHERE
						ts.scheduled_start_time < now()
						AND ts.deleted_on IS NULL
						AND ts.is_published = 1
						AND ts.actual_start_time IS NULL
						AND date_part(\'month\', ts.scheduled_start_time ) = ' . ( int ) $intMonth . '
						AND date_part(\'year\', ts.scheduled_start_time ) = ' . ( int ) $intYear;

					if( false == empty( $intDay ) ) {
						$strSql .= ' AND date_part(\'day\', scheduled_start_time ) = ' . ( int ) $intDay;
					}

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchMissedAttendenceTrainingSessions( $objDatabase ) {

		$strSql = 'SELECT
					ts.id,
					count ( ets.is_present ) AS present_count,
					DATE_PART ( \'day\', NOW ( ) ::timestamp - ts.scheduled_end_time::timestamp ) AS days_count,
					ts.name,
					ts.scheduled_start_time,
					ts.scheduled_end_time,
					u.id AS user_id,
					e.id as employee_id,
					e.name_full,
					e.name_first,
					e.email_address
				FROM
					training_sessions ts
					LEFT JOIN employee_training_sessions ets ON ( ets.training_session_id = ts.id )
					LEFT JOIN training_trainer_associations tta ON ( tta.id = ts.training_trainer_association_id )
					LEFT JOIN actions AS a ON ( a.id = tta.action_id )
					LEFT JOIN users AS u ON ( u.id = a.secondary_reference )
					LEFT JOIN employees AS e ON ( e.id = u.employee_id )
				WHERE
					ts.is_published = 1
					AND ts.actual_start_time IS NULL
					AND ts.actual_end_time IS NULL
					AND ( ( ts.scheduled_start_time BETWEEN \'2014-01-09\' AND now ( ) ) OR ( ts.scheduled_end_time BETWEEN \'2014-01-09\' AND now ( ) ) )
					AND deleted_by IS NULL
					AND deleted_on IS NULL
				GROUP BY
					ts.id,
					ts.name,
					ts.scheduled_start_time,
					ts.scheduled_end_time,
					u.id,
					e.id,
					e.name_full,
					e.name_first,
					e.email_address
				HAVING
					count ( ets.is_present ) = 0';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsByMonthByYear( $intMonth, $intYear, $objDatabase ) {

		$strSql = 'SELECT
						t.name as training_name,
						count( ets.is_present ) AS present_count,
						ts.name,
						ts.actual_start_time,
						e.preferred_name AS name_full,
						COALESCE( t1.name, t.name ) AS training_parent_name,
						string_agg( distinct cg.name, \', \') as goals
					FROM
						training_sessions AS ts
						LEFT JOIN trainings AS t ON ts.training_id = t.id
						LEFT JOIN training_trainer_associations AS tta ON ts.training_trainer_association_id = tta.id
						LEFT JOIN actions a ON tta.action_id = a.id
						LEFT JOIN users u ON a.secondary_reference = u.id
						LEFT JOIN employees e ON u.employee_id = e.id
						LEFT JOIN employee_training_sessions ets ON ( ets.training_session_id = ts.id )
						LEFT JOIN trainings t1 ON t1.id = t.parent_id
						LEFT JOIN training_session_company_goals tscg on ts.id=tscg.training_session_id
						LEFT JOIN company_goals cg on tscg.company_goal_id=cg.id
					WHERE ts.deleted_on IS NULL
						AND date_part( \'year\', ts.actual_start_time ) = ' . ( int ) $intYear . ' AND date_part( \'month\', ts.actual_start_time ) = ' . ( int ) $intMonth . '
					GROUP BY
						ts.name,
						t.name,
						ts.actual_start_time,
						e.name_full,
						e.preferred_name,
						t1.name,
						ets.is_present
					HAVING
						ets.is_present = 1
					ORDER BY
						ts.actual_start_time';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchTrainingGivenOfPreviousOneYearByUserIds( $arrintUserIds, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) ) return NULL;

		$strSql = ' SELECT
						date_trunc ( \'month\', ts.actual_start_time )::date AS start_date,
						TO_CHAR ( ( ( EXTRACT ( EPOCH FROM ( SUM ( ts.actual_end_time - ts.actual_start_time ) ) ::interval ) ) || \'second\' ) ::interval, \'HH24.MI\' ) AS result
					FROM
						training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON ( tta.id = ts.training_trainer_association_id )
						LEFT JOIN actions AS a ON ( a.id = tta.action_id )
						LEFT JOIN users AS u ON ( ( u.id = a.secondary_reference ) )
					WHERE
						ts.deleted_by IS NULL
						AND ts.deleted_on IS NULL
						AND ts.actual_start_time >= CAST ( date_trunc ( \'month\', ( CURRENT_DATE - \'11 month\' ::INTERVAL ) ) AS DATE )
						AND u.id IN ( \'' . implode( '\', \'', $arrintUserIds ) . '\' )
					GROUP BY
						date_trunc ( \'month\', ts.actual_start_time )
					ORDER BY
						date_trunc ( \'month\', ts.actual_start_time )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsCountByUserIds( $arrintUserIds, $objDatabase, $strFromDate = NULL, $strToDate = NULL ) {

		if( false == valArr( array_filter( $arrintUserIds ) ) ) return [];

		$strWhereAdditionalCondition = ( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? 'AND ts.actual_start_time > \'' . $strFromDate . '\' AND ts.actual_start_time < \'' . $strToDate . '\'': '';

		$strSql = 'SELECT * FROM (
					SELECT
						u.id,
						tta.id as training_trainer_assosiation_id,
						count(ts.id),
						dense_rank() OVER( ORDER BY count(ts.id) DESC) AS training_count_rank,
						round(CAST(SUM(EXTRACT(EPOCH FROM (ts.actual_end_time - ts.actual_start_time)) / 3600 ) as NUMERIC ),2) AS training_hours,
						dense_rank() OVER(ORDER BY SUM(EXTRACT(EPOCH FROM (ts.actual_end_time - ts.actual_start_time)) / 3600) DESC ) AS training_hours_rank
					FROM
						training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts. training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees AS e ON e.id = u.employee_id
					WHERE
						ts.deleted_by IS NULL
						AND a.secondary_reference IN ( ' . implode( ',', $arrintUserIds ) . ' ) ' . $strWhereAdditionalCondition . ' 
						GROUP BY u.id, tta.id
						ORDER BY u.id ) main';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsDetailsByUserId( $intUserId, $objDatabase, $strFromDate = NULL, $strToDate = NULL ) {

		$strWhereAdditionalCondition = ( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? ' AND ts.actual_start_time > \'' . $strFromDate . '\' AND ts.actual_start_time < \'' . $strToDate . '\'': '';

		$strSql = 'SELECT
					    ts.name,
					    ts.actual_start_time,
					    ts.actual_end_time,
					    round ( CAST ( SUM ( EXTRACT ( EPOCH
											FROM
							( ts.actual_end_time - ts.actual_start_time ) ) / 3600 ) AS NUMERIC ), 2 ) AS training_hours
					FROM
					    training_sessions ts
					    LEFT JOIN training_trainer_associations tta ON tta.id = ts.training_trainer_association_id
					    LEFT JOIN actions a ON a.id = tta.action_id
					    LEFT JOIN users u ON u.id = a.secondary_reference
					    LEFT JOIN employees e ON e.id = u.employee_id
					WHERE
					    ts.deleted_by IS NULL
					    AND a.secondary_reference = ' . ( int ) $intUserId . $strWhereAdditionalCondition . ' 
						GROUP BY
						    ts.name,
						    ts.scheduled_start_time,
						    ts.scheduled_end_time,
						    ts.id
						ORDER BY
						    ts.id DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomTrainingSessionsByTrainer( $intGroupId, $objDatabase, $intYear = NULL, $strOrderByField = NULL, $strOrderByType = NULL ) {

		$strWhereCondition = '';

		if( true == valStr( $intYear ) )
			$strWhereCondition .= ' AND date_part( \'year\', ts.actual_start_time ) = ' . ( int ) $intYear . ' AND date_part( \'year\', ts.actual_end_time ) = ' . ( int ) $intYear;

		$strSql = ' SELECT
						ts.*,
						e.id AS employee_id,
						e.name_full AS name_full,
						d.name as designation_name,
						t1.name AS training_parent_name,
						(
							SELECT
								count ( ts1.name )
							FROM
								training_sessions ts1
								LEFT JOIN training_trainer_associations AS tta ON tta.id = ts1.training_trainer_association_id
								LEFT JOIN actions AS a ON a.id = tta.action_id
								LEFT JOIN users AS u ON u.id = a.secondary_reference
								LEFT JOIN employees AS e1 ON e1.id = u.employee_id
							WHERE
								ts1.name = ts.name
								AND e1.id = e.id
								AND ts1.deleted_by IS NULL
						) AS count
					FROM
						employees e
						FULL JOIN users u ON ( u.employee_id = e.id )
						FULL JOIN designations d ON d.id = e.designation_id
						FULL JOIN user_groups ug ON ( u.id = ug.user_id AND ug.deleted_by IS NULL )
						FULL JOIN actions AS a ON ( u.id = a.secondary_reference AND a.action_type_id = ' . CActionType::TAG . ' )
						FULL JOIN training_trainer_associations AS tta ON a.id = tta.action_id
						FULL JOIN training_sessions AS ts ON ( tta.id = ts.training_trainer_association_id ' . $strWhereCondition . ' )
						FULL JOIN trainings AS t ON t.id = ts.training_id
						FULL JOIN trainings AS t1 ON t1.id = t.parent_id
					WHERE
						ug.group_id = ' . ( int ) $intGroupId . '
						AND ts.deleted_by IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		if( true == valStr( $strOrderByField ) && true == valStr( $strOrderByType ) ) {
			$strSql .= ' ORDER BY ' . $strOrderByField . ' ' . $strOrderByType;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUpcomingTrainingSessionsByCountryCodeByUserId( $strCountryCode, $strDate, $intCurrentUserId, $objDatabase ) {

		if( false == valStr( $strCountryCode ) ) return NULL;

		$strSql = 'SELECT
						ts.id,
						ts.name,
						ts.scheduled_start_time,
						ts.scheduled_end_time
					FROM
						training_sessions ts
					WHERE
						ts.scheduled_start_time IS NOT NULL
						AND ts.scheduled_start_time > \'' . $strDate . '\'
						AND ts.scheduled_end_time IS NOT NULL
						AND ts.actual_start_time IS NULL
						AND ts.actual_end_time IS NULL
						AND ts.deleted_by IS NULL
						AND ts.is_private = 0
						AND ts.is_published = 1
						AND ts.country_code = \'' . $strCountryCode . '\'
					UNION
					SELECT
						ts.id,
						ts.name,
						ts.scheduled_start_time,
						ts.scheduled_end_time
					FROM
						training_sessions ts
						LEFT JOIN employee_training_sessions ets ON ts.id = ets.training_session_id
					WHERE
						ts.is_private = 1
						AND ets.user_id = ' . ( int ) $intCurrentUserId . '
						AND ts.actual_start_time IS NULL
						AND ts.actual_end_time IS NULL
						AND ts.scheduled_start_time IS NOT NULL
						AND ts.scheduled_start_time > \'' . $strDate . '\'
						AND ts.scheduled_end_time IS NOT NULL
						AND ts.deleted_by IS NULL
						AND ts.is_published = 1
						AND ts.country_code = \'' . $strCountryCode . '\'
						ORDER BY scheduled_start_time';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTrainingSessionByParentTrainingSessionId( $intPageNo, $intPageSize, $intId, $objDatabase ) {

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit	= ( int ) $intPageSize;

		$strSql = ' SELECT
						ts.*,
						e.preferred_name AS name_full,
						ar.name AS action_result_name
					FROM
						training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN action_results ar ON ar.id = a.action_result_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees AS e ON e.id = u.employee_id
					WHERE
						ts.deleted_by IS NULL
						AND ts.parent_training_session_id = ' . ( int ) $intId . '
					ORDER BY
						ts.updated_on DESC OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTrainingSessionCountByParentTrainingSessionId( $intId, $objDatabase ) {

		$strWhere = NULL;

		$strWhere = ' WHERE
						deleted_by IS NULL
						AND parent_training_session_id = ' . ( int ) $intId;

		return self::fetchTrainingSessionCount( $strWhere, $objDatabase );
	}

	public static function fetchSubTrainingSessionsByParentTrainingSessionIds( $arrintParentSessionIds, $objDatabase ) {

		if( false == valArr( $arrintParentSessionIds ) ) return NULL;
		$strSql = 'SELECT
						id
					FROM
						training_sessions
					WHERE
					parent_training_session_id in (' . implode( ', ', $arrintParentSessionIds ) . ')';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionWithTrainerDetailsByIds( $arrintTrainingSessionIds, $objDatabase ) {

		if( false == valArr( $arrintTrainingSessionIds ) ) return NULL;
		$strSql = 'SELECT
						ts.*,
						e.preferred_name AS name_full,
						ar.name as action_result_name,
						a.action_result_id,
						u.id as user_id,
						t.name as training_name,
						t1.name AS parent_training_name,
						string_agg(distinct cg.name, \', \') as goals
					FROM
						training_sessions ts
						LEFT JOIN trainings as t on t.id = ts.training_id
						LEFT JOIN trainings as t1 on t1.id = t.parent_id
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN action_results as ar ON ar.id = a.action_result_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees e ON e.id = u.employee_id
						LEFT JOIN training_session_company_goals tscg ON ts.id = tscg.training_session_id
						LEFT JOIN company_goals cg ON tscg.company_goal_id = cg.id
					WHERE
						ts.deleted_on IS NULL
						AND ts.id IN ( ' . implode( ',', $arrintTrainingSessionIds ) . ' )
					GROUP BY
						ts.id,
						e.preferred_name,
						e.name_full,
						ar.name,
						a.action_result_id,
						u.id,
						t.name,
						t1.name';

			return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsWithTrainerDetailsByYear( $intYearResultId, $objDatabase ) {
		if( true == is_null( $intYearResultId ) ) return NULL;

		$strSql = 'SELECT
					COALESCE( SUM ( CASE
							WHEN ( a.action_result_id = ' . CActionResult::TRAINER . ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' ) THEN 1
							ELSE 0
						END ) , 0 ) AS trainer_sessions,
					COALESCE( SUM ( CASE
							WHEN ( a.action_result_id = ' . CActionResult::INTERESTED_TRAINER . ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ') THEN 1
							ELSE 0
						END ), 0 ) AS interested_trainer_sessions,
					COALESCE( SUM ( CASE
							WHEN ( a.action_result_id = ' . CActionResult::EXTERNAL_TRAINER . ' ) THEN 1
							ELSE 0
						END ), 0 ) AS external_trainer_sessions,
					COALESCE( SUM ( CASE
							WHEN ( a.action_result_id = ' . CActionResult::TRAINER . ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' ) THEN EXTRACT ( HOUR FROM ( ts.actual_end_time - ts.actual_start_time ) ) * 60 +
																				EXTRACT ( minute FROM ( ts.actual_end_time - ts.actual_start_time ) )
							ELSE 0
						END ), 0 ) AS trainer_tmti,
					COALESCE( SUM ( CASE
							WHEN ( a.action_result_id = ' . CActionResult::INTERESTED_TRAINER . ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' ) THEN EXTRACT ( HOUR FROM ( ts.actual_end_time - ts.actual_start_time ) ) * 60 +
																				EXTRACT ( minute FROM ( ts.actual_end_time - ts.actual_start_time ) )
							ELSE 0
						END ), 0 ) AS interested_trainer_tmti,
					COALESCE( SUM ( CASE
							WHEN ( a.action_result_id = ' . CActionResult::EXTERNAL_TRAINER . ' ) THEN EXTRACT ( HOUR FROM ( ts.actual_end_time - ts.actual_start_time ) ) * 60 +
																				EXTRACT ( minute FROM ( ts.actual_end_time - ts.actual_start_time ) )
							ELSE 0
						END ), 0 ) AS external_trainer_tmti,
					( SELECT
							COUNT ( DISTINCT e.id )
						FROM
							training_sessions AS ts
							LEFT JOIN training_trainer_associations AS tta ON tta.id = ts.training_trainer_association_id
							LEFT JOIN actions AS a ON a.id = tta.action_id
							LEFT JOIN users AS u ON u.id = a.secondary_reference
							LEFT JOIN employees AS e ON e.id = u.employee_id
						WHERE
							a.action_result_id = ' . CActionResult::TRAINER . '
							AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
							AND EXTRACT ( YEAR FROM ts.actual_start_time ) = ' . ( int ) $intYearResultId . '
					) AS total_trainer_count,
					( SELECT
							COUNT ( DISTINCT e.id )
						FROM
							training_sessions AS ts
							LEFT JOIN training_trainer_associations AS tta ON tta.id = ts.training_trainer_association_id
							LEFT JOIN actions AS a ON a.id = tta.action_id
							LEFT JOIN users AS u ON u.id = a.secondary_reference
							LEFT JOIN employees AS e ON e.id = u.employee_id
						WHERE
							a.action_result_id = ' . CActionResult::INTERESTED_TRAINER . '
							AND e.employee_status_type_id  = ' . CEmployeeStatusType::CURRENT . '
							AND EXTRACT ( YEAR FROM ts.actual_start_time ) = ' . ( int ) $intYearResultId . '
					) AS total_internal_trainer_count,
					( SELECT
							COUNT ( DISTINCT ea.id )
						FROM
							training_sessions AS ts
							LEFT JOIN training_trainer_associations AS tta ON tta.id = ts.training_trainer_association_id
							LEFT JOIN actions AS a ON a.id = tta.action_id
							LEFT JOIN employee_applications ea ON a.secondary_reference = ea.id
						WHERE
							a.action_result_id = ' . CActionResult::EXTERNAL_TRAINER . '
							AND EXTRACT ( YEAR FROM ts.actual_start_time ) = ' . ( int ) $intYearResultId . '
					) AS total_external_trainer_count
					FROM
						training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON tta.id = ts.training_trainer_association_id
						LEFT JOIN actions AS a ON a.id = tta.action_id
						LEFT JOIN users AS u ON u.id = a.secondary_reference
						LEFT JOIN employees AS e ON e.id = u.employee_id
					WHERE
						ts.deleted_on IS NULL
						AND tta.is_published = 1
						AND a.action_result_id IN ( ' . CActionResult::TRAINER . ', ' . CActionResult::INTERESTED_TRAINER . ', ' . CActionResult::EXTERNAL_TRAINER . ' )
						AND EXTRACT ( YEAR FROM ts.actual_start_time ) = ' . ( int ) $intYearResultId;

		$arrintResponse = fetchData( $strSql, $objDatabase );
		if( true == isset( $arrintResponse[0] ) ) return $arrintResponse[0];
	}

	public static function fetchTrainingSessionsCountByDate( $strStartDate, $strEndDate, $objDatabase, $arrintAddEmployeeIds = NULL, $arrintTeamIds = NULL, $boolIsDownload = false ) {

		if( false == valStr( $strStartDate ) && false == valStr( $strEndDate ) ) return NULL;

		$strWhereCondition	= '';

		if( true == valArr( $arrintAddEmployeeIds ) ) {
			$strWhereCondition	.= ' AND t.add_employee_id IN ( ' . implode( ',', $arrintAddEmployeeIds ) . ' )';
		}

		if( true == valArr( $arrintTeamIds ) ) {
			$strWhereCondition	.= ' AND t.id IN ( ' . implode( ',', $arrintTeamIds ) . ' )';
		}

		if( true == $boolIsDownload ) {

			$strSql = 'SELECT
							ts.id,
							count ( 1 ) AS expected_count,
							ts.scheduled_start_time::DATE as scheduled_date,
							to_char ( ts.scheduled_start_time::DATE, \'YYYY-MM\' ) AS month,
							e.preferred_name AS add_employee_name,
							ts.name,
							count ( CASE
										WHEN ets.is_present = 1 THEN 1
									END ) AS total_present,
							count ( CASE
										WHEN ets.is_present = 0 THEN 1
									END ) AS total_absent,
							t.name as team_name
						FROM
							training_sessions ts
							JOIN employee_training_sessions ets ON ( ts.id = ets.training_session_id )
							JOIN users u ON ( u.id = ets.user_id )
							JOIN team_employees te ON ( te.employee_id = u.employee_id AND te.is_primary_team = 1 )
							JOIN teams t ON ( t.id = te.team_id )
							JOIN employees e ON ( e.id = t.add_employee_id AND e.employee_status_type_id = 1 )
						WHERE
							ts.deleted_by IS NULL
							AND ts.scheduled_start_time::DATE BETWEEN \'' . $strStartDate . '\'
							AND \'' . $strEndDate . '\'
							' . $strWhereCondition . '
						GROUP BY
							month,
							ts.name,
							ts.id,
							e.preferred_name,
							t.name
						ORDER BY
							ts.id,
							month';
		} else {

			$strSql = '  SELECT
								count ( 1 ) AS count,
								to_char ( sub.scheduled_start_time::DATE, \'YYYY-MM\' ) AS month,
								sub.preferred_name as add_employee_name
							FROM
								(
									SELECT
										DISTINCT( ts.id ),
										ts.scheduled_start_time,
										e.preferred_name,
										e.id
									FROM
										training_sessions ts
										JOIN employee_training_sessions ets ON ( ts.id = ets.training_session_id )
										JOIN users u ON ( u.id = ets.user_id )
										JOIN team_employees te ON ( te.employee_id = u.employee_id AND te.is_primary_team = 1 )
										JOIN teams t ON ( t.id = te.team_id )
										JOIN employees e ON ( e.id = t.add_employee_id AND e.employee_status_type_id = 1 )
									WHERE
										ts.deleted_by IS NULL
										AND ts.scheduled_start_time::DATE BETWEEN \'' . $strStartDate . '\'
										AND \'' . $strEndDate . '\'
										' . $strWhereCondition . '
								) sub
							GROUP BY
								month,
								sub.preferred_name
							ORDER BY
								month';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionswithSubSessionsByTrainingSessionId( $intTrainingSessionId, $objDatabase ) {
		if( false == is_numeric( $intTrainingSessionId ) ) return NULL;

		$strSql = ' SELECT
						u.id as user_id,
						ts.id,
						ts.parent_training_session_id
					FROM
						training_sessions AS ts
						LEFT JOIN training_trainer_associations AS tta ON ( ts.training_trainer_association_id = tta.id )
						LEFT JOIN actions AS a ON ( tta.action_id = a.id )
						LEFT JOIN users AS u ON ( u.id = a.secondary_reference )
					WHERE
						ts.parent_training_session_id = ' . ( int ) $intTrainingSessionId . ' OR ts.id = ' . ( int ) $intTrainingSessionId . '';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchAverageRatingOfTrainingSessionByTrainingTrainerAssociationIdAndEmployeeId( $arrintEmployeeId, $intEmployeeId, $strFromDate, $strToDate, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) || false == valArr( $arrintEmployeeId ) ) return NULL;

		$strWhereAdditionalCondition = ( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? 'AND s.response_datetime > \'' . $strFromDate . '\' AND s.response_datetime < \'' . $strToDate . '\'': '';
		$strSql = ' SELECT main.average_training_rating,main.average_training_rating_rank FROM (
						SELECT s.subject_reference_id,
							round((SUM(sqa.numeric_weight) / count(s.id)),1) as average_training_rating,
							DENSE_RANK() OVER( ORDER BY round((SUM(sqa.numeric_weight) / count(s.id)), 1) DESC) as average_training_rating_rank
						FROM SURVEYS s
							   JOIN survey_question_answers sqa ON (
								 sqa.survey_id = s.id AND
								 sqa.survey_question_type_id = 1)
						WHERE s.survey_type_id = ' . CSurveyType::TRAINING . ' AND
								s.subject_reference_id IN (' . implode( ',', $arrintEmployeeId ) . ') ' . $strWhereAdditionalCondition . '
						GROUP BY s.subject_reference_id ) main
					WHERE main.subject_reference_id = ' . ( int ) $intEmployeeId;

		return current( fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchTrainingSessionByOfficeRoomIdByScheduledDate( $intOfficeRoomId, $strScheduledDate, $objDatabase ) {

		if( true == is_null( $intOfficeRoomId ) && false == valStr( $strScheduledDate ) ) return NULL;

		$strSql = 'SELECT 
						ts.name,
						ts.scheduled_start_time, 
						ts.scheduled_end_time 
					FROM
						training_sessions ts
						LEFT JOIN office_rooms ofr ON ( ofr.id = ts.office_room_id )
					WHERE 
						ts.scheduled_start_time::date = \'' . $strScheduledDate . '\' 
						AND ts.deleted_by IS NULL
						AND ofr.id = ' . ( int ) $intOfficeRoomId . '';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsByTestId( $intTestId, $objDatabase ) {
		if( true == is_null( $intTestId ) ) return NULL;

		$strSql = ' SELECT
						ts.*
					FROM
						training_sessions ts
						LEFT JOIN test_groups tg ON ( ts.id = tg.training_session_id )
					WHERE
						tg.test_id = ' . ( int ) $intTestId . '
					ORDER BY tg.updated_on DESC';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchUnAssignedHasAttendeesTrainingSessions( $objDatabase, $intTestId = NULL ) {
		$strWhere = '';

		if( false == empty( $intTestId ) ) {
			$strWhere = ' AND ( tg.test_id IS NULL OR tg.test_id <> ' . ( int ) $intTestId . ' )';
		}

		$strSql = ' SELECT
						ts.id,
						ts.name,
						ts.description,
						ts.scheduled_start_time,
						ets.training_session_id
					FROM
						training_sessions ts
						LEFT JOIN test_groups tg ON ( ts.id = tg.training_session_id )
						LEFT JOIN employee_training_sessions ets ON ( ets.training_session_id = ts.id )
					WHERE
						ts.is_published = 1
						AND ts.deleted_on IS NULL
						AND ts.country_code = \'' . CCountry::CODE_INDIA . '\'' . $strWhere . '
					GROUP BY
						ts.id,
						ts.name,
						ts.description,
						ts.scheduled_start_time,
						ets.training_session_id
					HAVING
						ets.training_session_id IS NOT NULL
					ORDER BY id DESC';

		return self::fetchTrainingSessions( $strSql, $objDatabase );
	}

	public static function fetchSessionsConductedBeforeOneMonth( $objDatabase, $boolOnlyInductionSessions = false ) {
		if( true == $boolOnlyInductionSessions ) {
			$strWhereCondition = ' AND ts.induction_session_id IS NOT NULL AND tg.test_id IS NOT NULL ';
		}

		$strSql = 'SELECT
						DISTINCT ( ts.id ),
						ts.name,
						ts.induction_session_id,
						ts.actual_start_time,
						e.name_full,
						e.email_address,
						tg.test_id
					FROM
						training_sessions ts
						JOIN surveys s ON( ts.id = s.trigger_reference_id )
						JOIN employees e ON( e.id = s.subject_reference_id )
						LEFT JOIN test_groups tg ON( ts.id = tg.training_session_id )
					WHERE
						ts.deleted_on IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND	ts.country_code = \'' . CCountry::CODE_INDIA . '\'
						AND ( ts.survey_template_id IS NOT NULL OR ts.parent_training_session_id IS NOT NULL ) ' . $strWhereCondition . '
						AND ts.actual_start_time::DATE = NOW()::DATE - INTERVAL \'30 days\'
					ORDER BY
						ts.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTrainingSessionsWithTestDetails( $objDatabase ) {
		$strSql = ' SELECT
						ts.id AS training_session_id,
						ts.induction_session_id,
						t.id AS test_id,
						t.name AS test_name,
						t.passing_percentage::FLOAT
					FROM
						test_groups tg
						JOIN training_sessions ts ON ( ts.id = tg.training_session_id )
						LEFT JOIN tests t ON ( t.id = tg.test_id )
					WHERE
						t.deleted_on IS NULL
						AND ts.induction_session_id IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchEmployeeIdByTrainingSessionIdByTrainingTrainerAssociationId( $intTrainingSessionId, $intTrainingTrainerAssociationId, $objDatabase ) {
		if( false == is_numeric( $intTrainingSessionId ) || false == is_numeric( $intTrainingTrainerAssociationId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						u.employee_id
					FROM
						training_sessions ts
						JOIN training_trainer_associations tta ON( ts.training_trainer_association_id = tta.id )
						JOIN actions a ON( tta.action_id = a.id )
						JOIN users u ON( a.secondary_reference = u.id )
					WHERE
						ts.id = ' . ( int ) $intTrainingSessionId . '
						AND ts.training_trainer_association_id = ' . ( int ) $intTrainingTrainerAssociationId;

		$arrintEmployeeIds = fetchData( $strSql, $objDatabase );
		if( true == valArr( $arrintEmployeeIds ) ) {
			return $arrintEmployeeIds[0]['employee_id'];
		}
		return NULL;
	}

}
?>