<?php

class CPsEventType extends CBasePsEventType {

	const ANNOUNCEMENT						= 6;
	const IMPLEMENTATION_PROJECT_MANAGEMENT = 7;

	public static $c_arrintExcludedPsEventTypes = [ self::IMPLEMENTATION_PROJECT_MANAGEMENT ];

}
?>