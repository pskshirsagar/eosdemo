<?php

class CTaskCompany extends CBaseTaskCompany {

	protected $m_strClientName;
	protected $m_strClientStatus;

	protected $m_boolIsKeyClient;

	protected $m_intDatabaseId;
	protected $m_intPsLeadId;
	protected $m_intKeyClientRank;
	protected $m_intClientRiskStatus;
	protected $m_intNumberOfUnits;
	protected $m_intSupportEmployeeId;
	protected $m_strSupportEmployeeName;

	/**
	 * Get Functions
	 */

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function getClientRiskStatus() {
		return $this->m_intClientRiskStatus;
	}

	public function getPsLeadId() {
		return $this->m_intPsLeadId;
	}

	public function getIsKeyClient() {
		return $this->m_boolIsKeyClient;
	}

	public function getKeyClientRank() {
		return $this->m_intKeyClientRank;
	}

	public function getDatabaseId() {
		return $this->m_intDatabaseId;
	}

	public function getClientStatus() {
		return $this->m_strClientStatus;
	}

	public function getNumberOfUnits() {
		return $this->m_intNumberOfUnits;
	}

	public function getSupportEmployeeId() {
		return $this->m_intSupportEmployeeId;
	}

	public function getSupportEmployeeName() {
		return $this->m_strSupportEmployeeName;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrstrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrstrValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrstrValues['client_name'] ) )					$this->setClientName( $arrstrValues['client_name'] );
		if( true == isset( $arrstrValues['ps_lead_id'] ) )					$this->setPsLeadId( $arrstrValues['ps_lead_id'] );
		if( true == isset( $arrstrValues['is_key_client'] ) )				$this->setIsKeyClient( $arrstrValues['is_key_client'] );
		if( true == isset( $arrstrValues['client_risk_status'] ) )			$this->setClientRiskStatus( $arrstrValues['client_risk_status'] );
		if( true == isset( $arrstrValues['key_client_rank'] ) )				$this->setKeyClientRank( $arrstrValues['key_client_rank'] );
		if( true == isset( $arrstrValues['database_id'] ) )					$this->setDatabaseId( $arrstrValues['database_id'] );
		if( true == isset( $arrstrValues['client_status'] ) )				$this->setClientStatus( $arrstrValues['client_status'] );
		( true == isset( $arrstrValues['number_of_units'] ) ) ? $this->setNumberOfUnits( $arrstrValues['number_of_units'] ) : $this->setNumberOfUnits( 0 );
		if( true == isset( $arrstrValues['support_employee_id'] ) )				$this->setSupportEmployeeId( $arrstrValues['support_employee_id'] );
		if( true == isset( $arrstrValues['support_employee_name'] ) )				$this->setSupportEmployeeName( $arrstrValues['support_employee_name'] );

		return;
	}

	public function setClientName( $strClientName ) {
		$this->m_strClientName = $strClientName;
	}

	public function setPsLeadId( $intPsLeadId ) {
		$this->m_intPsLeadId = $intPsLeadId;
	}

	public function setIsKeyClient( $boolIsKeyClient ) {
		$this->m_boolIsKeyClient = $boolIsKeyClient;
	}

	public function setClientRiskStatus( $intClientRiskStatus ) {
		$this->m_intClientRiskStatus = $intClientRiskStatus;
	}

	public function setKeyClientRank( $intKeyClientRank ) {
		$this->m_intKeyClientRank = $intKeyClientRank;
	}

	public function setDatabaseId( $intDatabaseId ) {
		$this->m_intDatabaseId = $intDatabaseId;
	}

	public function setClientStatus( $strClientStatus ) {
		$this->m_strClientStatus = $strClientStatus;
	}

	public function setNumberOfUnits( $intNumberOfUnits ) {
		$this->m_intNumberOfUnits = $intNumberOfUnits;
	}

	public function setSupportEmployeeId( $intSupportEmployeeId ) {
		$this->m_intSupportEmployeeId = $intSupportEmployeeId;
	}

	public function setSupportEmployeeName( $strSupportEmployeeName ) {
		$this->m_strSupportEmployeeName = $strSupportEmployeeName;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>