<?php

class CIpsClientPortalStat extends CBaseIpsClientPortalStat {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIpsPortalVersionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valActivePropertiesCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompletePropertiesCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>