<?php

class CAccountServiceLevelType extends CBaseAccountServiceLevelType {

	const ACCOUNT_SERVICE_LEVEL_0 = 1;
	const ACCOUNT_SERVICE_LEVEL_1 = 2;
	const ACCOUNT_SERVICE_LEVEL_2 = 3;
	const ACCOUNT_SERVICE_LEVEL_3 = 4;
	const ACCOUNT_SERVICE_LEVEL_4 = 5;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>