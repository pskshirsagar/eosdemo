<?php

class CCompensationGrade extends CBaseCompensationGrade {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayReviewYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDesignationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTechnologyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGradeValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStartAmountEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEndAmountEncrypted() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>