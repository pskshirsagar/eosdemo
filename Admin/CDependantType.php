<?php

class CDependantType extends CBaseDependantType {

	const SPOUSE 	= 1;
	const BROTHER 	= 2;
	const FATHER 	= 3;
	const MOTHER 	= 4;
	const CHILD 	= 5;
	const SISTER 	= 6;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>