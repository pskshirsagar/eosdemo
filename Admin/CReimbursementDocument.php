<?php

class CReimbursementDocument extends CBaseReimbursementDocument {

	protected $m_strReceiptFileName;

	public function getReceiptFileName() {
		return $this->m_strReceiptFileName;
	}

	public function setReceiptFileName( $strReceiptFileName ) {
		$this->m_strReceiptFileName = $strReceiptFileName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {

		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['receipt_filename'] ) )	$this->setReceiptFileName( $arrmixValues['receipt_filename'] );
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>