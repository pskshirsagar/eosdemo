<?php

use Psi\Eos\Admin\CPsProducts;

class CCompetitor extends CBaseCompetitor {

	const PROPERTY_SOLUTION_ID			= 16;
	const AUM							= 19;
	const YIELDSTAR						= 61;
	const LRO_RAINMAKER					= 67;

	const YARDI							= 2;
	const MRI							= 3;
	const REAL_PAGE						= 4;
	const PROPERTY_BOSS					= 26;
	const APPFOLIO						= 28;
	const REMANAGE_RETRANSFORM			= 29;
	const RENT_MANAGER					= 30;
	const BUILDIUM						= 33;
	const NONE							= 50;
	const AMSI							= 74;
	const ENTRATA						= 81;
	const TIMBERLINE					= 84;
	const YARDI_GENESIS					= 88;
	const JENARK						= 89;
	const PROPERTYWARE					= 90;
	const QUICKBOOKS					= 92;
	const RENT_RIGHT					= 94;
	const SKYLINE						= 95;
	const SPECTRA						= 96;
	const TENANT_PRO					= 97;
	const OTHER							= 102;
	const PROMAS						= 101;
	const RESMAN						= 119;
	const LEASE_UP_NEW_CONSTRUCTION		= 141;
	const EXCEL							= 145;
	const GENERIC						= 149;

	public static $c_arrintSIMCompetitors = array(
		self::AMSI,
		self::APPFOLIO,
		self::BUILDIUM,
		self::JENARK,
		self::LEASE_UP_NEW_CONSTRUCTION,
		self::PROMAS,
		self::PROPERTY_BOSS,
		self::PROPERTYWARE,
		self::REAL_PAGE,
		self::REMANAGE_RETRANSFORM,
		self::RENT_RIGHT,
		self::RESMAN,
		self::SKYLINE,
		self::SPECTRA,
		self::TENANT_PRO,
		self::TIMBERLINE,
		self::YARDI,
		self::YARDI_GENESIS,
		self::OTHER
	);

	public static $c_arrintWorkbookCompetitors = array(
		self::AMSI,
		self::APPFOLIO,
		self::BUILDIUM,
		self::EXCEL,
		self::ENTRATA,
		self::LEASE_UP_NEW_CONSTRUCTION,
		self::JENARK,
		self::MRI,
		self::NONE,
		self::OTHER,
		self::PROPERTY_BOSS,
		self::QUICKBOOKS,
		self::RENT_MANAGER,
		self::TENANT_PRO,
		self::YARDI,
		self::YARDI_GENESIS,
		self::REAL_PAGE,
		self::GENERIC
	);

	protected $m_intProductId;
	protected $m_intIsSelected;

	public function __construct() {
		parent::__construct();
		$this->m_intIsSelected = 0;
		return;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false );

		if( true == isset( $arrmixValues['is_selected'] ) ) $this->setIsSelected( $arrmixValues['is_selected'] );
		if( true == isset( $arrmixValues['ps_product_id'] ) ) $this->setProductId( $arrmixValues['ps_product_id'] );

		return;
	}

	public function setProductId( $intProductId ) {
		$this->m_intProductId = CStrings::strToIntDef( $intProductId, NULL, false );
	}

	public function setIsSelected( $intIsSelected ) {
		$this->m_intIsSelected = CStrings::strToIntDef( $intIsSelected, NULL, false );
	}

	public function getProductId() {
		return $this->m_intProductId;
	}

	public function getIsSelected() {
		return $this->m_intIsSelected;
	}

	public function createPsProductCompetitor() {

		$objCPsProductCompetitor = new CPsProductCompetitor();

		$objCPsProductCompetitor->setCompetitorId( $this->getId() );

		return $objCPsProductCompetitor;
	}

	public function valName( $objAdminDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required' ) );
		}

		if( true == $boolIsValid ) {
			$intConflictingCompetitorCount = \Psi\Eos\Admin\CCompetitors::createService()->fetchConflictingCCompetitorsCountByIdByName( $this->getId(), $this->getName(), $objAdminDatabase );

			if( 0 < $intConflictingCompetitorCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'The name you entered is already in use.' ) );
			}
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
						$boolIsValid &= $this->valName( $objAdminDatabase );
						$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchUnassignedPsProducts( $objAdminDatabase ) {
		return CPsProducts::createService()->fetchUnassignedPsProductsByCompetitorId( $this->getId(), $objAdminDatabase );
	}

	public function fetchPsProductCompetitors( $objAdminDatabase ) {
		return CPsProductCompetitors::fetchPsProductCompetitorsByCompetitorId( $this->getId(), $objAdminDatabase );
	}

}
?>