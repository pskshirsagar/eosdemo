<?php

class CFeedback extends CBaseFeedback {

	protected $m_objFeedback;

	public function setFeedbackDetails( $objFeedback ) {
		$this->m_objFeedback = $objFeedback;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProjectId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		if( false == valId( $this->m_objFeedback->getPsProductId() ) ) {
			$this->m_objFeedback->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is required.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valPsProductOptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFeedbackTypeId() {
		$boolIsValid = true;
		if( false == valId( $this->m_objFeedback->getFeedbackTypeId() ) ) {
			$this->m_objFeedback->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'feedback_type_id', 'Feedback type is required.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valPriorityTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		if( false == valStr( $this->m_objFeedback->getTitle() ) ) {
			$this->m_objFeedback->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Title is required.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( false == valStr( $this->m_objFeedback->getDescription() ) ) {
			$this->m_objFeedback->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCancelReason() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$this->m_objFeedback = $this;
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valFeedbackTypeId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>