<?php

class CInvoiceBatchStep extends CBaseInvoiceBatchStep {

	const NEW_BATCH 							= 10;
	const TRACK_1_CHARGE_POSTING_READY			= 20;
	const TRACK_1_CHARGES_POSTED				= 30;
	const TRACK_1_EFT_CHARGES_POSTING_READY		= 35;
	const TRACK_1_EFT_CHARGES_POSTED			= 36;
	const TRACK_1_INVOICE_GENERATION_READY		= 40;
	const TRACK_1_INVOICES_GENERATED			= 50;
	const TRACK_1_SALES_TAX_POST_READY			= 60;
	const TRACK_1_SALES_TAX_POSTED				= 70;
	const TRACK_1_PAYMENT_POSTING_READY			= 80;
	const TRACK_1_PAYMENTS_POSTED				= 90;
	const TRACK_1_INVOICE_EMAILING_READY		= 100;
	const TRACK_1_INVOICES_EMAILED				= 110;

	const TRACK_2_CHARGE_POSTING_READY			= 120;
	const TRACK_2_CHARGES_POSTED				= 130;
	const TRACK_2_EFT_CHARGES_POSTING_READY		= 135;
	const TRACK_2_EFT_CHARGES_POSTED			= 136;
	const TRACK_2_INVOICE_GENERATION_READY		= 140;
	const TRACK_2_INVOICES_GENERATED			= 150;
	const TRACK_2_SALES_TAX_POST_READY			= 160;
	const TRACK_2_SALES_TAX_POSTED				= 170;
	const TRACK_2_PAYMENT_POSTING_READY			= 180;
	const TRACK_2_PAYMENTS_POSTED				= 190;
	const TRACK_2_INVOICE_EMAILING_READY		= 200;
	const TRACK_2_INVOICES_EMAILED				= 210;

	const COMPLETE								= 1000;

	protected $m_arrstrNavigationPreferencesArray;

	public static function getInvoiceBatchStepNameByInvoiceBatchStepId( $intInvoiceBatchStepId ) {

		switch( $intInvoiceBatchStepId ) {
			case self::NEW_BATCH:
				$strInvoiceBatchStepName = 'Track 1 - Begin Track 1 Charge Posting';
				break;

			case self::TRACK_1_CHARGE_POSTING_READY:
				$strInvoiceBatchStepName = 'Track 1 - Charge Posting In Progress';
				break;

			case self::TRACK_1_CHARGES_POSTED:
				$strInvoiceBatchStepName = 'Track 1 - Charges Posted';
				break;

			case self::TRACK_1_EFT_CHARGES_POSTING_READY:
				$strInvoiceBatchStepName = 'Track 1 - Eft Charge Posting In Progress';
				break;

			case self::TRACK_1_EFT_CHARGES_POSTED:
				$strInvoiceBatchStepName = 'Track 1 - Eft Charges Posted';
				break;

			case self::TRACK_1_INVOICE_GENERATION_READY:
				$strInvoiceBatchStepName = 'Track 1 - Invoice Generation In Progress';
				break;

			case self::TRACK_1_INVOICES_GENERATED:
				$strInvoiceBatchStepName = 'Track 1 - Invoices Generated';
				break;

			case self::TRACK_1_SALES_TAX_POST_READY:
				$strInvoiceBatchStepName = 'Track 1 - Sales Tax Posting';
				break;

			case self::TRACK_1_SALES_TAX_POSTED:
				$strInvoiceBatchStepName = 'Track 1 - Sales Tax Posted';
				break;

			case self::TRACK_1_PAYMENT_POSTING_READY:
				$strInvoiceBatchStepName = 'Track 1 - Payment Posting In Progress';
				break;

			case self::TRACK_1_PAYMENTS_POSTED:
				$strInvoiceBatchStepName = 'Track 1 - Payments Posted';
				break;

			case self::TRACK_1_INVOICE_EMAILING_READY:
				$strInvoiceBatchStepName = 'Track 1 - Invoice Emailing In Progress';
				break;

			case self::TRACK_1_INVOICES_EMAILED:
				$strInvoiceBatchStepName = 'Track 1 - Invoices Emailed';
				break;

			case self::TRACK_2_CHARGE_POSTING_READY:
				$strInvoiceBatchStepName = 'Track 2 - Charge Posting In Progress';
				break;

			case self::TRACK_2_CHARGES_POSTED:
				$strInvoiceBatchStepName = 'Track 2 - Charges Posted';
				break;

			case self::TRACK_2_EFT_CHARGES_POSTING_READY:
				$strInvoiceBatchStepName = 'Track 2 - Eft Charge Posting In Progress';
				break;

			case self::TRACK_2_EFT_CHARGES_POSTED:
				$strInvoiceBatchStepName = 'Track 2 - Eft Charges Posted';
				break;

			case self::TRACK_2_INVOICE_GENERATION_READY:
				$strInvoiceBatchStepName = 'Track 2 - Invoice Generation In Progress';
				break;

			case self::TRACK_2_INVOICES_GENERATED:
				$strInvoiceBatchStepName = 'Track 2 - Invoices Generated';
				break;

			case self::TRACK_2_SALES_TAX_POST_READY:
				$strInvoiceBatchStepName = 'Track 2 - Sales Tax Posting';
				break;

			case self::TRACK_2_SALES_TAX_POSTED:
				$strInvoiceBatchStepName = 'Track 2 - Sales Tax Posted';
				break;

			case self::TRACK_2_PAYMENT_POSTING_READY:
				$strInvoiceBatchStepName = 'Track 2 - Payment Posting In Progress';
				break;

			case self::TRACK_2_PAYMENTS_POSTED:
				$strInvoiceBatchStepName = 'Track 2 - Payments Posted';
				break;

			case self::TRACK_2_INVOICE_EMAILING_READY:
				$strInvoiceBatchStepName = 'Track 2 - Invoice Emailing In Progress';
				break;

			case self::TRACK_2_INVOICES_EMAILED:
				$strInvoiceBatchStepName = 'Track 2 - Invoices Emailed';
				break;

			case self::COMPLETE:
				$strInvoiceBatchStepName = 'Batch Completed';
				break;

			default:
				// default case
				$strInvoiceBatchStepName = '';
				break;
		}
		return $strInvoiceBatchStepName;
	}

	public function getNextTitle() {

		switch( $this->getId() ) {

			case self::NEW_BATCH:
				return 'Track 1 - Begin Track 1 Charge Posting';
				break;

			case self::TRACK_1_CHARGES_POSTED:
				return 'Track 1 - Begin Track 1 Eft Charge Posting';
				break;

			case self::TRACK_1_EFT_CHARGES_POSTED:
				return 'Track 1 - Begin Track 1 Invoice Generation';
				break;

			case self::TRACK_1_INVOICES_GENERATED:
				return 'Track 1 - Begin Track 1 Sales Tax Posting';
				break;

			case self::TRACK_1_SALES_TAX_POSTED:
				return 'Track 1 - Begin Track 1 Payment Posting';
				break;

			case self::TRACK_1_PAYMENTS_POSTED:
				return 'Track 1 - Begin Track 1 Invoice Emailing';
				break;

			case self::TRACK_1_INVOICES_EMAILED:
				return 'Track 1 - Begin Track 2 Charge Posting';
				break;

			case self::TRACK_2_CHARGES_POSTED:
				return 'Track 2 - Begin Track 2 Eft Charge Posting';
				break;

			case self::TRACK_2_EFT_CHARGES_POSTED:
				return 'Track 2 - Begin Track 2 Invoice Generation';
				break;

			case self::TRACK_2_INVOICES_GENERATED:
				return 'Track 2 - Begin Track 2 Sales Tax Posting';
				break;

			case self::TRACK_2_SALES_TAX_POSTED:
				return 'Track 2 - Begin Track 2 Payment Posting';
				break;

			case self::TRACK_2_PAYMENTS_POSTED:
				return 'Track 2 - Begin Track 2 Invoice Emailing';
				break;

			case self::TRACK_2_INVOICES_EMAILED:
				return 'Track 2 - Flag Batch As Complete';
				break;

			default:
				// default case
				break;
		}

		return '';
	}

	public function getSimpleName() {

		switch( $this->getId() ) {

			case self::TRACK_1_CHARGES_POSTED:
				return 'Track 1 Charges Posted';
				break;

			case self::TRACK_1_EFT_CHARGES_POSTED:
				return 'Track 1 Eft Charges Posted';
				break;

			case self::TRACK_1_INVOICES_GENERATED:
				return 'Track 1 Invoices Generated';
				break;

			case self::TRACK_1_SALES_TAX_POSTED:
				return 'Track 1 Sales Tax Posted';
				break;

			case self::TRACK_1_PAYMENTS_POSTED:
				return 'Track 1 Payments Posted';
				break;

			case self::TRACK_1_INVOICES_EMAILED:
				return 'Track 1 Invoices Emailed';
				break;

			case self::TRACK_2_CHARGES_POSTED:
				return 'Track 2 Charges Posted';
				break;

			case self::TRACK_2_EFT_CHARGES_POSTED:
				return 'Track 2 Eft Charges Posted';
				break;

			case self::TRACK_2_INVOICES_GENERATED:
				return 'Track 2 Invoices Generated';
				break;

			case self::TRACK_2_SALES_TAX_POSTED:
				return 'Track 2 Sales Tax Posted';
				break;

			case self::TRACK_2_PAYMENTS_POSTED:
				return 'Track 2 Payments Posted';
				break;

			case self::TRACK_2_INVOICES_EMAILED:
				return 'Track 2 Invoices Emailed';
				break;

			default:
				return NULL;
			break;
		}
	}

}
?>