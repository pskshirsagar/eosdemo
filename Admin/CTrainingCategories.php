<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTrainingCategories
 * Do not add any new functions to this class.
 */

class CTrainingCategories extends CBaseTrainingCategories {

	public static function fetchTrainingCategoriesList( $objDatabase ) {

		$strSql = 'SELECT
						tc.*
					FROM
						training_categories AS tc
					WHERE
						tc.training_category_id IS NULL';

		return self::fetchTrainingCategories( $strSql, $objDatabase );
	}

	public static function fetchSubTrainingCategoriesList( $objDatabase ) {

		$strSql = 'SELECT
						tc.*
					FROM
						training_categories AS tc
					WHERE
						tc.training_category_id IS NOT NULL';

		return self::fetchTrainingCategories( $strSql, $objDatabase );
	}

}
?>