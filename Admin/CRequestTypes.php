<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CRequestTypes
 * Do not add any new functions to this class.
 */

class CRequestTypes extends CBaseRequestTypes {

	public static function fetchRequestTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CRequestType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchRequestType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRequestType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>