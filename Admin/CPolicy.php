<?php

class CPolicy extends CBasePolicy {

	public function valName() {
		$boolIsValid = true;

		if( false == valStr( $this->getName() ) || true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Policy name is required. ' ) );
			return $boolIsValid;
		}

		if( false == \Psi\CStringService::singleton()->preg_match( '/[a-zA-Z]+[a-zA-Z0-9 .\:\;\<\>\[\]\{\}\(\)\$\#\%\*\@\~\/\,\-\_\&\'\"\!\?\/\\\]+$/', $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Enter valid policy name. ' ) );
		}

		return $boolIsValid;
	}

	public function valUniqueTitle( $strCountryCode, $objDatabase ) {
		$boolIsValid = true;

		if( false == is_null( $this->getName() ) || true == valStr( $this->getName() ) ) {

			$intCountPolicies = CPolicies::fetchPoliciesCountByName( $this->getName(), $strCountryCode, $objDatabase );

			if( 0 != $intCountPolicies ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'unique name', ' Policy with this name already exists.' ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $boolIsEdit, $objDatabase = NULL, $strCountryCode ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'validate_employee_policy_name':
				$boolIsValid &= $this->valName();
				if( false == $boolIsEdit ) {
					$boolIsValid &= $this->valUniqueTitle( $strCountryCode, $objDatabase );
				}
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false, $boolSoftDelete = false ) {

		if( false == $boolSoftDelete ) {

			return parent::delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		} else {

			$this->setDeletedBy( $intCurrentUserId );
			$this->setDeletedOn( 'NOW()' );

			return parent::update( $intCurrentUserId, $objDatabase );
		}
		return;
	}

}
?>