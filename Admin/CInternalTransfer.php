<?php

class CInternalTransfer extends CBaseInternalTransfer {

    protected $m_strDebitProcessingBankAccountName;
    protected $m_strCreditProcessingBankAccountName;
    protected $m_strPaymentStatusType;
    protected $m_strInternalTransferType;

	public function setDebitProcessingBankAccountName( $strDebitProcessingBankAccountName ) {
	    $this->m_strDebitProcessingBankAccountName = CStrings::strTrimDef( $strDebitProcessingBankAccountName, 240, NULL, true );
	}

	public function getDebitProcessingBankAccountName() {
	    return $this->m_strDebitProcessingBankAccountName;
	}

	public function setCreditProcessingBankAccountName( $strCreditProcessingBankAccountName ) {
	    $this->m_strCreditProcessingBankAccountName = CStrings::strTrimDef( $strCreditProcessingBankAccountName, 240, NULL, true );
	}

	public function getCreditProcessingBankAccountName() {
	    return $this->m_strCreditProcessingBankAccountName;
	}

	public function setPaymentStatusType( $strPaymentStatusType ) {
	    $this->m_strPaymentStatusType = CStrings::strTrimDef( $strPaymentStatusType, 240, NULL, true );
	}

	public function getPaymentStatusType() {
	    return $this->m_strPaymentStatusType;
	}

	public function setInternalTransferType( $strInternalTransferType ) {
	    $this->m_strInternalTransferType = CStrings::strTrimDef( $strInternalTransferType, 240, NULL, true );
	}

	public function getInternalTransferType() {
	    return $this->m_strInternalTransferType;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valPaymentAmount() {
	    $boolIsValid = true;

	    if( true == empty( $this->m_intPaymentAmount ) || false == is_numeric( $this->m_intPaymentAmount ) ) {
	        $boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'payment_amount', 'Amount is required.' ) );
	    }

	    return $boolIsValid;
	}

	public function valInternalTransferTypeId() {
	    $boolIsValid = true;

	    if( true == empty( $this->m_intInternalTransferTypeId ) || false == is_numeric( $this->m_intInternalTransferTypeId ) ) {
	        $boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'internal_transfer_type_id', 'Internal Transfer Type is required.' ) );
	    }

	    return $boolIsValid;
	}

	public function valDebitProcessingBankAccountId() {
	    $boolIsValid = true;

	    if( true == empty( $this->m_intDebitProcessingBankAccountId ) || false == is_numeric( $this->m_intDebitProcessingBankAccountId ) ) {
	        $boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'debit_processing_bank_account_id', 'Transfer From Account is required.' ) );
	    }

	    return $boolIsValid;
	}

	public function valCreditProcessingBankAccountId() {
	    $boolIsValid = true;

	    if( true == empty( $this->m_intCreditProcessingBankAccountId ) || false == is_numeric( $this->m_intCreditProcessingBankAccountId ) ) {
	        $boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'credit_processing_bank_account_id', 'Transfer To Account is required.' ) );
	    }

	    return $boolIsValid;
	}

	public function valEffectiveOn() {
	    $boolIsValid = true;

        if( true == empty( $this->m_strEffectiveOn ) || false == CValidation::validateDate( $this->m_strEffectiveOn ) ) {
            $boolIsValid = false;
            $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'effective_on', 'Effective On Date is required.' ) );
       	}

	    return $boolIsValid;
	}

	public function valMerchantGatewayId() {
	    $boolIsValid = true;

	    if( ( false == empty( $this->m_intDebitProcessingBankAccountId ) && true == is_numeric( $this->m_intDebitProcessingBankAccountId ) ) && ( true == empty( $this->m_intMerchantGatewayId ) || false == is_numeric( $this->m_intMerchantGatewayId ) ) ) {
	        $boolIsValid = false;
	        $this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'merchant_gateway_id', 'Unable to fetch Merchant Gateway Id.' ) );
	    }

	    return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			    $boolIsValid &= $this->valPaymentAmount();
			    $boolIsValid &= $this->valDebitProcessingBankAccountId();
			    $boolIsValid &= $this->valCreditProcessingBankAccountId();
			    $boolIsValid &= $this->valEffectiveOn();
			    $boolIsValid &= $this->valMerchantGatewayId();
				break;

			case VALIDATE_DELETE:
                // Delete case
				break;

			default:
		   		// Default case
		   		break;
		}

		return $boolIsValid;
	}
}
?>
