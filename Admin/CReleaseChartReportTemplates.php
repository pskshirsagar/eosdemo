<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseChartReportTemplates
 * Do not add any new functions to this class.
 */

class CReleaseChartReportTemplates extends CBaseReleaseChartReportTemplates {

	public static function fetchAllReleaseChartReportTemplateNames( $objDatabase ) {

		$strSql = 'SELECT
						rcrt.*
					FROM
						release_chart_report_templates rcrt';

		return self::fetchCachedObjects( $strSql, 'CReleaseChartReportTemplate', $objDatabase, DATA_CACHE_MEMORY );
	}

	public static function fetchReleaseChartReportTemplatesByIds( $arrintChartIds, $objDatabase ) {

		if( false == valArr( $arrintChartIds ) ) {
			return NULL;
		}
		$strChartIds = implode( ', ', $arrintChartIds );

		$strSql = 'SELECT
						rcrt.id,
						rcrt.name,
						rcrt.chart_dataset,
						rcrt.chart_parameters,
						rct.name as chart_type
					FROM
						release_chart_report_templates rcrt
						LEFT JOIN release_chart_types rct ON( rcrt.release_chart_type_id = rct.id )
					WHERE
						rcrt.id IN ( ' . $strChartIds . ' )';

		$arrmixReleaseChartReportTemplates = fetchData( $strSql, $objDatabase );
		$arrmixReleaseChartReportTemplates = rekeyArray( 'id', $arrmixReleaseChartReportTemplates );

		return $arrmixReleaseChartReportTemplates;
	}

}
?>