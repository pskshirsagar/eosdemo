<?php

class CApiRequestIp extends CBaseApiRequestIp {

	protected $m_strClientName;

	/**
	 * Get Functions
	 */

	public function getClientName() {
		return $this->m_strClientName;
	}

	/**
	 * Set Functions
	 */

	public function setClientName( $strClientName ) {
		$this->m_strClientName = $strClientName;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['client_name'] ) )	$this->setClientName( $arrmixValues['client_name'] );

		return;
	}

	public function valCid() {
		$boolIsValid = true;

		if( false == valStr( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'Client Id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIpAddress( $objDatabase ) {
		$boolIsValid = true;

		if( false == valStr( $this->getIpAddress() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'IP Address is required.' ) );
			return $boolIsValid;
		}

		$objValidation = new \Psi\Libraries\UtilValidation\CValidation();

		if( false == $objValidation->validateIpAddress( $this->getIpAddress(), true, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'IP Address [ ' . $this->getIpAddress() . ' ] is incorrect.' ) );
		} else {
			$strFrom = ' api_request_ips ari
						JOIN api_requests ar ON ( ar.id = ari.api_request_id AND ar.cid = ari.cid )';

			$strWhere = ' WHERE
							ari.cid = ' . ( int ) $this->getCid() . '
							AND ari.ip_address = \'' . $this->getIpAddress() . '\'
							AND ar.final_approved_on IS NOT NULL';

			$intApiRequestIpCount = \Psi\Eos\Admin\CApiRequestIps::createService()->fetchApprovedApiRequestIpCount( $strWhere, $objDatabase, $strFrom );
			if( 0 != $intApiRequestIpCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ip_address', 'IP Address [ ' . $this->getIpAddress() . ' ] is already approved.' ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valIpAddress( $objDatabase );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>