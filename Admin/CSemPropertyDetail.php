<?php

class CSemPropertyDetail extends CBaseSemPropertyDetail {

	protected $m_strSearchAddress;

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDuplicateRecord( $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		if( true == valObj( $objAdminDatabase, 'CDatabase' ) && false == is_null( $this->getPropertyId() ) && ( CClients::$c_intNullCid !== $this->getCid() ) ) {

			if( 0 < CSemPropertyDetails::fetchSemPropertyDetailCount( 'WHERE cid = ' . ( int ) $this->getCid() . ' AND property_id = ' . ( int ) $this->getPropertyId() . ' AND id <> ' . ( int ) $this->getId(), $objAdminDatabase ) ) {
				$boolIsValid &= false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'duplicate', 'Record already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDuplicateRecord( $objAdminDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			case 'sem_property_setting_udpate':
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valDuplicateRecord( $objAdminDatabase );
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['search_address'] ) ) $this->setSearchAddress( $arrmixValues['search_address'] );
	}

	public function setSearchAddress( $strSearchAddress ) {

		$this->m_strSearchAddress = \Psi\CStringService::singleton()->htmlentities( \Psi\CStringService::singleton()->ucwords( $strSearchAddress ) );
	}

	/**
	 * Get Functions
	 */

	public function mapPropertyAddressData( $objPropertyAddress ) {

		$this->setStreetLine1( $objPropertyAddress->getStreetLine1() );
		$this->setStreetLine2( $objPropertyAddress->getStreetLine2() );
		$this->setStreetLine3( $objPropertyAddress->getStreetLine3() );
		$this->setCity( $objPropertyAddress->getCity() );
		$this->setCounty( $objPropertyAddress->getCounty() );
		$this->setStateCode( $objPropertyAddress->getStateCode() );
		$this->setProvince( $objPropertyAddress->getProvince() );
		$this->setPostalCode( $objPropertyAddress->getPostalCode() );
		$this->setCountryCode( $objPropertyAddress->getCountryCode() );
		$this->setLongitude( $objPropertyAddress->getLongitude() );
		$this->setLatitude( $objPropertyAddress->getLatitude() );

		return true;
	}

	public function getIsRequiredUpdate( $objPropertyAddress ) {
		$boolIsRequiredUpdate = false;

		if( $this->getStreetLine1() != $objPropertyAddress->getStreetLine1() ) {
			$this->setStreetLine1( $objPropertyAddress->getStreetLine1() );
			$boolIsRequiredUpdate = true;
		}

		if( $this->getStreetLine2() != $objPropertyAddress->getStreetLine2() ) {
			$this->setStreetLine2( $objPropertyAddress->getStreetLine2() );
			$boolIsRequiredUpdate = true;
		}

		if( $this->getStreetLine3() != $objPropertyAddress->getStreetLine3() ) {
			$this->setStreetLine3( $objPropertyAddress->getStreetLine3() );
			$boolIsRequiredUpdate = true;
		}

		if( $this->getCity() != $objPropertyAddress->getCity() ) {
			$this->setCity( $objPropertyAddress->getCity() );
			$boolIsRequiredUpdate = true;
		}

		if( $this->getCounty() != $objPropertyAddress->getCounty() ) {
			$this->setCounty( $objPropertyAddress->getCounty() );
			$boolIsRequiredUpdate = true;
		}

		if( $this->getStateCode() != $objPropertyAddress->getStateCode() ) {
			$this->setStateCode( $objPropertyAddress->getStateCode() );
			$boolIsRequiredUpdate = true;
		}

		if( $this->getProvince() != $objPropertyAddress->getProvince() ) {
			$this->setProvince( $objPropertyAddress->getProvince() );
			$boolIsRequiredUpdate = true;
		}

		if( $this->getPostalCode() != $objPropertyAddress->getPostalCode() ) {
			$this->setPostalCode( $objPropertyAddress->getPostalCode() );
			$boolIsRequiredUpdate = true;
		}

		if( $this->getCountryCode() != $objPropertyAddress->getCountryCode() ) {
			$this->setCountryCode( $objPropertyAddress->getCountryCode() );
			$boolIsRequiredUpdate = true;
		}

		if( $this->getLongitude() != $objPropertyAddress->getLongitude() ) {
			$this->setLongitude( $objPropertyAddress->getLongitude() );
			$boolIsRequiredUpdate = true;
		}

		if( $this->getLatitude() != $objPropertyAddress->getLatitude() ) {
			$this->setLatitude( $objPropertyAddress->getLatitude() );
			$boolIsRequiredUpdate = true;
		}

		return $boolIsRequiredUpdate;
	}

	public function getSearchAddress() {
		return $this->m_strSearchAddress;
	}

	public function getSeoFormattedCity() {
		// return \Psi\CStringService::singleton()->strtolower( preg_replace( array( '/[^a-zA-Z0-9\s_'. "'" .'\-]+/', '/(\s|-)+/' ), array( '', '-' ), $this->getCity() ));
		return trim( \Psi\CStringService::singleton()->strtolower( preg_replace( array( '/[^a-zA-Z0-9\s_\-]+/', '/(\s|-|_)+/' ), array( '', '-' ), $this->getCity() ) ) );
	}

	public function getSeoFormattedState() {
		return \Psi\CStringService::singleton()->strtolower( preg_replace( array( '/[^a-zA-Z0-9\s_\-]+/', '/(\s|-|_)+/' ), array( '', '-' ), \Psi\Eos\Admin\CStates::createService()->determineStateNameByStateCode( $this->getStateCode() ) ) );
	}

	public function fetchSemCampaign( $objAdminDatabase ) {
		return CSemCampaigns::fetchSemCampaignByStateCode( $this->getStateCode(), $objAdminDatabase );
	}

}
?>