<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDepartments
 * Do not add any new functions to this class.
 */

class CDepartments extends CBaseDepartments {

	public static function fetchDepartmentByUserId( $intUserId, $objDatabase ) {
		$strSql = 'SELECT
						d.* FROM departments d
						LEFT JOIN employees e ON ( e.department_id = d.id )
						LEFT JOIN users u ON ( u.employee_id = e.id )
					WHERE
						u.id = ' . ( int ) $intUserId;

		return self::fetchDepartment( $strSql, $objDatabase );
	}

	public static function fetchDepartmentsByIds( $arrintDepartmentIds, $objDatabase ) {
		if( false == valArr( $arrintDepartmentIds ) ) return NULL;
		return self::fetchDepartments( 'SELECT * FROM departments WHERE id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' ) ORDER BY order_num', $objDatabase );
	}

	public static function fetchAllDepartmentsByDepartmentId( $intActionResultId, $intDepartmentId, $objDatabase ) {
		$strSql = 'SELECT d.name,
							e.name_first,
							e.name_last,
							e.preferred_name,
							e.email_address,
							ea.current_title as current_title,
							ds.name as designation_name,
							count(ts.id) as training_sessions_count,
							a.action_result_id as action_result_id,
							t.name as tag_name
						FROM training_trainer_associations as tta
							LEFT JOIN actions as a ON a.id = tta.action_id AND a.action_result_id = ' . ( int ) $intActionResultId . '
							LEFT JOIN employee_assessments as ea ON ( ea.id = tta.employee_assessment_id AND ea.deleted_by IS NULL )
							LEFT JOIN training_sessions ts ON ts.training_trainer_association_id = tta.id
							LEFT JOIN users as u ON a.secondary_reference = u.id
							LEFT JOIN employees as e ON u.employee_id = e.id
							LEFT JOIN departments as d ON e.department_id = d.id
							LEFT JOIN designations as ds ON e.designation_id = ds.id
							LEFT JOIN tags as t ON t.id = a.primary_reference
						WHERE ds.deleted_by IS NULL
							AND tta.is_published = 1
							AND d.id = ' . ( int ) $intDepartmentId . '
							AND ts.deleted_on IS NULL';

			if( CActionResult::EXTERNAL_TRAINER != $intActionResultId ) {
				$strSql .= ' AND e.employee_status_type_id= ' . CEmployeeStatusType::CURRENT;
			}

			$strSql .= ' GROUP BY d.name,
							e.name_first,
							e.name_last,
							e.preferred_name,
							e.email_address,
							ea.current_title,
							ds.name,
							a.action_result_id,
							t.name
						ORDER BY e.name_first,
							e.name_last';
		return self::fetchDepartments( $strSql, $objDatabase );
	}

	public static function fetchDepartmentByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						d.*
					FROM departments d
						LEFT JOIN employees e ON ( e.department_id = d.id )
					WHERE
						e.id = ' . ( int ) $intEmployeeId;

		return self::fetchDepartment( $strSql, $objDatabase );
	}

	public static function fetchAssignedDepartmentsByTestId( $intTestId, $objDatabase ) {

		$strSql = 'SELECT
						d.*
					FROM
						departments d,
						test_groups et
					WHERE
						d.id = et.department_id
						AND et.test_id = ' . ( int ) $intTestId . '
					ORDER BY
						lower ( d.name )';

		return self::fetchDepartments( $strSql, $objDatabase );
	}

	public static function fetchUnAssignedDepartmentsByTestId( $intTestId, $objDatabase ) {

		$strSql = 'SELECT
						 d.*
					FROM
						departments d
					WHERE
						d.id NOT IN (
										SELECT
											DISTINCT d.id
										FROM
											departments d
										JOIN
											test_groups tg ON ( d.id = tg.department_id )
										WHERE
											tg.test_id = ' . ( int ) $intTestId .
													' )
					ORDER BY
						d.name';

		return self::fetchDepartments( $strSql, $objDatabase );
	}

	public static function fetchPublishedDepartments( $objDatabase, $boolIsSearch = false, $strSearchDepartmentName = NULL ) {
		$strWhere = '';
		if( true == $boolIsSearch ) {
			return fetchData( 'SELECT id, name FROM departments WHERE is_published = 1 ORDER BY name ASC', $objDatabase );
		} else {
			if( false == is_null( $strSearchDepartmentName ) ) {
				$strWhere = 'AND name ILIKE \'%' . $strSearchDepartmentName . '%\'';
			}
			return self::fetchDepartments( 'SELECT * FROM departments WHERE is_published = 1 ' . $strWhere . ' ORDER BY name ASC', $objDatabase );
		}

	}

	public static function fetchDepartmentsByCountryCode( $strCountryCode = NULL, $objDatabase ) {

		$strWhereCondition = '';

		if( true == valStr( $strCountryCode ) ) {
			$strWhereCondition = 'WHERE ea.country_code = \'' . $strCountryCode . '\' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '';
		}

		$strSql = ' SELECT
						d.id,
						d.name
					FROM
						employees e
						JOIN employee_addresses ea ON ( e.id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						JOIN departments d ON ( d.id = e.department_id )' . $strWhereCondition . '
					GROUP BY
						d.name,
						d.id
					ORDER BY
						d.name';
		return self::fetchDepartments( $strSql, $objDatabase );
	}

	public static function fetchAllDepartmentsWithCountryCode( $objDatabase, $strCountryCodes = NULL, $boolIsSearch = false ) {

		$strWhereCondition	= '';
		$strSelect			= '';

		$strSelect = ( false == $boolIsSearch ) ? ' SELECT dept.id, dept.name, d.country_code ' : ' SELECT DISTINCT(dept.id), dept.name ';
		if( true == valStr( $strCountryCodes ) ) {
			$strWhereCondition = 'AND dept.is_published = 1 AND d.country_code IN ( \'' . $strCountryCodes . '\' )';
		}

		$strSql = $strSelect . '
					FROM
						departments dept
						LEFT JOIN designations d ON ( dept.id = d.department_id )
					WHERE
						d.department_id IS NOT NULL ' . $strWhereCondition . '
					GROUP BY
						dept.id,
						dept.name,
						d.country_code
					ORDER BY
						dept.name';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchDepartmentsByHrRepresentative( $intHrRepresentativeEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						distinct (d.*)
					FROM
						departments d
						LEFT JOIN teams t ON ( d.id = t.department_id )
					WHERE
						t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDepartmentNameByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) return false;

		$strSql = 'SELECT
						d.name
					FROM
						departments d
						JOIN employees e ON ( d.id = e.department_id AND e.id =' . ( int ) $intEmployeeId . ' )';

		$arrstrData = fetchData( $strSql, $objDatabase );
		return $arrstrData[0]['name'];
	}

	public static function fetchDepartmentsByDesignationCountryCode( $strCountryCode, $objDatabase ) {

		$strSql = ' SELECT
						d.id,
						d.name
					FROM
						designations dg
						JOIN departments d ON ( dg.department_id = d.id )
					WHERE
						dg.country_code = \'' . $strCountryCode . '\'
						AND dg.deleted_by IS NULL
						AND d.is_published = 1
					GROUP BY
						d.name,
						d.id
					ORDER BY
						d.name';

		return self::fetchDepartments( $strSql, $objDatabase );
	}

	public static function fetchDepartmentsByReimbursementRequestTypeId( $intRequestTypeId, $objDatabase ) {

		if( false == is_numeric( $intRequestTypeId ) ) return false;

		$strWhereCondition = ( CRequestType::INDIAN_REIMBURSEMENT == $intRequestTypeId ) ? 'id <> ' . CDepartment::EXECUTIVE :' id <>' . CDepartment::ADMINISTRATION;

		$strSql = 'SELECT
							*
					FROM
							departments d
					WHERE
							' . $strWhereCondition . ' AND d.is_published = 1
					ORDER BY
								name ASC';

		return self::fetchDepartments( $strSql, $objDatabase );

	}

	public static function fetchDepartmentsWithCompanyReportsCount( $objDatabase ) {

		$strSql = 'SELECT
						d.id,
						d.name,
						count( rda.id ) as reports
					FROM departments d
					LEFT JOIN report_department_associations rda ON ( d.id = rda.department_id )
					WHERE
						is_published = 1
					GROUP BY
						d.id,
						d.name
					ORDER BY name ASC';

		return fetchData( $strSql, $objDatabase );

	}

}
?>