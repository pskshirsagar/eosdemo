<?php

class CPsProductModule extends CBasePsProductModule {

	protected $m_arrobjPsProductModules;
	protected $m_intBuildModuleFile;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjPsProductModules = [];

		return;
	}

	/**
	 * Get Functions
	 */

	public function getFilePath() {
		return $this->m_strFilePath;
	}

	public function getBuildModuleFile() {
		return $this->m_arrobjPsProductModules;
	}

	public function getPsProductModules() {
		return $this->m_arrobjPsProductModules;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['file_path'] ) ) $this->setFilePath( $arrmixValues['file_path'] );
		if( true == isset( $arrmixValues['build_module_file'] ) ) $this->setBuildModuleFile( $arrmixValues['build_module_file'] );

		return;
	}

	public function setDefaults() {

		$this->setIsPublic( 1 );
		$this->setIsPublished( 1 );
	}

	public function setFilePath( $strFilePath ) {
		$this->m_strFilePath = $strFilePath;
	}

	public function setBuildModuleFile( $intBuildModuleFile ) {
		$this->m_intBuildModuleFile = $intBuildModuleFile;
	}

	public function setPsProductModules( $arrobjPsProductModules ) {
		$this->m_arrobjPsProductModules = $arrobjPsProductModules;
	}

	public function addPsProductModule( $objPsProductModule ) {
		$this->m_arrobjPsProductModules[$objPsProductModule->getId()] = $objPsProductModule;
	}

	/**
	 * Validation Functions
	 */

	public function valPsProductId() {
		$boolIsValid = true;
		if( true == is_null( $this->getPsProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is required.' ) );
		}
		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( false == isset( $objDatabase ) ) {
			trigger_error( 'You must provide a database object in CPsProductModule::valName().', E_USER_ERROR );
		}

		if( false == isset( $this->m_strName ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Module handle is required.' ) );
			return $boolIsValid;
		}

		$strWhereSql = ' WHERE ps_product_id = ' . $this->getPsProductId() . ' AND name = \'' . CStrings::strTrimDef( $this->m_strName ) . '\' AND file_path = \'' . trim( $this->m_strFilePath ) . '\' AND is_published = 1';
		$strWhereSql .= ( false == is_null( $this->m_intId ) ) ? ' AND id <> ' . $this->m_intId  : '';

		$intCount = CPsProductModules::fetchPsProductModuleCount( $strWhereSql, $objDatabase );

		if( 0 < $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Module handle already exists.' ) );
		}

		$boolHasLowerCaseLetters = false;
		$boolHasIllegalCharacters = true;

		for( $x = 0; $x < \Psi\CStringService::singleton()->strlen( $this->m_strName ); $x++ ) {
			if( ( ( 97 > ( int ) ord( $this->m_strName[$x] ) && ( 57 < ( int ) ord( $this->m_strName[$x] ) || 48 > ( int ) ord( $this->m_strName[$x] ) ) ) || 122 < ( int ) ord( $this->m_strName[$x] ) ) && ( 95 != ( int ) ord( $this->m_strName[$x] ) && 45 != ( int ) ord( $this->m_strName[$x] ) ) ) {
				$boolHasIllegalCharacters &= false;
			}

			if( ( 97 <= ord( $this->m_strName[$x] ) && 122 >= ord( $this->m_strName[$x] ) ) ) {
				$boolHasLowerCaseLetters = true;
			}
		}

		if( false == $boolHasIllegalCharacters ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Only lower case letters and underscores are permitted in module handles.' ) );
		}

		if( false == $boolHasLowerCaseLetters ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Module handle must contain at least one lower case letter.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}
		return $boolIsValid;
	}

	public function valQaUserId() {
		$boolIsValid = true;

		if( true == is_null( $this->getQaUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'qa_user_id', 'QA is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDevUserId() {
		$boolIsValid = true;
		if( true == is_null( $this->getDevUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'dev_user_id', 'Developer is required.' ) );
		}
		return $boolIsValid;
	}

	public function valModuleFileCreation() {
		$boolIsValid = true;

		$arrstrPathInfo = pathinfo( $this->m_strFilePath );

		if( true == isset( $this->m_intBuildModuleFile ) && true == file_exists( $this->m_strFilePath ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'build_module_file', 'A module file already exists in this location.  The system prohibits overwriting preexisting module files.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valModuleFileCreation();
				$boolIsValid &= $this->valDevUserId();
				$boolIsValid &= $this->valQaUserId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Create Functions
	 */

	public function createSubPsProductModule() {

		$objSubProductModule = new CPsProductModule();
		$objSubProductModule->setDefaults();
		$objSubProductModule->setParentModuleId( $this->getId() );
		$objSubProductModule->setPsProductId( $this->getPsProductId() );

		return $objSubProductModule;
	}

}
?>