<?php

class CTransmissionVendorPropertyAudit extends CBaseTransmissionVendorPropertyAudit {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransmissionVendorId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCategoryIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAlternatePhoneNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFaxNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTollfreeNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valYearEstablished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBusinessHours() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialOffer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLogo() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFacebookPageUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTwitterHandle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPhoto() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWebsiteUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVideoUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSpecialOfferUrl() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddress1() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAddress2() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valState() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLatitude() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLongitude() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>