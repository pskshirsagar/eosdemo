<?php

class CInvoicePeriod extends CBaseInvoicePeriod {

	const FUTURE_1_MONTH 		= 1;
	const TRAILING_1_MONTH 		= 2;
	const FUTURE_1_QUARTER 		= 3;
	const FUTURE_6_MONTHS 		= 4;
	const FUTURE_1_YEAR 		= 5;

}
?>