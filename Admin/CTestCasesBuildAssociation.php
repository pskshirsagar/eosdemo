<?php

class CTestCasesBuildAssociation extends CBaseTestCasesBuildAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTestBuildId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTaskStatusId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExecutorEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExecutedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>