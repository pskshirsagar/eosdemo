<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskApprovalTypes
 * Do not add any new functions to this class.
 */

class CTaskApprovalTypes extends CBaseTaskApprovalTypes {

	public static function fetchTaskApprovalTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, CTaskApprovalType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchTaskApprovalType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, CTaskApprovalType::class, $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

}
?>