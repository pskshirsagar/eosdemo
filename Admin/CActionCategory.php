<?php

class CActionCategory extends CBaseActionCategory {

	const ID_GENERAL			= 1;
	const ID_ACCOUNTING			= 2;
	const ID_SUPPORT			= 3;
	const ID_SALES				= 4;
	const ID_RENEWAL			= 5;
	const ID_MERCHANT			= 6;
	const ID_COMPLAINT			= 7;
	const ID_IMPLEMENTATION		= 8;

	public static $c_arrintLeadActivityActionCategoryIds = [
		self::ID_GENERAL,
		self::ID_ACCOUNTING,
		self::ID_SUPPORT,
		self::ID_SALES,
		self::ID_RENEWAL,
		self::ID_MERCHANT,
		self::ID_COMPLAINT,
		self::ID_IMPLEMENTATION
	];

	public static $c_arrintDeptWiseLeadActivityActionCategoryIds = [
		CDepartment::SUCCESS_MANAGEMENT 	=> self::ID_GENERAL,
		CDepartment::TRAINING 				=> self::ID_IMPLEMENTATION,
		CDepartment::CONSULTING				=> self::ID_IMPLEMENTATION,
		CDepartment::PROJECT_MANAGEMENT		=> self::ID_IMPLEMENTATION,
		CDepartment::TECHNICAL_SUPPORT		=> self::ID_SUPPORT,
		CDepartment::SALES			=> self::ID_SALES,
		CDepartment::SALES_ENGINEER			=> self::ID_SALES,
		CDepartment::ACCOUNTING				=> self::ID_ACCOUNTING
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
