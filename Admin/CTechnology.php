<?php

class CTechnology extends CBaseTechnology {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDepartmentId() {
		$boolIsValid = true;

		if( false == valId( $this->getDepartmentId() ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'department_id', 'Department is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Technology is required.' ) );
		}

		if( true == $boolIsValid ) {
			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . ( int ) $this->getId() : '' );
			$strSql = " WHERE name ILIKE '" . trim( addslashes( $this->getName() ) ) . "'" . $strSqlCondition;

			$intCount = \Psi\Eos\Admin\CTechnologies::createService()->fetchTechnologyCount( $strSql, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Technology "' . trim( addslashes( $this->getName() ) ) . '" already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valDepartmentId();
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>