<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReportGroupAssociations
 * Do not add any new functions to this class.
 */

class CReportGroupAssociations extends CBaseReportGroupAssociations {

	public static function fetchReportGroupAssociationByReportIdByGroupIds( $intReportId, $arrintGroupIds, $objAdminDatabase ) {
		if( false == is_numeric( $intReportId ) || false == valArr( $arrintGroupIds ) ) {
			return false;
		}

		$strSql = '	SELECT
						*
					FROM
						report_group_associations rga
					WHERE
						rga.company_report_id =' . ( int ) $intReportId . '
						AND rga.group_id IN ( ' . implode( ',', $arrintGroupIds ) . ' )';

		return parent::fetchReportGroupAssociations( $strSql, $objAdminDatabase );
	}

}
?>