<?php

class CNewTeam extends CBaseNewTeam {

	const US_HR 						= 76;
	const INDIA_RECRUITMENT				= 92;
	const BLAKE_WEBSTER					= 153;
	const ID_TRAINING					= 228;

	const SUPPORT_DOMAIN_MARKETING 		= 308;
	const SUPPORT_DOMAIN_RESIDENT 		= 309;
	const SUPPORT_DOMAIN_LEASE 			= 310;
	const SUPPORT_DOMAIN_ENTRATA_CORE 	= 311;

	const JAMES_DRAPER					= 320;
	const NEW_JOINEE_INDIA				= 337;
	const ADMIN_2 						= 357;
	const GILBERT_THOMAS				= 616;
	const GILBERT_THOMAS_2				= 744;
	const RESIDENT_UTILITY				= 672;
	const HR_AND_GENERAL_DEV			= 455;
	const SALES_AND_MARKETING_QA		= 676;
	const OUTSIDE_SALES_TEAM_723		= 723;
	const OUTSIDE_SALES_TEAM_724	    = 724;
	const OUTSIDE_SALES_TEAM_725		= 725;
	const OUTSIDE_SALES_TEAM_726		= 726;
	const OUTSIDE_SALES_TEAM_727		= 727;
	const OUTSIDE_SALES_TEAM_747		= 747;

	const SYSTEMS_DEV_VI					= 14;
	const CLIENT_ADMIN_HR_GENERAL_DEV_I		= 455;
	const CLIENT_ADMIN_QA_III				= 742;
	const CLIENT_ADMIN_HR_GENERAL_DEV_III	= 653;
	const CLIENT_ADMIN_HR_GENERAL_DEV_IV	= 659;
	const CLIENT_ADMIN_QA_II				= 859;

	const SITE_TABLET_ANDROID			= 531;
	const SITE_TABLET_IOS				= 63;
	const SITE_TABLET_DEV_I				= 772;

	const LEASING_CENTER_CALL_TRACKING_ANALYSIS_FREESWITCH		= 113;

	const TEAM_NEW_JOINEE_INDIA			= 'New Joinee India';

	public static $c_arrintXmppSyncTeam		= array(
		self::SUPPORT_DOMAIN_ENTRATA_CORE,
		self::SUPPORT_DOMAIN_LEASE,
		self::SUPPORT_DOMAIN_MARKETING,
		self::SUPPORT_DOMAIN_RESIDENT

	);

	protected $m_boolIsPrimaryTeam;

	protected $m_intCountTeam;
	protected $m_intCountTeamMember;

	protected $m_strNameLast;
	protected $m_strNameFirst;
	protected $m_strManagerName;
	protected $m_strProductName;
	protected $m_strPreferredName;
	protected $m_strDepartmentName;
	protected $m_strProjectManagerName;
	protected $m_strHrRepresentativeName;

	protected $m_arrintTeamMemberIds;

	/**
	 * Create Functions
	 */

	public function createTeamEmployee() {

		$this->m_objTeamEmployee = new CTeamEmployee();
		$this->m_objTeamEmployee->setTeamId( $this->getId() );
		$this->m_objTeamEmployee->setManagerEmployeeId( $this->getManagerEmployeeId() );

		return  $this->m_objTeamEmployee;
	}

	/**
	 * Get Functions
	 */

	public function getIsPrimaryTeam() {
		return $this->m_boolIsPrimaryTeam;
	}

	public function getCountTeam() {
		return $this->m_intCountTeam;
	}

	public function getCountTeamMember() {
		return $this->m_intCountTeamMember;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getManagerName() {
		return $this->m_strManagerName;
	}

	public function getProductName() {
		return $this->m_strProductName;
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getProjectManagerName() {
		return $this->m_strProjectManagerName;
	}

	public function getHrRepresentativeName() {
		return $this->m_strHrRepresentativeName;
	}

	public function getTeamMemberIds() {
		return $this->m_arrintTeamMemberIds;
	}

	public function getTeamDepartmentName() {
		return $this->m_strTeamDepartmentName;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['name_first'] ) ) 				$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) 				$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['preferred_name'] ) ) 			$this->setPreferredName( $arrmixValues['preferred_name'] );
		if( true == isset( $arrmixValues['is_primary_team'] ) ) 		$this->setIsPrimaryTeam( $arrmixValues['is_primary_team'] );
		if( true == isset( $arrmixValues['my_team_members'] ) ) 		$this->setCountTeamMember( $arrmixValues['my_team_members'] );
		if( true == isset( $arrmixValues['my_teams'] ) ) 				$this->setCountTeam( $arrmixValues['my_teams'] );
		if( true == isset( $arrmixValues['my_team_member_ids'] ) ) 		$this->setTeamMemberIds( $arrmixValues['my_team_member_ids'] );
		if( true == isset( $arrmixValues['department_name'] ) ) 		$this->setDepartmentName( $arrmixValues['department_name'] );
		if( true == isset( $arrmixValues['manager_name'] ) )			$this->setManagerName( $arrmixValues['manager_name'] );
		if( true == isset( $arrmixValues['project_manager_name'] ) )	$this->setProjectManagerName( $arrmixValues['project_manager_name'] );
		if( true == isset( $arrmixValues['hr_representative_name'] ) )	$this->setHrRepresentativeName( $arrmixValues['hr_representative_name'] );
		if( true == isset( $arrmixValues['department_name'] ) )			$this->setTeamDepartmentName( $arrmixValues['department_name'] );
		if( true == isset( $arrmixValues['product_name'] ) )			$this->setProductName( $arrmixValues['product_name'] );

		return;
	}

	public function setIsPrimaryTeam( $boolIsPrimaryTeam ) {
		$this->m_boolIsPrimaryTeam = $boolIsPrimaryTeam;
	}

	public function setCountTeam( $intCountTeam ) {
		$this->m_intCountTeam = $intCountTeam;
	}

	public function setCountTeamMember( $intCountTeamMember ) {
		$this->m_intCountTeamMember = $intCountTeamMember;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setManagerName( $strManagerName ) {
		$this->m_strManagerName = $strManagerName;
	}

	public function setProductName( $strProductName ) {
		$this->m_strProductName = $strProductName;
	}

	public function setPreferredName( $strPreferredName ) {
		$this->m_strPreferredName = CStrings::strTrimDef( $strPreferredName, 100, NULL, true );
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	public function setProjectManagerName( $strProjectManagerName ) {
		$this->m_strProjectManagerName = $strProjectManagerName;
	}

	public function setHrRepresentativeName( $strHrRepresentativeName ) {
		$this->m_strHrRepresentativeName = $strHrRepresentativeName;
	}

	public function setTeamMemberIds( $arrintTeamMemberIds ) {
		$this->m_arrintTeamMemberIds = $arrintTeamMemberIds;
	}

	public function setTeamDepartmentName( $strTeamDepartmentName ) {
		$this->m_strTeamDepartmentName = $strTeamDepartmentName;
	}

	/**
	 * Validation Functions
	 */

	public function validateTeamEmployee( $intTeamId, $intEmployeeId, $objDatabase ) {

		$boolIsPresent = true;
		$objTeamEmployee = CNewTeamEmployees::fetchTeamEmployeesByTeamIdByEmployeeId( $intTeamId, $intEmployeeId, $objDatabase );

		if( true == valObj( $objTeamEmployee, 'CTeamEmployee' ) ) {

			$boolIsPresent = false;
		}
		return $boolIsPresent;
	}

	public function valManagerEmployeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getManagerEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'manager_employee_id', 'Manager is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valProjectEmployeeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSdmEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'project_employee_id', 'Software development manager is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valHrRepresentativeEmployeeId() {

		$boolIsValid = true;

		if( true == is_null( $this->getHrRepresentativeEmployeeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'hr_representative_employee_id', 'HR Representative is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valName( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Team name is required. ' ) );
		}

		if( true == $boolIsValid ) {
			$intConflictingTeamCount = CNewTeams::fetchConflictingTeamCountByName( $this->getName(), $this->getId(), $this->getManagerEmployeeId(), $objDatabase );

			if( 0 < $intConflictingTeamCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Team name already exists. ' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDepartmentId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDepartmentId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'department_id', 'Department is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valTeamTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTeamTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'team_type_id', 'Team type is required. ' ) );
		}

		return $boolIsValid;
	}

	public function valDeleteTeam( $objDatabase ) {
		$boolIsValid = true;

		$arrintTeamEmployeesCount = CNewTeamEmployees::fetchCurrentTeamEmployeesCountByTeamIds( array( $this->getId() => $this->getId() ), $objDatabase, $boolIsPrimaryTeam = true );
		if( true == valArr( $arrintTeamEmployeesCount ) && 0 < $arrintTeamEmployeesCount[0]['member_count'] ) {
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase, $strCountryCode = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valManagerEmployeeId();

				if( CCountry::CODE_INDIA == $strCountryCode ) {
					$boolIsValid &= $this->valProjectEmployeeId();
					$boolIsValid &= $this->valHrRepresentativeEmployeeId();
				}

				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valTeamTypeId();
				$boolIsValid &= $this->valDepartmentId();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valDeleteTeam( $objDatabase );
				break;

			default:
				// no validation avaliable for delete action
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Insert/Update Functions
	 *
	 */

	public function insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setName( addslashes( $this->getName() ) );
		$this->setDescription( addslashes( $this->getDescription() ) );

		return parent::insertOrUpdate( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>