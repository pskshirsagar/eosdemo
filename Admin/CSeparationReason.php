<?php

class CSeparationReason extends CBaseSeparationReason {
    const SEPARATION_REASON_PROFESSIONAL_GROWTH_ID	= 63;
    const SEPARATION_REASON_COMPENSATION_ID	        = 36;
    const SEPARATION_REASON_MOVING_OUT_OF_STATE_ID	= 89;
    const SEPARATION_REASON_COACHED_OUT_ID	        = 29;
    const SEPARATION_REASON_PERSONAL_REASON_ID	    = 61;
    const SEPARATION_REASON_DISSATISFIED_ID			= 22;
    const SEPARATION_REASON_SCHOOL_ID				= 27;
    const SEPARATION_REASON_OTHER_ID				= 25;
    const SEPARATION_REASON_NO_CALL_ID				= 90;
    const SEPARATION_REASON_LOA_FMLA_ID			    = 91;
    const SEPARATION_REASON_ADMIN_ERROR_ID			= 92;
    const SEPARATION_REASON_INVOLUNTARY_OTHER_ID	= 101;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>