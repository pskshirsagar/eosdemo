<?php

class CTrainingUserQuizResponse extends CBaseTrainingUserQuizResponse {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTrainingUserQuizId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpQuizQuestionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHelpQuizAnswerId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valAnswer() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCorrect() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>