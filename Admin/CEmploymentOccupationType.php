<?php

class CEmploymentOccupationType extends CBaseEmploymentOccupationType {

	const STUDENT		= 1;
	const PROFESSIONAL	= 2;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>