<?php

class CSurveyTemplateQuestion extends CBaseSurveyTemplateQuestion {

	protected $m_intSurveyId;

	protected $m_arrobjSurveyTemplateOptions;
	protected $m_arrobjSurveyTemplateQuestions;
	protected $m_arrobjChildSurveyTemplateOptions;
	protected $m_arrobjChildSurveyTemplateQuestions;

	const QUESTION_SUPPORT_NPS_SCORE		= 6;
	const QUESTION_MANAGER_NO_DUES 			= 1815;
	const QUESTION_FINANCE_NO_DUES 			= 1801;
	const QUESTION_ADMIN_NO_DUES 			= 1808;
	const QUESTION_HR_NO_DUES 				= 1796;
	const QUESTION_SECURITY_NO_DUES 		= 1782;
	const QUESTION_LIVE_IT_NO_DUES 			= 1792;
	const QUESTION_SYSTEM_ADMIN_NO_DUES		= 1779;
	const QUESTION_TRAINING_NO_DUES 		= 1768;
	const QUESTION_SELF_REVIEW				= 65;
	const QUESTION_REJECT_REASON			= 3213;
	const QUESTION_RE_CONSIDER				= 3212;
	const QUESTION_IT_HELPDESK				= 3870;
	const FHA 								= 1921;
	const REPRESENTING_THE_CLIENT 			= 1922;
	const GATHER_INFORMATION 				= 1923;
	const BEING_ABUSIVE						= 1924;
	const MAINTENANCE_EMERGENCY_PROTOCOL	= 2767;
	const SECURE_INFORMATION				= 2913;
	const SOFTWARE_NAVIGATION				= 1912;
	const QOI_OVERCOMING_OBJECTIONS			= 1916;
	const APPOINTMENT_SETTINGS 				= 1913;
	const LANUGAGE_AND_TONE					= 1919;
	const URGENCY							= 1914;
	const BUILDING_VALUE					= 1915;
	const EMPATHY							= 1917;
	const RAPPORT							= 1918;
	const ACTIVE_LISTENING					= 1910;
	const WORK_ORDER_OR_RESIDENT_PROTOCOL	= 1898;
	const EMERGENCY_MAINTENANCE_PROTOCOL	= 1899;
	const CONTACT_INFORMATION 				= 1901;
	const CALL_SPECIFICS					= 1902;
	const PROBING_QUESTIONS					= 1903;
	const NOTES_AND_CALL_SUBMISSION			= 1904;
	const ACCURATE_INFO_AND_ASSUMPTIONS		= 1905;
	const PHONE_ETIQUETTE					= 1906;
	const PROPERTY_SPECIFIC_PROTOCOL		= 1907;
	const LEAD_PROTOCOL						= 1908;
	const SURVEY_RATING_QUESTION_ID         = 65;
	const MANAGER_SURVEY_RATING_QUESTION_ID = 89;
	const MANAGER_SURVEY_APPLAUSE_QUESTION_ID = 4560;

	const QUARTERLY_SURVEY_ADDITIONAL_FEEDBACK_QUESTION_ID = 2029;
	const SURVEY_QUESTION_WHAT_WENT_WELL_QUESTION_ID    = 66;
    const SURVEY_QUESTION_WHAT_WENT_WRONG_QUESTION_ID	= 67;
	const MANAGER_SURVEY_WHAT_WENT_WELL_QUESTION_ID     = 90;
	const MANAGER_SURVEY_WHAT_WENT_WRONG_QUESTION_ID    = 91;

	public static $c_arrintSurveyTemplateWentWellWrongQuestionIds = array(
		self::SURVEY_QUESTION_WHAT_WENT_WELL_QUESTION_ID,
		self::SURVEY_QUESTION_WHAT_WENT_WRONG_QUESTION_ID,
		self::MANAGER_SURVEY_WHAT_WENT_WELL_QUESTION_ID,
		self::MANAGER_SURVEY_WHAT_WENT_WRONG_QUESTION_ID
	);

	public static $c_arrintSurveyTemplateQuestion = array(
	    self::QUESTION_ADMIN_NO_DUES,
	    self::QUESTION_HR_NO_DUES,
	    self::QUESTION_SECURITY_NO_DUES,
	    self::QUESTION_SYSTEM_ADMIN_NO_DUES,
	    self::QUESTION_LIVE_IT_NO_DUES,
	    self::QUESTION_FINANCE_NO_DUES,
	    self::QUESTION_MANAGER_NO_DUES,
	    self::QUESTION_TRAINING_NO_DUES,
	);

	public static $c_arrintDisqualifiersSurveyTemplateQuestionLabel = [
		self::FHA								=> 'FHA',
		self::REPRESENTING_THE_CLIENT			=> 'Representing the Client',
		self::GATHER_INFORMATION				=> 'Gather Information',
		self::BEING_ABUSIVE						=> 'Being Abusive',
		self::MAINTENANCE_EMERGENCY_PROTOCOL	=> 'Maintenance Emergency Protocol',
		self::SECURE_INFORMATION				=> 'Secure Information'
	];

	public static $c_arrintQoISurveyTemplateQuestionLabel = [
		self::SOFTWARE_NAVIGATION			=> 'Software Navigation',
		self::QOI_OVERCOMING_OBJECTIONS		=> 'Overcoming Objections',
		self::APPOINTMENT_SETTINGS			=> 'Appointment Settings',
		self::LANUGAGE_AND_TONE				=> 'Language And Tone',
		self::URGENCY						=> 'Urgency',
		self::BUILDING_VALUE				=> 'Building Value',
		self::EMPATHY						=> 'Empathy',
		self::RAPPORT						=> 'Rapport',
		self::ACTIVE_LISTENING				=> 'Active Listening'
	];


	public static $c_arrintBasicExpectationsSurveyTemplateQuestionLabel = [
		self::CONTACT_INFORMATION				=> 'Contact Information',
		self::CALL_SPECIFICS					=> 'Call Specifics',
		self::PROBING_QUESTIONS					=> 'Probing Questions',
		self::NOTES_AND_CALL_SUBMISSION			=> 'Notes And Call Submission',
		self::ACCURATE_INFO_AND_ASSUMPTIONS		=> 'Accurate Info And Assumptions',
		self::PHONE_ETIQUETTE					=> 'Phone Etiquette',
		self::PROPERTY_SPECIFIC_PROTOCOL		=> 'Property Protocols',
		self::LEAD_PROTOCOL						=> 'Lead Protocols',
		self::WORK_ORDER_OR_RESIDENT_PROTOCOL	=> 'Resident Protocols',
		self::EMERGENCY_MAINTENANCE_PROTOCOL	=> 'Emergency Maintenance Protocols',
	];

	public function valSurveyQuestionTypeId() {
		$boolIsValid = true;

		if( false == valId( $this->m_intSurveyQuestionTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_question_type_id', 'Question type is required.' ) );
		}

		return $boolIsValid;
	}

	public function valQuestion() {
		$boolIsValid = true;

		if( false == valStr( $this->m_strQuestion ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'question', 'Question is required.' ) );
		}

		return $boolIsValid;
	}

	public function valWidgetAction() {
		$boolIsValid = true;

		if( CSurveyQuestionType::WIDGET == $this->m_intSurveyQuestionTypeId ) {
			if( false == valStr( $this->getWidgetAction() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'widget_action', 'Module is required.' ) );
			} elseif( false == file_exists( PATH_INTERFACES_COMMON . 'surveys/widgets/' . $this->getWidgetAction() . '.tpl' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'widget_action', 'Module name must be valid. Template file is not available for given module.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSurveyQuestionTypeId();
				$boolIsValid &= $this->valQuestion();
				$boolIsValid &= $this->valWidgetAction();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;

	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['survey_id'] ) ) $this->setSurveyId( $arrmixValues['survey_id'] );
	}

	/**
	 * Set Functions
	 *
	 */

	public function setSurveyId( $intSurveyId ) {
		$this->m_intSurveyId = $intSurveyId;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getSurveyId() {
		return $this->m_intSurveyId;
	}

	public function getSurveyTemplateOptions() {
		return $this->m_arrobjSurveyTemplateOptions;
	}

	public function getChildSurveyTemplateQuestions() {
		return $this->m_arrobjChildSurveyTemplateQuestions;
	}

	public function getChildSurveyTemplateOptions() {
		return $this->m_arrobjChildSurveyTemplateOptions;
	}

	public function getSurveyTemplateQuestions() {
		return $this->m_arrobjSurveyTemplateQuestions;
	}

	public function addSurveyTemplateOption( $objSurveyTemplateOption ) {
		if( NULL == $objSurveyTemplateOption->getId() ) {
			$this->m_arrobjSurveyTemplateOptions[] = $objSurveyTemplateOption;
		} else {
			$this->m_arrobjSurveyTemplateOptions[$objSurveyTemplateOption->getId()] = $objSurveyTemplateOption;
		}
	}

	public function addChildSurveyTemplateQuestions( $arrobjChildSurveyTemplateQuestions ) {
		$this->m_arrobjChildSurveyTemplateQuestions = $arrobjChildSurveyTemplateQuestions;
	}

	public function addChildSurveyTemplateOptions( $arrobjChildSurveyTemplateOptions ) {
		$this->m_arrobjChildSurveyTemplateOptions = $arrobjChildSurveyTemplateOptions;
	}

	public function addSurveyTemplateQuestion( $objSurveyTemplateQuestion ) {
		$this->m_arrobjSurveyTemplateQuestions[$objSurveyTemplateQuestion->getId()] = $objSurveyTemplateQuestion;
	}

	 /**
	 * Other Functions
	 *
	 */

	public function delete( $intUserId, $objDatabase, $boolIsSoftDelete = false ) {

		if( true == $boolIsSoftDelete ) {
			$this->setDeletedBy( $intUserId );
			$this->setDeletedOn( 'NOW()' );
		} else {
			return parent::delete( $intUserId, $objDatabase );
		}

		return $this->update( $intUserId, $objDatabase );
	}

	public function createSurveyTemplateOption() {

		$objSurveyTemplateOption = new CSurveyTemplateOption();

		$objSurveyTemplateOption->setSurveyTemplateQuestionId( $this->m_intId );
		$objSurveyTemplateOption->setSurveyTemplateId( $this->m_intSurveyTemplateId );

		return $objSurveyTemplateOption;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchSurveyTemplateOptions( $objDatabase ) {
		return CSurveyTemplateOptions::fetchSurveyTemplateOptionsBySurveyTemplateQuestionId( $this->getId(), $objDatabase );
	}

	public function fetchChildSurveyTemplateQuestions( $objDatabase ) {
		return CSurveyTemplateQuestions::fetchSurveyTemplateQuestionsBySurveyTemplateQuestionId( $this->getId(), $objDatabase );
	}

}
?>