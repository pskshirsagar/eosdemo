<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTeamTypes
 * Do not add any new functions to this class.
 */

class CTeamTypes extends CBaseTeamTypes {

	public static function fetchTeamTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CTeamType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchTeamType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CTeamType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllTeamTypes( $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						team_types
					ORDER BY name';

		return self::fetchTeamTypes( $strSql, $objDatabase );
	}
}
?>