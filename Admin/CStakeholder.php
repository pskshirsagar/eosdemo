<?php

class CStakeholder extends CBaseStakeholder {

	protected $m_strEmployeeNameFull;
	protected $m_strEmployeeEmailAddress;
	protected $m_strEmployeeDepartmentName;
	protected $m_intEmployeeId;

	/**
	 * Get Functions
	 */

	public function getEmployeeNameFull() {
		return $this->m_strEmployeeNameFull;
	}

	public function getEmployeeEmailAddress() {
		return $this->m_strEmployeeEmailAddress;
	}

	public function getEmployeeDepartmentName() {
		return $this->m_strEmployeeDepartmentName;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	/**
	 * Set Functions
	 */

	public function setEmployeeNameFull( $strNameFull ) {
		$this->m_strEmployeeNameFull = $strNameFull;
	}

	public function setEmployeeEmailAddress( $strEmailAddress ) {
		$this->m_strEmployeeEmailAddress = $strEmailAddress;
	}

	public function setEmployeeDepartmentName( $strEmployeeDepartmentName ) {
		$this->m_strEmployeeDepartmentName = $strEmployeeDepartmentName;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['employee_name_full'] ) ) 			$this->setEmployeeNameFull( $arrmixValues['employee_name_full'] );
		if( true == isset( $arrmixValues['employee_email_address'] ) )		$this->setEmployeeEmailAddress( $arrmixValues['employee_email_address'] );
		if( true == isset( $arrmixValues['employee_department_name'] ) )	$this->setEmployeeDepartmentName( $arrmixValues['employee_department_name'] );
		if( true == isset( $arrmixValues['employee_id'] ) )					$this->setEmployeeId( $arrmixValues['employee_id'] );

		return;
	}

	/**
	 * Validate Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>