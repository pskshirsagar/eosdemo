<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CJobPostingCategories
 * Do not add any new functions to this class.
 */

class CJobPostingCategories extends CBaseJobPostingCategories {

	const ANYTHING_TECHNIE 	= 1;
	const SUPPORT 			= 2;
	const ADMINISTRATIVE 	= 3;

	public static function fetchJobPostingCategoriesByCountryCode( $strCountryCode, $objDatabase ) {
		if( false == valStr( $strCountryCode ) ) return NULL;
		$strSql = ' SELECT
						*
					FROM
						job_posting_categories
					WHERE
						country_code = \'' . $strCountryCode . '\'
					ORDER BY id';

		return parent::fetchJobPostingCategories( $strSql, $objDatabase );
	}
}
?>