<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestAnswerOptions
 * Do not add any new functions to this class.
 */

class CTestAnswerOptions extends CBaseTestAnswerOptions {

	public static function fetchQuestionOptionsByTestId( $intTestId, $objDatabase ) {
		return self::fetchTestAnswerOptions( 'SELECT * FROM test_answer_options WHERE test_id = ' . ( int ) $intTestId, $objDatabase );
	}

	public static function fetchTestAnswerOptionsByTestQuestionId( $intTestQuestionId, $objDatabase ) {
		return self::fetchTestAnswerOptions( 'SELECT * FROM test_answer_options WHERE test_question_id = ' . ( int ) $intTestQuestionId . ' ORDER BY id', $objDatabase );
	}

	public static function fetchTestAnswerOptionsByTestQuestionIdByTestId( $intTestQuestionId, $intTestId, $objDatabase ) {
		return self::fetchTestAnswerOptions( 'SELECT * FROM test_answer_options WHERE test_question_id = ' . ( int ) $intTestQuestionId . ' AND test_id = ' . ( int ) $intTestId, $objDatabase );
	}

	public static function fetchTestAnswerOptionsByTestQuestionIdByIsCorrect( $intTestQuestionId, $objDatabase ) {
		return self::fetchTestAnswerOption( 'SELECT * FROM test_answer_options WHERE test_question_id = ' . ( int ) $intTestQuestionId . ' AND is_correct = 1', $objDatabase );
	}

	public static function fetchTestAnswerOptionsByTestQuestionsIds( $arrintQuestionIds, $objDatabase ) {
		return self::fetchTestAnswerOptions( 'SELECT * FROM test_answer_options WHERE test_question_id IN(' . implode( ',', $arrintQuestionIds ) . ' ) ORDER BY id', $objDatabase );
	}
}
?>