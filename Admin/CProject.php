<?php

class CProject extends CBaseProject {

	protected $m_objProject;
	protected $m_intDaysInCurrentStatus;

	public function setProjectDetail( $objProject ) {
		$this->m_objProject = $objProject;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		if( false == valId( $this->m_objProject->getPsProductId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is required.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valPsProductOptionId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTeamBacklogId() {
		$boolIsValid = true;
		if( false == valId( $this->m_intTeamBacklogId ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Invalid Product Backlog Id.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;

		if( true == is_null( $this->m_objProject->getTitle() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Please enter project title.' ) );
			$boolIsValid = false;
		}

		if( false == valStr( $this->m_objProject->getTitle() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', 'Invalid project title.' ) );
			$boolIsValid = false;
		}
		return $boolIsValid;
	}

	public function valProjectStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objProject ) {
		$this->setProjectDetail( $objProject );
		$boolIsValid = true;
		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valTitle();
				$boolIsValid &= $this->valTeamBacklogId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function setDaysInCurrentStatus( $intDaysInCurrentStatus ) {
		$this->set( 'm_intDaysInCurrentStatus', CStrings::strToIntDef( $intDaysInCurrentStatus, NULL, false ) );
	}

	public function getDaysInCurrentStatus() {
		return $this->m_intDaysInCurrentStatus;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == array_key_exists( 'days_in_current_status', $arrmixValues ) ) {
			$this->setDaysInCurrentStatus( $arrmixValues['days_in_current_status'] );
		}
	}

}
?>