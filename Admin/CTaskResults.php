<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskResults
 * Do not add any new functions to this class.
 */

class CTaskResults extends CBaseTaskResults {

	public static function fetchTaskResults( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CTaskResult', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchTaskResult( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CTaskResult', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

}
?>