<?php

class CContractTerminationRequestStatus extends CBaseContractTerminationRequestStatus {

	const INCOMPLETE			= 1;
	const CLIENT_APPROVED		= 2;
	const MANAGEMENT_APPROVED	= 3;
	const IN_PROGRESS			= 4;
	const COMPLETE				= 5;
	const CANCELLED				= 6;
	const DENIED				= 7;
	const REVERTED				= 8;
	const PENDING				= 9;

	public static $c_arrintAllowEditTerminationRequest = [
		self::CLIENT_APPROVED, self::PENDING, self::MANAGEMENT_APPROVED, self::IN_PROGRESS, self::COMPLETE
	];

	public static $c_arrintDeniedCancelledTerminationRequest = [
		self::DENIED, self::CANCELLED, self::REVERTED
	];

	public static $c_arrintCompletedTerminationRequestStatusIds = [
		self::MANAGEMENT_APPROVED, self::COMPLETE
	];

	public static function getConstants() {
		$objClass = new ReflectionClass( __CLASS__ );
		return $objClass->getConstants();
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>