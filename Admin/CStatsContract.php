<?php

class CStatsContract extends CBaseStatsContract {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsLeadId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRenewalContractPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOriginalContractPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCommissionBucketId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractTerminationRequestId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCurrencyCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProductName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractProductUnitCount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractProductSubscriptionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractProductTransactionAcv() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientLastActiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientProductStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientProductLastActiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientProductIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientPropertyStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientPropertyLastActiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientPropertyIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientProductPropertyStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientProductPropertyLastActiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientProductPropertyIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractPropertyStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractPropertyLastActiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractPropertyIsActive() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractPropertyImplementationEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractPropertyImplementationDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractPropertyIsImplemented() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCloseDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractEnteredPipelineOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valProposalSentOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractWonOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractLostOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractTerminatedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeactivationDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationEmployeeName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationWeightScore() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationDelayTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationDelayType() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBillingStartedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonthlyRecurringAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMonthlyChangeAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionalAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionalChangeAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valImplementationAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valContractTerminationReason() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFirstClientProductPropertyRecord() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsLastClientProductPropertyRecord() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsFirstContractPropertyRecord() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsLastContractPropertyRecord() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientPropertyEntrataCoreStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valClientPropertyEntrataCoreLastActiveDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsTransactionalProduct() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPilot() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsContractProperty() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>