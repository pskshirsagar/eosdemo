<?php

class CWorkbookCategoryEntity extends CBaseWorkbookCategoryEntity {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWorkbookCategoryId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWorkbookEntityId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>