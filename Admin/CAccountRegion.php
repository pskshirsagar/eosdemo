<?php

class CAccountRegion extends CBaseAccountRegion {

	protected $m_objClient;
	protected $m_arrobjInvoices;

	public function __construct() {
		parent::__construct();

		$this->m_arrobjInvoices 	= [];
		$this->m_objClient 	= NULL;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setInvoices( $arrobjInvoices ) {
		$this->m_arrobjInvoices = $arrobjInvoices;
	}

	public function setClient( $objClient ) {
		$this->m_objClient = $objClient;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getInvoices() {
		return $this->m_arrobjInvoices;
	}

	public function getClient() {
		return $this->m_objClient;
	}

	/**
	 * Add Functions
	 *
	 */

	public function addInvoice( $objInvoice ) {
		$this->m_arrobjInvoices[$objInvoice->getId()] = $objInvoice;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required' ) );
		}

		return $boolIsValid;
	}

	public function valRegionName( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getRegionName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'region_name', 'Region name is required.' ) );
		}
		$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
		$intCount = \Psi\Eos\Admin\CAccountRegions::createService()->fetchAccountRegionCount( ' WHERE  lower( region_name ) = \'' . trim( \Psi\CStringService::singleton()->strtolower( addslashes( \Psi\CStringService::singleton()->strtolower( $this->getRegionName() ) ) ) ) . '\' AND cid = ' . ( int ) $this->getCid() . '' . $strSqlCondition, $objDatabase );

		if( 0 < $intCount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'region_name', 'Region name is already in use.' ) );
		}

		return $boolIsValid;
	}

	public function valNameFirst() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameFirst() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_first', 'First name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valNameLast() {
		$boolIsValid = true;

		if( true == is_null( $this->getNameLast() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name is required.' ) );

		} elseif( true == \Psi\CStringService::singleton()->stristr( $this->getNameLast(), '@' ) || true == \Psi\CStringService::singleton()->stristr( $this->getNameLast(), 'Content-type' ) ) {

			// This condition protects against email bots that are using forms to send spam.
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name_last', 'Last name cannot contain @ signs or email headers.' ) );
		}

		return $boolIsValid;
	}

	public function valPhoneNumber() {
		$boolIsValid = true;

		if( false == is_null( $this->getPhoneNumber() ) && 1 != CValidation::checkPhoneNumberFullValidation( $this->getPhoneNumber() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'phone_number', 'Phone number must be in xxx-xxx-xxxx format.', 504 ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valEmailAddress() {
		$boolIsValid = true;

		if( false == is_null( $this->getEmailAddress() ) ) {

			$arrstrEmailAddress = explode( ',', $this->getEmailAddress() );

			if( true == valArr( $arrstrEmailAddress ) ) {

				foreach( $arrstrEmailAddress as $strEmailAddress ) {

					if( false == is_null( $strEmailAddress ) && false == CValidation::validateEmailAddresses( $strEmailAddress ) ) {
						$boolIsValid = false;
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Valid email address is required.' ) );
						return $boolIsValid;
					}
				}
			}
		} else {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'email_address', 'Email address is required.' ) );
		}

		return $boolIsValid;
	}

	public function valInvoices() {
		$boolIsValid = true;

		if( 1 != $this->getInvoicesEmailed() && 1 != $this->getInvoicesMailed() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'invoices_emailed', 'You must email or mail invoices.' ) );
		}

		return $boolIsValid;
	}

	public function valAccountAssociation( $objDatabase ) {

		$boolIsValid = true;

		$intAccount = \Psi\Eos\Admin\CAccounts::createService()->fetchAccountCount( 'where account_region_id=\'' . $this->getId() . '\'', $objDatabase );

		if( 0 != $intAccount ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'account_regions_id', $this->getRegionName() . ': You can not delete a account region, when it is associated with any account.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valRegionName( $objDatabase );
				$boolIsValid &= $this->valNameFirst();
				$boolIsValid &= $this->valNameLast();
				$boolIsValid &= $this->valPhoneNumber();
				$boolIsValid &= $this->valEmailAddress();
				$boolIsValid &= $this->valInvoices();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valAccountAssociation( $objDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>
