<?php

class CEmployeeAddress extends CBaseEmployeeAddress {

	protected $m_strCountryStateCode;

	/**
	 * Validation Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['country_state_code'] ) ) {
			$this->setCountryStateCode( $arrmixValues['country_state_code'] );
		}
	}

	public function getCountryStateCode() {
		return $this->m_strCountryStateCode;
	}

	public function setCountryStateCode( $strStateCode ) {
		$this->m_strCountryStateCode = $strStateCode;
	}

	public function valEmployeeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intEmployeeId ) ) {
			$boolIsValid = false;
			trigger_error( ' No employee id was found in CEmployeeAddress::valEmployeeId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valAddressTypeId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intAddressTypeId ) ) {
			$boolIsValid = false;
			trigger_error( ' No address type id was found in CEmployeeAddress::valEmployeeId()', E_USER_ERROR );
		}

		return $boolIsValid;
	}

	public function valStreetLine1( $strEmployeeCountryCode = NULL ) {

		$boolIsValid = true;

		if( CCountry::CODE_INDIA == $strEmployeeCountryCode ) {

			if( CAddressType::PERMANENT == $this->getAddressTypeId() ) {
				$strAddressTypeName	= 'Permanent';
			} elseif( CAddressType::CURRENT == $this->getAddressTypeId() ) {
				$strAddressTypeName	= 'Current';
			} else {
				$strAddressTypeName	= 'Primary';
			}

			if( false == isset( $this->m_strStreetLine1 ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', $strAddressTypeName . ' address is required.' ) );
			}
	   	} else {
		   	if( false == isset( $this->m_strStreetLine1 ) ) {
		   		$boolIsValid = false;
		   		$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'street_line1', ' Primary address is required.' ) );
		   	}
		}

		return $boolIsValid;
	}

	public function valCity() {
		$boolIsValid 		= true;
		$strCityFormat		= '/^[a-zA-Z0-9 ]+$/';

		if( false == isset( $this->m_strCity ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', ' City is required.' ) );
		} elseif( preg_match( $strCityFormat, $this->m_strCity ) !== 1 ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', ' A valid City is Required.' ) );

		}

		return $boolIsValid;
	}

	public function valCountryStateId() {
		$boolIsValid = true;

		if( false == isset( $this->m_intCountryStateId ) ) {
			$boolIsValid = false;
			if( CAddressType::PRIMARY == $this->getAddressTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_state_id', ' Primary state is required.' ) );
			} elseif( CAddressType::PERMANENT == $this->getAddressTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_state_id', ' Permanent state is required.' ) );
			} elseif( CAddressType::CURRENT == $this->getAddressTypeId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_state_id', ' Current state is required.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;

		if( false == isset( $this->m_strCountryCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', 'Country is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPostalCode() {
		$boolIsValid = true;
		$strZipCodeFormat		= ( isset( $this->m_strCountryCode ) && in_array( $this->m_strCountryCode, [ CCountry::CODE_USA, CCountry::CODE_INDIA ] ) ) ? '/^[0-9 ]++$/' : NULL;

		if( false == isset( $this->m_strPostalCode ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'postal_code', 'Zip code is required.' ) );
		} elseif( false == is_null( $strZipCodeFormat ) && preg_match( $strZipCodeFormat, $this->m_strPostalCode ) !== 1 ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'city', 'A valid Zip Code is Required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $strEmployeeCountryCode = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valStreetLine1( $strEmployeeCountryCode );
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valCountryStateId();
				$boolIsValid &= $this->valCountryCode();
				$boolIsValid &= $this->valPostalCode();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valEmployeeId();
				$boolIsValid &= $this->valAddressTypeId();
				$boolIsValid &= $this->valStreetLine1( $strEmployeeCountryCode );
				$boolIsValid &= $this->valCity();
				$boolIsValid &= $this->valCountryStateId();
				$boolIsValid &= $this->valCountryCode();
				$boolIsValid &= $this->valPostalCode();
				break;

			case VALIDATE_DELETE:
			default:
			  	// no validation avaliable for delete action
			  	break;
		}

		return $boolIsValid;
	}

}
?>