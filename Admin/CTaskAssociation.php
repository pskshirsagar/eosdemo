<?php

class CTaskAssociation extends CBaseTaskAssociation {

	protected $m_intTaskTitle;
	protected $m_intTaskTypeId;
	protected $m_fltDeveloperStoryPoints;
	protected $m_strTaskDescription;
	protected $m_strTaskTypeName;
	protected $m_strTaskStatusName;
	protected $m_strTaskPriorityName;
	protected $m_strTaskUserName;
	protected $m_strTaskReleaseDateTime;
	protected $m_strParentTaskTitle;
	protected $m_strDeveloperFullName;
	protected $m_strTaskImpact;
	protected $m_strClientName;
	protected $m_intClusterId;
	protected $m_intTaskStatusId;
	protected $m_intTaskPriorityId;
	protected $m_intUserId;
	protected $m_intTaskReleaseId;

	const MINIMUM_LIMIT					= 1;
	const MAXIMUM_LIMIT					= 5;

	public function getTaskTitle() {
		return $this->m_intTaskTitle;
	}

	public function getTaskTypeId() {
		return $this->m_intTaskTypeId;
	}

	public function getTaskDescription() {
		return $this->m_strTaskDescription;
	}

	public function getTaskTypeName() {
		return $this->m_strTaskTypeName;
	}

	public function getTaskStatusName() {
		return $this->m_strTaskStatusName;
	}

	public function getTaskPriorityName() {
		return $this->m_strTaskPriorityName;
	}

	public function getTaskUserName() {
		return $this->m_strTaskUserName;
	}

	public function getTaskReleaseDateTime() {
		return $this->m_strTaskReleaseDateTime;
	}

	public function getParentTaskTitle() {
		return $this->m_strParentTaskTitle;
	}

	public function getDeveloperStoryPoints() {
		return $this->m_fltDeveloperStoryPoints;
	}

	public function getDueDate() {
		return $this->m_strDueDate;
	}

	public function getDeveloperFullName() {
		return $this->m_strDeveloperFullName;
	}

	public function getTaskImpact() {
		return $this->m_strTaskImpact;
	}

	public function getClientName() {
		return $this->m_strClientName;
	}

	public function getClusterId() {
		return $this->m_intClusterId;
	}

	public function getTaskStatusId() {
		return $this->m_intTaskStatusId;
	}

	public function getTaskPriorityId() {
		return $this->m_intTaskPriorityId;
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function getTaskReleaseId() {
		return $this->m_intTaskReleaseId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['title'] ) ) 					$this->setTaskTitle( $arrmixValues['title'] );
		if( true == isset( $arrmixValues['task_type_id'] ) ) 			$this->setTaskTypeId( $arrmixValues['task_type_id'] );
		if( true == isset( $arrmixValues['task_description'] ) ) 		$this->setTaskDescription( $arrmixValues['task_description'] );
		if( true == isset( $arrmixValues['task_type_name'] ) ) 			$this->setTaskTypeName( $arrmixValues['task_type_name'] );
		if( true == isset( $arrmixValues['task_status_name'] ) ) 		$this->setTaskStatusName( $arrmixValues['task_status_name'] );
		if( true == isset( $arrmixValues['task_priority_name'] ) ) 		$this->setTaskPriorityName( $arrmixValues['task_priority_name'] );
		if( true == isset( $arrmixValues['task_user_name'] ) ) 			$this->setTaskUserName( $arrmixValues['task_user_name'] );
		if( true == isset( $arrmixValues['task_release_datetime'] ) ) 	$this->setTaskReleaseDateTime( $arrmixValues['task_release_datetime'] );
		if( true == isset( $arrmixValues['parent_task_title'] ) ) 		$this->setParentTaskTitle( $arrmixValues['parent_task_title'] );
		if( true == isset( $arrmixValues['developer_story_points'] ) ) 	$this->setDeveloperStoryPoints( $arrmixValues['developer_story_points'] );
		if( true == isset( $arrmixValues['due_date'] ) ) 				$this->setDueDate( $arrmixValues['due_date'] );
		if( true == isset( $arrmixValues['developer_full_name'] ) ) 	$this->setDeveloperFullName( $arrmixValues['developer_full_name'] );
		if( true == isset( $arrmixValues['task_impact'] ) )        		$this->setTaskImpact( $arrmixValues['task_impact'] );
		if( true == isset( $arrmixValues['client_name'] ) )         	$this->setClientName( $arrmixValues['client_name'] );
		if( true == isset( $arrmixValues['cluster_id'] ) )				$this->setClusterId( $arrmixValues['cluster_id'] );
		if( true == isset( $arrmixValues['task_status_id'] ) )			$this->setTaskStatusId( $arrmixValues['task_status_id'] );
		if( true == isset( $arrmixValues['task_priority_id'] ) )		$this->setTaskPriorityId( $arrmixValues['task_priority_id'] );
		if( true == isset( $arrmixValues['user_id'] ) )					$this->setUserId( $arrmixValues['user_id'] );
		if( true == isset( $arrmixValues['task_release_id'] ) )			$this->setTaskReleaseId( $arrmixValues['task_release_id'] );
		return;
	}

	public function setTaskTitle( $strTaskTitle ) {
		$this->m_intTaskTitle = $strTaskTitle;
	}

	public function setTaskTypeId( $intTaskTypeId ) {
		$this->m_intTaskTypeId = $intTaskTypeId;
	}

	public function setTaskDescription( $strTaskDescription ) {
		$this->m_strTaskDescription = $strTaskDescription;
	}

	public function setTaskTypeName( $strTaskTypeName ) {
		$this->m_strTaskTypeName = $strTaskTypeName;
	}

	public function setTaskStatusName( $strTaskStatusName ) {
		$this->m_strTaskStatusName = $strTaskStatusName;
	}

	public function setTaskPriorityName( $strTaskPriorityName ) {
		$this->m_strTaskPriorityName = $strTaskPriorityName;
	}

	public function setTaskUserName( $strTaskUserName ) {
		$this->m_strTaskUserName = $strTaskUserName;
	}

	public function setTaskReleaseDateTime( $strTaskReleaseDateTime ) {
		$this->m_strTaskReleaseDateTime = $strTaskReleaseDateTime;
	}

	public function setParentTaskTitle( $strParentTaskTitle ) {
		$this->m_strParentTaskTitle = $strParentTaskTitle;
	}

	public function setDeveloperStoryPoints( $fltDeveloperStoryPoints ) {
		$this->m_fltDeveloperStoryPoints = $fltDeveloperStoryPoints;
	}

	public function setDueDate( $strDueDate ) {
		$this->m_strDueDate = $strDueDate;
	}

	public function setDeveloperFullName( $strDeveloperFullName ) {
		$this->m_strDeveloperFullName = $strDeveloperFullName;
	}

	public function setTaskImpact( $strTaskImpact ) {
		$this->m_strTaskImpact = $strTaskImpact;
	}

	public function setClientName( $strClientName ) {
		$this->m_strClientName = $strClientName;
	}

	public function setClusterId( $intClusterId ) {
		$this->m_intClusterId = $intClusterId;
	}

	public function setTaskStatusId( $intTaskStatusId ) {
		$this->m_intTaskStatusId = $intTaskStatusId;
	}

	public function setTaskPriorityId( $intTaskPriorityId ) {
		$this->m_intTaskPriorityId = $intTaskPriorityId;
	}

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setTaskReleaseId( $intTaskReleaseId ) {
		$this->m_intTaskReleaseId = $intTaskReleaseId;
	}

	public function valParentTaskId( $objAdminDatabase, $boolHasSubTaskType = false ) {

		$boolIsValid = true;

		$intTaskId = $this->getTaskId();

		if( true == is_null( $this->getParentTaskId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_parent_id', 'Parent task ID is required.' ) );
			return $boolIsValid;
		}

		if( false == is_null( $intTaskId ) ) {

			$objParentTask = \Psi\Eos\Admin\CTasks::createService()->fetchTaskById( $this->getParentTaskId(), $objAdminDatabase );

			if( false == is_null( $this->getTaskId() ) && $this->getParentTaskId() == $this->getTaskId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_parent_id', 'Parent task ID can not be same as task ID.' ) );
				return $boolIsValid;
			}

			if( false == valObj( $objParentTask, 'CTask' ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_parent_id', 'Please enter valid parent task ID.' ) );
				return $boolIsValid;
			}

			if( false == is_null( $objParentTask->getDeletedOn() ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_parent_id', 'Please enter valid parent task ID.' ) );
				return $boolIsValid;
			}

			if( CTaskType::SUPPORT != $objParentTask->getTaskTypeId() && CTaskType::BUG != $objParentTask->getTaskTypeId() && CTaskType::FEATURE != $objParentTask->getTaskTypeId() && CTaskType::PROJECT != $objParentTask->getTaskTypeId() && CTaskType::NPS != $objParentTask->getTaskTypeId() && CTaskType::TRAINING != $objParentTask->getTaskTypeId() && CTaskType::WEBSITE != $objParentTask->getTaskTypeId() && CTaskType::PROPERTY != $objParentTask->getTaskTypeId() && CTaskType::MERCHANT_ACCOUNT != $objParentTask->getTaskTypeId() ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'parent_task_id', 'Parent ID other than task type Bug, Feature, Project and Support cannot be added.' ) );
				return $boolIsValid;
			}

			if( true == $boolHasSubTaskType ) return $boolIsValid;

			if( false == is_null( $objParentTask->getId() ) ) {

				if( false == $boolHasSubTaskType ) {
					$objTask	= \Psi\Eos\Admin\CTasks::createService()->fetchTaskById( $this->getId(), $objAdminDatabase );

					if( true == valObj( $objTask, 'CTask' ) ) {
						$arrobjSubTasks = $objTask->fetchSubTasks( $objAdminDatabase );
						if( true == valArr( $arrobjSubTasks ) ) {
							$arrintSubTaskIds = array_flip( array_keys( $arrobjSubTasks ) );

							if( true == array_key_exists( $this->getParentTaskId(), $arrintSubTaskIds ) ) {
								$boolIsValid = false;
								$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_parent_id', 'Parent task ID can not be own sub task id.' ) );
							}
						}
					}
				}
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase, $boolHasSubTaskType = false ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valParentTaskId( $objAdminDatabase, $boolHasSubTaskType );
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>