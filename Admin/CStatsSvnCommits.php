<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsSvnCommits
 * Do not add any new functions to this class.
 */

class CStatsSvnCommits extends CBaseStatsSvnCommits {

	public static function fetchAllStatsSvnCommit( $objDatabase ) {

		$strSql = 'SELECT
						e.name_full,
						d.name as department_name,
						ssc.svn_commit_count,
						ssc.svn_commit_file_count
					FROM
						employees e
						LEFT JOIN stats_svn_commits ssc ON e.id = ssc.employee_id
						LEFT JOIN departments d on e.department_id = d.id
					WHERE
						ssc.svn_commit_count IS NOT NULL AND
						ssc.svn_commit_file_count IS NOT NULL AND
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					ORDER BY
						e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

}
?>