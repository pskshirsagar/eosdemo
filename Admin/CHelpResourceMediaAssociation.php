<?php

class CHelpResourceMediaAssociation extends CBaseHelpResourceMediaAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentHelpResourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTranslatedHelpResourceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valParentMediaTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTranslatedMediaTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLocaleCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>