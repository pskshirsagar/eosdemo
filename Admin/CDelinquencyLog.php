<?php

class CDelinquencyLog extends CBaseDelinquencyLog {

	public function valId() {
		return true;
	}

	public function valCid() {
		return true;
	}

	public function valAccountId() {
		return true;
	}

	public function valPropertyDelinquencyId() {
		return true;
	}

	public function valSystemEmailId() {
		return true;
	}

	public function valDelinquencyLevelTypeId() {
		return true;
	}

	public function valEligibleDelinquencyLevelTypeId() {
		return true;
	}

	public function valMaxDelinquencyLevelTypeId() {
		return true;
	}

	public function valInvoiceIds() {
		return true;
	}

	public function valThresholdAmount() {
		return true;
	}

	public function valOverdueAmount() {
		return true;
	}

	public function valApprovedBy() {
		return true;
	}

	public function valApprovedOn() {
		return true;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 *
	 */

	public function createConsolidateDelinquencyLog( $arrmixDelinquencyAccountDetail ) {
		$this->setCid( $arrmixDelinquencyAccountDetail['cid'] );
		$this->setAccountId( $arrmixDelinquencyAccountDetail['account_id'] );
		$this->setPropertyId( $arrmixDelinquencyAccountDetail['property_id'] );
		$this->setIsManualUpdate( $arrmixDelinquencyAccountDetail['is_manual_update'] );
		$this->setUserNote( $arrmixDelinquencyAccountDetail['user_notes'] );
		$this->setSystemNote( $arrmixDelinquencyAccountDetail['system_notes'] );
		$this->setPropertyDelinquencyId( $arrmixDelinquencyAccountDetail['property_delinquencies_id'] );
		$this->setDelinquencyLevelTypeId( $arrmixDelinquencyAccountDetail['delinquency_level_type_id'] );
		$this->setEligibleDelinquencyLevelTypeId( $arrmixDelinquencyAccountDetail['eligible_delinquency_level_type_id'] );
		$this->setMaxDelinquencyLevelTypeId( $arrmixDelinquencyAccountDetail['max_delinquency_level_type_id'] );
		$this->setThresholdAmount( $arrmixDelinquencyAccountDetail['delinquency_threshold_amount'] );
		$this->setApprovedBy( $arrmixDelinquencyAccountDetail['approved_by'] );
		$this->setApprovedOn( $arrmixDelinquencyAccountDetail['approved_on'] );
		return $this;
	}

}
?>