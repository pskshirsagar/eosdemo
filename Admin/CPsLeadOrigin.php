<?php

class CPsLeadOrigin extends CBasePsLeadOrigin {

	const INFO_REQUEST 						= 1;
	const ORDER 							= 2;
	const NEWSLETTER_SUBSCRIPTION 			= 3;
	const DEMO_ACCOUNT_REQUEST 				= 4;
	const VERIFIED_INDEXED_DOMAIN 			= 5;
	const UNVERIFIED_INDEXED_DOMAIN 		= 6;
	const MIDVERIFIED_INDEXED_DOMAIN 		= 7;
	const HEAVY_HITTERS 					= 8;
	const NMHC 								= 9;
	const YANKER_DOMAIN 					= 10;
	const MANUAL							= 11;
	const INITIAL_COMPANY_MIGRATION  		= 12;
	const VACANCY  							= 17;
	const NAA_2010  						= 18;
	const VAULTWARE  						= 19;
	const ELLIPSE_CLIENTS					= 20;
	const BRAINSTROMING_SESSIONS_2010_DALLAS = 21;
	const ENTRATA_AD_SYSTEM					= 22;
	const SALES_LEAD_GENERATOR 				= 66;
	const ENTRATA_SIGN_UP 					= 68;
	const ENTRATA_APP_STORE 				= 70;
	const MASS_EMAILS		 				= 78;

	protected $m_boolIsSelected;

	public static $c_arrintUnassignedPsLeadOriginIds = [ self::INFO_REQUEST, self::ORDER, self::NEWSLETTER_SUBSCRIPTION, self::DEMO_ACCOUNT_REQUEST, self::MANUAL ];

	public function __construct() {
		parent::__construct();

		$this->m_boolIsSelected = false;

		return;
	}

	public function setIsSelected( $boolIsSelected ) {
		$this->m_boolIsSelected = $boolIsSelected;
	}

	public function getIsSelected() {
		return $this->m_boolIsSelected;
	}

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		if( true == $boolIsValid ) {

			if( true == is_numeric( $this->getName() ) || ( 0 < preg_match( '@[^a-z0-9 ]+@i', $this->getName() ) ) ) {
				$boolIsValid = false;

				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name should be alphanumeric.' ) );
			}
		}

		if( true == $boolIsValid && 3 > strlen( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name must be at least 3 characters.' ) );
		}

		if( true == $boolIsValid ) {
			$strSqlCondition = ( 0 < $this->getId() ? ' AND id <>' . $this->getId() : '' );
			$intCount = \Psi\Eos\Admin\CPsLeadOrigins::createService()->fetchPsLeadOriginCount( ' WHERE lower( name ) = \'' . addslashes( \Psi\CStringService::singleton()->strtolower( $this->getName() ) ) . '\'' . $strSqlCondition, $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Lead Origin already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( true == is_null( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

   	public function valPsLeadOriginUsage( $objDatabase ) {
		$boolIsValid = true;

		$intPsLeadCount = \Psi\Eos\Admin\CPsLeads::createService()->fetchPsLeadCountByPsLeadOriginId( $this->getId(), $objDatabase );

		if( 0 < ( int ) $intPsLeadCount ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, '', 'Lead origin can not be deleted because it\'s in use with Leads.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				$boolIsValid &= $this->valDescription();
				break;

			case VALIDATE_DELETE:
				$boolIsValid &= $this->valPsLeadOriginUsage( $objDatabase );
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>