<?php

class CEmployeeEncryptionPermission extends CBaseEmployeeEncryptionPermission {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReferenceTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEncryptionSystemTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasReadAccess() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHasWriteAccess() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valHasOppositePermissionExists( $objAdminDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valHasOppositePermissionExists( $objAdminDatabase ) {
		$boolIsValid 		= true;
		$boolHasReadAccess 	= ( true == $this->m_intHasReadAccess ) ? false : true;

		if( true == valObj( $objAdminDatabase, 'CDatabase' ) ) {
			$arrobjOppositeEmployeeEncryptionPermission = CEmployeeEncryptionPermissions::fetchEmployeeEncryptionPermissionByEmployeeIdsByReferenceIdByReferenceTypeIdByEncryptionSystemTypeIdByHasReadAccess( array( $this->m_intEmployeeId ), $this->m_intReferenceId, $this->m_intReferenceTypeId, $this->m_intEncryptionSystemTypeId, $objAdminDatabase, $boolHasReadAccess );

			if( true == valArr( $arrobjOppositeEmployeeEncryptionPermission ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'opposite_permission', 'You can\'t create an exact opposite permission(s).' ) );
			}
		}

		return $boolIsValid;
	}

}
?>