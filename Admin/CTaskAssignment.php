<?php

class CTaskAssignment extends CBaseTaskAssignment {

	protected $m_strTaskTypeName;
	protected $m_strTaskCreatedOn;

	/**
	 * Get Functions
	 */

	public function getTaskTypeName() {
		return $this->m_strTaskTypeName;
	}

	public function getTaskCreatedOn() {
		return $this->m_strTaskCreatedOn;
	}

	/**
	 * Set Functions
	 */

	public function setTaskTypeName( $strTaskTypeName ) {
		$this->m_strTaskTypeName = $strTaskTypeName;
	}

	public function setTaskCreatedOn( $strTaskCreatedOn ) {
		$this->m_strTaskCreatedOn = $strTaskCreatedOn;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['task_type_name'] ) )   $this->setTaskTypeName( $arrmixValues['task_type_name'] );
		if( true == isset( $arrmixValues['task_created_on'] ) )  	$this->setTaskCreatedOn( $arrmixValues['task_created_on'] );

		return;
	}

	/**
	 * Validate Functions
	 */

	public function valTaskId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaskStatusId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_status_id', 'Task id for task assignment is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTaskStatusId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaskStatusId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_status_id', 'Task status id for task assignment is required.' ) );
		}

		return $boolIsValid;
	}

	public function valUserId() {
		$boolIsValid = true;
		if( true == is_null( $this->getUserId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'user_id', 'User id for task assignment is required.' ) );
		}

		return $boolIsValid;
	}

	public function valTaskTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTaskTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'task_type_id', 'Task type for task assignment is required.' ) );
		}

		return $boolIsValid;
	}

	/**
	 * Other Functions
	 */

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valTaskId();
				// $boolIsValid &= $this->valUserId();
				$boolIsValid &= $this->valTaskStatusId();
				$boolIsValid &= $this->valTaskTypeId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>