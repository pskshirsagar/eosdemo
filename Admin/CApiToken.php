<?php

class CApiToken extends CBaseApiToken {

	protected $m_arrobjScopes;
	protected $m_strClientId;
	protected $m_strApiTokenType;
	protected $m_strEntityReferenceType;
	protected $m_strOauthClientType;
	protected $m_strAppName;
	protected $m_strCompanyName;
	protected $m_boolIsExpired;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEntityReferenceTypeId() {
		$boolIsValid = true;
		if( false != is_null( $this->m_intEntityReferenceTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entity_reference_type_id', 'Entity Reference type is required.' ) );
			return $boolIsValid;
		}
		if( false == in_array( $this->m_intEntityReferenceTypeId, CEntityReferenceType::$c_arrintAllEnttityReferenceTypeIds ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entity_reference_type_id', 'Invalid value for entity reference type.' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valEntityReferenceId() {
		$boolIsValid = true;
		if( false != is_null( $this->m_intEntityReferenceId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'entity_reference_id', 'Entity Reference is required.' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valApiTokenTypeId() {
		$boolIsValid = true;
		if( false != is_null( $this->m_intApiTokenTypeId ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'api_token_type_id', 'Api token type is required.' ) );
			return $boolIsValid;
		}
		if( false == in_array( $this->m_intApiTokenTypeId, CApiTokenType::$c_arrintAllApiTokenTypes ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'api_token_type_id', 'Invalid Api token type.' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valToken() {
		$boolIsValid = true;
		if( false != is_null( $this->m_strToken ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'token', 'Token is required.' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valExpires() {
		$boolIsValid = true;
		if( false != is_null( $this->m_strExpires ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'token', 'Expiry date is required.' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valDeviceIdentifier() {
		$boolIsValid = true;
		if( false != is_null( $this->m_strDeviceIdentifier ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'token', 'Device identifier is required.' ) );
			return $boolIsValid;
		}
		return $boolIsValid;
	}

	public function valOauthClientId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOauthClientTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRefreshToken( $objDatabase ) {
		$boolIsValid = true;

		$boolIsValid &= $this->valEntityReferenceId();
		$boolIsValid &= $this->valEntityReferenceTypeId();
		$boolIsValid &= $this->valApiTokenTypeId();
		if( $this->m_intApiTokenTypeId == CApiTokenType::SITETABLET_REFRESH_TOKEN || $this->m_intApiTokenTypeId == CApiTokenType::ENTRATA_MAINTENANCE_REFRESH_TOKEN ) {
			$boolIsValid &= $this->valDeviceIdentifier();
		}
		$boolIsValid &= $this->valToken();
		$boolIsValid &= $this->valExpires();
		if( false != $boolIsValid ) {
			$strWhere = ' WHERE
							token = \'' . pg_escape_string( $this->getToken() ) . '\'
							AND entity_reference_id = ' . ( int ) $this->getEntityReferenceId() . '
							AND entity_reference_type_id = ' . ( int ) $this->getEntityReferenceTypeId() . '
							AND api_token_type_id = ' . $this->getApiTokenTypeId() . '
							AND device_identifier = \'' . pg_escape_string( $this->getDeviceIdentifier() ) . '\' ';
			$intCount = \Psi\Eos\Admin\CApiTokens::createService()->fetchApiTokenCount( $strWhere, $objDatabase );
			if( 0 > $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'api_token', 'Token is already exist.' ) );
			}
		}
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			case 'refresh_token':
				$boolIsValid &= $this->valRefreshToken( $objDatabase );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Extra Values Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['client_id'] ) ) $this->setClientId( $arrmixValues['client_id'] );
		if( true == isset( $arrmixValues['api_token_type'] ) ) $this->setApiTokenType( $arrmixValues['api_token_type'] );
		if( true == isset( $arrmixValues['entity_refernce_type'] ) ) $this->setEntityReferenceType( $arrmixValues['entity_refernce_type'] );
		if( true == isset( $arrmixValues['oauth_client_type'] ) ) $this->setOauthClientType( $arrmixValues['oauth_client_type'] );
		if( true == isset( $arrmixValues['app_name'] ) ) $this->setAppName( $arrmixValues['app_name'] );
		if( true == isset( $arrmixValues['company_name'] ) ) $this->setCompanyName( $arrmixValues['company_name'] );
		if( true == isset( $arrmixValues['is_expired'] ) ) $this->setIsExpired( $arrmixValues['is_expired'] );
		return;
	}

	public function getClientId() {
		return $this->m_strClientId;
	}

	private function setClientId( $strClientId ) {
		$this->m_strClientId = $strClientId;
	}

	public function setApiTokenType( $strApiTokenType ) {
		$this->m_strApiTokenType = $strApiTokenType;
	}

	public function setEntityReferenceType( $strEntityReferenceType ) {
		$this->m_strEntityReferenceType = $strEntityReferenceType;
	}

	public function setOauthClientType( $strOauthClientType ) {
		$this->m_strOauthClientType = $strOauthClientType;
	}

	public function setAppName( $strAppName ) {
		$this->m_strAppName = $strAppName;
	}

	public function setCompanyName( $strCompanyName ) {
		$this->m_strCompanyName = $strCompanyName;
	}

	public function setIsExpired( $boolIsExpired ) {
		$this->m_boolIsExpired = $boolIsExpired;
	}

	public function getIsExpired() {
		return $this->m_boolIsExpired;
	}

	public function getCompanyName() {
		return $this->m_strCompanyName;
	}

	public function getAppName() {
		return $this->m_strAppName;
	}

	public function getApiTokenType() {
		return $this->m_strApiTokenType;
	}

	public function getEntityReferenceType() {
		return $this->m_strEntityReferenceType;
	}

	public function getOauthClientType() {
		return $this->m_strOauthClientType;
	}

	public function getOrFetchScopes( CDatabase $objDatabase ) {
		if( true == empty( $this->m_arrobjScopes ) ) {
			$this->m_arrobjScopes = \Psi\Eos\Admin\CScopes::createService()->fetchScopesByOauthClientId( $this->getOauthClientId(), $objDatabase );

		}
		return $this->m_arrobjScopes;
	}

}
?>