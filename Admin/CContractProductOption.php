<?php

class CContractProductOption extends CBaseContractProductOption {

	public function valContractId() {
		$boolIsValid = true;

		if( true == is_null( $this->getContractId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_id', 'Contract is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPropertyId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'property_id', 'Property is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'PS product is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductOptionId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductOptionId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_option_id', 'PS product option is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valContractId();
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valPropertyId();
				$boolIsValid &= $this->valPsProductOptionId();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == is_null( $this->getUpdatedBy() ) || true == is_null( $this->getUpdatedOn() ) ) {
			$this->setUpdatedOn( 'NOW()' );
		}

		return parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
	}

}
?>