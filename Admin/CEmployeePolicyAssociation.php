<?php

class CEmployeePolicyAssociation extends CBaseEmployeePolicyAssociation {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPolicyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDesignationIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valStateCodes() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeIds() {
		$boolValid = true;
		return $boolValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>