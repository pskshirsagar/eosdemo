<?php

class CStatsRevenue extends CBaseStatsRevenue {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChargeCodeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRevenueTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTotalAmount() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTransactionMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valInvoiceMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valExportBatchMonth() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>