<?php

class CPsFilter extends CBasePsFilter {

	/**
	 * Other Functions
	 *
	 */

	public static function unserializeOrLoadPsFilter( $intPsFilterTypeId, $strRequestFilterName, $objDatabase, $objSession, $arrRequestData ) {
		require_once( PATH_ADMIN_LIBRARY . 'SearchFilters/CPsFilterFactory.class.php' );

		$strSessionName 	= self::getFilterSessionName( $strRequestFilterName );
		$objSessionFilter 	= CSessionCache::fetchObject( $strRequestFilterName );

		$objCurrentSession = $objSession->getValue( $strSessionName );

		if( true == isset( $arrRequestData['clear_session_filter'] ) && 1 == $arrRequestData['clear_session_filter'] ) {
			unset( $objCurrentSession );
			CSessionCache::removeObject( $strRequestFilterName );
		}

		// All modules not yet use zend session cache, so added one more condition for changed modules and keeping the old one for unchanged modules.

		if( true == isset( $arrRequestData[$strRequestFilterName] ) && true == isset( $arrRequestData[$strRequestFilterName]['id'] ) && true == is_numeric( $arrRequestData[$strRequestFilterName]['id'] ) ) {
			$objPsFilter = CPsFilters::fetchPsFilterById( $arrRequestData[$strRequestFilterName]['id'], $objDatabase );
			if( true == valObj( $objPsFilter, 'CPsFilter' ) ) {
				$objPsFilter = $objPsFilter->downCast( $intPsFilterTypeId );
			}
		} elseif( true == isset( $objCurrentSession ) && 0 < \Psi\CStringService::singleton()->strlen( $objCurrentSession ) && ( false == isset( $arrRequestData['clear_session_filter'] ) || 1 != $arrRequestData['clear_session_filter'] ) ) {
			$objPsFilter = unserialize( $objCurrentSession );
		} elseif( true == is_object( $objSessionFilter ) && ( false == isset( $arrRequestData['clear_session_filter'] ) || 1 != $arrRequestData['clear_session_filter'] ) ) {
			$objPsFilter = $objSessionFilter;
		} else {
			$objPsFilter = CPsFilterFactory::createPsFilter( $intPsFilterTypeId );

			if( true == isset( $arrRequestData[$strRequestFilterName] ) ) {
				$objPsFilter->setSerializedFilter( serialize( ( object ) $arrRequestData[$strRequestFilterName] ) );
			}
		}

		if( false == valObj( $objPsFilter, 'CPsFilter' ) ) {
			trigger_error( 'Application Error: Failed to load CPsFilter object.', E_USER_ERROR );
		}

		$objPsFilter->unserializeFilterValues();

		$objTempPsFilter 	 = unserialize( $objPsFilter->getSerializedFilter() );

		if( true == valObj( $objTempPsFilter, 'stdClass' ) && true == isset( $objTempPsFilter->task_status_types ) ) {
			$strTempTaskStatuses = $objTempPsFilter->task_status_types;
			unset( $objTempPsFilter->task_status_types );
			$objTempPsFilter->task_statuses = $strTempTaskStatuses;
			$objPsFilter 					= serialize( $objTempPsFilter );
		}

		return $objPsFilter;
	}

	public static function unserializeOrLoadPsFilterBySession( $intPsFilterTypeId, $strRequestFilterName, $objDatabase, $objSession ) {
		// this function sets/unsets Session filter directly from session data instead of session cache and filter object is returned accordingly.

		require_once( PATH_ADMIN_LIBRARY . 'SearchFilters/CPsFilterFactory.class.php' );

		$objCurrentSession = $objSession->getValue( array( 'client_admin', $strRequestFilterName ) );

		if( true == isset( $objCurrentSession ) ) {
			$objSessionFilter = $objSession->getValue( array( 'client_admin', $strRequestFilterName ) );
		}

		if( true == isset( $_REQUEST['client_admin']['clear_session_filter'] ) && 1 == $_REQUEST['client_admin']['clear_session_filter'] ) {
			unset( $objCurrentSession );
		}

		if( true == isset( $_REQUEST[$strRequestFilterName] ) && true == isset( $_REQUEST[$strRequestFilterName]['id'] ) && true == is_numeric( $_REQUEST[$strRequestFilterName]['id'] ) ) {
			$objPsFilter = CPsFilters::fetchPsFilterById( $_REQUEST[$strRequestFilterName]['id'], $objDatabase );
			if( true == valObj( $objPsFilter, 'CPsFilter' ) ) {
				$objPsFilter = $objPsFilter->downCast( $intPsFilterTypeId );
			}
		} elseif( true == is_object( $objSessionFilter ) && ( false == isset( $_REQUEST['client_admin']['clear_session_filter'] ) || 1 != $_REQUEST['client_admin']['clear_session_filter'] ) ) {
			$objPsFilter = $objSessionFilter;
		} else {
			$objPsFilter = CPsFilterFactory::createPsFilter( $intPsFilterTypeId );

			if( true == isset( $_REQUEST['client_admin'][$strRequestFilterName] ) ) {
				$objPsFilter->setSerializedFilter( serialize( ( object ) $_REQUEST['client_admin'][$strRequestFilterName] ) );
			}
		}

		if( false == valObj( $objPsFilter, 'CPsFilter' ) ) {
			trigger_error( 'Application Error: Failed to load CPsFilter object.', E_USER_ERROR );
		}

		$objPsFilter->unserializeFilterValues();

		$objTempPsFilter 	 = unserialize( $objPsFilter->getSerializedFilter() );

		if( true == valObj( $objTempPsFilter, 'stdClass' ) && true == isset( $objTempPsFilter->task_status_types ) ) {
			$strTempTaskStatuses = $objTempPsFilter->task_status_types;
			unset( $objTempPsFilter->task_status_types );
			$objTempPsFilter->task_statuses = $strTempTaskStatuses;
			$objPsFilter 					= serialize( $objTempPsFilter );
		}

		return $objPsFilter;
	}

	public function unserializeFilterValues() {
		return true;
	}

	public function downCast( $intPsFilterTypeId ) {
		require_once( PATH_ADMIN_LIBRARY . 'SearchFilters/CPsFilterFactory.class.php' );

		$objPsFilter = CPsFilterFactory::createPsFilter( $intPsFilterTypeId );

		$objPsFilter->setId( $this->getId() );
		$objPsFilter->setCid( $this->getCid() );
		$objPsFilter->setName( $this->getName() );
		$objPsFilter->setSerializedFilter( $this->getSerializedFilter() );
		$objPsFilter->setFilterTypeId( $this->getFilterTypeId() );

		return $objPsFilter;
	}

	public static function getFilterSessionName( $strFilterRequestName ) {
		return 'serialized_ps_' . $strFilterRequestName . '_object';
	}

}
?>