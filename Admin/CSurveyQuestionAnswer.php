<?php
use \Psi\Libraries\Cryptography\CCrypto;

class CSurveyQuestionAnswer extends CBaseSurveyQuestionAnswer {

	const COMMENTED_CLOSED_OUT 			= 1;
	const NON_COMMENTED_CLOSED_OUT		= 2;
	const NUMERIC_WEIGHT_ONE			= 1;
	const NUMERIC_WEIGHT_HUNDRED		= 100;
	const NUMERIC_WEIGHT_FIFTY			= 50;
	const THREE_RATING					= 3;
	const FOUR_RATING					= 4;
	const FIVE_RATING					= 5;
	const NPS_HIGHEST_NUMERIC_WEIGHT	= 10;
	const LENGTH_LIMIT					= 50;
	const LENGTH_LIMIT_MAX				= 500;
	const APPLOAUSE_LENGTH_LIMIT		= 200;

	protected $m_boolIsRequired;

	/**
	 * Validate Functions
	 */

	public function valSurveyTemplateId() {
		$boolIsValid = true;
		if( false == valId( $this->getSurveyTemplateId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_id', 'Survey Template is required.' ) );
		}
		return $boolIsValid;
	}

	public function valSurveyId() {
		$boolIsValid = true;
		if( false == valId( $this->getSurveyId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_id', 'Survey is required.' ) );
		}
		return $boolIsValid;
	}

	public function valSurveyTemplateQuestionGroupId() {
		$boolIsValid = true;
		if( false == valId( $this->getSurveyTemplateQuestionGroupId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_question_group_id', 'Survey template question group is required.' ) );
		}
		return $boolIsValid;
	}

	public function valSurveyQuestionTypeId() {
		$boolIsValid = true;
		if( false == valId( $this->getSurveyQuestionTypeId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_question_type_id', 'Survey question type is required.' ) );
		}
		return $boolIsValid;
	}

	public function valSurveyTemplateQuestionId() {
		$boolIsValid = true;
		if( false == valId( $this->getSurveyTemplateQuestionId() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_question_id', 'Survey template question is required.' ) );
		}
		return $boolIsValid;
	}

	public function valSurveyTemplateOptionId() {
		$boolIsValid = true;

		if( false == valId( $this->getSurveyTemplateOptionId() ) && true == $this->getIsRequired() ) {
			$boolIsValid = false;
			switch( $this->getSurveyQuestionTypeId() ) {

				case CSurveyQuestionType::RATING:
				case CSurveyQuestionType::RATING_W_OPTIONAL_TEXT:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', __( 'Rating is required.' ) ) );
					break;

				case CSurveyQuestionType::RADIO:
				case CSurveyQuestionType::SELECT_BOX:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', 'Please select an option.' ) );
					break;

				case CSurveyQuestionType::CHECKBOX:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', 'Please select at least one option.' ) );
					break;

				default:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', 'Answer is required.' ) );
					break;
			}
		}

		return $boolIsValid;
	}

	public function valAnswer() {
		$boolIsValid = true;

		if( false == valStr( $this->getAnswer() ) && true == $this->getIsRequired() && CSurveyTemplateQuestion::MANAGER_SURVEY_APPLAUSE_QUESTION_ID != $this->m_intSurveyTemplateQuestionId ) {
			$boolIsValid = false;
			switch( $this->getSurveyQuestionTypeId() ) {

				case CSurveyQuestionType::TEXT_AREA:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', 'Comment is required.' ) );
					break;

				case CSurveyQuestionType::INPUT:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', 'Answer is required.' ) );
					break;

				case CSurveyQuestionType::PASSWORD:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', 'Please enter text.' ) );
					break;

				default:
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', 'Answer is required.' ) );
					break;
			}
		}

		return $boolIsValid;
	}

	public function valNumericWeight() {
		$boolIsValid = true;
		if( false == valId( $this->getNumericWeight() ) ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_id', 'Value is required and must be numeric.' ) );
		}
		return $boolIsValid;
	}

	public function valSurveyOptionOrAnswer() {
		$boolIsValid = true;

		$arrintOptionSurveyQuestionTypes = array(
			CSurveyQuestionType::RATING,
			CSurveyQuestionType::RATING_W_OPTIONAL_TEXT,
			CSurveyQuestionType::RADIO, CSurveyQuestionType::SELECT_BOX,
			CSurveyQuestionType::CHECKBOX
		);

		$arrintAnswerSurveyQuestionTypes = array(
			CSurveyQuestionType::TEXT_AREA,
			CSurveyQuestionType::INPUT,
			CSurveyQuestionType::PASSWORD
		);

		if( true == in_array( $this->m_intSurveyQuestionTypeId, $arrintOptionSurveyQuestionTypes ) ) {
			$boolIsValid &= $this->valSurveyTemplateOptionId();
		} elseif( true == in_array( $this->m_intSurveyQuestionTypeId, $arrintAnswerSurveyQuestionTypes ) ) {
			$boolIsValid &= $this->valAnswer();
		}
		if( true == in_array( ( int ) $this->m_fltNumericWeight, [ self::FOUR_RATING, self::FIVE_RATING ] ) && false == valStr( $this->m_strAnswer ) && CSurveyTemplateQuestion::QUESTION_SELF_REVIEW == $this->m_intSurveyTemplateQuestionId ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', 'Comment is required.' ) );
			$boolIsValid = false;
		}
		if( 1 == $this->m_fltNumericWeight && false == valStr( $this->m_strAnswer ) && CSurveyTemplateQuestion::MANAGER_SURVEY_APPLAUSE_QUESTION_ID == $this->m_intSurveyTemplateQuestionId ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', 'Additional details are required to support your appreciation.' ) );
			$boolIsValid = false;
		}

		if( false == in_array( ( int ) $this->m_fltNumericWeight, [ self::THREE_RATING, self::FOUR_RATING, self::FIVE_RATING ] ) && false == valStr( $this->m_strAnswer ) && CSurveyTemplateQuestion::QUESTION_IT_HELPDESK == $this->m_intSurveyTemplateQuestionId && CSurveyTemplate::IT_HELPDESK_SURVEY == $this->getSurveyTemplateId() ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', 'Comment is required for 1 or 2 star ratings.' ) );
			$boolIsValid = false;
		}
		if( valStr( $this->m_strAnswer ) && ( strlen( $this->m_strAnswer ) < self::LENGTH_LIMIT ) && ( CSurveyTemplateQuestion::QUESTION_IT_HELPDESK == $this->m_intSurveyTemplateQuestionId ) && ( CSurveyTemplate::IT_HELPDESK_SURVEY == $this->getSurveyTemplateId() ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', 'Minimum 50 Characters required for feedback comment.' ) );
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	public function valNoDuesSurvey() {

		$boolIsValid = true;

		if( true == in_array( $this->getSurveyTemplateQuestionId(), CSurveyTemplateQuestion::$c_arrintSurveyTemplateQuestion ) && 0.00 == $this->getNumericWeight() ) {
			$boolIsValid &= false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'survey_template_option_id', 'Clearance for No-Dues is required.' ) );
		}
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valSurveyOptionOrAnswer();
				$boolIsValid &= $this->valNoDuesSurvey();
 				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setIsRequired( $boolIsRequired ) {
		$this->m_boolIsRequired = $boolIsRequired;
	}

	public function setEncryptedAnswer( $strAnswer ) {
		if( true == valStr( $strAnswer ) ) {
			$this->m_strAnswer = ( 'production' == CONFIG_ENVIRONMENT ) ? ( CCrypto::createService() )->encrypt( trim( $strAnswer ), CONFIG_SODIUM_KEY_FORM_ANSWERS ) : trim( $strAnswer );
		} else {
			$this->m_strAnswer = NULL;
		}
	}

	public function setEncryptedAnswerModified( $strAnswerModified ) {
		if( true == valStr( $strAnswerModified ) ) {
			$this->m_strAnswerModified = ( 'production' == CONFIG_ENVIRONMENT ) ? ( CCrypto::createService() )->encrypt( trim( $strAnswerModified ), CONFIG_SODIUM_KEY_FORM_ANSWERS ) : trim( $strAnswerModified );
		} else {
			$this->m_strAnswerModified = NULL;
		}
	}

	public function setEncryptedAnswerEmailResponse( $strAnswerEmailResponse ) {
		if( true == valStr( $strAnswerEmailResponse ) ) {
			$this->m_strAnswerEmailResponse = ( 'production' == CONFIG_ENVIRONMENT ) ? ( CCrypto::createService() )->encrypt( trim( $strAnswerEmailResponse ), CONFIG_SODIUM_KEY_FORM_ANSWERS ) : trim( $strAnswerEmailResponse );
		} else {
			$this->m_strAnswerEmailResponse = NULL;
		}
	}

	/**
	 * Get Functions
	 *
	 */

	public function getIsRequired() {
		return $this->m_boolIsRequired;
	}

	public function getDecryptedAnswer() {
		return CSurvey::getDecryptedData( $this->getAnswer() );
	}

	public function getDecryptedAnswerModified() {
		return CSurvey::getDecryptedData( $this->getAnswerModified() );
	}

	public function getDecryptedAnswerEmailResponse() {
		return CSurvey::getDecryptedData( $this->getAnswerEmailResponse() );
	}

}
?>
