<?php

use Psi\Eos\Admin\CPsProducts;
use Psi\Eos\Admin\CContracts;

class CContractProduct extends CBaseContractProduct {

	protected $m_intPropertyCount;
	protected $m_intUnitCount;
	protected $m_intIsTransactional;
	protected $m_intIsUnlimitedCount;
	protected $m_intIsBundle;
	protected $m_intBundleProductIds;
	protected $m_intAppId;
	protected $m_intProductBundleId;

	protected $m_fltPercentWeight;
	protected $m_fltTotalRecurringAmount;

	protected $m_strPsProductName;
	protected $m_strBundlePsProductName;
	protected $m_strPsProductShortName;
	protected $m_strRevenuetype;
	protected $m_strFrequencyName;
	protected $m_intPackageProductId;
	protected $m_intContractProductId;
	protected $m_intPaymentTypeId;

	protected $m_arrobjContractProperties;

	const PAYMENT_TYPE_PER_UNIT			= 1;
	const PAYMENT_TYPE_PER_PROPERTY		= 2;
	const PAYMENT_TYPE_MIN_MAX			= 3;

	public static $c_arrstrPaymentTypes = [
		self::PAYMENT_TYPE_PER_UNIT		=> 'Per Unit',
		self::PAYMENT_TYPE_PER_PROPERTY	=> 'Per Property',
		self::PAYMENT_TYPE_MIN_MAX		=> 'Min / Max'
	];

	/**
	 * Get Functions
	 */

	public function getPsProductName() {
		return $this->m_strPsProductName;
	}

	public function getPsProductShortName() {
		return $this->m_strPsProductShortName;
	}

	public function getRevenuetype() {
		return $this->m_strRevenuetype;
	}

	public function getFrequencyName() {
		return $this->m_strFrequencyName;
	}

	public function getPropertyCount() {
		return $this->m_intPropertyCount;
	}

	public function getUnitCount() {
		return $this->m_intUnitCount;
	}

	public function getIsTransactional() {
		return $this->m_intIsTransactional;
	}

	public function getIsUnlimitedCount() {
		return $this->m_intIsUnlimitedCount;
	}

	public function getIsBundle() {
		return $this->m_intIsBundle;
	}

	public function getProductBundleId() {
		return $this->m_intProductBundleId;
	}

	public function getBundleProductIds() {
		return $this->m_intBundleProductIds;
	}

	public function getPercentWeight() {
		return $this->m_fltPercentWeight;
	}

	public function getContractProperties() {
		return $this->m_arrobjContractProperties;
	}

	public function getAppId() {
		return $this->m_intAppId;
	}

	public function getBundlePsProductName() {
		return $this->m_strBundlePsProductName;
	}

	public function getTotalRecurringAmount() {
		return $this->m_fltTotalRecurringAmount;
	}

	public function getPackageProduct() {
		return $this->m_intPackageProductId;
	}

	public function getPaymentTypeId() {
		return $this->m_intPaymentTypeId;
	}

	/**
	 * Set Functions
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['product_name'] ) && 0 < strlen( trim( $arrmixValues['product_name'] ) ) ) $this->setPsProductName( $arrmixValues['product_name'] );
		if( true == isset( $arrmixValues['product_short_name'] ) && 0 < strlen( trim( $arrmixValues['product_short_name'] ) ) ) $this->setPsProductShortName( $arrmixValues['product_short_name'] );
		if( true == isset( $arrmixValues['revenue_type'] ) && 0 < strlen( trim( $arrmixValues['revenue_type'] ) ) ) $this->setRevenuetype( $arrmixValues['revenue_type'] );
		if( true == isset( $arrmixValues['property_count'] ) && 0 < strlen( trim( $arrmixValues['property_count'] ) ) ) $this->setPropertyCount( $arrmixValues['property_count'] );
		if( true == isset( $arrmixValues['unit_count'] ) && 0 < strlen( trim( $arrmixValues['unit_count'] ) ) ) $this->setUnitCount( $arrmixValues['unit_count'] );
		if( true == isset( $arrmixValues['is_transactional'] ) && true == is_numeric( trim( $arrmixValues['is_transactional'] ) ) ) $this->setIsTransactional( $arrmixValues['is_transactional'] );
		if( true == isset( $arrmixValues['is_unlimited_count'] ) && 0 < strlen( trim( $arrmixValues['is_unlimited_count'] ) ) ) $this->setIsUnlimitedCount( $arrmixValues['is_unlimited_count'] );
		if( true == isset( $arrmixValues['frequency_name'] ) && 0 < strlen( trim( $arrmixValues['frequency_name'] ) ) ) $this->setFrequencyName( $arrmixValues['frequency_name'] );
		if( true == isset( $arrmixValues['is_bundle'] ) && 0 < strlen( trim( $arrmixValues['is_bundle'] ) ) ) $this->setIsBundle( $arrmixValues['is_bundle'] );
		if( true == isset( $arrmixValues['bundle_product_ids'] ) ) $this->setBundleProductIds( $arrmixValues['bundle_product_ids'] );
		if( true == isset( $arrmixValues['percent_weight'] ) ) $this->setPercentWeight( $arrmixValues['percent_weight'] );
		if( true == isset( $arrmixValues['total_recurring_amount'] ) ) $this->setTotalRecurringAmount( $arrmixValues['total_recurring_amount'] );
		if( true == isset( $arrmixValues['product_bundle_id'] ) ) $this->setProductBundleId( $arrmixValues['product_bundle_id'] );
		if( true == isset( $arrmixValues['package_product_id'] ) ) $this->setPackageProductId( $arrmixValues['package_product_id'] );
		if( true == isset( $arrmixValues['payment_type_id'] ) ) $this->setPaymentTypeId( $arrmixValues['payment_type_id'] );

		return;
	}

	public function setPaymentTypeId( $intPaymentTypeId ) {
		$this->m_intPaymentTypeId = $intPaymentTypeId;
	}

	public function setPsProductName( $strPsProductName ) {
		$this->m_strPsProductName = $strPsProductName;
	}

	public function setPsProductShortName( $strPsProductShortName ) {
		$this->m_strPsProductShortName = $strPsProductShortName;
	}

	public function setRevenuetype( $strRevenuetype ) {
		$this->m_strRevenuetype = $strRevenuetype;
	}

	public function setFrequencyName( $strFrequencyName ) {
		$this->m_strFrequencyName = $strFrequencyName;
	}

	public function setPropertyCount( $strPropertyCount ) {
		$this->m_intPropertyCount = $strPropertyCount;
	}

	public function setUnitCount( $strUnitCount ) {
		$this->m_intUnitCount = $strUnitCount;
	}

	public function setIsTransactional( $intIsTransactional ) {
		$this->m_intIsTransactional = $intIsTransactional;
	}

	public function setIsUnlimitedCount( $intIsUnlimitedCount ) {
		$this->m_intIsUnlimitedCount = $intIsUnlimitedCount;
	}

	public function setIsBundle( $intIsBundle ) {
		$this->m_intIsBundle = $intIsBundle;
	}

	public function setProductBundleId( $intProductBundleId ) {
		$this->m_intProductBundleId = $intProductBundleId;
	}

	public function setBundleProductIds( $arrintBundleProductIds ) {
		$this->m_intBundleProductIds = $arrintBundleProductIds;
	}

	public function setPercentWeight( $fltPercentWeight ) {
		return $this->m_fltPercentWeight = $fltPercentWeight;
	}

	public function setContractProperties( $arrobjContractProperties ) {
		$this->m_arrobjContractProperties = $arrobjContractProperties;
	}

	public function setAppId( $intAppId ) {
		$this->m_intAppId = $intAppId;
	}

	public function setBundlePsProductName( $strBundlePsProductName ) {
		$this->m_strBundlePsProductName = $strBundlePsProductName;
	}

	public function setTotalRecurringAmount( $fltTotalRecurringAmount ) {
		$this->m_fltTotalRecurringAmount = CStrings::strToFloatDef( $fltTotalRecurringAmount, NULL, false, 2 );
	}

	/**
	 * Other Functions
	 */

	public function valCid() {
		$boolIsValid = true;

		if( true == is_null( $this->getCid() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'cid', 'client is required.' ) );
		}
		return $boolIsValid;
	}

	public function valContractId() {
		$boolIsValid = true;

		if( true == is_null( $this->getContractId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'contract_id', 'Contract is required.' ) );
		}

		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsProductId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_product_id', 'Product is required.' ) );
		}

		return $boolIsValid;
	}

	public function valEstimatedUnitCount() {
		$boolIsValid = true;

		if( 0 >= $this->getEstimatedUnitCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'estimated_unit_count', 'Estimated unit count cannot be zero.' ) );
		}

		return $boolIsValid;
	}

	public function valEstimatedPropertyCount() {
		$boolIsValid = true;

		if( 0 >= $this->getEstimatedPropertyCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'estimated_property_count', 'Estimated property count cannot be zero.' ) );
		} elseif( $this->getEstimatedPropertyCount() > $this->getEstimatedUnitCount() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'estimated_property_count', 'Estimated unit count cannot be less than the property count.' ) );
		}

		return $boolIsValid;
	}

	public function valTrainingAmount() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getTrainingAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'training_amount', 'Invalid Training amount.' ) );
		}

		return $boolIsValid;
	}

	public function valImplementationAmount() {

		$boolIsValid = true;

		if( false == is_numeric( $this->getImplementationAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'implementation_amount', 'Invalid implementation amount.' ) );
		}

		return $boolIsValid;
	}

	public function valRecurringAmount() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getRecurringAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'recurring_amount', 'Invalid recurring amount.' ) );
		}

		return $boolIsValid;
	}

	public function valTransactionalAmount() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getTransactionalAmount() ) && false == is_null( $this->getTransactionalAmount() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transactional_amount', 'Invalid transactional amount.' ) );
		}

		// if( 30 < $this->getTransactionalAmount() ) {
		//	$boolIsValid = false;
		//	$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'transactional_amount', 'Transaction amount cannot exceed $30.' ) );
		// }

		return $boolIsValid;
	}

	public function valTrainingHours() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getTrainingHours() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'training_hours', 'Invalid training hours.' ) );
		}

		return $boolIsValid;
	}

	public function valImplementationHours() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getImplementationHours() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'implementation_hours', 'Invalid implementation hours.' ) );
		}

		return $boolIsValid;
	}

	public function valUniversalUnitLimit() {
		$boolIsValid = true;

		if( false == is_numeric( $this->getUniversalUnitLimit() ) && false == is_null( $this->getUniversalUnitLimit() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'universal_unit_limit', 'Invalid maximum unit limit.' ) );
		}

		return $boolIsValid;
	}

	public function valMinMaxUnits() {
		$boolIsValid = true;

		if( 0 > $this->getMinUnits() || 0 > $this->getMaxUnits() ) {
			$boolIsValid = false;

			$strErrorMessages = ( 0 > $this->getMinUnits() ) ?  'Min units' : '';
			$strErrorMessages .= ( 0 > $this->getMaxUnits() ) ? ( true == valStr( $strErrorMessages ) ?  ' and max units ' : 'Max units' ) : '';

			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_max_units', $strErrorMessages . ' should not be less than 0.' ) );
		}

		if( ( int ) $this->getMinUnits() > ( int ) $this->getMaxUnits() ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'min_max_units', 'Max units should be greater than min units.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valTrainingAmount();
				$boolIsValid &= $this->valRecurringAmount();
				$boolIsValid &= $this->valImplementationAmount();
				$boolIsValid &= $this->valTransactionalAmount();
				$boolIsValid &= $this->valUniversalUnitLimit();
				$boolIsValid &= $this->valMinMaxUnits();
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valContractId();
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valTrainingAmount();
				$boolIsValid &= $this->valRecurringAmount();
				$boolIsValid &= $this->valImplementationAmount();
				$boolIsValid &= $this->valTransactionalAmount();
				$boolIsValid &= $this->valUniversalUnitLimit();
				$boolIsValid &= $this->valMinMaxUnits();
				break;

			case VALIDATE_DELETE:
				break;

			case 'contract_renew':
				$boolIsValid &= $this->valCid();
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valImplementationAmount();
				$boolIsValid &= $this->valRecurringAmount();
				$boolIsValid &= $this->valTransactionalAmount();
				$boolIsValid &= $this->valMinMaxUnits();
				break;

			case 'ps_website_request':
				$boolIsValid &= $this->valPsProductId();
				break;

			case 'app_store':
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valTrainingAmount();
				$boolIsValid &= $this->valRecurringAmount();
				$boolIsValid &= $this->valImplementationAmount();
				$boolIsValid &= $this->valTransactionalAmount();
				$boolIsValid &= $this->valUniversalUnitLimit();
				break;

			case 'estimated_property_count_unit_count':
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valTrainingAmount();
				$boolIsValid &= $this->valRecurringAmount();
				$boolIsValid &= $this->valImplementationAmount();
				$boolIsValid &= $this->valTransactionalAmount();
				$boolIsValid &= $this->valUniversalUnitLimit();
				$boolIsValid &= $this->valMinMaxUnits();
				$boolIsValid &= $this->valEstimatedUnitCount();
				$boolIsValid &= $this->valEstimatedPropertyCount();
				break;

			case 'insert_contract_product':
				$boolIsValid &= $this->valPsProductId();
				$boolIsValid &= $this->valEstimatedUnitCount();
				$boolIsValid &= $this->valEstimatedPropertyCount();
				$boolIsValid &= $this->valMinMaxUnits();
				$boolIsValid &= $this->valTransactionalAmount();
				break;

			default:
		}
		return $boolIsValid;
	}

	public function insert( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == $boolReturnSqlOnly ) {
			return parent::insert( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$objContract = CContracts::createService()->fetchContractById( $this->getContractId(), $objDatabase );

		if( CContractStatusType::CONTRACT_APPROVED != $objContract->getContractStatusTypeId() ) {
			$this->handleLogging( $intUserId, $objDatabase, $objContract );
		}

		return parent::insert( $intUserId, $objDatabase );
	}

	public function update( $intUserId, $objDatabase, $boolReturnSqlOnly = false ) {

		if( true == $boolReturnSqlOnly ) {
			return parent::update( $intUserId, $objDatabase, $boolReturnSqlOnly );
		}

		$objContract = CContracts::createService()->fetchContractById( $this->getContractId(), $objDatabase );

		if( CContractStatusType::CONTRACT_APPROVED != $objContract->getContractStatusTypeId() ) {
			$this->handleLogging( $intUserId, $objDatabase, $objContract );
		}

		if( false == parent::update( $intUserId, $objDatabase ) ) {
			return false;
		}

		return true;

	}

	public function createClone( $intUserId, $intContractId = NULL, $intPackageProductId = NULL ) {
		$objContractProduct = clone $this;
		$objContractProduct->setId( NULL );
		$objContractProduct->setContractId( $intContractId );
		$objContractProduct->setPackageProductId( $intPackageProductId );
		$objContractProduct->setCreatedBy( $intUserId );
		$objContractProduct->setCreatedOn( 'NOW()' );
		$objContractProduct->setUpdatedBy( $intUserId );
		$objContractProduct->setUpdatedOn( 'NOW()' );

		return $objContractProduct;
	}

	public function handleLogging( $intUserId, $objDatabase, $objContract = NULL ) {

		$boolIsValid = true;
		$arrobjActions = [];

		$arrobjPsProducts = CPsProducts::createService()->fetchAllPsProducts( $objDatabase, NULL, NULL, false );

		$arrobjCurrentContractProducts = \Psi\Eos\Admin\CContractProducts::createService()->fetchGroupedContractProductsAndBundlesByContractId( ( int ) $this->getContractId(), $objDatabase );

		$arrobjCurrentPsProductIds		= [];
		$arrobjCurrentProductBundleIds	= [];
		if( true == valArr( $arrobjCurrentContractProducts ) ) {
			foreach( $arrobjCurrentContractProducts as $objContractProduct ) {
				if( true == valId( $objContractProduct->getPsProductId() ) ) {
					$arrobjCurrentPsProductIds[$objContractProduct->getPsProductId()] = $objContractProduct;
				} else {
					$arrobjCurrentProductBundleIds[$objContractProduct->getBundlePsProductId()] = $objContractProduct;
				}
			}
		}

		$intPsProductId = $this->getPsProductId();

		if ( false == valArr( $arrobjCurrentPsProductIds ) ) $arrobjCurrentPsProductIds = [];

		if( !in_array( $this->getPsProductId(), array_keys( $arrobjCurrentPsProductIds ) ) ) {
			$objAction = $objContract->createAction();
			$objAction->setActionTypeId( CActionType::PRODUCT_ADDED );
			$objAction->setActionDescription( 'Product ' . $arrobjPsProducts[$intPsProductId]->getName() . ' was added.' );
			$arrobjActions[] = $objAction;
		}

		if( true == valArr( $arrobjCurrentContractProducts ) ) {
			foreach( $arrobjCurrentContractProducts as $objCurrentContractProduct ) {

				$intPsProductOrBundleId = $objCurrentContractProduct->getPsProductId();
				if( false == valId( $intPsProductOrBundleId ) ) {
					$intPsProductOrBundleId = $objCurrentContractProduct->getBundlePsProductId();
				}

				if( $intPsProductId == $intPsProductOrBundleId ) {
					if( $objCurrentContractProduct->getImplementationAmount() != $this->getImplementationAmount() ) {
						$objAction = $objContract->createAction();
						$objAction->setActionTypeId( CActionType::PRICE_CHANGE );
						$objAction->setActionDescription( $arrobjPsProducts[$intPsProductOrBundleId]->getName() . ' set-up price changed from ' . __( '{%f, 0, p:2}', [ $objCurrentContractProduct->getImplementationAmount() ] ) . ' to ' . __( '{%f, 0, p:2}', [ $this->getImplementationAmount() ] ) );
						$arrobjActions[] = $objAction;
					}

					if( $objCurrentContractProduct->getRecurringAmount() != $this->getRecurringAmount() ) {
						$objAction = $objContract->createAction();
						$objAction->setActionTypeId( CActionType::PRICE_CHANGE );
						$objAction->setActionDescription( $arrobjPsProducts[$intPsProductOrBundleId]->getName() . ' recurring price changed from ' . __( '{%f, 0, p:2}', [ $objCurrentContractProduct->getRecurringAmount() ] ) . ' to ' . __( '{%f, 0, p:2}', [ $this->getRecurringAmount() ] ) );
						$arrobjActions[] = $objAction;
					}
				}
			}
		}

		// Loop on the current products that are associated, and see if there are any that have changed pricing.

		if( true == isset( $arrobjActions ) && true == valArr( $arrobjActions ) ) {
			foreach( $arrobjActions as $objAction ) {
				$boolIsValid &= $objAction->insert( $intUserId, $objDatabase );
			}
		}

		return $boolIsValid;
	}

}
?>