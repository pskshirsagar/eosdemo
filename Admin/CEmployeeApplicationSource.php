<?php

class CEmployeeApplicationSource extends CBaseEmployeeApplicationSource {

	const CANDIDATE_REFERRAL		= 17;
	const EMPLOYEE_REFERRALS 		= 16;
	const ENTRATA_EMPLOYEE	 		= 28;
	const EX_EMPLOYEE_REFERENCE		= 37;
	const VENDOR_KELLY_SERVICES		= 38;
	const VENDOR_ADECCO_INDIA		= 39;

	public static $c_arrstrVendors = [
		self::VENDOR_KELLY_SERVICES => 'Kelly Services',
		self::VENDOR_ADECCO_INDIA => 'Adecco India'
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>