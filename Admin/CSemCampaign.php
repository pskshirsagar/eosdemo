<?php

class CSemCampaign extends CBaseSemCampaign {

	/**
	 * Get Functions
	 */

	public function getSeoName() {
		return trim( \Psi\CStringService::singleton()->strtolower( preg_replace( array( '/[^a-zA-Z0-9\s_\-]+/', '/(\s|-|_)+/' ), array( '', '-' ), $this->getName() ) ) );
	}

	public function getUrlFormattedName() {
		return \Psi\CStringService::singleton()->strtolower( str_replace( ' ', '-', \Psi\Eos\Admin\CStates::createService()->determineStateNameByStateCode( $this->getStateCode() ) ) );
	}

	/**
	 * Create Functions
	 */

	public function createSemAdGroup() {

		$objSemAdGroup	= new CSemAdGroup();

		$objSemAdGroup->setSemCampaignId( $this->getId() );

		return $objSemAdGroup;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchSemAdGroupByNameOrId( $strNameOrId, $objAdminDatabase ) {
		return CSemAdGroups::fetchSemAdGroupBySemCampaignIdByNameOrId( $this->getId(), $strNameOrId, $objAdminDatabase );
	}

	public function fetchSemCampaignSource( $objAdminDatabase ) {
		return CSemCampaignSources::fetchSemCampaignSourcesBySemCampaignId( $this->getId(), $objAdminDatabase );
	}

	public function fetchSemCampaignSourceBySemSourceId( $intSemSourceId, $objAdminDatabase ) {
		return CSemCampaignSources::fetchSemCampaignSourcesBySemCampaignIdBySemSourceId( $this->getId(), $intSemSourceId, $objAdminDatabase );
	}

}
?>