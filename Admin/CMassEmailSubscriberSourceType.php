<?php

class CMassEmailSubscriberSourceType extends CBaseMassEmailSubscriberSourceType {

	const MANUAL				= 1;
	const SEGMENT				= 2;
	const IMPORT_LIST			= 3;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>