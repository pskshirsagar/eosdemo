<?php

class CAccountInvoiceTransmissionDetail extends CBaseAccountInvoiceTransmissionDetail {

	public function valInvoiceTransmissionTypeDetails() {
		$boolIsValid = true;

		$strErroMsg = '';

		if( true == is_null( $this->getVendorUrl() ) || false == valStr( $this->getVendorUrl() ) ) {
			$strErroMsg .= 'Vendor URL is required.';
			$boolIsValid = false;
		}

		if( false == CValidation::checkUrl( $this->getVendorUrl() ) ) {
			$strErroMsg .= 'Vendor URL is not valid.';
			$boolIsValid = false;
		}

		if( true == is_null( $this->getSupplierDomain() ) || false == valStr( $this->getSupplierDomain() ) ) {
			$strErroMsg .= 'Supplier Domain is required. ';
			$boolIsValid = false;
		}

		if( true == is_null( $this->getSupplierIdentity() ) || false == valStr( $this->getSupplierIdentity() ) ) {
			$strErroMsg .= 'Supplier Identity is required. ';
			$boolIsValid = false;
		}

		if( true == is_null( $this->getBuyerDomain() ) || false == valStr( $this->getBuyerDomain() ) ) {
			$strErroMsg .= 'Buyer Domain is required.';
			$boolIsValid = false;
		}

		if( true == is_null( $this->getBuyerIdentity() ) || false == valStr( $this->getBuyerIdentity() ) ) {
			$strErroMsg .= 'Buyer Identity is required. ';
			$boolIsValid = false;
		}

		if( true == is_null( $this->getSenderDomain() ) || false == valStr( $this->getSenderDomain() ) ) {
			$strErroMsg .= 'Sender Domain is required.';
			$boolIsValid = false;
		}

		if( true == is_null( $this->getSenderIdentity() ) || false == valStr( $this->getSenderIdentity() ) ) {
			$strErroMsg .= 'Sender Identity is required. ';
			$boolIsValid = false;
		}

		if( true == is_null( $this->getSharedSecret() ) || false == valStr( $this->getSharedSecret() ) ) {
			$strErroMsg .= 'Shared Secret is required. ';
			$boolIsValid = false;
		}

		if( true == is_null( $this->getPurchaseOrderNumber() ) || false == valStr( $this->getPurchaseOrderNumber() ) ) {
			$strErroMsg .= 'Purchase Order Number dsfdsfdg is required. ';
			$boolIsValid = false;
		}

		if( true == valStr( $strErroMsg ) ) {
			$this->addErrorMsg( new CErrorMsg( NULL, '', $strErroMsg ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valInvoiceTransmissionTypeDetails();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
