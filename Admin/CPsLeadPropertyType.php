<?php

class CPsLeadPropertyType extends CBasePsLeadPropertyType {

	public function valPsLeadId() {
		$boolIsValid = true;

		if( true == is_null( $this->getPsLeadId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'ps_lead_id', 'Ps lead id is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valPsLeadId( $objAdminDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>