<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTaskTypeDepartments
 * Do not add any new functions to this class.
 */

class CTaskTypeDepartments extends CBaseTaskTypeDepartments {

	public static function fetchAllTaskTypeDepartments( $objDatabase ) {

		$strSql = 'SELECT
					 *
					FROM
						task_type_departments ttd
						JOIN task_types tt ON ttd.task_type_id = tt.id
						JOIN departments d ON d.id = ttd.department_id';

		return fetchData( $strSql, $objDatabase );

	}

	public static function deleteTaskTypeDepartmentsByTaskTypeIdByDepartmentIds( $intTaskTypeId, $arrintDepartmentIds, $objDatabase ) {

		if( false == is_null( $intTaskTypeId ) && false == valArr( $arrintDepartmentIds ) ) {
			return NULL;
		}

		$strSql = 'DELETE
					FROM
						task_type_departments
					WHERE
						task_type_id = ' . ( int ) $intTaskTypeId . ' AND department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' ) ';

		$objDataset = $objDatabase->createDataset();

		if( false == $objDataset->execute( $strSql ) ) {
			trigger_error( 'Error: sql failed to delete task type departments and set null the id field on transactions' );

		}
	}

}
?>