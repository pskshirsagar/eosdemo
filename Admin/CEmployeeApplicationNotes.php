<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationNotes
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationNotes extends CBaseEmployeeApplicationNotes {

	public static function fetchEmployeeApplicationNotesByEmployeeApplicationId( $intEmployeeApplicationId, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM employee_application_notes WHERE employee_application_id = ' . ( int ) $intEmployeeApplicationId . ' ORDER BY note_datetime';
		return self::fetchEmployeeApplicationNotes( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeApplicationNotesByEmployeeApplicationIds( $arrintEmployeeApplicationIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeApplicationIds ) ) return NULL;

		$strSql = 'SELECT * FROM employee_application_notes WHERE employee_application_id IN ( ' . implode( ',', $arrintEmployeeApplicationIds ) . ')';

		return self::fetchEmployeeApplicationNotes( $strSql, $objDatabase );
	}

	public static function fetchEmployeeApplicationNotesByEmployeeApplicationIdByNoteTypeIds( $intEmployeeApplicationId, $arrintNoteTypeIds, $objDatabase ) {

		if( false == is_numeric( $intEmployeeApplicationId ) || false == valArr( $arrintNoteTypeIds ) ) return NULL;

		$strSql = 'SELECT ean.*,
					e.preferred_name
					FROM employee_application_notes as ean
					JOIN users u on ean.created_by = u.id
					JOIN employees e on e.id = u.employee_id
					WHERE ean.employee_application_id=' . ( int ) $intEmployeeApplicationId . ' AND ean.note_type_id IN ( ' . implode( ', ', $arrintNoteTypeIds ) . ' )' .
					' ORDER BY id DESC';

		return fetchData( $strSql, $objDatabase );
	}

}
?>