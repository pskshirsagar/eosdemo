<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CDocumentTemplateParameters
 * Do not add any new functions to this class.
 */

class CDocumentTemplateParameters extends CBaseDocumentTemplateParameters {

	public static function fetchDocumentTemplateParametersByDocumentTemplateComponentIdsByDocumentTemplateId( $arrintDocumentTemplateComponentIds, $intDocumentTemplateId, $objDatabase ) {

		if( false == valArr( $arrintDocumentTemplateComponentIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						document_template_parameters
					WHERE
						document_template_component_id IN ( ' . implode( ',', $arrintDocumentTemplateComponentIds ) . ' )
						AND document_template_id = ' . ( int ) $intDocumentTemplateId . '
					ORDER BY key';

		return self::fetchDocumentTemplateParameters( $strSql, $objDatabase );
	}
}
?>