<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationScoreTypes
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationScoreTypes extends CBaseEmployeeApplicationScoreTypes {

	public static function fetchEmployeeApplicationScoreTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CEmployeeApplicationScoreType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchEmployeeApplicationScoreType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CEmployeeApplicationScoreType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllEmployeeApplicationScoreTypes( $objDatabase ) {
		return self::fetchEmployeeApplicationScoreTypes( 'SELECT * FROM employee_application_score_types', $objDatabase );
	}

}
?>