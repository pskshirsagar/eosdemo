<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTeams
 * Do not add any new functions to this class.
 */

class CTeams extends CBaseTeams {

	public static function fetchPrimaryTeamByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT t.*, te.is_primary_team FROM teams t,team_employees te WHERE te.is_primary_team = 1 AND te.employee_id = ' . ( int ) $intEmployeeId . ' AND t.id = te.team_id AND deleted_by IS NULL ORDER BY t.name;';
		return self::fetchTeam( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTeamsCount( $objTeamsFilter, $objDatabase ) {

		$strWhere = self::getSearchCriteria( $objTeamsFilter );

		$strSql = ' SELECT
						count(t.id)
					FROM
						teams t
						LEFT JOIN employees e ON ( t.manager_employee_id = e.id ) 
						LEFT JOIN employee_addresses ead ON ( e.id = ead.employee_id AND ead.address_type_id = ' . CAddressType::PRIMARY . ' )' . $strWhere;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];

		return 0;
	}

	public static function fetchActiveTeamsByIds( $arrintTeamsIds, $objDatabase ) {

		if( false == valArr( $arrintTeamsIds ) ) return NULL;

		return self::fetchTeams( sprintf( 'SELECT * FROM %s WHERE deleted_by IS NULL AND deleted_on IS NULL AND id IN( %s )', 'teams', implode( ',', $arrintTeamsIds ) ), $objDatabase );
	}

	public static function fetchConflictingTeamCountByName( $strTeamName, $intTeamId, $intManagerEmployeeId, $objDatabase ) {

		if( false == is_null( $strTeamName ) && false == is_null( $intManagerEmployeeId ) ) {

			$strWhereSql = ' WHERE
					name = \'' . trim( addslashes( $strTeamName ) ) . '\'
					AND manager_employee_id = \'' . ( int ) $intManagerEmployeeId . '\'
					AND deleted_by is NULL ';

			if( false == is_null( $intTeamId ) ) {
				$strWhereSql .= ' AND id != ' . ( int ) $intTeamId;
			}

			return self::fetchTeamCount( $strWhereSql, $objDatabase );
		} else {
			return NULL;
		}
	}

	public static function fetchTeamByManagerEmployeeId( $intManagerEmployeeId, $objDatabase ) {
		return self::fetchTeam( sprintf( 'SELECT * FROM teams WHERE deleted_by IS NULL AND manager_employee_id = %d LIMIT 1', ( int ) $intManagerEmployeeId ), $objDatabase );
	}

	public static function fetchAllTeams( $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams WHERE deleted_by IS NULL ORDER BY name' ), $objDatabase );
	}

	public static function fetchAllActiveTeams( $objDatabase, $strCountryCode = NULL ) {

		$strSubSql = $strJoinCondition = '';

		if( CCountry::CODE_USA == $strCountryCode ) {
			$strJoinCondition = 'JOIN employee_addresses ea ON ( e.id = ea.employee_id AND address_type_id = ' . CAddressType::PRIMARY . ' )
								LEFT JOIN departments d ON( d.id = t.department_id )';
			$strSubSql .= ' AND ea.country_code = \'' . $strCountryCode . '\'
							AND d.id = ' . CDepartment::CALL_CENTER;
		}

		$strSql = '	SELECT
						t.*
					FROM
						teams t
						LEFT JOIN employees e ON ( t.manager_employee_id = e.id )
						' . $strJoinCondition . '
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( e.date_started IS NULL OR e.date_terminated IS NULL OR NOW() < e.date_terminated )
						AND t.deleted_on IS NULL' . $strSubSql . '
					ORDER BY
						t.name,
						t.id';
		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchTeamsByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $strOrderBy = 'name', $boolUpdateTeam = false ) {
		if( true == valStr( $strOrderBy ) ) {
			$strOrderBy = $strOrderBy;
		}

		$strWhereClause = ( true == $boolUpdateTeam ) ? ' AND t.manager_employee_id = ' . ( int ) $intManagerEmployeeId : ' AND e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId;

		$strSql = 'SELECT 
						DISTINCT t.*
					FROM 
						teams t
						LEFT JOIN team_employees te ON( te.team_id = t.id AND te.is_primary_team =1 )
						JOIN employees e ON ( te.employee_id = e.id )
					WHERE
						t.deleted_by IS NULL
						' . $strWhereClause . ' ORDER BY ' . $strOrderBy;
		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchTeamsOfTeamEmployeesByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $boolPrimaryTeam = NULL, $arrstrDepartmentIds = [ CDepartment::DEVELOPMENT, CDepartment::CREATIVE, CDepartment::QA ] ) {

		$strCondition = ( false == is_null( $boolPrimaryTeam ) ) ? ' AND te.is_primary_team = ' . $boolPrimaryTeam : '';

		$strSql = 'SELECT DISTINCT
						t.id,
						t.name,
						t.manager_employee_id,
						e.name_first,
						e.name_last,
						e.preferred_name
					FROM
						teams AS team_manager
						JOIN employees e ON ( team_manager.manager_employee_id = e.reporting_manager_id )
						JOIN team_employees AS te ON ( te.employee_id = e.id )
						JOIN teams AS t ON ( te.employee_id = t.manager_employee_id )
					WHERE
						team_manager.id IN ( SELECT id FROM teams WHERE manager_employee_id = ' . ( int ) $intManagerEmployeeId . ' )
						AND e.employee_status_type_id = 1
						AND e.department_id IN ( ' . implode( ',', $arrstrDepartmentIds ) . ' )
						' . $strCondition . '
					ORDER BY
						e.name_first';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTeamsWithTeamEmployeeDetailsByManagerEmployeeIds( $intPageNo, $intPageSize, $arrintManagerEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintManagerEmployeeIds ) ) {
			return false;
		}

		$intOffset	= ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;

		$strSql = 'SELECT
						t.id,
						t.name,
						t.manager_employee_id,
						t.department_id,
						t.sdm_employee_id,
						t.hr_representative_employee_id,
						count(DISTINCT te.id) as my_team_members,
						count(DISTINCT tm.id) as my_teams,
						array_to_string(array_agg( DISTINCT tm.manager_employee_id), \', \' ) as my_team_member_ids
					FROM
						teams t
						LEFT JOIN team_employees te ON  ( t.id = te.team_id AND te.is_primary_team = 1 )
						LEFT JOIN teams tm ON tm.manager_employee_id = te.employee_id
						LEFT JOIN employees e ON ( e.id = te.employee_id AND e.employee_status_type_id = 1 )
					WHERE
						t.manager_employee_id IN (' . implode( ',', $arrintManagerEmployeeIds ) . ')
						AND ( t.deleted_by IS NULL OR tm.deleted_by IS NULL )
					GROUP BY
						t.id,
						t.name,
						t.manager_employee_id,
						t.sdm_employee_id,
						t.hr_representative_employee_id,
						t.department_id
					ORDER BY
						t.name ASC
					OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . ( int ) $intPageSize;

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchAllTeamsByEmployees( $objDatabase ) {

		$strSql = 'SELECT
						t.*,
						u.id,
						te.employee_id
					FROM
						teams t
						LEFT JOIN team_employees te ON ( t.id = te.team_id )
						LEFT JOIN users u ON ( u.employee_id = te.employee_id )
					WHERE
						te.is_primary_team = 1
					ORDER BY
						t.name';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchTeamsByManagerEmployeeIds( $arrintManagerEmployeeIds, $objDatabase, $intEmployeeStatusTypeId = CEmployeeStatusType::CURRENT ) {

		if( false == valArr( $arrintManagerEmployeeIds ) ) {
			 return false;
		}

		$strCondition = ( true == valId( $intEmployeeStatusTypeId ) ) ? ' AND e.employee_status_type_id = ' . ( int ) $intEmployeeStatusTypeId : NULL;

		$strSql = 'SELECT
						DISTINCT 
						t.id,
						e.reporting_manager_id as manager_employee_id,
						t.sdm_employee_id,
						t.add_employee_id,
						t.tpm_employee_id,
						t.vdba_employee_id,
						t.hr_representative_employee_id,
						t.team_type_id,
						t.name,
						t.ps_product_id,
						t.ps_product_option_id,
						t.updated_by,
						t.updated_on,
						t.created_by,
						t.created_on
					FROM
						team_employees te
						JOIN teams t ON ( t.id = te.team_id AND te.is_primary_team = 1 )
						JOIN employees e ON ( e.id = te.employee_id ' . $strCondition . ' )
					WHERE
						e.reporting_manager_id IN ( ' . sqlIntImplode( $arrintManagerEmployeeIds ) . ' )
						AND t.deleted_by IS NULL
					ORDER BY
						t.name';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchTeamsByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = '	SELECT *
					FROM teams
					WHERE id IN (
							SELECT DISTINCT (te.team_id)
							FROM team_employees te
								JOIN employees e ON (e.id = te.employee_id)
							WHERE te.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
								AND te.is_primary_team = 1
								AND e.employee_status_type_id = 1
								)
						AND deleted_on IS NULL
					ORDER BY name';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	// Fetching all teams of particular SDM

	public static function fetchAllTeamsByProjectManagerEmployeeIds( $arrintProjectManagerEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintProjectManagerEmployeeIds ) ) return NULL;

		$strSql = '	SELECT
						DISTINCT ON ( ts.id, ts.name ) ts.id,
						ts.manager_employee_id,
						u.id AS user_id,
						ts.name
					FROM
						employees e
						JOIN teams t ON e.id = t.manager_employee_id
						JOIN employees e1 ON ( e1.reporting_manager_id = t.manager_employee_id )
						JOIN teams ts ON ts.manager_employee_id = e1.id
						JOIN users u ON u.employee_id = ts.manager_employee_id
					WHERE
						e.reporting_manager_id IN (' . implode( ',', $arrintProjectManagerEmployeeIds ) . ')
						AND t.deleted_by IS NULL
					ORDER BY
						ts.name';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchTeamsByEmployeeManagerIds( $arrintEmployeeManagerIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeManagerIds ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT t.*
					FROM
						teams t
						JOIN team_employees te ON( te.team_id = t.id )
						JOIN employees e ON ( e.id = te.employee_id )
					WHERE
						e.reporting_manager_id IN ( ' . implode( ', ', $arrintEmployeeManagerIds ) . ' )
						AND t.deleted_by IS NULL
					ORDER BY
						t.name';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchAssignedTeamsByTestId( $intTestId, $objDatabase ) {

		$strSql = 'SELECT
						t.*
					FROM
						teams t,
						test_groups et
					WHERE
						t.id = et.team_id
						AND et.test_id::integer = ' . ( int ) $intTestId . '
					ORDER BY
						lower ( t.name )';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchUnAssignedTeamsByTestId( $intTestId, $objDatabase, $arrintManagerIds = NULL ) {

		if( false == empty( $arrintManagerIds ) ) {
			$strWhere = 'e.reporting_manager_id IN ( ' . implode( ', ', $arrintManagerIds ) . ' ) AND t.deleted_by IS NULL AND ';
		} else {
			$strWhere = '';
		}

		$strSql = ' SELECT
						t.*
					FROM
						teams t
						JOIN team_employees te ON( te.team_id = t.id )
						JOIN employees e ON ( e.id = te.employee_id )
					WHERE ' . $strWhere . '
						 t.id NOT IN (
										SELECT
											DISTINCT t.id
										FROM
											teams t
											JOIN test_groups tg ON ( t.id = tg.team_id )
										WHERE
											tg.test_id = ' . ( int ) $intTestId .
									' )
					ORDER BY
						t.name';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchPrimaryTeamsByHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId, $objDatabase ) {

		$strSql = '	SELECT
						t.*
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id )
					WHERE
						t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
						AND te.is_primary_team = 1
					ORDER BY
						t.name';
		return self:: fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchAllHrRepresentatives( $intEmployeeId = NULL, $objDatabase, $strCountryCode = NULL, $boolSuperUser = false ) {

		$strSubSql = '';

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		if( false == is_null( $intEmployeeId ) ) {
			$strSubSql = 'AND e.id <> ' . ( int ) $intEmployeeId;
		}

		if( CCountry::CODE_USA == $strCountryCode ) {
			$strSubSql .= ' AND t.department_id IS NOT NULL';
		}

		if( false == $boolSuperUser ) {
			$strWhere = ' AND ea.country_code = \'' . $strCountryCode . '\'';
		}

		$strSql = 'SELECT
						DISTINCT ( t.hr_representative_employee_id ),
						e.preferred_name,
						e.name_full
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id AND te.is_primary_team = 1 )
						JOIN employees e ON ( t.hr_representative_employee_id = e.id )
						JOIN designations d on (e.designation_id = d.id)
						JOIN employee_addresses ea ON ( ea.employee_id = e.id )
					WHERE
						t.hr_representative_employee_id IS NOT NULL
						AND e.date_terminated is null '
						. $strSubSql . $strWhere . '
					ORDER BY
						e.name_full ASC ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamsByHrRepresentativeEmployeeIdOrderByName( $intHrRepresentativeEmployeeId, $objDatabase ) {

		$arrintHrRepresentativeEmployeeIds = explode( ',', $intHrRepresentativeEmployeeId );

		if( true == valArr( $arrintHrRepresentativeEmployeeIds ) ) {
			$strSql = 'SELECT * FROM teams WHERE deleted_by IS NULL AND hr_representative_employee_id IN (' . implode( ',', $arrintHrRepresentativeEmployeeIds ) . ') ORDER BY name';
		}

		return self:: fetchTeams( $strSql, $objDatabase );

	}

	public static function fetchTeamsMangersByTeamIds( $arrintTeamIds, $objDatabase, $boolHonourBadgeManager = false ) {

		$strCondition = ( true == $boolHonourBadgeManager ) ? ' AND e.id = t.manager_employee_id' : '';

		$strSql = 'SELECT t.id AS team_id,
						e.id as employee_id,
						e.name_full AS manager_name_full,
						t.manager_employee_id
					FROM teams t
						JOIN team_employees te ON ( te.team_id = t.id )
						JOIN employees e1 ON ( te.employee_id = e1.id )
						LEFT JOIN employees e ON ( e1.reporting_manager_id = e.id )
					WHERE t.id IN (' . implode( ',', $arrintTeamIds ) . ') AND e.department_id <> ' . CDepartment::QA . $strCondition;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchAllTeamsExceptManagerEmployeeId( $intManagerEmployeeId = NULL, $objAdminDatabase ) {

		$strWhere 	= 'WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( e.date_terminated IS NULL OR NOW() < e.date_terminated )
						AND t.deleted_on IS NULL';

		$strOrderBy = 'ORDER BY name, id';

		if( true == is_numeric( $intManagerEmployeeId ) ) {
			$strWhere .= ' AND t.manager_employee_id != \'' . ( int ) $intManagerEmployeeId . '\' ';
		}

		$strSql = '	SELECT
						t.*
					FROM
						teams t
						LEFT JOIN employees e ON ( t.manager_employee_id = e.id )' . ' ' . $strWhere . ' ' . $strOrderBy;

		return self::fetchTeams( $strSql, $objAdminDatabase );
	}

	public static function fetchTeamsBySearchFilters( $arrstrFilteredExplodedSearch, $objAdminDatabase ) {

		$strSql = ' SELECT
						t.id,
						t.name,
						t.manager_employee_id,
						count ( DISTINCT tm.id ) AS my_teams
					FROM
						teams t
						LEFT JOIN team_employees te ON t.id = te.team_id
						LEFT JOIN teams tm ON tm.manager_employee_id = te.employee_id
						LEFT JOIN employees e ON ( t.manager_employee_id = e.id )
					WHERE
						t.deleted_on IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( t.name ILIKE \'%' . implode( '%\' AND t.name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_first ILIKE \'%' . implode( '%\' AND e.name_first ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_last ILIKE \'%' . implode( '%\' AND e.name_last ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\'
						OR e.preferred_name ILIKE \'%' . implode( '%\' AND e.preferred_name ILIKE \'%', $arrstrFilteredExplodedSearch ) . '%\' )
					GROUP BY
						t.id,
						t.name,
						t.manager_employee_id
					LIMIT
						10 ';

		return self::fetchTeams( $strSql, $objAdminDatabase );
	}

	protected static function getSearchCriteria( $objTeamsFilter ) {

		$strWhereCondition = ' WHERE e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
								AND ( e.date_terminated IS NULL OR NOW() < e.date_terminated )
								AND t.deleted_by IS NULL ';

		if( false == valObj( $objTeamsFilter, 'CTeamsFilter' ) ) {
			return $strWhereCondition;
		}

		$strWhereCondition .= ( true == valStr( $objTeamsFilter->getTeamName() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' t.name ILIKE \'%' . $objTeamsFilter->getTeamName() . '%\'' : '';
		$strWhereCondition .= ( true == valStr( $objTeamsFilter->getManagerEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' t.manager_employee_id = ' . $objTeamsFilter->getManagerEmployeeId() : '';
		$strWhereCondition .= ( true == valStr( $objTeamsFilter->getSdmEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' t.sdm_employee_id = ' . $objTeamsFilter->getSdmEmployeeId() : '';
		$strWhereCondition .= ( true == valStr( $objTeamsFilter->getHrRepresentativeEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' t.hr_representative_employee_id = ' . $objTeamsFilter->getHrRepresentativeEmployeeId() : '';
		$strWhereCondition .= ( true == valStr( $objTeamsFilter->getDepartmentId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' t.department_id = ' . $objTeamsFilter->getDepartmentId() : '';
		$strWhereCondition .= ( true == valStr( $objTeamsFilter->getPsProductId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ( 0 == \Psi\CStringService::singleton()->strcasecmp( 'ps_product_option', $objTeamsFilter->getProductField() ) ? ' t.ps_product_option_id = ' . $objTeamsFilter->getPsProductId() : ' t.ps_product_id = ' . $objTeamsFilter->getPsProductId() ) : '';
		$strWhereCondition .= ( true == valStr( $objTeamsFilter->getCountryCode() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . 'ead.country_code = \'' . $objTeamsFilter->getCountryCode() . '\'' : '';
		$strWhereCondition .= ( true == valStr( $objTeamsFilter->getTeamsWithoutProduct() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . 'ead.country_code = \'' . CCountry::CODE_INDIA . '\' AND t.ps_product_id IS NULL AND t.ps_product_option_id IS NULL AND t.deleted_by IS NULL' : '';

		return $strWhereCondition;
	}

	public static function fetchEmployeeDetailsWithReportingMangerId( $objDatabase ) {

		$strSql = 'SELECT DISTINCT (e.id) as employee_id,
									e.reporting_manager_id as parent_id,
									t.id as team_id,
									dept.name as department,
									desg.name as designation,
									e.email_address as email_id,
									e.name_full as name_full
							FROM teams t
								LEFT JOIN team_employees te on (t.id = te.team_id)
								LEFT JOIN employees e ON (e.id = te.employee_id AND te.is_primary_team = 1)
								OR (e.id = e.reporting_manager_id AND e.id NOT IN (
																					SELECT DISTINCT (
																						employee_id)
																					FROM team_employees
								))
								LEFT JOIN designations desg ON (desg.id = e.designation_id)
								LEFT JOIN departments dept ON (dept.id = e.department_id)
							WHERE e.date_terminated IS NULL AND
									e.employee_status_type_id = 1 AND
									e.id IS NOT NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamsByDepartmentIdByCountryCode( $intDepartmentId, $strCountryCode, $objDatabase ) {

		if( false == is_numeric( $intDepartmentId ) ) return NULL;

		$strSql = 'SELECT
						t.*
					FROM
						teams t
						LEFT JOIN employee_addresses ea ON ( t.manager_employee_id = ea.employee_id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
						LEFT JOIN employees e ON ( t.manager_employee_id = e.id )
					WHERE
						t.department_id = ' . ( int ) $intDepartmentId . '
						AND ea.country_code = \'' . $strCountryCode . '\'
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND ( e.date_started IS NULL
						OR e.date_terminated IS NULL
						OR NOW() < e.date_terminated )
						AND t.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDepartmentsByHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId, $objDatabase, $strCountryCode = NULL ) {

		if( false == valStr( $strCountryCode ) ) {
			$strCountryCode = CCountry::CODE_INDIA;
		}

		$strSql = 'SELECT
						d.id,
						d.name
					FROM
						teams t
						LEFT JOIN employee_addresses ea ON ( t.manager_employee_id = ea.employee_id )
						LEFT JOIN departments d ON ( d.id = t.department_id )
					WHERE
						ea.country_code = \'' . $strCountryCode . '\'
						AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . '
					GROUP BY
						d.id,
						d.name
					ORDER BY
						d.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchMyTeamData( $arrintUserIds, $boolShowSeverityDetails = false, $boolShowTeamAverage = false, $boolSearchByEmployeeId = false, $boolShowGraph = false, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) ) return NULL;

		if( true == $boolShowGraph ) {
			$strTimeInterval = '11 months';
		} else {
			$strTimeInterval = '90 days';
		}

		if( true == $boolSearchByEmployeeId ) {
			$strSearchByCondition = 'employee_id';
		} else {
			$strSearchByCondition = 'id';
		}

		if( true == $boolShowSeverityDetails ) {
			$strSql = '
					SELECT
						SUM( CASE WHEN t.dev_' . $strSearchByCondition . ' IN( ' . implode( ',', $arrintUserIds ) . ' ) THEN t.severity_count ELSE 0 END ) AS severity_count,
						ROUND( SUM( t.severity_count ) / COUNT( DISTINCT t.dev_' . $strSearchByCondition . ' ), 2 ) AS avg_severity_count,
						' . \Psi\Libraries\UtilFunctions\count( $arrintUserIds ) . ' AS team_count';

			$strTaskTypeStatusCondition = ' AND t.task_status_id != ' . CTaskStatus::COMPLETED . '
											AND t.task_type_id = ' . CTaskType::BUG . '
											AND td.severity_count > 0 ';

			$strTaskCondition = 't.updated_on';
		} else {
			$strSql = '
					SELECT
						SUM( CASE WHEN t.dev_' . $strSearchByCondition . ' IN( ' . implode( ',', $arrintUserIds ) . ' ) THEN t.dev_story_points ELSE 0 END ) AS dev_story_points,
						ROUND( SUM( t.dev_story_points ) / COUNT( DISTINCT t.dev_' . $strSearchByCondition . ' ), 2 ) AS avg_dev_story_points,
						COUNT( DISTINCT t.dev_' . $strSearchByCondition . ' ) AS dev_count,

						' . \Psi\Libraries\UtilFunctions\count( $arrintUserIds ) . ' AS team_count';

			$strTaskTypeStatusCondition = ' AND t.task_status_id = ' . CTaskStatus::COMPLETED . '
											AND t.task_type_id IN ( ' . CTaskType::BUG . ', ' . CTaskType::FEATURE . ', ' . CTaskType::AUTOMATION . ', ' . CTaskType::TEST_CASES . ', ' . CTaskType::DESIGN . ', ' . CTaskType::QUESTION_RESEARCH . ' )
											AND ( td.developer_story_points > 0 ) ';

			$strTaskCondition = 't.completed_on';
		}

		if( false == $boolShowTeamAverage ) {
			$strSql .= ',
						EXTRACT ( MONTH FROM ' . $strTaskCondition . ' ) team_data_month,
						EXTRACT ( YEAR FROM ' . $strTaskCondition . ' ) team_data_year ';
		}

		$strSql .= '
					FROM
						(
							SELECT
								e_dev.id AS dev_employee_id,
								u_dev.id AS dev_id,
								td.developer_story_points AS dev_story_points,
								td.severity_count,
								t.updated_on,
								t.completed_on
							FROM tasks t
								 JOIN task_details td ON t.id IS NOT NULL AND t.id = td.task_id
								 LEFT JOIN employees e_dev ON e_dev.id = td.developer_employee_id
								 LEFT JOIN users u_dev ON u_dev.employee_id = e_dev.id
							WHERE
								( td.developer_employee_id IS NOT NULL )
								AND ' . $strTaskCondition . ' >= DATE ( NOW () - INTERVAL \'' . $strTimeInterval . '\' )
								AND ( u_dev.' . $strSearchByCondition . ' IS NOT NULL )
								' . $strTaskTypeStatusCondition . '
						) t';

		if( false == $boolShowTeamAverage ) {
			$strSql .= ' GROUP BY
							team_data_month,
							team_data_year';
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamsByDepartmentIds( $arrintDepartmentIds, $objDatabase ) {
		if( false == valArr( $arrintDepartmentIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						teams
					WHERE
						deleted_by IS NULL
						AND department_id IN ( ' . implode( ',', array_filter( $arrintDepartmentIds ) ) . ' )
					ORDER BY
						name';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchSimplePrimaryTeamsByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {
		if( false == valArr( $arrintEmployeeIds ) ) return NULL;

		$strSql = 'SELECT
						t.*,
						te.employee_id,
						e.name_full,
						e.preferred_name
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id )
						JOIN employees e ON ( te.employee_id = e.id )
					WHERE
						te.is_primary_team = 1
						AND e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )
						AND t.deleted_by IS NULL
					ORDER BY
						t.name, e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamsDataByManagerEmployeeId( $intManagerEmployeeId, $objDatabase, $arrintExcludeTeamIds = NULL, $boolCurrentEmployee = false ) {

		if( true == is_null( $intManagerEmployeeId ) ) return NULL;
		$strWhereCondition = ( false == is_null( $arrintExcludeTeamIds ) ) ? 'AND t.id NOT IN(' . implode( ',', $arrintExcludeTeamIds ) . ')' : '';

		if( $boolCurrentEmployee ) {
			$strWhere = ' AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;
		}

		$strSql = 'SELECT
						DISTINCT t.id,
						e.reporting_manager_id AS manager_employee_id,
						name AS team_name,
						t.hr_representative_employee_id
					FROM
						teams t
						JOIN team_employees te ON(te.team_id = t.id AND t.deleted_by IS NULL AND te.is_primary_team = 1 )
						JOIN employees e ON ( te.employee_id = e.id )
					WHERE
						e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
						AND deleted_by IS NULL ' . $strWhereCondition . $strWhere . '
					ORDER BY
						name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamsByManageresByCountryCode( $strCountryCode, $objAdminDatabase, $intTeamId = NULL ) {
		$strWhereCondition = '';
		if( true == empty( $strCountryCode ) ) return NULL;

		if( true == valId( $intTeamId ) ) {
			$strWhereCondition = 'AND ( t.id != ' . $intTeamId . ' AND t.id != ' . CTeam::NEW_JOINEE_US . ' )';
		}

		$strSql = ' SELECT
						t.*
					FROM
						teams t
						JOIN employees e ON ( t.manager_employee_id = e.id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ')
						JOIN employee_addresses ea ON ( ea.employee_id = e.id AND ea.address_type_id = ' . CAddressType::PRIMARY . ' )
					WHERE
						t.deleted_on IS NULL ' . $strWhereCondition . '
						AND ea.country_code = \'' . $strCountryCode . '\' 
					ORDER BY t.name';

		return self::fetchTeams( $strSql, $objAdminDatabase );
	}

	public static function fetchAllTeamDetails( $objDatabase ) {
		return self::fetchTeams( sprintf( 'SELECT * FROM teams ORDER BY name' ), $objDatabase );
	}

	public static function fetchTeamsByPsProductIdsByDepartmentIds( $arrintPsProductIds = NULL, $arrintDepartmentIds = NULL, $objDatabase ) {
		$strWhere = '';

		if( true == valArr( $arrintPsProductIds ) ) {
			$strWhere .= ' AND ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' )';
		}

		if( true == valArr( $arrintDepartmentIds ) ) {
			$strWhere .= ' AND e.department_id IN ( ' . implode( ',', $arrintDepartmentIds ) . ' ) ';
		}

		$strSql = ' SELECT
						Distinct ON (t.id ) t.*
					FROM
						teams t
						JOIN team_employees te ON( te.team_id = t.id AND te.is_primary_team = 1 )
						JOIN employees e ON( e.id = te.employee_id AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
					WHERE
						deleted_by IS NULL' . $strWhere;

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchSdmEmployeeByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) return false;

		$strSql = 'SELECT
						t.id,
						t.name,
						e.reporting_manager_id AS manager_employee_id,
						t.sdm_employee_id,
						t.hr_representative_employee_id
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id AND te.is_primary_team = 1 )
						JOIN employees e ON ( te.employee_id = e.id AND e.reporting_manager_id = ' . ( int ) $intEmployeeId . ' )
					WHERE
						t.deleted_on IS NULL';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchADDByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeId ) ) return false;

		$strSql = 'SELECT
 						 e1.id,
						 e1.name_full
					FROM
						teams t
						JOIN team_employees te ON (t.id = te.team_id AND te.is_primary_team =1)
						JOIN employees e ON ( e.id = te.employee_id)
						JOIN employees e1 ON ( e1.id = t.add_employee_id)
					WHERE
						e.id = ' . ( int ) $intEmployeeId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchADDByTeamId( $intTeamId, $objDatabase ) {

		if( false == is_numeric( $intTeamId ) ) return false;

		$strSql = 'SELECT
 						e.id,
						e.name_full
					FROM
						teams t
						JOIN team_employees te ON (t.id = te.team_id AND te.is_primary_team =1)
						JOIN employees e ON ( e.id = t.add_employee_id)
					WHERE
						t.id = ' . ( int ) $intTeamId;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamsByAddEmployeeIds( $arrintAddEmployeeId, $objDatabase ) {

		if( false == valArr( $arrintAddEmployeeId ) ) return false;

		$strSql = 'SELECT
 						*
					FROM
						teams
					WHERE
						add_employee_id IN ( ' . implode( ',', $arrintAddEmployeeId ) . ' )
						AND deleted_on IS NULL';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchPrimaryTeamsSdmEmployeeIdByUserIds( $arrintUserIds, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) ) return false;

		$strSql = ' SELECT
						u.id as user_id,
						t.sdm_employee_id
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id )
						JOIN users u ON ( te.employee_id = u.employee_id )
					WHERE
						te.is_primary_team = 1
						AND u.id IN ( ' . implode( ',', $arrintUserIds ) . ' )
						AND deleted_by IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPrimaryTeamByUserId( $intUserId, $objDatabase ) {
		$strSql = ' SELECT
						t.*,
						te.is_primary_team
					FROM
						teams t
						JOIN team_employees te ON ( t.id = te.team_id )
						JOIN users u ON ( te.employee_id = u.employee_id )
					WHERE
						te.is_primary_team = 1
						AND u.id = ' . ( int ) $intUserId . '
						AND t.id = te.team_id
						AND deleted_by IS NULL
					ORDER BY
						t.name ';

		return self::fetchTeam( $strSql, $objDatabase );
	}

	public static function fetchSdmDetailsByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = '	SELECT
						t.*,
						e.preferred_name as sdm_employee
					FROM
						teams t
						JOIN team_employees te ON (t.id = te.team_id AND te.employee_id = ' . ( int ) $intEmployeeId . ' AND te.is_primary_team = 1 )
						JOIN employees e on (t.sdm_employee_id = e.id)';

		return self::fetchTeam( $strSql, $objDatabase );
	}

	public static function fetchTeamEmployeesByManagerEmployeeId( $intManagerEmployeeId, $objAdminDatabase ) {

		$strSql = 'SELECT e.id AS employee_id,
							e.name_first as first_name,
							e.name_last as last_name,
							e.preferred_name AS employee_name,
							e.designation_id,
							d.name AS designation_name,
							t.name team_name,
							sqa.answer as answer,
							sqa.numeric_weight as rating,
							e1.preferred_name as rated_by,
							s.created_on,
							avg_query.average_rating,
							sub.avg_task_count,
							d.country_code,
							u.id as user_id
					FROM teams t
					JOIN team_employees te ON (t.id = te.team_id)
					JOIN employees e ON (te.employee_id = e.id AND e.date_terminated IS NULL
					AND te.is_primary_team = 1 AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ')
					LEFT JOIN users u ON( u.employee_id = e.id )
					LEFT JOIN designations d ON (e.designation_id = d.id)
					LEFT JOIN
					(
						SELECT *
						FROM surveys
						WHERE id IN (
									SELECT max(id)
									FROM surveys
									WHERE survey_template_id = ' . CSurveyTemplate::HELPDESK_SURVEY . '
									AND survey_type_id = ' . CSurveyType::TRAINING . '
									GROUP BY subject_reference_id
							)
						ORDER BY id DESC
					) s ON ( s.subject_reference_id = e.id )
					LEFT JOIN (
						SELECT 
							CEIL( COUNT( td.task_id )/12 ) AS avg_task_count,
							td.developer_employee_id 
						FROM task_details td
						WHERE td.created_on > date_trunc(\'month\', CURRENT_DATE) - INTERVAL \'1 year\'
						GROUP BY td.developer_employee_id
					) sub ON (sub.developer_employee_id = s.subject_reference_id)
					LEFT JOIN employees e1 ON ( e1.id = s.responder_reference_id )
					LEFT JOIN survey_question_answers sqa ON ( sqa.survey_id = s.id )
					LEFT JOIN ( SELECT avg(sqa1.numeric_weight) AS average_rating,
									max(sqa1.survey_id) AS survey_id,s1.subject_reference_id
								FROM survey_question_answers sqa1
								LEFT JOIN surveys s1 ON (sqa1.survey_id = s1.id)
								WHERE s1.survey_template_id=' . CSurveyTemplate::HELPDESK_SURVEY . '
								AND s1.survey_type_id = ' . CSurveyType::TRAINING . '
								GROUP BY s1.subject_reference_id) avg_query ON (avg_query.subject_reference_id = e.id )
					WHERE
						e.id = ' . ( int ) $intManagerEmployeeId . ' OR e.id IN ( 
						WITH RECURSIVE all_employees( employee_id ) AS
					(
						SELECT
							e.id AS employee_id
						FROM
							employees e
						WHERE
							e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
							AND e.id <> ' . ( int ) $intManagerEmployeeId . '
						UNION ALL
						SELECT
							e.id AS employee_id
						FROM
							employees e,
							all_employees al
						WHERE
							e.reporting_manager_id = al.employee_id
							AND e.id <> ' . ( int ) $intManagerEmployeeId . ' )
						SELECT
							all_employees.employee_id
						FROM
							all_employees
							JOIN employees e ON ( all_employees.employee_id = e.id )
						WHERE
						 e.id <> ' . ( int ) $intManagerEmployeeId . ' AND e.date_terminated IS NULL AND e.employee_status_type_id = ' . ( int ) CEmployeeStatusType::CURRENT . ' 
						)
					ORDER BY
						e.name_first ASC';

		$arrmixFetchedData = fetchData( $strSql, $objAdminDatabase );

		if( true == valArr( $arrmixFetchedData ) ) {
			foreach( $arrmixFetchedData as &$arrmixData ) {
				$arrmixData['answer'] = CSurvey::getDecryptedData( $arrmixData['answer'] );
			}
		}

		return $arrmixFetchedData;
	}

	public static function fetchPrimaryTeamsDetails( $objDatabase ) {

		$strSql = 'SELECT
						t.id,
						t.name,
						count(te.id) AS member_count
					FROM
						team_employees te
						JOIN employees e ON te.employee_id = e.id
						JOIN teams t on t.id=te.team_id
					WHERE
						e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND te.is_primary_team = 1
						AND t.deleted_by IS NULL
					GROUP BY
						t.id,
						t.name
					HAVING
						count ( te.id ) > 0';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchManagerByEmployeeIds( $arrintEmployeeIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeIds ) ) {
			return false;
		}

		$strSql = 'SELECT
						t.id as team_id,
						e.reporting_manager_id AS manager_employee_id,
						e.id AS employee_id,
						t.tpm_employee_id,
						t.qam_employee_id,
						t.add_employee_id,
						u.id,
						t.hr_representative_employee_id,
						u2.id AS manager_user_id,
						u3.id as qa_mentor_id,
						u1.id AS tpoc_user_id,
						e.preferred_name as employee_name,
						e.department_id,
						bu.training_representative_employee_id,
						(
							array( SELECT u.id
							FROM users u
							WHERE u.employee_id IN ( t.tpm_employee_id, t.qam_employee_id, t.hr_representative_employee_id, bu.training_representative_employee_id)
						)) AS user_ids
					FROM
						teams t
					JOIN
						team_employees te ON ( te.team_id = t.id AND te.is_primary_team = 1 )
						LEFT JOIN employees e ON( e.id = te.employee_id )
						LEFT JOIN business_units bu ON( t.hr_representative_employee_id = bu.bu_hr_employee_id AND t.add_employee_id = bu.director_employee_id )
						LEFT JOIN users u on( t.qam_employee_id = u.employee_id )
						LEFT JOIN users u2 ON( u2.employee_id = e.reporting_manager_id )
						LEFT JOIN users u3 ON( u3.employee_id = t.qa_employee_id )
						LEFT JOIN users u1 ON( u1.employee_id = bu.training_representative_employee_id )
					WHERE
						e.id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchTeamsManagerByDepartmentIdsByCountryCode( $arrintDepartmentIds, $objDatabase, $intHrRepresentativeEmployeeId = NULL, $strCountryCode = NULL, $strSearch = NULL ) {
		if( false == valArr( $arrintDepartmentIds ) ) return NULL;

		$strWhereCondition = '';
		if( true == valStr( $strSearch ) ) {
			$strWhereCondition .= ' AND lower( t.name ) like \'%' . \Psi\CStringService::singleton()->strtolower( trim( $strSearch ) ) . '%\'';
		}

		if( false == is_null( $strCountryCode ) ) {
			$strWhereCondition .= ' AND dg.country_code = \'' . $strCountryCode . '\'';
		}

		if( true == is_numeric( $intHrRepresentativeEmployeeId ) ) {
			$strWhereCondition .= ' AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId;
		}

		$strSql = 'SELECT
						DISTINCT t.*,
						e.preferred_name as manager_name
					FROM
						teams t
						JOIN team_employees te ON ( te.team_id = t.id AND te.is_primary_team =1 )
						JOIN employees e ON ( t.manager_employee_id = e.id )
						JOIN designations dg ON ( e.designation_id = dg.id AND dg.deleted_by IS NULL )
					WHERE
						t.deleted_by IS NULL
						AND e.department_id IN ( ' . implode( ',', array_filter( $arrintDepartmentIds ) ) . ' )'
						. $strWhereCondition .
					'ORDER BY
						t.name';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchAllQaManagers( $objDatabase ) {

		$strSql = 'SELECT
						t.qam_employee_id,
						e.preferred_name
					FROM
						teams t
						JOIN employees e on ( e.id = t.qam_employee_id )
					GROUP BY
						e.preferred_name,t.qam_employee_id';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchTeamsData( $objAdminDatabase ) {

		$strSelectClause	= ', e.preferred_name';
		$strJoinClause		= ' JOIN employees e ON ( t.manager_employee_id = e.id ) ';

		$strSql = 'SELECT
						t.id,
					 	t.name ' . $strSelectClause . '
					FROM
						teams t ' . $strJoinClause . '
					WHERE
						t.deleted_by IS NULL';

		return fetchData( $strSql, $objAdminDatabase );

	}

	public static function fetchHrRepresentativeByAddId( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
					 	DISTINCT t.hr_representative_employee_id
					FROM
						teams t
					WHERE
						t.add_employee_id = ' . ( int ) $intEmployeeId . '
						AND t.deleted_on IS NULL';
		$arrintHrRepresentativeId = fetchdata( $strSql, $objDatabase );
		return $arrintHrRepresentativeId[0];

	}

	public static function fetchSdmByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT pp.id,
						pp.name,
						pp.sdm_employee_id
					FROM ps_products pp
					WHERE ' . ( int ) $intEmployeeId . ' IN (tpm_employee_id, tl_employee_id, sdm_employee_id, doe_employee_id,product_manager_employee_id, business_analyst_employee_id, product_owner_employee_id, sqm_employee_id, qam_employee_id 
						)
					UNION ALL
					SELECT ppo.id,
						ppo.name,
						ppo.sdm_employee_id
					FROM ps_product_options ppo
					WHERE ' . ( int ) $intEmployeeId . ' IN (tpm_employee_id, tl_employee_id, sdm_employee_id, doe_employee_id,product_manager_employee_id, business_analyst_employee_id, product_owner_employee_id, sqm_employee_id, qam_employee_id
						)
					UNION ALL
					SELECT pp.id,
						pp.name,
						pp.sdm_employee_id
					FROM teams t
						LEFT JOIN team_employees te ON (te.team_id = t.id)
						LEFT JOIN ps_products pp ON (pp.id = t.ps_product_id)
					WHERE ( ' . ( int ) $intEmployeeId . ' IN ( t.sdm_employee_id, t.qam_employee_id, t.add_employee_id ) or te.employee_id = ' . ( int ) $intEmployeeId . ' ) and
						te.is_primary_team = 1
					UNION ALL
					SELECT ppo.id,
						ppo.name,
						ppo.sdm_employee_id
					FROM teams t
						LEFT JOIN team_employees te ON (te.team_id = t.id)
						LEFT JOIN ps_product_options ppo ON (ppo.id = t.ps_product_option_id)
					WHERE ( ' . ( int ) $intEmployeeId . ' IN ( t.sdm_employee_id, t.qam_employee_id, t.add_employee_id ) or te.employee_id = ' . ( int ) $intEmployeeId . ' ) and
						te.is_primary_team = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAddByHrRepresentativeEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
					 	DISTINCT t.add_employee_id
					FROM
						teams t
					WHERE
						t.hr_representative_employee_id = ' . ( int ) $intEmployeeId . '
						AND t.deleted_on IS NULL';
		$arrintHrRepresentativeId = fetchdata( $strSql, $objDatabase );

		return $arrintHrRepresentativeId;

	}

	public static function fetchTeamManagerAndTeamsByDepartmentIds( $arrintDepartmentIds, $objDatabase ) {
		if( false == valArr( $arrintDepartmentIds ) ) return NULL;

		$strSql = 'SELECT
						t.*,
						e.preferred_name
					FROM
						teams t
						JOIN employees e ON ( t.manager_employee_id = e.id )
					WHERE
						t.deleted_by IS NULL
						AND t.department_id IN ( ' . implode( ',', array_filter( $arrintDepartmentIds ) ) . ' )
					ORDER BY
						name';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchSdmIdsByTeamIds( $arrintTeamIds, $objDatabase ) {

		if( false == valArr( $arrintTeamIds ) ) {
			return false;
		}

		$strSql = 'SELECT 
						sdm_employee_id
					FROM 
						teams
					WHERE 
						id IN ( ' . implode( ',', $arrintTeamIds ) . ' )';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTeamDetailsByTeamIds( $arrintTeamIds, $objDatabase ) {
		if( false == valArr( $arrintTeamIds ) ) return NULL;

		$strSql = 'SELECT
					 	t.manager_employee_id,
					 	t.sdm_employee_id,
					 	t.tpm_employee_id,
					 	t.qam_employee_id,
					 	t.qa_employee_id
					FROM
						teams t
					WHERE
						t.id IN ( ' . implode( ',', array_filter( $arrintTeamIds ) ) . ')
						AND t.deleted_on IS NULL';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchRecursiveTeamsDetailsByManagerEmployeeId( $intManagerEmployeeId, $intDepartmentId, $objDatabase ) {

		if( false == is_numeric( $intManagerEmployeeId ) ) {
			return false;
		}
		if( false == is_numeric( $intDepartmentId ) ) {
			return false;
		}

		$strSql = 'WITH RECURSIVE all_employees( employee_id, manager_employee_id ) AS (
					SELECT
						DISTINCT e.id AS employee_id,
						e.reporting_manager_id AS manager_employee_id
					FROM
						employees e
					WHERE
				        ( e.reporting_manager_id = ' . $intManagerEmployeeId . ' OR e.id = ' . $intManagerEmployeeId . ' )
				        AND e.reporting_manager_id != e.id
				    UNION ALL
				    SELECT
				        DISTINCT e.id AS employee_id,
						e.reporting_manager_id AS manager_employee_id
				    FROM
				        employees e,
				        all_employees al
				    WHERE
				        e.reporting_manager_id = al.employee_id
				        AND e.reporting_manager_id != e.id
				)
				SELECT DISTINCT tt.id,al.employee_id,tt.name,e22.preferred_name
				FROM teams as tt 
				    JOIN employees as e22 ON ( tt.manager_employee_id = e22.id and e22.employee_status_type_id = 1 AND e22.department_id = ' . $intDepartmentId . ' )
				    JOIN all_employees as al ON ( tt.manager_employee_id =al.employee_id)
				WHERE tt.deleted_by is NULL';

		return CTeams::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchSdmDoeTeamsDetailsByEmployeeId( $intSdmEmployeeId, $objDatabase ) {
		if( false == is_numeric( $intSdmEmployeeId ) ) {
			return false;
		}

		$strSql = 'SELECT
						t.*, e.preferred_name
					FROM
						teams t
						JOIN employees e ON ( t.manager_employee_id = e.id )
					WHERE
						deleted_by IS NULL
						AND deleted_on IS NULL
						AND ( 
							ps_product_id IN ( 
								SELECT 
									id 
								FROM ps_products 
								WHERE ' . ( int ) $intSdmEmployeeId . ' IN ( sdm_employee_id, doe_employee_id ) 
							)
							OR ps_product_option_id IN ( 
								SELECT id 
								FROM ps_product_options 
								WHERE ' . ( int ) $intSdmEmployeeId . ' IN ( sdm_employee_id, doe_employee_id ) 
							) 
						)
					';

		return CTeams::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchAddDetailsByHrRepresentativeEmployeeId( $intHrRepresentativeEmployeeId = NULL, $strCountryCode= NULL, $objDatabase ) {

		$strWhere = '';

		if( true == valStr( $strCountryCode ) ) {
			$strWhere = ' ea.country_code = \'' . $strCountryCode . '\' AND ';
		}
		if( false == is_null( $intHrRepresentativeEmployeeId ) ) {
			$strWhere .= 't.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . ' AND ';
		}
		$strSql = 'SELECT
					 	DISTINCT t.add_employee_id,
					 	e.preferred_name
					FROM
						teams t 
						JOIN employees e ON ( e.id = t.add_employee_id )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id )
					WHERE
						' . $strWhere . '
						t.deleted_on IS NULL
						ORDER BY e.preferred_name';
		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchTpmDetailsByHrRepresentativeEmployeeIdByAddEmployeeId( $intHrRepresentativeEmployeeId = NULL, $intAddEmployeeId = NULL, $strCountryCode= NULL, $objDatabase ) {
		$strWhere = '';

		if( true == valStr( $strCountryCode ) ) {
			$strWhere = ' AND ea.country_code = \'' . $strCountryCode . '\'';
		}

		if( true == is_numeric( $intHrRepresentativeEmployeeId ) && true == is_numeric( $intAddEmployeeId ) ) {
			$strWhere .= 'AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId . ' AND t.add_employee_id = ' . ( int ) $intAddEmployeeId;
		} elseif( true == is_numeric( $intHrRepresentativeEmployeeId ) ) {
			$strWhere .= 'AND t.hr_representative_employee_id = ' . ( int ) $intHrRepresentativeEmployeeId;
		} elseif( true == is_numeric( $intAddEmployeeId ) ) {
			$strWhere .= 'AND t.add_employee_id = ' . ( int ) $intAddEmployeeId;
		}
		$strSql = 'SELECT
					 	DISTINCT t.tpm_employee_id,
					 	e.preferred_name
					FROM
						teams t 
						JOIN team_employees te ON ( t.id = te.team_id AND te.is_primary_team = 1 )
						JOIN employees e ON ( t.tpm_employee_id = e.id )
						JOIN employee_addresses ea ON ( ea.employee_id = e.id )
					WHERE
						t.tpm_employee_id IS NOT NULL
						AND e.date_terminated is null ' . $strWhere . '
						ORDER BY e.preferred_name';

		return fetchdata( $strSql, $objDatabase );
	}

	public static function fetchPrimaryManagersByManagerEmployeeId( $intEmployeeId, $objAdminDatabase, $arrintDevQaEmployeeIds = NULL ) {
		if( false == valId( $intEmployeeId ) ) {
			return NULL;
		}

		if( true == valArr( $arrintDevQaEmployeeIds ) ) {
			$strWhere = ' AND ( e.reporting_manager_id = ' . ( int ) $intEmployeeId . ' OR e.id IN ( ' . implode( ',', $arrintDevQaEmployeeIds ) . ' ) )';
		} else {
			$strWhere = ' AND e.reporting_manager_id = ' . ( int ) $intEmployeeId;
		}

		$strSql = 'SELECT
						DISTINCT e.id AS employee_id,
						e.preferred_name,
						e.designation_id
					FROM
						employees e
					WHERE
						e.department_id IN( ' . implode( ',', CDepartment::$c_arrintSprintDepartmentIds ) . ' )
						AND e.employee_status_type_id = 1
						 ' . $strWhere . '
					ORDER BY e.preferred_name';

		return fetchdata( $strSql, $objAdminDatabase );
	}

	public static function fetchTrainingRepresentativeByUserId( $intUserId, $objDatabase ) {

		$strSql = 'SELECT e.id,
					e.preferred_name,
					e.email_address
				FROM teams t
					 JOIN team_employees te ON (t.id = te.team_id)
					 JOIN users u ON (u.employee_id = te.employee_id)
					 JOIN business_units bu ON ( t.hr_representative_employee_id = bu.bu_hr_employee_id )
					 JOIN employees e ON ( bu.training_representative_employee_id = e.id )
				WHERE
					u.id = ' . ( int ) $intUserId . '
					AND te.is_primary_team = 1';

		return fetchdata( $strSql, $objDatabase );
	}

	public static  function fetchPlannersStakeholdersByTeamIds( $arrintTeamids, $objAdminDatabase ) {

		if( false == valArr( $arrintTeamids ) ) {
			return false;
		}

		$strSql = 'SELECT 
						array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppp.sdm_employee_id IS NOT NULL) THEN ppp.sdm_employee_id::text || \',\' || pp.sdm_employee_id::text ELSE pp.sdm_employee_id::text END,\',\'), \',\') ) AS sdm_employee_ids,
						array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppp.sqm_employee_id IS NOT NULL) THEN ppp.sqm_employee_id::text || \',\' || pp.sqm_employee_id::text ELSE pp.sqm_employee_id::text END,\',\'), \',\') ) AS sqm_employee_ids,
						array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppp.doe_employee_id IS NOT NULL) THEN ppp.doe_employee_id::text || \',\' || pp.doe_employee_id::text ELSE pp.doe_employee_id::text END,\',\'), \',\') ) AS doe_employee_ids,
						array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppp.product_manager_employee_id IS NOT NULL) THEN ppp.product_manager_employee_id::text || \',\' || pp.product_manager_employee_id::text ELSE pp.product_manager_employee_id::text END,\',\'), \',\') ) AS product_manager_employee_ids,
						array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppp.product_owner_employee_id IS NOT NULL) THEN ppp.product_owner_employee_id::text || \',\' || pp.product_owner_employee_id::text ELSE pp.product_owner_employee_id::text END,\',\'), \',\') ) AS product_owner_employee_ids
					FROM 
						teams AS t
					JOIN ps_products AS pp ON ( t.ps_product_id = pp.id AND pp.deleted_by IS NULL AND t.deleted_by IS NULL AND t.id IN (  ' . implode( ',', $arrintTeamids ) . ' ) )
					LEFT JOIN ps_product_options AS ppp ON ( t.ps_product_option_id = ppp.id AND ppp.is_published = 1 )';
		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchTeamsByPsProductIdsByPsProductOptionIds( $arrintPsProductIds, $arrintPsProductOptionIds, $objDatabase ) {
		$strSql = ' SELECT
						*
					FROM
						teams
					WHERE
						ps_product_id IN ( ' . $arrintPsProductIds . ' )
						OR ps_product_option_id IN ( ' . $arrintPsProductOptionIds . ' )';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchTeamsByQaEmployeeId( $intQaEmployeeId, $objDatabase ) {
		return parent::fetchTeams( sprintf( 'SELECT * FROM teams WHERE deleted_by IS NULL AND qa_employee_id = %d', ( int ) $intQaEmployeeId ), $objDatabase );
	}

	public static function fetchTeamByTeamIdAndEmployeeId( $intTeamId, $intEmployeeId, $objDatabase ) {

		if( false == valId( $intTeamId ) || false == valId( $intEmployeeId ) ) {
			return NULL;
		}

		$strSql = ' SELECT
						t.*,
						e.reporting_manager_id AS manager_employee_id
					FROM
						teams AS t 
						JOIN team_employees AS te ON ( t.id = te.team_id AND te.is_primary_team = 1 )
						JOIN employees e on ( e.id = te.employee_id )
					WHERE
						t.id = ' . ( int ) $intTeamId . '
						AND te.employee_id = ' . ( int ) $intEmployeeId . ' LIMIT 1';

		return self::fetchTeam( $strSql, $objDatabase );
	}

	public static  function fetchRecursiveDevelopmentTeamsByQAManagerEmployeeId( $intQAManagerEmployeeId, $objAdminDatabase ) {

		if( false == valId( $intQAManagerEmployeeId ) ) {
			return NULL;
		}

		$strSql = 'WITH RECURSIVE all_employees( employee_id ) AS (
					SELECT
						e.id AS employee_id
					FROM
						employees e
					WHERE
						( e.reporting_manager_id = ' . ( int ) $intQAManagerEmployeeId . '  )
					UNION ALL
					SELECT
						e.id AS employee_id
					FROM
						employees e
						JOIN all_employees al ON ( e.reporting_manager_id = al.employee_id )
					)

					SELECT
						DISTINCT 
						t2.id,
						t2.name,
						e4.preferred_name
					FROM
						all_employees al
						JOIN team_employees AS te2 ON ( al.employee_id = te2.employee_id AND te2.is_primary_team = 1 )
						JOIN teams AS t2 ON ( te2.team_id = t2.id AND t2.deleted_by IS NULL) 
						JOIN employees AS e3 ON ( al.employee_id = e3.id AND e3.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
						JOIN employees AS e4 ON ( t2.manager_employee_id = e4.id AND e4.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . ' )
					ORDER BY
						t2.name ASC';

		return self::fetchTeams( $strSql, $objAdminDatabase );
	}

	public static function fetchBacklogTeamsByPsProductIdsByPsProductOptionIds( $arrintPsProductIds, $arrintPsProductOptionIds, $objDatabase ) {

		$strWhere = '';

		if( true == valArr( $arrintPsProductIds ) ) {
			$strWhere .= '( t.ps_product_id IN ( ' . implode( ',', $arrintPsProductIds ) . ' ) AND t.deleted_by IS NULL )';
		}

		if( true == valArr( $arrintPsProductOptionIds ) && true == valStr( $strWhere ) ) {
			$strWhere .= ' OR ( t.ps_product_option_id IN ( ' . implode( ',', $arrintPsProductOptionIds ) . ' ) AND t.deleted_by IS NULL )';
		}

		if( true == valArr( $arrintPsProductOptionIds ) && false == valStr( $strWhere ) ) {
			$strWhere .= ' ( t.ps_product_option_id IN ( ' . implode( ',', $arrintPsProductOptionIds ) . ' ) AND t.deleted_by IS NULL )';
		}
		$strSql = ' SELECT
						t.*,
						e.preferred_name
					FROM
						teams AS t
						JOIN employees AS e ON ( t.manager_employee_id = e.id )
					WHERE
						' . $strWhere;

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchBacklogTeams( $arrintTeamId = NULL, $objDatabase ) {

		$strWhere = '';

		if( true == valArr( $arrintTeamId ) ) {
			$strWhere = ' AND t.id IN ( ' . implode( ',', $arrintTeamId ) . ' ) ';
		}

		$strSql = 'SELECT
						t.*,
						e.preferred_name
					FROM
						teams t
						JOIN employees e ON ( t.manager_employee_id = e.id )
					WHERE
						t.deleted_by IS NULL
						' . $strWhere . '
					ORDER BY
						name';

		return self::fetchTeams( $strSql, $objDatabase );
	}

	public static function fetchTeamInfoByUserId( $intUserId, $objDatabase ) {

		$strSql = 'SELECT 
						t.qam_employee_id, 
						t.qa_employee_id,
						t.tpm_employee_id,
						t.manager_employee_id
					FROM
						team_employees te 
					JOIN users u on( u.employee_id = te.employee_id )
					JOIN teams t on(t.id = te.team_id )
					WHERE 
						u.id = ' . ( int ) $intUserId . '  ORDER BY t.updated_on DESC LIMIT 1';
		return self::fetchTeam( $strSql, $objDatabase );
	}

	public static function fetchRecursiveTeamsByManagerEmployeeId( $intManagerEmployeeId, $objAdminDatabase, $intTeamId = NULL, $boolJobPosting = false ) {
		$strWhereCondition = '';
		$strJoin = '';

		if( true == $boolJobPosting ) {
			$strJoin = ' JOIN resource_requisitions rr ON (rr.manager_employee_id = t.manager_employee_id )
	                     JOIN purchase_requests pr ON (pr.id = rr.purchase_request_id and pr.team_id = t.id )
	                     JOIN job_posting_purchase_requests jp ON ( jp.purchase_request_id = rr.purchase_request_id )
	                     JOIN ps_website_job_postings pwjp on( jp.ps_job_posting_id = pwjp.id )
	                     JOIN employee_applications ea ON ( ea.ps_website_job_posting_id = pwjp.id )
		                 WHERE pwjp.is_published = 1 AND
		                       pwjp.country_code = \'' . CCountry::CODE_USA . '\' AND
		                       pwjp.deleted_by IS NULL AND
		                       pr.approved_on IS NOT NULL AND
		                       ea.employee_id IS NULL AND
		                       pr.approver_employee_id IS NOT NULL AND 
		                       pr.approver_employee_id IS NOT NULL ';
		}

		if( false == valId( $intManagerEmployeeId ) ) {
			return false;
		}

		if( true == valId( $intTeamId ) ) {
			$strWhereCondition = ' Where ( te.team_id != ' . $intTeamId . ' AND te.team_id != ' . CTeam::NEW_JOINEE_US . ' ) ';
		}

		$strSql = 'WITH RECURSIVE empl_hierarchy AS (
					SELECT
						te.employee_id,
						te.team_id
					FROM team_employees te
						JOIN employees e on ( e.id = te.employee_id )
					WHERE
						e.reporting_manager_id = ' . ( int ) $intManagerEmployeeId . '
						AND te.is_primary_team = 1
					UNION ALL
					SELECT
						te.employee_id,
						te.team_id
					FROM empl_hierarchy r
						JOIN employees e on ( e.reporting_manager_id = r.employee_id )
						JOIN team_employees te ON ( te.employee_id = e.id )
					WHERE
						e.id != e.reporting_manager_id
						AND is_primary_team = 1
				)
				SELECT DISTINCT te.team_id,
					t.name,
					t.manager_employee_id
				FROM empl_hierarchy as eh
					JOIN teams as t ON (eh.team_id = t.id and t.deleted_by IS NULL)
					JOIN team_employees as te ON (t.id = te.team_id and te.is_primary_team = 1)' . $strWhereCondition . $strJoin;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeesById( $intEmployeeId, $objDatabase, $boolTeamView = false, $boolManager = false ) {

		if( false == valId( $intEmployeeId ) ) return false;

		if( true == $boolTeamView ) {
			$strWhere = ' AND t.manager_employee_id = ' . ( int ) $intEmployeeId;
		} elseif( true == $boolManager ) {
			$strWhere = ' AND ( e.reporting_manager_id = ' . ( int ) $intEmployeeId . ' OR e.id = ' . ( int ) $intEmployeeId . ' )';
		} else {
			$strWhere = ' AND e.id = ' . ( int ) $intEmployeeId;
		}

		$strSql = 'SELECT DISTINCT ON (e.id) e.id as employee_id,
						t.manager_employee_id as parent_id,
						e.reporting_manager_id as manager_id,
						t.id as team_id,
						t.name as team_name,
						t.manager_employee_id as team_lead_id,
						dept.name as department,
						desg.name as designation,
						e.email_address as email_id,
						e.name_full as name_full,
						e.preferred_name as preferred_name,
						e.birth_date as birth_date, 
						e.anniversary_date as anniversary_date
					FROM teams t
							LEFT JOIN team_employees te on ( t.id = te.team_id )
							LEFT JOIN employees e ON ( e.id = te.employee_id AND te.is_primary_team = 1 ) OR ( e.id = t.manager_employee_id AND e.id NOT IN ( SELECT DISTINCT(employee_id) FROM team_employees ) )
							LEFT JOIN designations desg ON (desg.id = e.designation_id)
							LEFT JOIN departments dept ON (dept.id = e.department_id)
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id = 1
						AND e.id IS NOT NULL
						AND t.deleted_by IS NULL'
		                . $strWhere;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchEmployeeCountByManagerId( $intEmployeeId, $boolTeamView, $objDatabase ) {

		if( false == valId( $intEmployeeId ) ) return false;

		if( true == $boolTeamView ) {
			$strWhere = ' AND t.manager_employee_id = ' . ( int ) $intEmployeeId;
		} else {
			$strWhere = ' AND e.reporting_manager_id = ' . ( int ) $intEmployeeId;
		}

		$strSql = 'SELECT
						count( DISTINCT e.id )
					FROM
						teams t
						LEFT JOIN team_employees te ON ( t.id = te.team_id )
						LEFT JOIN employees e ON ( e.id = te.employee_id AND te.is_primary_team = 1 )
					WHERE
						e.date_terminated IS NULL
						AND e.employee_status_type_id = 1
						AND e.id IS NOT NULL
						AND e.id != e.reporting_manager_id
						AND t.deleted_by IS NULL' . $strWhere;

		return fetchData( $strSql, $objDatabase );

	}

	public static  function fetchPlannersStakeholdersByProductIds( $arrintTeamids, $objAdminDatabase, $arrintPsProductId =NULL, $arrintPsProductOptionId=NULL ) {

		if( false == valArr( $arrintTeamids ) ) {
			return false;
		}
		$strWhere = '';
		$strSelect = 'array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppp.sdm_employee_id IS NOT NULL) THEN ppp.sdm_employee_id::text || \',\' || pp.sdm_employee_id::text ELSE pp.sdm_employee_id::text END,\',\'), \',\') ) AS sdm_employee_ids , 
					array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppp.sqm_employee_id IS NOT NULL) THEN ppp.sqm_employee_id::text || \',\' || pp.sqm_employee_id::text ELSE pp.sqm_employee_id::text END,\',\'), \',\') ) AS sqm_employee_ids ,
					array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppp.doe_employee_id IS NOT NULL) THEN ppp.doe_employee_id::text || \',\' || pp.doe_employee_id::text ELSE pp.doe_employee_id::text END,\',\'), \',\') ) AS doe_employee_ids ,
					array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppp.product_manager_employee_id IS NOT NULL) THEN ppp.product_manager_employee_id::text || \',\' || pp.product_manager_employee_id::text ELSE pp.product_manager_employee_id::text END,\',\'), \',\') ) AS product_manager_employee_ids ,
					array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppp.product_owner_employee_id IS NOT NULL) THEN ppp.product_owner_employee_id::text || \',\' || pp.product_owner_employee_id::text ELSE pp.product_owner_employee_id::text END,\',\'), \',\') ) AS product_owner_employee_ids ,
					array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppp.architect_employee_id IS NOT NULL) THEN ppp.architect_employee_id::text || \',\' || pp.architect_employee_id::text ELSE pp.architect_employee_id::text END,\',\'), \',\') ) AS architect_employee_ids ,
					array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppp.business_analyst_employee_id IS NOT NULL) THEN ppp.business_analyst_employee_id::text || \',\' || pp.business_analyst_employee_id::text ELSE pp.business_analyst_employee_id::text END,\',\'), \',\') ) AS business_analyst_employee_ids ,
					array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppe.employee_id IS NOT NULL) THEN ppe.employee_id::text || \',\' || ppe.employee_id::text ELSE ppe.employee_id::text END,\',\'), \',\') ) AS employee_ids';
		if( true == valArr( $arrintPsProductId ) && false == valArr( $arrintPsProductOptionId ) ) {
			$strSelect = 'array_to_json( string_to_array( string_agg(DISTINCT pp.sdm_employee_id::text ,\',\'), \',\') ) AS sdm_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT pp.sqm_employee_id::text ,\',\'), \',\') ) AS sqm_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT pp.doe_employee_id::text ,\',\'), \',\') ) AS doe_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT pp.product_manager_employee_id::text ,\',\'), \',\') ) AS product_manager_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT pp.product_owner_employee_id::text ,\',\'), \',\') ) AS product_owner_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT pp.architect_employee_id::text ,\',\'), \',\') ) AS architect_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT pp.business_analyst_employee_id::text ,\',\'), \',\') ) AS business_analyst_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppe.employee_id IS NOT NULL AND ppe.ps_product_option_id IN(' . implode( ',', $arrintPsProductId ) . ') ) THEN ppe.employee_id::text || \',\' || ppe.employee_id::text ELSE ppe.employee_id::text END,\',\'), \',\') ) AS employee_ids';
		} else if( false == valArr( $arrintPsProductId ) && true == valArr( $arrintPsProductOptionId ) ) {
			$strSelect = 'array_to_json( string_to_array( string_agg(DISTINCT ppp.sdm_employee_id::text,\',\'), \',\') ) AS sdm_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT ppp.sqm_employee_id::text,\',\'), \',\') ) AS sqm_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT ppp.doe_employee_id::text,\',\'), \',\') ) AS doe_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT ppp.product_manager_employee_id::text,\',\'), \',\') ) AS product_manager_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT ppp.product_owner_employee_id::text,\',\'), \',\') ) AS product_owner_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT ppp.architect_employee_id::text,\',\'), \',\') ) AS architect_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT ppp.business_analyst_employee_id::text,\',\'), \',\') ) AS business_analyst_employee_ids ,
							array_to_json( string_to_array( string_agg(DISTINCT CASE WHEN (t.ps_product_option_id IS NOT NULL AND ppe.employee_id IS NOT NULL AND ppe.ps_product_option_id IN(' . implode( ',', $arrintPsProductOptionId ) . ')  ) THEN ppe.employee_id::text || \',\' || ppe.employee_id::text ELSE ppe.employee_id::text END,\',\'), \',\') ) AS employee_ids';
		} else if( true == valArr( $arrintPsProductId ) && true == valArr( $arrintPsProductOptionId ) ) {
			$strWhere = ' WHERE pp.id IN(' . implode( ',', $arrintPsProductId ) . ') OR ppp.id IN(' . implode( ',', $arrintPsProductOptionId ) . ')   ';
		}

		$strSql = 'SELECT ' . $strSelect . '  FROM teams AS t
				 JOIN ps_products AS pp ON ( t.ps_product_id = pp.id AND pp.deleted_by IS NULL AND t.deleted_by IS NULL AND t.id IN (  ' . implode( ',', $arrintTeamids ) . ' ) )
				LEFT JOIN ps_product_options AS ppp ON ( t.ps_product_option_id = ppp.id AND ppp.is_published = 1 )
				LEFT JOIN ps_product_employees AS ppe ON (t.ps_product_id = ppe.ps_product_id AND ppe.ps_product_employee_type_id = 5 )' . $strWhere;

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function fetchAllQamanagersbyAddEmployeeId( $intAddEmployeeId, $objAdminDatabase ) {

		if( false == valId( $intAddEmployeeId ) ) {
			return false;
		}

		$strSql = '
				SELECT 
					DISTINCT t.qam_employee_id
					FROM teams t
					JOIN employees e ON (t.qa_employee_id = e.id)
					WHERE 
						t.add_employee_id = ' . ( int ) $intAddEmployeeId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND t.qam_employee_id IS NOT NULL 
						AND t.deleted_by IS NULL
		';

		return self::fetchTeams( $strSql, $objAdminDatabase );
	}

	public static function fetchTeamLeadByEmployeeId( $intEmployeeId, $objDatabase ) {

		if( false == valId( $intEmployeeId ) ) {
			return false;
		}

		$strSql = 'SELECT
						t.id AS team_id,
						e.reporting_manager_id AS manager_employee_id,
						e.id AS employee_id,
						t.manager_employee_id as team_lead_id
					FROM
						teams t
						JOIN team_employees te ON ( te.team_id = t.id AND te.is_primary_team = 1 )
						JOIN employees e ON ( te.employee_id = e.id )
					WHERE
						e.id =' . ( int ) $intEmployeeId;

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchActiveTeamDetailsByEmployeeId( $intEmployeeId, $boolSuperUser = false, $arrintDepartmentIds, $objAdminDatabase, $intTeamId = NULL, $boolScrumTeam = false ) {

		if( false == valId( $intEmployeeId ) ) {
			return NULL;
		}

		$strTeamCondition = '';
		$strWhere = 't.deleted_by IS NULL ';

		if( true == $boolScrumTeam ) {
			$strWhere .= ' AND t.team_type_id = ' . \CTeamType::SCRUM_TEAM;
		}

		if( false == $boolSuperUser ) {
			$strWhere .= ' AND ( ( ' . ( int ) $intEmployeeId . ' IN ( e1.reporting_manager_id, 
																	t.qam_employee_id, 
																	t.add_employee_id, 
																	t.tpm_employee_id, 
																	t.sdm_employee_id, 
																	pp.doe_employee_id,
																	pp.sdm_employee_id, 
																	pp.product_owner_employee_id, 
																	pp.product_manager_employee_id, 
																	pp.implementation_employee_id, 
																	pp.ux_employee_id, pp.architect_employee_id, 
																	pp.business_analyst_employee_id, 
																	pp.executive_employee_id, 
																	pp.sqm_employee_id,
																	ppp.sdm_employee_id, 
																	ppp.doe_employee_id, 
																	ppp.product_manager_employee_id, 
																	ppp.product_owner_employee_id, 
																	ppp.implementation_employee_id, 
																	ppp.ux_employee_id, 
																	ppp.architect_employee_id, 
																	ppp.business_analyst_employee_id, 
																	ppp.executive_employee_id, 
																	ppp.sqm_employee_id
																 ) )';

			$strWhere .= '  AND ( ' . ( int ) $intEmployeeId . ' IN (  e1.reporting_manager_id, t.qam_employee_id, t.add_employee_id, t.tpm_employee_id, t.sdm_employee_id, pp.doe_employee_id, pp.product_owner_employee_id, pp.sdm_employee_id,  pp.product_manager_employee_id, pp.implementation_employee_id, pp.ux_employee_id, pp.architect_employee_id, pp.business_analyst_employee_id, pp.executive_employee_id, pp.sqm_employee_id, ppp.sdm_employee_id, ppp.doe_employee_id, ppp.product_manager_employee_id, ppp.product_owner_employee_id, ppp.implementation_employee_id, ppp.ux_employee_id, ppp.architect_employee_id, ppp.business_analyst_employee_id, ppp.executive_employee_id, ppp.sqm_employee_id ) ) ' . $strTeamCondition . ' )';
		}

		if( false == $boolSuperUser ) {
			$strSql = '	SELECT
						DISTINCT t.id,t.name,e.name_full as team_name
					FROM
						teams AS t
						JOIN employees AS e ON ( t.manager_employee_id = e.id )
						LEFT JOIN ps_products AS pp ON ( pp.id = t.ps_product_id AND pp.deleted_by IS NULL )
						LEFT JOIN ps_product_options AS ppp ON ( ppp.id = t.ps_product_option_id AND ppp.is_published = 1 )
						LEFT JOIN team_employees AS te ON ( t.id = te.team_id )
						JOIN employees e1 ON ( te.employee_id = e1.id AND te.is_primary_team = 1 )
					WHERE
						' . $strWhere . ' AND t.department_id IN ( ' . implode( ',', array_filter( $arrintDepartmentIds ) ) . ' )
					GROUP BY
						t.id,e.id
					ORDER BY
						t.name ASC';
		} else {
			$strSql = '	SELECT
						DISTINCT t.id,t.name, e.name_full as team_name
					FROM
						teams AS t
						JOIN employees AS e ON ( t.manager_employee_id = e.id   )
				    WHERE
						' . $strWhere . ' AND ( CASE
       WHEN t.department_id IS NOT NULL THEN t.department_id IN ( ' . implode( ',', array_filter( $arrintDepartmentIds ) ) . ' ) ELSE e.department_id IN ( ' . implode( ',', array_filter( $arrintDepartmentIds ) ) . ' ) END )

					ORDER BY
						t.name ASC';
		}
		return fetchData( $strSql, $objAdminDatabase );

	}

}
?>