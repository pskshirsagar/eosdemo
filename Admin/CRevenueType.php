<?php

class CRevenueType extends CBaseRevenueType {

	const RECURRING 		= 1;
	const TRANSACTIONAL 	= 2;
	const IMPLEMENTATION 	= 3;
	const TRAINING 			= 4;
	const OTHER 			= 5;
	const NONREVENUE		= 6;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>