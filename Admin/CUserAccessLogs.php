<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserAccessLogs
 * Do not add any new functions to this class.
 */

class CUserAccessLogs extends CBaseUserAccessLogs {

	public static function fetchUserAccessLogs( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CUserAccessLog', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchUserAccessLog( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CUserAccessLog', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchDuplicateUserAccessLogs( $intUserId, $intPsProductId, $strModuleName, $strActionName, $strDate, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						user_access_logs ual
					WHERE
						ual.user_id = ' . ( int ) $intUserId . '
						AND ual.ps_product_id = ' . ( int ) $intPsProductId . '
						AND ual.module LIKE \'' . $strModuleName . '\'
						AND ual.action LIKE \'' . $strActionName . '\'
						AND date_part ( \'year\', ual.month ) = ' . date( 'Y', strtotime( $strDate ) ) . '
						AND date_part ( \'month\', ual.month ) = ' . date( 'm', strtotime( $strDate ) ) . '
						LIMIT 1;';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSimpleUserAccessLogs( $strOrderBy = NULL, $strSortDirection = NULL, $intPageSize = NULL, $intOffset = NULL, $intNumberOfDay, $objUserLogFilter = NULL, $objDatabase ) {

		$strWhere = ' WHERE ual.hit_count >= 1';

		if( false == is_null( $objUserLogFilter ) ) {

			$strUserName	= $objUserLogFilter->getUserName();

			if( false == is_null( $strUserName ) ) {

				$intCount		= substr_count( $strUserName, ' ' );
				$strSqlOperator = ' AND ';

				if( 0 < $intCount ) {
					$arrstrNames	= explode( ' ', $strUserName );
					$strFirstName	= $arrstrNames[0];
					$strLastName	= $arrstrNames[$intCount];

					if( true == empty( $strLastName ) ) {
						$strSqlOperator = ' OR ';
						$strLastName	= $arrstrNames[0];
					}

				} else {
					$strSqlOperator = ' OR ';
					$strFirstName	= $strUserName;
					$strLastName	= $strUserName;
				}

				if( true == valStr( $strFirstName ) ) {
					$strWhere .= ' AND ( e.name_first ILIKE \'%' . $strFirstName . '%\'';

					if( true == valStr( $strLastName ) ) {
						$strWhere .= $strSqlOperator . 'e.name_last ILIKE \'%' . $strLastName . '%\'';
					}
					$strWhere .= ' ) ';
				}

			}

			if( false == is_null( $objUserLogFilter->getUserIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objUserLogFilter->getUserIds() ) ) {
				$strWhere .= ' AND ual.user_id IN ( ' . implode( ', ', $objUserLogFilter->getUserIds() ) . ' )';
			}

			if( false == is_null( $objUserLogFilter->getProductIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objUserLogFilter->getProductIds() ) ) {
				$strWhere .= ' AND ual.ps_product_id IN ( ' . implode( ', ', $objUserLogFilter->getProductIds() ) . ' )';
			}

			$strModule = $objUserLogFilter->getModule();

			if( false == is_null( $strModule ) && false == empty( $strModule ) ) {
				$strWhere .= ' AND ual.module ILIKE \'%' . $strModule . '%\'';
			}

			$strAction = $objUserLogFilter->getAction();

			if( false == is_null( $strAction ) && false == empty( $strAction ) ) {
				$strWhere .= ' AND ual.action ILIKE \'%' . $strAction . '%\'';
			}

			$strFromDate = $objUserLogFilter->getFromMonth();

			if( false == is_null( $strFromDate ) && false == empty( $strFromDate ) ) {
				$strWhere .= ' AND ual.month >= \'' . $strFromDate . '\'';
			}

			$strToDate = $objUserLogFilter->getToMonth();

			if( false == is_null( $strToDate ) && false == empty( $strToDate ) ) {
				$strWhere .= ' AND ual.month <= \'' . $strToDate . '\'';
			}
		}

		if( true == is_null( $strFromDate ) && true == is_null( $strToDate ) && false == is_null( $intNumberOfDay ) ) {
			$strWhere .= ' AND EXTRACT( month FROM ( ual.month ) ) >= EXTRACT( month FROM ( NOW() - INTERVAL \'' . $intNumberOfDay . ' days\' ) ) ';
		}

		$strSql	= 'SELECT
						ual.*,
						e.name_first || \' \' || e.name_last as user_name
					FROM
						user_access_logs ual
						LEFT JOIN users u ON ( ual.user_id = u.id )
						LEFT JOIN employees e ON ( u.employee_id = e.id )
						' . $strWhere . '
						ORDER BY ' . $strOrderBy . ' ' . $strSortDirection . '
						OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intPageSize;
// display($strSql);die;
		return fetchData( $strSql, $objDatabase );
	}

	public static function loadSimpleUserAccessLogsCount( $intNumberOfDay, $objUserLogFilter = NULL, $objDatabase ) {

		$strWhere = ' WHERE ual.hit_count >= 1';

		if( false == is_null( $objUserLogFilter ) ) {

			$strUserName	= $objUserLogFilter->getUserName();

			if( false == is_null( $strUserName ) ) {

				$intCount		= substr_count( $strUserName, ' ' );
				$strSqlOperator = ' AND ';

				if( 0 < $intCount ) {
					$arrstrNames	= explode( ' ', $strUserName );
					$strFirstName	= $arrstrNames[0];
					$strLastName	= $arrstrNames[$intCount];

					if( true == empty( $strLastName ) ) {
						$strSqlOperator = ' OR ';
						$strLastName	= $arrstrNames[0];
					}

				} else {
					$strSqlOperator = ' OR ';
					$strFirstName	= $strUserName;
					$strLastName	= $strUserName;
				}

				if( true == valStr( $strFirstName ) ) {
					$strWhere .= ' AND ( e.name_first ILIKE \'%' . $strFirstName . '%\'';

					if( true == valStr( $strLastName ) ) {
						$strWhere .= $strSqlOperator . 'e.name_last ILIKE \'%' . $strLastName . '%\'';
					}
					$strWhere .= ' ) ';
				}

			}

			if( false == is_null( $objUserLogFilter->getUserIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objUserLogFilter->getUserIds() ) ) {
				$strWhere .= ' AND ual.user_id IN ( ' . implode( ', ', $objUserLogFilter->getUserIds() ) . ' )';
			}

			if( false == is_null( $objUserLogFilter->getProductIds() ) && 0 < \Psi\Libraries\UtilFunctions\count( $objUserLogFilter->getProductIds() ) ) {
				$strWhere .= ' AND ual.ps_product_id IN ( ' . implode( ', ', $objUserLogFilter->getProductIds() ) . ' )';
			}

			$strModule = $objUserLogFilter->getModule();

			if( false == is_null( $strModule ) && false == empty( $strModule ) ) {
				$strWhere .= ' AND ual.module ILIKE \'%' . $strModule . '%\'';
			}

			$strAction = $objUserLogFilter->getAction();

			if( false == is_null( $strAction ) && false == empty( $strAction ) ) {
				$strWhere .= ' AND ual.action ILIKE \'%' . $strAction . '%\'';
			}

			$strFromDate = $objUserLogFilter->getFromMonth();

			if( false == is_null( $strFromDate ) && false == empty( $strFromDate ) ) {
				$strWhere .= ' AND ual.month >= \'' . $strFromDate . '\'';
			}

			$strToDate = $objUserLogFilter->getToMonth();

			if( false == is_null( $strToDate ) && false == empty( $strToDate ) ) {
				$strWhere .= ' AND ual.month <= \'' . $strToDate . '\'';
			}
		}

		if( true == is_null( $strFromDate ) && true == is_null( $strToDate ) && false == is_null( $intNumberOfDay ) ) {
			$strWhere .= ' AND EXTRACT( month FROM ( ual.month ) ) >= EXTRACT( month FROM ( NOW() - INTERVAL \'' . $intNumberOfDay . ' days\' ) ) ';
		}

		$strSql = 'SELECT
						count ( ual.* )
					FROM
						user_access_logs ual
						LEFT JOIN users u ON ( ual.user_id = u.id )
						LEFT JOIN employees e ON ( u.employee_id = e.id )
						' . $strWhere;

		return fetchData( $strSql, $objDatabase );
	}

}
?>