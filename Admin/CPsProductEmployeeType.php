<?php

class CPsProductEmployeeType extends CBasePsProductEmployeeType {

	const CONTRACT_TERMINATION 			 = 1;
	const ASSOCAITE_DIRECTOR_DEVELOPMENT = 2;
	const QA_TEAM_LEAD					 = 3;
	const AUTOMATION_SPOC				 = 4;
	const SVP_PRODUCT_EMPLOYEE			 = 5;
	const REPORTING_ANALYST			     = 6;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>