<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CInternalTransfers
 * Do not add any new functions to this class.
 */

class CInternalTransfers extends CBaseInternalTransfers {

	public static function fetchPaginatedInternalTransfers( $intPageNo, $intPageSize, $objDatabase ) {

	    $intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
	    $intLimit = ( int ) $intPageSize;

	    $strSql = ' SELECT
	                     *
	                 FROM
	                     internal_transfers
	                 WHERE
	                     1 = 1
	                 ORDER BY id DESC OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit;

	    return self::fetchInternalTransfers( $strSql, $objDatabase );
	}

	public static function fetchPaginatedInternalTransfersCount( $objDatabase ) {
	    return self::fetchInternalTransferCount( 'WHERE 1 = 1', $objDatabase );
	}

	public static function fetchUnExportedInternalTransfersByDateGroupedByAccount( $strDate, $strCurrencyCode = NULL, $objDatabase ) {

	    $strWhereCondition = '';

        if( true == valStr( $strCurrencyCode ) ) {
            $strWhereCondition = ' AND currency_code = \'' . $strCurrencyCode . '\' ';
        }

		$strSql = ' SELECT
						batched_on AS transfer_date,
						credit_processing_bank_account_id,
						debit_processing_bank_account_id,
						SUM(payment_amount) AS daily_total,
						array_to_string( array_agg( id ), \',\' ) AS internal_transfer_ids,
						currency_code
	                 FROM
	                    internal_transfers
	                 WHERE
	                    export_batch_id IS NULL
						AND batched_on IS NOT NULL
						AND payment_status_type_id NOT IN( ' . ( int ) CPaymentStatusType::VOIDED . ' , ' . ( int ) CPaymentStatusType::CANCELLED . ' )
						AND (
							debit_processing_bank_account_id IN( ' . implode( ',', CProcessingBankAccount::$c_arrintAccountsRequiringDeposits ) . ' )
							OR credit_processing_bank_account_id IN( ' . implode( ',', CProcessingBankAccount::$c_arrintAccountsRequiringDeposits ) . ' )
						)
						AND date_trunc( \'day\', batched_on ) <= \'' . date( 'Y-m-d', strtotime( $strDate ) ) . '\'' . $strWhereCondition . '
					GROUP BY
						batched_on,
						credit_processing_bank_account_id,
						debit_processing_bank_account_id,
						currency_code
			        ORDER BY
						batched_on';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchInternalTransfersByExportBatchIdGroupedByAccount( $intExportBatchId, $objDatabase ) {
		$strSql = ' SELECT
						batched_on AS transfer_date,
						credit_processing_bank_account_id,
						debit_processing_bank_account_id,
						SUM(payment_amount) AS daily_total,
						array_to_string( array_agg( id ), \',\' ) AS internal_transfer_ids
	                 FROM
	                    internal_transfers
	                 WHERE
	                    export_batch_id = ' . ( int ) $intExportBatchId . '
					GROUP BY
						batched_on,
						credit_processing_bank_account_id,
						debit_processing_bank_account_id
			        ORDER BY
						batched_on';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchInternalTransfersByExportBatchIdGroupedByAccountByCurrencyCode( $intExportBatchId, $objDatabase ) {
		$strSql = ' SELECT
						batched_on AS transfer_date,
						credit_processing_bank_account_id,
						debit_processing_bank_account_id,
						SUM(payment_amount) AS daily_total,
						array_to_string( array_agg( id ), \',\' ) AS internal_transfer_ids,
						currency_code
	                 FROM
	                    internal_transfers
	                 WHERE
	                    export_batch_id = ' . ( int ) $intExportBatchId . '
					GROUP BY
						batched_on,
						credit_processing_bank_account_id,
						debit_processing_bank_account_id,
						currency_code
			        ORDER BY
						batched_on';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchInternalTransfersCurrencyCodesByExportBatchDate( $strExportBatchDate, $objDatabase ) {

		if ( false == valStr( $strExportBatchDate ) ) return NULL;

		$strSql = 'SELECT
						DISTINCT c.currency_code
					FROM
						currencies c
						JOIN internal_transfers intr
						ON intr.currency_code = c.currency_code
					WHERE
						c.is_published = TRUE 
						AND intr.export_batch_id IS NULL
						AND intr.batched_on IS NOT NULL
						AND intr.payment_status_type_id NOT IN( ' . ( int ) CPaymentStatusType::VOIDED . ' , ' . ( int ) CPaymentStatusType::CANCELLED . ' )
						AND (
							intr.debit_processing_bank_account_id IN( ' . implode( ',', CProcessingBankAccount::$c_arrintAccountsRequiringDeposits ) . ' )
							OR intr.credit_processing_bank_account_id IN( ' . implode( ',', CProcessingBankAccount::$c_arrintAccountsRequiringDeposits ) . ' )
						)
						AND date_trunc( \'day\', intr.batched_on ) <= \'' . date( 'Y-m-d', strtotime( $strExportBatchDate ) ) . '\'
					ORDER BY
						c.currency_code
						';

		return fetchData( $strSql, $objDatabase );
	}

}
