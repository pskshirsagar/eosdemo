<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CUserRepositoryPermissions
 * Do not add any new functions to this class.
 */

class CUserRepositoryPermissions extends CBaseUserRepositoryPermissions {

	public static function fetchAllUserRepositoryPermissions( $objDatabase ) {

		$strSql = 'SELECT
						urp.repository_id,
						CASE
							WHEN u.active_directory_guid IS NULL THEN u.svn_username
							ELSE u.username
						END AS username,
						CASE
							WHEN 1 = urp.is_write THEN \'rw\'
							WHEN 1 = urp.is_read THEN \'r\'
						END AS grants
					FROM
						user_repository_permissions urp
						JOIN users u ON u.id = urp.user_id AND u.is_disabled <> 1
					ORDER BY
						urp.repository_id,
						u.username';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchAssignedUsersByRepositoryId( $intRepositoryId, $objDatabase ) {

		$strSql = 'SELECT
						urp.repository_id,
						u.id as user_id,
						u.username,
						e.name_full,
						urp.is_read,
						urp.is_write
					FROM
						users u
						JOIN employees e ON( u.employee_id = e.id )
						JOIN user_repository_permissions urp ON( u.id = urp.user_id )
					where
						urp.repository_id = ' . ( int ) $intRepositoryId . '
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND u.is_disabled = 0
					ORDER BY
						u.username';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSearchUnAssignedUsers( $intRepositoryId, $arrstrExplodedSearch, $objDatabase ) {

		$strSql = 'SELECT
						u.username,
						u.id as user_id,
						e.name_full
					FROM
						users u
						JOIN employees e ON ( u.employee_id = e.id )
					WHERE
						u.id NOT IN (
										SELECT
											urp.user_id
										FROM
											user_repository_permissions urp
										WHERE
											urp.repository_id = ' . ( int ) $intRepositoryId . '
											AND urp.user_id = u.id
									)
						AND
						(
							(e.name_full ILIKE \'%' . implode( '%\' AND e.name_full ILIKE \'%', $arrstrExplodedSearch ) . '%\')
							OR (u.username ILIKE \'%' . implode( '%\' AND u.username ILIKE \'%', $arrstrExplodedSearch ) . '%\')
						)
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
						AND u.is_disabled = 0
					ORDER BY
						e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchUserRepositoryPermissionsByRepositoryIdByUserIds( $intRepositoryId, $arrintUserIds, $objDatabase ) {

		if( false == valArr( $arrintUserIds ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						user_repository_permissions
					WHERE
						repository_id = ' . ( int ) $intRepositoryId . '
						AND user_id IN ( ' . implode( ',', $arrintUserIds ) . ')';

		return self::fetchUserRepositoryPermissions( $strSql, $objDatabase );
	}

}
?>