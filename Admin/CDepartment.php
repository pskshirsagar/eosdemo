<?php

class CDepartment extends CBaseDepartment {

	const TECHNICAL_SUPPORT				= 1;
	const MERCHANT_PROCESSING			= 2;
	const DEVELOPMENT					= 3;
	const QA							= 4;
	const ACCOUNTING					= 5;
	const ADMINISTRATIVE				= 7;
	const MARKETING						= 8;
	const SUCCESS_MANAGEMENT			= 9;
	const HR							= 10;
	const RESIDENT_INSURE				= 11;
	const UTILITY_BILLING				= 12;
	const CALL_CENTER					= 13;
	const UTILITY_MANAGEMENT			= 14;
	const IT							= 15;
	const LEGAL							= 16;
	const UX							= 19;
	const IMPLEMENTATION				= 22;
	const INTERNAL_TRAINING				= 23;
	const TRAINING						= 24;
	const SEO_SERVICES					= 25;
	const RESIDENT_SREVICES				= 26;
	const CONSULTING					= 17;
	const CREATIVE						= 18;
	const ENTRATA_CORE					= 20;
	const PRODUCT_DEVELOPMENT			= 21;
	const TRAINING_AND_DEVELOPMENT		= 33;
	const RECRUITING					= 31;
	const ADMINISTRATION				= 55;
	const PROJECT_MANAGEMENT			= 53;

	const SALES							= 28;
	const SALES_DIALER					= 29;
	const EXECUTIVE						= 34;
	const SALES_ENGINEER				= 35;
	const SALES_ADMINS					= 36;
	const SALES_FARMERS					= 38;

	const SOFTWARE_DEVELOPMENT_MANAGERS	= 39;
	const ACCOUNTS						= 32;
	const INFORMATION_SECURITY			= 42;
	const DATA_PROCESSING				= 40;
	const MOBILE_TECHNOLOGY				= 48;
	const DATABASE						= 49;

	const RESIDENT_PAY					= 51;
	const MARKETING_OR_LEASING			= 52;
	const DATA_PROCESSING_DEPARTMENT	= 140;
	const COMPANY						= 47;

	protected $m_intTrainingSessionsCount;
	protected $m_intActionResultId;
	protected $m_intUserId;

	protected $m_strEmailAddress;
	protected $m_strDesignationName;
	protected $m_strCurrentTitle;
	protected $m_strTagName;
	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPreferredName;

	public static $c_arrintDepartments	= [
		self::ADMINISTRATION,
		self::EXECUTIVE,
		self::ACCOUNTS,
		self::TRAINING_AND_DEVELOPMENT,
		self::HR,
		self::RECRUITING
	];

	public static $c_arrintDeskNotMandatoryDepartments	= [
		self::DATA_PROCESSING
	];

	public static $c_arrintSalesClosersDepartmentIds					= [ self::SALES, self::SALES_FARMERS ];
	public static $c_arrintSalesDepartmentIds							= [ self::SALES, self::SALES_FARMERS, self::SALES_DIALER, self::SALES_ENGINEER, self::SALES_ADMINS ];
	public static $c_arrintDevelopmentDashboardDepartmentIds			= [ self::DEVELOPMENT, self::QA, self::PRODUCT_DEVELOPMENT, self::CREATIVE ];
	public static $c_arrintCommissionRecipientsDepartmentIds			= [ self::SALES, self::SALES_DIALER, self::SALES_ENGINEER, self::SALES_ADMINS, self::SALES_FARMERS, self::SEO_SERVICES, self::TECHNICAL_SUPPORT ];

	public static $c_arrintSupportDepartmentIds 						= [ self::TECHNICAL_SUPPORT ];
	public static $c_arrintAllowToDeleteFailedEmailsDepartmentIds		= [ self::TECHNICAL_SUPPORT, self::IT, self::SUCCESS_MANAGEMENT ];
	public static $c_arrintAllowContractTerminationDepartmentIds		= [ self::CONSULTING, self::PROJECT_MANAGEMENT, self::LEGAL ];
	public static $c_arrintStoryPointsVisibleDepartmentIds				= [ self::DEVELOPMENT, self::DATABASE, self::MOBILE_TECHNOLOGY, self::CREATIVE, self::INFORMATION_SECURITY ];
	public static $c_arrintReleaseFilterVisibleDepartmentIds			= [ self::DEVELOPMENT, self::DATABASE, self::MOBILE_TECHNOLOGY, self::CREATIVE, self::QA, self::INFORMATION_SECURITY ];
	public static $c_arrintWorkforceManagementCallQueueDepartmentIds	= [ self::CALL_CENTER, self::TECHNICAL_SUPPORT, self::SALES, self::RESIDENT_INSURE ];
	public static $c_arrintRandomEmployeeDepartmentIds 					= [ self::SALES, self::SALES_ENGINEER ];
	public static $c_arrintDevelopmentDepartmentIds 					= [ self::DEVELOPMENT, self::CREATIVE, self::PRODUCT_DEVELOPMENT, self::INFORMATION_SECURITY ];
	public static $c_arrintClientAssignmentDepartmentIds				= [ self::CONSULTING, self::PROJECT_MANAGEMENT, self::SUCCESS_MANAGEMENT, self::SALES, self::SALES_ADMINS, self::ACCOUNTING, self::SEO_SERVICES, self::CALL_CENTER, self::UTILITY_BILLING, self::UTILITY_MANAGEMENT ];
	public static $c_arrintDepartmentIds								= [ self::ENTRATA_CORE, self::RESIDENT_PAY, self::MARKETING_OR_LEASING, self::RESIDENT_SREVICES ];

	public static $c_arrintSalesPermissionDepartmentIds					= [ self::SALES, self::SALES_FARMERS, self::SALES_ENGINEER, self::SALES_ADMINS ];
	public static $c_arrintSalesPsLeadDepartmentIds						= [ self::SALES, self::SALES_ENGINEER, self::SALES_ADMINS ];
	public static $c_arrintSalesRenewalMetricsDepartmentIds				= [ self::SALES, self::SALES_ADMINS ];
	public static $c_arrintSalesQuotaDepartmentIds						= [ self::SALES ];
	public static $c_arrintSalesDemosDepartmentIds						= [ self::SALES, self::SALES_ADMINS ];
	public static $c_arrintAllowShowAllTeamsSalesDepartmentIds			= [ self::SALES_ADMINS, self::LEGAL, self::SALES_ENGINEER, self::ACCOUNTING ];

	public static $c_arrintAllowDelinquencyDepartmentIds				= [ self::ACCOUNTING, self::SALES, self::SALES_ADMINS ];
	public static $c_arrintTaskSalesSupportDepartmentIds				= [ self::TECHNICAL_SUPPORT, self::SALES, self::SALES_ADMINS, self::SALES_ENGINEER, self::SALES_FARMERS ];

	public static $c_arrintUsRecruitmentDepartmentIds					= [ self::CONSULTING, self::PROJECT_MANAGEMENT, self::SUCCESS_MANAGEMENT, self::SALES, self::SEO_SERVICES, self::SALES_ENGINEER, self::ADMINISTRATIVE, self::SALES, self::IMPLEMENTATION, self::PRODUCT_DEVELOPMENT, self::UX, self::UTILITY_MANAGEMENT, self::TRAINING, self::TECHNICAL_SUPPORT, self::ACCOUNTING, self::RESIDENT_INSURE, self::DEVELOPMENT, self::IT, self::LEGAL, self::HR, self::MERCHANT_PROCESSING, self::MARKETING, self::UTILITY_BILLING, self::CREATIVE ];
	public static $c_arrintIndiaRecruitmentDepartmentIds				= [ self::TECHNICAL_SUPPORT, self::DEVELOPMENT, self::QA, self::ACCOUNTING, self::ADMINISTRATIVE, self::HR, self::CREATIVE, self::IT ];

	public static $c_arrintImplementationDepartmentIds					= [ self::CONSULTING, self::PROJECT_MANAGEMENT ];
	public static $c_arrintImplementationDashboardDepartmentIds			= [ self::CONSULTING, self::PROJECT_MANAGEMENT, self::IMPLEMENTATION ];
	public static $c_arrintAllowedToEditTooltip							= [ self::CONSULTING, self::PROJECT_MANAGEMENT, self::IMPLEMENTATION, self::INTERNAL_TRAINING, self::TRAINING ];
	public static $c_arrintSEOEmployeeDepartmentIds						= [ self::SEO_SERVICES, self::CONSULTING, self::PROJECT_MANAGEMENT ];
	public static $c_arrintSprintDepartmentIds							= [ self::DEVELOPMENT, self::CREATIVE, self::INFORMATION_SECURITY, self::DATABASE, self::MOBILE_TECHNOLOGY, self::QA, self::DATA_PROCESSING ];
	public static $c_arrintSprintQaDepartmentIds						= [ self::QA, self::DATA_PROCESSING ];

	public static $c_arrintClientAdminReportsDepartmentIds				= [ self::ACCOUNTING => 'Accounting', self::HR => 'Hr', self::MERCHANT_PROCESSING => 'Merchant Processing', self::IMPLEMENTATION => 'Implementation', self::DEVELOPMENT => 'Development', self::CALL_CENTER => 'Leasing Center', self::TRAINING => 'Training', self::COMPANY => 'Company' ];
	public static $c_arrintUpdateNewTeamStructureDepartmentIds			= [ self::CREATIVE, self::DEVELOPMENT, self::QA, self::MOBILE_TECHNOLOGY ];

	public static $c_arrintPlannerDevelopmentDepartmentIds				= [ self::DEVELOPMENT, self::CREATIVE, self::INFORMATION_SECURITY, self::DATABASE, self::MOBILE_TECHNOLOGY ];
	public static $c_arrintPlannerQADepartmentIds						= [ self::QA, self::DATA_PROCESSING ];
	public static $c_arrintTestCasePermissionDepartmentIds				= [ self::DEVELOPMENT, self::QA, self::MOBILE_TECHNOLOGY ];
	public static $c_arrintQaRequiredDepartmentIds                      = [ self::QA ];
	public static $c_arrintLeaveAccessDepartmentIds                     = [ self::CREATIVE, self::DEVELOPMENT, self::QA ];
	public static $c_arrintDepartmentIdsForRCADropdown					= [ self::CREATIVE, self::DEVELOPMENT, self::QA, self::MOBILE_TECHNOLOGY, self::UX ];

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function getDesignationName() {
		return $this->m_strDesignationName;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getCurrentTitle() {
		return $this->m_strCurrentTitle;
	}

	public function getTrainingSessionsCount() {
		return $this->m_intTrainingSessionsCount;
	}

	public function getActionResultId() {
		return $this->m_intActionResultId;
	}

	public function getTagName() {
		return $this->m_strTagName;
	}

	public function getUserId() {
		return $this->m_intUserId;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setPreferredName( $strPreferredName ) {
		$this->m_strPreferredName = $strPreferredName;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setDesignationName( $strDesignationName ) {
		$this->m_strDesignationName = $strDesignationName;
	}

	public function setCurrentTitle( $strCurrentTitle ) {
		$this->m_strCurrentTitle = $strCurrentTitle;
	}

	public function setTrainingSessionsCount( $intTrainingSessionsCount ) {
		$this->m_intTrainingSessionsCount = $intTrainingSessionsCount;
	}

	public function setActionResultId( $intActionResultId ) {
		$this->m_intActionResultId = $intActionResultId;
	}

	public function setTagName( $strTagName ) {
		$this->m_strTagName = $strTagName;
	}

	public function setUserId( $intUserId ) {
		$this->m_intUserId = $intUserId;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrmixValues['name_first'] ) )					$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) ) 					$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['preferred_name'] ) )				$this->setPreferredName( $arrmixValues['preferred_name'] );
		if( true == isset( $arrmixValues['email_address'] ) )				$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['designation_name'] ) )			$this->setDesignationName( $arrmixValues['designation_name'] );
		if( true == isset( $arrmixValues['current_title'] ) )				$this->setCurrentTitle( $arrmixValues['current_title'] );
		if( true == isset( $arrmixValues['training_sessions_count'] ) )		$this->setTrainingSessionsCount( $arrmixValues['training_sessions_count'] );
		if( true == isset( $arrmixValues['action_result_id'] ) )			$this->setActionResultId( $arrmixValues['action_result_id'] );
		if( true == isset( $arrmixValues['tag_name'] ) )					$this->setTagName( $arrmixValues['tag_name'] );
		if( true == isset( $arrmixValues['user_id'] ) )						$this->setUserId( $arrmixValues['user_id'] );
		return;
	}

	/**
	 * If you are making any changes in loadSmartyConstants function then
	 * please make sure the same changes would be applied to loadTemplateConstants function also.
	 */

	public static function loadSmartyConstants( $objSmarty ) {

		$objSmarty->assign( 'DATA_PROCESSING',		self::DATA_PROCESSING );
		$objSmarty->assign( 'DEVELOPMENT',			self::DEVELOPMENT );
		$objSmarty->assign( 'QA',					self::QA );
		$objSmarty->assign( 'INFORMATION_SECURITY',	self::INFORMATION_SECURITY );
	}

	public static $c_arrstrDepartments = [
		'DATA_PROCESSING'		=> self::DATA_PROCESSING,
		'DEVELOPMENT'			=> self::DEVELOPMENT,
		'QA'					=> self::QA,
		'INFORMATION_SECURITY'	=> self::INFORMATION_SECURITY,
		'DEPARTMENT_CREATIVE'	=> self::CREATIVE
	];

}
?>