<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseNoteTypes
 * Do not add any new functions to this class.
 */

class CReleaseNoteTypes extends CBaseReleaseNoteTypes {

	public static function fetchReleaseNoteTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CReleaseNoteType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchReleaseNoteType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CReleaseNoteType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPublishedReleaseNoteTypes( $objDatabase ) {
		$strSql = ' SELECT
						*
					FROM
						release_note_types
					WHERE
						is_published = TRUE
					ORDER BY
						order_num';

		return self::fetchReleaseNoteTypes( $strSql, $objDatabase );
	}

}
?>