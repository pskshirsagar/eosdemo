<?php

class CCompanyValue extends CBaseCompanyValue {

	const TALK_TO_ME_GOOSE = 1;
	const BE_EXCELLENT_TO_EACH_OTHER = 2;
	const BUSINESS_IN_THE_FRONT_PARTY_IN_THE_BACK = 3;
	const BE_THE_REAL_DEAL = 4;
	const BE_THE_JONESES = 5;

	public static $c_arrintCompanyValues = [ self::TALK_TO_ME_GOOSE, self::BE_EXCELLENT_TO_EACH_OTHER, self::BUSINESS_IN_THE_FRONT_PARTY_IN_THE_BACK, self::BE_THE_REAL_DEAL, self::BE_THE_JONESES ];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNominationHint() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>