<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationSources
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationSources extends CBaseEmployeeApplicationSources {

	public static function fetchEmployeeApplicationSources( $strSql, $objDatabase ) {
		return self::fetchObjects( $strSql, 'CEmployeeApplicationSource', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchEmployeeApplicationSource( $strSql, $objDatabase ) {
		return self::fetchObject( $strSql, 'CEmployeeApplicationSource', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

}
?>