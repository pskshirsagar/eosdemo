<?php

class CCompanyPricingType extends CBaseCompanyPricingType {

	const RESIDENT_LEASE 									= 1; // Lease Execution
	const COST_PER_VOIP_MINUTE								= 2; // Scheduled Calls - calls
	const COST_PER_SMS										= 3; // SMS
	const MONTHLY_COST_PER_CALL_TRACKING_PHONE_NUMBER		= 4; // Vanity Numbers - call phone numbers
	const PER_MINUTE_CALL_TRACKING_FEE						= 5; // Vanity Numbers logs - call logs
	const PER_MINUTE_SUPPORT_FEES							= 6; // Per Minute Support Fees

	public static function getPricingTypeNameById( $intId ) {

		switch( $intId ) {
			case self::RESIDENT_LEASE:
				$strName = 'Resident Lease';
				break;

			case self::COST_PER_VOIP_MINUTE:
				$strName = 'Cost Per Voip Minute';
				break;

			case self::COST_PER_SMS:
				$strName = 'Cost Per Sms';
				break;

			case self::MONTHLY_COST_PER_CALL_TRACKING_PHONE_NUMBER:
				$strName = 'Monthly Cost Per Call Tracking Phone Number';
				break;

			case self::PER_MINUTE_CALL_TRACKING_FEE:
				$strName = 'Per Minute Call Tracking Fee';
				break;

			default:
				$strName = '';
		}

		return $strName;
	}
}
?>