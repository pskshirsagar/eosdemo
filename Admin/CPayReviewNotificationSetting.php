<?php

class CPayReviewNotificationSetting extends CBasePayReviewNotificationSetting {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPayReviewYear() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotificationStartDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotificationEndDate() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApproverLevel() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valNotificationScheduleDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCountryCode() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>