<?php

class CKeyResultLink extends CBaseKeyResultLink {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOkrKeyResultId() {
		$boolIsValid = true;
		if( false == valId( $this->getOkrKeyResultId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'okr_key_result_id', 'Key Result id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		if( false == valStr( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Link name is required.' ) );
		}

		return $boolIsValid;
	}

	public function valLink() {
		$boolIsValid = true;
		if( false == valStr( $this->getLink() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'link', 'Link URL is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valOkrKeyResultId();
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valLink();
				break;

			case 'validate_key_result_link':
				$boolIsValid &= $this->valName();
				$boolIsValid &= $this->valLink();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
