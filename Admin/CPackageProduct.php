<?php

use Psi\Eos\Admin\CPackageProductsRelationships;
use Psi\Eos\Admin\CPsProductRelationships;
use Psi\Eos\Admin\CProductBundles;
use Psi\Eos\Admin\CPackageProductDiscounts;
use Psi\Eos\Admin\CContractProperties;
use Psi\Eos\Admin\CPsProducts;
use Psi\Eos\Admin\CProperties;
use Psi\Eos\Admin\CContractProducts;

class CPackageProduct extends CBasePackageProduct {

	protected $m_intPackagePropertyCount;
	protected $m_intPackageUnitCount;
	protected $m_strPackageProduct;
	protected $m_strPackageProductTitle;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsLeadId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL, $arrmixPackage = NULL, $arrmixDiscounts = NULL, $intPsLeadId = NULL, $boolIsGlobalPackage= false, $boolIsImportPackage = false, $intContractId = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid = $this->valTitle( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			case 'validate_request_data':
				$boolIsValid = $this->validateRequestData( $arrmixPackage, $arrmixDiscounts, $objDatabase );
				break;

			case 'validate_duplicate_package':
				$boolIsValid = $this->validateDuplicatePackage( $arrmixPackage, $intPsLeadId, $objDatabase, $boolIsGlobalPackage, $boolIsImportPackage, $intContractId );
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function validateRequestData( $arrmixPackage, $arrmixDiscounts, $objDatabase ) {

		// Package details validation

		if( !valStr( trim( $arrmixPackage['title'] ) ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Package Title is required.' ) );
		}

		if( !valId( $arrmixPackage['property_count'] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Number of Properties are required.' ) );
		}

		if( !valId( $arrmixPackage['unit_count'] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Number of Units are required.' ) );
		}

		if( $arrmixPackage['unit_count'] < $arrmixPackage['property_count'] ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Number of Units should be greater than or equal to the Number of Properties.' ) );
		}

		if( !isset( $arrmixPackage['line_items'] ) && !isset( $arrmixPackage['bundles'] ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Minimum one product or bundle is required in the package.' ) );
		}

		if( valArr( $this->getErrorMsgs() ) ) {
			return false;
		}

		// Products tab validation

		$arrintAllPsProductIds = [];

		$arrobjPsProducts			= ( array ) \Psi\Eos\Admin\CPsProducts::createService()->fetchPackagePsProducts( $objDatabase );
		$arrmixProductDependencies	= ( array ) \Psi\Eos\Admin\CProductDependencies::createService()->fetchProductsDependencies( $objDatabase );

		$arrintProductDependency	= ( isset( $arrmixProductDependencies['product_dependency'] ) && valArr( $arrmixProductDependencies['product_dependency'] ) ? $arrmixProductDependencies['product_dependency'] : [] );
		$arrboolBundleDependency	= ( isset( $arrmixProductDependencies['bundle_dependency'] ) && valArr( $arrmixProductDependencies['bundle_dependency'] ) ? $arrmixProductDependencies['bundle_dependency'] : [] );

		if( isset( $arrmixPackage['line_items'] ) && valArr( $arrmixPackage['line_items'] ) ) {

			foreach( $arrmixPackage['line_items'] as $intIndex => $intPsProductId ) {

				if( !valId( $intPsProductId ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Select a product in Line Items or remove the empty row added.' ) );
					continue;
				}

				if( isset( $arrmixPackage['payment_types'][$intIndex] ) && CContractProduct::PAYMENT_TYPE_MIN_MAX == $arrmixPackage['payment_types'][$intIndex] ) {
					if( !valId( $arrmixPackage['payment_min_units'][$intIndex] ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Min Units are required.' ) );
					}

					if( !valId( $arrmixPackage['payment_max_units'][$intIndex] ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Max Units are required.' ) );
					}

					if( $arrmixPackage['payment_min_units'][$intIndex] > $arrmixPackage['payment_max_units'][$intIndex] ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Min Units should be less than or equal to the Max Units.' ) );
					}
				}

				$arrintAllPsProductIds[] = $intPsProductId;

				if( array_key_exists( $intPsProductId, $arrintProductDependency ) && valId( $arrintProductDependency[$intPsProductId] ) && !in_array( $arrintProductDependency[$intPsProductId], $arrmixPackage['line_items'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'You are adding "%s" in Line Items, you also need to add "%s" in Line items.', $arrobjPsProducts[$intPsProductId]->getName(), $arrobjPsProducts[$arrintProductDependency[$intPsProductId]]->getName() ) ) );
				}
			}
		}

		if( isset( $arrmixPackage['bundles'] ) && valArr( $arrmixPackage['bundles'] ) ) {

			foreach( $arrmixPackage['bundles'] as $arrmixBundleDetails ) {

				if( !valstr( $arrmixBundleDetails['name'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Bundle Name is required.' ) );
					continue;
				}

				$strBundleName = trim( $arrmixBundleDetails['name'] );

				if( !isset( $arrmixBundleDetails['product_ids'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'Minimum one product is required in "%s".', $strBundleName ) ) );
					continue;
				}

				if( isset( $arrmixBundleDetails['type'] ) && CContractProduct::PAYMENT_TYPE_MIN_MAX == $arrmixBundleDetails['type'] ) {
					if( !valId( $arrmixBundleDetails['min_units'] ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Min Units are required.' ) );
					}

					if( !valId( $arrmixBundleDetails['max_units'] ) ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Max Units are required.' ) );
					}

					if( $arrmixBundleDetails['min_units'] > $arrmixBundleDetails['max_units'] ) {
						$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Min Units should be less than or equal to the Max Units.' ) );
					}
				}

				if( valArr( $arrmixBundleDetails['product_ids'] ) ) {

					foreach( $arrmixBundleDetails['product_ids'] as $intPsProductId ) {

						if( !valId( $intPsProductId ) ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'Select a product in "%s" or remove the empty row added.', $strBundleName ) ) );
							continue;
						}

						if( valObj( $arrobjPsProducts[$intPsProductId], 'CPsProduct' ) && !$arrobjPsProducts[$intPsProductId]->getAllowInBundle() ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'Product "%s" is not allowed in "%s".', $arrobjPsProducts[$intPsProductId]->getName(), $strBundleName ) ) );
							continue;
						}

						$arrintAllPsProductIds[] = $intPsProductId;

						if( array_key_exists( $intPsProductId, $arrboolBundleDependency ) && true == $arrboolBundleDependency[$intPsProductId] && !in_array( $arrintProductDependency[$intPsProductId], $arrmixBundleDetails['product_ids'] ) ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'You are adding "%s" in "%s", you also need to add "%s" in the bundle.', $arrobjPsProducts[$intPsProductId]->getName(), $strBundleName, $arrobjPsProducts[$arrintProductDependency[$intPsProductId]]->getName() ) ) );
						}
					}
				}
			}
		}

		if( valArr( $arrintAllPsProductIds ) ) {

			$arrintRepeatedPsProductIds = array_count_values( $arrintAllPsProductIds );

			array_walk( $arrintRepeatedPsProductIds, function ( $intRepeatedCount, $intPsProductId ) use ( &$arrobjPsProducts ) {

				if( 1 < $intRepeatedCount ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'Product "%s" is repeated in the package.', ( valObj( $arrobjPsProducts[$intPsProductId], 'CPsProduct' ) ? $arrobjPsProducts[$intPsProductId]->getName() : NULL ) ) ) );
				}
			} );
		}

		if( valArr( $this->getErrorMsgs() ) ) {
			return false;
		}

		// Discounts tab validation

		if( valArr( $arrmixDiscounts ) ) {

			$arrmixLineItemsTotal	= [];
			$arrmixBundlesTotal		= [];

			foreach( $arrmixDiscounts as $intIndex => $arrmixDiscount ) {

				if( !valId( $arrmixDiscount['applied_product_id'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Select a product or remove the empty discount row added.' ) );
				}

				if( !is_numeric( $arrmixDiscount['setup_percentage'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Setup percentage are required.' ) );
				}

				if( $arrmixDiscount['setup_percentage'] > 100 || $arrmixDiscount['setup_percentage'] < 0 ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Valid Setup percentage are required.' ) );
				}

				if( !is_numeric( $arrmixDiscount['recurring_percentage'] ) ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Recurring percentage are required.' ) );
				}

				if( $arrmixDiscount['recurring_percentage'] > 100 || $arrmixDiscount['recurring_percentage'] < 0 ) {
					$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Valid Recurring percentage are required.' ) );
				}

				if( valArr( $this->getErrorMsgs() ) ) {
					return false;
				}

				if( 'line_item' == $arrmixDiscount['applied_product_type'] ) {
					$arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['setup']		= $arrmixDiscount['setup_percentage'] + ( isset( $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['setup'] ) ? $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['setup'] : 0 );
					$arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['recurring']	= $arrmixDiscount['recurring_percentage'] + ( isset( $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['recurring'] ) ? $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['recurring'] : 0 );

					$arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['setup_available']		= ( isset( $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['setup_available'] ) ? $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['setup_available'] : $arrmixDiscount['setup_available'] );
					$arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['recurring_available']	= ( isset( $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['recurring_available'] ) ? $arrmixLineItemsTotal[$arrmixDiscount['applied_product_id']]['recurring_available'] : $arrmixDiscount['recurring_available'] );
				}

				if( 'bundle' == $arrmixDiscount['applied_product_type'] ) {
					$arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['setup']		= $arrmixDiscount['setup_percentage'] + ( isset( $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['setup'] ) ? $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['setup'] : 0 );
					$arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['recurring']	= $arrmixDiscount['recurring_percentage'] + ( isset( $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['recurring'] ) ? $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['recurring'] : 0 );

					$arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['setup_available']		= ( isset( $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['setup_available'] ) ? $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['setup_available'] : $arrmixDiscount['setup_available'] );
					$arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['recurring_available']	= ( isset( $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['recurring_available'] ) ? $arrmixBundlesTotal[$arrmixDiscount['applied_product_id']]['recurring_available'] : $arrmixDiscount['recurring_available'] );
				}
			}
		}

		return ( $this->getErrorMsgs() ? false : true );
	}

	public function validateDuplicatePackage( $arrmixPackage, $intPsLeadId, $objDatabase, $boolIsGlobalPackage, $boolIsImportPackage, $intContractId ) {

		if( !$boolIsImportPackage ) {

			$arrintLineItemsProductIds = [];
			$arrintBundleProductIds  = [];
			if( valArr( $arrmixPackage['line_items'] ) ) {
				$arrintLineItemsProductIds = $arrmixPackage['line_items'];
			}
			if( valArr( $arrmixPackage['bundles'] ) ) {
				foreach( $arrmixPackage['bundles'] as $arrmixBundle ) {
					foreach( $arrmixBundle['product_ids'] as $intBundlePsProductId ) {
						$arrintBundleProductIds[] = $intBundlePsProductId;
					}
				}
			}
			$arrintPackageProductIds = array_merge( $arrintLineItemsProductIds, $arrintBundleProductIds );
			$intPsProductsCount      = count( $arrintPackageProductIds );
		} else {
			$arrintPackageProductIds = explode( ',', $arrmixPackage[0]['products'] );
			$intPsProductsCount = count( $arrintPackageProductIds );
		}

		$arrmixExistingProductPackages = \Psi\Eos\Admin\CPackageProducts::createService()->fetchPackageProductsByPsLeadIdByContractId( $intPsLeadId, $objDatabase, $boolIsGlobalPackage, $intContractId, $boolIsImportPackage, NULL );

		if( valArr( $arrmixExistingProductPackages ) ) {
			foreach( $arrmixExistingProductPackages as $arrmixExistingProductPackage ) {
				$arrintExistingPackageProductIds = explode( ',', $arrmixExistingProductPackage['products'] );
				if( count( $arrintExistingPackageProductIds ) == $intPsProductsCount ) {
					sort( $arrintPackageProductIds );
					sort( $arrintExistingPackageProductIds );
					if( $arrintPackageProductIds == $arrintExistingPackageProductIds ) {
						if( $boolIsGlobalPackage ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'Package "%s" is already exist with same products.', $arrmixExistingProductPackage['title'] ) ) );
						} elseif( $intContractId == $arrmixExistingProductPackage['contract_id'] ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'Package "%s" is already exist with same products.', $arrmixExistingProductPackage['title'] ) ) );
						} elseif( $intContractId != $arrmixExistingProductPackage['contract_id'] ) {
							$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'Please import existing package "%s" which contains same products as selected.', $arrmixExistingProductPackage['title'] ) ) );
						} else {
							return true;
						}

						return false;
					}
				}
			}
		}
		return true;

	}

	/**
	 * Function to import global package into a contract
	 */
	public function importPackageProduct( $intUserId, $objTargetContract, $objDatabase ) {

		if( valId( $this->getParentPackageProductId() ) ) {
			$this->addErrorMsg( 'Package must be a global package.' );
			return false;
		}
		$objPackageProduct = $this->createClone( $intUserId, $objDatabase );

		list( $objPackageProduct, $arrobjProductBundles, $arrobjPsProductRelationships, $arrobjPackageProductsRelationships, $arrobjPackageProductDiscounts, $arrobjContractProducts ) = $this->createContractProducts( $intUserId, $objTargetContract, $objDatabase, $objPackageProduct, NULL, NULL );

		switch( NULL ) {
			default:

				if( !$objPackageProduct->validate( VALIDATE_INSERT, $objDatabase ) || !$objPackageProduct->insert( $intUserId, $objDatabase ) ) {
					$this->addErrorMsg( getConsolidatedErrorMsg( [ $objPackageProduct ], ' ' ) );
					break;
				}

				if( valArr( $arrobjProductBundles ) && !CProductBundles::createService()->bulkInsert( $arrobjProductBundles, $intUserId, $objDatabase ) ) {
					$this->addErrorMsg( getConsolidatedErrorMsg( $arrobjProductBundles, ' ' ) );
					break;
				}

				if( valArr( $arrobjPsProductRelationships ) && !CPsProductRelationships::createService()->bulkInsert( $arrobjPsProductRelationships, $intUserId, $objDatabase ) ) {
					$this->addErrorMsg( getConsolidatedErrorMsg( $arrobjPsProductRelationships, ' ' ) );
					break;
				}

				if( valArr( $arrobjPackageProductsRelationships ) && !CPackageProductsRelationships::createService()->bulkInsert( $arrobjPackageProductsRelationships, $intUserId, $objDatabase ) ) {
					$this->addErrorMsg( getConsolidatedErrorMsg( $arrobjPackageProductsRelationships, ' ' ) );
					break;
				}

				if( valArr( $arrobjPackageProductDiscounts ) && !CPackageProductDiscounts::createService()->bulkInsert( $arrobjPackageProductDiscounts, $intUserId, $objDatabase ) ) {
					$this->addErrorMsg( getConsolidatedErrorMsg( $arrobjPackageProductDiscounts, ' ' ) );
					break;
				}

				if( valArr( $arrobjContractProducts ) && !CContractProducts::createService()->bulkInsert( $arrobjContractProducts, $intUserId, $objDatabase ) ) {
					$this->addErrorMsg( getConsolidatedErrorMsg( $arrobjContractProducts, ' ' ) );
					break;
				}
		}

		return valArr( $this->getErrorMsgs() ) ? false : true;
	}

	public function applyPackageToProperties( $arrintPropertyIds, $intUserId, $objDatabase ) {

		$boolApplied = true;

		foreach( $arrintPropertyIds as $intPropertyId ) {
			$boolApplied &= $this->applyPackageToProperty( $intPropertyId, $intUserId, $objDatabase );
		}

		return $boolApplied;
	}

	public function applyPackageToProperty( $intPropertyId, $intUserId, $objDatabase ) {

		$arrstrFieldsToUpdate = [
			'monthly_recurring_amount',
			'monthly_change_amount',
			'implementation_amount'
		];

		$arrobjExistingContractProperties	= ( array ) CContractProperties::createService()->fetchActiveContractPropertiesByPropertyId( $intPropertyId, $objDatabase );
		$arrobjPackageContractProducts		= ( array ) CContractProducts::createService()->fetchContractProductsByPackageProductId( $this->getId(), $objDatabase );

		$arrobjProductWiseExistingContractProperties = rekeyObjects( 'PsProductId', $arrobjExistingContractProperties );

		if( !valArr( $arrobjPackageContractProducts ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'No any Line Items / Bundles found in the package.' ) );
			return false;
		}

		$intCid = reset( $arrobjPackageContractProducts )->getCid();

		$arrobjPsProducts	= CPsProducts::createService()->fetchAllPsProducts( $objDatabase );
		$objProperty		= CProperties::createService()->fetchPropertyByIdByCid( $intPropertyId, $intCid, $objDatabase );

		$arrobjInsertContractProperties	= [];
		$arrobjUpdateContractProperties	= [];
		$arrobjDeleteContractProperties	= [];

		foreach( $arrobjPackageContractProducts as $objContractProduct ) {

			$objContractProperty = new CContractProperty();

			if( valObj( $arrobjProductWiseExistingContractProperties[$objContractProduct->getPsProductId()], 'CContractProperty' )
			    && $arrobjProductWiseExistingContractProperties[$objContractProduct->getPsProductId()]->getPackageProductId() != $objContractProduct->getPackageProductId() ) {
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, sprintf( 'Product %s is already associated with a property %s outside of this package.', $arrobjPsProducts[$objContractProduct->getPsProductId()]->getName(), $objProperty->getPropertyName() ) ) );
				continue;
			}

			foreach( $arrobjExistingContractProperties as $objExistingContractProperty ) {
				if( $objContractProduct->getPackageProductId() == $objExistingContractProperty->getPackageProductId()
					&& $objContractProduct->getBundlePsProductId() == $objExistingContractProperty->getBundlePsProductId()
				    && $objContractProduct->getPsProductId() == $objExistingContractProperty->getPsProductId() ) {
					$objContractProperty = $objExistingContractProperty;
					break;
				}
			}

			$objContractProperty->setCid( $objContractProduct->getCid() );
			$objContractProperty->setContractId( $objContractProduct->getContractId() );
			$objContractProperty->setCurrencyCode( $objContractProduct->getCurrencyCode() );

			$objContractProperty->setPsProductId( $objContractProduct->getPsProductId() );
			$objContractProperty->setBundlePsProductId( $objContractProduct->getBundlePsProductId() );
			$objContractProperty->setPackageProductId( $objContractProduct->getPackageProductId() );
			$objContractProperty->setPropertyId( $intPropertyId );
			$objContractProperty->setCommissionBucketId( $objContractProduct->getCommissionBucketId() );

			$arrmixContractProductDetails = json_decode( $objContractProduct->getDetails(), true );

			$intPaymentTypeId	= $arrmixContractProductDetails['payment_type_id'] ?? CContractProduct::PAYMENT_TYPE_PER_UNIT;
			$intMinUnits		= $arrmixContractProductDetails['min_units'] ?? 1;
			$intMaxUnits		= $arrmixContractProductDetails['max_units'] ?? 1;

			switch( $intPaymentTypeId ) {

				case CContractProduct::PAYMENT_TYPE_PER_PROPERTY:
					$intNumberOfUnits = 1;
					break;

				case CContractProduct::PAYMENT_TYPE_MIN_MAX:
					$intNumberOfUnits = $objProperty->getNumberOfUnits();

					if( $objProperty->getNumberOfUnits() <= $intMinUnits ) {
						$intNumberOfUnits = $intMinUnits;
					} elseif( $objProperty->getNumberOfUnits() >= $intMaxUnits ) {
						$intNumberOfUnits = $intMaxUnits;
					}
					break;

				default:
					$intNumberOfUnits = $objProperty->getNumberOfUnits();
			}

			$objContractProperty->setMonthlyRecurringAmount( $objContractProduct->getRecurringAmount() * $intNumberOfUnits );
			$objContractProperty->setMonthlyChangeAmount( $objContractProduct->getRecurringAmount() * $intNumberOfUnits );
			$objContractProperty->setImplementationAmount( $objContractProduct->getImplementationAmount() );
			$objContractProperty->setIsUnlimited( $objContractProduct->getIsUniversal() );

			if( valId( $objContractProperty->getId() ) ) {
				$arrobjUpdateContractProperties[] = $objContractProperty;
			} else {
				$arrobjInsertContractProperties[] = $objContractProperty;
			}
		}

		foreach( $arrobjExistingContractProperties as $objExistingContractProperty ) {

			$boolExists = false;

			if( !valId( $objExistingContractProperty->getPackageProductId() ) ) {
				continue;
			}

			foreach( $arrobjPackageContractProducts as $objContractProduct ) {
				if( $objContractProduct->getPackageProductId() == $objExistingContractProperty->getPackageProductId()
					&& $objContractProduct->getBundlePsProductId() == $objExistingContractProperty->getBundlePsProductId()
				    && $objContractProduct->getPsProductId() == $objExistingContractProperty->getPsProductId() ) {
					$boolExists = true;
					break;
				}
			}

			if( !$boolExists && $objExistingContractProperty->getPackageProductId() == $this->getId() ) {
				$arrobjDeleteContractProperties[] = $objExistingContractProperty;
			}
		}

		if( valArr( $this->getErrorMsgs() ) ) {
			return false;
		}

		if( valArr( $arrobjInsertContractProperties ) && !CContractProperties::createService()->bulkInsert( $arrobjInsertContractProperties, $intUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to insert contract properties.' ) );
			return false;
		}

		if( valArr( $arrobjUpdateContractProperties ) && !CContractProperties::createService()->bulkUpdate( $arrobjUpdateContractProperties, $arrstrFieldsToUpdate, $intUserId, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to update contract properties.' ) );
			return false;
		}

		if( valArr( $arrobjDeleteContractProperties ) && !CContractProperties::createService()->bulkDelete( $arrobjDeleteContractProperties, $objDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to delete contract properties.' ) );
			return false;
		}

		return true;
	}

	public function createClone( $intUserId, $objDatabase ) {

		$objPackageProduct = clone $this;

		$objPackageProduct->setId( $objPackageProduct->fetchNextId( $objDatabase ) );
		$objPackageProduct->setParentPackageProductId( $this->getId() );
		$objPackageProduct->setCreatedBy( $intUserId );
		$objPackageProduct->setCreatedOn( 'NOW()' );
		$objPackageProduct->setUpdatedBy( $intUserId );
		$objPackageProduct->setUpdatedOn( 'NOW()' );

		return $objPackageProduct;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		// Custom Data
		if( isset( $arrmixValues['package_property_count'] ) ) {
			$this->setPackagePropertyCount( trim( $arrmixValues['package_property_count'] ) );
		}

		if( isset( $arrmixValues['package_unit_count'] ) ) {
			$this->setPackageUnitCount( trim( $arrmixValues['package_unit_count'] ) );
		}

		if( true == isset( $arrmixValues['title'] ) && 0 < strlen( trim( $arrmixValues['title'] ) ) ) {
			$this->setPackageProductTitle( $arrmixValues['title'] );
		}
		if( true == isset( $arrmixValues['id'] ) ) $this->setPackageProductId( $arrmixValues['id'] );
	}

	public function setPackagePropertyCount( $intPackagePropertyCount ) {
		$this->m_intPackagePropertyCount = $intPackagePropertyCount;
	}

	public function setPackageUnitCount( $intPackageUnitCount ) {
		$this->m_intPackageUnitCount = $intPackageUnitCount;
	}

	public function setPackageProductId( $intPackageProductId ) {
		$this->m_strPackageProduct = $intPackageProductId;
	}

	public function setPackageProductTitle( $strTitle ) {
		$this->m_strPackageProductTitle = $strTitle;
	}

	public function getPackagePropertyCount() {
		return $this->m_intPackagePropertyCount;
	}

	public function getPackageUnitCount() {
		return $this->m_intPackageUnitCount;
	}

	public function getPackageProductTitle() {
		return $this->m_strPackageProductTitle;
	}

	public function getPackageProductId() {
		return $this->m_strPackageProduct;
	}

	/**
	 * Function to insert data in Contract Products.
	 */
	public function createContractProducts( $intUserId, $objTargetContract, $objDatabase, $objPackageProduct, $boolIsFromCreateProductPackage = NULL, $arrobjInsertPackageProductsRelationships = NULL, $arrobjInsertPackageProductDiscounts = NULL, $arrmixPackage = NULL ) {

		$arrobjContractProducts				= [];
		$arrobjPackageProductsRelationships	= [];
		$arrobjPackageProductDiscounts      = [];
		$arrobjProductBundles				= [];
		$arrobjPsProductRelationships		= [];

		$arrobjPsProducts				= CPsProducts::createService()->fetchPackagePsProducts( $objDatabase );
		$arrintUnlimitedPsProductIds	= array_keys( rekeyArray( 'ps_product_id', ( array ) CContractProperties::createService()->fetchAllUnlimitedPsProductsByCid( $objTargetContract->getCid(), $objDatabase ) ) );

		if( $boolIsFromCreateProductPackage ) {
			$intPackageProductId = $objPackageProduct->getId();
		} else {
			$intPackageProductId = $this->getId();
		}

		if( !valArr( $arrobjInsertPackageProductsRelationships ) ) {
			$arrobjParentPackageProductsRelationships = ( array ) CPackageProductsRelationships::createService()->fetchActivePackageProductsRelationshipsByPackageProductId( $intPackageProductId, $objDatabase );
		} else {
			$arrobjParentPackageProductsRelationships = $arrobjInsertPackageProductsRelationships;
		}

		if( valArr( $arrmixPackage['line_items'] ) ) {
			foreach( $arrmixPackage['line_items'] as $intLineItem ) {
				$arrmixLineItemPsProductIds[]['product_id'] = $intLineItem;
			}
		}

		foreach( $arrobjParentPackageProductsRelationships as $objParentPackageProductsRelationship ) {

			$objPackageProductsRelationship = $objParentPackageProductsRelationship->createClone( $objPackageProduct->getId(), $intUserId, $objDatabase );

			$arrobjParentPackageProductDiscounts = ( array ) CPackageProductDiscounts::createService()->fetchActivePackageProductDiscountsByPackageProductRelationshipId( $objParentPackageProductsRelationship->getId(), $objDatabase );

			$fltFinalRecurringDiscount	= 0;
			$fltFinalSetupDiscount		= 0;

			if( valArr( $arrobjParentPackageProductDiscounts ) ) {

				foreach( $arrobjParentPackageProductDiscounts as $objParentPackageProductDiscount ) {
					$objPackageProductDiscount       = $objParentPackageProductDiscount->createClone( $intUserId, $objPackageProductsRelationship->getId() );
					$arrobjPackageProductDiscounts[] = $objPackageProductDiscount;
					$fltFinalRecurringDiscount       += $objPackageProductDiscount->getRecurringDiscountPercentage();
					$fltFinalSetupDiscount           += $objPackageProductDiscount->getSetupDiscountPercentage();
				}

			} elseif( valArr( $arrobjInsertPackageProductDiscounts ) ) {

				$arrobjParentPackageProductDiscounts = $arrobjInsertPackageProductDiscounts;

				$arrobjRekeyedPackageProductDiscounts = rekeyObjects( 'PackageProductRelationshipId', $arrobjParentPackageProductDiscounts );

				if( array_key_exists( $objParentPackageProductsRelationship->getId(), $arrobjRekeyedPackageProductDiscounts ) ) {
					$fltFinalRecurringDiscount = $arrobjRekeyedPackageProductDiscounts[$objParentPackageProductsRelationship->getId()]->getRecurringDiscountPercentage();
					$fltFinalSetupDiscount     = $arrobjRekeyedPackageProductDiscounts[$objParentPackageProductsRelationship->getId()]->getSetupDiscountPercentage();
				}
			}

			if( valId( $objParentPackageProductsRelationship->getBundlePsProductId() ) ) {

				$objParentProductBundle = CProductBundles::createService()->fetchProductBundleById( $objParentPackageProductsRelationship->getBundlePsProductId(), $objDatabase );
				if( !$boolIsFromCreateProductPackage ) {
					$objProductBundle = $objParentProductBundle->createClone( $intUserId, $objDatabase );
					$objPackageProductsRelationship->setBundlePsProductId( $objProductBundle->getId() );
				} else {
					$objPackageProductsRelationship->setBundlePsProductId( $objParentPackageProductsRelationship->getBundlePsProductId() );
				}

				$arrobjParentPsProductRelationships = CPsProductRelationships::createService()->fetchPsProductRelationshipsByBundlePsProductId( $objParentProductBundle->getId(), $objDatabase );

				foreach( $arrobjParentPsProductRelationships as $objParentPsProductRelationship ) {

					if( !$boolIsFromCreateProductPackage ) {
						$objPsProductRelationship = $objParentPsProductRelationship->createClone( $intUserId, $objProductBundle->getId() );
						$arrobjPsProductRelationships[] = $objPsProductRelationship;
					}

					$intPsProductId = $objParentPsProductRelationship->getPsProductId();

					$objContractProduct = $objTargetContract->createPackageContractProduct( [
						'package_product_id' => $objPackageProduct->getId(),
						'is_universal' => ( in_array( $intPsProductId, $arrintUnlimitedPsProductIds ) ? 1 : 0 ),
						'ps_product_id' => $intPsProductId,
						'bundle_ps_product_id' => $boolIsFromCreateProductPackage ? $objParentPackageProductsRelationship->getBundlePsProductId() : $objProductBundle->getId(),
						'property_count' => $arrmixPackage['property_count'] ? $arrmixPackage['property_count'] : NULL,
						'unit_count' => $arrmixPackage['unit_count'] ? $arrmixPackage['unit_count'] : NULL,
					] );

					if( valObj( $arrobjPsProducts[$intPsProductId], 'CPsProduct' ) ) {
						$fltRecurringAmount = $arrobjPsProducts[$intPsProductId]->getUnitMonthlyRecurring() - ( $arrobjPsProducts[$intPsProductId]->getUnitMonthlyRecurring() * $fltFinalRecurringDiscount / 100 );
						$fltPropertySetup = $arrobjPsProducts[$intPsProductId]->getPropertySetup() - ( $arrobjPsProducts[$intPsProductId]->getPropertySetup() * $fltFinalSetupDiscount / 100 );

						$objContractProduct->setRecurringAmount( $fltRecurringAmount );
						$objContractProduct->setImplementationAmount( $fltPropertySetup );
						$objContractProduct->setTransactionalAmount( $arrobjPsProducts[$intPsProductId]->getUnitTransactionalAverage() );

						if( $boolIsFromCreateProductPackage ) {
							$objContractProduct->setEstimatedPropertyCount( $arrmixPackage['property_count'] );
							$objContractProduct->setEstimatedUnitCount( $arrmixPackage['unit_count'] );
							if( isset( $arrmixPackage['bundles'] ) && valArr( $arrmixPackage['bundles'] ) ) {
								foreach( $arrmixPackage['bundles'] as $arrmixBundle ) {
									if( in_array( $intPsProductId, $arrmixBundle['product_ids'] ) ) {
										$objContractProduct->setMinUnits( ( CContractProduct::PAYMENT_TYPE_MIN_MAX == $arrmixBundle['type'] ? $arrmixBundle['min_units'] : NULL ) );
										$objContractProduct->setMaxUnits( ( CContractProduct::PAYMENT_TYPE_MIN_MAX == $arrmixBundle['type'] ? $arrmixBundle['max_units'] : NULL ) );
										$objContractProduct->setDetails( json_encode( [ 'payment_type_id' => $arrmixBundle['type'] ] ) );
									}
								}
							}
						} else {
							$objContractProduct->setDetails( json_encode( [ 'payment_type_id' => CContractProduct::PAYMENT_TYPE_PER_UNIT ] ) );
						}
					}

					$arrobjContractProducts[] = $objContractProduct;
				}
				if( !$boolIsFromCreateProductPackage ) {
					$arrobjProductBundles[] = $objProductBundle;
				}

			} else {

				$intPsProductId = $objPackageProductsRelationship->getPsProductId();

				$objContractProduct = $objTargetContract->createPackageContractProduct( [
					'package_product_id' => $objPackageProduct->getId(),
					'is_universal' => ( in_array( $intPsProductId, $arrintUnlimitedPsProductIds ) ? 1 : 0 ),
					'ps_product_id' => $intPsProductId,
					'property_count' => $arrmixPackage['property_count'] ? $arrmixPackage['property_count'] : NULL,
					'unit_count' => $arrmixPackage['unit_count'] ? $arrmixPackage['unit_count'] : NULL,
					'payment_type' => $arrmixPackage['unit_count'] ? $arrmixPackage['unit_count'] : NULL,
				] );

				if( $boolIsFromCreateProductPackage && valArr( $arrmixLineItemPsProductIds ) ) {
					foreach( $arrmixLineItemPsProductIds as $intIndex => $arrmixLineItemPsProductId ) {
						if( $intPsProductId == $arrmixLineItemPsProductId['product_id'] ) {
							$objContractProduct->setMinUnits( ( CContractProduct::PAYMENT_TYPE_MIN_MAX == $arrmixPackage['payment_types'][$intIndex] ? $arrmixPackage['payment_min_units'][$intIndex] : NULL ) );
							$objContractProduct->setMaxUnits( ( CContractProduct::PAYMENT_TYPE_MIN_MAX == $arrmixPackage['payment_types'][$intIndex] ? $arrmixPackage['payment_max_units'][$intIndex] : NULL ) );
							$objContractProduct->setDetails( json_encode( [ 'payment_type_id' => $arrmixPackage['payment_types'][$intIndex] ] ) );
						}
					}
				} else {
					$objContractProduct->setDetails( json_encode( [ 'payment_type_id' => CContractProduct::PAYMENT_TYPE_PER_UNIT ] ) );
				}

				if( valObj( $arrobjPsProducts[$intPsProductId], 'CPsProduct' ) ) {
					$fltRecurringAmount = $arrobjPsProducts[$intPsProductId]->getUnitMonthlyRecurring() - ( $arrobjPsProducts[$intPsProductId]->getUnitMonthlyRecurring() * $fltFinalRecurringDiscount / 100 );
					$fltPropertySetup = $arrobjPsProducts[$intPsProductId]->getPropertySetup() - ( $arrobjPsProducts[$intPsProductId]->getPropertySetup() * $fltFinalSetupDiscount / 100 );

					$objContractProduct->setRecurringAmount( $fltRecurringAmount );
					$objContractProduct->setImplementationAmount( $fltPropertySetup );
					$objContractProduct->setTransactionalAmount( $arrobjPsProducts[$intPsProductId]->getUnitTransactionalAverage() );
				}

				$arrobjContractProducts[] = $objContractProduct;
			}
			if( !$boolIsFromCreateProductPackage ) {
				$arrobjPackageProductsRelationships[] = $objPackageProductsRelationship;
			}
		}
		if( !$boolIsFromCreateProductPackage ) {

			return array( $objPackageProduct, $arrobjProductBundles, $arrobjPsProductRelationships, $arrobjPackageProductsRelationships, $arrobjPackageProductDiscounts, $arrobjContractProducts );
		} else {
			return $arrobjContractProducts;
		}
	}

}
?>