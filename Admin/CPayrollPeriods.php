<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPayrollPeriods
 * Do not add any new functions to this class.
 */

class CPayrollPeriods extends CBasePayrollPeriods {

	public static function fetchPayrollPeriodsByYearByCountryCode( $strYear, $strCountryCode, $objDatabase ) {

		if( CCountry::CODE_INDIA == $strCountryCode ) {
			$strWhereClause = 'ORDER BY id DESC';
		} else {
			$strWhereClause = NULL;
		}

		return self::fetchPayrollPeriods( 'SELECT * FROM payroll_periods WHERE begin_date <= \'12/31/' . $strYear . '\' AND end_date >= \'01/01/' . $strYear . '\' AND country_code = \'' . $strCountryCode . '\' ' . $strWhereClause . '', $objDatabase );
	}

	public static function fetchLatestPayrollPeriodByCountryCode( $strCountryCode, $objDatabase ) {
		return self::fetchPayrollPeriod( 'SELECT * FROM payroll_periods WHERE country_code = \'' . $strCountryCode . '\' ORDER BY created_on desc LIMIT 1', $objDatabase );
	}

	public static function fetchAllPayrollPeriodsByCountryCode( $strCountryCode, $objDatabase, $intLimit = NULL ) {

		$strLimitClause = ( false == is_null( $intLimit ) ) ? ' OFFSET 0 LIMIT ' . ( int ) $intLimit : '';

		$strSql = 'SELECT * FROM payroll_periods WHERE country_code = \'' . $strCountryCode . '\' ORDER BY begin_date DESC' . $strLimitClause;

		return self::fetchPayrollPeriods( $strSql, $objDatabase );
	}

	public static function fetchPreviousPayrollPeriodByIdByCountryCode( $intId, $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT
					  *
				  FROM
					  payroll_periods
				  WHERE
					  id < ' . ( int ) $intId . '
					  AND country_code = \'' . $strCountryCode . '\'
				  ORDER BY
					  id DESC
				  LIMIT
					  1';

		return parent::fetchPayrollPeriod( $strSql, $objDatabase );
	}

	public static function fetchNextPayrollPeriodByIdByCountryCode( $intId, $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT
					  *
				  FROM
					  payroll_periods
				  WHERE
					  id > ' . ( int ) $intId . '
					  AND country_code = \'' . $strCountryCode . '\'
				  ORDER BY
					  id ASC
				  LIMIT
					  1';

		return parent::fetchPayrollPeriod( $strSql, $objDatabase );
	}

	public static function fetchPayrollPeriodsByIdsByCountryCode( $arrintPayrollPeriodIds, $strCountryCode, $objDatabase ) {

		$strSql = 'SELECT
					  *
				  FROM
					  payroll_periods
				  WHERE
					  id IN( ' . implode( ',', $arrintPayrollPeriodIds ) . ' )
					  AND country_code = \'' . $strCountryCode . '\'
				  ORDER BY
					  id ASC';

		return parent::fetchPayrollPeriods( $strSql, $objDatabase );
	}

	public static function fetchPayrollEmployeesReimbursementRequestByPayrollId( $arrintPayrollIds, $objDatabase ) {

		if( false == valArr( $arrintPayrollIds ) ) return NULL;

		$strSql = 'SELECT
						e.payroll_remote_primary_key,
						e.preferred_name,
						eet.adjust_ded_code,
						re.reimbursement_request_id,
						re.amount_encrypted
					FROM
						payroll_periods pp
						JOIN reimbursement_requests rr ON pp.id = rr.payroll_period_id
						JOIN employees e ON e.id = rr.requested_by
						JOIN reimbursement_expenses re ON re.reimbursement_request_id = rr.id
						JOIN employee_expense_types eet ON eet.id = re.employee_expense_type_id
					WHERE
						pp.id IN ( ' . implode( ',', $arrintPayrollIds ) . ' )
						AND re.deleted_by IS NULL
					ORDER BY e.preferred_name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchCustomPayrollPeriodsByCountryCode( $strCountryCode, $intFuturePayrollPeriodLimit, $intPreviousPayrollPeriodLimit, $objDatabase ) {

				if( false == valStr( $strCountryCode ) ) return NULL;

				$strCurrentDate = date( 'm/d/Y' );

				$strFuturePayrollPeriodLimitClause		= ( false == is_null( $intFuturePayrollPeriodLimit ) ) ? ' OFFSET 0 LIMIT ' . ( int ) $intFuturePayrollPeriodLimit : '';
				$strPreviousPayrollPeriodLimitClause	= ( false == is_null( $intPreviousPayrollPeriodLimit ) ) ? ' OFFSET 0 LIMIT ' . ( int ) $intPreviousPayrollPeriodLimit : '';

				$strSql = '
							(
								SELECT
									DISTINCT (pp.id),
									\'Pay Period Ending \'|| trim(to_char(pp.end_date, \'Month\') ) || \' \' || to_char(pp.end_date,\'dd&#44; yyyy\' ) AS end_date,
									pp.end_date AS period_end_date
								FROM
									payroll_periods AS pp
									JOIN reimbursement_requests AS rr ON (pp.id = rr.payroll_period_id)
								WHERE
									pp.end_date >= \'' . $strCurrentDate . '\'
									AND pp.country_code = \'' . $strCountryCode . '\'
								ORDER BY
									pp.end_date
									' . $strFuturePayrollPeriodLimitClause . '
							)
							UNION ALL
							(
								SELECT
									DISTINCT (pp.id),
									\'Pay Period Ending \'|| trim(to_char(pp.end_date, \'Month\') ) || \' \' || to_char(pp.end_date,\'dd&#44; yyyy\' ) AS end_date,
									pp.end_date AS period_end_date
								FROM
									payroll_periods AS pp
									JOIN reimbursement_requests AS rr ON (pp.id = rr.payroll_period_id)
								WHERE
									pp.end_date <= \'' . $strCurrentDate . '\'
									AND pp.country_code = \'' . $strCountryCode . '\'
								ORDER BY
									pp.end_date DESC
									' . $strPreviousPayrollPeriodLimitClause . ' ) ';

				return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPayrollPeriodsForAssigningToRequestsByCountryCode( $strCountryCode, $intFuturePayrollPeriodLimit, $objDatabase ) {

		if( false == valStr( $strCountryCode ) ) return NULL;

		$strFuturePayrollPeriodLimitClause	= ( false == is_null( $intFuturePayrollPeriodLimit ) ) ? ' OFFSET 0 LIMIT ' . ( int ) $intFuturePayrollPeriodLimit : '';

		$strSql = '
					(
						SELECT
							subqry.*
						FROM
							(
								SELECT
									DISTINCT (id),
									end_date
								FROM
									payroll_periods
								WHERE
									end_date::date < NOW()::date
									AND country_code = \'' . $strCountryCode . '\'
								ORDER BY
									end_date DESC
								LIMIT 2
							) AS subqry
						ORDER BY
							subqry.id
					)
					UNION ALL
					(
						SELECT
							DISTINCT (id),
							end_date
						FROM
							payroll_periods
						WHERE
							end_date::date >= NOW()::date
							AND country_code = \'' . $strCountryCode . '\'
						ORDER BY
							end_date
							' . $strFuturePayrollPeriodLimitClause . '
					) ';

		return self::fetchPayrollPeriods( $strSql, $objDatabase );
	}

	public static function fetchNextFuturePayrollPeriodsFromCurrentDateByCountryCode( $strCountryCode, $intLimit = NULL, $objDatabase, $boolFromPaidHoliday = false ) {

		if( false == valStr( $strCountryCode ) ) return NULL;

		$strCurrentDate = date( 'm/d/Y' );

		$strLimitClause = ( false == is_null( $intLimit ) ) ? ' OFFSET 0 LIMIT ' . ( int ) $intLimit : '';
		$strJoinClause	= ( false == $boolFromPaidHoliday ) ? 'JOIN reimbursement_requests AS rr ON (pp.id = rr.payroll_period_id)' : '';

		$strSql = 'SELECT
					DISTINCT (pp.id),
					pp.begin_date,
					pp.end_date,
					pp.hours_expected
			 FROM
				payroll_periods AS pp
				' . $strJoinClause . '
			 WHERE
					pp.end_date >= \'' . $strCurrentDate . '\'
					AND pp.country_code = \'' . $strCountryCode . '\'
			 ORDER BY
					pp.end_date
			 ' . ( string ) $strLimitClause;

		return ( true == $boolFromPaidHoliday ) ? self::fetchPayrollPeriods( $strSql, $objDatabase ) : fetchData( $strSql, $objDatabase );
	}

}
?>