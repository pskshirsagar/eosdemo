<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStorageObjects
 * Do not add any new functions to this class.
 */

class CStorageObjects extends CBaseStorageObjects {

	public static function fetchAllPaginatedStorageObjects( $intPageNo, $intPageSize, $intStorageType, $objDatabase, $strOrderByField = NULL, $strOrderByType = NULL ) {
		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit  = ( int ) $intPageSize;

		if( false == is_null( $strOrderByField ) ) {
			$strOrderBy = ' ORDER BY ' . addslashes( $strOrderByField );

			if( false == is_null( $strOrderByType ) ) {
				$strOrderBy .= ' ' . addslashes( $strOrderByType );
			} else {
				$strOrderBy .= ' ASC';
			}
		}

		if( false == is_null( $intStorageType ) ) {
			if( CStorageCleanupsManager::ONE_TIME_CLEANUP == $intStorageType ) {
				$strOrderBy        = ' ORDER BY folder_path ASC';
				$strWhereCondition = ' where ' . ' s. is_one_time_cleanup = true';
			} elseif( CStorageCleanupsManager::TEST_CLIENT_CLEANUP == $intStorageType ) {
				$strOrderBy        = ' ORDER BY folder_path ASC';
				$strWhereCondition = ' where ' . ' s. is_one_time_cleanup = false AND s. storage_type_id = 4';
			} else {
				$strOrderBy        = ' ORDER BY updated_on DESC';
				$strWhereCondition = ' where ' . ' s. is_one_time_cleanup = false AND s. storage_type_id = ' . ( int ) $intStorageType;
			}
		}

		$strSql = ' SELECT
						*
					FROM
						storage_objects s '
		          . $strWhereCondition
		          . $strOrderBy . ' OFFSET ' . ( int ) $intOffset . '
					LIMIT ' . $intLimit;
		return parent::fetchStorageObjects( $strSql, $objDatabase );
	}

	public static function fetchAllPaginatedStorageObjectsCount( $intStorageType, $objDatabase ) {

		if( false == is_null( $intStorageType ) ) {
			if( CStorageCleanupsManager::ONE_TIME_CLEANUP == $intStorageType ) {
				$strWhereCondition = ' where ' . ' s. is_one_time_cleanup = true';
			} elseif( CStorageCleanupsManager::TEST_CLIENT_CLEANUP == $intStorageType ) {
				$strWhereCondition = ' where ' . ' s. is_one_time_cleanup = false AND s. storage_type_id = 4';
			} else {
				$strWhereCondition = ' where ' . ' s. is_one_time_cleanup = false AND s. storage_type_id = ' . ( int ) $intStorageType;
			}
		}

		$strSql = ' SELECT
						count( s.* )
					FROM
						storage_objects s'
		          . $strWhereCondition;

		$arrintResponse = fetchData( $strSql, $objDatabase );

		$intCount = 0;

		if( true == isset( $arrintResponse[0]['count'] ) )
			$intCount = $arrintResponse[0]['count'];

		return $intCount;
	}

	public static function fetchStorageObjectsByIds( $arrintStorageObjectsIds, $objDatabase ) {

		if( false == valArr( $arrintStorageObjectsIds ) ) return NULL;
		return self::fetchStorageObjects( 'SELECT * FROM storage_objects WHERE id IN ( ' . implode( ',', $arrintStorageObjectsIds ) . ' )', $objDatabase );
	}

	public static function fetchStorageObjectByFolderNameByParentFolderId( $strFolderName, $intParentFolderId, $intStorageType, $objDatabase ) {

		if( true == is_null( $intParentFolderId ) ) {
			$strWhereCondition = 'parent_storage_object_id IS NULL';
		} else {
			$strWhereCondition = 'parent_storage_object_id = ' . ( int ) $intParentFolderId;
		}

		$strSql = "SELECT * FROM storage_objects WHERE folder_name = '" . $strFolderName . "' AND " . $strWhereCondition . ' AND storage_type_id = ' . ( int ) $intStorageType;

		return self::fetchStorageObjects( $strSql, $objDatabase );
	}

	public static function fetchStorageObjectByFolderNameByStorageTypeId( $strFolderName,  $intStorageTypeId, $objDatabase ) {

		$strSql = "SELECT * FROM storage_objects WHERE folder_name = '" . $strFolderName . "' AND storage_type_id = " . ( int ) $intStorageTypeId;

		return self::fetchStorageObject( $strSql, $objDatabase );
	}

	public static function fetchStorageObjectsByIsApprovedByIsRequireCleanupByCleanupCommand( $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						storage_objects s 
					where 
						s.is_approved = TRUE 
						AND s.cleanup_command IS NOT NULL 
						AND s.is_require_cleanup = TRUE 
						ORDER BY s.updated_on DESC ';

		return parent::fetchStorageObjects( $strSql, $objDatabase );
	}

	public static function fetchStorageObjectsByIsApprovedByIsRequireCleanupByCleanupCommandByIsOneTimeCleanup( $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						storage_objects s 
					where 
						s.is_approved = TRUE 
						AND s.cleanup_command IS NOT NULL 
						AND s.is_require_cleanup = TRUE 
						AND s.is_one_time_cleanup = TRUE 
						ORDER BY s.updated_on DESC ';

		return parent::fetchStorageObjects( $strSql, $objDatabase );
	}

	public static function putStorageObject( $intId, $intCleanStartDatetime, $intCleanEndDatetime, $strCleanupCommand, $objDatabase ) {

		if( true == is_null( $intId ) ) return false;

		$boolIsSuccess 		= true;
		$objStorageObject 	= self::fetchStorageObjectById( $intId, $objDatabase );
		if( false == valObj( $objStorageObject, 'CStorageObject' ) ) {
			$boolIsSuccess 		= false;
		}

		$objStorageObject->setCleanStartDatetime( $intCleanStartDatetime );

		$objStorageObject->setCleanEndDatetime( $intCleanEndDatetime );

		$objStorageObject->setCleanupCommand( $strCleanupCommand );

		if( true == $objStorageObject->getIsOneTimeCleanup() && false == is_null( $objStorageObject->getCleanStartDatetime() ) && false == is_null( $objStorageObject->getCleanEndDatetime() ) ) {
				$objStorageObject->setIsRequireCleanup( false );
		}

		$boolIsSuccess &= $objStorageObject->update( $intCurrentUserId = CUser::ID_SYSTEM, $objDatabase );

		return $boolIsSuccess;
	}

	public static function fetchAllStorageObjects( $objDatabase ) {
		return parent::fetchStorageObjects( sprintf( 'SELECT * FROM storage_objects' ), $objDatabase );
	}

	public static function fetchObjectCleanupCount( $objDatabase ) {

		$strSql = ' SELECT
                            storage_type_id,
                        sum ( object_cleanup_count )
							FROM
                            storage_objects
						GROUP BY
                         storage_type_id ';

		return fetchData( $strSql, $objDatabase );
	}

}
?>
