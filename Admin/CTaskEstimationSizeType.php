<?php

class CTaskEstimationSizeType extends CBaseTaskEstimationSizeType {

	const XLSIZE = 1;
	const LSIZE  = 2;
	const MSIZE  = 3;
	const SSIZE  = 4;
	const XSSIZE = 5;

	public static $c_arrstrTaskEstimationSize = [
		self::XSSIZE => 'XS',
		self::SSIZE  => 'S',
		self::MSIZE  => 'M',
		self::LSIZE  => 'L',

		self::XLSIZE => 'XL'
	];

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOrderNum() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>