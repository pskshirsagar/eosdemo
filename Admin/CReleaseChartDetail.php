<?php

class CReleaseChartDetail extends CBaseReleaseChartDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReleaseReportDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReleaseChartReportTemplateId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valChartData() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>