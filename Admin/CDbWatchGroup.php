<?php

class CDbWatchGroup extends CBaseDbWatchGroup {

	public function valGroupId() {
		$boolIsValid = true;

		if( true == is_null( $this->getGroupId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'group_id', 'Group id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDbWatchId() {
		$boolIsValid = true;

		if( true == is_null( $this->getDbWatchId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'db_watch_id', 'Db watch id is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valGroupId();
				$boolIsValid &= $this->valDbWatchId();
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}
}
?>