<?php

class CWorkbookDocument extends CBaseWorkbookDocument {

	protected $m_arrmixPutObjectResponse;

	public function setPutObjectResponse( $arrmixPutObjectResponse ) {
		$this->m_arrmixPutObjectResponse = $arrmixPutObjectResponse;
	}

	public function getPutObjectResponse() {
		return $this->m_arrmixPutObjectResponse;
	}

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCid() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPropertyId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valWorkbookId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportSectionTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valMigrationWorkbookStatusTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTagIds() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTitle() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFileName() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valFilePath() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRejectedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRejectedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRejectNote() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valApprovedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$boolReturn = parent::update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		if( $boolReturn && false == $boolReturnSqlOnly && false == is_null( $this->getPutObjectResponse() ) ) {
			$boolReturn &= $this->updateStoredObject( $intCurrentUserId, $objDatabase );
		}

		return $boolReturn;
	}

	public function insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$boolReturn = parent::insert( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		if( $boolReturn && false == $boolReturnSqlOnly ) {
			$boolReturn &= $this->insertStoredObject( $intCurrentUserId, $objDatabase );
		}

		return $boolReturn;
	}

	public function updateStoredObject( $intCurrentUserId, $objDatabase ) {

		$objSharedStoredObject = \Psi\Eos\Admin\CSharedStoredObjects::createService()->fetchSharedStoredObjectsByReferenceIdByReferenceTable( $this->getId(), static::TABLE_NAME, $objDatabase );
		$arrmixPutObjectResponse = $this->getPutObjectResponse();

		if( !valObj( $objSharedStoredObject, 'CSharedStoredObject' ) || is_null( $arrmixPutObjectResponse ) ) {
			return false;
		}

		$objSharedStoredObject->setVendor( $arrmixPutObjectResponse['request']['storageGateway'] );
		$objSharedStoredObject->setContainer( $arrmixPutObjectResponse['request']['container'] );
		$objSharedStoredObject->setKey( $arrmixPutObjectResponse['request']['key'] );
		$objSharedStoredObject->setTitle( $this->getFileName() );
		$objSharedStoredObject->setContentLength( \Psi\CStringService::singleton()->strlen( $arrmixPutObjectResponse['request']['data'] ) );
		$objSharedStoredObject->setEtag( $arrmixPutObjectResponse['eTag'] );

		$boolReturn = $objSharedStoredObject->update( $intCurrentUserId, $objDatabase );

		return $boolReturn;
	}

	public function insertStoredObject( $intCurrentUserId, $objDatabase ) {

		$arrmixPutObjectResponse = $this->getPutObjectResponse();

		if( is_null( $arrmixPutObjectResponse ) ) {
			return false;
		}

		$objSharedStoredObject = new CSharedStoredObject();
		$objSharedStoredObject->setReferenceTable( static::TABLE_NAME );
		$objSharedStoredObject->setReferenceId( $this->getId() );
		$objSharedStoredObject->setReferenceTag( NULL );
		$objSharedStoredObject->setVendor( $arrmixPutObjectResponse['request']['storageGateway'] );
		$objSharedStoredObject->setContainer( $arrmixPutObjectResponse['request']['container'] );
		$objSharedStoredObject->setKey( $arrmixPutObjectResponse['request']['key'] );
		$objSharedStoredObject->setTitle( $this->getFileName() );
		$objSharedStoredObject->setContentLength( \Psi\CStringService::singleton()->strlen( $arrmixPutObjectResponse['request']['data'] ) );
		$objSharedStoredObject->setEtag( $arrmixPutObjectResponse['eTag'] );
		// $objSharedStoredObject->setDetails( $arrmixPutObjectResponse['storageGateway'] );

		$boolReturn = $objSharedStoredObject->insert( $intCurrentUserId, $objDatabase );

		return $boolReturn;
	}

}
?>