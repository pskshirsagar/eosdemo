<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplateQuestions
 * Do not add any new functions to this class.
 */

class CSurveyTemplateQuestions extends CBaseSurveyTemplateQuestions {

	public static function fetchSurveyTemplateQuestions( $strSql, $objDatabase, $boolIsReturnKeyedArray = true ) {
		return parent::fetchObjects( $strSql, 'CSurveyTemplateQuestion', $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchSurveyTemplateQuestionsBySurveyTemplateIds( $arrintSurveyTemplateIds, $objDatabase, $arrintSurveyIds = NULL, $boolIsReturnKeyedArray = true ) {

		if( false == valArr( $arrintSurveyTemplateIds = array_filter( ( array ) $arrintSurveyTemplateIds ) ) ) return NULL;

		$strJoin = $strWhere = $strSelect = '';

		if( true == valArr( $arrintSurveyIds = array_filter( ( array ) $arrintSurveyIds ) ) ) {
			$strSelect  = ', s.id as survey_id';
			$strJoin 	= ' JOIN surveys s ON ( s.survey_template_id = sqg.survey_template_id )';
			$strWhere 	= ' AND s.id IN ( ' . implode( ',', $arrintSurveyIds ) . ' ) ';
		}

		$strSql = 'SELECT util_set_locale( \'' . CLocaleContainer::createService()->getLocaleCode() . '\', \'' . CLocaleContainer::createService()->getDefaultLocaleCode() . '\' );
					SELECT
						DISTINCT stq.* ' .
						$strSelect . '
					FROM
						survey_template_questions stq
						JOIN survey_template_question_groups sqg ON ( sqg.survey_template_id = stq.survey_template_id )' .
						$strJoin . '
					WHERE
						stq.is_published = 1
						AND sqg.survey_template_id IN (' . implode( ',', $arrintSurveyTemplateIds ) . ')' .
						$strWhere . '
					ORDER BY
						stq.order_num';

		return self::fetchSurveyTemplateQuestions( $strSql, $objDatabase, $boolIsReturnKeyedArray );
	}

	public static function fetchSurveyTemplateQuestionsByIdsBySurveyTemplateId( $intIds, $intSurveyTemplateId, $objDatabase ) {

		if( false == valArr( $intIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						survey_template_questions
					WHERE
						id IN (' . implode( ',', $intIds ) . ' )
						AND survey_template_id = ' . ( int ) $intSurveyTemplateId . '
					ORDER BY
						order_num';

		return self::fetchSurveyTemplateQuestions( $strSql, $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionByIdBySurveyTemplateId( $intId, $intSurveyTemplateId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						survey_template_questions
					WHERE
						id = ' . ( int ) $intId . '
						AND survey_template_id = ' . ( int ) $intSurveyTemplateId . '
					ORDER BY
						order_num';

		return self::fetchSurveyTemplateQuestion( $strSql, $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionsBySurveyTemplateQuestionGroupIdBySurveyTemplateId( $intSurveyTemplateQuestionGroupId, $intSurveyTemplateId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						survey_template_questions
					WHERE
						survey_template_question_group_id = ' . ( int ) $intSurveyTemplateQuestionGroupId . '
						AND survey_template_id = ' . ( int ) $intSurveyTemplateId . '
					ORDER BY
						order_num';

		return self::fetchSurveyTemplateQuestions( $strSql, $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionsBySurveyTemplateQuestionId( $intSurveyTemplateQuestionId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						survey_template_questions
					WHERE
						survey_template_question_id = ' . ( int ) $intSurveyTemplateQuestionId . '
					ORDER BY
						order_num';

		return self::fetchSurveyTemplateQuestions( $strSql, $objDatabase );
	}

	public static function fetchMaxSurveyTemplateQuestionOrderNumBySurveyTemplateQuestionGroupId( $intSurveyTemplateQuestionGroupId, $objDatabase ) {
		$strSql = 'SELECT
						Max(order_num)
					FROM
						survey_template_questions
					WHERE
						survey_template_question_group_id = ' . ( int ) $intSurveyTemplateQuestionGroupId;

		$arrintMaxOrderNum = fetchData( $strSql, $objDatabase );

		return ( int ) getArrayElementByKey( 'max', $arrintMaxOrderNum[0] );

	}

	public static function fetchSurveyTemplateQuestionsBySurveyTemplateId( $intSurveyTemplateId, $objDatabase, $objSurveyTemplateFilter = NULL ) {

		if( false == valId( $intSurveyTemplateId ) ) return NULL;

		$strWhere = $strOrderBy = $strGroupBy = '';
		$strSql = 'SELECT stq.*';

		if( true == valObj( $objSurveyTemplateFilter, 'CSurveySystemFilter' ) ) {

			$strSql = ( true == $objSurveyTemplateFilter->getIsCountSql() ) ? 'SELECT COUNT( stq.id )' : 'SELECT stq.*';
			$strWhere  = ( true == $objSurveyTemplateFilter->getIsChildQuestion() ) ? ' AND stq.survey_template_question_id IS NOT NULL' : ' AND stq.survey_template_question_id IS NULL';
			$strWhere .= ( false == is_null( $objSurveyTemplateFilter->getSurveyTemplateQuestionId() ) ) ? ' AND stq.survey_template_question_id = ' . $objSurveyTemplateFilter->getSurveyTemplateQuestionId() : '';
			$strWhere .= ( true == valStr( $objSurveyTemplateFilter->getAction() ) ) ? ' AND ' . $objSurveyTemplateFilter->getAction() . '= 1' : '';

			$strOrderBy = ( true == $objSurveyTemplateFilter->getIsCountSql() ) ? '' : ' ORDER BY stq.order_num, stqg.order_num';
			$strGroupBy = ( true == $objSurveyTemplateFilter->getIsCountSql() ) ? '' : ' GROUP BY stq.survey_template_question_group_id, stq.id, stqg.order_num';
		}

		$strWhere = ' WHERE stq.survey_template_id = ' . ( int ) $intSurveyTemplateId . $strWhere;

		$strSql .= ' FROM
						survey_template_questions stq
						JOIN survey_template_question_groups stqg ON ( stq.survey_template_question_group_id = stqg.id )'
				. $strWhere .
					$strGroupBy .
					$strOrderBy;

		if( true == valObj( $objSurveyTemplateFilter, 'CSurveySystemFilter' ) && true == $objSurveyTemplateFilter->getIsCountSql() ) {
			$arrstrData = fetchData( $strSql, $objDatabase );
			if( true == valArr( $arrstrData ) ) {
				return $arrstrData[0]['count'];
			}
		}

		return self::fetchSurveyTemplateQuestions( $strSql, $objDatabase );
	}

	public static function fetchTemplateQuestionsBySurveyTemplateId( $intSurveyTemplateId, $objAdminDatabase ) {
		if( false == is_numeric( $intSurveyTemplateId ) ) return NULL;

		$strSql = 'SELECT
						id,
						util_get_translated( \'question\', question, details, \'' . CLocaleContainer::createService()->getLocaleCode() . '\' ) AS question
					FROM
						survey_template_questions
					WHERE
						survey_question_type_id <> ' . CSurveyQuestionType::MATRIX . '
						AND survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND is_published = 1
					ORDER BY
						order_num';

		return fetchData( $strSql, $objAdminDatabase );
	}

}
?>