<?php
use Psi\Eos\Admin\CDmTemplates;

class CDmTemplate extends CBaseDmTemplate {

	public function valName( $objAdminDatabase ) {

		$boolIsValid = false;

		if( false == valObj( $objAdminDatabase, 'CDatabase' ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, 'Failed to load database object.' ) );

			return $boolIsValid;
		}

		if( false == valStr( $this->m_strName ) || true == preg_match( '/[\\\\@#!%&*+:?;,.\/]/', $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Please enter valid template name.' ) );

			return $boolIsValid;
		}

		if( 2 >= strlen( $this->m_strName ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Template name should contain at least 3 letters. ' ) );

			return $boolIsValid;
		}

		if( true == valId( $this->getId() ) ) {
			$objDmTemplate = CDmTemplates::createService()->fetchDmTemplateById( $this->getId(), $objAdminDatabase );

			if( false == valObj( $objDmTemplate, 'CDmTemplate' ) ) {
				return $boolIsValid;
			} elseif( \Psi\CStringService::singleton()->strtolower( $this->m_strName ) == \Psi\CStringService::singleton()->strtolower( $objDmTemplate->getName() ) ) {
					return true;
			}

		}

		if( true == $this->isDmTemplateNameExist( $objAdminDatabase ) ) {
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Dm Template name already exists.' ) );
		} else {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objAdminDatabase = NULL ) {

		$boolIsValid = true;

		switch( $strAction ) {

			case 'validate_template':
				$boolIsValid &= $this->valName( $objAdminDatabase );
				break;

			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function isDmTemplateNameExist( $objAdminDatabase ) {
		$boolIsValid = false;

		if( 0 < CDmTemplates::createService()->fetchDmTemplateCountByName( $this->m_strName, $objAdminDatabase ) ) {
			$boolIsValid = true;
		}

		return $boolIsValid;
	}

}
?>