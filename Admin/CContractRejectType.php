<?php

class CContractRejectType extends CBaseContractRejectType {

	// Cancel Contract Reject Types.
	const NOT_INTERESTED				= 1;
	const SOFTWARE_TOO_EXPENSIVE		= 2;
	const INCOMPATIBLE_BUSINESS_TYPE	= 4;
	const LEAD_TOO_SMALL				= 5;
	const LEAD_WOULDNT_RESPOND			= 6;
	const NOT_PROFITABLE				= 7;
	const NOT_QUALIFIED					= 8;
	const OTHER							= 9;
	const DUPLICATE						= 10;
	const TEST_ERROR					= 11;
	const NOT_THE_RIGHT_FIT				= 12;

	// Lost Contract Reject Types.
	const FEATURES_MISSING				= 3;
	const IMMATURITY_OF_PRODUCT			= 13;
	const LOST_TO_COMPETITOR			= 14;
	const NO_DECISSION					= 15;
	const PRICE_BUDGET					= 16;
	const IMPLEMENTATION_EXECUTION		= 17;
	const CANT_GET_OUT_OF_CURRENT_CONTRACT = 18;
	const DECISION_MAKER_LEFT_COMPANY = 19;
	const PRODUCT_DEFICIENCY = 20;

	public static $c_arrintCancelContractRejectTypes = array(
		self::NOT_INTERESTED,
		self::SOFTWARE_TOO_EXPENSIVE,
		self::INCOMPATIBLE_BUSINESS_TYPE,
		self::LEAD_TOO_SMALL,
		self::LEAD_WOULDNT_RESPOND,
		self::NOT_PROFITABLE,
		self::NOT_QUALIFIED,
		self::OTHER,
		self::DUPLICATE,
		self::TEST_ERROR,
		self::NOT_THE_RIGHT_FIT
	);

	public static $c_arrintLostContractRejectTypes = [
		self::NOT_INTERESTED,
		self::SOFTWARE_TOO_EXPENSIVE,
		self::INCOMPATIBLE_BUSINESS_TYPE,
		self::LEAD_TOO_SMALL,
		self::LEAD_WOULDNT_RESPOND,
		self::NOT_PROFITABLE,
		self::NOT_QUALIFIED,
		self::OTHER,
		self::DUPLICATE,
		self::TEST_ERROR,
		self::NOT_THE_RIGHT_FIT,
		self::FEATURES_MISSING,
		self::IMMATURITY_OF_PRODUCT,
		self::LOST_TO_COMPETITOR,
		self::NO_DECISSION,
		self::PRICE_BUDGET,
		self::IMPLEMENTATION_EXECUTION,
		self::CANT_GET_OUT_OF_CURRENT_CONTRACT,
		self::DECISION_MAKER_LEFT_COMPANY,
		self::PRODUCT_DEFICIENCY
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>