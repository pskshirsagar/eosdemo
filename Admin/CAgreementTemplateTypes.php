<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CAgreementTemplateTypes
 * Do not add any new functions to this class.
 */

class CAgreementTemplateTypes extends CBaseAgreementTemplateTypes {

	public static function fetchAgreementTemplateTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CAgreementTemplateType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchAgreementTemplateType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CAgreementTemplateType', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL, $boolIsAppendDatabaseId = false );
	}

	public static function fetchOrderedAgreementTemplateTypes( $objAdminDatabase ) {

		$strSql = 'SELECT * FROM agreement_template_types WHERE is_published=1 ORDER BY order_num';

		return self::fetchAgreementTemplateTypes( $strSql, $objAdminDatabase );
	}
}
?>