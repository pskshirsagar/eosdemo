<?php

class COrderFormStatusType extends CBaseOrderFormStatusType {

	const UNUSED            = 1;
	const READY_FOR_REVIEW  = 2;
	const CHANGE_REQUESTED  = 3;
	const AWAITING_PAYMENT  = 4;
	const ACCEPTED          = 5;
	const COMPLETED         = 6;
	const CANCELLED         = 7;
	const AWAITING_SIGNATURE = 8;
	const REORDER           = 9;

	public static $c_arrintNonCompletedOrderFormStatusTypeIds = [ self::UNUSED, self::READY_FOR_REVIEW, self::CHANGE_REQUESTED, self:: AWAITING_PAYMENT, self:: ACCEPTED, self::AWAITING_SIGNATURE, self::REORDER ];

	public static $c_arrintMerchantServicesPostSubmittedStatusTypeIds = [ self:: ACCEPTED, self::COMPLETED, self::CANCELLED ];

	public static $c_arrintRevisionReminderStatusTypeIds = [ self:: READY_FOR_REVIEW, self::CHANGE_REQUESTED ];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
