<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CReleaseReferenceTypes
 * Do not add any new functions to this class.
 */

class CReleaseReferenceTypes extends CBaseReleaseReferenceTypes {

	public static function fetchReleaseReferenceTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CReleaseReferenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchReleaseReferenceType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CReleaseReferenceType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>