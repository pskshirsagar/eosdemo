<?php

class CPsAssetStatusType extends CBasePsAssetStatusType {

	const AVAILABLE_NOT_ASSIGNED	= 1;
	const ASSIGNED					= 2;
	const CHANGE_REQUESTED			= 3;
	const DAMAGE_REQUESTED			= 4;
	const IN_REPAIR					= 5;
	const EXPIRED					= 6;
	const SCRAPPED					= 7;
	const INVALID					= 8;

	public static $c_arrintBulkPsAssetStatusChange	= [
		self::IN_REPAIR,
		self::EXPIRED,
		self::INVALID,
		self::SCRAPPED
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>