<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyQuestionAnswers
 * Do not add any new functions to this class.
 */

class CSurveyQuestionAnswers extends CBaseSurveyQuestionAnswers {

	const PAST_DATE 				= '1999-01-01';

	public static function fetchSurveyQuestionAnswerCountBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {

		$strSql = 'SELECT
						DISTINCT ( survey_id )
					FROM
						survey_question_answers
					WHERE
						survey_template_id = ' . ( int ) $intSurveyTemplateId;

		return \Psi\Libraries\UtilFunctions\count( fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchSurveyQuestionAnswersBySurveyIds( $arrintSurveyIds, $objDatabase ) {

		if( false == valArr( $arrintSurveyIds ) ) return NULL;

		$strSql = 'SELECT
						*
					FROM
						survey_question_answers
					WHERE
						survey_id IN (' . implode( ',', $arrintSurveyIds ) . ')';

		return self::fetchSurveyQuestionAnswers( $strSql, $objDatabase );
	}

	public static function fetchNumericWeightBySurveyIds( $arrintSubjectReferenceIds, $arrintSurveyIds, $objDatabase, $boolIsAddCompletedOnDate = false, $strLastAccessedOnDatetime = NULL, $strConsideredTillDatetime = NULL, $boolEmployeeAssessment = false ) {

		$strWhereClause = '';
		$strWhereClause .= ( true == $boolIsAddCompletedOnDate && true == valStr( $strLastAccessedOnDatetime ) ) ? ' AND s.response_datetime::date > \'' . $strLastAccessedOnDatetime . '\'': '';

		if( false == valArr( $arrintSubjectReferenceIds ) || false == valArr( $arrintSurveyIds ) ) return NULL;

		$strSelect = $strOrderBy = '';
		if( true == $boolEmployeeAssessment ) {
			$strSelect = 'DISTINCT ON ( es.survey_datetime ) es.survey_datetime,
					round( avg( coalesce ( ms.manager_rating, 0 ) ) OVER ( PARTITION BY survey_datetime ), 1) as average_manager,
					round( avg( coalesce ( es.employee_rating, 0 ) ) OVER ( PARTITION BY survey_datetime ), 1) as average_employee';

			$strWhereClause .= ' AND s.response_datetime::date <= \'' . $strConsideredTillDatetime . '\'';
			$strOrderBy = ' ORDER BY es.survey_datetime ASC';
		} else {
			$strSelect = 'es.employee_rating AS emp_rating,
					ms.manager_rating AS manager_rating,
					es.subject_reference_id,
					es.id as survey_id';
			$strOrderBy = 'ORDER BY
					es.employee_created_on DESC,
					ms.manager_created_on DESC';
		}

		$strSql = 'WITH employee_surveys AS (
						SELECT
							sqa.numeric_weight AS employee_rating,
							s.created_on AS employee_created_on,
							s.id AS survey_id,
							to_char ( s.survey_datetime, \'yyyy-mm\' ) as survey_datetime,
							s.subject_reference_id,
							s.id
						FROM
							surveys s
							INNER JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						WHERE
							s.subject_reference_id IN ( ' . implode( ',', $arrintSubjectReferenceIds ) . ' )
							AND s.declined_datetime IS NULL
							AND s.survey_type_id = ' . CSurveyType::PERSONAL_REVIEW . '
							AND sqa.numeric_weight IS NOT NULL ' . $strWhereClause . '
							AND s.id IN ( ' . implode( ',', $arrintSurveyIds ) . ' )
						ORDER BY
							s.created_by DESC ), manager_surveys AS (
						SELECT
							sqa.numeric_weight AS manager_rating,
							s.created_on AS manager_created_on,
							s.trigger_reference_id AS trigger_reference_id
						FROM
							surveys s
							INNER JOIN survey_question_answers sqa ON ( s.id = sqa.survey_id )
						WHERE
							s.subject_reference_id IN ( ' . implode( ',', $arrintSubjectReferenceIds ) . ' )
							AND s.declined_datetime IS NULL
							AND s.survey_type_id = ' . CSurveyType::MANAGER_SURVEY . '
							AND sqa.numeric_weight IS NOT NULL' . $strWhereClause . '
							AND s.id IN ( ' . implode( ',', $arrintSurveyIds ) . ' )
						ORDER BY
							s.created_by DESC )
				SELECT ' . $strSelect . '
				FROM
					employee_surveys es
					LEFT JOIN manager_surveys ms ON ( es.survey_id = ms.trigger_reference_id )' . $strOrderBy;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSupportNpsScore( $strWhereCondition, $objDatabase ) {
		if( 'month_to_date' == $strWhereCondition ) {
			$strWhereCondition = ' AND s.response_datetime BETWEEN (date_trunc(\'month\', NOW())) AND NOW() ';
		} elseif( 'fifteen_days_rolling' == $strWhereCondition ) {
			$strWhereCondition = ' AND s.response_datetime > now() - \'15 days\' :: INTERVAL ';
		} elseif( 'This_month' == $strWhereCondition ) {
			$strWhereCondition = ' AND s.response_datetime > now() - \'30 days\' :: INTERVAL ';
		} elseif( 'yesterday' == $strWhereCondition ) {
			$strWhereCondition = ' AND s.created_on::date = now()::date - \'1 day\' :: INTERVAL AND s.response_datetime < now() ';
		}

		$strSql = 'SELECT
						ROUND ( SUM ( ( CASE
											WHEN nps_score IN ( 9, 10 )
											THEN 1
											ELSE 0
										END ) -
										( CASE
											WHEN nps_score < 7
											THEN 1
											ELSE 0
										END ) ) / count ( * ) ::numeric ( 10, 2 ), 3 ) * 100 "NPS",
						SUM ( CASE
								WHEN nps_score IN ( 9, 10 )
								THEN 1
								ELSE 0
							 END ) "Promoters",
						SUM ( CASE
								WHEN nps_score IN ( 7, 8 ) THEN 1
								ELSE 0
							 END ) "Neutrals",
						SUM ( CASE
								WHEN nps_score < 7 THEN 1
								ELSE 0
							 END ) "Detractors"
					FROM
						(
							SELECT
								sqa.numeric_weight nps_score,
								row_number ( ) OVER ( PARTITION BY ta.task_id
							ORDER BY
								ta.created_on DESC )
							FROM
								surveys s
								LEFT JOIN survey_question_answers sqa ON sqa.survey_id = s.id
								LEFT JOIN task_assignments ta ON ta.task_id = s.trigger_reference_id AND ta.created_on < s.response_datetime
								LEFT JOIN users u ON u.id = ta.user_id
								INNER JOIN employees e ON e.id = u.employee_id AND e.department_id = ' . CDepartment::TECHNICAL_SUPPORT . '
								INNER JOIN ps_leads pl ON s.ps_lead_id = pl.id AND pl.company_status_type_id != ' . CCompanyStatusType::TEST_DATABASE . '
							WHERE
								s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . '
								AND s.response_datetime IS NOT NULL' . $strWhereCondition . '
								AND ta.user_id != ' . Cuser::ID_FIRST_RESPONSE . '
							ORDER BY
								s.response_datetime DESC
						) AS TEMP
					WHERE
					 row_number = 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSupportNpsScoreForTvSlides( $strWhereCondition, $objDatabase ) {

		if( 'nps_ten_last_comment' == $strWhereCondition ) {
			$strSelect			= 'SELECT task_id, responded_on, company_name, responder_reference_id, answer, nps_score, support_agent_id, preferred_name';
			$strWhereCondition 	= ' AND s.response_datetime > now( ) - \'1 month\' :: INTERVAL AND sqa.numeric_weight = 10 AND sqa.answer IS NOT NULL ';
			$strOrderBy			= ' ORDER BY responded_on DESC';
			$intLimit			= 1;
		} elseif( 'nps_top_ten' == $strWhereCondition ) {
			$strSelect			= 'SELECT COUNT(nps_score), support_agent_id';
			$strWhereCondition 	= ' AND s.response_datetime > now( ) - \'7 days\' :: INTERVAL AND sqa.numeric_weight = 10 ORDER BY s.response_datetime DESC';
			$strOrderBy			= '';
			$strGroupBy			= 'GROUP BY support_agent_id';
			$intLimit			= 3;
		} elseif( 'nps_ten_highlight' == $strWhereCondition ) {
			$strSelect		= ' SELECT id, task_id, responded_on, company_name, responder_reference_id, answer, nps_score, support_agent_id, preferred_name';
			$strWhereCondition = ' AND s.response_datetime > now( ) - \'15 days\' :: INTERVAL AND sqa.numeric_weight = 10';
			$strOrderBy		= ' ORDER BY random()';
			$intLimit		= 1;
		} elseif( 'we_got_one' == $strWhereCondition ) {
			$strSelect			= ' SELECT id, task_id, responded_on, company_name, responder_reference_id, answer, nps_score, support_agent_id, preferred_name';
			$strWhereCondition 	= ' AND s.response_datetime > now( ) - \'1 hour\' :: INTERVAL AND sqa.numeric_weight = 10 ';
			$strOrderBy			= ' ORDER BY responded_on DESC';
			$intLimit			= 1;
		}

		$strSql = $strSelect . '
					FROM
						(
							SELECT
								sqa.id,
								s.trigger_reference_id task_id,
								s.created_on::timestamp responded_on,
								pl.company_name,
								s.responder_reference_id,
								sqa.numeric_weight nps_score,
								e.id support_agent_id,
								COALESCE ( e.preferred_name, e.name_full ) as preferred_name,
								sqa.answer,
								row_number ( ) OVER ( PARTITION BY ta.task_id
							ORDER BY
								ta.created_on DESC )
							FROM
								surveys s
								LEFT JOIN survey_question_answers sqa ON sqa.survey_id = s.id
								LEFT JOIN task_assignments ta ON ta.task_id = s.trigger_reference_id AND ta.created_on < s.response_datetime
								LEFT JOIN users u ON u.id = ta.user_id
								INNER JOIN employees e ON e.id = u.employee_id AND e.department_id = ' . CDepartment::TECHNICAL_SUPPORT . '
								INNER JOIN ps_leads pl ON s.ps_lead_id = pl.id AND pl.company_status_type_id != ' . CCompanyStatusType::TEST_DATABASE . '
						WHERE
								s.survey_type_id = ' . CSurveyType::SUPPORT_REVIEW . '
								AND s.response_datetime IS NOT NULL
								AND ta.user_id !=' . Cuser::ID_FIRST_RESPONSE . '
								AND e.date_terminated IS NULL
							 ' . $strWhereCondition . '
						) AS TEMP
						' . $strGroupBy . '
						' . $strOrderBy;

		if( 0 < $intLimit ) $strSql .= ' LIMIT ' . ( int ) $intLimit;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSurveyQuestionAnswersCountBySurveyTemplateQuestionIds( $arrintSurveyTemplateQuestionIds, $objDatabase ) {

		if( false == valArr( $arrintSurveyTemplateQuestionIds ) ) return NULL;

		$strSql = ' SELECT
		 				count(id),
						survey_template_question_id
					FROM
						survey_question_answers
					WHERE
						survey_template_question_id IN ( ' . implode( ',', $arrintSurveyTemplateQuestionIds ) . ')
					GROUP BY
						survey_template_question_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNpsReviews( $strNpsReviewStatus, $arrstrCommentsFilter, $objDatabase, $arrintNpsUserIds = NULL, $arrstrSelectedDate = NULL ) {

		$strWhereCondition	= '';

		if( isset( $arrstrCommentsFilter['page_no'] ) && isset( $arrstrCommentsFilter['page_size'] ) ) {
			$intOffset	= ( 0 < $arrstrCommentsFilter['page_no'] ) ? $arrstrCommentsFilter['page_size'] * ( $arrstrCommentsFilter['page_no'] - 1 ) : 0;
			$intLimit	= ( int ) $arrstrCommentsFilter['page_size'];
		}

		$strQaStausSortByFollowupName = '';

		if( 'qa_status' == $arrstrCommentsFilter['sorting_field'] ) {
			$arrstrCommentsFilter['sorting_field'] = 'qa_status_with_followup';
		}

		$strFollowupUsersJoin = '';
		$strFollowupUsersSelect = '';
		$strFollowupConcatField = '\'default_preferred_name\'';
		$strNullsFirst = '';

		if( true == valArr( $arrintNpsUserIds ) ) {
			$strFollowupUsersJoin 	= ' LEFT JOIN users u ON u.id = sqa.follow_up_called_by AND u.id IN ( ' . implode( ',', $arrintNpsUserIds ) . ')
									 	LEFT JOIN employees e ON e.id = u.employee_id ';
			$strFollowupUsersSelect = '	e.preferred_name as follow_up_user_name,';
			$strFollowupConcatField = '	e.preferred_name';
		}
		$strFilterCondition = '';
		if( true == valArr( $arrstrSelectedDate ) ) {
			$strDateCondition = ' AND s.response_datetime BETWEEN \'' . $arrstrSelectedDate['first_day'] . '\'::timestamp AND \'' . $arrstrSelectedDate['last_day'] . '\'::timestamp';
		} elseif( ( true == valStr( $arrstrCommentsFilter['from_date'] ) && true == array_key_exists( 'from_date', $arrstrCommentsFilter ) )
			&& ( true == valStr( $arrstrCommentsFilter['to_date'] ) && true == array_key_exists( 'to_date', $arrstrCommentsFilter ) ) ) {
			$strDateCondition = ' AND s.response_datetime BETWEEN \'' . $arrstrCommentsFilter['from_date'] . ' 00:00:00\' AND \'' . $arrstrCommentsFilter['to_date'] . ' 23:59:59\'';
		} else {
			$strDateCondition = ' AND s.response_datetime BETWEEN \'' . date( 'm/d/Y', strtotime( '-7 days' ) ) . ' 00:00:00\' AND \'' . date( 'm/d/Y' ) . ' 23:59:59\'';
		}

		switch( $strNpsReviewStatus ) {

			case 'waiting_for_response_status':
				$strWhereCondition = ' AND ( length( sqa.answer ) <=' . CNpsReviewsManager::EXPECTED_COMMENT_LENGTH . ' OR sqa.answer IS NULL ) AND s.response_datetime > (now() - \'3 days\'::interval)::timestamp AND sqa.modified_by is NULL AND sqa.answer_modified is NULL AND sqa.answer_email_response is NULL AND sqa.is_closed NOT IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ')';
				break;

			case 'edit_comments_status':
				$strWhereCondition = ' AND ( ( ( sqa.answer is NOT NULL AND length( sqa.answer ) > ' . CNpsReviewsManager::EXPECTED_COMMENT_LENGTH . ' ) OR sqa.answer_email_response is NOT NULL ) AND sqa.modified_by is NULL AND sqa.answer_modified is NULL AND sqa.is_closed NOT IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) ) OR ( sqa.follow_up_called_by is NOT NULL AND sqa.follow_up_called_on is NOT NULL AND sqa.answer_modified IS NULL AND sqa.is_closed NOT IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) )';
				break;

			case 'completed_status':
				$strWhereCondition = ' AND sqa.modified_by is NOT NULL AND sqa.answer_modified is NOT NULL OR ( sqa.is_closed IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) ' . $strDateCondition . ' ) ';
				break;

			default:
				$strWhereCondition = '';
		}

		if( true == valArr( $arrstrCommentsFilter['company_ids'] ) && true == array_key_exists( 'company_ids', $arrstrCommentsFilter ) ) {
			$strFilterCondition .= ' AND p.cid IN( ' . implode( ',', $arrstrCommentsFilter['company_ids'] ) . ' )';
		}

		if( true == valArr( $arrstrCommentsFilter['selected_csm_employee_ids'] ) && true == array_key_exists( 'selected_csm_employee_ids', $arrstrCommentsFilter ) ) {
			$strFilterCondition .= ' AND pld.support_employee_id IN( ' . implode( ',', $arrstrCommentsFilter['selected_csm_employee_ids'] ) . ' )';
		}

		$strSql = 'SELECT
	 				 	p.company_name,
						p.name_first,
						p.name_last,
						p.permissioned_units,
						p.id AS person_id,
						p.email_address,
						pr.name,
						p.cid as person_cid,
						pr.is_executive,
						sqa.numeric_weight,
						sqa.id as survey_qa_id,
						sqa.answer,
						sqa.answer_modified,
						sqa.answer_email_response,
						sqa.is_closed,
						CASE WHEN sqa.is_hidden = TRUE THEN 1 ELSE 0 END AS is_hidden,
						sqa.follow_up_called_by,
						sqa.follow_up_called_on,
						sqa.scheduled_on,
						sqa.updated_on,
						' . $strFollowupUsersSelect . '
						s.response_datetime,
						sqa.modified_by,
						s.id as survey_id,
						st.task_id,
						t.task_status_id as task_status,
						pld.client_risk_status,
						pld.key_client_acv_rank,
						pld.key_client_override_rank,
						pld.ps_lead_id,
						prp.total_units,
						CASE
							WHEN sqa.answer IS NULL AND s.response_datetime > (now() - \'3 days\'::interval)::timestamp AND sqa.modified_by is NULL AND sqa.answer_modified is NULL AND sqa.answer_email_response is NULL AND sqa.is_closed NOT IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) THEN \'Waiting For Response\'
							WHEN ( ( sqa.answer is NOT NULL OR sqa.answer_email_response is NOT NULL ) AND sqa.answer_modified is NULL AND sqa.modified_by is NULL AND sqa.answer_modified is NULL AND sqa.is_closed NOT IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) ) OR ( sqa.follow_up_called_by is NOT NULL AND sqa.follow_up_called_on is NOT NULL AND sqa.answer_modified IS NULL AND sqa.is_closed NOT IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) ) THEN \'Edit Comment\'
							WHEN sqa.modified_by is NOT NULL AND sqa.answer_modified is NOT NULL OR ( sqa.is_closed IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) ' . $strDateCondition . ' ) THEN \'Complete\'
							WHEN sqa.modified_by is NULL AND sqa.answer_modified is NULL OR ( sqa.is_closed IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) ' . $strDateCondition . ' ) THEN \'Complete\'
						END as qa_status,
						CASE
							WHEN sqa.answer IS NULL AND s.response_datetime > (now() - \'3 days\'::interval)::timestamp AND sqa.modified_by is NULL AND sqa.answer_modified is NULL AND sqa.answer_email_response is NULL AND sqa.is_closed NOT IN (' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ') THEN concat( \'Waiting For Response\',' . $strFollowupConcatField . ' )
							WHEN ( ( sqa.answer is NOT NULL OR sqa.answer_email_response is NOT NULL ) AND sqa.answer_modified is NULL AND sqa.modified_by is NULL AND sqa.answer_modified is NULL AND sqa.is_closed NOT IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) ) OR ( sqa.follow_up_called_by is NOT NULL AND sqa.follow_up_called_on is NOT NULL AND sqa.answer_modified IS NULL AND sqa.is_closed NOT IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) ) THEN concat( \'Edit Comment\',' . $strFollowupConcatField . ' )
							WHEN sqa.modified_by is NOT NULL AND sqa.answer_modified is NOT NULL OR ( sqa.is_closed IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) ' . $strDateCondition . ' ) THEN concat( \'Complete\',' . $strFollowupConcatField . ' )
							WHEN sqa.modified_by is NULL AND sqa.answer_modified is NULL OR ( sqa.is_closed IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) ' . $strDateCondition . ' ) THEN \'Complete\'
						END as qa_status_with_followup
					FROM
						survey_question_answers sqa
						JOIN surveys s ON sqa.survey_id = s.id
						LEFT JOIN persons p ON s.responder_reference_id = p.id
						LEFT JOIN person_roles pr ON pr.id = p.person_role_id
						LEFT JOIN ps_lead_details pld ON pld.ps_lead_id = p.ps_lead_id
						JOIN clients c ON c.id = p.cid AND pld.cid = c.id
						LEFT JOIN
						(
						 SELECT
								SUM ( p.number_of_units ) AS total_units,
								p.cid
							FROM
								properties p
								JOIN clients mc ON ( p.cid = mc.id )
								JOIN ps_leads pl ON ( mc.ps_lead_id = pl.id )
							WHERE
								p.is_disabled <> 1
								AND pl.id = mc.ps_lead_id
								AND p.termination_date IS NULL
							GROUP BY
								p.cid
						) prp ON prp.cid = pld.cid
						LEFT JOIN survey_tasks st ON ( st.survey_id = s.id )
						LEFT JOIN tasks t ON t.id = st.task_id ' . $strFollowupUsersJoin . '

					WHERE
						( s.survey_type_id =' . ( int ) CSurveyType::CUSTOMER_NPS . $strDateCondition . '
						AND p.deleted_on is NULL' . $strWhereCondition . ' ) ' . $strFilterCondition . ' 
						AND c.company_status_type_id =' . CCompanyStatusType::CLIENT . '
						AND sqa.deleted_on IS NULL
						AND p.is_disabled <> 1
					ORDER BY ' . $arrstrCommentsFilter['sorting_field'] . ' ' . $arrstrCommentsFilter['sorting_order'] . $strQaStausSortByFollowupName;

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strSql .= ' OFFSET	' . ( int ) $intOffset . ' LIMIT	' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNpsReviewsCount( $strNpsReviewStatus, $objDatabase, $arrstrSelectedDate = NULL, $arrstrFilter = NULL ) {
		$strDateCondition = '';
		$strWhereCondition	= '';
		$strFilterCondition = '';
		if( true == valArr( $arrstrSelectedDate ) ) {
			$strDateCondition = ' AND s.response_datetime BETWEEN \'' . $arrstrSelectedDate['first_day'] . '\'::timestamp AND \'' . $arrstrSelectedDate['last_day'] . '\'::timestamp';
		} elseif( ( true == valStr( $arrstrFilter['from_date'] ) && true == array_key_exists( 'from_date', $arrstrFilter ) )
					&& ( true == valStr( $arrstrFilter['to_date'] ) && true == array_key_exists( 'to_date', $arrstrFilter ) ) ) {
			$strDateCondition = ' AND s.response_datetime BETWEEN \'' . $arrstrFilter['from_date'] . ' 00:00:00\' AND \'' . $arrstrFilter['to_date'] . ' 23:59:59\'';
		} else {
			$strDateCondition = ' AND s.response_datetime BETWEEN \'' . date( 'm/d/Y', strtotime( '-7 days' ) ) . ' 00:00:00\' AND \'' . date( 'm/d/Y' ) . ' 23:59:59\'';
		}

		switch( $strNpsReviewStatus ) {

			case 'waiting_for_response_status':
				$strWhereCondition = ' AND ( length( sqa.answer ) <=' . CNpsReviewsManager::EXPECTED_COMMENT_LENGTH . ' OR sqa.answer IS NULL ) AND s.response_datetime > (now() - \'3 days\'::interval)::timestamp AND sqa.modified_by is NULL AND sqa.answer_modified is NULL AND sqa.answer_email_response is NULL AND sqa.is_closed NOT IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ')';
				break;

			case 'edit_comments_status':
				$strWhereCondition = ' AND ( ( ( sqa.answer is NOT NULL AND length( sqa.answer ) > ' . CNpsReviewsManager::EXPECTED_COMMENT_LENGTH . ' ) OR sqa.answer_email_response is NOT NULL ) AND sqa.modified_by is NULL AND sqa.answer_modified is NULL AND sqa.is_closed NOT IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) ) OR ( sqa.follow_up_called_by is NOT NULL AND sqa.follow_up_called_on is NOT NULL AND sqa.answer_modified IS NULL AND sqa.is_closed NOT IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) )';
				break;

			case 'completed_status':
				$strWhereCondition = ' AND sqa.modified_by is NOT NULL AND sqa.answer_modified is NOT NULL OR ( sqa.is_closed IN ( ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . ',' . CSurveyQuestionAnswer::NON_COMMENTED_CLOSED_OUT . ' ) ' . $strDateCondition . ' ) ';
				break;

			default:
				$strWhereCondition = '';
		}

		if( true == valArr( $arrstrFilter['company_ids'] ) && true == array_key_exists( 'company_ids', $arrstrFilter ) ) {
			$strFilterCondition .= ' AND p.cid IN( ' . implode( ',', $arrstrFilter['company_ids'] ) . ' )';
		}

		if( true == valArr( $arrstrFilter['selected_csm_employee_ids'] ) && true == array_key_exists( 'selected_csm_employee_ids', $arrstrFilter ) ) {
			$strFilterCondition .= ' AND pld.support_employee_id IN( ' . implode( ',', $arrstrFilter['selected_csm_employee_ids'] ) . ' )';
		}

		$strSql = 'SELECT
	 					count(*)
					FROM
						survey_question_answers sqa
						JOIN surveys s ON sqa.survey_id = s.id
						JOIN persons p ON s.responder_reference_id = p.id
						LEFT JOIN ps_lead_details pld ON pld.ps_lead_id = p.ps_lead_id
						JOIN clients c ON c.id = p.cid AND pld.cid = c.id
					WHERE
						( s.survey_type_id =' . ( int ) CSurveyType::CUSTOMER_NPS . $strDateCondition . '
						AND p.deleted_on is NULL' . $strWhereCondition . ' ) ' . $strFilterCondition . ' 
						AND c.company_status_type_id =' . CCompanyStatusType::CLIENT . '
						AND sqa.deleted_on IS NULL
						AND p.is_disabled <> 1';

		return parent::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchDailyNuggets( $arrstrCommentsFilter, $objDatabase ) {

		if( false == array_key_exists( 'daily_nugget', $arrstrCommentsFilter ) ) {
			$intOffset	= ( 0 < $arrstrCommentsFilter['page_no'] ) ? $arrstrCommentsFilter['page_size'] * ( $arrstrCommentsFilter['page_no'] - 1 ) : 0;
			$intLimit	= ( int ) $arrstrCommentsFilter['page_size'];

		} else {
			$strWhereCondition = ' AND sqa.scheduled_on < ( now() )
									 AND sqa.scheduled_on::date != (\'' . self::PAST_DATE . '\')::date';
		}

		$strSql = 'SELECT
						DISTINCT sqa.scheduled_on,
						CASE
							WHEN sqa.scheduled_on::date = (\'' . self::PAST_DATE . '\')::date THEN \'Pending\'
							WHEN sqa.scheduled_on < (now())::timestamp THEN \'Sent\'
							WHEN sqa.scheduled_on > (now())::timestamp THEN \'Scheduled\'
						END as email_status
					FROM
						persons p
						JOIN surveys s on s.responder_reference_id = p.id
						JOIN survey_question_answers sqa on sqa.survey_id = s.id
					WHERE
						s.survey_type_id =' . ( int ) CSurveyType::CUSTOMER_NPS . '
						AND s.response_datetime > (now() - \'30 days\'::interval)::timestamp
						AND sqa.is_hidden != true
						AND sqa.scheduled_on is NOT NULL
						AND p.deleted_on is NULL' . $strWhereCondition;

		if( false == array_key_exists( 'daily_nugget', $arrstrCommentsFilter ) ) {
			$strSql .= ' Order by ' . $arrstrCommentsFilter['sorting_field'] . ' ' . $arrstrCommentsFilter['sorting_order'];
		} else {
			$strSql .= ' Order by sqa.scheduled_on DESC';
		}

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strSql .= ' OFFSET	' . ( int ) $intOffset . ' LIMIT	' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchDailyNuggetsCount( $objDatabase ) {

		$strSql = 'SELECT
						count(DISTINCT sqa.scheduled_on)
					FROM
						persons p
						JOIN surveys s on s.responder_reference_id = p.id
						JOIN survey_question_answers sqa on sqa.survey_id = s.id
					WHERE
						s.survey_type_id =' . CSurveyType::CUSTOMER_NPS . '
						AND s.response_datetime > (now() - \'30 days\'::interval)::timestamp
						AND sqa.is_hidden != true
						AND sqa.scheduled_on is NOT NULL
						AND p.deleted_on is NULL';

		return parent::fetchColumn( $strSql, 'count', $objDatabase );
	}

	public static function fetchPaginatedSurveyQuestionAnswersByDays( $arrstrCommentsFilter, $intFilterLastDays, $strCommentsSection, $objDatabase ) {

		if( false == valStr( $strCommentsSection ) ) return NULL;

		$intOffset	= ( 0 < $arrstrCommentsFilter['page_no'] ) ? $arrstrCommentsFilter['page_size'] * ( $arrstrCommentsFilter['page_no'] - 1 ) : 0;
		$intLimit	= ( int ) $arrstrCommentsFilter['page_size'];

		if( 'promoters' == $strCommentsSection ) {
			$strWhereCondition = ' AND sqa.numeric_weight >= ' . CSurvey::SCORE_PROMOTER;
		} elseif( 'detractors' == $strCommentsSection ) {
			$strWhereCondition = ' AND sqa.numeric_weight <= ' . CSurvey::SCORE_DETRACTOR;
		} elseif( 'passives' == $strCommentsSection ) {
			$strWhereCondition = ' AND sqa.numeric_weight > ' . CSurvey::SCORE_DETRACTOR . ' AND sqa.numeric_weight < ' . CSurvey::SCORE_PROMOTER;
		}

		$strSql = 'SELECT s.id AS survey_id,
							s.responder_reference_id,
							s.survey_type_id,
							p.ps_lead_id,
							sqa.answer,
							sqa.answer_modified,
							sqa.numeric_weight AS rating,
							(p.name_first || \' \' || p.name_last) AS company_user_fullname,
							pr.name AS person_role,
							pr.is_executive,
							p.permissioned_units,
							p.company_name,
							p.cid as company_id,
							s.response_datetime,
							s.survey_datetime,
							prp.total_units
					FROM survey_question_answers sqa
							LEFT JOIN surveys s ON sqa.survey_id = s.id
							LEFT JOIN persons p ON s.responder_reference_id = p.id
							LEFT JOIN person_roles pr ON pr.id = p.person_role_id
							LEFT JOIN ps_lead_details pld ON pld.ps_lead_id = p.ps_lead_id
							LEFT JOIN
							(
							SELECT SUM(p.number_of_units) AS total_units,
									p.cid
							FROM properties p
									LEFT JOIN clients mc ON (p.cid = mc.id)
									LEFT JOIN ps_leads pl ON (mc.ps_lead_id = pl.id)
							WHERE p.is_disabled <> 1 AND
									 pl.id = mc.ps_lead_id AND
									 p.termination_date IS NULL
							GROUP BY p.cid
							) prp ON prp.cid = pld.cid
						WHERE
							s.response_datetime >= now( ) - \'' . ( int ) $intFilterLastDays . ' days\' :: INTERVAL
							AND s.response_datetime <= now( )' . $strWhereCondition . '
							AND s.responder_reference_id IS NOT NULL
							AND s.survey_type_id = ' . ( int ) CSurveyType::CUSTOMER_NPS . '
							AND ( p.name_first IS NOT NULL OR p.name_last IS NOT NULL )
							AND sqa.answer_modified IS NOT NULL
							AND sqa.scheduled_on IS NULL
							AND sqa.is_hidden != true
							AND sqa.deleted_on IS NULL
							OR (
								sqa.is_closed = ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . '
								AND s.response_datetime >= now( ) - \'' . ( int ) $intFilterLastDays . ' days\' :: INTERVAL ' . $strWhereCondition . '
								AND s.survey_type_id = ' . ( int ) CSurveyType::CUSTOMER_NPS . '
									AND ( p.name_first IS NOT NULL OR p.name_last IS NOT NULL )
								AND ( sqa.answer_modified IS NOT NULL)
								AND sqa.scheduled_on IS NULL
								AND sqa.deleted_on IS NULL
								)
						ORDER BY ' . $arrstrCommentsFilter['order_by_field'] . ' ' . $arrstrCommentsFilter['order_by_type'] . ', pr.is_executive DESC';

		if( false == is_null( $intOffset ) && false == is_null( $intLimit ) ) {
			$strSql .= ' OFFSET	' . ( int ) $intOffset . ' LIMIT	' . ( int ) $intLimit;
		}

		return fetchData( $strSql, $objDatabase );

	}

	public static function fetchSurveyQuestionAnswersCountByDays( $intFilterLastDays, $strCommentsSection, $objDatabase ) {

		if( false == valStr( $strCommentsSection ) ) return NULL;

		if( 'promoters' == $strCommentsSection ) {
			$strWhereCondition = ' AND sqa.numeric_weight >= ' . ( int ) CSurvey::SCORE_PROMOTER;
		} elseif( 'detractors' == $strCommentsSection ) {
			$strWhereCondition = ' AND sqa.numeric_weight <= ' . ( int ) CSurvey::SCORE_DETRACTOR;
		} elseif( 'passives' == $strCommentsSection ) {
			$strWhereCondition = ' AND sqa.numeric_weight > ' . ( int ) CSurvey::SCORE_DETRACTOR . ' AND sqa.numeric_weight < ' . CSurvey::SCORE_PROMOTER;
		}

		$strSql = 'SELECT DISTINCT ( s.id )
						FROM surveys s
							LEFT JOIN survey_question_answers sqa ON (s.id = sqa.survey_id)
							LEFT JOIN persons p ON (p.id = s.responder_reference_id)
							LEFT JOIN person_roles pr ON (pr.id = p.person_role_id)
						WHERE
							s.response_datetime >= now( ) - \'' . ( int ) $intFilterLastDays . ' days\' :: INTERVAL
							AND s.response_datetime <= now( )' . $strWhereCondition . '
							AND s.responder_reference_id IS NOT NULL
							AND s.survey_type_id = ' . ( int ) CSurveyType::CUSTOMER_NPS . '
							AND ( p.name_first IS NOT NULL OR p.name_last IS NOT NULL )
							AND sqa.is_hidden != true
							AND sqa.answer_modified IS NOT NULL
							AND sqa.scheduled_on IS NULL
							AND sqa.deleted_on IS NULL
							OR (
								sqa.is_closed = ' . CSurveyQuestionAnswer::COMMENTED_CLOSED_OUT . '
								AND s.response_datetime >= now( ) - \'' . ( int ) $intFilterLastDays . ' days\' :: INTERVAL ' . $strWhereCondition . '
								AND s.survey_type_id = ' . ( int ) CSurveyType::CUSTOMER_NPS . '
								AND ( p.name_first IS NOT NULL OR p.name_last IS NOT NULL )
								AND ( sqa.answer_modified IS NOT NULL)
								AND sqa.scheduled_on IS NULL
								AND sqa.deleted_on IS NULL
								)';

		return \Psi\Libraries\UtilFunctions\count( fetchData( $strSql, $objDatabase ) );
	}

	public static function fetchScheduledAndUnScheduledSurveyQuestionAnswers( $strScheduledDate, $objDatabase ) {

		if( false == valStr( $strScheduledDate ) ) return NULL;

		if( self::PAST_DATE == $strScheduledDate ) {
			$strWhereCondition = 'AND s.response_datetime >= now() - \'30 days\'::INTERVAL
									AND s.response_datetime <= now()
									AND sqa.scheduled_on::date = \'' . $strScheduledDate . '\'::date';
		} else {
			$strWhereCondition = 'AND sqa.scheduled_on::date = \'' . $strScheduledDate . '\'::date';
		}

		$strSql = 'SELECT
						s.id AS survey_id,
						s.survey_type_id,
						s.responder_reference_id,
						sqa.answer_modified,
						sqa.numeric_weight AS rating,
						sqa.scheduled_on,
						(p.name_first || \' \' || p.name_last) AS company_user_fullname,
						pr.name AS person_role,
						pr.is_executive,
						p.permissioned_units,
						p.cid,
						p.id,
						p.ps_lead_id,
						p.company_name,
						prp.total_units
					FROM
						survey_question_answers sqa
						LEFT JOIN surveys s ON sqa.survey_id = s.id
						LEFT JOIN persons p ON s.responder_reference_id = p.id
						LEFT JOIN person_roles pr ON pr.id = p.person_role_id
						LEFT JOIN ps_lead_details pld ON pld.ps_lead_id = p.ps_lead_id
						LEFT JOIN
						(
							SELECT SUM(p.number_of_units) AS total_units,
								p.cid
							 FROM properties p
								LEFT JOIN clients mc ON (p.cid = mc.id)
								LEFT JOIN ps_leads pl ON (mc.ps_lead_id = pl.id)
							WHERE p.is_disabled <> 1 AND
								pl.id = mc.ps_lead_id AND
								p.termination_date IS NULL
							GROUP BY p.cid
						) prp ON prp.cid = pld.cid
					WHERE
						s.responder_reference_id IS NOT NULL
						AND sqa.is_hidden != true
						AND s.survey_type_id = ' . ( int ) CSurveyType::CUSTOMER_NPS . '
						AND sqa.answer_modified IS NOT NULL
						AND sqa.deleted_on IS NULL ' . $strWhereCondition . '
					ORDER BY
						pr.is_executive DESC,
						sqa.numeric_weight DESC';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSurveyQuestionAnswersByScheduledDate( $strScheduledDate, $objDatabase ) {

		if( false == valStr( $strScheduledDate ) ) return NULL;

		$strSql = 'SELECT
							*
						FROM
							survey_question_answers
						WHERE
							scheduled_on = \'' . $strScheduledDate . ' 14:00:00\'::timestamp';

		return self::fetchSurveyQuestionAnswers( $strSql, $objDatabase );
	}

	public static function fetchSurveyQuestionAnswersBySurveyIdsByScheduledDate( $arrintSurveyIds, $strScheduledDate = NULL, $objDatabase ) {

		if( false == valArr( $arrintSurveyIds ) ) return NULL;

		if( true == valStr( $strScheduledDate ) ) {
			$strWhereClauseConidtion = 'AND scheduled_on = \'' . $strScheduledDate . ' 14:00:00\'::timestamp';
		}

		$strSql = 'SELECT
							*
						FROM
							survey_question_answers
						WHERE
							survey_id IN (' . implode( ',', $arrintSurveyIds ) . ')' . $strWhereClauseConidtion;

		return self::fetchSurveyQuestionAnswers( $strSql, $objDatabase );
	}

	public static function fetchSurveyQuestionAnswersBySurveyIdBySurveyTemplateId( $arrintSurveyId, $intSurveyTemplateId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						survey_question_answers
					WHERE
						survey_id = ' . ( int ) $arrintSurveyId . ' AND
						survey_template_id = ' . ( int ) $intSurveyTemplateId;

		return self::fetchSurveyQuestionAnswers( $strSql, $objDatabase );
	}

	public static function fetchSurveyQuestionAnswerByPersonEmailId( $strMailReceievedFrom, $objDatabase ) {

		$strSql = 'SELECT
						sqa.id
					FROM
						survey_question_answers sqa
						JOIN surveys s ON s.id = sqa.survey_id
						JOIN persons p ON p.id = s.responder_reference_id
					WHERE
						LOWER( p.email_address ) = \'' . \Psi\CStringService::singleton()->strtolower( $strMailReceievedFrom ) . '\'
						AND s.survey_type_id = ' . CSurveyType::CUSTOMER_NPS . '
					ORDER BY
						s.response_datetime DESC LIMIT 1';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchSurveyQuestionAnswerBySubjectReferenceId( $intSubjectReferenceId, $objDatabase ) {

		$strSql = ' SELECT
						DISTINCT ON ( s.id )
						sqa.survey_template_id,
						sqa.survey_template_question_id,
						sqa.numeric_weight
					FROM
						surveys s
					JOIN survey_question_answers sqa ON (s.id = sqa.survey_id)
					WHERE
						s.subject_reference_id = ' . ( int ) $intSubjectReferenceId . ' AND
						s.survey_template_id IN (' . implode( ', ', CSurveyTemplate::$c_arrintNoDuesSurveyTemplates ) . ') AND
						sqa.survey_template_question_id IN (' . CSurveyTemplateQuestion::QUESTION_ADMIN_NO_DUES . ',' . CSurveyTemplateQuestion::QUESTION_FINANCE_NO_DUES . ',' . CSurveyTemplateQuestion::QUESTION_HR_NO_DUES . ',' . CSurveyTemplateQuestion::QUESTION_LIVE_IT_NO_DUES . ',' . CSurveyTemplateQuestion::QUESTION_SECURITY_NO_DUES . ',' . CSurveyTemplateQuestion::QUESTION_SYSTEM_ADMIN_NO_DUES . ',' . CSurveyTemplateQuestion::QUESTION_MANAGER_NO_DUES . ',' . CSurveyTemplateQuestion::QUESTION_TRAINING_NO_DUES . ')';

		return fetchData( $strSql, $objDatabase );

	}

}
?>