<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CRoles
 * Do not add any new functions to this class.
 */

class CRoles extends CBaseRoles {

	public static function fetchRoles( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CRole', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchRole( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CRole', $objDatabase, DATA_CACHE_MEMORY, $intSpecificLifetime = NULL );
	}

	public static function fetchAllRoles( $objDatabase ) {
		return parent::fetchRoles( 'SELECT * FROM roles WHERE is_published = 1 ORDER BY name', $objDatabase );
	}

	public static function fetchRolesByUserId( $intUserId, $objDatabase ) {

		$strSql = 'SELECT
						r.*
					FROM
						roles r,
						user_roles ur
					WHERE
						r.id = ur.role_id
						AND ur.deleted_by IS NULL
						AND ur.user_id = ' . ( int ) $intUserId;

		return parent::fetchRoles( $strSql, $objDatabase );
	}

	public static function fetchRolesByRoleIds( $arrintRoles, $objDatabase ) {

		if( false == valArr( $arrintRoles ) ) return false;

		$strSql = 'SELECT
						*
					FROM
						roles
					WHERE id IN (' . implode( ',', $arrintRoles ) . ')';

		return parent::fetchRoles( $strSql, $objDatabase );
	}

	public static function fetchRolesByRoleId( $intRoleId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						roles
					WHERE id=' . ( int ) $intRoleId;

		return self::fetchRole( $strSql, $objDatabase );
	}

}
?>