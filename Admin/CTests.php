<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTests
 * Do not add any new functions to this class.
 */
class CTests extends CBaseTests {

	// Survey will not be fetched using this filter.

	public static function fetchTestsByTestsFilter( $objTestsFilter, $objDatabase ) {

		$objTestsFilter = ( true == valObj( $objTestsFilter, 'CTestsFilter' ) ) ? $objTestsFilter : new CTestsFilter();

		$strTableNames 		= 'tests t';
		if( false == is_null( $objTestsFilter->getRequiredFields() ) ) {
			$strFields = $objTestsFilter->getRequiredFields();
		} else {
			$strFields = ' t.*';
		}
		$strCustomFields	= '';
		$arrstrCustomFields	= array();
		if( true == $objTestsFilter->getIsFalseCondition() ) {
			$strConditions = ' false';
		} else {
			$strConditions = ' true ';
		}

		$strSortBy			= ' ORDER BY t.updated_on DESC';
		$intOffset			= ( 0 < $objTestsFilter->getPageNo() ) ? ( int ) $objTestsFilter->getPageSize() * ( $objTestsFilter->getPageNo() - 1 ) : 0;
		$intLimit			= ( int ) $objTestsFilter->getPageSize();

		$arrstrAndSearchParameters = self::fetchSearchCriteria( $objTestsFilter );
		if( true == valStr( $objTestsFilter->getSortBy() ) ) $strSortBy = self::fetchSortByCriteria( $objTestsFilter );

		if( true == valStr( $objTestsFilter->getSortType() ) ) $strSortBy .= $objTestsFilter->getSortType();

		if( true == valArr( $objTestsFilter->getCustomFields() ) ) {
			foreach( $objTestsFilter->getCustomFields() as $strCustomFieldName ) {
				switch( $strCustomFieldName ) {
					case 'employees':
						$strTableNames .= ' JOIN employees e ON( e.id = u.employee_id )';
						break;

					case 'users':
						$strTableNames .= ' JOIN users u ON( t.created_by = u.id )';
						break;

					default:
						// default case
						break;
				}
			}

			if( true == valArr( $arrstrCustomFields ) ) {
				$strCustomFields = implode( ',', $arrstrCustomFields );
			}
		}

		$strConditions	.= ( true == valArr( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '';
		$strOffSetCondition	= ( 0 < $intLimit ) ? ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit : '';
		switch( $objTestsFilter->getReturnCriteria() ) {
			case CEosFilter::RETURN_CRITERIA_COUNT:

				$strSql = 'SELECT COUNT(t.id) FROM ' . $strTableNames . ' WHERE ' . $strConditions;
				$arrintCount = fetchData( $strSql, $objDatabase );
				return ( true == isset ( $arrintCount[0]['count'] ) ) ? ( int ) $arrintCount[0]['count'] : 0;
				break;

			case CEosFilter::RETURN_CRITERIA_CUSTOM_ARRAY:
				if( true == valStr( $strCustomFields ) ) {
					$strSql = 'SELECT ' . $strCustomFields . ' FROM ' . $strTableNames . ' WHERE ' . $strConditions . $strSortBy . $strOffSetCondition;
				}

			case CEosFilter::RETURN_CRITERIA_ARRAY:

				if( true == valStr( $strFields ) ) {
					$strFields	= ( true == valStr( $strCustomFields ) ) ? $strCustomFields . ', ' . $strFields : $strFields;
					$strSql 	= 'SELECT ' . $strFields . ' FROM ' . $strTableNames . ' WHERE ' . $strConditions . $strSortBy . $strOffSetCondition;
				}

				$arrstrMixedData 		= fetchData( $strSql, $objDatabase );
				return $arrstrMixedData;
				break;

			default:
				$strFields	= ( true == valStr( $strCustomFields ) ) ? $strCustomFields . ', ' . $strFields : $strFields;
				$strSql		= 'SELECT ' . $strFields . ' FROM ' . $strTableNames . ' WHERE ' . $strConditions . $strSortBy;

				if( true == $objTestsFilter->getReturnSingleRecord() ) {
					$strSql = $strSql . ' LIMIT 1;';
					return parent::fetchTest( $strSql, $objDatabase );
				}
				$strSql .= $strOffSetCondition;

				return parent::fetchTests( $strSql, $objDatabase );
				break;
		}
	}

	public static function fetchSearchCriteria( $objTestsFilter ) {

		$arrstrAndSearchParameters = array();

		$arrstrAndSearchParameters[] = 't.test_type_id = 1';
		if( true == valStr( $objTestsFilter->getDeletedBy() ) ) $arrstrAndSearchParameters[] = 't.deleted_by = ' . ( int ) $objTestsFilter->getDeletedBy() . '';
		elseif( false == $objTestsFilter->getIsDeleted() ) $arrstrAndSearchParameters[] = 't.deleted_by IS NULL';
		if( false == is_null( $objTestsFilter->getCreatedBy() ) ) 	$arrstrAndSearchParameters[] = 't.created_by = ' . ( int ) $objTestsFilter->getCreatedBy() . '';
		if( true == valArr( $objTestsFilter->getTestIds() ) ) 	$arrstrAndSearchParameters[] = 't.id IN(' . implode( ',', $objTestsFilter->getTestIds() ) . ')';
		if( false == is_null( $objTestsFilter->getPublished() ) ) $arrstrAndSearchParameters[] = 't.is_published = ' . ( int ) $objTestsFilter->getPublished();
		if( false == is_null( $objTestsFilter->getTestName() ) ) $arrstrAndSearchParameters[] = 't.name ILIKE \'%' . $objTestsFilter->getTestName() . '%\'';
		if( false == is_null( $objTestsFilter->getTestNameQuickSearch() ) ) $arrstrAndSearchParameters[] = 't.name ILIKE \'%' . addslashes( $objTestsFilter->getTestNameQuickSearch() ) . '%\'';
		if( true == $objTestsFilter->getIsDeleted() ) $arrstrAndSearchParameters[] = 't.deleted_by IS NOT NULL';

		return $arrstrAndSearchParameters;
	}

	public static function fetchSortByCriteria( $objTestsFilter ) {

		$strSortBy = ' ORDER BY ';

		switch( $objTestsFilter->getSortBy() ) {
			case 'name':
				$strSortBy .= ' t.name ';
				break;

			case 'created_by':
				$strSortBy .= ' e.name_full ';
				break;

			default:
				$strSortBy .= ' t.updated_on ';
				break;
		}

		return $strSortBy;
	}

	public static function fetchUnassociatedTestsToTestTemplate( $objDatabase, $intUserId = NULL ) {

		if( false == is_null( $intUserId ) ) {
			$strWhere = 'AND t.created_by = ' . ( int ) $intUserId;
		} else {
			$strWhere = '';
		}

		$strSql = 'SELECT
						t.*
					FROM
						tests t
					WHERE
						t.test_type_id = 1
						AND t.deleted_by IS NULL ' . $strWhere . '
						AND t.id NOT IN ( SELECT
												test_id
											FROM
												test_question_template_associations
											) ';

		return self::fetchTests( $strSql, $objDatabase );
	}

	public static function fetchTestsByTestTypeIdOrByCountryCode( $intTestTypeId, $objDatabase, $strCountryCode = NULL, $arrintTestIds = NULL ) {

		$strWhere = ' AND deleted_by IS NULL';

		if( true == valArr( $arrintTestIds ) ) {
			$strWhere .= ' AND id IN ( ' . implode( ',', $arrintTestIds ) . ' )';
		}

		$strSql = 'SELECT * FROM tests WHERE test_type_id = ' . ( int ) $intTestTypeId . $strWhere;
		if( false == is_null( $strCountryCode ) ) $strSql .= ' AND country_code IN( \'' . $strCountryCode . '\' )';

		$strSql .= ' ORDER BY name';

		return self::fetchTests( $strSql, $objDatabase );
	}

	public static function fetchPaginatedPublishedTestsCount( $arrintGroups, $intUserId, $objDatabase ) {

		if( false == valArr( $arrintGroups ) ) {
			return NULL;
		}

		$strGroups = implode( ',', $arrintGroups );

		$strSql	= 'SELECT
						count( distinct( t.id ) )
					FROM
						tests t,
						test_groups tg
					WHERE
						t.id = tg.test_id
						AND tg.group_id IN ( ' . $strGroups . ' )
						AND t.is_published = 1
						AND t.deleted_by IS NULL
						AND t.id NOT IN ( SELECT
												ts.test_id
											FROM
												test_submissions ts
											WHERE
												ts.user_id = ' . ( int ) $intUserId . ' )';

		return self::fetchTests( $strSql, $objDatabase );
	}

	public static function fetchPaginatedPublishedTestsByTestTypeIdByCountryCode( $intPageNo, $intPageSize, $intTestTypeId, $arrintGroupsIds, $intUserId, $strCountryCode, $objDatabase ) {

		if( false == valArr( $arrintGroupsIds ) ) return NULL;

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		$strSql	= 'SELECT
						t.*
					 FROM
						tests t
						FULL JOIN test_groups tg ON ( t.id = tg.test_id AND tg.group_id IN ( ' . implode( ',', $arrintGroupsIds ) . ' ) )
						FULL JOIN test_users tu ON ( t.id = tu.test_id AND tu.user_id = ' . ( int ) $intUserId . ' )
					WHERE
						t.test_type_id = ' . ( int ) $intTestTypeId . '
						AND t.country_code IN ( \'' . $strCountryCode . '\' )
						AND	t.is_published = 1
						AND	t.deleted_by IS NULL
						OFFSET ' . ( int ) $intOffset . '
						LIMIT ' . $intLimit;

		return self::fetchTests( $strSql, $objDatabase );
	}

	public static function fetchCountOrPeginatedPublishedTestsByEmployeeId( $arrmixParameteres, $objDatabase, $intPageNo = NULL, $intPageSize = NULL, $boolCount = false ) {

		if( false == is_numeric( $arrmixParameteres['employee_id'] ) ) return NULL;

		$intOffset = ( 0 < $intPageNo ) ? $intPageSize * ( $intPageNo - 1 ) : 0;
		$intLimit = ( int ) $intPageSize;

		if( true == $boolCount ) {
			$strSql = 'SELECT count( DISTINCT t.id ) ';
		} else {
			$strSql = 'SELECT DISTINCT (t.id) as test_id, t.test_type_id, t.name, t.is_mandatory,t.created_on, t.is_hide_score';
		}

		$strSql .= ' FROM
						tests t
						LEFT JOIN employee_tests et ON ( t.id = et.test_id AND et.employee_id = ' . ( int ) $arrmixParameteres['employee_id'] . ' )
						LEFT JOIN test_submissions ts ON ( ts.test_id = t.id AND ts.employee_id = ' . ( int ) $arrmixParameteres['employee_id'] . ' )
					WHERE
						t.deleted_by IS NULL
						AND t.is_published = 1
						AND ( ts.employee_id = ' . ( int ) $arrmixParameteres['employee_id'] . ' OR et.employee_id = ' . ( int ) $arrmixParameteres['employee_id'] . ' )';

		if( false == $boolCount ) {
			if( false == is_null( $intPageNo ) && false == is_null( $intPageSize ) ) $strSql .= 'ORDER BY t.created_on DESC OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;

			return self::fetchTests( $strSql, $objDatabase );
		} else {
			$arrintResponse = fetchData( $strSql, $objDatabase );
			if( true == isset( $arrintResponse[0]['count'] ) ) return $arrintResponse[0]['count'];
		}
	}

	public static function fetchTestsByTestQuestionGroupIdByDateRange( $intTestQuestionGroupId, $strStartDate = NULL, $strEndDate = NULL, $objDatabase ) {

		if( false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			$strSubSql = 'AND date_part( \'year\', test_users.created_on ) = date_part( \'year\', now() )';
		} else {
			$strSubSql = 'AND test_users.created_on BETWEEN \'' . $strStartDate . '\'::date AND \'' . $strEndDate . '\'::date ';
		}

		$strSql = 'SELECT
						t.id,
						t.name,
						test_users.created_on::date AS assigned_on
					FROM tests t
						LEFT JOIN test_question_associations tqa ON ( t.id = tqa.test_id )
						LEFT JOIN test_questions tq ON ( tq.id = tqa.test_question_id )
						LEFT JOIN (
							SELECT et.test_id, et.created_on FROM employee_tests et
							UNION SELECT tg.test_id, tg.created_on FROM test_groups tg
						) AS test_users ON ( test_users.test_id = t.id )
					WHERE t.is_published = 1
						AND tq.test_question_group_id = ' . ( int ) $intTestQuestionGroupId . '
						' . $strSubSql . '
					GROUP BY
						t.id,
						t.name,
						test_users.created_on
					ORDER BY
						test_users.created_on DESC,
						t.name';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestEmployeesByTestIdsByDateRange( $arrintTestIds, $strStartDate = NULL, $strEndDate = NULL, $objDatabase ) {

		if( false == valArr( $arrintTestIds ) ) return NULL;

		if( false == valStr( $strStartDate ) || false == valStr( $strEndDate ) ) {
			$strSubSql = 'AND date_part( \'year\', test_users.created_on ) = date_part( \'year\', now() )';
		} else {
			$strSubSql = 'AND test_users.created_on BETWEEN \'' . $strStartDate . '\'::date AND \'' . $strEndDate . '\'::date ';
		}

		$strSql = 'SELECT
						DISTINCT ( t.id, test_users.employee_id ),
						t.id,
						test_users.employee_id,
						test_users.created_on::date AS assigned_on,
						ts.id AS test_submition_id,
						CASE
							WHEN
								ts.score LIKE \'%/%\'
							THEN
								( ( CAST ( split_part( score, \'/\', 1 ) as FLOAT ) ) * 100 ) / ( CAST ( split_part ( score, \'/\', 2 ) as FLOAT ) )
						END as score
					FROM tests t
						 LEFT JOIN
							 ( SELECT et.test_id,et.employee_id,et.created_on FROM employee_tests et
								UNION SELECT tg.test_id,e.id as employee_id,tg.created_on FROM test_groups tg LEFT JOIN employees e ON ( e.department_id = tg.department_id OR e.office_id = tg.office_id OR e.designation_id = tg.designation_id )
								UNION SELECT tg.test_id,u.employee_id,tg.created_on FROM test_groups tg LEFT JOIN user_groups ug ON ( ug.group_id = tg.group_id AND ug.deleted_by IS NULL ) LEFT JOIN users u ON ( ug.user_id = u.id ) WHERE u.employee_id IS NOT NULL
								UNION SELECT tg.test_id,te.employee_id,tg.created_on FROM test_groups tg JOIN team_employees te ON ( te.team_id = tg.team_id AND te.is_primary_team = 1 ) LEFT JOIN employees e3 ON ( e3.id = te.employee_id  )
							 ) AS test_users ON ( test_users.test_id = t.id )
						LEFT JOIN test_submissions ts ON ( t.id = ts.test_id AND test_users.employee_id = ts.employee_id )
						LEFT JOIN employees e ON ( test_users.employee_id = e.id )
					WHERE t.deleted_on IS NULL
						AND test_users.employee_id IS NOT NULL
						AND t.id IN ( ' . implode( ',', $arrintTestIds ) . ' ) ' . $strSubSql;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestEmployeesByTestIdByAssignedDate( $intTestId, $strAssignedDate, $objDatabase, $strTestEmployeeFilter = NULL, $intQuestionId = NULL ) {

		// we have to change score fields datatype from varchar to integer, we will do it in next enhancement.
		if( true == is_null( $intTestId ) || false == valStr( $strAssignedDate ) ) {
			return NULL;
		}

		$strJoin = '';
		if( false == is_null( $intQuestionId ) && true == is_numeric( $intQuestionId ) ) {
			$strJoin = ' LEFT JOIN test_submission_answers tsa ON ( tsa.test_submission_id = ts.id ) ';
		}

		switch( $strTestEmployeeFilter ) {
			case 'unappreared':
				$strSubSql = ' AND ts.id IS NULL';
				break;

			case 'appreared':
				$strSubSql = ' AND ts.id IS NOT NULL';
				break;

			case 'failed':
				$strSubSql = ' AND ts.id IS NOT NULL
							   AND ( ( CASE
									WHEN
										ts.score LIKE \'%/%\'
									THEN
										( ( CAST ( split_part( score, \'/\', 1 ) as FLOAT ) ) * 100 ) / ( CAST ( split_part ( score, \'/\', 2 ) as FLOAT ) )
									END ) < ' . CTestSubmission::ZONE_GREEN . '
								OR ts.score IS NULL )';
				break;

			case 'success':
				$strSubSql = ' AND ts.id IS NOT NULL
								AND ( CASE
									WHEN
										ts.score LIKE \'%/%\'
									THEN
										( ( CAST ( split_part( score, \'/\', 1 ) as FLOAT ) ) * 100 ) / ( CAST ( split_part ( score, \'/\', 2 ) as FLOAT ) )
									END ) >= ' . CTestSubmission::ZONE_GREEN;
				break;

			case 'success_answer':
				$strSubSql = ' AND tsa.is_correct = 1
								AND tsa.test_question_id = ' . ( int ) $intQuestionId;
				break;

			case 'failed_answer':
				$strSubSql = ' AND tsa.is_correct = 0
								AND tsa.test_question_id = ' . ( int ) $intQuestionId;
				break;

			default:
				$strSubSql = '';
		}

		$strSql = 'SELECT
						t.id,
						test_users.employee_id,
						test_users.created_on AS assigned_on,
						ts.created_on AS submitted_on,
						ts.id AS test_submition_id,
						e.name_full,
						d.name AS department,
						e1.name_full AS manager_name,
						CASE
							WHEN
								ts.score LIKE \'%/%\'
							THEN
								( ( CAST ( split_part( score, \'/\', 1 ) as FLOAT ) ) * 100 ) / ( CAST ( split_part ( score, \'/\', 2 ) as FLOAT ) )
						END as score
					FROM tests t
						LEFT JOIN
							 ( SELECT et.test_id,et.employee_id,et.created_on FROM employee_tests et
								UNION SELECT tg.test_id,e.id as employee_id,tg.created_on FROM test_groups tg LEFT JOIN employees e ON ( e.department_id = tg.department_id OR e.office_id = tg.office_id OR e.designation_id = tg.designation_id )
								UNION SELECT tg.test_id,u.employee_id,tg.created_on FROM test_groups tg LEFT JOIN user_groups ug ON ( ug.group_id = tg.group_id AND ug.deleted_by IS NULL ) LEFT JOIN users u ON ( ug.user_id = u.id ) WHERE u.employee_id IS NOT NULL
								UNION SELECT tg.test_id,te.employee_id,tg.created_on FROM test_groups tg JOIN team_employees te ON ( te.team_id = tg.team_id AND te.is_primary_team = 1 ) LEFT JOIN employees e3 ON ( e3.id = te.employee_id  )
			  				 ) AS test_users ON ( test_users.test_id = t.id )
						LEFT JOIN test_submissions ts ON ( t.id = ts.test_id AND test_users.employee_id = ts.employee_id )
						LEFT JOIN employees e ON ( e.id = test_users.employee_id )
						LEFT JOIN departments d ON ( d.id = e.department_id )
						LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id ) '
						. $strJoin . '
					WHERE t.deleted_on IS NULL
							AND test_users.employee_id IS NOT NULL
							AND t.id = ' . ( int ) $intTestId . '
							AND date_trunc(\'day\', test_users.created_on ) =  date_trunc(\'day\', \'' . $strAssignedDate . '\'::date)
							' . $strSubSql . '
					ORDER BY
							e.name_full';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPendingTestsByDateRange( $strBeginDate, $strEndDate, $objDatabase ) {

		$strSql = ' SELECT
						filtered_data.name,
						filtered_data.employee_name,
						filtered_data.email_address,
						max ( filtered_data.assigned_date ) AS assigned_date,
						filtered_data.office_id,
						CASE
							WHEN e1.office_id IN ( ' . COffice::TOWER_8 . ' , ' . COffice::TOWER_9 . ' ) THEN e1.email_address
							ELSE NULL
						END AS primary_reporting_manager,
						CASE
							WHEN e2.office_id IN (' . COffice::TOWER_8 . ' , ' . COffice::TOWER_9 . ' ) THEN e2.email_address
							ELSE NULL
						END AS secondary_reporting_manager,
						CASE
							WHEN e3.office_id IN ( ' . COffice::TOWER_8 . ' , ' . COffice::TOWER_9 . ' ) THEN e3.email_address
							ELSE NULL
						END AS bu_hr
					FROM
						(
							SELECT
								t.name,
								t.id AS test_id,
								et.employee_id,
								e.name_full AS employee_name,
								e.email_address AS email_address,
								e.office_id,
								CASE
									WHEN t.published_on >= et.created_on THEN ( t.published_on )
									ELSE ( et.created_on )
								END AS assigned_date
							FROM
								tests t
								LEFT JOIN employee_tests et ON ( t.id = et.test_id )
								LEFT JOIN employees e ON ( e.id = et.employee_id )
							WHERE
								t.deleted_by IS NULL
								AND t.published_on IS NOT NULL
								AND t.is_published = 1
								AND e.employee_status_type_id = 1
		 						AND DATE ( t.published_on ) BETWEEN \'' . $strBeginDate . '\' AND  \'' . $strEndDate . '\'
						) AS filtered_data
							LEFT JOIN employees e ON ( filtered_data.employee_id = e.id )
							LEFT JOIN team_employees te ON ( filtered_data.employee_id = te.employee_id AND te.is_primary_team = 1 )
							LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
							LEFT JOIN employees e2 ON ( e2.id = e1.reporting_manager_id )
							LEFT JOIN teams t1 ON ( te.team_id = t1.id )
							LEFT JOIN employees e3 ON ( e3.id = t1.hr_representative_employee_id )
							LEFT JOIN test_submissions ts ON ( filtered_data.test_id = ts.test_id AND filtered_data.employee_id = ts.employee_id )
						WHERE
							ts.id IS NULL
						GROUP BY
							filtered_data.name,
							filtered_data.employee_name,
							filtered_data.email_address,
							filtered_data.office_id,
							e1.office_id,
							e1.email_address,
							e2.office_id,
							e2.email_address,
							e3.office_id,
							e3.email_address';
		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchPaginatedTestsByTestFilter( $objTestFilter, $objDatabase, $boolIsCount = false ) {

		$intOffset 		= ( 0 < $objTestFilter->getPageNo() ) ? $objTestFilter->getPageSize() * ( $objTestFilter->getPageNo() - 1 ) : 0;
		$intLimit  		= ( int ) $objTestFilter->getPageSize();
		$strOrderClause = '';
		$strOrder 		= '';

		if( false == is_null( $objTestFilter ) && true == valStr( $objTestFilter->getOrderByField() ) ) {
			$strOrder = ( false == $objTestFilter->getOrderByType() ) ? ' DESC ' : $objTestFilter->getOrderByType();

			$strOrderClause = ' ORDER BY ';

			switch( $objTestFilter->getOrderByField() ) {
				case 'department_id':
					$strOrderClause .= ' department_name ';
					break;

				case 'manager_employee_id':
					$strOrderClause .= ' reporting_manager ';
					break;

				case 'employee_id':
					$strOrderClause .= ' employee_name ';
					break;

				case 'is_attempted':
					$strOrderClause .= ' test_submission_id ';
					break;

				default:
					$strOrderClause .= $objTestFilter->getOrderByField();
					break;
			}

			$strOrderClause = $strOrderClause . ' ' . $strOrder;

		}

		$strWhere = self::getSearchCriteria( $objTestFilter );

		$strSql = ' SELECT
						DISTINCT ( filtered_data.id ),
						filtered_data.name,
						filtered_data.employee_name,
						filtered_data.reporting_manager,
						filtered_data.department_name,
						max ( filtered_data.assigned_on ) as assigned_on,
						filtered_data.attempted_on,
						filtered_data.test_submission_id,
						max(filtered_data.created_on) as created_on
					 FROM
						(
							SELECT
								t.id,
								t.name,
								e.name_full AS employee_name,
								e1.name_full AS reporting_manager,
								d.name AS department_name,
								ts.id AS test_submission_id,
								CASE
									WHEN DATE ( t.published_on ) >= DATE ( et.created_on ) THEN t.published_on
									ELSE et.created_on
								END AS assigned_on,
								ts.created_on AS attempted_on,
								et.created_on
							FROM
								tests t
								LEFT JOIN employee_tests et ON ( t.id = et.test_id )
								LEFT JOIN employees e ON ( e.id = et.employee_id )
								LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
								LEFT JOIN test_submissions ts ON ( t.id = ts.test_id AND e.id = ts.employee_id )
								 LEFT JOIN departments d ON ( d.id = e.department_id ) ' . $strWhere . '
							UNION
							SELECT
								t.id,
								t.name,
								e.name_full AS employee_name,
								e1.name_full AS reporting_manager,
								d.name AS department_name,
								ts.id AS test_submission_id,
								CASE
									WHEN DATE ( t.published_on ) >= DATE ( tg.created_on ) THEN t.published_on
									ELSE tg.created_on
								END AS assigned_on,
								ts.created_on AS attempted_on,
								tg.created_on
							FROM
								test_groups tg
								LEFT JOIN tests t ON ( tg.test_id = t.id )
								LEFT JOIN user_groups ug ON ( tg.group_id = ug.group_id AND ug.deleted_by IS NULL )
								LEFT JOIN users u ON ( u.id = ug.user_id )
								LEFT JOIN teams tm ON ( tm.id = tg.team_id )
								LEFT JOIN team_employees te1 ON ( te1.team_id = tg.team_id AND te1.is_primary_team = 1 )
								LEFT JOIN employees e ON ( e.id = u.employee_id OR e.department_id = tg.department_id OR e.office_id = tg.office_id OR e.designation_id = tg.designation_id OR e.id = te1.employee_id )
								LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
								LEFT JOIN departments d ON ( d.id = e.department_id )
								LEFT JOIN test_submissions ts ON ( tg.test_id = ts.test_id AND e.id = ts.employee_id ) ' . $strWhere . '
					) AS filtered_data
					WHERE
						test_submission_id IS NULL
					GROUP BY
						employee_name,
						reporting_manager,
						department_name,
						name,
						id,
						test_submission_id,
						attempted_on ' . $strOrderClause;

		if( false == $boolIsCount ) {
			$strSql .= ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit;
		}

		return fetchData( $strSql, $objDatabase );
	}

	protected static function getSearchCriteria( $objTestFilter ) {

		$strWhereCondition = ' WHERE
								t.deleted_by IS NULL
								AND t.published_on IS NOT NULL
								AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		if( false == valObj( $objTestFilter, 'CTestFilter' ) ) return $strWhereCondition;

		$strWhereCondition .= ( true == valStr( $objTestFilter->getName() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' t.name ILIKE \'%' . $objTestFilter->getName() . '%\'' : '';
		$strWhereCondition .= ( true == valStr( $objTestFilter->getManagerEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' e1.id = ' . $objTestFilter->getManagerEmployeeId() : '';
		$strWhereCondition .= ( true == valStr( $objTestFilter->getEmployeeId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' e.id = ' . $objTestFilter->getEmployeeId() : '';
		$strWhereCondition .= ( true == valStr( $objTestFilter->getDepartmentId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' e.department_id = ' . $objTestFilter->getDepartmentId() : '';
		$strWhereCondition .= ( true == valStr( $objTestFilter->getTestSubmissionId() ) ) ? ( ( true == valStr( $strWhereCondition ) ) ? ' AND ' : '' ) . ' ts.id IS NULL ' : '';
		return $strWhereCondition;
	}

	public static function fetchTestDetails( $arrintTrainingSessionIds, $objDatabase ) {
		if( false == valArr( $arrintTrainingSessionIds ) ) {
			return NULL;
		}

		$strSql = 'SELECT 
						DISTINCT (tsu.employee_id),
						t.name,
						ts.id,
						tsu.score
					FROM training_sessions ts
						JOIN employee_training_sessions ets ON (ts.id = ets.training_session_id)
						JOIN users u ON (u.id = ets.user_id)
						JOIN employees e ON (e.id = u.employee_id)
						JOIN induction_sessions ise ON (ise.id = ts.induction_session_id)
						JOIN test_groups tg ON (tg.training_session_id = ts.id)
						JOIN test_submissions tsu ON (tsu.test_id = tg.test_id AND tsu.employee_id = e.id )
						JOIN tests t ON (t.id = tg.test_id)
					WHERE
						ts.id IN (' . implode( ',', $arrintTrainingSessionIds ) . ')
						AND tsu.score IS NOT NULL
					ORDER BY
						tsu.employee_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestsByIds( $arrintTestIds, $objDatabase, $strOrderByField = NULL ) {
		if( false == valArr( $arrintTestIds ) ) {
			return NULL;
		}

		$strOrder = NULL;
		if( true == valStr( $strOrderByField ) ) {
			$strOrder = ' ORDER BY ' . $strOrderByField;
		}

		$strSql = ' SELECT
						* 
					FROM
						tests
					WHERE
						id IN ( ' . implode( ',', $arrintTestIds ) . ' ) ' . $strOrder;

		return parent::fetchTests( $strSql, $objDatabase );
	}

}
?>