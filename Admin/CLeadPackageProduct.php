<?php

class CLeadPackageProduct extends CBaseLeadPackageProduct {

	protected $m_strLeadPackageId;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valLeadPackageId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valBundlePsProductId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDetails() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function getPsLeadPackageId() {
		return $this->m_strLeadPackageId;
	}

	public function valListRecurringPrice() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valListSetupPrice() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['lead_package_id'] ) ) $this->setPsLeadPackageId( $arrmixValues['lead_package_id'] );
	}

	public function setPsLeadPackageId( $intLeadPackageId ) {
		$this->m_strLeadPackageId = $intLeadPackageId;
	}

}
?>