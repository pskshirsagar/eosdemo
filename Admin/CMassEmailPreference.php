<?php

class CMassEmailPreference extends CBaseMassEmailPreference {

	public function valMassEmailId() {
		$boolIsValid = true;

		if( true == is_null( $this->getMassEmailId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'mass_email_id', 'Mass email id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valKey() {
		$boolIsValid = true;

		if( true == is_null( $this->getKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'key', 'Key is required.' ) );
		}

		return $boolIsValid;
	}

	public function valValue() {
		$boolIsValid = true;

		if( true == is_null( $this->getValue() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'value', 'Value is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valMassEmailId();
				$boolIsValid &= $this->valKey();
				$boolIsValid &= $this->valValue();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>