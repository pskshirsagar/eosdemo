<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeApplicationContacts
 * Do not add any new functions to this class.
 */

class CEmployeeApplicationContacts extends CBaseEmployeeApplicationContacts {

	public static function fetchEmployeeApplicationContactsByEmployeeApplicationId( $intEmployeeApplicationId, $objAdminDatabase ) {
		$strSql = 'SELECT * FROM employee_application_contacts WHERE employee_application_id = ' . ( int ) $intEmployeeApplicationId . ' ORDER BY contact_datetime ';
		return self::fetchEmployeeApplicationContacts( $strSql, $objAdminDatabase );
	}

	public static function fetchEmployeeApplicationContactsByEmployeeApplicationIds( $arrintEmployeeApplicationIds, $objDatabase ) {

		if( false == valArr( $arrintEmployeeApplicationIds ) ) return NULL;

		$strSql = 'SELECT * FROM employee_application_contacts WHERE employee_application_id IN ( ' . implode( ',', $arrintEmployeeApplicationIds ) . ')';

		return self::fetchEmployeeApplicationContacts( $strSql, $objDatabase );
	}

}
?>