<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSurveyTemplateQuestionGroups
 * Do not add any new functions to this class.
 */

class CSurveyTemplateQuestionGroups extends CBaseSurveyTemplateQuestionGroups {

	public static function fetchSurveyTemplateQuestionGroupByIdBySurveyTemplateId( $intId, $intSurveyTemplateId, $objDatabase ) {

		return self::fetchSurveyTemplateQuestionGroup( sprintf( 'SELECT * FROM survey_template_question_groups WHERE id = %d AND survey_template_id = %d', ( int ) $intId, ( int ) $intSurveyTemplateId ), $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionGroupsBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						survey_template_question_groups
					WHERE
						survey_template_id = ' . ( int ) $intSurveyTemplateId . '
						AND deleted_on IS NULL
					ORDER BY order_num';

		return self::fetchSurveyTemplateQuestionGroups( $strSql, $objDatabase );
	}

	public static function fetchSurveyTemplateQuestionsGroupsBySurveyTemplateIds( $arrintSurveyTemplateIds, $objDatabase, $arrintSurveyIds = NULL, $boolCountSql = false ) {

		if( false == valArr( $arrintSurveyTemplateIds ) ) return NULL;

		$strJoin = $strWhere = $strOrderByClause = '';

		if( true == valArr( $arrintSurveyIds ) ) {
			$strJoin 	= ' JOIN surveys s ON ( s.survey_template_id = stg.survey_template_id )';
			$strWhere 	= ' AND s.id IN ( ' . implode( ',', $arrintSurveyIds ) . ' )';
		}

		$strSelectClause = 'DISTINCT stg.*';

		if( true == $boolCountSql ) {
			$strSelectClause = 'COUNT( DISTINCT( stg.id ) )';
		} else {
			$strOrderByClause = ' ORDER BY order_num';
		}

		$strSql = 'SELECT
						' . $strSelectClause . '
					FROM
						survey_template_question_groups stg ' .
						$strJoin . '
						JOIN
						survey_template_questions stq ON ( stg.survey_template_id = stq.survey_template_id AND stg.id = stq.survey_template_question_group_id AND stq.is_published = 1 )
					WHERE
						stg.deleted_on IS NULL
						AND stq.survey_template_id IN (' . implode( ',', $arrintSurveyTemplateIds ) . ')' .
						$strWhere .
						$strOrderByClause;

		if( true == $boolCountSql ) {
			$arrintData = fetchData( $strSql, $objDatabase );

			if( true == valArr( $arrintData ) ) {
				return $arrintData[0]['count'];
			}
		}

		return self::fetchSurveyTemplateQuestionGroups( $strSql, $objDatabase );
	}

	public static function fetchConflictingSurveyTemplateQuestionGroupBySurveyTemplateIdByName( $intSurveyTemplateId, $strName, $intId, $objDatabase ) {

		$strWhereSql = ' WHERE
							id <>' . ( int ) $intId . '
							AND survey_template_id = ' . ( int ) $intSurveyTemplateId . '
							AND lower( name ) = lower( ' . $strName . ' )';

		return self::fetchSurveyTemplateQuestionGroupCount( $strWhereSql, $objDatabase );
	}

	public static function fetchMaxSurveyTemplateQuestionGroupOrderNumBySurveyTemplateId( $intSurveyTemplateId, $objDatabase ) {
		$strSql = 'SELECT
						Max(order_num)
					FROM
						survey_template_question_groups
					WHERE
						survey_template_id = ' . ( int ) $intSurveyTemplateId;

		$arrintMaxOrderNum = fetchData( $strSql, $objDatabase );

		return ( int ) getArrayElementByKey( 'max', $arrintMaxOrderNum[0] );

	}
}
?>