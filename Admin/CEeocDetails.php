<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEeocDetails
 * Do not add any new functions to this class.
 */

class CEeocDetails extends CBaseEeocDetails {

	public static function fetchEeocDetailByEmployeeId( $intEmployeeId, $objDatabase ) {
		$strSql = 'SELECT
						*
					FROM
						eeoc_details
					WHERE
						employee_id = ' . ( int ) $intEmployeeId;

		return parent::fetchEeocDetail( $strSql, $objDatabase );
	}

}
?>