<?php

class CReportDetail extends CBaseReportDetail {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCaReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportDataTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportSqlDetailId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valReportSpreadsheetId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valVersionNumber() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsCurrentVersion() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>