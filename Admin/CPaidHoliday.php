<?php

class CPaidHoliday extends CBasePaidHoliday {

	public static $c_arrintNationalHolidays = array(
		'01/26',
		'05/01',
		'08/15',
		'10/02'
	);

	public function valCountryCode() {
		$boolIsValid = true;

	 	if( true == is_null( $this->getCountryCode() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'country_code', 'Country is required.' ) );
	 	}

		return $boolIsValid;
	}

	public function valTitle() {

		$boolIsValid = true;

		if( true == is_null( $this->getTitle() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'title', ' Title is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDate( $objDatabase ) {

		$boolIsValid = true;

		if( true == is_null( $this->getDate() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'date', ' Date is required.' ) );

		} elseif( false == is_null( $this->getDate() ) && false == CValidation::validateDate( $this->getDate(), true ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Paid holiday date is not a valid date.' ) );

		} elseif( false == is_null( $this->m_strDate ) && ( 'Sun' == date( 'D', strtotime( $this->m_strDate ) ) || 'Sat' == date( 'D', strtotime( $this->m_strDate ) ) ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Cannot add paid holiday on a weekend, select another date.' ) );

		} elseif( false == is_null( $this->getDate() ) ) {

			$intPaidHolidayByDateCount = CPaidHolidays::fetchPaidHolidaysCountByDate( $this->m_strDate, $this->m_intId, $this->getCountryCode(), $objDatabase );

			if( false == $this->validatePaidHoliday( $objDatabase ) && 0 < $intPaidHolidayByDateCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( NULL, NULL, ' Paid holiday already exists on ' . $this->m_strDate . ' date.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validatePaidHoliday( $objDatabase = NULL ) {
		$boolIsValid = true;

		$arrobjPaidHolidays = \Psi\Eos\Admin\CPaidHolidays::createService()->fetchAllPaidHolidays( $objDatabase, true );

		if( true == valArr( $arrobjPaidHolidays ) ) {
			foreach( $arrobjPaidHolidays as $objPaidHoliday ) {
				if( true == ( $objPaidHoliday->getCountryCode() == $this->getCountryCode() ) && true == ( strtotime( $objPaidHoliday->getDate() ) == strtotime( $this->getDate() ) ) ) {
					$boolIsValid = false;
					break;
				}
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
					$boolIsValid &= $this->valCountryCode();
					$boolIsValid &= $this->valTitle();
					$boolIsValid &= $this->valDate( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}
		return $boolIsValid;
	}

	/**
	 * Additional Functions
	 */

	public function prepareCallAgentHoliday( $objCallAgentHoliday = NULL ) {
		if( false == valObj( $objCallAgentHoliday, 'CCallAgentHoliday' ) ) {
			$objCallAgentHoliday = new CCallAgentHoliday();
		}

		$objCallAgentHoliday->setTitle( $this->getTitle() );
		$objCallAgentHoliday->setDescription( $this->getDescription() );
		$objCallAgentHoliday->setDate( $this->getDate() );
		$objCallAgentHoliday->setCountryCode( $this->getCountryCode() );
		$objCallAgentHoliday->setIsPublished( $this->getIsPublished() );
		$objCallAgentHoliday->setPaidHolidayId( $this->getId() );

		return $objCallAgentHoliday;
	}

}
?>