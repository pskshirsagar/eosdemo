<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPsNotificationTypes
 * Do not add any new functions to this class.
 */

class CPsNotificationTypes extends CBasePsNotificationTypes {

	public static function fetchPsNotificationTypes( $strSql, $objDatabase ) {
		return parent::fetchCachedObjects( $strSql, 'CPsNotificationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPsNotificationType( $strSql, $objDatabase ) {
		return parent::fetchCachedObject( $strSql, 'CPsNotificationType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

}
?>