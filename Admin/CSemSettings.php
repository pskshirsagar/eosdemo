<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemSettings
 * Do not add any new functions to this class.
 */

class CSemSettings extends CBaseSemSettings {

	public static function fetchSemSettingsBySemBudgetIds( $arrintSemBudgetIds, $objDatabase ) {
		if( false == valArr( $arrintSemBudgetIds ) ) return NULL;
		$strSql = 'SELECT
						ss.*
					FROM
						sem_settings ss,
						sem_budgets sb
					WHERE
						ss.cid = sb.cid
						AND ( ss.property_id = sb.property_id
								OR ( ss.property_id IS NULL
										AND ss.sem_ad_group_id IS NOT NULL
										AND ss.sem_ad_group_id = sb.sem_ad_group_id ))
						AND sb.id IN ( ' . implode( ',', $arrintSemBudgetIds ) . ' )';

		return self::fetchSemSettings( $strSql, $objDatabase );
	}

	public static function fetchSemSettingByPropertyId( $intPropertyId, $objAdminDatabase ) {
		return self::fetchSemSetting( 'SELECT * FROM sem_settings WHERE property_id = ' . ( int ) $intPropertyId, $objAdminDatabase );
	}

	public static function fetchSemSettingsByPropertyIds( $arrintPropertyIds, $objAdminDatabase ) {
		if( false == valArr( $arrintPropertyIds ) ) return NULL;
		return self::fetchSemSettings( 'SELECT * FROM sem_settings WHERE property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' ) ', $objAdminDatabase );
	}


}
?>