<?php

class CEmployeeTrainingSession extends CBaseEmployeeTrainingSession {

	protected $m_strNameFirst;
	protected $m_strNameLast;
	protected $m_strPreferredName;
	protected $m_strDesignationName;
	protected $m_strTrainingSessionName;
	protected $m_strTrainingName;
	protected $m_strEmailAddress;
	protected $m_strDepartmentName;
	protected $m_strActualStartTime;
	protected $m_strActualEndTime;
	protected $m_strLateBy;

	protected $m_intAttendeeCount;
	protected $m_intPresentCount;
	protected $m_intLateCount;
	protected $m_intEmployeeId;
	protected $m_intInductionSessionId;

	/**
	 * Get Functions
	 *
	 */

	public function getNameFirst() {
		return $this->m_strNameFirst;
	}

	public function getNameLast() {
		return $this->m_strNameLast;
	}

	public function getPreferredName() {
		return $this->m_strPreferredName;
	}

	public function getDesignationName() {
		return $this->m_strDesignationName;
	}

	public function getAttendeeCount() {
		return $this->m_intAttendeeCount;
	}

	public function getPresentCount() {
		return $this->m_intPresentCount;
	}

	public function getLateCount() {
		return $this->m_intLateCount;
	}

	public function getDepartmentName() {
		return $this->m_strDepartmentName;
	}

	public function getEmailAddress() {
		return $this->m_strEmailAddress;
	}

	public function getTrainingSessionName() {
		return $this->m_strTrainingSessionName;
	}

	public function getTrainingName() {
		return $this->m_strTrainingName;
	}

	public function getActualStartTime() {
		return $this->m_strActualStartTime;
	}

	public function getActualEndTime() {
		return $this->m_strActualEndTime;
	}

	public function getEmployeeId() {
		return $this->m_intEmployeeId;
	}

	public function getLateBy() {
		return $this->m_strLateBy;
	}

	public function getInductionSessionId() {
		return $this->m_intInductionSessionId;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( true == isset( $arrmixValues['training_session_name'] ) ) 	$this->setTrainingSessionName( $arrmixValues['training_session_name'] );
		if( true == isset( $arrmixValues['training_name'] ) ) 			$this->setTrainingName( $arrmixValues['training_name'] );
		if( true == isset( $arrmixValues['department_name'] ) )			$this->setDepartmentName( $arrmixValues['department_name'] );
		if( true == isset( $arrmixValues['email_address'] ) ) 			$this->setEmailAddress( $arrmixValues['email_address'] );
		if( true == isset( $arrmixValues['name_first'] ) )				$this->setNameFirst( $arrmixValues['name_first'] );
		if( true == isset( $arrmixValues['name_last'] ) )				$this->setNameLast( $arrmixValues['name_last'] );
		if( true == isset( $arrmixValues['preferred_name'] ) ) 			$this->setPreferredName( $arrmixValues['preferred_name'] );
		if( true == isset( $arrmixValues['designation_name'] ) )		$this->setDesignationName( $arrmixValues['designation_name'] );
		if( true == isset( $arrmixValues['attendee_count'] ) )			$this->setAttendeeCount( $arrmixValues['attendee_count'] );
		if( true == isset( $arrmixValues['present_count'] ) )			$this->setPresentCount( $arrmixValues['present_count'] );
		if( true == isset( $arrmixValues['late_count'] ) )				$this->setPresentCount( $arrmixValues['late_count'] );
		if( true == isset( $arrmixValues['actual_start_time'] ) )		$this->setActualStartTime( $arrmixValues['actual_start_time'] );
		if( true == isset( $arrmixValues['actual_end_time'] ) )			$this->setActualEndTime( $arrmixValues['actual_end_time'] );
		if( true == isset( $arrmixValues['employee_id'] ) )				$this->setEmployeeId( $arrmixValues['employee_id'] );
		if( true == isset( $arrmixValues['late_by'] ) )					$this->setLateBy( $arrmixValues['late_by'] );
		if( true == isset( $arrmixValues['induction_session_id'] ) )	$this->setInductionSessionId( $arrmixValues['induction_session_id'] );

		return;
	}

	public function setNameFirst( $strNameFirst ) {
		$this->m_strNameFirst = $strNameFirst;
	}

	public function setNameLast( $strNameLast ) {
		$this->m_strNameLast = $strNameLast;
	}

	public function setPreferredName( $strNameFull ) {
		$this->m_strPreferredName = $strNameFull;
	}

	public function setDesignationName( $strDesignationName ) {
		$this->m_strDesignationName = $strDesignationName;
	}

	public function setAttendeeCount( $intAttendeeCount ) {
		$this->m_intAttendeeCount = $intAttendeeCount;
	}

	public function setPresentCount( $intPresentCount ) {
		$this->m_intPresentCount = $intPresentCount;
	}

	public function setLateCount( $intLateCount ) {
		$this->m_intLateCount = $intLateCount;
	}

	public function setDepartmentName( $strDepartmentName ) {
		$this->m_strDepartmentName = $strDepartmentName;
	}

	public function setEmailAddress( $strEmailAddress ) {
		$this->m_strEmailAddress = $strEmailAddress;
	}

	public function setTrainingSessionName( $strTrainingSessionName ) {
		$this->m_strTrainingSessionName = $strTrainingSessionName;
	}

	public function setTrainingName( $strTrainingName ) {
		$this->m_strTrainingName = $strTrainingName;
	}

	public function setActualStartTime( $strActualStratTime ) {
		$this->m_strActualStartTime = $strActualStratTime;
	}

	public function setActualEndTime( $strActualEndTime ) {
		$this->m_strActualEndTime = $strActualEndTime;
	}

	public function setEmployeeId( $intEmployeeId ) {
		$this->m_intEmployeeId = $intEmployeeId;
	}

	public function setLateBy( $strLateBy ) {
		$this->m_strLateBy = $strLateBy;
	}

	public function setInductionSessionId( $intInductionSessionId ) {
		$this->m_intInductionSessionId = $intInductionSessionId;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valStartTime() {
		$boolIsValid = true;
		if( true == is_null( $this->getStartTime() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Start time is required.' ) );
		}
		return $boolIsValid;
	}

	public function valEndTime() {
		$boolIsValid = true;
		if( true == is_null( $this->getEndTime() ) ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'End time is required.' ) );
		}
		return $boolIsValid;
	}

	public function valTraineeNote() {
		$boolIsValid = true;
		if( false == $this->m_intIsAvailable && true == is_null( $this->m_strTraineeNote ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'add reason', 'Reason is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valTraineeNote();
				break;

			case VALIDATE_DELETE:
				break;

 			default:
				// no validation avaliable for delete action
				break;
		}

		return $boolIsValid;
	}

}
?>