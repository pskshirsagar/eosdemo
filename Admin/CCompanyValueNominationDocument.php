<?php

class CCompanyValueNominationDocument extends CBaseCompanyValueNominationDocument {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCompanyValueNominationId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPsDocumentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>