<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CSemCallLogs
 * Do not add any new functions to this class.
 */

class CSemCallLogs extends CBaseSemCallLogs {

	public static function fetchAllSemCallLogsByStastisticsEmailFilter( $objStastisticsEmailFilter, $objDatabase ) {

		if( ( false == valArr( $objStastisticsEmailFilter->getPropertyIds() ) )
			|| ( 0 == strlen( $objStastisticsEmailFilter->getStartDate() ) ) || ( 0 == strlen( $objStastisticsEmailFilter->getEndDate() ) ) ) {
			return NULL;
		}

		$arrintPropertyIds = $objStastisticsEmailFilter->getPropertyIds();

		$strSql = 'SELECT scl.property_id,
						count (id) calls_count
					FROM
						sem_call_logs scl
				   WHERE
				   		scl.cid = ' . ( int ) $objStastisticsEmailFilter->getCid() . '
				   	AND
				   		property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ' )
				   	AND
				   		call_datetime::date >= \'' . $objStastisticsEmailFilter->getStartDate() . '\'::date
					 AND
					 	call_datetime::date <= \'' . $objStastisticsEmailFilter->getEndDate() . '\'::date
				   	GROUP BY
						scl.property_id
					ORDER BY
						scl.property_id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>