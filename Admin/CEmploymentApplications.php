<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmploymentApplications
 * Do not add any new functions to this class.
 */

class CEmploymentApplications extends CBaseEmploymentApplications {

	public static function fetchAllEmploymentApplications( $objDatabase ) {

		$strSql = 'Select * from employment_applications ORDER BY updated_on DESC';

		return self::fetchEmploymentApplications( $strSql, $objDatabase );
	}

	public static function fetchEmploymentApplicationsByDepartmentIdByDepartmentIdNull( $intDepartmentId, $objDatabase ) {
		return self::fetchEmploymentApplications( sprintf( 'SELECT * FROM employment_applications WHERE department_id = %d OR department_id IS NULL ORDER BY name', ( int ) $intDepartmentId ), $objDatabase );
	}

}
?>