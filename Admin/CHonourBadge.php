<?php

class CHonourBadge extends CBaseHonourBadge {

	const FIRST_START_SEMIANNUAL_MONTH		= 1;
	const FIRST_END_SEMIANNUAL_MONTH		= 7;
	const SECOND_START_SEMIANNUAL_MONTH		= 8;
	const SECOND_END_SEMIANNUAL_MONTH		= 12;

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHonourBadgeTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHonourBadgeReasonTypeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valTargetEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valSourceEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valRemark() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valHonourBadgeDatetime() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase,$boolReturnSqlOnly = false ) {
		$this->setDeletedOn( 'NOW()' );
		$this->setDeletedBy( $intCurrentUserId );

		$this->setUpdatedBy( $intCurrentUserId );
		$this->setUpdatedOn( 'NOW()' );
		$this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );
		return true;
	}

}
?>