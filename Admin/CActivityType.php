<?php

class CActivityType extends CBaseActivityType {

	const INSERT_CROSS_PRODUCT_REQUEST = 8;
	const PROBLEM                      = 2;
	const STATUS_CHANGE                = 1;
	const RESEARCH                     = 3;
	const QUESTIONS                    = 4;
	const DECISION                     = 5;
	const ASSESTS                      = 6;
	const CONVERSATIONS                = 7;
	const ASSOCIATE_FEEDBACK           = 9;
	const PROTOTYPE                    = 10;
	const STORY                        = 11;
	const ACCEPTANCE_CRITERIA          = 12;
	const DEV_TASK                     = 13;
	const TEST_CASE                    = 14;
	const RELEASE_NOTE                 = 15;
	const PROJECT                      = 16;
	const PRODUCT                      = 17;
	const ATTACHMENT                   = 18;

	public static $c_arrintActivityTypeIds = [
		self::PROBLEM,
		self::QUESTIONS,
		self::INSERT_CROSS_PRODUCT_REQUEST
	];

	public static $c_arrintConversationAttachmentActivityTypeIds = [
		self::CONVERSATIONS,
		self::ATTACHMENT
	];

	public function valId() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valName() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function valIsPublished() {
		$boolIsValid = true;

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}

?>