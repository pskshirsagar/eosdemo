<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CEmployeeEncryptionAssociations
 * Do not add any new functions to this class.
 */

class CEmployeeEncryptionAssociations extends CBaseEmployeeEncryptionAssociations {

	public static function fetchEmployeeEncryptionAssociations( $strSql, $objDatabase ) {
		return parent::fetchObjects( $strSql, 'CEmployeeEncryptionAssociation', $objDatabase, $boolIsReturnKeyedArray = false );
	}

	public static function fetchEmployeeEncryptionAssociationsByEncryptionSystemTypeIdByEmployeeIds( $intEncryptionSystemTypeId, $arrintEmployeeIds, $objDatabase ) {
		if( true == empty( $intEncryptionSystemTypeId ) || true == empty( $arrintEmployeeIds ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						employee_encryption_associations eea
					WHERE
						eea.encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId . '
						AND eea.reference_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' )';

		return self::fetchEmployeeEncryptionAssociations( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEncryptionAssociationsByEncryptionSystemTypeIdByViewerEmployeeId( $intEncryptionSystemTypeId, $intViewerEmployeeId, $objDatabase ) {

		return self::fetchEmployeeEncryptionAssociations( sprintf( 'SELECT * FROM employee_encryption_associations WHERE encryption_system_type_id = %d AND viewer_employee_id = %d', ( int ) $intEncryptionSystemTypeId, ( int ) $intViewerEmployeeId ), $objDatabase );
	}

	public static function fetchEmployeeEncryptionAssociationsByEncryptionSystemTypeIdById( $intEncryptionSystemTypeId, $intCompensationAssociationId, $objDatabase ) {
		if( true == empty( $intEncryptionSystemTypeId ) || true == empty( $intCompensationAssociationId ) ) return NULL;

		$strSql = 'SELECT * FROM employee_encryption_associations WHERE encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId . ' AND id =' . ( int ) $intCompensationAssociationId;

		return self::fetchEmployeeEncryptionAssociations( $strSql, $objDatabase );
	}

	public static function fetchExistingCompensationAssociationsByEmployeeIdsAndType( $intEncryptionSystemTypeId, $arrintEmployeeIds, $objDatabase, $boolTypeCompensation ) {
		if( true == empty( $arrintEmployeeIds ) ) return NULL;

		if( true == $boolTypeCompensation ) {
			$strSql = ' SELECT
							DISTINCT ( eea.* )
						FROM
							employee_encryption_associations eea
						WHERE
							eea.encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId . '
							AND eea.id IN ( SELECT
											ecl.new_employee_encryption_association_id
										FROM
											employee_compensation_logs ecl
										WHERE
											ecl.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) AND bonus_employee_encryption_association_id IS NULL
										)	';
		} else {
			$strSql = ' SELECT
						eea.*
					FROM
						employee_compensation_logs ecl
						LEFT JOIN employee_encryption_associations eea ON ( eea.id = ecl.bonus_employee_encryption_association_id )
					WHERE
						eea.encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId . '
						AND ecl.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) AND bonus_employee_encryption_association_id IS NOT NULL';
		}
		return self::fetchEmployeeEncryptionAssociations( $strSql, $objDatabase );
	}

	public static function fetchEmployeeEncryptionAssociationsByEncryptionSystemTypeIdByViewerEmployeeIdByEmployeeIdByIds( $intEncryptionSystemTypeId, $intViewerEmployeeId, $intEmployeeId, $arrintEmployeeEncryptionAssociationIds, $objDatabase ) {

		$strSql = ' SELECT
						*
					FROM
						employee_encryption_associations
					WHERE
						encryption_system_type_id = ' . ( int ) $intEncryptionSystemTypeId . '
						AND id IN ( ' . implode( ',', $arrintEmployeeEncryptionAssociationIds ) . ' )
						AND reference_id = ' . ( int ) $intEmployeeId . '
						AND viewer_employee_id = ' . ( int ) $intViewerEmployeeId;

		return self::fetchEmployeeEncryptionAssociations( $strSql, $objDatabase );
	}

}
?>