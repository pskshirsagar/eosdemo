<?php

class CSemCampaignSource extends CBaseSemCampaignSource {

	protected $m_strName;
	protected $m_strSemAccountRemotePrimaryKey;

	/**
	 * Get Functions
	 */

	public function getName() {
		return $this->m_strName;
	}

	public function getSemAccountRemotePrimaryKey() {
		return $this->m_strSemAccountRemotePrimaryKey;
	}

	/**
	 * Set Functions
	 */

	public function setName( $strName ) {
		$this->m_strName = $strName;
	}

	public function setSemAccountRemotePrimaryKey( $strSemAccountRemotePrimaryKey ) {
		$this->m_strSemAccountRemotePrimaryKey = $strSemAccountRemotePrimaryKey;
	}

 	public function setValues( $arrValues, $boolStripSlashes = true, $boolDirectSet = false ) {
 		parent::setValues( $arrValues, $boolStripSlashes, $boolDirectSet );
		if( true == isset( $arrValues['sem_account_remote_primary_key'] ) ) $this->setSemAccountRemotePrimaryKey( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['sem_account_remote_primary_key'] ) : $arrValues['sem_account_remote_primary_key'] );
		if( true == isset( $arrValues['name'] ) ) $this->setName( ( true == $boolStripSlashes ) ? stripslashes( $arrValues['name'] ) : $arrValues['name'] );
		return;
 	}

	/**
	 * Validate Functions
	 */

	public function valSemSourceId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemSourceId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_source_id', 'Sem Source ID is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSemAccountId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemAccountId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_account_id', 'Sem account ID is required.' ) );
		}

		return $boolIsValid;
	}

	public function valSemCampaignId() {
		$boolIsValid = true;

		if( true == is_null( $this->getSemCampaignId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'sem_campaign_id', 'Sem campaign ID is required.' ) );
		}

		return $boolIsValid;
	}

	public function valRemotePrimaryKey() {
		$boolIsValid = true;

		if( true == is_null( $this->getRemotePrimaryKey() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'remote_primary_key', 'Remote primary key is required.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Fetch Functions
	 */

	public function fetchSemAdGroupSourceByRemotePrimaryKey( $strRemotePrimaryKey, $objAdminDatabase ) {
		return CSemAdGroupSources::fetchSemAdGroupSourceBySemSourceIdBySemCampaignIdByRemotePrimaryKey( $this->getSemSourceId(), $this->getSemCampaignId(), $strRemotePrimaryKey, $objAdminDatabase );
	}

	public function fetchSemAdGroupSourceByName( $strName, $objAdminDatabase ) {
		return CSemAdGroupSources::fetchSemAdGroupSourceBySemSourceIdBySemCampaignIdByName( $this->getSemSourceId(), $this->getSemCampaignId(), $strName, $objAdminDatabase );
	}
}
?>