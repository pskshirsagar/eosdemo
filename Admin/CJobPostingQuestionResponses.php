<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CJobPostingQuestionResponses
 * Do not add any new functions to this class.
 */

class CJobPostingQuestionResponses extends CBaseJobPostingQuestionResponses {

	public static function fetchJobPostingQuestionResponseByEmployeeApplicationIdByQuestionId( $intEmployeeApplicationId, $intQuestionId, $objDatabase ) {

		if( false == is_numeric( $intEmployeeApplicationId ) || false == is_numeric( $intQuestionId ) ) return NULL;

		$strSql = ' SELECT
						*
					FROM
						job_posting_question_responses
					WHERE
						employee_application_id = ' . ( int ) $intEmployeeApplicationId . '
						AND question_id = ' . ( int ) $intQuestionId;

		return self::fetchJobPostingQuestionResponse( $strSql, $objDatabase );
	}

	public static function fetchJobPostingQuestionAndAnswerByJobPostingIdsByCountryCode( $arrintJobPostingIds, $strCountryCode, $objDatabase ) {

		if( false == valArr( $arrintJobPostingIds ) || false == valStr( $strCountryCode ) ) return NULL;
		$strWhereCondition = '';

		$arrintJobPostingIds = array_filter( $arrintJobPostingIds );

		if( CCountry::CODE_USA == $strCountryCode ) {
			$strWhereCondition = ' OR jpq.job_posting_id = ' . CPsWebsiteJobPosting::DEFAULT_QUESTIONS_USA;
		}

		$strSql = ' SELECT
						jpq.id,
						jpq.question,
						jpqr.employee_application_id,
						jpqr.answer
					FROM
						job_posting_questions jpq
						JOIN ps_website_job_postings pwjp ON ( jpq.job_posting_id = pwjp.id )
						JOIN job_posting_question_responses jpqr ON ( jpq.id = jpqr.question_id )
					WHERE
						jpq.job_posting_id IN ( ' . implode( ',', $arrintJobPostingIds ) . ' )
						AND pwjp.country_code = \'' . addslashes( $strCountryCode ) . '\'
						AND jpq.deleted_on IS NULL ' . $strWhereCondition . '
					ORDER BY jpq.id';

		return fetchData( $strSql, $objDatabase );
	}

}
?>