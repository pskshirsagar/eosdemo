<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CTestSubmissions
 * Do not add any new functions to this class.
 */

class CTestSubmissions extends CBaseTestSubmissions {

	public static function fetchTestSubmissionsByTestSubmissionsFilter( $objTestSubmissionsFilter, $objDatabase ) {

		if( false == valObj( $objTestSubmissionsFilter, 'CTestSubmissionsFilter' ) ) {
			$objTestSubmissionsFilter = new CTestSubmissionsFilter();
		}

		$strTableNames	= 'test_submissions ts';
		$strFields		= ( false == is_null( $objTestSubmissionsFilter->getRequiredFields() ) ) ? $objTestSubmissionsFilter->getRequiredFields() : 'ts.*';

		if( true == $objTestSubmissionsFilter->getIsRequiredFieldScore() ) {
			$strFields 	.= ' , 
								CASE
								  WHEN split_part ( score, \'/\', 2 ) = \'\' OR CAST ( ( split_part ( score, \'/\', 2 ) ) as FLOAT ) = 0 THEN 0
								  ELSE CASE
							        WHEN ts.score LIKE \'%/%\' THEN ( ( CAST ( split_part ( score, \'/\', 1 ) as FLOAT ) ) * 100 ) / ( CAST ( split_part ( score, \'/\', 2 ) as FLOAT ) )
								   END
								END as score
								';
		}

		$strCustomFields	= '';
		$arrstrCustomFields	= array();
		$strConditions		= ( true == $objTestSubmissionsFilter->getIsFalseCondition() ) ? ' false ' : ' true ';
		$strSortBy			= ' ORDER BY ts.updated_on DESC';
		if ( true == valStr( $objTestSubmissionsFilter->getSortBy() ) ) $strSortBy = 'ORDER BY' . $objTestSubmissionsFilter->getSortBy();

		$intOffset			= ( 0 < $objTestSubmissionsFilter->getPageNo() ) ? ( int ) $objTestSubmissionsFilter->getPageSize() * ( $objTestSubmissionsFilter->getPageNo() - 1 ) : 0;
		$intLimit			= ( int ) $objTestSubmissionsFilter->getPageSize();

		$arrstrAndSearchParameters = self::fetchSearchCriteria( $objTestSubmissionsFilter );
		$arrstrAndQuickSearchParameters = self::fetchQuickSearchCriteria( $objTestSubmissionsFilter->getSearchCriteria() );
		$arrstrAndPermissionsParameters = self::fetchUserPermissionsCriteria( $objTestSubmissionsFilter );
		$arrstrAndSearchParameters = array_merge( $arrstrAndSearchParameters, $arrstrAndQuickSearchParameters, $arrstrAndPermissionsParameters );

		if( true == valStr( $objTestSubmissionsFilter->getSortBy() ) ) $strSortBy = self::fetchSortByCriteria( $objTestSubmissionsFilter );

		if( true == valArr( $objTestSubmissionsFilter->getCustomFields() ) ) {
			foreach( $objTestSubmissionsFilter->getCustomFields() as $strCustomFieldName ) {
				switch( $strCustomFieldName ) {

					case 'employee_name':
						$arrstrCustomFields['employee_name'] = ' e.name_first, e.name_last,
							CASE
								when ea.id IS NULL
									THEN CONCAT(e.name_first, \' \', e.name_last)
								WHEN ea.id IS NOT NULL
									THEN CONCAT(ea.name_first, \' \', ea.name_last)
								END AS name_full, ee.id as grader_employee_id, ee.name_first as grader_name_first, ee.name_last as grader_name_last';
						$strTableNames .= '	LEFT JOIN employees e on e.id = ts.employee_id LEFT JOIN employees ee on ee.id = ts.graded_by LEFT JOIN employee_applications ea ON ts.employee_application_id = ea.id ';
						break;

					case 'test_name':
						$arrstrCustomFields['test_name'] = ' t.name as test_name, t.is_hide_score';
						$strTableNames .= '	JOIN tests t ON t.id = ts.test_id';
						break;

					default:
						// default case
						break;
				}
			}

			if( true == valArr( $arrstrCustomFields ) ) {
				$strCustomFields = implode( ',', $arrstrCustomFields );
			}
		}

		$strConditions	.= ( true == valArr( $arrstrAndSearchParameters ) ) ? ' AND ' . implode( ' AND ', $arrstrAndSearchParameters ) : '';

		$strOffSetCondition	= ( 0 < $intLimit ) ? ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . ( int ) $intLimit : '';

		switch( $objTestSubmissionsFilter->getReturnCriteria() ) {
			case CEosFilter::RETURN_CRITERIA_COUNT:

				$strSql = 'SELECT COUNT( ts.id ) FROM ' . $strTableNames . ' WHERE ' . $strConditions;
				$arrintCount = fetchData( $strSql, $objDatabase );
				return ( true == isset ( $arrintCount[0]['count'] ) ) ? ( int ) $arrintCount[0]['count'] : 0;
				break;

			case CEosFilter::RETURN_CRITERIA_CUSTOM_ARRAY:
				if( true == valStr( $strCustomFields ) ) {
					$strSql = 'SELECT ' . $strCustomFields . ' FROM ' . $strTableNames . ' WHERE ' . $strConditions . $strSortBy . $strOffSetCondition;
				}

			case CEosFilter::RETURN_CRITERIA_ARRAY:

				if( true == valStr( $strFields ) ) {
					$strFields	= ( true == valStr( $strCustomFields ) ) ? $strCustomFields . ', ' . $strFields : $strFields;
					$strSql 	= 'SELECT ' . $strFields . ' FROM ' . $strTableNames . ' WHERE ' . $strConditions . $strSortBy . $strOffSetCondition;
				}

				$arrstrMixedData 		= fetchData( $strSql, $objDatabase );
				return $arrstrMixedData;
				break;

			default:
				$strFields	= ( true == valStr( $strCustomFields ) ) ? $strCustomFields . ', ' . $strFields : $strFields;
				$strSql		= 'SELECT ' . $strFields . ' FROM ' . $strTableNames . ' WHERE ' . $strConditions . $strSortBy;
				if( true == $objTestSubmissionsFilter->getReturnSingleRecord() ) {
					$strSql = $strSql . ' LIMIT 1;';
					return parent::fetchTestSubmission( $strSql, $objDatabase );
				}

				$strSql .= $strOffSetCondition;
				return parent::fetchTestSubmissions( $strSql, $objDatabase );
				break;
		}
	}

	public static function fetchTestSubmissionsWithTestUsersByTestIds( $arrintTestIds, $objDatabase ) {

		if( false == valArr( $arrintTestIds ) ) return false;

		$strSql = ' SELECT
						t.id as test_test_id,
						t.name as test_name,
						et.test_id as test_user_test_id,
						et.employee_id as test_employee_id,
						ts.id, ts.test_id,
						ts.score,
						e.id as employee_id,
						e.email_address,
						ts.employee_id,
						ts.graded_by
					FROM
						tests t
						LEFT JOIN employee_tests et ON ( t.id = et.test_id )
						LEFT JOIN employees e ON ( e.id = et.employee_id )
						LEFT JOIN test_submissions ts ON ( t.id = ts.test_id AND e.id = ts.employee_id )
					WHERE
						t.id IN ( ' . implode( ',', $arrintTestIds ) . ' )
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					UNION
					SELECT
						t.id as test_test_id,
						t.name as test_name,
						t.id AS test_user_test_id,
	 					e.id AS test_employee_id,
						ts.id, ts.test_id,
						ts.score,
						e.id as employee_id,
						e.email_address,
						ts.employee_id,
						ts.graded_by
					FROM
						test_groups tg
						LEFT JOIN tests t ON ( tg.test_id = t.id )
						LEFT JOIN user_groups ug ON ( tg.group_id = ug.group_id AND ug.deleted_by IS NULL )
						LEFT JOIN users u ON ( u.id = ug.user_id )
						LEFT JOIN team_employees te1 ON ( te1.team_id = tg.team_id AND te1.is_primary_team = 1 )
						LEFT JOIN employees e ON ( e.id = u.employee_id OR e.department_id = tg.department_id OR e.office_id = tg.office_id OR e.designation_id = tg.designation_id OR e.id = te1.employee_id )
						LEFT JOIN test_submissions ts ON ( tg.test_id = ts.test_id AND e.id = ts.employee_id )
					WHERE
						t.id IN ( ' . implode( ',', $arrintTestIds ) . ' )
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT;

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestSubmissionswithTestNameByTestTypeIdByEmployeeIdByYearByFilter( $intTestTypeId, $intEmployeeId, $intYear, $objTestScoreReportFilter, $objDatabase ) {

		$intOffset = ( 0 < $objTestScoreReportFilter->getPageNo() ) ? $objTestScoreReportFilter->getPageSize() * ( $objTestScoreReportFilter->getPageNo() - 1 ) : 0;
		$intLimit  = ( int ) $objTestScoreReportFilter->getPageSize();
		$strOrderClause = ' ORDER BY id DESC ';
		$strOrder = '';

		if( false == is_null( $objTestScoreReportFilter ) && false == is_null( $objTestScoreReportFilter->getOrderByField() ) && false == is_null( $objTestScoreReportFilter->getOrderByType() ) ) {
			$strOrder = ( 1 == $objTestScoreReportFilter->getOrderByType() ) ? ' DESC ' : ' ASC ';
			$strOrderClause = ' ORDER BY ' . $objTestScoreReportFilter->getOrderByField() . '' . $strOrder;
		}

		$strWhere = ' WHERE ts.employee_id = ' . ( int ) $intEmployeeId . ' AND t.test_type_id = ' . ( int ) $intTestTypeId;

		if( false == is_null( $intYear ) )
			$strWhere .= 'AND date_part(\'year\', ( ts.created_on) ) = ' . ( int ) $intYear;

		$strSql = 'SELECT
						ts.*,
						t.name as test_name,
						t.is_hide_score
					FROM
						test_submissions AS ts
						JOIN tests AS t ON ts.test_id = t.id';

		$strSql .= $strWhere . '' . $strOrderClause . ' OFFSET ' . ( int ) $intOffset . ' LIMIT ' . $intLimit . '';
		return self::fetchTestSubmissions( $strSql, $objDatabase );
	}

	public static function fetchLatestTestSubmissionByEmployeeId( $intEmployeeId, $objDatabase ) {

		$strSql = 'SELECT
						*
					FROM
						test_submissions ts
						LEFT JOIN tests t ON ts.test_id = t.id
					WHERE
						graded_on = (
									SELECT
										max( ts.graded_on )
									FROM
										test_submissions ts
									WHERE
										ts.employee_id = ' . ( int ) $intEmployeeId . '
									)
						AND ts.employee_id = ' . ( int ) $intEmployeeId . ' AND t.test_type_id = 1';

		return self::fetchTestSubmission( $strSql, $objDatabase );
	}

	public static function fetchSearchCriteria( $objTestSubmissionsFilter ) {

		$arrstrAndSearchParameters = array();
		// For published job employee id is null.Causing issue on live.so following condition added for checking Where Employee id is not null.
		if( true == $objTestSubmissionsFilter->getIsFromListingPage() ) $arrstrAndSearchParameters[] = ' ( ts.employee_id IS NOT NULL OR ts.employee_application_id IS NOT NULL ) AND t.test_type_id = 1';

		if( true == $objTestSubmissionsFilter->getTestType() ) $arrstrAndSearchParameters[] = ' t.test_type_id = 1 ';

		if( true == $objTestSubmissionsFilter->getIsGradePending() && true == $objTestSubmissionsFilter->getIsFromListingPage() ) {
			$arrstrAndSearchParameters[] = ' ( ts.graded_on IS NULL AND ts.graded_by = ' . $objTestSubmissionsFilter->getGraderEmployeeId() . ' OR ts.graded_by IS NULL ) ';
		} elseif( true == $objTestSubmissionsFilter->getIsGradePending() ) {
			$arrstrAndSearchParameters[] = ' ( ts.graded_on IS NULL OR ts.graded_by IS NULL ) ';
		}

		if( true == $objTestSubmissionsFilter->getIsGradedTests() ) $arrstrAndSearchParameters[] = ' ts.graded_by IS NOT NULL AND ts.graded_on IS NOT NULL ';
		if( false == is_null( $objTestSubmissionsFilter->getTestId() ) ) $arrstrAndSearchParameters[] = ' ts.test_id = ' . ( int ) $objTestSubmissionsFilter->getTestId();
		if( true == $objTestSubmissionsFilter->getIsIgnoredTestsCount() ) $arrstrAndSearchParameters[] = ' ts.graded_by = 1 AND ts.score IS NULL AND t.test_type_id = 1 ';
		if( true == valArr( $objTestSubmissionsFilter->getTestIds() ) ) $arrstrAndSearchParameters[] = ' ts.test_id IN ( ' . implode( ',', $objTestSubmissionsFilter->getTestIds() ) . ' ) ';
		if( false == is_null( $objTestSubmissionsFilter->getEmployeeId() ) ) $arrstrAndSearchParameters[] = ' ts.employee_id = ' . ( int ) $objTestSubmissionsFilter->getEmployeeId();
		if( true == valArr( $objTestSubmissionsFilter->getEmployeeIds() ) ) $arrstrAndSearchParameters[] = ' ts.employee_id IN ( ' . implode( ',', $objTestSubmissionsFilter->getEmployeeIds() ) . ' ) ';
		if( true == valArr( $objTestSubmissionsFilter->getGraderEmployeeIds() ) ) $arrstrAndSearchParameters[] = ' ts.graded_by IN ( ' . implode( ',', $objTestSubmissionsFilter->getGraderEmployeeIds() ) . ' ) ';
		if( false == is_null( $objTestSubmissionsFilter->getYear() ) ) $arrstrAndSearchParameters[] = ' date_part(\'year\', ( ts.created_on) ) = ' . ( int ) $objTestSubmissionsFilter->getYear();
		if( true == valStr( $objTestSubmissionsFilter->getGradedOnEndDate() ) ) $arrstrAndSearchParameters[] = ' ts.graded_on::date <= \'' . $objTestSubmissionsFilter->getGradedOnEndDate() . '\'::date ';
		if( true == valStr( $objTestSubmissionsFilter->getGradedOnStartDate() ) ) $arrstrAndSearchParameters[] = 'ts.score IS NOT NULL AND ts.graded_on::date >=\'' . $objTestSubmissionsFilter->getGradedOnStartDate() . '\'::date ';
		if( true == valStr( $objTestSubmissionsFilter->getTestName() ) ) $arrstrAndSearchParameters[] = ' t.name ILIKE \'%' . addslashes( $objTestSubmissionsFilter->getTestName() ) . '%\'';

		if( true == is_numeric( $objTestSubmissionsFilter->getFromPercentage() ) ) $arrstrAndSearchParameters[] = 'CASE
									WHEN
										ts.score LIKE \'%/%\'
									THEN
										( ( CAST ( split_part( score, \'/\', 1 ) as FLOAT ) ) * 100 ) / ( CAST ( split_part ( score, \'/\', 2 ) as FLOAT ) )
								END >= ' . $objTestSubmissionsFilter->getFromPercentage();

		if( true == is_numeric( $objTestSubmissionsFilter->getToPercentage() ) ) $arrstrAndSearchParameters[] = 'CASE
									WHEN
										ts.score LIKE \'%/%\'
									THEN
										( ( CAST ( split_part( score, \'/\', 1 ) as FLOAT ) ) * 100 ) / ( CAST ( split_part ( score, \'/\', 2 ) as FLOAT ) )
								END <= ' . $objTestSubmissionsFilter->getToPercentage();

		if( valArr( $objTestSubmissionsFilter->getTestSubmissionIds() ) ) $arrstrAndSearchParameters[] = ' ts.id IN ( ' . implode( ',', $objTestSubmissionsFilter->getTestSubmissionIds() ) . ' ) ';
		if( false == is_null( $objTestSubmissionsFilter->getEmployeeApplicationId() ) ) $arrstrAndSearchParameters[] = ' ts.employee_application_id = ' . ( int ) $objTestSubmissionsFilter->getEmployeeApplicationId();

		return $arrstrAndSearchParameters;
	}

	public static function fetchQuickSearchCriteria( $arrmixFilteredExplodedSearch ) {

		$arrstrAndQuickSearchParameters = array();

		if( true == valArr( $arrmixFilteredExplodedSearch ) ) {
			if( 'e:' == \Psi\CStringService::singleton()->substr( $arrmixFilteredExplodedSearch[0], 0, 2 ) ) {

				$arrstrSearchCriteria = explode( ' ', ltrim( \Psi\CStringService::singleton()->substr( $arrmixFilteredExplodedSearch[0], 2 ), ' ' ) );
				array_push( $arrstrSearchCriteria, ltrim( \Psi\CStringService::singleton()->substr( $arrmixFilteredExplodedSearch[0], 2 ), ' ' ) );

				$arrstrAndQuickSearchParameters[] = ' ( e.name_full ILIKE E\'%' . implode( '%\' AND e.name_full ILIKE E\'%', $arrstrSearchCriteria ) . '%\' OR e.name_first ILIKE E\'%' . implode( '%\' AND e.name_first ILIKE E\'%', $arrstrSearchCriteria ) . '%\' OR e.name_last ILIKE E\'%' . implode( '%\' AND e.name_last ILIKE E\'%', $arrstrSearchCriteria ) . '%\')';

			} elseif( 't:' == \Psi\CStringService::singleton()->substr( $arrmixFilteredExplodedSearch[0], 0, 2 ) ) {
				$arrstrSearchCriteria = explode( ' ', ltrim( \Psi\CStringService::singleton()->substr( $arrmixFilteredExplodedSearch[0], 2 ), ' ' ) );
				array_push( $arrstrSearchCriteria, ltrim( \Psi\CStringService::singleton()->substr( $arrmixFilteredExplodedSearch[0], 2 ), ' ' ) );

				$arrstrAndQuickSearchParameters[] = ' t.name ILIKE E\'%' . implode( '%\' AND t.name ILIKE E\'%', $arrstrSearchCriteria ) . '%\'';
			} else {
				$arrstrAndQuickSearchParameters[] = ' ( e.name_full ILIKE E\'%' . implode( '%\' OR e.name_full ILIKE E\'%', $arrmixFilteredExplodedSearch ) . '%\' OR e.name_first ILIKE E\'%' . implode( '%\' OR e.name_first ILIKE E\'%', $arrmixFilteredExplodedSearch ) . '%\' OR e.name_last ILIKE E\'%' . implode( '%\' OR e.name_last ILIKE E\'%', $arrmixFilteredExplodedSearch ) . '%\' OR t.name ILIKE E\'%' . implode( '%\' OR t.name ILIKE E\'%', $arrmixFilteredExplodedSearch ) . '%\' ) ';
			}
		}
		return $arrstrAndQuickSearchParameters;
	}

	public static function fetchUserPermissionsCriteria( $objTestSubmissionsFilter ) {

		$arrstrAndPermissionsParameters = array();

		if( true == $objTestSubmissionsFilter->getIsAdmin() ) {
			return  $arrstrAndPermissionsParameters;
		}

		if( valArr( $objTestSubmissionsFilter->getGraderTestIds() ) && false == is_null( $objTestSubmissionsFilter->getCreatorUserId() ) ) {
			$arrstrAndPermissionsParameters[] = ' ( ts.test_id IN ( ' . implode( ',', $objTestSubmissionsFilter->getGraderTestIds() ) . ' ) OR t.created_by = ' . $objTestSubmissionsFilter->getCreatorUserId() . ')';
		} else {
			if( valArr( $objTestSubmissionsFilter->getGraderTestIds() ) ) {
				$arrstrAndPermissionsParameters[] = ' ts.test_id IN ( ' . implode( ',', $objTestSubmissionsFilter->getGraderTestIds() ) . ' )';
			}

			if( false == is_null( $objTestSubmissionsFilter->getCreatorUserId() ) ) {
				$arrstrAndPermissionsParameters[] = ' t.created_by = ' . $objTestSubmissionsFilter->getCreatorUserId();
			}
		}

		return $arrstrAndPermissionsParameters;
	}

	public static function fetchSortByCriteria( $objTestSubmissionsFilter ) {

		$strSortBy = ' ORDER BY ';

		switch( $objTestSubmissionsFilter->getSortBy() ) {
			case 'test_name':
				$strSortBy .= ' t.name ';
				break;

			case 'given_by':
				$strSortBy .= ' e.name_first ';
				break;

			case 'graded_by':
				$strSortBy .= ' ee.name_first ';
				break;

			case 'submitted_on':
				$strSortBy .= ' ts.updated_on ';
				break;

			default:
				$strSortBy .= ' ts.updated_on ';
				break;
		}

		$strSortBy .= ' ' . $objTestSubmissionsFilter->getSortType();

		return $strSortBy;
	}

	public static function fetchOverallPercentageByUserIds( $arrintUserIds, $objDatabase, $strFromDate = NULL, $strToDate= NULL ) {

		if( false == valArr( array_filter( $arrintUserIds ) ) ) return [];

		$strWhere = '';
		$strWhere .= ( true == valStr( $strFromDate ) && true == valStr( $strToDate ) ) ? ' AND ts.created_on > \'' . $strFromDate . '\' AND ts.created_on < \'' . $strToDate . '\'' : '';

		$strSql = 'SELECT
						result.user_id,
						avg ( result.score )
					FROM
						(
							SELECT
								ts.employee_id,
								u.id as user_id,
								CASE
									WHEN
										ts.score LIKE \'%/%\'
									THEN
										( ( CAST ( split_part( score, \'/\', 1 ) as FLOAT ) ) * 100 ) / ( CAST ( split_part ( score, \'/\', 2 ) as FLOAT ) )
								END as score
							FROM
								test_submissions ts
								LEFT JOIN users u ON (u.employee_id = ts.employee_id )
							WHERE
								u.id IN ( ' . implode( ',', $arrintUserIds ) . ' )
								AND ts.score IS NOT NULL ' . $strWhere . '
							GROUP BY
								ts.employee_id,
								u.id,
								ts.score
						) AS result
					GROUP BY result.user_id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestAverageBySubmissionTestIds( $arrintTestIds, $objDatabase ) {

		$strSql = '
					SELECT
						result.name, result.id,
						round( avg( avg_score ) )
					FROM (
							SELECT t.id, t.name,
									CASE
									WHEN ts.score LIKE \'% / %\' THEN ( ( CAST (split_part( score, \'/\', 1 ) as
										FLOAT ) ) * 100) /( CAST ( split_part( score, \'/\', 2 ) as FLOAT ) )
									END as avg_score
							FROM
								test_submissions ts
	 							JOIN tests t on t.id = ts.test_id
							WHERE
								ts.score IS NOT NULL
								AND ts.test_id IN ( ' . implode( ',', $arrintTestIds ) . ' ) )
								as result
					GROUP BY result.name,result.id';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchNotGradedTestSubmissionsByDateRange( $strBeginDate, $strEndDate, $objDatabase ) {

		$strSql = 'SELECT
						t.name,
						e.name_full,
						e.name_first,
						e.email_address,
						e.office_id,
						min( ts.created_on ) AS created_on,
						tg.created_on as grader_created_on,
						tg.employee_id,
						t.id,
						CASE
							WHEN e1.office_id IN ( ' . COffice::TOWER_8 . ', ' . COffice::TOWER_9 . ' ) THEN e1.email_address
							ELSE NULL
						END  AS primary_reporting_manager,
						CASE
							WHEN e2.office_id IN ( ' . COffice::TOWER_8 . ', ' . COffice::TOWER_9 . ' ) THEN e2.email_address
							ELSE NULL
						END AS secondary_reporting_manager
					FROM
						test_submissions ts
						LEFT JOIN tests t ON ( t.id = ts.test_id )
						LEFT JOIN test_graders tg ON ( tg.test_id = t.id )
						LEFT JOIN employees e ON ( tg.employee_id = e.id )
						LEFT JOIN employees e1 ON ( e1.id = e.reporting_manager_id )
						LEFT JOIN team_employees te1 ON ( e1.id = te1.employee_id AND te1.is_primary_team = 1 )
						LEFT JOIN employees e2 ON ( e2.id = te1.manager_employee_id )
					WHERE
						ts.score IS NULL
						AND tg.id IS NOT NULL
						AND ts.employee_id IS NOT NULL
						AND ( ts.created_on BETWEEN \'' . $strBeginDate . '\'
						AND \'' . $strEndDate . '\' )
						AND ts.graded_by IS NULL
						AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
					GROUP BY
						t.name,
						e.name_full,
						e.name_first,
						e.email_address,
						e.office_id,
						tg.created_on,
						primary_reporting_manager,
						tg.employee_id,
						t.id,
						secondary_reporting_manager';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestSubmissionsByTestIdByEmployeeIds( $intTestId, $arrintEmployeeIds, $objDatabase ) {

		if( true == is_null( $intTestId ) ) return false;

		$strSql = 'SELECT e.name_full,
						tsa.test_question_id,
						ts.test_id,
						ts.employee_id,
						tsa.is_correct,
						tsa.points_granted
					FROM
						test_submission_answers tsa
						LEFT JOIN test_questions tq ON tq.id = tsa.test_question_id
						LEFT JOIN test_submissions as ts ON tsa.test_submission_id = ts.id
						LEFT JOIN employees e ON e.id = ts.employee_id
					WHERE
						ts.test_id = ' . ( int ) $intTestId . '
						AND tq.deleted_on IS NULL
						AND ts.employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ';

		return fetchData( $strSql, $objDatabase );
	}

	public static function fetchTestSubmissionCountByEmployeeApplicationId( $intEmployeeApplicationId, $objDatabase ) {
		return parent::fetchTestSubmissionCount( sprintf( 'WHERE employee_application_id = %d', ( int ) $intEmployeeApplicationId ), $objDatabase );
	}

	public static function fetchTestSubmissionsDataByTestIdsByEmployeeIds( $arrintTestIds, $arrintTestSubmissionIds, $objDatabase ) {
		if( false == valArr( $arrintTestIds ) || false == valArr( $arrintTestSubmissionIds ) ) return false;

		$strSql = ' SELECT
						*
					FROM
						test_submissions
					WHERE
						test_id IN ( ' . implode( ',', $arrintTestIds ) . ' ) 
						AND employee_id IN ( ' . implode( ',', $arrintTestSubmissionIds ) . ' ) ';

		return parent::fetchTestSubmissions( $strSql, $objDatabase );
	}

	public static function fetchCurrentTestSubmissionsByEmployeeIds( $arrintEmployeeIds, $objDatabase, $arrintTestIds = NULL ) {
		if( false == valArr( $arrintEmployeeIds ) ) {
			return NULL;
		}

		$strWhere = NULL;

		if( true == valArr( $arrintTestIds ) ) {
			$strWhere .= ' AND test_id IN ( ' . implode( ',', $arrintTestIds ) . ' )';
		}

		$strSql = ' SELECT
						submission_data.*
					FROM
						(
							SELECT
								test_id,
								employee_id,
								CASE
									WHEN
										score LIKE \'%/%\'
									THEN
										( ( CAST ( split_part( score, \'/\', 1 ) as FLOAT ) ) * 100 ) / ( CAST ( split_part ( score, \'/\', 2 ) as FLOAT ) )::FLOAT
								END AS score,
								row_number ( ) over ( partition BY test_id, employee_id
														ORDER BY
													 id DESC ) AS rank
							FROM
								test_submissions
							WHERE
								employee_id IN ( ' . implode( ',', $arrintEmployeeIds ) . ' ) ' . $strWhere . '
						) submission_data
					WHERE
						submission_data.rank = 1';

		return fetchData( $strSql, $objDatabase );
	}

}
?>