<?php

class CReportPermission extends CBaseReportPermission {

	const PRIMARY_OWNER_PERMISSION		= 1;
	const SECONDARY_OWNER_PERMISSION	= 2;
	const SHARE_PERMISSION				= 3;

	public static $c_arrintViewPermissions	= array( self::PRIMARY_OWNER_PERMISSION, self::SECONDARY_OWNER_PERMISSION, self:: SHARE_PERMISSION );
	public static $c_arrintEditPermissions	= array( self::PRIMARY_OWNER_PERMISSION, self::SECONDARY_OWNER_PERMISSION );

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valCaReportId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDepartmentId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valEmployeeId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valGroupId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsPrimaryOwner() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valIsSecondaryOwner() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valPrimarySecondaryOwner( $intPrimaryOwner, $arrintSecondaryOwner ) {
		$boolIsValid = true;

		if( true == valArr( $arrintSecondaryOwner ) && true == in_array( $intPrimaryOwner, $arrintSecondaryOwner ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_primary_secondary_owner', 'Secondary owner  must be different than primary owner .' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function valPrimaryViewableEmployee( $intPrimaryOwner, $arrintReportViewableEmployeeIds ) {
		$boolIsValid = true;

		if( true == valArr( $arrintReportViewableEmployeeIds ) && true == in_array( $intPrimaryOwner, $arrintReportViewableEmployeeIds ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'report_primary_viewable_employee', 'Viewable employee must be different than primary owner .' ) );
			return $boolIsValid;
		}

		return $boolIsValid;
	}

	public function validate( $strAction,$intPrimaryOwner, $arrintSecondaryOwner, $arrintReportViewableEmployeeIds ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
				$boolIsValid &= $this->valPrimarySecondaryOwner( $intPrimaryOwner, $arrintSecondaryOwner );
				break;

			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>