<?php

class CPersonSalesRole extends CBasePersonSalesRole {

	const DECISION_MAKER 	= 1;
	const USER_BUYER 		= 2;
	const TECH_BUYER 		= 3;
	const COACH				= 4;
	const ANTAGONIST		= 5;

	public static $c_arrstrPersonSalesRoles = [
		self::DECISION_MAKER => 'Decision Maker',
		self::USER_BUYER => 'User Buyer',
		self::TECH_BUYER => 'Tech Buyer',
		self::COACH => 'Coach',
		self::ANTAGONIST => 'Detractor'
	];

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
		}

		return $boolIsValid;
	}

}
?>