<?php

class COkrObjective extends CBaseOkrObjective {

	public function valId() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valOkrTeamId() {
		$boolIsValid = true;
		if( false == valId( $this->getOkrTeamId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'okr_team_id', 'OKR team id is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;
		if( false == valStr( $this->getDescription() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Objective description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valQuarter() {
		$boolIsValid = true;
		if( false == valStr( $this->getQuarter() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'quarter', 'Objective Quarter is required.' ) );
		}

		return $boolIsValid;
	}

	public function valYear() {
		$boolIsValid = true;
		if( false == valId( $this->getYear() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'year', 'Objective year is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDeletedBy() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function valDeletedOn() {
		$boolIsValid = true;
		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valOkrTeamId();
				$boolIsValid &= $this->valDescription();
				$boolIsValid &= $this->valQuarter();
				$boolIsValid &= $this->valYear();
				break;

			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

}
?>
