<?php

class CTest extends CBaseTest {

	const ID_PHP_NINJA								= 535;
	const ID_PHP_SAMURAI							= 537;
	const ID_PHP_NINJA_TEST							= 545;
	const ID_PHP_JEDI								= 595;
	const ID_XENTO_MANUAL_TEST						= 579;
	const ID_ASSOCIATE_POSTGRESQL					= 639;
	const ID_XMT_BASIC								= 876;
	const ID_FRONTEND_CRTIFICATION					= 891;
	const ID_FRONTEND_SAMURAI						= 978;


	protected $m_strScore;
	protected $m_strCreatedByName;
	protected $m_intTestSubmissionId;
	protected $m_intGradedBy;
	protected $m_intQuestionCount;
	protected $m_boolIsRestore;
	protected $m_intTestId;
	protected $m_intTrainingSessionId;
	protected $m_strTrainingSessionName;

	public static $c_arrintMandatoryQuestionIds = [ 28155, 28156, 28157, 31338, 31339, 31340 ];

	public static $c_arrintPhpCertificationTests = [
		self::ID_PHP_NINJA,
		self::ID_PHP_SAMURAI,
		self::ID_PHP_NINJA_TEST,
		self::ID_PHP_JEDI,
		self::ID_FRONTEND_CRTIFICATION
	];

	public static $c_arrintCertificationTests = [
		self::ID_PHP_NINJA,
		self::ID_PHP_SAMURAI,
		self::ID_PHP_JEDI,
		self::ID_XENTO_MANUAL_TEST,
		self::ID_XMT_BASIC,
		self::ID_ASSOCIATE_POSTGRESQL,
		self::ID_FRONTEND_CRTIFICATION
	];

	public static $c_arrintQaCertificationTests = [
		self::ID_XENTO_MANUAL_TEST,
		self::ID_XMT_BASIC,
	];

	public static $c_arrintDbaCertificationTests = [
		self::ID_ASSOCIATE_POSTGRESQL
	];

	public static $c_arrintTestsContainsPracticalQuestions = [
		self::ID_FRONTEND_CRTIFICATION,
		self::ID_FRONTEND_SAMURAI
	];

	public function __construct() {
		parent::__construct();

		$this->m_boolIsRestored = false;
		return;
	}

	/**
	 * Get Functions
	 *
	 */

	public function getIsRestored() {
		return $this->m_boolIsRestored;
	}

	public function getTestSubmissionId() {
		return $this->m_intTestSubmissionId;
	}

	public function getGradedBy() {
		return $this->m_intGradedBy;
	}

	public function getScore() {
		return $this->m_strScore;
	}

	public function getQuestionCount() {
		return $this->m_intQuestionCount;
	}

	public function getCreatedByName() {
		return $this->m_strCreatedByName;
	}

	public function getTestId() {
		return $this->m_intTestId;
	}

	public function getTrainingSessionId() {
		return $this->m_intTrainingSessionId;
	}

	public function getTrainingSessionName() {
		return $this->m_strTrainingSessionName;
	}

	/**
	 * Set Functions
	 *
	 */

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes, $boolDirectSet );

		if( isset( $arrmixValues['test_submission_id'] ) && $boolDirectSet ) $this->m_intTestSubmissionId = trim( $arrmixValues['test_submission_id'] );
		elseif ( isset( $arrmixValues['test_submission_id'] ) ) $this->setTestSubmissionId( $arrmixValues['test_submission_id'] );
		if( isset( $arrmixValues['graded_by'] ) && $boolDirectSet ) $this->m_intGradedBy = trim( $arrmixValues['graded_by'] );
		elseif ( isset( $arrmixValues['graded_by'] ) ) $this->setGradedBy( $arrmixValues['graded_by'] );
		if( isset( $arrmixValues['score'] ) && $boolDirectSet ) $this->m_strScore = trim( $arrmixValues['score'] );
		elseif ( isset( $arrmixValues['score'] ) ) $this->setScore( $arrmixValues['score'] );
		if( isset( $arrmixValues['question_count'] ) && $boolDirectSet ) $this->m_intQuestionCount = trim( $arrmixValues['question_count'] );
		elseif ( isset( $arrmixValues['question_count'] ) ) $this->setQuestionCount( $arrmixValues['question_count'] );
		if( isset( $arrmixValues['created_by_name'] ) && $boolDirectSet ) $this->m_strCreatedByName = trim( $arrmixValues['created_by_name'] );
		elseif ( isset( $arrmixValues['created_by_name'] ) ) $this->setCreatedByName( $arrmixValues['created_by_name'] );
		if( isset( $arrmixValues['test_id'] ) && $boolDirectSet ) $this->m_intTestId = trim( $arrmixValues['test_id'] );
		elseif ( isset( $arrmixValues['test_id'] ) ) $this->setTestId( $arrmixValues['test_id'] );
		if( isset( $arrmixValues['training_session_id'] ) && $boolDirectSet ) $this->m_intTrainingSessionId = trim( $arrmixValues['training_session_id'] );
		elseif ( isset( $arrmixValues['training_session_id'] ) ) $this->setTrainingSessionId( $arrmixValues['training_session_id'] );
		if( isset( $arrmixValues['training_session_name'] ) && $boolDirectSet ) $this->m_strTrainingSessionName = trim( $arrmixValues['training_session_name'] );
		elseif ( isset( $arrmixValues['training_session_name'] ) ) $this->setTrainingSessionName( $arrmixValues['training_session_name'] );
		return;
	}

	public function setIsRestored( $boolIsRestored ) {
		$this->m_boolIsRestored = $boolIsRestored;
	}

	public function setTestSubmissionId( $intTestSubmissionId ) {
		$this->m_intTestSubmissionId = $intTestSubmissionId;
	}

	public function setGradedBy( $intGradedBy ) {
		$this->m_intGradedBy = $intGradedBy;
	}

	public function setScore( $strScore ) {
		$this->m_strScore = $strScore;
	}

	public function setQuestionCount( $intQuestionCount ) {
		$this->m_intQuestionCount = $intQuestionCount;
	}

	public function setCreatedByName( $strCreatedByName ) {
		$this->m_strCreatedByName = $strCreatedByName;
	}

	public function setTestId( $intTestId ) {
		$this->m_intTestId = $intTestId;
	}

	public function setTrainingSessionId( $intTrainingSessionId ) {
		$this->m_intTrainingSessionId = $intTrainingSessionId;
	}

	public function setTrainingSessionName( $strTrainingSessionName ) {
		$this->m_strTrainingSessionName = $strTrainingSessionName;
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function fetchTestGroups( $objDatabase ) {
		return CTestGroups::fetchTestGroupsByTestId( $this->getId(), $objDatabase );
	}

	/**
	 * Create Functions
	 *
	 */

	public function createEmployeeTest( $intEmployeeId ) {

		$objEmployeeTest = new CEmployeeTest();
		$objEmployeeTest->setTestId( $this->getId() );
		$objEmployeeTest->setEmployeeId( $intEmployeeId );

		return $objEmployeeTest;
	}

	public function createTestQuestionAssociation( $intQuestionId ) {

		$objTestsQuestionAssociation = new CTestQuestionAssociation();
		$objTestsQuestionAssociation->setTestId( $this->getId() );
		$objTestsQuestionAssociation->setTestQuestionId( $intQuestionId );

		return $objTestsQuestionAssociation;
	}

	/**
	 * Validation Functions
	 *
	 */

	public function valName( $objDatabase, $boolIsUpdate ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );
		}

		if( true == $boolIsValid && true == isset( $objDatabase ) ) {
			if( true == $boolIsUpdate ) {
				$objTest = CTests::fetchTestById( $this->getId(), $objDatabase );
				if( true == valObj( $objTest, 'CTest' ) && $this->getName() == $objTest->getName() ) {
					return $boolIsValid;
				}
			}

			$strSql = ' WHERE name = \'' . trim( addslashes( $this->m_strName ) ) . '\' AND id <> ' . ( int ) $this->m_intId;
			$intCount = CTests::fetchRowCount( $strSql, 'tests', $objDatabase );

			if( 0 < $intCount ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is already in use.' ) );
			}
		}

		return $boolIsValid;
	}

	public function valNotifyEmailAddress() {
		$boolIsValid = true;

		if( true == isset ( $this->m_strNotifyEmailAddress ) && 0 < \Psi\CStringService::singleton()->strlen( trim( $this->m_strNotifyEmailAddress ) ) && false == CValidation::validateEmailAddresses( $this->m_strNotifyEmailAddress ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'notify_email_address', 'Notify email address is not valid.' ) );
		}

		return $boolIsValid;
	}

	public function valTestLevelTypeId() {
		$boolIsValid = true;

		if( true == is_null( $this->getTestLevelTypeId() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'test_level_type_id', 'Test level is required.' ) );
		}

		return $boolIsValid;
	}

	public function valDescription() {
		$boolIsValid = true;

		if( 0 >= \Psi\CStringService::singleton()->strlen( $this->m_strDescription ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'description', 'Description is required.' ) );
		}

		return $boolIsValid;
	}

	public function valMaxQuestions() {
		$boolIsValid = true;

		if( true == is_null( $this->getMaxQuestions() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_questions', 'Max question is required.' ) );
		} elseif( 0 == $this->getMaxQuestions() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_questions', 'Max question should be greater than zero .' ) );
		} elseif( 99 < $this->getMaxQuestions() ) {

			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'max_questions', 'Max question should be less than hundred .' ) );
		}
		return $boolIsValid;
	}

	public function valPassingPercent() {
		$boolIsValid = true;

		if( true == isset( $this->m_fltPassingPercentage ) && true == is_numeric( $this->m_fltPassingPercentage ) && ( 1 > $this->m_fltPassingPercentage || 100 < $this->m_fltPassingPercentage ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'passing_percentage', 'Passing percentage should be in between 1 to 100.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
		  		$boolIsValid &= $this->valName( $objDatabase, false );
				$boolIsValid &= $this->valDescription( $objDatabase );
				$boolIsValid &= $this->valMaxQuestions();
				$boolIsValid &= $this->valTestLevelTypeId();
				$boolIsValid &= $this->valNotifyEmailAddress();
				$boolIsValid &= $this->valPassingPercent();
				break;

			case 'validate_test_copy':
				$boolIsValid &= $this->valName( $objDatabase, false );
			   	$boolIsValid &= $this->valDescription( $objDatabase );
			   	$boolIsValid &= $this->valNotifyEmailAddress();
			   	break;

			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase, true );
				$boolIsValid &= $this->valDescription( $objDatabase );
				$boolIsValid &= $this->valTestLevelTypeId();
				$boolIsValid &= $this->valNotifyEmailAddress();
				$boolIsValid &= $this->valMaxQuestions();
				$boolIsValid &= $this->valPassingPercent();
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	/**
	 * Delete Functions
	 *
	 */

	public function delete( $intCurrenUserId, $objDataBase, $boolReturnSqlOnly = false ) {

		if( false == $this->getIsRestored() ) {
			$this->setDeletedBy( $intCurrenUserId );
		} else {
			$this->setDeletedBy( NULL );
		}
		$this->setDeletedOn( 'NOW()' );
		$this->setUpdatedBy( $intCurrenUserId );
		$this->setUpdatedOn( 'NOW()' );

		if( $this->update( $intCurrenUserId, $objDataBase, $boolReturnSqlOnly ) ) return true;
	}

}

?>