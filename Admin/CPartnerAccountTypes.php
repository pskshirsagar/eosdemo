<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPartnerAccountTypes
 * Do not add any new functions to this class.
 */

class CPartnerAccountTypes extends CBasePartnerAccountTypes {

	public static function fetchPartnerAccountTypes( $strSql, $objDatabase ) {
		return self::fetchCachedObjects( $strSql, 'CPartnerAccountType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchPartnerAccountType( $strSql, $objDatabase ) {
		return self::fetchCachedObject( $strSql, 'CPartnerAccountType', $objDatabase, DATA_CACHE_MEMORY, NULL, false );
	}

	public static function fetchAllPartnerAccountType( $objDatabase ) {
		$strSql = 'SELECT * FROM partner_account_types order by id ASC;';

		return self::fetchPartnerAccountTypes( $strSql, $objDatabase );
	}

}
?>