<?php

class CIntegrationUtilityReleasedVersion extends CBaseIntegrationUtilityReleasedVersion {

	const YARDI_CLIENT		= 2;
	const YARDI_GENESIS		= 21;
	const YARDI_ENTERPRIZE	= 22;

	public function valReleasedVersion() {
		$boolIsValid = true;

		if( true == is_null( $this->getReleasedVersion() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, ' Released version is required.' ) );
		} elseif( false == preg_match( '!^[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+$!', $this->getReleasedVersion() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, NULL, ' Incorrect version format.' ) );
		}

		return $boolIsValid;
	}

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valReleasedVersion();
				break;

			case VALIDATE_DELETE:
				break;

			default:
			$boolIsValid = false;
		}

		return $boolIsValid;
	}
}
?>