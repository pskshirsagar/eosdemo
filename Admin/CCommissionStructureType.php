<?php

class CCommissionStructureType extends CBaseCommissionStructureType {

	const NON_CASCADING 					= 1;
	const CASCADING 						= 2;
	const SALES_CLOSER_CONTRACT_BASED		= 3;
	const RESIDENT_INSURE_AGENT             = 4;
	const RESIDENT_INSURE_MANAGER           = 5;

	public static $c_arrintCommissionStructureTypesExcludedFromClawBack = [ self::SALES_CLOSER_CONTRACT_BASED ];
}
?>