<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CPropertyKeyValues
 * Do not add any new functions to this class.
 */

class CPropertyKeyValues extends CBasePropertyKeyValues {

	public static function fetchPropertyKeyValuesByWebsiteId( $intWebsiteId, $intCid, $objAdminDatabase, $objClientDatabase ) {
		$arrintPropertyIdsData = \Psi\Eos\Entrata\CWebsiteProperties::createService()->fetchPropertyIdsByWebsiteIdByCid( $intWebsiteId, $intCid, $objClientDatabase );

		foreach( $arrintPropertyIdsData as $arrobjPropertyIdData ) {
			$arrintPropertyIds[$arrobjPropertyIdData['property_id']] = $arrobjPropertyIdData['property_id'];
		}

		$strSql = '	SELECT
						cpkv.*
					FROM
						property_key_values cpkv
					WHERE
						cpkv.property_id IN ( ' . implode( ',', $arrintPropertyIds ) . ')';

		return self::fetchPropertyKeyValues( $strSql, $objAdminDatabase );
	}

}
?>