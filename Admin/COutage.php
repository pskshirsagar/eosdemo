<?php

class COutage extends CBaseOutage {

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = false;
				break;
		}

		return $boolIsValid;
	}

	public function delete( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly = false ) {
		$this->setDeletedBy( $intCurrentUserId );
		$this->setDeletedOn( date( 'm/d/Y H:i:s' ) );

		$this->update( $intCurrentUserId, $objDatabase, $boolReturnSqlOnly );

		return true;
	}

	public static function createOutage( $objOutage, $objTask, $objUser ) {

		if( false == valObj( $objOutage, 'COutage' ) ) {
			$objOutage = new COutage();
		}

		$arrmixOutageDetails = json_encode( array(
			'ps_products'        => array(),
			'ps_product_options' => array(),
			'client_databases'   => array(),
			'clients'            => array(),
			'cluster_id'         => array()
		) );

		$objOutage->setTaskId( $objTask->getId() );
		$objOutage->setCompletedBy( $objUser->getId() );
		$objOutage->setCompletedOn( 'NOW()' );
		$objOutage->setOutageDetails( $arrmixOutageDetails );
		return $objOutage;
	}

}
?>