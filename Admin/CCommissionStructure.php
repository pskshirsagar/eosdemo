<?php

use Psi\Eos\Admin\CCommissionStructures;
use Psi\Eos\Admin\CCommissionRates;
use Psi\Eos\Admin\CCommissionStructureTypes;

class CCommissionStructure extends CBaseCommissionStructure {

	const PRODUCT_COMMISSION = 228;

	protected $m_arrobjCommissionRates;
	protected $m_arrobjChargeCodes;

	// This variable tells us that the commission structure could be contained within another commission structure.
	// We primarily needed it for migrating the old commission system, but may be used also in validating that the same type of
	// commission structure already exists while adding a new structure.
	protected $m_boolIsContainable;

	// This variable tells us the number of accounts / clients this commission structure has been applied to.
	protected $m_intAppliedCount;

	public function __construct() {
		parent::__construct();

		$this->m_intAppliedCount = 0;

		return;
	}

	public function setDefaults() {

		$this->setDefaultPeriodMonths( 48 );
		return;
	}

	/**
	 * Create Functions
	 *
	 */

	public function createCommissionRate() {

		$objCommissionRate = new CCommissionRate();
		$objCommissionRate->setCommissionStructureId( $this->getId() );
		$objCommissionRate->setCommissionPercent( 0 );

		return $objCommissionRate;
	}

	public static function createContractCommissionStructure() {

		$objCommissionStructure = new CCommissionStructure();
		$objCommissionStructure->setDefaults();
		$objCommissionStructure->setIsContractBased( 1 );

		return $objCommissionStructure;
	}

	/**
	 * Validate Functions
	 *
	 */

	public function valName( $objDatabase ) {
		$boolIsValid = true;

		if( true == is_null( $this->getName() ) ) {
			$boolIsValid = false;
			$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name is required.' ) );

		} else {

			$arrobjCommissionStrutures = CCommissionStructures::createService()->fetchCommissionStructuresByName( addslashes( trim( $this->m_strName ) ), $objDatabase );

			if( false == is_null( $this->getId() ) && true == valArr( $arrobjCommissionStrutures ) ) {
				foreach( $arrobjCommissionStrutures as $intKey => $objCommissionStructure ) {
					if( $this->getId() == $objCommissionStructure->getId() ) {
						unset( $arrobjCommissionStrutures[$intKey] );
						break;
					}
				}
			}

			if( true == valArr( $arrobjCommissionStrutures ) ) {
				$boolIsValid = false;
				$this->addErrorMsg( new CErrorMsg( ERROR_TYPE_VALIDATION, 'name', 'Name already exists.' ) );
			}
		}

		return $boolIsValid;
	}

	public function validate( $strAction, $objDatabase = NULL ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
				$boolIsValid &= $this->valName( $objDatabase );
				break;

			case VALIDATE_DELETE:
				break;

			default:
				// default case
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

	public function getOrFetchCommissionRates( $objDatabase ) {

		if( false == valArr( $this->m_arrobjCommissionRates ) ) {
			$this->m_arrobjCommissionRates = $this->fetchCommissionRates( $objDatabase );
		}

		return $this->m_arrobjCommissionRates;
	}

	public function getCommissionRates() {
		return $this->m_arrobjCommissionRates;
	}

	public function getIsContainable() {
		return $this->m_boolIsContainable;
	}

	public function getChargeCodes() {
		return $this->m_arrobjChargeCodes;
	}

	public function getAppliedCount() {
		return $this->m_intAppliedCount;
	}

	public function setCommissionRates( $objCommissionRate ) {
		$this->m_arrobjCommissionRates[$objCommissionRate->getId()] = $objCommissionRate;
	}

	public function setChargeCodes( $objChargeCode ) {
		$this->m_arrobjChargeCodes[$objChargeCode->getId()] = $objChargeCode;
	}

	public function setIsContainable( $boolIsContainable ) {
		$this->m_boolIsContainable = $boolIsContainable;
	}

	public function setAppliedCount( $intAppliedCount ) {
		$this->m_intAppliedCount = $intAppliedCount;
	}

	public function setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet = false ) {
		parent::setValues( $arrmixValues, $boolStripSlashes = true, $boolDirectSet );

		if( true == isset( $arrmixValues['applied_count'] ) ) {
			$this->setAppliedCount( $arrmixValues['applied_count'] );
		}

		return;
	}

	public function fetchCommissionRates( $objDatabase ) {
		return CCommissionRates::createService()->fetchActiveCommissionRatesByCommissionStructureId( $this->getId(), $objDatabase );
	}

	public function fetchCommissionStructureType( $objDatabase ) {
		return CCommissionStructureTypes::createService()->fetchCommissionStructureTypeById( $this->getCommissionStructureTypeId(), $objDatabase );
	}

	public function fetchContainableCommissionStructures( $objDatabase ) {
		return CCommissionStructures::createService()->fetchContainableCommissionStructuresByCommissionStructure( $this, $objDatabase );
	}

	/**
	 * Fetch Functions
	 *
	 */

	public function determineIsContainable( $objDatabase ) {
		$arrobjContainableCommissionStructures = $this->fetchContainableCommissionStructures( $objDatabase );
		if( true == valArr( $arrobjContainableCommissionStructures ) ) {
			$this->m_boolIsContainable = true;
		}
	}

	/**
	 * Other Functions
	 *
	 */

	public function delete( $intUserId, $objDatabase, $boolIsHardDelete = false ) {

		if( true == $boolIsHardDelete ) {
			return parent::delete( $intUserId, $objDatabase );
			exit;
		}

		$this->setDeletedBy( $intUserId );
		$this->setDeletedOn( ' NOW() ' );

		return $this->update( $intUserId, $objDatabase );
	}

}
?>