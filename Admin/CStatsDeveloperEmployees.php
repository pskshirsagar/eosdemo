<?php

/**
 * EOS CUSTOM PLURAL CLASS
 * @deprecated This class is deprecated.
 * @see \Psi\Eos\Admin\CStatsDeveloperEmployees
 * Do not add any new functions to this class.
 */

class CStatsDeveloperEmployees extends CBaseStatsDeveloperEmployees {

	public static function fetchDeveloperEmployeeStatsStackRank( $objStdFilter, $objDatabase ) {

		$strSql = '
				SELECT *
				FROM (
					SELECT
						totals.employee_id,
						totals.date_started,
						totals.preferred_name,
						totals.num_employees,
						totals.is_manager,
						totals.story_points_released,
						calculate_grade( CASE
											WHEN totals.story_points_released > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.story_points_released ) ) * 10 )
											ELSE 1
										END::INTEGER ) AS story_points_released_grade,
						totals.bug_severity::numeric( 15, 1 ),
						calculate_grade( CASE
											WHEN totals.bug_severity > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.bug_severity DESC ) ) * 10 )
											ELSE 10
										END::INTEGER ) AS bug_severity_grade,
						totals.bugs_resolved,
						calculate_grade( CASE
											WHEN totals.bugs_resolved > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.bugs_resolved ) ) * 10 )
											ELSE 1
										END::INTEGER ) AS bugs_resolved_grade,
						totals.features_released,
						calculate_grade( CASE
											WHEN totals.features_released > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.features_released ) ) * 10 )
											ELSE 1
										END::INTEGER ) AS features_released_grade,
						totals.qa_rejections,
						calculate_grade( CASE
											WHEN totals.qa_rejections > 0 AND totals.num_employees > 0 THEN ( ( cume_dist( ) OVER( PARTITION BY totals.is_manager ORDER BY totals.qa_rejections DESC ) ) * 10 )
											ELSE 10
										END::INTEGER ) AS qa_rejections_grade
					FROM (
						SELECT
							COALESCE( managers.manager_emp_id, managers.employee_id ) AS employee_id,
							e.preferred_name,
							e.date_started,
							COUNT( DISTINCT managers.employee_id ) AS num_employees,
							managers.is_manager,
							CASE
								WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sde.story_points_released ) / COUNT( DISTINCT managers.employee_id )
								ELSE 0
							END AS story_points_released,
							CASE
								WHEN SUM( sde.bugs_caused ) > 0 THEN ( SUM( sde.bug_severity * sde.bugs_caused ) / SUM( sde.bugs_caused ) )
								ELSE 0
							END AS bug_severity,
							CASE
								WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sde.bugs_resolved ) / COUNT( DISTINCT managers.employee_id )
								ELSE 0
							END AS bugs_resolved,
							CASE
								WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sde.features_released ) / COUNT( DISTINCT managers.employee_id )
								ELSE 0
							END AS features_released,
							CASE
								WHEN COUNT( DISTINCT managers.employee_id ) > 0 THEN SUM( sde.qa_rejections ) / COUNT( DISTINCT managers.employee_id )
								ELSE 0
							END AS qa_rejections
						FROM (
							SELECT
								( get_all_parent_managers_from_employee( e.id ) ) . manager_emp_id,
								e.id as employee_id,
								1 AS is_manager
							FROM
								employees e
							WHERE
								e.department_id IN ( ' . implode( ',', CDepartment::$c_arrintStoryPointsVisibleDepartmentIds ) . ' )
							UNION ALL
							SELECT
								NULL AS manager_emp_id,
								e.id as employee_id,
								0 AS is_manager
							FROM
								employees e
							WHERE
								e.department_id IN ( ' . implode( ',', CDepartment::$c_arrintStoryPointsVisibleDepartmentIds ) . ' )
						) AS managers
							JOIN stats_developer_employees sde ON ( managers.employee_id = sde.employee_id )
							JOIN employees e ON ( COALESCE( managers.manager_emp_id, managers.employee_id ) = e.id )
							JOIN departments d ON ( e.department_id = d.id )
						WHERE
							sde.week > ( NOW() - INTERVAL \'90 days\' )
							AND sde.week >= \'04/01/2014\'
							AND e.department_id IN ( ' . implode( ',', CDepartment::$c_arrintStoryPointsVisibleDepartmentIds ) . ' )
							AND e.employee_status_type_id = ' . CEmployeeStatusType::CURRENT . '
							AND e.office_id IN ( ' . implode( ',', COffice::$c_arrintIndiaOfficeIds ) . ' )
						GROUP BY
							COALESCE( managers.manager_emp_id, managers.employee_id ),
							managers.is_manager,
							e.preferred_name,
							e.date_started,
							d.name
					) AS totals
					ORDER BY totals.is_manager, ' . ( ( false == valStr( $objStdFilter->sort_by ) ) ? 'employee_id' : $objStdFilter->sort_by . ' ' . $objStdFilter->sort_direction ) . '
				) AS full_result_set
				WHERE is_manager = ' . ( ( true == valStr( $objStdFilter->is_manager ) ) ? ( int ) $objStdFilter->is_manager : 0 ) . '
				' . ( ( true == valStr( $objStdFilter->employees ) ) ? ' AND employee_id IN ( ' . $objStdFilter->employees . ' ) ' : '' ) . ';';

		return fetchData( $strSql, $objDatabase );
	}

	public static function rebuildDeveloperEmployeeStats( $intDeveloperEmployeeId, $objAdminDatabase, $objLogsDatabase ) {

		set_time_limit( 600 );
		$objLogsDatabase->open();
		if( true == is_numeric( $intDeveloperEmployeeId ) ) {
			$strDeveloperEmployeeCondition = ' = ' . $intDeveloperEmployeeId;
			$strDeveloperUserCondition = ' = ' . CUsers::fetchUserByEmployeeId( $intDeveloperEmployeeId, $objAdminDatabase )->getId();
		} else {
			$strDeveloperEmployeeCondition = ' <> 0 ';
			$strDeveloperUserCondition = ' <> 0 ';
		}

		$objAdminDatabase->begin();
		if( false === fetchData( self::buildDeveloperEmployeeStatsInsertSql( $strDeveloperEmployeeCondition ), $objAdminDatabase ) ) {
			$objAdminDatabase->rollback();
			return false;
		}
		$objAdminDatabase->commit();

		$strSql = self::buildDeveloperEmployeeTasksSql( $strDeveloperEmployeeCondition );
		$strSql .= self::buildDeveloperEmployeeSvnSql( $strDeveloperEmployeeCondition, $strDeveloperUserCondition, $objLogsDatabase );
		$strSql .= self::buildDeveloperEmployeeQaRejectionsSql( $strDeveloperEmployeeCondition );
		$strSql .= self::buildDeveloperEmployeeBugSeveritySql( $strDeveloperEmployeeCondition );
		$strSql .= self::buildDeveloperEmployeeHourSql( $strDeveloperEmployeeCondition );

		return fetchData( $strSql, $objAdminDatabase );
	}

	public static function buildDeveloperEmployeeStatsInsertSql( $strDeveloperEmployeeCondition ) {

		$strSql = '
					DELETE FROM stats_developer_employees WHERE employee_id ' . $strDeveloperEmployeeCondition . ';

					INSERT INTO
						public.stats_developer_employees ( id, employee_id, week, created_by, created_on )

						SELECT
							nextval ( \'stats_developer_employees_id_seq\' ) AS id,
							de.employee_id,
							d.week,
							1 AS created_by,
							NOW () AS created_on
						FROM
							(   SELECT DISTINCT td.developer_employee_id employee_id,
										e.date_started,
										COALESCE( e.date_terminated, now() )::date date_terminated
								FROM tasks t
									JOIN task_details td ON td.task_id = t.id
									JOIN employees e ON e.id = td.developer_employee_id
									--JOIN employee_addresses ea ON ea.employee_id = e.id
										--AND ea.address_type_id = ' . CAddressType::PRIMARY . '
										--AND ea.country_code = \'' . CCountry::CODE_INDIA . '\'
								WHERE td.developer_employee_id IS NOT NULL
									AND e.department_id IN ( ' . implode( ',', CDepartment::$c_arrintStoryPointsVisibleDepartmentIds ) . ' )
									AND e.id <> ' . CEmployee::ID_ADMIN . '
							) AS de
							JOIN ( SELECT DISTINCT DATE_TRUNC ( \'week\', d.date ) AS week
									FROM dates d
								) d ON ( DATE_TRUNC ( \'week\', d.week ) <= DATE_TRUNC ( \'week\', NOW ( ) ) )
						WHERE
							( d.week BETWEEN de.date_started AND de.date_terminated )
							AND de.employee_id' . $strDeveloperEmployeeCondition . ';
				';

		return $strSql;
	}

	public static function buildDeveloperEmployeeTasksSql( $strDeveloperEmployeeCondition ) {

		$strSql = 'UPDATE
						stats_developer_employees sde
					SET
						story_points_released = data.story_points_released,
						tasks_released = data.tasks_released,
						bugs_resolved = data.bugs_resolved,
						features_released = data.features_released
					FROM (
							SELECT td.developer_employee_id employee_id,
								date_trunc( \'week\', tr.release_datetime )::date AS week,
								count(*) tasks_released,
								SUM( CASE WHEN t.task_type_id = ' . CTaskType::BUG . ' THEN 1 ELSE 0 END ) bugs_resolved,
								SUM( CASE WHEN t.task_type_id = ' . CTaskType::FEATURE . ' THEN 1 ELSE 0 END ) features_released,
								SUM( td.developer_story_points ) story_points_released
							FROM tasks t
								JOIN task_details td ON td.task_id = t.id
								JOIN task_releases tr ON tr.id = t.task_release_id
							WHERE td.developer_employee_id IS NOT NULL
								AND t.task_status_id IN ( ' . implode( ', ', CTaskStatus::$c_arrintCompletedTaskStatuses ) . ' )
								AND tr.release_datetime <= now()
							GROUP BY td.developer_employee_id,
									week
						) data
					WHERE
						sde.employee_id = data.employee_id
						AND sde.week = data.week
						AND sde.employee_id' . $strDeveloperEmployeeCondition . ';
					';

		return $strSql;
	}

	public static function buildDeveloperEmployeeSvnSql( $strDeveloperEmployeeCondition, $strDeveloperUserCondition, $objLogsDatabase ) {

		if( false == valObj( $objLogsDatabase, 'CDatabase' ) ) {
			return;
		}

		$strSql = '
			SELECT
				sub.user_id,
			    sub.week,
			    SUM( sub.total_line_insertions ) AS total_line_insertions,
			    SUM( sub.total_line_deletions ) AS total_line_deletions,
			    SUM( sub.files_added ) AS files_added,
				SUM( sub.files_deleted ) AS files_deleted,
				COUNT( sub.* ) AS svn_commits,
				SUM( sub.files_committed ) AS files_committed
			FROM (
			    SELECT
			        scl.id,
			        scl.user_id,
			        date_trunc( \'week\', scl.commit_datetime )::DATE AS week,
			        scl.total_line_insertions,
			        scl.total_line_deletions,
			        scl.files_added,
			        scl.files_deleted,
			        COUNT( sclf.* ) AS files_committed
			    FROM 
			        svn_commit_logs scl
			        LEFT JOIN svn_commit_log_files sclf ON sclf.svn_commit_log_id = scl.id
			    WHERE scl.user_id' . $strDeveloperUserCondition . '
			    GROUP BY
			        scl.id
		    ) sub
			GROUP BY
				sub.user_id,
				week
			ORDER BY
				sub.user_id,
				week DESC;
		';

		$arrmixSvnData = fetchData( $strSql, $objLogsDatabase );
		if( false == valArr( $arrmixSvnData ) ) {
			return;
		}

		$strSqlValues = '';

		foreach( $arrmixSvnData as $arrmixSvnDataRow ) {
			$arrmixSvnDataRow['week'] = '\'' . $arrmixSvnDataRow['week'] . '\'';

			if( 0 < strlen( $strSqlValues ) ) {
				$strSqlValues .= ',';
			}

			$strSqlValues .= '(' . implode( ',', $arrmixSvnDataRow ) . ')';
		}

		$strSql = '
				/* query to pull values from logs db' . $strSql . '
				*/';

		$strSql .= '
				CREATE TEMPORARY TABLE stats_svn (
					user_id INTEGER NOT NULL,
					week DATE NOT NULL,
					total_line_insertions INTEGER DEFAULT 0 NOT NULL,
					total_line_deletions INTEGER DEFAULT 0 NOT NULL,
					files_added INTEGER DEFAULT 0 NOT NULL,
					files_deleted INTEGER DEFAULT 0 NOT NULL,
					svn_commits INTEGER DEFAULT 0 NOT NULL,
					files_committed INTEGER DEFAULT 0 NOT NULL
				)
				ON COMMIT DROP;';

		if( 0 < strlen( $strSqlValues ) ) {
			$strSql .= '
				INSERT INTO stats_svn
				VALUES ' . $strSqlValues . ';';
		}

		$strSql .= '
				UPDATE
					stats_developer_employees sde
				SET
					total_line_insertions = data.total_line_insertions,
					total_line_deletions = data.total_line_deletions,
					files_added = data.files_added,
					files_deleted = data.files_deleted,
					svn_commits = data.svn_commits,
					files_committed = data.files_committed
				FROM (
						SELECT
							e.id AS employee_id,
							ss.week,
							ss.total_line_insertions,
							ss.total_line_deletions,
							ss.files_added,
							ss.files_deleted,
							ss.svn_commits,
							ss.files_committed
						FROM stats_svn ss
							JOIN users u on ss.user_id = u.id
							JOIN employees e ON e.id =  u.employee_id
					) data
				WHERE
					sde.employee_id = data.employee_id
					AND sde.week = data.week
					AND sde.employee_id' . $strDeveloperEmployeeCondition . ';';

		return $strSql;
	}

	public static function buildDeveloperEmployeeQaRejectionsSql( $strDeveloperEmployeeCondition ) {

		$strSql = '
				UPDATE
					stats_developer_employees sde
				SET
					qa_rejections = data.rejections
				FROM (
					SELECT
						td.developer_employee_id AS employee_id,
						qa_rejections.week,
						SUM( qa_rejections.rejections) AS rejections
					FROM
						tasks t
						JOIN task_details td ON td.task_id = t.id
						JOIN (
							SELECT
								task_id,
								DATE_TRUNC( \'week\', created_on )::date AS week,
								COUNT( ta_id ) AS rejections
							FROM (
								SELECT
									ta.task_id,
									ta.id AS ta_id,
									ta2.id AS ta_next_id,
									ta2.task_status_id,
									ta2.created_on,
									rank() OVER( PARTITION BY ta.task_id, ta.id ORDER BY ta2.id ) AS rank
								FROM
									task_assignments ta
									JOIN task_assignments ta2 ON ta.task_id = ta2.task_id AND ta2.id > ta.id
								WHERE
									ta.task_status_id IN( ' . CTaskStatus::READY_FOR_QA . ', ' . CTaskStatus::QA_IN_PROGRESS . ' )
									AND ta.task_type_id IN( ' . CTaskType::BUG . ', ' . CTaskType::FEATURE . ' )
							) AS next_assignment
							WHERE
								next_assignment.rank = 1
								AND next_assignment.task_status_id IN( ' . CTaskStatus::READY_FOR_DEV . ', ' . CTaskStatus::DEV_IN_PROGRESS . ' )
							GROUP BY
								task_id,
								DATE_TRUNC( \'week\', created_on )
						) AS qa_rejections ON qa_rejections.task_id = t.id
					WHERE
						td.developer_employee_id IS NOT NULL
					GROUP BY
						td.developer_employee_id,
						qa_rejections.week
				) data
				WHERE
					sde.employee_id = data.employee_id
					AND sde.week = data.week
					AND sde.employee_id' . $strDeveloperEmployeeCondition . ';';

		return $strSql;
	}

	public static function buildDeveloperEmployeeBugSeveritySql( $strDeveloperEmployeeCondition ) {

		$strSql = '
				UPDATE
					stats_developer_employees sde
				SET
					bug_severity = data.bug_severity,
					bugs_caused = data.bugs_caused
				FROM (
					SELECT
						e.id AS employee_id,
						date_trunc( \'week\', td.created_on )::DATE AS week,
						COALESCE( SUM( td.severity_count ), 0 ) AS bug_severity,
						SUM( CASE WHEN td.caused_by_svn_username IS NOT NULL THEN 1 ELSE 0 END ) AS bugs_caused
					FROM
						task_details td
						JOIN users u ON u.svn_username = td.caused_by_svn_username
						JOIN employees e ON e.id = u.employee_id
					GROUP BY e.id,
						date_trunc( \'week\', td.created_on )
				) data
				WHERE
					sde.employee_id = data.employee_id
					AND sde.week = data.week
					AND sde.employee_id' . $strDeveloperEmployeeCondition . ';';

		return $strSql;
	}

	public static function buildDeveloperEmployeeHourSql( $strDeveloperEmployeeCondition ) {

		$strSql = '
				UPDATE
					stats_developer_employees sde
				SET
					hours_worked = data.hours_worked
				FROM (
					SELECT
						eh.employee_id,
						date_trunc( \'week\', eh.date )::DATE AS week,
						COALESCE( SUM( EXTRACT( EPOCH FROM ( eh.end_datetime - eh.begin_datetime ) ) / 3600 ), 0 ) AS hours_worked
					FROM employee_hours eh
					WHERE eh.employee_hour_type_id = ' . CEmployeeHourType::FLOOR . '
					GROUP BY
						eh.employee_id,
						date_trunc( \'week\', eh.date )
				) data
				WHERE
					sde.employee_id = data.employee_id
					AND sde.week = data.week
					AND sde.employee_id' . $strDeveloperEmployeeCondition . ';';

		return $strSql;
	}

	public static function fetchQaRejectionCountAndRankByDesignationId( $intDesignationId, $strFromDate, $strToDate, $objDatabase ) {

		$strSql = 'SELECT
						sde.employee_id,
						SUM ( sde.qa_rejections ) AS qa_rejections,
						DENSE_RANK ( ) OVER (
						ORDER BY
						SUM ( sde.qa_rejections ) ASC ) AS qa_rejection_rank
					FROM
						stats_developer_employees sde
						JOIN employees e ON (e.id = sde.employee_id)
					WHERE
						sde.week > ( \'' . $strFromDate . '\' )::DATE AND sde.week < ( \'' . $strToDate . '\' )::DATE
						AND e.designation_id = ' . ( int ) $intDesignationId . '
					GROUP BY
						sde.employee_id';

		$arrstrData = fetchData( $strSql, $objDatabase );

		return $arrstrData;
	}

}
?>