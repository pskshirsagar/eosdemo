<?php

class CRequirementType extends CBaseRequirementType {

	const REPLACEMENT = 3;
	const VISA_PETITION = 5;
	const NEW_POSITION = 1;

	public function validate( $strAction ) {
		$boolIsValid = true;

		switch( $strAction ) {
			case VALIDATE_INSERT:
			case VALIDATE_UPDATE:
			case VALIDATE_DELETE:
				break;

			default:
				$boolIsValid = true;
				break;
		}

		return $boolIsValid;
	}

}
?>